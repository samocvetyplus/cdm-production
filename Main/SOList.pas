{***********************************************}
{  ������ ��������� ������� ��������������      }
{                                               }
{  Copyrigth (C) 2003-2004 basile for CDM       }
{  v0.22                                        }
{  create 14.04.2003                            }
{  last update 05.10.2004                       }
{***********************************************}

unit SOList;                                 

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, Menus, ImgList, StdCtrls,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, ComCtrls, db,
  DBGridEh, ActnList, TB2Item, xmldom, XMLIntf, msxmldom,
  XMLDoc, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar, Mask, DBCtrlsEh,
  FR_Desgn, FR_DSet, FR_DBSet, FR_Class, Printers, PrnDbgeh;

type
  TfmSOList = class(TfmListAncestor)
    acPrintAct: TAction;
    acPrintInv15: TAction;
    acExport: TAction;
    sdExport: TSaveDialog;
    tbsCreateSI: TTBSubmenuItem;
    lbComp: TLabel;
    cbComp: TDBComboBoxEh;
    PrintSOListGrid: TPrintDBGridEh;
    TBItem5: TTBItem;
    TBItem7: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem6: TTBItem;
    acUnionInvSOEl: TAction;
    procedure FormCreate(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintActExecute(Sender: TObject);
    procedure acPrintInv15Execute(Sender: TObject);
    procedure acPrintInv15Update(Sender: TObject);
    procedure acPrintActUpdate(Sender: TObject);
    procedure cbCompCloseUp(Sender: TObject; Accept: Boolean);
    procedure btnPrintClick(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure acUnionInvSOElExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  protected
    procedure DepClick(Sender : TObject); override;
    procedure acCreateSIExecute(Sender : TObject);
    procedure acCreateSIUpdate(Sender : TObject);
  end;

var
  fmSOList: TfmSOList;

implementation

uses MainData, DictData, SOEl, dbUtil, fmUtils, PrintData, MsgDialog, Data,
  InvData, InvCopy, PrintData2;

{$R *.dfm}


procedure TfmSOList.FormCreate(Sender: TObject);
var
  i,j : integer;
  Item : TMenuItem;
  FindDep : boolean;
  it : TTBItem;
  ac : TAction;
begin
  cbComp.OnCloseUp := nil;
  cbComp.Items.Clear;
  cbComp.Items.Assign(dm.dComp);
  cbComp.ItemIndex := 0;
  cbComp.OnCloseUp := cbCompCloseUp;
  dmMain.FSupname:=ROOT_KNAME_CODE;

  stbrStatus.Panels[1].Text := dm.User.wWhName;
  FCurrDep := dm.User.wWhId;
  FBD := dm.whBeginDate;
  FED := dm.whEndDate;
  
  j := 0;
  FindDep := False;
  for i:=0 to Pred(dm.CountDep) do
  begin
    if dm.DepInfo[i].PWh then
    begin
      if (FCurrDep = dm.DepInfo[i].DepId) then FindDep := True;
      Item := TMenuItem.Create(ppDep);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepClick;
      Item.RadioItem := True;
      inc(j);
      if j>30 then
      begin
        Item.Break := mbBreak;
        j:=0;
      end;
      ppDep.Items.Add(Item);
    end;
  end;
  if FindDep then
  begin
    dm.CurrDep := FCurrDep;
    laDep.Caption := dm.DepInfoById[FCurrDep].Name;
  end
  else begin
    FCurrDep := -1; 
    dm.CurrDep := -1;
    laDep.Caption := mnitAllWh.Caption;
  end;
  // ���������� ���� ��� ����������� � ����������� ����������
  ReOpenDataSet(dm.taCompLogin);

  while not dm.taCompLogin.Eof do
  begin
    if dm.taCompLoginCOMPID.AsInteger = dm.User.SelfCompId then
    begin
      dm.taCompLogin.Next;
      continue;
    end;
    ac := TAction.Create(acAEvent);
    ac.OnUpdate := acCreateSIUpdate;
    ac.OnExecute := acCreateSIExecute;
    ac.Caption := dm.taCompLoginNAME.AsString;
    ac.Tag := dm.taCompLoginCOMPID.AsInteger;
    it := TTBItem.Create(ppDoc);
    it.Action := ac;
    it.Caption := dm.taCompLoginNAME.AsString;
    it.Tag := dm.taCompLoginCOMPID.AsInteger;
    tbsCreateSI.Add(it);
    dm.taCompLogin.Next;
  end;

  inherited;

  if dm.User.wWhName <> '' then
  begin
    Self.Caption := Self.Caption + ' - ' + dm.User.wWhName;
  end;

  spitPrint.Enabled := false;
  spitPrint.Visible := false;
  
end;



procedure TfmSOList.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
if Key = VK_F6 then
    if dgMultiSelect in gridDocList.Options then
      begin
        gridDocList.Options := gridDocList.Options - [dgMultiSelect];
        MessageDialogA('������������� ����� ��������!', mtInformation);
      end
    else
      begin
        gridDocList.Options := gridDocList.Options + [dgMultiSelect];
        MessageDialogA('������������� ����� ��������!', mtInformation);
      end;
end;

procedure TfmSOList.DepClick(Sender: TObject);
begin
  FCurrDep := TMenuItem(Sender).Tag;
  dm.CurrDep := FCurrDep;
  inherited;
end;

procedure TfmSOList.acAddDocExecute(Sender: TObject);
begin

  if (FCurrDep=-1) then
  begin
    MessageDialogA('���������� ������� �����', mtInformation);
    eXit;
  end;

  inherited;

  with dmMain do
  begin
    PostDataSet(taSOList);
    ShowAndFreeForm(TfmSOEl, TForm(fmSOEl));
    PostDataSet(taSOList);
    RefreshDataSet(taSOList);
  end;

end;

procedure TfmSOList.acEditDocExecute(Sender: TObject);
begin
with dmMain do ShowAndFreeForm(tfmSOEl, TForm(fmSOEl));
{��� ������ � ������������� ��� ������ ����� Dataset. � ���� ���������
 �������� ��� ����������� ������������ ������������ INV_ID}
{  with dmMain do
  begin
    taSOList.Edit;
    ShowAndFreeForm(TfmSOEl, TForm(fmSOEl));
    taSOList.Transaction.CommitRetaining;
    RefreshDataSet(taSOList);
  end;}
end;

procedure TfmSOList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if AnsiUpperCase(Column.FieldName)='RECNO' then
    Background:= clMoneyGreen
  else if dmMain.taSOListISCLOSE.AsInteger=0 then
      Background:=clInfoBk
  else
  Background := clBtnFace;
end;

procedure TfmSOList.N1Click(Sender: TObject);
var Invs : string;
    i : integer;
begin
inherited;
 if (gridDocList.SelectedRows.Count > 1) then
    begin
      gridDocList.DataSource.dataSet.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[0]));
      Invs := gridDocList.DataSource.dataSet.FieldByName('INVID').AsString;
      dmPrint2.InvNumbers := gridDocList.DataSource.dataSet.FieldByName('DOCNO').AsString;
      for i := 1 to gridDocList.SelectedRows.Count - 1 do
        begin
          gridDocList.DataSource.dataSet.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[i]));
          Invs := Invs + ', ' + gridDocList.DataSource.dataSet.FieldByName('INVID').AsString;
          dmPrint2.InvNumbers := dmPrint2.InvNumbers + ', ' + gridDocList.DataSource.dataSet.FieldByName('DOCNO').AsString;
        end;
    end
  else
    begin
      Invs := gridDocList.DataSource.dataSet.FieldByName('INVID').AsString;
      dmPrint2.InvNumbers := gridDocList.DataSource.dataSet.FieldByName('DOCNO').AsString;
    end;
dmPrint2.SListToPrint(Invs);
end;

procedure TfmSOList.acPrintActExecute(Sender: TObject);
begin
  ShowMessage('������  ���� ������ �� ���������!');
//dmPrint.PrintDocumentA(dkSoel, VarArrayOf([VarArrayOf([dmMain.taSOListINVID.asinteger]),VarArrayOf([dmMain.taSOListINVID.Asinteger])]));
end;

procedure TfmSOList.acPrintInv15Execute(Sender: TObject);
begin
//  dmPrint.PrintDocumentA(dkSOList_Inv15, VarArrayOf([VarArrayOf([dmMain.taSOListINVID.asinteger]),VarArrayOf([dmMain.taSOListINVID.Asinteger])]));
//fmSOEl.taSumOrNotSOEl.Open;
//with dmPrint do
//  begin
//    frReport.Dataset:=frSumOrNotSOEl;
//    taDoc.Open;
//    frReport.LoadFromDB(taDoc, 2011);
//    frReport.DesignReport;
//    taDoc.Close;
//  end;
  ShowMessage('������ ��������� ������ �� ���������!')
end;

procedure TfmSOList.acPrintInv15Update(Sender: TObject);
begin
  acPrintInv15.Enabled := DataSet.Active and (not DataSet.IsEmpty)
     and (DataSet.FieldByName('SCHEMAID').AsInteger = 1);
end;



procedure TfmSOList.acUnionInvSOElExecute(Sender: TObject);
var
  RowIndex: Integer;
  CurrentInvID, DestInvID: String;
  BookMark: Pointer;
begin
  inherited;
  if dm.CurrDep = -1 then
    begin
      MessageDialog('����� �� ������!', mtWarning, [mbOK], 0);
      SysUtils.Abort;
    end;
  if gridDocList.Selectedrows.Count > 0 then
    begin
      Screen.Cursor := crSQLWait;
      BookMark := gridDocList.DataSource.DataSet.GetBookmark;

      with gridDocList.DataSource.DataSet do
        begin
          DisableControls;
          GotoBookMark(Pointer(gridDocList.Selectedrows.Items[0]));
          DestInvID := FieldByName('InvID').AsString;
          for RowIndex := 1 to gridDocList.Selectedrows.Count - 1 do
            begin
              GotoBookMark(Pointer(gridDocList.Selectedrows.Items[RowIndex]));
              CurrentInvID := FieldByName('InvID').AsString;
              try
                ExecSQL('execute procedure AddInvToSOEl(' + CurrentInvID + ', ' + DestInvID + ')', dmMain.quTmp, true);
              except
                on E: Exception do
                  begin
                    MessageDialog('������ ��� ���������� ��������� AddInvToSOEl:' + E.Message, mtError, [mbOK], 0);
                    Exit;
                    EnableControls;
                    Screen.Cursor := crDefault;
                  end;

              end;
            end;
          Close;
          Open;
          GotoBookmark(Pointer(BookMark));
          EnableControls;
        end;
      Screen.Cursor := crDefault;
    end
  else
    MessageDialog('�� ������� �� ����� ���������!', mtError, [mbOK], 0);
end;


procedure TfmSOList.btnPrintClick(Sender: TObject);
var Period: string;
begin
  inherited;
  Period := laPeriod.Caption;
  Delete(Period, 1, 2);
  if dmMain.taSOList.IsEmpty then
      ShowMessage('� ������� ������� ��� ��������� ��� ������!')
  else
    begin
      GridDocList.Columns[2].Visible := false;
      GridDocList.Columns[4].Visible := false;
      GridDocList.Columns[5].Visible := false;
      GridDocList.Columns[6].Visible := false;
      PrintSOListGrid.BeforeGridText.text := '������ ��������� ��������� � ' + UpperCase(Period) + ' (' + dmMain.taSOListSELFCOMPNAME.AsString + ')';
      Printer.Orientation := poLandscape;
      PrintSOListGrid.Preview;
      //PrintSOListGrid.Print;
      GridDocList.Columns[2].Visible := true;
      GridDocList.Columns[4].Visible := true;
      GridDocList.Columns[5].Visible := true;
      GridDocList.Columns[6].Visible := true;
    end;
end;

procedure TfmSOList.cbCompCloseUp(Sender: TObject; Accept: Boolean);
begin
  if cbComp.ItemIndex>0 then dmMain.FSupname := cbComp.Value
  else dmMain.FSupname := ROOT_KNAME_CODE;
  PostDataSet(dmMain.taSOList);
  ReOpenDataSet(dmMain.taSOList);
end;

procedure TfmSOList.acPrintActUpdate(Sender: TObject);
begin
  acPrintAct.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;


procedure TfmSOList.acPrintDocExecute(Sender: TObject);
begin
  inherited;
//
end;

procedure TfmSOList.acCreateSIExecute(Sender: TObject);
var
  c : TCopyInvInfo;
begin
  c.DefaultComp := (Sender as TAction).Tag;
  c.CanChangeComp := False;
  c.DefaultDep := -1;
  c.CanChangeDep := True;
  c.DepComp := True;
  c.BetweenOrg := True;
  c.CopyInvId := dmMain.taSOListINVID.AsInteger;
  c.CopyTypeFrom := 15;
  c.CopyType := 13;
  c.FromDep := -1;
  if dmMain.taSOListNDSID.IsNull then c.NDSId := -1
  else c.NDSId := dmMain.taSOListNDSID.AsInteger;
  ShowInvCopy(c);
end;

procedure TfmSOList.acCreateSIUpdate(Sender: TObject);
begin
  with (Sender as TAction) do
    Enabled := DataSet.Active and (not DataSet.IsEmpty)and(dmMain.taSOListISCLOSE.AsInteger=1);
end;

end.
