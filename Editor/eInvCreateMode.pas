unit eInvCreateMode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls;

type
  TfmeInvCreateMode = class(TfmEditor)
    rgrInvCreateMode: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmeInvCreateMode: TfmeInvCreateMode;

implementation

uses MainData;

{$R *.dfm}

procedure TfmeInvCreateMode.FormCreate(Sender: TObject);
begin
  inherited;
  rgrInvCreateMode.ItemIndex := 0;
end;

procedure TfmeInvCreateMode.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if ModalResult = mrOk then
    Editor.vResult := rgrInvCreateMode.ItemIndex;
  inherited;
end;

end.
