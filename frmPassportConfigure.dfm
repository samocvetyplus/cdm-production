object DialogPassportConfigure: TDialogPassportConfigure
  Left = 1
  Top = 174
  BorderStyle = bsToolWindow
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
  ClientHeight = 513
  ClientWidth = 1044
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object cxGrid1: TcxGrid
    Left = 336
    Top = 72
    Width = 345
    Height = 433
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object GridGroupItemsView: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DialogAffinage.DataSourceGroupItems
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.InfoText = #1053#1086#1074#1099#1081' '#1101#1083#1077#1084#1077#1085#1090' '#1075#1088#1091#1087#1087#1099
      NewItemRow.Visible = True
      OptionsCustomize.ColumnSorting = False
      OptionsView.NoDataToDisplayInfoText = ' '
      OptionsView.GroupByBox = False
      OptionsView.GroupRowStyle = grsOffice11
      object GridGroupItemsViewOperationTitle: TcxGridDBColumn
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        DataBinding.FieldName = 'Operation$Title'
        Options.Filtering = False
        Width = 169
      end
      object GridGroupItemsViewPlaceOfStorageTitle: TcxGridDBColumn
        Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
        DataBinding.FieldName = 'Place$Of$Storage$Title'
        Options.Filtering = False
        Width = 152
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = GridGroupItemsView
    end
  end
  object cxGrid2: TcxGrid
    Left = 9
    Top = 72
    Width = 321
    Height = 433
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object GridGroupsView: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = False
      NavigatorButtons.PriorPage.Visible = False
      NavigatorButtons.Prior.Visible = False
      NavigatorButtons.Next.Visible = False
      NavigatorButtons.NextPage.Visible = False
      NavigatorButtons.Last.Visible = False
      NavigatorButtons.Edit.Visible = False
      NavigatorButtons.Refresh.Visible = False
      NavigatorButtons.SaveBookmark.Visible = False
      NavigatorButtons.GotoBookmark.Visible = False
      NavigatorButtons.Filter.Visible = False
      DataController.DataSource = DialogAffinage.DataSourceGroups
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.InfoText = #1053#1086#1074#1072#1103' '#1075#1088#1091#1087#1087#1072
      NewItemRow.Visible = True
      OptionsCustomize.ColumnSorting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsView.NoDataToDisplayInfoText = ' '
      OptionsView.ScrollBars = ssVertical
      OptionsView.ColumnAutoWidth = True
      OptionsView.GroupByBox = False
      OptionsView.GroupRowStyle = grsOffice11
      object GridGroupsViewOperationTitle: TcxGridDBColumn
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        DataBinding.FieldName = 'Operation$Title'
      end
      object GridGroupsViewPlaceOfStorageTitle: TcxGridDBColumn
        Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
        DataBinding.FieldName = 'Place$Of$Storage$Title'
      end
    end
    object cxGrid2Level1: TcxGridLevel
      GridView = GridGroupsView
    end
  end
  object GroupsDockControl: TdxBarDockControl
    Left = 8
    Top = 8
    Width = 321
    Height = 56
    Align = dalNone
    BarManager = BarManager
  end
  object GroupItemsDockControl: TdxBarDockControl
    Left = 336
    Top = 8
    Width = 345
    Height = 56
    Align = dalNone
    BarManager = BarManager
  end
  object GridVariants: TcxGrid
    Left = 688
    Top = 72
    Width = 345
    Height = 433
    TabOrder = 6
    LookAndFeel.Kind = lfOffice11
    object GridVariantsView: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DialogAffinage.DataSourceGroupItemVariants
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NewItemRow.InfoText = #1053#1086#1074#1099#1081' '#1101#1083#1077#1084#1077#1085#1090' '#1075#1088#1091#1087#1087#1099
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnSorting = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsView.NoDataToDisplayInfoText = ' '
      OptionsView.ScrollBars = ssVertical
      OptionsView.GroupRowStyle = grsOffice11
      object cxGridDBColumn1: TcxGridDBColumn
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        DataBinding.FieldName = 'Operation$Title'
        Options.Filtering = False
        Width = 169
      end
      object cxGridDBColumn2: TcxGridDBColumn
        Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
        DataBinding.FieldName = 'Place$Of$Storage$Title'
        Options.Filtering = False
        Width = 152
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = GridVariantsView
    end
  end
  object VariantsDockControl: TdxBarDockControl
    Left = 688
    Top = 8
    Width = 345
    Height = 56
    Align = dalNone
    BarManager = BarManager
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.DisabledLargeImages = DialogAffinage.DisabledLargeImages
    ImageOptions.LargeImages = DialogAffinage.EnabledLargeImages
    PopupMenuLinks = <>
    Style = bmsOffice11
    UseSystemFont = True
    Left = 72
    Top = 152
    DockControlHeights = (
      0
      0
      0
      0)
    object BarManagerBar1: TdxBar
      Caption = #1043#1088#1091#1087#1087#1099
      CaptionButtons = <>
      DockControl = GroupsDockControl
      DockedDockControl = GroupsDockControl
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 231
      FloatTop = 169
      FloatClientWidth = 85
      FloatClientHeight = 213
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
      NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
      OldName = #1043#1088#1091#1087#1087#1072
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object BarManagerBar2: TdxBar
      Caption = #1069#1083#1077#1084#1077#1085#1090#1099' '#1075#1088#1091#1087#1087#1099
      CaptionButtons = <>
      DockControl = GroupItemsDockControl
      DockedDockControl = GroupItemsDockControl
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 431
      FloatTop = 474
      FloatClientWidth = 85
      FloatClientHeight = 213
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
      OldName = #1069#1083#1077#1084#1077#1085#1090#1099' '#1075#1088#1091#1087#1087#1099
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object BarManagerBar3: TdxBar
      Caption = #1042#1072#1088#1080#1072#1085#1090#1099' '#1101#1083#1077#1084#1077#1085#1090#1086#1074
      CaptionButtons = <>
      DockControl = VariantsDockControl
      DockedDockControl = VariantsDockControl
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 340
      FloatTop = 261
      FloatClientWidth = 23
      FloatClientHeight = 22
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'ButtonClose'
        end>
      NotDocking = [dsNone, dsLeft, dsTop, dsRight, dsBottom]
      OldName = #1042#1072#1088#1080#1072#1085#1090#1099' '#1101#1083#1077#1084#1077#1085#1090#1086#1074
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRecentItems = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Category = 0
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100
      Visible = ivAlways
      LargeImageIndex = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = DataSetInsertGroup
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = DeleteGroupItem
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = DeleteGroup
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = DataSetPostGroup
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = DataSetCancelGroup
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = DataSetPostGroupItem
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = DataSetCancelGroupItem
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = ActionVariant
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = InsertGroupItem
      Category = 0
      AutoGrayScale = False
    end
    object ButtonClose: TdxBarLargeButton
      Align = iaRight
      Caption = #1047#1072#1074#1077#1088#1096#1080#1090#1100
      Category = 0
      Hint = #1047#1072#1074#1077#1088#1096#1080#1090#1100
      Visible = ivAlways
      LargeImageIndex = 6
      OnClick = ButtonCloseClick
      AutoGrayScale = False
    end
    object dxBarButton1: TdxBarButton
      Caption = #1042#1077#1088#1085#1091#1090#1100' '#1075#1088#1091#1087#1087#1091
      Category = 0
      Hint = #1042#1077#1088#1085#1091#1090#1100' '#1075#1088#1091#1087#1087#1091
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = ReturnGroup
      Category = 0
      AutoGrayScale = False
      SyncImageIndex = False
      ImageIndex = 9
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
      LargeImageIndex = 17
      AutoGrayScale = False
    end
  end
  object ActionList: TActionList
    Images = DialogAffinage.EnabledLargeImages
    Left = 72
    Top = 184
    object DataSetInsertGroup: TDataSetInsert
      Category = 'Dataset'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Enabled = False
      Hint = 'Insert'
      ImageIndex = 0
      DataSource = DialogAffinage.DataSourceGroups
    end
    object DataSetDeleteGroup: TDataSetDelete
      Category = 'Dataset'
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Enabled = False
      Hint = 'Delete'
      ImageIndex = 1
      DataSource = DialogAffinage.DataSourceGroups
    end
    object DataSetInsertGroupItem: TAction
      Category = 'Dataset'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 0
      OnExecute = DataSetInsertGroupItemExecute
      OnUpdate = DataSetInsertGroupItemUpdate
    end
    object DataSetDeleteGroupItem: TDataSetDelete
      Category = 'Dataset'
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Enabled = False
      Hint = 'Delete'
      ImageIndex = 1
      DataSource = DialogAffinage.DataSourceGroupItems
    end
    object DataSetPostGroup: TDataSetPost
      Category = 'Dataset'
      Caption = #1055#1086#1076#1090#1074#1077#1088#1076#1080#1090#1100
      Enabled = False
      Hint = 'Post'
      ImageIndex = 13
      DataSource = DialogAffinage.DataSourceGroups
    end
    object DataSetCancelGroup: TDataSetCancel
      Category = 'Dataset'
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100
      Enabled = False
      Hint = 'Cancel'
      ImageIndex = 8
      DataSource = DialogAffinage.DataSourceGroups
    end
    object DataSetPostGroupItem: TDataSetPost
      Category = 'Dataset'
      Caption = #1055#1086#1076#1090#1074#1077#1088#1076#1080#1090#1100
      Enabled = False
      Hint = 'Post'
      ImageIndex = 13
      DataSource = DialogAffinage.DataSourceGroupItems
    end
    object DataSetCancelGroupItem: TDataSetCancel
      Category = 'Dataset'
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100
      Enabled = False
      Hint = 'Cancel'
      ImageIndex = 8
      DataSource = DialogAffinage.DataSourceGroupItems
    end
    object ActionVariant: TAction
      Category = 'Dataset'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 18
      OnExecute = ActionVariantExecute
      OnUpdate = ActionVariantUpdate
    end
    object DeleteGroup: TAction
      Category = 'Dataset'
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 1
      OnExecute = DeleteGroupExecute
      OnUpdate = DeleteGroupUpdate
    end
    object DeleteGroupItem: TAction
      Category = 'Dataset'
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 1
      OnExecute = DeleteGroupItemExecute
      OnUpdate = DeleteGroupItemUpdate
    end
    object InsertGroupItem: TAction
      Category = 'Dataset'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 0
      OnExecute = InsertGroupItemExecute
      OnUpdate = InsertGroupItemUpdate
    end
    object ReturnGroup: TAction
      Category = 'Dataset'
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1075#1088#1091#1087#1087#1091
      ImageIndex = 0
      OnExecute = ReturnGroupExecute
      OnUpdate = ReturnGroupUpdate
    end
  end
end
