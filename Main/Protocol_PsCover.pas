{*********************}
{ Copyrigth (C) 2005  }
{ Kornejchouk Basile  }
{*********************}

unit Protocol_PsCover;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList,  ImgList,  ExtCtrls,
  ComCtrls, Grids, DBGridEh, DB, FIBDataSet, pFIBDataSet, DBGridEhGrouping,
  GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmProtocol_PsCover = class(TfmAncestor)
    gridPsCover: TDBGridEh;
    taPsCover: TpFIBDataSet;
    dsrPsCover: TDataSource;
    taPsCoverNAME: TFIBStringField;
    taPsCoverW: TFIBFloatField;
    SpeedItem1: TSpeedItem;
    acRefresh: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taPsCoverBeforeOpen(DataSet: TDataSet);
    procedure acRefreshExecute(Sender: TObject);
  end;

var
  fmProtocol_PsCover: TfmProtocol_PsCover;

implementation

uses DictData, dbUtil, MainData;

{$R *.dfm}

procedure TfmProtocol_PsCover.FormCreate(Sender: TObject);
begin
  inherited;
  OpenDataSet(taPsCover);
end;

procedure TfmProtocol_PsCover.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(taPsCover);
  inherited;
end;

procedure TfmProtocol_PsCover.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TfmProtocol_PsCover.taPsCoverBeforeOpen(DataSet: TDataSet);
begin
  taPsCover.ParamByName('PROTOCOLID').AsInteger := dmMain.taPListID.AsInteger;
end;

procedure TfmProtocol_PsCover.acRefreshExecute(Sender: TObject);
begin
  ExecSQL('execute procedure CREATE_PROTOCOL_PSCOVER('+dmMain.taPListID.AsString+')', dm.quTmp);
  ReOpenDataSet(taPsCover);
end;

end.
