unit Log;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls;

type
  TfmLog = class(TfmEditor)
    lbxLog: TListBox;
    procedure FormDeactivate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOkClick(Sender: TObject);
  private
    { Private declarations }
  public
    procedure Clear;
    procedure Add(S: string; Obj : TObject = nil);
  end;

var
  fmLog: TfmLog;

implementation

{$R *.dfm}

{ TfmLog }

procedure TfmLog.Add(S: string; Obj : TObject = nil);
begin
  lbxLog.AddItem(S, Obj);
end;

procedure TfmLog.Clear;
begin
  lbxLog.Clear;
end;

procedure TfmLog.FormDeactivate(Sender: TObject);
begin
  Self.Hide;
end;

procedure TfmLog.btnCancelClick(Sender: TObject);
begin
  Hide;
end;

procedure TfmLog.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Hide;
  Action := caHide;
end;

procedure TfmLog.btnOkClick(Sender: TObject);
begin
  Self.Hide;
end;

end.
