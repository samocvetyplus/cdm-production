unit dArt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, RxPlacemnt, ImgList, Grids, DBGrids, RXDBCtrl,
  RxSpeedBar, ExtCtrls, ComCtrls, StdCtrls, Buttons,
  DBCtrls, Mask, RxToolEdit, db, Menus, ActnList,
  DBGridEh, TB2Item, DBCtrlsEh, TB2Dock, TB2Toolbar, ArtImage,
  DBGridEhGrouping, GridsEh;

type
  TfmArt = class(TfmDictAncestor)
    SpeedBar1: TSpeedBar;
    Label1: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    lbMat: TLabel;
    lbGood: TLabel;
    btnDo: TSpeedButton;
    lbFilter: TLabel;
    ppArt: TTBPopupMenu;
    TBItem4: TTBItem;
    acImage: TAction;
    acMemo: TAction;
    spitImage: TSpeedItem;
    edFArt: TDBEditEh;
    cmbxSemis: TDBComboBoxEh;
    cmbxMat: TDBComboBoxEh;
    edArt: TDBEditEh;
    spitPrint: TSpeedItem;
    acPrintAssort: TAction;
    ppPrint: TTBPopupMenu;
    TBItem1: TTBItem;
    Splitter: TSplitter;
    acIns: TAction;
    acAddIns: TAction;
    acDelIns: TAction;
    plIns1: TPanel;
    plIns: TPanel;
    TBDock1: TTBDock;
    tlbrIns: TTBToolbar;
    TBItem5: TTBItem;
    TBItem2: TTBItem;
    gridIns: TDBGridEh;
    TBItem6: TTBItem;
    acPrintArtIns: TAction;
    TBItem3: TTBItem;
    TBItem7: TTBItem;
    acPrintDiamond: TAction;
    SpeedItem4: TSpeedItem;
    acArtRoot: TAction;
    acDublRecord: TAction;
    TBItem8: TTBItem;
    TBItem9: TTBItem;
    acPrintAll: TAction;
    procedure FormCreate(Sender: TObject);
    procedure cmbxSemisChange(Sender: TObject);
    procedure cmbxMatChange(Sender: TObject);
    procedure btnDoClick(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edFArtChange(Sender: TObject);
    procedure edFArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure grKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure acImageExecute(Sender: TObject);
    procedure DBEditEh1EditButtons0Click(Sender: TObject; var Handled: Boolean);
    procedure acPrintAssortExecute(Sender: TObject);
    procedure acPrintAssortUpdate(Sender: TObject);
    procedure acInsExecute(Sender: TObject);
    procedure acAddInsExecute(Sender: TObject);
    procedure acDelInsExecute(Sender: TObject);
    procedure acDelInsUpdate(Sender: TObject);
    procedure acAddInsUpdate(Sender: TObject);
    procedure acPrintArtInsExecute(Sender: TObject);
    procedure gridDictColumns14EditButtonClick(Sender: TObject; var Handled: Boolean);
    procedure acPrintDiamondExecute(Sender: TObject);
    procedure acPrintDiamondUpdate(Sender: TObject);
    procedure acPrintArtInsUpdate(Sender: TObject);
    procedure acArtRootExecute(Sender: TObject);
    procedure acDublRecordExecute(Sender: TObject);
    procedure acDublRecordUpdate(Sender: TObject);
    procedure acPrintAllExecute(Sender: TObject);
    procedure acPrintAllUpdate(Sender: TObject);
    procedure gridDictColumns18EditButtonClick(Sender: TObject;
      var Handled: Boolean);
  private
    FImage : TfmImage;
  end;

const
  ArtTableSemis : array [1..6] of string = ('�', '�', '�', '��', '�', '�');
  ArtTableIns : array[0..9] of string = ('-', '�', '��', '��', '�', '', '', '�', '', '');

var
  fmArt: TfmArt;
  FindArtId : integer;
  FindArt : string;

implementation

uses DictData, dbTree, dbUtil, jpeg, MsgDialog, PrintData, eArtSzDiamond, fmUtils,
  pFIBDataSet, dArtRoot, frmDialogArticleStructure;

{$R *.dfm}

procedure TfmArt.FormCreate(Sender: TObject);
begin
  dm.ShowArtIns := False;
  dm.DictMode := 'dArt';
  dm.AArt := '%';
  CloseDataSet(dm.taMat);
  dm.taMat.Filtered := False;
  dm.taMat.Filter := 'GOOD=1'; 
  dm.taMat.Filtered := True;
  OpenDataSet(dm.taMat);
  inherited;
  edFArt.Text := '';
  edArt.Text := '';
  if(dm.WorkMode = 'aDistr')or
    (dm.WorkMode = 'aSInvProd')then
  begin
    edArt.Text := FindArt;
    ActiveControl := edArt;
    edArt.SelectAll;
  end;
  cmbxSemis.Items.Assign(dm.dGood);
  cmbxMat.Items.Assign(dm.dMatGood);

  dm.taProducer.Active := true;
  //ppArt.Skin := dm.ppSkin;
end;

procedure TfmArt.cmbxSemisChange(Sender: TObject);
begin
  with cmbxSemis, Items do
    dm.ASemisId := TNodeData(Objects[ItemIndex]).Code;
end;

procedure TfmArt.cmbxMatChange(Sender: TObject);
begin
  with cmbxMat, Items do
    dm.AMatId := TNodeData(Objects[ItemIndex]).Code;
end;

procedure TfmArt.btnDoClick(Sender: TObject);
begin
  dm.AArt := edFArt.Text+'%';
  ReOpenDataSets([dm.taArt]);
end;

procedure TfmArt.edArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
var
  b : boolean;
begin
  case Key of
    VK_RETURN : begin
      dm.taArt.DisableControls;
      Screen.Cursor := crSQLWait;
      try
        b := dm.taArt.Locate('ART', edArt.Text, []);
      finally
        dm.taArt.EnableControls;
        Screen.Cursor := crDefault;
      end;
      ActiveControl := gridDict;
      if not b then
      begin
        if MessageDialog('������� �� ������! �������� �����?', mtConfirmation, [mbYes, mbNo],0)<>mrYes then eXit;
        dm.taArt.Insert;
        { TODO : ����������� �������� }
        dm.taArtART.AsString := edArt.Text;
        dm.taArtSEMIS.AsString := ArtTableSemis[StrToIntDef(edArt.Text[1], 1)];
        dm.taArtINSID.AsString := ArtTableIns[StrToIntDef(edArt.Text[3], 0)];
        gridDict.SelectedField := dm.taArtUNITID;
      end;
    end;
  end;
end;

procedure TfmArt.edFArtChange(Sender: TObject);
begin
  dm.AArt := edFArt.Text;
end;

procedure TfmArt.edFArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case key of
    VK_RETURN :
       try
         Screen.Cursor := crSQLWait;
         dm.AArt := edFArt.Text+'%';
         ReOpenDataSets([dm.taArt]);
       finally
         Screen.Cursor := crDefault;
       end;
  end;
end;

procedure TfmArt.grKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : if(ssCtrl in Shift)and(FMode in [dmSelRecordEdit])then Self.ModalResult := mrOk;
  end;
end;

procedure TfmArt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if Assigned(FImage)then
  begin
    FImage.Close;
    FImage := nil;
  end;
  try
    PostDataSet(dm.taAIns);
  except
    CancelDataSet(dm.taAIns);
  end;
  CloseDataSet(dm.taAIns);
  inherited;
  dm.DictMode := '';
  CloseDataSet(dm.taMat);
  dm.taMat.Filtered := False;
  dm.taMat.Filter := '';

  dm.taProducer.Close;

end;

procedure TfmArt.btnOkClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfmArt.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfmArt.acImageExecute(Sender: TObject);
begin
  if Assigned(FImage)then
  begin
    FImage.Close;
    FImage := nil;
  end;
  FImage := ShowDbImage(dm.dsrArt, dm.taArtPICT, dm.taArtPICTHEIGHT, dm.taArtPICTWIDTH,  '�����������');
end;

procedure TfmArt.DBEditEh1EditButtons0Click(Sender: TObject; var Handled: Boolean);
begin
  if DataSet.Locate('ART', edArt.Text, []) then ActiveControl := gridDict
  else
    if MessageDialog('������� �� ������! �������� �����?', mtConfirmation, [mbNo, mbOk], 0)=mrOk then
    begin
      dm.taArt.Insert;
      dm.taArtART.AsString := edArt.Text;
      ActiveControl := gridDict;
    end;
end;

procedure TfmArt.acPrintAssortExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkDictArt, Null);
end;

procedure TfmArt.acPrintAssortUpdate(Sender: TObject);
begin
  acPrintAssort.Enabled := not DataSet.IsEmpty;
end;

procedure TfmArt.acInsExecute(Sender: TObject);
begin
  dm.ShowArtIns := not dm.ShowArtIns;
  Splitter.Visible := dm.ShowArtIns;
  plIns1.Visible := dm.ShowArtIns;
  if dm.ShowArtIns then ReOpenDataSet(dm.taAIns) else CloseDataSet(dm.taAIns);
end;

procedure TfmArt.acAddInsExecute(Sender: TObject);
begin
  dm.taAIns.Insert;
  ActiveControl := gridIns;
end;

procedure TfmArt.acDelInsExecute(Sender: TObject);
begin
  dm.taAIns.Delete;
end;

procedure TfmArt.acDelInsUpdate(Sender: TObject);
begin
  acDelIns.Enabled := not dm.taAIns.IsEmpty;
end;

procedure TfmArt.acAddInsUpdate(Sender: TObject);
begin
  acAddIns.Enabled := dm.taAIns.Active;
end;

procedure TfmArt.acPrintArtInsExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkArtIns, Null);
end;

procedure TfmArt.gridDictColumns14EditButtonClick(Sender: TObject; var Handled: Boolean);
begin
  dm.ArtSzDiamond := dm.taArtDIAMOND.AsString;
  ShowAndFreeForm(TfmeArtSzDiamond, TForm(fmeArtSzDiamond));
  if not(dm.taArt.State in [dsInsert, dsEdit]) then dm.taArt.Edit;
  dm.taArtDIAMOND.AsString := dm.ArtSzDiamond;
  Handled := True;
end;

procedure TfmArt.acPrintDiamondExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkArtDiamond, Null);
end;

procedure TfmArt.acPrintDiamondUpdate(Sender: TObject);
begin
  acPrintDiamond.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmArt.acPrintArtInsUpdate(Sender: TObject);
begin
  acPrintArtIns.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmArt.acArtRootExecute(Sender: TObject);
var
  Article: string;
begin
  Article := dm.taArtART.AsString;
  TDialogArticleStructure.Execute(Article);
end;

procedure TfmArt.acDublRecordExecute(Sender: TObject);
var
  Art : string;
begin
  Art := copy(dm.taArtART.AsString, 1, Length(dm.taArtART.AsString)-3);
  dm.taArt.CloneCurRecord(['ARTID', 'ART']);
  dm.taArtART.AsString := Art;
  gridDict.SelectedField := dm.taArtART;
  gridDict.EditorMode := True;
end;

procedure TfmArt.acDublRecordUpdate(Sender: TObject);
begin
  acDublRecord.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmArt.acPrintAllExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkArtAll, Null);
end;

procedure TfmArt.acPrintAllUpdate(Sender: TObject);
begin
  acPrintAll.Enabled := DataSet.Active and (not DataSet.IsEmpty);

end;

procedure TfmArt.gridDictColumns18EditButtonClick(Sender: TObject;
  var Handled: Boolean);
begin
  dm.ArtSzDiamond := dm.taArtSTONE.AsString;
  ShowAndFreeForm(TfmeArtSzDiamond, TForm(fmeArtSzDiamond));
  if not(dm.taArt.State in [dsInsert, dsEdit]) then dm.taArt.Edit;
  dm.taArtSTONE.AsString := dm.ArtSzDiamond;
  Handled := True;
end;

end.
