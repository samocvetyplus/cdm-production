unit Splash;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, RXCtrls, UtilLib;

type
  TfmSplash = class(TForm)
    Panel1: TPanel;
    lbState: TLabel;
    lbName: TRxLabel;
    lbVersion: TLabel;
    lbCompanyName: TLabel;
    Image: TImage;
    procedure FormCreate(Sender: TObject);
  end;

var
  fmSplash: TfmSplash;

implementation

uses DictData;

{$R *.DFM}

procedure TfmSplash.FormCreate(Sender: TObject);
var
  fv: TFileVersionInfo;
begin
  fv := GetFileVersionInfo(Application.ExeName);
  lbVersion.Caption := fv.FileVersion;
  lbCompanyName.Caption := fv.CompanyName;
end;

end.
