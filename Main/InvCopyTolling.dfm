inherited fmInvCopyTolling: TfmInvCopyTolling
  Left = 290
  Top = 149
  Caption = #1069#1082#1089#1087#1086#1088#1090
  ClientHeight = 112
  ClientWidth = 260
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 260
    Height = 112
    object Label1: TLabel [0]
      Left = 12
      Top = 12
      Width = 91
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object Label2: TLabel [1]
      Left = 12
      Top = 32
      Width = 83
      Height = 13
      Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object Label3: TLabel [2]
      Left = 12
      Top = 52
      Width = 80
      Height = 13
      Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
    end
    inherited btnOk: TBitBtn
      Left = 76
      Top = 80
      Caption = #1069#1082#1089#1087#1086#1088#1090
    end
    inherited btnCancel: TBitBtn
      Left = 168
      Top = 80
    end
    object edDocNo: TDBNumberEditEh
      Left = 112
      Top = 8
      Width = 93
      Height = 19
      DecimalPlaces = 0
      EditButton.Style = ebsUpDownEh
      EditButton.Visible = True
      EditButtons = <>
      Flat = True
      TabOrder = 2
      Visible = True
    end
    object edDocDate: TDBDateTimeEditEh
      Left = 112
      Top = 28
      Width = 93
      Height = 19
      EditButton.Style = ebsGlyphEh
      EditButtons = <>
      Flat = True
      Kind = dtkDateEh
      TabOrder = 3
      Visible = True
    end
    object lcbxDep: TDBLookupComboboxEh
      Left = 112
      Top = 48
      Width = 137
      Height = 19
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      EditButtons = <>
      Flat = True
      KeyField = 'DEPID'
      ListField = 'NAME'
      ListSource = dsrDep
      TabOrder = 4
      Visible = True
    end
  end
  object taDep: TpFIBDataSet
    Database = dm.db
    Transaction = dm.tr
    SelectSQL.Strings = (
      'select DEPID, NAME'
      'from D_Dep'
      'where SelfCompId=:SELFCOMPID')
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 12
    Top = 68
    poSQLINT64ToBCD = True
    object taDepDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taDepNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrDep: TDataSource
    DataSet = taDep
    Left = 48
    Top = 69
  end
end
