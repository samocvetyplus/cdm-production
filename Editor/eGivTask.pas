unit eGivTask;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, DBGridEh, DBLookupEh, Mask,
  DBCtrlsEh;

type
  TfmeGivTask = class(TfmEditor)
    Label1: TLabel;
    Label2: TLabel;
    edDate: TDBDateTimeEditEh;
    edDeps: TDBLookupComboboxEh;
    Label3: TLabel;
    edOper: TDBLookupComboboxEh;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmeGivTask: TfmeGivTask;

implementation

uses Data;

{$R *.dfm}

procedure TfmeGivTask.FormCreate(Sender: TObject);
begin
  inherited;
  edDate.Value:=Now;
end;

end.
