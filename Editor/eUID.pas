unit eUID;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RXCtrls, StdCtrls, RXSpin, ExtCtrls, Mask, DBCtrlsEh;

type
  TfmUID = class(TForm)
    RxSpeedButton1: TRxSpeedButton;
    RxSpeedButton2: TRxSpeedButton;
    Bevel1: TBevel;
    RxLabel1: TRxLabel;
    edUID: TDBNumberEditEh;
    procedure RxSpeedButton1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    UID:integer;
  end;

var
  fmUID: TfmUID;

implementation

{$R *.dfm}

procedure TfmUID.RxSpeedButton1Click(Sender: TObject);
begin
  UID:=edUID.Value;
end;

procedure TfmUID.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN
  then begin
   RxSpeedButton1Click(self);
   ModalResult:=mrOk;
  end;
  if Key=VK_ESCAPE then ModalResult:=mrCancel;
end;

end.
