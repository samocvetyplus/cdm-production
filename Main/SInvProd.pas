{***********************************************}
{  ����������� � ������������ ��                }
{  ����� ������� ���������                      }
{                                               }
{  Copyrigth (C) 2002-2003 basile for CDM       }
{  v1.72                                        }
{  create 18.10.2003                            }
{  last update 27.04.2003                       }
{***********************************************}
unit SInvProd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls, dbClient,
  DBCtrls, StdCtrls, Mask, RxToolEdit, RXDBCtrl, Buttons, Grids,
  ActnList, db, TB2Item, TB2Dock, TB2Toolbar, DBCtrlsEh, DBGridEh,
  Menus, pFIbDataSet, pFIBQuery, FIBQuery, DBLookupEh,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  cxCheckBox;

const
  SQLProducers: AnsiString = 'select compid, name from d_comp where producer = 1 and compid <> 1';

type

  TButtonInfo = class(TObject)
  public
    SheetNo : integer;
  end;

  TButtonPanel = class(TCustomPanel)
  protected
    FData : TButtonInfo;
  public
    property Caption;
    property Height;
    property BevelOuter;
    property Data : TButtonInfo read FData write FData;
    property OnClick;
    property OnMouseMove;
    constructor Create(AOwner : TComponent); override;
  end;

  TfmSInvProd = class(TfmAncestor)
    plWhAppl: TPanel;
    Splitter1: TSplitter;
    plSInvProd: TPanel;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    plGrid: TPanel;
    SpeedBar2: TSpeedBar;
    lbDocNo: TLabel;
    lbDocDate: TLabel;
    lbFromDepName: TLabel;
    lbDepName: TLabel;
    txtDepTo: TDBText;
    spbrWhAppl: TSpeedBar;
    acOpen: TAction;
    lbOper: TLabel;
    cmbxOper: TDBComboBoxEh;
    lbArt: TLabel;
    edFilterArt: TDBEditEh;
    txtDepFrom: TDBText;
    acPrint: TAction;
    gridWhAppl: TDBGridEh;
    edDocNo: TDBNumberEditEh;
    TBDock1: TTBDock;
    tlbrInv: TTBToolbar;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    lbOperWh: TLabel;
    cmbxSOper: TDBComboBoxEh;
    ppInv: TTBPopupMenu;
    acExcel: TAction;
    TBItem3: TTBItem;
    acArt2Ins: TAction;
    ppWh: TTBPopupMenu;
    TBItem4: TTBItem;
    acChangeArt2: TAction;
    deSDate: TDBDateTimeEditEh;
    TBItem5: TTBItem;
    acShowID: TAction;
    acChangeArt: TAction;
    TBDock2: TTBDock;
    tlbrWh: TTBToolbar;
    TBItem6: TTBItem;
    TBSubmenuItem1: TTBSubmenuItem;
    TBItem7: TTBItem;
    TBItem8: TTBItem;
    ppRejInv: TTBPopupMenu;
    ppPInv: TTBPopupMenu;
    TBItem9: TTBItem;
    acPDel: TAction;
    acRejDel: TAction;
    TBItem10: TTBItem;
    TBItem11: TTBItem;
    acPrice: TAction;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem12: TTBItem;
    acInvArt2Ins: TAction;
    pglView: TPageControl;
    tshGrid: TTabSheet;
    tshMatrix: TTabSheet;
    plPInv: TPanel;
    SpeedBar1: TSpeedBar;
    Label5: TLabel;
    txtPDocNo: TDBText;
    Label6: TLabel;
    txtPDocDate: TDBText;
    Panel1: TPanel;
    gridPEl: TDBGridEh;
    plRej: TPanel;
    spbrRej: TSpeedBar;
    Label2: TLabel;
    txtRejDocNo: TDBText;
    Label3: TLabel;
    txtRejDocDate: TDBText;
    plRejName: TPanel;
    gridRejEl: TDBGridEh;
    gridItem: TDBGridEh;
    TBDock3: TTBDock;
    tlbrSheet: TTBToolbar;
    ppMatrixInv: TTBPopupMenu;
    gridMatrixProd: TDBGridEh;
    TBItem13: TTBItem;
    quDDL: TpFIBQuery;
    TBDock4: TTBDock;
    tlbrFunc: TTBToolbar;
    TBItem16: TTBItem;
    TBSubmenuItem2: TTBSubmenuItem;
    TBItem17: TTBItem;
    TBItem18: TTBItem;
    acAddToFirstFreePlace: TAction;
    acCopyBuffer: TAction;
    acMultiSelect: TAction;
    TBItem14: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem15: TTBItem;
    acClearBuffer: TAction;
    TBItem19: TTBItem;
    acShowBuffer: TAction;
    acPrintBuffer: TAction;
    TBItem20: TTBItem;
    acFillSortInd: TAction;
    acChangeSz: TAction;
    TBItem21: TTBItem;
    TBItem22: TTBItem;
    TBItem23: TTBItem;
    Label1: TLabel;
    lcbxContract: TDBLookupComboboxEh;
    TBItem24: TTBItem;
    acCalcPrice: TAction;
    Label4: TLabel;
    lcbxInvColor: TDBLookupComboboxEh;
    SpeedButton1: TSpeedButton;
    acCheckRule: TAction;
    acCheckBarCodeReady: TAction;
    TBItem25: TTBItem;
    acPrintSticker: TAction;
    TBItem26: TTBItem;
    TBItem27: TTBItem;
    acStickerForward: TAction;
    acStickerBackward: TAction;
    TBItem28: TTBItem;
    acSetNullSticker_Count: TAction;
    acRefresh_Step: TAction;
    TBItem29: TTBItem;
    TBItem30: TTBItem;
    acCheckInv: TAction;
    TBItem31: TTBItem;
    acScale_GetW: TAction;
    TBItem32: TTBItem;
    TBSubmenuItem3: TTBSubmenuItem;
    TBItem33: TTBItem;
    acPhoto: TAction;
    Label7: TLabel;
    lcbxJobId: TDBLookupComboboxEh;
    Label8: TLabel;
    lcbxShipedBy: TDBLookupComboboxEh;
    GroupBox1: TGroupBox;
    cbForeignItems: TcxCheckBox;
    lcbxProducers: TDBLookupComboBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddExecute(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure gridWhApplDblClick(Sender: TObject);
    procedure gridWhApplKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
    procedure cmbxOperChange(Sender: TObject);
    procedure edFilterArtChange(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure cmbxSOperChange(Sender: TObject);
    procedure acExcelExecute(Sender: TObject);
    procedure acArt2InsExecute(Sender: TObject);
    procedure acArt2InsUpdate(Sender: TObject);
    procedure acChangeArt2Execute(Sender: TObject);
    procedure gridPElGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure gridRejElGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acChangeArt2Update(Sender: TObject);
    procedure acShowIDExecute(Sender: TObject);
    procedure acChangeArtUpdate(Sender: TObject);
    procedure acChangeArtExecute(Sender: TObject);
    procedure acPDelExecute(Sender: TObject);
    procedure acRejDelExecute(Sender: TObject);
    procedure acPDelUpdate(Sender: TObject);
    procedure acRejDelUpdate(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure gridItemKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acPriceExecute(Sender: TObject);
    procedure acInvArt2InsExecute(Sender: TObject);
    procedure acInvArt2InsUpdate(Sender: TObject);
    procedure gridMatrixProdGetCellParams(Sender: TObject;
      Column: TColumnEh; AFont: TFont; var Background: TColor;
      State: TGridDrawState);
    procedure acCopyBufferExecute(Sender: TObject);
    procedure acMultiSelectExecute(Sender: TObject);
    procedure acClearBufferExecute(Sender: TObject);
    procedure acClearBufferUpdate(Sender: TObject);
    procedure acShowBufferUpdate(Sender: TObject);
    procedure acShowBufferExecute(Sender: TObject);
    procedure acCopyBufferUpdate(Sender: TObject);
    procedure acPrintBufferUpdate(Sender: TObject);
    procedure acFillSortIndExecute(Sender: TObject);
    procedure acFillSortIndUpdate(Sender: TObject);
    procedure acChangeSzExecute(Sender: TObject);
    procedure acChangeSzUpdate(Sender: TObject);
    procedure acCalcPriceExecute(Sender: TObject);
    procedure lcbxContractUpdateData(Sender: TObject;
      var Handled: Boolean);
    procedure acCheckRuleUpdate(Sender: TObject);
    procedure acCheckBarCodeReadyExecute(Sender: TObject);
    procedure acPrintStickerExecute(Sender: TObject);
    procedure acPrintStickerUpdate(Sender: TObject);
    procedure acStickerForwardExecute(Sender: TObject);
    procedure acStickerBackwardExecute(Sender: TObject);
    procedure acSetNullSticker_CountExecute(Sender: TObject);
    procedure acSetNullSticker_CountUpdate(Sender: TObject);
    procedure acRefresh_StepUpdate(Sender: TObject);
    procedure acRefresh_StepExecute(Sender: TObject);
    procedure acCheckInvExecute(Sender: TObject);
    procedure acScale_GetWExecute(Sender: TObject);
    procedure acPhotoExecute(Sender: TObject);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acOpenUpdate(Sender: TObject);
    procedure cbForeignItemsPropertiesEditValueChanged(Sender: TObject);
  private
    FReadOnly : boolean;
    FSelComponent : TButtonPanel;
    FDocName : string;
    DataSetProducers: TClientDataSet;
    DataSourceProducers: TDataSource;
    procedure InsertWhRecord(DataSet: TDataSet; OperId, ArtId, Art2Id, SzId, U, Q : TField);
    procedure InitBtn(T : boolean);
    procedure FilterArt(DataSet: TDataSet; var Accept: Boolean);
    procedure SetOPeriod;
    procedure CreateSheetButton;
    procedure ButtonClick(Sender : TObject);
    procedure MouseMoveOverButton(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure BeforeOpen(Sender : TDataSet);
    procedure CellGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ReOpenMatrix;
    procedure FilterInvColor(DataSet: TDataSet; var Accept: Boolean);
    procedure InvColorGetCellParams(Sender: TObject; EditMode: Boolean; Params: TColCellParamsEh);
    procedure Access;
    procedure SetReadOnly(ReadOnly: boolean);
  protected

  end;

var
  fmSInvProd: TfmSInvProd;

implementation

uses ApplData, DictData, dbUtil, SItem, fmUtils, MainData, eW,
  Editor, dbTree, PrintData, MsgDialog, dArt2Ins, DictAncestor,
  dArt, InvData, dBufferUID, eSz, m207Export, eStickerDirection,
  ProductionConsts, IniFiles, eNotInInv, UtilLib, eArtIns,
  uUtils, ScaleData, ASList;

{$R *.dfm}

procedure HandlePost(E : Exception);
begin
  dmAppl.taAEl.Cancel;
  ShowMessage(E.Message);
end;

procedure TfmSInvProd.FormCreate(Sender: TObject);
var
  Ind : integer;
  i : integer;
  s : string;
  NoProc : string;
  bDrop : boolean;
//  sf : TStringField;
  v : Variant;
begin
  dm.WorkMode := 'aSInvProd';
  FDocName:='��������� ���';
  dmAppl.WhFrom := dmMain.taASListFROMDEPID.AsInteger;  //CurrFromDep;
  // ����������
  if dmAppl.trAppl.Active then dmAppl.trAppl.Commit;
  dmAppl.trAppl.StartTransaction;
  if dm.tr.Active then dm.tr.CommitRetaining
  else dm.tr.StartTransaction;
  // ���������������� ������� ������ ������������
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.ItemIndex := 0;
  dmAppl.FilterOperId := ROOT_OPER_CODE;

  cmbxSOper.Items.Assign(dm.dOper);
  cmbxSOper.Items.Delete(0);
  Ind := -1;
  for i:=0 to Pred(cmbxSOper.Items.Count) do
    if TNodeData(cmbxSOper.Items.Objects[i]).Code=dm.LastOperId then
    begin
      Ind := i;
      break;
    end;
  if(Ind = -1)then cmbxSOper.ItemIndex := 0
  else cmbxSOper.ItemIndex := Ind;
  dm.LastOperId := TNodeData(cmbxSOper.Items.Objects[cmbxSOper.ItemIndex]).Code;

  edFilterArt.Text := '';
  //tlbrSheet.Skin := dm.TBSkin;
  lcbxInvColor.DropDownBox.Columns[0].OnGetCellParams := InvColorGetCellParams;
  // ������� ������ ������
  dm.taInvColor.OnFilterRecord := FilterInvColor;
  dm.taInvColor.Filtered := True;
  OpenDataSets([dm.taContract, dm.taInvColor, dm.taMOL, dmAppl.taWhAppl,  dmMain.taASItem]);
  // ������?
  InitBtn(dmMain.taASListISCLOSE.AsInteger = 1);

  cbForeignItems.Checked := dmMain.taASListMC.AsInteger and 8 = 8;

  DataSetProducers := TClientDataSet.Create(Self);
  DataSourceProducers := TDataSource.Create(Self);

  DataSourceProducers.DataSet := DataSetProducers;

  DataSetProducers.Data := dm.QueryData(SQLProducers);

  lcbxProducers.ListSource := DataSourceProducers;
  lcbxProducers.KeyField := 'CompID';
  lcbxProducers.ListField := 'Name';

  lcbxProducers.Enabled := cbForeignItems.Checked;


  // ��������� ������� ��������� ��������� ������ � ���������� � ������������
  dmAppl.ADistrType := adtProd;
  v := ExecSelectSQL('select InvId from Inv where IType=8 and Ref='+
        dmMain.taASListINVID.AsString+' and WOrderId is null', dmAppl.quTmp);
        
  if VarIsNull(v)then dmAppl.RInvId :=0 else dmAppl.RInvId := v; 

  v := ExecSelectSQL('select InvId from Inv where IType=9 and Ref='+
        dmMain.taASListINVID.AsString+' and WOrderId is null', dmAppl.quTmp);
  if VarIsNull(v) then dmAppl.PInvId := 0 else dmAppl.PInvId := v;
  OpenDataSets([dmAppl.taRejInv, dmAppl.taPInv, dmAppl.taRejEl, dmAppl.taPEl]);
  if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty)then plRej.Visible := False;
  if((dmAppl.PInvId = 0) or dmAppl.taPInv.IsEmpty)then plPInv.Visible := False;
  // ���������� ����������� �������������� � �����. � �������� ��������
  SetOPeriod;

  Self.Resize;
  FSelComponent := nil;

  Access;
end;

procedure TfmSInvProd.FormClose(Sender: TObject; var Action: TCloseAction);
var
//  i : integer;
  Ini : TIniFile;
begin

  PostDataSets([dmAppl.taWhAppl, dmMain.taASItem, dmMain.taASList,
    dmAppl.taPInv, dmAppl.taPEl, dmAppl.taRejInv, dmAppl.taRejEl]);
  
  if(dmAppl.taRejEl.IsEmpty)and(not dmAppl.taRejInv.IsEmpty)then dmAppl.taRejInv.Delete;
  if(dmAppl.taPEl.IsEmpty)and(not dmAppl.taPInv.IsEmpty)then dmAppl.taPInv.Delete;

  CloseDataSets([dm.taContract, dm.taInvColor, dmAppl.taWhAppl, dmMain.taASItem]);
  dm.taInvColor.OnFilterRecord := nil;
  dm.taInvColor.Filtered := False;
  if dm.tr.Active then dm.tr.CommitRetaining;
  if dmAppl.trAppl.Active then dmAppl.trAppl.Commit;

  inherited;

  // ��������� ������� �����
  Ini := TIniFile.Create(GetIniFileName);

  try
    Ini.WriteInteger('LocalSetting', 'BarCodePrinterCount', dm.LocalSetting.BarCode_Count);
  finally
    Ini.Free;
  end;

  lcbxProducers.DataSource := nil;

  DataSourceProducers.Free;

  DataSetProducers.Free;

end;

procedure TfmSInvProd.acAddExecute(Sender: TObject);
var U : integer;
begin

  if FReadOnly then
  begin
    MessageDialog('������ ������� ��� ��� ������������� ��������������!', mtWarning, [mbOk], 0);
    eXit;
  end;

  PostDataSets([dmMain.taASList, dmMain.taASItem]);
  // ��������� ���� �� ���� �������� �� ����� ������� �������� �� ������ ������� ���������
  U := ExecSelectSQL('select UnitId from D_Art where D_ArtId = '+dmAppl.taWHApplARTID.AsString, dmMain.quTmp);
  if(U = 0)then // �� ��. ������� � ������������
  begin
    // ��������� ������� �� ������ ������� ���������
    u := ExecSelectSQL('select count(*) from SInfo where Art2Id='+dmAppl.taWHApplART2ID.AsString, dmMain.quTmp);
    if u <> 0 then
    begin
      acChangeArt2.Execute;
      acArt2Ins.Execute;
    end;
  end;
  dmAppl.ADistrNewU := dmAppl.taWHApplU.AsInteger;
  if ShowEditor(TfmeW, TfmEditor(fmeW))<>mrOk then eXit;
  dmMain.taASItem.Append;
  case (Sender as TComponent).Tag of
    1 : dmMain.taASItemSORTIND.AsInteger := dm.GetId2(0, dmMain.taASListINVID.AsInteger); // �������� � �����
    2 : dmMain.taASItemSORTIND.AsInteger := dm.GetId2(1, dmMain.taASListINVID.AsInteger); // �������� � ������ ������ �����
  end;
  dmMain.taASItem.Post;
  dmAppl.taWHAppl.Refresh;
  dm.LastOperId := TNodeData(cmbxSOper.Items.Objects[cmbxSOper.ItemIndex]).Code;
  // ����������� �������
  ReOpenMatrix;
  // ����������� �����
  if dm.LocalSetting.BarCode_Use then acPrintSticker.Execute;
end;

procedure TfmSInvProd.acOpenExecute(Sender: TObject);
begin

  PostDataSet(dmMain.taASList);
  if not (dmMain.taASList.State in [dsEdit,dsInsert]) then dmMain.taASList.Edit;
  if dmMain.taASItem.RecordCount=0 then raise Exception.Create('� ��������� ��� �������');

  if not (dm.IsAdm or (dm.User.Profile = 2)) then
  begin
    MessageDialog('�������� �������� ������ ��������� ������ ���. ���������!', mtError, [mbOk], 0);
    Exit;
  end;

  if acOpen.Caption = '������' then
  begin
    
    if dmMain.taASItem.Locate('Is$Printed', 0, []) then
    begin
      if MessageDialog('�� ����������� �� ��� ����� �� ���� ���������!'#13#10 + '�������, ��� ������ ������� �?', mtWarning, [mbYes, mbNo], 0) = mrNo then
      begin
        Sysutils.Abort;
      end;
    end;

    try
      ExecSQL('execute procedure CloseDoc("' + FDocName + '",' +dmMain.taASListINVID.AsString + ')', dm.quTmp);
      dmMain.taASList.Refresh;
      //dmMain.taASListISCLOSE.AsInteger := 1;
    except
      on E:Exception do
        begin
          MessageDialog('��� �������� ��������� �������� ������.'#13+E.Message, mtError, [mbOk], 0);
          eXit;
        end;
    end;
    acOpen.Caption    := '������';
    acOpen.Hint       := '������� ��������';
    acOpen.ImageIndex := 6;
  end
  else begin
    try
      ExecSQL('execute procedure OpenDoc("'+FDocName+'",'+dmMain.taASListINVID.AsString+')', dm.quTmp);
      dmMain.taASList.Refresh;
      //dmMain.taASListISCLOSE.AsInteger:=0;
    except
      on E:Exception do begin
        MessageDialog('��� �������� ��������� �������� ������.'#13+E.Message, mtError, [mbOk], 0);
        eXit;
      end;
    end;
    acOpen.Caption    := '������';
    acOpen.Hint       := '������� ��������';
    acOpen.ImageIndex := 5;
  end;
end;

procedure TfmSInvProd.acOpenUpdate(Sender: TObject);
begin
  inherited;
  acOpen.Enabled := (dm.IsAdm or (dm.User.Profile = 2)); 
end;

procedure TfmSInvProd.acDelExecute(Sender: TObject);
//var
//  sl : TStringList;
begin

  if FReadOnly then
  begin
    MessageDialog('������ ������� ��� ��� ������������� ��������������!', mtWarning, [mbOk], 0);
    eXit;
  end;
  
  PostDataSet(dmAppl.taWhAppl);

  if (dmMain.taASListISCLOSE.AsInteger = 1) then raise Exception.Create(rsInvClose);

  if (pglView.ActivePageIndex = 1) then // ���� ���������� ��������� ���
  begin
    exit;
  end;

  if dmMain.taASItemP0.AsInteger <> 1 then
    raise Exception.Create('���������� ������� �������, ������ ��� ��� �� ���� �� ������������');

  if not dmAppl.taWhAppl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([dmMain.taASItemART2ID.AsInteger,
     dmMain.taASItemSZID.AsInteger, dmMain.taASItemOPERID.AsString, dmMain.taASItemUA_OLD.AsInteger]), [])
  then
    try
      InsertWhRecord(dmMain.taASItem, dmMain.taASItemOPERID, dmMain.taASItemARTID,
            dmMain.taASItemART2ID, dmMain.taASItemSZID, dmMain.taASItemUA, dmMain.taASItemUA_OLD);
    except
      on E:Exception do MessageDialog('��� ���������� � ����� ����� �������� ������.'#13+
        '��������� InsertWhRecord'#13+
        '��� ����������� ������ ���������� ����� �� ��������� � ������ �����'#13+
        '�������� �������������'#13+
        E.Message, mtError, [mbOk], 0);
    end;
  try
    dmMain.taASItem.Delete;
  except
    on E:Exception do MessageDialog('��� �������� �� ��������� �������� ������.'#13+
        'dmMain.taASItem.Delete'#13+
        '��� ����������� ������ ���������� ����� �� ��������� � ������ �����'#13+
        '�������� �������������'#13+
        E.Message, mtError, [mbOk], 0);
  end;
  try
    dmAppl.taWHAppl.Refresh;
  except
    on E:Exception do MessageDialog('��� ���������� �������� �� ��������� �������� ������.'#13+
        '��� ����������� ������ ���������� ����� �� ��������� � ������ �����'#13+
        'dmAppl.taWHAppl.Refresh'+
        '�������� �������������'#13+
        E.Message, mtError, [mbOk], 0);
  end;
  //ReOpenMatrix;
end;

procedure TfmSInvProd.InsertWhRecord(DataSet: TDataSet; OperId, ArtId, Art2Id, SzId, U, Q : TField);
var v:Variant;
begin

  with dmAppl do
  begin
    v:= ExecSelectSQL('select Id from WhAppl where PsId='+IntToStr(dmAppl.WhFrom)+
      ' and OperId="'+OperId.AsString+'" and Art2Id='+Art2Id.AsString+' and Sz='+SzId.AsString+
      ' and U='+U.AsString, quTmp);
      if v<>null then NewWhApplId:=v
      else NewWhApplId:=dm.GetId(76);
      taWhAppl.Insert;
      taWHApplPSID.AsInteger := dmMain.taASListFROMDEPID.AsInteger;
      taWHApplOPERID.AsString := OperId.AsString;
      //taWHApplART.AsString := DataSet.FieldByName('ART').AsString;
      taWHApplART2ID.AsInteger := Art2Id.AsInteger;
      //taWHApplART2.AsString := Art2.AsString;
      taWHApplSZID.AsInteger := SzId.AsInteger;// DataSet.FieldByName('SZID').AsInteger;
      //taWHApplSZ.AsString := DataSet.FieldByName('SZNAME').AsString;
      taWHApplU.AsInteger := U.AsInteger;
      taWHApplQ.AsInteger := Q.AsInteger; // �������� �� �����
      taWhAppl.Post;
      taWhAppl.Refresh;
  end;
end;


procedure TfmSInvProd.gridWhApplDblClick(Sender: TObject);
begin
  acAdd.Execute;
end;

procedure TfmSInvProd.gridWhApplKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_SPACE : acAdd.Execute;
  end;
end;

procedure TfmSInvProd.cmbxOperChange(Sender: TObject);
begin
  dmAppl.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSet(dmAppl.taWHAppl);
end;

procedure TfmSInvProd.InitBtn(T: boolean);
begin
  if not T then // �������� ������
  begin
    acOpen.Caption := '������';
    acOpen.Hint := '������� ��������';
    acOpen.ImageIndex := 6;
  end
  else begin
    acOpen.Caption := '������';
    acOpen.Hint := '������� ��������';
    acOpen.ImageIndex := 5;
  end
end;

procedure TfmSInvProd.edFilterArtChange(Sender: TObject);
begin
  if edFilterArt.Text = '' then
  begin
    dmAppl.taWHAppl.Filtered := False;
    dmAppl.taWHAppl.OnFilterRecord := nil;
  end
  else begin
    dmAppl.taWHAppl.OnFilterRecord := FilterArt;
    dmAppl.taWHAppl.Filtered := True;
  end
end;

procedure TfmSInvProd.FilterArt(DataSet: TDataSet; var Accept: Boolean);
begin
  if copy(DataSet.FieldByName('ART').AsString, 0, Length(edFilterArt.Text))=edFilterArt.Text
  then Accept := True else Accept := False;
end;

procedure TfmSInvProd.acPrintExecute(Sender: TObject);
//var
//  i : integer;
begin
  dmInv.FCurrINVID := dmMain.taASListINVID.AsInteger;
  if(pglView.ActivePageIndex = 0)then dmPrint.PrintDocumentA(dkIn, VarArrayOf([VarArrayOf([dmMain.taASListINVID.AsInteger]),VarArrayOf([dmMain.taASListINVID.AsInteger])]))
  else begin
    exit;
  end;
end;

procedure TfmSInvProd.cmbxSOperChange(Sender: TObject);
begin
  dm.LastOperId := TNodeData(cmbxSOper.Items.Objects[cmbxSOper.ItemIndex]).Code;
end;

procedure TfmSInvProd.SetOPeriod;
var
//  dt : Variant;
  StoreDate: TDateTime;
begin
  if(dmMain.taASListFROMDEPID.IsNull) then raise EInternal.Create(Format(rsInternalError, ['141']));

  dm.DepBeginDate := dm.GetDepBeginDate(dmMain.taASListFROMDEPID.AsInteger);
  StoreDate:=dm.GetStoreBeginDate;

  dm.PsTypeBeginDate := 1;
  stbrStatus.Panels[0].Text := '������: '+DateToStr(dm.DepBeginDate);

end;

procedure TfmSInvProd.acExcelExecute(Sender: TObject);
var
  c : string;
  f : TFileName;
begin
  if dmMain.taASItem.IsEmpty then eXit;
  c := '��������� � '+dmMain.taASListDOCNO.AsString+' �� '+DateToStr(dmMain.taASListDOCDATE.AsDateTime);
  f := ExtractFileDir(ParamStr(0))+'\'+c+'.xls';
  GridToXLS(gridItem, f, c, True, False);
end;

procedure TfmSInvProd.acArt2InsExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'INSID'; sName: 'INSNAME'; sTable: 'D_INS'; sWhere: '');
begin
  dm.AArt2Id := dmAppl.taWHApplART2ID.AsInteger;
  ShowDictForm(TfmA2Ins, dm.dsrA2Ins, Rec, '�������������� �������� '+dmAppl.taWHApplART.AsString+' '+dmAppl.taWHApplART2.AsString,
    dmSelRecordEdit);
end;

procedure TfmSInvProd.acArt2InsUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not FReadOnly) and
                                 (dmAppl.taWHApplART2.AsString <> '-') and
                                 (not dmAppl.taWHAppl.IsEmpty);
end;

procedure TfmSInvProd.acChangeArt2Execute(Sender: TObject);
var
  NewArtId, NewArt2Id, NewSzId, NewU : integer;
  NewOperId : string;
begin
if not (dm.Isadm or (dm.User.Profile = 5)) then
  begin
    MessageDlg('� ��� ��� ���� ��� ������������� ������ ��������! ���������� � �������������!', mtWarning, [mbOk], 0);
    eXit;
  end;
  if MessageDialog('�������� ������ �������?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then eXit;
  NewArtId := dmAppl.taWHApplARTID.AsInteger;
  NewArt2Id := ExecSelectSQL('select Art2Id from Dubl_A2('+dmAppl.taWHApplART2ID.AsString+')', dmMain.quTmp);
  NewSzId := dmAppl.taWHApplSZID.AsInteger;
  NewOperId := dmAppl.taWHApplOPERID.AsString;
  NewU := dmAppl.taWHApplU.AsInteger;
  if not dmAppl._ShowEditorQ(dmAppl.taWhApplQ.AsInteger, dmAppl.taWhApplU.AsInteger, dmAppl.taWhApplU.AsInteger, 0) then eXit;
  with dmAppl do
  begin
    (* ����� � ������ ������� WhApplArt2Id *)
    if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty) then
    begin
      plRej.Visible := True;
      taRejInv.Append;
      taRejInv.Post;
      ReOpenDataSet(taRejInv);
    end;
    if not taRejEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([dmAppl.taWHApplART2ID.AsInteger,
          taWHApplSZID.AsInteger, taWHApplOPERID.AsString, taWHApplU.AsInteger]), []) then
    begin
      taRejEl.Append;
      try
        taRejElQ.AsInteger := dmAppl.ADistrQ;
        taRejElARTID.AsInteger := taWHApplARTID.AsInteger;
        taRejElART2ID.AsInteger := taWHApplART2ID.AsInteger;
        taRejElOPERID.AsString := taWHApplOPERID.AsString;
        taRejElSZID.AsInteger := taWHApplSZID. AsInteger;
        taRejElCOMMENTID.AsInteger := 9;
        taRejEl.Post;
      except
        on E : Exception do HandlePost(E);
      end;
    end
    else begin
      taRejEl.Edit;
      taRejElQ.AsInteger := taRejElQ.AsInteger + dmAppl.ADistrQ;
      taRejEl.Post;
    end;
    taWHAppl.Refresh;

    (* �������� � ������ ������� *)
    if(dmAppl.taPInv.IsEmpty or (dmAppl.PInvid = 0)) then
    begin
      plPInv.Visible := True;
      taPInv.Append;
      taPInv.Post;
      ReOpenDataSet(dmAppl.taPInv);
    end;
    if not taPEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([NewArt2Id, NewSzId, NewOperId, NewU]), []) then
    begin
      taPEl.Append;
      try
        taPElQ.AsInteger := dmAppl.ADistrQ;
        taPElARTID.AsInteger := NewArtId;
        taPElOPERID.AsString := NewOperId;
        taPElSZID.AsInteger := NewSzId;
        taPElART2ID.AsInteger := NewArt2Id;
        taPElU.AsInteger := NewU;
        taPElCOMMENTID.AsInteger := 9;
        taPEl.Post;
      except
        on E : Exception do HandlePost(E);
      end;
    end
    else begin
      taPEl.Edit;
      taPElQ.AsInteger := taPElQ.AsInteger + dmAppl.ADistrQ;
      taPEl.Post;
    end;
     // �������� ����� ���������
    (* �������� Id � ������ ��������� *)
    dmAppl.NewWhApplId := ExecSelectSQL('select Id from WhAppl where PsId='+dmMain.taASListFROMDEPID.AsString+
       ' and OperId="'+taPElOPERID.AsString+'" and Art2Id='+taPElART2ID.AsString+' and Sz='+taPElSZID.AsString+
       ' and U='+taPElU.AsString, dmMain.quTmp);
    taWhAppl.Append;
    taWHApplPSID.AsInteger := dmMain.taASListFROMDEPID.AsInteger;
    taWHApplOPERID.AsString := taPElOPERID.AsString;
    taWHApplOPERNAME.AsString := taPElOPERATION.AsString;
    taWhApplARTID.AsInteger := taPElARTID.AsInteger;
    taWHApplART.AsString := taPElART.AsString;
    taWHApplART2ID.AsInteger := taPElART2ID.AsInteger;
    taWHApplART2.AsString := taPElART2.AsString;
    taWHApplSZID.AsInteger := taPElSZID.AsInteger;
    taWHApplSZ.AsString := taPElSZ.AsString;
    taWHApplU.AsInteger := taPElU.AsInteger;
    taWHApplQ.AsInteger := ADistrQ;
    taWHAppl.Post;
  end;
end;

procedure TfmSInvProd.gridPElGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if AnsiUpperCase(Column.FieldName) = 'RECNO' then Background := clMoneyGreen;
end;

procedure TfmSInvProd.gridRejElGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if AnsiUpperCase(Column.FieldName) = 'RECNO' then Background := clMoneyGreen;
end;

procedure TfmSInvProd.acChangeArt2Update(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not FReadOnly) and
                                 (dmAppl.taWHApplART2.AsString <> '-') and
                                 (not dmAppl.taWHAppl.IsEmpty);
end;

procedure TfmSInvProd.acShowIDExecute(Sender: TObject);
begin
  gridPEl.FieldColumns['ART2ID'].Visible := not gridPEl.FieldColumns['ART2ID'].Visible;
  gridRejEl.FieldColumns['ART2ID'].Visible := not gridRejEl.FieldColumns['ART2ID'].Visible;
end;

procedure TfmSInvProd.acChangeArtUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not FReadOnly) and
                                 (not dmAppl.taWHAppl.IsEmpty);
end;

procedure TfmSInvProd.acChangeArtExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ARTID'; sName: 'ART'; sTable: 'D_ART'; sWhere: '');
var
  NewArtId, NewSzId, NewU : integer;
  NewOperId : string;
  NewArt2Id : Variant;
begin
{if not (dm.IsAdm or (dm.User.Profile = 5)) then
  MessageDlg('� ��� ��� ���� ��� ������������� ������ ��������! ���������� � �������������!', mtWarning, [mbOk], 0)
  else
    begin}
      dArt.FindArtId := dmAppl.taWHApplARTID.AsInteger;
      dArt.FindArt := dmAppl.taWHApplART.AsString;
      if ShowDictForm(TfmArt, dm.dsrArt, Rec, '���������� ���������',  dmSelRecordEdit, nil)<>mrOk then eXit;
      NewArtId := vResult;
      if(dmAppl.taWHApplART2.AsString = '-')then
      NewArt2Id := ExecSelectSQL('select Art2Id from Art2 where D_ArtId='+IntToStr(NewArtId)+' and Art2="-"', dmAppl.quTmp)
      else
      NewArt2Id := ExecSelectSQL('select Art2Id from Dubl_A2_2('+dmAppl.taWHApplART2ID.AsString+', '+IntToStr(NewArtId)+')', dmMain.quTmp);
      if VarIsNull(NewArt2Id)or(NewArt2Id = 0) then raise EInternal.Create(Format(rsInternalError, ['1']));
      NewSzId := dmAppl.taWHApplSZID.AsInteger;
      NewOperId := dmAppl.taWHApplOPERID.AsString;
      NewU := dmAppl.taWHApplU.AsInteger;

      if not dmAppl._ShowEditorQ(dmAppl.taWhApplQ.AsInteger, dmAppl.taWhApplU.AsInteger, dmAppl.taWhApplU.AsInteger, 1) then eXit;
        (* ����� � ������ ������� WhApplArt2Id *)
      if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty) then
        begin
          plRej.Visible := True;
          dmAppl.taRejInv.Append;
          dmAppl.taRejInv.Post;
          ReOpenDataSet(dmAppl.taRejInv);
        end;
      if not dmAppl.taRejEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([dmAppl.taWHApplART2ID.AsInteger, dmAppl.taWHApplSZID.AsInteger,
          dmAppl.taWHApplOPERID.AsString, dmAppl.taWHApplU.AsInteger]), [])then
        begin
          dmAppl.taRejEl.Append;
          try
            dmAppl.taRejElQ.AsInteger := dmAppl.ADistrQ;
            dmAppl.taRejElARTID.AsInteger := dmAppl.taWHApplARTID.AsInteger;
            dmAppl.taRejElOPERID.AsString := dmAppl.taWHApplOPERID.AsString;
            dmAppl.taRejElSZID.AsInteger := dmAppl.taWHApplSZID. AsInteger;
            dmAppl.taRejElART2ID.AsInteger := dmAppl.taWHApplART2ID.AsInteger;
            dmAppl.taRejElCOMMENTID.AsInteger := 2;
            dmAppl.taRejEl.Post;
          except
            on E : Exception do HandlePost(E);
          end;
        end
        else
          begin
            dmAppl.taRejEl.Edit;
            dmAppl.taRejElQ.AsInteger := dmAppl.taRejElQ.AsInteger+ dmAppl.ADistrQ;
            dmAppl.taRejEl.Post;
          end;
      dmAppl.taWHAppl.Refresh;
       (* �������� � ������ ������� *)
      if(dmAppl.taPInv.IsEmpty or (dmAppl.PInvid = 0)) then
        begin
          plPInv.Visible := True;
          dmAppl.taPInv.Append;
          dmAppl.taPInv.Post;
          ReOpenDataSets([dmAppl.taPInv]);
        end;
      if not dmAppl.taPEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([NewArt2Id, NewSzId, NewOperId, newU]), []) then
        begin
          dmAppl.taPEl.Append;
          try
            dmAppl.taPElQ.AsInteger := dmAppl.ADistrQ;
            dmAppl.taPElARTID.AsInteger := NewArtId;
            dmAppl.taPElOPERID.AsString := NewOperId;
            dmAppl.taPElSZID.AsInteger := NewSzId;
            dmAppl.taPElART2ID.AsInteger := NewArt2Id;
            dmAppl.taPElU.AsInteger := NewU;
            dmAppl.taPElCOMMENTID.AsInteger := 2;
            dmAppl.taPEl.Post;
          except
            on E : Exception do HandlePost(E);
          end;
        end
        else
          begin
            dmAppl.taPEl.Edit;
            dmAppl.taPElQ.AsInteger := dmAppl.taPElQ.AsInteger + dmAppl.ADistrQ;
            dmAppl.taPEl.Post;
          end;
      with dmAppl do
        if not taWHAppl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([NewArt2Id, NewSzId, NewOperId, NewU]), [])
          then InsertWhRecord(taPEl, taPElOPERID, taPElARTID, taPElART2ID, taPElSZID, taPElU, taPElQ)
        else taWHAppl.Refresh;
    //end;
end;

procedure TfmSInvProd.acPDelExecute(Sender: TObject);
begin
  with dmAppl do
  begin
    PostDataSet(taPEl);
    if not taWHAppl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([taPElART2ID.AsInteger, taPElSZID.AsInteger,
      taPElOPERID.AsString, taPElU.AsInteger]), [])
    then InsertWhRecord(taPEl, taPElOPERID, taPElARTID, taPElART2ID, taPElSZID, taPElU, taPElQ);
    taPEl.Delete;
    taWHAppl.Refresh;
  end;
end;

procedure TfmSInvProd.acRejDelExecute(Sender: TObject);
begin
  with dmAppl do
  begin
    PostDataSet(taRejEl);
    if not taWHAppl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([taRejElART2ID.AsInteger, taRejElSZID.AsInteger,
      taRejElOPERID.AsString, taRejElU.AsInteger]), [])
    then InsertWhRecord(taRejEl, taRejElOPERID, taRejElARTID, taRejElART2ID, taRejElSZID, taRejElU, taRejElQ);
    taRejEl.Delete;
    taWHAppl.Refresh;
  end;
end;

procedure TfmSInvProd.acPDelUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not dmAppl.taPEl.IsEmpty;
end;

procedure TfmSInvProd.acRejDelUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not dmAppl.taRejEl.IsEmpty;
end;

procedure TfmSInvProd.acAddUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (dmMain.taASListISCLOSE.AsInteger <> 1) and not dmAppl.taWHAppl.IsEmpty;
end;

procedure TfmSInvProd.acDelUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (dmMain.taASListISCLOSE.AsInteger<>1) and not dmMain.taASItem.IsEmpty; 
end;

procedure TfmSInvProd.gridItemGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  inherited;
  if dmMain.taAsItem.FieldByName('Is$Printed').AsInteger = 1 then Background := clMoneyGreen;
end;

procedure TfmSInvProd.gridItemKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  {case Key of
   // VK_SPACE : if(Shift = [])then acDel.Execute;
  end;}
end;

procedure TfmSInvProd.acPriceExecute(Sender: TObject);
begin
  inherited;
  dmInv.sqlUpdateInvPrice.ParamByName('INVID').AsInteger:=dmMain.taASListINVID.AsInteger;
  dmInv.sqlUpdateInvPrice.ExecQuery;
  dmInv.sqlUpdateInvPrice.Transaction.CommitRetaining;
  ReOpenDataSets([dmMain.taASItem]);
end;

procedure TfmSInvProd.acInvArt2InsExecute(Sender: TObject);
{const
  Rec : TRecName = (sId: 'INSID'; sName: 'INSNAME'; sTable: 'D_INS'; sWhere: '');}
begin
  dmInv.CurrArt2Id:=dmMain.taASItemART2ID.AsInteger;
  if fmArtIns=nil then
  begin
    fmArtIns:=TfmArtIns.Create(self);
    fmArtIns.Show;
  end
  {dm.AArt2Id := dmMain.taASItemART2ID.AsInteger;
  ShowDictForm(TfmA2Ins, dm.dsrA2Ins, Rec, '�������������� �������� '+dmMain.taASItemART.AsString+' '+dmMain.taASItemART2.AsString,
    dmSelRecordEdit);}
end;

procedure TfmSInvProd.acInvArt2InsUpdate(Sender: TObject);
begin
  acInvArt2Ins.Enabled := (not FReadOnly) and
                          (dmMain.taASItemART2.AsString <> '-') and
                          (not dmMain.taASItem.IsEmpty);
end;

procedure TfmSInvProd.CreateSheetButton;
var
  SheetButton : TButtonPanel;
  Separator : TTBSeparatorItem;
  Item : TTBControlItem;
  i, c : integer;
  SheetNo : integer;
begin
  (* ������� ������ *)
  tlbrSheet.BeginUpdate;
  try
    i := 0;
    while i<=Pred(tlbrSheet.ComponentCount)do
    begin
      if tlbrSheet.Components[i].InheritsFrom(TButtonPanel) then
         TButtonPanel(tlbrSheet.Components[i]).Free
      else inc(i);
    end;
  finally
    tlbrSheet.EndUpdate;
  end;
  Item := nil;
  c := ExecSelectSQL('select count(*) from SEl e, SItem it where e.InvId='+dmMain.taASListINVID.AsString+' and e.SelId=it.SelId', dmMain.quTmp);
  OpenDataSet(dm.taRec);
  if(c=0)then SheetNo := 1
  else if(c mod (dm.taRecX_MATRIXPROD.AsInteger*dm.taRecY_MATRIXPROD.AsInteger) = 0)
  then SheetNo := Trunc(c / (dm.taRecX_MATRIXPROD.AsInteger*dm.taRecY_MATRIXPROD.AsInteger))
  else SheetNo := Trunc(c / (dm.taRecX_MATRIXPROD.AsInteger*dm.taRecY_MATRIXPROD.AsInteger))+1;
  i := 1;
  tlbrSheet.BeginUpdate;
  try
    for i:=0 to Pred(SheetNo) do
    begin
      if Assigned(Item) then
      Separator := TTBSeparatorItem.Create(Item);
      SheetButton := TButtonPanel.Create(tlbrSheet);
      SheetButton.FData := TButtonInfo.Create;
      with SheetButton.FData do
      begin
        SheetNo := i+1;
      end;
      SheetButton.Height := 20;
      SheetButton.Width := 45;
      SheetButton.OnMouseMove := MouseMoveOverButton;
      SheetButton.OnClick := ButtonClick;
      SheetButton.BevelOuter := bvNone;
      SheetButton.Caption := '���� '+IntToStr(i+1);
      SheetButton.Tag := i;
      SheetButton.Parent := tlbrSheet;
    end;
  finally
    tlbrSheet.EndUpdate;
  end;
end;



{ TButtonPanel }

constructor TButtonPanel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

procedure TfmSInvProd.MouseMoveOverButton(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if Sender is TButtonPanel then
    if(FSelComponent <> (Sender as TButtonPanel)) then
    begin
      //if Assigned(FSelComponent)then FSelComponent.BevelOuter := bvNone;
      //if(Sender as TButtonPanel).BevelOuter = bvNone then (Sender as TButtonPanel).BevelOuter := bvRaised;
      //FSelComponent := Sender as TButtonPanel;
    end;
end;

procedure TfmSInvProd.ButtonClick(Sender: TObject);
begin
  exit;
  {if (Sender is TButtonPanel) then
  begin
    if Assigned(FSelComponent) then FSelComponent.BevelOuter := bvNone;
    (Sender as TButtonPanel).BevelOuter := bvLowered;
    //�������
    CloseDataSet(dmMain.taMatrixProd);
    dmMain.SheetMatrixProd := (Sender as TButtonPanel).FData.SheetNo;
    FSelComponent := (Sender as TButtonPanel);
    OpenDataSet(dmMain.taMatrixProd);
  end;}
end;

procedure TfmSInvProd.cbForeignItemsPropertiesEditValueChanged(Sender: TObject);
var
  mc: Integer;
begin
  inherited;

  with dmMain.taASList do
  begin

    mc := FieldByName('MC').AsInteger;

    if cbForeignItems.Checked then
    begin
      mc := mc or 8;
      lcbxProducers.Enabled := true;
    end else
    begin
      mc := mc xor 8;

      Edit;

      FieldByName('Producer$ID').AsInteger := 1;

      Post;

      lcbxProducers.Enabled := false;
    end;

    Edit;    

    FieldByName('MC').AsInteger := mc;

    Post;

  end;


end;

procedure TfmSInvProd.BeforeOpen(Sender: TDataSet);
begin
  {with TpFIBDataSet(dmMain.taMatrixProd).Params do
    if Assigned(FSelComponent) then ByName['SHEETNO'].AsInteger := (FSelComponent as TButtonPanel).FData.SheetNo
    else ByName['SHEETNO'].AsInteger := 1;}
end;

procedure TfmSInvProd.CellGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
  sl : TStringList;
begin
  sl := TStringList.Create;
  try
    sl.Delimiter := ';';
    sl.DelimitedText := Sender.AsString;
    Text := '���. '+sl.Values['ART']+#13+'UID='+sl.Values['UID'];
  finally
    sl.Free;
  end;
end;

procedure TfmSInvProd.gridMatrixProdGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if Odd(Column.Tag)then Background := clWindow else Background := clCream;
end;

procedure TfmSInvProd.ReOpenMatrix;
begin

end;

procedure TfmSInvProd.acCopyBufferExecute(Sender: TObject);
begin
  {dmMain.taMatrixProd.DisableControls;
  with gridMatrixProd do
  try
    dmMain.taMatrixProd.Bookmark := Selection.Rect.TopRow;
    while True do
    begin
       for j := Selection.Rect.LeftCol to Selection.Rect.RightCol do
         if Columns[j].Visible then
         begin
           sl := TStringList.Create;
           try
             sl.Delimiter := ';';
             sl.DelimitedText := Columns[j].Field.AsString;
             if not VarIsNull(Columns[j].Field.Value)then
               ExecSQL('execute procedure B_UID_I('+IntToStr(dm.User.UserId)+', '+
                 sl.Values['UID']+','+sl.Values['ARTID']+','+sl.Values['ART2ID']+','+
                 sl.Values['SZID']+','+sl.Values['W']+')', dmMain.quTmp);
           finally
             sl.Free;
           end;
         end;
       if dmMain.taMatrixProd.CompareBookmarks(Pointer(Selection.Rect.BottomRow),Pointer(dmMain.taMatrixProd.Bookmark)) = 0 then break;
       dmMain.taMatrixProd.Next;
       if dmMain.taMatrixProd.Eof then Break;
    end;
  finally
    dmMain.taMatrixProd.EnableControls;
  end;}
end;


procedure TfmSInvProd.acMultiSelectExecute(Sender: TObject);
begin
  if dgMultiSelect in gridMatrixProd.Options then
  begin
    gridMatrixProd.Selection.Clear;
    gridMatrixProd.Options := gridMatrixProd.Options - [dgMultiSelect];
  end
  else gridMatrixProd.Options := gridMatrixProd.Options+[dgMultiSelect];
end;

procedure TfmSInvProd.acClearBufferExecute(Sender: TObject);
begin
  ExecSQL('delete from B_UID where USERID='+IntToStr(dm.User.UserId), dmMain.quTmp);
end;

procedure TfmSInvProd.acClearBufferUpdate(Sender: TObject);
begin
  acClearBuffer.Enabled := ExecSelectSQL('select count(*) from B_UID where USERID='+IntToStr(dm.User.UserId), dmMain.quTmp)<>0;
end;

procedure TfmSInvProd.acShowBufferUpdate(Sender: TObject);
begin
  acShowBuffer.Enabled := ExecSelectSQL('select count(*) from B_UID where USERID='+IntToStr(dm.User.UserId), dmMain.quTmp)<>0;
end;

procedure TfmSInvProd.acShowBufferExecute(Sender: TObject);
const
  Rec : TRecName = (sId: ''; sName: ''; sTable: ''; sWhere: '');
begin
  ShowDictForm(TfmdBufferUID, dmMain.dsrB_UID, Rec, '����� �������', dmSelRecordReadOnly);
end;

procedure TfmSInvProd.acCopyBufferUpdate(Sender: TObject);
begin
  acCopyBuffer.Enabled := (dgMultiSelect in gridMatrixProd.Options);
end;

procedure TfmSInvProd.acPrintBufferUpdate(Sender: TObject);
begin
  acPrintBuffer.Enabled := ExecSelectSQL('select count(*) from B_UID where USERID='+IntToStr(dm.User.UserId), dmMain.quTmp)<>0;
end;

procedure TfmSInvProd.acFillSortIndExecute(Sender: TObject);
begin
  ExecSQL('execute procedure FILL_SINVPROD_SORTIND('+dmMain.taASListINVID.AsString+')', dmMain.quTmp);
end;

procedure TfmSInvProd.acFillSortIndUpdate(Sender: TObject);
begin
  acFillSortInd.Enabled := dm.IsAdm and dmMain.taASList.Active and (not dmMain.taASList.IsEmpty);
end;

procedure TfmSInvProd.acChangeSzExecute(Sender: TObject);
var
  NewArtId, NewArt2Id, NewSzId, NewU : integer;
  NewOperId : string;
begin
{if (Not dm.Isadm) or (dm.User.Profile <> 2) then
  begin
    MessageDlg('� ��� ��� ���� ��� ������������� ������ ��������! ���������� � �������������!', mtWarning, [mbOk], 0);
    eXit;
  end;}
  if ShowEditor(TfmeSz, TfmEditor(fmeSz))<>mrOk then eXit;
  if(ResultSz = dmAppl.taWHApplSZID.AsInteger)then raise EWarning.Create('������ �� �������!');
  NewArtId := dmAppl.taWHApplARTID.AsInteger;
  NewArt2Id := dmAppl.taWHApplART2ID.AsInteger;
  NewSzId := ResultSz;
  NewOperId := dmAppl.taWHApplOPERID.AsString;
  NewU := dmAppl.taWHApplU.AsInteger;
  if not dmAppl._ShowEditorQ(dmAppl.taWHApplQ.AsInteger, dmAppl.taWHApplU.AsInteger, dmAppl.taWHApplU.AsInteger, 1) then eXit;
  (* ����� � ������ ������� WhApplArt2Id *)
  if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty) then
  begin
    plRej.Visible := True;
    dmAppl.taRejInv.Append;
    dmAppl.taRejInv.Post;
    dmAppl.taRejInv.Transaction.CommitRetaining;
    ReOpenDataSet(dmAppl.taRejInv);
  end;
  if not dmAppl.taRejEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([dmAppl.taWHApplART2ID.AsInteger,
    dmAppl.taWHApplSZID.AsInteger, dmAppl.taWHApplOPERID.AsString, dmAppl.taWHApplU.AsInteger]), []) then
  begin
    dmAppl.taRejEl.Insert;
    try
      dmAppl.taRejElQ.AsInteger := dmAppl.ADistrQ;
      dmAppl.taRejElARTID.AsInteger := dmAppl.taWHApplARTID.AsInteger;
      dmAppl.taRejElOPERID.AsString := dmAppl.taWHApplOPERID.AsString;
      dmAppl.taRejElSZID.AsInteger := dmAppl.taWHApplSZID. AsInteger;
      dmAppl.taRejElART2ID.AsInteger := dmAppl.taWHApplART2ID.AsInteger;
      dmAppl.taRejElCOMMENTID.AsInteger := 1;
      dmAppl.taRejEl.Post;
    except
      on E : Exception do HandlePost(E);
    end;
  end
  else begin
    dmAppl.taRejEl.Edit;
    dmAppl.taRejElQ.AsInteger := dmAppl.taRejElQ.AsInteger + dmAppl.ADistrQ;
    dmAppl.taRejEl.Post;
  end;
  dmAppl.taWHAppl.Refresh;
  (* �������� � ������ ������� *)
  if(dmAppl.taPInv.IsEmpty or (dmAppl.PInvid = 0)) then
  begin
    plPInv.Visible := True;
    dmAppl.taPInv.Append;
    dmAppl.taPInv.Post;
    ReOpenDataSet(dmAppl.taPInv);
  end;
  if not dmAppl.taPEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([NewArt2Id, NewSzId, NewOperId, NewU]), []) then
  begin
    dmAppl.taPEl.Append;
    try
      dmAppl.taPElQ.AsInteger := dmAppl.ADistrQ;
      dmAppl.taPElARTID.AsInteger := NewArtId;
      dmAppl.taPElOPERID.AsString := NewOperId;
      dmAppl.taPElSZID.AsInteger := NewSzId;
      dmAppl.taPElART2ID.AsInteger := NewArt2Id;
      dmAppl.taPElU.AsInteger := NewU;
      dmAppl.taPElCOMMENTID.AsInteger := 1;
      dmAppl.taPEl.Post;
    except
      on E : Exception do HandlePost(E);
    end;
  end
  else begin
    dmAppl.taPEl.Edit;
    dmAppl.taPElQ.AsInteger := dmAppl.taPElQ.AsInteger + dmAppl.ADistrQ;
    dmAppl.taPEl.Post;
  end;
  with dmAppl do
    if not taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([NewArt2Id, NewSzId, NewOperId]), [])
    then InsertWhRecord(taPEl, taPElOPERID, taPElARTID, taPElART2ID, taPElSZID, taPElU, taPElQ)
    else taWHAppl.Refresh;
end;

procedure TfmSInvProd.acChangeSzUpdate(Sender: TObject);
begin
  acChangeSz.Enabled := (not FReadOnly) and (dmAppl.taWHAppl.Active) and
                        (not dmAppl.taWHAppl.IsEmpty);
end;

procedure TfmSInvProd.acCalcPriceExecute(Sender: TObject);
var Art2id:integer;
   pprice:single;
begin

 dmMain.taASItem.First;
 while not dmMain.taASItem.Eof do
 begin
   Art2id:=dmMain.taASItemART2ID.AsInteger;
   with dminv
   do begin
    sqlPublic.SQL.Clear;
    sqlPublic.SQL.Add('execute procedure Calc_Upprice(:ART2id,0)');
    sqlPublic.ParamByName('art2id').asinteger:=Art2id;
    sqlPublic.ExecQuery;
    sqlPublic.Transaction.CommitRetaining;
    sqlPublic.SQL.Clear;
    sqlPublic.SQL.Add('update price set Price=Calc_price where Art2id='+IntToStr(Art2id));
    sqlPublic.ExecQuery;
    sqlPublic.Transaction.CommitRetaining;
    if dmMain.taASItem.State in [dsEdit,dsInsert] then dmMain.taASItem.Post;
    dmMain.taASItem.Edit;
    dmMain.taASItemPrice.AsFloat := ExecSelectSQL('select Price from price where Art2id='+
                IntToStr(Art2id), dm.quTmp);
    dmMain.taASItem.Post;
  end;
  dmMain.taASItem.Next;
 end;
 REopenDataSet(dmMain.taASItem);
end;


procedure TfmSInvProd.FilterInvColor(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := dm.taInvColorINVTYPEID.AsInteger = 4;
end;

procedure TfmSInvProd.InvColorGetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
  Params.Background := dm.taInvColorCOLOR.AsInteger;
end;

procedure TfmSInvProd.lcbxContractUpdateData(Sender: TObject; var Handled: Boolean);
begin
  if not dmMain.taASItem.IsEmpty then raise Exception.Create('��������� �������� �������');
end;

procedure TfmSInvProd.acCheckRuleUpdate(Sender: TObject);
begin
  edDocNo.Enabled := dmMain.taASListISCLOSE.AsInteger = 0;
  deSDate.Enabled := dmMain.taASListISCLOSE.AsInteger = 0;
  cmbxSOper.Enabled := dmMain.taASListISCLOSE.AsInteger = 0;
  lcbxContract.Enabled := (dmMain.taASListISCLOSE.AsInteger = 0) and (dmMain.taASItem.IsEmpty);
  lcbxInvColor.Enabled := dmMain.taASListISCLOSE.AsInteger = 0;
  lcbxJobId.Enabled := dmMain.taASListISCLOSE.AsInteger = 0;
  lcbxShipedBy.Enabled := dmMain.taASListISCLOSE.AsInteger = 0;
end;

procedure TfmSInvProd.acCheckBarCodeReadyExecute(Sender: TObject);
begin
  if dmPrint.BarCode_IsReady then
  begin
    ShowMessage('�����')
  end
  else
    begin
      ShowMessage('�� �����');
    end;
end;

procedure TfmSInvProd.acPrintStickerExecute(Sender: TObject);
begin
  PostDataSet(dmMain.taASItem);
  //dmPrint.PrintSticker(dmMain.taASItemUID.AsInteger);
  dmPrint.PrintStickerNew(dmMain.taASItemUID.AsInteger, false);
end;

procedure TfmSInvProd.acPrintStickerUpdate(Sender: TObject);
begin
  acPrintSticker.Enabled := dmMain.taASItem.Active and (not dmMain.taASItem.IsEmpty);
end;

procedure TfmSInvProd.acStickerForwardExecute(Sender: TObject);
begin
  if ShowEditor(TfmeStikerDirection, TfmEditor(fmeStikerDirection), '������')<>mrOk then eXit;
  dmPrint.BarCode_Forward(Editor.vResult);
end;

procedure TfmSInvProd.acStickerBackwardExecute(Sender: TObject);
begin
  if ShowEditor(TfmeStikerDirection, TfmEditor(fmeStikerDirection), '�����')<>mrOk then eXit;
  dmPrint.BarCode_Backward(Editor.vResult);
end;

procedure TfmSInvProd.acSetNullSticker_CountExecute(Sender: TObject);
begin
  dm.BarCodePrinter_Count := 0;
end;

procedure TfmSInvProd.acSetNullSticker_CountUpdate(Sender: TObject);
begin
  acSetNullSticker_Count.Caption := '���-�� ����� '+IntToStr(dm.LocalSetting.BarCode_Count)+'. ��������?';
end;

procedure TfmSInvProd.acRefresh_StepUpdate(Sender: TObject);
begin
  acRefresh_Step.Caption := '������� ���������� '+dm.LocalSetting.BarCode_Step+'. ��������?';
end;

procedure TfmSInvProd.acRefresh_StepExecute(Sender: TObject);
var
  Ini :TIniFile;
  sect: TStringList;
begin
  Ini := TIniFile.Create(GetIniFileName);
  sect := TStringList.Create;
  try
    Ini.ReadSectionValues('LocalSetting', sect);
    dm.BarCodePrinter_Step := sect.Values['BarCodePrinterStep'];
  finally
    Ini.Free;
    sect.Free;
  end;
end;

procedure TfmSInvProd.acCheckInvExecute(Sender: TObject);
begin
  if dmMain.taASListISCLOSE.AsInteger=1  then
  begin
    MessageDialog('����� ��������� ��������� ����� �������!', mtWarning, [mbOk], 0);
    exit;
  end;

  with dminv, sqlPublic do
  begin
    SQl.Clear;
    Sql.Add('execute procedure UNCHECK_SITEM(:INVID)');
    ParamByName('INVID').AsInteger:=dmMain.taASListINVID.AsInteger;
    Prepare;
    ExecQuery;
    Transaction.CommitRetaining;
    fmNotInInv:=TfmNotInInv.Create(self);
    try
      fmNotInInv.ShowModal;
    finally
      fmNotInInv.Free;
    end;
  end;
end;

procedure TfmSInvProd.acScale_GetWExecute(Sender: TObject);
var
  w : double;
begin
  w := dmScale.GetWeigth;
  ShowMessage(FloatToStr(w)+#13'���������� '+FloatToStr(MyRound(W)));
end;

procedure TfmSInvProd.acPhotoExecute(Sender: TObject);
begin
  PostDataSet(dmMain.taASItem);
  dmPrint.PrintDocumentA(dkInvArtPhoto, VarArrayOf([VarArrayOf([dmMain.taASListINVID.AsInteger])]));
end;

procedure TfmSInvProd.Access;
begin
  if not (dm.IsAdm or (dm.User.Profile = 2) or (dm.User.Profile = 5)) then
  begin
    SetReadOnly(True);
  end;
end;

procedure TfmSInvProd.SetReadOnly(ReadOnly: boolean);
begin

  FReadOnly := ReadOnly;
  cbForeignItems.Properties.ReadOnly := ReadOnly;
  gridWhAppl.ReadOnly := ReadOnly;
  gridMatrixProd.ReadOnly := ReadOnly;
  edDocNo.ReadOnly := ReadOnly;
  deSDate.ReadOnly := ReadOnly;
  gridPEL.ReadOnly := ReadOnly;
  gridRejEL.ReadOnly := ReadOnly;
  lcbxContract.ReadOnly := ReadOnly;
  lcbxInvColor.ReadOnly := ReadOnly;
  lcbxJobID.ReadOnly := ReadOnly;
  lcbxShipedBy.ReadOnly := ReadOnly;
  gridItem.ReadOnly := ReadOnly;
  tbItem1.Enabled := not ReadOnly;
  tbItem6.Enabled := not ReadOnly;
  tbItem7.Enabled := not ReadOnly;
  tbItem8.Enabled := not ReadOnly;
  tbItem21.Enabled := not ReadOnly;
  tbItem16.Enabled := not ReadOnly;
  tbItem17.Enabled := not ReadOnly;
  tbItem18.Enabled := not ReadOnly;
  acAdd.Enabled := not ReadOnly;
  acDel.Enabled := not ReadOnly;
  acCalcPrice.Enabled := not ReadOnly;
  TBSubMenuItem2.Enabled := not ReadOnly;

end;

end.
