inherited fmDIList: TfmDIList
  Left = 0
  Top = 0
  Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074' '#1084#1077#1078#1076#1091' '#1082#1083#1072#1076#1086#1074#1099#1084#1080
  ClientHeight = 434
  ClientWidth = 762
  Position = poDesigned
  ExplicitWidth = 770
  ExplicitHeight = 468
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 415
    Width = 762
    ExplicitTop = 415
    ExplicitWidth = 762
  end
  inherited tb2: TSpeedBar
    Width = 762
    Height = 30
    BtnWidth = 120
    ExplicitWidth = 762
    ExplicitHeight = 30
    inherited laDep: TLabel
      Left = 384
      Width = 60
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Font.Color = clBlack
      ExplicitLeft = 384
      ExplicitWidth = 60
    end
    inherited laPeriod: TLabel
      Left = 612
      ExplicitLeft = 612
    end
    object lbDepFrom: TLabel [2]
      Left = 128
      Top = 8
      Width = 129
      Height = 13
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
      Transparent = True
    end
    inherited spitDep: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076' '#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
      Caption = #1057#1082#1083#1072#1076' '#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
      DropDownMenu = ppDepFrom
    end
    inherited spitPeriod: TSpeedItem
      Left = 491
    end
    object spitDepFrom: TSpeedItem
      BtnCaption = #1057#1082#1083#1072#1076' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
      Caption = #1057#1082#1083#1072#1076' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
      DropDownMenu = ppDep
      Hint = #1057#1082#1083#1072#1076' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100'|'
      Spacing = 1
      Left = 259
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  inherited tb1: TSpeedBar
    Width = 762
    ExplicitWidth = 762
    inherited spitEdit: TSpeedItem
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000104A2000104A2000104A2000104A2000104A2000104A2000104
        A2000104A2000104A2000104A200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000104A2005983FF000026FF000030FF000030FB00002FF200002FE900002E
        E1000030D8000031D0000034CB000104A200FF00FF00FF00FF00FF00FF00FF00
        FF000104A200ABC2FF006480FF006688FF006688FF006687FA006787F5006787
        F0005779E9004D70E4004D74E2000104A200FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000104A2000104A2000104A2000104A2000104A2000104A2000104
        A2000104A2000104A2000104A200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      OnClick = acEditDocExecute
    end
    inherited spitPrint: TSpeedItem
      Action = nil
      DropDownMenu = ppPrint
      OnClick = acPrintDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    Top = 72
    Width = 762
    Height = 343
    DataSource = dmMain.dsrDIList
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    SumList.Active = True
    UseMultiTitle = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1053#1086#1084#1077#1088
        Width = 55
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1044#1072#1090#1072
        Width = 63
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FROMDEPNAME'
        Footers = <>
        Title.Caption = #1050#1083#1072#1076#1086#1074#1072#1103'|'#1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
        Width = 93
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Title.Caption = #1050#1083#1072#1076#1086#1074#1072#1103'|'#1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
        Width = 108
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Title.Caption = #1052#1054#1051'|'#1042#1099#1076#1072#1083
        Width = 74
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'JOBNAME'
        Footers = <>
        Title.Caption = #1052#1054#1051'|'#1055#1088#1080#1085#1103#1083
        Width = 74
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footer.FieldName = 'Q'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1050#1086#1083'-'#1074#1086
        Width = 57
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1042#1077#1089
        Width = 52
      end>
  end
  inherited ilButtons: TImageList
    Left = 86
    Top = 228
  end
  inherited fmstr: TFormStorage
    Left = 128
    Top = 204
  end
  inherited ppDep: TPopupMenu
    Left = 252
    Top = 256
  end
  inherited acAEvent: TActionList
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
    inherited acPrintDoc: TAction
      OnExecute = acPrintDocExecute
    end
  end
  object ppDepFrom: TPopupMenu
    Left = 312
    Top = 256
    object mnitAllDepFrom: TMenuItem
      Tag = -1
      Caption = #1042#1089#1077' '#1089#1082#1083#1072#1076#1099
    end
    object mnitSepDepFrom: TMenuItem
      Caption = '-'
    end
  end
  object ppPrint: TPopupMenu
    Left = 416
    Top = 208
    object N1: TMenuItem
      Action = acPrintDoc
    end
    object N2: TMenuItem
      Caption = #1058#1086#1074#1072#1088
      OnClick = N2Click
    end
  end
end
