unit eUIDInfo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, ImgList, ActnList;

type
  TfmeUIDInfo = class(TfmEditor)
    btnHistory: TBitBtn;
    BitBtn1: TBitBtn;
    acEvent: TActionList;
    acHistory: TAction;
    acIns: TAction;
    im: TImageList;
    lbMsg: TLabel;
    mmLastAction: TMemo;
    procedure acHistoryExecute(Sender: TObject);
    procedure acInsExecute(Sender: TObject);
    procedure acHistoryUpdate(Sender: TObject);
    procedure acInsUpdate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FValidUID : boolean;
    FUID : integer;
    FArt2Id : integer;
  end;

  procedure ShowUIDInfo(UID:integer; Msg:string);

implementation

uses InvData, eArtIns, DictData, ItemHistory, dbUtil, fmUtils;

{$R *.dfm}

procedure ShowUIDInfo(UID:integer; Msg: string);
var
  fmeUIDInfo: TfmeUIDInfo;
begin
  fmeUIDInfo := TfmeUIDInfo.Create(nil);
  try
    fmeUIDInfo.FUID := UID;
    fmeUIDInfo.lbMsg.Caption := Msg+' #'+IntToStr(UID);
    fmeUIDInfo.ShowModal;
  finally
    FreeAndNil(fmeUIDInfo);
  end;
end;

procedure TfmeUIDInfo.acHistoryExecute(Sender: TObject);
begin
  CloseDataSet(dmInv.taItemHistory);
  dmInv.FCurrUID := FUID;
  OpenDataSet(dmInv.taItemHistory);
  ShowAndFreeForm(TfmItemHistory, Self, TForm(fmItemHistory), True, False);
  CloseDataSet(dmInv.taItemHistory);
end;

procedure TfmeUIDInfo.acInsExecute(Sender: TObject);
begin
  dmInv.CurrArt2Id:= FArt2Id;
  if fmArtIns=nil then
  begin
    fmArtIns:=TfmArtIns.Create(self);
    fmArtIns.ShowModal;
  end
end;

procedure TfmeUIDInfo.acHistoryUpdate(Sender: TObject);
begin
  acHistory.Enabled := FValidUID;
end;

procedure TfmeUIDInfo.FormShow(Sender: TObject);
var
  r : integer;
begin
  r := ExecSelectSQL('select count(*) from SITEM where UID='+IntToStr(FUID), dm.quTmp);
  FValidUID := r<>0;
  if FValidUID then
    FArt2Id := ExecSelectSQL('select first 1 ART2ID from SItem where UID='+IntToStr(FUID), dm.quTmp)
  else FArt2Id := 0;
  if not FValidUID then mmLastAction.Text := '������� � �'+IntToStr(FUID)+' ��� � �������'
  else begin
    CloseDataSet(dmInv.taItemHistory);
    dmInv.FCurrUID := FUID;
    OpenDataSet(dmInv.taItemHistory);
    dmInv.taItemHistory.Last;
    mmLastAction.Text := '��������� ��������: '+dmInv.taItemHistoryOPNAME.AsString+' �� '+
      dmInv.taItemHistoryDOCDATE.AsString;
    CloseDataSet(dmInv.taItemHistory);
  end;
end;

procedure TfmeUIDInfo.acInsUpdate(Sender: TObject);
begin
  acIns.Enabled := FValidUID;
end;

procedure TfmeUIDInfo.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_ESCAPE : Close;
  end;
end;

end.
