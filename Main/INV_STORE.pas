unit Inv_Store;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls, ComCtrls,
  Grids, DBGrids, RXDBCtrl, StdCtrls, INVDATA, dbUtil,
  Buttons, DBGridEh, DB, ActnList, TB2Item, TB2Dock, TB2Toolbar,
  DBGridEhGrouping, GridsEh;

type
  TfmINV_STORE = class(TfmAncestor)
    tb2: TSpeedBar;
    laDep: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    lbStore: TLabel;
    lbINV: TLabel;
    GrStore: TDBGridEh;
    grINV: TDBGridEh;
    acInsert: TAction;
    acRemove: TAction;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure acInsertExecute(Sender: TObject);
    procedure acRemoveExecute(Sender: TObject);
    procedure GrStoreKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);    
  end;

var
  fmINV_STORE: TfmINV_STORE;

implementation

uses DictData;

{$R *.dfm}

procedure TfmINV_STORE.FormCreate(Sender: TObject);
var controlDate:TDateTime;
    controlDate_STORE:TDateTime;
begin
  inherited;
  controlDate:=dm.GetStoreBeginDate;
  with dmINV do
  begin
    ReOpenDataSets([taINVStore,taSItem]);
    if ControlDate>taSListDOCDATE.AsDateTime then
    begin
      acInsert.Enabled:=false;
      acRemove.Enabled:=false;
    end
     else begin
       acInsert.Enabled:=true;
       acRemove.Enabled:=true;
    end;
  end;
end;

procedure TfmINV_STORE.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  with dmINV do
  begin
   PostDataSets([taSItem]);
   dmInv.Q:=taSItem.RecordCount;
   CloseDataSets([taINVStore,taSItem]);
  end;
end;

procedure TfmINV_STORE.FormShow(Sender: TObject);
begin 
  Caption:='�������: '+dmINV.taInvItemsFULLART.AsString;
  case dmINV.ITYPE of
  2,5: lbStore.Caption:='������� �� ������';
  3:  lbStore.Caption:='��������� �������';
  end;

end;

procedure TfmINV_STORE.acInsertExecute(Sender: TObject);
begin
  inherited;
  if dmINV.taSListISCLOSE.ASinteger=1 then
   begin
    ShowMessage('������ �������� �������� ���������!');
    SysUtils.Abort;
   end;
  if dmINv.taINVStore.RecordCount>0 then
  with dmINV do
  begin
//   FCurrDep:=taINVStoreUID.AsInteger;
   FCurrPrice:=taA2TPRICE.AsFloat;
   FCurrUID := taINVStoreUID.AsInteger;
   FCurrW:=taINVStoreW.AsFloat;
   FCurrARTID:=dm.taARTARTID.Asinteger;
   FCurrART2ID:=taA2ART2ID.Asinteger;
   FCurrSz:=taINVStoreSZID.Asinteger;
   taSItem.Append;
   taINVStore.Delete;
   PostDataSets([taSItem ]);
   PostDataSets([taInvStore]);
//   taSItem.Transaction.CommitRetaining;
//   ReOpenDataSets([taINVStore, taSITem]);

//   ReOpenDataSets([taINVStore]);
   if taStoreByItems.Locate ('UID',taSItemUID.AsInteger,[loCaseInsensitive]) then taStoreByItems.Delete;
  end;
end;

procedure TfmINV_STORE.acRemoveExecute(Sender: TObject);
begin
  inherited;
 if dmINV.taSListISCLOSE.ASinteger=1 then
   begin
    ShowMessage('������ �������� �������� ���������!');
    SysUtils.Abort;
   end;

  with dmINV do
  begin
   // �������� ��������� ������
   taStoreByItems.Append;
   taStoreByItemsUID.AsInteger:=taSItemUID.AsInteger;
   PostDataSet(taStoreByItems);
   RefreshDataSet(taStoreByItems);
   taInvStore.Append;
   taINVStoreUID.AsInteger:=taSItemUID.AsInteger;
   PostDataSet(taInvStore);
   RefreshDataSet(taInvStore);
   taSItem.Delete;
   PostDataSet(taSItem);
//   ReOpenDataSets([taINVStore, taSITem]);
  end;
end;

procedure TfmINV_STORE.GrStoreKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_SPACE : acInsert.Execute;
  end;
end;

end.


