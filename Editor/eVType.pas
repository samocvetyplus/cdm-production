{***************************************}
{  ���������� ������������ ���������    }
{  Copyright(C) 2004 Kornejchouk Basile }
{  last update 15.06.2004               } 
{***************************************}
unit eVType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, ExtCtrls, Buttons, Mask, DBCtrlsEh;

type
  TfmeVType = class(TfmEditor)
    rgrType: TRadioGroup;
    lbDep: TLabel;
    Label2: TLabel;
    cmbxDep: TDBComboBoxEh;
    deBD: TDBDateTimeEditEh;
    deED: TDBDateTimeEditEh;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure cmbxDepUpdateData(Sender: TObject; var Handled: Boolean);
    procedure cmbxDepEnter(Sender: TObject);
    procedure cmbxDepExit(Sender: TObject);
  private
    FChangeDep : boolean;
    procedure SetPeriodDep(DepId : integer);
  end;

var
  fmeVType: TfmeVType;
  DepId : integer;
  BD, ED : TDateTime;

implementation

uses fmUtils, DictData, dbTree, dbUtil, DateUtils, UtilLib,
  ProductionConsts;

{$R *.dfm}

procedure TfmeVType.FormCreate(Sender: TObject);
var
  i : integer;
begin
  cmbxDep.Items.Assign(dm.dWHProd);
  FChangeDep := False;
  case dm.User.Profile of
    1 : begin // ���������
      rgrType.ItemIndex := 0;
      rgrType.Enabled := False;
    end;
    4 : begin // ���������
      rgrType.ItemIndex := 1;
      rgrType.Enabled := False;
    end
    else rgrType.ItemIndex := 0;
  end;
  cmbxDep.OnUpdateData := nil;
  cmbxDep.OnEnter := nil;
  cmbxDep.OnExit := nil;
  cmbxDep.ItemIndex := -1;
  cmbxDep.Items.Delete(0);
  // ���������� �������� �� ���������
  for i:=0 to Pred(cmbxDep.Items.Count) do
  begin
    if TNodeData(cmbxDep.Items.Objects[i]).Code = dm.User.DepId then
    begin
      cmbxDep.ItemIndex := i;
      break;
    end;
  end;
  with cmbxDep do
    if(ItemIndex <> -1)then SetPeriodDep(TNodeData(Items.Objects[ItemIndex]).Code)
    else begin
      deBD.Value := StartOfTheMonth(StartOfTheDay(dm.ServerDate));
      deBD.Value := EndOfTheMonth(EndOfTheDay(dm.ServerDate));
    end;
  cmbxDep.OnUpdateData := cmbxDepUpdateData;
  cmbxDep.OnEnter := cmbxDepEnter;
  cmbxDep.OnExit := cmbxDepExit;
end;

procedure TfmeVType.FormClose(Sender: TObject; var Action: TCloseAction);
var
  FBD, FED : TDateTime;
begin
  case ModalResult of
    mrOk : begin
      if(rgrType.ItemIndex = -1)then raise EWarning.Create(rsNotDefineVType);
      if(cmbxDep.ItemIndex = -1)then raise EWarning.Create(rsNotDefineWh);
      if not ConvertVarToDateTime(deBD.Value, FBD) then
      begin
        ActiveControl := deBD;
        raise Exception.Create(Format(rsInvalidDate, ['']));
      end;

      if not ConvertVarToDateTime(deED.Value, FED) then
      begin
        ActiveControl := deED;
        raise Exception.Create(Format(rsInvalidDate, ['']));
      end;
      vResult := rgrType.ItemIndex;
      dm.CurrDep := TNodeData(cmbxDep.Items.Objects[cmbxDep.ItemIndex]).Code;
      dm.vBeginDate := deBD.Value;
      dm.vEndDate := deED.Value;
    end;
  end;

  inherited;
end;

procedure TfmeVType.FormShow(Sender: TObject);
begin
  ActiveControl := FindNextControl(btnCancel, True, True, False);
end;

procedure TfmeVType.SetPeriodDep(DepId: integer);
var
  v : Variant;
  FBD, FED : TDateTime;
begin
  // ���������� ������
  if(DepId <> -1)then
  begin
    v := ExecSelectSQL('select max(DocDate) from Act '+
      ' where T=2 and IsClose = 1 and DepId ='+IntToStr(DepId), dm.quTmp);
    { TODO : ������ ���� }
    if not VarIsNull(v) then FBD := StartOfTheDay(v+1) else FBD := StrToDate('01.04.2003');
    if FBD > EndOfTheDay(ToDay) then FED := EndOfTheDay(EndOfTheMonth(FBD))
    else FED := EndOfTheDay(ToDay);
    deBD.Value := FBD;
    deED.Value := FED;
  end
  else begin
    deBD.Value := StartOfTheMonth(Now);
    deED.Value := EndOfTheMonth(Now);
  end;
end;

procedure TfmeVType.cmbxDepUpdateData(Sender: TObject; var Handled: Boolean);
begin
  FChangeDep := True;
end;

procedure TfmeVType.cmbxDepEnter(Sender: TObject);
begin
  FChangeDep := False;
end;

procedure TfmeVType.cmbxDepExit(Sender: TObject);
begin
  if not FChangeDep then eXit;
  with cmbxDep do
    if(ItemIndex <> -1)then SetPeriodDep(TNodeData(Items.Objects[ItemIndex]).Code);
  FChangeDep := False;
end;

end.


