object dmInv: TdmInv
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 664
  Width = 993
  object taSList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Inv set'
      '  DOCNO=:DOCNO, '
      '  DOCDATE=:DOCDATE, '
      '  USERID=:USERID,'
      '  SUPID=:SUPID, '
      '  SSF=:SSF, '
      '  NDSID=:NDSID,'
      '  Q = :Q,'
      '  W = :W,'
      '  ISCLOSE=:ISCLOSE,'
      '  FROMDEPID=:FROMDEPID,'
      '  COMMISION=:COMMISION,'
      '  UPPRICE=:UPPRICE,'
      '  MEMO=:MEMO,'
      '  ANALIZ=:ANALIZ,'
      '  PAYTYPEID=:PAYTYPEID,'
      '  TRANS_PRICE=:TRANS_PRICE,'
      '  TRANS_PERCENT=:TRANS_PERCENT,'
      '  CONTRACTID=:CONTRACTID,'
      '  COLORID=:INVCOLORID,'
      '  PAYED=:PAYED,'
      '  SENDED=:SENDED,'
      '  ISBRACK=:ISBRACK, '
      '  Prihod_ID = :Prihod_ID,'
      '  Reference$ID = :Reference$ID'
      'where InvId=:OLD_InvId')
    DeleteSQL.Strings = (
      'delete from Inv  where InvId=:OLD_InvId')
    InsertSQL.Strings = (
      
        'insert into Inv(INVID, DOCNO, DOCDATE, ITYPE, DEPID, FROMDEPID,S' +
        'UPID,'
      
        ' USERID, ISCLOSE, NDSID,COMMISION, UPPRICE, PAYTYPEID, MC, CONTR' +
        'ACTID, COLORID,'
      ' PAYED, SENDED, TRANS_PERCENT, TRANS_PRICE, ISBRACK)'
      
        'values (:INVID, :DOCNO, :DOCDATE, :ITYPE, :DEPID, :FROMDEPID, :S' +
        'UPID,:USERID,'
      
        '      0, :NDSID,:COMMISION, :UPPRICE, :PAYTYPEID,:ISMC,:CONTRACT' +
        'ID, :INVCOLORID,'
      '  :PAYED, :SENDED, :TRANS_PERCENT, :TRANS_PRICE, :ISBRACK)')
    RefreshSQL.Strings = (
      'select '
      
        '    INVID, DOCNO, DOCDATE, ITYPE, DEPID, USERID, NDSID, SUPID, F' +
        'IO,'
      '    SUPNAME, SSF, REF, ISCLOSE, FROMDEPID, FROMDEPNAME, DEPNAME,'
      '    Q, ASSORT, SUMM1, SUMM2, SSFDT, W, COST, ISBRACK, MEMO,'
      '    IN_SUM, OUT_SUM, RAZ_SUM, COMMISION, UPPRICE, RESTQ, RESTW,'
      '    RESTSUM, ACC_SUMM, REFNO, REFTYPE, ANALIZ, PAYTYPEID,'
      '    ISMC, TRANS_PRICE, TRANS_PERCENT, CONTRACTID, CONTRACTNAME,'
      
        '    SCHEMAID, INVSUBTYPEID, INVSUBTYPENAME, INVCOLORID, COLOR, P' +
        'AYED, SENDED, W2,'
      '    Prihod_ID, '
      '    Prihod, Created, Reference$ID'
      'from SList_R(:INVID)')
    SelectSQL.Strings = (
      'select'
      '    INVID ,'
      '    DOCNO ,'
      '    DOCDATE ,'
      '    ITYPE ,'
      '    DEPID ,'
      '    USERID ,'
      '    NDSID ,'
      '    SUPID ,'
      '    FIO ,'
      '    SUPNAME ,'
      '    SSF,'
      '    REF,'
      '    ISCLOSE ,'
      '    FROMDEPID ,'
      '    FROMDEPNAME ,'
      '    DEPNAME ,'
      '    Q ,'
      '    ASSORT ,'
      '    SUMM1 ,'
      '    SUMM2 ,'
      '    SSFDT ,'
      '    W ,'
      '    COST ,'
      '    ISBRACK ,'
      '    MEMO ,'
      '    IN_SUM ,'
      '    OUT_SUM ,'
      '    RAZ_SUM ,'
      '    COMMISION ,'
      '    UPPRICE ,'
      '    RESTQ ,'
      '    RESTW ,'
      '    RESTSUM ,'
      '    ACC_SUMM ,'
      '    REFNO ,'
      '    REFTYPE ,'
      '    ANALIZ,'
      '    PAYTYPEID,'
      '    ISMC,'
      '    TRANS_PRICE,'
      '    TRANS_PERCENT,'
      '    CONTRACTID,'
      '    CONTRACTNAME,'
      '    SCHEMAID,'
      '    INVSUBTYPEID,'
      '    INVSUBTYPENAME,'
      '    INVCOLORID,'
      '    COLOR,'
      '    PAYED,'
      '    SENDED,'
      '    W2,'
      '    Prihod_ID,'
      '    Prihod,'
      '    Created,'
      '    Reference$ID'
      'from SList_S(:DepId1, :DepId2, :BD, :ED,:ITYPE_)'
      
        'where (SUPNAME between :SUPNAME1 and :SUPNAME2) or (SUPNAME is n' +
        'ull)'
      'order by DocDate')
    CacheModelOptions.BufferChunks = 1000
    AutoCalcFields = False
    AfterOpen = taSListAfterOpen
    AfterPost = CommitRetaining
    BeforeDelete = taSListBeforeDelete
    BeforeOpen = taSListBeforeOpen
    OnCalcFields = taSListCalcFields
    OnNewRecord = taSListNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 8
    Top = 4
    object taSListINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SLIST_S.INVID'
    end
    object taSListDOCNO: TIntegerField
      DisplayLabel = #8470' '#1072#1082#1090'.'
      FieldName = 'DOCNO'
      Origin = 'SLIST_S.DOCNO'
    end
    object taSListDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
      FieldName = 'DOCDATE'
      Origin = 'SLIST_S.DOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taSListITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'SLIST_S.ITYPE'
    end
    object taSListDEPID: TIntegerField
      DisplayWidth = 10
      FieldName = 'DEPID'
      Origin = 'SLIST_S.DEPID'
    end
    object taSListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'SLIST_S.USERID'
    end
    object taSListSUPID: TIntegerField
      FieldName = 'SUPID'
      Origin = 'SLIST_S.SUPID'
    end
    object taSListSUPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      DisplayWidth = 40
      FieldName = 'SUPNAME'
      Origin = 'SLIST_S.SUPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'SLIST_S.ISCLOSE'
    end
    object taSListDEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'DEPNAME'
      Origin = 'SLIST_S.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSListFROMDEPID: TIntegerField
      FieldName = 'FROMDEPID'
      Origin = 'SLIST_S.FROMDEPID'
    end
    object taSListFROMDEPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
      FieldName = 'FROMDEPNAME'
      Origin = 'SLIST_S.FROMDEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSListNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SLIST_S.NDSID'
    end
    object taSListREF: TIntegerField
      FieldName = 'REF'
      Origin = 'SLIST_S.REF'
    end
    object taSListSUMM1: TFloatField
      DisplayLabel = #1063#1080#1089#1090'. '#1089#1090'.'
      FieldName = 'SUMM1'
      Origin = 'SLIST_S.SUMM1'
      currency = True
    end
    object taSListSUMM2: TFloatField
      DisplayLabel = #1057#1090'. '#1089' '#1085#1072#1083'.'
      FieldName = 'SUMM2'
      Origin = 'SLIST_S.SUMM2'
      currency = True
    end
    object taSListQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'SLIST_S.Q'
    end
    object taSListIN_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072' '#1076#1086' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      FieldName = 'IN_SUM'
      Origin = 'SLIST_S.IN_SUM'
      currency = True
    end
    object taSListOUT_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072' '#1087#1086#1089#1083#1077' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      FieldName = 'OUT_SUM'
      Origin = 'SLIST_S.OUT_SUM'
      currency = True
    end
    object taSListRAZ_SUM: TFloatField
      DisplayLabel = #1056#1072#1079#1085#1080#1094#1072
      FieldName = 'RAZ_SUM'
      Origin = 'SLIST_S.RAZ_SUM'
      currency = True
    end
    object taSListISBRACK: TSmallintField
      FieldName = 'ISBRACK'
      Origin = 'SLIST_S.ISBRACK'
    end
    object taSListCOMMISION: TSmallintField
      FieldName = 'COMMISION'
      Origin = 'SLIST_S.COMMISION'
    end
    object taSListUPPRICE: TFloatField
      FieldName = 'UPPRICE'
      Origin = 'SLIST_S.UPPRICE'
      DisplayFormat = '0.##'
      EditFormat = '0.00'
    end
    object taSListRESTSUM: TFloatField
      DisplayLabel = #1054#1089#1090#1072#1090#1082#1080
      FieldName = 'RESTSUM'
      Origin = 'SLIST_S.RESTSUM'
      currency = True
    end
    object taSListACC_SUMM: TFloatField
      DisplayLabel = #1059#1095#1077#1090#1085#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'ACC_SUMM'
      Origin = 'SLIST_S.ACC_SUMM'
      currency = True
    end
    object taSListREFNO: TIntegerField
      FieldName = 'REFNO'
      Origin = 'SLIST_S.REFNO'
    end
    object taSListREFTYPE: TIntegerField
      FieldName = 'REFTYPE'
      Origin = 'SLIST_S.REFTYPE'
    end
    object taSListW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SLIST_S.W'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSListMEMO: TMemoField
      FieldName = 'MEMO'
      Origin = 'SLIST_S.MEMO'
      BlobType = ftMemo
      Size = 8
    end
    object taSListANALIZ: TSmallintField
      FieldName = 'ANALIZ'
      Origin = 'SLIST_S.ANALIZ'
    end
    object taSListRESTQ: TIntegerField
      FieldName = 'RESTQ'
      Origin = 'SLIST_S.RESTQ'
    end
    object taSListRESTW: TFloatField
      FieldName = 'RESTW'
      Origin = 'SLIST_S.RESTW'
    end
    object taSListFIO: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldKind = fkLookup
      FieldName = 'FIO'
      LookupDataSet = dmMain.taMol
      LookupKeyFields = 'MOLID'
      LookupResultField = 'FIO'
      KeyFields = 'USERID'
      Origin = 'SLIST_S.FIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
      Lookup = True
    end
    object taSListPAYTYPEID: TIntegerField
      FieldName = 'PAYTYPEID'
      Origin = 'SLIST_S.PAYTYPEID'
    end
    object taSListISMC: TSmallintField
      FieldName = 'ISMC'
      Origin = 'SLIST_S.ISMC'
    end
    object taSListTRANS_PRICE: TFloatField
      DisplayLabel = #1058#1088#1072#1085#1087#1086#1088#1090#1085#1099#1077' '#1088#1072#1089#1093#1086#1076#1099
      FieldName = 'TRANS_PRICE'
      Origin = 'SLIST_S.TRANS_PRICE'
      currency = True
    end
    object taSListTRANS_PERCENT: TFloatField
      FieldName = 'TRANS_PERCENT'
      Origin = 'SLIST_S.TRANS_PERCENT'
      DisplayFormat = '0.##'
    end
    object taSListSSF: TFIBStringField
      FieldName = 'SSF'
      EmptyStrToNull = True
    end
    object taSListASSORT: TFIBIntegerField
      FieldName = 'ASSORT'
    end
    object taSListSSFDT: TFIBDateTimeField
      FieldName = 'SSFDT'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taSListCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taSListCONTRACTID: TFIBIntegerField
      FieldName = 'CONTRACTID'
    end
    object taSListCONTRACTNAME: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1076#1086#1075#1086#1074#1086#1088#1072
      FieldName = 'CONTRACTNAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taSListSCHEMAID: TFIBIntegerField
      FieldName = 'SCHEMAID'
    end
    object taSListINVSUBTYPEID: TFIBIntegerField
      FieldName = 'INVSUBTYPEID'
    end
    object taSListINVSUBTYPENAME: TFIBStringField
      FieldName = 'INVSUBTYPENAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taSListINVCOLORID: TFIBIntegerField
      FieldName = 'INVCOLORID'
    end
    object taSListCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taSListPAYED: TFIBDateTimeField
      DisplayLabel = #1054#1087#1083#1072#1090#1072
      FieldName = 'PAYED'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taSListSENDED: TFIBDateTimeField
      DisplayLabel = #1054#1090#1087#1088#1072#1074#1082#1072
      FieldName = 'SENDED'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taSListW2: TFIBFloatField
      DisplayLabel = #1055#1086' '#1072#1082#1090#1091' '#1074#1099#1087#1086#1083#1085#1077#1085#1085#1099#1093' '#1088#1072#1073#1086#1090
      FieldName = 'W2'
      DisplayFormat = '0.###'
    end
    object taSListREVISIONPERCENT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'REVISIONPERCENT'
      Calculated = True
    end
    object taSListPRIHOD_ID: TFIBIntegerField
      FieldName = 'PRIHOD_ID'
    end
    object taSListPRIHOD: TFIBStringField
      FieldName = 'PRIHOD'
      Size = 200
      EmptyStrToNull = True
    end
    object taSListCREATED: TFIBDateTimeField
      FieldName = 'CREATED'
    end
  end
  object dsrSList: TDataSource
    DataSet = taSList
    Left = 8
    Top = 48
  end
  object taINVItems: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SEL set'
      '     PRICE=:PRICE,'
      '      AKCIZ=:AKCIZ,'
      '     PNDS=:PNDS,'
      '     NDSID=:NDSID,'
      '     AKCIZ=:AKCIZ,'
      '     UA=:UA,'
      '     OLDUA=:OLDUA,'
      '     UPPRICE=:UPPRICE,'
      '     PRICE2=:PRICE2'
      'where selid=:OLD_SELId'
      '')
    DeleteSQL.Strings = (
      'delete from Sel where SelId=:OLD_SelId')
    InsertSQL.Strings = (
      
        'insert into SEL(SELID, INVID, ART2ID, D_ARTID, PRICE, TPRICE, PR' +
        'ICE2,'
      '                         AKCIZ, PNDS, NDSID,UA,OLDUA)'
      
        'values    (:SELID, :INVID, :ART2ID, :D_ARTID, :PRICE, :TPRICE, :' +
        'PRICE2,'
      '                         :AKCIZ,:PNDS,:NDSID,:UA,:OLDUA)')
    RefreshSQL.Strings = (
      'SELECT SELID, INVID, ART2ID, ART2, FULLART, PRICE,           '
      '       TPRICE, QUANTITY, TOTALWEIGHT, Q, W, PRICE2, PNDS,'
      '       UNITID, NDSID, ART, USEMARGIN, FULLSUM, D_INSID,  '
      '       D_ARTID, EP2, SSUM, SSUMD, SSUMF, PRILL, AKCIZ, GOOD,'
      '       UA, OLDUA, UPPRICE, ACC_SUMM, SERVCOST, CHECK_NO'
      'from SEL_R(:SELID)')
    SelectSQL.Strings = (
      'SELECT '
      '       SELID, INVID, ART2ID, ART2, FULLART, PRICE,           '
      '       TPRICE, QUANTITY, TOTALWEIGHT, Q, W, PRICE2,'
      '       PNDS, UNITID, NDSID, ART, USEMARGIN, FULLSUM,'
      '       D_INSID, D_ARTID, EP2, SSUM, SSUMD, SSUMF,'
      '       PRILL, AKCIZ, GOOD, UA, OLDUA, UPPRICE, '
      '       ACC_SUMM, SERVCOST, CHECK_NO'
      'from SEL_S(:ASINVID)'
      'order by ART')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = taINVItemsAfterDelete
    AfterEdit = taINVItemsAfterEdit
    AfterInsert = CommitRetaining
    AfterOpen = taINVItemsAfterOpen
    AfterPost = CommitRetaining
    AfterScroll = taINVItemsAfterScroll
    BeforeOpen = taINVItemsBeforeOpen
    BeforePost = taINVItemsBeforePost
    OnCalcFields = CalcRecNo
    OnNewRecord = taINVItemsNewRecord
    AfterRefresh = taINVItemsAfterRefresh
    Transaction = dm.tr
    Database = dm.db
    Filtered = True
    Left = 133
    Top = 4
    object taINVItemsRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taINVItemsSELID: TIntegerField
      FieldName = 'SELID'
      Origin = 'SEL_S.SELID'
    end
    object taINVItemsINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SEL_S.INVID'
    end
    object taINVItemsART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'SEL_S.ART2ID'
    end
    object taINVItemsART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'SEL_S.ART2'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taINVItemsFULLART: TFIBStringField
      DisplayLabel = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
      FieldName = 'FULLART'
      Origin = 'SEL_S.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taINVItemsPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'PRICE'
      Origin = 'SEL_S.PRICE'
      currency = True
    end
    object taINVItemsTPRICE: TFloatField
      FieldName = 'TPRICE'
      Origin = 'SEL_S.TPRICE'
      currency = True
    end
    object taINVItemsQUANTITY: TSmallintField
      FieldName = 'QUANTITY'
      Origin = 'SEL_S.QUANTITY'
    end
    object taINVItemsTOTALWEIGHT: TFloatField
      FieldName = 'TOTALWEIGHT'
      Origin = 'SEL_S.TOTALWEIGHT'
    end
    object taINVItemsQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'SEL_S.Q'
      DisplayFormat = '0'
      EditFormat = '0'
    end
    object taINVItemsW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SEL_S.W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taINVItemsPRICE2: TFloatField
      DisplayLabel = #1054#1090#1087#1091#1089#1082#1085#1072#1103' '#1094#1077#1085#1072
      FieldName = 'PRICE2'
      Origin = 'SEL_S.PRICE2'
      currency = True
    end
    object taINVItemsPNDS: TFloatField
      DisplayLabel = #1053#1044#1057
      FieldName = 'PNDS'
      Origin = 'SEL_S.PNDS'
    end
    object taINVItemsUNITID: TIntegerField
      DisplayLabel = #1045#1048
      FieldName = 'UNITID'
      Origin = 'SEL_S.UNITID'
    end
    object taINVItemsNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SEL_S.NDSID'
    end
    object taINVItemsART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'SEL_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taINVItemsUSEMARGIN: TSmallintField
      FieldName = 'USEMARGIN'
      Origin = 'SEL_S.USEMARGIN'
    end
    object taINVItemsFULLSUM: TFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1089' '#1085#1072#1083#1086#1075'.'
      FieldName = 'FULLSUM'
      Origin = 'SEL_S.FULLSUM'
      currency = True
    end
    object taINVItemsD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'SEL_S.D_INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taINVItemsD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'SEL_S.D_ARTID'
    end
    object taINVItemsEP2: TFloatField
      FieldName = 'EP2'
      Origin = 'SEL_S.EP2'
    end
    object taINVItemsSSUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'SSUM'
      Origin = 'SEL_S.SSUM'
      currency = True
    end
    object taINVItemsSSUMD: TFloatField
      FieldName = 'SSUMD'
      Origin = 'SEL_S.SSUMD'
      currency = True
    end
    object taINVItemsSSUMF: TFloatField
      FieldName = 'SSUMF'
      Origin = 'SEL_S.SSUMF'
      currency = True
    end
    object taINVItemsGOOD: TFIBStringField
      FieldName = 'GOOD'
      Origin = 'SEL_S.GOOD'
      EmptyStrToNull = True
    end
    object taINVItemsPRILL: TFIBStringField
      DisplayLabel = #1055#1088#1086#1073#1072
      FieldName = 'PRILL'
      Origin = 'SEL_S.PRILL'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taINVItemsAKCIZ: TFloatField
      DisplayLabel = #1040#1082#1094#1080#1079
      FieldName = 'AKCIZ'
      Origin = 'SEL_S.AKCIZ'
    end
    object taINVItemsNDSNAME: TStringField
      DisplayLabel = #1053#1044#1057
      FieldKind = fkLookup
      FieldName = 'NDSNAME'
      LookupDataSet = dm.taNDS
      LookupKeyFields = 'NDSID'
      LookupResultField = 'NAME'
      KeyFields = 'NDSID'
      Lookup = True
    end
    object taINVItemsUNIT: TStringField
      DisplayLabel = #1045#1048
      FieldKind = fkLookup
      FieldName = 'UNIT'
      LookupDataSet = taUnit
      LookupKeyFields = 'UNITID'
      LookupResultField = 'U'
      KeyFields = 'UNITID'
      Size = 5
      Lookup = True
    end
    object taINVItemsUA: TSmallintField
      FieldName = 'UA'
      Origin = 'SEL_S.UA'
    end
    object taINVItemsOLDUA: TSmallintField
      FieldName = 'OLDUA'
      Origin = 'SEL_S.OLDUA'
    end
    object taINVItemsUPPRICE: TFloatField
      DisplayLabel = #1053#1072#1094#1077#1085#1082#1072', %'
      FieldName = 'UPPRICE'
      Origin = 'SEL_S.UPPRICE'
      DisplayFormat = '0.0'
      EditFormat = '0.0'
    end
    object taINVItemsACC_SUMM: TFloatField
      DisplayLabel = #1059#1095#1077#1090#1085#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'ACC_SUMM'
      Origin = 'SEL_S.ACC_SUMM'
      currency = True
    end
    object taINVItemsSERVCOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1091#1089#1083#1091#1075
      FieldName = 'SERVCOST'
      currency = True
    end
    object taINVItemsCHECK_NO: TFIBStringField
      DisplayLabel = '# '#1095#1077#1082#1072
      FieldName = 'CHECK_NO'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsrINVItems: TDataSource
    DataSet = taINVItems
    Left = 133
    Top = 48
  end
  object taDepName: TpFIBDataSet
    SelectSQL.Strings = (
      'select DEPID,Name'
      'from D_DEP'
      'where band(DEPTYPES,4)=4'
      'order by SortInd')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taDepNameBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 508
    Top = 4
    object taDepNameDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'D_DEP.DEPID'
      Required = True
    end
    object taDepNameNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrDEPNAME: TDataSource
    DataSet = taDepName
    Left = 508
    Top = 48
  end
  object dsSItem: TDataSource
    DataSet = taSItem
    Left = 187
    Top = 48
  end
  object taSItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SITEM'
      'set'
      '  SELID = :SELID,'
      '  W = :W,'
      '  SZ = :SZ,'
      '  UID = :UID,'
      '  ITYPE = :ITYPE,'
      '  DT = :DT,'
      '  ARTID = :ARTID,'
      '  ART2ID = :ART2ID,'
      '  AKCIZ = :AKCIZ,'
      '  DEPID = :DEPID,'
      '  P0 = :P0,'
      '  OPERID = :OPERID'
      'where'
      '  SITEMID = :OLD_SITEMID')
    DeleteSQL.Strings = (
      'delete from SITEM'
      'where'
      '  SITEMID = :OLD_SITEMID')
    InsertSQL.Strings = (
      'insert into SITEM'
      
        '  (SITEMID,SELID, W, SZ, UID, ITYPE, DT, ARTID, ART2ID, AKCIZ, D' +
        'EPID, P0, OPERID)'
      'values'
      
        '  (:SITEMID,:SELID, :W, :SZ, :UID, :ITYPE, :DT, :ARTID, :ART2ID,' +
        ' :AKCIZ, :DEPID, '
      '   :P0, :OPERID)')
    RefreshSQL.Strings = (
      'Select '
      '  SITEMID,'
      '  SELID,'
      '  SINFOID,'
      '  ARTID,'
      '  ART2ID,'
      '  W,'
      '  SZ,'
      '  UID,'
      '  REF,'
      '  Q0,'
      '  DT,'
      '  ITYPE,'
      '  DEPID,'
      '  PRCLOSED,'
      '  AKCIZ,'
      '  NDSID,'
      '  OLD_UID,'
      '  SAM_UID,'
      '  P0,'
      '  OPERID,'
      '  UA,'
      '  OLD_UA,'
      '  S_OPERID,'
      '  SEMISID,'
      '  UQ,'
      '  UW'
      'from SITEM '
      'where'
      '  SITEMID = :SITEMID')
    SelectSQL.Strings = (
      'SELECT S.SITEMID, S.SELID, S.W, S.SZ ,S.UID, S.ITYPE, S.DT, '
      'S.ARTID, S.ART2ID, S.AKCIZ, S.DEPID, S.P0, S.OPERID'
      'FROM SITEM S '
      'WHERE SELID=?SELID          '
      'order by S.SZ')
    OnUpdateRecord = taSItemUpdateRecord
    AfterDelete = taSItemAfterDelete
    AfterInsert = CommitRetaining
    AfterPost = CommitRetaining
    BeforeDelete = taSItemBeforeDelete
    BeforeOpen = taSItemBeforeOpen
    OnCalcFields = CalcRecNo
    OnNewRecord = taSItemNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 187
    Top = 4
    object taSItemRecNo: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taSItemSITEMID: TIntegerField
      FieldName = 'SITEMID'
      Origin = 'SITEM.SITEMID'
      Required = True
    end
    object taSItemSELID: TIntegerField
      FieldName = 'SELID'
      Origin = 'SITEM.SELID'
      Required = True
    end
    object taSItemW: TFloatField
      FieldName = 'W'
      Origin = 'SITEM.W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taSItemSZ: TIntegerField
      FieldName = 'SZ'
      Origin = 'SITEM.SZ'
      Required = True
    end
    object taSItemUID: TIntegerField
      DisplayLabel = #1059#1085'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
      Origin = 'SITEM.UID'
      Required = True
    end
    object taSItemITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'SITEM.ITYPE'
      Required = True
    end
    object taSItemDT: TDateTimeField
      FieldName = 'DT'
      Origin = 'SITEM.DT'
      Required = True
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object taSItemARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'SITEM.ARTID'
      Required = True
    end
    object taSItemART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'SITEM.ART2ID'
      Required = True
    end
    object taSItemAKCIZ: TFloatField
      FieldName = 'AKCIZ'
      Origin = 'SITEM.AKCIZ'
      Required = True
    end
    object taSItemDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SITEM.DEPID'
    end
    object taSItemP0: TSmallintField
      DefaultExpression = '0'
      FieldName = 'P0'
      Origin = 'SITEM.P0'
    end
    object taSItemOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'SITEM.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSItemszname: TStringField
      FieldKind = fkLookup
      FieldName = 'szname'
      LookupDataSet = dm.taSz
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'SZ'
      Lookup = True
    end
  end
  object taA2: TpFIBDataSet
    UpdateSQL.Strings = (
      'update  PRICE P'
      'set P.TPRICE=:TPRICE'
      'where P.ART2ID=:ART2ID and'
      '           P.DEPID=:DDEPID;'
      '')
    DeleteSQL.Strings = (
      'DELETE FROM ART2 WHERE ART2ID=?ART2ID')
    RefreshSQL.Strings = (
      'SELECT ART2Id,   ART2,  RQ, RW, TPRICE, PRICE ,DDEPID '
      'FROM ART2_S(:D_ARTID,:DEPID)'
      '')
    SelectSQL.Strings = (
      'SELECT ART2Id,   ART2,  RQ, RW, TPRICE, PRICE ,DDEPID, SZ '
      'FROM ART2_S(:D_ARTID,:DEPID)'
      '')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = taA2AfterPost
    AfterScroll = taA2AfterScroll
    BeforeDelete = DelConf
    BeforeEdit = taA2BeforeEdit
    BeforeOpen = taA2BeforeOpen
    BeforeRefresh = taA2BeforeRefresh
    Transaction = dm.tr
    Database = dm.db
    Left = 312
    Top = 4
    object taA2ART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'ART2.ART2ID'
      Required = True
    end
    object taA2ART2: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083'2'
      DisplayWidth = 200
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      Required = True
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taA2RQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'RQ'
      Origin = 'ART2.RQ'
      Required = True
    end
    object taA2RW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'RW'
      Origin = 'ART2.RW'
      Required = True
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taA2TPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'TPRICE'
      Origin = 'ART2_S.TPRICE'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taA2DDEPID: TIntegerField
      FieldName = 'DDEPID'
      Origin = 'ART2_S.DDEPID'
    end
    object taA2PRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'ART2_S.PRICE'
    end
    object taA2W: TFloatField
      FieldKind = fkCalculated
      FieldName = 'W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
      Calculated = True
    end
    object taA2SZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'ART2_S.SZ'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsA2: TDataSource
    DataSet = taA2
    Left = 312
    Top = 48
  end
  object dsUnit: TDataSource
    DataSet = taUnit
    Left = 344
    Top = 160
  end
  object taUnit: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from D_UNIT')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 344
    Top = 116
  end
  object taPrice: TpFIBDataSet
    SelectSQL.Strings = (
      'select PRICEID, PRICE, TPRICE, DEPID'
      'from PRICE')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 392
    Top = 116
  end
  object dsPrice: TDataSource
    DataSet = taPrice
    Left = 392
    Top = 160
  end
  object dsrStoreByArt: TDataSource
    DataSet = taStoreByArt
    Left = 370
    Top = 48
  end
  object taStoreByArt: TpFIBDataSet
    SelectSQL.Strings = (
      'Select ART, '
      '           ART2,'
      '           FULLART,'
      '           SZ,'
      '           RQ,'
      '           RW,'
      '           ART2ID,'
      '          SZID'
      'from  STORE_S(:DEPID1, :DEPID2, :WSZ)'
      'ORDER BY ART')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taStoreByArtBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 370
    Top = 4
    object taStoreByArtART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'STORE_S.ART'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taStoreByArtART2: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083'2'
      DisplayWidth = 30
      FieldName = 'ART2'
      Origin = 'STORE_S.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taStoreByArtSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'STORE_S.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taStoreByArtRQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'RQ'
      Origin = 'STORE_S.RQ'
    end
    object taStoreByArtRW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'RW'
      Origin = 'STORE_S.RW'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taStoreByArtFULLART: TFIBStringField
      DisplayLabel = #1055#1086#1083#1085'. '#1072#1088#1090#1080#1082#1091#1083
      FieldName = 'FULLART'
      Origin = 'STORE_S.FULLART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taStoreByArtART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'STORE_S.ART2ID'
    end
    object taStoreByArtSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'STORE_S.SZID'
    end
  end
  object dsrINVStore: TDataSource
    DataSet = taINVStore
    Left = 12
    Top = 160
  end
  object taINVStore: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure F_Update')
    InsertSQL.Strings = (
      'execute procedure F_Update')
    RefreshSQL.Strings = (
      
        'SELECT SN.UID, A2.ART, A2.ART2 , A2.FULLART, ds.NAME SZ, SN.W, D' +
        'S.ID SZID   '
      'FROM SINFO SN, ARt2 A2, D_SZ DS'
      'where sn.UID=:UID and '
      '           sn.art2id=a2.art2id and '
      '           sn.sz=ds.id'
      '')
    SelectSQL.Strings = (
      'select UID,'
      '          ART,'
      '         ART2,'
      '         FULLART,'
      '         SZ,'
      '         W,'
      '         SZID,'
      '         DOCDATE'
      
        'from SELECT_ITEMS(:DEPID1, :DEPID2, :ART2ID_1, :ART2ID_2, :STATE' +
        ')')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taINVStoreBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Description = #1048#1079#1076#1077#1083#1080#1103' '#1076#1083#1103' '#1072#1088#1090'2. '#1060#1086#1088#1084#1072' Inv_Store'
    Left = 12
    Top = 116
    object taINVStoreUID: TIntegerField
      DisplayLabel = #1059#1085'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
      Origin = 'STORE_ITEMS.UID'
    end
    object taINVStoreFULLART: TFIBStringField
      DisplayLabel = #1055#1086#1083#1085'. '#1072#1088#1090#1080#1082#1091#1083
      FieldName = 'FULLART'
      Origin = 'STORE_ITEMS.FULLART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taINVStoreART: TFIBStringField
      FieldName = 'ART'
      Origin = 'STORE_ITEMS.ART'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taINVStoreART2: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083'2'
      DisplayWidth = 200
      FieldName = 'ART2'
      Origin = 'STORE_ITEMS.ART2'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taINVStoreSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'STORE_ITEMS.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taINVStoreW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'STORE_ITEMS.W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taINVStoreSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'STORE_ITEMS.SZID'
    end
    object taINVStoreDOCDATE: TDateTimeField
      DisplayLabel = #1055#1086#1089#1083'. '#1076#1072#1090#1072
      FieldName = 'DOCDATE'
      Origin = 'SELECT_ITEMS.DOCDATE'
    end
  end
  object taComp: TpFIBDataSet
    SelectSQL.Strings = (
      'select c.COMPID, c.CODE, c.NAME, c.ADATE, '
      '       co.NO_CONTRACT NDOG, co.BD_CONTRACT DOGBD'
      
        'from D_COMP c left join COMPOWNER co on (c.COMPID=co.COMPID and ' +
        'co.SELFCOMPID=:SELFCOMPID)'
      'where c.COMPID<>:SELFCOMPID'
      'order by c.Name')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taCompBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 298
    Top = 116
    object taCompCOMPID: TIntegerField
      FieldName = 'COMPID'
      Origin = 'D_COMP.COMPID'
      Required = True
    end
    object taCompCODE: TFIBStringField
      FieldName = 'CODE'
      Origin = 'D_COMP.CODE'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taCompNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'D_COMP.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taCompADATE: TDateTimeField
      FieldName = 'ADATE'
      Origin = 'D_COMP.ADATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object taCompNDOG: TFIBStringField
      FieldName = 'NDOG'
      EmptyStrToNull = True
    end
    object taCompDOGBD: TFIBDateTimeField
      FieldName = 'DOGBD'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
  end
  object dsrCOMP: TDataSource
    DataSet = taComp
    Left = 298
    Top = 160
  end
  object dsrStateItems: TDataSource
    Left = 73
    Top = 160
  end
  object taStateItems: TpFIBDataSet
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 73
    Top = 116
  end
  object taStoreByItems: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure F_UPDATE;')
    InsertSQL.Strings = (
      'execute procedure F_UPDATE;')
    RefreshSQL.Strings = (
      'Select'
      '    UID ,'
      '    STATE ,'
      '    ART ,'
      '    ART2 ,'
      '    DOCDATE ,'
      '    SZ ,'
      '    W ,'
      '    U ,'
      '    PRICE ,'
      '    SUM_ ,'
      '    STATE_NAME ,'
      '    ART2ID ,'
      '    ARTID,'
      '    SZID,'
      '    PRICE2, SELFCOMPID, SELFCOMPNAME'
      'from CURRENT_STORE_S(:UID) ')
    SelectSQL.Strings = (
      'Select'
      '    UID ,'
      '    STATE ,'
      '    ART ,'
      '    ART2 ,'
      '    DOCDATE ,'
      '    SZ ,'
      '    W ,'
      '    U ,'
      '    PRICE ,'
      '    SUM_ ,'
      '    STATE_NAME ,'
      '    ART2ID ,'
      '    ARTID,'
      '    SZID,'
      '    PRICE2, SELFCOMPID, SELFCOMPNAME'
      'from CURRENT_STORE(:SUPID, :EDATE) '
      'where STATE between :ST1 and :ST2 and'
      '      ART starting with :ART '
      '     '
      ' ')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taStoreByItemsBeforeOpen
    OnCalcFields = CalcRecNo
    Transaction = dm.tr
    Database = dm.db
    OnFilterRecord = taStoreByItemsFilterRecord
    Left = 155
    Top = 116
    object taStoreByItemsRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taStoreByItemsUID: TIntegerField
      DisplayLabel = #1059#1085'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
      Origin = 'CURRENT_STORE.UID'
    end
    object taStoreByItemsSTATE: TIntegerField
      FieldName = 'STATE'
      Origin = 'CURRENT_STORE.STATE'
    end
    object taStoreByItemsART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'CURRENT_STORE.ART'
      EmptyStrToNull = True
    end
    object taStoreByItemsART2: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083'2'
      FieldName = 'ART2'
      Origin = 'CURRENT_STORE.ART2'
      EmptyStrToNull = True
    end
    object taStoreByItemsDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'DOCDATE'
      Origin = 'CURRENT_STORE.DOCDATE'
      DisplayFormat = 'DD.MM.YYYY hh:mm'
    end
    object taStoreByItemsSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'CURRENT_STORE.SZ'
      Size = 5
      EmptyStrToNull = True
    end
    object taStoreByItemsW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'CURRENT_STORE.W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taStoreByItemsU: TFIBStringField
      DisplayLabel = #1045#1048
      FieldName = 'U'
      Origin = 'CURRENT_STORE.U'
      Size = 10
      EmptyStrToNull = True
    end
    object taStoreByItemsPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'PRICE'
      Origin = 'CURRENT_STORE.PRICE'
      currency = True
    end
    object taStoreByItemsSUM_: TFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SUM_'
      Origin = 'CURRENT_STORE.SUM_'
      currency = True
    end
    object taStoreByItemsSTATE_NAME: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATE_NAME'
      Origin = 'CURRENT_STORE.STATE_NAME'
      EmptyStrToNull = True
    end
    object taStoreByItemsART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'CURRENT_STORE.ART2ID'
    end
    object taStoreByItemsARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'CURRENT_STORE.ARTID'
    end
    object taStoreByItemsSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'CURRENT_STORE.SZID'
    end
    object taStoreByItemsPRICE2: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1086#1090#1087'.'
      FieldName = 'PRICE2'
      currency = True
    end
    object taStoreByItemsSELFCOMPID: TFIBIntegerField
      FieldName = 'SELFCOMPID'
    end
    object taStoreByItemsSELFCOMPNAME: TFIBStringField
      DisplayLabel = #1042#1083#1072#1076#1077#1083#1077#1094
      FieldName = 'SELFCOMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsrStoreByItems: TDataSource
    DataSet = taStoreByItems
    Left = 155
    Top = 160
  end
  object taItemHistory: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ID, DOCDATE, ITYPE, DOCNO, CLOSE_,'
      '    OPNAME, DEPNAME, PRICE, PRICE2, SELFNAME,'
      '    ART2ID, ARTID, SORTIND'
      'from ITEM_HISTORY2(:UID)'
      'order by SORTIND'
      '')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taItemHistoryBeforeOpen
    OnCalcFields = taItemHistoryCalcFields
    Transaction = dm.tr
    Database = dm.db
    Left = 236
    Top = 116
    object taItemHistoryRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taItemHistoryITYPE: TSmallintField
      DisplayLabel = #1057#1086#1073#1099#1090#1080#1077
      FieldName = 'ITYPE'
      Origin = 'ITEM_HISTORY.ITYPE'
    end
    object taItemHistoryDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'DOCDATE'
      Origin = 'ITEM_HISTORY.DOCDATE'
    end
    object taItemHistoryDOCNO: TIntegerField
      DisplayLabel = #8470' '#1076#1086#1082'.'
      FieldName = 'DOCNO'
      Origin = 'ITEM_HISTORY.DOCNO'
    end
    object taItemHistoryCLOSE_: TSmallintField
      FieldName = 'CLOSE_'
      Origin = 'ITEM_HISTORY.CLOSE_'
    end
    object taItemHistoryOPNAME: TFIBStringField
      DisplayLabel = #1057#1086#1073#1099#1090#1080#1077
      FieldName = 'OPNAME'
      Origin = 'ITEM_HISTORY.OPNAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taItemHistoryDEPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100'/'#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
      FieldName = 'DEPNAME'
      Origin = 'ITEM_HISTORY.DEPNAME'
      Size = 50
      EmptyStrToNull = True
    end
    object taItemHistoryPRICE: TFloatField
      DisplayLabel = #1059#1095#1077#1090#1085#1072#1103' '#1094#1077#1085#1072
      FieldName = 'PRICE'
      Origin = 'ITEM_HISTORY.PRICE'
      currency = True
    end
    object taItemHistoryPRICE2: TFloatField
      DisplayLabel = #1054#1090#1087#1091#1089#1082#1085#1072#1103' '#1094#1077#1085#1072
      FieldName = 'PRICE2'
      Origin = 'ITEM_HISTORY.PRICE2'
      currency = True
    end
    object taItemHistorySELFNAME: TFIBStringField
      DisplayLabel = #1074#1083#1072#1076#1077#1083#1077#1094
      FieldName = 'SELFNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taItemHistoryID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taItemHistoryART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taItemHistoryARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
  end
  object dsrItemHistory: TDataSource
    DataSet = taItemHistory
    Left = 236
    Top = 160
  end
  object dsrPriceACTS: TDataSource
    DataSet = taPriceACTS
    Left = 16
    Top = 256
  end
  object taPriceACTS: TpFIBDataSet
    DeleteSQL.Strings = (
      'DELETE'
      ' from ACT_ITEM'
      'where ACT_ITEMID=:OLD_ACT_ITEMID')
    InsertSQL.Strings = (
      'INSERT'
      'INTO ACT_ITEM(ACT_ITEMID,OLD_PRICE,NEW_PRICE,PRICEID,INVID)'
      'VALUES(:ACT_ITEMID,333,:444,:PRICEID,:INVID)')
    SelectSQL.Strings = (
      'SELECT '
      '   ACT_ITEMID,'
      '   OLD_PRICE,'
      '   NEW_PRICE,'
      '   PRICEID,'
      '   ART,'
      '   ART2 ,'
      '   ACTDATE,'
      '   INVID,'
      '   Q,'
      '   SUM_Q0,'
      '   IN_SUM,'
      '   OUT_SUM,'
      '   RAZ_SUM'
      'FROM ACT_S(:INVID)'
      '')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeOpen = taPriceACTSBeforeOpen
    OnCalcFields = CalcRecNo
    OnNewRecord = taPriceACTSNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 16
    Top = 208
    object taPriceACTSRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taPriceACTSOLD_PRICE: TFloatField
      DisplayLabel = #1057#1090#1072#1088#1072#1103' '#1094#1077#1085#1072
      FieldName = 'OLD_PRICE'
      Origin = 'ACT_S.OLD_PRICE'
      currency = True
    end
    object taPriceACTSNEW_PRICE: TFloatField
      DisplayLabel = #1053#1086#1074#1072#1103' '#1094#1077#1085#1072
      FieldName = 'NEW_PRICE'
      Origin = 'ACT_S.NEW_PRICE'
      currency = True
    end
    object taPriceACTSART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'ACT_S.ART'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taPriceACTSART2: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083'2'
      DisplayWidth = 200
      FieldName = 'ART2'
      Origin = 'ACT_S.ART2'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taPriceACTSACTDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'ACTDATE'
      Origin = 'ACT_S.ACTDATE'
    end
    object taPriceACTSACT_ITEMID: TIntegerField
      FieldName = 'ACT_ITEMID'
      Origin = 'ACT_S.ACT_ITEMID'
    end
    object taPriceACTSINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'ACT_S.INVID'
    end
    object taPriceACTSPRICEID: TIntegerField
      FieldName = 'PRICEID'
      Origin = 'ACT_S.PRICEID'
    end
    object taPriceACTSSUM_Q0: TFloatField
      DisplayLabel = #1055#1077#1088#1077#1086#1094'. '#1074#1077#1089' ('#1082#1086#1083'-'#1074#1086')'
      FieldName = 'SUM_Q0'
      Origin = 'ACT_S.SUM_Q0'
      DisplayFormat = '0.00'
    end
    object taPriceACTSIN_SUM: TFloatField
      DisplayLabel = #1057#1090#1086#1080#1084'. '#1076#1086' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      FieldName = 'IN_SUM'
      Origin = 'ACT_S.IN_SUM'
      currency = True
    end
    object taPriceACTSOUT_SUM: TFloatField
      DisplayLabel = #1057#1090#1086#1080#1084'. '#1087#1086#1089#1083#1077' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      FieldName = 'OUT_SUM'
      Origin = 'ACT_S.OUT_SUM'
      currency = True
    end
    object taPriceACTSRAZ_SUM: TFloatField
      DisplayLabel = #1056#1072#1079#1085#1080#1094#1072
      FieldName = 'RAZ_SUM'
      Origin = 'ACT_S.RAZ_SUM'
      currency = True
    end
    object taPriceACTSQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'ACT_S.Q'
    end
  end
  object dsrINVNO: TDataSource
    DataSet = taINVNO
    Left = 185
    Top = 376
  end
  object taINVNO: TpFIBDataSet
    SelectSQL.Strings = (
      'select  DOCNO,INVID'
      'from INV')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 73
    Top = 208
  end
  object taStoreItems: TpFIBDataSet
    SelectSQL.Strings = (
      'select UID,'
      '          ART,'
      '         ART2,'
      '         FULLART,'
      '         SZ,'
      '          W'
      'from STORE_ITEMS(:DEPID1, :DEPID2, :ART2ID, :WSZ, :SZ)'
      'ORDER BY UID')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taStoreItemsBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 436
    Top = 4
    object taStoreItemsUID: TIntegerField
      DisplayLabel = #1059#1085'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
      Origin = 'STORE_ITEMS.UID'
    end
    object taStoreItemsART: TFIBStringField
      FieldName = 'ART'
      Origin = 'STORE_ITEMS.ART'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taStoreItemsART2: TFIBStringField
      DisplayWidth = 200
      FieldName = 'ART2'
      Origin = 'STORE_ITEMS.ART2'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taStoreItemsFULLART: TFIBStringField
      DisplayLabel = #1055#1086#1083#1085'. '#1072#1088#1090#1080#1082#1091#1083
      FieldName = 'FULLART'
      Origin = 'STORE_ITEMS.FULLART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taStoreItemsSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'STORE_ITEMS.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taStoreItemsW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'STORE_ITEMS.W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
  end
  object dsrStoreItems: TDataSource
    DataSet = taStoreItems
    Left = 436
    Top = 48
  end
  object sqlPrice: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'update SEL S'
      'set S.PRICE=:PRICE'
      'where S.INVID=:INVID and'
      '          S.ART2ID=:ART2ID;')
    Left = 584
    Top = 112
  end
  object dsrSUM: TDataSource
    DataSet = taSUM
    Left = 480
    Top = 160
  end
  object taSUM: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ASSORT,Q,W,SUMM'
      'FROM STORE_SUM(:STATE,:BD,:ED,:DEPID1,:DEPID2)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 480
    Top = 112
    object taSUMASSORT: TIntegerField
      FieldName = 'ASSORT'
      Origin = 'STORE_SUM.ASSORT'
    end
    object taSUMQ: TIntegerField
      FieldName = 'Q'
      Origin = 'STORE_SUM.Q'
    end
    object taSUMW: TFloatField
      FieldName = 'W'
      Origin = 'STORE_SUM.W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taSUMSUMM: TFloatField
      FieldName = 'SUMM'
      Origin = 'STORE_SUM.SUMM'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
  end
  object taInvByItems: TpFIBDataSet
    SelectSQL.Strings = (
      
        'Select UID, SZ, W, ART ,ART2, INS, U, SEMIS, PRILL, ART2ID, PRIC' +
        'E, PRICE2, SAMID, COMMENTART'
      'from INV_BY_ITEMS(:INVID)'
      '')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taInvByItemsBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 188
    Top = 208
    object taInvByItemsUID: TIntegerField
      FieldName = 'UID'
      Origin = 'INV_BY_ITEMS.UID'
    end
    object taInvByItemsART: TFIBStringField
      FieldName = 'ART'
      Origin = 'INV_BY_ITEMS.ART'
      EmptyStrToNull = True
    end
    object taInvByItemsART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'INV_BY_ITEMS.ART2'
      EmptyStrToNull = True
    end
    object taInvByItemsSZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'INV_BY_ITEMS.SZ'
      Size = 5
      EmptyStrToNull = True
    end
    object taInvByItemsW: TFloatField
      FieldName = 'W'
      Origin = 'INV_BY_ITEMS.W'
    end
    object taInvByItemsINS: TFIBStringField
      FieldName = 'INS'
      Origin = 'INV_BY_ITEMS.INS'
      Size = 5
      EmptyStrToNull = True
    end
    object taInvByItemsU: TFIBStringField
      FieldName = 'U'
      Origin = 'INV_BY_ITEMS.U'
      Size = 5
      EmptyStrToNull = True
    end
    object taInvByItemsSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Origin = 'INV_BY_ITEMS.SEMIS'
      Size = 5
      EmptyStrToNull = True
    end
    object taInvByItemsPRILL: TFIBStringField
      FieldName = 'PRILL'
      Origin = 'INV_BY_ITEMS.PRILL'
      EmptyStrToNull = True
    end
    object taInvByItemsART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'INV_BY_ITEMS.ART2ID'
    end
    object taInvByItemsPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'INV_BY_ITEMS.PRICE'
    end
    object taInvByItemsPRICE2: TFloatField
      FieldName = 'PRICE2'
      Origin = 'INV_BY_ITEMS.PRICE2'
    end
    object taInvByItemsSAMID: TFIBStringField
      FieldName = 'SAMID'
      Origin = 'INV_BY_ITEMS.SAMID'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taInvByItemsCOMMENTART: TFIBStringField
      FieldName = 'COMMENTART'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrINVByItems: TDataSource
    DataSet = taInvByItems
    Left = 188
    Top = 256
  end
  object taSZbyName: TpFIBDataSet
    SelectSQL.Strings = (
      'Select  ID'
      'from D_SZ '
      'where NAME=:SZ')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 584
    Top = 160
    object taSZbyNameID: TIntegerField
      FieldName = 'ID'
      Origin = 'D_SZ.ID'
      Required = True
    end
  end
  object taCURUID: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT '
      'UID'
      'from'
      'GET_CURRUID(:ART2ID,:SZID,:W)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 632
    Top = 224
    object taCURUIDUID: TIntegerField
      FieldName = 'UID'
      Origin = 'GET_CURRUID.UID'
    end
  end
  object taSELExport: TpFIBDataSet
    InsertSQL.Strings = (
      
        'insert into SEL(SELID, INVID, ART2ID, D_ARTID, PRICE, TPRICE, PR' +
        'ICE2,'
      '                         AKCIZ, PNDS, NDSID)'
      
        'values    (:SELID, :INVID, :ART2ID, :D_ARTID, :PRICE, :TPRICE, :' +
        'PRICE2,'
      '                         :AKCIZ,:PNDS,:NDSID)'
      '')
    SelectSQL.Strings = (
      'SELECT SELID, '
      '               INVID,'
      '               ART2ID,'
      '               ART2 ,'
      '               FULLART,'
      '               PRICE,           '
      '               TPRICE,     '
      '               QUANTITY,'
      '               TOTALWEIGHT,'
      '               Q,'
      '               W,   '
      '               PRICE2,'
      '               PNDS,'
      '               UNITID,'
      '               NDSID, '
      '               ART, '
      '               USEMARGIN,   '
      '               FULLSUM,'
      
        '              D_INSID ,  D_ARTID ,  EP2,  SSUM,   SSUMD, SSUMF, ' +
        'PRILL, AKCIZ, GOOD '
      'from SEL_S(:ASINVID)'
      'ORDER BY ART '
      ''
      ''
      '')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taSELExportBeforeOpen
    OnNewRecord = taSELExportNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 588
    Top = 360
    object taSELExportSELID: TIntegerField
      FieldName = 'SELID'
      Origin = 'SEL_S.SELID'
    end
    object taSELExportINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SEL_S.INVID'
    end
    object taSELExportART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'SEL_S.ART2ID'
    end
    object taSELExportART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'SEL_S.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taSELExportFULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'SEL_S.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSELExportPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'SEL_S.PRICE'
    end
    object taSELExportTPRICE: TFloatField
      FieldName = 'TPRICE'
      Origin = 'SEL_S.TPRICE'
    end
    object taSELExportQUANTITY: TSmallintField
      FieldName = 'QUANTITY'
      Origin = 'SEL_S.QUANTITY'
    end
    object taSELExportTOTALWEIGHT: TFloatField
      FieldName = 'TOTALWEIGHT'
      Origin = 'SEL_S.TOTALWEIGHT'
    end
    object taSELExportQ: TIntegerField
      FieldName = 'Q'
      Origin = 'SEL_S.Q'
    end
    object taSELExportW: TFloatField
      FieldName = 'W'
      Origin = 'SEL_S.W'
    end
    object taSELExportPRICE2: TFloatField
      FieldName = 'PRICE2'
      Origin = 'SEL_S.PRICE2'
    end
    object taSELExportPNDS: TFloatField
      FieldName = 'PNDS'
      Origin = 'SEL_S.PNDS'
    end
    object taSELExportUNITID: TIntegerField
      FieldName = 'UNITID'
      Origin = 'SEL_S.UNITID'
    end
    object taSELExportNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SEL_S.NDSID'
    end
    object taSELExportART: TFIBStringField
      FieldName = 'ART'
      Origin = 'SEL_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSELExportUSEMARGIN: TSmallintField
      FieldName = 'USEMARGIN'
      Origin = 'SEL_S.USEMARGIN'
    end
    object taSELExportFULLSUM: TFloatField
      FieldName = 'FULLSUM'
      Origin = 'SEL_S.FULLSUM'
    end
    object taSELExportD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'SEL_S.D_INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSELExportD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'SEL_S.D_ARTID'
    end
    object taSELExportEP2: TFloatField
      FieldName = 'EP2'
      Origin = 'SEL_S.EP2'
    end
    object taSELExportSSUM: TFloatField
      FieldName = 'SSUM'
      Origin = 'SEL_S.SSUM'
    end
    object taSELExportSSUMD: TFloatField
      FieldName = 'SSUMD'
      Origin = 'SEL_S.SSUMD'
    end
    object taSELExportSSUMF: TFloatField
      FieldName = 'SSUMF'
      Origin = 'SEL_S.SSUMF'
    end
    object taSELExportPRILL: TFIBStringField
      FieldName = 'PRILL'
      Origin = 'SEL_S.PRILL'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSELExportAKCIZ: TFloatField
      FieldName = 'AKCIZ'
      Origin = 'SEL_S.AKCIZ'
    end
    object taSELExportGOOD: TFIBStringField
      FieldName = 'GOOD'
      Origin = 'SEL_S.GOOD'
      EmptyStrToNull = True
    end
  end
  object taSIExport: TpFIBDataSet
    InsertSQL.Strings = (
      'INSERT INTO SITEM(SITEMID, SELID, UID, W, SZ,  ITYPE, DT,DEPID,'
      '                                    ARTID, ART2ID, AKCIZ)'
      'VALUES (?SITEMID, ?SELID, ?UID, ?W, ?SS,  ?ITYPE, ?DT,?DEPID, '
      '                                    ?ARTID,?ART2ID,  ?AKCIZ)')
    SelectSQL.Strings = (
      
        'SELECT S.SITEMID, S.SELID, S.W, S.SZ ss, S.UID,   S.ITYPE, S.DT,' +
        ' S.ARTID, S.ART2ID,  S.AKCIZ, S.DEPID'
      'FROM SITEM S'
      'WHERE S.SELID=?SELID')
    CacheModelOptions.BufferChunks = 1000
    OnNewRecord = taSIExportNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 588
    Top = 312
    object taSIExportSITEMID: TIntegerField
      FieldName = 'SITEMID'
      Origin = 'SITEM.SITEMID'
      Required = True
    end
    object taSIExportSELID: TIntegerField
      FieldName = 'SELID'
      Origin = 'SITEM.SELID'
      Required = True
    end
    object taSIExportW: TFloatField
      FieldName = 'W'
      Origin = 'SITEM.W'
      Required = True
    end
    object taSIExportSS: TIntegerField
      FieldName = 'SS'
      Origin = 'SITEM.SZ'
      Required = True
    end
    object taSIExportUID: TIntegerField
      FieldName = 'UID'
      Origin = 'SITEM.UID'
      Required = True
    end
    object taSIExportITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'SITEM.ITYPE'
      Required = True
    end
    object taSIExportDT: TDateTimeField
      FieldName = 'DT'
      Origin = 'SITEM.DT'
      Required = True
    end
    object taSIExportARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'SITEM.ARTID'
      Required = True
    end
    object taSIExportART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'SITEM.ART2ID'
      Required = True
    end
    object taSIExportAKCIZ: TFloatField
      FieldName = 'AKCIZ'
      Origin = 'SITEM.AKCIZ'
      Required = True
    end
    object taSIExportDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SITEM.DEPID'
    end
  end
  object taPriceDict: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRICE'
      'SET TPRICE=:TPRICE,'
      '        PRICE=:TPRICE'
      'where PRICEID=:OLD_PRICEID')
    SelectSQL.Strings = (
      'SELECT  '
      '  ART,'
      '  ART2,'
      '  DEP,'
      '  PRICE,'
      '  TPRICE,'
      '  UNIT,'
      '  PRICEID'
      'from DICT_PRICE(:DEPID1,:DEPID2,:ART_)'
      'order by ART')
    CacheModelOptions.BufferChunks = 1000
    AfterPost = taPriceDictAfterPost
    BeforeEdit = taPriceDictBeforeEdit
    BeforeOpen = taPriceDictBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 131
    Top = 208
    object taPriceDictART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'DICT_PRICE.ART'
      EmptyStrToNull = True
    end
    object taPriceDictART2: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083'2'
      FieldName = 'ART2'
      Origin = 'DICT_PRICE.ART2'
      EmptyStrToNull = True
    end
    object taPriceDictDEP: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'DEP'
      Origin = 'DICT_PRICE.DEP'
      Size = 40
      EmptyStrToNull = True
    end
    object taPriceDictPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'PRICE'
      Origin = 'DICT_PRICE.PRICE'
      currency = True
    end
    object taPriceDictUNIT: TFIBStringField
      DisplayLabel = #1045#1048
      FieldName = 'UNIT'
      Origin = 'DICT_PRICE.UNIT'
      Size = 10
      EmptyStrToNull = True
    end
    object taPriceDictTPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'TPRICE'
      Origin = 'DICT_PRICE.TPRICE'
      currency = True
    end
    object taPriceDictPRICEID: TIntegerField
      FieldName = 'PRICEID'
      Origin = 'DICT_PRICE.PRICEID'
    end
  end
  object dsrPriceDict: TDataSource
    DataSet = taPriceDict
    Left = 131
    Top = 256
  end
  object sqlACTPrice: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      
        'execute procedure SET_NEW_PRICE(:DEPID,:NEW_PRICE,              ' +
        '                                                 :NEW_DATE, :USE' +
        'RID)')
    Left = 16
    Top = 304
  end
  object taPACTS: TpFIBDataSet
    InsertSQL.Strings = (
      'INSERT'
      'INTO ACT_ITEM(ACT_ITEMID,OLD_PRICE,NEW_PRICE,PRICEID,INVID)'
      'VALUES(:ACT_ITEMID,:OLD_PRICE,:NEW_PRICE,:PRICEID,:INVID)')
    SelectSQL.Strings = (
      'SELECT * '
      'FROM ACT_ITEM')
    CacheModelOptions.BufferChunks = 1000
    OnNewRecord = taPACTSNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 16
    Top = 368
    object taPACTSACT_ITEMID: TIntegerField
      FieldName = 'ACT_ITEMID'
      Origin = 'ACT_ITEM.ACT_ITEMID'
      Required = True
    end
    object taPACTSOLD_PRICE: TFloatField
      FieldName = 'OLD_PRICE'
      Origin = 'ACT_ITEM.OLD_PRICE'
      Required = True
    end
    object taPACTSNEW_PRICE: TFloatField
      FieldName = 'NEW_PRICE'
      Origin = 'ACT_ITEM.NEW_PRICE'
      Required = True
    end
    object taPACTSPRICEID: TIntegerField
      FieldName = 'PRICEID'
      Origin = 'ACT_ITEM.PRICEID'
      Required = True
    end
    object taPACTSINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'ACT_ITEM.INVID'
    end
  end
  object dsrPACTS: TDataSource
    DataSet = taPriceACTS
    Left = 16
    Top = 416
  end
  object taASItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure SItem_U_A(:ID, :W, :SELID, :TPRICE)')
    DeleteSQL.Strings = (
      'delete from  SItem where SItemId=:OLD_Id')
    InsertSQL.Strings = (
      
        'execute procedure SItem_I_A(:ID, :SELID, :ARTID, :ART2ID, :OPERI' +
        'D,'
      '    :UID, :W, :SZID, :P0, :INVID, :UA)')
    RefreshSQL.Strings = (
      'select ID, SELID, ARTID, ART, ART2ID, ART2, OPERID, OPERNAME,'
      '    UID, W, SZID, SZNAME, U, P0, INVID, PRICE, TPRICE, UA'
      'from SItem_R_A(:ID)')
    SelectSQL.Strings = (
      'select ID, SELID, ARTID, ART, ART2ID, ART2, OPERID, OPERNAME,'
      '    UID, W, SZID, SZNAME, U, P0, INVID, PRICE, TPRICE, UA'
      'from SItem_S_A(:INVID)'
      'order by ART, ART2, SZNAME')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = taASItemAfterDelete
    AfterPost = taASItemAfterPost
    BeforeOpen = taASItemBeforeOpen
    OnCalcFields = CalcRecNo
    OnNewRecord = taASItemNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 572
    Top = 4
    object taASItemRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taASItemID: TIntegerField
      FieldName = 'ID'
      Origin = 'SITEM_S_A.ID'
    end
    object taASItemSELID: TIntegerField
      FieldName = 'SELID'
      Origin = 'SITEM_S_A.SELID'
    end
    object taASItemARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'SITEM_S_A.ARTID'
    end
    object taASItemART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'SITEM_S_A.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taASItemART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'SITEM_S_A.ART2ID'
    end
    object taASItemART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'SITEM_S_A.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taASItemOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'SITEM_S_A.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taASItemOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'SITEM_S_A.OPERNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taASItemUID: TIntegerField
      DisplayLabel = #1048#1076'. '#8470
      FieldName = 'UID'
      Origin = 'SITEM_S_A.UID'
    end
    object taASItemW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SITEM_S_A.W'
    end
    object taASItemSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'SITEM_S_A.SZID'
    end
    object taASItemSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      Origin = 'SITEM_S_A.SZNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taASItemU: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'SITEM_S_A.U'
      OnGetText = taASItemUGetText
    end
    object taASItemP0: TSmallintField
      FieldName = 'P0'
      Origin = 'SITEM_S_A.P0'
    end
    object taASItemINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SITEM_S_A.INVID'
    end
    object taASItemPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'SITEM_S_A.PRICE'
    end
    object taASItemTPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1091#1095#1077#1090#1085#1072#1103
      FieldName = 'TPRICE'
      Origin = 'SITEM_S_A.TPRICE'
      currency = True
    end
    object taASItemUA: TSmallintField
      FieldName = 'UA'
      Origin = 'SITEM_S_A.UA'
    end
  end
  object dsrASItem: TDataSource
    DataSet = taASItem
    Left = 572
    Top = 48
  end
  object taStoneItems: TpFIBDataSet
    SelectSQL.Strings = (
      'Select Stones, MATNAME, QUANTITY, WEIGHT,'
      '    COLORNAME, EDGSHAPEID, EDGTYPEID, CHROMATICITY,'
      '    CLEANNES, IGR'
      'from STONES_S(:ART2ID)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taStoneItemsBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 338
    Top = 204
    object taStoneItemsSTONES: TFIBStringField
      FieldName = 'STONES'
      Origin = 'STONES_S.STONES'
      Size = 100
      EmptyStrToNull = True
    end
    object taStoneItemsMATNAME: TFIBStringField
      FieldName = 'MATNAME'
      EmptyStrToNull = True
    end
    object taStoneItemsQUANTITY: TFIBIntegerField
      FieldName = 'QUANTITY'
    end
    object taStoneItemsWEIGHT: TFIBFloatField
      FieldName = 'WEIGHT'
    end
    object taStoneItemsCOLORNAME: TFIBStringField
      FieldName = 'COLORNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taStoneItemsEDGSHAPEID: TFIBStringField
      FieldName = 'EDGSHAPEID'
      Size = 10
      EmptyStrToNull = True
    end
    object taStoneItemsEDGTYPEID: TFIBStringField
      FieldName = 'EDGTYPEID'
      Size = 10
      EmptyStrToNull = True
    end
    object taStoneItemsCHROMATICITY: TFIBStringField
      FieldName = 'CHROMATICITY'
      Size = 10
      EmptyStrToNull = True
    end
    object taStoneItemsCLEANNES: TFIBStringField
      FieldName = 'CLEANNES'
      Size = 10
      EmptyStrToNull = True
    end
    object taStoneItemsIGR: TFIBStringField
      FieldName = 'IGR'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsrStoneItems: TDataSource
    DataSet = taStoneItems
    Left = 340
    Top = 252
  end
  object sqlCreateAnaliz: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'Execute procedure CREATE_APPL(:APPLID,:BD, :ED)')
    Left = 345
    Top = 516
  end
  object sqlUpdateARTSum: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'Execute procedure JA_UPDATE_SEL('
      '   :APPLID ,'
      '    :ART ,'
      '    :SZ ,'
      '    :INS ,'
      '    :INW,'
      '    :INQ ,'
      '    :DEBTORS ,'
      '    :DEBTORW ,'
      '    :DEBTORQ ,'
      '    :REJS,'
      '    :REJW ,'
      '    :REJQ ,'
      '    :SALES ,'
      '    :SALEW ,'
      '    :SALEQ ,'
      '    :RETS ,'
      '    :RETW ,'
      '    :RETQ ,'
      '    :OUTS ,'
      '    :OUTW ,'
      '    :OUTQ ,'
      '    :STORES ,'
      '    :STOREW ,'
      '    :STOREQ )')
    Left = 167
    Top = 412
  end
  object dsrError_NEA: TDataSource
    DataSet = taError_NEA
    Left = 640
    Top = 52
  end
  object taError_NEA: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT '
      'ARTID,'
      'ART'
      'FROM ERR_NOT_EMPTY_ART2')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 640
    Top = 4
    object taError_NEAART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'ERR_NOT_EMPTY_ART2.ART'
      EmptyStrToNull = True
    end
    object taError_NEAARTID: TIntegerField
      DisplayLabel = #1040#1088#1090#1048#1044
      FieldName = 'ARTID'
      Origin = 'ERR_NOT_EMPTY_ART2.ARTID'
    end
  end
  object dsrPact_Items: TDataSource
    DataSet = taPact_Items
    Left = 714
    Top = 54
  end
  object taPact_Items: TpFIBDataSet
    DeleteSQL.Strings = (
      'DELETE FROM PACT_ITEMS'
      'where ACT_ITEMSID=:OLD_ACT_ITEMSID')
    SelectSQL.Strings = (
      'SELECT'
      ' ACT_ITEMSID,'
      ' UID,'
      ' W, '
      ' OLD_PRICE,'
      ' NEW_PRICE,'
      ' OLD_PRICE*Q0,'
      ' NEW_PRICE*Q0'
      'FROM PACT_ITEMS'
      'WHERE ACT_ITEMID=:ACT_ITEMID')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    BeforeDelete = DelConf
    BeforeOpen = taPact_ItemsBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 712
    Top = 8
    object taPact_ItemsUID: TIntegerField
      FieldName = 'UID'
      Origin = 'PACT_ITEMS.UID'
      Required = True
    end
    object taPact_ItemsW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'PACT_ITEMS.W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taPact_ItemsOLD_PRICE: TFloatField
      DisplayLabel = #1057#1090#1072#1088#1072#1103' '#1094#1077#1085#1072
      FieldName = 'OLD_PRICE'
      Origin = 'PACT_ITEMS.OLD_PRICE'
      Required = True
      currency = True
    end
    object taPact_ItemsNEW_PRICE: TFloatField
      DisplayLabel = #1053#1086#1074#1072#1103' '#1094#1077#1085#1072
      FieldName = 'NEW_PRICE'
      Origin = 'PACT_ITEMS.NEW_PRICE'
      Required = True
      currency = True
    end
    object taPact_ItemsF_1: TFloatField
      DisplayLabel = #1057#1090#1072#1088#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'F_1'
      currency = True
    end
    object taPact_ItemsF_2: TFloatField
      DisplayLabel = #1053#1086#1074#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'F_2'
      currency = True
    end
  end
  object taWH_items: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT '
      '  SEMISID,'
      '  SEMISNAME, '
      '  FROM_Q,'
      '  FROM_W,'
      '  DEP_Q,'
      '  DEP_W'
      'from WHSEMIS_ITEMS(:FROMDEPID,:DEPID)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taWH_itemsBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 712
    Top = 112
    object taWH_itemsSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'WHSEMIS_ITEMS.SEMISID'
      Size = 10
      EmptyStrToNull = True
    end
    object taWH_itemsSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      DisplayWidth = 40
      FieldName = 'SEMISNAME'
      Origin = 'WHSEMIS_ITEMS.SEMISNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taWH_itemsFROM_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086' '#1054#1058
      FieldName = 'FROM_Q'
      Origin = 'WHSEMIS_ITEMS.FROM_Q'
    end
    object taWH_itemsFROM_W: TFloatField
      DisplayLabel = #1042#1077#1089'_'#1054#1058
      FieldName = 'FROM_W'
      Origin = 'WHSEMIS_ITEMS.FROM_W'
      DisplayFormat = '0.00'
    end
    object taWH_itemsDEP_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086' '#1042
      FieldName = 'DEP_Q'
      Origin = 'WHSEMIS_ITEMS.DEP_Q'
    end
    object taWH_itemsDEP_W: TFloatField
      DisplayLabel = #1042#1077#1089'_'#1058#1054
      FieldName = 'DEP_W'
      Origin = 'WHSEMIS_ITEMS.DEP_W'
      DisplayFormat = '0.00'
    end
  end
  object dsrWH_items: TDataSource
    DataSet = taWH_items
    Left = 712
    Top = 160
  end
  object taSIELItems: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SIEL set'
      '     Q=:Q,'
      '     W=:W'
      'where id=:OLD_Id')
    DeleteSQL.Strings = (
      'delete from Siel where Id=:OLD_Id')
    InsertSQL.Strings = (
      'insert into SIEL(ID, SEMIS, INVID, Q, W,T,PRICE,TPRICE,NDSID)'
      'values    (:ID,:SEMIS, :INVID_,:Q,:W,:T,:PRICE,:TPRICE,:NDSID)')
    RefreshSQL.Strings = (
      'SELECT '
      '  INVID_,'
      '  SEMIS,'
      '  T,'
      '  ID,'
      '  SEMISNAME,'
      '  W,'
      '  Q'
      'from'
      'SIEL_R(:ID)')
    SelectSQL.Strings = (
      'SELECT '
      '  INVID_,'
      '  SEMIS,'
      '  T,'
      '  ID,'
      '  SEMISNAME,'
      '  W,'
      '  Q,'
      '  UQ,'
      '  UW,'
      '  PRICE,'
      '  TPRICE,'
      '  NDSID,'
      '  NDS'
      'from'
      'SIEL_S(:INVID)')
    CacheModelOptions.BufferChunks = 1000
    AfterInsert = CommitRetaining
    AfterPost = taSIELItemsAfterPost
    BeforeOpen = taSIELItemsBeforeOpen
    OnCalcFields = CalcRecNo
    OnEditError = taSIELItemsEditError
    OnNewRecord = taSIELItemsNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 632
    Top = 112
    object taSIELItemsRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taSIELItemsSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'SIEL_S.SEMISNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taSIELItemsW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SIEL_S.W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taSIELItemsQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'SIEL_S.Q'
    end
    object taSIELItemsINVID_: TIntegerField
      FieldName = 'INVID_'
      Origin = 'SIEL_S.INVID_'
    end
    object taSIELItemsSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Origin = 'SIEL_S.SEMIS'
      Size = 10
      EmptyStrToNull = True
    end
    object taSIELItemsT: TSmallintField
      FieldName = 'T'
      Origin = 'SIEL_S.T'
    end
    object taSIELItemsID: TIntegerField
      FieldName = 'ID'
      Origin = 'SIEL_S.ID'
    end
    object taSIELItemsUQ: TSmallintField
      FieldName = 'UQ'
      Origin = 'SIEL_S.UQ'
    end
    object taSIELItemsUW: TSmallintField
      FieldName = 'UW'
      Origin = 'SIEL_S.UW'
    end
    object taSIELItemsPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'SIEL_S.PRICE'
    end
    object taSIELItemsTPRICE: TFloatField
      FieldName = 'TPRICE'
      Origin = 'SIEL_S.TPRICE'
    end
    object taSIELItemsNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SIEL_S.NDSID'
    end
    object taSIELItemsNDS: TFloatField
      FieldName = 'NDS'
      Origin = 'SIEL_S.NDS'
    end
  end
  object dsrSIEItems: TDataSource
    DataSet = taSIELItems
    Left = 632
    Top = 160
  end
  object sqlUpdateJA: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'Execute procedure '
      'UPDATE_JA(:APPLID)')
    Left = 163
    Top = 360
  end
  object sqlJA_SUM_AQ: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'Execute procedure JA_SUM_QNOPROCESS(:INVID)')
    Left = 260
    Top = 368
  end
  object taConst: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Const'
      'set VAL=:VAL, '
      '    STR_VAL=:STR_VAL'
      'where'
      'CONST_ID=:OLD_CONST_ID')
    SelectSQL.Strings = (
      'Select '
      'CONST_ID,'
      'NAME,'
      'VAL,'
      'STR_VAL'
      'From CONST')
    CacheModelOptions.BufferChunks = 1000
    AfterEdit = CommitRetaining
    Transaction = dm.tr
    Database = dm.db
    Left = 784
    Top = 8
    object taConstNAME: TFIBStringField
      DisplayLabel = #1050#1086#1085#1089#1090#1072#1085#1090#1072
      FieldName = 'NAME'
      Origin = 'CONST.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taConstVAL: TFloatField
      DisplayLabel = #1047#1085#1072#1095#1077#1085#1080#1077
      FieldName = 'VAL'
      Origin = 'CONST.VAL'
    end
    object taConstCONST_ID: TFIBIntegerField
      FieldName = 'CONST_ID'
    end
    object taConstSTR_VAL: TFIBStringField
      FieldName = 'STR_VAL'
      Size = 110
      EmptyStrToNull = True
    end
  end
  object dsrConst: TDataSource
    DataSet = taConst
    Left = 784
    Top = 56
  end
  object sqlRepriceForU: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      
        'execute procedure SET_NEW_PRICE_FOR_U(:DEPID,:K,:NEW_DATE,:USERI' +
        'D)')
    Left = 89
    Top = 304
  end
  object sqlAddSel: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      
        'execute procedure SEL_INSERT(:INVID,:ART2ID,:ARTID,:UID,:W,:SZ,:' +
        'PRICE)')
    Left = 896
    Top = 280
  end
  object sqlAddINV: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure INV_INSERT(:DOCNO,:INVID,:ITYPE,:DOCDATE,'
      
        '                                                     :DEPNAME,:S' +
        'UPNAME,:USERID)')
    Left = 800
    Top = 448
  end
  object sqlAddPact: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure PACT_INSERT(:UID, :ACT_ITEMID)')
    Left = 552
    Top = 512
  end
  object sqlRepriceINV: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure CREATE_ACT_PRICE_FOR_INV(:INVID,:UP)')
    Left = 171
    Top = 300
  end
  object taSemisReport: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Semis_Report_Dep set'
      '  FQ=:FQ,'
      '  FW=:FW,'
      '  QA=:QA,'
      '  FQA=:FQA'
      'where ID=:ID')
    DeleteSQL.Strings = (
      'delete from Semis_Report_Dep where ID=:ID')
    InsertSQL.Strings = (
      'insert into Semis_Report_Dep(ID, ACT_ID, SEMISID, OPERID,'
      '    DEPID, REST_IN_Q, REST_IN_W, IN_Q, IN_W,  IN_MOVE_Q,'
      '    IN_MOVE_W, OUT_Q, OUT_W, OUT_MOVE_Q, OUT_MOVE_W,'
      '    IN_PS_Q, IN_PS_W, OUT_PS_Q, OUT_PS_W,'
      '    REST_OUT_Q, REST_OUT_W, UQ, UW,'
      '    FQ, FW, RQ, RW, QA, FQA, BULK_Q, BULK_W,'
      '    AFF_S_Q, AFF_S_W, AFF_L_GET_Q, AFF_L_GET_W, '
      '    AFF_L_RET_Q,  AFF_L_RET_W, CHARGE_Q, CHARGE_W)'
      'values (:ID, :ACTID, :SEMISID, :OPERID,'
      '    :DEPID, :REST_IN_Q, :REST_IN_W, :IN_Q, :IN_W,  :IN_MOVE_Q,'
      '    :IN_MOVE_W, :OUT_Q, :OUT_W, :OUT_MOVE_Q, :OUT_MOVE_W,'
      '    :IN_PS_Q, :IN_PS_W, :OUT_PS_Q, :OUT_PS_W,'
      
        '    :REST_OUT_Q, :REST_OUT_W, :UQ, :UW, :FQ, :FW, :RQ, :RW, :QA,' +
        ' :FQA,'
      '    :BULK_Q, :BULK_W,  :AFF_S_Q, :AFF_S_W, :AFF_L_GET_Q, '
      
        '    :AFF_L_GET_W, :AFF_L_RET_Q,  :AFF_L_RET_W, :CHARGE_Q, :CHARG' +
        'E_W)')
    SelectSQL.Strings = (
      'select '
      '  ID, '
      '  ACTID,'
      '  REST_IN_Q, '
      '  REST_IN_W,'
      '  IN_Q, '
      '  IN_W, '
      '  IN_MOVE_Q, '
      '  IN_MOVE_W,'
      '  IN_PS_Q, '
      '  IN_PS_W,'
      '  OUT_Q, '
      '  OUT_W,'
      '  OUT_MOVE_Q, '
      '  OUT_MOVE_W,'
      '  OUT_PS_Q, '
      '  OUT_PS_W,'
      '  REST_OUT_Q, '
      '  REST_OUT_W,'
      '  UQ_NAME, '
      '  UW_NAME,'
      '  OPERATION, '
      '  SEMIS, '
      '  DEPNAME, '
      '  DEPID, '
      '  SEMISID, '
      '  OPERID, '
      '  UQ, '
      '  UW,'
      '  FQ, '
      '  FW, '
      '  RQ, '
      '  RW, '
      '  QA, '
      '  FQA, '
      '  BULK_Q, '
      '  BULK_W,'
      '  AFF_S_Q, '
      '  AFF_S_W, '
      '  AFF_L_GET_Q, '
      '  AFF_L_GET_W, '
      '  AFF_L_RET_Q,  '
      '  AFF_L_RET_W, '
      '  CHARGE_Q, '
      '  CHARGE_W'
      'from '
      '  SEMIS_REPORT_S(:ACTID)'
      'where '
      '  SEMIS between :SEMIS1 and :SEMIS2  and'
      '  OPERATION between :OPER1 and :OPER2 and'
      '  MATID between :MATID1 and :MATID2 '
      'order by '
      '  SEMIS, OPERATION')
    CacheModelOptions.BufferChunks = 1000
    BeforeDelete = taSemisReportBeforeDelete
    BeforeEdit = taSemisReportBeforeEdit
    BeforeOpen = taSemisReportBeforeOpen
    BeforePost = taSemisReportBeforePost
    OnCalcFields = taSemisReportCalcFields
    OnNewRecord = taSemisReportNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 262
    Top = 208
    object taSemisReportID: TIntegerField
      FieldName = 'ID'
      Origin = 'SEMIS_REPORT_S.ID'
    end
    object taSemisReportDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Origin = 'SEMIS_REPORT_S.DEPNAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taSemisReportREST_IN_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'REST_IN_Q'
      Origin = 'SEMIS_REPORT_S.REST_IN_Q'
    end
    object taSemisReportREST_IN_W: TFloatField
      DisplayLabel = #1042#1077#1089' '
      FieldName = 'REST_IN_W'
      Origin = 'SEMIS_REPORT_S.REST_IN_W'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSemisReportIN_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'IN_Q'
      Origin = 'SEMIS_REPORT_S.IN_Q'
    end
    object taSemisReportIN_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'IN_W'
      Origin = 'SEMIS_REPORT_S.IN_W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taSemisReportIN_MOVE_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'IN_MOVE_Q'
      Origin = 'SEMIS_REPORT_S.IN_MOVE_Q'
    end
    object taSemisReportIN_MOVE_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'IN_MOVE_W'
      Origin = 'SEMIS_REPORT_S.IN_MOVE_W'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSemisReportIN_PS_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'IN_PS_Q'
      Origin = 'SEMIS_REPORT_S.IN_PS_Q'
    end
    object taSemisReportIN_PS_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'IN_PS_W'
      Origin = 'SEMIS_REPORT_S.IN_PS_W'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSemisReportOUT_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'OUT_Q'
      Origin = 'SEMIS_REPORT_S.OUT_Q'
    end
    object taSemisReportOUT_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'OUT_W'
      Origin = 'SEMIS_REPORT_S.OUT_W'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSemisReportOUT_MOVE_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'OUT_MOVE_Q'
      Origin = 'SEMIS_REPORT_S.OUT_MOVE_Q'
    end
    object taSemisReportOUT_MOVE_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'OUT_MOVE_W'
      Origin = 'SEMIS_REPORT_S.OUT_MOVE_W'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSemisReportOUT_PS_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'OUT_PS_Q'
      Origin = 'SEMIS_REPORT_S.OUT_PS_Q'
    end
    object taSemisReportOUT_PS_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'OUT_PS_W'
      Origin = 'SEMIS_REPORT_S.OUT_PS_W'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSemisReportREST_OUT_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'REST_OUT_Q'
      Origin = 'SEMIS_REPORT_S.REST_OUT_Q'
    end
    object taSemisReportREST_OUT_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'REST_OUT_W'
      Origin = 'SEMIS_REPORT_S.REST_OUT_W'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taSemisReportUQ_NAME: TFIBStringField
      DisplayLabel = #1050#1086#1083'-'#1074#1072
      FieldName = 'UQ_NAME'
      Origin = 'SEMIS_REPORT_S.UQ_NAME'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSemisReportUW_NAME: TFIBStringField
      DisplayLabel = #1042#1077#1089#1072
      FieldName = 'UW_NAME'
      Origin = 'SEMIS_REPORT_S.UW_NAME'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSemisReportSEMIS: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMIS'
      Origin = 'SEMIS_REPORT_S.SEMIS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSemisReportOPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Origin = 'SEMIS_REPORT_S.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSemisReportDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SEMIS_REPORT_S.DEPID'
    end
    object taSemisReportSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'SEMIS_REPORT_S.SEMISID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSemisReportOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'SEMIS_REPORT_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSemisReportUQ: TSmallintField
      FieldName = 'UQ'
      Origin = 'SEMIS_REPORT_S.UQ'
    end
    object taSemisReportUW: TSmallintField
      FieldName = 'UW'
      Origin = 'SEMIS_REPORT_S.UW'
    end
    object taSemisReportFQ: TFloatField
      DisplayLabel = #1060#1072#1082#1090'. '#1082#1086#1083'-'#1074#1086
      FieldName = 'FQ'
      Origin = 'SEMIS_REPORT_S.FQ'
    end
    object taSemisReportFW: TFloatField
      DisplayLabel = #1060#1072#1082#1090'. '#1074#1077#1089
      FieldName = 'FW'
      Origin = 'SEMIS_REPORT_S.FW'
      DisplayFormat = '0.###'
    end
    object taSemisReportRQ: TFloatField
      FieldName = 'RQ'
      Origin = 'SEMIS_REPORT_S.RQ'
    end
    object taSemisReportRW: TFloatField
      FieldName = 'RW'
      Origin = 'SEMIS_REPORT_S.RW'
      DisplayFormat = '0.###'
    end
    object taSemisReportRQ1: TFloatField
      DisplayLabel = #1054#1089#1090#1072#1090#1086#1082' '#1082#1086#1083'-'#1074#1086
      FieldKind = fkCalculated
      FieldName = 'RQ1'
      Calculated = True
    end
    object taSemisReportRW1: TFloatField
      DisplayLabel = #1054#1089#1090#1072#1090#1086#1082' '#1074#1077#1089
      FieldKind = fkCalculated
      FieldName = 'RW1'
      DisplayFormat = '0.###'
      Calculated = True
    end
    object taSemisReportQA: TIntegerField
      DisplayLabel = #1059#1095#1077#1090#1085#1086#1077' '#1082#1086#1083'-'#1074#1086' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      FieldName = 'QA'
      Origin = 'SEMIS_REPORT_S.QA'
    end
    object taSemisReportFQA: TIntegerField
      DisplayLabel = #1060#1072#1082#1090'. '#1082#1086#1083'-'#1074#1086' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      FieldName = 'FQA'
      Origin = 'SEMIS_REPORT_S.FQA'
    end
    object taSemisReportACTID: TIntegerField
      FieldName = 'ACTID'
      Origin = 'SEMIS_REPORT_S.ACTID'
    end
    object taSemisReportOPERNAME: TStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldKind = fkLookup
      FieldName = 'OPERNAME'
      LookupDataSet = dm.taOper
      LookupKeyFields = 'OPERID'
      LookupResultField = 'OPERATION'
      KeyFields = 'OPERID'
      Size = 60
      Lookup = True
    end
    object taSemisReportSEMISNAME: TStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldKind = fkLookup
      FieldName = 'SEMISNAME'
      LookupDataSet = dm.taSemis
      LookupKeyFields = 'SEMISID'
      LookupResultField = 'SEMIS'
      KeyFields = 'SEMISID'
      Size = 40
      Lookup = True
    end
    object taSemisReportBULK_Q: TIntegerField
      FieldName = 'BULK_Q'
      Origin = 'SEMIS_REPORT_S.BULK_Q'
    end
    object taSemisReportBULK_W: TFloatField
      FieldName = 'BULK_W'
      Origin = 'SEMIS_REPORT_S.BULK_W'
    end
    object taSemisReportAFF_S_Q: TIntegerField
      DisplayLabel = #1040#1092#1092#1080#1085#1072#1078' '#1089#1098#1077#1084#1072' '#1082#1086#1083'-'#1074#1086
      FieldName = 'AFF_S_Q'
      Origin = 'SEMIS_REPORT_S.AFF_S_Q'
    end
    object taSemisReportAFF_S_W: TFloatField
      DisplayLabel = #1040#1092#1092#1080#1085#1072#1078' '#1089#1098#1077#1084#1072' '#1074#1077#1089#1072
      FieldName = 'AFF_S_W'
      Origin = 'SEMIS_REPORT_S.AFF_S_W'
      DisplayFormat = '0.###'
    end
    object taSemisReportAFF_L_GET_Q: TIntegerField
      DisplayLabel = #1040#1092#1092#1080#1085#1072#1078' '#1083#1086#1084#1072' '#1074#1099#1076#1072#1085#1086' '#1082#1086#1083'-'#1074#1086
      FieldName = 'AFF_L_GET_Q'
      Origin = 'SEMIS_REPORT_S.AFF_L_GET_Q'
    end
    object taSemisReportAFF_L_GET_W: TFloatField
      DisplayLabel = #1040#1092#1092#1080#1085#1072#1078' '#1083#1086#1084#1072' '#1074#1099#1076#1072#1085#1086' '#1074#1077#1089
      FieldName = 'AFF_L_GET_W'
      Origin = 'SEMIS_REPORT_S.AFF_L_GET_W'
      DisplayFormat = '0.###'
    end
    object taSemisReportAFF_L_RET_Q: TIntegerField
      DisplayLabel = #1040#1092#1092#1080#1085#1072#1078' '#1083#1086#1084#1072' '#1087#1088#1080#1085#1103#1090#1086' '#1082#1086#1083'-'#1074#1086
      FieldName = 'AFF_L_RET_Q'
      Origin = 'SEMIS_REPORT_S.AFF_L_RET_Q'
    end
    object taSemisReportAFF_L_RET_W: TFloatField
      DisplayLabel = #1040#1092#1092#1080#1085#1072#1078' '#1083#1086#1084#1072' '#1087#1088#1080#1085#1103#1090#1086' '#1074#1077#1089
      FieldName = 'AFF_L_RET_W'
      Origin = 'SEMIS_REPORT_S.AFF_L_RET_W'
      DisplayFormat = '0.###'
    end
    object taSemisReportCHARGE_Q: TIntegerField
      DisplayLabel = #1057#1087#1080#1089#1072#1085#1080#1077' '#1082#1086#1083'-'#1074#1086
      FieldName = 'CHARGE_Q'
      Origin = 'SEMIS_REPORT_S.CHARGE_Q'
    end
    object taSemisReportCHARGE_W: TFloatField
      DisplayLabel = #1057#1087#1080#1089#1072#1085#1080#1077' '#1074#1077#1089
      FieldName = 'CHARGE_W'
      Origin = 'SEMIS_REPORT_S.CHARGE_W'
      DisplayFormat = '0.###'
    end
  end
  object dsrSemisReport: TDataSource
    DataSet = taSemisReport
    Left = 262
    Top = 256
  end
  object sqlCreateSemisReport: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure Create_Semis_Report(:BD,:ED,:ACTID);')
    Left = 260
    Top = 304
  end
  object taStoreByItems2: TpFIBDataSet
    SelectSQL.Strings = (
      'select p.UID, p.ART, p.ART2, p.ART2ID, p.ARTID, p.SZ,'
      '       p.SZID, p.W, p.UNIT, p.SEMIS, p.STATE,'
      '       p.INS, p.STATE_NAME, p.CURPRICE, p.CURPRICE2, '
      '       p.LASTDATE, p.UNITID, p.SELFCOMPID, '
      '       c.NAME SELFCOMPNAME, p.BUH_10, m.Name Material'
      'from TMP_PROD_STORE p, D_Comp c, art2 a2, d_semis s, d_mat m'
      'where p.ART starting with :ART_ and'
      '      p.STATE between :ST1 and :ST2 and'
      '      coalesce(p.USERID,0)=0 and'
      '      p.SELFCOMPID=c.COMPID and'
      '      a2.art2id = p.art2id and'
      '      s.semisid = a2.semis and'
      '      s.mat between :MAT1 and :MAT2 and'
      '      m.id = s.mat'
      '')
    CacheModelOptions.BufferChunks = 1000
    AfterOpen = taStoreByItems2AfterOpen
    AfterScroll = taStoreByItems2AfterOpen
    BeforeOpen = taStoreByItems2BeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Description = #1057#1082#1083#1072#1076' '#1087#1086' '#1080#1079#1076#1077#1083#1100#1085#1086
    Left = 404
    Top = 204
    object taStoreByItems2UID: TIntegerField
      DisplayLabel = #1059#1085'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
      Origin = 'TMP_PROD_STORE.UID'
      Required = True
    end
    object taStoreByItems2ART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'TMP_PROD_STORE.ART'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taStoreByItems2ART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'TMP_PROD_STORE.ART2'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taStoreByItems2SZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'TMP_PROD_STORE.SZ'
      FixedChar = True
      Size = 5
      EmptyStrToNull = True
    end
    object taStoreByItems2W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'TMP_PROD_STORE.W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taStoreByItems2UNIT: TFIBStringField
      DisplayLabel = #1045#1048
      FieldName = 'UNIT'
      Origin = 'TMP_PROD_STORE.UNIT'
      FixedChar = True
      Size = 5
      EmptyStrToNull = True
    end
    object taStoreByItems2SEMIS: TFIBStringField
      DisplayLabel = #1058#1080#1087
      FieldName = 'SEMIS'
      Origin = 'TMP_PROD_STORE.SEMIS'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taStoreByItems2STATE: TSmallintField
      DisplayLabel = #1050#1086#1076
      FieldName = 'STATE'
      Origin = 'TMP_PROD_STORE.STATE'
    end
    object taStoreByItems2INS: TFIBStringField
      DisplayLabel = #1054#1060
      FieldName = 'INS'
      Origin = 'TMP_PROD_STORE.INS'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taStoreByItems2STATE_NAME: TFIBStringField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATE_NAME'
      Origin = 'TMP_PROD_STORE.STATE_NAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taStoreByItems2CURPRICE: TFloatField
      DisplayLabel = #1059#1095'. '#1094#1077#1085#1072
      FieldName = 'CURPRICE'
      Origin = 'TMP_PROD_STORE.CURPRICE'
      currency = True
    end
    object taStoreByItems2LASTDATE: TDateTimeField
      DisplayLabel = #1055#1086#1089#1083'. '#1076#1072#1090#1072
      FieldName = 'LASTDATE'
      Origin = 'TMP_PROD_STORE.LASTDATE'
    end
    object taStoreByItems2ART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'TMP_PROD_STORE.ART2ID'
    end
    object taStoreByItems2ARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'TMP_PROD_STORE.ARTID'
    end
    object taStoreByItems2SZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'TMP_PROD_STORE.SZID'
    end
    object taStoreByItems2CURPRICE2: TFIBFloatField
      DisplayLabel = #1054#1090#1087'. '#1094#1077#1085#1072
      FieldName = 'CURPRICE2'
      currency = True
    end
    object taStoreByItems2UNITID: TFIBIntegerField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'.'
      FieldName = 'UNITID'
    end
    object taStoreByItems2SELFCOMPID: TFIBIntegerField
      FieldName = 'SELFCOMPID'
    end
    object taStoreByItems2SELFCOMPNAME: TFIBStringField
      FieldName = 'SELFCOMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taStoreByItems2BUH_10: TFIBSmallIntField
      DisplayLabel = #1057#1095#1077#1090' '#1073#1091#1093'.'
      FieldName = 'BUH_10'
      OnGetText = taStoreByItems2BUH_10GetText
    end
    object taStoreByItems2Material: TFIBStringField
      FieldName = 'Material'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrStoreByItems2: TDataSource
    DataSet = taStoreByItems2
    Left = 404
    Top = 252
  end
  object sqlCreateProdStore: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure CREATE_PROD_STORE(:BDATE,:EDATE,:USERID)')
    Description = #1055#1077#1088#1077#1089#1095#1077#1090' '#1089#1082#1083#1072#1076#1072
    Left = 624
    Top = 512
  end
  object sqlUpdateStoresHeader: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure UPDATE_STORES_HEADER(:SUPNAME, :ADATE); ')
    Left = 48
    Top = 488
  end
  object sqlUpdateOutStores: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      
        'execute procedure UPDATE_OUT_STORES(:COMPNAME,:ART,:SZ,:STOREQ,:' +
        'STOREW)')
    Left = 48
    Top = 536
  end
  object taOutStore: TpFIBDataSet
    SelectSQL.Strings = (
      'select ART, ARTID, COMPID, STOREW, STOREQ, SZ, SZNAME '
      'from TMP_OUTSIDE_STORES'
      'where  ARTID=:ARTID and COMPID =:COMPID'
      'order by SZNAME')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taOutStoreBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 896
    Top = 8
    object taOutStoreART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'TMP_OUTSIDE_STORES.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taOutStoreARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'TMP_OUTSIDE_STORES.ARTID'
    end
    object taOutStoreCOMPID: TIntegerField
      FieldName = 'COMPID'
      Origin = 'TMP_OUTSIDE_STORES.COMPID'
    end
    object taOutStoreSTOREW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'STOREW'
      Origin = 'TMP_OUTSIDE_STORES.STOREW'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taOutStoreSTOREQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'STOREQ'
      Origin = 'TMP_OUTSIDE_STORES.STOREQ'
    end
    object taOutStoreSZ: TIntegerField
      FieldName = 'SZ'
      Origin = 'TMP_OUTSIDE_STORES.SZ'
    end
    object taOutStoreSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      Origin = 'TMP_OUTSIDE_STORES.SZNAME'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsrOutStore: TDataSource
    DataSet = taOutStore
    Left = 896
    Top = 56
  end
  object taFindArts: TpFIBDataSet
    SelectSQL.Strings = (
      'select ARTID, ART2ID  from FIND_ARTS(:ART,:ART2);')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 632
    Top = 272
    object taFindArtsARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'FIND_ARTS.ARTID'
    end
    object taFindArtsART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'FIND_ARTS.ART2ID'
    end
  end
  object taARTPrice: TpFIBDataSet
    UpdateSQL.Strings = (
      'update D_ART '
      'set K=:K '
      'where D_ARTID=:OLD_ARTID')
    RefreshSQL.Strings = (
      'select  ART, ARTID, K1, K2, K3, K , PICT, U'
      'from '
      'VIEW_DICT_PRICE_R(:OLD_ARTID)')
    SelectSQL.Strings = (
      'select  ART, ARTID, K1, K2, K3, K, ART_PRICE, PICT, U'
      'from VIEW_DICT_PRICE'
      'order by ART')
    CacheModelOptions.BufferChunks = 1000
    AfterEdit = CommitRetaining
    AfterPost = taARTPriceAfterPost
    AfterScroll = taARTPriceAfterScroll
    BeforeScroll = taARTPriceBeforeScroll
    Transaction = dm.tr
    Database = dm.db
    OnFilterRecord = taARTPriceFilterRecord
    Left = 344
    Top = 312
    object taARTPriceART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'VIEW_DICT_PRICE.ART'
      EmptyStrToNull = True
    end
    object taARTPriceARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'VIEW_DICT_PRICE.ARTID'
    end
    object taARTPriceK1: TFloatField
      FieldName = 'K1'
      Origin = 'VIEW_DICT_PRICE.K1'
    end
    object taARTPriceK2: TFloatField
      FieldName = 'K2'
      Origin = 'VIEW_DICT_PRICE.K2'
    end
    object taARTPriceK3: TFloatField
      FieldName = 'K3'
      Origin = 'VIEW_DICT_PRICE.K3'
    end
    object taARTPriceK: TFloatField
      FieldName = 'K'
      Origin = 'VIEW_DICT_PRICE.K'
    end
    object taARTPriceART_PRICE: TFloatField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083#1100#1085#1072#1103' '#1094#1077#1085#1072
      FieldName = 'ART_PRICE'
      Origin = 'VIEW_DICT_PRICE.ART_PRICE'
      currency = True
    end
    object taARTPricePICT: TBlobField
      FieldName = 'PICT'
      Origin = 'VIEW_DICT_PRICE.PICT'
      Size = 8
    end
    object taARTPriceU: TSmallintField
      FieldName = 'U'
      Origin = 'VIEW_DICT_PRICE.U'
    end
  end
  object dsrArtPrice: TDataSource
    DataSet = taARTPrice
    Left = 344
    Top = 360
  end
  object taARTPriceDet: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE PRICE'
      'SET  PRICE=:PRICE,'
      '     Up=:UP       '
      'where PRICEID=:OLD_PRICEID')
    DeleteSQL.Strings = (
      'delete from Price'
      'where art2id=:OLD_ART2ID')
    RefreshSQL.Strings = (
      
        'select P.PRICEID, P.PRICE, P.TPRICE, P.DEPID, A2.ART2, A2.ART2ID' +
        ', P.CALC_PRICE, P.UP'
      'from PRICE P, ART2  A2'
      'where P.ART2ID=A2.ART2ID and'
      '          P.PRICEID=:OLD_PRICEID '
      '')
    SelectSQL.Strings = (
      
        'select PRICEID, PRICE, TPRICE, DEPID, ART2, ART2ID, CALC_PRICE, ' +
        'UP'
      'from View_Price_Det_s(:ARTid)')
    CacheModelOptions.BufferChunks = 1000
    AfterEdit = CommitRetaining
    AfterPost = CommitRetaining
    AfterScroll = taARTPriceDetAfterScroll
    BeforeDelete = taARTPriceDetBeforeDelete
    BeforeOpen = taARTPriceDetBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 416
    Top = 312
    object taARTPriceDetPRICEID: TIntegerField
      FieldName = 'PRICEID'
      Origin = 'PRICE.PRICEID'
      Required = True
    end
    object taARTPriceDetPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'PRICE'
      Origin = 'PRICE.PRICE'
      Required = True
      currency = True
    end
    object taARTPriceDetTPRICE: TFloatField
      DisplayLabel = #1055#1088#1077#1076'. '#1094#1077#1085#1072
      FieldName = 'TPRICE'
      Origin = 'PRICE.TPRICE'
      Required = True
      currency = True
    end
    object taARTPriceDetART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taARTPriceDetDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'PRICE.DEPID'
    end
    object taARTPriceDetART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'ART2.ART2ID'
      Required = True
    end
    object taARTPriceDetCALC_PRICE: TFloatField
      DisplayLabel = #1056#1072#1089#1095'. '#1094#1077#1085#1072
      FieldName = 'CALC_PRICE'
      Origin = 'PRICE.CALC_PRICE'
      currency = True
    end
    object taARTPriceDetUP: TSmallintField
      FieldName = 'UP'
      Origin = 'VIEW_PRICE_DET_S.UP'
    end
  end
  object dsrArtPriceDet: TDataSource
    DataSet = taARTPriceDet
    Left = 416
    Top = 360
  end
  object sqlReturnPrice: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure RETURN_PRICE(:ART2ID,:ALL_)')
    Left = 344
    Top = 416
  end
  object sqlCalcPrice: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'Execute procedure Calc_UPPrice(:ART2ID,:ALL_)')
    Left = 416
    Top = 416
  end
  object sqlDEalPrice: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure DEAL_PRICE(:ART2ID,:ALL_)')
    Left = 480
    Top = 416
  end
  object taConst2: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Const'
      'set VAL=:VAL,'
      '    STR_VAL=:STR_VAL'
      'where'
      'CONST_ID=:OLD_CONST_ID')
    SelectSQL.Strings = (
      'Select '
      'CONST_ID,'
      'NAME,'
      'VAL'
      'From CONST')
    CacheModelOptions.BufferChunks = 1000
    AfterEdit = CommitRetaining
    Transaction = dm.tr
    Database = dm.db
    Left = 840
    Top = 8
    object IBStringField1: TFIBStringField
      DisplayLabel = #1050#1086#1085#1089#1090#1072#1085#1090#1072
      FieldName = 'NAME'
      Origin = 'CONST.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object FloatField1: TFloatField
      DisplayLabel = #1047#1085#1072#1095#1077#1085#1080#1077
      FieldName = 'VAL'
      Origin = 'CONST.VAL'
    end
  end
  object dsrConst2: TDataSource
    DataSet = taConst2
    Left = 840
    Top = 56
  end
  object sqlCreateActPrice: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure CREATE_ACT_PRICE(:NEW_DATE,:DEPID,:USERID);')
    Left = 344
    Top = 464
  end
  object taArtIns: TpFIBDataSet
    SelectSQL.Strings = (
      'select I.QUANTITY, I.WEIGHT, I.ID, I.ART2ID,'
      '       DS.SEMIS, DS.PRICE, DC.NAME, DS.SEMISID, DS.MAT'
      'from INS I, D_SEMIS DS, D_COMP DC'
      'where i.ART2ID=:ART2ID and '
      '      DS.SEMISID=I.INSID and  '
      '      I.COMPID=DC.COMPID    ')
    CacheModelOptions.BufferChunks = 1000
    AfterEdit = taArtInsAfterEdit
    BeforeOpen = taArtInsBeforeOpen
    OnCalcFields = taArtInsCalcFields
    Transaction = dm.tr
    Database = dm.db
    Left = 480
    Top = 312
    object taArtInsID: TIntegerField
      FieldName = 'ID'
      Origin = 'INS.ID'
      Required = True
    end
    object taArtInsQUANTITY: TSmallintField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'QUANTITY'
      Origin = 'INS.QUANTITY'
    end
    object taArtInsWEIGHT: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'WEIGHT'
      Origin = 'INS.WEIGHT'
    end
    object taArtInsART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'INS.ART2ID'
      Required = True
    end
    object taArtInsSEMIS: TFIBStringField
      DisplayLabel = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080
      FieldName = 'SEMIS'
      Origin = 'D_SEMIS.SEMIS'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taArtInsPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'PRICE'
      Origin = 'D_SEMIS.PRICE'
      Required = True
      currency = True
    end
    object taArtInsSUM: TFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldKind = fkCalculated
      FieldName = 'SUM'
      currency = True
      Calculated = True
    end
    object taArtInsNAME: TFIBStringField
      DisplayLabel = #1042#1083#1072#1076#1077#1083#1077#1094
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taArtInsSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Size = 10
      EmptyStrToNull = True
    end
    object taArtInsMAT: TFIBStringField
      FieldName = 'MAT'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsrArtIns: TDataSource
    DataSet = taArtIns
    Left = 480
    Top = 360
  end
  object sqlDeleteArt2: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'delete from Art2'
      'where art2id=:art2id')
    Left = 264
    Top = 424
  end
  object sqlAddLostStones: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure ADD_LOST_STONES(:INVID,:ART2ID,:INSID,:Q,:W);')
    Left = 900
    Top = 232
  end
  object taLostStones: TpFIBDataSet
    UpdateSQL.Strings = (
      'update LOST_STONES'
      'set QUANTITY=:QUANTITY,'
      'WEIGTH=:WEIGTH'
      'where ID=:OLD_Id')
    DeleteSQL.Strings = (
      'delete from LOST_STONES'
      'where ID=:OLd_ID')
    SelectSQL.Strings = (
      'select LS.ID, LS.QUANTITY, LS.WEIGTH, DS.SEMIS'
      'from  LOST_STONES LS, D_SEMIS DS, ACT A'
      'where  LS.ART2ID=:ART2ID and '
      '            LS.ACTID=A.ID  and '
      '            DS.SEMISID=LS.INSID and '
      '            A.INVID=:INVID'
      '      ')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taLostStonesBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 784
    Top = 112
    object taLostStonesSEMIS: TFIBStringField
      DisplayLabel = #1050#1072#1084#1077#1085#1100
      FieldName = 'SEMIS'
      Origin = 'D_SEMIS.SEMIS'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taLostStonesWEIGTH: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'WEIGTH'
      Origin = 'LOST_STONES.WEIGTH'
      DisplayFormat = '0.000'
      EditFormat = '0.000'
    end
    object taLostStonesQUANTITY: TSmallintField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'QUANTITY'
      Origin = 'LOST_STONES.QUANTITY'
    end
    object taLostStonesID: TIntegerField
      FieldName = 'ID'
      Origin = 'LOST_STONES.ID'
      Required = True
    end
  end
  object dsrLostStones: TDataSource
    DataSet = taLostStones
    Left = 784
    Top = 160
  end
  object taArtVedomost: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      'av.ACT_Q, av.ACT_SUM, av.ACT_W,'
      'av.ACTID,av.ART, av.ARTID,'
      'av.BRACK_Q, av.BRACK_SUM, av.BRACK_W,'
      'av.ID, av.IN_Q, av.IN_SUM, av.IN_W,'
      'av.REST1_Q, av.REST1_SUM, av.REST1_W,'
      'av.REST2_Q, av.REST2_SUM, av.REST2_W,'
      'av.RET_Q, av.RET_SUM, av.RET_W,'
      'av.SELL_Q, av.SELL_SUM, av.SELL_W'
      'FROM art_vedomost av'
      'where ACTID=:ACTID')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taArtVedomostBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 864
    Top = 120
    object taArtVedomostACT_Q: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'ACT_Q'
      Origin = 'ART_VEDOMOST.ACT_Q'
    end
    object taArtVedomostACT_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'ACT_SUM'
      Origin = 'ART_VEDOMOST.ACT_SUM'
      currency = True
    end
    object taArtVedomostACT_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'ACT_W'
      Origin = 'ART_VEDOMOST.ACT_W'
      DisplayFormat = '0.00'
    end
    object taArtVedomostACTID: TIntegerField
      FieldName = 'ACTID'
      Origin = 'ART_VEDOMOST.ACTID'
      Required = True
    end
    object taArtVedomostART: TFIBStringField
      FieldName = 'ART'
      Origin = 'ART_VEDOMOST.ART'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object taArtVedomostARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'ART_VEDOMOST.ARTID'
    end
    object taArtVedomostBRACK_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'BRACK_Q'
      Origin = 'ART_VEDOMOST.BRACK_Q'
    end
    object taArtVedomostBRACK_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'BRACK_SUM'
      Origin = 'ART_VEDOMOST.BRACK_SUM'
      currency = True
    end
    object taArtVedomostBRACK_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'BRACK_W'
      Origin = 'ART_VEDOMOST.BRACK_W'
      DisplayFormat = '0.00'
    end
    object taArtVedomostID: TIntegerField
      FieldName = 'ID'
      Origin = 'ART_VEDOMOST.ID'
      Required = True
    end
    object taArtVedomostIN_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'IN_Q'
      Origin = 'ART_VEDOMOST.IN_Q'
    end
    object taArtVedomostIN_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'IN_SUM'
      Origin = 'ART_VEDOMOST.IN_SUM'
      currency = True
    end
    object taArtVedomostIN_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'IN_W'
      Origin = 'ART_VEDOMOST.IN_W'
      DisplayFormat = '0.00'
    end
    object taArtVedomostREST1_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'REST1_Q'
      Origin = 'ART_VEDOMOST.REST1_Q'
    end
    object taArtVedomostREST1_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'REST1_SUM'
      Origin = 'ART_VEDOMOST.REST1_SUM'
      currency = True
    end
    object taArtVedomostREST1_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'REST1_W'
      Origin = 'ART_VEDOMOST.REST1_W'
      DisplayFormat = '0.00'
    end
    object taArtVedomostREST2_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'REST2_Q'
      Origin = 'ART_VEDOMOST.REST2_Q'
    end
    object taArtVedomostREST2_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'REST2_SUM'
      Origin = 'ART_VEDOMOST.REST2_SUM'
      currency = True
    end
    object taArtVedomostREST2_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'REST2_W'
      Origin = 'ART_VEDOMOST.REST2_W'
      DisplayFormat = '0.00'
    end
    object taArtVedomostRET_Q: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'RET_Q'
      Origin = 'ART_VEDOMOST.RET_Q'
    end
    object taArtVedomostRET_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'RET_SUM'
      Origin = 'ART_VEDOMOST.RET_SUM'
      currency = True
    end
    object taArtVedomostRET_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'RET_W'
      Origin = 'ART_VEDOMOST.RET_W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taArtVedomostSELL_Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'SELL_Q'
      Origin = 'ART_VEDOMOST.SELL_Q'
    end
    object taArtVedomostSELL_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'SELL_SUM'
      Origin = 'ART_VEDOMOST.SELL_SUM'
      currency = True
    end
    object taArtVedomostSELL_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'SELL_W'
      Origin = 'ART_VEDOMOST.SELL_W'
      DisplayFormat = '0.00'
    end
  end
  object dsrArtVedomost: TDataSource
    DataSet = taArtVedomost
    Left = 864
    Top = 168
  end
  object sqlUpdateInvPrice: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure Update_InvPrice(:Invid);')
    Left = 892
    Top = 332
  end
  object sqlClearSelectUp: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'update Price'
      'set UP=0')
    Left = 624
    Top = 456
  end
  object sqlCheckArt: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure Check_all_art2(:Artid,:check,:All)')
    Left = 480
    Top = 464
  end
  object taAVList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Act '
      'set IsClose=:IsCLOSE'
      'where ID=:NEW_ID')
    DeleteSQL.Strings = (
      'delete from ACT where ID=:ID')
    SelectSQL.Strings = (
      'select  ID, DOCNO, DOCDATE, USERID, BD, ISCLOSE, T, UPD'
      'from Act '
      'where BD between :BD and :ED and'
      '           T=4 '
      'order by DOCNO')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taAVListBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 532
    Top = 312
    object taAVListID: TIntegerField
      FieldName = 'ID'
      Origin = 'ACT.ID'
      Required = True
    end
    object taAVListDOCNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1074#1077#1076#1086#1084#1086#1089#1090#1080
      FieldName = 'DOCNO'
      Origin = 'ACT.DOCNO'
      Required = True
    end
    object taAVListDOCDATE: TDateTimeField
      DisplayLabel = #1050#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072
      FieldName = 'DOCDATE'
      Origin = 'ACT.DOCDATE'
      Required = True
    end
    object taAVListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'ACT.USERID'
      Required = True
    end
    object taAVListBD: TDateTimeField
      DisplayLabel = #1053#1072#1095#1072#1083#1086' '#1087#1077#1088#1086#1076#1072
      FieldName = 'BD'
      Origin = 'ACT.BD'
    end
    object taAVListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'ACT.ISCLOSE'
      Required = True
    end
    object taAVListT: TSmallintField
      FieldName = 'T'
      Origin = 'ACT.T'
      Required = True
    end
    object taAVListUPD: TSmallintField
      FieldName = 'UPD'
      Origin = 'ACT.UPD'
    end
  end
  object dsrAVList: TDataSource
    DataSet = taAVList
    Left = 532
    Top = 360
  end
  object sqlPublic: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    Left = 548
    Top = 456
  end
  object taLostActs: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select  A.ID, A.DOCNO, A.DOCDATE, A.USERID,  A.ISCLOSE, A.T, A.I' +
        'NVID, A.UPD, I.DOCNO'
      'from Act A, INV I  '
      'where A.DOCDATE between :BD and :ED and'
      '          A.InvID=I.INVID and'
      '          A.T=3 '
      'order by A.DOCNO')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taLostActsBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 652
    Top = 312
    object taLostActsDOCNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088
      FieldName = 'DOCNO'
      Origin = 'ACT.DOCNO'
      Required = True
    end
    object taLostActsDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'DOCDATE'
      Origin = 'ACT.DOCDATE'
      Required = True
    end
    object taLostActsUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'ACT.USERID'
      Required = True
    end
    object taLostActsUSERNAME: TStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldKind = fkLookup
      FieldName = 'USERNAME'
      LookupDataSet = dm.taMOL
      LookupKeyFields = 'MOLID'
      LookupResultField = 'FNAME'
      KeyFields = 'USERID'
      Size = 40
      Lookup = True
    end
    object taLostActsDOCNO1: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1089#1086#1086#1090#1074'. '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      FieldName = 'DOCNO1'
      Origin = 'INV.DOCNO'
    end
    object taLostActsINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'ACT.INVID'
    end
  end
  object dsrLostActs: TDataSource
    DataSet = taLostActs
    Left = 652
    Top = 360
  end
  object taAllLostStones: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '    INSID ,'
      '    QUANTITY ,'
      '    WEIGTH ,'
      '    EDGESHAPEID ,'
      '    EDGTYPEID ,'
      '    CHROMATICITY ,'
      '    CLEANNES ,'
      '    IGR ,'
      '    COLOR ,'
      '    ART,'
      '    ART2,'
      '    Semis '
      'from  LOST_STONES_S(:INVID)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taAllLostStonesBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 800
    Top = 312
    object taAllLostStonesART: TFIBStringField
      FieldName = 'ART'
      Origin = 'LOST_STONES_S.ART'
      Size = 30
      EmptyStrToNull = True
    end
    object taAllLostStonesART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'LOST_STONES_S.ART2'
      Size = 10
      EmptyStrToNull = True
    end
    object taAllLostStonesINSID: TFIBStringField
      DisplayLabel = #1042#1089#1090#1072#1074#1082#1072
      FieldName = 'INSID'
      Origin = 'LOST_STONES_S.INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAllLostStonesQUANTITY: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'QUANTITY'
      Origin = 'LOST_STONES_S.QUANTITY'
    end
    object taAllLostStonesWEIGTH: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'WEIGTH'
      Origin = 'LOST_STONES_S.WEIGTH'
      DisplayFormat = '0.000'
    end
    object taAllLostStonesEDGESHAPEID: TFIBStringField
      FieldName = 'EDGESHAPEID'
      Origin = 'LOST_STONES_S.EDGESHAPEID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAllLostStonesEDGTYPEID: TFIBStringField
      FieldName = 'EDGTYPEID'
      Origin = 'LOST_STONES_S.EDGTYPEID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAllLostStonesCHROMATICITY: TFIBStringField
      FieldName = 'CHROMATICITY'
      Origin = 'LOST_STONES_S.CHROMATICITY'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAllLostStonesCLEANNES: TFIBStringField
      DisplayLabel = #1063#1080#1089#1090#1086#1090#1072
      FieldName = 'CLEANNES'
      Origin = 'LOST_STONES_S.CLEANNES'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAllLostStonesCOLOR: TFIBStringField
      DisplayLabel = #1062#1074#1077#1090
      FieldName = 'COLOR'
      Origin = 'LOST_STONES_S.COLOR'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAllLostStonesSEMIS: TFIBStringField
      DisplayLabel = #1061#1072#1088'-'#1082#1080
      FieldName = 'SEMIS'
      Origin = 'LOST_STONES_S.SEMIS'
      Size = 40
      EmptyStrToNull = True
    end
    object taAllLostStonesIGR: TFIBStringField
      FieldName = 'IGR'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsrAllLostStones: TDataSource
    DataSet = taAllLostStones
    Left = 800
    Top = 360
  end
  object frLostStones: TfrDBDataSet
    DataSet = taAllLostStones
    Left = 800
    Top = 408
  end
  object taPactsPrint: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      ' PI.ACT_ITEMSID,'
      ' PI.UID,'
      ' PI.W, '
      ' PI.OLD_PRICE,'
      ' PI.NEW_PRICE,'
      ' PI.OLD_PRICE*PI.Q0,'
      ' PI.NEW_PRICE*PI.Q0,'
      ' A2. ART,'
      ' A2.ART2'
      ' FROM PACT_ITEMS PI,  ACT_ITEM AC, ART2  A2'
      ''
      'WHERE PI.ACT_ITEMID=AC.ACT_ITEMID and '
      '               ac.INVID=:INVID and '
      '               A2.Art2id=ac.art2id  ')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taPactsPrintBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 76
    Top = 368
  end
  object frPactsPrint: TfrDBDataSet
    DataSet = taPactsPrint
    Left = 76
    Top = 416
  end
  object taProdREport: TpFIBDataSet
    SelectSQL.Strings = (
      'select ART, SZ,'
      ' SELL_Q, SELL_W, SELL_SUMM,'
      
        ' RET_Q, RET_W, RET_SUMM, (Sell_Q-RET_Q), (Sell_w-RET_W), (SELL_S' +
        'UMM-RET_SUMM) '
      'from TMP_PROD_REPORT'
      'order by ART')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 716
    Top = 312
    object taProdREportART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'TMP_PROD_REPORT.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taProdREportSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'TMP_PROD_REPORT.SZ'
      FixedChar = True
      Size = 5
      EmptyStrToNull = True
    end
    object taProdREportSELL_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'SELL_W'
      Origin = 'TMP_PROD_REPORT.SELL_W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taProdREportSELL_SUMM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'SELL_SUMM'
      Origin = 'TMP_PROD_REPORT.SELL_SUMM'
      currency = True
    end
    object taProdREportRET_W: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'RET_W'
      Origin = 'TMP_PROD_REPORT.RET_W'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taProdREportRET_SUMM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'RET_SUMM'
      Origin = 'TMP_PROD_REPORT.RET_SUMM'
      currency = True
    end
    object taProdREportF_2: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'F_2'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taProdREportF_3: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'F_3'
      currency = True
    end
    object taProdREportSELL_Q: TFIBIntegerField
      FieldName = 'SELL_Q'
    end
    object taProdREportRET_Q: TFIBIntegerField
      FieldName = 'RET_Q'
    end
    object taProdREportF_1: TFIBBCDField
      FieldName = 'F_1'
      Size = 0
      RoundByScale = True
    end
  end
  object dsrProdReport: TDataSource
    DataSet = taProdREport
    Left = 716
    Top = 360
  end
  object frProdReport: TfrDBDataSet
    DataSet = taProdREport
    Left = 716
    Top = 412
  end
  object frStoreByItem: TfrDBDataSet
    DataSet = taStoreByItems2
    Left = 448
    Top = 252
  end
  object dsrCash: TDataSource
    DataSet = taCash
    Left = 152
    Top = 528
  end
  object taCash: TpFIBDataSet
    SelectSQL.Strings = (
      'select c.ID, c.COMPID, c.COST, c.NDS, c.OPERDATE,'
      '       s.NAME COMPNAME, c.NDOC, c.T'
      'from Cash c, D_Comp s'
      'where c.OPERDATE between :BD and :ED and'
      '      c.COMPID=s.COMPID and'
      '      c.SELFCOMPID=:SELFCOMPID'
      'order by c.OPERDATE')
    AfterPost = CommitRetaining
    BeforeOpen = taCashBeforeOpen
    OnCalcFields = taCashCalcFields
    Transaction = dm.tr
    Database = dm.db
    SQLScreenCursor = crSQLWait
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = 'B: '#1050#1072#1089#1089#1072' ('#1086#1087#1083#1072#1090#1072' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103#1084#1080')'
    Left = 152
    Top = 476
    poSQLINT64ToBCD = True
    object taCashID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taCashCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object taCashCOST: TFIBFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      currency = True
    end
    object taCashNDS: TFIBFloatField
      DisplayLabel = #1053#1044#1057
      FieldName = 'NDS'
      currency = True
    end
    object taCashOPERDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1086#1087#1083#1072#1090#1099
      FieldName = 'OPERDATE'
    end
    object taCashCOMPNAME: TFIBStringField
      DisplayLabel = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      FieldName = 'COMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taCashNDOC: TFIBStringField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1082#1072#1089#1089#1086#1074#1086#1075#1086' '#1095#1077#1082#1072
      FieldName = 'NDOC'
      Size = 10
      EmptyStrToNull = True
    end
    object taCashT: TFIBSmallIntField
      DisplayLabel = #1042#1080#1076
      FieldName = 'T'
      OnGetText = taCashTGetText
    end
    object taCashCOSTSELL: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'COSTSELL'
      currency = True
      Calculated = True
    end
    object taCashCOSTRET: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'COSTRET'
      currency = True
      Calculated = True
    end
  end
  object sqlAdd_All_in_INV: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'execute procedure Add_ALL_IN_INV(:INVID,:BRILL)')
    Left = 904
    Top = 388
  end
  object taInv_Store4ArtId: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure F_DELETE(0)')
    InsertSQL.Strings = (
      'execute procedure F_INSERT(0)')
    RefreshSQL.Strings = (
      'select '
      '  ARTID, ART2ID, UID, ART, ART2, FULLART, SZ,'
      '  W, SZID, DOCDATE, SPRICE, DEPID, Q0, SCOST, STATE_'
      'from SELECT_ITEMS_BY_ARTID_R(:UID, :STATE_)')
    SelectSQL.Strings = (
      'select '
      '  ARTID, ART2ID, UID, ART, ART2, FULLART, SZ,'
      '  W, SZID, DOCDATE, SPRICE, DEPID, Q0, SCOST, STATE_'
      
        'from SELECT_ITEMS_BY_ARTID(:DEPID1, :DEPID2, :ARTID1, :ARTID2, :' +
        'STATE, :SUPID)'
      'order by ART2, SZ')
    BeforeOpen = taInv_Store4ArtIdBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1048#1079#1076#1077#1083#1080#1103' '#1076#1083#1103' '#1072#1088#1090#1080#1082#1091#1083#1072
    Left = 228
    Top = 476
    poSQLINT64ToBCD = True
    object taInv_Store4ArtIdARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object taInv_Store4ArtIdART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taInv_Store4ArtIdUID: TFIBIntegerField
      DisplayLabel = #1059#1085'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
    end
    object taInv_Store4ArtIdART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taInv_Store4ArtIdART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taInv_Store4ArtIdFULLART: TFIBStringField
      DisplayLabel = #1055#1086#1083#1085#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
      FieldName = 'FULLART'
      Size = 60
      EmptyStrToNull = True
    end
    object taInv_Store4ArtIdSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      EmptyStrToNull = True
    end
    object taInv_Store4ArtIdW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taInv_Store4ArtIdSZID: TFIBIntegerField
      FieldName = 'SZID'
    end
    object taInv_Store4ArtIdDOCDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1087#1088#1080#1093#1086#1076#1072' '#1085#1072' '#1089#1082#1083#1072#1076
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taInv_Store4ArtIdSPRICE: TFIBFloatField
      DisplayLabel = #1059#1095#1077#1090#1085#1072#1103' '#1094#1077#1085#1072
      FieldName = 'SPRICE'
      currency = True
    end
    object taInv_Store4ArtIdDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taInv_Store4ArtIdQ0: TFIBFloatField
      FieldName = 'Q0'
    end
    object taInv_Store4ArtIdSCOST: TFIBFloatField
      DisplayLabel = #1059#1095#1077#1090#1085#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SCOST'
      currency = True
    end
    object taInv_Store4ArtIdSTATE_: TFIBSmallIntField
      FieldName = 'STATE_'
    end
  end
  object dsrInv_Store4ArtId: TDataSource
    DataSet = taInv_Store4ArtId
    Left = 228
    Top = 528
  end
  object taSItem4ArtId: TpFIBDataSet
    DeleteSQL.Strings = (
      'execute procedure F_DELETE(0)')
    InsertSQL.Strings = (
      'execute procedure F_INSERT(0)')
    RefreshSQL.Strings = (
      'SELECT '
      
        '  S.SITEMID, S.SELID, S.UID, S.ARTID, S.ART2ID, S.SZ, S.W, a2.AR' +
        'T2,'
      '  (e.Price2*s.Q0) COST2'
      'FROM SITEM S, ART2 a2, SEL e'
      'WHERE s.SITEMID=:SITEMID and'
      '      s.ART2ID=a2.ART2ID and'
      '      s.SELID=e.SELID')
    SelectSQL.Strings = (
      'SELECT '
      '    S.SITEMID, S.SELID, S.UID, S.ARTID, S.ART2ID, S.SZ, S.W, '
      '    a2.ART2, (e.Price2*s.Q0) COST2'
      'FROM SITEM S, ART2 a2, SEl e'
      'WHERE e.INVID=:INVID and'
      '      e.SELID=s.SELID and'
      '      s.ARTID=:ARTID and'
      '      s.ART2ID=a2.ART2ID       '
      'order by a2.ART2, S.SZ'
      '')
    BeforeOpen = taSItem4ArtIdBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = 
      #1048#1079#1076#1077#1083#1080#1103' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1076#1083#1103' '#1087#1077#1088#1074#1086#1075#1086' '#1072#1088#1090#1080#1082#1091#1083#1072', '#1074#1089#1103' '#1088#1072#1073#1086#1090#1072' '#1089' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1087 +
      #1088#1086#1080#1089#1093#1086#1076#1080#1090' '#1095#1077#1088#1077#1079' taSItem'
    Left = 252
    Top = 4
    poSQLINT64ToBCD = True
    object taSItem4ArtIdSITEMID: TFIBIntegerField
      FieldName = 'SITEMID'
    end
    object taSItem4ArtIdSELID: TFIBIntegerField
      FieldName = 'SELID'
    end
    object taSItem4ArtIdW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '0.00'
    end
    object taSItem4ArtIdSZ: TFIBIntegerField
      FieldName = 'SZ'
    end
    object taSItem4ArtIdUID: TFIBIntegerField
      DisplayLabel = #1059#1085'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
    end
    object taSItem4ArtIdARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object taSItem4ArtIdART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taSItem4ArtIdART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taSItem4ArtIdSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldKind = fkLookup
      FieldName = 'SZNAME'
      LookupDataSet = dm.taSz
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'SZ'
      EmptyStrToNull = True
      Lookup = True
    end
    object taSItem4ArtIdCOST2: TFIBFloatField
      DisplayLabel = #1054#1090#1087#1091#1089#1082#1085#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'COST2'
      currency = True
    end
  end
  object dsrSItem4ArtId: TDataSource
    DataSet = taSItem4ArtId
    Left = 252
    Top = 48
  end
  object taPriceList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update PRICE_LIST '
      'set PRICE=:PRICE'
      'where ID=:OLD_ID')
    RefreshSQL.Strings = (
      'select PL.ID,'
      '       PL.ARTID,'
      '       PL.PRICE,'
      '       DA.ART'
      'from PRICE_LIST PL, D_ART DA'
      'where  PL.artid=DA.d_artid and '
      '       PL.ID=:OLD_ID'
      ''
      '    ')
    SelectSQL.Strings = (
      'select PL.ID,'
      '       PL.ARTID,'
      '       PL.PRICE,'
      '       DA.ART'
      'from PRICE_LIST PL, D_ART DA'
      'where  PL.artid=DA.d_artid and '
      '       DA.Art like :AART '
      'order by DA.ART'
      '    ')
    AfterPost = CommitRetaining
    BeforeOpen = taPriceListBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 872
    Top = 448
    poSQLINT64ToBCD = True
    object taPriceListID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taPriceListARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object taPriceListPRICE: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'PRICE'
      currency = True
    end
    object taPriceListART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      EmptyStrToNull = True
    end
  end
  object dsrPriceList: TDataSource
    DataSet = taPriceList
    Left = 872
    Top = 496
  end
  object taRev_det: TpFIBDataSet
    RefreshSQL.Strings = (
      'select R.ID,'
      '       R.UID,'
      '       R.STATE,'
      '       SN.W,'
      '       SN.Art2id,'
      '       a2.art2,'
      '       a2.art,'
      '       P.Price,'
      '       sz.NAME SZNAME'
      'from '
      '    REVISION R '
      '  left outer join SINFO SN on (R.uid = sn.uid)'
      '  left outer join ART2 a2 on (a2.art2id = sn.art2id)'
      '  left outer join Price P on (a2.art2id = p.art2id) '
      '  left outer join D_Sz sz on ( sz.ID=sn.SZ )'
      '/* '#1076#1083#1103' '#1092#1080#1083#1100#1090#1088#1072' '#1087#1086' '#1084#1072#1090#1077#1088#1080#1072#1083#1091
      '  left outer join D_Art da on (a2.d_artid = da.d_artid)'
      '  left outer join d_semis ds on (da.semis = ds.semisid) '
      '                                                       */'
      'where'
      '  (R.INVID=:invid and p.depid=sn.depid) and '
      '  R.ID = :OLD_ID --and'
      '  --ds.MAT = 1'
      '    ')
    SelectSQL.Strings = (
      'select R.ID,'
      '       R.UID,'
      '       R.STATE,'
      '       SN.W,'
      '       SN.Art2id,'
      '       a2.art2,'
      '       a2.art,'
      '       P.Price,'
      '       sz.NAME SZNAME   '
      'from '
      '     REVISION R '
      '  left outer join SINFO SN on (R.uid = sn.uid)'
      '  left outer join ART2 a2 on (a2.art2id = sn.art2id)'
      '  left outer join Price P on (a2.art2id = p.art2id) '
      '  left outer join D_Sz sz on ( sz.ID=sn.SZ )'
      '  /* '#1076#1083#1103' '#1092#1080#1083#1100#1090#1088#1072' '#1087#1086' '#1084#1072#1090#1077#1088#1080#1072#1083#1091' '
      '    left outer join D_Art da on (a2.d_artid = da.d_artid)'
      '    left outer join d_semis ds on (da.semis = ds.semisid) '
      '                                                          */'
      'where '
      '   R.INVID=:invid and '
      '   p.depid=sn.depid -- and'
      '   -- ds.mat = 1'
      'order by '
      '   R.state, '
      '   R.uid, '
      '   a2.art, '
      '   a2.art2'
      '')
    BeforeOpen = taRev_detBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 928
    Top = 448
    poSQLINT64ToBCD = True
    object taRev_detID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taRev_detUID: TFIBIntegerField
      DisplayLabel = #1059#1085#1080#1082#1072#1083#1100#1085#1099#1081' '#1085#1086#1084#1077#1088
      FieldName = 'UID'
    end
    object taRev_detSTATE: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATE'
    end
    object taRev_detW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.00'
    end
    object taRev_detART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taRev_detART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taRev_detART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taRev_detPRICE: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'PRICE'
      currency = True
    end
    object taRev_detSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      EmptyStrToNull = True
    end
  end
  object dsrRev_Det: TDataSource
    DataSet = taRev_det
    Left = 936
    Top = 496
  end
  object frRev_det: TfrDBDataSet
    DataSet = taRev_det
    Left = 944
    Top = 552
  end
  object taNotInInv: TpFIBDataSet
    DeleteSQL.Strings = (
      'update SITEM'
      'set CHECK_=1 '
      'where SITEMID=:OLD_SITEMID')
    SelectSQL.Strings = (
      'SELECT'
      '    s.SITEMID,'
      '    s.ARTID,'
      '    s.ART2ID,'
      '    s.W,'
      '    s.SZ,'
      '    s.UID,'
      '    s.CHECK_,'
      '    A2.ARt,'
      '    A2.ARt2'
      'FROM'
      '    SITEM S, '
      '    sel SE,'
      '    ART2 A2'
      'where  '
      '  S.Check_=0   and '
      '  s.art2id=a2.art2id  and'
      '  S.SELID=SE.SELID and'
      '  SE.INVID=:INVID')
    BeforeOpen = taNotInInvBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 736
    Top = 512
    poSQLINT64ToBCD = True
    oProtectedEdit = True
    object taNotInInvARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object taNotInInvART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taNotInInvW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
    end
    object taNotInInvSZ: TFIBIntegerField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
    end
    object taNotInInvUID: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088
      FieldName = 'UID'
    end
    object taNotInInvCHECK_: TFIBSmallIntField
      FieldName = 'CHECK_'
    end
    object taNotInInvART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taNotInInvART2: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083'2'
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taNotInInvSITEMID: TFIBIntegerField
      FieldName = 'SITEMID'
    end
  end
  object dsrNotInInv: TDataSource
    DataSet = taNotInInv
    Left = 736
    Top = 560
  end
  object taEServParams: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ESERV_PARAMS'
      'SET     '
      '    MATID = ?MATID,'
      '    N = ?N,'
      '    SERVCOST = ?SERVCOST,'
      '    EXTRA = ?EXTRA,'
      '    U=?U'
      'WHERE'
      '    ID = ?OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ESERV_PARAMS'
      'WHERE'
      '        ID = ?OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ESERV_PARAMS('
      '    ID,'
      '    INVID,'
      '    MATID,'
      '    N,'
      '    SERVCOST,'
      '    EXTRA,'
      '    U'
      ')'
      'VALUES('
      '    ?ID,'
      '    ?INVID,'
      '    ?MATID,'
      '    ?N,'
      '    ?SERVCOST,'
      '    ?EXTRA,'
      '    ?U'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    INVID,'
      '    MATID,'
      '    N,'
      '    SERVCOST,'
      '    EXTRA,'
      '    U'
      'FROM'
      '    ESERV_PARAMS '
      ' WHERE '
      '        ESERV_PARAMS.ID = ?OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    INVID,'
      '    MATID,'
      '    N,'
      '    SERVCOST,'
      '    EXTRA,'
      '    U'
      'FROM'
      '    ESERV_PARAMS '
      'where INVID=:INVID')
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taEServParamsBeforeOpen
    OnNewRecord = taEServParamsNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080' ('#1087#1088#1080#1074#1103#1079#1072#1085#1086' '#1082' '#1085#1072#1082#1083#1072#1076#1085#1086#1081')'
    Left = 708
    Top = 224
    poSQLINT64ToBCD = True
    object taEServParamsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taEServParamsINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taEServParamsMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taEServParamsMATNAME: TFIBStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldKind = fkLookup
      FieldName = 'MATNAME'
      LookupDataSet = dm.taMat
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'MATID'
      Size = 40
      EmptyStrToNull = True
      Lookup = True
    end
    object taEServParamsN: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072' '#1087#1086#1090#1077#1088#1100
      FieldName = 'N'
    end
    object taEServParamsSERVCOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
      FieldName = 'SERVCOST'
      currency = True
    end
    object taEServParamsEXTRA: TFIBFloatField
      DisplayLabel = #1053#1072#1094#1077#1085#1082#1072' '#1085#1072' '#1089#1074#1086#1081' '#1084#1072#1090#1077#1088#1080#1072#1083
      FieldName = 'EXTRA'
    end
    object taEServParamsU: TFIBSmallIntField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'.'
      FieldName = 'U'
    end
  end
  object dsrEServParams: TDataSource
    DataSet = taEServParams
    Left = 708
    Top = 270
  end
  object taEServMat: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    INVID,'
      '    MATID,'
      '    COMPID,'
      '    N,'
      '    NW,'
      '    SCOST,'
      '    GETW,'
      '    OUTW'
      'FROM'
      '    ESERV_MAT ')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1052#1072#1090#1077#1088#1080#1072#1083#1099' '#1087#1086' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1087#1088#1086#1076#1072#1078#1080'/'#1074#1086#1079#1074#1088#1072#1090#1072
    Left = 780
    Top = 224
    poSQLINT64ToBCD = True
    object taEServMatID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taEServMatINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taEServMatMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taEServMatCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object taEServMatNW: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084'. '#1087#1086#1090#1077#1088#1080
      FieldName = 'NW'
      DisplayFormat = '0.###'
    end
    object taEServMatSCOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SCOST'
      currency = True
    end
    object taEServMatN: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072
      FieldName = 'N'
      DisplayFormat = '0%'
    end
    object taEServMatGETW: TFIBFloatField
      DisplayLabel = #1044#1086' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
      FieldName = 'GETW'
      DisplayFormat = '0.###'
    end
    object taEServMatOUTW: TFIBFloatField
      DisplayLabel = #1055#1086#1089#1083#1077' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
      FieldName = 'OUTW'
      DisplayFormat = '0.###'
    end
    object taEServMatCOMPNAME: TStringField
      DisplayLabel = #1042#1083#1072#1076#1077#1083#1077#1094
      FieldKind = fkLookup
      FieldName = 'COMPNAME'
      LookupDataSet = dm.taComp
      LookupKeyFields = 'COMPID'
      LookupResultField = 'NAME'
      KeyFields = 'COMPID'
      Size = 60
      Lookup = True
    end
    object taEServMatMATNAME: TStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldKind = fkLookup
      FieldName = 'MATNAME'
      LookupDataSet = dm.taMat
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'MATID'
      Size = 40
      Lookup = True
    end
  end
  object dsrEServMat: TDataSource
    DataSet = taEServMat
    Left = 780
    Top = 268
  end
  object taSListCalcPrihod: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  Prihod_W,'
      '  Need_W,'
      '  Diff_W,'
      '  Norma,'
      '  Poteri,'
      '  Prill$Name'
      'from SList_Calc_Prihod (:InvID, :Mode)')
    BeforeOpen = taSListCalcPrihodBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Description = #1055#1086#1076#1089#1095#1077#1090' '#1088#1072#1079#1085#1080#1094#1099' '#1079#1086#1083#1086#1090#1072' '#1084#1077#1078#1076#1091' '#1087#1088#1080#1093#1086#1076#1086#1084' '#1080' '#1088#1072#1089#1093#1086#1076#1086#1084
    Left = 432
    Top = 560
    object taSListCalcPrihodNEED_W: TFIBFloatField
      FieldName = 'NEED_W'
    end
    object taSListCalcPrihodDIFF_W: TFIBFloatField
      FieldName = 'DIFF_W'
    end
    object taSListCalcPrihodNORMA: TFIBFloatField
      FieldName = 'NORMA'
    end
    object taSListCalcPrihodPOTERI: TFIBFloatField
      FieldName = 'POTERI'
    end
    object taSListCalcPrihodPRIHOD_W: TFIBFloatField
      FieldName = 'PRIHOD_W'
    end
    object taSListCalcPrihodPRILLNAME: TFIBStringField
      FieldName = 'PRILL$NAME'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsrSListCalcPrihod: TDataSource
    DataSet = taSListCalcPrihod
    Left = 432
    Top = 608
  end
  object frEmptyInv: TfrDBDataSet
    DataSet = taEmptyInv
    Left = 64
    Top = 48
  end
  object taEmptyInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  i.Q, '
      '  i.W, '
      '  i.Cost,'
      '  i.Trans_Price,'
      '  dn.Name ndsName,'
      '  dn.NDS'
      'from '
      '  inv i'
      '  left outer join D_NDS dn on (i.ndsid = dn.ndsid)'
      'where '
      '  invid = ?InvID and'
      '  invSubType = 9')
    Transaction = dm.tr
    Database = dm.db
    DataSource = dsrSList
    Left = 64
    WaitEndMasterScroll = True
    object taEmptyInvQ: TFIBFloatField
      FieldName = 'Q'
    end
    object taEmptyInvW: TFIBFloatField
      FieldName = 'W'
    end
    object taEmptyInvCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taEmptyInvNDSNAME: TFIBStringField
      FieldName = 'NDSNAME'
      EmptyStrToNull = True
    end
    object taEmptyInvNDS: TFIBFloatField
      FieldName = 'NDS'
    end
    object taEmptyInvTRANS_PRICE: TFIBFloatField
      FieldName = 'TRANS_PRICE'
    end
  end
end
