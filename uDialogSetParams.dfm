object DialogSetParams: TDialogSetParams
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = #1047#1072#1087#1086#1083#1085#1077#1085#1080#1077' '#1087#1072#1088#1072#1084#1077#1090#1088#1086#1074
  ClientHeight = 163
  ClientWidth = 273
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object paramLabel: TLabel
    Left = 23
    Top = 8
    Width = 37
    Height = 13
    Caption = 'Caption'
  end
  object GroupBox: TGroupBox
    Left = 5
    Top = 35
    Width = 260
    Height = 89
    Caption = #1055#1077#1088#1080#1086#1076
    TabOrder = 0
    object Label2: TLabel
      Left = 24
      Top = 19
      Width = 37
      Height = 13
      Caption = #1053#1072#1095#1072#1083#1086
    end
    object Label3: TLabel
      Left = 24
      Top = 56
      Width = 31
      Height = 13
      Caption = #1050#1086#1085#1077#1094
    end
    object startDateBox: TcxDateEdit
      Left = 128
      Top = 16
      TabOrder = 0
      Width = 116
    end
    object endDateBox: TcxDateEdit
      Left = 128
      Top = 56
      TabOrder = 1
      Width = 116
    end
  end
  object BitBtn1: TBitBtn
    Left = 93
    Top = 130
    Width = 75
    Height = 25
    Caption = #1055#1088#1080#1085#1103#1090#1100
    TabOrder = 1
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 190
    Top = 130
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    Kind = bkCancel
  end
  object paramBox: TcxComboBox
    Left = 112
    Top = 8
    TabOrder = 3
    Width = 137
  end
end
