inherited fmTmpWHSemis: TfmTmpWHSemis
  Left = 12
  Top = 134
  Width = 796
  Height = 433
  Caption = #1057#1082#1083#1072#1076' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 380
    Width = 788
  end
  inherited tb1: TSpeedBar
    Width = 788
    object spitPrint: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 4
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = spitPrintClick
      SectionName = 'Untitled (0)'
    end
  end
  object gridWhSemis: TDBGridEh [2]
    Left = 0
    Top = 88
    Width = 788
    Height = 292
    Align = alClient
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmMain.dsrTmpWhSemis
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    RowSizingAllowed = True
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Width = 109
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SEMISNAME'
        Footers = <>
        Width = 110
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'MATNAME'
        Footers = <>
        Width = 89
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OPERNAME'
        Footers = <>
        Width = 104
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footer.FieldName = 'Q'
        Footer.ValueType = fvtSum
        Footers = <>
        Width = 48
      end
      item
        DisplayFormat = '0.###'
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footer.DisplayFormat = '0.###'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Width = 57
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'UQ'
        Footers = <>
        Title.Caption = #1045#1076'.'#1080#1079#1084'.|'#1082#1086#1083'-'#1074#1072
        Width = 66
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'UW'
        Footers = <>
        Title.Caption = #1045#1076'.'#1080#1079#1084'.|'#1074#1077#1089#1072
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object TBDock1: TTBDock [3]
    Left = 0
    Top = 42
    Width = 788
    Height = 23
    FixAlign = True
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      Caption = 'TBToolbar1'
      DockPos = 72
      DockRow = 1
      ShrinkMode = tbsmWrap
      TabOrder = 0
      object TBControlItem3: TTBControlItem
        Control = Label2
      end
      object TBControlItem4: TTBControlItem
        Control = cmbxDep
      end
      object TBControlItem5: TTBControlItem
        Control = Label3
      end
      object TBControlItem6: TTBControlItem
        Control = cmbxOper
      end
      object TBControlItem1: TTBControlItem
        Control = Label1
      end
      object TBControlItem2: TTBControlItem
        Control = cmbxSemis
      end
      object TBControlItem7: TTBControlItem
        Control = Label4
      end
      object TBControlItem8: TTBControlItem
        Control = cmbxMat
      end
      object Label1: TLabel
        Left = 393
        Top = 3
        Width = 80
        Height = 13
        Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090': '
      end
      object Label2: TLabel
        Left = 0
        Top = 3
        Width = 91
        Height = 13
        Caption = ' '#1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103': '
      end
      object Label3: TLabel
        Left = 212
        Top = 3
        Width = 59
        Height = 13
        Caption = ' '#1054#1087#1077#1088#1072#1094#1080#1103': '
      end
      object Label4: TLabel
        Left = 598
        Top = 3
        Width = 59
        Height = 13
        Caption = ' '#1052#1072#1090#1077#1088#1080#1072#1083': '
      end
      object cmbxSemis: TDBComboBoxEh
        Left = 473
        Top = 0
        Width = 125
        Height = 19
        DropDownBox.Rows = 20
        EditButtons = <>
        Flat = True
        TabOrder = 0
        Text = 'cmbxSemis'
        Visible = True
        OnChange = cmbxSemisChange
      end
      object cmbxDep: TDBComboBoxEh
        Left = 91
        Top = 0
        Width = 121
        Height = 19
        DropDownBox.Rows = 20
        EditButtons = <>
        Flat = True
        TabOrder = 1
        Text = 'cmbxDep'
        Visible = True
        OnChange = cmbxDepChange
      end
      object cmbxOper: TDBComboBoxEh
        Left = 271
        Top = 0
        Width = 122
        Height = 19
        DropDownBox.Rows = 20
        EditButtons = <>
        Flat = True
        TabOrder = 2
        Text = 'cmbxOper'
        Visible = True
        OnChange = cmbxOperChange
      end
      object cmbxMat: TDBComboBoxEh
        Left = 657
        Top = 0
        Width = 121
        Height = 19
        DropDownBox.Rows = 20
        EditButtons = <>
        Flat = True
        TabOrder = 3
        Text = 'cmbxMat'
        Visible = True
        OnChange = cmbxMatChange
      end
    end
  end
  object TBDock2: TTBDock [4]
    Left = 0
    Top = 65
    Width = 788
    Height = 23
    object TBToolbar2: TTBToolbar
      Left = 0
      Top = 0
      TabOrder = 0
      object TBControlItem9: TTBControlItem
        Control = Label5
      end
      object TBControlItem10: TTBControlItem
        Control = edBd
      end
      object TBControlItem11: TTBControlItem
        Control = Label6
      end
      object TBControlItem12: TTBControlItem
        Control = edEd
      end
      object TBItem1: TTBItem
        Action = acGo
      end
      object Label5: TLabel
        Left = 0
        Top = 3
        Width = 50
        Height = 13
        Caption = #1055#1077#1088#1080#1086#1076' '#1089' '
      end
      object Label6: TLabel
        Left = 137
        Top = 3
        Width = 18
        Height = 13
        Caption = ' '#1087#1086' '
      end
      object edBd: TDBDateTimeEditEh
        Left = 50
        Top = 0
        Width = 87
        Height = 19
        EditButtons = <>
        Flat = True
        Kind = dtkDateEh
        TabOrder = 0
        Visible = True
      end
      object edEd: TDBDateTimeEditEh
        Left = 155
        Top = 0
        Width = 87
        Height = 19
        EditButtons = <>
        Flat = True
        Kind = dtkDateEh
        TabOrder = 1
        Visible = True
      end
    end
  end
  inherited ilButtons: TImageList
    Left = 166
    Top = 180
  end
  inherited fmstr: TFormStorage
    Left = 112
    Top = 180
  end
  object acEvent: TActionList
    Left = 120
    Top = 244
    object acGo: TAction
      Caption = 'Go!'
      OnExecute = acGoExecute
    end
  end
  object PrintGird: TPrintDBGridEh
    DBGridEh = gridWhSemis
    Options = []
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 300
    Top = 232
  end
end
