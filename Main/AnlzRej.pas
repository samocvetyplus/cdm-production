unit AnlzRej;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList,  ImgList,  ExtCtrls,
  ComCtrls, DB, FIBDataSet, pFIBDataSet, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxShellBrowserDialog, cxGridLevel, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, TB2Item, TB2Dock,
  TB2Toolbar, rxPlacemnt, rxSpeedbar;

type
  TfmAnlzRej = class(TfmAncestor)
    taAnlzRej: TpFIBDataSet;
    taAnlzRejREJNAME: TFIBStringField;
    taAnlzRejPSNAME: TFIBStringField;
    taAnlzRejOPERATION: TFIBStringField;
    taAnlzRejW: TFIBFloatField;
    taAnlzRejSEMIS: TFIBStringField;
    dsrAnlzRej: TDataSource;
    taAnlzRejWOITEMID: TFIBIntegerField;
    taAnlzRejITDATE: TFIBDateTimeField;
    acPeriod: TAction;
    TBDock1: TTBDock;
    tlbrFunc: TTBToolbar;
    TBItem1: TTBItem;
    acExportExcel: TAction;
    SpeedItem1: TSpeedItem;
    acPrint: TAction;
    SpeedItem2: TSpeedItem;
    taAnlzRejDEPNAME: TFIBStringField;
    pgRej: TPageControl;
    tbshRej: TTabSheet;
    tbshRejCurr: TTabSheet;
    taAnlzWhRej: TpFIBDataSet;
    dsrAnlzWhRej: TDataSource;
    taAnlzWhRejID: TFIBIntegerField;
    taAnlzWhRejOPERATION: TFIBStringField;
    taAnlzWhRejQ: TFIBIntegerField;
    taAnlzWhRejW: TFIBFloatField;
    taAnlzWhRejSEMIS: TFIBStringField;
    taAnlzWhRejDEPNAME: TFIBStringField;
    GridRejDBTableView1: TcxGridDBTableView;
    GridRejLevel1: TcxGridLevel;
    GridRej: TcxGrid;
    GridWhRejDBTableView1: TcxGridDBTableView;
    GridWhRejLevel1: TcxGridLevel;
    GridWhRej: TcxGrid;
    GridWhRejDBTableView1OPERATION: TcxGridDBColumn;
    GridWhRejDBTableView1Q: TcxGridDBColumn;
    GridWhRejDBTableView1W: TcxGridDBColumn;
    GridWhRejDBTableView1SEMIS: TcxGridDBColumn;
    GridWhRejDBTableView1DEPNAME: TcxGridDBColumn;
    GridRejDBTableView1REJNAME: TcxGridDBColumn;
    GridRejDBTableView1PSNAME: TcxGridDBColumn;
    GridRejDBTableView1OPERATION: TcxGridDBColumn;
    GridRejDBTableView1W: TcxGridDBColumn;
    GridRejDBTableView1SEMIS: TcxGridDBColumn;
    GridRejDBTableView1ITDATE: TcxGridDBColumn;
    GridRejDBTableView1DEPNAME: TcxGridDBColumn;
    SaveDialog: TSaveDialog;
    ShellBrowserDialog: TcxShellBrowserDialog;
    taAnlzRejDOCNO: TFIBIntegerField;
    taAnlzRejDOCDATE: TFIBDateTimeField;
    GridRejDBTableView1Column1: TcxGridDBColumn;
    GridRejDBTableView1Column2: TcxGridDBColumn;
    GridRejDBBandedTableView1: TcxGridDBBandedTableView;
    taAnlzRejIDOCNO: TFIBIntegerField;
    taAnlzRejIDOCDATE: TFIBDateTimeField;
    GridRejDBTableView1Column3: TcxGridDBColumn;
    GridRejDBTableView1Column4: TcxGridDBColumn;
    GridRejDBBandedTableView1REJNAME: TcxGridDBBandedColumn;
    GridRejDBBandedTableView1PSNAME: TcxGridDBBandedColumn;
    GridRejDBBandedTableView1OPERATION: TcxGridDBBandedColumn;
    GridRejDBBandedTableView1W: TcxGridDBBandedColumn;
    GridRejDBBandedTableView1SEMIS: TcxGridDBBandedColumn;
    GridRejDBBandedTableView1ITDATE: TcxGridDBBandedColumn;
    GridRejDBBandedTableView1DEPNAME: TcxGridDBBandedColumn;
    GridRejDBBandedTableView1DOCNO: TcxGridDBBandedColumn;
    GridRejDBBandedTableView1DOCDATE: TcxGridDBBandedColumn;
    GridRejDBBandedTableView1IDOCNO: TcxGridDBBandedColumn;
    GridRejDBBandedTableView1IDOCDATE: TcxGridDBBandedColumn;
    taAnlzRejQ: TFIBFloatField;
    GridRejDBBandedTableView1Column1: TcxGridDBBandedColumn;
    taAnlzRejREJREF: TFIBIntegerField;
    procedure taAnlzRejBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acExportExcelExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
  private
    FBeginDate, FEndDate : TDateTime;
  end;

var
  fmAnlzRej: TfmAnlzRej;

implementation

uses DictData, dbUtil, Period, cxGridExportLink;

{$R *.dfm}

procedure TfmAnlzRej.taAnlzRejBeforeOpen(DataSet: TDataSet);
begin
  taAnlzRej.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(FBeginDate);
  taAnlzRej.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(FEndDate);
end;

procedure TfmAnlzRej.FormCreate(Sender: TObject);
begin
  FBeginDate := dm.BeginDate;
  FEndDate := dm.EndDate;
  //tlbrFunc.Skin := dm.TBSkin;
  acPeriod.Caption := '������ � '+DateToStr(FBeginDate)+' �� '+DateToStr(FEndDate);
  OpenDataSets([taAnlzRej, taAnlzWhRej]);
end;

procedure TfmAnlzRej.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taAnlzRej, taAnlzWhRej]);
end;

procedure TfmAnlzRej.acPeriodExecute(Sender: TObject);
var
  Bd, Ed : TDateTime;
begin
  Bd := FBeginDate;
  Ed := FEndDate;
  if ShowPeriodForm(Bd, Ed) then
  begin
    FBeginDate := Bd;
    FEndDate := Ed;
    acPeriod.Caption := '������ � '+DateToStr(FBeginDate)+' �� '+DateToStr(FEndDate);
    Application.ProcessMessages;
    ReOpenDataSets([taAnlzRej, taAnlzWhRej]);
    Application.ProcessMessages;
  end;
end;

procedure TfmAnlzRej.acExportExcelExecute(Sender: TObject);
var
  FileName: string;
  Grid: TcxGrid;
begin
  if ShellBrowserDialog.Execute then
  begin
    case pgRej.ActivePageIndex of
    0 :
     begin
      FileName := ShellBrowserDialog.Path + '\�������������� ����.xls';
      Grid := GridRej;
     end;
    1 :
     begin
       FileName := ShellBrowserDialog.Path + '\������� ����.xls';
       Grid := GridWhRej;
     end;
    end;
    ExportGridToExcel(FileName, Grid);
  end;
end;

procedure TfmAnlzRej.acPrintExecute(Sender: TObject);
begin
  {case pgRej.ActivePageIndex of
    //0 : dxPrinter.CurrentLink := dxRejLink;
    //1 : dxPrinter.CurrentLink := dxWhRejLink;
  end;}
  //dxPrinter.Preview(True,nil);
end;

end.
