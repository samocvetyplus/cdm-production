inherited fmRequestList: TfmRequestList
  Left = 164
  Top = 199
  Caption = #1057#1087#1080#1089#1086#1082' '#1079#1072#1103#1074#1086#1082' '#1086#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
  ClientHeight = 494
  ClientWidth = 704
  ExplicitWidth = 712
  ExplicitHeight = 528
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 475
    Width = 704
    ExplicitTop = 475
    ExplicitWidth = 704
  end
  inherited tb2: TSpeedBar
    Width = 704
    Visible = False
    ExplicitWidth = 704
  end
  inherited tb1: TSpeedBar
    Width = 704
    ExplicitWidth = 704
    inherited spitAdd: TSpeedItem
      Visible = False
    end
    inherited spitPrint: TSpeedItem [3]
      Action = acPrintRequestSummary
      BtnCaption = #1042#1089#1077' '#1079#1072#1103#1074#1082#1080
      Left = 131
      OnClick = acPrintRequestSummaryExecute
    end
    inherited spitDel: TSpeedItem [4]
      Left = 3
    end
    inherited spitEdit: TSpeedItem [5]
      Left = 67
      OnClick = acEditDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    Top = 94
    Width = 704
    Height = 381
    DataSource = dm.dsrA2Ins
    DrawMemoText = True
    FooterRowCount = 1
    FrozenCols = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    SumList.Active = True
    UseMultiTitle = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SUPCODE'
        Footers = <>
        Title.Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100'|'#1050#1086#1076
        Width = 77
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SUPNAME'
        Footers = <>
        Title.Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100'|'#1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        Title.EndEllipsis = True
        Width = 130
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1047#1072#1103#1074#1082#1072'|'#8470
        Title.EndEllipsis = True
        Width = 62
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1047#1072#1103#1074#1082#1072'|'#1044#1072#1090#1072
        Title.EndEllipsis = True
        Width = 57
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ABD'
        Footers = <>
        Title.EndEllipsis = True
        Width = 90
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'AED'
        Footers = <>
        Title.EndEllipsis = True
        Width = 71
      end
      item
        Checkboxes = True
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ANALIZ'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.EndEllipsis = True
        Width = 33
      end
      item
        Color = 16776176
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footer.FieldName = 'Q'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Width = 59
      end
      item
        Color = 16776176
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'MW'
        Footer.FieldName = 'MW'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Width = 43
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'MEMO'
        Footers = <>
        Title.Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
        Title.EndEllipsis = True
      end>
  end
  object TBDock1: TTBDock [4]
    Left = 0
    Top = 71
    Width = 704
    Height = 23
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      BorderStyle = bsNone
      TabOrder = 0
      object TBSubmenuItem1: TTBSubmenuItem
        Caption = #1040#1085#1072#1083#1080#1079
        DropdownCombo = True
        object TBItem5: TTBItem
          Action = acCheckAll
        end
        object TBItem6: TTBItem
          Action = acUncheckAll
        end
        object TBItem7: TTBItem
          Action = acInverse
        end
        object TBEditItem1: TTBEditItem
          EditCaption = #1044#1072#1090#1072
          OnAcceptText = TBEditItem1AcceptText
        end
      end
    end
  end
  inherited ilButtons: TImageList
    Top = 192
  end
  inherited ppDep: TPopupMenu
    Top = 92
  end
  inherited acAEvent: TActionList
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
    object acCheckAll: TAction
      Caption = #1054#1090#1084#1077#1090#1080#1090#1100' '#1074#1089#1077
      OnExecute = acCheckAllExecute
    end
    object acUncheckAll: TAction
      Caption = #1057#1085#1103#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1080#1077
      OnExecute = acUncheckAllExecute
    end
    object acInverse: TAction
      Caption = #1048#1085#1074#1077#1088#1089#1080#1103
      OnExecute = acInverseExecute
    end
    object acPrintRequestSummary: TAction
      Caption = #1048#1090#1086#1075#1086' '#1087#1086' '#1079#1072#1103#1074#1082#1072#1084
      ImageIndex = 4
      OnExecute = acPrintRequestSummaryExecute
    end
  end
  inherited ppDoc: TTBPopupMenu
    Left = 184
    Top = 144
    object TBSeparatorItem2: TTBSeparatorItem
    end
    object TBItem8: TTBItem
      Action = acPrintRequestSummary
    end
  end
end
