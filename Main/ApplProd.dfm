inherited fmApplProd: TfmApplProd
  Left = 52
  Top = 125
  VertScrollBar.Range = 0
  ActiveControl = edDocNo
  Align = alClient
  BorderStyle = bsNone
  Caption = #1047#1072#1103#1074#1082#1072
  ClientHeight = 381
  ClientWidth = 589
  OldCreateOrder = True
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 362
    Width = 589
  end
  inherited tb1: TSpeedBar
    Width = 589
  end
  object plMain: TPanel [2]
    Left = 0
    Top = 65
    Width = 589
    Height = 50
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 2
    object lbNoDoc: TLabel
      Left = 8
      Top = 8
      Width = 73
      Height = 13
      Caption = #1053#1086#1084#1077#1088' '#1079#1072#1103#1074#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbDateDoc: TLabel
      Left = 8
      Top = 30
      Width = 65
      Height = 13
      Caption = #1044#1072#1090#1072' '#1079#1072#1103#1074#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object SpeedButton1: TSpeedButton
      Left = 1016
      Top = 4
      Width = 133
      Height = 22
      Flat = True
    end
    object edDocNo: TDBEditEh
      Left = 92
      Top = 4
      Width = 73
      Height = 19
      EditButtons = <>
      Flat = True
      TabOrder = 0
      Visible = True
    end
    object edDocDate: TDBDateTimeEditEh
      Left = 92
      Top = 24
      Width = 74
      Height = 19
      EditButtons = <>
      Flat = True
      Kind = dtkDateEh
      TabOrder = 1
      Visible = True
    end
  end
  object TBDock1: TTBDock [3]
    Left = 0
    Top = 42
    Width = 589
    Height = 23
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      Caption = 'TBToolbar1'
      TabOrder = 0
      object TBControlItem1: TTBControlItem
        Control = Label2
      end
      object TBControlItem2: TTBControlItem
        Control = edFilterArt
      end
      object Label2: TLabel
        Left = 0
        Top = 3
        Width = 38
        Height = 13
        Caption = #1055#1086#1080#1089#1082': '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object edFilterArt: TDBEditEh
        Left = 38
        Top = 0
        Width = 74
        Height = 19
        EditButtons = <>
        Flat = True
        TabOrder = 0
        Visible = True
      end
    end
  end
  object gridAppl: TDBGridEh [4]
    Left = 0
    Top = 115
    Width = 589
    Height = 247
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dmAppl.dsrAppl
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghClearSelection, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Width = 58
      end
      item
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Width = 35
      end
      item
        EditButtons = <>
        FieldName = 'SZNAME'
        Footers = <>
        Width = 46
      end
      item
        EditButtons = <>
        FieldName = 'Q'
        Footers = <>
        Title.Caption = #1047#1072#1103#1074#1083#1077#1085#1086
        Width = 53
      end
      item
        EditButtons = <>
        FieldName = 'FQ'
        Footers = <>
        Width = 94
      end
      item
        EditButtons = <>
        FieldName = 'NQ'
        Footers = <>
        Width = 79
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'R'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Width = 53
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'MODEL'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'COMMENT'
        Footers = <>
        Width = 93
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ilButtons: TImageList
    Left = 126
    Top = 184
  end
  inherited fmstr: TFormStorage
    Options = [fpState]
    Left = 184
    Top = 184
  end
end
