object DialogAffinage: TDialogAffinage
  Left = 754
  Top = 0
  BorderIcons = [biMinimize]
  BorderStyle = bsDialog
  Caption = #1057#1098#1105#1084' - '#1079#1086#1083#1086#1090#1086
  ClientHeight = 581
  ClientWidth = 1089
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TdxStatusBar
    Left = 0
    Top = 561
    Width = 1089
    Height = 20
    Panels = <>
    PaintStyle = stpsOffice11
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
  end
  object PagesGroup: TcxPageControl
    Left = 0
    Top = 0
    Width = 1089
    Height = 561
    ActivePage = SheetGroupPassport
    Align = alClient
    HotTrack = True
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    TabOrder = 1
    ClientRectBottom = 561
    ClientRectRight = 1089
    ClientRectTop = 24
    object SheetGroupPassport: TcxTabSheet
      Caption = #1055#1072#1089#1087#1086#1088#1090#1072
      ImageIndex = 0
      object PagesPassport: TcxPageControl
        Left = 0
        Top = 0
        Width = 1089
        Height = 537
        ActivePage = SheetPassports
        Align = alClient
        HideTabs = True
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = False
        TabOrder = 0
        ClientRectBottom = 537
        ClientRectRight = 1089
        ClientRectTop = 0
        object SheetPassports: TcxTabSheet
          Caption = #1055#1072#1089#1087#1086#1088#1090#1072
          ImageIndex = 0
          object GridPassports: TcxGrid
            Left = 0
            Top = 56
            Width = 1089
            Height = 481
            Align = alClient
            TabOrder = 0
            LookAndFeel.Kind = lfOffice11
            object GridPassportsView: TcxGridDBBandedTableView
              OnDblClick = GridPassportsViewDblClick
              NavigatorButtons.ConfirmDelete = False
              DataController.DataSource = DataSourcePassports
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.ScrollBars = ssVertical
              OptionsView.GroupByBox = False
              OptionsView.Indicator = True
              Bands = <
                item
                  Width = 43
                end
                item
                  Caption = #1055#1077#1088#1080#1086#1076
                  Width = 185
                end
                item
                  Caption = #1057#1086#1079#1076#1072#1085
                  Width = 192
                end
                item
                  Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
                  Width = 284
                end>
              object GridPassportsViewDESIGNATION: TcxGridDBBandedColumn
                Caption = #8470
                DataBinding.FieldName = 'DESIGNATION'
                Options.Editing = False
                Options.Filtering = False
                Position.BandIndex = 0
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object GridPassportsViewPERIODBEGIN: TcxGridDBBandedColumn
                Caption = #1053#1072#1095#1072#1083#1086
                DataBinding.FieldName = 'PERIOD$BEGIN'
                HeaderAlignmentHorz = taCenter
                Options.Editing = False
                Options.Filtering = False
                Width = 74
                Position.BandIndex = 1
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object GridPassportsViewPERIODEND: TcxGridDBBandedColumn
                Caption = #1050#1086#1085#1077#1094
                DataBinding.FieldName = 'PERIOD$END'
                HeaderAlignmentHorz = taCenter
                Options.Editing = False
                Options.Filtering = False
                Width = 77
                Position.BandIndex = 1
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object GridPassportsViewCREATEDDATE: TcxGridDBBandedColumn
                Caption = #1044#1072#1090#1072
                DataBinding.FieldName = 'CREATED$DATE'
                Options.Editing = False
                Options.Filtering = False
                Width = 92
                Position.BandIndex = 2
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object GridPassportsViewCREATEDBY: TcxGridDBBandedColumn
                Caption = #1060#1072#1084#1080#1083#1080#1103' '#1048'.'#1054'.'
                DataBinding.FieldName = 'CREATED$BY$TITLE'
                HeaderAlignmentHorz = taCenter
                Options.Editing = False
                Width = 100
                Position.BandIndex = 2
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object GridPassportsViewSTATE: TcxGridDBBandedColumn
                Caption = #1054#1087#1080#1089#1072#1085#1080#1077
                DataBinding.FieldName = 'STATE$TITLE'
                Options.Editing = False
                Width = 78
                Position.BandIndex = 3
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object GridPassportsViewSTATEBY: TcxGridDBBandedColumn
                Caption = #1060#1072#1084#1080#1083#1080#1103' '#1048'.'#1054'.'
                DataBinding.FieldName = 'STATE$BY$TITLE'
                Options.Editing = False
                Options.Filtering = False
                Width = 97
                Position.BandIndex = 3
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object GridPassportsViewSTATEDATE: TcxGridDBBandedColumn
                Caption = #1044#1072#1090#1072
                DataBinding.FieldName = 'STATE$DATE'
                HeaderAlignmentHorz = taCenter
                Options.Editing = False
                Options.Filtering = False
                Width = 109
                Position.BandIndex = 3
                Position.ColIndex = 2
                Position.RowIndex = 0
              end
              object GridPassportsViewColumn1: TcxGridDBBandedColumn
                DataBinding.FieldName = 'ID'
                Position.BandIndex = 0
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
            end
            object GridPassportsLevel1: TcxGridLevel
              GridView = GridPassportsView
            end
          end
          object DockControlPassports: TdxBarDockControl
            Left = 0
            Top = 0
            Width = 1089
            Height = 56
            Align = dalTop
            BarManager = BarManager
          end
        end
        object SheetPassport: TcxTabSheet
          Tag = 1
          Caption = #1055#1072#1089#1087#1086#1088#1090
          ImageIndex = 1
          object GridPassportItems: TcxGrid
            Left = 208
            Top = 56
            Width = 881
            Height = 481
            Align = alClient
            TabOrder = 0
            LookAndFeel.Kind = lfOffice11
            LookAndFeel.NativeStyle = False
            object GridPassportView: TcxGridDBBandedTableView
              NavigatorButtons.ConfirmDelete = False
              OnCustomDrawCell = GridPassportViewCustomDrawCell
              DataController.DataSource = DataSourcePassportGroups
              DataController.Summary.DefaultGroupSummaryItems = <
                item
                  Kind = skSum
                  Position = spFooter
                  Column = GridPassportViewWEIGHTREMAINDEROUT
                end
                item
                  Kind = skSum
                  Position = spFooter
                  Column = GridPassportViewWEIGHTREMAINDERCURRENT
                end
                item
                  Kind = skSum
                  Position = spFooter
                  Column = GridPassportViewWEIGHTIN
                end
                item
                  Kind = skSum
                  Position = spFooter
                  Column = GridPassportViewWEIGHTREMAINDER
                end
                item
                  Kind = skSum
                  Position = spFooter
                  Column = GridPassportViewWEIGHT
                end
                item
                  Kind = skSum
                  Position = spFooter
                  Column = GridPassportViewWEIGHTREMAINDERIN
                end>
              DataController.Summary.FooterSummaryItems = <
                item
                  Kind = skSum
                  Column = GridPassportViewWEIGHTREMAINDERIN
                end
                item
                  Kind = skSum
                  Column = GridPassportViewWEIGHT
                end
                item
                  Kind = skSum
                  Column = GridPassportViewWEIGHTREMAINDER
                end
                item
                  Kind = skSum
                  Column = GridPassportViewWEIGHTIN
                end
                item
                  Kind = skSum
                  Column = GridPassportViewWEIGHTREMAINDERCURRENT
                end
                item
                  Kind = skSum
                  Column = GridPassportViewWEIGHTREMAINDEROUT
                end>
              DataController.Summary.SummaryGroups = <>
              OptionsCustomize.ColumnFiltering = False
              OptionsData.Deleting = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GroupFooters = gfAlwaysVisible
              OptionsView.Indicator = True
              Bands = <
                item
                  Caption = #1043#1088#1091#1087#1087#1072
                  Options.Moving = False
                  Width = 187
                end
                item
                  Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
                  Options.Moving = False
                  Width = 92
                end
                item
                  Caption = #1054#1087#1077#1088#1072#1094#1080#1103
                  Options.Moving = False
                end
                item
                  Caption = #1042#1077#1089
                  Options.Moving = False
                  Width = 742
                end
                item
                  Caption = #1057#1098#1077#1084
                  Options.Moving = False
                  Position.BandIndex = 3
                  Position.ColIndex = 0
                  Width = 231
                end
                item
                  Caption = #1055#1086#1083#1091#1095#1077#1085#1086
                  Options.Moving = False
                  Position.BandIndex = 3
                  Position.ColIndex = 1
                  Width = 77
                end
                item
                  Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1081
                  Options.Moving = False
                  Position.BandIndex = 3
                  Position.ColIndex = 3
                  Width = 77
                end
                item
                  Caption = #1055#1086#1090#1077#1088#1080
                  Options.Moving = False
                  Position.BandIndex = 3
                  Position.ColIndex = 2
                  Width = 357
                end>
              object GridPassportViewColumn2: TcxGridDBBandedColumn
                DataBinding.FieldName = 'ID'
                Visible = False
                Position.BandIndex = 0
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object GridPassportViewColumn1: TcxGridDBBandedColumn
                DataBinding.FieldName = 'Title'
                Options.Editing = False
                Options.Filtering = False
                Styles.Content = StyleVirtual
                Styles.OnGetContentStyle = GridPassportViewColumn1StylesGetContentStyle
                Width = 200
                Position.BandIndex = 0
                Position.ColIndex = 0
                Position.RowIndex = 0
                IsCaptionAssigned = True
              end
              object GridPassportViewWEIGHTREMAINDERIN: TcxGridDBBandedColumn
                Caption = #1042#1093#1086#1076#1103#1097#1080#1081
                DataBinding.FieldName = 'WEIGHT$REMAINDER$IN'
                Options.Editing = False
                Options.Filtering = False
                Options.Grouping = False
                Styles.Content = StyleReadOnly
                Styles.OnGetContentStyle = GridPassportViewColumn1StylesGetContentStyle
                Width = 100
                Position.BandIndex = 4
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object GridPassportViewWEIGHT: TcxGridDBBandedColumn
                Caption = #1058#1077#1082#1091#1097#1080#1081
                DataBinding.FieldName = 'WEIGHT'
                Options.Editing = False
                Options.Filtering = False
                Options.Grouping = False
                Styles.Content = StyleReadOnly
                Styles.OnGetContentStyle = GridPassportViewColumn1StylesGetContentStyle
                Width = 99
                Position.BandIndex = 4
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object GridPassportViewWEIGHTREMAINDER: TcxGridDBBandedColumn
                Caption = #1042#1089#1077#1075#1086
                DataBinding.FieldName = 'WEIGHT$REMAINDER'
                Options.Editing = False
                Options.Filtering = False
                Options.Grouping = False
                Styles.Content = StyleReadOnly
                Styles.OnGetContentStyle = GridPassportViewColumn1StylesGetContentStyle
                Width = 100
                Position.BandIndex = 4
                Position.ColIndex = 2
                Position.RowIndex = 0
              end
              object GridPassportViewColumn4: TcxGridDBBandedColumn
                Caption = #1053#1086#1088#1084#1072
                DataBinding.FieldName = 'WEIGHT$REMAINDER$NORM'
                Options.Editing = False
                Options.Filtering = False
                Options.IncSearch = False
                Options.Grouping = False
                Options.Moving = False
                Options.Sorting = False
                Styles.Content = StyleReadOnly
                Styles.OnGetContentStyle = GridPassportViewColumn1StylesGetContentStyle
                Width = 78
                Position.BandIndex = 7
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object GridPassportViewWEIGHTIN: TcxGridDBBandedColumn
                DataBinding.FieldName = 'WEIGHT$IN'
                Options.Editing = False
                Options.Filtering = False
                Options.Grouping = False
                Styles.Content = StyleReadOnly
                Styles.OnGetContentStyle = GridPassportViewColumn1StylesGetContentStyle
                Width = 68
                Position.BandIndex = 5
                Position.ColIndex = 0
                Position.RowIndex = 0
                IsCaptionAssigned = True
              end
              object GridPassportViewWEIGHTREMAINDERCURRENT: TcxGridDBBandedColumn
                Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1080#1077
                DataBinding.FieldName = 'WEIGHT$REMAINDER$FACT'
                Options.Editing = False
                Options.Filtering = False
                Options.Grouping = False
                Styles.Content = StyleReadOnly
                Styles.OnGetContentStyle = GridPassportViewColumn1StylesGetContentStyle
                Width = 96
                Position.BandIndex = 7
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object GridPassportViewWEIGHTREMAINDEROUT: TcxGridDBBandedColumn
                DataBinding.FieldName = 'WEIGHT$REMAINDER$OUT'
                PropertiesClassName = 'TcxButtonEditProperties'
                Properties.Buttons = <
                  item
                    Default = True
                    Kind = bkEllipsis
                  end>
                Properties.OnButtonClick = GridPassportViewWEIGHTREMAINDEROUTPropertiesButtonClick
                Options.Filtering = False
                Options.ShowEditButtons = isebAlways
                Options.Grouping = False
                Styles.OnGetContentStyle = GridPassportViewColumn1StylesGetContentStyle
                Width = 72
                Position.BandIndex = 6
                Position.ColIndex = 0
                Position.RowIndex = 0
                IsCaptionAssigned = True
              end
              object GridPassportViewColumn3: TcxGridDBBandedColumn
                Caption = #1057#1074#1077#1088#1093' '#1085#1086#1088#1084'.'
                DataBinding.FieldName = 'WEIGHT$REMAINDER$Plus'
                Styles.Content = StyleReadOnly
                Width = 94
                Position.BandIndex = 7
                Position.ColIndex = 2
                Position.RowIndex = 0
              end
              object GridPassportViewColumn5: TcxGridDBBandedColumn
                Caption = #1069#1082#1086#1085#1086#1084#1080#1103
                DataBinding.FieldName = 'WEIGHT$REMAINDER$Minus'
                Styles.Content = StyleReadOnly
                Width = 89
                Position.BandIndex = 7
                Position.ColIndex = 3
                Position.RowIndex = 0
              end
              object GridPassportViewColumn6: TcxGridDBBandedColumn
                DataBinding.FieldName = 'PlaceOfStorage$Title'
                Options.Editing = False
                Options.Filtering = False
                Styles.Content = StyleReadOnly
                Position.BandIndex = 1
                Position.ColIndex = 0
                Position.RowIndex = 0
                IsCaptionAssigned = True
              end
              object GridPassportViewColumn7: TcxGridDBBandedColumn
                DataBinding.FieldName = 'Operation$Title'
                Options.Editing = False
                Styles.Content = StyleReadOnly
                Position.BandIndex = 2
                Position.ColIndex = 0
                Position.RowIndex = 0
                IsCaptionAssigned = True
              end
              object GridPassportViewColumn8: TcxGridDBBandedColumn
                DataBinding.FieldName = 'Childs$Count'
                Visible = False
                Position.BandIndex = 0
                Position.ColIndex = 2
                Position.RowIndex = 0
              end
            end
            object GridPassportItemsView: TcxGridDBTableView
              NavigatorButtons.ConfirmDelete = False
              DataController.DataSource = DataSourcePassportItems
              DataController.DetailKeyFieldNames = 'GROUPID'
              DataController.KeyFieldNames = 'GROUPID'
              DataController.MasterKeyFieldNames = 'ID'
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '0.000'
                  Kind = skSum
                  FieldName = 'WEIGHT'
                  Column = GridPassportItemsViewWEIGHT
                end
                item
                  Format = '0.000'
                  Kind = skSum
                  FieldName = 'WEIGHT$IN'
                  Column = GridPassportItemsViewColumn1
                end
                item
                  Format = '0.000'
                  Kind = skSum
                  FieldName = 'WEIGHT$NORM'
                  Column = GridPassportItemsViewColumn2
                end>
              DataController.Summary.SummaryGroups = <>
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              object GridPassportItemsViewOPERATIONTITLE: TcxGridDBColumn
                Caption = #1054#1087#1077#1088#1072#1094#1080#1103
                DataBinding.FieldName = 'OPERATION$TITLE'
                Options.Editing = False
                Options.Filtering = False
                Options.Sorting = False
                Styles.Content = StyleReadOnly
                Width = 179
              end
              object GridPassportItemsViewPLACEOFSTORAGETITLE: TcxGridDBColumn
                Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
                DataBinding.FieldName = 'PLACE$OF$STORAGE$TITLE'
                Options.Editing = False
                Options.Filtering = False
                Options.Sorting = False
                Styles.Content = StyleReadOnly
                Width = 179
              end
              object GridPassportItemsViewWEIGHT: TcxGridDBColumn
                Caption = #1057#1098#1077#1084
                DataBinding.FieldName = 'WEIGHT'
                Options.Editing = False
                Options.Filtering = False
                Options.Sorting = False
                Styles.Content = StyleReadOnly
                Width = 89
              end
              object GridPassportItemsViewColumn2: TcxGridDBColumn
                Caption = #1053#1086#1088#1084#1072
                DataBinding.FieldName = 'WEIGHT$NORM'
                Options.Editing = False
                Options.Filtering = False
                Width = 94
              end
              object GridPassportItemsViewColumn1: TcxGridDBColumn
                Caption = #1055#1086#1083#1091#1095#1077#1085#1086
                DataBinding.FieldName = 'WEIGHT$IN'
                Options.Editing = False
                Options.Filtering = False
                Options.Sorting = False
                Width = 94
              end
            end
            object GridPassportLevel: TcxGridLevel
              GridView = GridPassportView
              object GridPassportItemsLevel2: TcxGridLevel
                GridView = GridPassportItemsView
              end
            end
          end
          object GridPassport: TcxDBVerticalGrid
            Left = 0
            Top = 56
            Width = 200
            Height = 481
            Align = alLeft
            LookAndFeel.Kind = lfOffice11
            OptionsView.ScrollBars = ssVertical
            OptionsView.CategoryExplorerStyle = True
            OptionsView.PaintStyle = psDelphi
            OptionsView.RowHeaderWidth = 97
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            TabOrder = 1
            DataController.DataSource = DataSourcePassports
            Version = 1
            object GridPassportDBEditorRow1: TcxDBEditorRow
              Properties.Caption = #8470
              Properties.DataBinding.FieldName = 'DESIGNATION'
              Properties.Options.Editing = False
              ID = 0
              ParentID = -1
              Index = 0
              Version = 1
            end
            object GridPassportCategoryRow1: TcxCategoryRow
              Properties.Caption = #1055#1077#1088#1080#1086#1076
              ID = 1
              ParentID = -1
              Index = 1
              Version = 1
            end
            object GridPassportDBEditorRow2: TcxDBEditorRow
              Properties.Caption = #1053#1072#1095#1072#1083#1086
              Properties.EditPropertiesClassName = 'TcxDateEditProperties'
              Properties.EditProperties.ReadOnly = False
              Properties.DataBinding.FieldName = 'PERIOD$BEGIN'
              Properties.Options.Editing = False
              ID = 2
              ParentID = -1
              Index = 2
              Version = 1
            end
            object GridPassportDBEditorRow3: TcxDBEditorRow
              Properties.Caption = #1050#1086#1085#1077#1094
              Properties.EditPropertiesClassName = 'TcxDateEditProperties'
              Properties.EditProperties.ReadOnly = True
              Properties.DataBinding.FieldName = 'PERIOD$END'
              Properties.Options.Editing = False
              ID = 3
              ParentID = -1
              Index = 3
              Version = 1
            end
            object GridPassportCategoryRow2: TcxCategoryRow
              Properties.Caption = #1057#1086#1079#1076#1072#1085#1080#1077
              ID = 4
              ParentID = -1
              Index = 4
              Version = 1
            end
            object GridPassportDBEditorRow4: TcxDBEditorRow
              Properties.Caption = #1044#1072#1090#1072
              Properties.EditPropertiesClassName = 'TcxDateEditProperties'
              Properties.DataBinding.FieldName = 'CREATED$DATE'
              Properties.Options.Editing = False
              ID = 5
              ParentID = -1
              Index = 5
              Version = 1
            end
            object GridPassportDBEditorRow5: TcxDBEditorRow
              Properties.Caption = #1060#1072#1084#1080#1083#1080#1103' '#1048'.'#1054'.'
              Properties.DataBinding.FieldName = 'CREATED$BY$TITLE'
              Properties.Options.Editing = False
              ID = 6
              ParentID = -1
              Index = 6
              Version = 1
            end
            object GridPassportCategoryRow3: TcxCategoryRow
              Properties.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
              ID = 7
              ParentID = -1
              Index = 7
              Version = 1
            end
            object GridPassportDBEditorRow6: TcxDBEditorRow
              Properties.Caption = #1054#1087#1080#1089#1072#1085#1080#1077
              Properties.DataBinding.FieldName = 'STATE$TITLE'
              Properties.Options.Editing = False
              ID = 8
              ParentID = -1
              Index = 8
              Version = 1
            end
            object GridPassportDBEditorRow7: TcxDBEditorRow
              Properties.Caption = #1044#1072#1090#1072
              Properties.EditPropertiesClassName = 'TcxDateEditProperties'
              Properties.DataBinding.FieldName = 'STATE$DATE'
              Properties.Options.Editing = False
              ID = 9
              ParentID = -1
              Index = 9
              Version = 1
            end
            object GridPassportDBEditorRow8: TcxDBEditorRow
              Properties.Caption = #1060#1072#1084#1080#1083#1080#1103' '#1048'.'#1054
              Properties.DataBinding.FieldName = 'STATE$BY$TITLE'
              Properties.Options.Editing = False
              ID = 10
              ParentID = -1
              Index = 10
              Version = 1
            end
            object GridPassportCategoryRow4: TcxCategoryRow
              Properties.Caption = #1042#1086#1079#1074#1088#1072#1090' '#1072#1092#1092#1080#1085#1072#1078#1072
              Visible = False
              ID = 11
              ParentID = -1
              Index = 11
              Version = 1
            end
            object GridPassportDBEditorRow9: TcxDBEditorRow
              Properties.Caption = #1053#1072#1095#1072#1083#1086
              Properties.EditPropertiesClassName = 'TcxDateEditProperties'
              Properties.DataBinding.FieldName = 'AFFINAGE$PERIOD$BEGIN'
              Properties.Options.Editing = False
              Visible = False
              ID = 12
              ParentID = -1
              Index = 12
              Version = 1
            end
            object GridPassportAffinagePeriodEndEditor: TcxDBEditorRow
              Properties.Caption = #1050#1086#1085#1077#1094
              Properties.EditPropertiesClassName = 'TcxDateEditProperties'
              Properties.DataBinding.FieldName = 'AFFINAGE$PERIOD$END'
              Visible = False
              ID = 13
              ParentID = -1
              Index = 13
              Version = 1
            end
          end
          object DockControlPassport: TdxBarDockControl
            Left = 0
            Top = 0
            Width = 1089
            Height = 56
            Align = dalTop
            BarManager = BarManager
          end
          object Splitter: TcxSplitter
            Left = 200
            Top = 56
            Width = 8
            Height = 481
            HotZoneClassName = 'TcxMediaPlayer9Style'
            AutoSnap = True
            MinSize = 200
            ResizeUpdate = True
            Control = GridPassport
          end
        end
      end
    end
    object SheetGroupInvoice: TcxTabSheet
      Caption = #1053#1072#1082#1083#1072#1076#1085#1099#1077
      ImageIndex = 1
      object PagesInvoice: TcxPageControl
        Left = 0
        Top = 0
        Width = 1089
        Height = 537
        ActivePage = SheetInvoices
        Align = alClient
        HideTabs = True
        TabOrder = 0
        ClientRectBottom = 537
        ClientRectRight = 1089
        ClientRectTop = 0
        object SheetInvoices: TcxTabSheet
          Caption = #1053#1072#1082#1083#1072#1076#1085#1099#1077
          ImageIndex = 0
          object GridInvoices: TcxGrid
            Left = 0
            Top = 56
            Width = 1089
            Height = 481
            Align = alClient
            TabOrder = 0
            LookAndFeel.Kind = lfOffice11
            LookAndFeel.NativeStyle = False
            object GridInvoicesDBBandedTableView1: TcxGridDBBandedTableView
              OnDblClick = GridInvoicesDBBandedTableView1DblClick
              NavigatorButtons.ConfirmDelete = False
              DataController.DataSource = DataSourceInvoices
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.GroupByBox = False
              Bands = <
                item
                  Width = 211
                end
                item
                  Caption = #1057#1086#1079#1076#1072#1085#1080#1077
                  Width = 208
                end
                item
                  Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
                  Width = 269
                end>
              object GridInvoicesDBBandedTableView1DESIGNATION: TcxGridDBBandedColumn
                Caption = #8470
                DataBinding.FieldName = 'DESIGNATION'
                Options.Editing = False
                Options.Filtering = False
                Options.Grouping = False
                Width = 56
                Position.BandIndex = 0
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object GridInvoicesDBBandedTableView1CLASSTITLE: TcxGridDBBandedColumn
                Caption = #1042#1080#1076
                DataBinding.FieldName = 'CLASS$TITLE'
                Options.Editing = False
                Width = 61
                Position.BandIndex = 0
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object GridInvoicesDBBandedTableView1DATE: TcxGridDBBandedColumn
                Caption = #1044#1072#1090#1072
                DataBinding.FieldName = 'DATE$'
                Options.Filtering = False
                Options.Grouping = False
                Width = 94
                Position.BandIndex = 0
                Position.ColIndex = 2
                Position.RowIndex = 0
              end
              object GridInvoicesDBBandedTableView1CREATEDDATE: TcxGridDBBandedColumn
                Caption = #1044#1072#1090#1072
                DataBinding.FieldName = 'CREATED$DATE'
                Options.Editing = False
                Options.Filtering = False
                Options.Grouping = False
                Width = 99
                Position.BandIndex = 1
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object GridInvoicesDBBandedTableView1CREATEDBYTITLE: TcxGridDBBandedColumn
                Caption = #1060#1072#1084#1080#1083#1080#1103' '#1048'.'#1054'.'
                DataBinding.FieldName = 'CREATED$BY$TITLE'
                Options.Editing = False
                Width = 109
                Position.BandIndex = 1
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object GridInvoicesDBBandedTableView1STATETITLE: TcxGridDBBandedColumn
                Caption = #1054#1087#1080#1089#1072#1085#1080#1077
                DataBinding.FieldName = 'STATE$TITLE'
                Options.Editing = False
                Width = 82
                Position.BandIndex = 2
                Position.ColIndex = 0
                Position.RowIndex = 0
              end
              object GridInvoicesDBBandedTableView1STATEBYTITLE: TcxGridDBBandedColumn
                Caption = #1060#1072#1084#1080#1083#1080#1103' '#1048'.'#1054'.'
                DataBinding.FieldName = 'STATE$BY$TITLE'
                Options.Editing = False
                Width = 100
                Position.BandIndex = 2
                Position.ColIndex = 1
                Position.RowIndex = 0
              end
              object GridInvoicesDBBandedTableView1STATEDATE: TcxGridDBBandedColumn
                Caption = #1044#1072#1090#1072
                DataBinding.FieldName = 'STATE$DATE'
                Options.Editing = False
                Options.Filtering = False
                Options.Grouping = False
                Width = 87
                Position.BandIndex = 2
                Position.ColIndex = 2
                Position.RowIndex = 0
              end
            end
            object GridInvoicesLevel1: TcxGridLevel
              GridView = GridInvoicesDBBandedTableView1
            end
          end
          object DockControlInvoices: TdxBarDockControl
            Left = 0
            Top = 0
            Width = 1089
            Height = 56
            Align = dalTop
            BarManager = BarManager
          end
        end
        object SheetInvoice: TcxTabSheet
          Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
          ImageIndex = 1
          object DockControlInvoice: TdxBarDockControl
            Left = 0
            Top = 0
            Width = 1089
            Height = 56
            Align = dalTop
            BarManager = BarManager
          end
          object GridInvoice: TcxDBVerticalGrid
            Left = 0
            Top = 56
            Width = 217
            Height = 481
            Align = alLeft
            LookAndFeel.Kind = lfOffice11
            OptionsView.ScrollBars = ssVertical
            OptionsView.CategoryExplorerStyle = True
            OptionsView.PaintStyle = psDelphi
            OptionsView.RowHeaderWidth = 111
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            TabOrder = 2
            DataController.DataSource = DataSourceInvoices
            Version = 1
            object GridInvoiceDESIGNATION1: TcxDBEditorRow
              Properties.Caption = #1042#1080#1076
              Properties.DataBinding.FieldName = 'CLASS$TITLE'
              Properties.Options.Editing = False
              ID = 0
              ParentID = -1
              Index = 0
              Version = 1
            end
            object GridInvoiceCLASSTITLE1: TcxDBEditorRow
              Properties.Caption = #1054#1073#1086#1079#1085#1072#1095#1077#1085#1080#1077
              Properties.DataBinding.FieldName = 'DESIGNATION'
              ID = 1
              ParentID = -1
              Index = 1
              Version = 1
            end
            object GridInvoiceDATE1: TcxDBEditorRow
              Properties.Caption = #1055#1077#1088#1080#1086#1076
              Properties.DataBinding.FieldName = 'DATE$'
              ID = 2
              ParentID = -1
              Index = 2
              Version = 1
            end
            object GridInvoiceCategoryRow1: TcxCategoryRow
              Properties.Caption = #1057#1086#1079#1076#1072#1085#1080#1077
              ID = 3
              ParentID = -1
              Index = 3
              Version = 1
            end
            object GridInvoiceDBEditorRow1: TcxDBEditorRow
              Properties.Caption = #1057#1086#1079#1076#1072#1085#1072
              Properties.DataBinding.FieldName = 'CREATED$DATE'
              Properties.Options.Editing = False
              ID = 4
              ParentID = -1
              Index = 4
              Version = 1
            end
            object GridInvoiceDBEditorRow2: TcxDBEditorRow
              Properties.Caption = #1060#1072#1084#1080#1083#1080#1103' '#1048'.'#1054'.'
              Properties.DataBinding.FieldName = 'CREATED$BY$TITLE'
              Properties.Options.Editing = False
              ID = 5
              ParentID = -1
              Index = 5
              Version = 1
            end
            object GridInvoiceCategoryRow2: TcxCategoryRow
              Properties.Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077
              ID = 6
              ParentID = -1
              Index = 6
              Version = 1
            end
            object GridInvoiceDBEditorRow4: TcxDBEditorRow
              Properties.DataBinding.FieldName = 'STATE$DATE'
              Properties.Options.Editing = False
              ID = 7
              ParentID = -1
              Index = 7
              Version = 1
            end
            object GridInvoiceDBEditorRow5: TcxDBEditorRow
              Properties.Caption = #1060#1072#1084#1080#1083#1080#1103' '#1048'.'#1054'.'
              Properties.DataBinding.FieldName = 'STATE$BY$TITLE'
              Properties.Options.Editing = False
              ID = 8
              ParentID = -1
              Index = 8
              Version = 1
            end
            object GridInvoiceCategoryRow3: TcxCategoryRow
              Properties.Caption = #1055#1088#1086#1095#1077#1077
              ID = 9
              ParentID = -1
              Index = 9
              Version = 1
            end
            object GridInvoiceDBEditorRow6: TcxDBEditorRow
              Properties.Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
              Properties.DataBinding.FieldName = 'Department$TITLE'
              ID = 10
              ParentID = -1
              Index = 10
              Version = 1
            end
            object GridInvoiceDBEditorRow7: TcxDBEditorRow
              Properties.Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
              Properties.DataBinding.FieldName = 'Company$TITLE'
              ID = 11
              ParentID = -1
              Index = 11
              Version = 1
            end
          end
          object cxSplitter1: TcxSplitter
            Left = 217
            Top = 56
            Width = 8
            Height = 481
            HotZoneClassName = 'TcxMediaPlayer9Style'
            AutoSnap = True
            ResizeUpdate = True
            Control = GridInvoice
          end
          object Panel1: TPanel
            Left = 225
            Top = 56
            Width = 864
            Height = 481
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 3
            object GridInvoiceItems: TcxGrid
              Left = 0
              Top = 56
              Width = 864
              Height = 425
              Align = alClient
              TabOrder = 0
              LookAndFeel.Kind = lfOffice11
              object GridInvoiceItemsView: TcxGridDBBandedTableView
                NavigatorButtons.ConfirmDelete = False
                DataController.DataSource = DataSourceInvoiceItems
                DataController.Summary.DefaultGroupSummaryItems = <
                  item
                    Kind = skSum
                    Position = spFooter
                    Column = GridInvoiceItemsViewMATERIALWEIGHT
                  end>
                DataController.Summary.FooterSummaryItems = <
                  item
                    Kind = skSum
                    Column = GridInvoiceItemsViewMATERIALWEIGHT
                  end>
                DataController.Summary.SummaryGroups = <>
                NewItemRow.InfoText = #1053#1072#1078#1084#1080#1090#1077' '#1079#1076#1077#1089#1100' '#1076#1083#1103' '#1076#1086#1073#1072#1074#1083#1077#1085#1080#1103' '#1085#1086#1074#1086#1081' '#1089#1090#1088#1086#1082#1080
                OptionsData.Appending = True
                OptionsData.Deleting = False
                OptionsView.ShowEditButtons = gsebAlways
                OptionsView.Footer = True
                OptionsView.GroupFooters = gfAlwaysVisible
                OptionsView.GroupRowStyle = grsOffice11
                OptionsView.Indicator = True
                Bands = <
                  item
                    Width = 357
                  end
                  item
                    Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
                    Width = 424
                  end
                  item
                    Caption = #1042#1077#1089
                    Position.BandIndex = 1
                    Position.ColIndex = 1
                    Width = 128
                  end
                  item
                    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
                    Position.BandIndex = 1
                    Position.ColIndex = 2
                    Width = 129
                  end
                  item
                    Position.BandIndex = 1
                    Position.ColIndex = 0
                    Width = 167
                  end>
                object GridInvoiceItemsViewOPERATIONTITLE: TcxGridDBBandedColumn
                  Caption = #1054#1087#1077#1088#1072#1094#1080#1103
                  DataBinding.FieldName = 'OPERATION$TITLE'
                  Options.ShowEditButtons = isebAlways
                  Width = 141
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object GridInvoiceItemsViewPLACEOFSTORAGETITLE: TcxGridDBBandedColumn
                  Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
                  DataBinding.FieldName = 'PLACE$OF$STORAGE$TITLE'
                  PropertiesClassName = 'TcxExtLookupComboBoxProperties'
                  Properties.View = GridPlaceOfStorageView
                  Properties.KeyFieldNames = 'ID'
                  Properties.OnInitPopup = GridInvoiceItemsViewPLACEOFSTORAGETITLEPropertiesInitPopup
                  Options.ShowEditButtons = isebAlways
                  Width = 157
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object GridInvoiceItemsViewMATERIALTITLE: TcxGridDBBandedColumn
                  Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
                  DataBinding.FieldName = 'MATERIAL$TITLE'
                  Position.BandIndex = 4
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object GridInvoiceItemsViewMATERIALWEIGHT: TcxGridDBBandedColumn
                  Caption = #1047#1085#1072#1095#1077#1085#1080#1077
                  DataBinding.FieldName = 'MATERIAL$WEIGHT'
                  Options.Filtering = False
                  Options.Grouping = False
                  Width = 70
                  Position.BandIndex = 2
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object GridInvoiceItemsViewMATERAILQUANTITY: TcxGridDBBandedColumn
                  Caption = #1047#1085#1072#1095#1077#1085#1080#1077
                  DataBinding.FieldName = 'MATERAIL$QUANTITY'
                  Options.Filtering = False
                  Options.Grouping = False
                  Width = 63
                  Position.BandIndex = 3
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object GridInvoiceItemsViewMATERIALMEASUREWEIGHTTITLE: TcxGridDBBandedColumn
                  Caption = #1045#1076'. '#1080#1079#1084'.'
                  DataBinding.FieldName = 'MATERIAL$MEASURE$WEIGHT$TITLE'
                  Options.Editing = False
                  Options.Filtering = False
                  Options.ShowEditButtons = isebNever
                  Options.Grouping = False
                  Width = 55
                  Position.BandIndex = 2
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object GridInvoiceItemsViewMATERIALMEASUREQUANTITYTITLE: TcxGridDBBandedColumn
                  Caption = #1045#1076'. '#1080#1079#1084'.'
                  DataBinding.FieldName = 'MATERIAL$MEASURE$QUANTITY$TITLE'
                  Options.Filtering = False
                  Options.Grouping = False
                  Width = 66
                  Position.BandIndex = 3
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
              end
              object GridInvoiceItemsLevel1: TcxGridLevel
                GridView = GridInvoiceItemsView
              end
            end
            object DockControlInvoiceItems: TdxBarDockControl
              Left = 0
              Top = 0
              Width = 864
              Height = 56
              Align = dalTop
              BarManager = BarManager
            end
            object GridPlaceOfStorage: TcxGrid
              Left = 24
              Top = 184
              Width = 393
              Height = 113
              TabOrder = 2
              Visible = False
              LookAndFeel.Kind = lfOffice11
              object GridPlaceOfStorageView: TcxGridDBTableView
                NavigatorButtons.ConfirmDelete = False
                DataController.KeyFieldNames = 'ID'
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                OptionsData.Deleting = False
                OptionsData.Editing = False
                OptionsData.Inserting = False
                OptionsView.ScrollBars = ssVertical
                OptionsView.GridLines = glNone
                OptionsView.GroupByBox = False
                OptionsView.Header = False
                object GridPlaceOfStorageViewTITLE: TcxGridDBColumn
                  Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
                  DataBinding.FieldName = 'TITLE'
                  Options.Editing = False
                  Options.Filtering = False
                  Options.Grouping = False
                end
              end
              object GridPlaceOfStorageLevel1: TcxGridLevel
                GridView = GridPlaceOfStorageView
              end
            end
          end
        end
      end
    end
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    CanCustomize = False
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.DisabledImages = dm.ilButtons
    ImageOptions.DisabledLargeImages = DisabledLargeImages
    ImageOptions.LargeImages = EnabledLargeImages
    ImageOptions.LargeIcons = True
    ImageOptions.MakeDisabledImagesFaded = True
    ImageOptions.UseLargeImagesForLargeIcons = True
    LookAndFeel.Kind = lfOffice11
    PopupMenuLinks = <>
    Style = bmsOffice11
    UseSystemFont = True
    Left = 48
    Top = 544
    DockControlHeights = (
      0
      0
      0
      0)
    object BarManagerBar1: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      Caption = #1055#1072#1089#1087#1086#1088#1090#1072
      CaptionButtons = <>
      DockControl = DockControlPassports
      DockedDockControl = DockControlPassports
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 226
      FloatTop = 375
      FloatClientWidth = 109
      FloatClientHeight = 202
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 77
          Visible = True
          ItemName = 'DateComboPassportPeriodBegin'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 79
          Visible = True
          ItemName = 'DateComboPassportPeriodEnd'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end>
      OldName = #1055#1072#1089#1087#1086#1088#1090#1072
      OneOnRow = True
      Row = 0
      ShowMark = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object BarManagerBar2: TdxBar
      Caption = #1055#1072#1089#1087#1086#1088#1090
      CaptionButtons = <>
      DockControl = DockControlPassport
      DockedDockControl = DockControlPassport
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 340
      FloatTop = 261
      FloatClientWidth = 23
      FloatClientHeight = 22
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonGroup'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end>
      OldName = #1055#1072#1089#1087#1086#1088#1090
      OneOnRow = True
      Row = 0
      ShowMark = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object BarManagerBar3: TdxBar
      Caption = #1053#1072#1082#1083#1072#1076#1085#1099#1077
      CaptionButtons = <>
      DockControl = DockControlInvoices
      DockedDockControl = DockControlInvoices
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 340
      FloatTop = 261
      FloatClientWidth = 39
      FloatClientHeight = 38
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 76
          Visible = True
          ItemName = 'DateComboInvoiceBegin'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 79
          Visible = True
          ItemName = 'DateComboInvoiceEnd'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end>
      MultiLine = True
      OldName = #1053#1072#1082#1083#1072#1076#1085#1099#1077
      OneOnRow = True
      Row = 0
      ShowMark = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarManagerBar4: TdxBar
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      CaptionButtons = <>
      DockControl = DockControlInvoice
      DockedDockControl = DockControlInvoice
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 340
      FloatTop = 261
      FloatClientWidth = 39
      FloatClientHeight = 38
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton14'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end>
      MultiLine = True
      OldName = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      OneOnRow = True
      Row = 0
      ShowMark = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarManagerBar5: TdxBar
      Caption = #1069#1083#1077#1084#1077#1085#1090#1099' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      CaptionButtons = <>
      DockControl = DockControlInvoiceItems
      DockedDockControl = DockControlInvoiceItems
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 340
      FloatTop = 261
      FloatClientWidth = 39
      FloatClientHeight = 38
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end>
      OldName = #1069#1083#1077#1084#1077#1085#1090#1099' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      OneOnRow = True
      Row = 0
      ShowMark = False
      SizeGrip = False
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = True
    end
    object BarButtonAppend: TdxBarButton
      Action = ActionPassportAppend
      Category = 0
      PaintStyle = psCaptionGlyph
    end
    object dxBarButton2: TdxBarButton
      Action = ActionPassportDelete
      Category = 0
      PaintStyle = psCaptionGlyph
    end
    object dxBarButton1: TdxBarButton
      Action = ActionPassportView
      Category = 0
      PaintStyle = psCaptionGlyph
    end
    object dxBarButton3: TdxBarButton
      Action = ActionPassportOpen
      Category = 0
      PaintStyle = psCaptionGlyph
    end
    object dxBarButton4: TdxBarButton
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Category = 0
      Visible = ivAlways
      ImageIndex = 4
      PaintStyle = psCaptionGlyph
    end
    object dxBarButton5: TdxBarButton
      Caption = '-'
      Category = 0
      Hint = '-'
      Visible = ivAlways
      ButtonStyle = bsDropDown
    end
    object dxBarButton6: TdxBarButton
      Action = ActionPassportExit
      Align = iaRight
      Category = 0
    end
    object dxBarStatic1: TdxBarStatic
      Caption = 'c '
      Category = 0
      Hint = 'c '
      Visible = ivAlways
    end
    object DateComboPassportPeriodBegin: TdxBarDateCombo
      Caption = #1053#1072#1095#1072#1083#1086
      Category = 0
      Enabled = False
      Hint = #1053#1072#1095#1072#1083#1086
      Visible = ivAlways
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDD00000000000DDDDD0FFFFFFFFF0D00000F0000000F0D0FFF0FFFFFFF
        FF0D0F000FFF11FFFF0D0FFF0FFF11FFFF0D0FF10FFFF11FFF0D0FF10FFFFF11
        FF0D0FF10FF11111FF0D0FF10FFFFFFFFF0D0FF104444444440D0FFF04444444
        440D044400000000000D04444444440DDDDD00000000000DDDDD}
      ShowCaption = True
      Width = 100
      Text = '05.12.2007'
      DateOnStart = bdsCustom
      ShowDayText = False
    end
    object dxBarStatic2: TdxBarStatic
      Caption = '...'
      Category = 0
      Visible = ivAlways
    end
    object DateComboPassportPeriodEnd: TdxBarDateCombo
      Caption = #1050#1086#1085#1077#1094
      Category = 0
      Hint = #1050#1086#1085#1077#1094
      Visible = ivAlways
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDD00000000000DDDDD0FFFFFFFFF0D00000F0000000F0D0FFF0FFFFFFF
        FF0D0F000FFF11FFFF0D0FFF0FFF11FFFF0D0FF10FFFF11FFF0D0FF10FFFFF11
        FF0D0FF10FF11111FF0D0FF10FFFFFFFFF0D0FF104444444440D0FFF04444444
        440D044400000000000D04444444440DDDDD00000000000DDDDD}
      ShowCaption = True
      Width = 100
      Text = '05.12.2007'
      DateOnStart = bdsCustom
      ShowDayText = False
    end
    object dxBarStatic3: TdxBarStatic
      Caption = #1055#1077#1088#1080#1086#1076
      Category = 0
      Hint = #1055#1077#1088#1080#1086#1076
      Visible = ivAlways
      ImageIndex = 5
    end
    object dxBarButton7: TdxBarButton
      Action = ActionPassportExit
      Align = iaRight
      Category = 0
    end
    object ButtonPassportOpen: TdxBarButton
      Action = ActionPassportOpen
      Category = 0
      PaintStyle = psCaptionGlyph
    end
    object dxBarButton9: TdxBarButton
      Action = ActionPassportRefresh
      Category = 0
      PaintStyle = psCaptionGlyph
    end
    object ButtonPassportClose: TdxBarButton
      Action = ActionPassportClose
      Category = 0
      PaintStyle = psCaptionGlyph
    end
    object dxBarButton11: TdxBarButton
      Action = ActionPassportExit
      Align = iaRight
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = ActionPassportAppend
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = ActionPassportDelete
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = ActionPassportView
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = ActionPassportOpen
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = ActionPassportClose
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = ActionPassportRefresh
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = ActionPassportExit
      Align = iaRight
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Caption = #1055#1077#1088#1080#1086#1076
      Category = 0
      Hint = #1055#1077#1088#1080#1086#1076' '#1087#1072#1089#1087#1086#1088#1090#1072
      Visible = ivAlways
      LargeImageIndex = 5
      AutoGrayScale = False
      SyncImageIndex = False
      ImageIndex = 5
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Caption = #1055#1077#1088#1080#1086#1076
      Category = 0
      Hint = #1055#1077#1088#1080#1086#1076
      Visible = ivAlways
      LargeImageIndex = 5
      AutoGrayScale = False
    end
    object DateComboInvoiceBegin: TdxBarDateCombo
      Caption = #1053#1072#1095#1072#1083#1086
      Category = 0
      Hint = #1053#1072#1095#1072#1083#1086
      Visible = ivAlways
      OnChange = DateComboInvoiceBeginChange
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDD00000000000DDDDD0FFFFFFFFF0D00000F0000000F0D0FFF0FFFFFFF
        FF0D0F000FFF11FFFF0D0FFF0FFF11FFFF0D0FF10FFFF11FFF0D0FF10FFFFF11
        FF0D0FF10FF11111FF0D0FF10FFFFFFFFF0D0FF104444444440D0FFF04444444
        440D044400000000000D04444444440DDDDD00000000000DDDDD}
      ShowCaption = True
      Width = 100
      ShowDayText = False
    end
    object DateComboInvoiceEnd: TdxBarDateCombo
      Caption = #1050#1086#1085#1077#1094
      Category = 0
      Hint = #1050#1086#1085#1077#1094
      Visible = ivAlways
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
        DDDDDDDD00000000000DDDDD0FFFFFFFFF0D00000F0000000F0D0FFF0FFFFFFF
        FF0D0F000FFF11FFFF0D0FFF0FFF11FFFF0D0FF10FFFF11FFF0D0FF10FFFFF11
        FF0D0FF10FF11111FF0D0FF10FFFFFFFFF0D0FF104444444440D0FFF04444444
        440D044400000000000D04444444440DDDDD00000000000DDDDD}
      ShowCaption = True
      Width = 100
      ShowDayText = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Tag = 1
      Action = ActionInvoiceAppend
      Category = 0
      ButtonStyle = bsDropDown
      AutoGrayScale = False
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = ActionInvoiceDelete
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = ActionInvoiceView
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = ActionInvoiceOpen
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Action = ActionInvoiceClose
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = ActionInvoiceExit
      Align = iaRight
      Category = 0
      AutoGrayScale = False
    end
    object dxBarButton8: TdxBarButton
      Action = ActionInvoiceAppendIn
      Category = 0
    end
    object dxBarButton10: TdxBarButton
      Action = ActionInvoiceAppendOut
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = ActionInvoiceItemAdd
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = ActionInvoiceItemDelete
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = ActionInvoicePrint
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = ActionInvoicePrint
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = ActionInvoiceItemApply
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = ActionInvoiceItemCancel
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = ActionInvoiceAppendIn
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = ActionInvoiceAppendOut
      Caption = #1042#1099#1076#1072#1095#1072
      Category = 0
      AutoGrayScale = False
    end
    object dxBarCombo1: TdxBarCombo
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
      ItemIndex = -1
    end
    object ButtonGroup: TdxBarLargeButton
      Action = ActionPassportDoGrouping
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Caption = #1053#1072#1089#1090#1088#1086#1080#1090#1100
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 17
      AutoGrayScale = False
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = ActionExit
      Align = iaRight
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = ActionExit
      Align = iaRight
      Category = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = ActionPassportPrint
      Category = 0
      AutoGrayScale = False
    end
    object dxBarButton12: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Caption = #1050#1085#1086#1087#1082#1072
      Category = 0
      Hint = #1050#1085#1086#1087#1082#1072
      Visible = ivAlways
      LargeImageIndex = 10
      AutoGrayScale = False
      SyncImageIndex = False
      ImageIndex = 10
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Caption = 'DebugButton'
      Category = 0
      Hint = 'DebugButton'
      Visible = ivAlways
      LargeImageIndex = 17
    end
    object dxBarButton13: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton30: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivNever
      ButtonStyle = bsChecked
      LargeImageIndex = 17
      UnclickAfterDoing = False
      OnClick = dxBarLargeButton30Click
      AutoGrayScale = False
    end
  end
  object Actions: TActionList
    Images = EnabledLargeImages
    Left = 16
    Top = 544
    object ActionPassportAppend: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1072#1089#1087#1086#1088#1090' '#1085#1072' '#1091#1082#1072#1079#1072#1085#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
      ImageIndex = 0
      OnExecute = ActionPassportAppendExecute
      OnUpdate = ActionPassportAppendUpdate
    end
    object ActionPassportDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1087#1072#1089#1087#1086#1088#1090
      ImageIndex = 1
      OnExecute = ActionPassportDeleteExecute
      OnUpdate = ActionPassportDeleteUpdate
    end
    object ActionPassportOpen: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Hint = #1054#1090#1082#1088#1099#1090#1100' '#1087#1072#1089#1087#1086#1088#1090
      ImageIndex = 4
      OnExecute = ActionPassportOpenExecute
      OnUpdate = ActionPassportOpenUpdate
    end
    object ActionPassportClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1087#1072#1089#1087#1086#1088#1090
      ImageIndex = 3
      OnExecute = ActionPassportCloseExecute
      OnUpdate = ActionPassportCloseUpdate
    end
    object ActionPassportView: TAction
      Caption = #1057#1084#1086#1090#1088#1077#1090#1100
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088#1077#1090#1100' '#1087#1072#1089#1087#1086#1088#1090
      ImageIndex = 2
      OnExecute = ActionPassportViewExecute
      OnUpdate = ActionPassportViewUpdate
    end
    object ActionPassportExit: TAction
      Caption = #1047#1072#1074#1077#1088#1096#1080#1090#1100
      Hint = #1047#1072#1074#1077#1088#1096#1080#1090#1100' '#1088#1072#1073#1086#1090#1091' '#1089' '#1087#1072#1089#1087#1086#1088#1090#1086#1084
      ImageIndex = 6
      OnExecute = ActionPassportExitExecute
      OnUpdate = ActionPassportExitUpdate
    end
    object ActionPassportRefresh: TAction
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100' '#1087#1072#1089#1087#1086#1088#1090
      ImageIndex = 7
      OnExecute = ActionPassportRefreshExecute
      OnUpdate = ActionPassportRefreshUpdate
    end
    object ActionInvoiceAppendIn: TAction
      Tag = 2
      Caption = #1055#1088#1080#1077#1084' '
      ImageIndex = 0
      OnExecute = ActionInvoiceAppendExecute
      OnUpdate = ActionInvoiceAppendUpdate
    end
    object ActionInvoiceAppendOut: TAction
      Tag = 1
      Caption = #1057#1076#1072#1095#1072
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 0
      OnExecute = ActionInvoiceAppendExecute
      OnUpdate = ActionInvoiceAppendUpdate
    end
    object ActionInvoiceDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 1
      OnExecute = ActionInvoiceDeleteExecute
      OnUpdate = ActionInvoiceDeleteUpdate
    end
    object ActionInvoiceView: TAction
      Caption = #1057#1084#1086#1090#1088#1077#1090#1100
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088#1077#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 2
      OnExecute = ActionInvoiceViewExecute
      OnUpdate = ActionInvoiceViewUpdate
    end
    object ActionInvoiceOpen: TAction
      Caption = #1054#1090#1082#1088#1099#1090#1100
      Hint = #1054#1090#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 4
      OnExecute = ActionInvoiceOpenExecute
      OnUpdate = ActionInvoiceOpenUpdate
    end
    object ActionInvoiceClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      Hint = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 3
      OnExecute = ActionInvoiceCloseExecute
      OnUpdate = ActionInvoiceCloseUpdate
    end
    object ActionInvoiceExit: TAction
      Caption = #1047#1072#1074#1077#1088#1096#1080#1090#1100
      Hint = #1047#1072#1074#1077#1088#1096#1080#1090#1100' '#1088#1072#1073#1086#1090#1091' '#1089' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 6
      OnExecute = ActionInvoiceExitExecute
      OnUpdate = ActionInvoiceExitUpdate
    end
    object ActionInvoiceAppend: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 0
      OnExecute = ActionInvoiceAppendExecute
      OnUpdate = ActionInvoiceAppendUpdate
    end
    object ActionInvoiceItemAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 0
      OnExecute = ActionInvoiceItemAddExecute
      OnUpdate = ActionInvoiceItemAddUpdate
    end
    object ActionInvoiceItemDelete: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 1
      OnExecute = ActionInvoiceItemDeleteExecute
      OnUpdate = ActionInvoiceItemDeleteUpdate
    end
    object ActionInvoicePrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 11
      OnExecute = ActionInvoicePrintExecute
      OnUpdate = ActionInvoicePrintUpdate
    end
    object ActionInvoiceItemApply: TAction
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      ImageIndex = 13
      OnExecute = ActionInvoiceItemApplyExecute
      OnUpdate = ActionInvoiceItemApplyUpdate
    end
    object ActionInvoiceItemCancel: TAction
      Caption = #1054#1090#1084#1077#1085#1080#1090#1100
      ImageIndex = 12
      OnExecute = ActionInvoiceItemCancelExecute
      OnUpdate = ActionInvoiceItemCancelUpdate
    end
    object ActionPassportDoGrouping: TAction
      Caption = #1043#1088#1091#1087#1087#1099
      ImageIndex = 16
      OnExecute = ActionPassportDoGroupingExecute
      OnUpdate = ActionPassportDoGroupingUpdate
    end
    object ActionExit: TAction
      Caption = #1047#1072#1074#1077#1088#1096#1080#1090#1100
      ImageIndex = 6
      OnExecute = ActionExitExecute
    end
    object ActionPassportPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 11
      OnExecute = ActionPassportPrintExecute
    end
  end
  object DataSourcePassports: TDataSource
    DataSet = DataSetPassports
    Left = 56
    Top = 312
  end
  object DataSetPassports: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure AFFINAGE$PASSPORT_U(:ID, :affinage$period$end)')
    DeleteSQL.Strings = (
      'execute procedure AFFINAGE$PASSPORT_D(:ID)')
    InsertSQL.Strings = (
      
        'execute procedure AFFINAGE$PASSPORT_I(:ID, :PERIOD$BEGIN, :PERIO' +
        'D$END, :Material$ID)')
    RefreshSQL.Strings = (
      'select '
      '  ID,'
      '  Designation,'
      '  Period$Begin,'
      '  Period$End,'
      '  Created$Date,'
      '  Created$By,'
      '  Created$By$Title,'
      '  State,'
      '  State$Title,'
      '  State$By,'
      '  State$By$Title,'
      '  State$Date,'
      '  Affinage$Period$Begin,'
      '  Affinage$Period$End,'
      '  Material$ID'
      'from'
      '  AFFINAGE$PASSPORT_R(:ID)')
    SelectSQL.Strings = (
      'select '
      '  ID,'
      '  Designation,'
      '  Period$Begin,'
      '  Period$End,'
      '  Created$Date,'
      '  Created$By,'
      '  Created$By$Title,'
      '  State,'
      '  State$Title,'
      '  State$By,'
      '  State$By$Title,'
      '  State$Date,'
      '  Affinage$Period$Begin,'
      '  Affinage$Period$End,'
      '  Material$ID'
      'from'
      '  AFFINAGE$PASSPORT_S')
    BeforeEdit = DataSetPassportsBeforeEdit
    BeforePost = OnDataSetValidate
    Transaction = Transaction
    Database = dm.db
    Left = 24
    Top = 312
    oStartTransaction = False
    oFetchAll = True
    object DataSetPassportsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetPassportsDESIGNATION: TFIBIntegerField
      FieldName = 'DESIGNATION'
    end
    object DataSetPassportsPERIODBEGIN: TFIBDateTimeField
      FieldName = 'PERIOD$BEGIN'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object DataSetPassportsPERIODEND: TFIBDateTimeField
      FieldName = 'PERIOD$END'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object DataSetPassportsCREATEDDATE: TFIBDateTimeField
      FieldName = 'CREATED$DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object DataSetPassportsCREATEDBY: TFIBIntegerField
      FieldName = 'CREATED$BY'
    end
    object DataSetPassportsCREATEDBYTITLE: TFIBStringField
      FieldName = 'CREATED$BY$TITLE'
      Size = 32
      EmptyStrToNull = True
    end
    object DataSetPassportsSTATE: TFIBIntegerField
      FieldName = 'STATE'
    end
    object DataSetPassportsSTATETITLE: TFIBStringField
      FieldName = 'STATE$TITLE'
      Size = 8
      EmptyStrToNull = True
    end
    object DataSetPassportsSTATEBY: TFIBIntegerField
      FieldName = 'STATE$BY'
    end
    object DataSetPassportsSTATEBYTITLE: TFIBStringField
      FieldName = 'STATE$BY$TITLE'
      Size = 32
      EmptyStrToNull = True
    end
    object DataSetPassportsSTATEDATE: TFIBDateTimeField
      FieldName = 'STATE$DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object DataSetPassportsAFFINAGEPERIODBEGIN: TFIBDateTimeField
      FieldName = 'AFFINAGE$PERIOD$BEGIN'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object DataSetPassportsAFFINAGEPERIODEND: TFIBDateTimeField
      FieldName = 'AFFINAGE$PERIOD$END'
      OnChange = DataSetPassportsAFFINAGEPERIODENDChange
      DisplayFormat = 'dd.mm.yyyy'
    end
    object DataSetPassportsMATERIALID: TFIBIntegerField
      FieldName = 'MATERIAL$ID'
    end
  end
  object HintStyleController: TcxHintStyleController
    Global = False
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'MS Sans Serif'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'MS Sans Serif'
    HintStyle.Font.Style = []
    HintStyle.IconSize = cxisSmall
    HintStyle.IconType = cxhiInformation
    HintStyle.Rounded = True
    Left = 80
    Top = 544
  end
  object DataSetPassportItems: TpFIBDataSet
    UpdateSQL.Strings = (
      
        'execute procedure AFFINAGE$PASSPORT$ITEM_U(:ID, :WEIGHT$REMAINDE' +
        'R$OUT)')
    RefreshSQL.Strings = (
      'select'
      '  ID,'
      '  Passport$Id,'
      '  Operation$Id,'
      '  Operation$Title,'
      '  Place$Of$Storage$Id,'
      '  Place$Of$Storage$Title,'
      '  Weight,'
      '  Weight$Norm,'
      '  Weight$Out,'
      '  Weight$In,'
      '  Weight$Remainder$Current,'
      '  Weight$Remainder$Out,'
      '  Weight$Remainder$In'
      'from'
      '  AFFINAGE$PASSPORT$ITEM_R(:ID)')
    SelectSQL.Strings = (
      'select'
      '  ID,'
      '  Passport$Id,'
      '  Operation$Id,'
      '  Operation$Title,'
      '  Place$Of$Storage$Id,'
      '  Place$Of$Storage$Title,'
      '  Weight,'
      '  Weight$Norm,'
      '  Weight$Out,'
      '  Weight$In,'
      '--  Weight$Done,'
      '  Weight$Remainder$Current,'
      '  Weight$Remainder$Out,'
      '  Weight$Remainder$In,'
      '  cast(0 as NUMERIC(10,3)) Weight$Remainder,'
      '  cast(0 as integer) GroupID'
      'from'
      '  AFFINAGE$PASSPORT$ITEM_S(:PassportID)')
    AfterPost = DataSetCommitRetaining
    BeforeOpen = DataSetPassportItemsBeforeOpen
    BeforePost = OnDataSetValidate
    OnCalcFields = DataSetPassportItemsCalcFields
    Transaction = Transaction
    Database = dm.db
    Left = 24
    Top = 344
    oStartTransaction = False
    oFetchAll = True
    object DataSetPassportItemsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetPassportItemsPASSPORTID: TFIBIntegerField
      FieldName = 'PASSPORT$ID'
    end
    object DataSetPassportItemsOPERATIONID: TFIBIntegerField
      FieldName = 'OPERATION$ID'
    end
    object DataSetPassportItemsOPERATIONTITLE: TFIBStringField
      FieldName = 'OPERATION$TITLE'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetPassportItemsPLACEOFSTORAGEID: TFIBIntegerField
      FieldName = 'PLACE$OF$STORAGE$ID'
    end
    object DataSetPassportItemsPLACEOFSTORAGETITLE: TFIBStringField
      FieldName = 'PLACE$OF$STORAGE$TITLE'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetPassportItemsWEIGHT: TFIBBCDField
      FieldName = 'WEIGHT'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object DataSetPassportItemsWEIGHTOUT: TFIBBCDField
      FieldName = 'WEIGHT$OUT'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object DataSetPassportItemsWEIGHTIN: TFIBBCDField
      FieldName = 'WEIGHT$IN'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object DataSetPassportItemsWEIGHTREMAINDERCURRENT: TFIBBCDField
      FieldName = 'WEIGHT$REMAINDER$CURRENT'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object DataSetPassportItemsWEIGHTREMAINDEROUT: TFIBBCDField
      FieldName = 'WEIGHT$REMAINDER$OUT'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object DataSetPassportItemsWEIGHTREMAINDERIN: TFIBBCDField
      FieldName = 'WEIGHT$REMAINDER$IN'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object DataSetPassportItemsWEIGHTREMAINDER: TFIBBCDField
      FieldName = 'WEIGHT$REMAINDER'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object DataSetPassportItemsWEIGHTNORM: TFIBBCDField
      FieldName = 'WEIGHT$NORM'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object DataSetPassportItemsGROUPID: TFIBIntegerField
      FieldName = 'GROUPID'
    end
  end
  object TmpProcedure: TpFIBStoredProc
    Transaction = TmpTransaction
    Database = dm.db
    Left = 24
    Top = 280
  end
  object DataSourcePassportItems: TDataSource
    DataSet = PassportItems
    Left = 448
    Top = 392
  end
  object EnabledLargeImages: TImageList
    Height = 32
    Width = 32
    Left = 112
    Top = 544
    Bitmap = {
      494C010113001800040020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000080000000A000000001002000000000000040
      0100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FAEEDBFFF2D4
      A3FFFCFCFAFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FBF2E4FFFBF2
      E2FFFBF2E2FFFBF2E2FFFBF2E2FFFBF2E2FFFBF2E2FFFBF2E2FFFBF2E2FFFBF2
      E2FFFBF2E2FFFBF3E6FF00000000000000000000000000000000F7F7F8FFE4E4
      F3FFE2E2F2FFE2E2F2FFE2E2F2FFE2E2F2FFE2E2F2FFE2E2F2FFE2E2F2FFE2E2
      F2FFE2E2F2FFE2E2F2FFF3F3F9FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFEFEFFEDDFC9FFE0981CFFE397
      16FFE6AF50FFEFEAE1FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F3F3F3FF74A984FF5D926DFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFDE90
      08FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE90
      08FFDE9008FFDE9008FF00000000000000000000000000000000B0B0BEFF0611
      9DFF06119DFF06119DFF06119DFF06119DFF06119DFF06119DFF06119DFF0611
      9DFF06119DFF06119DFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EDDFC9FFE2A030FFF0AD3CFFFABF
      5CFFE59D22FFE6AF50FFFCFCFAFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000090BA9DFF128637FF278545FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9E1
      9DFFFFF2BDFFFFF2BDFFFFF2BDFFFFF2BDFFFFF2BDFFFFF2BDFFFFF2BDFFFFF2
      BDFFF9E19DFFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF2D76F3FF3282F6FF388EF8FF3B96FAFF3C96FAFF3890F8FF3384F6FF2E79
      F4FF2869F1FF1840CEFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FEFEFEFFEDDFC9FFE0981CFFF0AD3CFFFFC76AFFFFC7
      6AFFFABF5CFFE39716FFF2D4A3FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFCFFB1CEBAFF1F9044FF399D58FF309650FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9DC
      93FFFFECB0FFFFECB0FFFFECB0FFFFECB0FFFFECB0FFFFECB0FFFFECB0FFFFEC
      B0FFF9DC93FFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF2B6FF2FF2F79F4FF3384F6FF3689F7FF3689F7FF3485F6FF307BF4FF2C72
      F2FF2663EFFF163CCDFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EDDFC9FFE2A030FFF0AD3CFFFDC465FFFFC76AFFFFC7
      6AFFF0AD3CFFE0981CFFFAEEDBFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C7DCCDFF279148FF3B9D5AFF80CA98FF309650FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9D2
      81FFFFE19CFFFFE19CFFFFE19CFFFFE19CFFFFE19CFFFFE19CFFFFE19CFFFFE1
      9CFFF9D281FFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF2664EFFF296DF1FF2D75F3FF2F79F4FF2F79F4FF2E76F3FF2A6EF2FF2766
      F0FF225AEDFF1437CCFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FEFEFEFFEDDFC9FFE0981CFFF0AD3CFFFFC76AFFFFC76AFFFDC465FFF0AD
      3CFFE2A030FFEDDFC9FF00000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DCDF
      DDFF229045FF4BA667FFA5DCB6FF94D4A8FF309650FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9CA
      70FFFFD687FFFFD687FFFFD687FFFFD687FFFFD687FFFFD687FFFFD687FFFFD6
      87FFF9CA70FFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF225AEDFF2562EFFF2868F0FF296BF1FF296CF1FF2869F1FF2563EFFF225C
      EEFF1E51EBFF1131CBFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EDDFC9FFE2A030FFF0AD3CFFFDC465FFFFC76AFFFFC76AFFF0AD3CFFE098
      1CFFEDDFC9FFFEFEFEFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E3E6E3FF4E9B
      66FF44A161FFA2D7B2FFB1F8CCFF98D4ABFF29914AFF309650FF309650FF3096
      50FF309650FF309650FF309650FF309650FF309650FF309650FF309650FF2887
      46FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9BF
      5CFFFFC96EFFFFC96EFFFFC96EFFFFC96EFFFFC96EFFFFC96EFFFFC96EFFFFC9
      6EFFF9BF5CFFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF1C4CEAFF1F53ECFF2158EDFF225BEEFF225BEEFF2159EDFF1F54ECFF1C4E
      EBFF1844E9FF0E29C9FF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFEFFEDDF
      C9FFE0981CFFF0AD3CFFFFC76AFFFFC76AFFFDC465FFF0AD3CFFE2A030FFEDDF
      C9FF000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F5F6F5FF4AA366FF3C9C
      5AFFACDABBFF7EF3ABFF96F5BBFFD8F4E1FF9BD4ADFF99D4ACFF96D4A9FF94D4
      A8FF90D4A6FF8ED4A5FF8BD4A2FF89D4A1FF86D49FFF84D49DFF6CC589FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9BD
      58FFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC7
      6AFFF9BD58FFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF1741E8FF1946E9FF1B4BEAFF1D4EEBFF1D4EEBFF1B4CEAFF1A47E9FF1842
      E9FF143BE7FF0E27C9FF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EDDFC9FFE2A0
      30FFF0AD3CFFFDC465FFFFC76AFFFFC76AFFF0AD3CFFE0981CFFEDDFC9FFFEFE
      FEFF000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F5F6F5FF80B791FF349854FFA3D0
      B1FFA5F6C5FF4EEE8CFF9CF6BFFFF2FFF6FFEEFFF3FFEBFFF1FFE6FFEEFFE3FF
      ECFFDEFFE9FFDBFFE6FFD7FFE4FFD3FFE1FFCFFFDEFFCCFFDCFFA6E9BCFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9BD
      58FFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC7
      6AFFF9BD58FFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF163EE8FF143BE7FF153CE7FF153DE8FF153DE8FF153CE7FF143BE7FF153D
      E8FF1D4EEBFF1943CEFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFEFEFFEDDFC9FFE0981CFFF0AD
      3CFFFFC76AFFFFC76AFFFDC465FFF0AD3CFFE2A030FFEDDFC9FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FCFCFCFF7CBC90FF2E954EFF6DB583FFC6F9
      DAFF50EF8DFF4EEE8CFF8DF4B5FFCDFADEFFCAFADCFFC7FADAFFC2FAD7FFBFFA
      D5FFBBFAD2FFB8FAD0FFB4FACEFFB1FACBFFB8FBD0FFD0FEDFFFADE9C0FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFEAA4
      2CFFEDA935FFEDA935FFEDA935FFEDA935FFEDA935FFEDA935FFEDA935FFEDA9
      35FFEAA42CFFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF0F29BCFF0C21BAFF0A1DBAFF091CBAFF091CBAFF0A1DBAFF0B20BAFF0F27
      BCFF1538BFFF122EB0FF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EDDFC9FFE2A030FFF0AD3CFFFDC4
      65FFFFC76AFFFFC76AFFF0AD3CFFE0981CFFEDDFC9FFFEFEFEFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B5CDBCFF279248FF5EAD77FFDAFBE7FF72F3
      A3FF56EF91FF53EF90FF58EF92FF5DF096FF59EF94FF57EF92FF54EF90FF51EE
      8EFF4EEE8CFF4BED8AFF48ED89FF46EC87FF6AF19EFFCDFDDEFFB3E9C4FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFDE90
      08FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE90
      08FFDE9008FFDE9008FF00000000000000000000000000000000E7E7E9FF0611
      9DFF06119DFF06119DFF06119DFF06119DFF06119DFF06119DFF06119DFF0611
      9DFF06119DFF06119DFF06119DFF000000000000000000000000000000000000
      00000000000000000000ECECEDFFBEC5CFFFFCFCFCFF00000000000000000000
      00000000000000000000FEFEFEFFEDDFC9FFE0981CFFF0AD3CFFFFC76AFFFFC7
      6AFFFDC465FFF0AD3CFFE2A030FFEDDFC9FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000AAC1B1FF248F46FF5DAD76FFEEFDF4FF78F4A7FF60F1
      98FF5DF196FF5BF094FF57F092FF55F091FF52EF8FFF50EF8DFF4CEE8BFF4AEE
      89FF47ED87FF44ED86FF41EC84FF3FEC82FF66F09CFFD4FDE2FFBAE9C9FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000ECECECFF606060FF606060FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000606060FF606060FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C2C9D2FF2270D1FF9CBBE1FFFCFCFCFF000000000000
      00000000000000000000EDDFC9FFE2A030FFF0AD3CFFFDC465FFFFC76AFFFFC7
      6AFFF0AD3CFFE0981CFFEDDFC9FFFEFEFEFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DCE3DEFF289249FF4CA468FFF5FEF8FFA3F8C3FF68F29DFF66F2
      9CFF63F19AFF60F198FF5DF196FF5BF094FF58F092FF55EF91FF52EF8FFF50EE
      8DFF4DEE8BFF4AEE8AFF47ED87FF45ED86FF6CF1A0FFDBFDE7FFBFE9CCFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000ECECECFF606060FF606060FFB4B4B4FFB4B4B4FFB4B4
      B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4
      B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4B4FF606060FF606060FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000AEB7C3FF1274D4FF126ED2FF92A7C1FF000000000000
      0000FEFEFEFFEDDFC9FFE0981CFFF0AD3CFFFFC76AFFFFC76AFFFDC465FFF0AD
      3CFFE2A030FFEDDFC9FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D7E1DAFF389A57FF3A9B58FFF9FDFBFFADF9C9FF75F4A5FF70F4A2FF6DF4
      A1FF6AF39EFF68F39DFF64F29BFF62F299FF5FF197FF5DF196FF59F094FF57F0
      92FF54EF90FF52EF8EFF4EEE8CFF4CEE8BFF74F2A5FFE3FDECFFC5E9D1FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F2F2F2FF606060FF606060FF606060FF606060FF6060
      60FF606060FF606060FF606060FF606060FF606060FF606060FF606060FF6060
      60FF606060FF606060FF606060FF606060FF606060FF606060FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000AAB4C0FF1C84DCFF34A9ECFF1B72D2FFB9C3D1FF0000
      0000E8DCCBFF85666BFFDBA148FFFDC668FFFFC76AFFFFC76AFFF0AD3CFFE098
      1CFFEDDFC9FFFEFEFEFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004BA368FF389A57FFDEEEE3FFD2FCE2FF7FF6ACFF79F5A8FF76F5A6FF73F4
      A4FF70F4A2FF6EF3A1FF6AF39FFF68F29DFF65F29BFF62F199FF5FF197FF5DF1
      96FF5AF094FF57F092FF54EF90FF52EF8FFF7AF2A8FFE9FDF1FFCBE9D5FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BFBFBFFF606060FFDADADAFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080ADE4FF2C97E2FF56D8FFFF3AB1EFFF2373D0FFC1BC
      B1FF493D82FF1433CAFF3C3B9AFFCFA97EFFFDC669FFF0AD3CFFE2A030FFEDDF
      C9FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000269148FF4FA56AFFF2F9F5FFC8FBDBFF84F7AFFF80F7ADFF7DF6ABFF7BF6
      A9FF77F5A7FF75F5A6FF71F4A3FF6FF4A2FF6CF3A0FF6AF39EFF67F29CFF64F2
      9BFF61F199FF5FF197FF5BF095FF59F093FF80F3ADFFF0FDF5FFD2E9D9FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BFBFBFFF606060FFDADADAFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000072A4E1FF34A0E6FF5CDCFFFF55D6FEFF408EC1FF2D2E
      7EFF1537D4FF235AF8FF1E4ADFFF3C3B9AFFDBA149FFE0981CFFEDDFC9FFFEFE
      FEFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C5DACCFF238F45FF82BF96FFF6FDF8FFADFACAFF86F7B1FF83F7AEFF80F6
      ADFF7DF6ABFF7BF6A9FF77F5A7FF75F5A6FF72F4A3FF70F4A2FF6DF3A0FF6AF3
      9EFF67F29CFF64F29BFF61F198FF5FF197FF85F4B0FFF1FDF6FFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BFBFBFFF606060FFDADADAFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F8F8F8FFDCE0
      E5FFC0C7D0FFA0BADAFF2B77D2FF47B7EDFF64DFFDFF65BFD3FF333386FF0F2C
      CEFF1D4DF2FF235AF9FF2762FCFF1637CBFF795E72FFEDDFC9FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C9DBCEFF238F44FF7CBD90FFF2FEF6FFADFACAFF8AF8B3FF87F8
      B2FF84F7AFFF82F7AEFF7FF6ACFF7CF6ABFF79F5A8FF77F5A7FF73F5A5FF71F4
      A3FF6EF4A1FF6CF39FFF68F39DFF66F29CFF8AF5B4FFF2FEF6FFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E3E7ECFF0565D0FF0565
      D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565
      D0FF0565D0FF0565D0FF0565D0FFF1F2F3FF0000000000000000000000000000
      000000000000000000000000000000000000F4F7FAFF84A0C4FF4E7EBAFF327B
      D4FF247AD4FF2888D9FF43ACE7FF65D9F9FF6FC5D3FF3F4788FF0C24CAFF153B
      E8FF1C4BF2FF2258F8FF1B44DBFF2E2D95FFE5DACEFFFEFEFEFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009FC0A9FF218E44FFA9D3B6FFEAFEF2FF98FABDFF8DF9
      B5FF8AF8B3FF88F8B2FF85F7B0FF82F7AEFF7FF6ACFF7CF6AAFF79F5A8FF77F5
      A7FF74F4A5FF71F4A3FF6EF3A1FF6CF3A0FF8FF6B6FFF2FEF6FFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF3A9F
      E4FF3A9FE4FF3A9FE4FF3A9FE4FF3A9FE4FF3A9FE4FF3A9FE4FF3A9FE4FF3A9F
      E4FF3A9FE4FF3A9FE4FF0565D0FFD5D9E0FF0000000000000000000000000000
      000000000000000000000000000000000000EFF4FBFF377ED5FF1C79D3FF6BD1
      F0FF8CF8FFFF86F5FFFF7EF0FFFF7EE1E8FF353282FF0C23C8FF1235E6FF143A
      E8FF1A48EEFF163AD5FF172490FF6C8EC0FFE7E8EBFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A8CAB3FF2D944DFFABD5B9FFDDFDE9FF9EFA
      C0FF91F9B8FF8FF9B7FF8CF9B4FF89F8B3FF86F8B1FF84F7AFFF81F7ADFF7EF6
      ACFF7BF6AAFF79F5A8FF75F5A6FF73F5A4FF94F6BAFFF3FEF7FFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF6DE0
      FFFF6DE0FFFF6DE0FFFF6DE0FFFF6DE0FFFF6DE0FFFF6DE0FFFF6DE0FFFF6DE0
      FFFF6DE0FFFF6DE0FFFF0565D0FFD5D9E0FF0000000000000000000000000000
      000000000000000000000000000000000000FEFEFEFFD7DBE1FF4D8DDAFF217D
      D4FF74DCF4FF8BF8FFFF84F3FFFF81E7EFFF2E3B98FF0C23C8FF1235E6FF1439
      E8FF1130D0FF1C2696FF429FD4FF167CD7FF4B83C9FFCED9E7FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FCFCFCFF76B98BFF2F954FFFD2E9D9FFD1FD
      E1FF97FABCFF95FABAFFB6FBCFFFD8FDE6FFD7FCE5FFD6FCE5FFD6FCE4FFD5FC
      E4FFD4FCE3FFD3FCE2FFD2FCE2FFD1FBE1FFDCFCE8FFFBFEFCFFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF62D9
      FFFF62D9FFFF62D9FFFF62D9FFFF62D9FFFF62D9FFFF62D9FFFF62D9FFFF62D9
      FFFF62D9FFFF62D9FFFF0565D0FFD5D9E0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F8F8F9FF9CB3
      D0FF2474D1FF46A8E3FF84F1FCFF84F3FFFF78D6E7FF3B539FFF0C23C8FF0C24
      C9FF263798FF56B5DBFF51D5FFFF4BD1FFFF2598E6FF0E6CD1FF93B0D5FFECEE
      F0FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F5F6F5FF79B68CFF339752FFCAE4
      D1FFC4FDD9FF9EFBC0FFCCFDDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF5AD3
      FFFF5AD3FFFF5AD3FFFF5AD3FFFF5AD3FFFF5AD3FFFF5AD3FFFF5AD3FFFF5AD3
      FFFF5AD3FFFF5AD3FFFF0565D0FFD5D9E0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B1C0D3FF2573D1FF69D0F1FF8AF7FFFF82F2FFFF77D5E7FF2D3B98FF1D23
      92FF60BBDBFF60DCFDFF57D9FFFF51D5FFFF44C8FBFF2496E5FF0D63CCFF669C
      DFFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F5F6F5FF49A265FF389A
      57FFDDF2E4FFB9FDD1FFCFFDE0FFE9F4EDFFAAD4B7FFAAD4B7FFAAD4B7FFAAD4
      B7FFAAD4B7FFAAD4B7FFAAD4B7FFAAD4B7FFAAD4B7FFAAD4B7FF8EC59FFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF4FCB
      FFFF4FCBFFFF4FCBFFFF4FCBFFFF4FCBFFFF4FCBFFFF4FCBFFFF4FCBFFFF4FCB
      FFFF4FCBFFFF4FCBFFFF0565D0FFD5D9E0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D0D5DCFF2976D2FF89F0FAFF90FBFFFF89F6FFFF83F2FFFF81E2E8FF7EDC
      E1FF6BE3FEFF48BAEFFF228BDDFF1C7AD5FF2B76D2FF4084D7FF6B8DB8FFAABE
      D6FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E3E6E3FF4C9C
      65FF3E9D5CFFD6EDDDFFDEFEEAFFAAD4B7FF1B833BFF309650FF309650FF3096
      50FF309650FF309650FF309650FF309650FF309650FF309650FF309650FF2887
      46FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF46C6
      FFFF46C6FFFF46C6FFFF46C6FFFF46C6FFFF46C6FFFF46C6FFFF46C6FFFF46C6
      FFFF46C6FFFF46C6FFFF0565D0FFD5D9E0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A2BBDBFF2682D5FF96FFFFFF95FDFFFF8FFAFFFF88F6FFFF80F1FFFF7AED
      FFFF6ADFFBFF1D80D8FF799DCBFFB5BDC8FFD5D9E0FFEAECEFFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DCDF
      DDFF239046FF45A061FFE5F3EAFFAAD4B7FF2B8245FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF3BBE
      FFFF3BBEFFFF3BBEFFFF3BBEFFFF3BBEFFFF3BBEFFFF3BBEFFFF3BBEFFFF3BBE
      FFFF3BBEFFFF3BBEFFFF0565D0FFD5D9E0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004C8CD9FF5ABCE9FF8DF4FBFF72D7F2FF308DDAFF1672D1FF70DBF6FF81F1
      FFFF6CDEF9FF1B7CD6FFDCE4EEFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C7DCCDFF289149FF4CA467FF98CAA8FF309650FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF33B9
      FFFF33B9FFFF33B9FFFF33B9FFFF33B9FFFF33B9FFFF33B9FFFF33B9FFFF33B9
      FFFF33B9FFFF33B9FFFF0565D0FFD5D9E0FF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F8F9
      FAFF166ED0FF4FB1E6FF2D8BD9FF1B71D0FF83A5CFFF6790C4FF2686D8FF73DF
      F7FF6BD9F6FF1574D3FFF8F8F9FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFCFFB1CEBAFF269047FF3E9D5CFF309650FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AEC1D8FF0565D0FF0565
      D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565
      D0FF0565D0FF0565D0FF0565D0FFD7DBE1FF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B5C6
      DBFF0B62CCFF397FD5FF9CB3D0FFF1F2F3FF00000000000000006B93C6FF2581
      D7FF5FC8EFFF1E76D2FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000090BA9DFF128637FF278545FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7F8FBFFE2ECF9FFE2EC
      F9FFE2ECF9FFE2ECF9FFE2ECF9FFE2ECF9FFE2ECF9FFE2ECF9FFE2ECF9FFE2EC
      F9FFE2ECF9FFE2ECF9FFE8F0FAFFFBFBFCFF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D9E1
      ECFFA7BFDDFFE3E6EAFF00000000000000000000000000000000E7E9EDFF528F
      DBFF1877D3FF2F7BD4FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F3F3F3FF74A984FF5D926DFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F8F9
      FAFF4688D8FF4688D8FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F2F7FCFFEEF4FBFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BBBBE0FFB1B1DCFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F5F9F7FFA3DDB9FFBBE6CBFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EEF4FBFFE3EDF9FFE8F0FAFFFDFDFEFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F8F8F8FFAEAEC6FF3939A7FF020698FF020291FF4142ADFFAEAE
      C6FFF8F8F8FF000000000000000000000000000000000000000000000000EAEC
      EBFF8AD4A6FF21B159FF08A947FF05A847FF39B669FFAECAB9FFF8F8F8FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DBDBEB00B1B1D400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7EB00A1A1CB00F1F1F50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ECF2FBFF2C77D2FF4487D7FF2C77D3FF90B8E7FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E3E3E6FF7272B7FF12169CFF091CBBFF0A1EBFFF06089BFF0E12ADFF181A
      A2FF7272B7FFE3E3E6FF00000000000000000000000000000000C0DACAFF4EB1
      74FF16B656FF3EDE83FF1DBE5FFF19C065FF17BE63FF15AD52FF72C190FFE3E6
      E4FF000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FAFAFA00C1C1D6001D208600111887006E6EA000EEEE
      EE00000000000000000000000000000000000000000000000000000000000000
      000000000000DCDCDF002D2F8D00121D910037379100D0D0DB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E6EFF9FF337BD4FF8BB7E7FFDDF7FEFFC6E4F7FF4588D8FF8DB6E7FFFDFD
      FEFF000000000000000000000000000000000000000000000000E3E3E8FF8E8E
      BEFF10149BFF091AB3FF1132DFFF1235E6FF0C23C8FF06089BFF121AB8FF1218
      B7FF0D10AAFF13159FFF9595C2FFE3E3E8FFC8DBCFFF69C88EFF15B255FF3BD5
      7DFF5DFCA5FF5FFFA8FF20C162FF1EC56CFF2DD783FF29D27DFF12B75AFF11AB
      4FFF8EC5A3FFE3E8E5FF00000000000000000000000000000000000000000000
      00000000000000000000B0B0CB0022228400455DC2006184E5001E248E006E6E
      A000000000000000000000000000000000000000000000000000000000000000
      0000DCDCDF0047478F00253BAE004B78EF002135A90037389100EAEAEE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F8F9F800B1D7BD00D0DFD500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FBFC
      FEFF4989D9FF639BDEFFB2ECFDFF85E8FFFF95ECFFFFBEE3F7FF337BD3FF8DB6
      E7FF0000000000000000000000000000000000000000000000004040A9FF0F1B
      A7FF1539D7FF1842EAFF1339E8FF1235E6FF0C23C8FF060A9CFF131CBBFF131A
      B9FF1218B7FF1015B2FF1013A4FF39529DFF25B05CFF33C371FF68EFA8FF6EFD
      B2FF61FFAAFF5FFFA8FF20C162FF21C66FFF30D885FF2ED783FF2BD480FF20C8
      70FF11B053FF40B96FFF00000000000000000000000000000000000000000000
      0000FAFAFA00B0B0CB00161B85005976D3006B92FA004D74F4006589EA00222E
      99006E6EA000EEEEEE000000000000000000000000000000000000000000DCDC
      DF002E2F8D002A3EAE004F7DFA003A65F4005281F8002C45B80037389100D0D0
      DB00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000212D
      93002D7D570025924800229246009CB7A4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FDFDFEFF89B3
      E6FF5794DCFFACE2FAFF6FE1FFFF72E3FFFF76E5FFFF83E9FFFFBFE3F7FF4688
      D8FF90B8E7FFFDFDFEFF0000000000000000000000000000000006109FFF1E4E
      E9FF2053F6FF1B4AF0FF163DEAFF1236E6FF0C23C8FF070B9DFF1420BFFF141E
      BDFF131CBAFF131AB9FF1218B7FF094187FF33C16FFF93FFCDFF83FFC1FF76FF
      B8FF66FFADFF5FFFA8FF20C162FF26C872FF36DA89FF34D987FF30D885FF2ED7
      84FF25CE77FF07AB4AFF00000000000000000000000000000000000000000000
      0000B0B0CB00222286005F7AD3007BA1F9002A4EEB001437E600486EF200688C
      EA001F258E006E6EA00000000000000000000000000000000000DCDCDF004747
      8F002D41AE005883F3002349EB001337E6003761F2005684F8002538A9003738
      9100F1F1F5000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004469E5003384
      7B002692490079D596006CCC8B0022924600B9C8BD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A5C5ECFF2F79
      D3FFADDCF7FF78E0FFFF6CDFFFFF6FE1FFFF72E3FFFF76E5FFFF95ECFFFFC6E4
      F7FF2C76D3FFE8F0FAFF000000000000000000000000000000000610A0FF2051
      EAFF2157F8FF1D4DF2FF1741ECFF1337E7FF0C23C8FF070C9EFF1523C1FF1421
      BFFF141EBDFF131CBBFF121AB9FF094288FF34C170FF97FFD0FF88FFC5FF7BFF
      BBFF6AFFB0FF60FFA9FF20C162FF29C974FF3CDC8DFF39DB8BFF35DA88FF32D9
      86FF27CE79FF07AB4AFF00000000000000000000000000000000000000000000
      00001D1D82004151B00089AEFD00395DEE001235E6001235E6001336E6003E63
      EF006C8FEA00242F99006E6EA000EEEEEE0000000000DCDCDF002F2F8D003244
      AE005E89FA00254AEB001235E6001235E600163AE7004874F6005781EF001520
      9100A8A8CF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001133D3000E715B002792
      490081DEA0006FF4A0008BF8B2007DDA9B002292460087A29000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFEFEFFC2D7F2FF2673D1FFA0CE
      F2FF6ADAFFFF64DAFFFF68DCFFFF6BDEFFFF6FE1FFFF72E3FFFF85E8FFFFDDF7
      FEFF4487D7FFE3EDF9FF000000000000000000000000000000000611A0FF2153
      ECFF245BFAFF1F52F5FF1945EEFF153BE9FF0C23C8FF070D9FFF1627C5FF1625
      C3FF1522C1FF1420BFFF141EBDFF0A448AFF35C171FF9CFFD3FF8EFFC9FF80FF
      C0FF70FFB4FF64FFABFF20C162FF2ECA77FF41DE92FF3FDD90FF3BDC8DFF39DB
      8BFF2DD07DFF08AB4BFF00000000000000000000000000000000000000000000
      00009C9CB400202388006983D7006D91F7001538E6001235E6001235E6001336
      E6004C71F2006F91EA0020268E006E6EA000DCDCDF0047478F003547AE00678E
      F300264CEB001235E6001235E6001235E6003960F0006491FD002B40AE002E2F
      8D00EAEAED000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001335D30010666A0027934A007FD8
      9B006BF39F003DEB810046ED87008BF8B2006CCC8B0012823C00264382000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D3E2F5FF2774D2FF94C4EEFF7ADA
      FEFF5ED6FFFF61D8FFFF65DAFFFF68DCFFFF6CDFFFFF6FE1FFFFB1ECFDFF8FBA
      E9FF2A76D2FFEEF4FBFF000000000000000000000000000000000611A0FF2154
      ECFF255FFCFF2055F7FF1B49F0FF153BE6FF0612ABFF040898FF1627C4FF1627
      C5FF1525C3FF1523C1FF1421BFFF0A458BFF35C171FF9EFFD4FF92FFCCFF85FF
      C3FF75FFB7FF54EC99FF0DAF4DFF17B65CFF43DC90FF44DF93FF41DE91FF3EDD
      8FFF31D27FFF09AB4BFF00000000000000000000000000000000000000000000
      000000000000B9B9C600202388005E74C8007093F7002245E9001235E6001235
      E6001336E6004165EF007394EA002731990013157F00394AAE006C94FA00294D
      EB001235E6001235E6001437E6003A60EF006B94F5003043AE0047478F00DCDC
      DF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000566CB8002487600028934A0091E4AC006EF3
      A00038EB7E0038EB7E0038EB7E003BEB80008BF8B2007DDA9B0015894600194A
      9800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F8FAFDFF2E78D3FF7DB0E7FF70D7FFFF57D1
      FFFF5BD3FFFF5DD5FFFF61D8FFFF64DAFFFF78E0FFFFADE3FAFF6099DDFF337C
      D4FFECF2FBFF00000000000000000000000000000000000000000611A0FF2154
      ECFF2762FDFF2052EFFF0F2AC4FF040C9DFF0A1CB4FF0B1FBBFF050C9FFF0D18
      AEFF1525C2FF1627C5FF1525C3FF0A478DFF35C171FF9FFFD5FF97FECFFF77F1
      B3FF2FC56FFF15B254FF37CF78FF2AC76CFF0DAE4EFF27C46EFF41DA8EFF44DF
      93FF36D483FF0AAC4CFF00000000000000000000000000000000000000000000
      000000000000000000009C9CB400212488006F88D7007395F7001639E6001235
      E6001235E6001336E6005075F2007696EA00546BC600779AF3002A4FEB001235
      E6001235E6001235E6003459ED006F99FB003346AE002E2F8D00DCDCDF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000008F91A50014604D0028934A0093DCAA0074F3A4003DEB
      810038EB7E0038EB7E0038EB7E0038EB7E0046ED87008BF8B2006CCC8B000E81
      44000D35AB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FAFBFEFF6B9FDFFF5999DFFF82D4FBFF51CDFFFF54CF
      FFFF57D1FFFF5BD3FFFF5ED6FFFF6ADAFFFFAFDFF8FF5593DCFF4889D8FFE6EF
      F9FF0000000000000000000000000000000000000000000000000611A0FF2154
      ECFF1E4BE2FF0B1CAEFF0713A5FF1435D1FF1A46EDFF1740EBFF0F2BD4FF0715
      B0FF070D9FFF111FB8FF1627C5FF0B498EFF35C171FF9FFFD5FF57D692FF1DB4
      5BFF35C573FF66EAA5FF75FFB7FF68FCADFF3EDE84FF14B656FF16B559FF36D1
      80FF3AD586FF0BAC4DFF00000000000000000000000000000000000000000000
      0000000000000000000000000000B9B9C600212488006378C8007697F7002346
      E9001235E6001235E6001336E6004468EF006A8EF7002D50EB001235E6001235
      E6001437E600375BED00799EF400384AAE0047478F00DCDCDF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EAECEA0045A0620028934A009FE4B6007BF4A90043ED85003DEB
      820039EB7E0038EB7E0038EB7E0038EB7E0038EB7E003BEB80008BF8B2007DDA
      9C000E8144002253990000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FCFDFEFF6099DDFF4387D8FF87D7FDFF4CC8FFFF4DCAFFFF50CC
      FFFF54CFFFFF57D1FFFF7ADAFEFFA3D1F3FF2E78D3FF89B3E6FFFBFCFEFF0000
      0000000000000000000000000000000000000000000000000000040998FF0C1D
      AEFF0712A1FF1637CCFF245CF8FF2156F7FF1C4CF2FF1944EEFF143AE8FF1235
      E6FF0C25CCFF050FA7FF0912A6FF084487FF1AB158FF25B862FF2EBD6AFF6FE3
      A8FF92FFCCFF87FFC4FF7AFFBBFF6FFFB3FF63FFAAFF5CFCA5FF35D579FF11B2
      52FF15B459FF07A847FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9CB40021248800758DD700799A
      F700163AE6001235E6001235E6001336E6001A3DE7001235E6001235E6001235
      E600395DED007FA5FB003B4CAE002F308D00DCDCDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EAECEA005AA5720029934A00A1DCB40082F5AD0057F092004DEE8C0047ED
      880052EE8E0089F6B20062F199003AEB7F0038EB7E0038EB7E0046ED87008EF8
      B4006ECC8D00188A46002B468200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FEFEFEFFA0C2EBFF307AD3FF85C9F5FF46C4FFFF46C5FFFF4AC8FFFF4DCA
      FFFF51CDFFFF70D7FFFF96C5EEFF2573D2FFA5C5ECFFFDFDFEFF000000000000
      000000000000000000000000000000000000000000000000000000008DFF050E
      9DFF2154ECFF2763FDFF2661FCFF2259F8FF1D4FF3FF1A47EFFF153DEAFF1236
      E6FF1235E6FF1030DEFF191FA2FF6F5941FF709721FF4BC170FF91F7C8FF9EFF
      D4FF96FFCEFF8BFFC7FF7EFFBDFF73FFB6FF66FFACFF5FFFA8FF5DFDA6FF4FEF
      96FF0DAE4DFF00A23FFF00000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B9B9C60022248800687C
      C8007C9DF7002549EA001336E7001235E6001235E6001235E6001336E600395C
      ED0089AAF4003F4FAE0047478F00DCDCDF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EAEC
      EA0045A0620029934B00B2E4C20093F7B90066F39C0060F1980059F0930066F1
      9C00BEFBD400B2EAC300C1F8D30084F4AE003AEB7F0038EB7E0038EB7E003BEB
      80009BFABD0086DAA10013823D0087A290000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FEFEFEFFF5F5F5FFD1D1D1FFABABABFF9A9A9AFFB3B3B3FFDFDF
      DFFFC6D5E8FF2975D0FF7FC7F6FF44C1FFFF40C1FFFF42C3FFFF46C5FFFF4CC8
      FFFF82D5FBFF7DB0E7FF2673D1FFC2D7F2FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5B5D0FF4B4B
      A3FF0B19A7FF1B44DAFF2660FBFF245CFAFF1F52F5FF1C4AF1FF1740ECFF1338
      E8FF2035C6FF59497CFFC67F17FFE0910BFFE08E07FFC68F0BFF93BF65FF8FE9
      AFFF9AFFD1FF90FFCAFF83FFC1FF78FFB9FF6BFFB0FF5EFCA6FF40E186FF18B7
      58FF4BB072FFB5D4C1FF00000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008E8EA9001518
      810099B4EF00587BF200143AE8001338E7001237E7001235E6001B3EE7007598
      F7005E72C60014157F00DCDCDF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EAECEA005AA5
      720029934B00B2DCBF00A2F9C3007AF5A90070F4A2006AF39F0075F4A600BDFB
      D300AEE3BF002E9950006BBE8500C6F8D70078F3A7003DEB810038EB7E0038EB
      7E0051EE8E00A3FAC20075CC910024924700A7C3AF00FCFCFC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFCFFBABABAFF7D7D7DFF8B8B8BFFB5B5B5FFC2C2C2FFADADADFF8F8F
      8FFF5C6879FF6987A4FF48BDFAFF39BCFFFF3CBFFFFF40C1FFFF46C4FFFF87D7
      FDFF5999DFFF2E78D3FFD2E2F5FFFEFEFEFF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EAEA
      ECFF8787C9FF1A1F9FFF112CBFFF2051ECFF2055F6FF1D4DF2FF173FE8FF363E
      A7FFA16B32FFD78E15FFF4B448FFEBA52FFFEB971EFFF49E2EFFD8900DFFB39D
      25FF85D389FF8BF9C5FF86FFC4FF7BFFBCFF5FF2A3FF2CCB6FFF1EB058FF87D3
      A4FFEAECEBFF0000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DCDCDF0031328D00505C
      AE009CBBFB00385EEF00163DEA00153CE900143AE9001439E8001539E7004C6E
      F00088A4EA002D3799006E6EA000EEEEEE000000000000000000000000000000
      00000000000000000000000000000000000000000000E7EBE8002D944E002993
      4B00C3E5CF00B3FBCD0089F8B30083F7AF007CF6AA0081F6AD00C9FBDB00B8E3
      C6001C8B46001D5F8A000770520054AE7300CFF9DD009DF7BF0042EC840039EB
      7E0038EB7E0044EC8500A7FAC5008DDAA7001A8D3E0080BE9300FCFCFC000000
      000000000000000000000000000000000000000000000000000000000000FCFC
      FCFF888888FF8F8F8FFFE3E2E2FFF1F0F0FFEAE7E7FFE6E3E3FFE6E3E3FFEAE7
      E7FFD8D8D8FF989898FF5A7B8BFF3EAAE2FF39BCFFFF44C1FFFF85C9F5FF4388
      D9FF6A9FE0FFF7FAFDFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EAEAEFFF6767A7FF1A1D9DFF1531C5FF394DBBFF8E6346FFD78D
      15FFF0AE3EFFFDC364FFFFC76AFFEDA935FFED9922FFFFA744FFFDA540FFF09B
      27FFD78F0BFFA2A330FF81DC8FFF49D484FF1EAF57FF67B284FFEAEFECFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DCDCDF0047478F00545FAE00AEC7
      F400476EF2001943ED001741EC00163FEB00163EEA00153CE900143BE900163B
      E8005E80F3008BA7EA00252A8E006E6EA0000000000000000000000000000000
      00000000000000000000000000000000000000000000A1CFAF0028924A00B5D9
      C000E0FEEB00A0FBC20093FAB9008DF9B60090F8B700CBFBDD00C0E3CC002D96
      4E00286997001840E4001341C5000771530070BE8800D4F9E00090F5B60046ED
      87003AEB7F0038EB7E0053EE8F00ABFAC70084D29D00229246009CC7A900FCFC
      FC00000000000000000000000000000000000000000000000000FDFDFDFFB0B0
      B0FF939393FFE5E5E5FFF0EEEEFFECEAEAFFE9E6E6FFE6E3E3FFE2DFDFFFDFDB
      DBFFDFDBDBFFE1DEDEFFA0A1A1FF5A7B8BFF48BDFAFF7FC7F6FF2F7AD4FF5F98
      DDFFFAFBFEFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F5F5F5FFBCBCD8FF584D93FFAC6F23FFE9A733FFF7C1
      62FFFFC96FFFFFC76AFFFFC76AFFEDA935FFED9922FFFFA744FFFFA744FFFFA7
      44FFF7A135FFE9951AFFAF9312FF59B462FFBCDDC8FFF5F5F5FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DCDCDF0031328D005862AE00B5D0FD005178
      F5001A47EF001946EE001A45EE003C63F1006083F4002D54EE00163EEB00153D
      EA00173DE9005073F1008EAAEA00303899006E6EA000EEEEEE00000000000000
      00000000000000000000000000000000000000000000F1F5F200399A570064B0
      7C00F3F9F500DAFEE700A2FCC300A2FBC300DEFCEA00C8E3D000208D46002E6E
      9800000000000000000000000000375FDE00177F550058AF7600DAF9E400A7F8
      C6004BED8A003FEC820039EB7F0045EC8600AFFACA00ACE9BF002694490080BE
      9300FCFCFC000000000000000000000000000000000000000000E2E2E2FF8080
      80FFF0EFEFFFF3F2F2FFF0EEEEFFE5E3E3FFE9E6E6FFE6E3E3FFE2DFDFFFDFDB
      DBFFDBD7D7FFD8D4D4FFE0DEDEFF989898FF6987A4FF2B75D1FFA1C2EBFFFDFD
      FEFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A12CFFFFDB90FFFFD6
      86FFFFCF7AFFFFCA70FFFFC76AFFEDA935FFED9922FFFFA744FFFFA744FFFFA7
      44FFFFA744FFFFA744FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DCDCDF0047478F005C65AE00C3D9F500547DF600204F
      F2001B4AF1001B49F0003F68F200A4C1FC00A8C0EF0097B5FA00254EEE001740
      EC00163FEB00183FEA006386F40092ACEA00262B8E006E6EA000000000000000
      0000000000000000000000000000000000000000000000000000D0DCD400399A
      57007FBE9300F3F9F500D9FEE700DEFEEA00B9DCC4002F965000307199001F4C
      E9000000000000000000000000000000000089A9C700238A560075BE8C00DEF9
      E7009AF6BD004FEE8D0042EC84003CEB800055EE9100B3FACD0099DBAE002694
      49009CC7A900FCFCFC00000000000000000000000000000000009C9C9CFFB4B4
      B4FFF7F6F6FFF4F2F2FFDDDBDBFF959595FFC4C2C2FFE6E3E3FFE2DFDFFFDFDB
      DBFFDCD7D7FFD8D4D4FFDBD7D7FFD8D7D7FF5C6879FFC6D6E8FFFEFEFEFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A12DFFFFDF97FFFFDA
      8FFFFFD483FFFFCF79FFFFC96DFFEDA935FFED9923FFFFA744FFFFA744FFFFA7
      44FFFFA744FFFFA744FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DCDCDF0032338D005F68AE00CEE5FE006F95F9001F51F5001E50
      F4001F4FF300446EF500B0C8F3005761AE00171981007888C8009FBDFB003960
      F1001842ED001742EC001942EC005478F20096AFEA00323A99006E6EA000EEEE
      EE0000000000000000000000000000000000000000000000000000000000EAEE
      EB00399A570064B07C00EFF7F100AFD6BB0015873B003F7F9B00000000000000
      000000000000000000000000000000000000000000006E81B900248C56004BA6
      6A00D9F4E200B1F8CC0054EF900048ED880041EC830049ED8900B8FAD000B5E9
      C50027944A0080BE9300FDFDFD000000000000000000000000007F7F7FFFD8D8
      D8FFF7F6F6FFE0DFDFFF808080FFACACACFF7A7A7AFFB3B1B1FFE3DFDFFFDFDC
      DCFFDCD7D7FFD8D4D4FFD5D0D0FFE7E4E4FF909090FFDFDFDFFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A22EFFFFE4A0FFFFE0
      98FFFFD98DFFFFD483FFFFCE77FFEDAA38FFED9D29FFFFAC4CFFFFA744FFFFA7
      44FFFFA744FFFFA744FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DCDCDF0047478F005F68AE00CEE1F500739AFB00285CF7002054F6001F53
      F5004773F600B4CFFC005A64AE0031328D008E8EA900242688008FA2D700A3C0
      FB002852F0001944EE001843ED001A43EC00688BF50099B1EA00282C8E006E6E
      A000000000000000000000000000000000000000000000000000000000000000
      0000B3BFBA0011723F004FA66B00238F46004583A3002658ED00000000000000
      00000000000000000000000000000000000000000000000000007590B300238B
      55005FB07900DEF4E500A3F7C30058F093004BEE8A0045ED860060F09800C4FB
      D800A1DBB30028944A00BFDEC9000000000000000000000000007C7C7CFFE0E0
      E0FFE3E2E2FF8C8C8CFFBABABAFFFDFDFDFFDDDDDDFF7A7A7AFFBFBCBCFFDFDC
      DCFFDCD8D8FFD9D4D4FFD5D0D0FFDCD7D7FFADADADFFB3B3B3FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A22FFFFFE7A7FFFFE3
      9FFFFFDE95FFFFD98CFFF8C467FFE49B1DFFE49516FFF8A944FFFFAB4AFFFFA7
      44FFFFA744FFFFA744FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000036368F00434A9D00DEF2FF00A0C0FD00235BFA00235AF9002359F8004A78
      F900BED3F4005E67AE0047478F00DCDCDF0000000000B9B9C600252788007D8C
      C800A7C3FB003F68F3001A47EF001946EE00224CEF0088A8F900A0B7E8002125
      8A00ADADD1000000000000000000000000000000000000000000000000000000
      0000000000003D4891004AA56F006FA2B9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007384
      B90034936A0044A06600E3F4E800BBF9D3005DF0960053EF90009BF6BE00E3FE
      ED00A6DBB70028944B00BFDEC9000000000000000000000000007D7D7DFFCBCB
      CBFF818181FFBABABAFF000000000000000000000000ECECECFF7A7A7AFFAFAD
      ADFFDCD8D8FFD9D4D4FFD5D0D0FFDAD5D5FFC2C2C2FF9A9A9AFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A331FFFFEBAEFFFFE7
      A8FFF9D586FFECB044FFE19614FFECA833FFECA732FFE19411FFEC9B25FFF9A6
      40FFFFA744FFFFA744FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008080B800272A8A00B9C9E700CEE5FE005887FC00245DFA004B7BFB00BAD5
      FD005F68AE0032338D00DCDCDF000000000000000000000000009C9CB4002527
      880095A6D700B1CCFC003964F2001B48F0006589F600BAD5FD008698D3001B1D
      8300CDCDE3000000000000000000000000000000000000000000000000000000
      00000000000000000000B2C2DE00CCE2FB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000839AC00038966A0062B07B00E6F4EA00B1F8CD00A1F7C200EEFEF400C9E9
      D30027924B0080BE9300FDFDFD000000000000000000000000007C7C7CFF7777
      77FFBABABAFFFDFDFDFF00000000000000000000000000000000DDDDDDFF7A7A
      7AFFBAB7B7FFD9D4D4FFD5D0D0FFDAD5D5FFB5B5B5FFABABABFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A431FFFEEBAFFFF2C8
      6FFFE29C1FFFE6A32BFFF8C466FFFEC96EFFFEC567FFF8BB55FFE69D20FFE292
      0FFFF29E2EFFFEA642FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFC008E8EBA002C308D00B4C4E400CCE3FE007AA2FD00BFD4F4005F68
      AE0047478F00DCDCDF000000000000000000000000000000000000000000B9B9
      C60025278800828FC800BBD5FD0096B4FA00CBE2FC008D9DD30023248600B0B0
      CB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007E8CC3003B996A0048A26600E9F4ED00FFFFFF00B7DBC2002891
      4B009CC7A900FCFCFC0000000000000000000000000000000000B7B7B7FFBFBF
      BFFF000000000000000000000000000000000000000000000000FDFDFDFFACAC
      ACFF8F8D8DFFD3CECEFFD5D0D0FFE7E3E3FF8B8B8BFFD1D1D1FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE19C23FFE5A42EFFE3A0
      27FFF5CE79FFFEDF98FFFFD98CFFFFD380FFFFCA70FFFFC76AFFFEC668FFF5B7
      4DFFE39817FFE59313FFE1961AFFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F5F5F5006464A90032379100CBDEF300D8EEFF005F68AE003233
      8D00DCDCDF000000000000000000000000000000000000000000000000000000
      00009C9CB400262788009DACD700D8EDFF0092A2D3001F228500B0B0CB00FAFA
      FA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000008BA0C00044A06B0063B07B00BFDFC90029914B007DBB
      9000FCFCFC000000000000000000000000000000000000000000FCFCFCFFFEFE
      FEFF000000000000000000000000000000000000000000000000BABABAFF7D7D
      7DFFCBC8C8FFD9D5D5FFD9D5D5FFDDDCDCFF7D7D7DFFF5F5F5FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D4CDC3FFDF9619FFE7AA38FFFAE1
      9CFFFFEAACFFFFE5A3FFFFDE96FFFFD88BFFFFD07BFFFFC96FFFFFC76AFFFFC7
      6AFFFABE5BFFE59C1DFFDF9619FFD4CDC3FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F5F5F5008080B200313590004C54A30047478F00DCDC
      DF00000000000000000000000000000000000000000000000000000000000000
      000000000000B9B9C60026288800626BB00024248600B0B0CB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005F69AB000B6145000B8132009CC7A900FCFC
      FC00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFDFDFFBABABAFF878686FFCFCB
      CBFFDCD8D8FFDAD6D6FFDBD8D8FF8F8F8FFFB9B9B9FFFEFEFEFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FCFCFCFFE2D4BCFFE3A336FFE4A3
      2FFFF6D485FFFDE6A6FFFFE4A1FFFFDE97FFFFD789FFFFD17CFFFDC76AFFF6B7
      4EFFE49B1EFFE3A336FFE2D4BCFFFCFCFCFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F5F5F5006464A9002F2F8B00DCDCDF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000A0A0B70028288800B0B0CB00FAFAFA00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AFB3C800E4EFE700FEFEFE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BABABAFF7E7E7EFFD1CFCFFFE0DD
      DDFFDFDBDBFFE3E1E1FF929292FF888888FFFCFCFCFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E8E5E0FFD9BD
      8EFFE09B1FFFEAB248FFFBE09BFFFFE3A0FFFFDD92FFFBD07CFFEAA630FFE097
      19FFD9BD8EFFE8E5E0FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FEFEFEFFBFBFBFFF777777FFC6C5C5FFD7D6D6FFD1CF
      CFFFB3B2B2FF808080FFB0B0B0FFFCFCFCFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F0E8DCFFDCB26BFFE19B20FFEEBB58FFEEB953FFE19A1FFFDCB26BFFF0E8
      DCFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FCFCFCFFB7B7B7FF7C7C7CFF7D7D7DFF7E7E7EFF8080
      80FF9C9C9CFFE2E2E2FFFDFDFDFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F9F8F8FFD4C6AEFFE3A53AFFE3A53AFFD4C6AEFFF9F8F8FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F4F4F400698FA000698FA000307C9C00307C
      9C00307C9C00307C9C00307C9C00307C9C00307C9C00307C9C00307C9C00307C
      9C00307C9C00307C9C00307C9C004C849B00F4F4F40000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F1008282820083838300838383008383830083838300838383008383
      8300838383008383830083838300838383008383830083838300838383008383
      8300838383008383830083838300838383008383830083838300838383008282
      8200000000000000000000000000000000000000000000000000000000000000
      0000E7E7E7008282820083838300838383008383830083838300838383008383
      8300838383008383830083838300838383008383830083838300838383008383
      8300838383008383830083838300838383008383830083838300838383008282
      8200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF00307C9C00CEF8FB00D0FCFF00D0FC
      FF00D0FCFF00D0FCFF00D0FCFF00D0FCFF00D0FCFF00D0FCFF00D0FCFF00D0FC
      FF00D0FCFF00D0FCFF00CEF8FB00307C9C00DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006AC1EB00609ED400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000066CAEB006393CB00B8E4F50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10084848400FDFDFD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFCFC008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80084848400FDFDFD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00DCDEDC00238D380097BA9B00FAFAFA00FFFFFF00FCFCFC008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF00307C9C00D0FCFF00A6E6FF00A2E4
      FF009DE1FF0099DFFF0094DCFF0090DAFF008AD8FF0086D6FF0084D5FF0084D5
      FF0084D5FF0084D5FF00D0FCFF00307C9C00DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005BCFFA003AA0D6000B1B86001118870033578D001582
      3A00000000000000000000000000000000000000000000000000000000000000
      00000000000068AEB80009248D00121D9100142F91004CACDB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00F8F8F800F6F6F600F6F6F600F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F4F4F400F4F4F400F4F4F400F4F4F400F3F3
      F300F3F3F300F3F3F300F3F3F300F3F3F300F2F2F200F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00F8F8F800F6F6F600F6F6F600F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F4F4F400F4F4F400F4F4F400F4F4F400F3F3
      F300F3F3F300D1D6D30023934500128E3900629D7200F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF00307C9C00D0FCFF0090E1FF008BDE
      FF0084DBFF007ED8FF0077D5FF0072D2FF006BCFFF0066CCFF005FC9FF005CC7
      FF005BC7FF005BC7FF00D0FCFF00307C9C00DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004092CB00101E8400455DC2006184E50019228C000A3C
      4D00000000000000000000000000000000000000000000000000000000000000
      000013783800253A8200253BAE004B78EF002035A900142F910000389F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00F1F1F100CCCFCC00208B36008BAE9000E7E7
      E700ECECEC00EBEBEB00EBEBEB00EAEAEA00EAEAEA00E9E9E900E9E9E900E9E9
      E900E8E8E800E8E8E800E7E7E700E7E7E700E6E6E600F2F2F200FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00F1F1F100EDEDED00EDEDED00ECECEC00ECEC
      EC00ECECEC00CACDCA00C9CFCB00C8CECA00C8CECA00C8CDC900C8CDC900C8CD
      C900C8CDC900A9BAAE001D873600D1FFE1003EA860003B9C5300D4E0D7008383
      8300000000000000000000000000000000000000000000000000FEFEFE00C2C2
      C200696A6A0074787800747878006F717100307C9C00D0FCFF0097E5FF0092E2
      FF008BDFFF0085DCFF007FD9FF0079D6FF0072D2FF006DD0FF0066CCFF0060CA
      FF005BC7FF005BC7FF00D0FCFF00307C9C006F7171007478780074787800696A
      6A00C2C2C200FEFEFE0000000000000000000000000000000000000000000000
      0000FAFAFA00002A9000111A85005976D3006B92FA004D74F4006589EA00222E
      9900556D8F0058E08C0000000000000000000000000000000000000000009CDA
      B400071B6C002A3EAE004F7DFA003A65F4005281F8002C45B800000E7E00D0D0
      DB00000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00F2F2F200CED3CF0022924400128E3900609B
      7000EDEDED00ECECEC00ECECEC00ECECEC00EBEBEB00EBEBEB00EAEAEA00EAEA
      EA00E9E9E900E9E9E900E8E8E800E8E8E800E7E7E700F3F3F300FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00F2F2F200EFEFEF00EEEEEE00EEEEEE00EDED
      ED00EDEDED001E873000238C3A00238A3A00238A3900238A3900228A3900228A
      3900228A39001E86340006701600D0FFE400B9FCCF0049AA670024913E006F77
      7100000000000000000000000000000000000000000000000000F0F0F0007476
      7600F9F9F900F0F0F000777A7A00B2B1B100307C9C00D0FCFF00A0EAFF009BE6
      FF0094E3FF008FE0FF0088DDFF0082DAFF007BD7FF0076D4FF006FD1FF006ACE
      FF0063CBFF005EC8FF00D0FCFF00307C9C00A29F9F0075787800EFEFEF00F9F9
      F90074757500F0F0F00000000000000000000000000000000000000000000000
      0000B0B0CB00030A7C005F7AD3007BA1F9002A4EEB001437E600486EF200688C
      EA001D258C0029687300000000000000000000000000000000004FCF83003446
      82002D41AE005883F3002349EB001337E6003761F2005684F8002336A8003738
      9100F1F1F500000000000000000000000000DCDEDC00DBE0DC00DBE0DC00DBE0
      DC00C7CEC90072777300DBE0DC00D0D5D200AEBFB3001E883700D1FFE1003EA8
      60003A9A5200C5D1C800ECECEC00ECECEC00EBEBEB00EBEBEB00EBEBEB00EAEA
      EA00EAEAEA00E9E9E900E9E9E900E8E8E800E8E8E800F3F3F300FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000CCD0CD0085858500FFFFFF00F2F2F200EFEFEF00EFEFEF00EEEEEE00EEEE
      EE00EDEDED000B843100E0F7E800E5FFF300E3FFEF00DFFFED00DDFFEB00DAFF
      EA00D8FFE800D4FFE700D4FFE5009EF8BF0031E97A00ADFFCB0086D29F000682
      2D008CCC9E000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F3F2F20074787800DBD9D900447F9700CEF7FC00A7EDFF00A2EA
      FF009BE7FF0096E4FF008FE1FF0089DEFF0083DBFF007DD8FF0076D4FF0071D2
      FF006ACEFF0064CCFF00CBF5FC00447F9700C2BEBE0074787800ECEBEB00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      00001D1D82004151B00089AEFD00395DEE001235E6001235E6001336E6003E63
      EF006C8FEA00242F99001865680034DB76000000000030CB7000142C7B003244
      AE005E89FA00254AEB001235E6001235E600163AE7004874F6005781EF001520
      9100A8A8CF0000000000000000000000000021893300268F3D00268D3C00268D
      3C00479A5900137B2A00268D3C00248C3B001F87350007711700D0FFE400B9FC
      CF0049AA6700218E3C00CAD2CC00EDEDED00EDEDED00ECECEC00ECECEC00EBEB
      EB00EBEBEB00EBEBEB00EAEAEA00EAEAEA00E9E9E900F4F4F400FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      000076AE820085858500FFFFFF00F3F3F300F0F0F000F0F0F000EFEFEF00EFEF
      EF00EEEEEE0009822F00DDEFE2004FEF8E0050EE8D004CEC8A004CED8B004BED
      8A004CED8A004BED8A004CED8B0047EC870036EA7D0039EA7E0090FCB8009DDA
      AF000D86330081BB9000FCFCFC00000000000000000000000000EDEDED007478
      7800FDFDFD00EBE9E90074787800E2E1E1007578780074787800747878007478
      7800747878007478780074787800747878007478780074787800747878007478
      780074787800747878007478780074777700C9C4C40074787800D8D4D400FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      00009C9CB4000A1281006983D7006D91F7001538E6001235E6001235E6001336
      E6004C71F2006F91EA00172588001865680030CB700010416B003547AE00678E
      F300264CEB001235E6001235E6001235E6003960F0006491FD002B40AE002E2F
      8D00EAEAED000000000000000000000000000B843100E0F7E800E5FFF300E3FF
      EF00DFFFED00DDFFEB00DAFFEA00D8FFE800D4FFE700D4FFE5009EF8BF0031E9
      7A00ADFFCB0086D29F000C88330082C29500EDEDED00EDEDED00ECECEC00ECEC
      EC00EBEBEB00EBEBEB00EBEBEB00EAEAEA00EAEAEA00F4F4F400FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000DCF1E50085858500FFFFFF00F4F4F400F1F1F100F1F1F100F0F0F000F0F0
      F000EFEFEF0009823000E4EFE7004DF08C004FEE8C0042EC85003AEB7F0035EA
      7C0035EA7C0035EA7C0035EA7C0036EA7D0037EB7D0037EA7D002CE9760076F7
      A700CBFFDD0029974B0038A25500E4E6E4000000000000000000EDEDED007478
      7800FDFDFD00EDEBEB0074787800F4F3F300DCDBDB00DBDADA00DAD8D800D8D7
      D700D6D5D500D5D3D300D4D2D200D3D0D000D1CFCF00CFCDCD00CECBCB00CDCA
      CA00CBC8C800CAC7C700C8C5C500C7C4C400D7D2D200787B7B00D9D4D400FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      0000000000005B739500171E84005E74C8007093F7002245E9001235E6001235
      E6001336E6004165EF007394EA002731990009137900394AAE006C94FA00294D
      EB001235E6001235E6001437E6003A60EF006B94F5003043AE00232C7C00A5B3
      B7000000000000000000000000000000000009822F00DDEFE2004FEF8E0050EE
      8D004CEC8A004CED8B004BED8A004CED8A004BED8A004CED8B0047EC870036EA
      7D0039EA7E0090FCB8009DDAAF000D86330078B38800EBEBEB00EEEEEE00EDED
      ED00EDEDED00ECECEC00ECECEC00EBEBEB00EBEBEB00F5F5F500FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      00007AE4A30085858500FFFFFF00F5F5F500F2F2F200F2F2F200F1F1F100F1F1
      F100F0F0F0000A823000E7EFE90063F39B0063F2990058EF92004FEF8D0045EC
      86003CEA810037EA7C0037EA7D0037EB7D0038EB7E0038EB7E0037EB7D0033E9
      7A0058F19200CAFBDA005BB57800319244000000000000000000EDEDED007478
      7800FDFDFD00EEECEC0075787800747878007478780074787800747878007478
      7800747878007478780074787800747878007478780074787800747878007478
      78007478780074787800747878007478780074787800777A7A00DAD6D600FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      000000000000000000001C247A000F1380006F88D7007395F7001639E6001235
      E6001235E6001336E6005075F2007696EA00546BC600779AF3002A4FEB001235
      E6001235E6001235E6003459ED006F99FB003346AE00060A7D000F1F8E000000
      00000000000000000000000000000000000009823000E4EFE7004DF08C004FEE
      8C0042EC85003AEB7F0035EA7C0035EA7C0035EA7C0035EA7C0036EA7D0037EB
      7D0037EA7D002CE9760076F7A700CBFFDD0029974B00349E5200D4D6D400EEEE
      EE00EDEDED00EDEDED00ECECEC00ECECEC00EBEBEB00F5F5F500FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      000073E4A00085858500FFFFFF00F5F5F500F3F3F300F2F2F200F2F2F200F2F2
      F200F1F1F1000A823100E9EFEA007EF8AB007BF6AA0070F4A30068F39D005DF1
      960055F0900049ED880041ED830039EA7D0036EA7D0037EB7D0037EB7D0033E9
      7B006FF2A100D4FDE2005BB57700319244000000000000000000EDEDED007478
      7800FDFDFD00F0EEEE00EFEEEE00F6F5F500F6F5F500F6F5F500F6F5F500F6F5
      F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5
      F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5F500F0EEEE00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      00000000000000000000000000004868C500141A88006378C8007697F7002346
      E9001235E6001235E6001336E6004468EF006A8EF7002D50EB001235E6001235
      E6001437E600375BED00799EF400384AAE001C288E00557CDD00000000000000
      0000000000000000000000000000000000000A823000E7EFE90063F39B0063F2
      990058EF92004FEF8D0045EC86003CEA810037EA7C0037EA7D0037EB7D0038EB
      7E0038EB7E0037EB7D0033E97A0058F19200CAFBDA005BB578002D8F4100E5EC
      E700EEEEEE00EEEEEE00EEEEEE00EDEDED00EDEDED00F5F5F500FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      000082E6A80085858500FFFFFF00F6F6F600F4F4F400F4F4F400F3F3F300F3F3
      F300F2F2F2000A823100E7EEE90091FCB9008CF8B50082F6AD0079F6A9006DF3
      A00066F19B005AF0940051EE8E0047EE870040EB830036EA7E0027E9730098F9
      BD00E3FFEC002A974C0038A25500E4E6E4000000000000000000EDEDED007478
      7800FDFDFD00F1EFEF00F0EEEE00EEECEC00EDEBEB00ECE9E900EBE8E800E8E4
      E300DAD0C400CEBEA900C4AD8D00C3AB8800C2AA8700C2AA8B00C9B8A300D2C7
      BB00DCD8D700DDD8D800DCD7D700DAD6D600D9D5D500D7D3D300DDD9D900FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000F2AAA000D148600758DD700799A
      F700163AE6001235E6001235E6001336E6001A3DE7001235E6001235E6001235
      E600395DED007FA5FB003B4CAE00070E8900102EC90000000000000000000000
      0000000000000000000000000000000000000A823100E9EFEA007EF8AB007BF6
      AA0070F4A30068F39D005DF1960055F0900049ED880041ED830039EA7D0036EA
      7D0037EB7D0037EB7D0033E97B006FF2A100D4FDE2005BB577002E8F4100E6EC
      E800EFEFEF00EFEFEF00EEEEEE00EEEEEE00EDEDED00F6F6F600FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      000092EAB40085858500FFFFFF00F7F7F700F5F5F500F4F4F400F4F4F400F3F3
      F300F3F3F3000A823100E4EEE700B5FFD000B1FDCC00A7FBC6009EFAC10096F9
      BA008EF8B60084F5AE007DF5AB006FF1A20056EF910055EF9100BAFED400AFDA
      BB000D86340081BB9000FCFCFC00000000000000000000000000EDEDED007478
      7800FDFDFD00F2F1F100F1EFEF00F0EEEE00EEECEC00EDEBEB00D9CCBC00B189
      4F00B0854400AD803D00AD803D00AD803D00AD803D00AD803D00AD803D00AD80
      3D00AE844800D2C8BE00DCD8D800DCD7D700DAD6D600D9D5D500DEDADA00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001435BB000E148700687C
      C8007C9DF7002549EA001336E7001235E6001235E6001235E6001336E600395C
      ED0089AAF4003F4FAE00050F8800102EC9000000000000000000000000000000
      0000000000000000000000000000000000000A823100E7EEE90091FCB9008CF8
      B50082F6AD0079F6A9006DF3A00066F19B005AF0940051EE8E0047EE870040EB
      830036EA7E0027E9730098F9BD00E3FFEC002A974C00359F5200D7D9D700F1F1
      F100F0F0F000F0F0F000EFEFEF00EFEFEF00EEEEEE00F6F6F600FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      00009EEBBA0085858500FFFFFF00F7F7F700F6F6F600F5F5F500F5F5F500F5F5
      F500F4F4F4000E843400F1F7F300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CAFADD0070F4A300DEFFEC00A2D3B1000682
      2D008CCC9E000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F3F2F200F2F1F100F1EFEF00ECE8E400CAB49600AE823F00DAB9
      7700FFEAAE00FFECB100FFEEB300FFEEB400FFEDB300FFECB100FFEAAD00FFE8
      A800E0BF7A00AF834000C0A88B00DAD4D100DCD8D800DAD6D600DFDCDC00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000102AA1000E12
      810099B4EF00587BF200143AE8001338E7001237E7001235E6001B3EE7007598
      F7005E72C60006097E00102EC900000000000000000000000000000000000000
      0000000000000000000000000000000000000A823100E4EEE700B5FFD000B1FD
      CC00A7FBC6009EFAC10096F9BA008EF8B60084F5AE007DF5AB006FF1A20056EF
      910055EF9100BAFED400AFDABB000D8634007AB58A00EFEFEF00F2F2F200F2F2
      F200F1F1F100F1F1F100F0F0F000F0F0F000EFEFEF00F7F7F700FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000B6EECB0085858500FFFFFF00F8F8F800F6F6F600F6F6F600F5F5F500F5F5
      F500F5F5F5001F883100248D3B00248C3900248C3900248B3900248B3900248B
      3900238B39001F86340007701300FFFFFF00EEFDF30058AA720024913C006F77
      7100000000000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F5F4F400F3F2F200F2F1F100C8B19200AF833F00ECCB8600FFE4
      A200FFE7A700FFE8AA00FFEAAC00FFEAAD00FFE9AB00FFE8AA00FFE6A600FFE4
      A200FFE19C00ECC88000AF823F00BEA68600DDD9D900DCD8D800E0DDDD00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001C48D7000B148B00505C
      AE009CBBFB00385EEF00163DEA00153CE900143AE9001439E8001539E7004C6E
      F00088A4EA002D379900081795001131D7000000000000000000000000000000
      0000000000000000000000000000000000000E843400F1F7F300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CAFADD0070F4
      A300DEFFEC00A2D3B1000C88330086C69900F4F4F400F4F4F400F3F3F300F3F3
      F300F2F2F200F2F2F200F1F1F100F1F1F100F0F0F000F7F7F700FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00F9F9F900F8F8F800F7F7F700F7F7F700F6F6
      F600F6F6F600D3D6D300D2D7D300D2D7D300D1D6D300D1D6D300D0D6D200D0D6
      D200D0D5D200B0C2B5001F883400FFFFFF0051A76C003D9D5300D4E0D7008383
      8300000000000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F6F5F500F5F4F400E3D9CD00B0823D00EDC67B00FFDD9400FFDF
      9800FFE29D00FFE3A000FFE4A200FFE5A200FFE4A100FFE3A000FFE19C00FFDF
      9800FFDC9200FFD98C00EDC27500AE813C00CDBDAB00DDD9D900E2DEDE00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001E4EDA0009198D00545FAE00AEC7
      F400476EF2001943ED001741EC00163FEB00163EEA00153CE900143BE900163B
      E8005E80F3008BA7EA001B218D00081795000000000000000000000000000000
      00000000000000000000000000000000000021893300268F3C00268D3B00268D
      3B00479A5800137B2900268D3B00258D3A002087340007701300FFFFFF00EEFD
      F30058AA7200228F3B00D0D8D200F5F5F500F5F5F500F4F4F400F4F4F400F3F3
      F300F3F3F300F2F2F200F2F2F200F2F2F200F1F1F100F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      000076AE820085858500FFFFFF00FAFAFA00F8F8F800F8F8F800F7F7F700F7F7
      F700F6F6F600F6F6F600F5F5F500F5F5F500F5F5F500F4F4F400F4F4F400F3F3
      F300F3F3F300D0D5D20023934500178E3D00629C7000F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F8F7F700F3F1EF00AF854700E7B96800FFD58400FFD88B00FFDA
      9000FFDD9400FFDE9600FFDF9800FFE09900FFDF9800FFDE9600FFDC9300FFDA
      8F00FFD78A00FFD58400FFD17C00E0AF5D00AD803F00DCD7D500E3DFDF00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      00000000000000000000000000004873DD00141D8C005862AE00B5D0FD005178
      F5001A47EF001946EE001A45EE003C63F1006083F4002D54EE00163EEB00153D
      EA00173DE9005073F1008EAAEA0030389900192998003556DC00000000000000
      000000000000000000000000000000000000DCDEDC00DBE0DC00DBE0DC00DBE0
      DC00C7CEC90072777300DBE0DC00D8DDDA00B5C7BB0020893500FFFFFF0051A7
      6C003D9D5300CDD9D100F7F7F700F6F6F600F6F6F600F5F5F500F5F5F500F5F5
      F500F4F4F400F4F4F400F3F3F300F3F3F300F2F2F200F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000CCD0CD0085858500FFFFFF00FBFBFB00F9F9F900F9F9F900F8F8F800F8F8
      F800F8F8F800F7F7F700F7F7F700F6F6F600F6F6F600F5F5F500F5F5F500F5F5
      F500F4F4F400D2D5D200218C36008FB29400EDEDED00F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F9F8F800E6DCCF00AD803D00FFCB7300FFCF7900FFD27F00FFD4
      8300FFD68800FFD88A00FFD88B00FFD98C00FFD88B00FFD88900FFD68600FFD4
      8300FFD17E00FFCE7800FFCA7100FFC86C00AD803D00D2C6B900E5E1E100FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      000000000000000000007381B600252982005C65AE00C3D9F500547DF600204F
      F2001B4AF1001B49F0003F68F200A4C1FC00A8C0EF0097B5FA00254EEE001740
      EC00163FEB00183FEA006386F40092ACEA0020268C002E378B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FBFBFB00D7DCD90024944600178E3D0065A0
      7300F8F8F800F8F8F800F7F7F700F7F7F700F6F6F600F6F6F600F5F5F500F5F5
      F500F5F5F500F4F4F400F4F4F400F3F3F300F3F3F300F9F9F900FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FBFBFB00FAFAFA00FAFAFA00F9F9F900F9F9
      F900F8F8F800F8F8F800F7F7F700F7F7F700F6F6F600F6F6F600F5F5F500F5F5
      F500F5F5F500F4F4F400F4F4F400F3F3F300F3F3F300F9F9F900FFFFFF008383
      8300000000000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00FBFBFB00E0D3C200AD803D00FFC76B00FFC96F00FFCC7400FFCE
      7800FFD07C00FFD27F00FFD28000FFD38100FFD38000FFD27F00FFD07B00FFCE
      7800FFCB7300FFC96E00FFC76B00FFC76A00AD803D00D1C2B000F0EEEE00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      000000000000202680000D0F7A005F68AE00CEE5FE006F95F9001F51F5001E50
      F4001F4FF300446EF500B0C8F3005761AE0010127E007888C8009FBDFB003960
      F1001842ED001742EC001942EC005478F20096AFEA00323A9900101371002329
      8700000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FCFCFC00D8DBD800228D380094B79800F5F5
      F500F9F9F900F9F9F900F8F8F800F8F8F800F8F8F800F7F7F700F0F0F000F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500EFEFEF00F4F4F4008686
      8600000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FCFCFC00FBFBFB00FBFBFB00FAFAFA00FAFA
      FA00F9F9F900F9F9F900F8F8F800F8F8F800F8F8F800F7F7F700F0F0F000F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500EFEFEF00F4F4F4008686
      8600000000000000000000000000000000000000000000000000F2F2F2007375
      7500EEEEEE00CBCBCB00BCAF9E00B0833E00E3AC5400E3AC5400E3AC5500E3AD
      5600E3AE5900E3AF5A00E3B05C00E3B05C00E3B05C00E3B05B00E3AE5800E3AD
      5600E3AC5500E3AC5400E3AC5400E3AC5400AD803D00BCAF9E00CBCBCB00EEEE
      EE0073757500F2F2F20000000000000000000000000000000000000000000000
      0000AFC5B800393F82005F68AE00CEE1F500739AFB00285CF7002054F6001F53
      F5004773F600B4CFFC005A64AE000F2F77001F83610013247D008FA2D700A3C0
      FB002852F0001944EE001843ED001A43EC00688BF50099B1EA00262B8C005862
      8D00000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FDFDFD00FCFCFC00FBFBFB00FBFBFB00FBFB
      FB00FAFAFA00FAFAFA00F9F9F900F9F9F900F8F8F800F8F8F800848484008383
      8300848484008484840084848400848484008484840084848400848484008686
      8600000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FDFDFD00FCFCFC00FBFBFB00FBFBFB00FBFB
      FB00FAFAFA00FAFAFA00F9F9F900F9F9F900F8F8F800F8F8F800848484008383
      8300848484008484840084848400848484008484840084848400848484008282
      8200000000000000000000000000000000000000000000000000FCFCFC00C6C6
      C6007A7B7B00747878007478780080776700A17D4800AD803D00AD803D00AD80
      3D00AD803D00AD803D00AD803D00AD803D00AD803D00AD803D00AD803D00AD80
      3D00AD803D00AD803D00AD803D00A67D41009C7A490078787500747878007A7B
      7B00C6C6C600FCFCFC0000000000000000000000000000000000000000000000
      000036368F00434A9D00DEF2FF00A0C0FD00235BFA00235AF9002359F8004A78
      F900BED3F4005E67AE0010416B0031CB70000000000029AA6800182680007D8C
      C800A7C3FB003F68F3001A47EF001946EE00224CEF0088A8F900A0B7E8002125
      8A00ADADD1000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FEFEFE00FDFDFD00FDFDFD00FCFCFC00FCFC
      FC00FBFBFB00FBFBFB00FAFAFA00FAFAFA00F9F9F900F9F9F90084848400C9C9
      C900E1E1E100E1E1E100E1E1E100E1E1E100DDDDDD00B1B1B10087878700D6D6
      D600000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FEFEFE00FDFDFD00FDFDFD00FCFCFC00FCFC
      FC00FBFBFB00FBFBFB00FAFAFA00FAFAFA00F9F9F900F9F9F90084848400C9C9
      C900E1E1E100E1E1E100E1E1E100E1E1E100DDDDDD00B1B1B10080808000B9B9
      B900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FDFDFD00F1F1F100EFEF
      EF00EEEEEE00EDEDED00EBEBEB00EAEAEA00E9E9E900E7E7E700E6E6E600E6E6
      E600E6E6E600E6E6E600FDFDFD00787C7C00DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008080B800272A8A00B9C9E700CEE5FE005887FC00245DFA004B7BFB00BAD5
      FD005F68AE001B317E003CCC7700000000000000000000000000409379002427
      870095A6D700B1CCFC003964F2001B48F0006589F600BAD5FD008698D3001B1D
      8300CDCDE3000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FEFEFE00FEFEFE00FDFDFD00FDFDFD00FCFC
      FC00FCFCFC00FBFBFB00FBFBFB00FBFBFB00FAFAFA00FAFAFA0084848400C9C9
      C900E1E1E100E1E1E100E1E1E100E1E1E100B1B1B10087878700D6D6D600FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FEFEFE00FEFEFE00FDFDFD00FDFDFD00FCFC
      FC00FCFCFC00FBFBFB00FBFBFB00FBFBFB00FAFAFA00FAFAFA0084848400C9C9
      C900E1E1E100E1E1E100E1E1E100E1E1E100B1B1B10084848400B9B9B900FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FFFFFF00F3F3F300F1F1
      F100F0F0F000EFEFEF00EDEDED00ECECEC00EAEAEA00E9E9E900E7E7E700E6E6
      E600E6E6E600E6E6E600FFFFFF0074787800DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFC008E8EBA002C308D00B4C4E400CCE3FE007AA2FD00BFD4F4005F68
      AE0043478C0073D39B000000000000000000000000000000000000000000ADB8
      BE00111D7800828FC800BBD5FD0096B4FA00CBE2FC008D9DD30023248600B0B0
      CB00000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFE00FEFE
      FE00FDFDFD00FDFDFD00FCFCFC00FCFCFC00FBFBFB00FBFBFB0084848400C9C9
      C900E1E1E100E1E1E100DDDDDD00B1B1B10087878700D6D6D600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFE00FEFE
      FE00FDFDFD00FDFDFD00FCFCFC00FCFCFC00FBFBFB00FBFBFB0084848400C9C9
      C900E1E1E100E1E1E100DDDDDD00B1B1B10080808000B9B9B900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FFFFFF00F5F5F500F3F3
      F300F2F2F200F0F0F000EFEFEF00EEEEEE00EDEDED00EBEBEB00EAEAEA00E8E8
      E800E7E7E700E6E6E600FFFFFF0074787800DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F5F5F5006464A90032379100CBDEF300D8EEFF005F68AE000C20
      6D00D2DBD8000000000000000000000000000000000000000000000000000000
      000012564000202383009DACD700D8EDFF0092A2D3001F228500B0B0CB00FAFA
      FA00000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFE
      FE00FEFEFE00FDFDFD00FDFDFD00FCFCFC00FCFCFC00FBFBFB0084848400C9C9
      C900E1E1E100E1E1E100B1B1B10087878700D6D6D600FEFEFE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFE
      FE00FEFEFE00FDFDFD00FDFDFD00FCFCFC00FCFCFC00FBFBFB0084848400C9C9
      C900E1E1E100E1E1E100B1B1B10084848400B9B9B900FDFDFD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FFFFFF00F7F7F700F5F5
      F500F4F4F400F2F2F200F1F1F100F0F0F000EEEEEE00EDEDED00EBEBEB00EAEA
      EA00E9E9E900E7E7E700FFFFFF0074787800DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F5F5F5008080B20030348F004C54A300373E81001979
      3B00000000000000000000000000000000000000000000000000000000000000
      000000000000899B9A001B1D7D00626BB00024248600B0B0CB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FEFEFE00FEFEFE00FEFEFE00FDFDFD00FDFDFD0084848400C9C9
      C900DDDDDD00B1B1B10087878700D6D6D6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FEFEFE00FEFEFE00FEFEFE00FDFDFD00FDFDFD0084848400C9C9
      C900DDDDDD00B1B1B10080808000B9B9B9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FFFFFF00F9F9F900F7F7
      F700F6F6F600F5F5F500F3F3F300F2F2F200F1F1F100EFEFEF00EEEEEE00ECEC
      EC00EBEBEB00E9E9E900FFFFFF0074787800DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F5F5F5005C5CA1001B1B77008DA296000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005D5D740025258500B0B0CB00FAFAFA00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FEFEFE00FEFEFE00FEFEFE00FDFDFD0084848400C9C9
      C900B1B1B10087878700D6D6D600FEFEFE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FEFEFE00FEFEFE00FEFEFE00FDFDFD0084848400C9C9
      C900B1B1B10084848400B9B9B900FDFDFD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FFFFFF00FAFAFA00F9F9
      F900F8F8F800F7F7F700F5F5F500F4F4F400F2F2F200F1F1F100EFEFEF00EEEE
      EE00EDEDED00EBEBEB00FFFFFF0074787800DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484009D9D
      9D0087878700D6D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484009D9D
      9D0080808000B9B9B90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF00777A7A00FEFEFE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FEFEFE00777A7A00DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10084848400F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500EDEDED00828282008484
      8400D6D6D600FEFEFE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80084848400F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500EDEDED00828282008181
      8100B9B9B900FDFDFD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E5E5E5006A6A6A0075787800747878007478
      7800747878007478780074787800747878007478780074787800747878007478
      780074787800747878007478780071747400E5E5E50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F9F9F9008F8F8F0094949400949494009494940094949400949494009494
      940094949400949494009494940094949400949494009494940085858500E1E1
      E100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F4F4F4008B8B8B0091919100919191009191910091919100919191009191
      910091919100919191009191910091919100919191009191910085858500CBCB
      CB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F2F7F200F3F6F300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F3F3F300F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1
      F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1
      F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1
      F100FAFAFA000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E0E0F4009295
      D9004549BD00171BAC00070BA6000514BB000514BB000609A4001518A8004346
      BA009192D600DFE0F30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000096B496002981290082A08200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FEFEFE00F2F5FA00E2E8F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8
      F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8
      F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8F500EAEEF700FEFEFE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000868686009B9B9B00A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0
      A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0
      A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A0009696
      9600C7C7C7000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D1D2EF00575CC7000A13B200071D
      CB000328E4000131FA000033FF000033FF000033FF000033FF000030F9000226
      E3000419C600060CAA005456C000D0D0ED000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C0CDC0004295
      430013981E001CAD2A0080A08000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F0F3F9001E5FBA000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B400286FBF00C5D1EB000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094949400E1E1E100F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4
      F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4
      F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400CECE
      CE00C7C7C7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000777CD3000D18B6000C29D7000938FA000638
      FF000336FF000134FF000033FF000033FF000033FF000033FF000033FF000033
      FF000033FF000031F900041FD300070EAC007376CD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7F7F7008CB48C00238F2A0020AF
      32002AC5400020B5320081A18100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004D84C900A9EDF600A9FBFD009FF9FE0092F6FE0090F5FE008DF4FE008CF4
      FE008BF4FE008BF4FE008BF4FE008BF4FE008BF4FE008BF4FE008AF4FE008AF4
      FE008AF4FE008AF4FE0089F4FE008EF6FE00A5FCFE00A4FBFD003770C000FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E4E4E400F3F3F300F2F2F200F1F1F100F0F0F000EFEFEF00EEEE
      EE00EEEEEE00D6D6D600B1B1B100B0B0B000DADADA00EAEAEA00E9E9E900E8E8
      E800E7E7E700E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600EAEAEA00D1D1
      D100C7C7C7000000000000000000000000000000000000000000000000000000
      000000000000F0F0FA003C44C300172AC8001948FA001042FF000D3FFF000B3D
      FF00083AFF000538FF000235FF000033FF000033FF000033FF000033FF000033
      FF000033FF000033FF000033FF000131FA000617BE00373AB800EFEFF9000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F9F9F900BDD1BD002C8E30001FA52F0035D0500032CC
      4B0030C9480028BB3B0082A18200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD009DF8FE005FE6FF003EDDFF0035DAFF002AD7FF0021D5
      FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4
      FF001ED4FF001ED4FF001ED4FF0023D6FF007EF1FF00A5FCFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E4E4E400F3F3F300F3F3F300F2F2F200F1F1F100F0F0F000EFEF
      EF00EEEEEE00C9C9C90013131300131313007F7F7F00EAEAEA00E9E9E900E9E9
      E900E8E8E800E7E7E700E6E6E600E6E6E600E6E6E600E6E6E600EAEAEA00D1D1
      D100C7C7C7000000000000000000000000000000000000000000000000000000
      0000F0F0FA003E47C600263FD3002051FF001849FF001547FF001244FF001042
      FF000D3FFF000A3CFF00073AFF000537FF000235FF000033FF000033FF000033
      FF000033FF000033FF000033FF000033FF000033FF00061DCB00383CBA00EFF0
      F900000000000000000000000000000000000000000000000000000000000000
      000000000000F8F9F8006594650023952D0032C54C003BD458003AD3550037D0
      550036CF51002BBE4200407E400082A1820096B19600B3C7B300E8EBE8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD0093F5FE004BE0FF0034DAFF0029D7FF001BD3FF0010D0
      FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0003CCFF0055E5FF00A7FCFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E5E5E500F5F5F500F4F4F400F3F3F300F2F2F200F1F1F100F0F0
      F000F0F0F000EFEFEF00575757000101010024242400C0C0C000EAEAEA00EAEA
      EA00E9E9E900E8E8E800E7E7E700E6E6E600E6E6E600E6E6E600EAEAEA00D1D1
      D100C7C7C7000000000000000000000000000000000000000000000000000000
      00004149CA002942D6002858FF002051FF001D4EFF001A4CFF001749FF001546
      FF001244FF000F41FF000C3EFF00093CFF000739FF000437FF000134FF000033
      FF000033FF000033FF000033FF000033FF000033FF000033FF00071ECC00383E
      BD0000000000000000000000000000000000000000000000000000000000F6F6
      F6009BB09B00268C2C0030BF480042DB630042DB630042DB630041DA620040D9
      60003DD65C003CD559002DC046002BBF420021AF3400199F26000E8B16001E8A
      220072AF7300B4C4B400F6F6F600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD0097F5FE0055E3FF003FDDFF0034DAFF0027D6FF001CD3
      FF000ED0FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0055E5FF00A7FBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E5E5E500F5F5F500F5F5F500F4F4F400F3F3F300F2F2F200F1F1
      F100F0F0F000F0F0F000B5B5B5001C1C1C000303030079797900EBEBEB00EBEB
      EB00E9E9E900E9E9E900E8E8E800E7E7E700E6E6E600E6E6E600EAEAEA00D1D1
      D100C7C7C7000000000000000000000000000000000000000000000000007C84
      DD002E43D1003868FF002758FF002555FF002253FF001F50FF001C4DFF001A4B
      FF001748FF001446FF001143FF000F41FF000C3EFF00093BFF000639FF000336
      FF000134FF000033FF000033FF000033FF000033FF000033FF000033FF000A1C
      C400777BD3000000000000000000000000000000000000000000F5F8F5005D95
      5D0026A435003BCF590046DF680047E06A0047E06B0049E26D0048E16C0046DF
      670043DC64003ED75E003BD4590037D0530032CC4B002DC7450027C23C0020B8
      320016A3210015901B0053915300BDCEBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD009BF6FE0062E6FF004EE1FF0042DDFF0035DAFF0029D6
      FF001CD3FF0011D0FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0056E5FF00A8FBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E6E6E600F6F6F600F6F6F600F5F5F500F4F4F400F3F3F300F2F2
      F200F1F1F100F1F1F100ECECEC006E6E6E00000000000D0D0D00D9D9D900ECEC
      EC00EBEBEB00EAEAEA00E9E9E900E8E8E800E7E7E700E7E7E700EAEAEA00D1D1
      D100C7C7C7000000000000000000000000000000000000000000D3D6F4001E2E
      C9005480FB00416DFF004872FF00456FFF003462FF002455FF002152FF001F50
      FF001C4DFF00194AFF001648FF001445FF001143FF000E40FF000B3EFF00093B
      FF000638FF000336FF000134FF000033FF000033FF000033FF000033FF000131
      FA000E19B800D1D3F00000000000000000000000000000000000F3F6F300518F
      510027AC3B0041D763004CE572004EE7750050E9780050E979004FE876004DE6
      730049E26D0045DE680041DC62003CD75B0037D1530032CC4B002BC5410026BF
      390020B930001BB62A0013A91D0007850A00729F7200ECEEEC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD009FF7FE006DE9FF005AE4FF004EE1FF0041DDFF0036DA
      FF0029D7FF001DD3FF0010D0FF0006CDFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0057E5FF00A9FBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E6E6E600F7F7F700F7F7F700F5F5F500F5F5F500F4F4F400F3F3
      F300F2F2F200F1F1F100F0F0F000CECECE0006060600000000008A8A8A00ECEC
      EC00EBEBEB00EBEBEB00EAEAEA00E9E9E900E8E8E800E7E7E700EBEBEB00D1D1
      D100C7C7C7000000000000000000000000000000000000000000626CDA005870
      E4007596FF006F90FF006B8DFF00668AFF006286FF00ABBEFF00FFFFFF00FFFF
      FF007492FF001E4FFF001B4DFF00194AFF001647FF001345FF001042FF006887
      FF00FFFFFF00FFFFFF00829BFF000235FF000134FF000033FF000033FF000033
      FF000826DB005A61CC000000000000000000000000000000000000000000F3F3
      F30088BB8800228D2A0043D5650055F0810056EF810056EF820055EE7F0050E9
      7B004DE6740042D56200138A1D00138D1C00148F1F001B9C280020AE310026BF
      3A0023BD34001DB72B0016AF210011AB190009870C00579E5700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00A4F8FE007AEDFF0068E8FF005CE4FF004FE1FF0043DD
      FF0036DAFF002BD7FF001ED4FF0012D0FF0007CEFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0057E5FF00ABFBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E7E7E700F8F8F800F8F8F800F7F7F700F6F6F600F5F5F500F4F4
      F400F3F3F300F3F3F300F2F2F200F1F1F10069696900010101002E2E2E00CDCD
      CD00EDEDED00ECECEC00EBEBEB00EAEAEA00E9E9E900E9E9E900ECECEC00D1D1
      D100C7C7C70000000000000000000000000000000000E2E4F8002536D0008BAA
      FC007999FF007596FF007293FF006E90FF00698CFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007593FF002051FF001E4FFF001B4CFF00184AFF006D8CFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00073AFF000538FF000235FF000033FF000033
      FF000132FB00111DBE00E1E2F600000000000000000000000000000000000000
      000000000000EAEDEA00538A54002DA43D0055EA80005EF88E005BF4880056EF
      820051EB790040D4620083A0830000000000EFF3EF00CBD8CB0093AE93005C94
      5C003193350015951D0013A71E0012AB1B000CA61200038907008AAF8A000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00A7F9FE0085F0FF0074EBFF0068E8FF005BE4FF0050E1
      FF0043DEFF0037DBFF002AD7FF001FD4FF0011D0FF0007CEFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0058E5FF00ACFBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E7E7E700F9F9F900F8F8F800F7F7F700F7F7F700F6F6F600F5F5
      F500F4F4F400F3F3F300F2F2F200F2F2F200BABABA001C1C1C00060606008B8B
      8B00EDEDED00EDEDED00ECECEC00EBEBEB00EAEAEA00E9E9E900ECECEC00D1D1
      D100C7C7C700000000000000000000000000000000009CA4EB005167E2008EAC
      FF00809FFF007C9CFF007899FF007495FF007092FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007695FF002353FF002051FF007290FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000C3FFF000A3CFF000739FF000437FF000235
      FF000033FF000C26D500979CE200000000000000000000000000000000000000
      00000000000000000000F6F6F600A6C1A6002A95340045D0680060FB91005AF3
      850052EB7B0042D4640081A18100000000000000000000000000000000000000
      0000DBDFDB00A8C2A80028892A0007880D000CA6130007A00A002F892F00E0E4
      E000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00ACFAFE0092F3FF0082EFFF0076EBFF0069E8FF005EE4
      FF0051E1FF0045DEFF0038DBFF002CD7FF001FD4FF0014D1FF0008CEFF0002CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0059E5FF00ADFBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E8E8E800FAFAFA00FAFAFA00F9F9F900F8F8F800F7F7F700F6F6
      F600F5F5F500F4F4F400F4F4F400F3F3F300EEEEEE007C7C7C00000000001A1A
      1A00E1E1E100EEEEEE00EDEDED00ECECEC00EBEBEB00EAEAEA00EDEDED00D1D1
      D100C7C7C700000000000000000000000000000000005866DF007D97F2008CAA
      FF0086A5FF0083A2FF007F9EFF007B9BFF007798FF00A8BDFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007896FF007795FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF006C8CFF001243FF000F41FF000C3EFF00093CFF000639
      FF000436FF00082FEB004F58CF00000000000000000000000000000000000000
      000000000000000000000000000000000000EDEEED0079A779002CA13D0047D7
      6B0053EE7C0041D4620081A18100000000000000000000000000000000000000
      00000000000000000000FEFEFE00AFC4AF00228322000493070004890400B4BD
      B400000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00AFFBFE009DF6FF008EF2FF0082EFFF0075EBFF006AE8
      FF005DE5FF0051E2FF0044DEFF0039DBFF002BD7FF0020D4FF0013D1FF0009CE
      FF0001CCFF0001CCFF0001CCFF0002CCFF0059E5FF00AEFBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E8E8E800FBFBFB00FAFAFA00F9F9F900F9F9F900F8F8F800F7F7
      F700E2E2E2008F8F8F007A7A7A00797979007979790065656500000000000000
      000092929200EFEFEF00EDEDED00EDEDED00ECECEC00EBEBEB00EEEEEE00D1D1
      D100C7C7C700000000000000000000000000000000003043DA00A1BDFC008EAB
      FF008CAAFF0089A7FF0085A4FF0082A1FF007E9DFF007A9AFF00AABFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007190FF00194BFF001648FF001445FF001143FF000E40FF000B3E
      FF00093BFF000737FB002531C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000ADBEAD003992
      3C0033BC4D003FD4600080A08000000000000000000000000000000000000000
      000000000000000000000000000000000000CAD5CA00338B330000850000ACB6
      AC00000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B4FCFE00A9F9FF009CF6FF0090F2FF0083EFFF0078EB
      FF006BE8FF005FE5FF0052E2FF0046DEFF0039DBFF002ED8FF0021D4FF0015D1
      FF0009CEFF0002CCFF0001CCFF0002CCFF005AE5FF00B0FBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E9E9E900FCFCFC00FBFBFB00FAFAFA00FAFAFA00F9F9F900F8F8
      F800CECECE002929290000000000000000000000000000000000000000000000
      000050505000F0F0F000EFEFEF00EEEEEE00EDEDED00ECECEC00EFEFEF00D1D1
      D100C7C7C700000000000000000000000000000000002D42DE00A9C4FF0091AE
      FF0090ACFF008EABFF008CA9FF0088A6FF0084A3FF0081A0FF007D9DFF00ACC0
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF007695FF002152FF001E4FFF001B4DFF00194AFF001648FF001345FF001042
      FF000E40FF000B3DFF001927C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000082A882002388270082A38200000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EAEDEA001A7B1A00B3BC
      B300000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B8FCFE00B4FCFF00A8F9FF009CF6FF008FF2FF0084EF
      FF0077ECFF006CE9FF005EE5FF0053E2FF0046DFFF003ADBFF002DD8FF0022D5
      FF0015D1FF000ACFFF0001CCFF0002CCFF005BE5FF00B0FBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E9E9E900FDFDFD00FCFCFC00FBFBFB00FBFBFB00FAFAFA00F9F9
      F900DCDCDC006E6E6E0052525200525252005252520051515100515151005050
      500086868600F1F1F100EFEFEF00EFEFEF00EEEEEE00EDEDED00EFEFEF00D2D2
      D200C7C7C700000000000000000000000000000000004960E700ABC6FF0095B0
      FF0093AFFF0091ADFF008FABFF008DAAFF008BA8FF0087A5FF0083A2FF00809F
      FF00AEC2FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B99
      FF002959FF002656FF002354FF002151FF001E4FFF001B4CFF00184AFF001547
      FF001344FF001042FF001730D700000000000000000000000000000000000000
      0000FEFEFE000000000000000000000000000000000000000000000000000000
      0000FEFEFE00CED3CE00D1DED1000000000000000000FAFAFA00000000000000
      0000000000000000000000000000000000000000000000000000CBD7CB00E8EB
      E800000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B9FDFE00BCFEFF00B5FCFF00AAF9FF009EF6FF0093F3
      FF0086F0FF007AEDFF006DE9FF0062E6FF0054E3FF0049DFFF003CDCFF0030D9
      FF0023D5FF0018D2FF000CCFFF0006CCFF0060E7FE00B2FCFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000082829400C3C3DA00D4D4EB00D3D3EA00D2D2EA00D2D2E900D1D1E800D0D0
      E800CFCFE700CFCFE600CECEE600CECEE500CDCDE400CCCCE400CBCBE300CACA
      E200CACAE100C9C9E100C8C8E000C8C8DF00C7C7DE00C6C6DE00C8C8E000AFAF
      C600C1C1C700000000000000000000000000000000004B62EA00B1CAFF0098B2
      FF0096B1FF0094AFFF0092AEFF0090ACFF008EABFF008CA9FF008AA7FF0086A4
      FF00B1C4FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007D9B
      FF002E5EFF002B5BFF002859FF002656FF002353FF002051FF001D4EFF001A4C
      FF001849FF001547FF002C44DA00000000000000000000000000000000000000
      0000B9C2B9003F923F00F7F7F700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084A584003E933E00BDCB
      BD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400AAEBF600B9FCFD00BBFDFE00BAFCFE00B5FBFE00ACF9FE00A5F7
      FE009BF4FE0094F2FE008CF0FE0084EEFE007BEBFE0074EAFE006BE7FE0063E5
      FE005AE2FE0053E0FE0049DFFE0048DFFE009BF4FE00AEF5FA000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      00001E1E8E0027279C002A2AA0002A2AA0002A2A9F002A2A9F002A2A9F002929
      9F0029299F0029299F0029299E0029299E0029299E0029299E0028289E002828
      9E0028289E0028289E0028289D0028289D0028289D0027279D0028289D002323
      98009A9ACB00000000000000000000000000000000002A42E600B3CCFF009CB6
      FF0099B3FF0097B2FF0095B0FF0093AFFF0091ADFF0090ACFF008EABFF00B7C9
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF007F9DFF003060FF002D5DFF002B5BFF002858FF002556FF002253FF001F50
      FF001D4EFF001A4BFF001F30D100000000000000000000000000000000000000
      0000ACB6AC0008810C007DAB7D00FBFBFB000000000000000000000000000000
      0000000000000000000000000000000000000000000080A180000F9816001A88
      1D0088B28800F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004A83C80075AFDB00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FC
      FD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FC
      FD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD0076B2DC003A75C2000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000611A0002154EC002865FF002865FF002865FF003C73FF006D95FD00537F
      FB002A5EF8005980F8003360F4001B4BF1004F71F2004163EF004967EE002D4E
      EA001537E6004661EB004661EB001235E6001235E6001235E6001235E6000C23
      C8008D8DCC00000000000000000000000000000000003950EA00ACC5FE00A4BD
      FF009CB5FF009AB4FF0098B2FF0096B1FF0094B0FF0093AEFF00BACCFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00809EFF003262FF00305FFF002D5DFF002A5AFF002758FF002455
      FF002253FF003563FC002D3FD600000000000000000000000000000000000000
      0000B0B9B00017A22200169C220049994A00DADEDA0000000000000000000000
      0000000000000000000000000000000000000000000081A1810018AC25001BB6
      28000E95150021862200BBD0BB00F7F7F7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FEFEFE00316AAF000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4002A5EA100FCFCFC000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000611A0002154EC002865FF002865FF002865FF004278FF00A7BFFF00D6E1
      FE004574FA00C0CFFD0091A9F900426AF400DCE3FC007B93F400B1BFF800627A
      EF003C5AEA00D7DDFA00B0BBF6001235E6001235E6001235E6001235E6000C23
      C8008D8DCC00000000000000000000000000000000006275F0008CA5F900AEC6
      FF009FB7FF009DB6FF009BB4FF0099B3FF0098B2FF00BDCEFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B6C9FF00B5C7FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00829FFF003564FF003261FF002F5FFF002C5CFF002A5A
      FF002757FF00385FF3005968E100000000000000000000000000000000000000
      0000C9CFC90024922A002CC6420022AF34002C8A2E007DA47D00F8F9F8000000
      0000000000000000000000000000000000000000000081A181001DAF2B001EB7
      2E0019B3260013A71D001587180064946400F1F3F10000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD008E756A00F3DED700FDE9E200E9D2C8008E756A00FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000611A0002154EC002865FF002865FF002865FF002865FF003971FE00C9D8
      FE006C91FB007899FB00ECF0FE00CDD8FC00DFE5FC004568F100B1BFF9006981
      F100BDC7F800CDD4F900B0BBF6001235E6001235E6001235E6001235E6000C23
      C8008D8DCC0000000000000000000000000000000000A3AFF700637DF500B7CF
      FF00A3BBFF00A0B8FF009EB7FF009CB5FF009AB4FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00B9CBFF008EAAFF008CA9FF00B6C9FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF003A69FF003766FF003464FF003161FF002F5E
      FF003060FF003E5CE9009EA8EF00000000000000000000000000000000000000
      0000FCFCFC006A9E6A0026B63A0035CF500037D1530028B43D001B8A21005BA5
      5D00ACC5AC00CCD1CC00E7E9E700F9F9F9000000000082A1820022B5320024BD
      36001FB82F001BB4290015B020000E9E150019811A0097AD9700F6F6F6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FEFEFE008E756A00F4E1D800FEEBE400F2DED5008E756A00FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000611A0002154EC002865FF002865FF002865FF002865FF003A71FF00C9D8
      FF006D93FC003B6CF900D3DDFD008EA7F900BFCCFA002650EF00B2C0F900AAB8
      F700D1D8FA008597F200B0BBF6001235E6001235E6001235E6001235E6000C23
      C8008D8DCC0000000000000000000000000000000000E5E8FD003A56F300B2C9
      FE00AFC6FF00A3BBFF00A1B9FF009FB8FF009DB6FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00BCCDFF0093AEFF0091ADFF008FABFF008DAAFF00B7C9FF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF003F6DFF003C6BFF003968FF003666FF003363
      FF004E7BFD002B43E100E3E6FA00000000000000000000000000000000000000
      000000000000E9EAE9001E8521002FBF47003ED85D003ED85F003DD55C0035C5
      4F0029AD3D00329F3C003E9742004A954A00557E55002B702B0024B7380028C1
      3C0022BB35001FB82E0018B1260015AF1F000DA1140008880B005F985F00DFE7
      DF00000000000000000000000000000000000000000000000000000000000000
      000000000000FEFEFE008E756A00EFDDD500FEEDE700FDEAE4008E756A00DEDC
      DB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000611A0002154EC002865FF002865FF003065E9003265E5003B72FA00C9D8
      FF006D94FD00235BF900C3D1FD00E4EAFE007793F7001A48F000B2C1F900FBFC
      FE005B75EF006179EF00B3BDF1001E3CD2001D3BD4001436E2001235E6000C23
      C8008D8DCC000000000000000000000000000000000000000000667BF8007891
      FA00BAD1FF00AAC0FF00A4BBFF00A2BAFF00A0B9FF00CFDCFF00FFFFFF00FFFF
      FF00BFD0FF0097B2FF0096B0FF0094AFFF0092AEFF0090ACFF008EABFF00B8CA
      FF00FFFFFF00FFFFFF00A3BAFF004472FF004170FF003E6DFF003B6AFF00406F
      FF004A6DF1006B7BEB0000000000000000000000000000000000000000000000
      00000000000000000000D3DFD300428943002EB9460044DC660048E16C0047E1
      6B0048E26B0046E1690044DE660040DB61003CD85A0038D3540031CA4A002DC6
      440027C03B0022BB34001DB62A0018B1240012AB1B000DA71400069709000179
      010092B29200F9FAF90000000000000000000000000000000000000000000000
      000000000000FEFEFE008E756A00EFDDD500FEEDE700FDEAE4008E756A000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008E756A008E756A008E756A008E756A008E756A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000009119C002051E8002865FF003C64CB00586482005A647E00506DB60095B2
      FA005683FD00245CFA007395FA00B3C4FB003F68F4001B49F1007F98F5009BAD
      F7001B41E9004764EC009EA3B900555B7B00545B7D003247B3001235E6000B20
      C3008F8FCB000000000000000000000000000000000000000000D8DEFD003D5B
      F900B5CBFF00B9CFFF00A8BFFF00A5BCFF00A4BBFF00A2BAFF00A0B8FF009EB7
      FF009CB5FF009AB4FF0098B3FF0097B1FF0095B0FF0093AFFF0091ADFF008FAC
      FF008EAAFF007094FF004F7CFF004977FF004674FF004372FF004472FF006592
      FE002F49E800D6DBFA0000000000000000000000000000000000000000000000
      0000000000000000000000000000E5E6E50064A865001F92290042D5640050EA
      78004DE775004BE4710047E16A0043DC65003ED75D003AD3570033CC4E0030C9
      480029C23E0025BE38001EB72F001AB3270014AD1D000FA8170009A40D000393
      050019741900EEF5EE0000000000000000000000000000000000000000000000
      000000000000FEFEFE00C2AEA400E4D1C900FEEFE900FEEDE700D0B8AD00B7A8
      A100FEFEFE000000000000000000000000000000000000000000FEFEFE00D4D0
      CF00BA9D8F00F6DCD100FEE4DA00FDE2D700B99E9100E2E0DF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000045469F001028BB001A43D9004B598B00AAA7A700CBC7C700575E77002648
      C5001941D800183ED600173BD4001538D3001435D1001232D000112ECE00102C
      CC000E28CB000D26C9006F6F7400C0BDBD00AFACAC00575C7D000B21C500040B
      A100B9B9D8000000000000000000000000000000000000000000000000008E9F
      FD005875FD00BFD4FF00BAD0FF00AAC0FF00A6BDFF00A5BCFF00A3BAFF00A1B9
      FF009FB8FF009DB6FF009BB5FF009AB3FF0098B2FF0096B1FF0094B0FF0092AE
      FF0091ADFF008EABFF0089A7FF007397FF005E87FF005882FF006E98FF00415F
      F0008896F2000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E9ECE9006C986C003F99
      450034B1480038C355003DCF5D003FD35F003ED45D0039CF570038D2540033CC
      4C002DC6430027C03B0021BA31001CB52B0015AF21000FA31600108613005B8E
      5B00E9ECE9000000000000000000000000000000000000000000000000000000
      00000000000000000000D1C8C400C9B2A700FDF0EA00FEF0EA00F8E8E200C8AF
      A300B1A29A00D6D4D300F1F1F100FEFEFE00F6F5F500E4E2E200B6ADA900B498
      8900F4DCD300FEE7DE00FEE6DD00EAD0C500B3A09700FBFBFB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BFBFCC005E5EAC0042429D0056567800B7B4B400DDD9D9005E5E6D004747
      940042429D0042429D0042429D0042429D0042429D0042429D0042429D004242
      9D0042429D0042429D0079787800CBC8C800BEBBBB006767760044449F007070
      B200F0F0F1000000000000000000000000000000000000000000000000000000
      00005D76FE006B87FF00C0D5FF00BBD0FF00ACC2FF00A8BEFF00A6BDFF00A4BB
      FF00A2BAFF00A0B9FF009FB7FF009DB6FF009BB4FF0099B3FF0097B2FF0095B0
      FF0093AFFF0092AEFF0090ACFF008EABFF008EABFF00A0BEFF005B77F500556B
      F100000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7EA
      E700BCC3BC008FB38F0060A86300479E4B0035983B002088260034C84D0035CE
      4F002DC6440029C23E0022BB34001FBA2D00109A180019841B00B2CAB200F6F6
      F600000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EAE9E800B59A8F00F5E9E300FEF1EC00FEF1EC00F9EB
      E500D8C5BB00C0A69C00AC978D0097877F00B59D9000BBA19500D0B8AD00F0DB
      D200FEE9E100FEE8E000F9E3DA00BD9F9200DFDBD80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000081818100BBB9B900E3DFDF007E7E7E00DADA
      DA00000000000000000000000000000000000000000000000000000000000000
      000000000000000000007A797900D1CECE00C2C0C00086868600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F2F4FF005D77FF006B87FF00C1D5FF00BED3FF00B2C7FF00A9BFFF00A7BD
      FF00A5BCFF00A4BBFF00A2BAFF00A0B8FF009EB7FF009CB5FF009AB4FF0098B3
      FF0096B1FF0095B0FF0093AFFF009AB6FF00A7C4FF005D7AF800576EF400F2F3
      FE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000083A1830031C54A0036CF
      50002FC847002BC540001CAB2A001B8C1F007CAA7C00F0F1F000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D4D0CF00B89E9000F3E7E200FEF3EE00FEF2
      ED00FEF1EC00FDEFEA00FCEEE800FCEDE700FCECE600FDECE600FEECE500FEEC
      E500FEEBE400FDE9E200CCB2A500B6A7A000FEFEFE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000081818100BFBDBD00E8E5E5007E7E7E00DADA
      DA00000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7A7A00D5D3D300C6C4C40086868600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F2F4FF005D77FF005A78FF00B9CEFF00C0D5FF00BCD2FF00B3C8
      FF00AAC1FF00A6BDFF00A4BCFF00A3BAFF00A1B9FF009FB8FF009DB6FF009BB5
      FF009BB5FF00A0BBFF00ABC6FF00A5C0FF00516EF9005970F700F2F3FE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000081A1810031C54A0036CF
      51002EC946001BA42A00368E3800B5C4B5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FEFEFE00C9B8B000B9A09300F8EEEA00FDF2
      EE00FEF3EE00FEF2ED00FEF1EC00FFF1EC00FEF0EA00FEEFE900FEEEE800FEED
      E600FBE9E200D3BBAF00B9A29600E9E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000081818100C2C2C200ECEAEA007E7E7E00DADA
      DA00000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7A7A00D9D8D800CAC9C90086868600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008FA1FF004160FF007D97FF00B8CEFF00C0D5
      FF00BED4FF00B9CFFF00B5CCFF00B0C7FF00AEC5FF00ADC4FF00AFC7FF00B0C9
      FF00B3CCFF00AAC4FF007390FE003D5CFB008D9FFC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000081A1810030C3480029BA
      3F00258A29008AAD8A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FBFBFB00D4CECB00B49A8E00D0BD
      B400EBDDD700F6EAE500FDF2ED00FDF1ED00FDF1EC00F9ECE600EEDDD600DFCA
      C100B99E9200B9ABA400F9F8F800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008686860098989800B1B0B00084848400E3E3
      E300000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B00A7A7A7009A99990094949400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D9DFFF00768CFF004160FF006B87
      FF0096AEFF00B7CCFF00BED3FF00BCD2FF00BBD1FF00BAD1FF00B0C8FF008FA9
      FF006784FF00405FFF006981FF00D9DFFE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007F9F7F00118D1C004194
      4300D1D5D100FEFEFE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E2E0DF00D9D1
      CE00C5B1A800BBA39700B89E9100B89E9100B89D9000B5998B00C3ADA300D4C6
      C000D7D5D400F7F7F70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F4F4F400C2C2C200BCBCBC00E3E3E3000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E5E5E500BFBFBF00C6C6C600EFEFEF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E6EAFF00A7B5
      FF006981FF004462FF004060FF005977FF005977FF003858FF004462FF006981
      FF00A7B5FF00E6EAFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C9D7C900C5DDC500FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EAEAEE009E9ED7009595D300C4C4
      E600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000070AA7000016C010002760300017602000175020001750100006B000070AA
      7000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DCDCE0002E2FAB002733C100303FD1001319
      B1005757BA00E1E1EA0000000000000000000000000000000000000000000000
      0000FDFDFD00E7ECF600CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9
      EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9
      EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00DAE1F100FDFDFD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BFD9
      BF00027003000AA00F0009A20E0008A10B00069F0900059E070003990500006F
      0100AFCFAF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DCDCE0004747A700313FC300607DF7005A75FF00475C
      F300191FB4005757BA0000000000000000000000000000000000000000000000
      0000E4E9F4001156B6000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B400236CBD009FB3DE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000080B5
      8100088A0D000EA715000CA512000BA4100009A20E0008A10B00069F09000383
      04007FB37F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DCDCE0002F30AB003A4BC3006C90F800234BD2000130B9001D44
      D1004154F3001116B100C4C4E600000000000000000000000000000000000000
      00003272C100A9EDF600A9FBFD009FF9FE0092F6FE0090F5FE008DF4FE008CF4
      FE008BF4FE008BF4FE008BF4FE008BF4FE008BF4FE008BF4FE008AF4FE008AF4
      FE008AF4FE008AF4FE0089F4FE008EF6FE00A5FCFE00A3FBFD002765BB00FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000081B7
      82000B8D110011AA1A0010A917000EA715000CA512000BA4100009A20E000484
      06007FB37F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DCDCE0004747A7003E51C30080A6F600284CDC00022BC400002FB9000130
      B8004D64FF002733D1009595D300000000000000000000000000000000000000
      00000053B400B9FCFD009DF8FE005FE6FF003EDDFF0035DAFF002AD7FF0021D5
      FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4
      FF001ED4FF001ED4FF001ED4FF0023D6FF007EF1FF00A5FCFD000053B400FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000082B9
      84000E91160015AE1F0013AC1D0011AA1A0010A917000EA715000CA512000686
      08007FB37F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DCDC
      E0002F30AB003E51C3007FA8FB002D4CE500001FD0000026C6000230BC001D47
      CE004E65F7001E27C1009E9ED700000000000000000000000000000000000000
      00000053B400B9FCFD0093F5FE004BE0FF0034DAFF0029D7FF001BD3FF0010D0
      FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0003CCFF0055E5FF00A7FCFD000053B400FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000083BB
      860011951A0019B2250017B0220015AE1F0013AC1D0011AA1A0010A917000888
      0C007FB37F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DCDCE0004747
      A7003E51C30082AAF7002D46EC00031ADE00001ED3000024C900224AD2005979
      F6002632C3002D2EAB00EAEAEE00000000000000000000000000000000000000
      00000053B400B9FCFD0097F5FE0055E3FF003FDDFF0034DAFF0027D6FF001CD3
      FF000ED0FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0055E5FF00A7FBFD000053B400FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000084BD
      880014991F001DB62B001BB4280019B2250017B0220015AE1F0013AC1D000A8C
      0F0080B581000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DCDCE0002F30AB003E51
      C3007FA6FD002D42F3000010E7000015E000031FD600294DDD006C8DF6002F3D
      C3004747A700DCDCE00000000000000000000000000000000000000000000000
      00000053B400B9FCFD009BF6FE0062E6FF004EE1FF0042DDFF0035DAFF0029D6
      FF001CD3FF0011D0FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0056E5FF00A8FBFD000053B400FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000085BF
      8A00179D250021BA31001FB82E001DB62B001BB4280019B2250017B022000D8F
      140081B682000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EBE7E300CFC0AA00CFC0
      AA00CFC0AA00D7CAB800F0EDEA0000000000DCDCE0004747A7003E51C30082A9
      F7002D3EF800030DF100000EE9000013E2002D4CE5007BA3F9003647C3002E2F
      AB00DCDCE0000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD009FF7FE006DE9FF005AE4FF004EE1FF0041DDFF0036DA
      FF0029D7FF001DD3FF0010D0FF0006CDFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0057E5FF00A9FBFD000053B400FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000086C2
      8C001BA22A0025BE380023BC350021BA31001FB82E001DB62B001BB428001093
      180081B884000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F3F3
      F800BFD0D1007D9EBB0095C5D1009BC6D1009FC4D1009EC3D1009EC3D1004995
      6F00C7E1DD007AD8AB005DD599005DD599005DD599005DD5990077D8AA00BDE1
      D60047956E0086BAD10087BBD10087BCD10095C5D10095C5D1007D9EBB00BFD0
      D100C0D1D100FCFCFD0000000000000000000000000000000000000000000000
      00000000000000000000E0DDD800C3A88300B87D2600B4710B00BA780F00BB78
      0F00BA770C00B7740900B36F0B00BD873900614B79004550B20083ABFE002F3E
      FC000004F8000007F3000310EC002D46ED0082AAF6003E50C3004747A700DCDC
      E000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00A4F8FE007AEDFF0068E8FF005CE4FF004FE1FF0043DD
      FF0036DAFF002BD7FF001ED4FF0012D0FF0007CEFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0057E5FF00ABFBFD000053B400FDFD
      FD0000000000000000000000000000000000000000000000000000000000B8E2
      BF008DCF99008CCD97008BCC96008BCA95008AC9930089C8920089C691004DA8
      58001EA730002AC33E0027C03B0025BE380023BC350021BA31001FB82E001397
      1D0045994A0082B9850082B8840081B7830081B7820080B6810080B581007FB4
      8000CFE2CF000000000000000000000000000000000000000000000000003542
      8F0020289100303CA1002E3BA1002D3AA1002B39A1002A38A1002837A1002735
      A1002534A1002433A1002232A1002131A1001F2FA1001E2EA1001C2DA1001B2C
      A100192BA100182AA1001629A1001528A1001427A1001225A1001124A1000F23
      A100030B7F00C0D1D10000000000000000000000000000000000000000000000
      0000FCFCFC00D5C8B500B9802E00BC7D1900D2992F00DBA33600DEA63500DEA5
      3200DCA02B00D89B2400D0901A00C7851200AB690B009D7643007D92DB001921
      FD000004F9000007F5002D42F3007FA7FC003E51C3002F30AB00DCDCE0000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00A7F9FE0085F0FF0074EBFF0068E8FF005BE4FF0050E1
      FF0043DEFF0037DBFF002AD7FF001FD4FF0011D0FF0007CEFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0058E5FF00ACFBFD000053B400FDFD
      FD0000000000000000000000000000000000000000000000000082CF920025AE
      400031C14F002FBE4C002DBB49002BB845002AB6420027B33F0026B13C0024AE
      390029BA3F002EC745002CC542002AC33E0027C03B0025BE380023BC35001CAB
      2A001499200013971D0011951B00109318000E9116000D8F14000B8D12000A8C
      10000473060070AC71000000000000000000000000000000000000000000252C
      91006390FD006390FD006390FD006390FD006390FD006390FD006390FD006390
      FD006390FD006390FD006390FD006390FD006390FD006390FD006390FD006390
      FD006390FD006390FD006390FD006390FD006390FD006390FD006390FD006390
      FD001225A100BFD0D1000000000000000000000000000000000000000000FCFC
      FC00D4B18000B6771700D7A33F00E8B85200E7B64E00E6B44900E4B04300E3B1
      4800E1AB3D00DFA53200DCA12C00DB9E2700D6971E00C3801000A87224007F84
      B7001923FB002F41F90082AAF7003E51C3004747A700DCDCE000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00ACFAFE0092F3FF0082EFFF0076EBFF0069E8FF005EE4
      FF0051E1FF0045DEFF0038DBFF002CD7FF001FD4FF0014D1FF0008CEFF0002CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0059E5FF00ADFBFD000053B400FDFD
      FD0000000000000000000000000000000000000000000000000027B4460049E1
      6E0049E26E0046DF6A0044DD660041DA62003FD85E003CD55B003AD3570038D1
      530035CE500033CC4C0030C949002EC745002CC542002AC33E0027C03B0025BE
      380023BC350021BA31001FB82E001DB62B001BB4280019B2250017B0220015AE
      1F0012A81B00047106000000000000000000000000000000000000000000252C
      910093B5FC001C4AF1001A48EF001A48EF001945EE001843ED001740EC00163E
      EA00153BE9001439E8001337E7001235E6001235E6001235E6001235E6001235
      E6001235E6001235E6001235E6001235E6001235E6001235E6002047EC004579
      FD001729A100BFD0D1000000000000000000000000000000000000000000CDBD
      A700B6781800DBAA4A00EDC26000EBBF5C00EBBF6000F0D08B00F6E4BD00F7E9
      C900F6E5C300F2DBAC00E7BC6700DFA63600DCA02A00DA9C2400C27F1000A872
      24007D92DA0083ABFE003E51C3002F30AB00DCDCE00000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00AFFBFE009DF6FF008EF2FF0082EFFF0075EBFF006AE8
      FF005DE5FF0051E2FF0044DEFF0039DBFF002BD7FF0020D4FF0013D1FF0009CE
      FF0001CCFF0001CCFF0001CCFF0002CCFF0059E5FF00AEFBFD000053B400FDFD
      FD0000000000000000000000000000000000000000000000000032C5560051EA
      79004EE775004BE4720049E26E0046DF6A0044DD660041DA62003FD85E003CD5
      5B003AD3570038D1530035CE500033CC4C0030C949002EC745002CC542002AC3
      3E0027C03B0025BE380023BC350021BA31001FB82E001DB62B001BB4280019B2
      250017B0220009800E000000000000000000000000000000000000000000252C
      9100A3C2FD001E50F4001D4DF3001D4DF3001C4BF1001B49F0001A46EF001944
      EE001841EC00163FEB00153CEA00143AE9001337E7001236E6001235E6001235
      E6001235E6001235E6001235E6001235E6001235E6001235E600254BEC005686
      FD001D2EA100BFD0D10000000000000000000000000000000000DCD3C800B77C
      2600E2B65900F1CB6E00F0C96D00F3D59100FCF6E300FFFCEA00FFFEE700FFFF
      E600FFFFE700FFFFEA00FEFDF000F7EACC00E5B65800DEA53300DBA02A00C584
      16009D7643004550B2004747A700DCDCE0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B4FCFE00A9F9FF009CF6FF0090F2FF0083EFFF0078EB
      FF006BE8FF005FE5FF0052E2FF0046DEFF0039DBFF002ED8FF0021D4FF0015D1
      FF0009CEFF0002CCFF0001CCFF0002CCFF005AE5FF00B0FBFD000053B400FDFD
      FD0000000000000000000000000000000000000000000000000036CB5C0056EF
      810053EC7D0051EA79004EE775004BE4720049E26E0046DF6A0044DD660041DA
      62003FD85E003CD55B003AD3570038D1530035CE500033CC4C0030C949002EC7
      45002CC542002AC33E0027C03B0025BE380023BC350021BA31001FB82E001DB6
      2B001BB428000B8412000000000000000000000000000000000000000000252C
      9100B0CDFD002055F6001F52F5001F52F5001E50F4001D4DF2001C4BF1001B49
      F0001A46EF001844ED001741EC00163FEB00153CEA00143AE8001337E7001236
      E6001235E6001235E6001235E6001235E6001235E6001235E600284EEC006390
      FD002232A100BFD0D10000000000000000000000000000000000CFA87000C68E
      3000F5D17900F3CF7600F6DC9D00FDF5DC00FFF6CF00FFF8D200FFFBDE00FFFE
      E500FFFFE700FFFFE700FFFFE700FFFFEC00F8EDD300E6B95C00DFA73500DBA0
      2E00AC6B0F00614B7900DCDCE000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B8FCFE00B4FCFF00A8F9FF009CF6FF008FF2FF0084EF
      FF0077ECFF006CE9FF005EE5FF0053E2FF0046DFFF003ADBFF002DD8FF0022D5
      FF0015D1FF000ACFFF0001CCFF0002CCFF005BE5FF00B0FBFD000053B400FDFD
      FD000000000000000000000000000000000000000000000000003AD163005BF4
      8A0059F2850056EF810053EC7D0051EA79004EE775004BE4720049E26E0046DF
      6A0044DD660041DA62003FD85E003CD55B003AD3570038D1530035CE500033CC
      4C0030C949002EC745002CC542002AC33E0027C03B0025BE380023BC350021BA
      31001FB82E000E8816000000000000000000000000000000000000000000252C
      9100BDD7FE00235AF9002258F8002258F8002155F7002053F6001F50F4001D4E
      F3001C4BF2001B49F1001A47EF001944EE001842ED001740EC00163DEA00143B
      E9001338E7001236E6001235E6001235E6001235E6001235E6002D51EC00749D
      FD002836A100BFD0D100000000000000000000000000F6F5F300B6761700E8C0
      6900F7D68100F7DA8F00FEF7E000FFF1C000FFF8DE00FFF6D200FFF8D200FFFB
      DB00FFFEE500FFFFE600FFFFE700FFFFE700FFFFEC00F8ECCF00E4B04600E2AC
      3C00CC902400BD88390000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B9FDFE00BCFEFF00B5FCFF00AAF9FF009EF6FF0093F3
      FF0086F0FF007AEDFF006DE9FF0062E6FF0054E3FF0049DFFF003CDCFF0030D9
      FF0023D5FF0018D2FF000CCFFF0006CCFF0060E7FE00B2FCFD000053B400FDFD
      FD000000000000000000000000000000000000000000000000003ED86A0061FA
      92005EF78E005BF48A0059F2850056EF810053EC7D0051EA79004EE775004BE4
      720049E26E0046DF6A0044DD660041DA62003FD85E003CD55B003AD3570038D1
      530035CE500033CC4C0030C949002EC745002CC542002AC33E0027C03B0025BE
      380023BC3500108C1B000000000000000000000000000000000000000000252C
      9100BFD9FE00255FFC00245DFA00245DFA00235AF9002258F8002155F7001F53
      F5001F50F4001D4EF3001C4CF2001B49F0001A47EF001944EE001842ED00163F
      EB00163DEA00143BE8001338E8001236E6001235E6001235E6003154EC0081A8
      FD002D3AA100BFD0D100000000000000000000000000ECDDC900C0852600F5D5
      8200F9D98700FBEBC100FFF0C100FFF4D100FFFBEC00FFF3C300FFF5C900FFF8
      D200FFFBDE00FFFEE500FFFFE700FFFFE700FFFFE700FEFDF100ECC67600E4B2
      4600D8A13500B4731000F0EDEA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400AAEBF600B9FCFD00BBFDFE00BAFCFE00B5FBFE00ACF9FE00A5F7
      FE009BF4FE0094F2FE008CF0FE0084EEFE007BEBFE0074EAFE006BE7FE0063E5
      FE005AE2FE0053E0FE0049DFFE0047DFFE009BF4FE00ADF5FA000053B400FDFD
      FD0000000000000000000000000000000000000000000000000038D5650062FC
      950064FD950061FA92005EF78E005BF48A0059F2850056EF810053EC7D0051EA
      79004EE775004BE4720049E26E0046DF6A0044DD660041DA62003FD85E003CD5
      5B003AD3570038D1530035CE500033CC4C0030C949002EC745002CC542002AC3
      3E0026BD39000E861A000000000000000000000000000000000000000000252C
      9100C7E0FF005587FE005485FD005485FD005384FC005282FC005180FB00517E
      FA00507CF8004F7BF7004E78F7004D77F6004B74F5004972F400476EF300456C
      F2004369F1004167F0003F63EF003D61EE003B5EED00395CEC005274F10095B8
      FD00333FA100BFD0D100000000000000000000000000E0C6A300D5A54C00FBDF
      8F00FBE09700FDF3D500FFECB100FFFBEF00FFF9E500FFEEB500FFF2BF00FFF4
      C700FFF8D200FFFBDB00FFFEE500FFFFE600FFFFE700FFFFEA00F6E2B700E8B9
      5100E3B24800BB7B1500D7CAB800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000286CBE006DAAD900B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FC
      FD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FC
      FD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD0073B0DB001D60B9000000
      00000000000000000000000000000000000000000000000000008FE9A9003DDB
      6C004EE97E004DE77C004AE4780048E1750046DE710044DB6E0042D86A003FD5
      660048DF700051EA79004EE775004BE4720049E26E0046DF6A0044DD660038CC
      57002DBC48002BB9450029B6420027B43F0025B13C0023AF390022AC360020AA
      33001491230078BB7F000000000000000000000000000000000000000000252C
      9100C7E0FF00919FD0008595D0008495D0008495D0008495D0008494D0008494
      D0008494CF008494CF008493CF008393CF008292CF008090CF007D8ECF007A8C
      CF007789CF007588CE007285CE007083CE006D81CE006A7FCE006B7FCE00C7E0
      FF00252C9100BFD0D100000000000000000000000000D9BB9000DFB55F00FDE1
      9300FDE5A300FEF2CD00FFEDB700FFFDF700FFF9E800FFEDB300FFEFB700FFF2
      BF00FFF5C900FFF8D200FFFBDE00FFFEE500FFFFE700FFFFE700F9EBCC00EBBF
      5E00E9BB5400BE801B00CFC0AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FDFDFD00094DA0000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4000E499400F9F9F9000000
      000000000000000000000000000000000000000000000000000000000000D9F8
      E2009BEBB2009AE9B00099E7AF0098E5AD0097E4AC0096E2AA0096E0A90060D0
      7D0042D86A0056EF810053EC7D0051EA79004EE775004BE4720049E26E0033C4
      520057BB69008DCF99008CCE98008CCC96008BCB95008AC9940089C8920089C7
      9100B4DBB9000000000000000000000000000000000000000000000000003E52
      900036439000252C9100252C9100252C9100252C9100252C9100252C9100252C
      9100252C9100252C9100252C9100252C9100252C9100252C9100252C9100252C
      9100252C9100252C9100252C9100252C9100252C9100252C9100252C9100252C
      910035438F00CADED400000000000000000000000000D9B98D00E2B96500FEE4
      9700FEE7A700FEF2CC00FFEDB900FFFEFC00FFFDF700FFF0C200FFECAF00FFEE
      B500FFF2BF00FFF4C700FFF8D200FFFBDB00FFFEE500FFFFE600FAEFD200EEC7
      6C00ECC15F00BF821F00CFC0AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FBFBFB00896F6300F3DED700FDE9E200E9D2C800896F6300FDFD
      FD0000000000000000000000000000000000000000000000000000000000FEFE
      FE00CBC8C700CAADA000FCDED200FCDDD100CAAB9E00CBC8C700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000097E2
      AB0046DE71005BF48A0059F2850056EF810053EC7D0051EA79004EE7750037C9
      590090D59E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DDC29C00D9AC5500FEE5
      9900FEE7A500FFF4D300FFEBB000FFFCF300FFFFFF00FFF9E700FFEBAC00FFEC
      AF00FFEFB700FFF2BF00FFF5C900FFF8D200FFFBDE00FFFEE700FAECCA00F0C9
      6C00EDC36400BE811E00CFC0AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD00896F6300F4E1D800FEEBE400F2DED500896F6300FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      0000C0BCBA00D0B2A400FEE1D500FDDFD300CBAC9E00CCC9C800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000098E6
      AE004BE4790061FA92005EF78E005BF48A0059F2850056EF810053EC7D003CCF
      600091D8A1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E8D5BB00C6903400FDE3
      9600FFE69B00FFF4D600FFEBB000FFF6DB00FFFFFF00FFFFFF00FFFAEB00FFF1
      C500FFEFB800FFF0BC00FFF4C800FFF6D400FFF8D200FFFCEA00F8E0A500F3CE
      7400E8BD6100B7771500EDE7DF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD00896F6300EFDDD500FEEDE700FDEAE400896F6300C2BE
      BD0000000000000000000000000000000000000000000000000000000000FEFE
      FE00BEA79C00E1C4B800FEE3D800FDE1D600C5A89A00D2CFCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009AE9
      B1004EE97E0065FE980064FD950061FA92005EF78E005BF48A0059F2850040D5
      670093DBA4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F6EFE500B97C1C00F2D3
      8300FFE69A00FFEAAD00FFF5D600FFEBB000FFFCF600FFFFFF00FFFFFF00FFFD
      F900FFFAEC00FFFAEC00FFFBEF00FFF7D900FFF6CF00FEF8E700F7D78600F5D2
      7B00DFB25600B97E2900FCFCFC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD0096786A00E4D1C900FEEFE900FEEDE700CCB3A800B7A8
      A100FEFEFE000000000000000000000000000000000000000000FEFEFE00D4D0
      CF00B2948500F6DBD000FEE4DA00FDE1D600A2827300E2E0DF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009CED
      B40050ED810066FF990066FF990065FE980064FD950061FA92005EF78E0044DB
      6E0095DEA7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C28F4800D6A8
      5100FFE69A00FFE69A00FFF2CB00FFF2CA00FFEBAD00FFF4D600FFFBEE00FFFC
      F600FFFCF200FFF9E800FFF1C300FFF0BD00FEF7DF00FBE5AA00F9D98500F6D6
      8100C0862700C3A8830000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000A7918600C8B0A400FDF0EA00FEF0EA00F8E8E200C7AE
      A200B1A29A00D6D4D300F1F1F100FEFEFE00F6F5F500E4E2E200B5ACA800AF93
      8400F4DCD300FEE7DE00FEE6DD00EAD0C50096786A00DEDCDC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009EF0
      B80051F0850066FF990066FF990066FF990066FF990065FE980064FD950049E1
      750096E2AA000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CBBFAE00B77A
      1F00F5D78900FFE69A00FFE7A100FFF2CB00FFF5D600FFEAAE00FFEAAB00FFEC
      B300FFECB100FFEAAA00FFF0C100FFF8E200FDE9B200FBDF9000FADC8B00E2B8
      6000BA802E00E0DDD80000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C1B7B300A8877900F5E9E300FEF1EC00FEF1EC00F9EB
      E500D6C3B900BAA09500A893890097877F00B29A8D00B59B8F00CDB5AA00F0DB
      D200FEE9E100FEE8E000F9E3DA00BD9E9100A28F840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009EF2
      B90053F3870066FF990066FF990066FF990066FF990066FF990066FF99004DE7
      7C0098E5AD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FCFCFC00CCAD
      8300BF852700F5D68700FFE69A00FFE69A00FFEAAD00FFF5D600FFF4D300FFF3
      CD00FFF3CF00FFF5D900FEF1CB00FEE7A400FDE29400FCE19200E5BD6800B87A
      1D00D5C8B5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009B827800B79D8F00F3E7E200FEF3EE00FEF2
      ED00FEF1EC00FDEFEA00FCEEE800FCEDE600FCECE600FDECE600FEECE500FEEC
      E500FEEBE400FDE9E200CCB1A400977A6D00DEDAD70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009EF2
      B90053F3870066FF990066FF990066FF990066FF990066FF990066FF99004EEA
      7F009AE9B0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F3F2
      F100C2904900BF852700F5D78900FFE69A00FFE69A00FFE69B00FFE9A600FFEA
      AB00FFEAAA00FFE8A300FEE59900FEE59800FEE49700EBC77400B87A1E00D4B1
      8000FCFCFC000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D3CFCE00A5877800B89F9200F8EEEA00FDF2
      EE00FEF3EE00FEF2ED00FEF1EC00FFF1EC00FEF0EA00FEEFE900FEEEE800FEED
      E600FBE9E200D3BBAF00A9897900A89892000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C2F7
      D30044E9790064FD970066FF990066FF990066FF990066FF990063FD970040E2
      7200D9F8E2000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F3F2F100CCAD8300B77A1F00D6A85100F2D38300FDE39600FFE69A00FFE6
      9A00FFE69A00FFE69A00FCE19400EDCB7B00CA943A00B77D2700CDBDA700FCFC
      FC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6B5AD00A1887C00B3978A00D0BC
      B300EBDDD700F6EAE500FDF2ED00FDF1ED00FDF1EC00F9ECE600EEDDD600DFCA
      C100B89B8E00A3897C00B59E9200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000093F0B10041E8760049EC7D0049EC7D0049EC7D0049EC7D0041E7760092EF
      AF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FCFCFC00CBBFAE00C28F4800B97C1C00C6903400D9AC5500E3BA
      6600E0B76200D6A85100C38A2E00B7791B00CDA36900DCD3C800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A2897E00AD96
      8C00B89D9100B79D8F00B89E9100B89E9100B89D9000B5998B00B89C8F00B99D
      90009F877C00B3A69F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F6EFE500E8D5BB00DDC29C00D9B9
      8D00D9BB9000E0C6A300ECDDC900F6F5F3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D8D0
      CD00C4B0A700BBA39700B89E9100B89E9100B89D9000B5998B00C2ACA200D3C5
      BF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000A00000000100010000000000000A00000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFF00000000
      FFFFFFFFFFFFFFC7FFFFFFFF00000000C003C001FFFFFF03FFFC7FFF00000000
      C003C001FFFFFF01FFFC7FFF00000000C003C001FFFFFC01FFF07FFF00000000
      C003C001FFFFFC01FFF07FFF00000000C003C001FFFFF003FFE07FFF00000000
      C003C001FFFFF003FFC0000F00000000C003C001FFFFC00FFF80000F00000000
      C003C001FFFFC00FFF00000F00000000C003C001FFFF003FFE00000F00000000
      C003C001FFFF003FFE00000F00000000C003C001FC7C00FFFC00000F00000000
      FC7FFF3FFC3C00FFF800000F00000000FC00003FFC3003FFF000000F00000000
      FC00003FFC1003FFF000000F00000000FFFE3FFFFC000FFFF000000F00000000
      FFFE3FFFFC000FFFF000000F00000000FFFE3FFFC0003FFFF800000F00000000
      FF8000FF00003FFFFC00000F00000000FF8000FF00007FFFFE00000F00000000
      FF8000FF00003FFFFE00000F00000000FF8000FFC0000FFFFF00000F00000000
      FF8000FFF0000FFFFF80000F00000000FF8000FFF0000FFFFFC0000F00000000
      FF8000FFF0003FFFFFE07FFF00000000FF8000FFF001FFFFFFF07FFF00000000
      FF8000FFE001FFFFFFF07FFF00000000FF8000FFE0C3FFFFFFFC7FFF00000000
      FF8000FFE3C3FFFFFFFC7FFF00000000FFFFFFFFFFE3FFFFFFFFFFFF00000000
      FFFFFFFFFFF3FFFFFFFFFFFF00000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF3FF8FFFFFFFFFFFFFFFFFFFFFFFC3FF807E01F
      FF3FFC7FFFFFFFFFFFFFF83FF003C00FFC0FF83FFFFFFFFFFFFFF00FC0000003
      FC0FF01FFFF1FFFFFFFFE00FC0000003F003E00FFFE0FFFFFFFFC003C0000003
      F003C007FFC07FFFFFFFC003C0000003F0008007FF803FFFFFFF0003C0000003
      F0000007FF001FFFFFFF0003C0000003F800000FFE000FFFFFFE0007C0000003
      FC00001FFC0007FFFFFC000FC0000003FE00003FF80003FFFFF8001FC0000003
      FF00007FF00001FFFFF0003FC0000003FF8000FFE00000FFF80000FFC0000003
      FFC001FFC000003FF00000FFE0000007FF8000FF8000001FE00003FFF800001F
      FF0000FF8000000FC00007FFFC00003FFE00003F800E0007C0000FFFFF0000FF
      FC00003FC00F0003C0001FFFFF0000FFF800000FE03F8001C0003FFFFF0000FF
      F000000FF03FC001C0003FFFFF0000FFF0008007F8FFE001C3803FFFFF0000FF
      F001C007FCFFF001C3C03FFFFF0000FFF003E00FFFFFF803CFC03FFFFF0000FF
      F807F00FFFFFFC07CFC03FFFFF0000FFFC0FF83FFFFFFE0FFF003FFFFF0000FF
      FE1FFC3FFFFFFF1FFF007FFFFFC003FFFFFFFFFFFFFFFFFFFC00FFFFFFF00FFF
      FFFFFFFFFFFFFFFFFC01FFFFFFF81FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFE00007FFFFFFFFFF000000FF000000FFE00007F
      FF3FFC7FF000000FF000000FFE00007FFC0FF83FF000000FF000000FFE00007F
      FC0FF01FF000000FF000000FC0000003F003E00FF000000FF000000FC0000003
      F003C0070000000FF0000007C0000003F00080070000000FF0000001C0000003
      F00000070000000FF0000000C0000003F800000F0000000FF0000000C0000003
      FC00001F0000000FF0000000C0000003FE00003F0000000FF0000000C0000003
      FF00007F0000000FF0000001C0000003FF8000FF0000000FF0000007C0000003
      FFC001FF0000000FF000000FC0000003FF8000FF0000000FF000000FC0000003
      FF0000FF0000000FF000000FC0000003FE00003F0000000FF000000FC0000003
      FC00003FF000000FF000000FC0000003F800000FF000000FF000000FC0000003
      F000000FF000000FF000000FC0000003F0008007F000000FF000000FFE00007F
      F001C007F000000FF000000FFE00007FF003E00FF000003FF000003FFE00007F
      F807F00FF000003FF000003FFE00007FFC0FF83FF00000FFF00000FFFE00007F
      FE1FFC3FF00000FFF00000FFFE00007FFFFFFFFFF00003FFF00003FFFE00007F
      FFFFFFFFF00003FFF00003FFFE00007FFFFFFFFFF0000FFFF0000FFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FFFF
      FFFFFFFFF0000007FFC003FFFFF1FFFFF000001FF0000007FF0000FFFFC1FFFF
      F000001FF0000007FE00007FFF01FFFFF000000FF0000007F800001FFC01FFFF
      F000000FF0000007F000000FF8001FFFF000000FF0000007F000000FE00001FF
      F000000FF0000007E0000007C00000FFF000000FF0000007C0000003C000003F
      F000000FF0000007C0000003E000003FF000000FF000000780000001F801001F
      F000000FF000000780000001FC01F00FF000000FF000000780000001FF01FC0F
      F000000FF000000780000001FFC1FF0FF000000FF000000780000001FFF1FF8F
      F000000FF000000780000001F7F1BFCFF000000FF000000780000001F1FF8FFF
      F000000FF000000780000001F0FF83FFF000001FF000000780000001F07F80FF
      F000001FF000000780000001F01F807FF80FFFFFF000000780000001F000801F
      F80FFFFFF000000780000001F800000FF80FFFFFF0000007C0000003FC000003
      F81FF07FF0000007C0000003FE000003F807C03FF0000007E0000007FF800007
      FC00003FF0000007F000000FFFE0000FFC00007FFE0FFC3FF000000FFFFF803F
      FE00007FFE0FFC3FF800001FFFFF80FFFE0000FFFE0FFC3FFE00007FFFFF83FF
      FF0001FFFE0FFC3FFF0000FFFFFF83FFFFC003FFFE1FFC3FFFC003FFFFFF8FFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFF00FFFFFFFFFFFFFFFFE03F000001F
      FFE007FFFFFFFFFFFFFFFC03F000001FFFE007FFFFFFFFFFFFFFF801F000000F
      FFE007FFFFFFFFFFFFFFF001F000000FFFE007FFFFFFFFFFFFFFE001F000000F
      FFE007FFFFFFFFFFFFFFC001F000000FFFE007FFFFFFFFFFFFFF8003F000000F
      FFE007FFFFFFFFFFFF810007F000000FFFE007FFE0000003FC00000FF000000F
      E0000007E0000003F000001FF000000FC0000003E0000003E000003FF000000F
      C0000003E0000003E000007FF000000FC0000003E0000003C00000FFF000000F
      C0000003E0000003C00001FFF000000FC0000003E0000003800003FFF000000F
      C0000003E0000003800001FFF000000FC0000003E0000003800001FFF000001F
      C0000003E0000003800001FFF000001FE0000007E0000003800001FFF80FE03F
      FFE007FFFFFFFFFF800001FFF80FF03FFFE007FFFFFFFFFF800001FFF80FE03F
      FFE007FFFFFFFFFF800001FFF807C03FFFE007FFFFFFFFFFC00003FFFC00003F
      FFE007FFFFFFFFFFC00003FFFC00007FFFE007FFFFFFFFFFC00007FFFE00007F
      FFE007FFFFFFFFFFE00007FFFE0000FFFFE007FFFFFFFFFFF0000FFFFF0001FF
      FFF00FFFFFFFFFFFF8003FFFFFC003FFFFFFFFFFFFFFFFFFFF00FFFFFFE00FFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object DataSetInvoices: TpFIBDataSet
    UpdateSQL.Strings = (
      
        'EXECUTE PROCEDURE AFFINAGE$INVOICE_U(:ID, :DESIGNATION, :DATE$, ' +
        ' :Department$ID,'
      '  :Target$company$ID'
      ')')
    DeleteSQL.Strings = (
      'EXECUTE PROCEDURE AFFINAGE$INVOICE_D(:ID)')
    InsertSQL.Strings = (
      'EXECUTE PROCEDURE AFFINAGE$INVOICE_I(:ID, :CLASS)')
    RefreshSQL.Strings = (
      'select'
      '  ID,'
      '  Designation,'
      '  Class,'
      '  Class$Title,'
      '  Date$,'
      '  Created$Date,'
      '  Created$By,'
      '  Created$By$Title,'
      '  State,'
      '  State$Title,'
      '  State$By,'
      '  State$By$Title,'
      '  State$Date,'
      '  Department$ID,'
      '  Target$company$ID'
      'from'
      '  Affinage$Invoice_R(:ID)')
    SelectSQL.Strings = (
      'select'
      '  ID,'
      '  Designation,'
      '  Class,'
      '  Class$Title,'
      '  Date$,'
      '  Created$Date,'
      '  Created$By,'
      '  Created$By$Title,'
      '  State,'
      '  State$Title,'
      '  State$By,'
      '  State$By$Title,'
      '  State$Date,'
      '  Department$Id,'
      '  Target$Company$Id'
      'from'
      '  Affinage$Invoice_S(:Period$Begin, :Period$End)')
    AfterDelete = DataSetInvoicesAfterDelete
    AfterInsert = DataSetInvoicesAfterInsert
    AfterPost = DataSetCommitRetaining
    BeforeOpen = DataSetInvoicesBeforeOpen
    BeforePost = OnDataSetValidate
    Transaction = Transaction
    Database = dm.db
    Left = 24
    Top = 376
    object DataSetInvoicesID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetInvoicesDESIGNATION: TFIBIntegerField
      FieldName = 'DESIGNATION'
    end
    object DataSetInvoicesCLASS: TFIBIntegerField
      FieldName = 'CLASS'
    end
    object DataSetInvoicesCLASSTITLE: TFIBStringField
      FieldName = 'CLASS$TITLE'
      Size = 8
      EmptyStrToNull = True
    end
    object DataSetInvoicesDATE: TFIBDateTimeField
      FieldName = 'DATE$'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object DataSetInvoicesCREATEDDATE: TFIBDateTimeField
      FieldName = 'CREATED$DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm '
    end
    object DataSetInvoicesCREATEDBY: TFIBIntegerField
      FieldName = 'CREATED$BY'
    end
    object DataSetInvoicesCREATEDBYTITLE: TFIBStringField
      FieldName = 'CREATED$BY$TITLE'
      Size = 32
      EmptyStrToNull = True
    end
    object DataSetInvoicesSTATE: TFIBIntegerField
      FieldName = 'STATE'
    end
    object DataSetInvoicesSTATETITLE: TFIBStringField
      FieldName = 'STATE$TITLE'
      Size = 8
      EmptyStrToNull = True
    end
    object DataSetInvoicesSTATEBY: TFIBIntegerField
      FieldName = 'STATE$BY'
    end
    object DataSetInvoicesSTATEBYTITLE: TFIBStringField
      FieldName = 'STATE$BY$TITLE'
      Size = 32
      EmptyStrToNull = True
    end
    object DataSetInvoicesSTATEDATE: TFIBDateTimeField
      FieldName = 'STATE$DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object DataSetInvoicesDEPARTMENTID: TFIBIntegerField
      FieldName = 'DEPARTMENT$ID'
    end
    object DataSetInvoicesDepartmentTITLE: TStringField
      FieldKind = fkLookup
      FieldName = 'Department$TITLE'
      LookupDataSet = DataSetDepartments
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'DEPARTMENT$ID'
      Size = 64
      Lookup = True
    end
    object DataSetInvoicesTARGETCOMPANYID: TFIBIntegerField
      FieldName = 'TARGET$COMPANY$ID'
    end
    object DataSetInvoicesCompanyTITLE: TStringField
      FieldKind = fkLookup
      FieldName = 'Company$TITLE'
      LookupDataSet = DataSetCompanies
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'TARGET$COMPANY$ID'
      Size = 64
      Lookup = True
    end
  end
  object DataSetInvoiceItems: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure AFFINAGE$INVOICE$ITEM_U('
      '  :ID,'
      '  :Operation$ID,'
      '  :Place$Of$Storage$ID,'
      '  :Weight,'
      '  :Material$ID,'
      '  :Material$Weight,'
      '  :Materail$Quantity,'
      '  :Material$Measure$Weight$Id,'
      '  :Material$Measure$Quantity$Id)'
      '')
    DeleteSQL.Strings = (
      'execute procedure AFFINAGE$INVOICE$ITEM_D(:ID)')
    InsertSQL.Strings = (
      'execute procedure AFFINAGE$INVOICE$ITEM_I('
      '  :ID,'
      '  :Invoice$ID,'
      '  :Operation$ID,'
      '  :Place$Of$Storage$ID,'
      '  :Weight,'
      '  :Material$ID,'
      '  :Material$Weight,'
      '  :Materail$Quantity,'
      '  :Material$Measure$Weight$Id,'
      '  :Material$Measure$Quantity$Id)'
      '')
    RefreshSQL.Strings = (
      'select '
      '  ID,'
      '  Invoice$ID,'
      '  Operation$ID,'
      '  Place$Of$Storage$ID,'
      '  Weight,'
      '  Material$Id,'
      '  Material$Weight,'
      '  Materail$Quantity,'
      '  Material$Measure$Weight$ID,'
      '  Material$Measure$Quantity$ID'
      ''
      'from'
      '  Affinage$Invoice$Item_R(:ID)')
    SelectSQL.Strings = (
      'select '
      '  Id,'
      '  Invoice$Id,'
      '  Operation$Id,'
      '  Place$Of$Storage$Id,'
      '  Weight,'
      '  Material$Id,'
      '  Material$Weight,'
      '  Materail$Quantity,'
      '  Material$Measure$Weight$ID,'
      '  Material$Measure$Quantity$ID'
      'from'
      '  Affinage$Invoice$Item_S(:Invoice$ID)')
    AfterCancel = DataSetInvoiceItemsAfterCancel
    AfterPost = DataSetCommitRetaining
    BeforeInsert = DataSetInvoiceItemsBeforeInsert
    BeforeOpen = DataSetInvoiceItemsBeforeOpen
    BeforePost = DataSetInvoiceItemsBeforePost
    OnNewRecord = DataSetInvoiceItemsNewRecord
    Transaction = Transaction
    Database = dm.db
    Left = 24
    Top = 408
    object DataSetInvoiceItemsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetInvoiceItemsINVOICEID: TFIBIntegerField
      FieldName = 'INVOICE$ID'
    end
    object DataSetInvoiceItemsOPERATIONID: TFIBIntegerField
      Tag = 1
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION$ID'
    end
    object DataSetInvoiceItemsOPERATIONTITLE: TStringField
      FieldKind = fkLookup
      FieldName = 'OPERATION$TITLE'
      LookupDataSet = DataSetOperations
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'OPERATION$ID'
      Size = 64
      Lookup = True
    end
    object DataSetInvoiceItemsPLACEOFSTORAGEID: TFIBIntegerField
      Tag = 1
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'PLACE$OF$STORAGE$ID'
    end
    object DataSetInvoiceItemsPLACEOFSTORAGETITLE: TFIBStringField
      FieldKind = fkLookup
      FieldName = 'PLACE$OF$STORAGE$TITLE'
      LookupDataSet = DataSetPlaceOfStorage
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'PLACE$OF$STORAGE$ID'
      Size = 64
      EmptyStrToNull = True
      Lookup = True
    end
    object DataSetInvoiceItemsWEIGHT: TFIBBCDField
      FieldName = 'WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object DataSetInvoiceItemsMATERIALID: TFIBIntegerField
      Tag = 1
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldName = 'MATERIAL$ID'
    end
    object DataSetInvoiceItemsMATERIALTITLE: TFIBStringField
      FieldKind = fkLookup
      FieldName = 'MATERIAL$TITLE'
      LookupDataSet = DataSetMaterial
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'MATERIAL$ID'
      Size = 64
      EmptyStrToNull = True
      Lookup = True
    end
    object DataSetInvoiceItemsMATERIALWEIGHT: TFIBBCDField
      FieldName = 'MATERIAL$WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object DataSetInvoiceItemsMATERAILQUANTITY: TFIBIntegerField
      FieldName = 'MATERAIL$QUANTITY'
    end
    object DataSetInvoiceItemsMATERIALMEASUREWEIGHTID: TFIBIntegerField
      FieldName = 'MATERIAL$MEASURE$WEIGHT$ID'
    end
    object DataSetInvoiceItemsMATERIALMEASURETITLE: TStringField
      FieldKind = fkLookup
      FieldName = 'MATERIAL$MEASURE$WEIGHT$TITLE'
      LookupDataSet = DataSetMeasures
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'MATERIAL$MEASURE$WEIGHT$ID'
      Lookup = True
    end
    object DataSetInvoiceItemsMATERIALMEASUREQUANTITYID: TFIBIntegerField
      FieldName = 'MATERIAL$MEASURE$QUANTITY$ID'
    end
    object DataSetInvoiceItemsMATERIALMEASUREQUANTITYTITLE: TStringField
      FieldKind = fkLookup
      FieldName = 'MATERIAL$MEASURE$QUANTITY$TITLE'
      LookupDataSet = DataSetMeasures
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'MATERIAL$MEASURE$QUANTITY$ID'
      Size = 64
      Lookup = True
    end
  end
  object DataSourceInvoices: TDataSource
    DataSet = DataSetInvoices
    Left = 56
    Top = 376
  end
  object DataSourceInvoiceItems: TDataSource
    DataSet = DataSetInvoiceItems
    OnDataChange = DataSourceInvoiceItemsDataChange
    Left = 56
    Top = 408
  end
  object DisabledLargeImages: TImageList
    Height = 32
    Width = 32
    Left = 144
    Top = 544
  end
  object DataSetOperations: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, TITLE from AFFINAGE$OPERATION_S')
    Transaction = Transaction
    Database = dm.db
    Left = 24
    Top = 440
    oFetchAll = True
    object DataSetOperationsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetOperationsTITLE: TFIBStringField
      FieldName = 'TITLE'
      Size = 64
      EmptyStrToNull = True
    end
  end
  object DataSetPlaceOfStorage: TpFIBDataSet
    SelectSQL.Strings = (
      'select id, title from AFFINAGE$PLACE$OF$STORAGE_S(:OPERATION$ID)')
    Transaction = Transaction
    Database = dm.db
    Left = 24
    Top = 472
    oFetchAll = True
    object DataSetPlaceOfStorageID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetPlaceOfStorageTITLE: TFIBStringField
      FieldName = 'TITLE'
      Size = 64
      EmptyStrToNull = True
    end
  end
  object DataSetMaterial: TpFIBDataSet
    SelectSQL.Strings = (
      'select id, title from AFFINAGE$MATERIAL_S(:ClASS)')
    BeforeOpen = DataSetMaterialBeforeOpen
    Transaction = Transaction
    Database = dm.db
    Left = 24
    Top = 504
    oFetchAll = True
    object DataSetMaterialID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetMaterialTITLE: TFIBStringField
      FieldName = 'TITLE'
      Size = 64
      EmptyStrToNull = True
    end
  end
  object ReportInvoiceOut: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbPrint, pbExit, pbPageSetup]
    StoreInDFM = True
    Title = #1055#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1099#1081' '#1087#1088#1086#1089#1084#1086#1090#1088
    RebuildPrinter = False
    Left = 88
    Top = 312
    ReportForm = {
      19000000554C0000190000000001000100FFFFFFFFFF090000009A0B00003408
      00004800000012000000120000001200000001FFFF00000000FFFF0000000000
      00000000000000030400466F726D000F000080DC000000780000007C0100002C
      010000040000000200CB0000000C005265706F72745469746C65310002010000
      0000200000002F0400004C0100003000000001000000000000000000FFFFFF1F
      00000000000000000000000000FFFF0000000000020000000100000000000000
      01000000C800000014000000010000000000000200390100000E005265706F72
      7453756D6D6172793100020100000000600200002F040000A800000030000100
      01000000000000000000FFFFFF1F00000000000000000000000000FFFF000000
      000002000000010000000000000001000000C800000014000000010000000000
      000200310300000B004D61737465724461746131000201000000002C0200002F
      040000140000003100050001000000000000000000FFFFFF1F000000001B0066
      72444244617461536574496E766F6963655072696E744974656D000000000600
      0500626567696E0DA5002020696620285B44617461536574496E766F69636550
      72696E744974656D2E224954454D24494E44455824494E2447524F5550225D20
      3D203129206F7220285B44617461536574496E766F6963655072696E74497465
      6D2E224954454D244D4154455249414C245449544C45225D203D2027C8F2EEE3
      EE20EFEE20EDE0EAEBE0E4EDEEE93A2729207468656E204672616D65546F702E
      56697369626C65203A3D20547275650D21002020656C7365204672616D65546F
      702E56697369626C65203A3D2046616C73653B0D6F002020696620285B446174
      61536574496E766F6963655072696E744974656D2E224954454D244D41544552
      49414C245449544C45225D203D2027C8F2EEE3EE20EFEE20EDE0EAEBE0E4EDEE
      E93A2729207468656E204672616D65426F74746F6D2E56697369626C65203A3D
      20547275650D24002020656C7365204672616D65426F74746F6D2E5669736962
      6C65203A3D2046616C73653B0D0300656E6400FFFF0000000000020000000100
      00000000000001000000C8000000140000000100000000000002009E0300000D
      004D61737465724865616465723100020100000000840100002F0400008C0000
      003000040001000000000000000000FFFFFF1F00000000000000000000000000
      FFFF000000000002000000010000000000000001000000C80000001400000001
      00000000000000008104000005004D656D6F310002006103000024000000BC00
      0000240000000300000001000000000000000000FFFFFF1F2C02000000000003
      002200D2E8EFEEE2E0FF20ECE5E6EEF2F0E0F1EBE5E2E0FF20F4EEF0ECE020B9
      20CC2D31350D2C00D3F2E2E5F0E6E4E5EDE020CFEEF1F2E0EDEEE2EBE5EDE8E5
      EC20C3EEF1EAEEECF1F2E0F2E020D0EEF1F1E8E80D1100EEF22033302E31302E
      393720B9203731E000000000FFFF000000000002000000010000000205004172
      69616C0006000000000000000000000000000100020000000000FFFFFF000000
      00020000000000000000000B05000005004D656D6F32000200DB010000680000
      0082000000140000000B00000001000000000000000000FFFFFF1F2C02000000
      000001000C00CDC0CACBC0C4CDC0DF20B92000000000FFFF0000000000020000
      0001000000000500417269616C000C0000000200000000000000000001000200
      00000000FFFFFF0000000002000000000000000000A805000005004D656D6F33
      000200A80100007C000000150100001400000003000000010000000000000000
      00FFFFFF1F2C02000000000001001F00EDE020EEF2EFF3F1EA20ECE0F2E5F0E8
      E0EBEEE220EDE020F1F2EEF0EEEDF300000000FFFF0000000000020000000100
      0000030500417269616C000C0000000200000000000000000001000200000000
      00FFFFFF00000000020000000000000000002A06000005004D656D6F34000200
      BD0300009C000000600000001400000003000F0001000000000000000100FFFF
      FF1F2C02000000000001000400CAEEE4FB00000000FFFF000000000002000000
      01000000020500417269616C000A000000000000000000020000000100020000
      000000FFFFFF0000000002000000000000000000A606000005004D656D6F3500
      0200BD030000C4000000600000001400000003000F0001000000000000000100
      FFFFFF1F2C020000000000000000000000FFFF00000000000200000001000000
      020500417269616C000A000000020000000000000000000100020000000000FF
      FFFF00000000020000000000000000002B07000005004D656D6F37000200BD03
      0000B0000000600000001400000003000F0001000000000000000100FFFFFF1F
      2C020000000000010007003033313530303700000000FFFF0000000000020000
      0001000000020500417269616C000A0000000000000000000200000001000200
      00000000FFFFFF0000000002000000000000000000A707000005004D656D6F36
      000200BC0300009C000000600000003C00000003000F00020000000000000000
      00FFFFFF1F2C020000000000000000000000FFFF000000000002000000010000
      00000500417269616C000A000000020000000000020000000100020000000000
      FFFFFF00000000020000000000000000003208000005004D656D6F3800020048
      030000B000000074000000140000000300000001000000000000000000FFFFFF
      1F2C02000000000001000D00D4EEF0ECE020EFEE20CECAD3C400000000FFFF00
      000000000200000001000000000500417269616C000A00000000000000000001
      0000000100020000000000FFFFFF0000000002000000000000000000B7080000
      05004D656D6F3900020048030000C40000007400000014000000030000000100
      0000000000000000FFFFFF1F2C02000000000001000700EFEE20CECACFCE0000
      0000FFFF00000000000200000001000000000500417269616C000A0000000000
      00000000010000000100020000000000FFFFFF00000000020000000000000000
      006909000006004D656D6F3130000200A4000000C4000000A002000014000000
      8300000001000000000000000000FFFFFF1F2C020000000000010033005B4461
      7461536574496E766F6963655072696E744865616465722E2248454144455224
      534F5552434524434F4D50414E59225D00000000FFFF00000000000200000001
      000000000500417269616C000A00000000000000000000000000010002000000
      0000FFFFFF0000000002000000000000000000FA09000006004D656D6F313100
      02004C000000E4000000540000003C00000003000F0001000000000000000000
      FFFFFF1F2C02000000000002000400C4E0F2E00D0B00F1EEF1F2E0E2EBE5EDE8
      FF00000000FFFF00000000000200000001000000000500417269616C000A0000
      000000000000000A0000000100020000000000FFFFFF00000000020000000000
      000000008E0A000006004D656D6F3132000200A0000000E4000000440000003C
      00000003000F0001000000000000000000FFFFFF1F2C02000000000003000300
      CAEEE40D0400E2E8E4E00D0800EEEFE5F0E0F6E8E800000000FFFF0000000000
      0200000001000000000500417269616C000A0000000000000000000A00000001
      00020000000000FFFFFF0000000002000000000000000000180B000006004D65
      6D6F3133000200E4000000E4000000EC0000001400000003000F000100000000
      0000000000FFFFFF1F2C02000000000001000B00CEF2EFF0E0E2E8F2E5EBFC00
      000000FFFF00000000000200000001000000000500417269616C000A00000000
      00000000000A0000000100020000000000FFFFFF000000000200000000000000
      0000A80B000006004D656D6F3134000200A8030000F800000070000000280000
      0003000F0001000000000000000000FFFFFF1F2C02000000000002000300EAEE
      E40D0B00E8F1EFEEEBEDE8F2E5EBFF00000000FFFF0000000000020000000100
      0000000500417269616C000A0000000000000000000200000001000200000000
      00FFFFFF0000000002000000000000000000390C000006004D656D6F31370002
      0068010000F8000000680000002800000003000F0001000000000000000000FF
      FFFF1F2C02000000000002000300E2E8E40D0C00E4E5FFF2E5EBFCEDEEF1F2E8
      00000000FFFF00000000000200000001000000000500417269616C000A000000
      0000000000000A0000000100020000000000FFFFFF0000000002000000000000
      000000C20C000006004D656D6F3138000200D0010000E4000000EC0000001400
      000003000F0001000000000000000000FFFFFF1F2C02000000000001000A00CF
      EEEBF3F7E0F2E5EBFC00000000FFFF0000000000020000000100000000050041
      7269616C000A0000000000000000000A0000000100020000000000FFFFFF0000
      0000020000000000000000005A0D000006004D656D6F3230000200E4000000F8
      000000840000002800000003000F0001000000000000000000FFFFFF1F2C0200
      0000000001001900F1F2F0F3EAF2F3F0EDEEE520EFEEE4F0E0E7E4E5EBE5EDE8
      E500000000FFFF00000000000200000001000000000500417269616C000A0000
      000000000000000A0000000100020000000000FFFFFF00000000020000000000
      00000000F20D000006004D656D6F3232000200BC020000E40000005C01000014
      00000003000F0001000000000000000000FFFFFF1F2C02000000000001001900
      CEF2E2E5F2F1F2E2E5EDEDFBE920E7E020EFEEF1F2E0E2EAF300000000FFFF00
      000000000200000001000000000500417269616C000A0000000000000000000A
      0000000100020000000000FFFFFF00000000020000000000000000009B0E0000
      06004D656D6F32330002004C00000020010000540000001400000003000F0001
      000000000000000000FFFFFF1F2C02000000000001002A005B44617461536574
      496E766F6963655072696E744865616465722E22484541444552244441544524
      225D00000000FFFF00000000000200000001000000000500417269616C000A00
      00000000000000000A0000000100020000000000FFFFFF000000000200000000
      00000000001A0F000006004D656D6F3234000200A00000002001000044000000
      1400000003000F0001000000000000000000FFFFFF1F2C020000000000010000
      0000000000FFFF00000000000200000001000000000500417269616C000A0000
      000000000000000A0000000100020000000000FFFFFF00000000020000000000
      00000000CE0F000006004D656D6F3235000200E4000000200100008400000014
      00000003000F0001000000000000000000FFFFFF1F2C02000000000001003500
      5B44617461536574496E766F6963655072696E744865616465722E2248454144
      4552244445504152544D454E54245449544C45225D00000000FFFF0000000000
      0200000001000000000500417269616C000A0000000000000000000A00000001
      00020000000000FFFFFF00000000020000000000000000004D10000006004D65
      6D6F32360002006801000020010000680000001400000003000F000100000000
      0000000000FFFFFF1F2C0200000000000100000000000000FFFF000000000002
      00000001000000000500417269616C000A0000000000000000000A0000000100
      020000000000FFFFFF0000000002000000000000000000CC10000006004D656D
      6F3237000200D001000020010000840000001400000003000F00010000000000
      00000000FFFFFF1F2C0200000000000100000000000000FFFF00000000000200
      000001000000000500417269616C000A0000000000000000000A000000010002
      0000000000FFFFFF00000000020000000000000000004B11000006004D656D6F
      32380002005402000020010000680000001400000003000F0001000000000000
      000000FFFFFF1F2C0200000000000100000000000000FFFF0000000000020000
      0001000000000500417269616C000A0000000000000000000A00000001000200
      00000000FFFFFF0000000002000000000000000000CA11000006004D656D6F32
      39000200BC02000020010000840000001400000003000F000100000000000000
      0000FFFFFF1F2C0200000000000100000000000000FFFF000000000002000000
      01000000000500417269616C000A0000000000000000000A0000000100020000
      000000FFFFFF00000000020000000000000000004912000006004D656D6F3330
      0002004003000020010000680000001400000003000F00010000000000000000
      00FFFFFF1F2C0200000000000100000000000000FFFF00000000000200000001
      000000000500417269616C000A0000000000000000000A000000010002000000
      0000FFFFFF0000000002000000000000000000C812000006004D656D6F333100
      0200A803000020010000700000001400000003000F0001000000000000000000
      FFFFFF1F2C0200000000000100000000000000FFFF0000000000020000000100
      0000000500417269616C000A0000000000000000000A00000001000200000000
      00FFFFFF00000000020000000000000000005013000006004D656D6F33320002
      004C0000003801000048000000140000000300000001000000000000000000FF
      FFFF1F2C02000000000001000900CEF1EDEEE2E0EDE8E500000000FFFF000000
      00000200000001000000000500417269616C000A000000000000000000080000
      000100020000000000FFFFFF0000000002000000000000000000D31300000600
      4D656D6F33330002004C000000500100002C0000001400000003000000010000
      00000000000000FFFFFF1F2C02000000000001000400CAEEECF300000000FFFF
      00000000000200000001000000000500417269616C000A000000000000000000
      080000000100020000000000FFFFFF00000000020000000000000000005C1400
      0006004D656D6F333400020024020000500100004C0000001400000003000000
      01000000000000000000FFFFFF1F2C02000000000001000A00D7E5F0E5E720EA
      EEE3EE00000000FFFF00000000000200000001000000000500417269616C000A
      000000000000000000080000000100020000000000FFFFFF0000000002000000
      0000000000000615000006004D656D6F33360002009800000038010000800300
      00140000008300000001000000000000000000FFFFFF1F2C0200000000000100
      2B005B44617461536574496E766F6963655072696E744865616465722E224845
      4144455224524541534F4E225D00000000FFFF00000000000200000001000000
      000500417269616C000A000000000000000000080000000100020000000000FF
      FFFF0000000002000000000000000000B815000006004D656D6F33370002007C
      00000050010000A4010000140000008300000001000000000000000000FFFFFF
      1F2C020000000000010033005B44617461536574496E766F6963655072696E74
      4865616465722E224845414445522454415247455424434F4D50414E59225D00
      000000FFFF00000000000200000001000000000500417269616C000A00000000
      0000000000080000000100020000000000FFFFFF000000000200000000000000
      00003516000006004D656D6F33350002007402000050010000A4010000140000
      008300000001000000000000000000FFFFFF1F2C020000000000000000000000
      FFFF00000000000200000001000000000500417269616C000A00000000000000
      0000080000000100020000000000FFFFFF0000000002000000000000000000CA
      16000006004D656D6F33380002004C000000840100007C000000280000000300
      0F0001000000000000000000FFFFFF1F2C02000000000001001600CAEEF0F0E5
      F1EFEEEDE4E8F0F3FEF9E8E920F1F7E5F200000000FFFF000000000002000000
      01000000000500417269616C00090000000000000000000A0000000100020000
      000000FFFFFF00000000020000000000000000005E17000006004D656D6F3339
      000200C800000084010000CC0000002800000003000F00010000000000000000
      00FFFFFF1F2C02000000000001001500CCE0F2E5F0E8E0EBFCEDFBE520F6E5ED
      EDEEF1F2E800000000FFFF00000000000200000001000000000500417269616C
      00090000000000000000000A0000000100020000000000FFFFFF000000000200
      0000000000000000F017000006004D656D6F3430000200940100008401000058
      0000002800000003000F0001000000000000000000FFFFFF1F2C020000000000
      02000700C5E4E8EDE8F6E00D0900E8E7ECE5F0E5EDE8FF00000000FFFF000000
      00000200000001000000000500417269616C00090000000000000000000A0000
      000100020000000000FFFFFF0000000002000000000000000000751800000600
      4D656D6F34310002002C02000084010000400000007800000003000F00010000
      00000000000000FFFFFF1F2C02000000000001000600CAEEEB2DE2EE00000000
      FFFF00000000000200000001000000000500417269616C000900000000000000
      00000A0000000100020000000000FFFFFF0000000002000000000000000000F4
      18000006004D656D6F34320002004C00000020010000CC030000140000000300
      0F0002000000000000000000FFFFFF1F2C0200000000000100000000000000FF
      FF00000000000200000001000000000500417269616C000A0000000000000000
      000A0000000100020000000000FFFFFF00000000020000000000000000008219
      000006004D656D6F34330002004C000000AC0100003C0000005000000003000F
      0001000000000000000000FFFFFF1F2C02000000000002000500F1F7E5F22C0D
      0700F1F3E1F1F7E5F200000000FFFF0000000000020000000100000000050041
      7269616C00090000000000000000000A0000000100020000000000FFFFFF0000
      000002000000000000000000211A000006004D656D6F343400020088000000AC
      010000400000005000000003000F0001000000000000000000FFFFFF1F2C0200
      0000000004000300EAEEE40D0800E0EDE0EBE8F2E82D0D0700F7E5F1EAEEE3EE
      0D0500F3F7E5F2E000000000FFFF000000000002000000010000000005004172
      69616C00090000000000000000000A0000000100020000000000FFFFFF000000
      0002000000000000000000C51A000006004D656D6F3435000200C8000000AC01
      00009C0000005000000003000F0001000000000000000000FFFFFF1F2C020000
      00000003000D00EDE0E8ECE5EDEEE2E0EDE8E52C0D0D00F1EEF0F22C20F0E0E7
      ECE5F02C0D0500ECE0F0EAE000000000FFFF0000000000020000000100000000
      0500417269616C00090000000000000000000A0000000100020000000000FFFF
      FF0000000002000000000000000000481B000006004D656D6F34360002009401
      0000AC0100002C0000005000000003000F0001000000000000000000FFFFFF1F
      2C02000000000001000400E2E5F1E000000000FFFF0000000000020000000100
      0000000500417269616C00090000000000000000000A00000001000200000000
      00FFFFFF0000000002000000000000000000CF1B000006004D656D6F34380002
      0084030000AC010000400000005000000003000F0001000000000000000000FF
      FFFF1F2C02000000000001000800EFE0F1EFEEF0F2E000000000FFFF00000000
      000200000001000000000500417269616C000A0000000000000000000A000000
      0100020000000000FFFFFF0000000002000000000000000000661C000006004D
      656D6F343900020064010000AC010000300000005000000003000F0001000000
      000000000000FFFFFF1F2C02000000000002001000EDEEECE5ED2DEAEBE0F2F3
      F02DEDFBE90D0500EDEEECE5F000000000FFFF00000000000200000001000000
      000500417269616C00090000000000000000000A0000000100020000000000FF
      FFFF0000000002000000000000000000EB1C000006004D656D6F3530000200C0
      010000AC0100002C0000005000000003000F0001000000000000000000FFFFFF
      1F2C02000000000001000600EAEEEB2DE2E000000000FFFF0000000000020000
      0001000000000500417269616C00090000000000000000000A00000001000200
      00000000FFFFFF00000000020000000000000000009E1D000006004D656D6F35
      31000200C403000084010000540000007800000003000F000100000000000000
      0000FFFFFF1F2C02000000000004000A00CFEEF0FFE4EAEEE2FBE90D0C00EDEE
      ECE5F020E7E0EFE8F1E80D0C00EFEE20F1EAEBE0E4F1EAEEE90D0900EAE0F0F2
      EEF2E5EAE500000000FFFF00000000000200000001000000000500417269616C
      000A0000000000000000000A0000000100020000000000FFFFFF000000000200
      0000000000000000361E000006004D656D6F3136000200D0010000F800000084
      0000002800000003000F0001000000000000000000FFFFFF1F2C020000000000
      01001900F1F2F0F3EAF2F3F0EDEEE520EFEEE4F0E0E7E4E5EBE5EDE8E5000000
      00FFFF00000000000200000001000000000500417269616C000A000000000000
      0000000A0000000100020000000000FFFFFF0000000002000000000000000000
      C71E000006004D656D6F323100020054020000F8000000680000002800000003
      000F0001000000000000000000FFFFFF1F2C02000000000002000300E2E8E40D
      0C00E4E5FFF2E5EBFCEDEEF1F2E800000000FFFF000000000002000000010000
      00000500417269616C000A0000000000000000000A0000000100020000000000
      FFFFFF00000000020000000000000000005F1F000006004D656D6F3135000200
      BC020000F8000000840000002800000003000F0001000000000000000000FFFF
      FF1F2C02000000000001001900F1F2F0F3EAF2F3F0EDEEE520EFEEE4F0E0E7E4
      E5EBE5EDE8E500000000FFFF0000000000020000000100000000050041726961
      6C000A0000000000000000000A0000000100020000000000FFFFFF0000000002
      000000000000000000F01F000006004D656D6F313900020040030000F8000000
      680000002800000003000F0001000000000000000000FFFFFF1F2C0200000000
      0002000300E2E8E40D0C00E4E5FFF2E5EBFCEDEEF1F2E800000000FFFF000000
      00000200000001000000000500417269616C000A0000000000000000000A0000
      000100020000000000FFFFFF0000000002000000000000000000742000000600
      4D656D6F35340002004C03000084010000780000002800000003000F00010000
      00000000000000FFFFFF1F2C02000000000001000500CDEEECE5F000000000FF
      FF00000000000200000001000000000500417269616C00090000000000000000
      000A0000000100020000000000FFFFFF00000000020000000000000000000421
      000006004D656D6F35360002006C02000084010000380000007800000003000F
      0001000000000000000000FFFFFF1F2C02000000000002000500D6E5EDE02C0D
      0900F0F3E12E20EAEEEF2E00000000FFFF000000000002000000010000000005
      00417269616C00090000000000000000000A0000000100020000000000FFFFFF
      0000000002000000000000000000A821000006004D656D6F3537000200A40200
      0084010000380000007800000003000F0001000000000000000000FFFFFF1F2C
      02000000000004000600D1F3ECECE0200D0900E1E5E720F3F7E5F2E00D0400CD
      C4D12C0D0900F0F3E12E20EAEEEF2E00000000FFFF0000000000020000000100
      0000000500417269616C00090000000000000000000A00000001000200000000
      00FFFFFF00000000020000000000000000004A22000006004D656D6F35380002
      001403000084010000380000007800000003000F0001000000000000000000FF
      FFFF1F2C02000000000004000500C2F1E5E3EE0D0800F120F3F7E5F2EEEC0D04
      00CDC4D12C0D0900F0F3E12E20EAEEEF2E00000000FFFF000000000002000000
      01000000000500417269616C00090000000000000000000A0000000100020000
      000000FFFFFF0000000002000000000000000000D822000006004D656D6F3539
      0002004C030000AC010000380000005000000003000F00010000000000000000
      00FFFFFF1F2C02000000000002000600E8EDE2E5ED2D0D0600F2E0F0EDFBE900
      000000FFFF00000000000200000001000000000500417269616C000900000000
      00000000000A0000000100020000000000FFFFFF000000000200000000000000
      00005823000006004D656D6F36300002004C000000FC0100003C000000140000
      0003000F0001000000000000000000FFFFFF1F2C020000000000010001003100
      000000FFFF00000000000200000001000000000500417269616C000600000000
      00000000000A0000000100020000000000FFFFFF000000000200000000000000
      0000D823000006004D656D6F363100020088000000FC01000040000000140000
      0003000F0001000000000000000000FFFFFF1F2C020000000000010001003200
      000000FFFF00000000000200000001000000000500417269616C000600000000
      00000000000A0000000100020000000000FFFFFF000000000200000000000000
      00005824000006004D656D6F3632000200C8000000FC0100009C000000140000
      0003000F0001000000000000000000FFFFFF1F2C020000000000010001003300
      000000FFFF00000000000200000001000000000500417269616C000600000000
      00000000000A0000000100020000000000FFFFFF000000000200000000000000
      0000D824000006004D656D6F363300020064010000FC01000030000000140000
      0003000F0001000000000000000000FFFFFF1F2C020000000000010001003400
      000000FFFF00000000000200000001000000000500417269616C000600000000
      00000000000A0000000100020000000000FFFFFF000000000200000000000000
      00005825000006004D656D6F363400020094010000FC0100002C000000140000
      0003000F0001000000000000000000FFFFFF1F2C020000000000010001003500
      000000FFFF00000000000200000001000000000500417269616C000600000000
      00000000000A0000000100020000000000FFFFFF000000000200000000000000
      0000D825000006004D656D6F3635000200C0010000FC0100002C000000140000
      0003000F0001000000000000000000FFFFFF1F2C020000000000010001003600
      000000FFFF00000000000200000001000000000500417269616C000600000000
      00000000000A0000000100020000000000FFFFFF000000000200000000000000
      00005826000006004D656D6F3636000200EC010000FC01000040000000140000
      0003000F0001000000000000000000FFFFFF1F2C020000000000010001003700
      000000FFFF00000000000200000001000000000500417269616C000600000000
      00000000000A0000000100020000000000FFFFFF000000000200000000000000
      0000D826000006004D656D6F36370002002C020000FC01000040000000140000
      0003000F0001000000000000000000FFFFFF1F2C020000000000010001003800
      000000FFFF00000000000200000001000000000500417269616C000600000000
      00000000000A0000000100020000000000FFFFFF000000000200000000000000
      00005827000006004D656D6F36380002006C020000FC01000038000000140000
      0003000F0001000000000000000000FFFFFF1F2C020000000000010001003900
      000000FFFF00000000000200000001000000000500417269616C000600000000
      00000000000A0000000100020000000000FFFFFF000000000200000000000000
      0000D927000006004D656D6F3639000200A4020000FC01000038000000140000
      0003000F0001000000000000000000FFFFFF1F2C020000000000010002003130
      00000000FFFF00000000000200000001000000000500417269616C0006000000
      0000000000000A0000000100020000000000FFFFFF0000000002000000000000
      0000005A28000006004D656D6F3730000200DC020000FC010000380000001400
      000003000F0001000000000000000000FFFFFF1F2C0200000000000100020031
      3100000000FFFF00000000000200000001000000000500417269616C00060000
      000000000000000A0000000100020000000000FFFFFF00000000020000000000
      00000000DB28000006004D656D6F373100020014030000FC0100003800000014
      00000003000F0001000000000000000000FFFFFF1F2C02000000000001000200
      313200000000FFFF00000000000200000001000000000500417269616C000600
      00000000000000000A0000000100020000000000FFFFFF000000000200000000
      00000000005C29000006004D656D6F37320002004C030000FC01000038000000
      1400000003000F0001000000000000000000FFFFFF1F2C020000000000010002
      00313300000000FFFF00000000000200000001000000000500417269616C0006
      0000000000000000000A0000000100020000000000FFFFFF0000000002000000
      000000000000DD29000006004D656D6F373300020084030000FC010000400000
      001400000003000F0001000000000000000000FFFFFF1F2C0200000000000100
      0200313400000000FFFF00000000000200000001000000000500417269616C00
      060000000000000000000A0000000100020000000000FFFFFF00000000020000
      00000000000000742A000006004D656D6F3734000200DC020000840100003800
      00007800000003000F0001000000000000000000FFFFFF1F2C02000000000003
      000500D1F3ECECE00D0400CDC4D12C0D0900F0F3E12E20EAEEEF2E00000000FF
      FF00000000000200000001000000000500417269616C00090000000000000000
      000A0000000100020000000000FFFFFF0000000002000000000000000000F52A
      000006004D656D6F3735000200C4030000FC010000540000001400000003000F
      0001000000000000000000FFFFFF1F2C02000000000001000200313500000000
      FFFF00000000000200000001000000000500417269616C000600000000000000
      00000A0000000100020000000000FFFFFF000000000200000000000000000074
      2B000006004D656D6F37360002004C0000002C0200003C000000140000000300
      0F0001000000000000000000FFFFFF1F2C0200000000000100000000000000FF
      FF00000000000200000001000000000500417269616C000A0000000000000000
      000A0000000100020000000000FFFFFF0000000002000000000000000000F32B
      000006004D656D6F3737000200880000002C020000400000001400000003000F
      0001000000000000000000FFFFFF1F2C0200000000000100000000000000FFFF
      00000000000200000001000000000500417269616C000A000000000000000000
      0A0000000100020000000000FFFFFF0000000002000000000000000000A12C00
      0006004D656D6F3738000200C80000002C0200009C0000001400000003000F00
      01000000000000000000FFFFFF1F2C02000000000001002F005B446174615365
      74496E766F6963655072696E744974656D2E224954454D244D4154455249414C
      245449544C45225D00000000FFFF000000000002000000010000000005004172
      69616C000A000000000000000000080000000100020000000000FFFFFF000000
      0002000000000000000000202D000006004D656D6F3739000200640100002C02
      0000300000001400000003000F0001000000000000000000FFFFFF1F2C020000
      0000000100000000000000FFFF00000000000200000001000000000500417269
      616C000A0000000000000000000A0000000100020000000000FFFFFF00000000
      02000000000000000000D42D000006004D656D6F3830000200940100002C0200
      002C0000001400000003000F0001000000000000000000FFFFFF1F2C02000000
      0000010035005B44617461536574496E766F6963655072696E744974656D2E22
      4954454D244D45415355524524574549474854245449544C45225D00000000FF
      FF00000000000200000001000000000500417269616C000A0000000000000000
      000A0000000100020000000000FFFFFF00000000020000000000000000008A2E
      000006004D656D6F3831000200C00100002C0200002C0000001400000003000F
      0001000000000000000000FFFFFF1F2C020000000000010037005B4461746153
      6574496E766F6963655072696E744974656D2E224954454D244D454153555245
      245155414E54495459245449544C45225D00000000FFFF000000000002000000
      01000000000500417269616C000A0000000000000000000A0000000100020000
      000000FFFFFF0000000002000000000000000000392F000006004D656D6F3832
      000200EC0100002C020000400000001400000043000F00010000000000000000
      00FFFFFF1F2C020000000000010030005B44617461536574496E766F69636550
      72696E744974656D2E224954454D244D4154455249414C24574549474854225D
      00000000FFFF00000000000200000001000000000500417269616C000A000000
      0000000000000A0000000100020000000000FFFFFF0000000002000000000000
      000000EA2F000006004D656D6F38330002002C0200002C020000400000001400
      000043000F0001000000000000000000FFFFFF1F2C020000000000010032005B
      44617461536574496E766F6963655072696E744974656D2E224954454D244D41
      54455249414C245155414E54495459225D00000000FFFF000000000002000000
      01000000000500417269616C000A0000000000000000000A0000000100020000
      000000FFFFFF00000000020000000000000000006930000006004D656D6F3834
      0002006C0200002C020000380000001400000003000F00010000000000000000
      00FFFFFF1F2C0200000000000100000000000000FFFF00000000000200000001
      000000000500417269616C000A0000000000000000000A000000010002000000
      0000FFFFFF0000000002000000000000000000E830000006004D656D6F383500
      0200A40200002C020000380000001400000003000F0001000000000000000000
      FFFFFF1F2C0200000000000100000000000000FFFF0000000000020000000100
      0000000500417269616C000A0000000000000000000A00000001000200000000
      00FFFFFF00000000020000000000000000006731000006004D656D6F38360002
      00DC0200002C020000380000001400000003000F0001000000000000000000FF
      FFFF1F2C0200000000000100000000000000FFFF000000000002000000010000
      00000500417269616C000A0000000000000000000A0000000100020000000000
      FFFFFF0000000002000000000000000000E631000006004D656D6F3837000200
      140300002C020000380000001400000003000F0001000000000000000000FFFF
      FF1F2C0200000000000100000000000000FFFF00000000000200000001000000
      000500417269616C000A0000000000000000000A0000000100020000000000FF
      FFFF00000000020000000000000000006532000006004D656D6F38380002004C
      0300002C020000380000001400000003000F0001000000000000000000FFFFFF
      1F2C0200000000000100000000000000FFFF0000000000020000000100000000
      0500417269616C000A0000000000000000000A0000000100020000000000FFFF
      FF0000000002000000000000000000E432000006004D656D6F38390002008403
      00002C020000400000001400000003000F0001000000000000000000FFFFFF1F
      2C0200000000000100000000000000FFFF000000000002000000010000000005
      00417269616C000A0000000000000000000A0000000100020000000000FFFFFF
      00000000020000000000000000006333000006004D656D6F3930000200C40300
      002C020000540000001400000003000F0001000000000000000000FFFFFF1F2C
      0200000000000100000000000000FFFF00000000000200000001000000000500
      417269616C000A0000000000000000000A0000000100020000000000FFFFFF00
      000000020000000000000000001234000006004D656D6F353300020060020000
      680000003A000000140000008300000001000000000000000000FFFFFF1F2C02
      0000000000010030005B44617461536574496E766F6963655072696E74486561
      6465722E224845414445522444455349474E4154494F4E225D00000000FFFF00
      000000000200000001000000000500417269616C000C00000002000000000002
      0000000100020000000000FFFFFF00000000020000000000000000009F340000
      06004D656D6F35320002004C000000700200006C000000140000000300000001
      000000000000000000FFFFFF1F2C02000000000001000E00C2F1E5E3EE20EEF2
      EFF3F9E5EDEE00000000FFFF0000000000020000000100000000050041726961
      6C000A000000000000000000000000000100020000000000FFFFFF0000000002
      0000000000000000001E35000006004D656D6F3931000200BC00000070020000
      78010000140000008300000001000000000000000000FFFFFF1F2C0200000000
      000100000000000000FFFF00000000000200000001000000000500417269616C
      000A000000000000000000000000000100020000000000FFFFFF000000000200
      00000000000000009D35000006004D656D6F393200020050010000B00200006C
      000000140000008300000001000000000000000000FFFFFF1F2C020000000000
      0100000000000000FFFF00000000000200000001000000000500417269616C00
      0A000000000000000000000000000100020000000000FFFFFF00000000020000
      000000000000002536000006004D656D6F393300020050010000C40200006C00
      00000C0000000300000001000000000000000000FFFFFF1F2C02000000000001
      00090028EFEEE4EFE8F1FC2900000000FFFF0000000000020000000100000000
      0500417269616C0006000000000000000000020000000100020000000000FFFF
      FF0000000002000000000000000000B936000006004D656D6F3934000200C001
      0000C40200008C0000000C0000000300000001000000000000000000FFFFFF1F
      2C0200000000000100150028F0E0F1F8E8F4F0EEE2EAE020EFEEE4EFE8F1E829
      00000000FFFF00000000000200000001000000000500417269616C0006000000
      000000000000020000000100020000000000FFFFFF0000000002000000000000
      0000003837000006004D656D6F3935000200C0010000B00200008C0000001400
      00008300000001000000000000000000FFFFFF1F2C0200000000000100000000
      000000FFFF00000000000200000001000000000500417269616C000A00000000
      0000000000000000000100020000000000FFFFFF000000000200000000000000
      0000B737000006004D656D6F3938000200C0000000B00200008C000000140000
      008300000001000000000000000000FFFFFF1F2C020000000000010000000000
      0000FFFF00000000000200000001000000000500417269616C000A0000000000
      00000000000000000100020000000000FFFFFF00000000020000000000000000
      003638000006004D656D6F393900020024030000880200005800000014000000
      8300000001000000000000000000FFFFFF1F2C02000000000001000000000000
      00FFFF00000000000200000001000000000500417269616C000A000000000000
      000000000000000100020000000000FFFFFF0000000002000000000000000000
      CB38000007004D656D6F31303000020084020000880200009C00000014000000
      0300000001000000000000000000FFFFFF1F2C02000000000001001500E220F2
      EEEC20F7E8F1EBE520F1F3ECECE020CDC4D100000000FFFF0000000000020000
      0001000000000500417269616C000A0000000000000000000000000001000200
      00000000FFFFFF00000000020000000000000000004F39000007004D656D6F31
      303100020038020000880200001C000000140000000300000001000000000000
      000000FFFFFF1F2C02000000000001000400EAEEEF2E00000000FFFF00000000
      000200000001000000000500417269616C000A00000000000000000000000000
      0100020000000000FFFFFF0000000002000000000000000000CF39000007004D
      656D6F313032000200BC01000088020000780000001400000083000000010000
      00000000000000FFFFFF1F2C0200000000000100000000000000FFFF00000000
      000200000001000000000500417269616C000A00000000000000000000000000
      0100020000000000FFFFFF0000000002000000000000000000533A000007004D
      656D6F3130330002009801000088020000200000001400000003000000010000
      00000000000000FFFFFF1F2C02000000000001000400F0F3E12E00000000FFFF
      00000000000200000001000000000500417269616C000A000000000000000000
      000000000100020000000000FFFFFF0000000002000000000000000000D33A00
      0007004D656D6F31303400020090000000880200000001000014000000830000
      0001000000000000000000FFFFFF1F2C0200000000000100000000000000FFFF
      00000000000200000001000000000500417269616C000A000000000000000000
      000000000100020000000000FFFFFF00000000020000000000000000005B3B00
      0007004D656D6F3130350002004C000000880200004000000014000000030000
      0001000000000000000000FFFFFF1F2C02000000000001000800EDE020F1F3EC
      ECF300000000FFFF00000000000200000001000000000500417269616C000A00
      0000000000000000000000000100020000000000FFFFFF000000000200000000
      0000000000E73B000007004D656D6F3130360002003802000070020000600000
      00140000000300000001000000000000000000FFFFFF1F2C0200000000000100
      0C00EDE0E8ECE5EDEEE2E0EDE8E900000000FFFF000000000002000000010000
      00000500417269616C000A000000000000000000000000000100020000000000
      FFFFFF00000000020000000000000000006B3C000007004D656D6F3130370002
      00800300008802000020000000140000000300000001000000000000000000FF
      FFFF1F2C02000000000001000400F0F3E12E00000000FFFF0000000000020000
      0001000000000500417269616C000A0000000000000000000000000001000200
      00000000FFFFFF0000000002000000000000000000EB3C000007004D656D6F31
      3038000200A40300008802000058000000140000008300000001000000000000
      000000FFFFFF1F2C0200000000000100000000000000FFFF0000000000020000
      0001000000000500417269616C000A0000000000000000000000000001000200
      00000000FFFFFF00000000020000000000000000006F3D000007004D656D6F31
      303900020014040000900300001C000000140000000300000001000000000000
      000000FFFFFF1F2C02000000000001000400EAEEEF2E00000000FFFF00000000
      000200000001000000000500417269616C000A00000000000000000000000000
      0100020000000000FFFFFF0000000002000000000000000000FE3D000007004D
      656D6F3131300002004C000000B0020000700000001400000003000000010000
      00000000000000FFFFFF1F2C02000000000001000F00CEF2EFF3F1EA20F0E0E7
      F0E5F8E8EB00000000FFFF00000000000200000001000000000500417269616C
      000A000000000000000000000000000100020000000000FFFFFF000000000200
      0000000000000000893E000007004D656D6F313131000200C0000000C4020000
      8C0000000C0000000300000001000000000000000000FFFFFF1F2C0200000000
      0001000B0028E4EEEBE6EDEEF1F2FC2900000000FFFF00000000000200000001
      000000000500417269616C000600000000000000000002000000010002000000
      0000FFFFFF00000000020000000000000000001A3F000007004D656D6F313132
      00020064020000B00200008C0000001400000003000000010000000000000000
      00FFFFFF1F2C02000000000001001100C3EBE0E2EDFBE920E1F3F5E3E0EBF2E5
      F000000000FFFF00000000000200000001000000000500417269616C000A0000
      00020000000000000000000100020000000000FFFFFF00000000020000000000
      00000000993F000006004D656D6F3936000200F4020000B00200006C00000014
      0000008300000001000000000000000000FFFFFF1F2C02000000000001000000
      00000000FFFF00000000000200000001000000000500417269616C000A000000
      000000000000000000000100020000000000FFFFFF0000000002000000000000
      0000002140000006004D656D6F3937000200F4020000C40200006C0000000C00
      00000300000001000000000000000000FFFFFF1F2C0200000000000100090028
      EFEEE4EFE8F1FC2900000000FFFF000000000002000000010000000005004172
      69616C0006000000000000000000020000000100020000000000FFFFFF000000
      0002000000000000000000A140000007004D656D6F31313300020064030000B0
      0200008C000000140000008300000001000000000000000000FFFFFF1F2C0200
      000000000100000000000000FFFF000000000002000000010000000005004172
      69616C000A000000000000000000000000000100020000000000FFFFFF000000
      00020000000000000000003641000007004D656D6F31313400020064030000C4
      0200008C0000000C0000000300000001000000000000000000FFFFFF1F2C0200
      000000000100150028F0E0F1F8E8F4F0EEE2EAE020EFEEE4EFE8F1E829000000
      00FFFF00000000000200000001000000000500417269616C0006000000000000
      000000020000000100020000000000FFFFFF0000000002000000000000000000
      BE41000007004D656D6F3131350002004C000000E40200004000000014000000
      0300000001000000000000000000FFFFFF1F2C02000000000001000800CEF2EF
      F3F1F2E8EB00000000FFFF00000000000200000001000000000500417269616C
      000A000000000000000000000000000100020000000000FFFFFF000000000200
      00000000000000003E42000007004D656D6F31313600020090000000E4020000
      8C000000140000008300000001000000000000000000FFFFFF1F2C0200000000
      000100000000000000FFFF00000000000200000001000000000500417269616C
      000A000000000000000000000000000100020000000000FFFFFF000000000200
      0000000000000000BE42000007004D656D6F31313700020020010000E4020000
      6C000000140000008300000001000000000000000000FFFFFF1F2C0200000000
      000100000000000000FFFF00000000000200000001000000000500417269616C
      000A000000000000000000000000000100020000000000FFFFFF000000000200
      00000000000000003E43000007004D656D6F31313800020090010000E4020000
      8C000000140000008300000001000000000000000000FFFFFF1F2C0200000000
      000100000000000000FFFF00000000000200000001000000000500417269616C
      000A000000000000000000000000000100020000000000FFFFFF000000000200
      0000000000000000C943000007004D656D6F31313900020090000000F8020000
      8C0000000C0000000300000001000000000000000000FFFFFF1F2C0200000000
      0001000B0028E4EEEBE6EDEEF1F2FC2900000000FFFF00000000000200000001
      000000000500417269616C000600000000000000000002000000010002000000
      0000FFFFFF00000000020000000000000000005244000007004D656D6F313230
      00020020010000F80200006C0000000C00000003000000010000000000000000
      00FFFFFF1F2C0200000000000100090028EFEEE4EFE8F1FC2900000000FFFF00
      000000000200000001000000000500417269616C000600000000000000000002
      0000000100020000000000FFFFFF0000000002000000000000000000E7440000
      07004D656D6F31323100020090010000F80200008C0000000C00000003000000
      01000000000000000000FFFFFF1F2C0200000000000100150028F0E0F1F8E8F4
      F0EEE2EAE020EFEEE4EFE8F1E82900000000FFFF000000000002000000010000
      00000500417269616C0006000000000000000000020000000100020000000000
      FFFFFF00000000020000000000000000006E45000007004D656D6F3132320002
      0028020000E402000038000000140000000300000001000000000000000000FF
      FFFF1F2C02000000000001000700CFEEEBF3F7E8EB00000000FFFF0000000000
      0200000001000000000500417269616C000A0000000000000000000000000001
      00020000000000FFFFFF0000000002000000000000000000EE45000007004D65
      6D6F31323300020064020000E40200008C000000140000008300000001000000
      000000000000FFFFFF1F2C0200000000000100000000000000FFFF0000000000
      0200000001000000000500417269616C000A0000000000000000000000000001
      00020000000000FFFFFF00000000020000000000000000006E46000007004D65
      6D6F313234000200F4020000E40200006C000000140000008300000001000000
      000000000000FFFFFF1F2C0200000000000100000000000000FFFF0000000000
      0200000001000000000500417269616C000A0000000000000000000000000001
      00020000000000FFFFFF0000000002000000000000000000EE46000007004D65
      6D6F31323500020064030000E40200008C000000140000008300000001000000
      000000000000FFFFFF1F2C0200000000000100000000000000FFFF0000000000
      0200000001000000000500417269616C000A0000000000000000000000000001
      00020000000000FFFFFF00000000020000000000000000007947000007004D65
      6D6F31323600020064020000F80200008C0000000C0000000300000001000000
      000000000000FFFFFF1F2C02000000000001000B0028E4EEEBE6EDEEF1F2FC29
      00000000FFFF00000000000200000001000000000500417269616C0006000000
      000000000000020000000100020000000000FFFFFF0000000002000000000000
      0000000248000007004D656D6F313237000200F4020000F80200006C0000000C
      0000000300000001000000000000000000FFFFFF1F2C02000000000001000900
      28EFEEE4EFE8F1FC2900000000FFFF0000000000020000000100000000050041
      7269616C0006000000000000000000020000000100020000000000FFFFFF0000
      0000020000000000000000009748000007004D656D6F31323800020064030000
      F80200008C0000000C0000000300000001000000000000000000FFFFFF1F2C02
      00000000000100150028F0E0F1F8E8F4F0EEE2EAE020EFEEE4EFE8F1E8290000
      0000FFFF00000000000200000001000000000500417269616C00060000000000
      00000000020000000100020000000000FFFFFF00000000020000000000000000
      00194900000B004672616D65426F74746F6D0002004C0000002C020000CC0300
      00140000000300070002000000000000000000FFFFFF1F2C0200000000000000
      00000000FFFF00000000000200000001000000000500417269616C0006000000
      020000000000020000000100020000000000FFFFFF0000000002000000000000
      0000009C49000007004D656D6F313239000200EC010000840100004000000078
      00000003000F0001000000000000000000FFFFFF1F2C02000000000001000300
      C2E5F100000000FFFF00000000000200000001000000000500417269616C0009
      0000000000000000000A0000000100020000000000FFFFFF0000000002000000
      000000000000194A000006004D656D6F34370002004C00000084010000CC0300
      007800000003000F0002000000000000000000FFFFFF1F2C0200000000000000
      00000000FFFF00000000000200000001000000000500417269616C000A000000
      000000000000080000000100020000000000FFFFFF0000000002000000000000
      000000964A000006004D656D6F35350002004C000000FC010000CC0300001400
      000003000F0002000000000000000000FFFFFF1F2C0200000000000000000000
      00FFFF00000000000200000001000000000500417269616C000A000000000000
      000000080000000100020000000000FFFFFF0000000002000000000000000000
      184B00000B004672616D654D6964646C650002004C0000002C020000CC030000
      140000000300050002000000000000000000FFFFFF1F2C020000000000000000
      000000FFFF00000000000200000001000000000500417269616C000600000002
      0000000000020000000100020000000000FFFFFF000000000200000000000000
      0000974B000008004672616D65546F700002004C0000002C020000CC03000014
      00000003000D0002000000000000000000FFFFFF1F2C02000000000000000000
      0000FFFF00000000000200000001000000000500417269616C00060000000200
      00000000020000000100020000000000FFFFFF00000000020000000000000000
      00234C000007004D656D6F3133300002004C000000C400000054000000140000
      000300000001000000000000000000FFFFFF1F2C02000000000001000C00CEF0
      E3E0EDE8E7E0F6E8FF2000000000FFFF00000000000200000001000000000500
      417269616C000A000000000000000000080000000100020000000000FFFFFF00
      0000000200000000000000FEFEFF000000000000000000000000FC0000000000
      00000000000000000000005800C182BFA04E42E340553BEBC252C3E440}
  end
  object DataSetCompanies: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, Title from AFFINAGE$COMPANY_S')
    Transaction = Transaction
    Database = dm.db
    Left = 168
    Top = 408
    oFetchAll = True
    object DataSetCompaniesID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetCompaniesTITLE: TFIBStringField
      FieldName = 'TITLE'
      Size = 64
      EmptyStrToNull = True
    end
  end
  object Transaction: TpFIBTransaction
    DefaultDatabase = dm.db
    TimeoutAction = TARollback
    Left = 56
    Top = 280
  end
  object DataSetDepartments: TpFIBDataSet
    SelectSQL.Strings = (
      'select id, title from AFFINAGE$DEPARTMENT_S')
    Transaction = Transaction
    Database = dm.db
    Left = 56
    Top = 504
    oFetchAll = True
    object DataSetDepartmentsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetDepartmentsTITLE: TFIBStringField
      FieldName = 'TITLE'
      Size = 64
      EmptyStrToNull = True
    end
  end
  object DataSetMeasures: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, TITLE, DEsignation from AFFINAGE$MEASURE_S')
    Transaction = Transaction
    Database = dm.db
    Left = 88
    Top = 504
    oFetchAll = True
    object DataSetMeasuresID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetMeasuresTITLE: TFIBStringField
      FieldName = 'TITLE'
      Size = 32
      EmptyStrToNull = True
    end
    object DataSetMeasuresDESIGNATION: TFIBStringField
      FieldName = 'DESIGNATION'
      Size = 8
      EmptyStrToNull = True
    end
  end
  object DataSetInvoicePrintHeader: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  Header$Designation,'
      '  Header$Date$,'
      '  Header$Source$Company,'
      '  Header$Target$Company,'
      '  Header$Department$Title,'
      '  Header$Reason'
      'from '
      '  AFFINAGE$INVOICE$PRINT$HEADER(:ID);')
    BeforeOpen = DataSetInvoicePrintHeaderBeforeOpen
    Transaction = Transaction
    Database = dm.db
    Left = 168
    Top = 344
    oFetchAll = True
    object DataSetInvoicePrintHeaderHEADERDESIGNATION: TFIBIntegerField
      FieldName = 'HEADER$DESIGNATION'
    end
    object DataSetInvoicePrintHeaderHEADERDATE: TFIBDateTimeField
      FieldName = 'HEADER$DATE$'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object DataSetInvoicePrintHeaderHEADERSOURCECOMPANY: TFIBStringField
      FieldName = 'HEADER$SOURCE$COMPANY'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetInvoicePrintHeaderHEADERTARGETCOMPANY: TFIBStringField
      FieldName = 'HEADER$TARGET$COMPANY'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetInvoicePrintHeaderHEADERDEPARTMENTTITLE: TFIBStringField
      FieldName = 'HEADER$DEPARTMENT$TITLE'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetInvoicePrintHeaderHEADERREASON: TFIBStringField
      FieldName = 'HEADER$REASON'
      Size = 64
      EmptyStrToNull = True
    end
  end
  object DataSetInvoicePrintItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  Item$Operation$Title,'
      '  Item$Place$Of$Storage$Title,'
      '  Item$Weight,'
      '  Item$Material$Title,'
      '  Item$Measure$Weight$Title,'
      '  Item$Measure$Quantity$Title,'
      '  Item$Material$Weight,'
      '  Item$Material$Quantity,'
      '  ITEM$INDEX$IN$GROUP'
      'from'
      '  AFFINAGE$INVOICE$PRINT$ITEM2(:ID) ')
    BeforeOpen = DataSetInvoicePrintItemBeforeOpen
    Transaction = Transaction
    Database = dm.db
    Left = 168
    Top = 376
    oFetchAll = True
    object DataSetInvoicePrintItemITEMOPERATIONTITLE: TFIBStringField
      FieldName = 'ITEM$OPERATION$TITLE'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetInvoicePrintItemITEMPLACEOFSTORAGETITLE: TFIBStringField
      FieldName = 'ITEM$PLACE$OF$STORAGE$TITLE'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetInvoicePrintItemITEMWEIGHT: TFIBBCDField
      FieldName = 'ITEM$WEIGHT'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object DataSetInvoicePrintItemITEMMATERIALTITLE: TFIBStringField
      FieldName = 'ITEM$MATERIAL$TITLE'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetInvoicePrintItemITEMMEASUREWEIGHTTITLE: TFIBStringField
      FieldName = 'ITEM$MEASURE$WEIGHT$TITLE'
      Size = 8
      EmptyStrToNull = True
    end
    object DataSetInvoicePrintItemITEMMEASUREQUANTITYTITLE: TFIBStringField
      FieldName = 'ITEM$MEASURE$QUANTITY$TITLE'
      Size = 8
      EmptyStrToNull = True
    end
    object DataSetInvoicePrintItemITEMMATERIALWEIGHT: TFIBBCDField
      FieldName = 'ITEM$MATERIAL$WEIGHT'
      DisplayFormat = '#,##0.000'
      EditFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object DataSetInvoicePrintItemITEMMATERIALQUANTITY: TFIBIntegerField
      FieldName = 'ITEM$MATERIAL$QUANTITY'
    end
    object DataSetInvoicePrintItemITEMINDEXINGROUP: TFIBIntegerField
      FieldName = 'ITEM$INDEX$IN$GROUP'
    end
  end
  object DataSourcePlaceOfStorageCustom: TDataSource
    DataSet = DataSetPlaceOfStorageCustom
    Left = 248
    Top = 480
  end
  object DataSetPlaceOfStorageCustom: TpFIBDataSet
    SelectSQL.Strings = (
      'select id, title from AFFINAGE$PLACE$OF$STORAGE_S(:OPERATION$ID)')
    BeforeOpen = DataSetPlaceOfStorageCustomBeforeOpen
    Transaction = Transaction
    Database = dm.db
    Left = 280
    Top = 480
  end
  object DataSourcePlaceOfStorage: TDataSource
    DataSet = DataSetPlaceOfStorage
    Left = 248
    Top = 448
  end
  object frDBDataSetInvoicePrintHeader: TfrDBDataSet
    DataSet = DataSetInvoicePrintHeader
    OpenDataSource = False
    Left = 136
    Top = 344
  end
  object frDBDataSetInvoicePrintItem: TfrDBDataSet
    DataSet = DataSetInvoicePrintItem
    OpenDataSource = False
    Left = 136
    Top = 376
  end
  object ReportInvoiceIn: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbPrint, pbExit, pbPageSetup]
    StoreInDFM = True
    Title = #1055#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1099#1081' '#1087#1088#1086#1089#1084#1086#1090#1088
    RebuildPrinter = False
    Left = 120
    Top = 312
    ReportForm = {
      190000003D3A0000190000000001000100FFFFFFFFFF090000009A0B00003408
      00004800000012000000120000001200000001FFFF00000000FFFF0000000000
      00000000000000030400466F726D000F000080DC000000780000007C0100002C
      010000040000000200CB0000000C005265706F72745469746C65310002010000
      0000240000002F040000380100003000000001000000000000000000FFFFFF1F
      00000000000000000000000000FFFF0000000000020000000100000000000000
      01000000C800000014000000010000000000000200390100000E005265706F72
      7453756D6D6172793100020100000000700200002F0400006C00000030000100
      01000000000000000000FFFFFF1F00000000000000000000000000FFFF000000
      000002000000010000000000000001000000C800000014000000010000000000
      000200BF0100000B004D6173746572446174613100020100000000180200002F
      040000140000003100050001000000000000000000FFFFFF1F000000001B0066
      72444244617461536574496E766F6963655072696E744974656D000000000000
      00FFFF000000000002000000010000000000000001000000C800000014000000
      0100000000000002002C0200000D004D61737465724865616465723100020100
      000000740100002F0400008C0000003000040001000000000000000000FFFFFF
      1F00000000000000000000000000FFFF00000000000200000001000000000000
      0001000000C800000014000000010000000000000200990200000D004D617374
      6572466F6F7465723100020100000000440200002F0400001400000030000600
      01000000000000000000FFFFFF1F00000000000000000000000000FFFF000000
      000002000000010000000000000001000000C800000014000000010000000000
      0000001803000008004672616D65546F700002004C00000044020000CC030000
      140000000300080002000000000000000000FFFFFF1F2C020000000000000000
      000000FFFF00000000000200000001000000000500417269616C000600000002
      0000000000020000000100020000000000FFFFFF000000000200000000000000
      00009A0300000B004672616D654D6964646C650002004C00000018020000CC03
      0000140000000300050002000000000000000000FFFFFF1F2C02000000000000
      0000000000FFFF00000000000200000001000000000500417269616C00060000
      00020000000000020000000100020000000000FFFFFF00000000020000000000
      000000001904000006004D656D6F34320002004C00000038010000CC03000014
      00000003000F0002000000000000000000FFFFFF1F2C02000000000001000000
      00000000FFFF00000000000200000001000000000500417269616C000A000000
      0000000000000A0000000100020000000000FFFFFF0000000002000000000000
      0000009604000006004D656D6F35350002004C000000EC010000CC0300001400
      000003000F0002000000000000000000FFFFFF1F2C0200000000000000000000
      00FFFF00000000000200000001000000000500417269616C000A000000000000
      000000080000000100020000000000FFFFFF0000000002000000000000000000
      1305000006004D656D6F34370002004C00000074010000CC0300007800000003
      000F0002000000000000000000FFFFFF1F2C020000000000000000000000FFFF
      00000000000200000001000000000500417269616C000A000000000000000000
      080000000100020000000000FFFFFF0000000002000000000000000000F50500
      0005004D656D6F310002006103000028000000BC000000240000000300000001
      000000000000000000FFFFFF1F2C02000000000003002100D2E8EFEEE2E0FF20
      ECE5E6EEF2F0E0F1EBE5E2E0FF20F4EEF0ECE020B920CC2D340D2C00D3F2E2E5
      F0E6E4E5EDE020CFEEF1F2E0EDEEE2EBE5EDE8E5EC20C3EEF1EAEEECF1F2E0F2
      E020D0EEF1F1E8E80D1100EEF22033302E31302E393720B9203731E000000000
      FFFF00000000000200000001000000020500417269616C000600000000000000
      0000000000000100020000000000FFFFFF000000000200000000000000000085
      06000005004D656D6F32000200970100006C000000C6000000140000000B0000
      0001000000000000000000FFFFFF1F2C02000000000001001200CFD0C8D5CEC4
      CDDBC920CED0C4C5D020B92000000000FFFF0000000000020000000100000000
      0500417269616C000C000000020000000000000000000100020000000000FFFF
      FF00000000020000000000000000000707000005004D656D6F34000200BD0300
      008C000000600000001400000003000F0001000000000000000100FFFFFF1F2C
      02000000000001000400CAEEE4FB00000000FFFF000000000002000000010000
      00020500417269616C000A000000000000000000020000000100020000000000
      FFFFFF00000000020000000000000000008307000005004D656D6F35000200BD
      030000B4000000600000001400000003000F0001000000000000000100FFFFFF
      1F2C020000000000000000000000FFFF00000000000200000001000000020500
      417269616C000A000000020000000000000000000100020000000000FFFFFF00
      000000020000000000000000000808000005004D656D6F37000200BD030000A0
      000000600000001400000003000F0001000000000000000100FFFFFF1F2C0200
      00000000010007003033313530303300000000FFFF0000000000020000000100
      0000020500417269616C000A0000000000000000000200000001000200000000
      00FFFFFF00000000020000000000000000008408000005004D656D6F36000200
      BC0300008C000000600000003C00000003000F0002000000000000000000FFFF
      FF1F2C020000000000000000000000FFFF000000000002000000010000000005
      00417269616C000A000000020000000000020000000100020000000000FFFFFF
      00000000020000000000000000000F09000005004D656D6F3800020048030000
      A000000074000000140000000300000001000000000000000000FFFFFF1F2C02
      000000000001000D00D4EEF0ECE020EFEE20CECAD3C400000000FFFF00000000
      000200000001000000000500417269616C000A00000000000000000001000000
      0100020000000000FFFFFF00000000020000000000000000009409000005004D
      656D6F3900020048030000B40000007400000014000000030000000100000000
      0000000000FFFFFF1F2C02000000000001000700EFEE20CECACFCE00000000FF
      FF00000000000200000001000000000500417269616C000A0000000000000000
      00010000000100020000000000FFFFFF0000000002000000000000000000460A
      000006004D656D6F3130000200A4000000B4000000A002000014000000830000
      0001000000000000000000FFFFFF1F2C020000000000010033005B4461746153
      6574496E766F6963655072696E744865616465722E2248454144455224534F55
      52434524434F4D50414E59225D00000000FFFF00000000000200000001000000
      000500417269616C000A000000000000000000000000000100020000000000FF
      FFFF0000000002000000000000000000D70A000006004D656D6F31310002004C
      000000E8000000540000005000000003000F0001000000000000000000FFFFFF
      1F2C02000000000002000400C4E0F2E00D0B00F1EEF1F2E0E2EBE5EDE8FF0000
      0000FFFF00000000000200000001000000000500417269616C000A0000000000
      000000000A0000000100020000000000FFFFFF00000000020000000000000000
      006B0B000006004D656D6F3132000200A0000000E80000004400000050000000
      03000F0001000000000000000000FFFFFF1F2C02000000000003000300CAEEE4
      0D0400E2E8E4E00D0800EEEFE5F0E0F6E8E800000000FFFF0000000000020000
      0001000000000500417269616C000A0000000000000000000A00000001000200
      00000000FFFFFF0000000002000000000000000000F30B000006004D656D6F31
      3300020068010000E8000000E40000001400000003000F000100000000000000
      0000FFFFFF1F2C02000000000001000900CFEEF1F2E0E2F9E8EA00000000FFFF
      00000000000200000001000000000500417269616C000A000000000000000000
      0A0000000100020000000000FFFFFF0000000002000000000000000000750C00
      0006004D656D6F313700020030020000FC0000001C0000003C00000003000F00
      01000000000000000000FFFFFF1F2C02000000000001000300EAEEE400000000
      FFFF00000000000200000001000000000500417269616C000A00000000000000
      00000A0000000100020000000000FFFFFF000000000200000000000000000000
      0D000006004D656D6F323000020068010000FC000000C80000003C0000000300
      0F0001000000000000000000FFFFFF1F2C02000000000001000C00EDE0E8ECE5
      EDEEE2E0EDE8E500000000FFFF00000000000200000001000000000500417269
      616C000A0000000000000000000A0000000100020000000000FFFFFF00000000
      02000000000000000000A90D000006004D656D6F32330002004C000000380100
      00540000001400000003000F0001000000000000000000FFFFFF1F2C02000000
      000001002A005B44617461536574496E766F6963655072696E74486561646572
      2E22484541444552244441544524225D00000000FFFF00000000000200000001
      000000000500417269616C000A0000000000000000000A000000010002000000
      0000FFFFFF0000000002000000000000000000280E000006004D656D6F323400
      0200A000000038010000440000001400000003000F0001000000000000000000
      FFFFFF1F2C0200000000000100000000000000FFFF0000000000020000000100
      0000000500417269616C000A0000000000000000000A00000001000200000000
      00FFFFFF0000000002000000000000000000DC0E000006004D656D6F32350002
      00E400000038010000840000001400000003000F0001000000000000000000FF
      FFFF1F2C020000000000010035005B44617461536574496E766F696365507269
      6E744865616465722E22484541444552244445504152544D454E54245449544C
      45225D00000000FFFF00000000000200000001000000000500417269616C000A
      000000000000000000080000000100020000000000FFFFFF0000000002000000
      0000000000008E0F000006004D656D6F32360002006801000038010000C80000
      001400000003000F0001000000000000000000FFFFFF1F2C0200000000000100
      33005B44617461536574496E766F6963655072696E744865616465722E224845
      414445522454415247455424434F4D50414E59225D00000000FFFF0000000000
      0200000001000000000500417269616C000A0000000000000000000800000001
      00020000000000FFFFFF00000000020000000000000000000D10000006004D65
      6D6F323800020030020000380100001C0000001400000003000F000100000000
      0000000000FFFFFF1F2C0200000000000100000000000000FFFF000000000002
      00000001000000000500417269616C000A0000000000000000000A0000000100
      020000000000FFFFFF00000000020000000000000000008C10000006004D656D
      6F32390002004C02000038010000480000001400000003000F00010000000000
      00000000FFFFFF1F2C0200000000000100000000000000FFFF00000000000200
      000001000000000500417269616C000A0000000000000000000A000000010002
      0000000000FFFFFF00000000020000000000000000001511000006004D656D6F
      3330000200B003000024010000680000001400000003000F0001000000000000
      000000FFFFFF1F2C02000000000001000A00EFEBE0F2E5E6EDEEE3EE00000000
      FFFF00000000000200000001000000000500417269616C000A00000000000000
      00000A0000000100020000000000FFFFFF0000000002000000000000000000A5
      11000006004D656D6F3331000200300300002401000080000000140000000300
      0F0001000000000000000000FFFFFF1F2C02000000000001001100F1EEEFF0EE
      E2EEE4E8F2E5EBFCEDEEE3EE00000000FFFF0000000000020000000100000000
      0500417269616C000A0000000000000000000A0000000100020000000000FFFF
      FF00000000020000000000000000003912000006004D656D6F33390002004C00
      000074010000580100002800000003000F0001000000000000000000FFFFFF1F
      2C02000000000001001500CCE0F2E5F0E8E0EBFCEDFBE520F6E5EDEDEEF1F2E8
      00000000FFFF00000000000200000001000000000500417269616C0009000000
      0000000000000A0000000100020000000000FFFFFF0000000002000000000000
      000000CB12000006004D656D6F3430000200A4010000740100007C0000002800
      000003000F0001000000000000000000FFFFFF1F2C02000000000002000700C5
      E4E8EDE8F6E00D0900E8E7ECE5F0E5EDE8FF00000000FFFF0000000000020000
      0001000000000500417269616C00090000000000000000000A00000001000200
      00000000FFFFFF00000000020000000000000000006F13000006004D656D6F34
      350002004C0000009C010000F00000005000000003000F000100000000000000
      0000FFFFFF1F2C02000000000003000D00EDE0E8ECE5EDEEE2E0EDE8E52C0D0D
      00F1EEF0F22C20F0E0E7ECE5F02C0D0500ECE0F0EAE000000000FFFF00000000
      000200000001000000000500417269616C00090000000000000000000A000000
      0100020000000000FFFFFF0000000002000000000000000000FA13000006004D
      656D6F3436000200C40100009C0100005C0000005000000003000F0001000000
      000000000000FFFFFF1F2C02000000000001000C00EDE0E8ECE5EDEEE2E0EDE8
      E500000000FFFF00000000000200000001000000000500417269616C00090000
      000000000000000A0000000100020000000000FFFFFF00000000020000000000
      000000008F14000006004D656D6F34390002003C0100009C0100006800000050
      00000003000F0001000000000000000000FFFFFF1F2C02000000000002000E00
      EDEEECE5EDEAEBE0F2F3F0EDFBE90D0500EDEEECE5F000000000FFFF00000000
      000200000001000000000500417269616C00090000000000000000000A000000
      0100020000000000FFFFFF00000000020000000000000000001115000006004D
      656D6F3530000200A40100009C010000200000005000000003000F0001000000
      000000000000FFFFFF1F2C02000000000001000300EAEEE400000000FFFF0000
      0000000200000001000000000500417269616C00090000000000000000000A00
      00000100020000000000FFFFFF0000000002000000000000000000BE15000006
      004D656D6F3531000200C403000074010000540000007800000003000F000100
      0000000000000000FFFFFF1F2C02000000000001002E00CFEEF0FFE4EAEEE2FB
      E920EDEEECE5F020E7E0EFE8F1E820EFEE20F1EAEBE0E4F1EAEEE920EAE0F0F2
      EEF2E5EAE500000000FFFF00000000000200000001000000000500417269616C
      000A0000000000000000000A0000000100020000000000FFFFFF000000000200
      00000000000000004D16000006004D656D6F3534000200800300007401000044
      0000007800000003000F0001000000000000000000FFFFFF1F2C020000000000
      02000500CDEEECE5F00D0800EFE0F1EFEEF0F2E000000000FFFF000000000002
      00000001000000000500417269616C00090000000000000000000A0000000100
      020000000000FFFFFF0000000002000000000000000000DD16000006004D656D
      6F3536000200A002000074010000380000007800000003000F00010000000000
      00000000FFFFFF1F2C02000000000002000500D6E5EDE02C0D0900F0F3E12E20
      EAEEEF2E00000000FFFF00000000000200000001000000000500417269616C00
      090000000000000000000A0000000100020000000000FFFFFF00000000020000
      000000000000008117000006004D656D6F3537000200D8020000740100003800
      00007800000003000F0001000000000000000000FFFFFF1F2C02000000000004
      000600D1F3ECECE0200D0900E1E5E720F3F7E5F2E00D0400CDC4D12C0D0900F0
      F3E12E20EAEEEF2E00000000FFFF000000000002000000010000000005004172
      69616C00090000000000000000000A0000000100020000000000FFFFFF000000
      00020000000000000000002318000006004D656D6F3538000200480300007401
      0000380000007800000003000F0001000000000000000000FFFFFF1F2C020000
      00000004000500C2F1E5E3EE0D0800F120F3F7E5F2EEEC0D0400CDC4D12C0D09
      00F0F3E12E20EAEEEF2E00000000FFFF00000000000200000001000000000500
      417269616C00090000000000000000000A0000000100020000000000FFFFFF00
      00000002000000000000000000A318000006004D656D6F36300002004C000000
      EC010000F00000001400000003000F0001000000000000000000FFFFFF1F2C02
      0000000000010001003100000000FFFF00000000000200000001000000000500
      417269616C00060000000000000000000A0000000100020000000000FFFFFF00
      000000020000000000000000002319000006004D656D6F36320002003C010000
      EC010000680000001400000003000F0001000000000000000000FFFFFF1F2C02
      0000000000010001003200000000FFFF00000000000200000001000000000500
      417269616C00060000000000000000000A0000000100020000000000FFFFFF00
      00000002000000000000000000A319000006004D656D6F3633000200A4010000
      EC010000200000001400000003000F0001000000000000000000FFFFFF1F2C02
      0000000000010001003300000000FFFF00000000000200000001000000000500
      417269616C00060000000000000000000A0000000100020000000000FFFFFF00
      00000002000000000000000000231A000006004D656D6F3634000200C4010000
      EC0100005C0000001400000003000F0001000000000000000000FFFFFF1F2C02
      0000000000010001003400000000FFFF00000000000200000001000000000500
      417269616C00060000000000000000000A0000000100020000000000FFFFFF00
      00000002000000000000000000A31A000006004D656D6F363500020020020000
      EC010000400000001400000003000F0001000000000000000000FFFFFF1F2C02
      0000000000010001003500000000FFFF00000000000200000001000000000500
      417269616C00060000000000000000000A0000000100020000000000FFFFFF00
      00000002000000000000000000231B000006004D656D6F363600020060020000
      EC010000400000001400000003000F0001000000000000000000FFFFFF1F2C02
      0000000000010001003600000000FFFF00000000000200000001000000000500
      417269616C00060000000000000000000A0000000100020000000000FFFFFF00
      00000002000000000000000000A31B000006004D656D6F3637000200A0020000
      EC010000380000001400000003000F0001000000000000000000FFFFFF1F2C02
      0000000000010001003700000000FFFF00000000000200000001000000000500
      417269616C00060000000000000000000A0000000100020000000000FFFFFF00
      00000002000000000000000000231C000006004D656D6F3638000200D8020000
      EC010000380000001400000003000F0001000000000000000000FFFFFF1F2C02
      0000000000010001003800000000FFFF00000000000200000001000000000500
      417269616C00060000000000000000000A0000000100020000000000FFFFFF00
      00000002000000000000000000A31C000006004D656D6F363900020010030000
      EC010000380000001400000003000F0001000000000000000000FFFFFF1F2C02
      0000000000010001003900000000FFFF00000000000200000001000000000500
      417269616C00060000000000000000000A0000000100020000000000FFFFFF00
      00000002000000000000000000241D000006004D656D6F373000020048030000
      EC010000380000001400000003000F0001000000000000000000FFFFFF1F2C02
      000000000001000200313000000000FFFF000000000002000000010000000005
      00417269616C00060000000000000000000A0000000100020000000000FFFFFF
      0000000002000000000000000000A51D000006004D656D6F3731000200800300
      00EC010000440000001400000003000F0001000000000000000000FFFFFF1F2C
      02000000000001000200313100000000FFFF0000000000020000000100000000
      0500417269616C00060000000000000000000A0000000100020000000000FFFF
      FF0000000002000000000000000000261E000006004D656D6F3732000200C403
      0000EC010000540000001400000003000F0001000000000000000000FFFFFF1F
      2C02000000000001000200313200000000FFFF00000000000200000001000000
      000500417269616C00060000000000000000000A0000000100020000000000FF
      FFFF0000000002000000000000000000BD1E000006004D656D6F373400020010
      03000074010000380000007800000003000F0001000000000000000000FFFFFF
      1F2C02000000000003000500D1F3ECECE00D0400CDC4D12C0D0900F0F3E12E20
      EAEEEF2E00000000FFFF00000000000200000001000000000500417269616C00
      090000000000000000000A0000000100020000000000FFFFFF00000000020000
      000000000000006B1F000006004D656D6F37380002004C00000018020000F000
      00001400000003000F0001000000000000000000FFFFFF1F2C02000000000001
      002F005B44617461536574496E766F6963655072696E744974656D2E22495445
      4D244D4154455249414C245449544C45225D00000000FFFF0000000000020000
      0001000000000500417269616C000A0000000000000000000800000001000200
      00000000FFFFFF0000000002000000000000000000EA1F000006004D656D6F37
      390002003C01000018020000680000001400000003000F000100000000000000
      0000FFFFFF1F2C0200000000000100000000000000FFFF000000000002000000
      01000000000500417269616C000A0000000000000000000A0000000100020000
      000000FFFFFF00000000020000000000000000006920000006004D656D6F3830
      000200A401000018020000200000001400000003000F00010000000000000000
      00FFFFFF1F2C0200000000000100000000000000FFFF00000000000200000001
      000000000500417269616C000A0000000000000000000A000000010002000000
      0000FFFFFF00000000020000000000000000001D21000006004D656D6F383100
      0200C4010000180200005C0000001400000003000F0001000000000000000000
      FFFFFF1F2C020000000000010035005B44617461536574496E766F6963655072
      696E744974656D2E224954454D244D4541535552452457454947485424544954
      4C45225D00000000FFFF00000000000200000001000000000500417269616C00
      0A0000000000000000000A0000000100020000000000FFFFFF00000000020000
      00000000000000CC21000006004D656D6F383200020060020000180200004000
      00001400000043000F0001000000000000000000FFFFFF1F2C02000000000001
      0030005B44617461536574496E766F6963655072696E744974656D2E22495445
      4D244D4154455249414C24574549474854225D00000000FFFF00000000000200
      000001000000000500417269616C000A0000000000000000000A000000010002
      0000000000FFFFFF00000000020000000000000000004B22000006004D656D6F
      3835000200A002000018020000380000001400000003000F0001000000000000
      000000FFFFFF1F2C0200000000000100000000000000FFFF0000000000020000
      0001000000000500417269616C000A0000000000000000000A00000001000200
      00000000FFFFFF0000000002000000000000000000CA22000006004D656D6F38
      36000200D802000018020000380000001400000003000F000100000000000000
      0000FFFFFF1F2C0200000000000100000000000000FFFF000000000002000000
      01000000000500417269616C000A0000000000000000000A0000000100020000
      000000FFFFFF00000000020000000000000000004923000006004D656D6F3837
      0002001003000018020000380000001400000003000F00010000000000000000
      00FFFFFF1F2C0200000000000100000000000000FFFF00000000000200000001
      000000000500417269616C000A0000000000000000000A000000010002000000
      0000FFFFFF0000000002000000000000000000C823000006004D656D6F383800
      02004803000018020000380000001400000003000F0001000000000000000000
      FFFFFF1F2C0200000000000100000000000000FFFF0000000000020000000100
      0000000500417269616C000A0000000000000000000A00000001000200000000
      00FFFFFF00000000020000000000000000004724000006004D656D6F38390002
      008003000018020000440000001400000003000F0001000000000000000000FF
      FFFF1F2C0200000000000100000000000000FFFF000000000002000000010000
      00000500417269616C000A0000000000000000000A0000000100020000000000
      FFFFFF0000000002000000000000000000C624000006004D656D6F3930000200
      C403000018020000540000001400000003000F0001000000000000000000FFFF
      FF1F2C0200000000000100000000000000FFFF00000000000200000001000000
      000500417269616C000A0000000000000000000A0000000100020000000000FF
      FFFF00000000020000000000000000007525000006004D656D6F353300020060
      0200006C0000003A000000140000008300000001000000000000000000FFFFFF
      1F2C020000000000010030005B44617461536574496E766F6963655072696E74
      4865616465722E224845414445522444455349474E4154494F4E225D00000000
      FFFF00000000000200000001000000000500417269616C000C00000002000000
      0000020000000100020000000000FFFFFF0000000002000000000000000000F4
      25000006004D656D6F3932000200180100007C0200006C000000140000008300
      000001000000000000000000FFFFFF1F2C0200000000000100000000000000FF
      FF00000000000200000001000000000500417269616C000A0000000000000000
      00000000000100020000000000FFFFFF00000000020000000000000000007C26
      000006004D656D6F393300020018010000900200006C0000000C000000030000
      0001000000000000000000FFFFFF1F2C0200000000000100090028EFEEE4EFE8
      F1FC2900000000FFFF00000000000200000001000000000500417269616C0006
      000000000000000000020000000100020000000000FFFFFF0000000002000000
      0000000000001027000006004D656D6F393400020088010000900200008C0000
      000C0000000300000001000000000000000000FFFFFF1F2C0200000000000100
      150028F0E0F1F8E8F4F0EEE2EAE020EFEEE4EFE8F1E82900000000FFFF000000
      00000200000001000000000500417269616C0006000000000000000000020000
      000100020000000000FFFFFF00000000020000000000000000008F2700000600
      4D656D6F3935000200880100007C0200008C0000001400000083000000010000
      00000000000000FFFFFF1F2C0200000000000100000000000000FFFF00000000
      000200000001000000000500417269616C000A00000000000000000000000000
      0100020000000000FFFFFF00000000020000000000000000000E28000006004D
      656D6F3938000200880000007C0200008C000000140000008300000001000000
      000000000000FFFFFF1F2C0200000000000100000000000000FFFF0000000000
      0200000001000000000500417269616C000A0000000000000000000000000001
      00020000000000FFFFFF00000000020000000000000000009228000007004D65
      6D6F31303900020014040000900300001C000000140000000300000001000000
      000000000000FFFFFF1F2C02000000000001000400EAEEEF2E00000000FFFF00
      000000000200000001000000000500417269616C000A00000000000000000000
      0000000100020000000000FFFFFF000000000200000000000000000018290000
      07004D656D6F3131300002004C0000007C020000380000001400000003000000
      01000000000000000000FFFFFF1F2C02000000000001000600CFF0E8EDFFEB00
      000000FFFF00000000000200000001000000000500417269616C000A00000000
      0000000000000000000100020000000000FFFFFF000000000200000000000000
      0000A329000007004D656D6F31313100020088000000900200008C0000000C00
      00000300000001000000000000000000FFFFFF1F2C02000000000001000B0028
      E4EEEBE6EDEEF1F2FC2900000000FFFF00000000000200000001000000000500
      417269616C0006000000000000000000020000000100020000000000FFFFFF00
      00000002000000000000000000272A000007004D656D6F3131350002004C0000
      00B802000038000000140000000300000001000000000000000000FFFFFF1F2C
      02000000000001000400D1E4E0EB00000000FFFF000000000002000000010000
      00000500417269616C000A000000000000000000000000000100020000000000
      FFFFFF0000000002000000000000000000A72A000007004D656D6F3131360002
      0088000000B80200008C000000140000008300000001000000000000000000FF
      FFFF1F2C0200000000000100000000000000FFFF000000000002000000010000
      00000500417269616C000A000000000000000000000000000100020000000000
      FFFFFF0000000002000000000000000000272B000007004D656D6F3131370002
      0018010000B80200006C000000140000008300000001000000000000000000FF
      FFFF1F2C0200000000000100000000000000FFFF000000000002000000010000
      00000500417269616C000A000000000000000000000000000100020000000000
      FFFFFF0000000002000000000000000000A72B000007004D656D6F3131380002
      0088010000B80200008C000000140000008300000001000000000000000000FF
      FFFF1F2C0200000000000100000000000000FFFF000000000002000000010000
      00000500417269616C000A000000000000000000000000000100020000000000
      FFFFFF0000000002000000000000000000322C000007004D656D6F3131390002
      0088000000CC0200008C0000000C0000000300000001000000000000000000FF
      FFFF1F2C02000000000001000B0028E4EEEBE6EDEEF1F2FC2900000000FFFF00
      000000000200000001000000000500417269616C000600000000000000000002
      0000000100020000000000FFFFFF0000000002000000000000000000BB2C0000
      07004D656D6F31323000020018010000CC0200006C0000000C00000003000000
      01000000000000000000FFFFFF1F2C0200000000000100090028EFEEE4EFE8F1
      FC2900000000FFFF00000000000200000001000000000500417269616C000600
      0000000000000000020000000100020000000000FFFFFF000000000200000000
      0000000000502D000007004D656D6F31323100020088010000CC0200008C0000
      000C0000000300000001000000000000000000FFFFFF1F2C0200000000000100
      150028F0E0F1F8E8F4F0EEE2EAE020EFEEE4EFE8F1E82900000000FFFF000000
      00000200000001000000000500417269616C0006000000000000000000020000
      000100020000000000FFFFFF0000000002000000000000000000DC2D00000700
      4D656D6F3133300002004C000000B40000005400000014000000030000000100
      0000000000000000FFFFFF1F2C02000000000001000C00CEF0E3E0EDE8E7E0F6
      E8FF2000000000FFFF00000000000200000001000000000500417269616C000A
      000000000000000000080000000100020000000000FFFFFF0000000002000000
      000000000000752E000007004D656D6F3133310002004C000000CC000000B400
      0000140000000300000001000000000000000000FFFFFF1F2C02000000000001
      001900D1F2F0F3EAF2F3F0EDEEE520EFEEE4F0E0E7E4E5EBE5EDE8E500000000
      FFFF00000000000200000001000000000500417269616C000A00000000000000
      0000080000000100020000000000FFFFFF00000000020000000000000000002A
      2F000007004D656D6F31333200020004010000CC000000400200001400000083
      00000001000000000000000000FFFFFF1F2C020000000000010035005B446174
      61536574496E766F6963655072696E744865616465722E224845414445522444
      45504152544D454E54245449544C45225D00000000FFFF000000000002000000
      01000000000500417269616C000A000000000000000000080000000100020000
      000000FFFFFF0000000002000000000000000000AD2F000005004D656D6F3300
      0200E4000000E8000000840000005000000003000F0001000000000000000000
      FFFFFF1F2C02000000000001000500D1EAEBE0E400000000FFFF000000000002
      00000001000000000500417269616C000A0000000000000000000A0000000100
      020000000000FFFFFF00000000020000000000000000004030000006004D656D
      6F31340002004C020000E8000000480000005000000003000F00010000000000
      00000000FFFFFF1F2C02000000000002000900D1F2F0E0F5EEE2E0FF0D0800EA
      EEECEFE0EDE8FF00000000FFFF00000000000200000001000000000500417269
      616C000A0000000000000000000A0000000100020000000000FFFFFF00000000
      02000000000000000000D530000006004D656D6F313500020094020000E80000
      009C0000001400000003000F0001000000000000000000FFFFFF1F2C02000000
      000001001600CAEEF0F0E5F1EFEEEDE4E8F0F3FEF9E8E920F1F7E5F200000000
      FFFF00000000000200000001000000000500417269616C000900000000000000
      00000A0000000100020000000000FFFFFF000000000200000000000000000063
      31000006004D656D6F313600020094020000FC000000380000003C0000000300
      0F0001000000000000000000FFFFFF1F2C02000000000002000500F1F7E5F22C
      0D0700F1F3E1F1F7E5F200000000FFFF00000000000200000001000000000500
      417269616C00090000000000000000000A0000000100020000000000FFFFFF00
      00000002000000000000000000FE31000006004D656D6F3138000200CC020000
      FC000000640000003C00000003000F0001000000000000000000FFFFFF1F2C02
      000000000003000300EAEEE40D0E00E0EDE0EBE8F2E8F7E5F1EAEEE3EE0D0500
      F3F7E5F2E000000000FFFF00000000000200000001000000000500417269616C
      00090000000000000000000A0000000100020000000000FFFFFF000000000200
      00000000000000008C32000006004D656D6F313900020030030000E8000000E8
      0000003C00000003000F0001000000000000000000FFFFFF1F2C020000000000
      01000F00CDEEECE5F020E4EEEAF3ECE5EDF2E000000000FFFF00000000000200
      000001000000000500417269616C00090000000000000000000A000000010002
      0000000000FFFFFF00000000020000000000000000000B33000006004D656D6F
      32310002009402000038010000380000001400000003000F0001000000000000
      000000FFFFFF1F2C0200000000000100000000000000FFFF0000000000020000
      0001000000000500417269616C000A0000000000000000000A00000001000200
      00000000FFFFFF00000000020000000000000000008A33000006004D656D6F32
      32000200CC02000038010000640000001400000003000F000100000000000000
      0000FFFFFF1F2C0200000000000100000000000000FFFF000000000002000000
      01000000000500417269616C000A0000000000000000000A0000000100020000
      000000FFFFFF00000000020000000000000000000A34000007004D656D6F3133
      330002003003000038010000800000001400000003000F000100000000000000
      0000FFFFFF1F2C0200000000000100000000000000FFFF000000000002000000
      01000000000500417269616C000A0000000000000000000A0000000100020000
      000000FFFFFF00000000020000000000000000008A34000007004D656D6F3133
      34000200B003000038010000680000001400000003000F000100000000000000
      0000FFFFFF1F2C0200000000000100000000000000FFFF000000000002000000
      01000000000500417269616C000A0000000000000000000A0000000100020000
      000000FFFFFF00000000020000000000000000001735000006004D656D6F3237
      000200200200009C010000400000005000000003000F00010000000000000000
      00FFFFFF1F2C02000000000002000200EFEE0D0900E4EEEAF3ECE5EDF2F30000
      0000FFFF00000000000200000001000000000500417269616C00090000000000
      000000000A0000000100020000000000FFFFFF00000000020000000000000000
      009D35000006004D656D6F3332000200600200009C0100004000000050000000
      03000F0001000000000000000000FFFFFF1F2C02000000000001000700EFF0E8
      EDFFF2EE00000000FFFF00000000000200000001000000000500417269616C00
      090000000000000000000A0000000100020000000000FFFFFF00000000020000
      000000000000002636000006004D656D6F333300020020020000740100008000
      00002800000003000F0001000000000000000000FFFFFF1F2C02000000000001
      000A00CAEEEBE8F7E5F1F2E2EE00000000FFFF00000000000200000001000000
      000500417269616C00090000000000000000000A0000000100020000000000FF
      FFFF0000000002000000000000000000AA36000006004D656D6F333400020020
      0200004402000040000000140000000300000001000000000000000000FFFFFF
      1F2C02000000000001000500C8F2EEE3EE00000000FFFF000000000002000000
      01000000000500417269616C000A0000000000000000000A0000000100020000
      000000FFFFFF00000000020000000000000000006037000006004D656D6F3335
      0002006002000044020000400000001400000043000F00020000000000000000
      00FFFFFF1F2C020000000000010037005B53554D285B44617461536574496E76
      6F6963655072696E744974656D2E224954454D244D4154455249414C24574549
      474854225D295D00000000FFFF00000000000200000001000000000500417269
      616C000A0000000000000000000A0000000100020000000000FFFFFF00000000
      020000000000000000000F38000006004D656D6F333700020020020000180200
      00400000001400000043000F0001000000000000000000FFFFFF1F2C02000000
      0000010030005B44617461536574496E766F6963655072696E744974656D2E22
      4954454D244D4154455249414C24574549474854225D00000000FFFF00000000
      000200000001000000000500417269616C000A0000000000000000000A000000
      0100020000000000FFFFFF00000000020000000000000000008E38000006004D
      656D6F3336000200A002000044020000380000001400000003000F0002000000
      000000000000FFFFFF1F2C0200000000000100000000000000FFFF0000000000
      0200000001000000000500417269616C000A0000000000000000000A00000001
      00020000000000FFFFFF00000000020000000000000000000D39000006004D65
      6D6F3338000200D802000044020000380000001400000003000F000200000000
      0000000000FFFFFF1F2C0200000000000100000000000000FFFF000000000002
      00000001000000000500417269616C000A0000000000000000000A0000000100
      020000000000FFFFFF00000000020000000000000000008C39000006004D656D
      6F34310002001003000044020000380000001400000003000F00020000000000
      00000000FFFFFF1F2C0200000000000100000000000000FFFF00000000000200
      000001000000000500417269616C000A0000000000000000000A000000010002
      0000000000FFFFFF00000000020000000000000000000B3A000006004D656D6F
      34330002004803000044020000380000001400000003000F0002000000000000
      000000FFFFFF1F2C0200000000000100000000000000FFFF0000000000020000
      0001000000000500417269616C000A0000000000000000000A00000001000200
      00000000FFFFFF000000000200000000000000FEFEFF00000000000000000000
      0000FC000000000000000000000000000000005800C182BFA04E42E340553BEB
      C252C3E440}
  end
  object TmpTransaction: TpFIBTransaction
    DefaultDatabase = dm.db
    TimeoutAction = TARollback
    Left = 24
    Top = 248
  end
  object Groups: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'GroupsIndex'
        Fields = 'Title'
      end
      item
        Name = 'GroupsIndex2'
        Fields = 'ID'
      end>
    IndexFieldNames = 'Title'
    Params = <>
    StoreDefs = True
    BeforePost = GroupsBeforePost
    Left = 440
    Top = 168
    object GroupsID: TAutoIncField
      FieldName = 'ID'
    end
    object GroupsOperationID: TIntegerField
      FieldName = 'Operation$ID'
    end
    object GroupsOperationTitle: TStringField
      FieldKind = fkLookup
      FieldName = 'Operation$Title'
      LookupDataSet = COperations
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'Operation$ID'
      Size = 64
      Lookup = True
    end
    object GroupsPlaceOfStorageId: TIntegerField
      FieldName = 'Place$Of$Storage$Id'
    end
    object GroupsPlaceOfStorageTitle: TStringField
      FieldKind = fkLookup
      FieldName = 'Place$Of$Storage$Title'
      LookupDataSet = CPlaceOfStorage
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'Place$Of$Storage$Id'
      Size = 64
      Lookup = True
    end
    object GroupsTitle: TStringField
      FieldName = 'Title'
      Size = 256
    end
    object GroupsWEIGHTRemainderIN: TBCDField
      FieldName = 'WEIGHT$Remainder$IN'
      Precision = 32
      Size = 3
    end
    object GroupsWEIGHTRemainderOUT: TBCDField
      FieldName = 'WEIGHT$Remainder$OUT'
      Precision = 32
      Size = 3
    end
    object GroupsItems: TDataSetField
      FieldName = 'Items'
    end
  end
  object GroupItems: TClientDataSet
    Aggregates = <>
    DataSetField = GroupsItems
    FieldDefs = <
      item
        Name = 'Operation$ID'
        Attributes = [faUnNamed]
        DataType = ftInteger
      end
      item
        Name = 'Place$Of$Storage$Id'
        Attributes = [faUnNamed]
        DataType = ftInteger
      end>
    IndexDefs = <
      item
        Name = 'GroupItemsIndex'
        Fields = 'Operation$ID;Place$Of$Storage$ID'
      end>
    IndexFieldNames = 'Operation$ID;Place$Of$Storage$ID'
    Params = <>
    StoreDefs = True
    BeforeInsert = GroupItemsBeforeInsert
    BeforeEdit = GroupItemsBeforeEdit
    BeforePost = GroupItemsBeforePost
    AfterPost = GroupItemsAfterPost
    AfterCancel = GroupItemsAfterCancel
    AfterDelete = GroupItemsAfterDelete
    Left = 440
    Top = 200
    object GroupItemsOperationID: TIntegerField
      FieldName = 'Operation$ID'
    end
    object GroupItemsOperationTitle: TStringField
      FieldKind = fkLookup
      FieldName = 'Operation$Title'
      LookupDataSet = COperations
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'Operation$ID'
      Size = 64
      Lookup = True
    end
    object GroupItemsPlaceOfStorageId: TIntegerField
      FieldName = 'Place$Of$Storage$Id'
    end
    object GroupItemsPlaceOfStorageTitle: TStringField
      FieldKind = fkLookup
      FieldName = 'Place$Of$Storage$Title'
      LookupDataSet = CPlaceOfStorage
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'Place$Of$Storage$Id'
      Size = 64
      Lookup = True
    end
  end
  object DataSourceGroups: TDataSource
    DataSet = Groups
    Left = 480
    Top = 168
  end
  object DataSourceGroupItems: TDataSource
    DataSet = GroupItems
    Left = 480
    Top = 200
  end
  object GroupItemVariants: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'GroupItemVariantsIndex'
        Fields = 'Operation$Title;Place$Of$Storage$Title'
      end>
    IndexFieldNames = 'Operation$Title;Place$Of$Storage$Title'
    Params = <>
    StoreDefs = True
    Left = 440
    Top = 240
    object GroupItemVariantsOperationID: TIntegerField
      FieldName = 'Operation$ID'
    end
    object GroupItemVariantsOperationTitle: TStringField
      FieldName = 'Operation$Title'
      Size = 64
    end
    object GroupItemVariantsPlaceOfStorageTitle: TStringField
      FieldName = 'Place$Of$Storage$Title'
      Size = 64
    end
    object GroupItemVariantsPlaceOfStorageId: TIntegerField
      FieldName = 'Place$Of$Storage$Id'
    end
  end
  object GroupItemsAll: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'GroupItemsAllIndex'
        Fields = 'Operation$ID;Place$Of$Storage$Id'
      end
      item
        Name = 'GroupItemsAllIndex2'
        Fields = 'Operation$ID'
      end
      item
        Name = 'GroupItemsAllIndex3'
        Fields = 'Place$Of$Storage$ID'
      end>
    IndexFieldNames = 'Operation$ID;Place$Of$Storage$Id'
    Params = <>
    StoreDefs = True
    Left = 440
    Top = 272
    object GroupItemsAllOperationID: TIntegerField
      FieldName = 'Operation$ID'
    end
    object GroupItemsAllPlaceOfStorageID: TIntegerField
      FieldName = 'Place$Of$Storage$Id'
    end
    object GroupItemsAllGroupID: TIntegerField
      FieldName = 'Group$ID'
    end
  end
  object DataSourceGroupItemVariants: TDataSource
    DataSet = GroupItemVariants
    Left = 480
    Top = 240
  end
  object PassportItems: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'ID'
        DataType = ftInteger
      end
      item
        Name = 'PASSPORT$ID'
        DataType = ftInteger
      end
      item
        Name = 'OPERATION$ID'
        DataType = ftInteger
      end
      item
        Name = 'OPERATION$TITLE'
        DataType = ftString
        Size = 64
      end
      item
        Name = 'PLACE$OF$STORAGE$ID'
        DataType = ftInteger
      end
      item
        Name = 'PLACE$OF$STORAGE$TITLE'
        DataType = ftString
        Size = 64
      end
      item
        Name = 'WEIGHT'
        DataType = ftBCD
        Precision = 32
        Size = 3
      end
      item
        Name = 'WEIGHT$OUT'
        DataType = ftBCD
        Precision = 32
        Size = 3
      end
      item
        Name = 'WEIGHT$IN'
        DataType = ftBCD
        Precision = 32
        Size = 3
      end
      item
        Name = 'WEIGHT$REMAINDER$CURRENT'
        DataType = ftBCD
        Precision = 32
        Size = 3
      end
      item
        Name = 'WEIGHT$REMAINDER$OUT'
        DataType = ftBCD
        Precision = 32
        Size = 3
      end
      item
        Name = 'WEIGHT$REMAINDER$IN'
        DataType = ftBCD
        Precision = 32
        Size = 3
      end
      item
        Name = 'WEIGHT$REMAINDER'
        DataType = ftBCD
        Precision = 32
        Size = 3
      end
      item
        Name = 'WEIGHT$NORM'
        DataType = ftBCD
        Precision = 32
        Size = 3
      end
      item
        Name = 'GROUPID'
        DataType = ftInteger
      end
      item
        Name = 'WEIGHT$DONE'
        DataType = ftBCD
        Precision = 32
        Size = 3
      end>
    IndexDefs = <
      item
        Name = 'PassportItemsIndex'
        Fields = 'Group$ID;Operation$Title;Place$Of$Storage$Title'
      end>
    IndexFieldNames = 'GroupID;Operation$Title;Place$Of$Storage$Title'
    Params = <
      item
        DataType = ftInteger
        Name = 'PASSPORTID'
        ParamType = ptInput
      end>
    ProviderName = 'ProviderPassportItems'
    StoreDefs = True
    BeforeOpen = PassportItemsBeforeOpen
    Left = 384
    Top = 392
    object PassportItemsID: TIntegerField
      FieldName = 'ID'
    end
    object PassportItemsPASSPORTID: TIntegerField
      FieldName = 'PASSPORT$ID'
    end
    object PassportItemsOPERATIONID: TIntegerField
      FieldName = 'OPERATION$ID'
    end
    object PassportItemsOPERATIONTITLE: TStringField
      FieldName = 'OPERATION$TITLE'
      Size = 64
    end
    object PassportItemsPLACEOFSTORAGEID: TIntegerField
      FieldName = 'PLACE$OF$STORAGE$ID'
    end
    object PassportItemsPLACEOFSTORAGETITLE: TStringField
      FieldName = 'PLACE$OF$STORAGE$TITLE'
      Size = 64
    end
    object PassportItemsWEIGHT: TBCDField
      FieldName = 'WEIGHT'
      Precision = 32
      Size = 3
    end
    object PassportItemsWEIGHTIN: TBCDField
      FieldName = 'WEIGHT$IN'
      Precision = 32
      Size = 3
    end
    object PassportItemsWEIGHTNORM: TBCDField
      FieldName = 'WEIGHT$NORM'
      Precision = 32
      Size = 3
    end
    object PassportItemsGROUPID: TIntegerField
      FieldName = 'GROUPID'
    end
  end
  object ProviderPassportItems: TDataSetProvider
    DataSet = DataSetPassportItems
    Left = 416
    Top = 392
  end
  object PassportGroups: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <
      item
        Name = 'GroupsIndex'
        Fields = 'Title'
      end
      item
        Name = 'PassportGroupsIndex2'
        Fields = 'ID'
      end>
    IndexFieldNames = 'Title'
    Params = <>
    StoreDefs = True
    BeforeEdit = PassportGroupsBeforeEdit
    BeforePost = PassportGroupsBeforePost
    OnCalcFields = PassportGroupsCalcFields
    Left = 384
    Top = 360
    object PassportGroupsID: TIntegerField
      FieldName = 'ID'
    end
    object PassportGroupsTitle: TStringField
      FieldName = 'Title'
      Size = 256
    end
    object PassportGroupsWEIGHT: TBCDField
      FieldName = 'WEIGHT'
      Precision = 32
      Size = 3
    end
    object PassportGroupsWEIGHTNORM: TBCDField
      FieldName = 'WEIGHT$NORM'
      Precision = 32
      Size = 3
    end
    object PassportGroupsWEIGHTOUT: TBCDField
      FieldName = 'WEIGHT$OUT'
      Precision = 32
      Size = 3
    end
    object PassportGroupsWEIGHTIN: TBCDField
      FieldName = 'WEIGHT$IN'
      Precision = 32
      Size = 3
    end
    object PassportGroupsWEIGHTREMAINDERFACT: TBCDField
      FieldKind = fkCalculated
      FieldName = 'WEIGHT$REMAINDER$FACT'
      Precision = 32
      Size = 3
      Calculated = True
    end
    object PassportGroupsWEIGHTREMAINDERMinus: TBCDField
      FieldKind = fkCalculated
      FieldName = 'WEIGHT$REMAINDER$Minus'
      Precision = 32
      Size = 3
      Calculated = True
    end
    object PassportGroupsWEIGHTREMAINDERPlus: TBCDField
      FieldKind = fkCalculated
      FieldName = 'WEIGHT$REMAINDER$Plus'
      Precision = 32
      Size = 3
      Calculated = True
    end
    object PassportGroupsWEIGHTRemainderNORM: TBCDField
      FieldKind = fkCalculated
      FieldName = 'WEIGHT$REMAINDER$NORM'
      Precision = 32
      Size = 3
      Calculated = True
    end
    object PassportGroupsWEIGHTREMAINDEROUT: TBCDField
      FieldName = 'WEIGHT$REMAINDER$OUT'
      Precision = 32
      Size = 3
    end
    object PassportGroupsWEIGHTREMAINDERIN: TBCDField
      FieldName = 'WEIGHT$REMAINDER$IN'
      Precision = 32
      Size = 3
    end
    object PassportGroupsWEIGHTREMAINDER: TBCDField
      FieldName = 'WEIGHT$REMAINDER'
      Precision = 32
      Size = 3
    end
    object PassportGroupsOperationID: TIntegerField
      FieldName = 'Operation$ID'
    end
    object PassportGroupsPlaceOfStorageID: TIntegerField
      FieldName = 'Place$Of$Storage$ID'
    end
    object PassportGroupsOperationTitle: TStringField
      FieldKind = fkLookup
      FieldName = 'Operation$Title'
      LookupDataSet = COperations
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'Operation$ID'
      Lookup = True
    end
    object PassportGroupsPlaceOfStorageTitle: TStringField
      FieldKind = fkLookup
      FieldName = 'PlaceOfStorage$Title'
      LookupDataSet = CPlaceOfStorage
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'Place$Of$Storage$ID'
      Lookup = True
    end
    object PassportGroupsCHILDSCOUNT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Childs$Count'
      Calculated = True
    end
    object PassportGroupsDonePercent: TBCDField
      FieldKind = fkCalculated
      FieldName = 'Done$Percent'
      Size = 2
      Calculated = True
    end
  end
  object DataSourcePassportGroups: TDataSource
    DataSet = PassportGroups
    Left = 448
    Top = 360
  end
  object cxStyleRepository: TcxStyleRepository
    Left = 120
    Top = 504
    PixelsPerInch = 96
    object StyleReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnFace
    end
    object StyleVirtual: TcxStyle
      AssignedValues = [svColor]
      Color = clMoneyGreen
    end
  end
  object TmpQuery: TpFIBQuery
    Transaction = Transaction
    Database = dm.db
    Left = 88
    Top = 280
  end
  object Printer: TdxComponentPrinter
    CurrentLink = GridPassportItemsPrinterLink
    PreviewOptions.Caption = #1055#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1099#1081' '#1087#1088#1086#1089#1084#1086#1090#1088
    PreviewOptions.VisibleOptions = [pvoPageSetup, pvoPrint, pvoReportFileOperations]
    PreviewOptions.WindowState = wsMaximized
    PrintTitle = #1055#1077#1095#1072#1090#1100
    Version = 0
    Left = 152
    Top = 504
    object GridPassportItemsPrinterLink: TdxGridReportLink
      Active = True
      Component = GridPassportItems
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 10000
      PrinterPage.Margins.Left = 10000
      PrinterPage.Margins.Right = 10000
      PrinterPage.Margins.Top = 10000
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.Caption = #1055#1072#1089#1087#1086#1088#1090' '#1072#1092#1092#1080#1085#1072#1078#1072
      ReportDocument.CreationDate = 42522.582048460640000000
      ShrinkToPageWidth = True
      OptionsExpanding.ExpandMasterRows = True
      OptionsOnEveryPage.Footers = False
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.ExpandButtons = False
      OptionsView.FilterBar = False
      OptionsView.GroupFooters = False
      BuiltInReportLink = True
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 104
    Top = 208
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
    end
  end
  object dxPSEngineController1: TdxPSEngineController
    DialogsLookAndFeel.Kind = lfOffice11
    PreviewDialogStyle = 'Standard'
    Left = 184
    Top = 504
  end
  object COperations: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'ProviderOperations'
    Left = 96
    Top = 440
    object COperationsID: TIntegerField
      FieldName = 'ID'
    end
    object COperationsTITLE: TStringField
      FieldName = 'TITLE'
      Size = 64
    end
  end
  object CPlaceOfStorage: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'OPERATION$ID'
        ParamType = ptInputOutput
      end>
    ProviderName = 'ProviderPlaceOfStorage'
    Left = 96
    Top = 472
    object CPlaceOfStorageID: TIntegerField
      FieldName = 'ID'
    end
    object CPlaceOfStorageTITLE: TStringField
      FieldName = 'TITLE'
      Size = 64
    end
  end
  object ProviderOperations: TDataSetProvider
    DataSet = DataSetOperations
    Left = 56
    Top = 440
  end
  object ProviderPlaceOfStorage: TDataSetProvider
    DataSet = DataSetPlaceOfStorage
    Left = 56
    Top = 472
  end
  object dxMemData1: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 400
    Top = 248
  end
  object dxMemData2: TdxMemData
    Indexes = <>
    SortOptions = []
    Left = 232
    Top = 544
  end
  object cxPropertiesStore1: TcxPropertiesStore
    Components = <>
    StorageName = 'cxPropertiesStore1'
    Left = 272
    Top = 520
  end
end
