unit DocAncestor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Ancestor, RXDBCtrl, StdCtrls, Mask, DBCtrls, ExtCtrls,
  RxPlacemnt, ImgList, RxSpeedBar, ComCtrls, Grids, db, Menus, DBGridEh, DBCtrlsEh,
  TB2Item, ActnList
  {$ifdef IBX}, IbCustomDataSet, IbSQL {$endif IBX}
  {$ifdef FIBP}, FIBDataSet, pFIBDataSet, pFIBQuery, DBGridEhGrouping,
  GridsEh {$endif FIBP};

type
  TDocAncestorClass = class of TfmDocAncestor;

  TfmDocAncestor = class(TfmAncestor)
    plMain: TPanel;
    lbNoDoc: TLabel;
    lbDateDoc: TLabel;
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;
    spitPrint: TSpeedItem;
    lbDep: TLabel;
    txtDep: TDBText;
    spitClose: TSpeedItem;
    gridItem: TDBGridEh;
    edNoDoc: TDBEditEh;
    dtedDateDoc: TDBDateTimeEditEh;
    ppPrint: TTBPopupMenu;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure spitAddClick(Sender: TObject);
    procedure spitDelClick(Sender: TObject);
    procedure spitCloseClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  protected
    FId : integer;
    FReadOnly : boolean;
    FDocName : string; // ������ ������������� ��� ���� � ������������ �� �����
                       // ����� ���������
    //FAccess : boolean; // ����� �� ������ � ������������ �������
    {$ifdef IBX}
    FMasterDataSet : TIbCustomDataSet;
    FQuery : TIBSQL;
    {$endif IBX}
    {$ifdef FIBP}
    FMasterDataSet : TpFIBDataSet;
    FQuery : TpFIBQuery;
    {$endif FIBP}
    procedure CloseDoc; virtual;
    procedure OpenDoc; virtual;
    procedure InitBtn(T : boolean);
  private
    {$ifdef IBX}
    function GetDataSet: TIbCustomDataSet;
    procedure SetDataSet(const Value: TIbCustomDataSet);
    {$endif IBX}
    {$ifdef FIBP}
    function GetDataSet: TpFIBDataSet;
    procedure SetDataSet(const Value: TpFIBDataSet);
    {$endif FIBP}
  public
    {$ifdef IBX}
    property DataSet : TIbCustomDataSet read GetDataSet write SetDataSet;
    property MasterDataSet : TIbCustomDataSet read FMasterDataSet write FMasterDataSet;
    constructor Create(AOwner : TComponent; MasterDataSet : TIbCustomDataSet; Id : integer; quTmp : TIBSQL);
    {$endif IBX}
    {$ifdef FIBP}
    property DataSet : TpFIBDataSet read GetDataSet write SetDataSet;
    property MasterDataSet : TpFIBDataSet read FMasterDataSet write FMasterDataSet;
    constructor Create(AOwner : TComponent; MasterDataSet : TpFIBDataSet; Id : integer; quTmp : TpFIBQuery);
    {$endif FIBP}
  end;

var
  fmDocAncestor: TfmDocAncestor;

{$ifdef IBX}
function ShowDocForm(FC: TDocAncestorClass; MasterDataSet : TIbCustomDataSet; Id : integer; quTmp : TIBSQL) : word;
{$endif IBX}
{$ifdef FIBP}
function ShowDocForm(FC: TDocAncestorClass; MasterDataSet : TpFIBDataSet; Id : integer; quTmp : TpFIBQuery) : word;
{$endif FIBP}

implementation

uses dbUtil;

(*
  �������� ������� ���������
    1. ��������� DataSource ���� data-���������
    2. ��������� ������ OpenDoc � CloseDoc � ������ ���� ����������
       ���������� ��������� ������� �� �������� � �������� ���������
       �� ��������� ��� �������� ��������� ����������� ��������� CloseDoc
       � ����������� FDocName � FId
       ��� �������� ��������� ����������� ��������� OpenDoc � �����������
       FDocName � FId

*)

{$R *.DFM}
{$ifdef IBX}
function ShowDocForm(FC: TDocAncestorClass; MasterDataSet : TIbCustomDataSet; Id : integer; quTmp : TIBSQL) : word;
{$endif IBX}
{$ifdef FIBP}
function ShowDocForm(FC: TDocAncestorClass; MasterDataSet : TpFIBDataSet; Id : integer; quTmp : TpFIBQuery) : word;
{$endif}
var
  Doc : TfmDocAncestor;
begin
  Screen.Cursor:=crHourGlass;
  try
    Application.ProcessMessages;
    Doc := nil;
    Doc := FC.Create(Application, MasterDataSet, Id, quTmp);
    Screen.Cursor:=crDefault;
    Result := Doc.ShowModal;
  finally
    Screen.Cursor:=crDefault;
    FreeAndNil(Doc);
  end;
end;

{ TfmDocAncestor }
{$ifdef IBX}
function TfmDocAncestor.GetDataSet: TIbCustomDataSet;
begin
  Result := gridItem.DataSource.DataSet as TIbCustomDataSet;
end;

procedure TfmDocAncestor.SetDataSet(const Value: TIbCustomDataSet);
begin
  gridItem.DataSource.DataSet := Value;
end;
{$endif IBX}

{$ifdef FIBP}
function TfmDocAncestor.GetDataSet: TpFIBDataSet;
begin
  Result := gridItem.DataSource.DataSet as TpFIBDataSet;
end;

procedure TfmDocAncestor.SetDataSet(const Value: TpFIBDataSet);
begin
  gridItem.DataSource.DataSet := Value;
end;
{$endif FIBP}

procedure TfmDocAncestor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  //ReOpendatasets([MasterDataSet, DataSet]);
  if MasterDataSet.State in [dsEdit, dsInsert]
    then
      MasterDataSet.Post;
  if DataSet.State in [dsEdit, dsInsert]
    then
      DataSet.Post;
  if DataSet.Transaction.Active then DataSet.Transaction.CommitRetaining;
  CloseDataSets([DataSet]);
  Action := caFree;
end;

procedure TfmDocAncestor.FormCreate(Sender: TObject);
begin
  inherited;
  //��������� ������
  if Assigned(gridItem.DataSource)and Assigned(gridItem.DataSource.Dataset) then
  begin
    if not DataSet.Transaction.Active then DataSet.Transaction.StartTransaction
    else DataSet.Transaction.CommitRetaining;
    ReOpenDataSets([DataSet]);
  end;
end;

procedure TfmDocAncestor.spitAddClick(Sender: TObject);
begin
  DataSet.Append;
  DataSet.Transaction.CommitRetaining;
end;

procedure TfmDocAncestor.spitDelClick(Sender: TObject);
begin
  DataSet.Delete
end;

procedure TfmDocAncestor.spitCloseClick(Sender: TObject);
begin
  PostDataSet(MasterDataSet);
  if spitClose.BtnCaption = '������' then
    try
      CloseDoc;
      spitClose.BtnCaption := '������';
      spitClose.Hint := '������� ��������';
      spitClose.ImageIndex := 5;
      MasterDataSet.Transaction.CommitRetaining;
      RefreshDataSets([MasterDataSet]); // ???
    except
      on E: Exception do
        { TODO : !!! }
        //if not TranslateIbMsg(E, DataSet. Database, DataSet.Transaction) then ShowMessage(E.Message);
    end
  else
    try
      OpenDoc;
      spitClose.BtnCaption := '������';
      spitClose.Hint := '������� ��������';
      spitClose.ImageIndex := 6;
      MasterDataSet.Transaction.CommitRetaining;
      RefreshDataSets([MasterDataSet]); // ???
    finally
    end;
end;

procedure TfmDocAncestor.CloseDoc;
begin
  ExecSQL('execute procedure CloseDoc2('#39+FDocName+#39', '+IntToStr(FId)+')', FQuery);
end;

procedure TfmDocAncestor.OpenDoc;
begin
  ExecSQL('execute procedure OpenDoc2('#39+FDocName+#39', '+IntToStr(FId)+')', FQuery);
end;

procedure TfmDocAncestor.InitBtn(T : boolean);
begin
  if not T then // �������� ������
  begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 6;
  end
  else begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 5;
  end
end;

procedure TfmDocAncestor.FormShow(Sender: TObject);
begin
  inherited;
  PostDataSet(MasterDataSet);
end;

{$ifdef IBX}
constructor TfmDocAncestor.Create(AOwner: TComponent; MasterDataSet: TIbCustomDataSet; Id: integer; quTmp: TIBSQL);
{$endif IBX}
{$ifdef FIBP}
constructor TfmDocAncestor.Create(AOwner: TComponent; MasterDataSet: TpFIBDataSet; Id: integer; quTmp: TpFIBQuery);
{$endif FIBP}
begin
  FReadOnly := False;
  FMasterDataSet := MasterDataSet;
  FQuery := quTmp;
  FId := Id;
  inherited Create(AOwner);
end;

end.
