object dmPrint: TdmPrint
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 880
  Width = 1442
  object frReport: TfrReport
    DefaultCopies = 2
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbExit]
    RebuildPrinter = False
    OnBeginBand = frReportBeginBand
    OnBeforePrint = frReportBeforePrint
    Left = 68
    Top = 8
    ReportForm = {19000000}
  end
  object trPrint: TpFIBTransaction
    DefaultDatabase = dm.db
    TimeoutAction = TACommitRetaining
    Left = 16
    Top = 8
  end
  object quTmp: TpFIBQuery
    Transaction = trPrint
    Database = dm.db
    Left = 208
    Top = 8
  end
  object frDesigner: TfrDesigner
    OnSaveReport = frDesignerSaveReport
    Left = 116
    Top = 8
  end
  object taSelf: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select c.COMPID, c.CODE, c.NAME, c.SNAME, c.POSTINDEX, c.COUNTRY' +
        ','
      
        '    c.CITY, c.ADDRESS,  c.PHONE, c.FAX, c.EMAIL, c.WWW,  c.BOSSF' +
        'IO,'
      '    c.BOSSPHONE, c.BUHFIO, c.BUHPHONE, c.OFIO, c.OFIOPHONE,'
      
        '    c.INN, c.BIK, c.OKPO, c.OKONH, c.BANK, c.BILL, c.KBANK, c.KB' +
        'ILL,  c.ADRBILL    '
      'from D_Comp c'
      'where c.CompId=:SELFCOMPID')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taSelfBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 252
    Top = 8
    object taSelfCOMPID: TIntegerField
      FieldName = 'COMPID'
    end
    object taSelfCODE: TFIBStringField
      FieldName = 'CODE'
      Size = 10
      EmptyStrToNull = True
    end
    object taSelfNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
    object taSelfPOSTINDEX: TFIBStringField
      FieldName = 'POSTINDEX'
      EmptyStrToNull = True
    end
    object taSelfCOUNTRY: TFIBStringField
      FieldName = 'COUNTRY'
      EmptyStrToNull = True
    end
    object taSelfCITY: TFIBStringField
      FieldName = 'CITY'
      Size = 30
      EmptyStrToNull = True
    end
    object taSelfADDRESS: TFIBStringField
      FieldName = 'ADDRESS'
      Size = 80
      EmptyStrToNull = True
    end
    object taSelfPHONE: TFIBStringField
      FieldName = 'PHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object taSelfFAX: TFIBStringField
      FieldName = 'FAX'
      Size = 30
      EmptyStrToNull = True
    end
    object taSelfEMAIL: TFIBStringField
      FieldName = 'EMAIL'
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfWWW: TFIBStringField
      FieldName = 'WWW'
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfBOSSFIO: TFIBStringField
      FieldName = 'BOSSFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfBOSSPHONE: TFIBStringField
      FieldName = 'BOSSPHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object taSelfBUHFIO: TFIBStringField
      FieldName = 'BUHFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfBUHPHONE: TFIBStringField
      FieldName = 'BUHPHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object taSelfOFIO: TFIBStringField
      FieldName = 'OFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfOFIOPHONE: TFIBStringField
      FieldName = 'OFIOPHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object taSelfINN: TFIBStringField
      FieldName = 'INN'
      Size = 30
      EmptyStrToNull = True
    end
    object taSelfBIK: TFIBStringField
      FieldName = 'BIK'
      Size = 10
      EmptyStrToNull = True
    end
    object taSelfOKPO: TFIBStringField
      FieldName = 'OKPO'
      EmptyStrToNull = True
    end
    object taSelfOKONH: TFIBStringField
      FieldName = 'OKONH'
      EmptyStrToNull = True
    end
    object taSelfBANK: TFIBStringField
      FieldName = 'BANK'
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfBILL: TFIBStringField
      FieldName = 'BILL'
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfKBANK: TFIBStringField
      FieldName = 'KBANK'
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfKBILL: TFIBStringField
      FieldName = 'KBILL'
      Size = 60
      EmptyStrToNull = True
    end
    object taSelfADRBILL: TFIBStringField
      FieldName = 'ADRBILL'
      Size = 80
      EmptyStrToNull = True
    end
  end
  object quInv16: TpFIBDataSet
    SelectSQL.Strings = (
      'select INVID Link, MOLID, DOCNO, DOCDATE, JOBFACE, DEPNAME,'
      '   WNO, Operation'
      'from Doc_inv16(:DOCID)         '
      '           ')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 16
    Top = 56
    object quInv16LINK: TIntegerField
      FieldName = 'LINK'
      Required = True
    end
    object quInv16MOLID: TFIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'MOLID'
      ReadOnly = True
      Size = 60
      EmptyStrToNull = True
    end
    object quInv16DOCNO: TIntegerField
      FieldName = 'DOCNO'
    end
    object quInv16DOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object quInv16JOBFACE: TFIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'JOBFACE'
      ReadOnly = True
      Size = 60
      EmptyStrToNull = True
    end
    object quInv16DEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object quInv16WNO: TIntegerField
      FieldName = 'WNO'
    end
    object quInv16OPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsrInv16: TDataSource
    DataSet = quInv16
    Left = 16
    Top = 104
  end
  object frdsrInv16: TfrDBDataSet
    DataSet = quInv16
    Left = 16
    Top = 150
  end
  object quInv16Item: TpFIBDataSet
    SelectSQL.Strings = (
      'select w.WOItemId,  w.ITDATE, w.WORDERID, w.OperId,'
      
        '       w.UQ, w.UW, s.Semis, w.Q, w.W, o.Operation, w.AINV, w.APP' +
        'LINV,'
      '       w.ITTYPE,'
      '       case w.ITTYPE'
      '         when 0 then '#39#1042#39
      '         when 1 then '#39#1053#39
      '         when 3 then '#39#1042#1074#39
      '         when 5 then '#39#1041#39
      '         when 6 then '#39#1044#39
      '         when 7 then '#39#1053#1085#39' '
      '       end ITTYPENAME   '
      
        'from WOItem w left join D_oper o on (w.OperId=o.OperId), D_Semis' +
        ' s  '
      'where w.InvId=:Link and'
      '           w.Semis=s.SemisId'
      '           '
      '           ')
    CacheModelOptions.BufferChunks = 1000
    AfterScroll = quInv16ItemAfterScroll
    BeforeOpen = quInv16ItemBeforeOpen
    OnCalcFields = quInv16ItemCalcFields
    Transaction = trPrint
    Database = dm.db
    DataSource = dsrInv16
    Left = 76
    Top = 56
    dcForceOpen = True
    oFetchAll = True
    object quInv16ItemRecNo: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object quInv16ItemWOITEMID: TIntegerField
      FieldName = 'WOITEMID'
      Required = True
    end
    object quInv16ItemITDATE: TDateTimeField
      FieldName = 'ITDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object quInv16ItemWORDERID: TIntegerField
      FieldName = 'WORDERID'
      Required = True
    end
    object quInv16ItemOPERID: TFIBStringField
      FieldName = 'OPERID'
      Required = True
      Size = 10
      EmptyStrToNull = True
    end
    object quInv16ItemUQ: TSmallintField
      FieldName = 'UQ'
    end
    object quInv16ItemUW: TSmallintField
      FieldName = 'UW'
    end
    object quInv16ItemSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Size = 40
      EmptyStrToNull = True
    end
    object quInv16ItemQ: TFloatField
      DefaultExpression = '0'
      FieldName = 'Q'
    end
    object quInv16ItemW: TFloatField
      DefaultExpression = '0'
      FieldName = 'W'
    end
    object quInv16ItemOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Origin = 'D_OPER.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object quInv16ItemAINV: TIntegerField
      FieldName = 'AINV'
      Origin = 'WOITEM.AINV'
    end
    object quInv16ItemUQNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'UQNAME'
      Size = 10
      Calculated = True
    end
    object quInv16ItemUWNAME: TStringField
      FieldKind = fkCalculated
      FieldName = 'UWNAME'
      Size = 10
      Calculated = True
    end
    object quInv16ItemAPPLINV: TIntegerField
      FieldName = 'APPLINV'
      Origin = 'WOITEM.APPLINV'
    end
    object quInv16ItemITTYPE: TFIBSmallIntField
      FieldName = 'ITTYPE'
    end
    object quInv16ItemITTYPENAME: TFIBStringField
      FieldName = 'ITTYPENAME'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object frdsrInv16Item: TfrDBDataSet
    DataSet = quInv16Item
    Left = 76
    Top = 104
  end
  object taDoc: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Doc set'
      '  Doc=:Doc,'
      '  Name=:Name'
      'where Id=:OLD_Id')
    InsertSQL.Strings = (
      'insert into Doc(Id, Doc, Name)'
      'values (:Id, :Doc, :Name)')
    SelectSQL.Strings = (
      'select Id, Doc, Name '
      'from Doc')
    CacheModelOptions.BufferChunks = 1000
    OnNewRecord = taDocNewRecord
    Transaction = trPrint
    Database = dm.db
    Left = 164
    Top = 8
    object taDocID: TIntegerField
      FieldName = 'ID'
      Origin = 'DOC.ID'
      Required = True
    end
    object taDocDOC: TMemoField
      FieldName = 'DOC'
      Origin = 'DOC.DOC'
      BlobType = ftMemo
      Size = 8
    end
    object taDocNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'DOC.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object taOper: TpFIBDataSet
    SelectSQL.Strings = (
      'select o.OperId, o.Operation, o.Rate, t.Name Tails'
      'from D_Oper o, D_Tails t'
      'where o.Tails=t.Id'
      'order by Operation')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 352
    Top = 8
    object taOperOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'D_OPER.OPERID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taOperOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Origin = 'D_OPER.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taOperRATE: TFloatField
      FieldName = 'RATE'
      Origin = 'D_OPER.RATE'
    end
    object taOperTAILS: TFIBStringField
      FieldName = 'TAILS'
      Origin = 'D_TAILS.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object frOper: TfrDBDataSet
    DataSet = taOper
    Left = 352
    Top = 104
  end
  object dsrOper: TDataSource
    DataSet = taOper
    Left = 352
    Top = 56
  end
  object taMOL: TpFIBDataSet
    SelectSQL.Strings = (
      'select MOLID, FNAME, LNAME, MNAME, FIO, SHEETNO'
      'from D_Mol'
      'where IsWork=1'
      'order by FIO')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 400
    Top = 8
    object taMOLMOLID: TIntegerField
      FieldName = 'MOLID'
      Origin = 'D_MOL.MOLID'
      Required = True
    end
    object taMOLFNAME: TFIBStringField
      FieldName = 'FNAME'
      Origin = 'D_MOL.FNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taMOLLNAME: TFIBStringField
      FieldName = 'LNAME'
      Origin = 'D_MOL.LNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taMOLMNAME: TFIBStringField
      FieldName = 'MNAME'
      Origin = 'D_MOL.MNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taMOLFIO: TFIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'FIO'
      Origin = 'D_MOL.FIO'
      ReadOnly = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taMOLSHEETNO: TSmallintField
      FieldName = 'SHEETNO'
      Origin = 'D_MOL.SHEETNO'
    end
  end
  object frMOL: TfrDBDataSet
    DataSet = taMOL
    Left = 400
    Top = 104
  end
  object dsrMol: TDataSource
    DataSet = taMOL
    Left = 400
    Top = 56
  end
  object taCalcF: TpFIBDataSet
    SelectSQL.Strings = (
      'select TAILS, F, FTOTAL, NTOTAL, GET, GET_Q, DONE,'
      
        '    DONE_Q, RET, RET_Q, REJ, REJ_Q, REWORK, REWORK_Q, R, OUT, GE' +
        'T_GR, DONE_GR,'
      '    RET_GR, REWORK_GR, REJ_GR, DESCR_GR,'
      '    GET_E, DONE_E, RET_E, REJ_E, REWORK_E, R_E, INPUT, WZP'
      'from S_CalcF(:BD, :ED, :MATID1, :MATID2)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taCalcFBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 640
    Top = 160
    object taCalcFTAILS: TFIBStringField
      FieldName = 'TAILS'
      Origin = 'S_CALCF.TAILS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taCalcFF: TFloatField
      FieldName = 'F'
      Origin = 'S_CALCF.F'
    end
    object taCalcFFTOTAL: TFloatField
      FieldName = 'FTOTAL'
      Origin = 'S_CALCF.FTOTAL'
    end
    object taCalcFNTOTAL: TFloatField
      FieldName = 'NTOTAL'
      Origin = 'S_CALCF.NTOTAL'
    end
    object taCalcFGET: TFloatField
      FieldName = 'GET'
      Origin = 'S_CALCF.GET'
    end
    object taCalcFGET_Q: TFloatField
      FieldName = 'GET_Q'
      Origin = 'S_CALCF.GET_Q'
    end
    object taCalcFDONE: TFloatField
      FieldName = 'DONE'
      Origin = 'S_CALCF.DONE'
    end
    object taCalcFDONE_Q: TFloatField
      FieldName = 'DONE_Q'
      Origin = 'S_CALCF.DONE_Q'
    end
    object taCalcFRET: TFloatField
      FieldName = 'RET'
      Origin = 'S_CALCF.RET'
    end
    object taCalcFRET_Q: TFloatField
      FieldName = 'RET_Q'
      Origin = 'S_CALCF.RET_Q'
    end
    object taCalcFREJ: TFloatField
      FieldName = 'REJ'
      Origin = 'S_CALCF.REJ'
    end
    object taCalcFREJ_Q: TFloatField
      FieldName = 'REJ_Q'
      Origin = 'S_CALCF.REJ_Q'
    end
    object taCalcFREWORK: TFloatField
      FieldName = 'REWORK'
      Origin = 'S_CALCF.REWORK'
    end
    object taCalcFREWORK_Q: TFloatField
      FieldName = 'REWORK_Q'
      Origin = 'S_CALCF.REWORK_Q'
    end
    object taCalcFR: TFloatField
      FieldName = 'R'
      Origin = 'S_CALCF.R'
    end
    object taCalcFOUT: TFloatField
      FieldName = 'OUT'
      Origin = 'S_CALCF.OUT'
    end
    object taCalcFGET_GR: TFloatField
      FieldName = 'GET_GR'
      Origin = 'S_CALCF.GET_GR'
    end
    object taCalcFDONE_GR: TFloatField
      FieldName = 'DONE_GR'
      Origin = 'S_CALCF.DONE_GR'
    end
    object taCalcFRET_GR: TFloatField
      FieldName = 'RET_GR'
      Origin = 'S_CALCF.RET_GR'
    end
    object taCalcFREWORK_GR: TFloatField
      FieldName = 'REWORK_GR'
      Origin = 'S_CALCF.REWORK_GR'
    end
    object taCalcFREJ_GR: TFloatField
      FieldName = 'REJ_GR'
      Origin = 'S_CALCF.REJ_GR'
    end
    object taCalcFDESCR_GR: TFIBStringField
      FieldName = 'DESCR_GR'
      Origin = 'S_CALCF.DESCR_GR'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taCalcFGET_E: TFloatField
      FieldName = 'GET_E'
      Origin = 'S_CALCF.GET_E'
    end
    object taCalcFDONE_E: TFloatField
      FieldName = 'DONE_E'
      Origin = 'S_CALCF.DONE_E'
    end
    object taCalcFRET_E: TFloatField
      FieldName = 'RET_E'
      Origin = 'S_CALCF.RET_E'
    end
    object taCalcFREJ_E: TFloatField
      FieldName = 'REJ_E'
      Origin = 'S_CALCF.REJ_E'
    end
    object taCalcFREWORK_E: TFloatField
      FieldName = 'REWORK_E'
      Origin = 'S_CALCF.REWORK_E'
    end
    object taCalcFR_E: TFloatField
      FieldName = 'R_E'
      Origin = 'S_CALCF.R_E'
    end
    object taCalcFINPUT: TFloatField
      FieldName = 'INPUT'
      Origin = 'S_CALCF.INPUT'
    end
    object taCalcFWZP: TFloatField
      FieldName = 'WZP'
      Origin = 'S_CALCF.WZP'
    end
  end
  object frCalcF: TfrDBDataSet
    DataSet = taCalcF
    Left = 640
    Top = 204
  end
  object taProtocol: TpFIBDataSet
    SelectSQL.Strings = (
      'select a.ID, a.DocNo, a.DocDate, a.BD'
      'from Act a'
      'where a.ID=:ID')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 16
    Top = 204
    object taProtocolID: TIntegerField
      FieldName = 'ID'
      Origin = 'ACT.ID'
      Required = True
    end
    object taProtocolDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'ACT.DOCNO'
    end
    object taProtocolDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'ACT.DOCDATE'
    end
    object taProtocolBD: TDateTimeField
      FieldName = 'BD'
      Origin = 'ACT.BD'
    end
  end
  object taPItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, NAME, F, N, E, S, I, H, WZ, OUT'
      'from Doc_Protocol(:ACTID, :T)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 72
    Top = 204
    object taPItemID: TIntegerField
      FieldName = 'ID'
      Origin = 'AITEM.ID'
      Required = True
    end
    object taPItemF: TFloatField
      FieldName = 'F'
      Origin = 'AITEM.F'
    end
    object taPItemN: TFloatField
      FieldName = 'N'
      Origin = 'AITEM.N'
    end
    object taPItemE: TFloatField
      FieldName = 'E'
      Origin = 'AITEM.E'
    end
    object taPItemS: TFloatField
      FieldName = 'S'
      Origin = 'AITEM.S'
    end
    object taPItemI: TFloatField
      FieldName = 'I'
      Origin = 'AITEM.I'
    end
    object taPItemH: TFloatField
      FieldName = 'H'
      Origin = 'AITEM.H'
    end
    object taPItemWZ: TFloatField
      FieldName = 'WZ'
      Origin = 'AITEM.WZ'
    end
    object taPItemOUT: TFloatField
      FieldName = 'OUT'
      Origin = 'AITEM.OUT'
    end
    object taPItemNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'DOC_PROTOCOL.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsrProtocol: TDataSource
    DataSet = taProtocol
    Left = 16
    Top = 252
  end
  object frProtocol: TfrDBDataSet
    DataSet = taProtocol
    Left = 8
    Top = 304
  end
  object frPItem: TfrDBDataSet
    DataSet = taPItem
    Left = 72
    Top = 252
  end
  object taMol_T: TpFIBDataSet
    SelectSQL.Strings = (
      'select TAILS, F, FTOTAL, NTOTAL, GET, GET_Q,'
      '    DONE, DONE_Q, RET, RET_Q, REJ, REJ_Q,'
      '    REWORK, REWORK_Q, R, OUT, GET_GR, DONE_GR,'
      '    RET_GR, REWORK_GR, REJ_GR, DESCR_GR'
      'from S_CalcF_MOL(:BD, :ED, :GRMAT1, :GRMAT2, :MOLID)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taMol_TBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 900
    Top = 236
    object taMol_TTAILS: TFIBStringField
      FieldName = 'TAILS'
      Origin = 'S_CALCF_MOL.TAILS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taMol_TF: TFloatField
      FieldName = 'F'
      Origin = 'S_CALCF_MOL.F'
    end
    object taMol_TFTOTAL: TFloatField
      FieldName = 'FTOTAL'
      Origin = 'S_CALCF_MOL.FTOTAL'
    end
    object taMol_TNTOTAL: TFloatField
      FieldName = 'NTOTAL'
      Origin = 'S_CALCF_MOL.NTOTAL'
    end
    object taMol_TGET: TFloatField
      FieldName = 'GET'
      Origin = 'S_CALCF_MOL.GET'
    end
    object taMol_TGET_Q: TFloatField
      FieldName = 'GET_Q'
      Origin = 'S_CALCF_MOL.GET_Q'
    end
    object taMol_TDONE: TFloatField
      FieldName = 'DONE'
      Origin = 'S_CALCF_MOL.DONE'
    end
    object taMol_TDONE_Q: TFloatField
      FieldName = 'DONE_Q'
      Origin = 'S_CALCF_MOL.DONE_Q'
    end
    object taMol_TRET: TFloatField
      FieldName = 'RET'
      Origin = 'S_CALCF_MOL.RET'
    end
    object taMol_TRET_Q: TFloatField
      FieldName = 'RET_Q'
      Origin = 'S_CALCF_MOL.RET_Q'
    end
    object taMol_TREJ: TFloatField
      FieldName = 'REJ'
      Origin = 'S_CALCF_MOL.REJ'
    end
    object taMol_TREJ_Q: TFloatField
      FieldName = 'REJ_Q'
      Origin = 'S_CALCF_MOL.REJ_Q'
    end
    object taMol_TREWORK: TFloatField
      FieldName = 'REWORK'
      Origin = 'S_CALCF_MOL.REWORK'
    end
    object taMol_TREWORK_Q: TFloatField
      FieldName = 'REWORK_Q'
      Origin = 'S_CALCF_MOL.REWORK_Q'
    end
    object taMol_TR: TFloatField
      FieldName = 'R'
      Origin = 'S_CALCF_MOL.R'
    end
    object taMol_TOUT: TFloatField
      FieldName = 'OUT'
      Origin = 'S_CALCF_MOL.OUT'
    end
    object taMol_TGET_GR: TFloatField
      FieldName = 'GET_GR'
      Origin = 'S_CALCF_MOL.GET_GR'
    end
    object taMol_TDONE_GR: TFloatField
      FieldName = 'DONE_GR'
      Origin = 'S_CALCF_MOL.DONE_GR'
    end
    object taMol_TRET_GR: TFloatField
      FieldName = 'RET_GR'
      Origin = 'S_CALCF_MOL.RET_GR'
    end
    object taMol_TREWORK_GR: TFloatField
      FieldName = 'REWORK_GR'
      Origin = 'S_CALCF_MOL.REWORK_GR'
    end
    object taMol_TREJ_GR: TFloatField
      FieldName = 'REJ_GR'
      Origin = 'S_CALCF_MOL.REJ_GR'
    end
    object taMol_TDESCR_GR: TFIBStringField
      FieldName = 'DESCR_GR'
      Origin = 'S_CALCF_MOL.DESCR_GR'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
  end
  object frMol_T: TfrDBDataSet
    DataSet = taMol_T
    Left = 900
    Top = 280
  end
  object taActE: TpFIBDataSet
    SelectSQL.Strings = (
      'select SEMISID, SEMIS, Q, W, Q_AFTER, W_AFTER, NAME'
      'from Doc_Act(:BD, :ED, :DEPID)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    DataSource = dsrPS
    Left = 120
    Top = 204
    object taActESEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'DOC_ACT.SEMISID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taActESEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Origin = 'DOC_ACT.SEMIS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taActEQ: TFloatField
      FieldName = 'Q'
      Origin = 'DOC_ACT.Q'
    end
    object taActEW: TFloatField
      FieldName = 'W'
      Origin = 'DOC_ACT.W'
    end
    object taActEQ_AFTER: TFloatField
      FieldName = 'Q_AFTER'
      Origin = 'DOC_ACT.Q_AFTER'
    end
    object taActEW_AFTER: TFloatField
      FieldName = 'W_AFTER'
      Origin = 'DOC_ACT.W_AFTER'
    end
    object taActENAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'DOC_ACT.NAME'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
  end
  object frActE: TfrDBDataSet
    DataSet = taActE
    Left = 120
    Top = 252
  end
  object taAppl: TpFIBDataSet
    SelectSQL.Strings = (
      'select FIO, H, I, S, BD, DOCDATE, WZ'
      'from Doc_Ob(:ACTID)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 812
    Top = 544
    object taApplH: TFloatField
      FieldName = 'H'
      Origin = 'AITEM.H'
    end
    object taApplFIO: TFIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'FIO'
      Origin = 'D_MOL.FIO'
      ReadOnly = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taApplI: TFloatField
      FieldName = 'I'
      Origin = 'AITEM.I'
    end
    object taApplS: TFloatField
      FieldName = 'S'
      Origin = 'AITEM.S'
    end
    object taApplBD: TDateTimeField
      FieldName = 'BD'
      Origin = 'ACT.BD'
    end
    object taApplDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'ACT.DOCDATE'
    end
    object taApplWZ: TFloatField
      FieldName = 'WZ'
      Origin = 'AITEM.WZ'
    end
  end
  object frAppl: TfrDBDataSet
    DataSet = taAppl
    Left = 812
    Top = 592
  end
  object taZP: TpFIBDataSet
    SelectSQL.Strings = (
      'select OPERNAME, FIO, ZP, U, NP, NR, DONE, E, S,'
      '  TAILS, BD, ED, ZPTOTAL'
      'from Doc_Zp(:ID)'
      'order by FIO, OperName')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 864
    Top = 544
    object taZPOPERNAME: TFIBStringField
      FieldName = 'OPERNAME'
      Origin = 'DOC_ZP.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taZPFIO: TFIBStringField
      FieldName = 'FIO'
      Origin = 'DOC_ZP.FIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taZPZP: TFloatField
      FieldName = 'ZP'
      Origin = 'DOC_ZP.ZP'
    end
    object taZPU: TFloatField
      FieldName = 'U'
      Origin = 'DOC_ZP.U'
    end
    object taZPNP: TFloatField
      FieldName = 'NP'
      Origin = 'DOC_ZP.NP'
    end
    object taZPNR: TFloatField
      FieldName = 'NR'
      Origin = 'DOC_ZP.NR'
    end
    object taZPDONE: TFloatField
      FieldName = 'DONE'
      Origin = 'DOC_ZP.DONE'
    end
    object taZPE: TFloatField
      FieldName = 'E'
      Origin = 'DOC_ZP.E'
    end
    object taZPS: TFloatField
      FieldName = 'S'
      Origin = 'DOC_ZP.S'
    end
    object taZPBD: TDateTimeField
      FieldName = 'BD'
      Origin = 'DOC_ZP.BD'
    end
    object taZPED: TDateTimeField
      FieldName = 'ED'
      Origin = 'DOC_ZP.ED'
    end
    object taZPZPTOTAL: TFloatField
      FieldName = 'ZPTOTAL'
      Origin = 'DOC_ZP.ZPTOTAL'
    end
    object taZPTAILS: TFIBStringField
      FieldName = 'TAILS'
      Origin = 'DOC_ZP.TAILS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object frZP: TfrDBDataSet
    DataSet = taZP
    Left = 864
    Top = 592
  end
  object taZPTotal: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select sum(it.ZP + (it.NR*it.ZP)/100 + NP - U+NR2) ZP, d.NAME, z' +
        '.BD, z.DocDate'
      'from ZItem it, D_Dep d, Zp z'
      'where it.ZpId=:ID and'
      '          z.Id=it.ZpId and'
      '          it.PsId=d.DepId '
      'group by d.Name, z.BD, z.DocDate')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 908
    Top = 544
    object taZPTotalZP: TFloatField
      FieldName = 'ZP'
    end
    object taZPTotalBD: TDateTimeField
      FieldName = 'BD'
    end
    object taZPTotalDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
    end
    object taZPTotalNAME: TFIBStringField
      FieldName = 'NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object frZPTotal: TfrDBDataSet
    DataSet = taZPTotal
    Left = 908
    Top = 592
  end
  object taJ_MatPS: TpFIBDataSet
    SelectSQL.Strings = (
      'select ONO, ODATE, INO, IDATE, ITTYPE, SEMIS, ITDATE,'
      '    Q, W, E'
      'from DOC_JPS(:DEPID, :OPERID, :MATID1, :MATID2, :BD, :ED)'
      'order by IDATE')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taJ_MatPSBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 452
    Top = 152
    object taJ_MatPSONO: TIntegerField
      FieldName = 'ONO'
      Origin = 'DOC_JPS.ONO'
    end
    object taJ_MatPSODATE: TDateTimeField
      FieldName = 'ODATE'
      Origin = 'DOC_JPS.ODATE'
    end
    object taJ_MatPSINO: TIntegerField
      FieldName = 'INO'
      Origin = 'DOC_JPS.INO'
    end
    object taJ_MatPSIDATE: TDateTimeField
      FieldName = 'IDATE'
      Origin = 'DOC_JPS.IDATE'
    end
    object taJ_MatPSITTYPE: TSmallintField
      FieldName = 'ITTYPE'
      Origin = 'DOC_JPS.ITTYPE'
    end
    object taJ_MatPSSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Origin = 'DOC_JPS.SEMIS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taJ_MatPSITDATE: TDateTimeField
      FieldName = 'ITDATE'
      Origin = 'DOC_JPS.ITDATE'
    end
    object taJ_MatPSQ: TFloatField
      FieldName = 'Q'
      Origin = 'DOC_JPS.Q'
    end
    object taJ_MatPSW: TFloatField
      FieldName = 'W'
      Origin = 'DOC_JPS.W'
    end
    object taJ_MatPSE: TSmallintField
      FieldName = 'E'
      Origin = 'DOC_JPS.E'
    end
  end
  object taPS: TpFIBDataSet
    SelectSQL.Strings = (
      'select DEPID, SNAME, NAME, SORTIND'
      'from D_DEP'
      'where PS=1'
      'order by SortInd')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 448
    Top = 8
    object taPSDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'D_DEP.DEPID'
      Required = True
    end
    object taPSSNAME: TFIBStringField
      FieldName = 'SNAME'
      Origin = 'D_DEP.SNAME'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object taPSNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taPSSORTIND: TIntegerField
      FieldName = 'SORTIND'
      Origin = 'D_DEP.SORTIND'
    end
  end
  object dsrPS: TDataSource
    DataSet = taPS
    Left = 448
    Top = 56
  end
  object frPS: TfrDBDataSet
    DataSet = taPS
    Left = 448
    Top = 104
  end
  object frJ_MatPS: TfrDBDataSet
    DataSet = taJ_MatPS
    Left = 452
    Top = 205
  end
  object taCalcF_PS: TpFIBDataSet
    SelectSQL.Strings = (
      'select TAILS, F, FTOTAL, NTOTAL, GET, GET_Q,'
      '    DONE, DONE_Q, RET, RET_Q, REJ, REJ_Q,'
      '    REWORK, REWORK_Q, R, OUT, GET_GR, DONE_GR,'
      '    RET_GR, REWORK_GR, REJ_GR, DESCR_GR,'
      '    GET_E, DONE_E, RET_E, REJ_E, REWORK_E, R_E, INPUT,'
      '    WZP'
      'from S_CalcF_PS(:BD, :ED, :MATID1, :MATID2, :PSID)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taCalcF_PSBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 692
    Top = 160
    object taCalcF_PSTAILS: TFIBStringField
      FieldName = 'TAILS'
      Origin = 'S_CALCF_MOL.TAILS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taCalcF_PSF: TFloatField
      FieldName = 'F'
      Origin = 'S_CALCF_MOL.F'
    end
    object taCalcF_PSFTOTAL: TFloatField
      FieldName = 'FTOTAL'
      Origin = 'S_CALCF_MOL.FTOTAL'
    end
    object taCalcF_PSNTOTAL: TFloatField
      FieldName = 'NTOTAL'
      Origin = 'S_CALCF_MOL.NTOTAL'
    end
    object taCalcF_PSGET: TFloatField
      FieldName = 'GET'
      Origin = 'S_CALCF_MOL.GET'
    end
    object taCalcF_PSGET_Q: TFloatField
      FieldName = 'GET_Q'
      Origin = 'S_CALCF_MOL.GET_Q'
    end
    object taCalcF_PSDONE: TFloatField
      FieldName = 'DONE'
      Origin = 'S_CALCF_MOL.DONE'
    end
    object taCalcF_PSDONE_Q: TFloatField
      FieldName = 'DONE_Q'
      Origin = 'S_CALCF_MOL.DONE_Q'
    end
    object taCalcF_PSRET: TFloatField
      FieldName = 'RET'
      Origin = 'S_CALCF_MOL.RET'
    end
    object taCalcF_PSRET_Q: TFloatField
      FieldName = 'RET_Q'
      Origin = 'S_CALCF_MOL.RET_Q'
    end
    object taCalcF_PSREJ: TFloatField
      FieldName = 'REJ'
      Origin = 'S_CALCF_MOL.REJ'
    end
    object taCalcF_PSREJ_Q: TFloatField
      FieldName = 'REJ_Q'
      Origin = 'S_CALCF_MOL.REJ_Q'
    end
    object taCalcF_PSREWORK: TFloatField
      FieldName = 'REWORK'
      Origin = 'S_CALCF_MOL.REWORK'
    end
    object taCalcF_PSREWORK_Q: TFloatField
      FieldName = 'REWORK_Q'
      Origin = 'S_CALCF_MOL.REWORK_Q'
    end
    object taCalcF_PSR: TFloatField
      FieldName = 'R'
      Origin = 'S_CALCF_MOL.R'
    end
    object taCalcF_PSOUT: TFloatField
      FieldName = 'OUT'
      Origin = 'S_CALCF_MOL.OUT'
    end
    object taCalcF_PSGET_GR: TFloatField
      FieldName = 'GET_GR'
      Origin = 'S_CALCF_MOL.GET_GR'
    end
    object taCalcF_PSDONE_GR: TFloatField
      FieldName = 'DONE_GR'
      Origin = 'S_CALCF_MOL.DONE_GR'
    end
    object taCalcF_PSRET_GR: TFloatField
      FieldName = 'RET_GR'
      Origin = 'S_CALCF_MOL.RET_GR'
    end
    object taCalcF_PSREWORK_GR: TFloatField
      FieldName = 'REWORK_GR'
      Origin = 'S_CALCF_MOL.REWORK_GR'
    end
    object taCalcF_PSREJ_GR: TFloatField
      FieldName = 'REJ_GR'
      Origin = 'S_CALCF_MOL.REJ_GR'
    end
    object taCalcF_PSDESCR_GR: TFIBStringField
      FieldName = 'DESCR_GR'
      Origin = 'S_CALCF_MOL.DESCR_GR'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taCalcF_PSGET_E: TFloatField
      FieldName = 'GET_E'
      Origin = 'S_CALCF_MOL.GET_E'
    end
    object taCalcF_PSDONE_E: TFloatField
      FieldName = 'DONE_E'
      Origin = 'S_CALCF_MOL.DONE_E'
    end
    object taCalcF_PSRET_E: TFloatField
      FieldName = 'RET_E'
      Origin = 'S_CALCF_MOL.RET_E'
    end
    object taCalcF_PSREJ_E: TFloatField
      FieldName = 'REJ_E'
      Origin = 'S_CALCF_MOL.REJ_E'
    end
    object taCalcF_PSREWORK_E: TFloatField
      FieldName = 'REWORK_E'
      Origin = 'S_CALCF_MOL.REWORK_E'
    end
    object taCalcF_PSR_E: TFloatField
      FieldName = 'R_E'
      Origin = 'S_CALCF_MOL.R_E'
    end
    object taCalcF_PSINPUT: TFloatField
      FieldName = 'INPUT'
      Origin = 'S_CALCF_PS.INPUT'
    end
    object taCalcF_PSWZP: TFloatField
      FieldName = 'WZP'
      Origin = 'S_CALCF_PS.WZP'
    end
  end
  object frCalcF_PS: TfrDBDataSet
    DataSet = taCalcF_PS
    Left = 692
    Top = 204
  end
  object taJ_MatPS1: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, SEMIS, GET, REWORK, DONE, RET, REJ, OUT,'
      '  GET_Q, REWORK_Q, DONE_Q, RET_Q, REJ_Q, OUT_Q'
      
        'from DOC_JPS1(:PSID, :OPERID1, :OPERID2, :MATID1, :MATID2,   :BD' +
        ', :ED)'
      'order by SEMIS')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taJ_MatPS1BeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 495
    Top = 152
  end
  object frJ_MatPS1: TfrDBDataSet
    DataSet = taJ_MatPS1
    Left = 511
    Top = 204
  end
  object taCalcF_PSOper: TpFIBDataSet
    SelectSQL.Strings = (
      'select NTOTAL, GET, GET_Q, DONE,'
      '    DONE_Q, RET, RET_Q, REJ, REJ_Q,'
      '    REWORK, REWORK_Q, R, OUT, Z, GET_GR, DONE_GR,'
      '    RET_GR, REJ_GR, REWORK_GR, DESCR_GR,'
      '    GET_E, DONE_E, RET_E, REJ_E, REWORK_E, R_E,'
      '    INPUT, WZP'
      
        'from S_CalcF_PSOper(:BD, :ED, :MATID1, :MATID2, :PSID, :OPERID, ' +
        '1)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taCalcF_PSOperBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 776
    Top = 128
    object taCalcF_PSOperNTOTAL: TFloatField
      FieldName = 'NTOTAL'
      Origin = 'S_CALCF_PSOPER.NTOTAL'
    end
    object taCalcF_PSOperGET: TFloatField
      FieldName = 'GET'
      Origin = 'S_CALCF_PSOPER.GET'
    end
    object taCalcF_PSOperGET_Q: TFloatField
      FieldName = 'GET_Q'
      Origin = 'S_CALCF_PSOPER.GET_Q'
    end
    object taCalcF_PSOperDONE: TFloatField
      FieldName = 'DONE'
      Origin = 'S_CALCF_PSOPER.DONE'
    end
    object taCalcF_PSOperDONE_Q: TFloatField
      FieldName = 'DONE_Q'
      Origin = 'S_CALCF_PSOPER.DONE_Q'
    end
    object taCalcF_PSOperRET: TFloatField
      FieldName = 'RET'
      Origin = 'S_CALCF_PSOPER.RET'
    end
    object taCalcF_PSOperRET_Q: TFloatField
      FieldName = 'RET_Q'
      Origin = 'S_CALCF_PSOPER.RET_Q'
    end
    object taCalcF_PSOperREJ: TFloatField
      FieldName = 'REJ'
      Origin = 'S_CALCF_PSOPER.REJ'
    end
    object taCalcF_PSOperREJ_Q: TFloatField
      FieldName = 'REJ_Q'
      Origin = 'S_CALCF_PSOPER.REJ_Q'
    end
    object taCalcF_PSOperREWORK: TFloatField
      FieldName = 'REWORK'
      Origin = 'S_CALCF_PSOPER.REWORK'
    end
    object taCalcF_PSOperREWORK_Q: TFloatField
      FieldName = 'REWORK_Q'
      Origin = 'S_CALCF_PSOPER.REWORK_Q'
    end
    object taCalcF_PSOperR: TFloatField
      FieldName = 'R'
      Origin = 'S_CALCF_PSOPER.R'
    end
    object taCalcF_PSOperOUT: TFloatField
      FieldName = 'OUT'
      Origin = 'S_CALCF_PSOPER.OUT'
    end
    object taCalcF_PSOperZ: TFloatField
      FieldName = 'Z'
      Origin = 'S_CALCF_PSOPER.Z'
    end
    object taCalcF_PSOperGET_GR: TFloatField
      FieldName = 'GET_GR'
      Origin = 'S_CALCF_PSOPER.GET_GR'
    end
    object taCalcF_PSOperDONE_GR: TFloatField
      FieldName = 'DONE_GR'
      Origin = 'S_CALCF_PSOPER.DONE_GR'
    end
    object taCalcF_PSOperRET_GR: TFloatField
      FieldName = 'RET_GR'
      Origin = 'S_CALCF_PSOPER.RET_GR'
    end
    object taCalcF_PSOperREJ_GR: TFloatField
      FieldName = 'REJ_GR'
      Origin = 'S_CALCF_PSOPER.REJ_GR'
    end
    object taCalcF_PSOperREWORK_GR: TFloatField
      FieldName = 'REWORK_GR'
      Origin = 'S_CALCF_PSOPER.REWORK_GR'
    end
    object taCalcF_PSOperDESCR_GR: TFIBStringField
      FieldName = 'DESCR_GR'
      Origin = 'S_CALCF_PSOPER.DESCR_GR'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taCalcF_PSOperGET_E: TFloatField
      FieldName = 'GET_E'
      Origin = 'S_CALCF_PSOPER.GET_E'
    end
    object taCalcF_PSOperDONE_E: TFloatField
      FieldName = 'DONE_E'
      Origin = 'S_CALCF_PSOPER.DONE_E'
    end
    object taCalcF_PSOperRET_E: TFloatField
      FieldName = 'RET_E'
      Origin = 'S_CALCF_PSOPER.RET_E'
    end
    object taCalcF_PSOperREJ_E: TFloatField
      FieldName = 'REJ_E'
      Origin = 'S_CALCF_PSOPER.REJ_E'
    end
    object taCalcF_PSOperREWORK_E: TFloatField
      FieldName = 'REWORK_E'
      Origin = 'S_CALCF_PSOPER.REWORK_E'
    end
    object taCalcF_PSOperR_E: TFloatField
      FieldName = 'R_E'
      Origin = 'S_CALCF_PSOPER.R_E'
    end
    object taCalcF_PSOperINPUT: TFloatField
      FieldName = 'INPUT'
      Origin = 'S_CALCF_PSOPER.INPUT'
    end
    object taCalcF_PSOperWZP: TFloatField
      FieldName = 'WZP'
      Origin = 'S_CALCF_PSOPER.WZP'
    end
  end
  object frCalcF_PSOper: TfrDBDataSet
    DataSet = taCalcF_PSOper
    Left = 776
    Top = 172
  end
  object frIN: TfrDBDataSet
    DataSet = taSItemsPrint
    Left = 772
    Top = 284
  end
  object taInvToPrint: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT '
      '          DOCNO ,'
      '          DOCDATE ,'
      '          SUPNAME ,'
      '          CUSTNAME ,'
      '          SUPADDRESS ,'
      '          SUPPHONE ,'
      '          CUSTADDRESS ,'
      '          CUSTPHONE ,'
      '          SUPINN ,'
      '          SUPOKPO ,'
      '          SUPOKONH, '
      '          SUPBILL ,'
      '          SUPKBILL,'
      '          SUPBIK ,'
      '          SUPBANK ,'
      '          CUSTINN ,'
      '          CUSTOKPO ,'
      '          CUSTOKONH ,'
      '          CUSTBILL ,'
      '          CUSTKBILL ,'
      '          CUSTBIK ,'
      '          CUSTBANK,'
      '          COMMISION,'
      '          TRANS_PRICE,'
      '          SUPKPP,'
      '          PAYTYPENAME,'
      '          ITYPE,'
      '          SUPFACTADDRESS,'
      '          CUSTFACTADDRESS'
      '   '
      'from INV_TO_PRINT(:INVID)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taInvToPrintBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 536
    Top = 256
    object taInvToPrintDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'INV_TO_PRINT.DOCNO'
    end
    object taInvToPrintDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'INV_TO_PRINT.DOCDATE'
    end
    object taInvToPrintSUPNAME: TFIBStringField
      FieldName = 'SUPNAME'
      Origin = 'INV_TO_PRINT.SUPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taInvToPrintCUSTNAME: TFIBStringField
      FieldName = 'CUSTNAME'
      Origin = 'INV_TO_PRINT.CUSTNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taInvToPrintSUPADDRESS: TFIBStringField
      FieldName = 'SUPADDRESS'
      Origin = 'INV_TO_PRINT.SUPADDRESS'
      Size = 400
      EmptyStrToNull = True
    end
    object taInvToPrintSUPPHONE: TFIBStringField
      FieldName = 'SUPPHONE'
      Origin = 'INV_TO_PRINT.SUPPHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object taInvToPrintCUSTADDRESS: TFIBStringField
      FieldName = 'CUSTADDRESS'
      Origin = 'INV_TO_PRINT.CUSTADDRESS'
      Size = 400
      EmptyStrToNull = True
    end
    object taInvToPrintCUSTPHONE: TFIBStringField
      FieldName = 'CUSTPHONE'
      Origin = 'INV_TO_PRINT.CUSTPHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object taInvToPrintSUPINN: TFIBStringField
      FieldName = 'SUPINN'
      Origin = 'INV_TO_PRINT.SUPINN'
      Size = 30
      EmptyStrToNull = True
    end
    object taInvToPrintSUPOKPO: TFIBStringField
      FieldName = 'SUPOKPO'
      Origin = 'INV_TO_PRINT.SUPOKPO'
      EmptyStrToNull = True
    end
    object taInvToPrintSUPOKONH: TFIBStringField
      FieldName = 'SUPOKONH'
      Origin = 'INV_TO_PRINT.SUPOKONH'
      Size = 30
      EmptyStrToNull = True
    end
    object taInvToPrintSUPBILL: TFIBStringField
      FieldName = 'SUPBILL'
      Origin = 'INV_TO_PRINT.SUPBILL'
      Size = 60
      EmptyStrToNull = True
    end
    object taInvToPrintSUPKBILL: TFIBStringField
      FieldName = 'SUPKBILL'
      Origin = 'INV_TO_PRINT.SUPKBILL'
      Size = 60
      EmptyStrToNull = True
    end
    object taInvToPrintSUPBIK: TFIBStringField
      FieldName = 'SUPBIK'
      Origin = 'INV_TO_PRINT.SUPBIK'
      Size = 30
      EmptyStrToNull = True
    end
    object taInvToPrintCUSTINN: TFIBStringField
      FieldName = 'CUSTINN'
      Origin = 'INV_TO_PRINT.CUSTINN'
      Size = 30
      EmptyStrToNull = True
    end
    object taInvToPrintCUSTOKPO: TFIBStringField
      FieldName = 'CUSTOKPO'
      Origin = 'INV_TO_PRINT.CUSTOKPO'
      EmptyStrToNull = True
    end
    object taInvToPrintCUSTOKONH: TFIBStringField
      FieldName = 'CUSTOKONH'
      Origin = 'INV_TO_PRINT.CUSTOKONH'
      Size = 30
      EmptyStrToNull = True
    end
    object taInvToPrintCUSTBILL: TFIBStringField
      FieldName = 'CUSTBILL'
      Origin = 'INV_TO_PRINT.CUSTBILL'
      Size = 50
      EmptyStrToNull = True
    end
    object taInvToPrintCUSTKBILL: TFIBStringField
      FieldName = 'CUSTKBILL'
      Origin = 'INV_TO_PRINT.CUSTKBILL'
      Size = 40
      EmptyStrToNull = True
    end
    object taInvToPrintCUSTBIK: TFIBStringField
      FieldName = 'CUSTBIK'
      Origin = 'INV_TO_PRINT.CUSTBIK'
      Size = 30
      EmptyStrToNull = True
    end
    object taInvToPrintSUPBANK: TFIBStringField
      FieldName = 'SUPBANK'
      Origin = 'INV_TO_PRINT.SUPBANK'
      Size = 80
      EmptyStrToNull = True
    end
    object taInvToPrintCUSTBANK: TFIBStringField
      FieldName = 'CUSTBANK'
      Origin = 'INV_TO_PRINT.CUSTBANK'
      Size = 80
      EmptyStrToNull = True
    end
    object taInvToPrintCOMMISION: TSmallintField
      FieldName = 'COMMISION'
      Origin = 'INV_TO_PRINT.COMMISION'
    end
    object taInvToPrintTRANS_PRICE: TFloatField
      FieldName = 'TRANS_PRICE'
      Origin = 'INV_TO_PRINT.TRANS_PRICE'
    end
    object taInvToPrintSUPKPP: TFIBStringField
      FieldName = 'SUPKPP'
      Size = 40
      EmptyStrToNull = True
    end
    object taInvToPrintPAYTYPENAME: TFIBStringField
      FieldName = 'PAYTYPENAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taInvToPrintITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object taInvToPrintSUPFACTADDRESS: TFIBStringField
      FieldName = 'SUPFACTADDRESS'
      Size = 400
      EmptyStrToNull = True
    end
    object taInvToPrintCUSTFACTADDRESS: TFIBStringField
      FieldName = 'CUSTFACTADDRESS'
      Size = 400
      EmptyStrToNull = True
    end
  end
  object frInvToPrint: TfrDBDataSet
    DataSet = taInvToPrint
    Left = 536
    Top = 300
  end
  object taVedomostToPrint: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT '
      '    AV.REST1_W ,'
      '    AV.REST1_Q ,'
      '    AV.REST1_SUM ,'
      '    AV.IN_W ,'
      '    AV.IN_Q ,'
      '    AV.IN_SUM ,'
      '    AV.RET_AKCIZ,'
      '    AV.RET_NDS,'
      '    AV.RET_FULLSUM,'
      '    AV.RET_W ,'
      '    AV.RET_Q ,'
      '    AV.RET_SUM ,'
      '    AV.SELL_W ,'
      '    AV.SELL_Q ,'
      '    AV.SELL_SUM ,'
      '    AV.SELL_AKCIZ,'
      '    AV.SELL_NDS,'
      '    AV.SELL_FULLSUM,'
      '    AV.REST2_W ,'
      '    AV.REST2_Q ,'
      '    AV.REST2_SUM ,'
      '    AV.ART,'
      '    A.BD,'
      '    A.DOCDATE,'
      '    AV.BRACK_Q,'
      '    AV.BRACK_W,'
      '    AV.BRACK_SUM,'
      '    AV.ACT_Q,'
      '    AV.ACT_W,'
      '    AV.ACT_SUM'
      'FROM  ART_VEDOMOST AV, ACT A'
      'where  A.ID=:ACTID and'
      '            Av.ACTID=A.ID'
      'ORDER BY ART')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taVedomostToPrintBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 472
    Top = 256
    object taVedomostToPrintREST1_W: TFloatField
      FieldName = 'REST1_W'
      Origin = 'ART_VEDOMOST.REST1_W'
    end
    object taVedomostToPrintREST1_Q: TIntegerField
      FieldName = 'REST1_Q'
      Origin = 'ART_VEDOMOST.REST1_Q'
    end
    object taVedomostToPrintREST1_SUM: TFloatField
      FieldName = 'REST1_SUM'
      Origin = 'ART_VEDOMOST.REST1_SUM'
    end
    object taVedomostToPrintIN_W: TFloatField
      FieldName = 'IN_W'
      Origin = 'ART_VEDOMOST.IN_W'
    end
    object taVedomostToPrintIN_Q: TIntegerField
      FieldName = 'IN_Q'
      Origin = 'ART_VEDOMOST.IN_Q'
    end
    object taVedomostToPrintIN_SUM: TFloatField
      FieldName = 'IN_SUM'
      Origin = 'ART_VEDOMOST.IN_SUM'
    end
    object taVedomostToPrintRET_AKCIZ: TFloatField
      FieldName = 'RET_AKCIZ'
      Origin = 'ART_VEDOMOST.RET_AKCIZ'
    end
    object taVedomostToPrintRET_NDS: TFloatField
      FieldName = 'RET_NDS'
      Origin = 'ART_VEDOMOST.RET_NDS'
    end
    object taVedomostToPrintRET_FULLSUM: TFloatField
      FieldName = 'RET_FULLSUM'
      Origin = 'ART_VEDOMOST.RET_FULLSUM'
    end
    object taVedomostToPrintRET_W: TFloatField
      FieldName = 'RET_W'
      Origin = 'ART_VEDOMOST.RET_W'
    end
    object taVedomostToPrintRET_Q: TFloatField
      FieldName = 'RET_Q'
      Origin = 'ART_VEDOMOST.RET_Q'
    end
    object taVedomostToPrintRET_SUM: TFloatField
      FieldName = 'RET_SUM'
      Origin = 'ART_VEDOMOST.RET_SUM'
    end
    object taVedomostToPrintSELL_W: TFloatField
      FieldName = 'SELL_W'
      Origin = 'ART_VEDOMOST.SELL_W'
    end
    object taVedomostToPrintSELL_Q: TIntegerField
      FieldName = 'SELL_Q'
      Origin = 'ART_VEDOMOST.SELL_Q'
    end
    object taVedomostToPrintSELL_SUM: TFloatField
      FieldName = 'SELL_SUM'
      Origin = 'ART_VEDOMOST.SELL_SUM'
    end
    object taVedomostToPrintSELL_AKCIZ: TFloatField
      FieldName = 'SELL_AKCIZ'
      Origin = 'ART_VEDOMOST.SELL_AKCIZ'
    end
    object taVedomostToPrintSELL_NDS: TFloatField
      FieldName = 'SELL_NDS'
      Origin = 'ART_VEDOMOST.SELL_NDS'
    end
    object taVedomostToPrintSELL_FULLSUM: TFloatField
      FieldName = 'SELL_FULLSUM'
      Origin = 'ART_VEDOMOST.SELL_FULLSUM'
    end
    object taVedomostToPrintREST2_W: TFloatField
      FieldName = 'REST2_W'
      Origin = 'ART_VEDOMOST.REST2_W'
    end
    object taVedomostToPrintREST2_Q: TIntegerField
      FieldName = 'REST2_Q'
      Origin = 'ART_VEDOMOST.REST2_Q'
    end
    object taVedomostToPrintREST2_SUM: TFloatField
      FieldName = 'REST2_SUM'
      Origin = 'ART_VEDOMOST.REST2_SUM'
    end
    object taVedomostToPrintART: TFIBStringField
      FieldName = 'ART'
      Origin = 'ART_VEDOMOST.ART'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object taVedomostToPrintBD: TDateTimeField
      FieldName = 'BD'
      Origin = 'ACT.BD'
    end
    object taVedomostToPrintDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'ACT.DOCDATE'
      Required = True
    end
    object taVedomostToPrintBRACK_Q: TIntegerField
      FieldName = 'BRACK_Q'
      Origin = 'ART_VEDOMOST.BRACK_Q'
    end
    object taVedomostToPrintBRACK_W: TFloatField
      FieldName = 'BRACK_W'
      Origin = 'ART_VEDOMOST.BRACK_W'
    end
    object taVedomostToPrintBRACK_SUM: TFloatField
      FieldName = 'BRACK_SUM'
      Origin = 'ART_VEDOMOST.BRACK_SUM'
    end
    object taVedomostToPrintACT_Q: TFloatField
      FieldName = 'ACT_Q'
      Origin = 'ART_VEDOMOST.ACT_Q'
    end
    object taVedomostToPrintACT_W: TFloatField
      FieldName = 'ACT_W'
      Origin = 'ART_VEDOMOST.ACT_W'
    end
    object taVedomostToPrintACT_SUM: TFloatField
      FieldName = 'ACT_SUM'
      Origin = 'ART_VEDOMOST.ACT_SUM'
    end
  end
  object frVedomostTOPrint: TfrDBDataSet
    DataSet = taVedomostToPrint
    Left = 472
    Top = 300
  end
  object taACTRePrice: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT '
      '   ART ,'
      '    ART2 ,'
      '    OLD_PRICE ,'
      '    NEW_PRICE ,'
      '    U ,'
      '    INS ,'
      '    Q ,'
      '    W,'
      '   SUM_Q0,'
      '   IN_SUM,'
      '   OUT_SUM,'
      '   RAZ_SUM'
      'from DOC_ACTS(:INVID)'
      'order by ART')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taACTRePriceBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 684
    Top = 256
    object taACTRePriceART: TFIBStringField
      FieldName = 'ART'
      Origin = 'DOC_ACTS.ART'
      EmptyStrToNull = True
    end
    object taACTRePriceART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'DOC_ACTS.ART2'
      EmptyStrToNull = True
    end
    object taACTRePriceOLD_PRICE: TFloatField
      FieldName = 'OLD_PRICE'
      Origin = 'DOC_ACTS.OLD_PRICE'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
      currency = True
    end
    object taACTRePriceNEW_PRICE: TFloatField
      FieldName = 'NEW_PRICE'
      Origin = 'DOC_ACTS.NEW_PRICE'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
      currency = True
    end
    object taACTRePriceU: TFIBStringField
      FieldName = 'U'
      Origin = 'DOC_ACTS.U'
      Size = 5
      EmptyStrToNull = True
    end
    object taACTRePriceINS: TFIBStringField
      FieldName = 'INS'
      Origin = 'DOC_ACTS.INS'
      Size = 5
      EmptyStrToNull = True
    end
    object taACTRePriceQ: TIntegerField
      FieldName = 'Q'
      Origin = 'DOC_ACTS.Q'
    end
    object taACTRePriceW: TFloatField
      FieldName = 'W'
      Origin = 'DOC_ACTS.W'
    end
    object taACTRePriceSUM_Q0: TFloatField
      DisplayLabel = #1042#1077#1089' ('#1082#1086#1083'-'#1074#1086') '#1076#1086' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      FieldName = 'SUM_Q0'
      Origin = 'DOC_ACTS.SUM_Q0'
      DisplayFormat = '0.00'
    end
    object taACTRePriceIN_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072' '#1076#1086' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      FieldName = 'IN_SUM'
      Origin = 'DOC_ACTS.IN_SUM'
      currency = True
    end
    object taACTRePriceOUT_SUM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072' '#1087#1086#1089#1083#1077' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      FieldName = 'OUT_SUM'
      Origin = 'DOC_ACTS.OUT_SUM'
      currency = True
    end
    object taACTRePriceRAZ_SUM: TFloatField
      DisplayLabel = #1056#1072#1079#1085#1080#1094#1072
      FieldName = 'RAZ_SUM'
      Origin = 'DOC_ACTS.RAZ_SUM'
      currency = True
    end
  end
  object frStones: TfrDBDataSet
    DataSource = dmInv.dsrStoneItems
    Left = 540
    Top = 4
  end
  object taProdAppl: TpFIBDataSet
    SelectSQL.Strings = (
      'select ART, SZNAME, Q, APPLNO, APPLDATE'
      'from Doc_Appl(:ID)'
      'order by Art, SzName, Q')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 384
    Top = 604
    object taProdApplART: TFIBStringField
      FieldName = 'ART'
      Origin = 'DOC_APPL.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taProdApplSZNAME: TFIBStringField
      FieldName = 'SZNAME'
      Origin = 'DOC_APPL.SZNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taProdApplQ: TFloatField
      FieldName = 'Q'
      Origin = 'DOC_APPL.Q'
    end
    object taProdApplAPPLNO: TIntegerField
      FieldName = 'APPLNO'
      Origin = 'DOC_APPL.APPLNO'
    end
    object taProdApplAPPLDATE: TDateTimeField
      FieldName = 'APPLDATE'
      Origin = 'DOC_APPL.APPLDATE'
    end
  end
  object frProdAppl: TfrDBDataSet
    DataSet = taProdAppl
    Left = 384
    Top = 652
  end
  object taSellAnaliz: TpFIBDataSet
    SelectSQL.Strings = (
      'select APPLNO, APPLDATE, ART, SZNAME, WQ, Q, INS, INW,'
      '    INQ, DEBTORS, DEBTORW, DEBTORQ, REJS, REJW,'
      '    REJQ, SALES, SALEW, SALEQ, RETS, RETW, RETQ, OUTS,'
      '    OUTW, OUTQ, STORES, STOREW, STOREQ '
      'from Doc_ProdAnaliz(:ID)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 444
    Top = 532
  end
  object frSellAnaliz: TfrDBDataSet
    DataSet = taSellAnaliz
    Left = 444
    Top = 580
  end
  object taPSOper: TpFIBDataSet
    SelectSQL.Strings = (
      'select o.OperId, o.Operation, o.Rate, t.Name Tails '
      'from D_Oper o, D_PsOper po, D_Tails t'
      'where po.DepId=:DEPID and  '
      '           po.OperId=o.OperId and'
      '           o.Tails=t.Id'
      'order by Operation')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taPSOperBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 496
    Top = 8
    object taPSOperOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'D_OPER.OPERID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taPSOperOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Origin = 'D_OPER.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taPSOperRATE: TFloatField
      FieldName = 'RATE'
      Origin = 'D_OPER.RATE'
    end
    object taPSOperTAILS: TFIBStringField
      FieldName = 'TAILS'
      Origin = 'D_TAILS.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrPSOper: TDataSource
    DataSet = taPSOper
    Left = 496
    Top = 55
  end
  object frPSOper: TfrDBDataSet
    DataSet = taPSOper
    Left = 496
    Top = 100
  end
  object taSJ_MatPS: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, SEMIS, GET, REWORK, DONE, RET, REJ, OUT,'
      
        '  GET_Q, REWORK_Q, DONE_Q, RET_Q, REJ_Q, OUT_Q, O1,     EDGSHAPE' +
        'ID, EDGTID, CHROMATICITY, CLEANNES, SZ'
      
        'from DOC_JPS2(:PSID, :OPERID1, :OPERID2, :MATID1, :MATID2, :BD, ' +
        ':ED)'
      'order by O1, EdgShapeId, EdgTId, Chromaticity, Cleannes, Sz')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taSJ_PSOperBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 573
    Top = 160
    object taSJ_MatPSID: TFIBStringField
      FieldName = 'ID'
      Origin = 'DOC_JPS2.ID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSJ_MatPSSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Origin = 'DOC_JPS2.SEMIS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSJ_MatPSGET: TFloatField
      FieldName = 'GET'
      Origin = 'DOC_JPS2.GET'
    end
    object taSJ_MatPSREWORK: TFloatField
      FieldName = 'REWORK'
      Origin = 'DOC_JPS2.REWORK'
    end
    object taSJ_MatPSDONE: TFloatField
      FieldName = 'DONE'
      Origin = 'DOC_JPS2.DONE'
    end
    object taSJ_MatPSRET: TFloatField
      FieldName = 'RET'
      Origin = 'DOC_JPS2.RET'
    end
    object taSJ_MatPSREJ: TFloatField
      FieldName = 'REJ'
      Origin = 'DOC_JPS2.REJ'
    end
    object taSJ_MatPSOUT: TFloatField
      FieldName = 'OUT'
      Origin = 'DOC_JPS2.OUT'
    end
    object taSJ_MatPSGET_Q: TFloatField
      FieldName = 'GET_Q'
      Origin = 'DOC_JPS2.GET_Q'
    end
    object taSJ_MatPSREWORK_Q: TFloatField
      FieldName = 'REWORK_Q'
      Origin = 'DOC_JPS2.REWORK_Q'
    end
    object taSJ_MatPSDONE_Q: TFloatField
      FieldName = 'DONE_Q'
      Origin = 'DOC_JPS2.DONE_Q'
    end
    object taSJ_MatPSRET_Q: TFloatField
      FieldName = 'RET_Q'
      Origin = 'DOC_JPS2.RET_Q'
    end
    object taSJ_MatPSREJ_Q: TFloatField
      FieldName = 'REJ_Q'
      Origin = 'DOC_JPS2.REJ_Q'
    end
    object taSJ_MatPSOUT_Q: TFloatField
      FieldName = 'OUT_Q'
      Origin = 'DOC_JPS2.OUT_Q'
    end
    object taSJ_MatPSO1: TFIBStringField
      FieldName = 'O1'
      Origin = 'DOC_JPS2.O1'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSJ_MatPSEDGSHAPEID: TFIBStringField
      FieldName = 'EDGSHAPEID'
      Origin = 'DOC_JPS2.EDGSHAPEID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSJ_MatPSEDGTID: TFIBStringField
      FieldName = 'EDGTID'
      Origin = 'DOC_JPS2.EDGTID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSJ_MatPSCHROMATICITY: TFIBStringField
      FieldName = 'CHROMATICITY'
      Origin = 'DOC_JPS2.CHROMATICITY'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSJ_MatPSCLEANNES: TFIBStringField
      FieldName = 'CLEANNES'
      Origin = 'DOC_JPS2.CLEANNES'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSJ_MatPSSZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'DOC_JPS2.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object frSJ_MatPS: TfrDBDataSet
    DataSet = taSJ_MatPS
    Left = 574
    Top = 204
  end
  object taCalcSF_PSOper: TpFIBDataSet
    SelectSQL.Strings = (
      'select NTOTAL, GET, GET_Q, DONE,'
      '    DONE_Q, RET, RET_Q, REJ, REJ_Q,'
      '    REWORK, REWORK_Q, R, OUT, Z, GET_GR, DONE_GR,'
      '    RET_GR, REJ_GR, REWORK_GR, DESCR_GR,'
      '    GET_E, DONE_E, RET_E, REJ_E, REWORK_E, R_E,'
      '    INPUT, WZP'
      
        'from S_CalcF_PSOper(:BD, :ED, :MATID1, :MATID2, :PSID, :OPERID, ' +
        '1)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taCalcSF_PSOperBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 866
    Top = 128
  end
  object frCalcSF_PSOper: TfrDBDataSet
    DataSet = taCalcSF_PSOper
    Left = 868
    Top = 172
  end
  object taSItemsPrint: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT SELID, '
      '               INVID,'
      '               ART2ID,'
      '               ART2 ,'
      '               FULLART,'
      '               PRICE,           '
      '               TPRICE,     '
      '               QUANTITY,'
      '               TOTALWEIGHT,'
      '               Q,'
      '               W,   '
      '               PRICE2,'
      '               PNDS,'
      '               UNITID,'
      '               NDSID, '
      '               ART, '
      '               USEMARGIN,   '
      '               FULLSUM,'
      
        '              D_INSID ,  D_ARTID ,  EP2,  SSUM,   SSUMD, SSUMF, ' +
        'PRILL, AKCIZ, GOOD, ACC_SUMM, SZname, mat,'
      '              INSSNAME'
      'from SEL_S1(:ASINVID)'
      'ORDER BY ART '
      '')
    CacheModelOptions.BufferChunks = 1000
    AfterScroll = taSItemsPrintAfterScroll
    Transaction = dm.tr
    Database = dm.db
    Left = 768
    Top = 240
    object taSItemsPrintSELID: TIntegerField
      FieldName = 'SELID'
      Origin = 'SEL_S.SELID'
    end
    object taSItemsPrintINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SEL_S.INVID'
    end
    object taSItemsPrintART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'SEL_S.ART2ID'
    end
    object taSItemsPrintART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'SEL_S.ART2'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taSItemsPrintFULLART: TFIBStringField
      FieldName = 'FULLART'
      Origin = 'SEL_S.FULLART'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSItemsPrintPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'SEL_S.PRICE'
    end
    object taSItemsPrintTPRICE: TFloatField
      FieldName = 'TPRICE'
      Origin = 'SEL_S.TPRICE'
    end
    object taSItemsPrintQUANTITY: TSmallintField
      FieldName = 'QUANTITY'
      Origin = 'SEL_S.QUANTITY'
    end
    object taSItemsPrintTOTALWEIGHT: TFloatField
      FieldName = 'TOTALWEIGHT'
      Origin = 'SEL_S.TOTALWEIGHT'
    end
    object taSItemsPrintQ: TIntegerField
      FieldName = 'Q'
      Origin = 'SEL_S.Q'
    end
    object taSItemsPrintW: TFloatField
      FieldName = 'W'
      Origin = 'SEL_S.W'
    end
    object taSItemsPrintPRICE2: TFloatField
      FieldName = 'PRICE2'
      Origin = 'SEL_S.PRICE2'
    end
    object taSItemsPrintPNDS: TFloatField
      FieldName = 'PNDS'
      Origin = 'SEL_S.PNDS'
    end
    object taSItemsPrintUNITID: TIntegerField
      FieldName = 'UNITID'
      Origin = 'SEL_S.UNITID'
    end
    object taSItemsPrintNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SEL_S.NDSID'
    end
    object taSItemsPrintART: TFIBStringField
      FieldName = 'ART'
      Origin = 'SEL_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSItemsPrintUSEMARGIN: TSmallintField
      FieldName = 'USEMARGIN'
      Origin = 'SEL_S.USEMARGIN'
    end
    object taSItemsPrintFULLSUM: TFloatField
      FieldName = 'FULLSUM'
      Origin = 'SEL_S.FULLSUM'
    end
    object taSItemsPrintD_INSID: TFIBStringField
      FieldName = 'D_INSID'
      Origin = 'SEL_S.D_INSID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSItemsPrintD_ARTID: TIntegerField
      FieldName = 'D_ARTID'
      Origin = 'SEL_S.D_ARTID'
    end
    object taSItemsPrintEP2: TFloatField
      FieldName = 'EP2'
      Origin = 'SEL_S.EP2'
    end
    object taSItemsPrintSSUM: TFloatField
      FieldName = 'SSUM'
      Origin = 'SEL_S.SSUM'
    end
    object taSItemsPrintSSUMD: TFloatField
      FieldName = 'SSUMD'
      Origin = 'SEL_S.SSUMD'
    end
    object taSItemsPrintSSUMF: TFloatField
      FieldName = 'SSUMF'
      Origin = 'SEL_S.SSUMF'
    end
    object taSItemsPrintPRILL: TFIBStringField
      FieldName = 'PRILL'
      Origin = 'SEL_S.PRILL'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSItemsPrintAKCIZ: TFloatField
      FieldName = 'AKCIZ'
      Origin = 'SEL_S.AKCIZ'
    end
    object taSItemsPrintGOOD: TFIBStringField
      FieldName = 'GOOD'
      Origin = 'SEL_S.GOOD'
      EmptyStrToNull = True
    end
    object taSItemsPrintUNIT: TStringField
      FieldKind = fkLookup
      FieldName = 'UNIT'
      LookupDataSet = taUnit
      LookupKeyFields = 'UNITID'
      LookupResultField = 'U'
      KeyFields = 'UNITID'
      Size = 5
      Lookup = True
    end
    object taSItemsPrintACC_SUMM: TFloatField
      FieldName = 'ACC_SUMM'
      Origin = 'SEL_S.ACC_SUMM'
      currency = True
    end
    object taSItemsPrintSZNAME: TFIBStringField
      FieldName = 'SZNAME'
      Origin = 'SEL_S.SZNAME'
      EmptyStrToNull = True
    end
    object taSItemsPrintMAT: TFIBStringField
      FieldName = 'MAT'
      Size = 40
      EmptyStrToNull = True
    end
    object taSItemsPrintINSSNAME: TFIBStringField
      FieldName = 'INSSNAME'
      EmptyStrToNull = True
    end
  end
  object frActRePrice: TfrDBDataSet
    DataSet = taACTRePrice
    Left = 688
    Top = 304
  end
  object frWhArt: TfrDBDataSet
    DataSet = dmMain.taWhArt
    Left = 792
    Top = 56
  end
  object taUnit: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from D_UNIT')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 584
    Top = 4
  end
  object dsUnit: TDataSource
    DataSet = taUnit
    Left = 576
    Top = 48
  end
  object taStoneItems: TpFIBDataSet
    SelectSQL.Strings = (
      'Select Stones'
      'from STONES_S(:ART2ID)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taStoneItemsBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 840
    Top = 240
    object taStoneItemsSTONES: TFIBStringField
      FieldName = 'STONES'
      Origin = 'STONES_S.STONES'
      Size = 100
      EmptyStrToNull = True
    end
  end
  object frStoneItems: TfrDBDataSet
    DataSet = taStoneItems
    Left = 840
    Top = 284
  end
  object taINVHeader: TpFIBDataSet
    SelectSQL.Strings = (
      'select * '
      'from INV_HEADER2(:INVID)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taINVHeaderBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 1080
    Top = 61
    object taINVHeaderSUPNAME: TFIBStringField
      FieldName = 'SUPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taINVHeaderFROMDEPNAME: TFIBStringField
      FieldName = 'FROMDEPNAME'
      Size = 50
      EmptyStrToNull = True
    end
    object taINVHeaderDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object taINVHeaderDOCNO: TFIBIntegerField
      FieldName = 'DOCNO'
    end
    object taINVHeaderSSFDT: TFIBDateTimeField
      FieldName = 'SSFDT'
    end
    object taINVHeaderSSF: TFIBStringField
      FieldName = 'SSF'
      Size = 40
      EmptyStrToNull = True
    end
    object taINVHeaderSELFNAME: TFIBStringField
      FieldName = 'SELFNAME'
      Size = 100
      EmptyStrToNull = True
    end
    object taINVHeaderDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Size = 50
      EmptyStrToNull = True
    end
    object taINVHeaderDOGNO: TFIBStringField
      FieldName = 'DOGNO'
      EmptyStrToNull = True
    end
    object taINVHeaderDOGDATE: TFIBDateTimeField
      FieldName = 'DOGDATE'
    end
    object taINVHeaderUSERNAME: TFIBStringField
      FieldName = 'USERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taINVHeaderBOSSFIO: TFIBStringField
      FieldName = 'BOSSFIO'
      Size = 120
      EmptyStrToNull = True
    end
    object taINVHeaderOFIO: TFIBStringField
      FieldName = 'OFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taINVHeaderSELFCOMPANYINN: TFIBStringField
      FieldName = 'SELF$COMPANY$INN'
      Size = 30
      EmptyStrToNull = True
    end
    object taINVHeaderSUPPLIERBOSSFIO: TFIBStringField
      FieldName = 'SUPPLIER$BOSS$FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taINVHeaderSELFPHONE: TFIBStringField
      FieldName = 'SELFPHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object taINVHeaderSELFADDRESS: TFIBStringField
      FieldName = 'SELFADDRESS'
      Size = 80
      EmptyStrToNull = True
    end
    object taINVHeadersuppliercity: TStringField
      FieldName = 'supplier$city'
    end
    object taINVHeaderSHORTBOSS: TStringField
      FieldName = 'SHORT$BOSS'
    end
  end
  object frInvHeader: TfrDBDataSet
    DataSet = taINVHeader
    Left = 896
    Top = 384
  end
  object taSIListToPrint: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select SUPNAME, FROMDEPNAME, DEPNAME, SSF, SSFDT, DOCNO, DOCDATE' +
        ', SELFNAME,'
      
        '       DOGNO, DOGDATE, BOSSFIO, SELF$COMPANY$INN, SUPPLIER$BOSS$' +
        'FIO,'
      
        '       SELF$COMPANY$ADDRESS, SUPLIER$COMPANY$ADDRESS, SUPLIER$CO' +
        'MPANY$INN '
      'from INV_HEADER(:INVID)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 608
    Top = 256
    object taSIListToPrintSUPNAME: TFIBStringField
      FieldName = 'SUPNAME'
      Size = 120
      EmptyStrToNull = True
    end
    object taSIListToPrintFROMDEPNAME: TFIBStringField
      FieldName = 'FROMDEPNAME'
      Size = 50
      EmptyStrToNull = True
    end
    object taSIListToPrintDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Size = 50
      EmptyStrToNull = True
    end
    object taSIListToPrintSSF: TFIBStringField
      FieldName = 'SSF'
      Size = 40
      EmptyStrToNull = True
    end
    object taSIListToPrintSSFDT: TFIBDateTimeField
      FieldName = 'SSFDT'
    end
    object taSIListToPrintDOCNO: TFIBIntegerField
      FieldName = 'DOCNO'
    end
    object taSIListToPrintDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object taSIListToPrintSELFNAME: TFIBStringField
      FieldName = 'SELFNAME'
      Size = 100
      EmptyStrToNull = True
    end
    object taSIListToPrintDOGNO: TFIBStringField
      FieldName = 'DOGNO'
      EmptyStrToNull = True
    end
    object taSIListToPrintDOGDATE: TFIBDateTimeField
      FieldName = 'DOGDATE'
    end
    object taSIListToPrintBOSSFIO: TFIBStringField
      FieldName = 'BOSSFIO'
      Size = 120
      EmptyStrToNull = True
    end
    object taSIListToPrintSELFCOMPANYINN: TFIBStringField
      FieldName = 'SELF$COMPANY$INN'
      Size = 30
      EmptyStrToNull = True
    end
    object taSIListToPrintSUPPLIERBOSSFIO: TFIBStringField
      FieldName = 'SUPPLIER$BOSS$FIO'
      Size = 120
      EmptyStrToNull = True
    end
    object taSIListToPrintSELFCOMPANYADDRESS: TFIBStringField
      FieldName = 'SELF$COMPANY$ADDRESS'
      Size = 80
      EmptyStrToNull = True
    end
    object taSIListToPrintSUPLIERCOMPANYADDRESS: TFIBStringField
      FieldName = 'SUPLIER$COMPANY$ADDRESS'
      Size = 80
      EmptyStrToNull = True
    end
    object taSIListToPrintSUPLIERCOMPANYINN: TFIBStringField
      FieldName = 'SUPLIER$COMPANY$INN'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object frSIListTOPrint: TfrDBDataSet
    DataSet = taSIListToPrint
    Left = 608
    Top = 300
  end
  object taSielToPrint: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  SEMIS, '
      '  PRICE, '
      '  W, '
      '  Q, '
      '  NDS, '
      '  UQ, '
      '  UW, '
      '  COST, '
      '  SEMIS_NAME, '
      '  QW, '
      '  UQW,'
      '  PRILL_NAME'
      'from '
      '  DOC_SIEL_NEW(:INVID)')
    CacheModelOptions.BufferChunks = 1000
    AfterOpen = taSielToPrintAfterOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 832
    Top = 336
    object taSielToPrintSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Origin = 'DOC_SIEL.SEMIS'
      Size = 10
      EmptyStrToNull = True
    end
    object taSielToPrintPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'DOC_SIEL.PRICE'
      currency = True
    end
    object taSielToPrintW: TFloatField
      FieldName = 'W'
      Origin = 'DOC_SIEL.W'
    end
    object taSielToPrintQ: TIntegerField
      FieldName = 'Q'
      Origin = 'DOC_SIEL.Q'
    end
    object taSielToPrintNDS: TFloatField
      FieldName = 'NDS'
      Origin = 'DOC_SIEL.NDS'
    end
    object taSielToPrintUQ: TFIBStringField
      FieldName = 'UQ'
      Origin = 'DOC_SIEL.UQ'
      Size = 10
      EmptyStrToNull = True
    end
    object taSielToPrintUW: TFIBStringField
      FieldName = 'UW'
      Origin = 'DOC_SIEL.UW'
      Size = 10
      EmptyStrToNull = True
    end
    object taSielToPrintCOST: TFloatField
      FieldName = 'COST'
      Origin = 'DOC_SIEL.COST'
      currency = True
    end
    object taSielToPrintSEMIS_NAME: TFIBStringField
      FieldName = 'SEMIS_NAME'
      Origin = 'DOC_SIEL.SEMIS_NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taSielToPrintQW: TFloatField
      FieldName = 'QW'
    end
    object taSielToPrintUQW: TStringField
      FieldName = 'UQW'
      Size = 10
    end
    object taSielToPrintPRILL_NAME: TFIBStringField
      FieldName = 'PRILL_NAME'
      EmptyStrToNull = True
    end
  end
  object frSIELToPrint: TfrDBDataSet
    DataSet = taSielToPrint
    Left = 832
    Top = 384
  end
  object taVedomostToPrint2: TpFIBDataSet
    SelectSQL.Strings = (
      'Select DOCDATE,'
      'DOCNO,'
      'Q,'
      'W,'
      'INSUM,'
      'OUTSUM,'
      'RETSUM,'
      'RETTOPRODSUM,'
      'TONAME,'
      'FROMNAME,'
      'BDATE,'
      'EDATE,'
      'INVID,'
      'INQ,'
      'INW, RETQ, RETW, OUTQ, OUTW, RETTOPRODW,'
      'ACC_SUMM, ITYPE,'
      'acc$in,'
      'acc$ret,'
      'acc$out'
      'from DOC_VEDOMOST_2(:BD,:ED,:T0, :FILTER$MATERIAL)'
      'order by DOCDATE, INVID, DOCNO')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taVedomostToPrint2BeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 896
    Top = 436
    object taVedomostToPrint2DOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'VEDOMOST_TO_PRINT2.DOCDATE'
    end
    object taVedomostToPrint2DOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'VEDOMOST_TO_PRINT2.DOCNO'
    end
    object taVedomostToPrint2Q: TIntegerField
      FieldName = 'Q'
      Origin = 'VEDOMOST_TO_PRINT2.Q'
    end
    object taVedomostToPrint2W: TFloatField
      FieldName = 'W'
      Origin = 'VEDOMOST_TO_PRINT2.W'
    end
    object taVedomostToPrint2OUTSUM: TFloatField
      FieldName = 'OUTSUM'
      Origin = 'VEDOMOST_TO_PRINT2.OUTSUM'
      currency = True
    end
    object taVedomostToPrint2RETSUM: TFloatField
      FieldName = 'RETSUM'
      Origin = 'VEDOMOST_TO_PRINT2.RETSUM'
      currency = True
    end
    object taVedomostToPrint2RETTOPRODSUM: TFloatField
      FieldName = 'RETTOPRODSUM'
      Origin = 'VEDOMOST_TO_PRINT2.RETTOPRODSUM'
      currency = True
    end
    object taVedomostToPrint2INSUM: TFloatField
      FieldName = 'INSUM'
      Origin = 'VEDOMOST_TO_PRINT2.INSUM'
      currency = True
    end
    object taVedomostToPrint2TONAME: TFIBStringField
      FieldName = 'TONAME'
      Origin = 'VEDOMOST_TO_PRINT2.TONAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taVedomostToPrint2FROMNAME: TFIBStringField
      FieldName = 'FROMNAME'
      Origin = 'VEDOMOST_TO_PRINT2.FROMNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taVedomostToPrint2BDATE: TDateTimeField
      FieldName = 'BDATE'
      Origin = 'VEDOMOST_TO_PRINT2.BDATE'
    end
    object taVedomostToPrint2EDATE: TDateTimeField
      FieldName = 'EDATE'
      Origin = 'VEDOMOST_TO_PRINT2.EDATE'
    end
    object taVedomostToPrint2INVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'DOC_VEDOMOST_2.INVID'
    end
    object taVedomostToPrint2INQ: TIntegerField
      FieldName = 'INQ'
      Origin = 'DOC_VEDOMOST_2.INQ'
    end
    object taVedomostToPrint2INW: TFloatField
      FieldName = 'INW'
      Origin = 'DOC_VEDOMOST_2.INW'
    end
    object taVedomostToPrint2RETQ: TIntegerField
      FieldName = 'RETQ'
      Origin = 'DOC_VEDOMOST_2.RETQ'
    end
    object taVedomostToPrint2RETW: TFloatField
      FieldName = 'RETW'
      Origin = 'DOC_VEDOMOST_2.RETW'
    end
    object taVedomostToPrint2OUTQ: TIntegerField
      FieldName = 'OUTQ'
      Origin = 'DOC_VEDOMOST_2.OUTQ'
    end
    object taVedomostToPrint2OUTW: TFloatField
      FieldName = 'OUTW'
      Origin = 'DOC_VEDOMOST_2.OUTW'
    end
    object taVedomostToPrint2ACC_SUMM: TFloatField
      FieldName = 'ACC_SUMM'
      Origin = 'DOC_VEDOMOST_2.ACC_SUMM'
      currency = True
    end
    object taVedomostToPrint2RETTOPRODW: TFloatField
      FieldName = 'RETTOPRODW'
      Origin = 'DOC_VEDOMOST_2.RETTOPRODW'
    end
    object taVedomostToPrint2ITYPE: TFIBIntegerField
      FieldName = 'ITYPE'
    end
    object taVedomostToPrint2ACCIN: TFIBBCDField
      FieldName = 'ACC$IN'
      Size = 2
      RoundByScale = True
    end
    object taVedomostToPrint2ACCRET: TFIBBCDField
      FieldName = 'ACC$RET'
      Size = 2
      RoundByScale = True
    end
    object taVedomostToPrint2ACCOUT: TFIBBCDField
      FieldName = 'ACC$OUT'
      Size = 2
      RoundByScale = True
    end
  end
  object frVedomostToPrint2: TfrDBDataSet
    DataSet = taVedomostToPrint2
    Left = 908
    Top = 488
  end
  object frWhSemis: TfrDBDataSet
    DataSet = dmMain.taWhS
    Left = 844
    Top = 56
  end
  object taPItem_d: TpFIBDataSet
    SelectSQL.Strings = (
      'select NAME, F, N, E, S, I, H, DONEW, RW, U'
      'from Doc_Protocol_d(:ACTID, :T)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 168
    Top = 204
    object taPItem_dNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'DOC_PROTOCOL_D.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taPItem_dF: TFloatField
      FieldName = 'F'
      Origin = 'DOC_PROTOCOL_D.F'
    end
    object taPItem_dN: TFloatField
      FieldName = 'N'
      Origin = 'DOC_PROTOCOL_D.N'
    end
    object taPItem_dE: TFloatField
      FieldName = 'E'
      Origin = 'DOC_PROTOCOL_D.E'
    end
    object taPItem_dS: TFloatField
      FieldName = 'S'
      Origin = 'DOC_PROTOCOL_D.S'
    end
    object taPItem_dI: TFloatField
      FieldName = 'I'
      Origin = 'DOC_PROTOCOL_D.I'
    end
    object taPItem_dH: TFloatField
      FieldName = 'H'
      Origin = 'DOC_PROTOCOL_D.H'
    end
    object taPItem_dDONEW: TFloatField
      FieldName = 'DONEW'
      Origin = 'DOC_PROTOCOL_D.DONEW'
    end
    object taPItem_dRW: TFloatField
      FieldName = 'RW'
      Origin = 'DOC_PROTOCOL_D.RW'
    end
    object taPItem_dU: TFloatField
      FieldName = 'U'
      Origin = 'DOC_PROTOCOL_D.U'
    end
  end
  object frPItem_d: TfrDBDataSet
    DataSet = taPItem_d
    Left = 168
    Top = 252
  end
  object taOb_d: TpFIBDataSet
    SelectSQL.Strings = (
      'select FIO, H, I, S, BD, DOCDATE, WZ'
      'from Doc_Ob_d(:ACTID)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 224
    Top = 204
    object taOb_dFIO: TFIBStringField
      FieldName = 'FIO'
      Origin = 'DOC_OB.FIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taOb_dH: TFloatField
      FieldName = 'H'
      Origin = 'DOC_OB.H'
    end
    object taOb_dI: TFloatField
      FieldName = 'I'
      Origin = 'DOC_OB.I'
    end
    object taOb_dS: TFloatField
      FieldName = 'S'
      Origin = 'DOC_OB.S'
    end
    object taOb_dBD: TDateTimeField
      FieldName = 'BD'
      Origin = 'DOC_OB.BD'
    end
    object taOb_dDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'DOC_OB.DOCDATE'
    end
    object taOb_dWZ: TFloatField
      FieldName = 'WZ'
      Origin = 'DOC_OB.WZ'
    end
  end
  object frOb_d: TfrDBDataSet
    DataSet = taOb_d
    Left = 224
    Top = 252
  end
  object taDIELToPrint: TpFIBDataSet
    SelectSQL.Strings = (
      
        'SELECT ID, SEMIS, SEMISNAME, W, Q, UQ_NAME, UW_NAME, OPER_NAME, ' +
        'AEL_REF '
      'from SIEL_S(:INVID)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 752
    Top = 328
    object taDIELToPrintID: TIntegerField
      FieldName = 'ID'
      Origin = 'SIEL_S.ID'
    end
    object taDIELToPrintSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Origin = 'SIEL_S.SEMIS'
      Size = 10
      EmptyStrToNull = True
    end
    object taDIELToPrintSEMISNAME: TFIBStringField
      FieldName = 'SEMISNAME'
      Origin = 'SIEL_S.SEMISNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taDIELToPrintW: TFloatField
      FieldName = 'W'
      Origin = 'SIEL_S.W'
    end
    object taDIELToPrintQ: TIntegerField
      FieldName = 'Q'
      Origin = 'SIEL_S.Q'
    end
    object taDIELToPrintUQ_NAME: TFIBStringField
      FieldName = 'UQ_NAME'
      Origin = 'SIEL_S.UQ_NAME'
      EmptyStrToNull = True
    end
    object taDIELToPrintUW_NAME: TFIBStringField
      FieldName = 'UW_NAME'
      Origin = 'SIEL_S.UW_NAME'
      EmptyStrToNull = True
    end
    object taDIELToPrintOPER_NAME: TFIBStringField
      FieldName = 'OPER_NAME'
      Origin = 'SIEL_S.OPER_NAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taDIELToPrintAEL_REF: TIntegerField
      FieldName = 'AEL_REF'
      Origin = 'SIEL_S.AEL_REF'
    end
  end
  object frDIELToPrint: TfrDBDataSet
    DataSet = taDIELToPrint
    Left = 752
    Top = 372
  end
  object taAELToPrint: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT ART, ART2, SZ, OPERATION, Q, U '
      'from AEL_S(:AEL_REF )')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    DataSource = dsrDIel
    Left = 648
    Top = 356
    object taAELToPrintART: TFIBStringField
      FieldName = 'ART'
      Origin = 'AEL_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taAELToPrintART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'AEL_S.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taAELToPrintSZ: TFIBStringField
      FieldName = 'SZ'
      Origin = 'AEL_S.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taAELToPrintOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Origin = 'AEL_S.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAELToPrintQ: TIntegerField
      FieldName = 'Q'
      Origin = 'AEL_S.Q'
    end
    object taAELToPrintU: TSmallintField
      FieldName = 'U'
      Origin = 'AEL_S.U'
    end
  end
  object frAELToPrint: TfrDBDataSet
    DataSet = taAELToPrint
    Left = 648
    Top = 400
  end
  object taVedToPrint2_SUM: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '  SUPNAME,'
      '  OUTSUM,'
      '  RETSUM,'
      '  ACC_RETSUM,'
      '  ACC_INSUM,'
      '  ACC_OUTSUM,'
      '  SACC_RETSUM,'
      '  SACC_INSUM,'
      '  SACC_OUTSUM,'
      '  TRANS_SUM,'
      '  OUTW_SUM,'
      '  RETW_SUM'
      'from '
      '  DOC_VEDOMOST_2_SUM(:BD,:ED,:T0, :FILTER$MATERIAL)'
      'order by '
      '  SupName'
      '')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taVedToPrint2_SUMBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 812
    Top = 440
    object taVedToPrint2_SUMSUPNAME: TFIBStringField
      FieldName = 'SUPNAME'
      Origin = 'DOC_VEDOMOST_2_SUM.SUPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taVedToPrint2_SUMOUTSUM: TFloatField
      FieldName = 'OUTSUM'
      Origin = 'DOC_VEDOMOST_2_SUM.OUTSUM'
    end
    object taVedToPrint2_SUMRETSUM: TFloatField
      FieldName = 'RETSUM'
      Origin = 'DOC_VEDOMOST_2_SUM.RETSUM'
    end
    object taVedToPrint2_SUMACC_RETSUM: TFloatField
      FieldName = 'ACC_RETSUM'
      Origin = 'DOC_VEDOMOST_2_SUM.ACC_RETSUM'
    end
    object taVedToPrint2_SUMACC_INSUM: TFloatField
      FieldName = 'ACC_INSUM'
      Origin = 'DOC_VEDOMOST_2_SUM.ACC_INSUM'
    end
    object taVedToPrint2_SUMACC_OUTSUM: TFloatField
      FieldName = 'ACC_OUTSUM'
      Origin = 'DOC_VEDOMOST_2_SUM.ACC_OUTSUM'
    end
    object taVedToPrint2_SUMSACC_RETSUM: TFloatField
      FieldName = 'SACC_RETSUM'
      Origin = 'DOC_VEDOMOST_2_SUM.SACC_RETSUM'
    end
    object taVedToPrint2_SUMSACC_INSUM: TFloatField
      FieldName = 'SACC_INSUM'
      Origin = 'DOC_VEDOMOST_2_SUM.SACC_INSUM'
    end
    object taVedToPrint2_SUMSACC_OUTSUM: TFloatField
      FieldName = 'SACC_OUTSUM'
      Origin = 'DOC_VEDOMOST_2_SUM.SACC_OUTSUM'
    end
    object taVedToPrint2_SUMTRANS_SUM: TFIBFloatField
      FieldName = 'TRANS_SUM'
    end
    object taVedToPrint2_SUMOUTW_SUM: TFIBFloatField
      FieldName = 'OUTW_SUM'
    end
    object taVedToPrint2_SUMRETW_SUM: TFIBFloatField
      FieldName = 'RETW_SUM'
    end
  end
  object frVedToPrint2_Sum: TfrDBDataSet
    DataSet = taVedToPrint2_SUM
    Left = 816
    Top = 488
  end
  object dsrDIel: TDataSource
    DataSet = taDIELToPrint
    Left = 752
    Top = 420
  end
  object taSJInfo: TpFIBDataSet
    SelectSQL.Strings = (
      'select  ID, DOCNO, DOCDATE, USERID, BD, ISCLOSE, T, MATID'
      'from Act '
      'where ID=:PROTOCOLID')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taSJInfoBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 16
    Top = 508
    object taSJInfoID: TIntegerField
      FieldName = 'ID'
      Origin = 'ACT.ID'
      Required = True
    end
    object taSJInfoDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'ACT.DOCNO'
    end
    object taSJInfoDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'ACT.DOCDATE'
    end
    object taSJInfoUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'ACT.USERID'
      Required = True
    end
    object taSJInfoBD: TDateTimeField
      FieldName = 'BD'
      Origin = 'ACT.BD'
    end
    object taSJInfoISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'ACT.ISCLOSE'
    end
    object taSJInfoT: TSmallintField
      FieldName = 'T'
      Origin = 'ACT.T'
    end
    object taSJInfoMATID: TFIBStringField
      FieldName = 'MATID'
      Origin = 'ACT.MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
  end
  object frSJInfo: TfrDBDataSet
    DataSet = taSJInfo
    Left = 16
    Top = 552
  end
  object taSJ_PSOper: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select s.SEMIS SEMISNAME,  its.INW, its.INQ, its.FQ, its.FW,    ' +
        ' its.RQ, its.RW, its.UQ, its.UW, its.GETW, its.GETQ, its.DONEW, ' +
        '    its.DONEQ, its.RETW, its.RETQ, its.REJW, its.REJQ, its.REWOR' +
        'KW,'
      '  its.REWORKQ, its.NKW, its.NKQ, its.IQ, its.IW'
      'from AIt_InSemis its,  AItem it, D_Semis s'
      'where it.ACTID=:PROTOCOLID and'
      '           it.PSID=:PSID and'
      '           it.OPERID=:OPERID and'
      '           it.ID=its.PROTOCOLID and'
      '           its.SEMISID=s.SEMISID'
      'order by s.SEMIS'
      '')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taSJ_PSOperBeforeOpen
    Transaction = trPrint
    Database = dm.db
    SQLScreenCursor = crSQLWait
    Left = 72
    Top = 508
    oFetchAll = True
    object taSJ_PSOperSEMISNAME: TFIBStringField
      FieldName = 'SEMISNAME'
      Origin = 'D_SEMIS.SEMIS'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSJ_PSOperINW: TFloatField
      DefaultExpression = '0'
      FieldName = 'INW'
      Origin = 'AIT_INSEMIS.INW'
      Required = True
    end
    object taSJ_PSOperINQ: TIntegerField
      FieldName = 'INQ'
      Origin = 'AIT_INSEMIS.INQ'
      Required = True
    end
    object taSJ_PSOperFQ: TIntegerField
      FieldName = 'FQ'
      Origin = 'AIT_INSEMIS.FQ'
      Required = True
    end
    object taSJ_PSOperFW: TFloatField
      DefaultExpression = '0'
      FieldName = 'FW'
      Origin = 'AIT_INSEMIS.FW'
      Required = True
    end
    object taSJ_PSOperRQ: TIntegerField
      FieldName = 'RQ'
      Origin = 'AIT_INSEMIS.RQ'
      Required = True
    end
    object taSJ_PSOperRW: TFloatField
      DefaultExpression = '0'
      FieldName = 'RW'
      Origin = 'AIT_INSEMIS.RW'
      Required = True
    end
    object taSJ_PSOperUQ: TSmallintField
      FieldName = 'UQ'
      Origin = 'AIT_INSEMIS.UQ'
      Required = True
    end
    object taSJ_PSOperUW: TSmallintField
      FieldName = 'UW'
      Origin = 'AIT_INSEMIS.UW'
      Required = True
    end
    object taSJ_PSOperGETW: TFloatField
      DefaultExpression = '0'
      FieldName = 'GETW'
      Origin = 'AIT_INSEMIS.GETW'
      Required = True
    end
    object taSJ_PSOperGETQ: TIntegerField
      FieldName = 'GETQ'
      Origin = 'AIT_INSEMIS.GETQ'
      Required = True
    end
    object taSJ_PSOperDONEW: TFloatField
      DefaultExpression = '0'
      FieldName = 'DONEW'
      Origin = 'AIT_INSEMIS.DONEW'
      Required = True
    end
    object taSJ_PSOperDONEQ: TIntegerField
      FieldName = 'DONEQ'
      Origin = 'AIT_INSEMIS.DONEQ'
      Required = True
    end
    object taSJ_PSOperRETW: TFloatField
      DefaultExpression = '0'
      FieldName = 'RETW'
      Origin = 'AIT_INSEMIS.RETW'
      Required = True
    end
    object taSJ_PSOperRETQ: TIntegerField
      FieldName = 'RETQ'
      Origin = 'AIT_INSEMIS.RETQ'
      Required = True
    end
    object taSJ_PSOperREJW: TFloatField
      DefaultExpression = '0'
      FieldName = 'REJW'
      Origin = 'AIT_INSEMIS.REJW'
      Required = True
    end
    object taSJ_PSOperREJQ: TIntegerField
      FieldName = 'REJQ'
      Origin = 'AIT_INSEMIS.REJQ'
      Required = True
    end
    object taSJ_PSOperREWORKW: TFloatField
      DefaultExpression = '0'
      FieldName = 'REWORKW'
      Origin = 'AIT_INSEMIS.REWORKW'
      Required = True
    end
    object taSJ_PSOperREWORKQ: TIntegerField
      FieldName = 'REWORKQ'
      Origin = 'AIT_INSEMIS.REWORKQ'
      Required = True
    end
    object taSJ_PSOperNKW: TFloatField
      DefaultExpression = '0'
      FieldName = 'NKW'
      Origin = 'AIT_INSEMIS.NKW'
      Required = True
    end
    object taSJ_PSOperNKQ: TIntegerField
      FieldName = 'NKQ'
      Origin = 'AIT_INSEMIS.NKQ'
      Required = True
    end
    object taSJ_PSOperIQ: TIntegerField
      FieldName = 'IQ'
      Origin = 'AIT_INSEMIS.IQ'
      Required = True
    end
    object taSJ_PSOperIW: TFloatField
      DefaultExpression = '0'
      FieldName = 'IW'
      Origin = 'AIT_INSEMIS.IW'
      Required = True
    end
  end
  object frSJ_PSOper: TfrDBDataSet
    DataSet = taSJ_PSOper
    Left = 200
    Top = 552
  end
  object taProtPS: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct it.PSID, d.NAME DEPNAME '
      'from AItem it, D_Dep d'
      'where it.ACTID=:PROTOCOLID and '
      '      it.PSID=d.DEPID '
      'order by d.NAME'
      '        ')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taProtPSBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 136
    Top = 508
    object taProtPSPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'AITEM.PSID'
      Required = True
    end
    object taProtPSDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object frProtPS: TfrDBDataSet
    DataSet = taProtPS
    Left = 67
    Top = 552
  end
  object taProtPSOper: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.OPERID, o.OPERATION'
      'from AItem it, D_OPER o'
      'where it.ACTID=:PROTOCOLID and'
      '           it.PSID=:PSID and'
      '           it.OPERID=o.OPERID'
      'order by o.OPERATION')
    CacheModelOptions.BufferChunks = 1000
    AfterOpen = taProtPSOperAfterOpen
    AfterScroll = taProtPSOperAfterScroll
    BeforeOpen = taProtPSOperBeforeOpen
    Transaction = trPrint
    Database = dm.db
    DataSource = dsrProtPS
    Left = 200
    Top = 508
    oFetchAll = True
    object taProtPSOperOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AITEM.OPERID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taProtPSOperOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Origin = 'D_OPER.OPERATION'
      Required = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsrProtPS: TDataSource
    DataSet = taProtPS
    Left = 12
    Top = 600
  end
  object frProtPSOper: TfrDBDataSet
    DataSet = taProtPSOper
    Left = 128
    Top = 552
  end
  object taSJ_OperT: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  it.F1 F, '
      '  it.DONEW1 DONE_GOLD,  '
      '  it.N1 N, '
      '  it.S1 S, '
      '  it.U,'
      '  sum(its.INQ) INQ, '
      '  sum(its.INW) INW,'
      '     /*it.INW, it.INQ,*/'
      '  sum(its.FQ) RQ, '
      '  sum(its.FW) RW,'
      '     /* it.RQ, it.RW, */'
      '  sum(its.GETW) GETW, '
      '  sum(its.GETQ) GETQ, '
      '  sum(its.DONEW) DONEW, '
      '  sum(its.DONEQ) DONEQ, '
      '  sum(its.RETW) RETW, '
      '  sum(its.RETQ) RETQ, '
      '  sum(its.REJW) REJW, '
      '  sum(its.REJQ) REJQ,'
      '  sum(its.REWORKW) REWORKW, '
      '  sum(its.REWORKQ) REWORKQ,'
      '  sum(its.NKW) NKW, '
      '  sum(its.NKQ) NKQ,'
      '  sum(its.IQ) IQ, '
      '  sum(its.IW) IW, '
      '  m.NAME MATNAME, '
      '  m.ID MATID,'
      '  sum(its.RQ) DONE_MAT_RQ, '
      '  sum(its.RW) DONE_MAT_RW,'
      '  sum(its.N2) LossesNorm '
      'from '
      '  AItem it, '
      '  AIt_InSemis its, '
      '  D_Semis s, '
      '  D_Mat m'
      'where '
      '  it.ACTID=:PROTOCOLID and'
      '  it.PSID=:PSID and'
      '  it.OPERID=:OPERID and'
      '  it.ID=its.PROTOCOLID and'
      '  its.SEMISID=s.SEMISID and'
      '  s.MAT=m.ID '
      'group by '
      '  it.F1, '
      '  it.DONEW1, '
      '  it.N1, '
      '  it.S1, '
      '     /*it.RQ, it.RW,*/ '
      '  it.U, '
      '     /*it.INW, it.INQ,*/ '
      '  m.NAME, '
      '  m.ID')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taSJ_OperTBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 256
    Top = 508
    object taSJ_OperTF: TFloatField
      FieldName = 'F'
    end
    object taSJ_OperTN: TFloatField
      FieldName = 'N'
    end
    object taSJ_OperTS: TFloatField
      FieldName = 'S'
    end
    object taSJ_OperTU: TFloatField
      FieldName = 'U'
    end
    object taSJ_OperTINW: TFloatField
      FieldName = 'INW'
    end
    object taSJ_OperTGETW: TFloatField
      FieldName = 'GETW'
    end
    object taSJ_OperTDONEW: TFloatField
      FieldName = 'DONEW'
    end
    object taSJ_OperTRETW: TFloatField
      FieldName = 'RETW'
    end
    object taSJ_OperTREJW: TFloatField
      FieldName = 'REJW'
    end
    object taSJ_OperTREWORKW: TFloatField
      FieldName = 'REWORKW'
    end
    object taSJ_OperTNKW: TFloatField
      FieldName = 'NKW'
    end
    object taSJ_OperTIW: TFloatField
      FieldName = 'IW'
    end
    object taSJ_OperTMATNAME: TFIBStringField
      FieldName = 'MATNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSJ_OperTMATID: TFIBStringField
      FieldName = 'MATID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSJ_OperTDONE_GOLD: TFloatField
      FieldName = 'DONE_GOLD'
    end
    object taSJ_OperTRW: TFloatField
      FieldName = 'RW'
    end
    object taSJ_OperTDONE_MAT_RW: TFloatField
      FieldName = 'DONE_MAT_RW'
    end
    object taSJ_OperTINQ: TFIBBCDField
      FieldName = 'INQ'
      Size = 0
      RoundByScale = True
    end
    object taSJ_OperTRQ: TFIBBCDField
      FieldName = 'RQ'
      Size = 0
      RoundByScale = True
    end
    object taSJ_OperTGETQ: TFIBBCDField
      FieldName = 'GETQ'
      Size = 0
      RoundByScale = True
    end
    object taSJ_OperTDONEQ: TFIBBCDField
      FieldName = 'DONEQ'
      Size = 0
      RoundByScale = True
    end
    object taSJ_OperTRETQ: TFIBBCDField
      FieldName = 'RETQ'
      Size = 0
      RoundByScale = True
    end
    object taSJ_OperTREJQ: TFIBBCDField
      FieldName = 'REJQ'
      Size = 0
      RoundByScale = True
    end
    object taSJ_OperTREWORKQ: TFIBBCDField
      FieldName = 'REWORKQ'
      Size = 0
      RoundByScale = True
    end
    object taSJ_OperTNKQ: TFIBBCDField
      FieldName = 'NKQ'
      Size = 0
      RoundByScale = True
    end
    object taSJ_OperTIQ: TFIBBCDField
      FieldName = 'IQ'
      Size = 0
      RoundByScale = True
    end
    object taSJ_OperTDONE_MAT_RQ: TFIBBCDField
      FieldName = 'DONE_MAT_RQ'
      Size = 0
      RoundByScale = True
    end
    object taSJ_OperTLOSSESNORM: TFIBFloatField
      FieldName = 'LOSSESNORM'
    end
  end
  object frSJ_OperT: TfrDBDataSet
    DataSet = taSJ_OperT
    Left = 263
    Top = 552
  end
  object taSJ_PST: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  F, '
      '  N, '
      '  S, '
      '  RQ, '
      '  RW, '
      '  U, '
      '  INW, '
      '  INQ,  '
      '  GETW, '
      '  GETQ, '
      '  DONEW, '
      '  DONEQ,  '
      '  RETW, '
      '  RETQ,  '
      '  REJW, '
      '  REJQ,'
      '  REWORKW, '
      '  REWORKQ, '
      '  NKW, '
      '  NKQ, '
      '  IQ, '
      '  IW, '
      '  MATID, '
      '  MATNAME,'
      '  DONE_GOLD, '
      '  DONE_MAT_RQ, '
      '  DONE_MAT_RW, '
      '  N2'
      'from '
      '  Doc_SJ_PST(:PROTOCOLID, :PSID)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taSJ_PSTBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 328
    Top = 508
    object taSJ_PSTF: TFloatField
      FieldName = 'F'
    end
    object taSJ_PSTN: TFloatField
      FieldName = 'N'
    end
    object taSJ_PSTS: TFloatField
      FieldName = 'S'
    end
    object taSJ_PSTRQ: TIntegerField
      FieldName = 'RQ'
    end
    object taSJ_PSTRW: TFloatField
      FieldName = 'RW'
    end
    object taSJ_PSTU: TFloatField
      FieldName = 'U'
    end
    object taSJ_PSTINW: TFloatField
      FieldName = 'INW'
    end
    object taSJ_PSTINQ: TIntegerField
      FieldName = 'INQ'
    end
    object taSJ_PSTGETW: TFloatField
      FieldName = 'GETW'
    end
    object taSJ_PSTGETQ: TIntegerField
      FieldName = 'GETQ'
    end
    object taSJ_PSTDONEW: TFloatField
      FieldName = 'DONEW'
    end
    object taSJ_PSTDONEQ: TIntegerField
      FieldName = 'DONEQ'
    end
    object taSJ_PSTRETW: TFloatField
      FieldName = 'RETW'
    end
    object taSJ_PSTRETQ: TIntegerField
      FieldName = 'RETQ'
    end
    object taSJ_PSTREJW: TFloatField
      FieldName = 'REJW'
    end
    object taSJ_PSTREJQ: TIntegerField
      FieldName = 'REJQ'
    end
    object taSJ_PSTREWORKW: TFloatField
      FieldName = 'REWORKW'
    end
    object taSJ_PSTREWORKQ: TIntegerField
      FieldName = 'REWORKQ'
    end
    object taSJ_PSTNKW: TFloatField
      FieldName = 'NKW'
    end
    object taSJ_PSTNKQ: TIntegerField
      FieldName = 'NKQ'
    end
    object taSJ_PSTIQ: TIntegerField
      FieldName = 'IQ'
    end
    object taSJ_PSTIW: TFloatField
      FieldName = 'IW'
    end
    object taSJ_PSTMATID: TFIBStringField
      FieldName = 'MATID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSJ_PSTMATNAME: TFIBStringField
      FieldName = 'MATNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSJ_PSTDONE_GOLD: TFloatField
      FieldName = 'DONE_GOLD'
      Origin = 'DOC_SJ_PST.DONE_GOLD'
    end
    object taSJ_PSTDONE_MAT_RQ: TFloatField
      FieldName = 'DONE_MAT_RQ'
      Origin = 'DOC_SJ_PST.DONE_MAT_RQ'
    end
    object taSJ_PSTDONE_MAT_RW: TFloatField
      FieldName = 'DONE_MAT_RW'
      Origin = 'DOC_SJ_PST.DONE_MAT_RW'
    end
    object taSJ_PSTN2: TFIBFloatField
      FieldName = 'N2'
    end
  end
  object frSJ_PST: TfrDBDataSet
    DataSet = taSJ_PST
    Left = 328
    Top = 552
  end
  object dsrProtPSOper: TDataSource
    DataSet = taProtPSOper
    Left = 64
    Top = 600
  end
  object taSOelToPrint: TpFIBDataSet
    SelectSQL.Strings = (
      'select * '
      'from DOC_SOEL(:INVID)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 576
    Top = 356
  end
  object frSoelToPrint: TfrDBDataSet
    DataSet = taSOelToPrint
    Left = 576
    Top = 404
  end
  object taSJ_PSTails: TpFIBDataSet
    SelectSQL.Strings = (
      'select sum(a.N) N, t.NAME TAILNAME'
      'from ActTails a, D_Tails t'
      'where a.PSID=:PSID and'
      '           a.TAILS=t.ID and'
      '           a.ACTID=:PROTOCOLID'
      'group by t.NAME, a.PSID')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taSJ_PSTailsBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 392
    Top = 508
    object taSJ_PSTailsN: TFloatField
      FieldName = 'N'
    end
    object taSJ_PSTailsTAILNAME: TFIBStringField
      FieldName = 'TAILNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object frSJ_PSTails: TfrDBDataSet
    DataSet = taSJ_PSTails
    Left = 388
    Top = 552
  end
  object taSemisReport: TpFIBDataSet
    SelectSQL.Strings = (
      'select a.ID, a.DOCNO, a.DOCDATE, a.BD, a.ISCLOSE,'
      '    a.USERID, a.MATID, a.T, a.DEPID, d.NAME DEPNAME'
      'from Act a, D_Dep d'
      'where a.DEPID=d.DEPID and'
      '          a.ID=:ACTID'
      '')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 512
    Top = 452
  end
  object frSemisReport: TfrDBDataSet
    DataSet = taSemisReport
    Left = 512
    Top = 500
  end
  object taProtTails_d: TpFIBDataSet
    SelectSQL.Strings = (
      'select sum(N) N, t.NAME TAILNAME'
      'from ActTails it, D_Tails t'
      'where it.ACTID=:PROTOCOLID and          '
      '           it.TAILS=t.ID'
      'group by t.NAME'
      '')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taProtTails_dBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 284
    Top = 204
    object taProtTails_dTAILNAME: TFIBStringField
      FieldName = 'TAILNAME'
      Origin = 'G_TAILS_F.TAILNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taProtTails_dN: TFloatField
      FieldName = 'N'
      Origin = 'G_TAILS_F.N'
    end
  end
  object frProtTails_d: TfrDBDataSet
    DataSet = taProtTails_d
    Left = 284
    Top = 252
  end
  object taSRItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from SEMIS_REPORT_S(:ACTID)'
      'where SEMIS between :SEMIS1 and :SEMIS2  and'
      '           OPERATION between :OPER1 and :OPER2 and'
      '           MATID between :MATID1 and :MATID2 '
      'order by SEMIS, OPERATION')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taSRItemBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 572
    Top = 452
  end
  object frSRItem: TfrDBDataSet
    DataSet = taSRItem
    Left = 572
    Top = 500
  end
  object taVList: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select  a.ID, a.DOCNO, a.DOCDATE, a.USERID, a.BD, a.ISCLOSE, a.T' +
        ',      a.MATID, m.FIO USERNAME, a.DEPID, d.NAME DEPNAME'
      'from Act a, D_MOL m, D_Dep d'
      'where a.ID=:ACTID and'
      '           a.USERID=m.MOLID and'
      '           a.DEPID=d.DEPID '
      'order by a.DOCNO')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 292
    Top = 308
  end
  object taVSheet: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      'ID, '
      'REST_IN_Q, REST_IN_W,'
      'IN_Q, IN_W, '
      'IN_MOVE_Q, IN_MOVE_W,'
      'IN_PS_Q, IN_PS_W,'
      'OUT_Q, OUT_W,'
      'OUT_MOVE_Q, OUT_MOVE_W,'
      'OUT_PS_Q, OUT_PS_W,'
      'REST_OUT_Q, REST_OUT_W,'
      'UQ_NAME, UW_NAME,'
      'OPERATION, SEMIS, DEPNAME , DEPID, SEMISID, OPERID, UQ, UW,'
      '  FQ, FW, RQ, RW, Semis$Price'
      'from SEMIS_REPORT_S(:ACTID)'
      'where SEMIS between :SEMIS1 and :SEMIS2  and'
      '           OPERATION between :OPER1 and :OPER2 and'
      '           MATID between :MATID1 and :MATID2 '
      'order by OPERATION, SEMIS ')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taVSheetBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 348
    Top = 308
    object taVSheetID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taVSheetREST_IN_Q: TFIBIntegerField
      FieldName = 'REST_IN_Q'
    end
    object taVSheetREST_IN_W: TFIBFloatField
      FieldName = 'REST_IN_W'
    end
    object taVSheetIN_Q: TFIBIntegerField
      FieldName = 'IN_Q'
    end
    object taVSheetIN_W: TFIBFloatField
      FieldName = 'IN_W'
    end
    object taVSheetIN_MOVE_Q: TFIBIntegerField
      FieldName = 'IN_MOVE_Q'
    end
    object taVSheetIN_MOVE_W: TFIBFloatField
      FieldName = 'IN_MOVE_W'
    end
    object taVSheetIN_PS_Q: TFIBIntegerField
      FieldName = 'IN_PS_Q'
    end
    object taVSheetIN_PS_W: TFIBFloatField
      FieldName = 'IN_PS_W'
    end
    object taVSheetOUT_Q: TFIBIntegerField
      FieldName = 'OUT_Q'
    end
    object taVSheetOUT_W: TFIBFloatField
      FieldName = 'OUT_W'
    end
    object taVSheetOUT_MOVE_Q: TFIBIntegerField
      FieldName = 'OUT_MOVE_Q'
    end
    object taVSheetOUT_MOVE_W: TFIBFloatField
      FieldName = 'OUT_MOVE_W'
    end
    object taVSheetOUT_PS_Q: TFIBIntegerField
      FieldName = 'OUT_PS_Q'
    end
    object taVSheetOUT_PS_W: TFIBFloatField
      FieldName = 'OUT_PS_W'
    end
    object taVSheetREST_OUT_Q: TFIBIntegerField
      FieldName = 'REST_OUT_Q'
    end
    object taVSheetREST_OUT_W: TFIBFloatField
      FieldName = 'REST_OUT_W'
    end
    object taVSheetUQ_NAME: TFIBStringField
      FieldName = 'UQ_NAME'
      Size = 10
      EmptyStrToNull = True
    end
    object taVSheetUW_NAME: TFIBStringField
      FieldName = 'UW_NAME'
      Size = 10
      EmptyStrToNull = True
    end
    object taVSheetOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = True
    end
    object taVSheetSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Size = 40
      EmptyStrToNull = True
    end
    object taVSheetDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taVSheetDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taVSheetSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Size = 10
      EmptyStrToNull = True
    end
    object taVSheetOPERID: TFIBStringField
      FieldName = 'OPERID'
      Size = 10
      EmptyStrToNull = True
    end
    object taVSheetUQ: TFIBSmallIntField
      FieldName = 'UQ'
    end
    object taVSheetUW: TFIBSmallIntField
      FieldName = 'UW'
    end
    object taVSheetFQ: TFIBFloatField
      FieldName = 'FQ'
    end
    object taVSheetFW: TFIBFloatField
      FieldName = 'FW'
    end
    object taVSheetRQ: TFIBFloatField
      FieldName = 'RQ'
    end
    object taVSheetRW: TFIBFloatField
      FieldName = 'RW'
    end
    object taVSheetSEMISPRICE: TFIBFloatField
      FieldName = 'SEMIS$PRICE'
    end
  end
  object frVList: TfrDBDataSet
    DataSet = taVList
    Left = 292
    Top = 356
  end
  object frVSheet: TfrDBDataSet
    DataSet = taVSheet
    Left = 348
    Top = 356
  end
  object taVSheetMat: TpFIBDataSet
    SelectSQL.Strings = (
      'select MATID, MATNAME, REST_OUT_W, FW, RW, DETAIL, '
      '          REST_OUT_W_PURE, FW_PURE, RW_PURE '
      
        'from DOC_VSHEETMAT(:ACTID, :MATID1, :MATID2, :SEMISID1, :SEMISID' +
        '2, :OPERID1, :OPERID2)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taVSheetMatBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 408
    Top = 308
    object taVSheetMatMATID: TFIBStringField
      FieldName = 'MATID'
      Origin = 'DOC_VSHEETMAT.MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taVSheetMatMATNAME: TFIBStringField
      FieldName = 'MATNAME'
      Origin = 'DOC_VSHEETMAT.MATNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taVSheetMatREST_OUT_W: TFloatField
      FieldName = 'REST_OUT_W'
      Origin = 'DOC_VSHEETMAT.REST_OUT_W'
    end
    object taVSheetMatFW: TFloatField
      FieldName = 'FW'
      Origin = 'DOC_VSHEETMAT.FW'
    end
    object taVSheetMatRW: TFloatField
      FieldName = 'RW'
      Origin = 'DOC_VSHEETMAT.RW'
    end
    object taVSheetMatDETAIL: TSmallintField
      FieldName = 'DETAIL'
      Origin = 'DOC_VSHEETMAT.DETAIL'
    end
    object taVSheetMatREST_OUT_W_PURE: TFloatField
      FieldName = 'REST_OUT_W_PURE'
      Origin = 'DOC_VSHEETMAT.REST_OUT_W_PURE'
    end
    object taVSheetMatFW_PURE: TFloatField
      FieldName = 'FW_PURE'
      Origin = 'DOC_VSHEETMAT.FW_PURE'
    end
    object taVSheetMatRW_PURE: TFloatField
      FieldName = 'RW_PURE'
      Origin = 'DOC_VSHEETMAT.RW_PURE'
    end
  end
  object frVSheetMat: TfrDBDataSet
    DataSet = taVSheetMat
    Left = 408
    Top = 355
  end
  object taVSheetMat1: TpFIBDataSet
    SelectSQL.Strings = (
      'select MATID, MATNAME, DETAIL, '
      '          REST_IN_W, IN_W, IN_MOVE_W, OUT_W,'
      '          OUT_MOVE_W,  IN_PS_W, OUT_PS_W, REST_OUT_W,'
      '          REST_IN_Q, IN_Q, IN_MOVE_Q, OUT_Q,'
      '          OUT_MOVE_Q,  IN_PS_Q, OUT_PS_Q, REST_OUT_Q,'
      '          BULK_Q, BULK_W, AFF_S_Q, AFF_S_W,'
      '          AFF_L_GET_Q, AFF_L_GET_W, AFF_L_RET_Q, AFF_L_RET_W,'
      '          CHARGE_Q, CHARGE_W '
      
        'from DOC_VSHEETMAT1(:ACTID, :MATID1, :MATID2, :SEMISID1,   :SEMI' +
        'SID2, :OPERID1, :OPERID2)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taVSheetMat1BeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 708
    Top = 444
  end
  object frVSheetMat1: TfrDBDataSet
    DataSet = taVSheetMat1
    Left = 636
    Top = 500
  end
  object taInv16Assort: TpFIBDataSet
    SelectSQL.Strings = (
      'select a2.ART, a2.ART2, sz.NAME, e.Q'
      'from AEl e, Art2 a2, D_Sz sz'
      'where e.ART2ID=a2.ART2ID and'
      '           e.SZ=sz.ID and'
      '           e.INVID=:INVID'
      'order by a2.ART, a2.ART2, sz.NAME')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taInv16AssortBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 144
    Top = 56
    dcForceOpen = True
  end
  object frInv16Assort: TfrDBDataSet
    DataSet = taInv16Assort
    Left = 144
    Top = 104
  end
  object dsrInv16Item: TDataSource
    DataSet = quInv16Item
    Left = 76
    Top = 148
  end
  object frWhAppl: TfrDBDataSet
    DataSet = dmAppl.taWHAppl
    Left = 792
    Top = 8
  end
  object taProtocol_PS: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '   a.ID ACTID,'
      '   a.DOCNO,'
      '   a.DOCDATE,'
      '   d.DEPID PSID,'
      '   d.NAME PSNAME'
      'from Act a, D_Dep d'
      'where'
      '   a.ID = :ACTID and'
      '   d.DEPID = :PSID')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taProtocol_PSBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 960
    Top = 392
    dcForceOpen = True
    oFetchAll = True
    object taProtocol_PSACTID: TIntegerField
      FieldName = 'ACTID'
      Origin = 'ACT.ID'
      Required = True
    end
    object taProtocol_PSDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'ACT.DOCNO'
      Required = True
    end
    object taProtocol_PSDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'ACT.DOCDATE'
      Required = True
    end
    object taProtocol_PSPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'AITEM.PSID'
      Required = True
    end
    object taProtocol_PSPSNAME: TFIBStringField
      FieldName = 'PSNAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object frProtocol_PS: TfrDBDataSet
    DataSet = taProtocol_PS
    Left = 1024
    Top = 388
  end
  object taPItem_PS: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  s.SEMIS SEMISNAME,'
      '  ait.FQ,'
      '  ait.FW,'
      '  cast(1 as integer) g'
      'from'
      '  AItem ai,'
      '  AIt_InSemis ait,'
      '  D_Semis s'
      'where'
      '   ai.ActID = :ActID and'
      '   ai.PSID = :PSID and'
      '   ai.OPERID = :OPERID and'
      '   ait.PROTOCOLID = ai.ID and                               '
      '   s.SEMISID = ait.SEMISID and'
      '   s.mat = :MATID and'
      '   not (ait.FQ=0 and equal(ait.FW, 0, 0.001)=1)'
      ''
      ''
      ''
      'union'
      ''
      'select'
      '   cast('#39#1057#1098#1077#1084#39' as char(40)) SEMISNAME, '
      '   cast(0 as integer) FQ,'
      '   cast(l.weight as DOUBLE PRECISION) FW,'
      '   cast(2 as integer) g'
      'from d_oper o, affinage$losses l'
      'where'
      '   o.operid = :OPERID and'
      '   l.passport$id = - current_connection and'
      '   l.operation$id = o.id and'
      '   l.place$of$storage$id = :PSID'
      '   '
      'order by 4, 1'
      ''
      '   ')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taPItem_PSBeforeOpen
    Transaction = trPrint
    Database = dm.db
    DataSource = dsrPOper_PS
    Left = 960
    Top = 208
    dcForceOpen = True
    oFetchAll = True
  end
  object frPItem_PS: TfrDBDataSet
    DataSet = taPItem_PS
    OpenDataSource = False
    Left = 1032
    Top = 208
  end
  object taPArt_PS: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select p.U, p.FQ, a2.ART, a2.ART2, o.OPERATION OPERNAME,        ' +
        '    sz.NAME SZNAME '
      'from PArt p, Art2 a2, D_Sz sz, D_Oper o '
      'where p.ACTID=:ACTID and'
      '           p.PSID=:PSID and '
      '           p.OPERID =o.OPERID and     '
      '           p.ART2ID=a2.ART2ID and'
      '           p.SZID=sz.ID and'
      '           p.FQ <> 0'
      'order by a2.ART, a2.ART2, sz.NAME, o.SORTIND')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taPArt_PSBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 1020
    Top = 464
    oFetchAll = True
    object taPArt_PSU: TSmallintField
      FieldName = 'U'
      Origin = 'PART.U'
      Required = True
    end
    object taPArt_PSFQ: TIntegerField
      FieldName = 'FQ'
      Origin = 'PART.FQ'
      Required = True
    end
    object taPArt_PSART: TFIBStringField
      FieldName = 'ART'
      Origin = 'ART2.ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taPArt_PSART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taPArt_PSOPERNAME: TFIBStringField
      FieldName = 'OPERNAME'
      Origin = 'D_OPER.OPERATION'
      Required = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taPArt_PSSZNAME: TFIBStringField
      FieldName = 'SZNAME'
      Origin = 'D_SZ.NAME'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
  end
  object frPArt_PS: TfrDBDataSet
    DataSet = taPArt_PS
    Left = 1020
    Top = 512
  end
  object dsrProtocol_PS: TDataSource
    DataSet = taProtocol_PS
    Left = 1088
    Top = 397
  end
  object frDictArt: TfrDBDataSet
    DataSet = dm.taArt
    Left = 844
    Top = 8
  end
  object frAffInv: TfrDBDataSet
    DataSet = taAffInv
    Left = 276
    Top = 656
  end
  object frAffinaj: TfrDBDataSet
    DataSet = taAffinaj
    Left = 328
    Top = 656
  end
  object taBulk: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.INVID, i.DOCNO, i.DOCDATE, i.Q, i.W, d.NAME DEPNAME'
      'from Inv i, D_Dep d'
      'where i.INVID=:INVID and'
      '           i.DEPID=d.DEPID')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 16
    Top = 644
    object taBulkINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'INV.INVID'
      Required = True
    end
    object taBulkDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'INV.DOCNO'
    end
    object taBulkDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'INV.DOCDATE'
    end
    object taBulkQ: TFloatField
      FieldName = 'Q'
      Origin = 'INV.Q'
    end
    object taBulkW: TFloatField
      FieldName = 'W'
      Origin = 'INV.W'
    end
    object taBulkDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object taBulkItem: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select se.ID, se.INVID, se.SEMIS,  se.Q, se.W, se.TPRICE, se.PRI' +
        'CE,                  se.T, se.COST, se.OPERID, se.Q0, se.W0, s.S' +
        'EMIS SEMISNAME'
      'from SIEl se, D_Semis s'
      'where se.INVID=:INVID and'
      '           se.SEMIS=s.SEMISID '
      'order by s.SORTIND')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taBulkItemBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 56
    Top = 644
  end
  object dsrBulk: TDataSource
    DataSet = taBulk
    Left = 16
    Top = 740
  end
  object frBulk: TfrDBDataSet
    DataSet = taBulk
    Left = 16
    Top = 692
  end
  object frBulkItem: TfrDBDataSet
    DataSet = taBulkItem
    Left = 56
    Top = 692
  end
  object taKolieReport: TpFIBDataSet
    SelectSQL.Strings = (
      'select  ART,'
      '           LENGTH_,'
      '           Q,'
      '           BD,'
      '           ED'
      'from  REPORT_KOLIE(:BDATE,:EDATE)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taKolieReportBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 728
    Top = 480
  end
  object frKolieReport: TfrDBDataSet
    DataSet = taKolieReport
    Left = 728
    Top = 528
  end
  object taInvChangeReport: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select INVID, DOCDATE, DOCNO, DEPNAME, SUPNAME, FROMDEPNAME, Q, ' +
        'W, FULLSUM, ITYPENAME'
      'from INV_CHANGE_REPORT(:ADATE)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 728
    Top = 576
    object taInvChangeReportINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'INV_CHANGE_REPORT.INVID'
    end
    object taInvChangeReportDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'INV_CHANGE_REPORT.DOCDATE'
    end
    object taInvChangeReportDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'INV_CHANGE_REPORT.DOCNO'
    end
    object taInvChangeReportDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Origin = 'INV_CHANGE_REPORT.DEPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taInvChangeReportSUPNAME: TFIBStringField
      FieldName = 'SUPNAME'
      Origin = 'INV_CHANGE_REPORT.SUPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taInvChangeReportFROMDEPNAME: TFIBStringField
      FieldName = 'FROMDEPNAME'
      Origin = 'INV_CHANGE_REPORT.FROMDEPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taInvChangeReportQ: TIntegerField
      FieldName = 'Q'
      Origin = 'INV_CHANGE_REPORT.Q'
    end
    object taInvChangeReportW: TFloatField
      FieldName = 'W'
      Origin = 'INV_CHANGE_REPORT.W'
    end
    object taInvChangeReportFULLSUM: TFloatField
      FieldName = 'FULLSUM'
      Origin = 'INV_CHANGE_REPORT.FULLSUM'
    end
    object taInvChangeReportITYPENAME: TFIBStringField
      FieldName = 'ITYPENAME'
      Origin = 'INV_CHANGE_REPORT.ITYPENAME'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object frInvChangeReport: TfrDBDataSet
    DataSet = taInvChangeReport
    Left = 728
    Top = 624
  end
  object taDelInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select  DOCNO, DOCDATE, INVID, UPD from DEL_INV')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 656
    Top = 576
    object taDelInvDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'DEL_INV.DOCNO'
    end
    object taDelInvDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'DEL_INV.DOCDATE'
    end
    object taDelInvINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'DEL_INV.INVID'
    end
    object taDelInvUPD: TSmallintField
      FieldName = 'UPD'
      Origin = 'DEL_INV.UPD'
    end
  end
  object frDelInv: TfrDBDataSet
    DataSet = taDelInv
    Left = 656
    Top = 624
  end
  object taAffInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.INVID, i.DOCNO, i.DOCDATE, i.Q, i.W, d.NAME DEPNAME,'
      '          c.NAME COMPNAME, sup.NAME SUPNAME '
      'from Inv i left join D_Dep d on (i.DEPID=d.DEPID)'
      '               left join D_Comp c on (i.COMPID=c.COMPID) '
      '               left join D_Comp sup on (i.SUPID=sup.COMPID)'
      'where i.INVID=:INVID'
      ''
      '')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 276
    Top = 608
    object taAffInvINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'INV.INVID'
      Required = True
    end
    object taAffInvDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'INV.DOCNO'
    end
    object taAffInvDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'INV.DOCDATE'
    end
    object taAffInvQ: TFloatField
      FieldName = 'Q'
      Origin = 'INV.Q'
    end
    object taAffInvW: TFloatField
      FieldName = 'W'
      Origin = 'INV.W'
    end
    object taAffInvDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taAffInvCOMPNAME: TFIBStringField
      FieldName = 'COMPNAME'
      Origin = 'D_COMP.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAffInvSUPNAME: TFIBStringField
      FieldName = 'SUPNAME'
      Origin = 'D_COMP.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
  end
  object taAffinaj: TpFIBDataSet
    SelectSQL.Strings = (
      'select SEMISNAME, Q, W, DEPNAME, UQ, UW, SORTIND'
      'from Doc_Affinaj(:INVID, :T)'
      'order by SORTIND'
      '')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 320
    Top = 600
    object taAffinajSEMISNAME: TFIBStringField
      FieldName = 'SEMISNAME'
      Origin = 'DOC_AFFINAJ.SEMISNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAffinajQ: TIntegerField
      FieldName = 'Q'
      Origin = 'DOC_AFFINAJ.Q'
    end
    object taAffinajW: TFloatField
      FieldName = 'W'
      Origin = 'DOC_AFFINAJ.W'
    end
    object taAffinajDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Origin = 'DOC_AFFINAJ.DEPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAffinajUQ: TSmallintField
      FieldName = 'UQ'
      Origin = 'DOC_AFFINAJ.UQ'
      OnGetText = UQGetText
    end
    object taAffinajUW: TSmallintField
      FieldName = 'UW'
      Origin = 'DOC_AFFINAJ.UW'
      OnGetText = UWGetText
    end
    object taAffinajSORTIND: TIntegerField
      FieldName = 'SORTIND'
      Origin = 'DOC_AFFINAJ.SORTIND'
    end
  end
  object dsrAffInv: TDataSource
    DataSet = taAffInv
    Left = 276
    Top = 704
  end
  object frTextExport1: TfrTextExport
    ShowDialog = False
    ScaleX = 1.000000000000000000
    ScaleY = 1.000000000000000000
    KillEmptyLines = False
    ConvertToOEM = True
    UsePseudographic = True
    PageBreaks = False
    Left = 728
    Top = 56
  end
  object frOperOrder: TfrDBDataSet
    DataSet = dm.taOper
    Left = 912
    Top = 8
  end
  object frAssortDet: TfrDBDataSet
    DataSet = dmMain.taVArt
    Left = 912
    Top = 56
  end
  object frArtIns: TfrDBDataSet
    DataSet = dm.taArt
    Left = 912
    Top = 104
  end
  object taStoreByItems: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select p.UID, p.ART, p.ART2, p.W, p.CURPRICE, p.COST, p.SZ, p.SE' +
        'LFCOMPID, s.Sname'
      'from TMP_PROD_STORE p, art2 a2, d_semis s, d_mat m'
      'where p.STATE=0 and'
      '      coalesce(p.USERID,0)=0 and'
      '      p.SELFCOMPID between :SELFCOMPID1 and :SELFCOMPID2 and'
      '      a2.art2id = p.art2id and'
      '      s.semisid = a2.semis and'
      '      s.mat between :MAT1 and :MAT2 and'
      '      m.id = s.mat'
      'order by p.ART, p.SZ, p.ART2, p.W')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 400
    Top = 152
    object taStoreByItemsUID: TIntegerField
      FieldName = 'UID'
      Origin = 'TMP_PROD_STORE.UID'
      Required = True
    end
    object taStoreByItemsART: TFIBStringField
      FieldName = 'ART'
      Origin = 'TMP_PROD_STORE.ART'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taStoreByItemsART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'TMP_PROD_STORE.ART2'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taStoreByItemsW: TFloatField
      FieldName = 'W'
      Origin = 'TMP_PROD_STORE.W'
    end
    object taStoreByItemsCURPRICE: TFloatField
      FieldName = 'CURPRICE'
      Origin = 'TMP_PROD_STORE.CURPRICE'
    end
    object taStoreByItemsCOST: TFloatField
      FieldName = 'COST'
      Origin = 'TMP_PROD_STORE.COST'
    end
    object taStoreByItemsSZ: TFIBStringField
      FieldName = 'SZ'
      Size = 5
      EmptyStrToNull = True
    end
    object taStoreByItemsSELFCOMPID: TFIBIntegerField
      FieldName = 'SELFCOMPID'
    end
    object taStoreByItemsSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
  object frStoreByItems: TfrDBDataSet
    DataSet = taStoreByItems
    Left = 384
    Top = 208
  end
  object taStoneInInv: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select sum(ai.Quantity) Q, sum(ai.Weight) W, s.SEMIS, s.UQ, s.UW' +
        '/*, a2.D_CompId, c.Name COMPOWNER*/'
      
        'from Inv i, SEl e, SItem it, D_Art a, AIns ai, D_Semis s, Art2 a' +
        '2, D_Comp c'
      'where i.INVID in (__INSERT_TEXT_THIS__) and           '
      '           i.InvId = e.InvId and'
      '          e.SelId = it.SelId and'
      '          e.D_ArtId = a.D_ArtId and'
      '          a.D_ArtId = ai.ArtId and'
      '          ai.InsId = s.SemisId and'
      '          e.Art2Id=a2.Art2Id and'
      '          a2.Art2='#39'-'#39' and'
      '          a2.D_CompId=c.CompId           '
      'group by s.SEMIS, s.UQ, s.UW/*, a2.D_CompId, c.Name*/'
      ''
      'union'
      ''
      
        'select sum(ai.Quantity) Q, sum(ai.Weight) W, s.SEMIS, s.UQ, s.UW' +
        '/*, ai.CompId, c.Name COMPOWNER*/'
      
        'from Inv i, SEl e, SItem it, D_Art a, Ins ai, D_Semis s, Art2 a2' +
        ', D_Comp c'
      'where i.INVID in (__INSERT_TEXT_THIS__) and           '
      '           i.InvId = e.InvId and'
      '          e.SelId = it.SelId and'
      '          e.D_ArtId = a.D_ArtId and'
      '          a2.Art2Id = ai.Art2Id and'
      '          ai.InsId = s.SemisId and'
      '          e.Art2Id = a2.Art2Id and'
      '          a2.Art2<>'#39'-'#39' and'
      '          ai.CompId=c.CompId   '
      'group by s.SEMIS, s.UQ, s.UW/*, ai.CompId, c.Name*/'
      ''
      '')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 1020
    Top = 8
    object taStoneInInvW: TFloatField
      FieldName = 'W'
    end
    object taStoneInInvSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taStoneInInvUQ: TSmallintField
      FieldName = 'UQ'
      Required = True
      OnGetText = UQGetText
    end
    object taStoneInInvUW: TSmallintField
      FieldName = 'UW'
      Required = True
      OnGetText = UWGetText
    end
    object taStoneInInvQ: TFIBBCDField
      FieldName = 'Q'
      Size = 0
      RoundByScale = True
    end
  end
  object frStoneInInv: TfrDBDataSet
    DataSet = taStoneInInv
    Left = 1020
    Top = 52
  end
  object frMatrixProd: TfrDBDataSet
    DataSet = dmMain.taMatrixProd
    Left = 656
    Top = 8
  end
  object taTagIns: TpFIBDataSet
    SelectSQL.Strings = (
      'select InsName'
      'from TagIns(:ART2ID)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 300
    Top = 8
  end
  object frB_UID: TfrDBDataSet
    DataSet = dmMain.taB_UID
    Left = 656
    Top = 56
  end
  object taSemisInInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(*) Q, sum(it.W) W, s.SEMIS'
      'from Inv i, SEl e, SItem it, Art2 a2, D_Semis s'
      'where i.INVID in (__INSERT_TEXT_THIS__) and           '
      '           i.InvId = e.InvId and'
      '          e.SelId = it.SelId and'
      '          e.Art2Id=a2.Art2Id and'
      '          a2.Semis=s.SemisId'
      'group by s.SEMIS')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 284
    Top = 56
    object taSemisInInvQ: TIntegerField
      FieldName = 'Q'
      Required = True
    end
    object taSemisInInvW: TFloatField
      FieldName = 'W'
    end
    object taSemisInInvSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object frSemisInInv: TfrDBDataSet
    DataSet = taSemisInInv
    Left = 284
    Top = 100
  end
  object taSJ_T: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select m.Id MATID, m.Name MATNAME, it.UQ, it.UW, sum(it.RQ) Q, s' +
        'um(it.RW) W'
      '    from AIt_InSemis it, AItem e, D_Semis s, D_Mat m'
      '    where e.ActId=:PROTOCOLID and'
      '          e.Id = it.ProtocolId and'
      '          it.SemisId = s.SemisId and'
      '          s.Mat = m.Id and'
      '          m.Id <> '#39'1'#39' /* '#1047#1086#1083#1086#1090#1086' */'
      '    group by m.Id, m.Name, it.UQ, it.UW'
      '')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taSJ_TBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 448
    Top = 436
    object taSJ_TMATID: TFIBStringField
      FieldName = 'MATID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSJ_TMATNAME: TFIBStringField
      FieldName = 'MATNAME'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSJ_TUQ: TSmallintField
      FieldName = 'UQ'
      Required = True
      OnGetText = UQGetText
    end
    object taSJ_TUW: TSmallintField
      FieldName = 'UW'
      Required = True
      OnGetText = UWGetText
    end
    object taSJ_TW: TFloatField
      FieldName = 'W'
    end
    object taSJ_TQ: TFIBBCDField
      FieldName = 'Q'
      Size = 0
      RoundByScale = True
    end
  end
  object frSJ_T: TfrDBDataSet
    DataSet = taSJ_T
    Left = 448
    Top = 480
  end
  object taSJFact_Mat: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID MATID, NAME MATNAME'
      'from D_Mat'
      'order by SORTIND')
    AfterOpen = taSJFact_MatAfterOpen
    AfterScroll = taSJFact_MatAfterScroll
    Transaction = trPrint
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 124
    Top = 600
    poSQLINT64ToBCD = True
    object taSJFact_MatMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSJFact_MatMATNAME: TFIBStringField
      FieldName = 'MATNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object taSJFact_PS: TpFIBDataSet
    SelectSQL.Strings = (
      'select d.Name PSNAME, sum(its.FQ) FQ, sum(its.FW*its.S0) FW'
      'from AIt_InSemis its,  AItem it, D_Semis s, D_Dep d'
      'where it.ACTID=:PROTOCOLID and      '
      '      it.ID=its.PROTOCOLID and'
      '      its.SEMISID=s.SEMISID and'
      '      its.MATID=:MATID and'
      '      it.PsId = d.DepId'
      'group by d.NAME'
      'order by d.NAME')
    BeforeOpen = taSJFact_PSBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Filtered = True
    OnFilterRecord = taSJFact_PSFilterRecord
    Left = 188
    Top = 600
    poSQLINT64ToBCD = True
    object taSJFact_PSPSNAME: TFIBStringField
      FieldName = 'PSNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taSJFact_PSFQ: TFIBBCDField
      FieldName = 'FQ'
      Size = 0
      RoundByScale = True
    end
    object taSJFact_PSFW: TFIBFloatField
      FieldName = 'FW'
    end
  end
  object frSJFact_Mat: TfrDBDataSet
    DataSet = taSJFact_Mat
    Left = 124
    Top = 644
  end
  object frSJFact_PS: TfrDBDataSet
    DataSet = taSJFact_PS
    Left = 184
    Top = 644
  end
  object taZpAssort: TpFIBDataSet
    SelectSQL.Strings = (
      'select OPERATION, PSNAME, Q, KT'
      'from Doc_ZpAssort(:ZITEMID)'
      'order by KT'
      '')
    Transaction = trPrint
    Database = dm.db
    Left = 104
    Top = 696
    poSQLINT64ToBCD = True
  end
  object frZpAssort: TfrDBDataSet
    DataSet = taZpAssort
    Left = 104
    Top = 740
  end
  object frRTFExport1: TfrRTFExport
    ScaleX = 1.300000000000000000
    ScaleY = 1.000000000000000000
    Left = 592
    Top = 104
  end
  object taVSheetAssort: TpFIBDataSet
    SelectSQL.Strings = (
      'select a2.ART, a2.ART2, sz.NAME SZNAME, it.Q, it.U, it.FQ'
      'from AIn_Art it, Art2 a2, D_Sz sz'
      'where it.VITEMID=:VITEMID and'
      '      it.ART2ID=a2.ART2ID and'
      '      it.SZID=sz.ID and'
      '      it.Q<>0 '
      'order by a2.ART, a2.ART2, sz.NAME')
    BeforeOpen = taVSheetAssortBeforeOpen
    Transaction = trPrint
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1057#1087#1080#1089#1086#1082' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072' '#1076#1083#1103' '#1089#1083#1080#1095#1080#1090#1077#1083#1100#1085#1086#1081' '#1074#1077#1076#1086#1084#1086#1089#1090#1080' '#1087#1086' '#1086#1087#1077#1088#1072#1094#1080#1080
    Left = 604
    Top = 544
    poSQLINT64ToBCD = True
  end
  object frVSheetAssort: TfrDBDataSet
    DataSet = taVSheetAssort
    Left = 604
    Top = 592
  end
  object taVSheetOperAssort: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select it.ID VITEMID, a.DOCNO, a.DOCDATE, d.NAME DEPNAME, o.OPER' +
        'ATION, s.SEMIS'
      'from Act a, Semis_Report_Dep it, D_Dep d, D_Oper o, D_Semis s'
      'where a.ID=:ACTID and '
      '      a.ID=it.ACT_ID and'
      '      a.DEPID=d.DEPID and'
      '      it.OPERID=o.OPERID and'
      '      it.QA<>0 and'
      '      it.SEMISID=s.SEMISID'
      'order by o.OPERATION, s.SEMIS')
    AfterOpen = taVSheetOperAssortAfterOpen
    AfterScroll = taVSheetOperAssortAfterScroll
    Transaction = trPrint
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1054#1087#1077#1088#1072#1094#1080#1080' '#1085#1072' '#1082#1086#1090#1086#1088#1099#1093' '#1077#1089#1090#1100' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1090' '#1074' '#1089#1083#1080#1095#1080#1090#1077#1083#1100#1085#1086#1081' '#1074#1077#1076#1086#1084#1086#1089#1090#1080
    Left = 516
    Top = 544
    poSQLINT64ToBCD = True
    object taVSheetOperAssortVITEMID: TFIBIntegerField
      FieldName = 'VITEMID'
    end
    object taVSheetOperAssortDOCNO: TFIBIntegerField
      FieldName = 'DOCNO'
    end
    object taVSheetOperAssortDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object taVSheetOperAssortDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taVSheetOperAssortOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = True
    end
    object taVSheetOperAssortSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object frVSheetOperAssort: TfrDBDataSet
    DataSet = taVSheetOperAssort
    Left = 516
    Top = 592
  end
  object frBarCode: TfrBarCodeObject
    Left = 216
    Top = 64
  end
  object quStickerUID: TpFIBQuery
    Transaction = trPrint
    Database = dm.db
    SQL.Strings = (
      'select first 1 '
      '  it.UID, '
      '  a2.ART, '
      '  a2.ART2, '
      '  a2.ART2ID,'
      '  a.STICKER_COMMENT, '
      '  it.W, '
      '  sz.NAME SZNAME, '
      '  a.SEMIS,'
      '  ds.SName '
      'from '
      '  SItem it '
      '  left outer join Art2 a2 on (it.ART2ID=a2.ART2ID)'
      '  left outer join D_Art a on (it.ARTID=a.D_ARTID)'
      '  left outer join D_SZ sz on (it.SZ=sz.ID)'
      '  left outer join D_Semis ds on (it.SemisID = ds.SemisID)'
      'where '
      '  it.UID=:UID '
      'order by '
      '  it.SITEMID desc')
    Left = 220
    Top = 112
    qoAutoCommit = True
    qoStartTransaction = True
    qoTrimCharFields = True
  end
  object taStickerIns: TpFIBDataSet
    SelectSQL.Strings = (
      'select STONES'
      'from STONES_S(:ART2ID)'
      'order by STONES')
    Transaction = trPrint
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 352
    Top = 152
    poSQLINT64ToBCD = True
  end
  object taProdApplRoot: TpFIBDataSet
    SelectSQL.Strings = (
      'select id, designation, sum(quantity) quantity'
      'from application$root$items(:id)'
      'group by designation,id'
      '')
    Transaction = trPrint
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1042#1099#1073#1086#1082#1072' '#1073#1072#1079#1086#1074#1099#1093' '#1072#1088#1090#1080#1082#1091#1083#1086#1074' '#1076#1083#1103' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' '#1079#1072#1082#1072#1079
    Left = 384
    Top = 700
    poSQLINT64ToBCD = True
    object taProdApplRootID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taProdApplRootDESIGNATION: TFIBStringField
      FieldName = 'DESIGNATION'
      Size = 32
      EmptyStrToNull = True
    end
    object taProdApplRootQUANTITY: TFIBBCDField
      FieldName = 'QUANTITY'
      Size = 0
      RoundByScale = True
    end
  end
  object frProdApplRoot: TfrDBDataSet
    DataSet = taProdApplRoot
    Left = 384
    Top = 748
  end
  object frRichObject: TfrRichObject
    Left = 716
    Top = 8
  end
  object frArtAll: TfrDBDataSet
    DataSet = dm.taArt
    Left = 952
    Top = 144
  end
  object frXMLExcelExport1: TfrXMLExcelExport
    Left = 672
    Top = 120
  end
  object taOrderReport: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from REPORT$ORDER(:ORDER$ID)')
    Transaction = dm.tr
    Database = dm.db
    Left = 176
    Top = 704
    object taOrderReportORDERITEMID: TFIBIntegerField
      FieldName = 'ORDER$ITEM$ID'
    end
    object taOrderReportMODEL: TFIBStringField
      FieldName = 'MODEL'
      Size = 32
      EmptyStrToNull = True
    end
    object taOrderReportA: TFIBFloatField
      FieldName = 'A'
    end
    object taOrderReportB: TFIBFloatField
      FieldName = 'B'
    end
    object taOrderReportP1: TFIBIntegerField
      FieldName = 'P1'
    end
    object taOrderReportP2: TFIBIntegerField
      FieldName = 'P2'
    end
    object taOrderReportP3: TFIBIntegerField
      FieldName = 'P3'
    end
    object taOrderReportP41: TFIBIntegerField
      FieldName = 'P41'
    end
    object taOrderReportP42: TFIBIntegerField
      FieldName = 'P42'
    end
  end
  object frOrderReport: TfrDBDataSet
    DataSet = taOrderReport
    Left = 176
    Top = 752
  end
  object taPMat_Ps: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct m.id matid, m.name matname'
      'from AItem ai,'
      '     AIt_InSemis ait,'
      '     D_Semis s,'
      '     d_mat m'
      'where'
      '    ai.ACTID = :ACTID and'
      '    ai.PSID = :PSID and'
      '    ait.PROTOCOLID = ai.ID and'
      '    s.SEMISID = ait.SEMISID and'
      '    m.id = s.mat and'
      '    not (ait.FQ=0 and equal(ait.FW, 0, 0.001)=1)'
      'order by m.sortind')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taPMat_PsBeforeOpen
    Transaction = trPrint
    Database = dm.db
    Left = 960
    Top = 328
    dcForceOpen = True
    oFetchAll = True
  end
  object frPMat_PS: TfrDBDataSet
    DataSet = taPMat_Ps
    Left = 1016
    Top = 328
  end
  object dsrPMat_Ps: TDataSource
    DataSet = taPMat_Ps
    Left = 1072
    Top = 336
  end
  object taPOper_PS: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct o.operid, o.operation opername'
      'from AItem ai,'
      '     AIt_InSemis ait,'
      '     D_Semis s,'
      '     d_oper o'
      'where'
      '    ai.ACTID = :ACTID and'
      '    ai.PSID = :PSID and '
      '    o.operid = ai.operid and'
      '    ait.PROTOCOLID = ai.ID and'
      '    s.SEMISID = ait.SEMISID and'
      '    s.mat = :MATID and'
      '    not (ait.FQ=0 and equal(ait.FW, 0, 0.001)=1)'
      'order by o.operation')
    CacheModelOptions.BufferChunks = 1000
    AfterOpen = taPOper_PSAfterOpen
    BeforeOpen = taPOper_PSBeforeOpen
    Transaction = trPrint
    Database = dm.db
    DataSource = dsrPMat_Ps
    Left = 960
    Top = 264
    dcForceOpen = True
    oFetchAll = True
  end
  object frPOper_PS: TfrDBDataSet
    DataSet = taPOper_PS
    OpenDataSource = False
    Left = 1016
    Top = 264
  end
  object dsrPOper_PS: TDataSource
    DataSet = taPOper_PS
    Left = 1072
    Top = 272
  end
  object taStoneInInvSum: TpFIBDataSet
    SelectSQL.Strings = (
      'select sum(ai.Quantity) Q, sum(ai.Weight) W,'
      'm.name matname'
      
        'from Inv i, SEl e, SItem it, D_Art a, AIns ai, D_Semis s, Art2 a' +
        '2, D_Comp c, d_mat m'
      'where i.INVID in (__INSERT_TEXT_THIS__) and           '
      '           i.InvId = e.InvId and'
      '          e.SelId = it.SelId and'
      '          e.D_ArtId = a.D_ArtId and'
      '          a.D_ArtId = ai.ArtId and'
      '          ai.InsId = s.SemisId and'
      '          e.Art2Id=a2.Art2Id and'
      '          a2.Art2='#39'-'#39' and'
      '          a2.D_CompId=c.CompId  and'
      '          m.id = s.mat          '
      'group by m.name'
      ''
      'union'
      ''
      'select sum(ai.Quantity) Q, sum(ai.Weight) W,'
      'm.name matname'
      
        'from Inv i, SEl e, SItem it, D_Art a, Ins ai, D_Semis s, Art2 a2' +
        ', D_Comp c, d_mat m'
      'where i.INVID in (__INSERT_TEXT_THIS__) and           '
      '           i.InvId = e.InvId and'
      '          e.SelId = it.SelId and'
      '          e.D_ArtId = a.D_ArtId and'
      '          a2.Art2Id = ai.Art2Id and'
      '          ai.InsId = s.SemisId and'
      '          e.Art2Id = a2.Art2Id and'
      '          a2.Art2<>'#39'-'#39' and'
      '          ai.CompId=c.CompId and  '
      '          m.id = s.mat '
      'group by m.name')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 1020
    Top = 104
    object taStoneInInvSumQ: TFIBBCDField
      FieldName = 'Q'
      Size = 0
      RoundByScale = True
    end
    object taStoneInInvSumW: TFIBFloatField
      FieldName = 'W'
    end
    object taStoneInInvSumMATNAME: TFIBStringField
      FieldName = 'MATNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object frStoneInInvSum: TfrDBDataSet
    DataSet = taStoneInInvSum
    Left = 1020
    Top = 148
  end
  object taInvoiceHeader: TpFIBDataSet
    SelectSQL.Strings = (
      'select * '
      'from invoice$header(:INVID)')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 1096
    Top = 5
  end
  object frInvoiceHeader: TfrDBDataSet
    DataSet = taInvoiceHeader
    Left = 1080
    Top = 112
  end
  object taProdApplRing: TpFIBDataSet
    SelectSQL.Strings = (
      'select ART, SUM(Q) Q'
      'from Doc_Appl(:ID)'
      'where ART like '#39'1%'#39
      'group by ART')
    CacheModelOptions.BufferChunks = 1000
    Transaction = trPrint
    Database = dm.db
    Left = 480
    Top = 652
    object FIBStringField1: TFIBStringField
      FieldName = 'ART'
      Origin = 'DOC_APPL.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object FloatField1: TFloatField
      FieldName = 'Q'
      Origin = 'DOC_APPL.Q'
    end
  end
  object frProdApplRing: TfrDBDataSet
    DataSet = taProdApplRing
    Left = 480
    Top = 708
  end
  object frSumOrNotSOEl: TfrDBDataSet
    DataSet = taSumOrNotSOEl
    Left = 72
    Top = 352
  end
  object taProdApplResp: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  OnlyDate(jr.Start$Semis$Date) StartD,'
      '  OnlyDate(jr.Finish$Semis$Date) FinishD,'
      '  OnlyDate(jr.Ready$Semis$Date)  ReadyD,'
      '  ds.Name StartN,'
      '  df.Name FinishN,'
      '  dr.Name ReadyN'
      'from'
      '  ja_resp_persons jr'
      
        '  left outer join d_dep ds on (jr.start$semis$resp$person = ds.d' +
        'epid)'
      
        '  left outer join d_dep df on (jr.finish$semis$resp$person = df.' +
        'depid)'
      
        '  left outer join d_dep dr on (jr.ready$prod$resp$person = dr.de' +
        'pid)'
      'where'
      '  jr.invid = :ApplID')
    BeforeOpen = taProdApplRespBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 552
    Top = 656
    object taProdApplRespSTARTD: TFIBDateTimeField
      FieldName = 'STARTD'
    end
    object taProdApplRespFINISHD: TFIBDateTimeField
      FieldName = 'FINISHD'
    end
    object taProdApplRespREADYD: TFIBDateTimeField
      FieldName = 'READYD'
    end
    object taProdApplRespSTARTN: TFIBStringField
      FieldName = 'STARTN'
      Size = 40
      EmptyStrToNull = True
    end
    object taProdApplRespFINISHN: TFIBStringField
      FieldName = 'FINISHN'
      Size = 40
      EmptyStrToNull = True
    end
    object taProdApplRespREADYN: TFIBStringField
      FieldName = 'READYN'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object frProdApplResp: TfrDBDataSet
    DataSet = taProdApplResp
    Left = 552
    Top = 704
  end
  object taStickerUID: TpFIBDataSet
    SelectSQL.Strings = (
      'select first 1 '
      '  it.UID, '
      '  a2.ART, '
      '  a2.ART2, '
      '  a2.ART2ID,'
      '  a.STICKER_COMMENT, '
      '  it.W, '
      '  sz.NAME SZNAME, '
      '  a.SEMIS,'
      '  ds.SName '
      'from '
      '  SItem it '
      '  left outer join Art2 a2 on (it.ART2ID=a2.ART2ID)'
      '  left outer join D_Art a on (it.ARTID=a.D_ARTID)'
      '  left outer join D_SZ sz on (it.SZ=sz.ID)'
      '  left outer join D_Semis ds on (it.SemisID = ds.SemisID)'
      'where '
      '  it.UID=:UID '
      'order by '
      '  it.SITEMID desc')
    Transaction = dm.tr
    Database = dm.db
    Left = 216
    Top = 160
    object taStickerUIDUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object taStickerUIDART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taStickerUIDART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taStickerUIDART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taStickerUIDSTICKER_COMMENT: TFIBStringField
      FieldName = 'STICKER_COMMENT'
      Size = 40
      EmptyStrToNull = True
    end
    object taStickerUIDW: TFIBFloatField
      FieldName = 'W'
    end
    object taStickerUIDSZNAME: TFIBStringField
      FieldName = 'SZNAME'
      EmptyStrToNull = True
    end
    object taStickerUIDSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Size = 10
      EmptyStrToNull = True
    end
    object taStickerUIDSNAME: TFIBStringField
      FieldName = 'SNAME'
      EmptyStrToNull = True
    end
  end
  object frStickerUID: TfrDBDataSet
    DataSet = taStickerUID
    Left = 280
    Top = 160
  end
  object frxReport: TfrxReport
    Version = '4.9.72'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41222.562891307870000000
    ReportOptions.LastChange = 41222.562891307870000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnUserFunction = frxReportUserFunction
    Left = 32
    Top = 408
    Datasets = <>
    Variables = <>
    Style = <>
  end
  object frxBarCode: TfrxBarCodeObject
    Left = 248
    Top = 456
  end
  object frxDesigner: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    OnSaveReport = frxDesignerSaveReport
    Left = 32
    Top = 456
  end
  object frxItems: TfrxDBDataset
    UserName = 'Items'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 96
    Top = 456
  end
  object frActTolling: TfrDBDataSet
    Left = 8
    Top = 360
  end
  object taSumOrNotSOEl: TpFIBDataSet
    DeleteSQL.Strings = (
      ''
      '        ')
    InsertSQL.Strings = (
      
        'insert into SIEl(ID, INVID, SEMIS, Q,  W, TPRICE, PRICE, NDSID, ' +
        'NDS, T, COST, OPERID, FOR_Q_U, REF)'
      
        'values (:ID, :INVID, :SEMIS, :Q, :W, :TPRICE, :PRICE, :NDSID, :N' +
        'DS, 2, :COST, :OPERID,:FOR_Q_U, :REF)')
    RefreshSQL.Strings = (
      'select '
      '  SemisName,'
      '  IdSemis,'
      '  Q,'
      '  SumWeight,'
      '  For_Q_U,'
      '  NDS,'
      '  SumCost,'
      '  UW,'
      '  UQ,'
      '  Oper,'
      '  HandPrice'
      'from'
      '  SumOrNotSOEl(:inv_id)')
    SelectSQL.Strings = (
      'select '
      '  SemisName,'
      '  IdSemis,'
      '  Q,'
      '  SumWeight,'
      '  For_Q_U,'
      '  NDS,'
      '  SumCost,'
      '  UW,'
      '  UQ,'
      '  Oper,'
      '  HandPrice'
      'from'
      '  SumOrNotSOEl(:inv_id)')
    BeforeOpen = taSumOrNotSOElBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 72
    Top = 304
    object taSumOrNotSOElSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taSumOrNotSOElIDSEMIS: TFIBStringField
      FieldName = 'IDSEMIS'
      Size = 10
      EmptyStrToNull = True
    end
    object taSumOrNotSOElQ: TFIBFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taSumOrNotSOElSUMWEIGHT: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'SUMWEIGHT'
      DisplayFormat = '#.##'
    end
    object taSumOrNotSOElFOR_Q_U: TFIBSmallIntField
      DisplayLabel = #1062#1077#1085#1072' '#1079#1072' '#1074#1077#1089'/'#1082#1086#1083'-'#1074#1086
      FieldName = 'FOR_Q_U'
    end
    object taSumOrNotSOElNDS: TFIBFloatField
      DisplayLabel = #1053#1044#1057
      FieldName = 'NDS'
    end
    object taSumOrNotSOElSUMCOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SUMCOST'
      DisplayFormat = '#.00 '#1088'.'
    end
    object taSumOrNotSOElUW: TFIBStringField
      FieldName = 'UW'
      Size = 10
      EmptyStrToNull = True
    end
    object taSumOrNotSOElUQ: TFIBStringField
      FieldName = 'UQ'
      Size = 10
      EmptyStrToNull = True
    end
    object taSumOrNotSOElOPER: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPER'
      Size = 50
      EmptyStrToNull = True
    end
    object taSumOrNotSOElHANDPRICE: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'HANDPRICE'
      DisplayFormat = '#.00 '#1088'.'
    end
  end
  object taPrintRequestSummary: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  a2.art ART,'
      '  sum(br.q) Q'
      'from'
      '  buyer_request br'
      '  left outer join inv i on (br.invid = i.invid)'
      '  left outer join Art2 a2 on (br.art2id = a2.art2id)'
      'where'
      '  i.itype = 23 and'
      '  i.analiz = 0 and'
      '  i.isprocess = 0 and'
      '  br.q > 0'
      'group by'
      '  a2.art')
    Transaction = dm.tr
    Database = dm.db
    Left = 496
    Top = 360
    object taPrintRequestSummaryART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taPrintRequestSummaryQ: TFIBBCDField
      FieldName = 'Q'
      Size = 0
      RoundByScale = True
    end
  end
  object frPrintRequestSummary: TfrDBDataSet
    DataSet = taPrintRequestSummary
    Left = 496
    Top = 408
  end
  object taReferencedInvoiceItems: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  Name,'
      '  Sub$Name,'
      '  Unit,'
      '  Income$Price,'
      '  Sell$Price,'
      '  Quantity,'
      '  Weight,'
      '  Cost,'
      '  Prill'
      'from'
      '  Get$Referenced$Invoice$Items(:Invoice$ID)')
    Transaction = dm.tr
    Database = dm.db
    Left = 1120
    Top = 472
    object taReferencedInvoiceItemsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taReferencedInvoiceItemsSUBNAME: TFIBStringField
      FieldName = 'SUB$NAME'
      EmptyStrToNull = True
    end
    object taReferencedInvoiceItemsUNIT: TFIBStringField
      FieldName = 'UNIT'
      Size = 10
      EmptyStrToNull = True
    end
    object taReferencedInvoiceItemsINCOMEPRICE: TFIBBCDField
      FieldName = 'INCOME$PRICE'
      Size = 2
      RoundByScale = True
    end
    object taReferencedInvoiceItemsSELLPRICE: TFIBBCDField
      FieldName = 'SELL$PRICE'
      Size = 2
      RoundByScale = True
    end
    object taReferencedInvoiceItemsQUANTITY: TFIBBCDField
      FieldName = 'QUANTITY'
      Size = 3
      RoundByScale = True
    end
    object taReferencedInvoiceItemsWEIGHT: TFIBBCDField
      FieldName = 'WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object taReferencedInvoiceItemsCOST: TFIBBCDField
      FieldName = 'COST'
      Size = 2
      RoundByScale = True
    end
    object taReferencedInvoiceItemsPRILL: TFIBStringField
      FieldName = 'PRILL'
      EmptyStrToNull = True
    end
  end
  object frReferencedInvoiceItems: TfrDBDataSet
    DataSet = taReferencedInvoiceItems
    Left = 1128
    Top = 520
  end
  object frxHeader: TfrxDBDataset
    UserName = 'Header'
    CloseDataSource = False
    BCDToCurrency = False
    Left = 96
    Top = 408
  end
end
