unit dPaytype;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList,  ImgList,  Grids,
  DBGridEh, Buttons, ExtCtrls, ComCtrls, DBGridEhGrouping, rxPlacemnt,
  rxSpeedbar, GridsEh;

type
  TfmPaytypeDict = class(TfmDictAncestor)
  end;

var
  fmPaytypeDict: TfmPaytypeDict;

implementation

uses DictData;

{$R *.dfm}

end.
