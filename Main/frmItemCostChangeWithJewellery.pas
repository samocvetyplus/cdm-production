unit frmItemCostChangeWithJewellery;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, StdCtrls, DB, DBClient;

type
  TItemCostChangeWithJewellery = class(TForm)
    Grid: TDBGridEh;
    DataSource: TDataSource;
    DataSet: TClientDataSet;
    DataSetModel: TStringField;
    DataSetSubModel: TStringField;
    DataSetCheck: TIntegerField;
    DataSetCost: TCurrencyField;
    Button3: TButton;
    Button4: TButton;
    procedure GridTitleBtnClick(Sender: TObject; ACol: Integer;
      Column: TColumnEh);
    procedure GridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure GridTitleClick(Column: TColumnEh);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ItemCostChangeWithJewellery: TItemCostChangeWithJewellery;

implementation

{$R *.dfm}

procedure TItemCostChangeWithJewellery.GridTitleBtnClick(
  Sender: TObject; ACol: Integer; Column: TColumnEh);
begin
  Sender := nil;
end;

procedure TItemCostChangeWithJewellery.GridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  Sender := nil;
  with Grid.Canvas do
  begin
     if DataCol = 1 then
     Color := clBtnShadow;
     FillRect(Rect);
  end;
end;

procedure TItemCostChangeWithJewellery.GridTitleClick(Column: TColumnEh);
begin
  Column := nil;
end;

end.
