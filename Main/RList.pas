unit RList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor,  TB2Item, ActnList, Menus,
  ImgList, Grids, DBGridEh,  StdCtrls, ExtCtrls, ComCtrls, MsgDialog,
  DBUtil, fmUtils, Rev_Det, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmRList = class(TfmListAncestor)
    procedure acDelDocExecute(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  private
    { Private declarations }
   procedure DepClick(Sender : TObject);override;
  public
    { Public declarations }
  end;

var
  fmRList: TfmRList;

implementation

uses InvData, DictData, DocAncestor, DB, PrintData ;

{$R *.dfm}

procedure TfmRList.acDelDocExecute(Sender: TObject);
begin
 if dmINV.taSListISCLOSE.ASinteger=1 then
 begin
  ShowMessage('������ �������� �������� ���������!');
  SysUtils.Abort;
 end;
  inherited;
end;

procedure TfmRList.acAddDocExecute(Sender: TObject);
begin
  with dmINV do
  begin
{
    if dmInv.SType = 0 then ShowAndFreeForm(TfmSInv_det, TForm(fmSInv_det))
    else ShowAndFreeForm(TfmSInvProd, TForm(fmSInvProd));}
    try
      inherited;
      PostDataSet(taSList);
      sqlPublic.SQL.Clear;
      sqlPublic.SQL.Add('Execute procedure Create_Revision_List(:INVID)');
      sqlPublic.ParamByName('INVID').AsInteger:=taSListINVID.AsInteger;
      sqlPublic.Prepare;
      sqlPublic.ExecQuery;
      sqlPublic.Transaction.CommitRetaining;
    except
       on E: Exception do
       begin
         CancelDataSet(DataSet);
         if not (E is EAbort) then
         Application.MessageBox(PChar(E.Message), '������', MB_OK);
       end;
    end;
  end;
end;

procedure TfmRList.FormCreate(Sender: TObject);
var i,j:integer;
  Item : TMenuItem;
  FindDep:boolean;
begin

//���������� PopupMenu �� FDepInfo;
  j := 0;
  for i:=0 to Pred(dm.CountDep) do
    if dm.DepInfo[i].Wh then
    begin
      if (FCurrDep = dm.DepInfo[i].DepId)then FindDep := True;
      Item := TMenuItem.Create(ppDep);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepClick;
      Item.RadioItem := True;
      if Item.Tag = dm.DepDef then DepClick(Item);
      inc(j);
      if j>30 then
      begin
        Item.Break := mbBreak;
        j:=0;
      end;
      ppDep.Items.Add(Item);
    end;

  if FindDep then
  begin
    dm.CurrDep := FCurrDep;
    laDep.Caption := dm.DepInfoById[FCurrDep].Name;
  end
  else begin
    dm.CurrDep := -1;
    laDep.Caption := mnitAllWh.Caption;
  end;

  inherited;
end;

procedure TfmRList.gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if dmInv.taSListIsClose.AsInteger=0 then Background:=clInfoBk
  else Background:=clBtnFace;
end;

procedure TfmRList.DepClick(Sender: TObject);
begin
  inherited;
  dm.CurrDep := TMenuItem(Sender).Tag;
end;

procedure TfmRList.acEditDocExecute(Sender: TObject);
begin
  with dmINV do
  begin
//    taSList.Edit;
    ShowDocForm (TfmRev_det, taRev_det, taSListINVID.AsInteger  ,dm.quTmp);
    PostDataSet(taSList);
    RefreshDataSet(taSList);
  end;
end;

procedure TfmRList.acPrintDocExecute(Sender: TObject);
begin
  inherited;
  dmPrint.PrintDocumentA(dkRevision, VarArrayOf([VarArrayOf([dmInv.FcurrINVID]),VarArrayOf([dmInv.FcurrINVID])]));
end;

end.
