unit dAList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList, RxPlacemnt, ImgList, RxSpeedBar, Buttons,
  Grids, DBGrids, RXDBCtrl, ExtCtrls, ComCtrls, DBGridEh, StdCtrls,
  DBGridEhGrouping, GridsEh;

type
  TfmdAList = class(TfmDictAncestor)
    Button1: TButton;
    acCloseAppl: TAction;
    procedure acCloseApplExecute(Sender: TObject);
    procedure acCloseApplUpdate(Sender: TObject);
    procedure gridDictGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  end;

var
  fmdAList: TfmdAList;

implementation

uses ApplData, dbUtil, MsgDialog;

{$R *.dfm}

procedure TfmdAList.acCloseApplExecute(Sender: TObject);
begin
  if MessageDialog('Закрыть производственный заказ?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  ExecSQL('update Inv set IsProcess=1 where InvId='+dmAppl.taAProdListID.AsString+' and IType=1', dmAppl.quTmp);
  dmAppl.taAProdList.Refresh;
end;

procedure TfmdAList.acCloseApplUpdate(Sender: TObject);
begin
  acCloseAppl.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmdAList.gridDictGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(dmAppl.taAProdListISPROCESS.AsInteger = 1)then Background := clBtnFace;
end;

end.
