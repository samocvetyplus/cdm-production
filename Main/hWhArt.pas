{******************************************}
{  ������� �������� ��������               }
{  Copyrigth (C) 2002-2003 basile for CDM  }
{  v1.02                                   }
{******************************************}
unit hWhArt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  TB2Dock, TB2Toolbar, TB2Item, Grids, DBGridEh, StdCtrls, Mask, DBCtrlsEh,
  ActnList, Db, Menus, DBGridEhGrouping, GridsEh, rxPlacemnt,
  rxSpeedbar;

type
  TfmHWhArt = class(TfmAncestor)
    TBDock1: TTBDock;
    gridHWhArt: TDBGridEh;
    gridIn: TDBGridEh;
    tlbrFilter: TTBToolbar;
    TBItem2: TTBItem;
    acEvent: TActionList;
    acFilter: TAction;
    plIn: TPanel;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    TBControlItem2: TTBControlItem;
    cmbxSz: TDBComboBoxEh;
    TBSeparatorItem1: TTBSeparatorItem;
    TBSeparatorItem2: TTBSeparatorItem;
    acShowID: TAction;
    cmbxPS: TDBComboBoxEh;
    TBControlItem3: TTBControlItem;
    TBSeparatorItem3: TTBSeparatorItem;
    TBControlItem4: TTBControlItem;
    Label2: TLabel;
    ppDetail: TTBPopupMenu;
    acSum: TAction;
    TBItem1: TTBItem;
    TBDock2: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem5: TTBControlItem;
    Label3: TLabel;
    cmbxDetailType: TDBComboBoxEh;
    TBControlItem6: TTBControlItem;
    TBControlItem7: TTBControlItem;
    Label4: TLabel;
    edBD: TDBDateTimeEditEh;
    TBControlItem8: TTBControlItem;
    TBControlItem9: TTBControlItem;
    Label5: TLabel;
    edED: TDBDateTimeEditEh;
    TBControlItem10: TTBControlItem;
    TBSeparatorItem4: TTBSeparatorItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure gridHWhArtGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acFilterExecute(Sender: TObject);
    procedure acShowIdExecute(Sender: TObject);
    procedure gridInGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acSumExecute(Sender: TObject);
    procedure cmbxDetailTypeChange(Sender: TObject);
  private
    FFilterDepId : integer;
    FFilterFromDepId : integer;
    FFilterDetailType : integer;
    FBeginDate, FEndDate : TDateTime;
    procedure FilterDataSet(DataSet : TDataSet; var Accept : boolean);
  end;

var
  fmHWhArt: TfmHWhArt;

implementation

uses MainData, dbUtil, DictData, dbTree, DateUtils, InvData, MsgDialog,
  ProductionConsts;

{$R *.dfm}

procedure TfmHWhArt.FormCreate(Sender: TObject);
var
  nd : TNodeData;
begin
  gridHWhArt.FieldColumns['DOCID'].Visible := False;
  gridHWhArt.FieldColumns['WINVID'].Visible := False;
  gridHWhArt.FieldColumns['WORDERID'].Visible := False;
  //ppDetail.Skin := dm.ppSkin;  
  Application.ProcessMessages;
  FBeginDate := StartOfTheMonth(Now);
  FEndDate := EndOfTheMonth(Now);
  edBD.Value := FBeginDate;
  edED.Value := FEndDate;
  ExecSQL('execute procedure HIST_WHART_1('+IntToStr(dmMain.hArt2Id)+','+IntToStr(dm.User.UserId)+','+#39+
    DateTimeToStr(FBeginDate)+#39','#39+DateTimeToStr(FEndDate)+#39')', dmMain.quTmp);
  Application.ProcessMessages;
  // ��������� ���������� ���������
  FFilterDepId := -1;
  FFilterFromDepId := -1;
  FFilterDetailType := -1;
  cmbxDetailType.ItemIndex := 0;
  dmMain.taTmp_WhArt.OnFilterRecord := FilterDataSet;
  dmMain.taTmp_WhArt.Filtered := True;
  dmMain.taHWhArt.OnFilterRecord := FilterDataSet;
  dmMain.taHWhArt.Filtered := True;
  OpenDataSets([dmMain.taTmp_WhArt, dmMain.taHWhArt]);
  cmbxSz.OnChange := nil;
  cmbxSz.Items.Clear;
  cmbxSz.Items.Assign(dm.dSz);
  cmbxSz.ItemIndex := 0;
  // ��������� ������ ����������/�����������
  cmbxPS.Items.Clear;
  nd := TNodeData.Create;
  nd.Code := -1;
  nd.Name := '*���';
  cmbxPS.Items.AddObject(nd.Name, nd);
  cmbxPS.ItemIndex := 0;
  dmMain.taTmp_WhArt.DisableControls;
  try
    dmMain.taTmp_WhArt.First;
    while not dmMain.taTmp_WhArt.Eof do
    begin
      if (dmMain.taTmp_WhArtDEPNAME.AsString <> '') and
         (cmbxPS.Items.IndexOf(dmMain.taTmp_WhArtDEPNAME.AsString) = -1)then
      begin
        nd := TNodeData.Create;
        nd.Code := dmMain.taTmp_WhArtDEPID.AsInteger;
        nd.Name := dmMain.taTmp_WhArtDEPNAME.AsString;
        cmbxPS.Items.AddObject(dmMain.taTmp_WhArtDEPNAME.AsString, nd);
      end;
      dmMain.taTmp_WhArt.Next;
    end;
    dmMain.taTmp_WhArt.First;
  finally
    dmMain.taTmp_WhArt.EnableControls;
  end;
  dmMain.taHWhArt.DisableControls;
  try
    dmMain.taHWhArt.First;
    while not dmMain.taHWhArt.Eof do
    begin
      if(dmMain.taHWhArtDEPNAME.AsString <> '') and
        (cmbxPS.Items.IndexOf(dmMain.taHWhArtDEPNAME.AsString) = -1)then
      begin
        nd := TNodeData.Create;
        nd.Code := dmMain.taHWhArtDEPID.AsInteger;
        nd.Name := dmMain.taHWhArtDEPNAME.AsString;
        cmbxPS.Items.AddObject(nd.Name, nd);
      end;
      if(dmMain.taHWhArtDEPFROMNAME.AsString <> '') and
        (cmbxPS.Items.IndexOf(dmMain.taHWhArtDEPFROMNAME.AsString) = -1)then
      begin
        nd := TNodeData.Create;
        nd.Code := dmMain.taHWhArtDEPFROMID.AsInteger;
        nd.Name := dmMain.taHWhArtDEPFROMNAME.AsString;
        cmbxPS.Items.AddObject(nd.Name, nd);
      end;
      dmMain.taHWhArt.Next;
    end;
    dmMain.taHWhArt.First;
  finally
    dmMain.taHWhArt.EnableControls;
  end;
end;

procedure TfmHWhArt.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i : integer;
begin
  for i:=0 to Pred(cmbxPS.Items.Count) do
    if Assigned(cmbxPS.Items.Objects[i]) then cmbxPS.Items.Objects[i].Free;
  dmMain.taTmp_WhArt.Filtered := False;
  dmMain.taTmp_WhArt.OnFilterRecord := nil;
  dmMain.taHWhArt.Filtered := False;
  dmMain.taHWhArt.OnFilterRecord := nil;
  CloseDataSets([dmMain.taTmp_WhArt, dmMain.taHWhArt]);
end;

procedure TfmHWhArt.gridHWhArtGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(Column.Field.FieldName = 'ITYPE')then
    case dmMain.taHWhArtITYPE.AsInteger of
     -1 : Background := $00F8EB94;
     -2 : Background := $00F8EB94;
      4 : Background := clSell;
      6 : Background := clWhite;
      7 : Background := clSkyBlue;
      8 : Background := clRed;
      9 : Background := clSkyBlue;
      12 : Background := clReturn;
    end;
end;

procedure TfmHWhArt.acFilterExecute(Sender: TObject);
var
  Bd, Ed : TDateTime;
begin
  dmMain.hSzId := TNodeData(cmbxSz.Items.Objects[cmbxSz.ItemIndex]).Code;
  FFilterDepId := TNodeData(cmbxPS.Items.Objects[cmbxPS.ItemIndex]).Code;
  FFilterFromDepId := FFilterDepId;
  try
    Bd := VarToDateTime(edBD.Value);
  except
    on E:EVariantError do
    begin
      MessageDialogA(Format(rsInvalidDate, ['���������']), mtError);
      eXit;
    end
  end;
  try
    Ed := VarToDateTime(edED.Value);
  except
    on E:EVariantError do
    begin
      MessageDialogA(Format(rsInvalidDate, ['��������']), mtError);
      eXit;
    end
  end;
  if(DateOf(Bd)<>DateOf(FBeginDate))or
    (DateOf(Ed)<>DateOf(FEndDate))then
  begin
    FBeginDate := DateOf(Bd);
    FEndDate := StartOfTheDay(Ed);
    ExecSQL('execute procedure HIST_WHART_1('+IntToStr(dmMain.hArt2Id)+','+IntToStr(dm.User.UserId)+','+#39+
      DateTimeToStr(FBeginDate)+#39','#39+DateTimeToStr(FEndDate)+#39')', dmMain.quTmp);
  end;
  ReOpenDataSets([dmMain.taTmp_WhArt, dmMain.taHWHArt]);
end;

procedure TfmHWhArt.acShowIdExecute(Sender: TObject);
begin
  gridHWhArt.FieldColumns['DOCID'].Visible := not gridHWhArt.FieldColumns['DOCID'].Visible;
  gridHWhArt.FieldColumns['WINVID'].Visible := not gridHWhArt.FieldColumns['WINVID'].Visible;
  gridHWhArt.FieldColumns['WORDERID'].Visible := not gridHWhArt.FieldColumns['WORDERID'].Visible;
end;

procedure TfmHWhArt.gridInGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if(Column.Field.FieldName = 'ITYPE')then
    case dmMain.taTmp_WhArtITYPE.AsInteger of
     -1 : Background := $00F8EB94;
     -2 : Background := $00F8EB94;
    end;
end;

procedure TfmHWhArt.FilterDataSet(DataSet: TDataSet; var Accept: boolean);
begin
  if(AnsiUpperCase(DataSet.Name) = 'TATMP_WHART')then
  begin
    if(FFilterDepId = -1)then Accept := True
    else Accept := dmMain.taTmp_WhArtDEPID.AsInteger = FFilterDepId;
  end
  else if(AnsiUpperCase(DataSet.Name) = 'TAHWHART')then
  begin
    if(FFilterDepId = -1)and(FFilterFromDepId = -1)then Accept := True
    else begin
      Accept := (dmMain.taHWhArtDEPID.AsInteger = FFilterDepId) or
                (dmMain.taHWhArtDEPFROMID.AsInteger = FFilterFromDepId);
    end;
    if Accept then
      if(FFilterDetailType = -1)then Accept := True
      else Accept := FFilterDetailType = dmMain.taHWhArtITYPE.AsInteger;
  end;
end;

procedure TfmHWhArt.acSumExecute(Sender: TObject);
var
  i, s : integer;
begin
  if(gridHWhArt.SelectedRows.Count > 1)  then
  begin
    s := 0;
    for i:=0 to Pred(gridHWhArt.SelectedRows.Count) do
    begin
      dmMain.taHWhArt.GotoBookmark(Pointer(gridHWhArt.SelectedRows.Items[i]));
      s := s+dmMain.taHWhArtQ.AsInteger;
    end;
    MessageDialogA(IntToStr(s), mtCustom);
  end;
end;

procedure TfmHWhArt.cmbxDetailTypeChange(Sender: TObject);
begin
  FFilterDetailType := StrToIntDef(cmbxDetailType.KeyItems[cmbxDetailType.ItemIndex], -1);
  ReOpenDataSet(dmMain.taHWhArt);
end;

end.
