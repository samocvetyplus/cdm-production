unit Request;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus, TB2Item, ActnList,  ImgList, Grids,
  DBGridEh, DBCtrlsEh, StdCtrls, Mask, DBCtrls, ExtCtrls, 
  ComCtrls, Buttons, DBLookupEh, Db, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmRequest = class(TfmDocAncestor)
    Label1: TLabel;
    txtSupName: TDBText;
    Label2: TLabel;
    Label3: TLabel;
    edABD: TDBDateTimeEditEh;
    edAED: TDBDateTimeEditEh;
    acShowID: TAction;
    SpeedItem1: TSpeedItem;
    acCreate: TAction;
    Label4: TLabel;
    edArt: TDBEditEh;
    TBItem1: TTBItem;
    acPrintAppl: TAction;
    Label5: TLabel;
    mmComment: TDBMemo;
    Label18: TLabel;
    edBoss: TDBEditEh;
    edBossPhone: TDBEditEh;
    Label37: TLabel;
    edOFIO: TDBEditEh;
    edOFIOPhone: TDBEditEh;
    Label39: TLabel;
    edPostAddress: TDBEditEh;
    Label6: TLabel;
    edPhone: TDBEditEh;
    Label7: TLabel;
    edFax: TDBEditEh;
    Bevel1: TBevel;
    acViewComp: TAction;
    SpeedButton1: TSpeedButton;
    Label8: TLabel;
    lcbxInvColor: TDBLookupComboboxEh;
    procedure FormCreate(Sender: TObject);
    procedure acShowIDExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCreateExecute(Sender: TObject);
    procedure edArtChange(Sender: TObject);
    procedure acPrintApplExecute(Sender: TObject);
    procedure acViewCompExecute(Sender: TObject);
    procedure spitCloseClick(Sender: TObject);
  private
    procedure FilterInvColor(DataSet: TDataSet; var Accept: Boolean);
    procedure InvColorGetCellParams(Sender: TObject; EditMode: Boolean; Params: TColCellParamsEh);
  end;

var
  fmRequest: TfmRequest;

implementation

uses Data, DictData, dbUtil, PrintData, DbEditor, eComp, MsgDialog, InvData,
  ProductionConsts;

{$R *.dfm}

procedure TfmRequest.FormCreate(Sender: TObject);
begin
  FDocName := '������ �� ����������';
  edNoDoc.DataSource := dmData.dsrRequestList;
  dtedDateDoc.DataSource := dmData.dsrRequestList;
  gridItem.DataSource := dmData.dsrRequest;
  inherited;
  // �������� �����
  dm.taInvColor.OnFilterRecord := FilterInvColor;
  dm.taInvColor.Filtered := True;
  OpenDataSet(dm.taInvColor);
  lcbxInvColor.DropDownBox.Columns[0].OnGetCellParams := InvColorGetCellParams;
end;

procedure TfmRequest.acShowIDExecute(Sender: TObject);
begin
  gridItem.FieldColumns['ARTID'].Visible := not gridItem.FieldColumns['ARTID'].Visible;
  gridItem.FieldColumns['ART2ID'].Visible := not gridItem.FieldColumns['ART2ID'].Visible;
  gridItem.FieldColumns['SZID'].Visible := not gridItem.FieldColumns['SZID'].Visible;
  gridItem.FieldColumns['ID'].Visible := not gridItem.FieldColumns['ID'].Visible;
  gridItem.FieldColumns['INVID'].Visible := not gridItem.FieldColumns['INVID'].Visible;
end;

procedure TfmRequest.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  gridItem.FieldColumns['ARTID'].Visible := False;
  gridItem.FieldColumns['ART2ID'].Visible := False;
  gridItem.FieldColumns['SZID'].Visible := False;
  gridItem.FieldColumns['ID'].Visible := False;
  gridItem.FieldColumns['INVID'].Visible := False;
  dmData.taRequest.Filtered := False;
  dmData.taRequest.Filter := '';
  CloseDataSet(dm.taInvColor);
  dm.taInvColor.Filtered := False;
  dm.taInvColor.OnFilterRecord := nil;
end;

procedure TfmRequest.acCreateExecute(Sender: TObject);
begin
  ExecSQL('execute procedure Create_Request('+dmData.taRequestListINVID.AsString+')', dm.quTmp);
  ReOpenDataSet(DataSet);
end;

procedure TfmRequest.edArtChange(Sender: TObject);
begin
  if(edArt.Text = '')then dmData.taRequest.Filtered := False
  else begin
    dmData.taRequest.Filtered := False;
    dmData.taRequest.Filter := 'ART='#39+edArt.Text+'*'#39;
    dmData.taRequest.Filtered := True;
  end
end;

procedure TfmRequest.acPrintApplExecute(Sender: TObject);
begin
  PostDataSet(DataSet);
  dmPrint.PrintDocumentA(dkBuyer_Request, VarArrayOf([VarArrayOf([dmData.taRequestListINVID.AsInteger])]));
end;

procedure TfmRequest.acViewCompExecute(Sender: TObject);

begin
  ReOpenDataSet(dm.taComp);
  if not dm.taComp.Locate('COMPID', dmData.taRequestListSUPID.AsInteger, []) then
  begin
    CloseDataSet(dm.taComp);
    raise EInternal.Create(Format(rsInternalError, ['003']));
  end;
  if ShowDbEditor(TfmeComp, dm.dsrComp, emEdit, True)<>mrOk then CancelDataSet(dm.taComp);
  CloseDataSet(dm.taComp);
end;

procedure TfmRequest.spitCloseClick(Sender: TObject);
var
  OldInvId: integer;
  InvId : integer;
begin
  PostDataSets([dmData.taRequest, dmData.taRequestList]);
  if MessageDialog('������� ������ ����� ��������� � ����� ������� ����� ������. �� ��������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
  OldInvId := dmData.taRequestListINVID.AsInteger;
  InvId := ExecSelectSQL('select gen_id(gen_inv_id, 1) from rdb$database', dm.quTmp);
  ExecSQL('insert into Inv(INVID, DOCNO, DOCDATE, ISPROCESS, SUPID, USERID, ITYPE, ANALIZ)'+
    'values('+IntToStr(InvId)+', (select NEWID from GetId(88)), "NOW", 0, '+
    dmData.taRequestListSUPID.AsString+', '+IntToStr(dm.User.UserId)+', 23, 0)', dm.quTmp);
  CloseDataSet(dmData.taRequest);
  dmData.taRequestList.Delete;
  dmData.taRequestList.Insert;
  dmData.taRequestListINVID.AsInteger := InvId;
  dmData.taRequestList.Post;
  dmData.taRequestList.Refresh;
  ExecSQL('update Inv set IsProcess=1 where InvId='+IntToStr(OldInvId), dm.quTmp);
  OpenDataSet(dmData.taRequest);
end;

procedure TfmRequest.FilterInvColor(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := dm.taInvColorINVTYPEID.AsInteger = dmInv.IType;
end;

procedure TfmRequest.InvColorGetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
  Params.Background := dm.taInvColorCOLOR.AsInteger;
end;

end.
