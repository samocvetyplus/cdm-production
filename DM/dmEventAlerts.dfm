object EventAlertsData: TEventAlertsData
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 167
  Width = 83
  object DataBase: TpFIBDatabase
    SQLDialect = 1
    Timeout = 0
    WaitForRestoreConnect = 0
    Left = 16
    Top = 8
  end
  object Transaction: TpFIBTransaction
    DefaultDatabase = DataBase
    TimeoutAction = TARollback
    Left = 16
    Top = 56
  end
  object Alerter: TSIBfibEventAlerter
    Events.Strings = (
      'TechnologyGroups.end'
      'TechnologyGroups.start')
    OnEventAlert = AlerterEventAlert
    Database = DataBase
    Left = 16
    Top = 104
  end
end
