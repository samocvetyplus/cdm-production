unit PrintPersonalInfo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, rxToolEdit, ExtCtrls, Grids, DBGrids;

type
  TPlaceOfStorageInventoryPrint = class(TForm)
    BDEdit: TDateEdit;
    EDEdit: TDateEdit;
    Label1: TLabel;
    Label2: TLabel;
    Button1: TButton;
    GroupBox1: TGroupBox;
    Current: TRadioButton;
    Random: TRadioButton;
    procedure Button1Click(Sender: TObject);
    procedure RandomClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CurrentClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  PlaceOfStorageInventoryPrint: TPlaceOfStorageInventoryPrint;

implementation

Uses MainData, dmReportInventory, PList;
{$R *.dfm}



procedure TPlaceOfStorageInventoryPrint.Button1Click(Sender: TObject);
begin
if (Current.Checked = false) and (Random.Checked = false)
  then
    ShowMessage('���������� ������� ��� �����!')
  else
    begin
      if Current.Checked then
        begin
          ReportInventory := TReportInventory.Create(Self);
          ReportInventory.BD := dmMain.taPlist.FieldByName('BD').AsString;
          ReportInventory.ED := dmMain.taPlist.FieldByName('ED').AsString;
          ReportInventory.APlaceOfStorageID := dmMain.taProtocol_dPSID.AsInteger;
          //Showmessage('BD: ' + ReportInventory.BD + '; ED: ' + ReportInventory.ED + '; PSID: ' + IntToStr(ReportInventory.APlaceOfStorageID));
          ReportInventory.Prepare;
          ReportInventory.Free;
        end;
      if Random.Checked then
        begin
          if (BDEdit.Text = '') or (EDEdit.Text = '') then
            ShowMessage('������� ������!')
          else
            begin
              ReportInventory := TReportInventory.Create(Self);
              ReportInventory.BD := BDEdit.Text;
              ReportInventory.ED := EDEdit.Text;
              ReportInventory.APlaceOfStorageID := dmMain.taProtocol_dPSID.AsInteger;
              ReportInventory.Prepare;
              ReportInventory.Free;
            end;
        end;
    end;
end;



procedure TPlaceOfStorageInventoryPrint.CurrentClick(Sender: TObject);
begin
  BDEdit.Enabled := false;
  EDEdit.Enabled := false;
end;

procedure TPlaceOfStorageInventoryPrint.FormCreate(Sender: TObject);
begin
  BDEdit.Text := '';
  EDEdit.Text := '';
  BDEdit.Enabled := false;
  EDEdit.Enabled := false;
end;

procedure TPlaceOfStorageInventoryPrint.RandomClick(Sender: TObject);
begin
  BDEdit.Enabled := True;
  EDEdit.Enabled := True;
end;

end.
