object fmDialogImport: TfmDialogImport
  Left = 0
  Top = 0
  Caption = 'fmDialogImport'
  ClientHeight = 86
  ClientWidth = 327
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object aLabel: TLabel
    Left = 8
    Top = 27
    Width = 26
    Height = 13
    Caption = #1060#1072#1081#1083
  end
  object FilenameEdit: TFilenameEdit
    Left = 40
    Top = 24
    Width = 273
    Height = 21
    DefaultExt = 'txt'
    Filter = #1058#1077#1082#1089#1090#1086#1074#1099#1077' '#1092#1072#1081#1083#1099'|*.txt'
    DialogOptions = [ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    DialogTitle = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083' '#1076#1083#1103' '#1080#1084#1087#1086#1088#1090#1072
    NumGlyphs = 1
    TabOrder = 0
    Text = 'FilenameEdit'
  end
  object ButtonImport: TButton
    Left = 238
    Top = 51
    Width = 75
    Height = 25
    Action = acImport
    TabOrder = 1
  end
  object ActionList: TActionList
    Left = 24
    Top = 56
    object acImport: TAction
      Caption = #1055#1086#1077#1093#1072#1083#1080
    end
  end
end
