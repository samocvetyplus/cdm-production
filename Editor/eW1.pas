unit eW1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh;

type
  TfmEditor1 = class(TfmEditor)
    Label1: TLabel;
    edW: TDBNumberEditEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmEditor1: TfmEditor1;

implementation

uses UtilLib, MainData;

{$R *.dfm}

procedure TfmEditor1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk : begin
       {if(ActiveControl = btnOk)then ModalResult := mrOk
       else if(ActiveControl = btnCancel)then ModalResult := mrCancel
       else begin
         ActiveControl := FindNextControl(ActiveControl, True, True, False);
         SysUtils.Abort;
       end;}
       try
         dmMain.SemisW := ConvertStrToFloat(edW.Text);
       except
         ActiveControl := edW;
         SysUtils.Abort;
       end;
    end;
  inherited;
end;

end.
