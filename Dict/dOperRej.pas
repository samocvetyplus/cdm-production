unit dOperRej;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList,  ImgList,  Buttons,
  Grids, ExtCtrls, ComCtrls, DBGridEh, DBGridEhGrouping, rxPlacemnt,
  rxSpeedbar, GridsEh;

type
  TfmOperRej = class(TfmDictAncestor)
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmOperRej: TfmOperRej;

implementation

uses DictData, dbUtil;

{$R *.dfm}

procedure TfmOperRej.FormCreate(Sender: TObject);
begin
  OpenDataSets([dm.taSemis, dm.taMat]);
  inherited;
end;

procedure TfmOperRej.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dm.taSemis, dm.taMat]);
  inherited;
end;

end.
