unit dPrill;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, RxPlacemnt, ImgList, Grids, DBGrids, RXDBCtrl,
  ExtCtrls, ComCtrls, ActnList, DBGridEh,
  Buttons, DBGridEhGrouping, rxSpeedbar, GridsEh;

type
  TfmPrill = class(TfmDictAncestor)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmPrill: TfmPrill;

implementation

uses DictData;

{$R *.dfm}

procedure TfmPrill.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dm.LoadArtSL(PRILL_DICT);
end;

end.
