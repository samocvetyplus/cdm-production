unit Rev_Det;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus, TB2Item, ActnList,  ImgList, Grids,
  DBGridEh, DBCtrlsEh, StdCtrls, Mask, DBCtrls, ExtCtrls,  fmUtils,
  ComCtrls, DBUtil, TB2Dock, TB2Toolbar, cxControls, cxContainer, cxEdit,
  cxProgressBar, cxDBProgressBar, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar, cxGraphics,
  cxLookAndFeels, cxLookAndFeelPainters;

type
  TfmRev_Det = class(TfmDocAncestor)
    acAdd: TAction;
    acClose: TAction;
    acPrint: TAction;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    edFilterArt: TDBEditEh;
    TBControlItem2: TTBControlItem;
    TBItem1: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    acUIDHisttory: TAction;
    lbScanUID: TLabel;
    ProgressBar: TcxDBProgressBar;
    procedure acAddExecute(Sender: TObject);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acCloseExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acUIDHisttoryExecute(Sender: TObject);
    procedure edFilterArtChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  public
   FUID:integer;
   procedure InsertUID(UId:integer);
  end;

const clFirst=clwhite;  // ������ ����
      clSecond= $00D1E8C8; // ����
      clThird=clRed; // ���� ������� ������� ���� �� ������

implementation

uses MainData, InvData, FIBQuery, pFIBQuery, DB, eUID, PrintData, MsgDialog,
  ItemHistory;

{$R *.dfm}

procedure TfmRev_Det.acAddExecute(Sender: TObject);
begin
//  inherited;
  try
   fmUID:=TfmUID.Create(self);
   if fmUID.ShowModal=mrOk
   then begin
     FUID:=fmUID.UID;
     InsertUID(FUID);
   end;
  finally
   fmUID.Free;
  end;
end;

procedure TfmRev_Det.gridItemGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if (dminv.taRev_detSTATE.AsInteger=0) then Background:=clFirst;
  if  (dminv.taRev_detSTATE.AsInteger=1) then Background:=clSecond;
  if  (dminv.taRev_detSTATE.AsInteger=2) then Background:=clThird;
{
  if (Column.FieldName='STATE') and (dminv.taRev_detSTATE.AsInteger=0) then Background:=clFirst;
  if (Column.FieldName='STATE') and (dminv.taRev_detSTATE.AsInteger=1) then Background:=clSecond;
  if (Column.FieldName='STATE') and (dminv.taRev_detSTATE.AsInteger=2) then Background:=clThird;
}
end;

procedure TfmRev_Det.InsertUID(UId: integer);
begin
   with  dmInv, sqlPublic do
   try
     Sql.Clear;
     SQL.Add('execute procedure ADD_UID_IN_REVISION(:UId,:INVID)');
     ParamByName('UID').AsInteger:=UID;
     ParamByName('INVID').AsInteger:=taSListINVID.AsInteger;
     Prepare;
     try
       ExecQuery;
       Transaction.CommitRetaining;
       ReOpenDataSet(taRev_det);
       taRev_det.Locate('UID',UID,[loCaseInsensitive]);
       taSList.Refresh;
     except
       on E:Exception do begin
         MessageDialog(E.Message, mtConfirmation, [mbOk], 0);
         Transaction.CommitRetaining;
       end
     end;
   except
     on E:Exception do MessageDialog('InsertUID '+E.Message, mtError, [mbYes], 0);
   end;
end;

procedure TfmRev_Det.acCloseExecute(Sender: TObject);
begin
 inherited;
{
  PostDataSet(dmInv.taSList);
  if spitClose.BtnCaption = '������'  then
  begin
    if dmINV.taINVItems.RecordCount=0 then raise EWarning.Create(rsInvEmpty);
    ExecSQL('execute procedure CloseDoc("'+FDocName+'",'+dmInv.taSListINVID.AsString+')', dm.quTmp);
    dmInv.taSList.Refresh;
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 5;
  end
  else begin
    ExecSQL('execute procedure OpenDoc("'+FDocName+'",'+dmInv.taSListINVID.AsString+')', dm.quTmp);
    dm.quTmp.Transaction.CommitRetaining;
    dmInv.taSList.Refresh;
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 6;
  end
}
end;

procedure TfmRev_Det.acPrintExecute(Sender: TObject);
begin
  inherited;
  dmPrint.PrintDocumentA(dkRevision, VarArrayOf([VarArrayOf([dmInv.FcurrINVID]),VarArrayOf([dmInv.FcurrINVID])]));
end;

procedure TfmRev_Det.acUIDHisttoryExecute(Sender: TObject);
begin
  CloseDataSet(dmInv.taItemHistory);
  if dmInv.taRev_detUID.IsNull then dmInv.FCurrUID := 0
  else dmInv.FCurrUID := dmInv.taRev_detUID.AsInteger;
  ShowAndFreeForm(TfmItemHistory, Self, TForm(fmItemHistory), True, False);
  CloseDataSet(dmInv.taItemHistory);
end;

procedure TfmRev_Det.edFilterArtChange(Sender: TObject);
begin
  if(edFilterArt.Text='')then dmInv.taRev_det.Filtered := False
  else begin
    dmInv.taRev_det.DisableScrollEvents;
    dmInv.taRev_det.DisableControls;
    try
      dmInv.taRev_det.Filtered := False;
      dmInv.taRev_det.Filter := 'ART='#39+edFilterArt.Text+'*'+#39;
      dmInv.taRev_det.Filtered := True;
      gridItem.SumList.RecalcAll;
    finally
      dmInv.taRev_det.EnableScrollEvents;
      dmInv.taRev_det.EnableControls;
    end;
  end;
end;

procedure TfmRev_Det.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dmInv.taRev_det.Filtered := False;
  dmInv.taRev_det.Filter := '';
end;

procedure TfmRev_Det.FormCreate(Sender: TObject);
begin
  gridItem.DataSource := dmInv.dsrRev_Det;
  edNoDoc.DataSource := dmInv.dsrSList;
  dtedDateDoc.DataSource := dmInv.dsrSList;
  lbScanUID.Caption := '';
  inherited;
  FDocName := '�������������� ��';
  InitBtn(Boolean(dmInv.taSListISCLOSE.AsInteger));
end;

end.
