object fmEDate: TfmEDate
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = #1042#1074#1077#1076#1080#1090#1077' '#1076#1072#1090#1091
  ClientHeight = 101
  ClientWidth = 207
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object DateEdit: TcxDateEdit
    Left = 78
    Top = 24
    Properties.DateOnError = deToday
    Properties.SaveTime = False
    Properties.ShowTime = False
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
    TabOrder = 0
    Width = 121
  end
  object cxLabel: TcxLabel
    Left = 16
    Top = 25
    Caption = #1044#1072#1090#1072
    Style.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.Kind = lfOffice11
  end
  object bContinue: TcxButton
    Left = 43
    Top = 68
    Width = 75
    Height = 25
    Caption = #1055#1088#1086#1076#1086#1083#1078#1080#1090#1100
    ModalResult = 1
    TabOrder = 2
    LookAndFeel.Kind = lfOffice11
  end
  object bCancel: TcxButton
    Left = 124
    Top = 68
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 3
    LookAndFeel.Kind = lfOffice11
  end
end
