object dmData: TdmData
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 554
  Width = 883
  object taActWorkList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Inv set'
      '  DOCNO=:DOCNO,'
      '  DOCDATE=:DOCDATE,'
      '  ISCLOSE=:ISCLOSE,'
      '  INVSUBTYPE=:INVSUBTYPEID,'
      '  NORM = :NORM'
      'where INVID=:INVID')
    DeleteSQL.Strings = (
      'delete from Inv where INVID=:INVID')
    InsertSQL.Strings = (
      
        'insert into Inv(INVID, DOCNO, DOCDATE, SUPID, ABD, AED, USERID, ' +
        'ISCLOSE, ITYPE, INVSUBTYPE)'
      
        'values (:INVID, :DOCNO, :DOCDATE, :COMPID, :BD, :ED, :USERID, :I' +
        'SCLOSE, 21, :INVSUBTYPEID)'
      '')
    RefreshSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, COMPID,'
      '       BD, ED, USERID, COMPNAME, USERNAME, ISCLOSE,'
      '       INVSUBTYPEID, INVSUBTYPENAME, REFINVID,'
      '       REFDOCNO, REFDOCDATE, REFNAME, NO_CONTRACT, '
      '       BD_CONTRACT, SELFCOMPNAME, SELFBOSSFIO, COMPBOSSFIO,'
      '       COST, REFISCLOSE, NORM'
      'from ACTWORKLIST_R(:INVID)')
    SelectSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, COMPID,'
      '       BD, ED, USERID, COMPNAME, USERNAME, ISCLOSE,'
      '       INVSUBTYPEID, INVSUBTYPENAME, REFINVID,'
      '       REFDOCNO, REFDOCDATE, REFNAME, NO_CONTRACT,'
      '       BD_CONTRACT, SELFCOMPNAME, SELFBOSSFIO, COMPBOSSFIO,'
      '       COST, REFISCLOSE, NORM'
      'from ACTWORKLIST_S(:BD, :ED)'
      'order by DOCDATE')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeDelete = taActWorkListBeforeDelete
    BeforeOpen = taActWorkListBeforeOpen
    OnNewRecord = taActWorkListNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 24
    Top = 12
    poSQLINT64ToBCD = True
    object taActWorkListINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taActWorkListDOCNO: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1072#1082#1090#1072
      FieldName = 'DOCNO'
    end
    object taActWorkListDOCDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1072#1082#1090#1072
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taActWorkListCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object taActWorkListBD: TFIBDateTimeField
      DisplayLabel = #1053#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072
      FieldName = 'BD'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taActWorkListED: TFIBDateTimeField
      DisplayLabel = #1050#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072
      FieldName = 'ED'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taActWorkListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taActWorkListCOMPNAME: TFIBStringField
      DisplayLabel = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      FieldName = 'COMPNAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taActWorkListUSERNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'USERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taActWorkListISCLOSE: TFIBSmallIntField
      FieldName = 'ISCLOSE'
    end
    object taActWorkListINVSUBTYPEID: TFIBIntegerField
      FieldName = 'INVSUBTYPEID'
    end
    object taActWorkListINVSUBTYPENAME: TFIBStringField
      DisplayLabel = #1058#1080#1087
      FieldName = 'INVSUBTYPENAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taActWorkListREFINVID: TFIBIntegerField
      FieldName = 'REFINVID'
    end
    object taActWorkListREFDOCNO: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      FieldName = 'REFDOCNO'
    end
    object taActWorkListREFDOCDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      FieldName = 'REFDOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taActWorkListREFNAME: TFIBStringField
      DisplayLabel = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      FieldName = 'REFNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taActWorkListNO_CONTRACT: TFIBStringField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1076#1086#1075#1086#1074#1086#1088#1072
      FieldName = 'NO_CONTRACT'
      EmptyStrToNull = True
    end
    object taActWorkListBD_CONTRACT: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1076#1086#1075#1086#1074#1086#1088#1072
      FieldName = 'BD_CONTRACT'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taActWorkListSELFBOSSFIO: TFIBStringField
      DisplayLabel = #1060#1048#1054' '#1087#1086#1076#1088#1103#1076#1095#1080#1082#1072
      FieldName = 'SELFBOSSFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taActWorkListCOMPBOSSFIO: TFIBStringField
      DisplayLabel = #1060#1048#1054' '#1079#1072#1082#1072#1079#1095#1080#1082#1072
      FieldName = 'COMPBOSSFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taActWorkListSELFCOMPNAME: TFIBStringField
      FieldName = 'SELFCOMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taActWorkListCOST: TFIBFloatField
      DisplayLabel = #1059#1089#1083#1091#1075#1080
      FieldName = 'COST'
      currency = True
    end
    object taActWorkListREFISCLOSE: TFIBSmallIntField
      FieldName = 'REFISCLOSE'
    end
    object taActWorkListNORM: TFIBFloatField
      FieldName = 'NORM'
    end
  end
  object dsrActWorkList: TDataSource
    DataSet = taActWorkList
    Left = 24
    Top = 60
  end
  object taActWork: TpFIBDataSet
    UpdateSQL.Strings = (
      'update MatItem set'
      '  N=:N,'
      '  MATID=:MATID,'
      '  ADDBYUSER=:ADDBYUSER,'
      '  UW=:UW,'
      '  PRICE0=:PRICE0  '
      'where ID=:ID')
    DeleteSQL.Strings = (
      'delete from MatItem where ID=:ID')
    InsertSQL.Strings = (
      
        'insert into MatItem(ID, INVID, MATID, N, GETW, RETW, NW, ADDBYUS' +
        'ER, UW, CLIENTMAT, PRICE0)'
      
        'values (:ID, :INVID, :MATID, :N, :GETW, :RETW, :NW, :ADDBYUSER, ' +
        ':UW, :CLIENTMAT, 0)')
    SelectSQL.Strings = (
      'select ID, INVID, MATID, N, GETW, RETW, NW, ADDBYUSER, '
      '    UW, CLIENTMAT, COST, PRICE0'
      'from ActWork_S(:INVID)'
      '      '
      '      ')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taActWorkBeforeOpen
    OnNewRecord = taActWorkNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 104
    Top = 12
    poSQLINT64ToBCD = True
    object taActWorkID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taActWorkINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taActWorkMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taActWorkN: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072
      FieldName = 'N'
    end
    object taActWorkGETW: TFIBFloatField
      DisplayLabel = #1055#1088#1080#1085#1103#1090#1086
      FieldName = 'GETW'
      DisplayFormat = '0.###'
    end
    object taActWorkRETW: TFIBFloatField
      DisplayLabel = #1042#1099#1076#1072#1085#1086
      FieldName = 'RETW'
      DisplayFormat = '0.###'
    end
    object taActWorkNW: TFIBFloatField
      DisplayLabel = #1042#1077#1089' '#1087#1086' '#1085#1086#1088#1084#1077
      FieldName = 'NW'
      DisplayFormat = '0.###'
    end
    object taActWorkMATNAME: TStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldKind = fkLookup
      FieldName = 'MATNAME'
      LookupDataSet = dm.taMat
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'MATID'
      Size = 40
      Lookup = True
    end
    object taActWorkADDBYUSER: TFIBSmallIntField
      DisplayLabel = #1044#1086#1073#1072#1074#1083#1077#1085#1072' '#1087#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1077#1084
      FieldName = 'ADDBYUSER'
    end
    object taActWorkUW: TFIBSmallIntField
      DisplayLabel = #1045#1048
      FieldName = 'UW'
    end
    object taActWorkCLIENTMAT: TFIBSmallIntField
      DisplayLabel = #1042#1083#1072#1076#1077#1083#1077#1094
      FieldName = 'CLIENTMAT'
    end
    object taActWorkCOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'COST'
      currency = True
    end
    object taActWorkPRICE0: TFIBFloatField
      DisplayLabel = #1059#1095#1077#1090#1085#1072#1103' '#1094#1077#1085#1072
      FieldName = 'PRICE0'
      currency = True
    end
  end
  object dsrActWork: TDataSource
    DataSet = taActWork
    Left = 104
    Top = 60
  end
  object taOutAnaliz: TpFIBDataSet
    UpdateSQL.Strings = (
      'update '
      'TMP_SLIST_FOR_COMP'
      'set A=:A'
      'where '
      'INVID=:OLD_INVID')
    SelectSQL.Strings = (
      
        'select INVID, DOCNO, DOCDATE, COMPNAME, TYPENAME, Q, W, SUMM, A ' +
        'from TMP_SLIST_FOR_COMP'
      'order by TYPENAME')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 24
    Top = 112
    poSQLINT64ToBCD = True
    object taOutAnalizINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'TMP_SLIST_FOR_COMP.INVID'
      Required = True
    end
    object taOutAnalizDOCNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088
      FieldName = 'DOCNO'
      Origin = 'TMP_SLIST_FOR_COMP.DOCNO'
    end
    object taOutAnalizDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'DOCDATE'
      Origin = 'TMP_SLIST_FOR_COMP.DOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object taOutAnalizCOMPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
      FieldName = 'COMPNAME'
      Origin = 'TMP_SLIST_FOR_COMP.COMPNAME'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taOutAnalizTYPENAME: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      FieldName = 'TYPENAME'
      Origin = 'TMP_SLIST_FOR_COMP.TYPENAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taOutAnalizQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'TMP_SLIST_FOR_COMP.Q'
    end
    object taOutAnalizW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'TMP_SLIST_FOR_COMP.W'
      DisplayFormat = '0.00'
    end
    object taOutAnalizSUMM: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'SUMM'
      Origin = 'TMP_SLIST_FOR_COMP.SUMM'
      currency = True
    end
    object taOutAnalizA: TSmallintField
      DefaultExpression = '0'
      DisplayLabel = #1054#1073#1088#1072#1073#1072#1090#1099#1074#1072#1090#1100
      FieldName = 'A'
      Origin = 'TMP_SLIST_FOR_COMP.A'
    end
  end
  object dsrOutAnaliz: TDataSource
    DataSet = taOutAnaliz
    Left = 24
    Top = 160
  end
  object taServItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SERVITEM set'
      '  COMMENT=:COMMENT'
      'where ID=:ID')
    DeleteSQL.Strings = (
      'delete from ServItem where ID=:ID')
    InsertSQL.Strings = (
      'execute procedure Stub(0)')
    RefreshSQL.Strings = (
      'select s.ID, s.INVID, s.SITEMID, s.SCOST, s.UID,'
      '       it.ART2ID, it.SZ SZID, it.ARTID, a2.ART2,'
      '       a2.ART, sz.NAME SZNAME, it.W, s.COMMENT'
      'from ServItem s, SItem it, Art2 a2, D_Sz sz'
      'where s.ID =:ID and'
      '      s.SITEMID=it.SITEMID and'
      '      it.ART2ID=a2.ART2ID and'
      '      it.SZ=sz.ID')
    SelectSQL.Strings = (
      'select s.ID, s.INVID, s.SITEMID, s.SCOST, s.UID,'
      '       it.ART2ID, it.SZ SZID, it.ARTID, a2.ART2,'
      '       a2.ART, sz.NAME SZNAME, it.W, s.COMMENT'
      'from ServItem s, SItem it, Art2 a2, D_Sz sz'
      'where s.INVID=:INVID and'
      '      s.SITEMID=it.SITEMID and'
      '      it.ART2ID=a2.ART2ID and'
      '      it.SZ=sz.ID '
      'order by s.ID')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = ConfirmDelete
    BeforeOpen = taServItemBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 164
    Top = 12
    poSQLINT64ToBCD = True
    object taServItemID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taServItemINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taServItemSITEMID: TFIBIntegerField
      FieldName = 'SITEMID'
    end
    object taServItemSCOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SCOST'
      currency = True
    end
    object taServItemUID: TFIBIntegerField
      DisplayLabel = #1059#1085'. '#1085#1086#1084#1077#1088
      FieldName = 'UID'
    end
    object taServItemART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taServItemSZID: TFIBIntegerField
      FieldName = 'SZID'
    end
    object taServItemARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object taServItemART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taServItemART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taServItemSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      EmptyStrToNull = True
    end
    object taServItemW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taServItemCOMMENT: TFIBStringField
      DisplayLabel = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
      FieldName = 'COMMENT'
      Size = 80
      EmptyStrToNull = True
    end
  end
  object dsrServItem: TDataSource
    DataSet = taServItem
    Left = 164
    Top = 60
  end
  object taVerificationList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update INV set'
      '  DOCNO=:DOCNO,'
      '  DOCDATE=:DOCDATE,'
      '  ISCLOSE=:ISCLOSE'
      'where INVID=:INVID')
    DeleteSQL.Strings = (
      'delete from INV where INVID=:INVID')
    InsertSQL.Strings = (
      
        'insert into INV(INVID, DOCNO, DOCDATE, USERID, ISCLOSE, SUPID, I' +
        'TYPE, ABD, AED)'
      
        'values (:INVID, :DOCNO, :DOCDATE, :USERID, :ISCLOSE, :SUPID, 22,' +
        ' :BD, :ED)')
    RefreshSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, USERID, USERNAME, SUPID, SUPNAME, '
      '       ISCLOSE, BD, ED'
      'from ActVerificationList_R(:INVID)')
    SelectSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, USERID, USERNAME, SUPID,'
      '  SUPNAME, ISCLOSE, BD, ED'
      'from ActVerificationList_S(:BD, :ED)'
      'order by DOCDATE desc')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = taVerificationListBeforeDelete
    BeforeOpen = taVerificationListBeforeOpen
    OnNewRecord = taVerificationListNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1057#1087#1080#1089#1086#1082' '#1072#1082#1090#1086#1074' '#1087#1088#1086#1074#1077#1088#1082#1080
    Left = 104
    Top = 112
    poSQLINT64ToBCD = True
    object taVerificationListINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taVerificationListDOCNO: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1072#1082#1090#1072
      FieldName = 'DOCNO'
    end
    object taVerificationListDOCDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1072#1082#1090#1072
      FieldName = 'DOCDATE'
    end
    object taVerificationListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taVerificationListISCLOSE: TFIBSmallIntField
      FieldName = 'ISCLOSE'
    end
    object taVerificationListSUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
    object taVerificationListSUPNAME: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
      FieldName = 'SUPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taVerificationListUSERNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'USERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taVerificationListBD: TFIBDateTimeField
      DisplayLabel = #1053#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072
      FieldName = 'BD'
    end
    object taVerificationListED: TFIBDateTimeField
      DisplayLabel = #1050#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072
      FieldName = 'ED'
    end
  end
  object dsrVerificationList: TDataSource
    DataSet = taVerificationListTolling
    Left = 104
    Top = 160
  end
  object taActVerification: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  case '
      '    it.InvSubTypeId '
      '    when 20 then -1000'
      '    when 21 then 1000'
      '     else it.InvSubTypeId'
      '  end, '
      '  it.ID, '
      '  it.INVID, '
      '  it.MATID, '
      '  it.INVSUBTYPEID, '
      '  it.Q, '
      '  it.W, '
      '  it.N, '
      '  st.Name INVSUBNAME, '
      '  m.Name MATNAME    '
      'from '
      '  AWItem it '
      '  left outer join D_InvSubType st on (it.InvSubTypeId = st.Id)'
      '  left outer join D_Mat m on (it.MatId = m.Id)'
      'where '
      '  it.INVID=:INVID'
      'order by '
      '  m.SORTIND, 1')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taActVerificationBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 192
    Top = 112
    poSQLINT64ToBCD = True
    object taActVerificationID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taActVerificationINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taActVerificationMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taActVerificationINVSUBTYPEID: TFIBIntegerField
      FieldName = 'INVSUBTYPEID'
    end
    object taActVerificationMATNAME: TFIBStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldName = 'MATNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taActVerificationINVSUBNAME: TFIBStringField
      DisplayLabel = #1042#1080#1076
      FieldName = 'INVSUBNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taActVerificationQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taActVerificationW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.###'
    end
    object taActVerificationN: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072
      FieldName = 'N'
      DisplayFormat = '0.###'
    end
  end
  object dsrActVerification: TDataSource
    DataSet = taActVerification
    Left = 192
    Top = 160
  end
  object taRequestList: TpFIBDataSet
    UpdateSQL.Strings = (
      
        'execute procedure REQUESTLIST_U(:INVID, :SUPID, :DOCNO, :DOCDATE' +
        ', :ABD,'
      
        '    :AED, :ANALIZ, :MEMO, :BOSSFIO, :BOSSPHONE, :OFIO, :OFIOPHON' +
        'E,'
      '    :POSTADDRESS, :PHONE, :FAX, :EMAIL, :INVCOLORID)'
      '    ')
    DeleteSQL.Strings = (
      'EXECUTE PROCEDURE F_DELETE(0)'
      '        ')
    InsertSQL.Strings = (
      'execute procedure F_INSERT(0)')
    RefreshSQL.Strings = (
      'SELECT'
      '    INVID, DOCNO, DOCDATE, ABD, AED, SUPID, SUPNAME, SUPCODE,'
      '    Q, MW, ANALIZ, MEMO, BOSSFIO, BOSSPHONE, OFIO, OFIOPHONE,'
      '    POSTADDRESS, PHONE, FAX, EMAIL, INVCOLORID, COLOR, SUPCOLOR'
      'FROM REQUESTLIST_R(:INVID) ')
    SelectSQL.Strings = (
      'SELECT'
      '    INVID, DOCNO, DOCDATE, ABD,'
      '    AED, SUPID, SUPNAME, SUPCODE,'
      '    Q, MW, ANALIZ, MEMO, BOSSFIO,'
      '    BOSSPHONE, OFIO, OFIOPHONE,'
      '    POSTADDRESS, PHONE, FAX, EMAIL, '
      '    INVCOLORID, COLOR, SUPCOLOR'
      'FROM REQUESTLIST_S(:BD, :ED) '
      'order by SUPCODE')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = taRequestListBeforeDelete
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 232
    Top = 12
    poSQLINT64ToBCD = True
    object taRequestListINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taRequestListDOCNO: TFIBIntegerField
      FieldName = 'DOCNO'
    end
    object taRequestListDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taRequestListABD: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1087#1086#1076#1072#1095#1080' '#1079#1072#1103#1074#1082#1080
      FieldName = 'ABD'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taRequestListAED: TFIBDateTimeField
      DisplayLabel = #1057#1088#1086#1082' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103
      FieldName = 'AED'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taRequestListSUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
    object taRequestListSUPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      FieldName = 'SUPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taRequestListQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taRequestListMW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'MW'
      DisplayFormat = '0.###'
    end
    object taRequestListANALIZ: TFIBSmallIntField
      DisplayLabel = #1040#1085#1072#1083#1080#1079
      FieldName = 'ANALIZ'
    end
    object taRequestListSUPCODE: TFIBStringField
      DisplayLabel = #1050#1086#1076
      FieldName = 'SUPCODE'
      Size = 60
      EmptyStrToNull = True
    end
    object taRequestListMEMO: TMemoField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077
      FieldName = 'MEMO'
      BlobType = ftMemo
      Size = 8
    end
    object taRequestListBOSSFIO: TFIBStringField
      FieldName = 'BOSSFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taRequestListBOSSPHONE: TFIBStringField
      FieldName = 'BOSSPHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object taRequestListOFIO: TFIBStringField
      FieldName = 'OFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taRequestListOFIOPHONE: TFIBStringField
      FieldName = 'OFIOPHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object taRequestListPOSTADDRESS: TFIBStringField
      FieldName = 'POSTADDRESS'
      Size = 400
      EmptyStrToNull = True
    end
    object taRequestListPHONE: TFIBStringField
      FieldName = 'PHONE'
      Size = 30
      EmptyStrToNull = True
    end
    object taRequestListFAX: TFIBStringField
      FieldName = 'FAX'
      Size = 30
      EmptyStrToNull = True
    end
    object taRequestListEMAIL: TFIBStringField
      FieldName = 'EMAIL'
      Size = 60
      EmptyStrToNull = True
    end
    object taRequestListINVCOLORID: TFIBIntegerField
      FieldName = 'INVCOLORID'
    end
    object taRequestListCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taRequestListSUPCOLOR: TFIBIntegerField
      FieldName = 'SUPCOLOR'
      Visible = False
    end
  end
  object dsrRequestList: TDataSource
    DataSet = taRequestList
    Left = 232
    Top = 60
  end
  object taRequest: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE BUYER_REQUEST'
      'SET '
      '    ID = ?ID,'
      '    INVID = ?INVID,'
      '    ARTID = ?ARTID,    '
      '    ART2ID = ?ART2ID,'
      '    SZID = ?SZID,'
      '    OPERID = ?OPERID,'
      '    Q = ?Q'
      'WHERE'
      '    ID = ?OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM BUYER_REQUEST'
      'WHERE'
      '        ID = ?OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO BUYER_REQUEST('
      '    ID,'
      '    INVID,'
      '    ARTID,'
      '    ART2ID,'
      '    SZID,    '
      '    OPERID,'
      '    Q'
      ')'
      'VALUES('
      '    ?ID,'
      '    ?INVID,'
      '    ?ARTID,'
      '    ?ART2ID,'
      '    ?SZID,'
      '    ?OPERID,'
      '    ?Q'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    INVID,'
      '    ARTID,'
      '    ART,'
      '    ART2ID,'
      '    ART2,'
      '    SZID,'
      '    SZNAME,'
      '    OPERID,'
      '    OPERNAME,'
      '    Q,'
      '    MW'
      'FROM'
      '    REQUEST_R(:ID)'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    INVID,'
      '    ARTID,'
      '    ART,'
      '    ART2ID,'
      '    ART2,'
      '    SZID,'
      '    SZNAME,'
      '    OPERID,'
      '    OPERNAME,'
      '    Q,'
      '    MW'
      'FROM'
      '    REQUEST_S(:INVID) '
      'order by ART, SZNAME')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = taRequestBeforeDelete
    BeforeOpen = taRequestBeforeOpen
    BeforePost = taRequestBeforePost
    OnNewRecord = taRequestNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 304
    Top = 12
    poSQLINT64ToBCD = True
    object taRequestID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taRequestINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taRequestARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object taRequestART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      OnValidate = taRequestARTValidate
      EmptyStrToNull = True
    end
    object taRequestART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taRequestART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taRequestSZID: TFIBIntegerField
      FieldName = 'SZID'
    end
    object taRequestSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      OnValidate = taRequestSZNAMEValidate
      EmptyStrToNull = True
    end
    object taRequestOPERID: TFIBStringField
      FieldName = 'OPERID'
      Size = 10
      EmptyStrToNull = True
    end
    object taRequestOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taRequestQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taRequestMW: TFIBFloatField
      DisplayLabel = #1057#1088#1077#1076#1085#1080#1081' '#1074#1077#1089
      FieldName = 'MW'
      DisplayFormat = '0.###'
    end
  end
  object dsrRequest: TDataSource
    DataSet = taRequest
    Left = 304
    Top = 60
  end
  object taZpTPS: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.ZPID, it.PSID, it.S, it.E, it.REJ,'
      '       d.NAME PSNAME, it.RET_PERCENT, it.REJ_PERCENT'
      'from ZItem_Ps it, D_Dep d'
      'where it.ZPID=:ZPID and '
      '      it.PsID=d.DEPID'
      'order by d.SortInd')
    AfterClose = CommitRetaining
    AfterOpen = CommitRetaining
    BeforeOpen = taZpTPSBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 272
    Top = 112
    poSQLINT64ToBCD = True
    object taZpTPSID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taZpTPSZPID: TFIBIntegerField
      FieldName = 'ZPID'
    end
    object taZpTPSPSID: TFIBIntegerField
      FieldName = 'PSID'
    end
    object taZpTPSS: TFIBFloatField
      DisplayLabel = #1057#1074#1077#1088#1093#1085#1086#1088#1084#1072#1090#1080#1074#1085#1099#1077
      FieldName = 'S'
      DisplayFormat = '0.###'
    end
    object taZpTPSE: TFIBFloatField
      DisplayLabel = #1069#1082#1086#1085#1086#1084#1080#1103
      FieldName = 'E'
      DisplayFormat = '0.###'
    end
    object taZpTPSREJ: TFIBFloatField
      DisplayLabel = #1041#1088#1072#1082
      FieldName = 'REJ'
      DisplayFormat = '0.###'
    end
    object taZpTPSPSNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'PSNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taZpTPSRET_PERCENT: TFIBFloatField
      DisplayLabel = #1055#1088#1086#1094#1077#1085#1090' '#1074#1086#1079#1074#1088#1072#1090#1072' '#1086#1090#1093#1086#1076#1086#1074
      FieldName = 'RET_PERCENT'
      DisplayFormat = '0.##%'
    end
    object taZpTPSREJ_PERCENT: TFIBFloatField
      DisplayLabel = #1055#1088#1086#1094#1077#1085#1090' '#1073#1088#1072#1082#1072
      FieldName = 'REJ_PERCENT'
      DisplayFormat = '0.##%'
    end
  end
  object dsrZpTPS: TDataSource
    DataSet = taZpTPS
    Left = 272
    Top = 160
  end
  object taExportDoc: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from EXPORT_DOC_EXT(:DOCID, :T)'
      '')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1064#1072#1087#1082#1072' '#1101#1082#1089#1087#1086#1088#1090#1080#1088#1082#1091#1077#1084#1086#1081' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    Left = 28
    Top = 220
    poSQLINT64ToBCD = True
  end
  object taExportDocItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from EXPORT_DOCITEM_EXT(:DOCID, :T)')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1055#1086#1079#1080#1094#1080#1080' '#1101#1082#1089#1087#1086#1088#1090#1080#1088#1091#1077#1084#1086#1081' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    Left = 28
    Top = 268
    poSQLINT64ToBCD = True
  end
  object taMBList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update INV set'
      '  ISCLOSE=:ISCLOSE,'
      '  ABD=:ABD,'
      '  AED=:AED'
      'where INVID=:INVID')
    DeleteSQL.Strings = (
      'delete from Inv where INVID=:INVID')
    InsertSQL.Strings = (
      
        'insert into INV(INVID,DOCNO,DOCDATE,USERID,ABD,AED,SELFCOMPID,IS' +
        'CLOSE,ITYPE)'
      
        'values(:INVID,:DOCNO,:DOCDATE,:USERID,:ABD,:AED,:SELFCOMPID,:ISC' +
        'LOSE,25)')
    RefreshSQL.Strings = (
      'select i.INVID, i.DOCNO, i.DOCDATE, i.USERID, i.ABD, i.AED,'
      '       m.FIO USERNAME, i.SELFCOMPID, i.ISCLOSE'
      'from Inv i, D_MOL m'
      'where i.INVID=:INVID and'
      '      i.USERID=m.MOLID')
    SelectSQL.Strings = (
      'select i.INVID, i.DOCNO, i.DOCDATE, i.USERID, i.ABD, i.AED,'
      '       m.FIO USERNAME, i.SELFCOMPID, i.ISCLOSE'
      'from Inv i, D_MOL m'
      'where i.ITYPE=25 and'
      '      i.USERID=m.MOLID and'
      '      i.ABD between :BD and :ED and'
      '      i.SELFCOMPID=:SELFCOMPID'
      'order by i.DOCDATE')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taMBListBeforeOpen
    OnNewRecord = taMBListNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1057#1087#1080#1089#1086#1082' '#1084#1072#1090#1077#1088#1080#1072#1083#1100#1085#1099#1093' '#1073#1072#1083#1072#1085#1089#1086#1074
    Left = 160
    Top = 220
    poSQLINT64ToBCD = True
    object taMBListINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taMBListDOCNO: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      FieldName = 'DOCNO'
    end
    object taMBListDOCDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taMBListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taMBListUSERNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'USERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taMBListABD: TFIBDateTimeField
      DisplayLabel = #1053#1072#1095#1072#1083#1086
      FieldName = 'ABD'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taMBListAED: TFIBDateTimeField
      DisplayLabel = #1050#1086#1085#1077#1094
      FieldName = 'AED'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taMBListSELFCOMPID: TFIBIntegerField
      FieldName = 'SELFCOMPID'
    end
    object taMBListISCLOSE: TFIBSmallIntField
      DefaultExpression = '0'
      FieldName = 'ISCLOSE'
    end
  end
  object dsrMBList: TDataSource
    DataSet = taMBList
    Left = 160
    Top = 264
  end
  object taMatBalance: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.INVID, it.MATID, it.W, it.DESCR,'
      '       m.NAME MATNAME'
      'from MatBalance it, D_Mat m'
      'where it.INVID=:INVID and'
      '      it.MATID=m.ID and'
      '      it.T=0'
      '      '
      '      ')
    BeforeOpen = taMatBalanceBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 228
    Top = 220
    poSQLINT64ToBCD = True
    object taMatBalanceID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taMatBalanceINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taMatBalanceMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taMatBalanceW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.##'
    end
    object taMatBalanceDESCR: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077
      FieldName = 'DESCR'
      Size = 200
      EmptyStrToNull = True
    end
    object taMatBalanceMATNAME: TFIBStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldName = 'MATNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrMatBalance: TDataSource
    DataSet = taMatBalance
    Left = 228
    Top = 264
  end
  object taAWMOList: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from Inv where INVID=:INVID')
    InsertSQL.Strings = (
      
        'insert into INV(INVID,DOCNO,DOCDATE,USERID,ABD,AED,SELFCOMPID,IS' +
        'CLOSE,ITYPE,SUPID,REF,Q,W,W2,COST,AKCIZ,UPPRICE,TRANS_PERCENT,UE' +
        ')'
      
        'values(:INVID,:DOCNO,:DOCDATE,:USERID,:ABD,:AED,:SELFCOMPID,:ISC' +
        'LOSE,26,:SUPID,:REF,0,0,0,0,0,0,0,0)')
    RefreshSQL.Strings = (
      'select i.INVID, i.DOCNO, i.DOCDATE, i.USERID, i.ABD, i.AED,'
      '       m.FIO USERNAME, i.SELFCOMPID, i.ISCLOSE, i.SUPID,'
      
        '       c.NAME SUPNAME, i.REF, i.Q, i.W, i.W2, i.Cost, i.Akciz, i' +
        '.UpPrice,'
      '       i.TRANS_PERCENT, i.UE'
      'from Inv i, D_MOL m, D_COMP c'
      'where i.INVID=:INVID and'
      '      i.USERID=m.MOLID and'
      '      i.SUPID=c.COMPID')
    SelectSQL.Strings = (
      'select i.INVID, i.DOCNO, i.DOCDATE, i.USERID, i.ABD, i.AED,'
      '       m.FIO USERNAME, i.SELFCOMPID, i.ISCLOSE, i.SUPID,'
      
        '       c.NAME SUPNAME, i.REF, i.Q, i.W, i.W2, i.Cost, i.Akciz, i' +
        '.UpPrice,'
      '       i.UE, i.TRANS_PERCENT'
      'from Inv i, D_MOL m, D_COMP c'
      'where i.ITYPE=26 and'
      '      i.USERID=m.MOLID and'
      '      i.DOCNO=:DOCNO and'
      '      i.SELFCOMPID=:SELFCOMPID and'
      '      i.SUPID=c.COMPID'
      'order by i.DOCDATE')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taAWMOListBeforeOpen
    OnNewRecord = taAWMOListNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 324
    Top = 216
    poSQLINT64ToBCD = True
    object taAWMOListINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taAWMOListDOCNO: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      FieldName = 'DOCNO'
    end
    object taAWMOListDOCDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taAWMOListUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taAWMOListABD: TFIBDateTimeField
      DisplayLabel = #1053#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072
      FieldName = 'ABD'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taAWMOListAED: TFIBDateTimeField
      DisplayLabel = #1054#1082#1086#1085#1095#1072#1085#1080#1077' '#1087#1077#1088#1080#1086#1076#1072
      FieldName = 'AED'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taAWMOListUSERNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'USERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taAWMOListSELFCOMPID: TFIBIntegerField
      FieldName = 'SELFCOMPID'
    end
    object taAWMOListISCLOSE: TFIBSmallIntField
      DefaultExpression = '0'
      FieldName = 'ISCLOSE'
    end
    object taAWMOListSUPID: TFIBIntegerField
      FieldName = 'SUPID'
    end
    object taAWMOListSUPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
      FieldName = 'SUPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taAWMOListREF: TFIBIntegerField
      FieldName = 'REF'
    end
    object taAWMOListQ: TFIBFloatField
      DisplayLabel = #1055#1088#1086#1076#1072#1085#1086
      FieldName = 'Q'
      currency = True
    end
    object taAWMOListW: TFIBFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090
      FieldName = 'W'
      currency = True
    end
    object taAWMOListW2: TFIBFloatField
      DisplayLabel = #1054#1087#1083#1072#1090#1072
      FieldName = 'W2'
      currency = True
    end
    object taAWMOListCOST: TFIBFloatField
      DisplayLabel = #1054#1089#1090#1072#1090#1086#1082' '#1086#1087#1083#1072#1090#1099
      FieldName = 'COST'
      currency = True
    end
    object taAWMOListAKCIZ: TFIBFloatField
      DisplayLabel = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090' '#1089#1087#1080#1089#1072#1085#1080#1103
      FieldName = 'AKCIZ'
      DisplayFormat = '0.0000'
    end
    object taAWMOListUPPRICE: TFIBFloatField
      DisplayLabel = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1086#1089#1090#1072#1090#1086#1082' '#1086#1087#1083#1072#1090#1099
      FieldName = 'UPPRICE'
      currency = True
    end
    object taAWMOListUE: TFIBFloatField
      DisplayLabel = #1042#1093#1086#1076'. '#1090#1086#1074#1072#1088
      FieldName = 'UE'
      currency = True
    end
    object taAWMOListTRANS_PERCENT: TFIBFloatField
      DisplayLabel = #1048#1089#1093'. '#1090#1086#1074#1072#1088
      FieldName = 'TRANS_PERCENT'
      currency = True
    end
  end
  object dsrAWMOList: TDataSource
    DataSet = taAWMOList
    Left = 324
    Top = 264
  end
  object taAWMO: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.INVID, it.MATID, it.IW, it.IC0, '
      '       it.S, it.SC0, it.R, it.RC0,  it.DW, it.DC0, '
      '       it.KC, it.NW, it.NC0, it.OW, it.OC0, m.NAME MATNAME,'
      '       it.NRW, it.NRC0'
      'from AWMatOff it, D_Mat m'
      'where it.MATID=m.ID and'
      '      it.INVID=:INVID'
      'order by m.SORTIND')
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taAWMOBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 400
    Top = 216
    poSQLINT64ToBCD = True
    object taAWMOID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taAWMOINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taAWMOMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taAWMOMATNAME: TFIBStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldName = 'MATNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taAWMOIW: TFIBFloatField
      DisplayLabel = #1042#1093#1086#1076' '#1074#1077#1089
      FieldName = 'IW'
      DisplayFormat = '0.###'
    end
    object taAWMOIC0: TFIBFloatField
      DisplayLabel = #1042#1093#1086#1076' '#1089#1090#1086#1080#1086#1084#1089#1090#1100
      FieldName = 'IC0'
      currency = True
    end
    object taAWMOS: TFIBFloatField
      DisplayLabel = #1055#1088#1086#1076#1072#1085#1086' '#1074#1077#1089
      FieldName = 'S'
      DisplayFormat = '0.###'
    end
    object taAWMOSC0: TFIBFloatField
      DisplayLabel = #1055#1088#1086#1076#1072#1085#1086' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SC0'
      currency = True
    end
    object taAWMOR: TFIBFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1074#1077#1089
      FieldName = 'R'
      DisplayFormat = '0.###'
    end
    object taAWMORC0: TFIBFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'RC0'
      currency = True
    end
    object taAWMODW: TFIBFloatField
      DisplayLabel = #1057#1087#1080#1089#1072#1085#1085#1099#1081' '#1074#1077#1089' '
      FieldName = 'DW'
      DisplayFormat = '0.###'
    end
    object taAWMODC0: TFIBFloatField
      DisplayLabel = #1057#1087#1080#1089#1072#1085#1085#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'DC0'
      currency = True
    end
    object taAWMOKC: TFIBFloatField
      DisplayLabel = #1055#1088#1086#1094#1077#1085#1090' '#1086#1087#1083#1072#1090#1099
      FieldName = 'KC'
      DisplayFormat = '0.0000'
    end
    object taAWMONW: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072' '#1087#1086#1090#1077#1088#1100
      FieldName = 'NW'
      DisplayFormat = '0.###'
    end
    object taAWMONC0: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1085#1086#1088#1084' '#1087#1086#1090#1077#1088#1100
      FieldName = 'NC0'
      currency = True
    end
    object taAWMOOW: TFIBFloatField
      DisplayLabel = #1048#1089#1093#1086#1076'. '#1074#1077#1089
      FieldName = 'OW'
      DisplayFormat = '0.###'
    end
    object taAWMOOC0: TFIBFloatField
      DisplayLabel = #1048#1089#1093#1086#1076'. '#1082#1086#1083'-'#1074#1086
      FieldName = 'OC0'
      currency = True
    end
    object taAWMONRW: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      FieldName = 'NRW'
      DisplayFormat = '0.###'
    end
    object taAWMONRC0: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072' '#1074#1086#1079#1074#1088#1072#1090#1072
      FieldName = 'NRC0'
      currency = True
    end
  end
  object dsrAWMO: TDataSource
    DataSet = taAWMO
    Left = 400
    Top = 264
  end
  object taTaskList: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE TASKS'
      'SET '
      '    TTYPE = :TTYPE,'
      '    N = :N,'
      '    DAT = :DAT,'
      '    DEPID = :DEPID,'
      '    DESCR = :DESCR,'
      '    CRBYUSERID = :CREATEBYUSERID,'
      '    BRIEFDESCR = :BRIEFDESCR,'
      '    K = :K'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    TASKS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO TASKS('
      '    ID,'
      '    TTYPE,'
      '    N,'
      '    DAT,'
      '    DEPID,'
      '    DESCR,'
      '    CRBYUSERID,'
      '    BRIEFDESCR,'
      '    K'
      ')'
      'VALUES('
      '    :ID,'
      '    :TTYPE,'
      '    :N,'
      '    :DAT,'
      '    :DEPID,'
      '    :DESCR,'
      '    :CREATEBYUSERID,'
      '    :BRIEFDESCR,'
      '    :K'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    TTYPE,'
      '    N,'
      '    DAT,'
      '    DEPID,'
      '    DEPNAME,'
      '    BRIEFDESCR,'
      '    DESCR,'
      '    CREATEBYUSERID,'
      '    CREATEBYUSER,'
      '    K,'
      '    COLOR'
      'FROM'
      '    TASKLIST_R(:ID)'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    TTYPE,'
      '    N,'
      '    DAT,'
      '    DEPID,'
      '    DEPNAME,'
      '    BRIEFDESCR,'
      '    DESCR,'
      '    CREATEBYUSERID,'
      '    CREATEBYUSER,'
      '    K,'
      '    COLOR'
      'FROM'
      '    TASKLIST_S(:BDATE,'
      '    :EDATE) ')
    AutoUpdateOptions.UpdateTableName = 'TASKS'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_TASKS_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taTaskListBeforeOpen
    OnCalcFields = taTaskListCalcFields
    OnNewRecord = taTaskListNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 520
    Top = 16
    poSQLINT64ToBCD = True
    object taTaskListID: TFIBIntegerField
      FieldName = 'ID'
      Visible = False
    end
    object taTaskListTTYPE: TFIBIntegerField
      FieldName = 'TTYPE'
      Visible = False
    end
    object taTaskListN: TFIBIntegerField
      DisplayLabel = #8470
      FieldName = 'N'
    end
    object taTaskListDAT: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'DAT'
    end
    object taTaskListDEPID: TFIBIntegerField
      FieldName = 'DEPID'
      Visible = False
    end
    object taTaskListDEPNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'DEPNAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taTaskListBRIEFDESCR: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089#1072#1085#1080#1077
      FieldName = 'BRIEFDESCR'
      Size = 200
      EmptyStrToNull = True
    end
    object taTaskListDESCR: TFIBMemoField
      FieldName = 'DESCR'
      Visible = False
      BlobType = ftMemo
      Size = 8
    end
    object taTaskListCREATEBYUSERID: TFIBIntegerField
      FieldName = 'CREATEBYUSERID'
      Visible = False
    end
    object taTaskListCREATEBYUSER: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'CREATEBYUSER'
      Size = 60
      EmptyStrToNull = True
    end
    object taTaskListK: TFIBFloatField
      DisplayLabel = #1050#1086#1101#1092'.'#1089#1083#1086#1078#1085#1086#1089#1090#1080
      FieldName = 'K'
    end
    object taTaskListRECNO: TIntegerField
      DisplayLabel = #8470' '#1087'/'#1087
      FieldKind = fkCalculated
      FieldName = 'RECNO'
      Calculated = True
    end
    object taTaskListCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
      Visible = False
    end
    object taTaskListTYPE: TStringField
      DisplayLabel = #1058#1080#1087
      FieldKind = fkLookup
      FieldName = 'TYPE'
      LookupDataSet = taLookupTaskType
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'TTYPE'
      Size = 30
      Lookup = True
    end
  end
  object dsrTaskList: TDataSource
    DataSet = taTaskList
    Left = 520
    Top = 64
  end
  object taLookupTaskType: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    NAME,'
      '    COLOR'
      'FROM'
      '    D_TASKTYPE'
      'ORDER BY NAME ')
    Transaction = dm.tr
    Database = dm.db
    Left = 608
    Top = 16
    poSQLINT64ToBCD = True
    object taLookupTaskTypeID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taLookupTaskTypeNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taLookupTaskTypeCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
  end
  object dsrLookupTaskType: TDataSource
    DataSet = taLookupTaskType
    Left = 608
    Top = 64
  end
  object taTaskOperations: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select TASKID, DTASKOPERID, TASKOPERNAME, TASK_OPERID, STATE, DE' +
        'PID, USERID, GIVEDATE, DONEDATE, USERNAME, DEPNAME'
      'from TASK_OPERATION_S ('
      '    :I_TASKID)')
    BeforeOpen = taTaskOperationsBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 520
    Top = 128
    poSQLINT64ToBCD = True
    object taTaskOperationsTASKID: TFIBIntegerField
      FieldName = 'TASKID'
    end
    object taTaskOperationsDTASKOPERID: TFIBIntegerField
      FieldName = 'DTASKOPERID'
    end
    object taTaskOperationsTASKOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'TASKOPERNAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taTaskOperationsTASK_OPERID: TFIBIntegerField
      FieldName = 'TASK_OPERID'
    end
    object taTaskOperationsSTATE: TFIBSmallIntField
      DisplayLabel = #1057#1090#1072#1090#1091#1089
      FieldName = 'STATE'
      OnGetText = taTaskOperationsSTATEGetText
    end
    object taTaskOperationsDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taTaskOperationsUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taTaskOperationsGIVEDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
      FieldName = 'GIVEDATE'
    end
    object taTaskOperationsDONEDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103
      FieldName = 'DONEDATE'
    end
    object taTaskOperationsUSERNAME: TFIBStringField
      FieldName = 'USERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taTaskOperationsDEPNAME: TFIBStringField
      DisplayLabel = #1048#1089#1087#1086#1083#1085#1080#1090#1077#1083#1100
      FieldName = 'DEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsTaskOperations: TDataSource
    DataSet = taTaskOperations
    Left = 520
    Top = 176
  end
  object dsLookupDeps: TDataSource
    DataSet = taLookupDeps
    Left = 608
    Top = 176
  end
  object taLookupDeps: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    DEPID,'
      '    COLOR,'
      '    SNAME,'
      '    NAME,'
      '    WH,'
      '    WP,'
      '    MO,'
      '    DEPTYPES,'
      '    REF,'
      '    SELFCOMPID'
      'FROM'
      '    D_DEP '
      'where selfcompid = :selfcompid'
      'order by sortind'
      '')
    Transaction = dm.tr
    Database = dm.db
    Left = 608
    Top = 128
    poSQLINT64ToBCD = True
    object taLookupDepsDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taLookupDepsCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taLookupDepsSNAME: TFIBStringField
      FieldName = 'SNAME'
      Size = 30
      EmptyStrToNull = True
    end
    object taLookupDepsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taLookupDepsWH: TFIBSmallIntField
      FieldName = 'WH'
    end
    object taLookupDepsWP: TFIBSmallIntField
      FieldName = 'WP'
    end
    object taLookupDepsMO: TFIBSmallIntField
      FieldName = 'MO'
    end
    object taLookupDepsDEPTYPES: TFIBIntegerField
      FieldName = 'DEPTYPES'
    end
    object taLookupDepsREF: TFIBIntegerField
      FieldName = 'REF'
    end
    object taLookupDepsSELFCOMPID: TFIBIntegerField
      FieldName = 'SELFCOMPID'
    end
  end
  object dsTaskOperSigns: TDataSource
    DataSet = taTaskOperSigns
    Left = 720
    Top = 176
  end
  object taTaskOperSigns: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE TASKOPER_SIGN'
      'SET '
      '    TASKOPERID = :TASKOPERID,'
      '    MOLID = :MOLID,'
      '    COMENT = :COMENT,'
      '    DAT = :DAT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    TASKOPER_SIGN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO TASKOPER_SIGN('
      '    ID,'
      '    TASKOPERID,'
      '    MOLID,'
      '    COMENT,'
      '    DAT'
      ')'
      'VALUES('
      '    :ID,'
      '    :TASKOPERID,'
      '    :MOLID,'
      '    :COMENT,'
      '    :DAT'
      ')')
    RefreshSQL.Strings = (
      'SELECT ID,'
      '             TASKOPERID,'
      '             MOLID,'
      '             COMENT,'
      '             DAT'
      '      FROM TASKOPER_SIGN'
      'where TASKOPER_SIGN.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT ID,'
      '             TASKOPERID,'
      '             MOLID,'
      '             COMENT,'
      '             DAT'
      '      FROM TASKOPER_SIGN'
      'where TASKOPERID=:TASK_OPERID')
    AutoUpdateOptions.UpdateTableName = 'TASKOPER_SIGN'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_TASKOPER_SIGN_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    OnNewRecord = taTaskOperSignsNewRecord
    Transaction = dm.tr
    Database = dm.db
    DataSource = dsTaskOperations
    Left = 720
    Top = 128
    poSQLINT64ToBCD = True
    object taTaskOperSignsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taTaskOperSignsTASKOPERID: TFIBIntegerField
      FieldName = 'TASKOPERID'
    end
    object taTaskOperSignsMOLID: TFIBIntegerField
      FieldName = 'MOLID'
    end
    object taTaskOperSignsDAT: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'DAT'
    end
    object taTaskOperSignsMOLNAME: TStringField
      DisplayLabel = #1054#1051
      FieldKind = fkLookup
      FieldName = 'MOLNAME'
      LookupDataSet = taLookupMol
      LookupKeyFields = 'MOLID'
      LookupResultField = 'FIO'
      KeyFields = 'MOLID'
      Size = 60
      Lookup = True
    end
    object taTaskOperSignsCOMENT: TFIBStringField
      DisplayLabel = #1050#1086#1084#1077#1085#1090#1072#1088#1080#1081
      FieldName = 'COMENT'
      Size = 400
      EmptyStrToNull = True
    end
  end
  object taDeps: TpFIBDataSet
    SelectSQL.Strings = (
      'select DEPID, NAME'
      'from D_DEP'
      'order by sortind')
    Transaction = dm.tr
    Database = dm.db
    Left = 520
    Top = 240
    poSQLINT64ToBCD = True
    object taDepsDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taDepsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object taOperToGive: TpFIBDataSet
    SelectSQL.Strings = (
      'select operid, TASKOPERNAME'
      'from TASK_OPER_FORGIVE_S (:taskid)')
    BeforeOpen = taOperToGiveBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 608
    Top = 240
    poSQLINT64ToBCD = True
  end
  object dsDeps: TDataSource
    DataSet = taDeps
    Left = 520
    Top = 296
  end
  object dsOperToGive: TDataSource
    DataSet = taOperToGive
    Left = 608
    Top = 296
  end
  object quAddOper: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      
        'execute procedure TASKOPER_ADD(:TASKID, :OPERID, :DAT, :DEPID, :' +
        'MOLID, :STATE)')
    Left = 824
    Top = 184
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object taLookupMol: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    MOLID,'
      '    FNAME,'
      '    LNAME,'
      '    MNAME,'
      '    FIO'
      'FROM'
      '    D_MOL '
      'order by SORTIND')
    Transaction = dm.tr
    Database = dm.db
    Left = 824
    Top = 128
    poSQLINT64ToBCD = True
    object taLookupMolMOLID: TFIBIntegerField
      FieldName = 'MOLID'
    end
    object taLookupMolFNAME: TFIBStringField
      FieldName = 'FNAME'
      EmptyStrToNull = True
    end
    object taLookupMolLNAME: TFIBStringField
      FieldName = 'LNAME'
      EmptyStrToNull = True
    end
    object taLookupMolMNAME: TFIBStringField
      FieldName = 'MNAME'
      EmptyStrToNull = True
    end
    object taLookupMolFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object taTaskImg: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE TASK_IMAGE'
      'SET '
      '    TASKID = :TASKID,'
      '    NAME = :NAME,'
      '    IMG = :IMG'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    TASK_IMAGE'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO TASK_IMAGE('
      '    ID,'
      '    TASKID,'
      '    NAME,'
      '    IMG'
      ')'
      'VALUES('
      '    :ID,'
      '    :TASKID,'
      '    :NAME,'
      '    :IMG'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    ID,'
      '    TASKID,'
      '    NAME,'
      '    IMG'
      'FROM'
      '    TASK_IMAGE '
      'WHERE'
      ' TASK_IMAGE.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    ID,'
      '    TASKID,'
      '    NAME,'
      '    IMG'
      'FROM'
      '    TASK_IMAGE '
      'WHERE TASKID = :TASK_ID')
    AutoUpdateOptions.UpdateTableName = 'TASK_IMAGE'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_TASK_IMAGE_ID'
    AutoUpdateOptions.WhenGetGenID = wgOnNewRecord
    BeforeOpen = taTaskImgBeforeOpen
    OnNewRecord = taTaskImgNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 720
    Top = 240
    poSQLINT64ToBCD = True
    object taTaskImgID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taTaskImgTASKID: TFIBIntegerField
      FieldName = 'TASKID'
    end
    object taTaskImgNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 50
      EmptyStrToNull = True
    end
    object taTaskImgIMG: TFIBBlobField
      FieldName = 'IMG'
      Size = 8
    end
  end
  object dsTaskImg: TDataSource
    DataSet = taTaskImg
    Left = 720
    Top = 296
  end
  object frOLEExcelExport1: TfrOLEExcelExport
    CellsAlign = False
    CellsFillColor = False
    CellsFontColor = False
    CellsFontName = False
    CellsFontSize = False
    CellsFontStyle = False
    Left = 160
    Top = 320
  end
  object taActWorkCommon: TpFIBDataSet
    UpdateSQL.Strings = (
      'update MatItem set'
      '  N=:N,'
      '  MATID=:MATID,'
      '  ADDBYUSER=:ADDBYUSER,'
      '  UW=:UW,'
      '  PRICE0=:PRICE0  '
      'where ID=:ID')
    DeleteSQL.Strings = (
      'delete from MatItem where ID=:ID')
    InsertSQL.Strings = (
      
        'insert into MatItem(ID, INVID, MATID, N, GETW, RETW, NW, ADDBYUS' +
        'ER, UW, CLIENTMAT, PRICE0)'
      
        'values (:ID, :INVID, :MATID, :N, :GETW, :RETW, :NW, :ADDBYUSER, ' +
        ':UW, :CLIENTMAT, 0)')
    SelectSQL.Strings = (
      'select ab.MATID,'
      '       ab.UW,'
      '       ab.N,'
      '       ab.CLIENTMAT,'
      '       SUM(ab.GETW) GETW,'
      '       SUM(ab.RETW) RETW,'
      '       SUM(ab.NW) NW, '
      '       SUM(ab.COST) COST,'
      '       SUM(ab.PRICE0) PRICE0'
      'from ACTWORKLIST_S(:BD, :ED) a,'
      '     ActWork_S(a.invid) ab'
      'where a.compid = :Company$ID'
      'group by 1,2,3,4'
      '      '
      '      ')
    BeforeOpen = taActWorkCommonBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 64
    Top = 380
    poSQLINT64ToBCD = True
    object FIBStringField1: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object FIBFloatField1: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072
      FieldName = 'N'
    end
    object FIBFloatField2: TFIBFloatField
      DisplayLabel = #1055#1088#1080#1085#1103#1090#1086
      FieldName = 'GETW'
      DisplayFormat = '0.###'
    end
    object FIBFloatField3: TFIBFloatField
      DisplayLabel = #1042#1099#1076#1072#1085#1086
      FieldName = 'RETW'
      DisplayFormat = '0.###'
    end
    object FIBFloatField4: TFIBFloatField
      DisplayLabel = #1042#1077#1089' '#1087#1086' '#1085#1086#1088#1084#1077
      FieldName = 'NW'
      DisplayFormat = '0.###'
    end
    object StringField1: TStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldKind = fkLookup
      FieldName = 'MATNAME'
      LookupDataSet = dm.taMat
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'MATID'
      Size = 40
      Lookup = True
    end
    object FIBSmallIntField2: TFIBSmallIntField
      DisplayLabel = #1045#1048
      FieldName = 'UW'
    end
    object FIBSmallIntField3: TFIBSmallIntField
      DisplayLabel = #1042#1083#1072#1076#1077#1083#1077#1094
      FieldName = 'CLIENTMAT'
    end
    object FIBFloatField5: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'COST'
      currency = True
    end
    object FIBFloatField6: TFIBFloatField
      DisplayLabel = #1059#1095#1077#1090#1085#1072#1103' '#1094#1077#1085#1072
      FieldName = 'PRICE0'
      currency = True
    end
  end
  object taActWorkListCommon: TpFIBDataSet
    DeleteSQL.Strings = (
      '')
    InsertSQL.Strings = (
      ''
      '')
    RefreshSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'select'
      '  COMPNAME,'
      '  COMPBOSSFIO,'
      '  NO_CONTRACT,'
      '  BD_CONTRACT,'
      '  SELFCOMPNAME,'
      '  SELFBOSSFIO, '
      '  SUM(COST) COST'
      'from ACTWORKLIST_S(:BD, :ED)'
      'where compid = :Company$ID'
      'group by 1,2,3,4,5,6')
    BeforeOpen = taActWorkListCommonBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 64
    Top = 436
    poSQLINT64ToBCD = True
    object FIBStringField2: TFIBStringField
      DisplayLabel = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      FieldName = 'COMPNAME'
      Size = 80
      EmptyStrToNull = True
    end
    object FIBStringField6: TFIBStringField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1076#1086#1075#1086#1074#1086#1088#1072
      FieldName = 'NO_CONTRACT'
      EmptyStrToNull = True
    end
    object FIBDateTimeField5: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1076#1086#1075#1086#1074#1086#1088#1072
      FieldName = 'BD_CONTRACT'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object FIBStringField7: TFIBStringField
      DisplayLabel = #1060#1048#1054' '#1087#1086#1076#1088#1103#1076#1095#1080#1082#1072
      FieldName = 'SELFBOSSFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object FIBStringField8: TFIBStringField
      DisplayLabel = #1060#1048#1054' '#1079#1072#1082#1072#1079#1095#1080#1082#1072
      FieldName = 'COMPBOSSFIO'
      Size = 60
      EmptyStrToNull = True
    end
    object FIBStringField9: TFIBStringField
      FieldName = 'SELFCOMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object FIBFloatField7: TFIBFloatField
      DisplayLabel = #1059#1089#1083#1091#1075#1080
      FieldName = 'COST'
      currency = True
    end
  end
  object taVerificationListTolling: TpFIBDataSet
    UpdateSQL.Strings = (
      'update inv set'
      '  DocNo = :DocNo,'
      '  DocDate = :DocDate,'
      '  SupID = :SupID,'
      '  ABD = :ABD,'
      '  AED = :AED'
      'where InvID = :InvID')
    DeleteSQL.Strings = (
      'delete from inv where InvID = :InvID')
    InsertSQL.Strings = (
      'insert into '
      
        'Inv(InvID, DocNo, DocDate, IType, IsClose, SupID, SelfcompID, AB' +
        'D, AED, UserID, CompID)'
      
        'values(:InvID, :DocNo, :DocDate, 27, 0, :SupID, :SelfCompID, :AB' +
        'D, :AED, :USERID, :CompID)')
    RefreshSQL.Strings = (
      'select '
      
        '  i.InvID, i.DocNo, i.DocDate, i.UserID, dm.FIO UserName, i.SupI' +
        'D, i.SelfCompID, '
      
        '  i.ABD, i.AED, i.IsClose, i.IType, dc.Name SupName, d.Name Self' +
        'CompName'
      'from '
      '  Inv i'
      '  left outer join D_Comp dc on (dc.compid = i.supid)'
      '  left outer join D_Comp d on (d.compid = i.selfcompid)'
      '  left outer join d_mol dm on (i.UserID = dm.MolID)'
      'where '
      '  i.IType = 27 and '
      '  i.SelfCompId = :SelfCompID')
    SelectSQL.Strings = (
      'select '
      
        '  i.InvID, i.DocNo, i.DocDate, i.UserID, dm.FIO UserName, i.SupI' +
        'D, i.SelfCompID, '
      
        '  i.ABD, i.AED, i.IsClose, i.IType, dc.Name SupName, d.Name Self' +
        'CompName'
      'from '
      '  Inv i'
      '  left outer join D_Comp dc on (dc.compid = i.supid)'
      '  left outer join D_Comp d on (d.compid = i.selfcompid)'
      '  left outer join d_mol dm on (i.UserID = dm.MolID)'
      'where '
      '  i.IType = 27 and '
      '  i.SelfCompId = :SelfCompID')
    AfterDelete = taVerificationListTollingAfterDelete
    AfterPost = taVerificationListTollingAfterPost
    BeforeOpen = taVerificationListTollingBeforeOpen
    OnNewRecord = taVerificationListTollingNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 400
    Top = 72
    object taVerificationListTollingINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taVerificationListTollingDOCNO: TFIBIntegerField
      DisplayLabel = #1040#1082#1090'|'#1053#1086#1084#1077#1088
      DisplayWidth = 10
      FieldName = 'DOCNO'
    end
    object taVerificationListTollingDOCDATE: TFIBDateTimeField
      DisplayLabel = #1040#1082#1090'|'#1044#1072#1090#1072
      DisplayWidth = 15
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taVerificationListTollingUSERID: TFIBIntegerField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      DisplayWidth = 22
      FieldName = 'USERID'
    end
    object taVerificationListTollingSUPID: TFIBIntegerField
      DisplayWidth = 15
      FieldName = 'SUPID'
    end
    object taVerificationListTollingSELFCOMPID: TFIBIntegerField
      DisplayLabel = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      DisplayWidth = 20
      FieldName = 'SELFCOMPID'
    end
    object taVerificationListTollingABD: TFIBDateTimeField
      DisplayLabel = #1055#1077#1088#1080#1086#1076'|'#1053#1072#1095#1072#1083#1086
      DisplayWidth = 15
      FieldName = 'ABD'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taVerificationListTollingAED: TFIBDateTimeField
      DisplayLabel = #1055#1077#1088#1080#1086#1076'|'#1050#1086#1085#1077#1094
      DisplayWidth = 15
      FieldName = 'AED'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taVerificationListTollingISCLOSE: TFIBSmallIntField
      FieldName = 'ISCLOSE'
    end
    object taVerificationListTollingITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object taVerificationListTollingUSERNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'USERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taVerificationListTollingSUPNAME: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
      FieldName = 'SUPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taVerificationListTollingSELFCOMPNAME: TFIBStringField
      FieldName = 'SELFCOMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsVerificationListTolling: TDataSource
    AutoEdit = False
    DataSet = taVerificationListTolling
    Left = 400
    Top = 120
  end
end
