unit SInv_Det_SIList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, ExtCtrls, StdCtrls, FIBDatabase, pFIBDatabase,
  DB, FIBDataSet, pFIBDataSet, Menus, ListAncestor,
  DBGridEhGrouping, GridsEh, rxSpeedbar;

type
  Tfm_SInv_Det_SIList = class(TForm)
    Panel1: TPanel;
    bt_Ok: TButton;
    bt_Cancel: TButton;
    DataSet_SIList: TpFIBDataSet;
    DataSource_SIList: TDataSource;
    tb2: TSpeedBar;
    laDep: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    spitDep: TSpeedItem;
    spitPeriod: TSpeedItem;
    DBGrid_DocList: TDBGridEh;
    ppm_Dep: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    procedure DataSet_SIListBeforeOpen(DataSet: TDataSet);
    procedure N1Click(Sender: TObject);
    procedure spitPeriodClick(Sender: TObject);
  private
    db: TpFIBDataBase;
    Executed: Boolean;
    FBeginDate: TDateTime;
    FEndDate: TDateTime;

    procedure Execute;
    procedure AllRefresh;
    procedure ShowPeriod;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    property DataBase: TpFIBDataBase write db;

    function ShowModal: Integer; override;
    procedure DepClick(Sender : TObject);
  end;

implementation

uses MainData, dbUtil, DictData, SIEl, fmUtils, PrintData, InvData, INVCopy,
     Period;
{$R *.dfm}

procedure Tfm_SInv_Det_SIList.AllRefresh;
begin
  try
    Screen.Cursor := crHourGlass;
    DataSet_SIList.Close;
    DataSet_SIList.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

constructor Tfm_SInv_Det_SIList.Create(AOwner: TComponent);
var
  i, j : integer;
  Item : TMenuItem;
  s: string;
begin
  inherited;

  //���������� PopupMenu �� FDepInfo;
  j := 0;
  for i:=0 to Pred(dm.CountDep) do
  begin
    if (dm.DepInfo[i].DepTypes and $2)=$2 then
    begin
      Item := TMenuItem.Create(ppm_Dep);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepClick;
      Item.RadioItem := True;
      inc(j);
      if j>30 then
      begin
        Item.Break := mbBreak;
        j:=0;
      end;
      ppm_Dep.Items.Add(Item);
    end;
  end;

  dm.CurrDep := -1;
  laDep.Caption := '��� ������';

  FBeginDate := dm.BeginDate;
  FEndDate := dm.EndDate;

  ShowPeriod;
end;

destructor Tfm_SInv_Det_SIList.Destroy;
begin

  inherited;
end;

procedure Tfm_SInv_Det_SIList.Execute;
begin
  DataSet_SIList.DataBase := db;

  AllRefresh;
  Executed := True;
end;

function Tfm_SInv_Det_SIList.ShowModal: Integer;
begin
  if not Executed then
    Execute;

  Result := inherited ShowModal;  
end;

procedure Tfm_SInv_Det_SIList.DataSet_SIListBeforeOpen(DataSet: TDataSet);
begin
  with DataSet_SIList do
  begin
    if dm.CurrDep = -1 then
    begin
      ParamByName('DepID1').AsInteger := -MAXINT;
      ParamByName('DepID2').AsInteger := MAXINT;
    end
    else begin
      ParamByName('DepID1').AsInteger := dm.CurrDep;
      ParamByName('DepID2').AsInteger := dm.CurrDep;
    end;
    ParamByName('BD').AsDateTime := FBeginDate;
    ParamByName('ED').AsDateTime := FEndDate;
  end;
end;

procedure Tfm_SInv_Det_SIList.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  laDep.Caption := TMenuItem(Sender).Caption;
  AllRefresh;
end;

procedure Tfm_SInv_Det_SIList.N1Click(Sender: TObject);
begin
  DepClick(Sender);
end;

procedure Tfm_SInv_Det_SIList.spitPeriodClick(Sender: TObject);
begin
  if ShowPeriodForm(FBeginDate, FEndDate) then
  begin
    ShowPeriod;
    AllRefresh;
  end;
end;

procedure Tfm_SInv_Det_SIList.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(FBeginDate) + ' �� ' +DateToStr(FEndDate);
end;

end.
