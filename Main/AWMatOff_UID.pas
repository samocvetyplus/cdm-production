unit AWMatOff_UID;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList,  ImgList,  ExtCtrls,
  ComCtrls, Grids, DBGridEh, DB, FIBDataSet, pFIBDataSet, DBGridEhGrouping,
  GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmAWMatOff_UID = class(TfmAncestor)
    DBGridEh1: TDBGridEh;
    taAWMatOff_UID: TpFIBDataSet;
    taAWMatOff_UIDID: TFIBIntegerField;
    taAWMatOff_UIDUID: TFIBIntegerField;
    taAWMatOff_UIDINVID: TFIBIntegerField;
    taAWMatOff_UIDCOST0: TFIBFloatField;
    taAWMatOff_UIDDOCDATE: TFIBDateTimeField;
    taAWMatOff_UIDDOCNO: TFIBIntegerField;
    dsrAWMatOff_UID: TDataSource;
    taAWMatOff_UIDW: TFIBFloatField;
    taAWMatOff_UIDPRICE0: TFIBFloatField;
    taAWMatOff_UIDN: TFIBFloatField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmAWMatOff_UID: TfmAWMatOff_UID;

implementation

uses DictData, AWMatOff, Data, dbUtil;

{$R *.dfm}

procedure TfmAWMatOff_UID.FormCreate(Sender: TObject);
begin
  taAWMatOff_UID.ParamByName('ID').AsInteger := dmData.taAWMOID.AsInteger;
  taAWMatOff_UID.Open;
end;

procedure TfmAWMatOff_UID.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  CloseDataSet(taAWMatOff_UID);
end;

end.
