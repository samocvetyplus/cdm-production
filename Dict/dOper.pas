unit dOper;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DictAncestor, RxPlacemnt, ImgList, Grids, DBGrids, RXDBCtrl,
  ComCtrls, ExtCtrls, db, ActnList, Buttons, DBGridEh, TB2Item,
  Menus, Variants, StdCtrls, TB2Dock, TB2Toolbar, Mask, DBCtrlsEh,
  DBLookupEh, FIBDataSet, pFIBDataSet, DBGridEhGrouping, rxSpeedbar,
  GridsEh;

type
  TfmOper = class(TfmDictAncestor)
    spitIn: TSpeedItem;
    SpeedItem5: TSpeedItem;
    acIn: TAction;
    acOut: TAction;
    spitTails: TSpeedItem;
    acTails: TAction;
    spitRej: TSpeedItem;
    acRej: TAction;
    acTransform: TAction;
    SpeedButton1: TSpeedButton;
    acPrintOrderNorma: TAction;
    SpeedItem6: TSpeedItem;
    ppPrint: TTBPopupMenu;
    TBItem1: TTBItem;
    TBDock1: TTBDock;
    tlbrHistory: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    lcbxNorma: TDBLookupComboboxEh;
    TBControlItem2: TTBControlItem;
    TBControlItem3: TTBControlItem;
    Label2: TLabel;
    TBSeparatorItem1: TTBSeparatorItem;
    lcbxZp: TDBLookupComboboxEh;
    TBControlItem4: TTBControlItem;
    TBItem2: TTBItem;
    acOpenNormaHistory: TAction;
    acOpenZpHistory: TAction;
    TBItem4: TTBItem;
    acCreateNormaOrder: TAction;
    acCreateZpOrder: TAction;
    TBItem3: TTBItem;
    TBItem5: TTBItem;
    taNHDate: TpFIBDataSet;
    dsrNHDate: TDataSource;
    taZpHDate: TpFIBDataSet;
    dsrZpHDate: TDataSource;
    taZpHDateHDATE: TFIBDateTimeField;
    taNHDateHDATE: TFIBDateTimeField;
    TBDock2: TTBDock;
    tlbrOrderDate: TTBToolbar;
    TBControlItem5: TTBControlItem;
    Label3: TLabel;
    TBSeparatorItem2: TTBSeparatorItem;
    TBControlItem7: TTBControlItem;
    Label4: TLabel;
    acSetNormaDate: TAction;
    acSetZpRateDate: TAction;
    edNormaDate: TDBDateTimeEditEh;
    TBControlItem9: TTBControlItem;
    edZpRateDate: TDBDateTimeEditEh;
    TBControlItem10: TTBControlItem;
    function grGetCellCheckBox(Sender: TObject; Field: TField; var StateCheckBox: Integer): Boolean;
    procedure acInExecute(Sender: TObject);
    procedure acOutExecute(Sender: TObject);
    procedure acTailsExecute(Sender: TObject);
    procedure acRejExecute(Sender: TObject);
    procedure acTransformExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPrintOrderNormaExecute(Sender: TObject);
    procedure acPrintOrderNormaUpdate(Sender: TObject);
    procedure acOpenNormaHistoryExecute(Sender: TObject);
    procedure acOpenNormaHistoryUpdate(Sender: TObject);
    procedure acOpenZpHistoryUpdate(Sender: TObject);
    procedure acOpenZpHistoryExecute(Sender: TObject);
    procedure acCreateNormaOrderExecute(Sender: TObject);
    procedure acCreateNormaOrderUpdate(Sender: TObject);
    procedure acCreateZpOrderExecute(Sender: TObject);
    procedure acCreateZpOrderUpdate(Sender: TObject);
    procedure NormaUpdateData(Sender: TObject; var Text: String;
      var Value: Variant; var UseText, Handled: Boolean);
    procedure ZpUpdateData(Sender: TObject; var Text: String;
      var Value: Variant; var UseText, Handled: Boolean);
    procedure edNormaDateButtonClick(Sender: TObject;
      var Handled: Boolean);
    procedure edNormaDateChange(Sender: TObject);
    procedure edZpRateDateButtonClick(Sender: TObject;
      var Handled: Boolean);
    procedure edZpRateDateChange(Sender: TObject);
  private
    FModifyNormaDate, FModifyZpDate : boolean;
    FModifyNorma, FModifyZp: boolean;
  end;

implementation

uses DictData, dOperIn, dOperOut, dOperTails, dOperRej, dTransMat,
  PrintData, eOperOrder, OperHistory, dbUtil, MsgDialog;

{$R *.DFM}

function TfmOper.grGetCellCheckBox(Sender: TObject; Field: TField;
  var StateCheckBox: Integer): Boolean;
begin
  Result:=(Field.FieldName='NQ')or(Field.FieldName='LAST');
end;

procedure TfmOper.acInExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: ''; sTable: 'D_OperIn'; sWhere: '');
begin
  ShowDictForm(TfmOperIn, dm.dsrOperIn, Rec, '����� ��� �������� '+dm.taOperOPERATION.AsString);
end;

procedure TfmOper.acOutExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'SEMISID'; sTable: 'D_OperOut'; sWhere: '');
begin
  ShowDictForm(TfmOperOut, dm.dsrOperOut, Rec, '��������� �������� '+dm.taOperOPERATION.AsString);
end;

procedure TfmOper.acTailsExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'SEMISID'; sTable: 'D_OperTails'; sWhere: '');
begin
  ShowDictForm(TfmOperTails, dm.dsrOperTail, Rec, '������ �������� '+dm.taOperOPERATION.AsString);
end;

procedure TfmOper.acRejExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'SEMISID'; sTable: 'D_OperRej'; sWhere: '');
begin
  ShowDictForm(TfmOperRej, dm.dsrOperRej, Rec, '���� �������� '+dm.taOperOPERATION.AsString);
end;

procedure TfmOper.acTransformExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'MATNAME'; sTable: 'TransMat'; sWhere: '');
begin
  // ��� �������� ��������� �� ������ �������� ����� ��������� ����������� � ������ ������
  ShowDictForm(TfmTransMat, dm.dsrTransMat, Rec, '���������, ������������� � ���� �������� '+dm.taOperOPERATION.AsString+' � ������');
end;

procedure TfmOper.FormCreate(Sender: TObject);
begin
  FModifyNorma := False;
  FModifyZp := False;
  FModifyZpDate := False;
  FModifyNormaDate := False;
  inherited;
  gridDict.FieldColumns['RATE'].Visible := False;
  gridDict.FieldColumns['ZT'].Visible := False;
  gridDict.FieldColumns['N'].Visible := False;
  gridDict.FieldColumns['N1'].Visible := False;
  gridDict.FieldColumns['NQ'].Visible := False;
  gridDict.FieldColumns['TAILSNAME'].Visible := False;
  gridDict.FieldColumns['ZBONUS'].Visible := False;

  if(dm.User.Profile = -1)or(dm.User.Profile = 3)or(dm.User.Profile = 1) then
  begin
    tlbrHistory.Visible := True;
    tlbrOrderDate.Visible := True;
    gridDict.FieldColumns['HIST_RATE'].Visible := True;
    gridDict.FieldColumns['HIST_ZT'].Visible := True;
    gridDict.FieldColumns['HIST_N'].Visible := True;
    gridDict.FieldColumns['HIST_N1'].Visible := True;
    gridDict.FieldColumns['HIST_NQ'].Visible := True;
    gridDict.FieldColumns['HIST_TAILSNAME'].Visible := True;
    gridDict.FieldColumns['HIST_ZBONUS'].Visible := True;
  end
  else begin
    tlbrHistory.Visible := False;
    tlbrOrderDate.Visible := False;
    gridDict.FieldColumns['HIST_RATE'].Visible := False;
    gridDict.FieldColumns['HIST_ZT'].Visible := False;
    gridDict.FieldColumns['HIST_N'].Visible := False;
    gridDict.FieldColumns['HIST_N1'].Visible := False;
    gridDict.FieldColumns['HIST_NQ'].Visible := False;
    gridDict.FieldColumns['HIST_TAILSNAME'].Visible := False;
    gridDict.FieldColumns['HIST_ZBONUS'].Visible := False;
  end;
  OpenDataSets([taNHDate, taZpHDate]);
  //ppPrint.Skin := dm.ppSkin;
  if VarIsNull(dm.Rec.NormaOrderDate) then
  begin
    edNormaDate.Value := Null;
    edNormaDate.EditButton.Visible := True;
  end
  else begin
    edNormaDate.Value := dm.Rec.NormaOrderDate;
    edNormaDate.EditButton.Visible := False;
  end;
  if VarIsNull(dm.Rec.ZpRateOrderDate) then
  begin
    edZpRateDate.Value := Null;
    edZpRateDate.EditButton.Visible := True;
  end
  else begin
    edZpRateDate.Value := dm.Rec.ZpRateOrderDate;
    edZpRateDate.EditButton.Visible := False;
  end;
end;

procedure TfmOper.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i: integer;
begin
  if FModifyNorma then
    case  MessageDialog('���� �������� ����������� ���������� ��������!'#13+
       '������� ����� ������?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
      mrYes : acCreateNormaOrder.Execute;
      mrNo : ;
      mrCancel : SysUtils.Abort;
    end;
  if FModifyZp then
    case MessageDialog('���� �������� ������ ���������� �����!'#13+
      '������� ������?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
      mrYes : acCreateZpOrder.Execute;
      mrNo : ;
      mrCancel: SysUtils.Abort;
    end;
  inherited;
  dm.LoadArtSL(OPER_DICT);
  dm.LoadArtSL(OPERNOGOOD_DICT);
  CloseDataSets([taNHDate, taZpHDate]);
  // ������� ����� - �������
  for i:=Pred(Screen.FormCount) downto 0 do
    if Screen.Forms[i].InheritsFrom(TfmOperHistory) then
    begin
      TfmOperHistory(Screen.Forms[i]).Close;
      TfmOperHistory(Screen.Forms[i]).Free;
    end;
end;

procedure TfmOper.acPrintOrderNormaExecute(Sender: TObject);
begin
  inherited;

  try
   fmOperOrder:=TfmOperOrder.Create(self);
   if fmOperOrder.ShowModal=mrOk then  begin
    dmPrint.FDate:=fmOperOrder.edDate.Value;
    dmPrint.FNo:=fmOperOrder.edNo.Text;
    dmPrint.PrintDocumentA(dkOperOrder,null);
   end;

  finally
   fmOperOrder.Free;
  end;
end;

procedure TfmOper.acPrintOrderNormaUpdate(Sender: TObject);
begin
  acPrintOrderNorma.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmOper.acOpenNormaHistoryExecute(Sender: TObject);
begin
  ShowOperHistory(0, lcbxNorma.KeyValue);
end;

procedure TfmOper.acOpenNormaHistoryUpdate(Sender: TObject);
begin
  acOpenNormaHistory.Enabled := not VarIsNull(lcbxNorma.KeyValue);
end;

procedure TfmOper.acOpenZpHistoryUpdate(Sender: TObject);
begin
  acOpenZpHistory.Enabled := not VarIsNull(lcbxZp.KeyValue);
end;

procedure TfmOper.acOpenZpHistoryExecute(Sender: TObject);
begin
  ShowOperHistory(1, lcbxZp.KeyValue);
end;

procedure TfmOper.acCreateNormaOrderExecute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  PostDataSet(dm.taOper);
  Bookmark := dm.taOper.GetBookmark;
  dm.taOper.DisableScrollEvents;
  dm.taOper.DisableControls;
  try
    ExecSQL('execute procedure Create_History_OperNormaOrder("'+VarToStr(dm.Rec.NormaOrderDate)+'")', dm.quTmp);
    dm.taOper.First;
    while not dm.taOper.Eof do
    begin
      dm.taOper.Edit;
      dm.taOperN.AsFloat := dm.taOperHIST_N.AsFloat;
      dm.taOperN1.AsFloat := dm.taOperHIST_N1.AsFloat;
      dm.taOperNQ.AsInteger := dm.taOperHIST_NQ.AsInteger;
      dm.taOperTAILS.AsInteger := dm.taOperHIST_TAILS.AsInteger;
      dm.taOper.Post;
      dm.taOper.Next;
    end;
    FModifyNorma := False;
    ReOpenDataSet(taNHDate);
    ExecSQL('update D_Comp set NORMAORDERDATE="'+DateTimeToStr(Now)+'" '+
      'where CompId='+IntToStr(dm.User.SelfCompId), dm.quTmp);
    dm.UpdateNormaOrderDate(Now);
    edNormaDate.EditButton.Visible := False;
    edNormaDate.Value := Now;
  finally
    dm.taOper.GotoBookmark(Bookmark);
    dm.taOper.EnableScrollEvents;
    dm.taOper.EnableControls;
  end;
end;

procedure TfmOper.acCreateNormaOrderUpdate(Sender: TObject);
begin
  acCreateNormaOrder.Enabled := FModifyNorma and (not VarIsNull(dm.Rec.NormaOrderDate));
end;

procedure TfmOper.acCreateZpOrderExecute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  PostDataSet(dm.taOper);
  Bookmark := dm.taOper.GetBookmark;
  dm.taOper.DisableControls;
  dm.taOper.DisableScrollEvents;
  try
    ExecSQL('execute procedure Create_History_ZpRateOrder("'+VarToStr(dm.Rec.ZpRateOrderDate)+'")', dm.quTmp);
    dm.taOper.First;
    while not dm.taOper.Eof do
    begin
      dm.taOper.Edit;
      dm.taOperZT.AsInteger := dm.taOperHIST_ZT.AsInteger;
      dm.taOperRATE.AsFloat := dm.taOperHIST_RATE.AsFloat;
      dm.taOperZBONUS.AsInteger := dm.taOperHIST_ZBONUS.AsInteger;
      dm.taOper.Post;
      dm.taOper.Next;
    end;
    FModifyZp := False;
    ReOpenDataSet(taZpHDate);
    ExecSQL('update D_Comp set ZPRATEORDERDATE="'+VarToStr(Now)+'" '+
      'where CompId='+IntToStr(dm.User.SelfCompId), dm.quTmp);
    dm.UpdateZpRateOrderDate(Now);
    edZpRateDate.EditButton.Visible := False;
    edZpRateDate.Value := Now;
  finally
    dm.taOper.GotoBookmark(Bookmark);
    dm.taOper.EnableScrollEvents;
    dm.taOper.EnableControls;
  end;
end;

procedure TfmOper.acCreateZpOrderUpdate(Sender: TObject);
begin
  acCreateZpOrder.Enabled := FModifyZp and (not VarIsNull(dm.Rec.ZpRateOrderDate));
end;

procedure TfmOper.NormaUpdateData(Sender: TObject;
  var Text: String; var Value: Variant; var UseText, Handled: Boolean);
begin
  FModifyNorma := True;
end;

procedure TfmOper.ZpUpdateData(Sender: TObject; var Text: String;
  var Value: Variant; var UseText, Handled: Boolean);
begin
  FModifyZp := True;
end;

procedure TfmOper.edNormaDateButtonClick(Sender: TObject; var Handled: Boolean);
begin
  FModifyNormaDate := True;
  Handled := True;
end;

procedure TfmOper.edNormaDateChange(Sender: TObject);
begin
  if not FModifyNormaDate then eXit;
  ExecSQL('update D_Comp set NORMAORDERDATE="'+VarToStr(edNormaDate.Value)+'" '+
    'where CompId='+IntToStr(dm.User.SelfCompId), dm.quTmp);
  dm.UpdateNormaOrderDate(edNormaDate.Value);
  edNormaDate.EditButton.Visible := False;
  FModifyNormaDate := False;
end;

procedure TfmOper.edZpRateDateButtonClick(Sender: TObject; var Handled: Boolean);
begin
  FModifyZpDate := True;
  Handled := True;
end;

procedure TfmOper.edZpRateDateChange(Sender: TObject);
begin
  if not FModifyZpDate then Exit;
  ExecSQL('update D_Comp set ZPRATEORDERDATE="'+VarToStr(edZpRateDate.Value)+'" '+
    'where CompId='+IntToStr(dm.User.SelfCompId), dm.quTmp);
  dm.UpdateZpRateOrderDate(edZpRateDate.Value);
  edZpRateDate.EditButton.Visible := False;
  FModifyZpDate := False;
end;


end.
