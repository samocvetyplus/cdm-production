unit ExecAppl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls, ComCtrls,
  Grids, DBGridEh, pFIBQuery, db, PrnDbgeh, ActnList, FIBQuery,
  DBGridEhGrouping, GridsEh;

type
  TfmExecAppl = class(TfmAncestor)
    Panel1: TPanel;
    gridExecAppl: TDBGridEh;
    SpeedItem1: TSpeedItem;
    quDDL: TpFIBQuery;
    PrintExecAppl: TPrintDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
    procedure gridExecApplGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  private
    FList : TStringList;
    procedure FindMin;
    function GenSelectSQL : string;
    procedure GetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure Filter(DataSet: TDataSet; var Accept: Boolean);
  public
  end;

var
  fmExecAppl: TfmExecAppl;

implementation

uses MainData, ApplData, dbUtil, DictData;

{$R *.dfm}

{ TfmExecAppl }

function TfmExecAppl.GenSelectSQL: string;
var
  Select, From, Where, Order, ApplIds, Into, Returns, SelectField : string;
  i : string;
  bDrop : boolean;
  Field : TField;
begin
  bDrop := False;
  quDDL.Close;
  try
    quDDL.SQL.Text := 'select count(*) from RDB$PROCEDURES where RDB$PROCEDURE_NAME="R_EXECAPPL_'+IntToStr(dm.User.UserId)+'"';
    quDDL.ExecQuery;
    bDrop := (quDDL.Fields[0].AsInteger<>0);
    quDDL.Close;
    quDDL.Transaction.CommitRetaining;
  except
    ShowMessage('������� ������������ �� ������ ');
  end;
  if bDrop then
    try
      quDDL.SQL.Text := 'drop procedure R_EXECAPPL_'+IntToStr(dm.User.UserId);
      quDDL.ExecQuery;
      quDDL.Transaction.CommitRetaining;
      quDDL.Close;
    except
      ShowMessage('Not unique user?');
    end;

  {Select := 'select j.ARTID, a2.ART, sz.NAME SZNAME ';
  From   := 'from Ja j, Art2 a2, D_Sz sz';
  Where  := 'where j.ART2ID=a2.ART2ID and '+
            'j.Art2id in (select distinct Art2Id from Ja where ApplId=j.ApplId) and '+
            'j.SZ=sz.ID and '+
            'j.ApplId in (';}
  Order  := '';
  //Into   := 'into :ARTID, :ART, :SZNAME ';
  SelectField := 'select ARTID, ART, SZID, SZNAME';
  Returns := 'ARTID integer, ART char(20), SZID integer, SzName char(20)';

  with dmAppl do
  begin
    quGetExecApplId.Open;
    if quGetExecApplId.IsEmpty then raise Exception.Create('�� ������ ��� ���������');
    i := '1';
    Select := '';
    while not quGetExecApplId.Eof do
    begin
      Select := Select + ' select sum(j.Q), sum(j.FQ) from Ja j '+
            ' where j.ApplId='+quGetExecApplIdAPPLID.AsString+' and '+
            '       j.ArtId=:ARTID and j.Sz=:SZID '+
            'into :Q_'+quGetExecApplIdAPPLID.AsString+', :F_'+quGetExecApplIdAPPLID.AsString+'; '+
            'if (Q_'+quGetExecApplIdAPPLID.AsString+' is null)then Q_'+quGetExecApplIdAPPLID.AsString+'=0;'+
            'if (F_'+quGetExecApplIdAPPLID.AsString+' is null)then F_'+quGetExecApplIdAPPLID.AsString+'=0;';



      SelectField := SelectField +', Q_'+quGetExecApplIdAPPLID.AsString+', F_'+quGetExecApplIdAPPLID.AsString;

      Field := TIntegerField.Create(dmAppl.taExecAppl);
      Field.FieldKind := fkData;
      Field.FieldName := 'Q_'+quGetExecApplIdAPPLID.AsString;
      Field.Name := dmAppl.taExecAppl.Name+Field.FieldName;
      Field.DataSet := dmAppl.taExecAppl;
      Field.DisplayLabel := '��������';
      Field.Tag := 0;
      Field.OnGetText := GetText;
      Field.OnSetText := nil;

      Field := TIntegerField.Create(dmAppl.taExecAppl);
      Field.FieldKind := fkData;
      Field.FieldName := 'F_'+quGetExecApplIdAPPLID.AsString;
      Field.Name := dmAppl.taExecAppl.Name+Field.FieldName;
      Field.DataSet := dmAppl.taExecAppl;
      Field.DisplayLabel := '����.';
      Field.Tag := 0;
      Field.OnGetText := GetText;
      Field.OnSetText := nil;

      Returns := Returns + ', Q_'+quGetExecApplIdAPPLID.AsString+' integer, '+
         'F_'+quGetExecApplIdAPPLID.AsString+' integer';

      i := IntToStr(StrToInt(i)+1);
      ApplIds := ApplIds + quGetExecApplIdAPPLID.AsString+',';
      quGetExecApplId.Next;
    end;
    quGetExecApplId.Close;
    ApplIds := copy(ApplIds, 0, Length(ApplIds)-1);
    Select := Select + ' select sum(j.Q), sum(j.FQ) from Ja j '+
            ' where j.ApplId in ('+ApplIds+') and '+
            '       j.ArtId=:ARTID and j.Sz=:SZID '+
            'into :TQ, :TF;';

    //Into := Into + ', :TQ, :TF';
    SelectField := SelectField +', TQ, TF, TP';

    Field := TIntegerField.Create(dmAppl.taExecAppl);
    Field.FieldKind := fkData;
    Field.FieldName := 'TQ';
    Field.Name := dmAppl.taExecAppl.Name+Field.FieldName;
    Field.DataSet := dmAppl.taExecAppl;
    Field.DisplayLabel := '��������';
    Field.Tag := 0;
    Field.OnGetText := GetText;
    Field.OnSetText := nil;

    Field := TIntegerField.Create(dmAppl.taExecAppl);
    Field.FieldKind := fkData;
    Field.FieldName := 'TF';
    Field.Name := dmAppl.taExecAppl.Name+Field.FieldName;
    Field.DataSet := dmAppl.taExecAppl;
    Field.DisplayLabel := '������� � ������';
    Field.Tag := 0;
    Field.OnGetText := GetText;
    Field.OnSetText := nil;

    Field := TFloatField.Create(dmAppl.taExecAppl);
    Field.FieldKind := fkData;
    Field.FieldName := 'TP';
    Field.Name := dmAppl.taExecAppl.Name+Field.FieldName;
    Field.DataSet := dmAppl.taExecAppl;
    Field.DisplayLabel := '%';
    Field.Tag := 0;
    Field.OnGetText := GetText;
    Field.OnSetText := nil;

    Returns := Returns + ', TQ integer, TF integer, TP double precision';
    Where := Where + ApplIds+')';
  end;

  Result := 'create procedure R_EXECAPPL_'+IntToStr(dm.User.UserId)+
    ' returns('+Returns+') as begin '+
    'for '+
       'select distinct j.ARTID, j.Sz, a.Art, sz.Name '+
       'from Ja j, D_Art a, D_Sz sz '+
       'where j.ApplId in ('+ApplIds+') and '+
       '      j.ArtId=a.D_ArtId and '+
       '      j.Sz=sz.Id '+
       'order by a.Art, sz.Name '+
       'into :ArtId, :szid, :Art, :SzName '+
     'do begin '+
       Select+' '+
       'if (TF is null) then TF = 0;'+
       'if (TQ is null) then TQ = 0;'+

       'if (TQ = 0)then '+
       '  if (TF = 0)then TP=0; else TP=TF; '+
       'else '+
       '  if(TF = 0)then TP=0; else TP=(TF/TQ);'+
       'suspend;'+
     'end '+
  'end;';

  quDDL.Close;
  quDDL.SQL.Text := Result;
  quDDL.ExecQuery;
  quDDL.Close;
  quDDL.Transaction.CommitRetaining;
  Result := SelectField + ' from R_EXECAPPL_'+IntToStr(dm.User.UserId);
end;

procedure TfmExecAppl.FormCreate(Sender: TObject);
var
  i : integer;
  ApplId : integer;
  DocNo, s : string;
  Docdate : TDateTime;
begin
  FList := TStringList.Create;
  (* ������� ������� � ����� *)
  gridExecAppl.Columns.BeginUpdate;
  with gridExecAppl, Columns do
  try
    i := 0;
    while i<=Pred(Count) do
      if(Pos('_', Columns[i].FieldName)<>0)then Columns[i].Free
      else inc(i);
  finally
   EndUpdate;
  end;
  if dm.tr.Active then dm.tr.CommitRetaining
  else dm.tr.StartTransaction;
  with dmAppl do
  begin
    CloseDataSet(taExecAppl);
    taExecAppl.Filtered := true;
    taExecAppl.OnFilterRecord := Filter;
    taExecAppl.SelectSQL.Text := GenSelectSQL;

    OpenDataSet(taExecAppl);
    FindMin;

    (* ������� ������� � ����� *)
    gridExecAppl.Columns.BeginUpdate;
    with dmAppl.taExecAppl do
    try
      for i:=0 to Pred(Fields.Count) do
        if(Pos('_', Fields[i].FieldName)<>0)or
          (Fields[i].FieldName[1]='T')then
        with gridExecAppl.Columns.Add do
        begin
            // Id �������� � ����������
            s := copy(Fields[i].FieldName, 0, 2);
            if(s='Q_')or(s='F_')then
            begin
              ApplId := StrToIntDef(copy(Fields[i].FieldName, 3, Length(Fields[i].FieldName)), 0);
              DocNo := ExecSelectSQL('select DocNo from Inv where InvId='+IntToStr(ApplId), quTmp);
              DocDate := ExecSelectSQL('select DocDate from Inv where InvId='+IntToStr(ApplId), quTmp);
              DocNo := '�'+DocNo+' ('+DateToStr(DocDate)+')';
            end
            else DocNo := '�����';

            FieldName := Fields[i].FieldName;
            if(Fields[i].FieldName[1]='T') then Color := clInfoBk
            else Color := clWhite;
            Alignment := taCenter;
            Width := 25;
            ReadOnly := True;

            Title.Caption := DocNo+'|'+Fields[i].DisplayLabel;
            Title.Orientation := tohVertical;
            ToolTips := True;
            Title.Alignment := taCenter;
          end;
      finally
        gridExecAppl.Columns.EndUpdate;
      end;
  end
end;

procedure TfmExecAppl.GetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text := '-'
  else case Sender.DataType of
    ftInteger : Text := Sender.AsString;
    ftFloat   : Text := FormatFloat('0.##', Sender.AsFloat);
  end;
end;

procedure TfmExecAppl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmAppl.taExecAppl);
  inherited;
end;

procedure TfmExecAppl.SpeedItem1Click(Sender: TObject);
begin
  PrintExecAppl.Preview;
end;

procedure TfmExecAppl.Filter(DataSet: TDataSet; var Accept: Boolean);
var
  i : integer;
begin
  Accept := False;
  for i:=0 to Pred(DataSet.FieldCount) do
  begin
    if(Pos('_', DataSet.Fields[i].FieldName)<>0)then
      if(not((DataSet.Fields[i].IsNull)or(DataSet.Fields[i].AsInteger=0)))then
      begin
        Accept := True;
        eXit;
      end
  end;
end;

procedure TfmExecAppl.FindMin;
var
  MinCount : integer;
  p : double;
  Ind : string;
  i : integer;
  ArtId, SzId : integer;
begin
  FList.Clear;
  Screen.Cursor := crHourGlass;
  with dmAppl do
  try
    MinCount := ExecSelectSQL('select Val from Const where Const_Id=5', quTmp);
    while not taExecAppl.Eof do
    begin
      FList.Values[taExecApplARTID.AsString+ ' '+taExecApplSZID.AsString] := '0';
      taExecAppl.Next;
    end;

    for i:=0 to Pred(MinCount) do
    begin
      taExecAppl.First;
      p := 999.99;
      Ind := '';
      while not taExecAppl.Eof do
      begin
        if p > taExecAppl.FieldByName('TP').AsFloat then
        begin
          if(FList.Values[taExecApplARTID.AsString+' '+taExecApplSZID.AsString] <> '1')then
          begin
            p := taExecAppl.FieldByName('TP').AsFloat;
            Ind := taExecApplARTID.AsString+' '+taExecApplSZID.AsString;
          end;
        end;
        dmAppl.taExecAppl.Next;
      end;
      FList.Values[Ind] := '1';
    end;
    taExecAppl.First;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmExecAppl.gridExecApplGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  with dmAppl do
  begin
    if(taExecAppl.FieldByName('TP').Value = 1)then eXit;
    if(FList.Values[taExecApplARTID.AsString+' '+taExecApplSZID.AsString]='1')
    then Background := clRed;
  end
end;

end.
