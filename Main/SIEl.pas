unit SIEl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  DBCtrls, RxToolEdit, RXDBCtrl, StdCtrls, Mask, Grids, DBGrids,
  DBGridEh, DBSumLst, DBCtrlsEh, db, PrnDbgeh, DBLookupEh, ActnList, Menus,
  TB2Item, FIBDataSet, pFIBDataSet, rxStrUtils, DBGridEhGrouping, GridsEh,
  rxPlacemnt, rxSpeedbar, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxSplitter, dxBar,
  cxContainer, cxGroupBox;

type
  TfmSIEl = class(TfmAncestor)
    paHeader: TPanel;
    lbDocDate: TLabel;
    lbDocNo: TLabel;
    lbDepId: TLabel;
    txtDep: TDBText;
    lbSup: TLabel;
    lbNDSId: TLabel;
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;
    spitClose: TSpeedItem;
    gridEl: TDBGridEh;
    lbSSF: TLabel;
    lbSSFDate: TLabel;
    edSSF: TDBEditEh;
    edSSFDT: TDBDateTimeEditEh;
    prntItem: TPrintDBGridEh;
    SpeedItem1: TSpeedItem;
    lcbxNDS: TDBLookupComboboxEh;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lcbxDepFrom: TDBLookupComboboxEh;
    acAdd: TAction;
    acDel: TAction;
    acPrint: TAction;
    acBulk: TAction;
    SpeedItem2: TSpeedItem;
    Label1: TLabel;
    lcbxContract: TDBLookupComboboxEh;
    acShowId: TAction;
    ppPrint: TTBPopupMenu;
    acPrintStone: TAction;
    acPrintMetal: TAction;
    acPrintActSemisGet: TAction;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    Label3: TLabel;
    lcbxSubType: TDBLookupComboboxEh;
    acMultiSelect: TAction;
    Label4: TLabel;
    lcbxInvColor: TDBLookupComboboxEh;
    taSupplier: TpFIBDataSet;
    dsrSupplier: TDataSource;
    taSupplierCOMPID: TFIBIntegerField;
    taSupplierNAME: TFIBStringField;
    SpeedItem3: TSpeedItem;
    acImportBuyInvoice: TAction;
    OpenDialog: TOpenDialog;
    taBuyMap: TpFIBDataSet;
    acEServParams: TAction;
    CheckBox: TCheckBox;
    Splitter: TcxSplitter;
    ViewCrude: TcxGridDBTableView;
    LevelCrude: TcxGridLevel;
    GridCrude: TcxGrid;
    DataSetCrude: TpFIBDataSet;
    DataSourceCrude: TDataSource;
    DataSetCrudeINVOICEID: TFIBIntegerField;
    DataSetCrudeINVOICEN: TFIBIntegerField;
    DataSetCrudeINVOICEDATE: TFIBDateTimeField;
    DataSetCrudeWEIGHT: TFIBBCDField;
    DataSetCrudeCOST: TFIBBCDField;
    ViewCrudeINVOICEN: TcxGridDBColumn;
    ViewCrudeINVOICEDATE: TcxGridDBColumn;
    ViewCrudeWEIGHT: TcxGridDBColumn;
    ViewCrudeCOST: TcxGridDBColumn;
    BarManager: TdxBarManager;
    Bar: TdxBar;
    BarDockControl: TdxBarDockControl;
    ComboBox: TcxGroupBox;
    dxBarLargeButton1: TdxBarLargeButton;
    LinkButton: TdxBarButton;
    acLink: TAction;
    ViewCrudeINVOICEID: TcxGridDBColumn;
    Label2: TLabel;
    labelStateDate: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spitCloseClick(Sender: TObject);
    procedure lcbxNDSExit(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acBulkExecute(Sender: TObject);
    procedure lcbxNDSEnter(Sender: TObject);
    procedure lcbxNDSChange(Sender: TObject);
    procedure acShowIdExecute(Sender: TObject);
    procedure gridElEnter(Sender: TObject);
    procedure chbxUEClick(Sender: TObject);
    procedure acPrintStoneExecute(Sender: TObject);
    procedure acPrintMetalUpdate(Sender: TObject);
    procedure acPrintStoneUpdate(Sender: TObject);
    procedure acPrintMetalExecute(Sender: TObject);
    procedure acPrintActSemisGetExecute(Sender: TObject);
    procedure acPrintActSemisGetUpdate(Sender: TObject);
    procedure lcbxContractEnter(Sender: TObject);
    procedure lcbxContractExit(Sender: TObject);
    procedure lcbxContractUpdateData(Sender: TObject;
      var Handled: Boolean);
    procedure acMultiSelectExecute(Sender: TObject);
    procedure acImportBuyInvoiceUpdate(Sender: TObject);
    procedure acImportBuyInvoiceExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acBulkUpdate(Sender: TObject);
    procedure acEServParamsExecute(Sender: TObject);
    procedure CheckBoxClick(Sender: TObject);
    procedure lcbxSubTypeKeyValueChanged(Sender: TObject);
    procedure lcbxContractChange(Sender: TObject);
    procedure lcbxDepFromChange(Sender: TObject);
    procedure DataSetCrudeBeforeOpen(DataSet: TDataSet);
    procedure SplitterBeforeOpen(Sender: TObject; var NewSize: Integer;
      var AllowOpen: Boolean);
    procedure acLinkExecute(Sender: TObject);
    procedure SplitterAfterClose(Sender: TObject);
    procedure ViewCrudeCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
  private
    ReadOnly: Boolean;
    ReadOnlyMode : boolean;
    FChangeNDS : boolean;
    FChangeContract : boolean;
    procedure UpdateControls;
    procedure CloseDoc;
    procedure OpenDoc;
    procedure InitBtn(T : boolean);
    procedure FilterInvColor(DataSet: TDataSet; var Accept: Boolean);
    procedure InvColorGetCellParams(Sender: TObject; EditMode: Boolean; Params: TColCellParamsEh);
  end;

var
  fmSIEl: TfmSIEl;

implementation

uses MainData, dbUtil, DictData, ApplData, fmUtils, DateUtils, InvData, MsgDialog,
     UtilLib, PrintData, uBuyInvoice, uDialogs, DictAncestor, EServParams;

{$R *.dfm}

procedure TfmSIEl.FormCreate(Sender: TObject);
begin
  FChangeNDS := False;
  FChangeContract := False;
  dmMain.UseUE := False;
  dmInv.ITYPE := 13;
  dm.SchemaId := dmMain.taSIListSCHEMAID.AsInteger;
  CheckBox.OnClick := nil;

  LinkButton.ImageIndex := 11;

  if dmMain.taSIListREFERENCEID.AsInteger > 0 then
  begin

    LinkButton.ImageIndex := 10;

  end;

  if Not dmMain.taSIListCREATED.IsNull then
  begin

    labelStateDate.Caption := FormatDateTime('DD/MM/YYYY', dmMain.taSIListCREATED.AsDateTime);

  end;

  if not dmMain.taSIListREF.IsNull then
    CheckBox.State := cbChecked;

  CheckBox.OnClick := acEServParamsExecute;
  dm.WorkMode := 'aSIEl';
  inherited;
  // ��������
  lcbxInvColor.DropDownBox.Columns[0].OnGetCellParams := InvColorGetCellParams;
  // ������� ������ ������
  dm.taInvColor.OnFilterRecord := FilterInvColor;
  dm.taInvColor.Filtered := True;
  OpenDataSets([dm.taNDS, taSupplier, dm.taContract, dm.taSemis, dm.taOper, dm.taInvColor, dmMain.taSIEl]);
  InitBtn(dmMain.taSIListISCLOSE.AsInteger=1);
  (* �������� ������� *)
  dm.DepBeginDate := dm.GetDepBeginDate(dmMain.taSIListDEPID.AsInteger);

  if (dm.DepBeginDate > dmMain.taSIListDOCDATE.AsDateTime) then
  begin

    ReadOnlyMode := True;

    stbrStatus.Panels[0].Text := '�������� ������ ���������� � '+DateToStr(dm.DepBeginDate);

  end else
  begin

    if ((dmMain.taSIListDEPID.AsInteger = dm.User.wWhId) or (dm.User.Profile = -1)) then
    begin

      ReadOnlyMode := False;

    end else
    begin

      ReadOnlyMode := true;

      stbrStatus.Panels[0].Text := '��� �������������� ���������� ����� � ' + dmMain.taSIListDEPNAME.AsString;

    end;

  end;

  UpdateControls;

  ReOpenDataSet(dmMain.taInvSubType);

  if dm.User.wWhName <> '' then
  begin
    Self.Caption := Self.Caption + ' - ' + dm.User.wWhName;
  end;

  Splitter.CloseSplitter;

end;

procedure TfmSIEl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FChangeNDS := False;
  PostDataSets([dmMain.taSIList, dmMain.taSIEl]);
  CloseDataSet(dmMain.taSIEl);
  CloseDataSets([dm.taNDS, taSupplier, dm.taContract, dm.taSemis, dmMain.taInvSubType, dm.taOper, dm.taInvColor]);
  dm.taInvColor.OnFilterRecord := nil;
  dm.taInvColor.Filtered := False;
end;

procedure TfmSIEl.spitCloseClick(Sender: TObject);
var Found: Variant;
begin
  PostDataSet(dmMain.taSIEl);
  if (not dm.IsAdm) and (dm.User.SelfCompId = 1) then
    if not ((dm.User.Profile = 1) and ((dm.User.DepId = 55) or (dm.User.DepId = 22))) then
      begin
        DialogErrorOKMessage('� ��� ��� ���� ��� ��������/�������� ��������� ����������� ����������');
        Exit;
      end;

  if spitClose.BtnCaption = '������' then
    begin
      if(dmMain.taSIEl.RecordCount = 0) then raise EWarning.Create('������ ������� ������ ���������!');

      if ((dmMain.taSIListCONTRACTID.AsInteger in [2, 4, 5]) and (VarIsNull(lcbxSubType.Value))) then
        begin
          MessageDialog('������ ��������: �� ������ ��� �������!', mtError, [mbOk], 0);
          eXit;
        end;
      try
        found := ExecSelectSQL('select se.id from SIEl se left outer join D_Semis ds' +
              ' on (se.semis = ds.semisid) where se.InvId = ' + dmMain.taSIListINVID.AsString +
              ' and ds.good = 1 and se.operid is null', dmMain.quTmp);
        if not VarIsNull(Found) then
          begin
            ShowMessage('� ��������������, ���������� �������,' +
                        ' ���������� ���������� ��������!');
            eXit;
          end;
        PostDataSet(dmMain.taSIList);
        Screen.Cursor := crHourGlass;
        CloseDoc;
        Screen.Cursor := crDefault;
//        try
//          ExecSQL('execute procedure Calc_SemisPrice("'+DateToStr(StartOfTheYear(Now))+'" ,"'+
//          DateToStr(EndOfTheYear(Now))+'", '+dmMain.taSIListINVID.AsString+')', dmMain.quTmp);
//        finally
//          Screen.Cursor := crDefault;
//        end;
        spitClose.BtnCaption := '������';
        spitClose.Hint := '������� ��������';
        spitClose.ImageIndex := 5;
        RefreshDataSet(dmMain.taSILIst);
      except
        on E: Exception do
        { TODO : !!! }
        //if not TranslateIbMsg(E, dmMain.taSIEl.Database, dmMain.taSIEl.Transaction)
             //then
           ShowMessage(E.Message);
      end
    end
  else
    try
      PostDataSet(dmMain.taSIList);
      OpenDoc;
      spitClose.BtnCaption := '������';
      spitClose.Hint := '������� ��������';
      spitClose.ImageIndex := 6;

      RefreshDataSet(dmMain.taSILIst);
    except
      on E: Exception do
      { TODO : !!! }
      //if not TranslateIbMsg(E, dmMain.taSIEl.Database, dmMain.taSIEl.Transaction)
           //then
           ShowMessage(E.Message);
    end;
  UpdateControls;
end;



procedure TfmSIEl.SplitterAfterClose(Sender: TObject);
begin
  inherited;

  if dataSetCrude.Active then
  begin

    DataSetCrude.Close;
    
  end;
  
end;

procedure TfmSIEl.SplitterBeforeOpen(Sender: TObject; var NewSize: Integer;
  var AllowOpen: Boolean);
begin
  inherited;

  if VarIsNull(lcbxDepFrom.KeyValue) then
  begin

    AllowOpen := false;

  end else
  begin

    DataSetCrude.CloseOpen(false);

    AllowOpen := True;

  end;

end;

procedure TfmSIEl.UpdateControls;
begin
  if ReadOnlyMode then ReadOnly := True
  else ReadOnly := dmMain.taSIListISCLOSE.AsInteger = 1;

  lbDocNo.Enabled := not ReadOnly;
  edSN.Enabled := not ReadOnly;
  lbDocDate.Enabled := not ReadOnly;
  deSDate.Enabled := not ReadOnly;
  lbSup.Enabled := not ReadOnly;
  lbDepId.Enabled := not ReadOnly;
  txtDep.Enabled := not ReadOnly;
  lcbxDepFrom.Enabled := not ReadOnly;
  lbNDSId.Enabled := not ReadOnly;
  lcbxNDS.Enabled := not ReadOnly;
  lbSSF.Enabled := not ReadOnly;
  edSSF.Enabled := not ReadOnly;
  lbSSFDate.Enabled := not ReadOnly;
  edSSFDT.Enabled := not ReadOnly;
  lcbxContract.Enabled := not ReadOnly;
  lcbxSubType.Enabled := not ReadOnly;
  lcbxInvColor.Enabled := not ReadOnly;
  gridEl.ReadOnly := ReadOnly;
  CheckBox.Enabled := not ReadOnly;
  spitClose.Enabled := not ReadOnlyMode;
end;

procedure TfmSIEl.ViewCrudeCustomDrawCell(Sender: TcxCustomGridTableView;
  ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
  var ADone: Boolean);
begin
  inherited;

  if AViewInfo.GridRecord.Values[ViewCrudeINVOICEID.Index] = dmMain.taSIListREFERENCEID.AsInteger then
  begin

    ACanvas.Brush.Color := clYellow;

    ADone := True;

  end;

end;

procedure TfmSIEl.CheckBoxClick(Sender: TObject);
begin
  if Sender is TCheckBox then
    acEServParams.Execute;
end;

procedure TfmSIEl.CloseDoc;
begin
  ExecSQL('execute procedure CloseDoc("������ ����������", '+dmMain.taSIListINVID.AsString+')', dmMain.quTmp);
end;


procedure TfmSIEl.DataSetCrudeBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  DataSetCrude.ParamByName('Agent$ID').AsInteger := lcbxDepFrom.KeyValue;
end;

procedure TfmSIEl.OpenDoc;
begin
  ExecSQL('execute procedure OpenDoc("������ ����������", '+dmMain.taSIListINVID.AsString+')', dmMain.quTmp);
end;

procedure TfmSIEl.InitBtn(T: boolean);
begin
  if not T then // �������� ������
  begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 6;
  end
  else begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 5;
  end
end;

procedure TfmSIEl.lcbxNDSExit(Sender: TObject);
begin
  FChangeNDS := False;
  PostDataSet(dmMain.taSIEl);
end;

procedure TfmSIEl.lcbxSubTypeKeyValueChanged(Sender: TObject);
begin
  inherited;
  with dmMain.taSIList do
    begin
      Edit;
      FieldByName('InvSubTypeID').AsInteger := lcbxSubType.KeyValue;
      Post;
    end;
end;

procedure TfmSIEl.acAddExecute(Sender: TObject);
begin
  inherited;
  dmMain.taSIEl.Append;
end;

procedure TfmSIEl.acDelExecute(Sender: TObject);
begin
  inherited;
  dmMain.taSIEl.Delete;
end;

procedure TfmSIEl.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := not ReadOnly;
end;

procedure TfmSIEl.acEServParamsExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'MATID'; sTable: 'ESERV_PARAMS'; sWhere: '');
begin
  if CheckBox.State = cbChecked then
    begin
      ShowDictForm(TfmEServParams, dmInv.dsrEServParams, Rec, TAction(Sender).Caption, dmDictionary, nil);
      dmMain.taSIList.Edit;
      dmMain.taSIListREF.AsInteger := dmInv.EServParamID;
      dmMain.taSIList.Post;
    end;
  if CheckBox.State = cbUnChecked then
    begin
      with dmInv.taEServParams do
        begin
          Open;
          Edit;
          Delete;
          Close;
        end;
      dmMain.taSIList.Edit;
      dmMain.taSIListREF.AsVariant := null;
      dmMain.taSIList.Post;
    end;
end;

procedure TfmSIEl.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := not ReadOnly;
end;

procedure TfmSIEl.acBulkExecute(Sender: TObject);
begin
  PostDataSet(dmMain.taSIEl);
  with dminv.sqlPublic do
  try
    SQl.Clear;
    SQl.Add('execute procedure CREATE_BULK_FOR_BRILL(:INVID);');
    ParamByName('INVID').ASInteger:=dmMain.taSIListINVID.AsInteger;
    ExecQuery;
    Transaction.CommitRetaining;
    MessageDialogA('��� ������ ������� ������', mtInformation);
  except
    on E:Exception do MessageDialogA(E.message, mtWarning);
  end;
end;

procedure TfmSIEl.acBulkUpdate(Sender: TObject);
begin
  acBulk.Enabled := not ReadOnly;
end;

procedure TfmSIEl.lcbxNDSEnter(Sender: TObject);
begin
  FChangeNDS := True;
end;

procedure TfmSIEl.lcbxNDSChange(Sender: TObject);
var
  Bookmark : Pointer;
  Cost : double;
begin
  if not FChangeNDS then eXit;
  PostDataSet(dmMain.taSIEl);
  Bookmark := dmMain.taSIEl.GetBookmark;
  dmMain.taSIEl.DisableScrollEvents;
  dmMain.taSIEl.OnCalcFields := nil;
  try
    dmMain.taSIEl.First;
    if not dm.taNDS.Locate('NDSID', lcbxNDS.KeyValue, []) then
      raise Exception.Create('�� ������ ��� � ����������� ���');
    while not dmMain.taSIEl.Eof do
    begin
      dmMain.taSIEL.Edit;
      Cost := fif(dmMain.taSIElFOR_Q_U.AsInteger=0, dmMain.taSIElTPRICE.AsFloat*dmMain.taSIElW.AsFloat,
            dmMain.taSIElTPRICE.AsFloat*dmMain.taSIElQ.AsInteger);
      dmMain.taSIElNDS.AsFloat := dm.CalcNDS(Cost, dm.taNDSNDS.AsFloat, dm.taNDST.AsInteger);
      dmMain.taSIElNDSID.AsInteger := lcbxNDS.KeyValue;
      if (dm.taNDST.AsInteger = 0) then dmMain.taSIElCOST.AsFloat := Cost+dmMain.taSIElNDS.AsFloat
      else dmMain.taSIElCOST.AsFloat := Cost;
      dmMain.taSIEL.Post;
      dmMain.taSIEl.Next;
    end;
  finally
    dmMain.taSIEl.GotoBookmark(Bookmark);
    dmMain.taSIEl.EnableControls;
    dmMain.taSIEl.EnableScrollEvents;
    dmMain.taSIEl.OnCalcFields := dmMain.taSIElCalcFields;
  end;
end;

procedure TfmSIEl.acShowIdExecute(Sender: TObject);
begin
  gridEl.FieldColumns['NDSID'].Visible := not gridEl.FieldColumns['NDSID'].Visible;
  gridEl.FieldColumns['ID'].Visible := not gridEl.FieldColumns['ID'].Visible;
end;

procedure TfmSIEl.gridElEnter(Sender: TObject);
begin
  PostDataSet(dmMain.taSIList);
end;

procedure TfmSIEl.chbxUEClick(Sender: TObject);
begin
//  dmMain.UseUE := chbxUE.Checked;
end;

procedure TfmSIEl.acPrintStoneExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taSIList, dmMain.taSIEl]);
  dmPrint.PrintDocumentA(dkSIList_STONE, VarArrayOf([VarArrayOf([dmMain.taSIListINVID.AsInteger]), VarArrayOf([dmMain.taSIListINVID.AsInteger])]));
end;

procedure TfmSIEl.acPrintStoneUpdate(Sender: TObject);
begin
  acPrintStone.Enabled := dmMain.taSIList.Active and dmMain.taSIEl.Active and (not dmMain.taSIEl.IsEmpty);
end;

procedure TfmSIEl.acPrintMetalUpdate(Sender: TObject);
begin
  acPrintMetal.Enabled := dmMain.taSIList.Active and dmMain.taSIEl.Active and (not dmMain.taSIEl.IsEmpty);
end;

procedure TfmSIEl.acPrintMetalExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taSIList, dmMain.taSIEl]);
  dmPrint.PrintDocumentA(dkSIList_Metall, VarArrayOf([VarArrayOf([dmMain.taSIListINVID.asinteger]),VarArrayOf([dmMain.taSIListINVID.asinteger])]));
end;

procedure TfmSIEl.acPrintActSemisGetExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taSIList, dmMain.taSIEl]);
  dmPrint.PrintDocumentA(dkSIList_ActSemisGet, VarArrayOf([VarArrayOf([dmMain.taSIListINVID.AsInteger]),VarArrayOf([dmMain.taSIListINVID.AsInteger])]));
end;

procedure TfmSIEl.acPrintActSemisGetUpdate(Sender: TObject);
begin
  acPrintActSemisGet.Enabled := dmMain.taSIList.Active and dmMain.taSIEl.Active and
     (not dmMain.taSIEl.IsEmpty) and (dmMain.taSIListSCHEMAID.AsInteger=1);
end;

procedure TfmSIEl.lcbxContractChange(Sender: TObject);
begin
  inherited;
  with dmMain, taSIList do
  begin
    Edit;
    if ((lcbxContract.KeyValue = 2) or (lcbxContract.KeyValue = 4)) and (taSIListINVSUBTYPEID.IsNull) then
      taSIListINVSUBTYPEID.AsInteger := 5
    else
      if ((lcbxContract.KeyValue = 5) and (taSIListINVSUBTYPEID.IsNull)) then
        taSIListINVSUBTYPEID.AsInteger := 7;
    //Post;
    Refresh;
  end;
end;

procedure TfmSIEl.lcbxContractEnter(Sender: TObject);
begin
  FChangeContract := False;
end;

procedure TfmSIEl.lcbxContractExit(Sender: TObject);
begin
  if not FChangeContract then eXit;
  dm.SchemaId := dm.taContractT0.AsInteger;
  ReOpenDataSet(dmMain.taInvSubType);
end;

procedure TfmSIEl.lcbxContractUpdateData(Sender: TObject; var Handled: Boolean);
begin
  if dm.taContractT0.AsInteger <> dmMain.taSIListSCHEMAID.AsInteger then
  begin
    FChangeContract := True;
    dm.SchemaId := dm.taContractT0.AsInteger;
    ReOpenDataSet(dmMain.taInvSubType);
  end;
end;

procedure TfmSIEl.lcbxDepFromChange(Sender: TObject);
begin
  inherited;
  DataSetCrude.CloseOpen(false);
end;

procedure TfmSIEl.acMultiSelectExecute(Sender: TObject);
begin
  if (dgMultiSelect in gridEl.Options) then
    gridEl.Options := gridEl.Options - [dgMultiSelect]
  else gridEl.Options := gridEl.Options + [dgMultiSelect];
end;

procedure TfmSIEl.FilterInvColor(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := dm.taInvColorINVTYPEID.AsInteger = 13;
end;

procedure TfmSIEl.InvColorGetCellParams(Sender: TObject; EditMode: Boolean; Params: TColCellParamsEh);
begin
  Params.Background := dm.taInvColorCOLOR.AsInteger;
end;

procedure TfmSIEl.acImportBuyInvoiceUpdate(Sender: TObject);
begin
  acImportBuyInvoice.Enabled := not ReadOnly;
end;

procedure TfmSIEl.acLinkExecute(Sender: TObject);
begin

  if linkButton.ImageIndex = 11 then
  begin

    with dmMain.taSIList do
    begin

      Edit;

      FieldByName('Reference$ID').AsInteger := DataSetCrudeINVOICEID.AsInteger;

      Post;

    end;

    LinkButton.ImageIndex := 10;

    LinkButton.Caption := '������� �����';

    Splitter.CloseSplitter;

  end else

  if LinkButton.ImageIndex = 10 then
  begin

    with dmMain.taSIList do
    begin

      Edit;

      FieldByName('Reference$ID').AsInteger := 0;

      Post;

    end;

    LinkButton.ImageIndex := 11;

    LinkButton.Caption := '������� �����';

    Splitter.CloseSplitter;

  end;

  DataSetCrude.Refresh;

end;

procedure TfmSIEl.acImportBuyInvoiceExecute(Sender: TObject);
var
  i: Integer;
  Document: IXMLDocumentType;
  Elements: IXMLElementsType;
  Element: IXMLElementType;
  ProbeID: Integer;
  MemoID: Integer;
  Weight: Double;
  Price:Double;
  SemisID: string;
  InvoiceN: Integer;
  InvoiceDate: TDate;
  Header: TStringList;
  Department: string;
  DepartmentID: Integer;
  AShortDateFormat: string;
  ADecimalSeparator : char;
begin
  AShortDateFormat := ShortDateFormat;

  if OpenDialog.Execute then
  begin
    Document := LoadDocument(OpenDialog.FileName);
    Header := TStringList.Create;
    ExtractStrings(['$'], [], PChar(OpenDialog.FileName), Header);
    i := WordCount(OpenDialog.FileName, ['$']);
    Department := '';
    for i := 2 to i - 3 do
    Department := Department + ' ' + Header[i-1];
    DepartmentID := 0;
    Department := Trim(Department);
    if Department = '������� �����' then DepartmentID := 1152 else
    if Department = '���' then DepartmentID := 1758 else
    if Department = '������� ��������' then DepartmentID := 993 else
    if Department = '������' then DepartmentID := 1263 else
    if Department = '�����������' then DepartmentID :=  1460 else
    if Department = '�����' then DepartmentID :=  1590 else
    if Department = '���� ����' then DepartmentID :=  1670 else
    if Department = '���' then DepartmentID := 1910 else
    if Department = '����������' then DepartmentID := 1912 else
    if Department = '����� ������' then departmentID := 1990;

    ADecimalSeparator := DecimalSeparator;
    DecimalSeparator := ',';

    i := WordCount(OpenDialog.FileName, ['$']);
    Dec(i);
    InvoiceN := StrToInt(Header[i-1]);
    ShortDateFormat := 'yyyy.mm.yy';
    InvoiceDate := StrToDate(Header[i-2]);
    ShortDateFormat := AShortDateFormat;
    Header.Free;
    if dmMain.taSIList.State = dsBrowse then
    dmMain.taSIList.Edit;
    dmMain.taSIListSSF.AsString := IntToStr(InvoiceN);
    dmMain.taSIListSSFDT.AsDateTime := InvoiceDate;
    dmMain.taSIListSUPID.AsInteger := DepartmentID;
    Elements := Document.Elements;
    taBuyMap.Active := True;
    for i := 0 to Elements.Count - 1 do
    begin
      Element := Elements[i];
      ProbeID := Element.ProbeID;
      MemoID := Element.MemoID;
      Weight := Double(Element.Weight);
      Price := Element.Price;
      SemisID := '';
      if taBuyMap.Locate('probe$id;memo$id', VarArrayOf([ProbeID, MemoID]), []) then
      begin
        SemisID := Trim(taBuyMap.FieldByName('Semis$ID').AsString);
      end;
      if SemisID <> '' then
      begin
        dmMain.taSIEl.Append;
        dmMain.taSIElSEMIS.AsString := SemisID;
        dmMain.taSIElW.AsFloat := Weight;
        dmMain.taSIElTPRICE.AsFloat := Price;
        dmMain.taSIEl.Post;
      end;
    end;
    taBuyMap.Active := False;
    Document := nil;
  end;
  ShortDateFormat := AShortDateFormat;
  DecimalSeparator := ADecimalSeparator;
end;

end.

