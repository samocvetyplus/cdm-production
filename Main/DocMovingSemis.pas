unit DocMovingSemis;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls,
  ComCtrls, Grids, DBGridEh, DB, FIBDataSet, pFIBDataSet, DBGridEhGrouping,
  GridsEh;

type
  TfmDocMoving = class(TfmAncestor)
    dbgList: TDBGridEh;
    taDocMoving: TpFIBDataSet;
    dsrDocMoving: TDataSource;
    taDocMovingDOCNO: TFIBIntegerField;
    taDocMovingDOCDATE: TFIBDateTimeField;
    taDocMovingCOMPNAME: TFIBStringField;
    taDocMovingINVTYPENAME: TFIBStringField;
    taDocMovingPRICE: TFIBFloatField;
    taDocMovingW: TFIBFloatField;
    taDocMovingCOST: TFIBFloatField;
    taDocMovingNDS: TFIBFloatField;
    taDocMovingDEPNAME: TFIBStringField;
    procedure FormCreate(Sender: TObject);
  end;


procedure ShowDocMovingSemis(Semis:string);

implementation

uses DictData;

{$R *.dfm}

procedure ShowDocMovingSemis(Semis:string);
var
  F : TfmDocMoving;
begin
  Application.ProcessMessages;
  try
    F := TfmDocMoving.Create(Application);
    F.taDocMoving.ParamByName('SEMISID').AsString := Semis;
    F.taDocMoving.Open;
    F.ShowModal;
    F.taDocMoving.Close;
  finally
    FreeAndNil(F);
  end;
end;

procedure TfmDocMoving.FormCreate(Sender: TObject);
begin
  dbgList.Invalidate;
end;

end.
