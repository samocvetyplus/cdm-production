unit ACTbyItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList, ExtCtrls, ComCtrls,
  Grids, DBGrids, RXDBCtrl, dbUtil, DB, fmUtils,
  DBGridEh, ActnList, DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmACTbyItem = class(TfmAncestor)
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    DBGridEh1: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmACTbyItem: TfmACTbyItem;

implementation

uses InvData, PrintData, Pact_Items, SInvProd, ePriceItem;

{$R *.dfm}

procedure TfmACTbyItem.FormCreate(Sender: TObject);
begin
  inherited;
  Caption:='��� ���������� �'+ dmINV.taSListDOCNO.AsString+' �� '+dmINV.taSListDOCDATE.AsString;
  ReOpenDataSets([dmINV.taPriceACTS, dmInv.taPactsPrint]);
end;


procedure TfmACTbyItem.SpeedItem1Click(Sender: TObject);
begin
  inherited;
  dmPrint.PrintDocumentA(dkACTRePrice,null);
end;

procedure TfmACTbyItem.SpeedItem2Click(Sender: TObject);
begin
  inherited;
  ReOpenDataSet(dmInv.taPact_Items);
  ShowAndFreeForm(TfmPact_Items, TForm(fmPact_Items));
end;

procedure TfmACTbyItem.SpeedItem3Click(Sender: TObject);
begin
  inherited;
  try
   fmPriceItem:=TfmPriceItem.Create(self);
   if fmPriceItem.ShowModal=mrOk then
   begin
    
   end;
  finally
   fmPriceItem.Free;
  end;
end;

procedure TfmACTbyItem.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key=VK_DELETE)and(MessageDlg('������� ������?',mtWarning,[mbOk,mbCancel],0)=mrOk) then
  dmInv.taPriceACTS.Delete;

end;

procedure TfmACTbyItem.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
   CloseDataSets([dmInv.taPact_Items, dmInv.taPactsPrint]);
end;

end.
