unit OperHistory;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, DB, FIBDataSet, pFIBDataSet;

type
  TfmOperHistory = class(TForm)
    gridNorma: TDBGridEh;
    taNormaHistory: TpFIBDataSet;
    taNormaHistoryOPERNAME: TFIBStringField;
    taNormaHistoryN: TFIBFloatField;
    taNormaHistoryN1: TFIBFloatField;
    taNormaHistoryTAILNAME: TFIBStringField;
    dsrNormaHistory: TDataSource;
    dsrZpHistory: TDataSource;
    taZpHistory: TpFIBDataSet;
    gridZp: TDBGridEh;
    taZpHistoryOPERNAME: TFIBStringField;
    taZpHistoryRATE: TFIBFloatField;
    taZpHistoryZP: TFIBStringField;
    taNormaHistoryNQ: TFIBStringField;
    taNormaHistoryRecNo: TIntegerField;
    taZpHistoryRecNo: TIntegerField;
    taZpHistoryZBONUS: TFIBSmallIntField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure taNormaHistoryCalcFields(DataSet: TDataSet);
    procedure taZpHistoryCalcFields(DataSet: TDataSet);
  private
  public
  end;

procedure ShowOperHistory(Kind: byte; HDate: TDateTime);

implementation

uses dOper, DateUtils, dbUtil, fmUtils;

{$R *.dfm}

procedure ShowOperHistory(Kind: byte; HDate: TDateTime);
var
  fmOperHistory: TfmOperHistory;
begin
  fmOperHistory := TfmOperHistory.Create(nil);
  if(Kind = 0)then
  begin
    fmOperHistory.gridNorma.Visible := True;
    fmOperHistory.taNormaHistory.ParamByName('HDATE').AsTimestamp := DateTimeToTimeStamp(HDate);
    fmOperHistory.taNormaHistory.Open;
    fmOperHistory.Caption := '����������� ������ '+DateToStr(HDate);
  end
  else begin
    fmOperHistory.gridZp.Visible := True;
    fmOperHistory.taZpHistory.ParamByName('HDATE').AsTimestamp := DateTimeToTimeStamp(HDate);
    fmOperHistory.taZpHistory.Open;
    fmOperHistory.Caption := '�������� �/� '+DateToStr(HDate);
  end;
  SetWindowTransp(fmOperHistory.Handle, 206);
  fmOperHistory.Show;
end;

procedure TfmOperHistory.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taNormaHistory, taZpHistory]);
  Action:= caFree;
end;

procedure TfmOperHistory.taNormaHistoryCalcFields(DataSet: TDataSet);
begin
  taNormaHistoryRecNo.AsInteger := taNormaHistory.RecNo;
end;

procedure TfmOperHistory.taZpHistoryCalcFields(DataSet: TDataSet);
begin
  taZpHistoryRecNo.AsInteger := taZpHistory.RecNo;
end;

end.

