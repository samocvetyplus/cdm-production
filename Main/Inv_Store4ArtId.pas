unit Inv_Store4ArtID;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGrids, RXDBCtrl, StdCtrls, INVDATA, dbUtil,
  Buttons, DBGridEh, DB, ActnList, TB2Item, TB2Dock, TB2Toolbar,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmInv_Store4ArtId = class(TfmAncestor)
    tb2: TSpeedBar;
    laDep: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    Label1: TLabel;
    Label2: TLabel;
    Panel1: TPanel;
    lbStore: TLabel;
    lbINV: TLabel;
    gridStore: TDBGridEh;
    gridSItem: TDBGridEh;
    acInsert: TAction;
    acRemove: TAction;
    TBDock1: TTBDock;
    Toolbar: TTBToolbar;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acShowID: TAction;
    Splitter1: TSplitter;
    FormStorage: TFormStorage;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure acInsertExecute(Sender: TObject);
    procedure acRemoveExecute(Sender: TObject);
    procedure gridStoreKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acInsertUpdate(Sender: TObject);
    procedure acRemoveUpdate(Sender: TObject);
    procedure acShowIDExecute(Sender: TObject);
  private
    FControlDate : TDateTime;
  end;

var
  fmInv_Store4ArtId: TfmInv_Store4ArtId;

implementation

uses DictData, INV_STORE, SItem, ProductionConsts, MsgDialog, SInv_det;

{$R *.dfm}

procedure TfmInv_Store4ArtId.FormCreate(Sender: TObject);
begin
  inherited;

  FControlDate:=dm.GetStoreBeginDate;

  with dmINV do
  begin
    OpenDataSets([taInv_Store4ArtId, taSItem4ArtId]);
  end;
end;

procedure TfmInv_Store4ArtId.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dmInv do
  begin
    PostDataSet(taSItem4ArtId);
    CloseDataSets([taInv_Store4ArtId, taSItem4ArtId]);
  end;
  inherited;
end;

procedure TfmInv_Store4ArtId.FormShow(Sender: TObject);
begin
  Caption := '�������: '+dmInv.FCurrFullArt;
  laDep.Caption := dm.DepNameById[dm.CurrDep];
  case dmINV.ITYPE of
    2,12 : lbStore.Caption := '������� �� ������';
    3    : lbStore.Caption := '��������� �������';
    else lbStore.Caption := '';
  end;
end;

procedure TfmInv_Store4ArtId.acInsertExecute(Sender: TObject);
begin
  if dmINV.taSListISCLOSE.AsInteger=1 then
    raise Exception.Create(Format(rsInvClose, [dmINV.taSListDOCNO.AsString]));
  with dmInv do
  begin
    // ���� art2id � ������� sel
    if not taInvItems.Locate('ART2ID, PRICE2', VarArrayOf([taInv_Store4ArtIdART2ID.AsInteger, taInv_Store4ArtIdSPRICE.AsCurrency]),[]) then
    begin
      //taInvItems.Locate('Art2ID, Weight', VarArrayOf([taInv_Store4ArtIdART2ID.AsInteger, taInv_Store4ArtIdPRICE.AsFloat]), [])
      FCurrArt:=taInv_Store4ArtIdART.AsString;
      FCurrArt2Id:= taInv_Store4ArtIdART2ID.AsInteger;
      taInvItems.Append;
      taInvItems.BeforePost := nil;
      PostDataSet(taInvItems);
      taInvItems.BeforePost := taINVItemsBeforePost;
    end;
    // ��������� ������� � ������� sitem
    FCurrUID := taInv_Store4ArtIdUID.AsInteger;
    FCurrW := taInv_Store4ArtIdW.AsFloat;
    FCurrSz := taInv_Store4ArtIdSZID.Asinteger;
    taInv_Store4ArtId.Delete;
    OpenDataSet(taSItem);
    taSItem.Append;
    // �������� ��������� ������ � ����� ������ taSIem4ArtId
    taSItem4ArtId.Append;
    taSItem4ArtIdSITEMID.AsInteger := taSItemSITEMID.AsInteger;
    PostDataSets([taSItem, taSItem4ArtId]);
    CloseDataSet(taSItem);
    taINVItems.Refresh;
    fmSInv_det.DeleteInvoiceArticle(taSItem4ArtIdUID.AsInteger);
    //if taStoreByItems.Locate('UID', taSItem4ArtIdUID.AsInteger, []) then taStoreByItems.Delete;
  end;
end;

procedure TfmInv_Store4ArtId.acRemoveExecute(Sender: TObject);
var
  UID : integer;
begin
  if dmINV.taSListISCLOSE.ASinteger=1 then
    raise Exception.Create(Format(rsInvClose, [dmINV.taSListDOCNO.AsString]));

  with dmINV do
  begin
    // ������� �� �������������� ������ � ���������
    if not taINVItems.Locate('ART2ID', taSItem4ArtIdART2ID.AsInteger, [])then
      raise EInternal.Create(Format(rsInternalError, ['82']));

    OpenDataSet(taSItem);
    if not taSItem.Locate('UID', taSItem4ArtIdUID.AsInteger, [])then
    begin
      ReOpenDataSet(taSItem);
      if not taSItem.Locate('UID', taSItem4ArtIdUID.AsInteger, []) then
      begin
        MessageDialog('��������� ����������:'#13+
             'taSItem4ArtIdUID='+taSItem4ArtIdUID.AsString+','#13+
             'taSItemSELID='+taSItemSELID.AsString+','#13+
             'taInvItemsSELID='+taINVItemsSELID.AsString, mtError, [mbOk], 0);
        raise EInternal.Create(Format(rsInternalError, ['81']));
      end;
    end;
    // ��������� �������� �� ����� ����� �����
    UID := taSItem4ArtIdUID.AsInteger;
    taSItem4ArtId.Delete;
    // �������� �������� �� ��
    taSItem.Delete;
    // �������� ������ ���������
    if taSItem.IsEmpty then taINVItems.Delete
    else taINVItems.Refresh;
    CloseDataSet(taSItem);
    // �������� � ������ ����� �����
    taInv_Store4ArtId.Insert;
    taInv_Store4ArtIdUID.AsInteger:=UID;
    case dmInv.IType of
      2, 12 : taInv_Store4ArtIdSTATE_.AsInteger := 0;
      3 : taInv_Store4ArtIdSTATE_.AsInteger := 1;
      else raise Exception.Create(rsNotImplementation);
    end;
    PostDataSet(taInv_Store4ArtId);
    RefreshDataSet(taInv_Store4ArtId);
    // �������� ����������� ������ � ����� ��-��������
    fmSInv_det.InsertInvoiceArticle(UID);
    //taStoreByItems.Insert;
    //taStoreByItemsUID.AsInteger:=UID;
    //PostDataSet(taStoreByItems);
    //RefreshDataSet(taStoreByItems);
  end;
end;

procedure TfmInv_Store4ArtId.gridStoreKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_SPACE : acInsert.Execute;
  end;
end;

procedure TfmInv_Store4ArtId.acInsertUpdate(Sender: TObject);
begin
  acInsert.Enabled := (FControlDate<dmInv.taSListDOCDATE.AsDateTime)and
     dmInv.taInv_Store4ArtId.Active and (not dmInv.taInv_Store4ArtId.IsEmpty) and
     dmInv.taSItem4ArtId.Active;
end;

procedure TfmInv_Store4ArtId.acRemoveUpdate(Sender: TObject);
begin
  acRemove.Enabled := (FControlDate < dmInv.taSListDOCDATE.AsDateTime)and
     dmInv.taInv_Store4ArtId.Active and (not dmInv.taSItem4ArtId.IsEmpty) and
     dmInv.taSItem4ArtId.Active;
end;

procedure TfmInv_Store4ArtId.acShowIDExecute(Sender: TObject);
begin
  gridStore.FieldColumns['ART2ID'].Visible := not gridStore.FieldColumns['ART2ID'].Visible;
  gridStore.FieldColumns['SZID'].Visible := not gridStore.FieldColumns['SZID'].Visible;
  gridSItem.FieldColumns['SITEMID'].Visible := not gridSItem.FieldColumns['SITEMID'].Visible;
  gridSItem.FieldColumns['ART2ID'].Visible := not gridSItem.FieldColumns['ART2ID'].Visible;
  gridSItem.FieldColumns['SZ'].Visible := not gridSItem.FieldColumns['SZ'].Visible;
end;

end.


