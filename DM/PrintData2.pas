unit PrintData2;

interface

uses
  SysUtils, Classes, FR_DSet, FR_DBSet, DB, FIBDataSet, pFIBDataSet, PrintData,
  FR_Class, frXMLExl, frRtfExp, frOLEExl, dialogs;

type
  TdmPrint2 = class(TDataModule)
    frActWorkList: TfrDBDataSet;
    frActWork: TfrDBDataSet;
    taComp: TpFIBDataSet;
    frComp: TfrDBDataSet;
    taRequest: TpFIBDataSet;
    frRequest: TfrDBDataSet;
    taPItem_PS_Balance: TpFIBDataSet;
    frPItem_PS: TfrDBDataSet;
    taAWG: TpFIBDataSet;
    frAWG: TfrDBDataSet;
    taVerificationAct: TpFIBDataSet;
    frVerificationAct: TfrDBDataSet;
    taVerificationHeader: TpFIBDataSet;
    frVerificationHeader: TfrDBDataSet;
    taMX18: TpFIBDataSet;
    frMX18: TfrDBDataSet;
    frMX18Item: TfrDBDataSet;
    taMX18Item: TpFIBDataSet;
    taAWRetServ: TpFIBDataSet;
    frAWRetServ: TfrDBDataSet;
    taAWRej: TpFIBDataSet;
    frAWRej: TfrDBDataSet;
    taAWTorg12: TpFIBDataSet;
    taAWTorg12Item: TpFIBDataSet;
    frAWTorg12: TfrDBDataSet;
    frAWTorg12Item: TfrDBDataSet;
    frRevision_Sum: TfrDBDataSet;
    taRevision_Sum: TpFIBDataSet;
    taRevision_SumOVER_COUNT: TFIBIntegerField;
    taRevision_SumOVER_W: TFIBFloatField;
    taRevision_SumNEED_COUNT: TFIBIntegerField;
    taRevision_SumNEED_W: TFIBFloatField;
    taAWG_Null: TpFIBDataSet;
    frAWG_Null: TfrDBDataSet;
    taInvArtPhoto: TpFIBDataSet;
    frInvArtPhoto: TfrDBDataSet;
    taAWMOTotal: TpFIBDataSet;
    frAWMOTotal: TfrDBDataSet;
    taAWMOTotalItem: TpFIBDataSet;
    frAWMOTotalItem: TfrDBDataSet;
    dsrAWMOTotal: TDataSource;
    taInvUID: TpFIBDataSet;
    frInvUID: TfrDBDataSet;
    taZpKArt: TpFIBDataSet;
    frZpKSemis: TfrDBDataSet;
    frZpKArt: TfrDBDataSet;
    taZpKSemis: TpFIBDataSet;
    taZpKSemisID: TIntegerField;
    taZpKSemisSEMIS: TFIBStringField;
    taZpKSemisK: TFloatField;
    taZpKTech: TpFIBDataSet;
    taZpKTechID: TIntegerField;
    taZpKTechNAME: TFIBStringField;
    taZpKTechK: TFloatField;
    taZpKIns: TpFIBDataSet;
    taZpKInsID: TIntegerField;
    taZpKInsNAME: TFIBStringField;
    taZpKInsK: TFloatField;
    taZpKW: TpFIBDataSet;
    taZpKWID: TIntegerField;
    taZpKWMINW: TFloatField;
    taZpKWMAXW: TFloatField;
    taZpKWK1: TFloatField;
    taZpKWK2: TFloatField;
    taZpKWK3: TFloatField;
    frZpKTech: TfrDBDataSet;
    frZpKIns: TfrDBDataSet;
    frZpKW: TfrDBDataSet;
    taZpKWOPERATION: TFIBStringField;
    taAW_SelfMat_Ext: TpFIBDataSet;
    taPTHead_Ext: TpFIBDataSet;
    taPT_Ext: TpFIBDataSet;
    frPTHead_Ext: TfrDBDataSet;
    frPT_Ext: TfrDBDataSet;
    frAW_SelfMat_ext: TfrDBDataSet;
    frAW_Ext: TfrDBDataSet;
    taAW_Ext: TpFIBDataSet;
    taAWG_Head: TpFIBDataSet;
    frAWG_Head: TfrDBDataSet;
    taAWG_HeadBD: TFIBDateTimeField;
    taAWG_HeadED: TFIBDateTimeField;
    taAWG_HeadSELFCOMPNAME: TFIBStringField;
    taAWG_HeadSUPNAME: TFIBStringField;
    taMB_Move: TpFIBDataSet;
    frMB_Move: TfrDBDataSet;
    frXMLExcelExport1: TfrXMLExcelExport;
    frActWorkCommon: TfrDBDataSet;
    frActWorkListCommon: TfrDBDataSet;
    taMX18Common: TpFIBDataSet;
    taMX18ItemCommon: TpFIBDataSet;
    frMX18Common: TfrDBDataSet;
    frMX18ItemCommon: TfrDBDataSet;
    taVerActMaster: TpFIBDataSet;
    taVerActDetail: TpFIBDataSet;
    frVerActMaster: TfrDBDataSet;
    frVerActDetail: TfrDBDataSet;
    dsrVerActMaster: TDataSource;
    dsrVerActDetail: TDataSource;
    taPT_ExtMATNAME: TFIBStringField;
    taPT_ExtGETW: TFIBFloatField;
    taPT_ExtSCOST: TFIBFloatField;
    taPT_ExtNW: TFIBFloatField;
    taPT_ExtSCOST1: TFIBFloatField;
    taPT_ExtN: TFIBFloatField;
    taPT_ExtGETW1: TFIBFloatField;
    taPT_ExtOUTW: TFIBFloatField;
    taInvUIDUID: TFIBIntegerField;
    taInvUIDART: TFIBStringField;
    taInvUIDART2: TFIBStringField;
    taInvUIDW: TFIBFloatField;
    taInvUIDSZNAME: TFIBStringField;
    taInvUIDSEMIS: TFIBStringField;
    taInvUIDTPRICE: TFIBFloatField;
    taInvUIDPRICE2: TFIBFloatField;
    taInvUIDPRILL: TFIBStringField;
    taInvUIDQ: TFIBIntegerField;
    taInvUIDMATNAME: TFIBStringField;
    taInvUIDINSNAME: TFIBStringField;
    taInvUIDART2ID: TFIBIntegerField;
    taInvListPrint: TpFIBDataSet;
    frInvListPrint: TfrDBDataSet;
    procedure SelectedInvsGetValue(const ParName: String; var ParValue: Variant);
    procedure taPItem_PS_BalanceBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taRevision_SumBeforeOpen(DataSet: TDataSet);
    procedure taAWMOTotalItemBeforeOpen(DataSet: TDataSet);
    procedure taCompAfterOpen(DataSet: TDataSet);
    procedure taMX18ItemCommonBeforeOpen(DataSet: TDataSet);
    procedure taVerActMasterBeforeOpen(DataSet: TDataSet);
    procedure taVerActDetailBeforeOpen(DataSet: TDataSet);
    procedure taPT_ExtAfterOpen(DataSet: TDataSet);
    procedure taPTHead_ExtAfterOpen(DataSet: TDataSet);
    procedure taInvUIDBeforeOpen(DataSet: TDataSet);
    procedure taInvUIDAfterScroll(DataSet: TDataSet);
  public
    InvNumbers: String;
    procedure SListToPrint(InvIDs: string);
  end;

var
  dmPrint2: TdmPrint2;

implementation

uses Data, DictData, dbUtil, InvData, MainData, SIList;

{$R *.dfm}

procedure TdmPrint2.taPItem_PS_BalanceBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    //???
    //ByName['PROTOCOLID'].AsInteger := dmPrint.taProtocol_PSPROTOCOLID.AsInteger;
    //ByName['OPERID'].AsString := dmPrint.taProtocol_PSOPERID.AsString;
  end;
end;

procedure TdmPrint2.taPTHead_ExtAfterOpen(DataSet: TDataSet);
begin
  //dmInv.taSListCalcPrihod.CloseOpen(true);
end;

procedure TdmPrint2.taPT_ExtAfterOpen(DataSet: TDataSet);
begin
  //ShowMessage('INVID: ' + taPT_Ext.ParamByName('InvID').AsString + ' ; ' + 'SUPID' + taPT_Ext.ParamByName('SUPID').AsString);
  //if not DataSet.IsEmpty then ShowMessage('PT_Ext is empty') else ShowMessage(TpFIBDataset(DataSet).FieldByName('MATNAME').AsString);
end;

procedure TdmPrint2.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TdmPrint2.taRevision_SumBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['INVID'].AsInteger := dmInv.FCurrINVID;
  end;
end;

procedure TdmPrint2.SelectedInvsGetValue(const ParName: String; var ParValue: Variant);
begin
  if ParName = '���������' then ParValue := InvNumbers;
  if ParName = '���' then
    case taInvListPrint.FieldByName('ITYPE').AsInteger of
      13: ParValue := '�����������';
      14: ParValue := '�����������';
      15: ParValue := '�������';
    end;
end;


procedure TdmPrint2.SListToPrint(InvIDs: string);
begin
  taInvListPrint.Close;
  taInvListPrint.SelectSQL.Text := 'select s.Semis, ds.SEMIS SemisName, i.IType,' + #10#13
  + 'sum(s.W) W, sum (s.Q) Q ' + #10#13
  + 'from SIEL s' + #10#13
  + 'left outer join D_Semis ds on (s.Semis = ds.semisid)' + #10#13
  + 'left outer join Inv i on (s.invID = i.InvID)' + #10#13
  + 'where s.InvID in (' + InvIDs + ')' + #13#10
  + 'group by s.SEMIS, ds.SEMIS, i.iType';

  taInvListPrint.Open;
  with dmPrint do
    begin
      FDocID := 102;
      taDoc.Open;
      frReport.LoadFromDb(taDoc, FDocID);
      frReport.OnGetValue := SelectedInvsGetValue;
      frReport.PrepareReport;
      if dm.IsAdm then
        frReport.DesignReport
      else
        frReport.PrintPreparedReport('', 1, frReport.DefaultCollate, frAll);
    end;
end;

procedure TdmPrint2.taVerActDetailBeforeOpen(DataSet: TDataSet);
begin
    taVerActDetail.ParamByName('InvID').AsInteger := dmData.taVerificationListINVID.AsInteger;
end;

procedure TdmPrint2.taVerActMasterBeforeOpen(DataSet: TDataSet);
begin
  taVerActMaster.ParamByName('InvID').AsInteger := dmData.taVerificationListINVID.AsInteger;
end;

procedure TdmPrint2.taAWMOTotalItemBeforeOpen(DataSet: TDataSet);
begin
  taAWMOTotalItem.ParamByName('INVID').AsInteger := taAWMOTotal.FieldByName('INVID').AsInteger;
  taAWMOTotalItem.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
  taAWMOTotalItem.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
end;

procedure TdmPrint2.taCompAfterOpen(DataSet: TDataSet);
begin
 // ShowMessage(':COMPID1 = ' + taComp.ParamByName('COMPID1').AsString + ', COMPID2 = ' + taComp.ParamByName('COMPID2').AsString)
end;

procedure TdmPrint2.taInvUIDAfterScroll(DataSet: TDataSet);
begin
  dmPrint.taStoneItems.CloseOpen(false);
end;

procedure TdmPrint2.taInvUIDBeforeOpen(DataSet: TDataSet);
begin
  taInvUID.ParamByName('InvID').AsInteger := dmInv.taSList.FieldByName('InvId').AsInteger;
end;

procedure TdmPrint2.taMX18ItemCommonBeforeOpen(DataSet: TDataSet);
begin
  taMX18ItemCommon.ParamByName('Company$ID').AsInteger := StrToInt(dmMain.FilterCompId);
  taMX18ItemCommon.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
  taMX18ItemCommon.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
end;

end.
