unit SelectInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList,  ImgList,  ExtCtrls,
  ComCtrls, Grids, DBGridEh, dbUtil, Period, DBGridEhGrouping, GridsEh,
  rxPlacemnt, rxSpeedbar;

type
  TfmSelectInv = class(TfmAncestor)
    gridInv: TDBGridEh;
    SpeedItem1: TSpeedItem;
    acPeriod: TAction;
    SpeedItem2: TSpeedItem;
    acSelectAll: TAction;
    acDiselectAll: TAction;
    SpeedItem3: TSpeedItem;
    acAnaliz: TAction;
    SpeedItem4: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acSelectAllExecute(Sender: TObject);
    procedure acDiselectAllExecute(Sender: TObject);
    procedure gridInvGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acAnalizExecute(Sender: TObject);
  public
    FBD,FED:TDateTime;
  end;

var
  fmSelectInv: TfmSelectInv;

implementation

uses Data, DictData, InvData, DB;

{$R *.dfm}

procedure TfmSelectInv.FormCreate(Sender: TObject);
begin
//inherited;
 // OpenDataSets([dmAdd.taOutAnaliz]);
  FBD:=dmData.BeginDate;
  FED:=dmData.EndDate;
  dminv.sqlPublic.SQL.Clear;
  dminv.sqlPublic.SQL.Add('execute procedure CREATE_TMP_SLIST_FOR_COMP(:COMPID,:BDATE,:EDATE)');
  dminv.sqlPublic.ParamByName('COMPID').AsInteger:=dminv.taSListSUPID.AsInteger;
  dminv.sqlPublic.ParamByName('BDATE').AsTimeStamp:=DateTimeToTimeStamp(dmData.BeginDate);
  dminv.sqlPublic.ParamByName('EDATE').AsTimeStamp:=DateTimeToTimeStamp(dmData.EndDate);
  dminv.sqlPublic.ExecQuery;
  dminv.sqlPublic.Transaction.CommitRetaining;
  OpenDataSet(dmData.taOutAnaliz);
end;

procedure TfmSelectInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PostDataSet(dmData.taOutAnaliz);
  CloseDataSet(dmData.taOutAnaliz);
  inherited;
end;

procedure TfmSelectInv.acPeriodExecute(Sender: TObject);
begin
  inherited;
  if ShowPeriodForm(FBD, FED) then
  begin
    dmData.BeginDate:=FBD;
    dmData.EndDate:=FED;
    dmInv.sqlPublic.SQL.Clear;
    dmInv.sqlPublic.SQL.Add('execute procedure CREATE_TMP_SLIST_FOR_COMP(:COMPID,:BDATE,:EDATE)');
    dmInv.sqlPublic.ParamByName('COMPID').AsInteger:=dminv.taSListSUPID.AsInteger;
    dmInv.sqlPublic.ParamByName('BDATE').AsTimeStamp:=DateTimeToTimeStamp(dmData.BeginDate);
    dmInv.sqlPublic.ParamByName('EDATE').AsTimeStamp:=DateTimeToTimeStamp(dmData.EndDate);
    dmInv.sqlPublic.ExecQuery;
    dmInv.sqlPublic.Transaction.CommitRetaining;
    ReOpenDataSet(dmData.taOutAnaliz);
  end;
end;

procedure TfmSelectInv.acSelectAllExecute(Sender: TObject);
begin
    dminv.sqlPublic.SQL.Clear;
    dminv.sqlPublic.SQL.Add('update TMP_SLIST_FOR_COMP set a=1');
    dminv.sqlPublic.ExecQuery;
    dminv.sqlPublic.Transaction.CommitRetaining;
    ReOpenDataSet(dmData.taOutAnaliz);
end;

procedure TfmSelectInv.acDiselectAllExecute(Sender: TObject);
begin
  inherited;
  dminv.sqlPublic.SQL.Clear;
  dminv.sqlPublic.SQL.Add('update TMP_SLIST_FOR_COMP set a=0');
  dminv.sqlPublic.ExecQuery;
  dminv.sqlPublic.Transaction.CommitRetaining;
  ReOpenDataSet(dmData.taOutAnaliz);
end;

procedure TfmSelectInv.gridInvGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if dmData.taOutAnalizTYPENAME.AsString='�������' then Background:=clSell
  else Background:=clOut;
end;

procedure TfmSelectInv.acAnalizExecute(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crHourGlass;
  dminv.sqlPublic.SQL.Clear;
  dminv.sqlPublic.SQL.Add('execute procedure Create_OUT_STORE(:COMPID)');
  dminv.sqlPublic.ParamByName('COMPID').AsInteger:=dminv.taSListSUPID.AsInteger;
  dminv.sqlPublic.ExecQuery;
  dminv.sqlPublic.Transaction.CommitRetaining;
  ReOpenDataSet(dmData.taOutAnaliz);
  Screen.Cursor := crDefault;
end;

end.
