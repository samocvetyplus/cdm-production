inherited fmdRej: TfmdRej
  Left = 180
  Top = 188
  Width = 519
  Height = 411
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085#1085#1086#1075#1086' '#1073#1088#1072#1082#1072
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 358
    Width = 511
  end
  inherited plDict: TPanel
    Width = 511
    Height = 316
    inherited plSelect: TPanel
      Top = 273
      Width = 507
      inherited btnOk: TSpeedButton
        Left = 329
      end
      inherited btnCancel: TSpeedButton
        Left = 413
      end
    end
    inherited gridDict: TDBGridEh
      Width = 507
      Height = 271
      DataSource = dm.dsrRej
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NAME'
          Footers = <>
          Title.EndEllipsis = True
          Width = 165
        end
        item
          Checkboxes = True
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'PSIDMUSTHAVEVALUE'
          Footers = <>
          KeyList.Strings = (
            '1'
            '0')
          Title.EndEllipsis = True
          Width = 67
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          DropDownRows = 15
          DropDownSpecRow.CellsText = #1085#1077' '#1079#1072#1076#1072#1085
          DropDownSpecRow.ShortCut = 16573
          DropDownSpecRow.Visible = True
          DropDownWidth = -1
          EditButtons = <>
          FieldName = 'PSNAME'
          Footers = <>
          Title.EndEllipsis = True
          Width = 133
        end>
    end
  end
  inherited tb1: TSpeedBar
    Width = 511
  end
  inherited fmstr: TFormStorage
    Active = False
  end
end
