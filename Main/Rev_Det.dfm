inherited fmRev_Det: TfmRev_Det
  Left = 249
  Top = 228
  Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
  ClientHeight = 385
  ExplicitWidth = 320
  ExplicitHeight = 419
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 366
    Panels = <
      item
        Width = 100
      end
      item
        Width = 50
      end>
    ExplicitTop = 366
  end
  inherited tb1: TSpeedBar
    inherited spitDel: TSpeedItem [2]
    end
    inherited spitPrint: TSpeedItem [3]
      Action = acPrint
      DropDownMenu = nil
      Hint = ''
      MarkDropDown = False
      OnClick = acPrintExecute
    end
    inherited spitClose: TSpeedItem [4]
    end
    inherited spitAdd: TSpeedItem [5]
      Action = acAdd
      Hint = ''
      OnClick = acAddExecute
    end
  end
  inherited plMain: TPanel
    Height = 55
    ExplicitHeight = 55
    inherited lbDep: TLabel
      Visible = False
    end
    inherited txtDep: TDBText
      Left = 244
      Visible = False
      ExplicitLeft = 244
    end
    object lbScanUID: TLabel [4]
      Left = 204
      Top = 24
      Width = 7
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -19
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited edNoDoc: TDBEditEh
      DataField = 'DOCNO'
    end
    inherited dtedDateDoc: TDBDateTimeEditEh
      DataField = 'DOCDATE'
    end
  end
  inherited gridItem: TDBGridEh
    Top = 120
    Height = 225
    Color = clWhite
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    SumList.Active = True
    UseMultiTitle = True
    OnGetCellParams = gridItemGetCellParams
    Columns = <
      item
        Color = 15396590
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'UID'
        Footer.FieldName = 'ID'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clBlue
        Footer.Font.Height = -11
        Footer.Font.Name = 'MS Sans Serif'
        Footer.Font.Style = []
        Footer.ValueType = fvtCount
        Footers = <>
        Width = 76
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Width = 91
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'STATE'
        Footers = <>
        KeyList.Strings = (
          '0'
          '1'
          '2')
        PickList.Strings = (
          #1053#1077#1090' '#1074' '#1085#1072#1083#1080#1095#1080#1080
          #1045#1089#1090#1100' '#1074' '#1085#1072#1083#1080#1095#1080#1080
          #1051#1080#1096#1085#1077#1077)
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'PRICE'
        Footers = <>
      end>
  end
  object TBDock1: TTBDock [4]
    Left = 0
    Top = 97
    Width = 657
    Height = 23
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      Caption = 'TBToolbar1'
      TabOrder = 0
      object TBControlItem1: TTBControlItem
        Control = Label1
      end
      object TBControlItem2: TTBControlItem
        Control = edFilterArt
      end
      object TBSeparatorItem1: TTBSeparatorItem
      end
      object TBItem1: TTBItem
        Action = acUIDHisttory
      end
      object Label1: TLabel
        Left = 0
        Top = 3
        Width = 106
        Height = 13
        Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1072#1088#1090#1080#1082#1091#1083#1091' '
      end
      object edFilterArt: TDBEditEh
        Left = 106
        Top = 0
        Width = 121
        Height = 19
        EditButtons = <>
        Flat = True
        TabOrder = 0
        Visible = True
        OnChange = edFilterArtChange
      end
    end
  end
  object ProgressBar: TcxDBProgressBar [5]
    Left = 0
    Top = 345
    Align = alBottom
    DataBinding.DataField = 'REVISIONPERCENT'
    DataBinding.DataSource = dmInv.dsrSList
    TabOrder = 5
    Width = 657
  end
  inherited ilButtons: TImageList
    Left = 318
    Top = 188
  end
  inherited ActionList2: TActionList
    object acAdd: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = acAddExecute
    end
    object acClose: TAction
      Caption = 'acClose'
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      OnExecute = acPrintExecute
    end
    object acUIDHisttory: TAction
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1103
      OnExecute = acUIDHisttoryExecute
    end
  end
  inherited ppPrint: TTBPopupMenu
    Left = 248
    Top = 180
  end
end
