object PatternsImport: TPatternsImport
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 234
  Width = 278
  object OD: TOpenDialog
    DefaultExt = '*.xls, *.xslx'
    Filter = '*.xls,*.xlsx'
    InitialDir = 'Y:\_'#1055#1088#1072#1081#1089#1099
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing, ofDontAddToRecent]
    Title = #1042#1099#1073#1077#1088#1080#1090#1077' '#1092#1072#1081#1083' '#1089' '#1094#1077#1085#1072#1084#1080
    Left = 8
    Top = 104
  end
  object XLS: TXLSReadWriteII4
    Version = xvExcelUnknown
    Sheets = <
      item
        Name = 'Sheet1'
        DefaultColWidth = 8
        DefaultRowHeight = 255
        PrintSettings.Copies = 0
        PrintSettings.FooterMargin = 0.500000000000000000
        PrintSettings.FooterMarginCm = 1.270000000000000000
        PrintSettings.HeaderMargin = 0.500000000000000000
        PrintSettings.HeaderMarginCm = 1.270000000000000000
        PrintSettings.MarginBottom = 1.000000000000000000
        PrintSettings.MarginLeft = 0.750000000000000000
        PrintSettings.MarginRight = 0.750000000000000000
        PrintSettings.MarginTop = 1.000000000000000000
        PrintSettings.MarginBottomCm = 2.540000000000000000
        PrintSettings.MarginLeftCm = 1.905000000000000000
        PrintSettings.MarginRightCm = 1.905000000000000000
        PrintSettings.MarginTopCm = 2.540000000000000000
        PrintSettings.Options = [psoPortrait]
        PrintSettings.PaperSize = psA4
        PrintSettings.ScalingFactor = 100
        PrintSettings.StartingPage = 1
        PrintSettings.HorizPagebreaks = <>
        PrintSettings.VertPagebreaks = <>
        PrintSettings.Resolution = 600
        PrintSettings.FitWidth = 1
        PrintSettings.FitHeight = 1
        MergedCells = <>
        Options = [soGridlines, soRowColHeadings, soShowZeros]
        WorkspaceOptions = [woShowAutoBreaks, woRowSumsBelow, woColSumsRight, woOutlineSymbols]
        SheetProtection = [spEditObjects, spEditScenarios, spEditCellFormatting, spEditColumnFormatting, spEditRowFormatting, spInsertColumns, spInsertRows, spInsertHyperlinks, spDeleteColumns, spDeleteRows, spSelectLockedCells, spSortCellRange, spEditAutoFileters, spEditPivotTables, spSelectUnlockedCells]
        Zoom = 0
        ZoomPreview = 0
        RecalcFormulas = True
        Hidden = hsVisible
        Validations = <>
        DrawingObjects.Texts = <>
        DrawingObjects.Notes = <>
        DrawingObjects.Basics = <>
        DrawingObjects.AutoShapes = <>
        DrawingObjects.Pictures = <>
        ControlsObjects.ListBoxes = <>
        ControlsObjects.ComboBoxes = <>
        ControlsObjects.Buttons = <>
        ControlsObjects.CheckBoxes = <>
        ControlsObjects.RadioButtons = <>
        Hyperlinks = <>
        ConditionalFormats = <>
      end>
    Workbook.Left = 100
    Workbook.Top = 100
    Workbook.Width = 10000
    Workbook.Height = 7000
    Workbook.SelectedTab = 0
    Workbook.Options = [woHScroll, woVScroll, woTabs]
    OptionsDialog.SaveExtLinkVal = False
    OptionsDialog.CalcCount = 100
    OptionsDialog.CalcMode = cmAutomatic
    OptionsDialog.Delta = 0.001000000000000000
    OptionsDialog.ShowObjects = soShowAll
    OptionsDialog.Iteration = False
    OptionsDialog.PrecisionAsDisplayed = True
    OptionsDialog.R1C1Mode = False
    OptionsDialog.RecalcBeforeSave = False
    OptionsDialog.Uncalced = False
    OptionsDialog.SaveRecalc = True
    BookProtected = False
    Backup = False
    RefreshAll = False
    StrTRUE = 'TRUE'
    StrFALSE = 'FALSE'
    ShowFormulas = False
    IsMac = False
    PreserveMacros = True
    ComponentVersion = '4.00.20a'
    MSOPictures = <>
    RecomendReadOnly = False
    TempFileMode = tfmOnDisk
    Left = 8
    Top = 160
  end
  object ds_main: TpFIBDataSet
    Left = 152
    Top = 56
  end
  object ds_technology: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  tga.article$id,'
      '  da.art, '
      '  tg.price$position,'
      '  smartround(da.mw, 1, 1000) middle$weight'
      'from '
      '  technology$group$articles tga '
      
        '  left outer join technology$groups tg on (tga.group$id = tg.gro' +
        'up$id)'
      '  left outer join d_art da on (tga.article$id = da.d_artid)'
      'where '
      '  tg.price$position > 0 and'
      '  tg.group$material$id = :material$id'
      'order by'
      '  tg.price$position, tga.article$id'
      '  '
      '  ')
    BeforeOpen = ds_technologyBeforeOpen
    Transaction = TR
    Database = DB
    Left = 152
    Top = 8
    object ds_technologyARTICLEID: TFIBIntegerField
      FieldName = 'ARTICLE$ID'
    end
    object ds_technologyART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object ds_technologyPRICEPOSITION: TFIBSmallIntField
      FieldName = 'PRICE$POSITION'
    end
    object ds_technologyMIDDLEWEIGHT: TFIBFloatField
      FieldName = 'MIDDLE$WEIGHT'
    end
  end
  object CDS: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 216
    Top = 8
  end
  object Patterns: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from pattern$items'
      'where company$id = 1'
      'order by id')
    Transaction = TR
    Database = DB
    Left = 88
    Top = 8
    object PatternsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object PatternsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 256
      EmptyStrToNull = True
    end
    object PatternsPATTERNORDER: TFIBIntegerField
      FieldName = 'PATTERN$ORDER'
    end
    object PatternsBITPARAMS: TFIBIntegerField
      FieldName = 'BIT$PARAMS'
    end
    object PatternsCOMPANYID: TFIBIntegerField
      FieldName = 'COMPANY$ID'
    end
    object PatternsFIELDNAME: TFIBStringField
      FieldName = 'FIELD$NAME'
      Size = 64
      EmptyStrToNull = True
    end
  end
  object TR: TpFIBTransaction
    Active = True
    DefaultDatabase = DB
    TimeoutAction = TARollback
    TRParams.Strings = (
      'write'
      'isc_tpb_nowait'
      'read_committed'
      'rec_version')
    TPBMode = tpbDefault
    Left = 8
    Top = 56
  end
  object DB: TpFIBDatabase
    Connected = True
    DBName = '10.1.0.21:production'
    DBParams.Strings = (
      'user_name=SYSDBA'
      'password=masterkey'
      'lc_ctype=WIN1251')
    DefaultTransaction = TR
    SQLDialect = 1
    Timeout = 0
    WaitForRestoreConnect = 0
    Left = 8
    Top = 8
  end
  object Settings: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from pattern$settings'
      'where company$id = 1')
    Transaction = TR
    Database = DB
    Left = 88
    Top = 56
    object SettingsCOMPANYID: TFIBIntegerField
      FieldName = 'COMPANY$ID'
    end
    object SettingsFILENAME: TFIBStringField
      FieldName = 'FILE$NAME'
      Size = 256
      EmptyStrToNull = True
    end
    object SettingsDECIMALDIGITS: TFIBIntegerField
      FieldName = 'DECIMAL$DIGITS'
    end
    object SettingsOUTPUTDIRECTORY: TFIBStringField
      FieldName = 'OUTPUT$DIRECTORY'
      Size = 256
      EmptyStrToNull = True
    end
    object SettingsBITPARAMS: TFIBIntegerField
      FieldName = 'BIT$PARAMS'
    end
    object SettingsMAXSKIPCELLSCOUNT: TFIBSmallIntField
      FieldName = 'MAX$SKIP$CELLS$COUNT'
    end
  end
end
