unit InvCopy;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, RXCtrls, RxDBComb, RxToolEdit, Mask, RXDBCtrl,
  RxLookup, DBUtil, RXSpin, ExtCtrls, DBCtrlsEh, DBLookupEh,
  DBGridEh, DB, FIBDataSet, pFIBDataSet, FIBQuery, pFIBQuery;

type
  TCopyInvInfo = record
    DefaultComp   : integer;
    CanChangeComp : boolean;
    DefaultDep    : integer;
    FromDep       : integer;
    CanChangeDep  : boolean;
    //DepTypes      : integer; // ��� ������������� 
    DepComp       : boolean; // �������� ��� ������������� ��� ������ ��� DefaultCompId
    BetweenOrg    : boolean; // ����������� ���� ����� �������������
    CopyInvId     : integer; // ��������� �� ��������� ������� �������� �����
    CopyTypeFrom  : integer; // ��� ��������� �� ��������� ������� �������� �����
    CopyType      : integer; // ��� ��������� ������� ���������� ��� �����������
    NDSId         : integer; // ������ �� ���������� ������ ��� default = -1
    SelfCompId    : integer; 
  end;

  TfmInvCopy = class(TForm)
    btnCreate: TBitBtn;
    bvl: TBevel;
    Label1: TLabel;
    Label2: TLabel;
    lbDep: TLabel;
    Label4: TLabel;
    edDocNo: TDBNumberEditEh;
    edDocDate: TDBDateTimeEditEh;
    btnCancel: TBitBtn;
    lcSup: TDBLookupComboboxEh;
    lcNDS: TDBLookupComboboxEh;
    taWhDep: TpFIBDataSet;
    dsrWhDep: TDataSource;
    Label3: TLabel;
    lcDep: TDBLookupComboboxEh;
    taComp: TpFIBDataSet;
    dsrComp: TDataSource;
    sqlInvCopy: TpFIBQuery;
    procedure btnCreateClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FNDSActive : boolean; // ��� �� ������ ���������� ���
    FInfo : TCopyInvInfo;
    procedure Init;
  end;

procedure ShowInvCopy(CopyInfo : TCopyInvInfo; const Exec : boolean=False);

implementation

uses InvData, DictData, MainData, ProductionConsts, fmUtils, MsgDialog;

{$R *.dfm}

procedure ShowInvCopy(CopyInfo : TCopyInvInfo;const Exec: boolean = False);
var
  fmInvCopy : TfmInvCopy;
begin
  fmInvCopy := TfmInvCopy.Create(nil);
  try
    fmInvCopy.FInfo := CopyInfo;
    fmInvCopy.Init;
    if not Exec then fmInvCopy.ShowModal
    else begin
       fmInvCopy.btnCreateClick(nil);
    end
  finally
    FreeAndNil(fmInvCopy);
  end;
end;

procedure TfmInvCopy.btnCreateClick(Sender: TObject);
var
  ControlDate:TDateTime;
begin
  if (lcSup.Text='')or VarIsNull(lcSup.KeyValue) then
  begin
    ActiveControl := lcSup;
    raise EWarning.Create(rsNotDefineCompId);
  end;
  if (lcDep.Text='')or VarIsNull(lcDep.KeyValue) then
  begin
    ActiveControl := lcDep;
    raise EWarning.Create(rsNotDefineWh);
  end;
  if (lcNDS.Text='')or VarIsNull(lcNDS.KeyValue) then
  begin
    ActiveControl := lcNDS;
    raise EWarning.Create(rsNotDefineNDS);
  end;
  with FInfo do
  begin
    sqlInvCopy.ParamByName('INVID').AsInteger:= CopyInvId;
    sqlInvCopy.ParamByName('OUTTYPE').AsInteger:= CopyType;
    sqlInvCopy.ParamByName('DOCNO').AsInteger := edDocNo.Value;
    sqlInvCopy.ParamByName('DOCDATE').AsDateTime := edDocDate.Value;
    sqlInvCopy.ParamByName('NDSID').AsVariant := lcNDS.KeyValue;
    sqlInvCopy.ParamByName('USERID').AsInteger := dm.User.UserId;
    case CopyTypeFrom of
      // �� ������� � ������� �� ����������
      2 : case CopyType of
            3 : begin
              sqlInvCopy.ParamByName('DEPID').AsInteger := lcDep.KeyValue;
              sqlInvCopy.ParamByName('SUPID').AsInteger := lcSup.KeyValue;
            end
            else raise EInternal.Create(Format(rsInternalError, ['221']));
          end;
      3 : case CopyType of
            // �� �������� �� ���������� � �������
            2 : begin
              sqlINVCopy.ParamByName('DEPID').AsInteger:= lcDep.KeyValue;
              sqlINVCopy.ParamByName('SUPID').AsInteger:= lcSup.KeyValue;
              sqlINVCopy.ParamByName('FROMDEPID').AsInteger := FInfo.SelfCompId;
            end;
            // �� ������� �� ���������� � ������� �� ������������
            12 : begin
               ControlDate:=dm.GetDepBeginDate(lcSup.KeyValue);
               if ControlDate>edDocDate.Value then
               begin
                 Showmessage('���� ��������� �� ����� ���� ������ ���� ������������');
                 eXit;
               end;
               sqlINVCopy.ParamByName('DEPID').AsInteger:= lcDep.KeyValue;
               sqlINVCopy.ParamByName('FROMDEPID').AsInteger:= FInfo.FromDep;
            end
            else raise EInternal.Create(Format(rsInternalError, ['221']));
         end;
      // �� �������� � �������
      4 : case CopyType of
           2 : begin
             sqlINVCopy.ParamByName('DEPID').AsInteger:= lcDep.KeyValue;
             sqlINVCopy.ParamByName('SUPID').AsInteger:= lcSup.KeyValue;
             sqlINVCopy.ParamByName('FROMDEPID').AsInteger := FInfo.SelfCompId;
           end
           else raise EInternal.Create(Format(rsInternalError, ['221']));
         end;
      // �� ����������� ����������
      13 : case CopyType of
             15 : begin // � ������ ����������
               sqlInvCopy.ParamByName('DEPID').AsInteger:= lcDep.KeyValue; //dmMain.taSIListDEPID.AsInteger;
               sqlInvCopy.ParamByName('SUPID').AsInteger:= lcSup.KeyValue;
             end
             else raise EInternal.Create(Format(rsInternalError, ['221']));
           end;
      // �� ������� ����������
      15 : case CopyType of
             13 : begin // � ����������� ���������� ������ �����������
               sqlINVCopy.ParamByName('DEPID').AsInteger := lcDep.KeyValue;
               sqlINVCopy.ParamByName('SUPID').AsInteger := dm.User.SelfCompId;
             end
             else raise EInternal.Create(Format(rsInternalError, ['221']));
           end;
      else raise EInternal.Create(Format(rsInternalError, ['222']));
    end; // case
  end; // with
 // showmessage(sqlinvcopy.parambyname('outtype').AsString);
  sqlInvCopy.ExecQuery;
  sqlInvCopy.Transaction.CommitRetaining;
  ModalResult:=mrOk;
  MessageDialog('����������� ������� ���������', mtInformation, [mbOk], 0);
end;

procedure TfmInvCopy.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([taComp, taWhDep]);
  if not FNDSActive then CloseDataSet(dm.taNDS);
  Action := caFree;
end;

procedure TfmInvCopy.Init;
begin
  // ������������ ������ ���������
  case FInfo.CopyType of
    { TODO : ���� }
    2 : // �������
      case FInfo.CopyTypeFrom of
        3,4 : edDocNo.Value := dm.GetId(24);
        else raise EInternal.Create(Format(rsInternalError, ['222']));
      end;
    { TODO : ���� }
    3 : // ������� �� ����������
      case FInfo.CopyTypeFrom of
        2 : edDocNo.Value := dm.GetId(25);
        else raise EInternal.Create(Format(rsInternalError, ['222']));
      end;
    { TODO : ���� }
    12 : // ������� �� ������������
      case FInfo.CopyTypeFrom of
        3 : begin
          edDocNo.Value := dm.GetId(46);
        end;
        else raise EInternal.Create(Format(rsInternalError, ['222']));
      end;
    { TODO : ���� }
    15 : // ������ ����������
      case FInfo.CopyTypeFrom of
        13 : edDocNo.Value := dm.GetId(36);
        else raise EInternal.Create(Format(rsInternalError, ['222']));
      end;
    13 : // ������ ����������
      case FInfo.CopyTypeFrom of
        15 : begin
          if FInfo.BetweenOrg then edDocNo.Value := dm.GetId4(1, FInfo.DefaultComp)
          else edDocNo.Value := dm.GetId(31);
        end;
        else raise EInternal.Create(Format(rsInternalError, ['222']));
      end;
    else raise EInternal.Create(Format(rsInternalError, ['221']));
  end;
  edDocDate.Value := dm.ServerDateTime;
  FNDSActive := dm.taNDS.Active;
  if not FNDSActive then OpenDataSet(dm.taNDS);
  if not(FInfo.NDSId = -1) then lcNDS.KeyValue := FInfo.NDSId;
  if FInfo.CanChangeDep then
  begin
    if FInfo.DepComp then
    begin
      taWhDep.ParamByName('SELFCOMPID1').AsInteger := FInfo.DefaultComp;
      taWhDep.ParamByName('SELFCOMPID2').AsInteger := FInfo.DefaultComp;
    end
    else begin
      taWhDep.ParamByName('SELFCOMPID1').AsInteger := -MAXINT;
      taWhDep.ParamByName('SELFCOMPID2').AsInteger := MAXINT;
    end;
  end
  else begin
    taWhDep.ParamByName('SELFCOMPID1').AsInteger := -MAXINT;
    taWhDep.ParamByName('SELFCOMPID2').AsInteger := MAXINT;
  end;
  OpenDataSets([taComp, taWhDep]);
  if(FInfo.DefaultDep<>-1) then lcDep.KeyValue := FInfo.DefaultDep;
  // ���������� ����� � �����������
  with FInfo do
  begin
    lcSup.Enabled := CanChangeComp;
    if not CanChangeComp then
      lcSup.KeyValue := DefaultComp;
    lcDep.Enabled := CanChangeDep;
    if not CanChangeDep then
      lcDep.KeyValue := DefaultDep;
  end;
end;

end.
