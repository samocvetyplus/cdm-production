object dmMain: TdmMain
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 530
  Width = 983
  object taWOrderList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update WOrder set'
      '   DOCNO=:DOCNO, '
      '   DOCDATE=:DOCDATE,  '
      '   MOLID=:MOLID, '
      '   DEPID=:DEPID,'
      '   JOBFACE=:JOBID, '
      '   ISCLOSE=:ISCLOSE,       '
      '   DEFOPER=:DEFOPER,'
      '   JOBDEPID=:JOBDEPID,'
      '   PROTOCOLID=:PROTOCOLID'
      'where WOrderId=:OLD_WOrderId')
    DeleteSQL.Strings = (
      'delete from WOrder where WOrderId=:OLD_WOrderId')
    InsertSQL.Strings = (
      'insert into WOrder(WORDERID, DOCNO, DOCDATE, MOLID, DEPID,'
      '    JOBFACE, ISCLOSE, DEFOPER, JOBDEPID, PROTOCOLID)'
      'values (:WORDERID, :DOCNO, :DOCDATE,  :MOLID, :DEPID, '
      '   :JOBID, :ISCLOSE, :DEFOPER, :JOBDEPID, :PROTOCOLID)')
    RefreshSQL.Strings = (
      'select WORDERID, DOCNO, DOCDATE,  MOLID, FIO, DEPID,'
      '    DEPNAME,  JOBID, JOBFACE, ISCLOSE, DEFOPER, DEFOPERNAME, '
      '    JOBDEPID, JOBDEPNAME, PROTOCOLID'
      'from WOrderList_R(:WOrderId)')
    SelectSQL.Strings = (
      'select WORDERID, DOCNO, DOCDATE,  MOLID, FIO, DEPID,'
      
        '    DEPNAME, JOBID, JOBFACE, ISCLOSE, DEFOPER, DEFOPERNAME, JOBD' +
        'EPID, JOBDEPNAME, PROTOCOLID    '
      
        'from WOrderList_S(:BD, :ED, :DEP1, :DEP2, :SHOWKINDDOC1, :SHOWKI' +
        'NDDOC2)'
      'order by DocDate')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taWOrderListBeforeDelete
    BeforeEdit = taWOrderListBeforeEdit
    BeforeOpen = taWOrderListBeforeOpen
    OnNewRecord = taWOrderListNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy'
    Left = 24
    Top = 8
    object taWOrderListDOCNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1085#1072#1088#1103#1076#1072
      FieldName = 'DOCNO'
    end
    object taWOrderListWORDERID: TIntegerField
      FieldName = 'WORDERID'
    end
    object taWOrderListMOLID: TIntegerField
      FieldName = 'MOLID'
    end
    object taWOrderListFIO: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = True
    end
    object taWOrderListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
    end
    object taWOrderListJOBID: TIntegerField
      FieldName = 'JOBID'
    end
    object taWOrderListJOBFACE: TFIBStringField
      DisplayLabel = #1048#1089#1087#1086#1083#1085#1080#1090#1077#1083#1100
      FieldName = 'JOBFACE'
      Size = 60
      EmptyStrToNull = True
    end
    object taWOrderListDEPID: TIntegerField
      FieldName = 'DEPID'
    end
    object taWOrderListDEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'DEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taWOrderListDEFOPER: TFIBStringField
      FieldName = 'DEFOPER'
      Origin = 'WORDERLIST_S.DEFOPER'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWOrderListDEFOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102
      FieldName = 'DEFOPERNAME'
      Origin = 'WORDERLIST_S.DEFOPERNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWOrderListJOBDEPID: TIntegerField
      FieldName = 'JOBDEPID'
      Origin = 'WORDERLIST_S.JOBDEPID'
    end
    object taWOrderListJOBDEPNAME: TFIBStringField
      FieldName = 'JOBDEPNAME'
      Origin = 'WORDERLIST_S.JOBDEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taWOrderListPROTOCOLID: TIntegerField
      FieldName = 'PROTOCOLID'
      Origin = 'WORDERLIST_S.PROTOCOLID'
    end
    object taWOrderListDOCDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1088#1103#1076#1072
      FieldName = 'DOCDATE'
    end
  end
  object dsrWOrderList: TDataSource
    DataSet = taWOrderList
    Left = 24
    Top = 52
  end
  object taWOrder: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure WOItem_U(:WOITEMID, :WORDERID,  :OPERID, '
      
        '  :SEMIS,  :ITDATE, :Q,  :W,  :ITTYPE, :INVID,  :DOCNO, :DOCDATE' +
        ',  '
      
        '   :USERID, :E, :DEPID, :JOBDEPID, :JOBID, :AINV, :REF, :ART2ID,' +
        ' '
      
        '   :APPLINV, :FROMOPERID, :REJREF, :REJPSID, :REJID, :PROTOCOLID' +
        ')')
    DeleteSQL.Strings = (
      'delete from WOItem where WOItemId=:OLD_WOITEMID')
    InsertSQL.Strings = (
      
        'execute procedure WOItem_I(:WOITEMID, :WORDERID,  :OPERID, :SEMI' +
        'S,  :ITDATE,'
      '     :Q,  :W,  :ITTYPE,  :INVID, :USERID, :E, :DOCNO, :DOCDATE,'
      
        '     :DEPID, :JOBDEPID, :JOBID, :AINV, :REF, :ART2ID, :APPLINV, ' +
        ':FROMOPERID, :REJREF,'
      '     :REJPSID, :REJID, :PROTOCOLID)')
    RefreshSQL.Strings = (
      'select WOITEMID,  WORDERID, OPERID,  SEMIS,  SEMISNAME1, ITDATE,'
      '      Q,  W,  ITTYPE, DOCNO, DOCDATE, INVID, USERID, E,'
      '      DEPID, JOBDEPID, JOBID, AINV, REF, ART2ID, APPLINV,'
      '      FROMOPERID, ADOCNO, REJREF, REJPSID, REJPSNAME, FULLART,'
      '      ART, ART2, UQ, UW, REJID, PROTOCOLID'
      'from WOItem_R(:WOITEMID)')
    SelectSQL.Strings = (
      
        'select WOITEMID,  WORDERID, OPERID,  SEMIS, SEMISNAME1,         ' +
        '    ITDATE, Q,  W,  ITTYPE, INVID, DOCNO, DOCDATE, USERID, E, '
      '  DEPID, JOBDEPID, JOBID, AINV, REF,  ART2ID,'
      
        '  APPLINV, FROMOPERID, ADOCNO, REJREF, REJPSID,              REJ' +
        'PSNAME, FULLART, ART, ART2, UQ, UW, REJID,'
      '  PROTOCOLID'
      'from WOItem_S(:WORDERID)'
      'ORDER BY DOCNO, ITDATE')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = taWOrderAfterDelete
    AfterInsert = taWOrderAfterInsert
    AfterOpen = taWOrderAfterOpen
    AfterPost = taWOrderAfterPost
    AfterScroll = taWOrderAfterScroll
    BeforeDelete = taWOrderBeforeDelete
    BeforeEdit = taWOrderBeforeEdit
    BeforeInsert = taWOrderBeforeInsert
    BeforeOpen = taWOrderBeforeOpen
    BeforePost = taWOrderBeforePost
    OnCalcFields = CalcRecNo
    OnNewRecord = taWOrderNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 92
    Top = 8
    object taWOrderRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taWOrderWORDERID: TIntegerField
      FieldName = 'WORDERID'
    end
    object taWOrderWOITEMID: TIntegerField
      FieldName = 'WOITEMID'
    end
    object taWOrderOPERID: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERID'
      Size = 10
      EmptyStrToNull = True
    end
    object taWOrderSEMIS: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092'-'#1082
      FieldName = 'SEMIS'
      Size = 10
      EmptyStrToNull = True
    end
    object taWOrderITDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
      FieldName = 'ITDATE'
      DisplayFormat = 'DD.MM.YY'
    end
    object taWOrderITTYPE: TIntegerField
      DisplayLabel = #1058#1080#1087' '#1087#1077#1088#1077#1076#1072#1095#1080
      FieldName = 'ITTYPE'
      OnGetText = ITTYPEGetText
      OnSetText = ITTYPESetText
    end
    object taWOrderQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      DisplayWidth = 10
      FieldName = 'Q'
      OnChange = taWOrderQChange
    end
    object taWOrderW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      OnChange = taWOrderWChange
    end
    object taWOrderDOCNO: TIntegerField
      DisplayLabel = #8470' '#1085#1072#1082#1083'.'
      FieldName = 'DOCNO'
    end
    object taWOrderDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
      FieldName = 'DOCDATE'
      DisplayFormat = 'DD.MM.YY'
    end
    object taWOrderINVID: TIntegerField
      FieldName = 'INVID'
    end
    object taWOrderUserName: TStringField
      DisplayLabel = #1042#1099#1076#1072#1083
      FieldKind = fkLookup
      FieldName = 'USERNAME'
      LookupDataSet = taMolIO
      LookupKeyFields = 'MOLID'
      LookupResultField = 'FIO'
      KeyFields = 'USERID'
      LookupCache = True
      Size = 60
      Lookup = True
    end
    object taWOrderE: TSmallintField
      FieldName = 'E'
      Origin = 'WOITEM_S.E'
    end
    object taWOrderOPERNAME: TStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldKind = fkLookup
      FieldName = 'OPERNAME'
      LookupDataSet = dm.taOper
      LookupKeyFields = 'OPERID'
      LookupResultField = 'OPERATION'
      KeyFields = 'OPERID'
      LookupCache = True
      Size = 40
      Lookup = True
    end
    object taWOrderDEPID: TIntegerField
      DisplayLabel = #1052#1061'1'
      FieldName = 'DEPID'
      Origin = 'WOITEM_S.DEPID'
    end
    object taWOrderUSERID: TIntegerField
      DisplayLabel = #1042#1099#1076#1072#1083
      FieldName = 'USERID'
      Origin = 'WOITEM_S.USERID'
    end
    object taWOrderDEPNAME: TStringField
      DisplayLabel = #1052#1061'1'
      FieldKind = fkLookup
      FieldName = 'DEPNAME'
      LookupDataSet = dm.taDepart
      LookupKeyFields = 'DEPID'
      LookupResultField = 'NAME'
      KeyFields = 'DEPID'
      LookupCache = True
      Size = 40
      Lookup = True
    end
    object taWOrderJOBID: TIntegerField
      FieldName = 'JOBID'
      Origin = 'WOITEM_S.JOBID'
    end
    object taWOrderJOBDEPID: TIntegerField
      DisplayLabel = #1052#1061'2'
      FieldName = 'JOBDEPID'
      Origin = 'WOITEM_S.JOBDEPID'
    end
    object taWOrderJOBNAME: TStringField
      DisplayLabel = #1055#1086#1083#1091#1095#1080#1083
      FieldKind = fkLookup
      FieldName = 'JOBNAME'
      LookupDataSet = taMol
      LookupKeyFields = 'MOLID'
      LookupResultField = 'FIO'
      KeyFields = 'JOBID'
      LookupCache = True
      Size = 60
      Lookup = True
    end
    object taWOrderJOBDEPNAME: TStringField
      DisplayLabel = #1052#1061'2'
      FieldKind = fkLookup
      FieldName = 'JOBDEPNAME'
      LookupDataSet = dm.taDepart
      LookupKeyFields = 'DEPID'
      LookupResultField = 'NAME'
      KeyFields = 'JOBDEPID'
      LookupCache = True
      Size = 40
      Lookup = True
    end
    object taWOrderSEMISNAME: TStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldKind = fkLookup
      FieldName = 'SEMISNAME'
      LookupDataSet = dm.taSemis
      LookupKeyFields = 'SEMISID'
      LookupResultField = 'SEMIS'
      KeyFields = 'SEMIS'
      LookupCache = True
      Size = 40
      Lookup = True
    end
    object taWOrderAINV: TIntegerField
      FieldName = 'AINV'
      Origin = 'WOITEM_S.AINV'
    end
    object taWOrderREF: TIntegerField
      FieldName = 'REF'
      Origin = 'WOITEM_S.REF'
    end
    object taWOrderART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'WOITEM_S.ART2ID'
    end
    object taWOrderSEMISNAME1: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME1'
      Origin = 'WOITEM_S.SEMISNAME1'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWOrderAPPLINV: TIntegerField
      FieldName = 'APPLINV'
      Origin = 'WOITEM_S.APPLINV'
    end
    object taWOrderFROMOPERID: TFIBStringField
      DisplayLabel = #1057' '#1086#1087#1077#1088#1072#1094#1080#1080
      FieldName = 'FROMOPERID'
      Origin = 'WOITEM_S.FROMOPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWOrderFROMOPERNAME: TStringField
      DisplayLabel = #1057' '#1086#1087#1077#1088#1072#1094#1080#1080
      FieldKind = fkLookup
      FieldName = 'FROMOPERNAME'
      LookupDataSet = dm.taOper
      LookupKeyFields = 'OPERID'
      LookupResultField = 'OPERATION'
      KeyFields = 'FROMOPERID'
      LookupCache = True
      Size = 60
      Lookup = True
    end
    object taWOrderADOCNO: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1080#1103
      FieldName = 'ADOCNO'
      Origin = 'WOITEM_S.ADOCNO'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWOrderREJREF: TIntegerField
      FieldName = 'REJREF'
      Origin = 'WOITEM_S.REJREF'
    end
    object taWOrderREJPSID: TIntegerField
      FieldName = 'REJPSID'
      Origin = 'WOITEM_S.REJPSID'
    end
    object taWOrderREJPSNAME: TFIBStringField
      DisplayLabel = #1042#1080#1085#1086#1074#1085#1080#1082' '#1073#1088#1072#1082#1072
      FieldName = 'REJPSNAME'
      Origin = 'WOITEM_S.REJPSNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWOrderFULLART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'FULLART'
      Origin = 'WOITEM_S.FULLART'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWOrderART: TFIBStringField
      FieldName = 'ART'
      Origin = 'WOITEM_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taWOrderART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'WOITEM_S.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taWOrderUQ: TSmallintField
      FieldName = 'UQ'
      Origin = 'WOITEM_S.UQ'
    end
    object taWOrderUW: TSmallintField
      FieldName = 'UW'
      Origin = 'WOITEM_S.UW'
    end
    object taWOrderREJID: TIntegerField
      FieldName = 'REJID'
      Origin = 'WOITEM_S.REJID'
    end
    object taWOrderREJNAME: TStringField
      DisplayLabel = #1055#1088#1080#1095#1080#1085#1072' '#1073#1088#1072#1082#1072
      FieldKind = fkLookup
      FieldName = 'REJNAME'
      LookupDataSet = dm.taRej
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'REJID'
      LookupCache = True
      Size = 60
      Lookup = True
    end
    object taWOrderPROTOCOLID: TIntegerField
      FieldName = 'PROTOCOLID'
      Origin = 'WOITEM_S.PROTOCOLID'
    end
  end
  object dsrWOrder: TDataSource
    DataSet = taWOrder
    Left = 92
    Top = 52
  end
  object quTmp: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    Left = 588
    Top = 52
  end
  object quBuffer: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'select InvId'
      'from CopyIt(:WOITEMID, :WORDERID, :INVID, :ITTYPE)')
    Left = 636
    Top = 4
  end
  object quWOrder_T: TpFIBDataSet
    SelectSQL.Strings = (
      'select GET, DONE, RET,  REJ, REWORK,  NK, F, STONEQ, STONEW,'
      '    BRW, BRQ'
      'from WOItem_T(:WORDERID, :BD, :ED, :Material)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = quWOrder_TBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 96
    Top = 108
    object quWOrder_TGET: TFloatField
      FieldName = 'GET'
      Origin = 'WOITEM_T.GET'
      DisplayFormat = '0.###'
    end
    object quWOrder_TDONE: TFloatField
      FieldName = 'DONE'
      Origin = 'WOITEM_T.DONE'
      DisplayFormat = '0.###'
    end
    object quWOrder_TRET: TFloatField
      FieldName = 'RET'
      Origin = 'WOITEM_T.RET'
      DisplayFormat = '0.###'
    end
    object quWOrder_TREJ: TFloatField
      FieldName = 'REJ'
      Origin = 'WOITEM_T.REJ'
      DisplayFormat = '0.###'
    end
    object quWOrder_TREWORK: TFloatField
      FieldName = 'REWORK'
      Origin = 'WOITEM_T.REWORK'
      DisplayFormat = '0.###'
    end
    object quWOrder_TF: TFloatField
      FieldName = 'F'
      Origin = 'WOITEM_T.F'
      DisplayFormat = '0.000'
    end
    object quWOrder_TSTONEQ: TFloatField
      FieldName = 'STONEQ'
      Origin = 'WOITEM_T.STONEQ'
    end
    object quWOrder_TSTONEW: TFloatField
      FieldName = 'STONEW'
      Origin = 'WOITEM_T.STONEW'
      DisplayFormat = '0.###'
    end
    object quWOrder_TBRW: TFloatField
      FieldName = 'BRW'
      Origin = 'WOITEM_T.BRW'
      DisplayFormat = '0.###'
    end
    object quWOrder_TBRQ: TFloatField
      FieldName = 'BRQ'
      Origin = 'WOITEM_T.BRQ'
    end
    object quWOrder_TNK: TFloatField
      FieldName = 'NK'
      Origin = 'WOITEM_T.NK'
    end
  end
  object dsrWOrder_T: TDataSource
    DataSet = quWOrder_T
    Left = 96
    Top = 152
  end
  object taPList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Act set'
      '  DOCNO=:DOCNO,'
      '  DOCDATE=:DOCDATE,'
      '  BD=:BD,'
      '  ISCLOSE=:ISCLOSE  '
      'where ID=:OLD_ID')
    DeleteSQL.Strings = (
      'delete from Act where Id=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into Act(ID, DOCNO, DOCDATE, USERID, BD, ISCLOSE, T, MATI' +
        'D, A, UPD)'
      
        'values(:ID, :DOCNO, :DOCDATE, :USERID, :BD, :ISCLOSE, :T, :MATID' +
        ', :A, :UPD)')
    RefreshSQL.Strings = (
      'select a.ID, a.DOCNO, a.DOCDATE, a.USERID, a.BD, a.ISCLOSE, '
      '      a.T, a.MATID, a.A, m.NAME, a.UPD'
      'from Act a, D_Mat m'
      'where a.ID=:OLD_ID and'
      '          a.MatId=m.Id ')
    SelectSQL.Strings = (
      
        'select  ID, DOCNO, DOCDATE, USERID, BD, ISCLOSE, T, MATID, A, UP' +
        'D'
      'from Act '
      'where DOCDATE between :BD and :ED and'
      '      MATID between :MATID1 and :MATID2 and'
      '      T in (0,1) and'
      '      SELFCOMPID=:SELFCOMPID       '
      'order by DOCNO')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taPListBeforeDelete
    BeforeOpen = taPListBeforeOpen
    OnCalcFields = taPListCalcFields
    OnNewRecord = taPListNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 16
    Top = 200
    object taPListRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taPListID: TIntegerField
      FieldName = 'ID'
      Origin = 'ACT.ID'
      Required = True
    end
    object taPListDOCNO: TIntegerField
      DisplayLabel = #8470' '#1072#1082#1090#1072
      FieldName = 'DOCNO'
      Origin = 'ACT.DOCNO'
    end
    object taPListDOCDATE: TDateTimeField
      DisplayLabel = #1055#1086
      FieldName = 'DOCDATE'
      Origin = 'ACT.DOCDATE'
      DisplayFormat = 'DD.MM.YYYY'
    end
    object taPListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'ACT.USERID'
    end
    object taPListUSERNAME: TStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldKind = fkLookup
      FieldName = 'USERNAME'
      LookupDataSet = taMol
      LookupKeyFields = 'MOLID'
      LookupResultField = 'FIO'
      KeyFields = 'USERID'
      Size = 60
      Lookup = True
    end
    object taPListBD: TDateTimeField
      DisplayLabel = #1057
      FieldName = 'BD'
      Origin = 'ACT.BD'
      DisplayFormat = 'DD.MM.YYYY'
    end
    object taPListED: TDateField
      FieldKind = fkCalculated
      FieldName = 'ED'
      DisplayFormat = 'DD.MM.YYYY'
      Calculated = True
    end
    object taPListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'ACT.ISCLOSE'
      Required = True
    end
    object taPListT: TSmallintField
      FieldName = 'T'
      Origin = 'ACT.T'
    end
    object taPListMATNAME: TStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldKind = fkLookup
      FieldName = 'MATNAME'
      LookupDataSet = dm.taMat
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'MATID'
      Size = 40
      Lookup = True
    end
    object taPListMATID: TFIBStringField
      FieldName = 'MATID'
      Origin = 'ACT.MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taPListA: TSmallintField
      FieldName = 'A'
      Origin = 'ACT.A'
    end
    object taPListUPD: TSmallintField
      DisplayLabel = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
      FieldName = 'UPD'
      Origin = 'ACT.UPD'
      OnGetText = taPListUPDGetText
    end
  end
  object taProtocol: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AItem set'
      '  F=:F, '
      '  N=:N, '
      '  E=:E, '
      '  S=:S, '
      '  I=:I, '
      '  H=:H, '
      '  WZ=:WZ,  '
      '  OUT=:OUT,'
      '  OUT_PS=:OUT_PS'
      'where ID=:OLD_Id '
      '  ')
    DeleteSQL.Strings = (
      'delete from AItem where Id=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into AItem(ID, PSID, OPERID, ACTID, F, N, E, S, I, H, WZ,' +
        ' OUT, OUT_PS)'
      
        'values (:ID, :PSID, :OPERID, :ACTID, :F, :N, :E, :S, :I, :H, :WZ' +
        ', :OUT, :OUT_PS)')
    RefreshSQL.Strings = (
      
        'select a.ID, a.ACTID, a.PSID, a.OPERID, a.F, a.N, a.E, a.S, a.I,' +
        ' a.H, '
      '   a.WZ, a.OUT, d.NAME PSNAME, a.OUT_PS'
      'from AItem a, D_Dep d '
      'where a.Id=:Id and'
      '           a.PsId=d.DepId    '
      '          ')
    SelectSQL.Strings = (
      
        'select ID, ACTID, PSID, OPERID, F, N, E, S, I, H, WZ, OUT, PSNAM' +
        'E,'
      '   OUT_PS'
      'from AItem_S(:ACTID)')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeEdit = taProtocolBeforeEdit
    BeforeOpen = taProtocolBeforeOpen
    OnCalcFields = taProtocolCalcFields
    OnNewRecord = taProtocolNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 76
    Top = 200
    object taProtocolRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taProtocolID: TIntegerField
      FieldName = 'ID'
      Origin = 'AITEM.ID'
      Required = True
    end
    object taProtocolACTID: TIntegerField
      FieldName = 'ACTID'
      Origin = 'AITEM.ACTID'
    end
    object taProtocolF: TFloatField
      DisplayLabel = #1060#1072#1082#1090'.'
      FieldName = 'F'
      Origin = 'AITEM.F'
      DisplayFormat = '0.###'
    end
    object taProtocolN: TFloatField
      DisplayLabel = #1053#1086#1088#1084#1072
      FieldName = 'N'
      Origin = 'AITEM.N'
      DisplayFormat = '0.###'
    end
    object taProtocolE: TFloatField
      DisplayLabel = #1069#1082#1086#1085#1086#1084#1080#1103
      FieldName = 'E'
      Origin = 'AITEM.E'
      DisplayFormat = '0.###'
    end
    object taProtocolS: TFloatField
      DisplayLabel = #1057#1074#1077#1088#1093#1085#1086#1088#1084'. '#1087#1086#1090#1077#1088#1080
      FieldName = 'S'
      Origin = 'AITEM.S'
      DisplayFormat = '0.###'
    end
    object taProtocolI: TFloatField
      DisplayLabel = #1048#1079#1083#1080#1096#1082#1080
      FieldName = 'I'
      Origin = 'AITEM.I'
      DisplayFormat = '0.###'
    end
    object taProtocolH: TFloatField
      DisplayLabel = #1053#1077#1076#1086#1089#1090#1072#1095#1072
      FieldName = 'H'
      Origin = 'AITEM.H'
      DisplayFormat = '0.###'
    end
    object taProtocolWZ: TFloatField
      DisplayLabel = #1042#1077#1089' ('#1079'/'#1087')'
      FieldName = 'WZ'
      Origin = 'AITEM.WZ'
      DisplayFormat = '0.###'
    end
    object taProtocolOUT: TFloatField
      DisplayLabel = #1048#1089#1093#1086#1076'.'
      FieldName = 'OUT'
      Origin = 'AITEM.OUT'
      DisplayFormat = '0.###'
    end
    object taProtocolOUT1: TFloatField
      DisplayLabel = #1048#1089#1093'.'
      FieldKind = fkCalculated
      FieldName = 'OUT1'
      DisplayFormat = '0.###'
      Calculated = True
    end
    object taProtocolPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'AITEM_S.PSID'
    end
    object taProtocolOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AITEM_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taProtocolPSNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'PSNAME'
      Origin = 'AITEM_S.PSNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taProtocolOPERNAME: TStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldKind = fkLookup
      FieldName = 'OPERNAME'
      LookupDataSet = dm.taOper
      LookupKeyFields = 'OPERID'
      LookupResultField = 'OPERATION'
      KeyFields = 'OPERID'
      Size = 40
      Lookup = True
    end
    object taProtocolOUT_PS1: TFloatField
      DisplayLabel = #1048#1090#1086#1075#1086' '#1080#1089#1093'.'
      FieldKind = fkCalculated
      FieldName = 'OUT_PS1'
      DisplayFormat = '0.###'
      Calculated = True
    end
    object taProtocolOUT_PS: TFloatField
      DisplayLabel = #1048#1090#1086#1075#1086' '#1080#1089#1093'.'
      FieldName = 'OUT_PS'
      Origin = 'AITEM_S.OUT_PS'
      DisplayFormat = '0.###'
    end
  end
  object dsrPList: TDataSource
    DataSet = taPList
    Left = 16
    Top = 244
  end
  object dsrProtocol: TDataSource
    DataSet = taProtocol
    Left = 76
    Top = 245
  end
  object taZList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Zp set'
      '  DOCNO=:DOCNO, '
      '  DOCDATE=:DOCDATE,'
      '  BD=:BD'
      'where id=:OLD_Id')
    DeleteSQL.Strings = (
      'delete from Zp where id=:OLD_Id')
    InsertSQL.Strings = (
      'insert into Zp(ID, DOCNO, DOCDATE, USERID, BD)'
      'values (:ID, :DOCNO, :DOCDATE, :USERID, :BD)')
    RefreshSQL.Strings = (
      'select ID, DOCNO, DOCDATE, USERID, BD '
      'from Zp'
      'where Id=:Id')
    SelectSQL.Strings = (
      'select ID, DOCNO, DOCDATE, USERID, BD '
      'from Zp'
      'where DocDate between :BD and :ED and'
      '      SELFCOMPID=:SELFCOMPID  '
      'order by DocDate')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taZListBeforeDelete
    BeforeOpen = taZListBeforeOpen
    OnCalcFields = taZListCalcFields
    OnNewRecord = taZListNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 16
    Top = 304
    object taZListRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taZListID: TIntegerField
      FieldName = 'ID'
      Origin = 'ZP.ID'
      Required = True
    end
    object taZListDOCNO: TIntegerField
      DisplayLabel = #8470' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      FieldName = 'DOCNO'
      Origin = 'ZP.DOCNO'
    end
    object taZListDOCDATE: TDateTimeField
      DisplayLabel = #1055#1086
      FieldName = 'DOCDATE'
      Origin = 'ZP.DOCDATE'
      DisplayFormat = 'DD.MM.YYYY'
    end
    object taZListUSERID: TIntegerField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'USERID'
      Origin = 'ZP.USERID'
    end
    object taZListBD: TDateTimeField
      DisplayLabel = 'C'
      FieldName = 'BD'
      Origin = 'ZP.BD'
      DisplayFormat = 'DD.MM.YYYY'
    end
    object taZListUSERNAME: TStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldKind = fkLookup
      FieldName = 'USERNAME'
      LookupDataSet = taMol
      LookupKeyFields = 'MOLID'
      LookupResultField = 'FIO'
      KeyFields = 'USERID'
      Size = 60
      Lookup = True
    end
    object taZListED: TDateField
      FieldKind = fkCalculated
      FieldName = 'ED'
      Calculated = True
    end
  end
  object dsrZList: TDataSource
    DataSet = taZList
    Left = 16
    Top = 352
  end
  object taZItem: TpFIBDataSet
    UpdateSQL.Strings = (
      'update ZITem set'
      '  U=:U, '
      '  NP=:NP, '
      '  NR=:NR,'
      '  DOC=:DOC,'
      '  NR2=:NR2,'
      '  OPER=:OPER,'
      '  PSID=:PSID'
      'where ID=:OLD_ID')
    DeleteSQL.Strings = (
      'delete from ZItem where Id=:OLD_Id')
    InsertSQL.Strings = (
      'insert into ZItem(ID, OPER, ZP, U,  NP, NR, DONE, E,  S,ZPID, '
      '   PSID, NR2, DOC)'
      'values (:ID, :OPER, :ZP, :U,  :NP, :NR, :DONE, :E,  :S, :ZPID, '
      '   :PSID, :NR2, :DOC)')
    SelectSQL.Strings = (
      
        'select z.ID, z.OPER, z.ZP, z.U, z.NP, z.NR, z.DONE, z.E, z.S, z.' +
        'ZPID,           z.PSID, d.Name, z.DOC, z.NR2, z.EP, z.ARTK, z.RA' +
        'TE, z.ZT,   z.ZPDONE'
      'from ZItem z, D_Dep d'
      'where d.DEPID=z.PSID and'
      '           z.ZPID=:ZPID'
      'order by d.NAME, z.OPER')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeOpen = taZItemBeforeOpen
    OnCalcFields = taZItemCalcFields
    OnNewRecord = taZItemNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 64
    Top = 304
    object taZItemID: TIntegerField
      FieldName = 'ID'
      Origin = 'ZITEM.ID'
      Required = True
    end
    object taZItemOPER: TFIBStringField
      FieldName = 'OPER'
      Origin = 'ZITEM.OPER'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taZItemZP: TFloatField
      DisplayLabel = #1047#1072#1088#1087#1083#1072#1090#1072
      FieldName = 'ZP'
      Origin = 'ZITEM.ZP'
      currency = True
    end
    object taZItemU: TFloatField
      DisplayLabel = #1059#1076#1077#1088#1078#1072#1090#1100', '#1088#1091#1073
      FieldName = 'U'
      Origin = 'ZITEM.U'
    end
    object taZItemNP: TFloatField
      DisplayLabel = #1053#1072#1076#1073#1072#1074#1080#1090#1100', '#1088#1091#1073
      FieldName = 'NP'
      Origin = 'ZITEM.NP'
    end
    object taZItemNR: TFloatField
      DisplayLabel = #1053#1072#1076#1073#1072#1074#1080#1090#1100', %'
      FieldName = 'NR'
      Origin = 'ZITEM.NR'
    end
    object taZItemNR2: TFloatField
      DisplayLabel = #1053#1072#1076#1073#1072#1074#1080#1090#1100'2, '#1088#1091#1073
      FieldName = 'NR2'
      Origin = 'ZITEM.NR2'
      currency = True
    end
    object taZItemDONE: TFloatField
      DisplayLabel = #1048#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1086
      FieldName = 'DONE'
      Origin = 'ZITEM.DONE'
      DisplayFormat = '0.###'
    end
    object taZItemE: TFloatField
      DisplayLabel = #1069#1082#1086#1085#1086#1084#1080#1103
      FieldName = 'E'
      Origin = 'ZITEM.E'
      DisplayFormat = '0.###'
    end
    object taZItemS: TFloatField
      DisplayLabel = #1057#1074#1077#1088#1093#1085#1086#1088#1084'. '#1087#1086#1090#1077#1088#1080
      FieldName = 'S'
      Origin = 'ZITEM.S'
      DisplayFormat = '0.###'
    end
    object taZItemZPID: TIntegerField
      FieldName = 'ZPID'
      Origin = 'ZITEM.ZPID'
    end
    object taZItemOPERNAME: TStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldKind = fkLookup
      FieldName = 'OPERNAME'
      LookupDataSet = dm.taOper
      LookupKeyFields = 'OPERID'
      LookupResultField = 'OPERATION'
      KeyFields = 'OPER'
      Size = 60
      Lookup = True
    end
    object taZItemTZP: TFloatField
      DisplayLabel = #1048#1090#1086#1075#1086
      FieldKind = fkCalculated
      FieldName = 'TZP'
      currency = True
      Calculated = True
    end
    object taZItemPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'ZITEM.PSID'
      Required = True
    end
    object taZItemNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'NAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taZItemPSNAME: TStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldKind = fkLookup
      FieldName = 'PSNAME'
      LookupDataSet = taPsDep
      LookupKeyFields = 'DEPID'
      LookupResultField = 'NAME'
      KeyFields = 'PSID'
      Size = 40
      Lookup = True
    end
    object taZItemDOC: TSmallintField
      DisplayLabel = #1054#1090#1095#1077#1090
      FieldName = 'DOC'
      Origin = 'ZITEM.DOC'
      Required = True
    end
    object taZItemEP: TFloatField
      DisplayLabel = #1069#1082#1086#1085#1086#1084#1080#1103' %'
      FieldName = 'EP'
      Origin = 'ZITEM.EP'
    end
    object taZItemEP1: TFloatField
      DisplayLabel = #1069#1082#1086#1085#1086#1084#1080#1103' %'
      FieldKind = fkCalculated
      FieldName = 'EP1'
      DisplayFormat = '0.00'
      Calculated = True
    end
    object taZItemRATE: TFloatField
      DisplayLabel = #1041#1072#1079#1086#1074#1072#1103' '#1089#1090#1072#1074#1082#1072
      FieldName = 'RATE'
      Origin = 'D_OPER.RATE'
      currency = True
    end
    object taZItemZT: TSmallintField
      FieldName = 'ZT'
      Origin = 'D_OPER.ZT'
    end
    object taZItemARTK: TFloatField
      DisplayLabel = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090', %'
      FieldName = 'ARTK'
      Origin = 'ZITEM.ARTK'
      DisplayFormat = '0.00%'
    end
    object taZItemZPDONE: TFloatField
      DisplayLabel = #1057#1076#1077#1083#1072#1085#1086
      FieldName = 'ZPDONE'
      Origin = 'ZITEM.ZPDONE'
      ReadOnly = True
      Required = True
      DisplayFormat = '0.###'
    end
  end
  object dsrZItem: TDataSource
    DataSet = taZItem
    Left = 64
    Top = 352
  end
  object taMol: TpFIBDataSet
    SelectSQL.Strings = (
      'select MOLID, FNAME, LNAME, MNAME, FIO'
      'from D_MOL'
      'order by SortInd')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 760
    Top = 4
  end
  object taMolIO: TpFIBDataSet
    SelectSQL.Strings = (
      'select m.MOLID, m.FNAME, m.LNAME, m.MNAME, m.FIO'
      'from D_MOL m, D_PS p, D_DEP d'
      'where p.MOLID=m.MOLID and'
      '           p.DEPID=d.DEPID and'
      '           band(d.DEPTYPES, 2)=2     '
      ''
      'union'
      'select m.MOLID, m.FNAME, m.LNAME, m.MNAME, m.FIO'
      'from D_MOL m'
      'where m.MOLID=:MOLID and'
      '      m.ADM=1')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taMolIOBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 760
    Top = 48
  end
  object taPSMol: TpFIBDataSet
    SelectSQL.Strings = (
      'select ps.MolId, m.FIO,  ps.DepId '
      'from D_Ps ps, D_Mol m'
      'where ps.DepId=:DEPID  and'
      '           ps.MolId=m.MolId')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taPSMolBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 252
    Top = 8
    object taPSMolMOLID: TIntegerField
      FieldName = 'MOLID'
      Origin = 'D_PS.MOLID'
      Required = True
    end
    object taPSMolFIO: TFIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'FIO'
      Origin = 'D_MOL.FIO'
      ReadOnly = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taPSMolDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'D_PS.DEPID'
      Required = True
    end
  end
  object taPsDep: TpFIBDataSet
    SelectSQL.Strings = (
      'select DEPID, PARENT, CHILD, COLOR, SNAME, NAME,'
      '    SORTIND'
      'from D_Dep'
      'where band(DEPTYPES, 1)=1 and'
      '           band(DEPTYPES, 16)<>16 and'
      
        '           (select count(*) from D_Ps p where  p.DEPID=DEPID) > ' +
        '0'
      'order by SORTIND')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 200
    Top = 8
    object taPsDepDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'D_DEP.DEPID'
      Required = True
    end
    object taPsDepPARENT: TIntegerField
      FieldName = 'PARENT'
      Origin = 'D_DEP.PARENT'
    end
    object taPsDepCHILD: TIntegerField
      FieldName = 'CHILD'
      Origin = 'D_DEP.CHILD'
    end
    object taPsDepSNAME: TFIBStringField
      FieldName = 'SNAME'
      Origin = 'D_DEP.SNAME'
      FixedChar = True
      Size = 30
      EmptyStrToNull = True
    end
    object taPsDepCOLOR: TIntegerField
      FieldName = 'COLOR'
      Origin = 'D_DEP.COLOR'
    end
    object taPsDepNAME: TFIBStringField
      FieldName = 'NAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taPsDepSORTIND: TIntegerField
      FieldName = 'SORTIND'
      Origin = 'D_DEP.SORTIND'
    end
  end
  object dsrPsDep: TDataSource
    DataSet = taPsDep
    Left = 200
    Top = 52
  end
  object dsrPsMol: TDataSource
    DataSet = taPSMol
    Left = 252
    Top = 52
  end
  object taStoneSemis: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select s.SemisId, s.Semis, s.SName, s.UQ, s.UW, s.Q0, s.MAT, s.P' +
        'RILL, s.GOOD,'
      '  s.EDGSHAPEID, s.EDGTID, s.CLEANNES, s.CHROMATICITY, s.IGR,'
      '  s.COLOR, s.SZ, s.O1'
      'from D_Semis s, D_Mat m'
      'where m.Id=s.Mat and'
      '          m.IsIns=1           '
      'order by s.SORTIND')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 816
    Top = 4
  end
  object dsStoneSemis: TDataSource
    DataSet = taStoneSemis
    Left = 816
    Top = 48
  end
  object taErrWOrder: TpFIBDataSet
    SelectSQL.Strings = (
      'select INV_ID, INV_DOCNO,  INV_DATE, INV_WORDERID,'
      '      INV_WORDERDOCNO, INV_WORDERDOCDATE,'
      '      IT_ID,  IT_ITDATE, IT_WORDERID,  IT_WORDERDOCNO,'
      '      IT_WORDERDOCDATE, IT_SEMIS, IT_W'
      'from err_worder_id')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 548
    Top = 416
    object taErrWOrderINV_ID: TIntegerField
      FieldName = 'INV_ID'
      Origin = 'ERR_WORDER_ID.INV_ID'
      Required = True
    end
    object taErrWOrderINV_DOCNO: TIntegerField
      FieldName = 'INV_DOCNO'
      Origin = 'ERR_WORDER_ID.INV_DOCNO'
    end
    object taErrWOrderINV_DATE: TDateTimeField
      FieldName = 'INV_DATE'
      Origin = 'ERR_WORDER_ID.INV_DATE'
    end
    object taErrWOrderINV_WORDERID: TIntegerField
      FieldName = 'INV_WORDERID'
      Origin = 'ERR_WORDER_ID.INV_WORDERID'
      Required = True
    end
    object taErrWOrderINV_WORDERDOCNO: TIntegerField
      FieldName = 'INV_WORDERDOCNO'
      Origin = 'ERR_WORDER_ID.INV_WORDERDOCNO'
    end
    object taErrWOrderINV_WORDERDOCDATE: TDateTimeField
      FieldName = 'INV_WORDERDOCDATE'
      Origin = 'ERR_WORDER_ID.INV_WORDERDOCDATE'
    end
    object taErrWOrderIT_ID: TIntegerField
      FieldName = 'IT_ID'
      Origin = 'ERR_WORDER_ID.IT_ID'
      Required = True
    end
    object taErrWOrderIT_ITDATE: TDateTimeField
      FieldName = 'IT_ITDATE'
      Origin = 'ERR_WORDER_ID.IT_ITDATE'
    end
    object taErrWOrderIT_WORDERID: TIntegerField
      FieldName = 'IT_WORDERID'
      Origin = 'ERR_WORDER_ID.IT_WORDERID'
      Required = True
    end
    object taErrWOrderIT_WORDERDOCNO: TIntegerField
      FieldName = 'IT_WORDERDOCNO'
      Origin = 'ERR_WORDER_ID.IT_WORDERDOCNO'
    end
    object taErrWOrderIT_WORDERDOCDATE: TDateTimeField
      FieldName = 'IT_WORDERDOCDATE'
      Origin = 'ERR_WORDER_ID.IT_WORDERDOCDATE'
    end
    object taErrWOrderIT_SEMIS: TFIBStringField
      FieldName = 'IT_SEMIS'
      Origin = 'ERR_WORDER_ID.IT_SEMIS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taErrWOrderIT_W: TFloatField
      FieldName = 'IT_W'
      Origin = 'ERR_WORDER_ID.IT_W'
    end
  end
  object dsrErrWOrder: TDataSource
    DataSet = taErrWOrder
    Left = 548
    Top = 464
  end
  object taPsOper: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  po.ID,'
      '  po.OPERID, '
      '  po.DEPID, '
      '  o.OPERATION'
      'from '
      '  D_PsOper po'
      '  left outer join D_Oper o on (po.OperId=o.OperId)'
      'where '
      '  po.DepId = :DEPID'
      'order by '
      '  o.OperId')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeOpen = taPsOperBeforeOpen
    OnCalcFields = CalcRecNo
    OnNewRecord = taPsOperNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 147
    Top = 8
    object taPsOperID: TIntegerField
      FieldName = 'ID'
      Origin = 'D_PSOPER.ID'
      Required = True
    end
    object taPsOperOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'D_PSOPER.OPERID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taPsOperDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'D_PSOPER.DEPID'
      Required = True
    end
    object taPsOperOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Origin = 'D_OPER.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsrPsOper: TDataSource
    DataSet = taPsOper
    Left = 148
    Top = 52
  end
  object dsrSIList: TDataSource
    DataSet = taSIList
    Left = 364
    Top = 48
  end
  object taSIList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Inv set'
      '  DOCNO=:DOCNO, '
      '  DOCDATE=:DOCDATE, '
      '  USERID=:USERID,'
      '  SUPID=:SUPID, '
      '  SSF=:SSF, '
      '  SSFDT=:SSFDT,'
      '  ISCLOSE=:ISCLOSE,'
      '  NDSID=:NDSID,'
      '  REF = :REF,'
      '  CONTRACTID=:CONTRACTID,'
      '  UE=:UE,'
      '  INVSUBTYPE=:INVSUBTYPEID,'
      '  COLORID=:INVCOLORID,'
      '  Reference$ID = :Reference$ID'
      'where InvId=:OLD_InvId')
    DeleteSQL.Strings = (
      'delete from Inv where InvId=:OLD_InvId')
    InsertSQL.Strings = (
      
        'insert into Inv(INVID, DOCNO, DOCDATE, ITYPE, DEPID, SUPID, USER' +
        'ID, '
      
        '     ISCLOSE, NDSID, SSF, SSFDT, CONTRACTID, UE, INVSUBTYPE, COL' +
        'ORID)'
      
        'values (:INVID, :DOCNO, :DOCDATE, :ITYPE, :DEPID, :SUPID,:USERID' +
        ','
      
        '      0, :NDSID, :SSF, :SSFDT, :CONTRACTID, :UE, :INVSUBTYPEID, ' +
        ':INVCOLORID)')
    RefreshSQL.Strings = (
      'select INVID, DOCNO, DOCDATE , ITYPE, DEPID, USERID,'
      '    SUPID,  FIO, SUPNAME,  ISCLOSE, DEPNAME,  NDSID, REF,'
      
        '    Q, SSF, SSFDT, W, COST, CONTRACTID, CONTRACTNAME, UE, SCHEMA' +
        'ID,'
      
        '    INVSUBTYPEID, INVSUBTYPENAME, INVCOLORID, COLOR, W3, Created' +
        ', Reference$ID'
      'from SList_R2(:INVID)')
    SelectSQL.Strings = (
      'select INVID, DOCNO, DOCDATE , ITYPE, DEPID, USERID,'
      '    SUPID,  FIO, SUPNAME,  ISCLOSE, DEPNAME,  NDSID, REF,'
      '    Q, SSF, SSFDT, W, COST, CONTRACTID, CONTRACTNAME,'
      
        '    UE, SCHEMAID, INVSUBTYPEID, INVSUBTYPENAME, INVCOLORID, COLO' +
        'R, W3,'
      '    SelfCompName, LinesCount, created, reference$id'
      'from SList_S(:DepId1, :DepId2, :BD, :ED,:ITYPE_)'
      
        'where (SUPNAME between :SUPNAME1 and :SUPNAME2) or (SUPNAME is n' +
        'ull)'
      'order by DocDate')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taSIListBeforeDelete
    BeforeOpen = taSIListBeforeOpen
    OnDeleteError = taSIListDeleteError
    OnNewRecord = taSIListNewRecord
    OnPostError = taSIListPostError
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 364
    Top = 4
    object taSIListINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SLIST_S.INVID'
    end
    object taSIListDOCNO: TIntegerField
      DisplayLabel = #8470' '#1085#1072#1082#1083'.'
      FieldName = 'DOCNO'
      Origin = 'SLIST_S.DOCNO'
    end
    object taSIListDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
      FieldName = 'DOCDATE'
      Origin = 'SLIST_S.DOCDATE'
      OnChange = taSIListDOCDATEChange
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taSIListITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'SLIST_S.ITYPE'
    end
    object taSIListDEPID: TIntegerField
      DisplayWidth = 10
      FieldName = 'DEPID'
      Origin = 'SLIST_S.DEPID'
    end
    object taSIListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'SLIST_S.USERID'
    end
    object taSIListSUPID: TIntegerField
      FieldName = 'SUPID'
      Origin = 'SLIST_S.SUPID'
    end
    object taSIListSUPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      FieldName = 'SUPNAME'
      Origin = 'SLIST_S.SUPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSIListFIO: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldKind = fkLookup
      FieldName = 'FIO'
      LookupDataSet = taMol
      LookupKeyFields = 'MOLID'
      LookupResultField = 'FIO'
      KeyFields = 'USERID'
      Origin = 'SLIST_S.FIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
      Lookup = True
    end
    object taSIListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'SLIST_S.ISCLOSE'
    end
    object taSIListDEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'DEPNAME'
      Origin = 'SLIST_S.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSIListNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SLIST_S.NDSID'
    end
    object taSIListREF: TIntegerField
      FieldName = 'REF'
      Origin = 'SLIST_S.REF'
    end
    object taSIListQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'SLIST_S.Q'
    end
    object taSIListSSF: TFIBStringField
      DisplayLabel = #8470' '#1089'.'#1092'.'
      FieldName = 'SSF'
      Origin = 'SLIST_S.SSF'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSIListSSFDT: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1089'.'#1092'.'
      FieldName = 'SSFDT'
      Origin = 'SLIST_S.SSFDT'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taSIListW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SLIST_S.W'
      DisplayFormat = '0.###'
    end
    object taSIListCOST: TFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'COST'
      Origin = 'SLIST_S.COST'
      currency = True
    end
    object taSIListCONTRACTID: TFIBIntegerField
      FieldName = 'CONTRACTID'
    end
    object taSIListCONTRACTNAME: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1076#1086#1075#1086#1074#1086#1088#1072
      FieldName = 'CONTRACTNAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taSIListUE: TFIBFloatField
      FieldName = 'UE'
    end
    object taSIListSCHEMAID: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1091#1095#1077#1090#1072
      FieldName = 'SCHEMAID'
    end
    object taSIListINVSUBTYPEID: TFIBIntegerField
      FieldName = 'INVSUBTYPEID'
    end
    object taSIListINVSUBTYPENAME: TFIBStringField
      FieldName = 'INVSUBTYPENAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taSIListINVCOLORID: TFIBIntegerField
      FieldName = 'INVCOLORID'
    end
    object taSIListCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taSIListW3: TFIBFloatField
      FieldName = 'W3'
      DisplayFormat = '0.###'
    end
    object taSIListSELFCOMPNAME: TFIBStringField
      FieldName = 'SELFCOMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taSIListLINESCOUNT: TFIBIntegerField
      FieldName = 'LINESCOUNT'
    end
    object taSIListCREATED: TFIBDateTimeField
      FieldName = 'CREATED'
      DisplayFormat = 'DD/MM/YYYY'
    end
    object taSIListREFERENCEID: TFIBIntegerField
      FieldName = 'REFERENCE$ID'
    end
  end
  object taSIEl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SIEl set'
      '  SEMIS=:SEMIS,'
      '  Q=:Q,'
      '  W=:W,'
      '  NDS=:NDS,'
      '  NDSID=:NDSID,'
      '  TPRICE=:TPRICE,'
      '  COST=:COST,'
      '  OPERID=:OPERID,'
      '  FOR_Q_U=:FOR_Q_U,'
      '  UEPRICE=:UEPRICE'
      'where Id=:OLD_Id')
    DeleteSQL.Strings = (
      'delete from SIEl where ID=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into SIEl(ID, INVID, SEMIS, Q,  W, TPRICE, PRICE, NDSID, ' +
        'NDS, T, COST, OPERID, FOR_Q_U, UEPRICE)'
      
        'values (:ID, :INVID, :SEMIS, :Q, :W, :TPRICE, :PRICE, :NDSID, :N' +
        'DS, 0, :COST, :OPERID,:FOR_Q_U, :UEPRICE)')
    RefreshSQL.Strings = (
      'select '
      '  se.ID, '
      '  se.INVID, '
      '  se.SEMIS,  '
      '  se.Q, '
      '  se.W, '
      '  se.TPRICE, '
      '  se.PRICE,'
      '  se.NDSID, '
      '  se.NDS, '
      '  se.T, '
      '  se.COST, '
      '  se.OPERID, '
      '  se.FOR_Q_U,'
      '  se.UEPRICE   '
      'from '
      '  SIEl se'
      'where  '
      '  se.ID=:ID')
    SelectSQL.Strings = (
      'select '
      '  se.ID, '
      '  se.INVID, '
      '  se.SEMIS, '
      '  se.Q, '
      '  se.W, '
      '  se.TPRICE, '
      '  se.PRICE,                '
      '  se.NDSID, '
      '  se.NDS, '
      '  se.T, '
      '  se.COST, '
      '  se.OPERID, '
      '  se.FOR_Q_U,'
      '  se.UEPrice'
      'from '
      '  SIEl se '
      '  left outer join D_Nds nds on (se.NdsId = nds.NdsId)'
      'where '
      '  se.InvId=:INVID'
      'order by '
      '   se.Id')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeEdit = SICheckClose
    BeforeInsert = SICheckClose
    BeforeOpen = taSIElBeforeOpen
    OnCalcFields = taSIElCalcFields
    OnNewRecord = taSIElNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 470
    Top = 4
    object taSIElRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taSIElID: TIntegerField
      FieldName = 'ID'
      Origin = 'SIEL.ID'
      Required = True
    end
    object taSIElINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SIEL.INVID'
      Required = True
    end
    object taSIElSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Origin = 'SIEL.SEMIS'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSIElQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'SIEL.Q'
      Required = True
      OnChange = taSIElQChange
    end
    object taSIElW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SIEL.W'
      Required = True
      OnChange = taSIElWChange
      DisplayFormat = '0.###'
    end
    object taSIElTPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'TPRICE'
      Origin = 'SIEL.TPRICE'
      Required = True
      OnChange = taSIElTPRICEChange
      currency = True
    end
    object taSIElPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'SIEL.PRICE'
      Required = True
      currency = True
    end
    object taSIElNDS: TFloatField
      DisplayLabel = #1053#1044#1057
      FieldName = 'NDS'
      Origin = 'SIEL.NDS'
      Required = True
      currency = True
    end
    object taSIElSEMISNAME: TStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldKind = fkLookup
      FieldName = 'SEMISNAME'
      LookupDataSet = dm.taSemis
      LookupKeyFields = 'SEMISID'
      LookupResultField = 'SEMIS'
      KeyFields = 'SEMIS'
      LookupCache = True
      Size = 40
      Lookup = True
    end
    object taSIElUQ: TStringField
      DisplayLabel = #1045#1048' '#1082#1086#1083'-'#1074#1072
      FieldKind = fkCalculated
      FieldName = 'UQ'
      OnGetText = UQGetText
      Size = 10
      Calculated = True
    end
    object taSIElNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SIEL.NDSID'
      Required = True
    end
    object taSIElUW: TStringField
      DisplayLabel = #1045#1048' '#1074#1077#1089#1072
      FieldKind = fkCalculated
      FieldName = 'UW'
      OnGetText = UWGetText
      Size = 10
      Calculated = True
    end
    object taSIElT: TSmallintField
      FieldName = 'T'
      Origin = 'SIEL.T'
    end
    object taSIElCOST: TFloatField
      DisplayLabel = #1057#1091#1084#1084#1072
      FieldName = 'COST'
      Origin = 'SIEL.COST'
      Required = True
      currency = True
    end
    object taSIElOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'SIEL.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSIElOPERNAME: TStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldKind = fkLookup
      FieldName = 'OPERNAME'
      LookupDataSet = dm.taOper
      LookupKeyFields = 'OPERID'
      LookupResultField = 'OPERATION'
      KeyFields = 'OPERID'
      LookupCache = True
      Size = 60
      Lookup = True
    end
    object taSIElFOR_Q_U: TSmallintField
      DisplayLabel = #1062#1077#1085#1072' '#1079#1072' '#1074#1077#1089'/'#1082#1086#1083'-'#1074#1086
      FieldName = 'FOR_Q_U'
      Origin = 'SIEL.FOR_Q_U'
      Required = True
      OnChange = taSIElFOR_Q_UChange
    end
    object taSIElUEPRICE: TFIBFloatField
      FieldName = 'UEPRICE'
      OnChange = taSIElUEPRICEChange
      currency = True
    end
  end
  object dsrSIEl: TDataSource
    DataSet = taSIEl
    Left = 470
    Top = 47
  end
  object taTmp: TpFIBDataSet
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 588
    Top = 4
  end
  object taWhSemis: TpFIBDataSet
    UpdateSQL.Strings = (
      'update WhSemis set'
      '  Id=null '
      'where Id is null'
      '')
    InsertSQL.Strings = (
      'execute procedure F_INSERT(:DEPID)')
    RefreshSQL.Strings = (
      
        'select DEPID, SEMISID, Q, W, UQ, UW, SEMISNAME, OPERID,         ' +
        '           '
      '          OPERNAME, GOOD, DEPNAME, SORTINDDEPID, SORTIND'
      'from WhSemis_R(:DEPID, :SEMISID, :OPERID)')
    SelectSQL.Strings = (
      
        'select DEPID, DEPNAME, SEMISID, Q, W, UQ, UW, SEMISNAME,        ' +
        '   OPERID, OPERNAME, GOOD, SORTINDDEPID, SORTIND'
      
        'from WhSemis_S(:OPERID, :DEPID, :MATID1, :MATID2,  :T,        :F' +
        'ILTEROPERID)'
      'order by SORTINDDEPID, SORTIND'
      ''
      '            '
      '            ')
    CacheModelOptions.BufferChunks = 1000
    AfterOpen = taWhSemisAfterOpen
    BeforeOpen = taWhSemisBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 24
    Top = 108
    object taWhSemisDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'WHSEMIS.DEPID'
      Required = True
    end
    object taWhSemisSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'WHSEMIS.SEMISID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWhSemisQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'WHSEMIS_S.Q'
    end
    object taWhSemisW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'WHSEMIS.W'
      Required = True
      DisplayFormat = '0.###'
    end
    object taWhSemisUQ: TSmallintField
      DisplayLabel = #1045#1048' '#1082#1086#1083'-'#1074#1072
      FieldName = 'UQ'
      Origin = 'WHSEMIS.UQ'
      OnGetText = UQGetText
    end
    object taWhSemisUW: TSmallintField
      DisplayLabel = #1045#1048' '#1074#1077#1089#1072
      FieldName = 'UW'
      Origin = 'WHSEMIS.UW'
      OnGetText = UWGetText
    end
    object taWhSemisSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'D_SEMIS.SEMIS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWhSemisOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'WHSEMIS_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWhSemisOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'WHSEMIS_S.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taWhSemisGOOD: TSmallintField
      FieldName = 'GOOD'
      Origin = 'WHSEMIS_S.GOOD'
    end
    object taWhSemisDEPNAME: TFIBStringField
      DisplayLabel = #1052#1061
      FieldName = 'DEPNAME'
      Origin = 'WHSEMIS_S.DEPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taWhSemisSORTINDDEPID: TIntegerField
      FieldName = 'SORTINDDEPID'
      Origin = 'WHSEMIS_S.SORTINDDEPID'
    end
    object taWhSemisSORTIND: TIntegerField
      FieldName = 'SORTIND'
      Origin = 'WHSEMIS_S.SORTIND'
    end
  end
  object dsrWhSemis: TDataSource
    DataSet = taWhSemis
    Left = 24
    Top = 152
  end
  object taErrWhArt: TpFIBDataSet
    SelectSQL.Strings = (
      'select PSID, OPERID,  ART2ID,  ART, ART2, SZID, RQ, FQ,'
      '    U, WHID, d.Name'
      'from Compare_WhAppl(:T) c,  D_Dep d'
      'where d.DepId=c.PsId')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 616
    Top = 416
    object taErrWhArtPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'COMPARE_WHAPPL.PSID'
    end
    object taErrWhArtOPERID: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERID'
      Origin = 'COMPARE_WHAPPL.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taErrWhArtART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'COMPARE_WHAPPL.ART2ID'
    end
    object taErrWhArtART: TFIBStringField
      FieldName = 'ART'
      Origin = 'COMPARE_WHAPPL.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taErrWhArtART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'COMPARE_WHAPPL.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taErrWhArtSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'COMPARE_WHAPPL.SZID'
    end
    object taErrWhArtRQ: TIntegerField
      DisplayLabel = #1053#1072' '#1089#1082#1083#1072#1076#1077
      FieldName = 'RQ'
      Origin = 'COMPARE_WHAPPL.RQ'
    end
    object taErrWhArtFQ: TIntegerField
      DisplayLabel = #1055#1086#1089#1095#1080#1090#1072#1085#1086
      FieldName = 'FQ'
      Origin = 'COMPARE_WHAPPL.FQ'
    end
    object taErrWhArtU: TSmallintField
      FieldName = 'U'
      Origin = 'COMPARE_WHAPPL.U'
    end
    object taErrWhArtWHID: TIntegerField
      FieldName = 'WHID'
      Origin = 'COMPARE_WHAPPL.WHID'
    end
    object taErrWhArtNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'NAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrErrWhArt: TDataSource
    DataSet = taErrWhArt
    Left = 616
    Top = 464
  end
  object taASItem: TpFIBDataSet
    UpdateSQL.Strings = (
      
        'execute procedure SItem_U_A(:ID, :W, :SELID, :PRICE, :SORTIND, :' +
        'IS$PRINTED)')
    DeleteSQL.Strings = (
      'delete from  SItem where SItemId=:OLD_Id')
    InsertSQL.Strings = (
      
        'execute procedure SItem_I_A(:ID, :SELID, :ARTID, :ART2ID, :OPERI' +
        'D,'
      
        '    :UID, :W, :SZID, :P0, :INVID, :UA, :UA_OLD, :S_OPERID, :SORT' +
        'IND)')
    RefreshSQL.Strings = (
      'select ID, SELID, ARTID, ART, ART2ID, ART2, OPERID, OPERNAME,'
      
        '    UID, W, SZID, SZNAME, U, P0, INVID, PRICE, TPRICE, UA, UA_OL' +
        'D,'
      '    S_OPERID, ARTU, S_OPERNAME, SORTIND'
      'from SItem_R_A(:ID)')
    SelectSQL.Strings = (
      'select ID, SELID, ARTID, ART, ART2ID, ART2, OPERID, OPERNAME,'
      
        '    UID, W, SZID, SZNAME, U, P0, INVID, PRICE, TPRICE, UA, UA_OL' +
        'D,'
      '    S_OPERID, ARTU, S_OPERNAME, SORTIND, Is$Printed'
      'from SItem_S_A(:INVID)'
      'order by ART, ART2, SZNAME')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeEdit = ASICheckClose
    BeforeInsert = ASICheckClose
    BeforeOpen = taASItemBeforeOpen
    OnCalcFields = taASItemCalcFields
    OnDeleteError = taASItemDeleteError
    OnNewRecord = taASItemNewRecord
    OnPostError = taASItemPostError
    Transaction = dm.tr
    Database = dm.db
    Left = 316
    Top = 100
    oFetchAll = True
    object taASItemRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taASItemID: TIntegerField
      FieldName = 'ID'
      Origin = 'SITEM_S_A.ID'
    end
    object taASItemSELID: TIntegerField
      FieldName = 'SELID'
      Origin = 'SITEM_S_A.SELID'
    end
    object taASItemARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'SITEM_S_A.ARTID'
    end
    object taASItemART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'SITEM_S_A.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taASItemART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'SITEM_S_A.ART2ID'
    end
    object taASItemART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'SITEM_S_A.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taASItemOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'SITEM_S_A.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taASItemOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088'. ('#1072#1089#1089#1086#1088#1090')'
      FieldName = 'OPERNAME'
      Origin = 'SITEM_S_A.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taASItemUID: TIntegerField
      DisplayLabel = #1048#1076'. '#8470
      FieldName = 'UID'
      Origin = 'SITEM_S_A.UID'
    end
    object taASItemW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SITEM_S_A.W'
    end
    object taASItemSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'SITEM_S_A.SZID'
    end
    object taASItemSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      Origin = 'SITEM_S_A.SZNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taASItemU: TSmallintField
      DisplayLabel = #1045#1076'.  '#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'SITEM_S_A.U'
    end
    object taASItemP0: TSmallintField
      FieldName = 'P0'
      Origin = 'SITEM_S_A.P0'
    end
    object taASItemINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SITEM_S_A.INVID'
    end
    object taASItemPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1091#1095#1077#1090#1085#1072#1103
      FieldName = 'PRICE'
      Origin = 'SITEM_S_A.PRICE'
      currency = True
    end
    object taASItemTPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1091#1095#1077#1090#1085#1072#1103
      FieldName = 'TPRICE'
      Origin = 'SITEM_S_A.TPRICE'
      currency = True
    end
    object taASItemUA: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'. '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      FieldName = 'UA'
      Origin = 'SITEM_S_A.UA'
      OnGetText = UGetText
    end
    object taASItemCOST: TFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldKind = fkCalculated
      FieldName = 'COST'
      currency = True
      Calculated = True
    end
    object taASItemUA_OLD: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'UA_OLD'
      Origin = 'SITEM_S_A.UA_OLD'
      OnGetText = taASItemUA_OLDGetText
    end
    object taASItemS_OPERID: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088' ('#1087#1086#1083#1091#1092'-'#1090#1099')'
      FieldName = 'S_OPERID'
      Origin = 'SITEM_S_A.S_OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taASItemARTU: TSmallintField
      FieldName = 'ARTU'
      Origin = 'SITEM_S_A.ARTU'
    end
    object taASItemS_OPERNAME: TFIBStringField
      FieldName = 'S_OPERNAME'
      Origin = 'SITEM_S_A.S_OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taASItemSORTIND: TIntegerField
      FieldName = 'SORTIND'
      Origin = 'SITEM_S_A.SORTIND'
    end
    object taASItemISPRINTED: TFIBSmallIntField
      FieldName = 'IS$PRINTED'
    end
  end
  object dsrASItem: TDataSource
    DataSet = taASItem
    Left = 316
    Top = 144
  end
  object taASList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Inv set'
      '  DOCNO=:DOCNO, '
      '  DOCDATE=:DOCDATE,   '
      '  ISCLOSE=:ISCLOSE,'
      '  CONTRACTID=:CONTRACTID,'
      '  COLORID=:INVCOLORID,'
      '  JOBID=:JOBID,'
      '  Shiped_By=:ShipedBy,'
      '  MC = :MC,'
      '  CompID = :Producer$ID'
      'where InvId=:OLD_InvId')
    DeleteSQL.Strings = (
      'delete from Inv  where InvId=:OLD_InvId')
    InsertSQL.Strings = (
      'insert into Inv(INVID, DOCNO, DOCDATE, ITYPE, DEPID, FROMDEPID,'
      
        '   USERID, ISCLOSE, NDSID, CONTRACTID, COLORID, JOBID, MC, CompI' +
        'D)'
      'values (:INVID, :DOCNO, :DOCDATE, :ITYPE, :DEPID, :FROMDEPID, '
      
        '   :USERID, :ISCLOSE, :NDSID, :CONTRACTID, :INVCOLORID, :JOBID, ' +
        '0, 1)')
    RefreshSQL.Strings = (
      'select INVID, DOCNO, DOCDATE , ITYPE, DEPID, DEPNAME,'
      '   USERID, NDSID, FIO, ISCLOSE, REF, FROMDEPID, '
      
        '   FROMDEPNAME, Q, W, COST, JOBID, JOBNAME, SCOST, CONTRACTID, C' +
        'ONTRACTNAME,'
      '   INVCOLORID, COLOR, MC, Producer$ID, Producer$Name'
      'from DList_R(:INVID)')
    SelectSQL.Strings = (
      'select INVID, DOCNO, DOCDATE , ITYPE, DEPID, DEPNAME,'
      '   USERID, NDSID, FIO, ISCLOSE, REF, FROMDEPID, '
      '   FROMDEPNAME, Q, W, COST, JOBID, JOBNAME, SCOST, CONTRACTID, '
      
        '   CONTRACTNAME, INVCOLORID, COLOR, SHIPEDBY, MC, Producer$ID, P' +
        'roducer$Name'
      
        'from DList_S_NEW(:FROMDEPID1, :FROMDEPID2, :DEPID1, :DEPID2, :BD' +
        ', :ED, :ITYPE_)'
      'order by DocDate')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taASListBeforeDelete
    BeforeOpen = taASListBeforeOpen
    OnNewRecord = taASListNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1054#1090#1075#1088#1091#1079#1082#1072' '#1089' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072
    Left = 264
    Top = 100
    object taASListINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SLIST_S.INVID'
    end
    object taASListDOCNO: TIntegerField
      DisplayLabel = #8470' '#1085#1072#1082#1083'.'
      FieldName = 'DOCNO'
      Origin = 'SLIST_S.DOCNO'
    end
    object taASListDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
      FieldName = 'DOCDATE'
      Origin = 'SLIST_S.DOCDATE'
      OnValidate = taASListDOCDATEValidate
    end
    object taASListITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'SLIST_S.ITYPE'
    end
    object taASListDEPID: TIntegerField
      DisplayWidth = 10
      FieldName = 'DEPID'
      Origin = 'SLIST_S.DEPID'
    end
    object taASListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'SLIST_S.USERID'
    end
    object taASListFIO: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldKind = fkLookup
      FieldName = 'FIO'
      LookupDataSet = taMol
      LookupKeyFields = 'MOLID'
      LookupResultField = 'FIO'
      KeyFields = 'USERID'
      Origin = 'SLIST_S.FIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
      Lookup = True
    end
    object taASListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'SLIST_S.ISCLOSE'
    end
    object taASListDEPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
      FieldName = 'DEPNAME'
      Origin = 'SLIST_S.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taASListFROMDEPID: TIntegerField
      FieldName = 'FROMDEPID'
      Origin = 'SLIST_S.FROMDEPID'
    end
    object taASListFROMDEPNAME: TFIBStringField
      DisplayLabel = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
      FieldName = 'FROMDEPNAME'
      Origin = 'SLIST_S.FROMDEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taASListNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SLIST_S.NDSID'
    end
    object taASListREF: TIntegerField
      FieldName = 'REF'
      Origin = 'SLIST_S.REF'
    end
    object taASListQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'SLIST_S.Q'
    end
    object taASListW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SLIST_S.W'
    end
    object taASListCOST: TFloatField
      FieldName = 'COST'
      Origin = 'DLIST_S.COST'
    end
    object taASListSCOST: TFIBFloatField
      DisplayLabel = #1059#1095#1077#1090#1085#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'SCOST'
      currency = True
    end
    object taASListCONTRACTID: TFIBIntegerField
      FieldName = 'CONTRACTID'
      OnChange = taASListCONTRACTIDChange
    end
    object taASListCONTRACTNAME: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1076#1086#1075#1086#1074#1086#1088#1072
      FieldName = 'CONTRACTNAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taASListINVCOLORID: TFIBIntegerField
      FieldName = 'INVCOLORID'
    end
    object taASListCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
    object taASListJOBID: TFIBIntegerField
      FieldName = 'JOBID'
    end
    object taASListJOBNAME: TFIBStringField
      FieldName = 'JOBNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taASListShipedBy: TFIBIntegerField
      FieldName = 'ShipedBy'
    end
    object taASListMC: TFIBIntegerField
      FieldName = 'MC'
    end
    object taASListPRODUCERID: TFIBIntegerField
      FieldName = 'PRODUCER$ID'
    end
    object taASListPRODUCERNAME: TFIBStringField
      FieldName = 'PRODUCER$NAME'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsrASList: TDataSource
    DataSet = taASList
    Left = 264
    Top = 145
  end
  object taErrWhSemis: TpFIBDataSet
    SelectSQL.Strings = (
      'select DEPID, DEPNAME, SEMISID, SEMISNAME,'
      '    OPERID, OPERNAME, FQ, FW, RQ, RW, UQ,  UW'
      'from Compare_WhSemis(:T)'
      'order by  DepName')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 684
    Top = 416
    object taErrWhSemisDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'COMPARE_WHSEMIS.DEPID'
    end
    object taErrWhSemisDEPNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'DEPNAME'
      Origin = 'COMPARE_WHSEMIS.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taErrWhSemisSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'COMPARE_WHSEMIS.SEMISID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taErrWhSemisSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'COMPARE_WHSEMIS.SEMISNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taErrWhSemisOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'COMPARE_WHSEMIS.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taErrWhSemisOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'COMPARE_WHSEMIS.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taErrWhSemisFQ: TIntegerField
      DisplayLabel = #1055#1086#1089#1095#1080#1090#1072#1085#1086' '#1082#1086#1083'-'#1074#1086
      FieldName = 'FQ'
      Origin = 'COMPARE_WHSEMIS.FQ'
    end
    object taErrWhSemisFW: TFloatField
      DisplayLabel = #1055#1086#1089#1095#1080#1090#1072#1085' '#1074#1077#1089
      FieldName = 'FW'
      Origin = 'COMPARE_WHSEMIS.FW'
      DisplayFormat = '0.###'
    end
    object taErrWhSemisRQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'RQ'
      Origin = 'COMPARE_WHSEMIS.RQ'
    end
    object taErrWhSemisRW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'RW'
      Origin = 'COMPARE_WHSEMIS.RW'
      DisplayFormat = '0.###'
    end
    object taErrWhSemisUQ: TSmallintField
      FieldName = 'UQ'
      Origin = 'COMPARE_WHSEMIS.UQ'
      OnGetText = UQGetText
    end
    object taErrWhSemisUW: TSmallintField
      FieldName = 'UW'
      Origin = 'COMPARE_WHSEMIS.UW'
      OnGetText = UWGetText
    end
  end
  object dsrErrWhSemis: TDataSource
    DataSet = taErrWhSemis
    Left = 686
    Top = 464
  end
  object taErrSInvProd: TpFIBDataSet
    SelectSQL.Strings = (
      'select INVID, SELID,  ITEMID,  ARTID,  ART,  ART2ID,'
      '    ART2,  UID,  UA,  OPERID'
      'from ERR_SINV_PROD')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 476
    Top = 416
    object taErrSInvProdINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'ERR_SINV_PROD.INVID'
      Required = True
    end
    object taErrSInvProdSELID: TIntegerField
      FieldName = 'SELID'
      Origin = 'ERR_SINV_PROD.SELID'
      Required = True
    end
    object taErrSInvProdITEMID: TIntegerField
      FieldName = 'ITEMID'
      Origin = 'ERR_SINV_PROD.ITEMID'
      Required = True
    end
    object taErrSInvProdARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'ERR_SINV_PROD.ARTID'
    end
    object taErrSInvProdART: TIntegerField
      FieldName = 'ART'
      Origin = 'ERR_SINV_PROD.ART'
    end
    object taErrSInvProdART2ID: TFIBStringField
      FieldName = 'ART2ID'
      Origin = 'ERR_SINV_PROD.ART2ID'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taErrSInvProdART2: TFIBStringField
      FieldName = 'ART2'
      Origin = 'ERR_SINV_PROD.ART2'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taErrSInvProdUID: TIntegerField
      FieldName = 'UID'
      Origin = 'ERR_SINV_PROD.UID'
    end
    object taErrSInvProdUA: TSmallintField
      FieldName = 'UA'
      Origin = 'ERR_SINV_PROD.UA'
    end
    object taErrSInvProdOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'ERR_SINV_PROD.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
  end
  object dsrErrSinvProd: TDataSource
    DataSet = taErrSInvProd
    Left = 476
    Top = 464
  end
  object taWhArt: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, PSID, OPERID, ARTID, ART2ID, SZID, T, Q, ART2,'
      '    ART, SZ, OPERNAME, U, PSNAME'
      'from WHAppl_S1(:DEPID1, :DEPID2, :OPERID1, :OPERID2)'
      'where ART like :ART'
      'order by ART, ART2, SZ')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterOpen = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeOpen = taWhArtBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 376
    Top = 100
    object taWhArtID: TIntegerField
      FieldName = 'ID'
      Origin = 'WHAPPL_S1.ID'
    end
    object taWhArtPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'WHAPPL_S1.PSID'
    end
    object taWhArtOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'WHAPPL_S1.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWhArtARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'WHAPPL_S1.ARTID'
    end
    object taWhArtART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'WHAPPL_S1.ART2ID'
    end
    object taWhArtSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'WHAPPL_S1.SZID'
    end
    object taWhArtT: TSmallintField
      FieldName = 'T'
      Origin = 'WHAPPL_S1.T'
    end
    object taWhArtQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'WHAPPL_S1.Q'
    end
    object taWhArtART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'WHAPPL_S1.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taWhArtART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'WHAPPL_S1.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taWhArtSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'WHAPPL_S1.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taWhArtOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'WHAPPL_S1.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taWhArtU: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'WHAPPL_S1.U'
      OnGetText = UGetText
    end
    object taWhArtPSNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'PSNAME'
      Origin = 'WHAPPL_S1.PSNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrWhArt: TDataSource
    DataSet = taWhArt
    Left = 376
    Top = 144
  end
  object taWhS: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, DEPID, DEPNAME, SEMISID, SEMISNAME,'
      '    MATID, MATNAME, OPERID, OPERNAME, Q, W, UQ, UW, Q0'
      'from WhSemis_S1(:DEPID1, :DEPID2, :OPERID1, :OPERID2, :SEMISID1,'
      '  :SEMISID2, :MATID1, :MATID2)'
      'where not(Q=0  and equal(W, 0, 0.001)=1)'
      'order by SEMISNAME')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeOpen = taWhSBeforeOpen
    OnCalcFields = taWhSCalcFields
    Transaction = dm.tr
    Database = dm.db
    Left = 432
    Top = 100
    object taWhSID: TIntegerField
      FieldName = 'ID'
      Origin = 'WHSEMIS_S1.ID'
    end
    object taWhSDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'WHSEMIS_S1.DEPID'
    end
    object taWhSDEPNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'DEPNAME'
      Origin = 'WHSEMIS_S1.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWhSSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'WHSEMIS_S1.SEMISID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWhSSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'WHSEMIS_S1.SEMISNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWhSMATID: TFIBStringField
      FieldName = 'MATID'
      Origin = 'WHSEMIS_S1.MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWhSMATNAME: TFIBStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldName = 'MATNAME'
      Origin = 'WHSEMIS_S1.MATNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWhSOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'WHSEMIS_S1.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWhSOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'WHSEMIS_S1.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taWhSQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'WHSEMIS_S1.Q'
    end
    object taWhSW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'WHSEMIS_S1.W'
    end
    object taWhSUQ: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'. '#1082#1086#1083'-'#1074#1072
      FieldName = 'UQ'
      Origin = 'WHSEMIS_S1.UQ'
      OnGetText = UQGetText
    end
    object taWhSUW: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'. '#1074#1077#1089#1072
      FieldName = 'UW'
      Origin = 'WHSEMIS_S1.UW'
      OnGetText = UWGetText
    end
    object taWhSQ0: TFIBFloatField
      FieldName = 'Q0'
    end
    object taWhSW0: TFIBFloatField
      FieldKind = fkCalculated
      FieldName = 'W0'
      DisplayFormat = '0.###'
      Calculated = True
    end
  end
  object dsrWhS: TDataSource
    DataSet = taWhS
    Left = 432
    Top = 144
  end
  object taPIt_InSemis_d: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AIt_InSemis set'
      '  FQ=:FQ,'
      '  FW=:FW'
      'where ID=:OLD_ID')
    DeleteSQL.Strings = (
      'delete from AIt_InSemis where ID=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into AIt_InSemis(ID, PROTOCOLID, PSID, OPERID, FROMOPERID' +
        ', SEMISID, '
      '    FQ, FW, RQ, RW, DONEQ, DONEW, GETQ, GETW, '
      '    INQ, INW, IQ, IW, NKQ, NKW,'
      '    REJQ, REJW, RETQ, RETW, REWORKQ, REWORKW, S0)'
      'values(:ID, :PROTOCOLID, :PSID, :OPERID, :FROMOPERID, :SEMISID, '
      '    :FQ, :FW, :RQ, :RW, 0, 0, 0, 0,'
      '    0, 0, 0, 0, 0, 0,'
      '    0, 0, 0, 0, 0, 0, :S0)')
    SelectSQL.Strings = (
      'select'
      '  a.ID, a.PROTOCOLID, a.PSID, d.name PSNAME,'
      '  a.OPERID, o.Operation OPERNAME, a.FROMOPERID,'
      '  a.SEMISID, s.Semis SEMISNAME, s.GOOD, s.Q0,'
      
        '  a.FQ, a.FW, a.GETW, a.GETQ, a.DONEW, a.DONEQ,                 ' +
        '          a.RETW, a.RETQ, a.REJW,  a.REJQ, a.REWORKW, '
      '  a.REWORKQ, a.NKW, a.NKQ, a.IQ, a.IW, a.INW, a.INQ            '
      'from '
      '  AIt_InSemis a '
      '  left outer join D_Oper o2 on (a.FROMOPERID = o2.OPERID) '
      '  left outer join D_Semis s on (a.SEMISID = s.SEMISID)'
      '  left outer join D_oper o on (a.OPERID = o.OPERID)'
      '  left outer join D_Dep d on (a.PSID = d.DEPID)'
      'where '
      '  a.ProtocolId=:PROTOCOLID and '
      '  d.DEPID=:PSID and'
      '  o.OPERID=:OPERID and'
      '  s.MAT between :MATID1 and :MATID2            '
      'order by '
      '  s.SEMIS'
      '    '
      '')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeEdit = taPIt_InSemis_dBeforeEdit
    BeforeOpen = taPIt_InSemis_dBeforeOpen
    OnCalcFields = taPIt_InSemis_dCalcFields
    OnNewRecord = taPIt_InSemis_dNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 218
    Top = 200
    object taPIt_InSemis_dID: TIntegerField
      FieldName = 'ID'
      Origin = 'AIT_SEMIS_S_G.ID'
    end
    object taPIt_InSemis_dPROTOCOLID: TIntegerField
      FieldName = 'PROTOCOLID'
      Origin = 'AIT_SEMIS_S_G.PROTOCOLID'
    end
    object taPIt_InSemis_dPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'AIT_SEMIS_S_G.PSID'
    end
    object taPIt_InSemis_dPSNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'PSNAME'
      Origin = 'AIT_SEMIS_S_G.PSNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taPIt_InSemis_dOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AIT_SEMIS_S_G.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taPIt_InSemis_dOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'AIT_SEMIS_S_G.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taPIt_InSemis_dSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'AIT_SEMIS_S_G.SEMISID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taPIt_InSemis_dSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'AIT_SEMIS_S_G.SEMISNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taPIt_InSemis_dFQ: TIntegerField
      DisplayLabel = #1060#1072#1082#1090'. '#1082#1086#1083'-'#1074#1086
      FieldName = 'FQ'
      Origin = 'AIT_SEMIS_S_G.FQ'
    end
    object taPIt_InSemis_dFW: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1060#1072#1082#1090'. '#1074#1077#1089
      FieldName = 'FW'
      Origin = 'AIT_SEMIS_S_G.FW'
      DisplayFormat = '0.###'
    end
    object taPIt_InSemis_dRQ: TIntegerField
      DisplayLabel = #1057#1087#1080#1089#1072#1085#1080#1077' '#1082#1086#1083'-'#1074#1086
      FieldKind = fkCalculated
      FieldName = 'RQ'
      Calculated = True
    end
    object taPIt_InSemis_dRW: TFloatField
      DisplayLabel = #1057#1087#1080#1089#1072#1085#1080#1077' '#1074#1077#1089
      FieldKind = fkCalculated
      FieldName = 'RW'
      DisplayFormat = '0.###'
      Calculated = True
    end
    object taPIt_InSemis_dSEMISNAME1: TStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldKind = fkLookup
      FieldName = 'SEMISNAME1'
      LookupDataSet = dm.taSemis
      LookupKeyFields = 'SEMISID'
      LookupResultField = 'SEMIS'
      KeyFields = 'SEMISID'
      Size = 40
      Lookup = True
    end
    object taPIt_InSemis_dGOOD: TSmallintField
      DefaultExpression = '0'
      FieldName = 'GOOD'
      Origin = 'AIT_SEMIS_S_G.GOOD'
    end
    object taPIt_InSemis_dGETW: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1042#1099#1076#1072#1085#1086' '#1074#1077#1089
      FieldName = 'GETW'
      Origin = 'AIT_SEMIS_S_G.GETW'
      DisplayFormat = '0.###'
    end
    object taPIt_InSemis_dGETQ: TIntegerField
      DisplayLabel = #1042#1099#1076#1072#1085#1086' '#1082#1086#1083'-'#1074#1086
      FieldName = 'GETQ'
      Origin = 'AIT_SEMIS_S_G.GETQ'
    end
    object taPIt_InSemis_dDONEW: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1053#1072#1088#1103#1076' '#1074#1077#1089
      FieldName = 'DONEW'
      Origin = 'AIT_SEMIS_S_G.DONEW'
      DisplayFormat = '0.###'
    end
    object taPIt_InSemis_dDONEQ: TIntegerField
      DisplayLabel = #1053#1072#1088#1103#1076' '#1082#1086#1083'-'#1074#1086
      FieldName = 'DONEQ'
      Origin = 'AIT_SEMIS_S_G.DONEQ'
    end
    object taPIt_InSemis_dRETW: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1074#1077#1089
      FieldName = 'RETW'
      Origin = 'AIT_SEMIS_S_G.RETW'
      DisplayFormat = '0.###'
    end
    object taPIt_InSemis_dRETQ: TIntegerField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1082#1086#1083'-'#1074#1086
      FieldName = 'RETQ'
      Origin = 'AIT_SEMIS_S_G.RETQ'
    end
    object taPIt_InSemis_dREJW: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1041#1088#1072#1082' '#1074#1077#1089
      FieldName = 'REJW'
      Origin = 'AIT_SEMIS_S_G.REJW'
      DisplayFormat = '0.###'
    end
    object taPIt_InSemis_dREJQ: TIntegerField
      DisplayLabel = #1042#1077#1089' '#1082#1086#1083'-'#1074#1086
      FieldName = 'REJQ'
      Origin = 'AIT_SEMIS_S_G.REJQ'
    end
    object taPIt_InSemis_dREWORKW: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1044#1086#1088#1072#1073#1086#1090#1082#1072' '#1074#1077#1089
      FieldName = 'REWORKW'
      Origin = 'AIT_SEMIS_S_G.REWORKW'
      DisplayFormat = '0.###'
    end
    object taPIt_InSemis_dREWORKQ: TIntegerField
      DisplayLabel = #1044#1086#1088#1072#1073#1086#1090#1082#1072' '#1082#1086#1083'-'#1074#1086
      FieldName = 'REWORKQ'
      Origin = 'AIT_SEMIS_S_G.REWORKQ'
    end
    object taPIt_InSemis_dNKW: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1053#1077#1082#1086#1084#1087#1083#1077#1082#1090' '#1074#1077#1089
      FieldName = 'NKW'
      Origin = 'AIT_SEMIS_S_G.NKW'
      DisplayFormat = '0.###'
    end
    object taPIt_InSemis_dNKQ: TIntegerField
      FieldName = 'NKQ'
      Origin = 'AIT_SEMIS_S_G.NKQ'
    end
    object taPIt_InSemis_dIW: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1048#1090#1086#1075#1086' '#1074#1077#1089
      FieldName = 'IW'
      Origin = 'AIT_INSEMIS.IW'
      Required = True
      DisplayFormat = '0.###'
    end
    object taPIt_InSemis_dIQ: TIntegerField
      DisplayLabel = #1048#1090#1086#1075#1086' '#1082#1086#1083'-'#1074#1086
      FieldName = 'IQ'
      Origin = 'AIT_INSEMIS.IQ'
      Required = True
    end
    object taPIt_InSemis_dINW: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1042#1093#1086#1076'. '#1074#1077#1089
      FieldName = 'INW'
      Origin = 'AIT_INSEMIS.INW'
      Required = True
      DisplayFormat = '0.###'
    end
    object taPIt_InSemis_dINQ: TIntegerField
      DisplayLabel = #1042#1093#1086#1076'. '#1082#1086#1083'-'#1074#1086
      FieldName = 'INQ'
      Origin = 'AIT_INSEMIS.INQ'
      Required = True
    end
    object taPIt_InSemis_dQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'Q'
      Calculated = True
    end
    object taPIt_InSemis_dW: TFloatField
      FieldKind = fkCalculated
      FieldName = 'W'
      DisplayFormat = '0.###'
      Calculated = True
    end
    object taPIt_InSemis_dQ0: TFloatField
      DefaultExpression = '0'
      FieldName = 'Q0'
      Origin = 'D_SEMIS.Q0'
      Required = True
    end
    object taPIt_InSemis_dFROMOPERID: TFIBStringField
      FieldName = 'FROMOPERID'
      Origin = 'AIT_INSEMIS.FROMOPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taPIt_InSemis_dFROMOPERNAME: TStringField
      DisplayLabel = #1055#1086#1089#1083#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
      FieldKind = fkLookup
      FieldName = 'FROMOPERNAME'
      LookupDataSet = dm.taOper
      LookupKeyFields = 'OPERID'
      LookupResultField = 'OPERATION'
      KeyFields = 'FROMOPERID'
      Size = 60
      Lookup = True
    end
  end
  object dsrPIt_InSemis_d: TDataSource
    DataSet = taPIt_InSemis_d
    Left = 218
    Top = 245
  end
  object taProtocol_d: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AITEM set'
      '    F1=:F, '
      '    F=:F_999,'
      '    S=:S_999,'
      '    E=:E_999,'
      '    S1=:S,'
      '    E1=:E, '
      '    RW=:RW,'
      '    RQ=:RQ, '
      '    DONEW1=:DONEW,'
      '    DONEQ=:DONEQ,'
      '    H=:H,'
      '    I=:I,'
      '    U=:U,'
      '    Q_0=:Q_0,'
      '    Q_1=:Q_1,'
      '    N2 = :N2'
      'where ID=:OLD_ID')
    RefreshSQL.Strings = (
      
        'select ID, ACTID, PSID, PSNAME, OPERID, OPERNAME, F, N, N2, E, S' +
        ', I, H, DONEW, RW, RQ, U, '
      
        '   F_999, E_999, S_999, N_999, INW, INQ, DONEQ, Q_0, Q_1, Q_A, F' +
        'Q_A,'
      '   TRANSID, MATID, MATNAME, RET_PERCENT, REJ_PERCENT'
      'from AItem_R_G(:ACTID, :ID, :PSID, :OPERID, :TRANSID)')
    SelectSQL.Strings = (
      'select '
      '  ID, '
      '  ACTID, '
      '  PSID, '
      '  PSNAME, '
      '  OPERID, '
      '  OPERNAME, '
      '  F, '
      '  N, '
      '  E, '
      '  S, '
      '  I, '
      '  H, '
      '  DONEW, '
      '  RW, '
      '  RQ, '
      '  U, '
      '  F_999, '
      '  E_999, '
      '  S_999, '
      '  N_999, '
      '  INW, '
      '  INQ,  '
      '  DONEQ, '
      '  Q_0, '
      '  Q_1, '
      '  Q_A, '
      '  FQ_A,'
      '  TRANSID, '
      '  MATID, '
      '  MATNAME, '
      '  RET_PERCENT,'
      '  REJ_PERCENT, '
      '  LOSSES, '
      '  N2,'
      '  Shortage'
      'from '
      '  AItem_S_G(:ACTID)')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeEdit = taProtocol_dBeforeEdit
    BeforeOpen = taProtocol_dBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    OnFilterRecord = taProtocol_dFilterRecord
    Left = 140
    Top = 200
    object taProtocol_dID: TIntegerField
      FieldName = 'ID'
      Origin = 'AITEM_S_G.ID'
    end
    object taProtocol_dACTID: TIntegerField
      FieldName = 'ACTID'
      Origin = 'AITEM_S_G.ACTID'
    end
    object taProtocol_dPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'AITEM_S_G.PSID'
    end
    object taProtocol_dPSNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'PSNAME'
      Origin = 'AITEM_S_G.PSNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taProtocol_dOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AITEM_S_G.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taProtocol_dOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taProtocol_dF: TFloatField
      DisplayLabel = #1060#1072#1082#1090'.'
      FieldName = 'F'
      Origin = 'AITEM_S_G.F'
      DisplayFormat = '0.###'
    end
    object taProtocol_dN: TFloatField
      DisplayLabel = #1053#1086#1088#1084#1072
      FieldName = 'N'
      Origin = 'AITEM_S_G.N'
      DisplayFormat = '0.###'
    end
    object taProtocol_dE: TFloatField
      DisplayLabel = #1069#1082#1086#1085#1086#1084#1080#1103
      FieldName = 'E'
      Origin = 'AITEM_S_G.E'
      DisplayFormat = '0.###'
    end
    object taProtocol_dS: TFloatField
      DisplayLabel = #1057#1074#1077#1088#1093#1085#1086#1088#1084'.'
      FieldName = 'S'
      Origin = 'AITEM_S_G.S'
      DisplayFormat = '0.###'
    end
    object taProtocol_dI: TFloatField
      DisplayLabel = #1048#1079#1083#1080#1096#1082#1080
      FieldName = 'I'
      Origin = 'AITEM_S_G.I'
    end
    object taProtocol_dH: TFloatField
      DisplayLabel = #1053#1077#1076#1086#1089#1090#1072#1095#1072
      FieldName = 'H'
      Origin = 'AITEM_S_G.H'
    end
    object taProtocol_dDONEW: TFloatField
      DisplayLabel = #1050' '#1089#1087#1080#1089#1072#1085#1080#1102
      FieldName = 'DONEW'
      Origin = 'AITEM_S_G.DONEW'
      DisplayFormat = '0.###'
    end
    object taProtocol_dRW: TFloatField
      DisplayLabel = #1042#1077#1089' '#1085#1072#1083'.'
      FieldName = 'RW'
      Origin = 'AITEM_S_G.RW'
      DisplayFormat = '0.###'
    end
    object taProtocol_dU: TFloatField
      DisplayLabel = #1059#1076#1077#1088#1078#1072#1085#1080#1077
      FieldName = 'U'
      Origin = 'AITEM_S_G.U'
      DisplayFormat = '0.###'
    end
    object taProtocol_dF_999: TFloatField
      FieldName = 'F_999'
      Origin = 'AITEM_S_G.F_999'
    end
    object taProtocol_dE_999: TFloatField
      FieldName = 'E_999'
      Origin = 'AITEM_S_G.E_999'
    end
    object taProtocol_dS_999: TFloatField
      FieldName = 'S_999'
      Origin = 'AITEM_S_G.S_999'
    end
    object taProtocol_dN_999: TFloatField
      FieldName = 'N_999'
      Origin = 'AITEM_S_G.N_999'
    end
    object taProtocol_dINW: TFloatField
      DisplayLabel = #1042#1093'. '#1074#1077#1089
      FieldName = 'INW'
      Origin = 'AITEM_S_G.INW'
      DisplayFormat = '0.###'
    end
    object taProtocol_dINQ: TIntegerField
      DisplayLabel = #1042#1093'. '#1082#1086#1083'-'#1074#1086
      FieldName = 'INQ'
      Origin = 'AITEM_S_G.INQ'
      DisplayFormat = '0.###'
    end
    object taProtocol_dRQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086' '#1085#1072#1083
      FieldName = 'RQ'
      Origin = 'AITEM_S_G.RQ'
    end
    object taProtocol_dDONEQ: TIntegerField
      DisplayLabel = #1050' '#1089#1087#1080#1089#1072#1085#1080#1102' '#1082#1086#1083'-'#1074#1086
      FieldName = 'DONEQ'
      Origin = 'AITEM_S_G.DONEQ'
    end
    object taProtocol_dQ_0: TFloatField
      DisplayLabel = #1042#1099#1076#1072#1085#1086
      FieldName = 'Q_0'
      Origin = 'AITEM_S_G.Q_0'
    end
    object taProtocol_dQ_1: TFloatField
      DisplayLabel = #1055#1088#1080#1085#1103#1090#1086
      FieldName = 'Q_1'
      Origin = 'AITEM_S_G.Q_1'
    end
    object taProtocol_dQ_A: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      FieldName = 'Q_A'
      Origin = 'AITEM_S_G.Q_A'
    end
    object taProtocol_dFQ_A: TIntegerField
      DisplayLabel = #1060#1072#1082#1090'. '#1082#1086#1083'-'#1074#1086' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      FieldName = 'FQ_A'
      Origin = 'AITEM_S_G.FQ_A'
    end
    object taProtocol_dMATNAME: TFIBStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldName = 'MATNAME'
      Origin = 'AITEM_S_G.MATNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taProtocol_dMATID: TFIBStringField
      FieldName = 'MATID'
      Origin = 'AITEM_S_G.MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taProtocol_dTRANSID: TIntegerField
      FieldName = 'TRANSID'
      Origin = 'AITEM_S_G.TRANSID'
    end
    object taProtocol_dRET_PERCENT: TFIBFloatField
      FieldName = 'RET_PERCENT'
      DisplayFormat = '0.##%'
    end
    object taProtocol_dREJ_PERCENT: TFIBFloatField
      FieldName = 'REJ_PERCENT'
      DisplayFormat = '0.##%'
    end
    object taProtocol_dLOSSES: TFIBBCDField
      FieldName = 'LOSSES'
      Size = 3
      RoundByScale = True
    end
    object taProtocol_dN2: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072' '#1087#1086#1090#1077#1088#1100
      FieldName = 'N2'
      Origin = 'AItem_S_G.N2'
      DisplayFormat = '0.###'
    end
    object taProtocol_dSHORTAGE: TFIBBCDField
      FieldName = 'SHORTAGE'
      Size = 3
      RoundByScale = True
    end
  end
  object dsrProtocol_d: TDataSource
    DataSet = taProtocol_d
    Left = 140
    Top = 245
  end
  object taDIList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Inv set'
      '  DOCNO=:DOCNO, '
      '  DOCDATE=:DOCDATE,   '
      '  ISCLOSE=:ISCLOSE,'
      '  USERID=:USERID,'
      '  JOBID=:JOBID,'
      '  COLORID=:INVCOLORID'
      'where INVID=:OLD_INVID')
    DeleteSQL.Strings = (
      'delete from Inv where InvId=:OLD_InvId')
    InsertSQL.Strings = (
      'insert into Inv(INVID, DOCNO, DOCDATE, ITYPE, DEPID, FROMDEPID,'
      '   USERID, ISCLOSE, NDSID, JOBID, COLORID)'
      'values (:INVID, :DOCNO, :DOCDATE, :ITYPE, :DEPID, :FROMDEPID, '
      '   :USERID, :ISCLOSE, :NDSID, :JOBID, :INVCOLORID)')
    RefreshSQL.Strings = (
      'select INVID, DOCNO, DOCDATE , ITYPE, DEPID, DEPNAME,'
      '   USERID, NDSID, FIO, ISCLOSE, REF, FROMDEPID, '
      '   FROMDEPNAME, Q, W, COST, JOBID, JOBNAME, INVCOLORID, COLOR'
      'from DList_R(:INVID)')
    SelectSQL.Strings = (
      'select INVID, DOCNO, DOCDATE , ITYPE, DEPID, DEPNAME,'
      '   USERID, NDSID, FIO, ISCLOSE, REF, FROMDEPID, '
      '   FROMDEPNAME, Q, W, COST, JOBID, JOBNAME, INVCOLORID, COLOR'
      
        'from DList_S(:FROMDEPID1, :FROMDEPID2, :DEPID1, :DEPID2, :BD, :E' +
        'D,:ITYPE_)'
      'order by DocDate')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taDIListBeforeDelete
    BeforeOpen = taDIListBeforeOpen
    OnNewRecord = taDIListNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 488
    Top = 100
    object taDIListINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'DLIST_S.INVID'
    end
    object taDIListDOCNO: TIntegerField
      DisplayLabel = #8470' '#1085#1072#1082#1083'.'
      FieldName = 'DOCNO'
      Origin = 'DLIST_S.DOCNO'
    end
    object taDIListDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
      FieldName = 'DOCDATE'
      Origin = 'DLIST_S.DOCDATE'
      OnChange = taDIListDOCDATEChange
    end
    object taDIListITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'DLIST_S.ITYPE'
    end
    object taDIListDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'DLIST_S.DEPID'
    end
    object taDIListDEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076' - '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
      FieldName = 'DEPNAME'
      Origin = 'DLIST_S.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taDIListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'DLIST_S.USERID'
    end
    object taDIListNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'DLIST_S.NDSID'
    end
    object taDIListFIO: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'FIO'
      Origin = 'DLIST_S.FIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taDIListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'DLIST_S.ISCLOSE'
    end
    object taDIListREF: TIntegerField
      FieldName = 'REF'
      Origin = 'DLIST_S.REF'
    end
    object taDIListFROMDEPID: TIntegerField
      FieldName = 'FROMDEPID'
      Origin = 'DLIST_S.FROMDEPID'
    end
    object taDIListFROMDEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076' - '#1086#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
      FieldName = 'FROMDEPNAME'
      Origin = 'DLIST_S.FROMDEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taDIListQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'DLIST_S.Q'
    end
    object taDIListW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'DLIST_S.W'
    end
    object taDIListCOST: TFloatField
      FieldName = 'COST'
      Origin = 'DLIST_S.COST'
    end
    object taDIListJOBID: TIntegerField
      FieldName = 'JOBID'
      Origin = 'DLIST_S.JOBID'
    end
    object taDIListJOBNAME: TFIBStringField
      DisplayLabel = #1055#1088#1080#1085#1103#1083
      FieldName = 'JOBNAME'
      Origin = 'DLIST_S.JOBNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taDIListINVCOLORID: TFIBIntegerField
      FieldName = 'INVCOLORID'
    end
    object taDIListCOLOR: TFIBIntegerField
      FieldName = 'COLOR'
    end
  end
  object taDIEl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SIEl set'
      '  SEMIS=:SEMIS,'
      '  Q=:Q,'
      '  W=:W,'
      '  NDS=:NDS,'
      '  TPRICE=:TPRICE,'
      '  COST=:COST,'
      '  OPERID=:OPERID,'
      '  REF=:REF,'
      '  FROMOPERID=:FROMOPERID '
      'where ID=:OLD_ID')
    DeleteSQL.Strings = (
      'delete from SIEl where ID=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into SIEl(ID, INVID, SEMIS, Q,  W, TPRICE, PRICE, NDSID, ' +
        'NDS, T, COST, OPERID, REF, FROMOPERID)'
      
        'values (:ID, :INVID, :SEMIS, :Q, :W, :TPRICE, :PRICE, :NDSID, :N' +
        'DS, :T, 0, :OPERID, :REF, :FROMOPERID)'
      '')
    RefreshSQL.Strings = (
      
        'select se.ID, se.INVID, se.SEMIS,  se.Q, se.W, se.TPRICE, se.PRI' +
        'CE,                se.NDSID, se.NDS, se.T, se.COST, se.OPERID, s' +
        'e.UQ, se.UW,'
      
        '        o.OPERATION OPERNAME, se.REF, o1.OPERATION FROMOPERNAME,' +
        ' se.FROMOPERID,'
      '        se.GOOD'
      'from SIEl se left join D_Oper o on (se.OPERID=o.OPERID)'
      
        '                    left join D_Oper o1 on(se.FROMOPERID=o1.OPER' +
        'ID),'
      '        D_Nds nds       '
      'where se.ID=:ID and'
      '          se.NDSID=nds.NDSID')
    SelectSQL.Strings = (
      'select '
      '  se.ID, se.INVID, se.SEMIS,  se.Q, se.W, se.TPRICE, se.PRICE,'
      '  se.NDSID, se.NDS, se.T, se.COST, se.OPERID, se.UQ, se.UW,'
      '  o.OPERATION OPERNAME, se.REF, o1.OPERATION FROMOPERNAME, '
      '  se.FROMOPERID, se.GOOD'
      'from '
      '  SIEl se '
      '  left join D_Oper o on (se.OPERID=o.OPERID)'
      '  left join D_Oper o1 on(se.FROMOPERID=o1.OPERID)        '
      '  left outer join D_Nds nds on (se.NDSID=nds.NDSID)'
      'where '
      '  se.INVID=:INVID'
      'order by se.ID')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterOpen = taDIElAfterOpen
    AfterPost = CommitRetaininig
    AfterScroll = taDIElAfterScroll
    BeforeClose = PostBeforeClose
    BeforeEdit = DICheckClose
    BeforeInsert = DICheckClose
    BeforeOpen = taDIElBeforeOpen
    OnNewRecord = taDIElNewRecord
    OnPostError = taDIElPostError
    Transaction = dm.tr
    Database = dm.db
    Left = 544
    Top = 100
    object taDIElID: TIntegerField
      FieldName = 'ID'
      Origin = 'SIEL.ID'
      Required = True
    end
    object taDIElINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SIEL.INVID'
      Required = True
    end
    object taDIElSEMIS: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMIS'
      Origin = 'SIEL.SEMIS'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taDIElQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'SIEL.Q'
      Required = True
    end
    object taDIElW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SIEL.W'
      Required = True
    end
    object taDIElTPRICE: TFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'TPRICE'
      Origin = 'SIEL.TPRICE'
      Required = True
    end
    object taDIElNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SIEL.NDSID'
      Required = True
    end
    object taDIElPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'SIEL.PRICE'
      Required = True
    end
    object taDIElNDS: TFloatField
      DisplayLabel = #1053#1044#1057
      FieldName = 'NDS'
      Origin = 'SIEL.NDS'
      Required = True
    end
    object taDIElT: TSmallintField
      FieldName = 'T'
      Origin = 'SIEL.T'
    end
    object taDIElCOST: TFloatField
      FieldName = 'COST'
      Origin = 'SIEL.COST'
      Required = True
    end
    object taDIElOPERID: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERID'
      Origin = 'SIEL.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taDIElSEMISNAME: TStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldKind = fkLookup
      FieldName = 'SEMISNAME'
      LookupDataSet = dm.taSemis
      LookupKeyFields = 'SEMISID'
      LookupResultField = 'SEMIS'
      KeyFields = 'SEMIS'
      Size = 60
      Lookup = True
    end
    object taDIElUQ: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'. '#1082#1086#1083'-'#1074#1072
      FieldName = 'UQ'
      Origin = 'SIEL.UQ'
      Required = True
      OnGetText = UQGetText
    end
    object taDIElUW: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'. '#1074#1077#1089#1072
      FieldName = 'UW'
      Origin = 'SIEL.UW'
      Required = True
      OnGetText = UWGetText
    end
    object taDIElOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'D_OPER.OPERATION'
      OnGetText = taDIElOPERNAMEGetText
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taDIElREF: TIntegerField
      FieldName = 'REF'
      Origin = 'SIEL.REF'
    end
    object taDIElFROMOPERNAME: TFIBStringField
      DisplayLabel = #1057' '#1086#1087#1077#1088#1072#1094#1080#1080
      FieldName = 'FROMOPERNAME'
      Origin = 'D_OPER.OPERATION'
      OnGetText = taDIElFROMOPERNAMEGetText
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taDIElFROMOPERID: TFIBStringField
      FieldName = 'FROMOPERID'
      Origin = 'SIEL.FROMOPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taDIElGOOD: TSmallintField
      FieldName = 'GOOD'
      Origin = 'SIEL.GOOD'
    end
  end
  object dsrDIList: TDataSource
    DataSet = taDIList
    Left = 488
    Top = 144
  end
  object dsrDIEl: TDataSource
    DataSet = taDIEl
    Left = 542
    Top = 144
  end
  object taDWHSemis: TpFIBDataSet
    InsertSQL.Strings = (
      'execute procedure F_Insert(:ID)')
    RefreshSQL.Strings = (
      
        'SELECT ID, DEPFROMID, DEPFROMNAME, DEPID, SEMISID, SEMISNAME, GO' +
        'OD, MATID,'
      '    MATNAME, OPERID, OPERNAME, Q_FROM, W_FROM,'
      '    Q, W, UQ, UW'
      'from WHSEMIS_R2(:ID, :DEPID, :DEPID)')
    SelectSQL.Strings = (
      
        'SELECT ID, DEPFROMID, DEPFROMNAME, DEPID, SEMISID, SEMISNAME, GO' +
        'OD, '
      '    MATID, MATNAME, OPERID, OPERNAME, Q_FROM, W_FROM,'
      '    Q, W, UQ, UW'
      'from WHSEMIS_S2(:DEPID1, :DEPID2,  :DEPFROMID1, '
      
        '       :DEPFROMID2,  :OPERID1, :OPERID2,  :SEMISID1, :SEMISID2, ' +
        '       :MATID1, :MATID2)')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeOpen = taDWHSemisBeforeOpen
    OnNewRecord = taDWHSemisNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 744
    Top = 92
    object taDWHSemisID: TIntegerField
      FieldName = 'ID'
      Origin = 'WHSEMIS_S2.ID'
    end
    object taDWHSemisSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'WHSEMIS_S2.SEMISID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taDWHSemisSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'WHSEMIS_S2.SEMISNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taDWHSemisMATID: TFIBStringField
      FieldName = 'MATID'
      Origin = 'WHSEMIS_S2.MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taDWHSemisMATNAME: TFIBStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldName = 'MATNAME'
      Origin = 'WHSEMIS_S2.MATNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taDWHSemisOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'WHSEMIS_S2.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taDWHSemisOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'WHSEMIS_S2.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taDWHSemisQ_FROM: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q_FROM'
      Origin = 'WHSEMIS_S2.Q_FROM'
    end
    object taDWHSemisW_FROM: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W_FROM'
      Origin = 'WHSEMIS_S2.W_FROM'
      DisplayFormat = '0.###'
    end
    object taDWHSemisQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
      FieldName = 'Q'
      Origin = 'WHSEMIS_S2.Q'
    end
    object taDWHSemisW: TFloatField
      DisplayLabel = #1042#1077#1089' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1100
      FieldName = 'W'
      Origin = 'WHSEMIS_S2.W'
      DisplayFormat = '0.###'
    end
    object taDWHSemisUQ: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079'. '#1082#1086#1083'-'#1074#1072
      FieldName = 'UQ'
      Origin = 'WHSEMIS_S2.UQ'
      OnGetText = UQGetText
    end
    object taDWHSemisUW: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079'. '#1074#1077#1089#1072
      FieldName = 'UW'
      Origin = 'WHSEMIS_S2.UW'
      OnGetText = UWGetText
    end
    object taDWHSemisDEPFROMID: TIntegerField
      FieldName = 'DEPFROMID'
      Origin = 'WHSEMIS_S2.DEPFROMID'
    end
    object taDWHSemisDEPFROMNAME: TFIBStringField
      FieldName = 'DEPFROMNAME'
      Origin = 'WHSEMIS_S2.DEPFROMNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taDWHSemisDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'WHSEMIS_S2.DEPID'
    end
    object taDWHSemisGOOD: TSmallintField
      FieldName = 'GOOD'
      Origin = 'WHSEMIS_S2.GOOD'
    end
  end
  object dsrDWHSemis: TDataSource
    DataSet = taDWHSemis
    Left = 744
    Top = 136
  end
  object taHWhArt: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, USERID, ITYPE, ARTID, ART2ID, SZID, U, OPERID,'
      '    DEPID, DEPFROMID, OLD_U, OLD_OPERID, OLD_ART2ID,'
      '    ART, ART2, SZNAME, OPERNAME, DEPNAME, '
      '    DEPFROMNAME, DOCID, DOCNO, DOCDATE, WINVID,'
      '    WINVNO, WINVDATE, WINVSEMIS, WORDERID, WORDERNO,'
      '    WORDERDATE, Q'
      'from hWhArt '
      'where SZID between :SZID1 and :SZID2 and'
      '           USERID=:USERID and'
      '           ART2ID=:ART2ID and'
      '           Q<>0 and'
      '           T2>0'
      'order by DOCDATE, T2, WINVDATE')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taHWhArtBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    OnFilterRecord = taHWhArtFilterRecord
    Left = 812
    Top = 92
    object taHWhArtID: TIntegerField
      FieldName = 'ID'
      Origin = 'HWHART.ID'
      Required = True
    end
    object taHWhArtUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'HWHART.USERID'
      Required = True
    end
    object taHWhArtITYPE: TSmallintField
      DisplayLabel = #1058#1080#1087' '#1076#1074#1080#1078#1077#1085#1080#1103
      FieldName = 'ITYPE'
      Origin = 'HWHART.ITYPE'
      Required = True
      OnGetText = taHWhArtITYPEGetText
    end
    object taHWhArtARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'HWHART.ARTID'
      Required = True
    end
    object taHWhArtART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'HWHART.ART2ID'
      Required = True
    end
    object taHWhArtSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'HWHART.SZID'
      Required = True
    end
    object taHWhArtU: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'HWHART.U'
      Required = True
      OnGetText = UGetText
    end
    object taHWhArtOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'HWHART.OPERID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taHWhArtDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'HWHART.DEPID'
      Required = True
    end
    object taHWhArtDEPFROMID: TIntegerField
      FieldName = 'DEPFROMID'
      Origin = 'HWHART.DEPFROMID'
    end
    object taHWhArtOLD_U: TSmallintField
      FieldName = 'OLD_U'
      Origin = 'HWHART.OLD_U'
    end
    object taHWhArtOLD_OPERID: TFIBStringField
      FieldName = 'OLD_OPERID'
      Origin = 'HWHART.OLD_OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taHWhArtOLD_ART2ID: TIntegerField
      FieldName = 'OLD_ART2ID'
      Origin = 'HWHART.OLD_ART2ID'
    end
    object taHWhArtART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'HWHART.ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taHWhArtART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'HWHART.ART2'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taHWhArtSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      Origin = 'HWHART.SZNAME'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taHWhArtOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'HWHART.OPERNAME'
      Required = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taHWhArtDEPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
      FieldName = 'DEPNAME'
      Origin = 'HWHART.DEPNAME'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taHWhArtDEPFROMNAME: TFIBStringField
      DisplayLabel = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
      FieldName = 'DEPFROMNAME'
      Origin = 'HWHART.DEPFROMNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taHWhArtDOCID: TIntegerField
      FieldName = 'DOCID'
      Origin = 'HWHART.DOCID'
      Required = True
    end
    object taHWhArtDOCNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1085#1072#1082#1083'. '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      FieldName = 'DOCNO'
      Origin = 'HWHART.DOCNO'
    end
    object taHWhArtDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      FieldName = 'DOCDATE'
      Origin = 'HWHART.DOCDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taHWhArtWINVID: TIntegerField
      FieldName = 'WINVID'
      Origin = 'HWHART.WINVID'
    end
    object taHWhArtWINVNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1085#1072#1082#1083'. '#1085#1072#1088#1103#1076#1072
      FieldName = 'WINVNO'
      Origin = 'HWHART.WINVNO'
    end
    object taHWhArtWINVDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'. '#1085#1072#1088#1103#1076#1072
      FieldName = 'WINVDATE'
      Origin = 'HWHART.WINVDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taHWhArtWINVSEMIS: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091'-'#1090' '#1085#1072#1082#1083'. '#1085#1072#1088#1103#1076#1072
      FieldName = 'WINVSEMIS'
      Origin = 'HWHART.WINVSEMIS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taHWhArtWORDERID: TIntegerField
      FieldName = 'WORDERID'
      Origin = 'HWHART.WORDERID'
    end
    object taHWhArtWORDERNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1085#1072#1088#1103#1076#1072'/'#1072#1082#1090#1072
      FieldName = 'WORDERNO'
      Origin = 'HWHART.WORDERNO'
    end
    object taHWhArtWORDERDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1088#1103#1076#1072'/'#1072#1082#1090#1072
      FieldName = 'WORDERDATE'
      Origin = 'HWHART.WORDERDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taHWhArtQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'HWHART.Q'
      Required = True
    end
  end
  object dsrHWhArt: TDataSource
    DataSet = taHWhArt
    Left = 812
    Top = 136
  end
  object taABuffer: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, USERID, PSID, ART2ID, SZID, OPERID, U, Q, ARTID'
      'from B_AInv'
      'where USERID=:USERID')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeOpen = taABufferBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 712
    Top = 4
    object taABufferID: TIntegerField
      FieldName = 'ID'
      Origin = 'B_AINV.ID'
      Required = True
    end
    object taABufferUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'B_AINV.USERID'
      Required = True
    end
    object taABufferPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'B_AINV.PSID'
      Required = True
    end
    object taABufferART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'B_AINV.ART2ID'
      Required = True
    end
    object taABufferSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'B_AINV.SZID'
      Required = True
    end
    object taABufferOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'B_AINV.OPERID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taABufferU: TSmallintField
      FieldName = 'U'
      Origin = 'B_AINV.U'
      Required = True
    end
    object taABufferQ: TIntegerField
      FieldName = 'Q'
      Origin = 'B_AINV.Q'
      Required = True
    end
    object taABufferARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'B_AINV.ARTID'
      Required = True
    end
  end
  object taTmp_WhArt: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select  ITYPE, ART, ART2, SZNAME, U, OPERNAME, DEPID, DEPNAME,  ' +
        '          sum(Q) Q'
      'from hWhArt h '
      'where h.USERID=:USERID and'
      '           h.ART2ID=:ART2ID and                      '
      '           h.SZID between :SZID1 and :SZID2 and'
      '           h.T2 in (-1, -2)'
      'group by ITYPE, ART, ART2, SZNAME, U, OPERNAME, DEPID, DEPNAME'
      'order by ART, ART2, SZNAME, U'
      '')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taTmp_WhArtBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 884
    Top = 92
    object taTmp_WhArtITYPE: TSmallintField
      DisplayLabel = #1058#1080#1087' '#1076#1074#1080#1078#1077#1085#1080#1103
      FieldName = 'ITYPE'
      Required = True
      OnGetText = taHWhArtITYPEGetText
    end
    object taTmp_WhArtART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taTmp_WhArtART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taTmp_WhArtSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taTmp_WhArtU: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'U'
      Required = True
      OnGetText = UGetText
    end
    object taTmp_WhArtOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Required = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taTmp_WhArtDEPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
      FieldName = 'DEPNAME'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taTmp_WhArtDEPID: TIntegerField
      FieldName = 'DEPID'
      Required = True
    end
    object taTmp_WhArtQ: TFIBBCDField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Size = 0
      RoundByScale = True
    end
  end
  object dsrTmp_WhArt: TDataSource
    DataSet = taTmp_WhArt
    Left = 885
    Top = 136
  end
  object taSBuffer: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from B_WINV where ID=:OLD_ID')
    SelectSQL.Strings = (
      'select b.ID, b.USERID, b.DEPID, b.SEMIS, b.OPERID, b.Q, b.W, '
      '         s.GOOD,  s.SEMIS SEMISNAME'
      'from B_WINV b, D_Semis s'
      'where b.USERID=:USERID and'
      '           b.SEMIS=s.SEMISID')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeOpen = taSBufferBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 712
    Top = 48
    object taSBufferID: TIntegerField
      FieldName = 'ID'
      Origin = 'B_WINV.ID'
      Required = True
    end
    object taSBufferUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'B_WINV.USERID'
      Required = True
    end
    object taSBufferDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'B_WINV.DEPID'
      Required = True
    end
    object taSBufferSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Origin = 'B_WINV.SEMIS'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSBufferOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'B_WINV.OPERID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSBufferQ: TFloatField
      FieldName = 'Q'
      Origin = 'B_WINV.Q'
      Required = True
    end
    object taSBufferW: TFloatField
      FieldName = 'W'
      Origin = 'B_WINV.W'
      Required = True
    end
    object taSBufferGOOD: TSmallintField
      FieldName = 'GOOD'
      Origin = 'D_SEMIS.GOOD'
      Required = True
    end
    object taSBufferSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'D_SEMIS.SEMIS'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object taSemisT1: TpFIBDataSet
    SelectSQL.Strings = (
      'select SEMISID, SEMIS'
      'from D_Semis'
      'where bit(Types,0)=1')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 884
    Top = 4
    object taSemisT1SEMISID: TFIBStringField
      DisplayLabel = #1050#1086#1076
      FieldName = 'SEMISID'
      Origin = 'D_SEMIS.SEMISID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSemisT1SEMIS: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMIS'
      Origin = 'D_SEMIS.SEMIS'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrSemisT1: TDataSource
    DataSet = taSemisT1
    Left = 884
    Top = 48
  end
  object taAssortInv: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Inv set'
      '  DOCDATE=:DOCDATE'
      'where INVID=:AINV')
    SelectSQL.Strings = (
      'select DOCNO, DOCDATE, AINV'
      'from AInv_S1(:WOITEMID)')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterOpen = taAssortInvAfterOpen
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeOpen = taAssortInvBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 156
    Top = 109
    object taAssortInvDOCNO: TFIBStringField
      FieldName = 'DOCNO'
      Origin = 'AINV_S1.DOCNO'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taAssortInvDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'AINV_S1.DOCDATE'
    end
    object taAssortInvAINV: TFIBIntegerField
      FieldName = 'AINV'
    end
  end
  object taAssortEl: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      '  OPERID, OPERATION, OPERIDFROM, OPERATIONFROM,'
      '  ART2ID_OLD, ART2_OLD, U, U_OLD, Q1'
      'from AEl_S1(:WOITEMID)'
      'order by ART, ART2, SZ')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taAssortElBeforeOpen
    OnCalcFields = CalcRecNo
    Transaction = dm.tr
    Database = dm.db
    Left = 212
    Top = 108
    object taAssortElRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taAssortElID: TIntegerField
      FieldName = 'ID'
      Origin = 'AEL_S1.ID'
    end
    object taAssortElINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'AEL_S1.INVID'
    end
    object taAssortElARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'AEL_S1.ARTID'
    end
    object taAssortElART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'AEL_S1.ART2ID'
    end
    object taAssortElSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'AEL_S1.SZID'
    end
    object taAssortElART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'AEL_S1.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taAssortElART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'AEL_S1.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taAssortElSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'AEL_S1.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taAssortElQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'AEL_S1.Q'
    end
    object taAssortElT: TSmallintField
      FieldName = 'T'
      Origin = 'AEL_S1.T'
    end
    object taAssortElOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AEL_S1.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAssortElOPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Origin = 'AEL_S1.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAssortElOPERIDFROM: TFIBStringField
      FieldName = 'OPERIDFROM'
      Origin = 'AEL_S1.OPERIDFROM'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAssortElOPERATIONFROM: TFIBStringField
      FieldName = 'OPERATIONFROM'
      Origin = 'AEL_S1.OPERATIONFROM'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAssortElART2ID_OLD: TIntegerField
      FieldName = 'ART2ID_OLD'
      Origin = 'AEL_S1.ART2ID_OLD'
    end
    object taAssortElART2_OLD: TFIBStringField
      FieldName = 'ART2_OLD'
      Origin = 'AEL_S1.ART2_OLD'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taAssortElU: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'AEL_S1.U'
      OnGetText = UGetText
    end
    object taAssortElU_OLD: TSmallintField
      FieldName = 'U_OLD'
      Origin = 'AEL_S1.U_OLD'
    end
    object taAssortElQ1: TIntegerField
      FieldName = 'Q1'
      Origin = 'AEL_S1.Q1'
    end
  end
  object dsrAssortInv: TDataSource
    DataSet = taAssortInv
    Left = 160
    Top = 154
  end
  object dsrAssortEl: TDataSource
    DataSet = taAssortEl
    Left = 207
    Top = 153
  end
  object taSOList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Inv set'
      '  DOCNO=:DOCNO, '
      '  DOCDATE=:DOCDATE, '
      '  USERID=:USERID,'
      '  SUPID=:SUPID, '
      '  SSF=:SSF, '
      '  SSFDT=:SSFDT,'
      '  ISCLOSE=:ISCLOSE, '
      '  MEMO=:MEMO,'
      '  CONTRACTID=:CONTRACTID,'
      '  INVSUBTYPE=:INVSUBTYPEID,'
      '  Reference$ID = :Reference$ID'
      'where InvId=:OLD_INVID')
    DeleteSQL.Strings = (
      'delete from Inv where InvId=:OLD_INVID')
    InsertSQL.Strings = (
      
        'insert into Inv(INVID, DOCNO, DOCDATE, ITYPE, DEPID, SUPID, USER' +
        'ID, '
      
        '     ISCLOSE, NDSID, SSF, SSFDT, MEMO, CONTRACTID, INVSUBTYPE, C' +
        'REATED, REFERENCE$ID)'
      
        'values (:INVID, :DOCNO, :DOCDATE, :ITYPE, :DEPID, :SUPID,:USERID' +
        ','
      
        '      0, :NDSID, :SSF, :SSFDT, :MEMO, :CONTRACTID, :INVSUBTYPEID' +
        ', '#39'Now'#39', 0)')
    RefreshSQL.Strings = (
      'select INVID, DOCNO, DOCDATE , ITYPE, DEPID, USERID,'
      '    SUPID,  FIO, SUPNAME,  ISCLOSE, DEPNAME,  NDSID, REF,'
      '    Q, SSF, SSFDT, W, COST, MEMO, CONTRACTID, CONTRACTNAME,'
      
        '    SCHEMAID, INVSUBTYPEID, INVSUBTYPENAME, W3, CREATED, REFEREN' +
        'CE$ID'
      'from SList_R2(:INVID)')
    SelectSQL.Strings = (
      'select INVID, DOCNO, DOCDATE , ITYPE, DEPID, USERID,'
      '    SUPID,  FIO, SUPNAME,  ISCLOSE, DEPNAME,  NDSID, REF,'
      '    Q, SSF, SSFDT, W, COST, MEMO, CONTRACTID, CONTRACTNAME,'
      
        '    SCHEMAID, INVSUBTYPEID, INVSUBTYPENAME, W3, SELFCOMPNAME, CR' +
        'EATED, REFERENCE$ID'
      'from SList_S(:DEPID1, :DEPID2, :BD, :ED,:ITYPE_)'
      
        'where (SUPNAME between :SUPNAME1 and :SUPNAME2) or (SUPNAME is n' +
        'ull)'
      'order by DocDate')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = taSOListAfterPost
    BeforeClose = PostBeforeClose
    BeforeDelete = taSOListBeforeDelete
    BeforeOpen = taSOListBeforeOpen
    OnDeleteError = taSOListDeleteError
    OnNewRecord = taSOListNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1057#1087#1080#1089#1086#1082' '#1085#1072#1082#1083#1072#1076#1085#1099#1093' '#1088#1072#1089#1093#1086#1076#1072' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
    Left = 500
    Top = 196
    object taSOListINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SLIST_S.INVID'
    end
    object taSOListDOCNO: TIntegerField
      DisplayLabel = #8470' '#1085#1072#1082#1083'.'
      FieldName = 'DOCNO'
      Origin = 'SLIST_S.DOCNO'
    end
    object taSOListDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
      FieldName = 'DOCDATE'
      Origin = 'SLIST_S.DOCDATE'
      OnChange = taSOListDOCDATEChange
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taSOListITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'SLIST_S.ITYPE'
    end
    object taSOListDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SLIST_S.DEPID'
    end
    object taSOListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'SLIST_S.USERID'
    end
    object taSOListSUPID: TIntegerField
      FieldName = 'SUPID'
      Origin = 'SLIST_S.SUPID'
    end
    object taSOListFIO: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'FIO'
      Origin = 'SLIST_S.FIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSOListSUPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
      FieldName = 'SUPNAME'
      Origin = 'SLIST_S.SUPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSOListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'SLIST_S.ISCLOSE'
    end
    object taSOListDEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'DEPNAME'
      Origin = 'SLIST_S.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSOListNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SLIST_S.NDSID'
    end
    object taSOListREF: TIntegerField
      FieldName = 'REF'
      Origin = 'SLIST_S.REF'
    end
    object taSOListQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'SLIST_S.Q'
    end
    object taSOListSSF: TFIBStringField
      FieldName = 'SSF'
      Origin = 'SLIST_S.SSF'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSOListSSFDT: TDateTimeField
      FieldName = 'SSFDT'
      Origin = 'SLIST_S.SSFDT'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object taSOListW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SLIST_S.W'
    end
    object taSOListCOST: TFloatField
      FieldName = 'COST'
      Origin = 'SLIST_S.COST'
    end
    object taSOListMEMO: TMemoField
      FieldName = 'MEMO'
      Origin = 'SLIST_S.MEMO'
      BlobType = ftMemo
      Size = 8
    end
    object taSOListCONTRACTID: TFIBIntegerField
      FieldName = 'CONTRACTID'
    end
    object taSOListCONTRACTNAME: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1076#1086#1075#1086#1074#1086#1088#1072
      FieldName = 'CONTRACTNAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taSOListSCHEMAID: TFIBIntegerField
      FieldName = 'SCHEMAID'
    end
    object taSOListINVSUBTYPEID: TFIBIntegerField
      FieldName = 'INVSUBTYPEID'
    end
    object taSOListINVSUBTYPENAME: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1088#1072#1089#1093#1086#1076#1072
      FieldName = 'INVSUBTYPENAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taSOListW3: TFIBFloatField
      FieldName = 'W3'
    end
    object taSOListSELFCOMPNAME: TFIBStringField
      FieldName = 'SELFCOMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taSOListCREATED: TFIBDateTimeField
      FieldName = 'CREATED'
    end
    object taSOListREFERENCEID: TFIBIntegerField
      FieldName = 'REFERENCE$ID'
    end
  end
  object dsrSOList: TDataSource
    DataSet = taSOList
    Left = 500
    Top = 240
  end
  object dsrSOItem: TDataSource
    DataSet = taSOEl
    Left = 560
    Top = 240
  end
  object taTmpWhSemis: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, DEPID, DEPNAME, SEMISID, SEMISNAME,'
      '    MATID, MATNAME, OPERID, OPERNAME, Q, W, UQ, UW'
      'from WhSemis_S3(:DEPID1, :DEPID2, :OPERID1, :OPERID2, :SEMISID1,'
      '  :SEMISID2, :MATID1, :MATID2)'
      'order by SEMISNAME')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeOpen = taWhSBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 452
    Top = 304
    object taTmpWhSemisID: TIntegerField
      FieldName = 'ID'
      Origin = 'WHSEMIS_S3.ID'
    end
    object taTmpWhSemisDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'WHSEMIS_S3.DEPID'
    end
    object taTmpWhSemisDEPNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'DEPNAME'
      Origin = 'WHSEMIS_S3.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taTmpWhSemisSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'WHSEMIS_S3.SEMISID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taTmpWhSemisSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'WHSEMIS_S3.SEMISNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taTmpWhSemisMATID: TFIBStringField
      FieldName = 'MATID'
      Origin = 'WHSEMIS_S3.MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taTmpWhSemisMATNAME: TFIBStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldName = 'MATNAME'
      Origin = 'WHSEMIS_S3.MATNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taTmpWhSemisOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'WHSEMIS_S3.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taTmpWhSemisOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'WHSEMIS_S3.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taTmpWhSemisQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'WHSEMIS_S3.Q'
    end
    object taTmpWhSemisW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'WHSEMIS_S3.W'
    end
    object taTmpWhSemisUQ: TSmallintField
      FieldName = 'UQ'
      Origin = 'WHSEMIS_S3.UQ'
      OnGetText = UQGetText
    end
    object taTmpWhSemisUW: TSmallintField
      FieldName = 'UW'
      Origin = 'WHSEMIS_S3.UW'
      OnGetText = UWGetText
    end
  end
  object dsrTmpWhSemis: TDataSource
    DataSet = taTmpWhSemis
    Left = 452
    Top = 352
  end
  object taProtSJInfo: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.DOCNO INVNO, o.DOCNO WORDERNO,'
      '          it.ITDATE, s.SEMIS SEMISNAME, it.W, it.Q,'
      '          it.ITTYPE, d.NAME WHNAME'
      'from Worder o, Inv i, woItem it, D_Semis s, D_Dep d'
      'where o.WORDERID = i.WORDERID and'
      '        i.INVID = it.INVID and'
      '        i.DEPID=:PSID and'
      '        it.ITDATE between :BD and :ED and'
      '        it.OPERID=:OPERID and'
      '        it.E = 0 and'
      '        it.SEMIS=s.SEMISID and'
      '        s.SEMISID = :SEMISID and'
      '        i.FROMDEPID=d.DEPID'
      '        ')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taProtSJInfoBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 296
    Top = 200
    object taProtSJInfoINVNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      FieldName = 'INVNO'
      Origin = 'INV.DOCNO'
    end
    object taProtSJInfoWORDERNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1085#1072#1088#1103#1076#1072
      FieldName = 'WORDERNO'
      Origin = 'WORDER.DOCNO'
    end
    object taProtSJInfoITDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
      FieldName = 'ITDATE'
      Origin = 'WOITEM.ITDATE'
      Required = True
    end
    object taProtSJInfoSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'D_SEMIS.SEMIS'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taProtSJInfoW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'WOITEM.W'
      Required = True
      DisplayFormat = '0.###'
    end
    object taProtSJInfoQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'WOITEM.Q'
      Required = True
    end
    object taProtSJInfoITTYPE: TSmallintField
      DisplayLabel = #1058#1080#1087' '#1087#1077#1088#1077#1076#1072#1095#1080
      FieldName = 'ITTYPE'
      Origin = 'WOITEM.ITTYPE'
      Required = True
      OnGetText = ITTYPEGetText
    end
    object taProtSJInfoWHNAME: TFIBStringField
      DisplayLabel = #1050#1083#1072#1076#1086#1074#1072#1103
      FieldName = 'WHNAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrProtSJInfo: TDataSource
    DataSet = taProtSJInfo
    Left = 296
    Top = 245
  end
  object taProtWhItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select DOCNO, DOCDATE, DEPID, DEPNAME, DEPFROMID,'
      '    DEPFROMNAME, SEMISID, SEMISNAME, OPERID, OPERNAME,'
      '    UQ, UW, Q, W, T, T1, FROMOPERID, FROMOPERNAME,'
      '    WORDERNO, WORDERDATE'
      
        'from Semis_Report_Detail(:ACTID, :DEPID, :SEMISID, :OPERID, :UW,' +
        ' :UQ)'
      'order by DOCDATE ')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taProtWhItemBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 636
    Top = 304
    object taProtWhItemDOCNO: TIntegerField
      DisplayLabel = #8470' '#1085#1072#1082#1083'.'
      FieldName = 'DOCNO'
      Origin = 'SEMIS_REPORT_DETAIL.DOCNO'
    end
    object taProtWhItemDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
      FieldName = 'DOCDATE'
      Origin = 'SEMIS_REPORT_DETAIL.DOCDATE'
    end
    object taProtWhItemDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SEMIS_REPORT_DETAIL.DEPID'
    end
    object taProtWhItemDEPNAME: TFIBStringField
      DisplayLabel = #1050#1083#1072#1076#1086#1074#1072#1103
      FieldName = 'DEPNAME'
      Origin = 'SEMIS_REPORT_DETAIL.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taProtWhItemDEPFROMID: TIntegerField
      FieldName = 'DEPFROMID'
      Origin = 'SEMIS_REPORT_DETAIL.DEPFROMID'
    end
    object taProtWhItemDEPFROMNAME: TFIBStringField
      DisplayLabel = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      FieldName = 'DEPFROMNAME'
      Origin = 'SEMIS_REPORT_DETAIL.DEPFROMNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taProtWhItemSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'SEMIS_REPORT_DETAIL.SEMISID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taProtWhItemSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'SEMIS_REPORT_DETAIL.SEMISNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taProtWhItemOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'SEMIS_REPORT_DETAIL.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taProtWhItemOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'SEMIS_REPORT_DETAIL.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taProtWhItemUQ: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'. '#1082#1086#1083'-'#1074#1072
      FieldName = 'UQ'
      Origin = 'SEMIS_REPORT_DETAIL.UQ'
      OnGetText = UQGetText
    end
    object taProtWhItemUW: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'. '#1074#1077#1089#1072
      FieldName = 'UW'
      Origin = 'SEMIS_REPORT_DETAIL.UW'
      OnGetText = UWGetText
    end
    object taProtWhItemQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'SEMIS_REPORT_DETAIL.Q'
    end
    object taProtWhItemW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SEMIS_REPORT_DETAIL.W'
      DisplayFormat = '0.###'
    end
    object taProtWhItemT: TSmallintField
      DisplayLabel = #1054#1087#1080#1089'.'
      FieldName = 'T'
      Origin = 'SEMIS_REPORT_DETAIL.T'
      OnGetText = taProtWhItemTGetText
    end
    object taProtWhItemT1: TSmallintField
      DisplayLabel = #1054#1087#1080#1089'2'
      FieldName = 'T1'
      Origin = 'SEMIS_REPORT_DETAIL.T1'
      OnGetText = taProtWhItemT1GetText
    end
    object taProtWhItemFROMOPERID: TFIBStringField
      FieldName = 'FROMOPERID'
      Origin = 'SEMIS_REPORT_DETAIL.FROMOPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taProtWhItemFROMOPERNAME: TFIBStringField
      DisplayLabel = #1057' '#1086#1087#1077#1088#1072#1094#1080#1080
      FieldName = 'FROMOPERNAME'
      Origin = 'SEMIS_REPORT_DETAIL.FROMOPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taProtWhItemWORDERNO: TIntegerField
      DisplayLabel = #8470' '#1085#1072#1088#1103#1076#1072
      FieldName = 'WORDERNO'
      Origin = 'SEMIS_REPORT_DETAIL.WORDERNO'
    end
    object taProtWhItemWORDERDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1088#1103#1076#1072
      FieldName = 'WORDERDATE'
      Origin = 'SEMIS_REPORT_DETAIL.WORDERDATE'
    end
  end
  object dsrProtWHItem: TDataSource
    DataSet = taProtWhItem
    Left = 636
    Top = 352
  end
  object taVList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Act set'
      '  DOCNO=:DOCNO,'
      '  DOCDATE=:DOCDATE,'
      '  BD=:BD,'
      '  ISCLOSE=:ISCLOSE,'
      '  A=:A,'
      '  CREATED=:CREATED'
      'where ID=:OLD_ID')
    DeleteSQL.Strings = (
      'delete from Act where Id=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into Act(ID, DOCNO, DOCDATE, USERID, BD, ISCLOSE, T, DEPI' +
        'D, A, BUH, CREATED)'
      
        'values(:ID, :DOCNO, :DOCDATE, :USERID, :BD, :ISCLOSE, :T, :DEPID' +
        ', :A, :BUH, :CREATED)')
    RefreshSQL.Strings = (
      'select ID, DOCNO, DOCDATE, USERID, BD, ED, ISCLOSE, T,      '
      '       MATID, USERNAME, DEPID, DEPNAME, A,'
      '       BUH, CREATED, REFID, REFDATE, REFNO, REFNAME '
      'from VList_R(:ID)')
    SelectSQL.Strings = (
      'select ID, DOCNO, DOCDATE, USERID, BD, ED, ISCLOSE, T,      '
      '  MATID, USERNAME, DEPID, DEPNAME, A,'
      '  BUH, CREATED, REFID, REFDATE, REFNO, REFNAME '
      'from VList_S(:BD, :ED)'
      'order by DOCNO')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterOpen = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taVListBeforeDelete
    BeforeOpen = taVListBeforeOpen
    OnNewRecord = taVListNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 520
    Top = 304
    object taVListID: TIntegerField
      FieldName = 'ID'
      Origin = 'ACT.ID'
      Required = True
    end
    object taVListDOCNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1085#1072#1082#1083'.'
      FieldName = 'DOCNO'
      Origin = 'ACT.DOCNO'
    end
    object taVListDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
      FieldName = 'DOCDATE'
      Origin = 'ACT.DOCDATE'
    end
    object taVListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'ACT.USERID'
      Required = True
    end
    object taVListBD: TDateTimeField
      DisplayLabel = #1053#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072
      FieldName = 'BD'
      Origin = 'ACT.BD'
    end
    object taVListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'ACT.ISCLOSE'
    end
    object taVListT: TSmallintField
      FieldName = 'T'
      Origin = 'ACT.T'
    end
    object taVListMATID: TFIBStringField
      FieldName = 'MATID'
      Origin = 'ACT.MATID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taVListUSERNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldKind = fkInternalCalc
      FieldName = 'USERNAME'
      Origin = 'D_MOL.FIO'
      ReadOnly = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taVListDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'ACT.DEPID'
    end
    object taVListDEPNAME: TFIBStringField
      DisplayLabel = #1050#1083#1072#1076#1086#1074#1072#1103
      FieldName = 'DEPNAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taVListA: TSmallintField
      FieldName = 'A'
      Origin = 'ACT.A'
    end
    object taVListBUH: TSmallintField
      FieldName = 'BUH'
      Origin = 'ACT.BUH'
    end
    object taVListCREATED: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103'/'#1086#1073#1085#1086#1074#1083#1077#1085#1080#1103
      FieldName = 'CREATED'
    end
    object taVListREFID: TFIBIntegerField
      FieldName = 'REFID'
    end
    object taVListREFDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'REFDATE'
    end
    object taVListREFNAME: TFIBStringField
      DisplayLabel = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      FieldName = 'REFNAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taVListREFNO: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088
      FieldName = 'REFNO'
    end
    object taVListED: TFIBDateTimeField
      DisplayLabel = #1050#1086#1085#1077#1094
      FieldName = 'ED'
    end
  end
  object dsrVList: TDataSource
    DataSet = taVList
    Left = 520
    Top = 352
  end
  object taPSMOL1: TpFIBDataSet
    SelectSQL.Strings = (
      'select ps.MolId, m.FIO MOLNAME,  ps.DepId '
      'from D_Ps ps, D_Mol m'
      'where ps.DEPID=:DEPID  and'
      '           ps.MOLID=m.MOLID')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taPSMOL1BeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 712
    Top = 300
  end
  object dsrPSMOL1: TDataSource
    DataSet = taPSMOL1
    Left = 712
    Top = 352
  end
  object taZ_Assembly: TpFIBDataSet
    SelectSQL.Strings = (
      'select d.NAME DEPNAME, o.OPERATION OPERNAME,'
      '         s.SEMIS SEMISNAME, z.Q, z.QC '
      'from Zp_Assembly z, D_Dep d, D_Oper o, D_Semis s'
      'where z.ZPID=:ZPID and'
      '          z.PSID=d.DEPID and'
      '          z.OPERID=o.OPERID and'
      '          z.SEMISID=s.SEMISID '
      'order by d.NAME')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taZ_AssemblyBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 128
    Top = 304
    object taZ_AssemblyDEPNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'DEPNAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taZ_AssemblyOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'D_OPER.OPERATION'
      Required = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taZ_AssemblySEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'D_SEMIS.SEMIS'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taZ_AssemblyQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'ZP_ASSEMBLY.Q'
    end
    object taZ_AssemblyQC: TIntegerField
      DisplayLabel = #1057#1083#1086#1078'.'
      FieldName = 'QC'
      Origin = 'ZP_ASSEMBLY.QC'
    end
  end
  object dsrZ_Assembly: TDataSource
    DataSet = taZ_Assembly
    Left = 128
    Top = 352
  end
  object taVArt: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AIn_Art set'
      '  FQ=:FQ,'
      '  SZID=:SZID,'
      '  ART2ID=:ART2ID,'
      '  ARTID=:ARTID'
      'where ID=:ID')
    DeleteSQL.Strings = (
      'delete from AIn_Art where ID=:ID')
    InsertSQL.Strings = (
      
        'execute procedure AIn_Art_I(:ID, :VITEMID, :DEPID, :ARTID, :ART2' +
        'ID, :SZID, :OPERID, :U,'
      '  :REST_IN_Q, :IN7_Q, :IN9_Q, :MOVE_IN_Q, :MOVE_OUT_Q, :OUT_Q, '
      '  :PROD_Q, :RET_PROD_Q, :Q, :FQ)')
    RefreshSQL.Strings = (
      
        'select it.ID, it.VITEMID, it.DEPID, it.ARTID, it.ART2ID, it.SZID' +
        ','
      '    it.OPERID, it.U, it.Q, it.FQ, o.OPERATION OPERNAME, '
      '    d.NAME DEPNAME, a2.ART, a2.ART2, sz.NAME SZNAME,'
      '    it.OUT_Q, it.PROD_Q, it.RET_PROD_Q, it.MOVE_IN_Q,'
      '    it.MOVE_OUT_Q, it.REST_IN_Q, it.IN7_Q, it.IN9_Q'
      'from AIn_Art it, D_Oper o, D_Dep d, Art2 a2, D_Sz sz'
      'where it.ID=:ID and'
      '           it.OPERID=o.OPERID and'
      '           it.DEPID=d.DEPID and '
      '           it.ART2ID=a2.ART2ID and'
      '           it.SZID=sz.ID')
    SelectSQL.Strings = (
      
        'select it.ID, it.VITEMID, it.DEPID, it.ARTID, it.ART2ID, it.SZID' +
        ','
      '    it.OPERID, it.U, it.Q, it.FQ, o.OPERATION OPERNAME, '
      '    d.NAME DEPNAME, a2.ART, a2.ART2, sz.NAME SZNAME,'
      '    it.OUT_Q, it.PROD_Q, it.RET_PROD_Q, it.MOVE_IN_Q,'
      '    it.MOVE_OUT_Q, it.REST_IN_Q, it.IN7_Q, it.IN9_Q'
      'from AIn_Art it, D_Oper o, D_Dep d, Art2 a2, D_Sz sz'
      'where it.VITEMID=:VITEMID and'
      '           it.OPERID=o.OPERID and'
      '           it.DEPID=d.DEPID and '
      '           it.ART2ID=a2.ART2ID and'
      '           it.SZID=sz.ID and'
      '           (not (it.Q=0 and it.FQ=0))'
      'order by a2.ART,  a2.ART2, sz.NAME'
      '           ')
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taVArtBeforeDelete
    BeforeOpen = taVArtBeforeOpen
    OnCalcFields = taVArtCalcFields
    OnNewRecord = taVArtNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 576
    Top = 304
    object taVArtID: TIntegerField
      FieldName = 'ID'
      Origin = 'AIN_ART.ID'
      Required = True
    end
    object taVArtVITEMID: TIntegerField
      FieldName = 'VITEMID'
      Origin = 'AIN_ART.VITEMID'
      Required = True
    end
    object taVArtDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'AIN_ART.DEPID'
      Required = True
    end
    object taVArtARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'AIN_ART.ARTID'
      Required = True
    end
    object taVArtART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'AIN_ART.ART2ID'
      Required = True
    end
    object taVArtOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AIN_ART.OPERID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taVArtU: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'AIN_ART.U'
      Required = True
      OnGetText = UGetText
      OnSetText = USetText
    end
    object taVArtQ: TIntegerField
      DisplayLabel = #1059#1095#1077#1090#1085#1086#1077' '#1082#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'AIN_ART.Q'
      Required = True
    end
    object taVArtFQ: TIntegerField
      DisplayLabel = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077' '#1082#1086#1083'-'#1074#1086
      FieldName = 'FQ'
      Origin = 'AIN_ART.FQ'
      Required = True
    end
    object taVArtOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'D_OPER.OPERATION'
      Required = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taVArtDEPNAME: TFIBStringField
      DisplayLabel = #1050#1083#1072#1076#1086#1074#1072#1103
      FieldName = 'DEPNAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taVArtART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'ART2.ART'
      Required = True
      OnValidate = taVArtARTValidate
      FixedChar = True
      EmptyStrToNull = True
    end
    object taVArtART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      Required = True
      OnValidate = taVArtART2Validate
      FixedChar = True
      EmptyStrToNull = True
    end
    object taVArtSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      Origin = 'D_SZ.NAME'
      Required = True
      OnValidate = taVArtSZNAMEValidate
      FixedChar = True
      EmptyStrToNull = True
    end
    object taVArtSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'AIN_ART.SZID'
      Required = True
    end
    object taVArtOUT_Q: TIntegerField
      DisplayLabel = #1057#1085#1103#1090#1086' '#1089' '#1088#1072#1073#1086#1090#1099
      FieldName = 'OUT_Q'
      Origin = 'AIN_ART.OUT_Q'
      Required = True
    end
    object taVArtPROD_Q: TIntegerField
      DisplayLabel = #1054#1090#1075#1088#1091#1078#1077#1085#1086' '#1075#1086#1090'. '#1087#1088#1086#1076#1091#1082#1094#1080#1103
      FieldName = 'PROD_Q'
      Origin = 'AIN_ART.PROD_Q'
      Required = True
    end
    object taVArtRET_PROD_Q: TIntegerField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1075#1086#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
      FieldName = 'RET_PROD_Q'
      Origin = 'AIN_ART.RET_PROD_Q'
      Required = True
    end
    object taVArtMOVE_IN_Q: TIntegerField
      DisplayLabel = #1042#1085#1091#1090#1088'. '#1087#1077#1088#1077#1084'. '#1042
      FieldName = 'MOVE_IN_Q'
      Origin = 'AIN_ART.MOVE_IN_Q'
      Required = True
    end
    object taVArtMOVE_OUT_Q: TIntegerField
      DisplayLabel = #1042#1085#1091#1090#1088'. '#1087#1077#1088#1077#1084'. '#1054#1090
      FieldName = 'MOVE_OUT_Q'
      Origin = 'AIN_ART.MOVE_OUT_Q'
      Required = True
    end
    object taVArtREST_IN_Q: TIntegerField
      DisplayLabel = #1042#1093#1086#1076#1103#1097#1080#1077
      FieldName = 'REST_IN_Q'
      Origin = 'AIN_ART.REST_IN_Q'
      Required = True
    end
    object taVArtIN7_Q: TIntegerField
      DisplayLabel = #1055#1088#1080#1085#1103#1090#1086' '#1074' '#1088#1072#1073#1086#1090#1091
      FieldName = 'IN7_Q'
      Origin = 'AIN_ART.IN7_Q'
      Required = True
    end
    object taVArtIN9_Q: TIntegerField
      DisplayLabel = #1055#1088#1080#1085#1103#1090#1086' '#1087#1086' '#1079#1072#1103#1074#1082#1077
      FieldName = 'IN9_Q'
      Origin = 'AIN_ART.IN9_Q'
      Required = True
    end
    object taVArtRQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RQ'
      Calculated = True
    end
  end
  object dsrVArt: TDataSource
    DataSet = taVArt
    Left = 576
    Top = 352
  end
  object taBulkList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Inv set'
      '  DOCNO=:DOCNO, '
      '  DOCDATE=:DOCDATE, '
      '  USERID=:USERID,'
      '  SUPID=:SUPID, '
      '  ISCLOSE=:ISCLOSE'
      'where InvId=:OLD_INVID')
    DeleteSQL.Strings = (
      'delete from Inv where InvId=:OLD_INVID')
    InsertSQL.Strings = (
      
        'insert into Inv(INVID, DOCNO, DOCDATE, ITYPE, DEPID, SUPID, USER' +
        'ID, '
      '   ISCLOSE)'
      
        'values (:INVID, :DOCNO, :DOCDATE, :ITYPE, :DEPID, :SUPID,:USERID' +
        ',  '
      '   0)')
    RefreshSQL.Strings = (
      
        'select SL.INVID, SL.DOCNO, SL.DOCDATE , SL.ITYPE, SL.DEPID,    S' +
        'L.USERID,'
      
        '   SL.SUPID,  SL.FIO, SL.SUPNAME,  SL.ISCLOSE, SL.DEPNAME,      ' +
        '   SL.NDSID, SL.REF,'
      '   SL.Q, SL.SSF, SL.SSFDT, SL.W, SL.COST, SL.REFNO'
      'from SList_R(:INVID) SL'
      '')
    SelectSQL.Strings = (
      
        'select SL.INVID, SL.DOCNO, SL.DOCDATE , SL.ITYPE, SL.DEPID,    S' +
        'L.USERID,'
      
        '   SL.SUPID,  SL.FIO, SL.SUPNAME,  SL.ISCLOSE, SL.DEPNAME,      ' +
        '   SL.NDSID, SL.REF,'
      '   SL.Q, SL.SSF, SL.SSFDT, SL.W, SL.COST, SL.REFNO'
      'from SList_S(:DEPID1, :DEPID2, :BD, :ED, :ITYPE_) SL'
      'order by DOCDATE')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taBulkListBeforeDelete
    BeforeOpen = taBulkListBeforeOpen
    OnNewRecord = taBulkListNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 16
    Top = 408
    object taBulkListINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SLIST_S.INVID'
    end
    object taBulkListDOCNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1072#1082#1090#1072
      FieldName = 'DOCNO'
      Origin = 'SLIST_S.DOCNO'
    end
    object taBulkListDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1072#1082#1090#1072
      FieldName = 'DOCDATE'
      Origin = 'SLIST_S.DOCDATE'
    end
    object taBulkListITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'SLIST_S.ITYPE'
    end
    object taBulkListDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SLIST_S.DEPID'
    end
    object taBulkListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'SLIST_S.USERID'
    end
    object taBulkListSUPID: TIntegerField
      FieldName = 'SUPID'
      Origin = 'SLIST_S.SUPID'
    end
    object taBulkListFIO: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'FIO'
      Origin = 'SLIST_S.FIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taBulkListSUPNAME: TFIBStringField
      FieldName = 'SUPNAME'
      Origin = 'SLIST_S.SUPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taBulkListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'SLIST_S.ISCLOSE'
    end
    object taBulkListDEPNAME: TFIBStringField
      DisplayLabel = #1050#1083#1072#1076#1086#1074#1072#1103
      FieldName = 'DEPNAME'
      Origin = 'SLIST_S.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taBulkListNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SLIST_S.NDSID'
    end
    object taBulkListREF: TIntegerField
      FieldName = 'REF'
      Origin = 'SLIST_S.REF'
    end
    object taBulkListQ: TIntegerField
      FieldName = 'Q'
      Origin = 'SLIST_S.Q'
    end
    object taBulkListSSF: TFIBStringField
      FieldName = 'SSF'
      Origin = 'SLIST_S.SSF'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taBulkListSSFDT: TDateTimeField
      FieldName = 'SSFDT'
      Origin = 'SLIST_S.SSFDT'
    end
    object taBulkListW: TFloatField
      FieldName = 'W'
      Origin = 'SLIST_S.W'
      DisplayFormat = '0.###'
    end
    object taBulkListCOST: TFloatField
      FieldName = 'COST'
      Origin = 'SLIST_S.COST'
    end
    object taBulkListREFNO: TIntegerField
      FieldName = 'REFNO'
      Origin = 'SLIST_S.REFNO'
    end
  end
  object taBulk: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SIEl set'
      '  SEMIS=:SEMIS,'
      '  Q=:Q,'
      '  W=:W,  '
      '  Q0=:Q0,'
      '  W0=:W0,'
      '  TPRICE=:TPRICE '
      'where ID=:OLD_ID')
    DeleteSQL.Strings = (
      'delete from SIEl where ID=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into SIEl(ID, INVID, SEMIS, Q,  W, TPRICE, PRICE, T, OPER' +
        'ID, Q0, W0)'
      
        'values (:ID, :INVID, :SEMIS, :Q, :W, :TPRICE, :PRICE, 3, :OPERID' +
        ', :Q0, :W0)')
    SelectSQL.Strings = (
      
        'select se.ID, se.INVID, se.SEMIS,  se.Q, se.W, se.TPRICE, se.PRI' +
        'CE, se.T, se.COST, se.OPERID, se.Q0, se.W0'
      'from SIEl se'
      'where se.INVID=:INVID'
      'order by se.SEMIS')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeEdit = BulkCheckClose
    BeforeInsert = BulkCheckClose
    BeforeOpen = taBulkBeforeOpen
    BeforePost = taBulkBeforePost
    OnCalcFields = taBulkCalcFields
    OnNewRecord = taBulkNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 72
    Top = 408
    object taBulkID: TIntegerField
      FieldName = 'ID'
      Origin = 'SIEL.ID'
      Required = True
    end
    object taBulkINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SIEL.INVID'
      Required = True
    end
    object taBulkSEMIS: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMIS'
      Origin = 'SIEL.SEMIS'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taBulkQ: TFloatField
      DefaultExpression = '0'
      FieldName = 'Q'
      Origin = 'SIEL.Q'
      Required = True
    end
    object taBulkW: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1042#1077#1089' '#1087#1086#1089#1083#1077' '#1086#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103
      FieldName = 'W'
      Origin = 'SIEL.W'
      Required = True
      DisplayFormat = '0.##0'
    end
    object taBulkTPRICE: TFloatField
      FieldName = 'TPRICE'
      Origin = 'SIEL.TPRICE'
      Required = True
    end
    object taBulkPRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'SIEL.PRICE'
      Required = True
    end
    object taBulkT: TSmallintField
      FieldName = 'T'
      Origin = 'SIEL.T'
    end
    object taBulkOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'SIEL.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taBulkQ0: TIntegerField
      FieldName = 'Q0'
      Origin = 'SIEL.Q0'
    end
    object taBulkW0: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1042#1077#1089' '#1076#1086' '#1086#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103
      FieldName = 'W0'
      Origin = 'SIEL.W0'
      DisplayFormat = '0.##0'
    end
    object taBulkSEMISNAME: TStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldKind = fkLookup
      FieldName = 'SEMISNAME'
      LookupDataSet = dm.taSemis
      LookupKeyFields = 'SEMISID'
      LookupResultField = 'SEMIS'
      KeyFields = 'SEMIS'
      Size = 40
      Lookup = True
    end
    object taBulkRW: TFloatField
      DisplayLabel = #1042#1077#1089#1086#1074#1072#1103' '#1088#1072#1079#1085#1080#1094#1072
      FieldKind = fkCalculated
      FieldName = 'RW'
      DisplayFormat = '0.###'
      Calculated = True
    end
    object taBulkCOST: TFloatField
      FieldName = 'COST'
      Origin = 'SIEL.COST'
      Required = True
    end
  end
  object dsrBulkList: TDataSource
    DataSet = taBulkList
    Left = 16
    Top = 456
  end
  object dsrBulk: TDataSource
    DataSet = taBulk
    Left = 68
    Top = 456
  end
  object taPArt: TpFIBDataSet
    UpdateSQL.Strings = (
      'update PArt set'
      '  OPERID=:OPERID,'
      '  FQ=:FQ'
      'where ID=:ID')
    DeleteSQL.Strings = (
      'delete from PArt where ID=:ID')
    InsertSQL.Strings = (
      
        'execute procedure PArt_I(:ID, :ACTID, :PSID, :OPERID, :ARTID, :A' +
        'RT2ID, :SZID, :U, '
      '   :REST_IN_Q, :GET_Q, :DONE_Q, :REJ_Q, :Q, :FQ)')
    SelectSQL.Strings = (
      'select p.ID, p.ACTID, p.ARTID, '
      '    p.ART2ID, p.SZID, p.OPERID, p.U, p.REST_IN_Q,'
      '    p.GET_Q, p.DONE_Q, p.REJ_Q, p.Q, p.FQ,'
      '    a2.ART, a2.ART2, o.OPERATION OPERNAME, d.NAME DEPNAME,'
      '    sz.NAME SZNAME, p.PSID'
      'from PArt p, Art2 a2, D_Sz sz, D_Oper o, D_Dep d'
      'where p.PSID=d.DepId and'
      '      d.DepId=:DEPID and'
      '      p.OperId =o.OperId and'
      '           p.ACTID=:ACTID and'
      '           p.ART2ID=a2.ART2ID and'
      '           p.SZID=sz.ID and'
      '           (not (p.Q=0 and p.FQ=0))'
      'order by a2.ART, a2.ART2, sz.NAME')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taPArtBeforeDelete
    BeforeOpen = taPArtBeforeOpen
    OnCalcFields = taPArtCalcFields
    OnNewRecord = taPArtNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 360
    Top = 200
    object taPArtID: TIntegerField
      FieldName = 'ID'
      Origin = 'PART.ID'
      Required = True
    end
    object taPArtACTID: TIntegerField
      FieldName = 'ACTID'
      Origin = 'PART.ACTID'
      Required = True
    end
    object taPArtARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'PART.ARTID'
      Required = True
    end
    object taPArtART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'PART.ART2ID'
      Required = True
    end
    object taPArtSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'PART.SZID'
      Required = True
    end
    object taPArtOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'PART.OPERID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taPArtREST_IN_Q: TIntegerField
      DisplayLabel = #1042#1093#1086#1076#1103#1097#1080#1077
      FieldName = 'REST_IN_Q'
      Origin = 'PART.REST_IN_Q'
      Required = True
    end
    object taPArtU: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'PART.U'
      Required = True
    end
    object taPArtGET_Q: TIntegerField
      DisplayLabel = #1056#1072#1089#1093#1086#1076
      FieldName = 'GET_Q'
      Origin = 'PART.GET_Q'
      Required = True
    end
    object taPArtDONE_Q: TIntegerField
      DisplayLabel = #1055#1088#1080#1093#1086#1076
      FieldName = 'DONE_Q'
      Origin = 'PART.DONE_Q'
      Required = True
    end
    object taPArtREJ_Q: TIntegerField
      DisplayLabel = #1041#1088#1072#1082
      FieldName = 'REJ_Q'
      Origin = 'PART.REJ_Q'
      Required = True
    end
    object taPArtQ: TIntegerField
      DisplayLabel = #1059#1095#1077#1090#1085#1086#1077' '#1082#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'PART.Q'
      Required = True
    end
    object taPArtFQ: TIntegerField
      DisplayLabel = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077' '#1082#1086#1083'-'#1074#1086
      FieldName = 'FQ'
      Origin = 'PART.FQ'
      Required = True
    end
    object taPArtOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'D_OPER.OPERATION'
      Required = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taPArtDEPNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'DEPNAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taPArtART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'ART2.ART'
      Required = True
      OnValidate = taPArtARTValidate
      FixedChar = True
      EmptyStrToNull = True
    end
    object taPArtART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      Required = True
      OnValidate = taPArtART2Validate
      FixedChar = True
      EmptyStrToNull = True
    end
    object taPArtSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      Origin = 'D_SZ.NAME'
      Required = True
      OnValidate = taPArtSZNAMEValidate
      FixedChar = True
      EmptyStrToNull = True
    end
    object taPArtRQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'RQ'
      Calculated = True
    end
    object taPArtOPERATIONNAME: TStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldKind = fkLookup
      FieldName = 'OPERATIONNAME'
      LookupDataSet = dm.taOper
      LookupKeyFields = 'OPERID'
      LookupResultField = 'OPERATION'
      KeyFields = 'OPERID'
      Size = 60
      Lookup = True
    end
    object taPArtPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'PART.PSID'
      Required = True
    end
  end
  object dsrPArt: TDataSource
    DataSet = taPArt
    Left = 360
    Top = 244
  end
  object taZpAssort: TpFIBDataSet
    SelectSQL.Strings = (
      'select ARTID, ART, Q, MW, K,  ARTK'
      'from ZpAssort(:ZITEMID)'
      'order by ART')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taZpAssortBeforeOpen
    OnCalcFields = taZpAssortCalcFields
    Transaction = dm.tr
    Database = dm.db
    Left = 200
    Top = 304
    object taZpAssortARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'ZPASSORT.ARTID'
    end
    object taZpAssortART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'ZPASSORT.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taZpAssortQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'ZPASSORT.Q'
    end
    object taZpAssortMW: TFloatField
      DisplayLabel = #1057#1088#1077#1076#1085#1080#1081' '#1074#1077#1089
      FieldName = 'MW'
      Origin = 'ZPASSORT.MW'
      DisplayFormat = '0.000'
    end
    object taZpAssortK: TFloatField
      DisplayLabel = #1048#1090#1086#1075#1086#1074#1099#1081' '#1082#1086#1101#1092'.'
      FieldName = 'K'
      Origin = 'ZPASSORT.K'
    end
    object taZpAssortARTK: TFloatField
      DisplayLabel = #1048#1085#1076#1080#1074#1080#1076'. '#1082#1086#1101#1092'.'
      FieldName = 'ARTK'
      Origin = 'ZPASSORT.ARTK'
    end
    object taZpAssortKT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'KT'
      Calculated = True
    end
  end
  object dsrZpAssort: TDataSource
    DataSet = taZpAssort
    Left = 200
    Top = 352
  end
  object taAFList: TpFIBDataSet
    UpdateSQL.Strings = (
      
        'execute procedure AFList$U(:INVID, :DOCNO, :DOCDATE, :DEPID, :SU' +
        'PID, :REF, :IS$FOREIGN$GOLD)')
    DeleteSQL.Strings = (
      'delete from Inv where InvId=:INVID')
    InsertSQL.Strings = (
      
        'execute procedure AFList$I(:INVID, :DOCNO, :DOCDATE, :COMPID, :D' +
        'EPID, :ITYPE, :SUPID, '
      '    :USERID, :ISCLOSE, :REF)')
    RefreshSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, ITYPE, USERID, USERNAME,'
      '    COMPID, COMPNAME, SUPID, SUPNAME, DEPID, DEPNAME,'
      '    ISCLOSE, W0, W, REF, REFDOCNO, REFDOCDATE, is$foreign$gold'
      'from AFList_R(:INVID)')
    SelectSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, ITYPE, USERID, USERNAME,'
      '    COMPID, COMPNAME, SUPID, SUPNAME, DEPID, DEPNAME,'
      
        '    ISCLOSE, W0, W, REF, REFDOCNO, REFDOCDATE, W1, is$foreign$go' +
        'ld'
      'from AFList_S(:BD, :ED)')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taAFListBeforeDelete
    BeforeOpen = taAFListBeforeOpen
    OnCalcFields = taAFListCalcFields
    OnNewRecord = taAFListNewRecord
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    Left = 144
    Top = 412
    object taAFListINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'INV.INVID'
      Required = True
    end
    object taAFListDOCNO: TIntegerField
      DisplayLabel = #8470' '#1085#1072#1082#1083'.'
      FieldName = 'DOCNO'
      Origin = 'INV.DOCNO'
    end
    object taAFListDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1085#1072#1082#1083'.'
      FieldName = 'DOCDATE'
      Origin = 'INV.DOCDATE'
    end
    object taAFListITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'INV.ITYPE'
    end
    object taAFListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'INV.USERID'
    end
    object taAFListUSERNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'USERNAME'
      Origin = 'AFLIST_S.USERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAFListSUPID: TIntegerField
      FieldName = 'SUPID'
      Origin = 'INV.SUPID'
    end
    object taAFListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'INV.ISCLOSE'
    end
    object taAFListDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'INV.DEPID'
      OnValidate = taAFListDEPIDValidate
    end
    object taAFListDEPNAME: TFIBStringField
      DisplayLabel = #1050#1083#1072#1076#1086#1074#1072#1103
      FieldName = 'DEPNAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taAFListSUPNAME: TFIBStringField
      DisplayLabel = #1050#1086#1084#1091
      FieldName = 'SUPNAME'
      Origin = 'D_COMP.NAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAFListCOMPID: TIntegerField
      FieldName = 'COMPID'
      Origin = 'AFLIST_S.COMPID'
    end
    object taAFListCOMPNAME: TFIBStringField
      DisplayLabel = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
      FieldName = 'COMPNAME'
      Origin = 'AFLIST_S.COMPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAFListR: TFloatField
      DisplayLabel = #1056#1072#1079#1085#1080#1094#1072
      FieldKind = fkCalculated
      FieldName = 'R'
      DisplayFormat = '0.###'
      Calculated = True
    end
    object taAFListREF: TFIBIntegerField
      FieldName = 'REF'
    end
    object taAFListREFDOCNO: TFIBIntegerField
      DisplayLabel = #1040#1082#1090' '#1087#1086#1090#1077#1088#1100' '#1085#1086#1084#1077#1088
      FieldName = 'REFDOCNO'
    end
    object taAFListREFDOCDATE: TFIBDateTimeField
      DisplayLabel = #1040#1082#1090' '#1087#1086#1090#1077#1088#1100' '#1076#1072#1090#1072
      FieldName = 'REFDOCDATE'
    end
    object taAFListW0: TFIBFloatField
      DisplayLabel = #1042#1099#1076#1072#1085#1086
      FieldName = 'W0'
      Origin = 'tAFList_S.W0'
      DisplayFormat = '0.###'
    end
    object taAFListW1: TFIBBCDField
      DisplayLabel = #1055#1088#1080#1085#1103#1090#1086
      FieldName = 'W1'
      Origin = 'tAFList_S.W1'
      DisplayFormat = '0.###'
      Size = 3
      RoundByScale = True
    end
    object taAFListW: TFIBFloatField
      FieldName = 'W'
    end
    object taAFListISFOREIGNGOLD: TFIBSmallIntField
      FieldName = 'IS$FOREIGN$GOLD'
    end
  end
  object dsrAFList: TDataSource
    DataSet = taAFList
    Left = 144
    Top = 456
  end
  object taAffinaj: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SIEl set'
      '  SEMISAFFID=:SEMISAFFID,'
      '  Q=:Q,'
      '  W=:W,'
      '  Q0=:Q0,'
      '  W0=:W0'
      'where ID=:ID')
    DeleteSQL.Strings = (
      'delete from SIEL where ID=:ID')
    InsertSQL.Strings = (
      
        'execute procedure Affinaj_I(:ID, :INVID, :SEMISID, :Q, :W, :T, :' +
        'SEMISAFFID, :DEPID, :Q0, :W0, :OPERID, :COVERDEPID)')
    RefreshSQL.Strings = (
      'select ID, INVID, SEMISID, SEMISNAME, Q, W, T, SEMISAFFID,'
      '       DEPID, DEPNAME, UQ, UW, Q0, W0, OPERID, OPERNAME,'
      '       COVERDEPID, COVERDEPNAME  '
      'from Affinaj_R(:ID)')
    SelectSQL.Strings = (
      'select ID, INVID, SEMISID, SEMISNAME, Q, W, T, SEMISAFFID,'
      '       DEPID, DEPNAME, UQ, UW, Q0, W0, OPERID, OPERNAME,'
      '       COVERDEPID, COVERDEPNAME'
      'from Affinaj_S(:INVID)'
      'order by T, SEMISNAME')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = taAffinajAfterPost
    BeforeDelete = taAffinajBeforeDelete
    BeforeOpen = taAffinajBeforeOpen
    BeforePost = taAffinajBeforePost
    OnNewRecord = taAffinajNewRecord
    OnPostError = taAffinajPostError
    Transaction = dm.tr
    Database = dm.db
    Left = 200
    Top = 412
    object taAffinajID: TIntegerField
      FieldName = 'ID'
      Origin = 'AFFINAJ_S.ID'
    end
    object taAffinajINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'AFFINAJ_S.INVID'
    end
    object taAffinajSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'AFFINAJ_S.SEMISID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAffinajSEMISNAME: TFIBStringField
      FieldName = 'SEMISNAME'
      Origin = 'AFFINAJ_S.SEMISNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taAffinajQ: TIntegerField
      FieldName = 'Q'
      Origin = 'AFFINAJ_S.Q'
      OnChange = taAffinajWQChange
    end
    object taAffinajW: TFloatField
      FieldName = 'W'
      Origin = 'AFFINAJ_S.W'
      OnChange = taAffinajWQChange
      DisplayFormat = '0.###'
    end
    object taAffinajT: TSmallintField
      FieldName = 'T'
      Origin = 'AFFINAJ_S.T'
      OnGetText = taAffinajTGetText
    end
    object taAffinajSEMISAFFID: TIntegerField
      FieldName = 'SEMISAFFID'
      Origin = 'AFFINAJ_S.SEMISAFFID'
    end
    object taAffinajDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'AFFINAJ_S.DEPID'
    end
    object taAffinajDEPNAME: TFIBStringField
      DisplayLabel = #1050#1083#1072#1076#1086#1074#1072#1103
      FieldName = 'DEPNAME'
      Origin = 'AFFINAJ_S.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taAffinajSEMISAFFNAME: TStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090', '#1089#1086#1076#1077#1088#1078#1072#1097#1080#1081' '#1076#1088#1072#1075'. '#1084#1077#1090#1072#1083#1083#1099
      FieldKind = fkLookup
      FieldName = 'SEMISAFFNAME'
      LookupDataSet = dm.taSemisAff
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'SEMISAFFID'
      Size = 40
      Lookup = True
    end
    object taAffinajUQ: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'. '#1082#1086#1083'-'#1074#1072
      FieldName = 'UQ'
      Origin = 'AFFINAJ_S.UQ'
    end
    object taAffinajUW: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'. '#1074#1077#1089#1072
      FieldName = 'UW'
      Origin = 'AFFINAJ_S.UW'
    end
    object taAffinajQ0: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q0'
      Origin = 'AFFINAJ_S.Q0'
    end
    object taAffinajW0: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W0'
      Origin = 'AFFINAJ_S.W0'
      DisplayFormat = '0.###'
    end
    object taAffinajOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AFFINAJ_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAffinajOPERNAME: TFIBStringField
      FieldName = 'OPERNAME'
      Origin = 'AFFINAJ_S.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAffinajCOVERDEPID: TFIBIntegerField
      FieldName = 'COVERDEPID'
    end
    object taAffinajCOVERDEPNAME: TFIBStringField
      DisplayLabel = #1059#1095#1072#1089#1090#1086#1082' '#1089#1098#1077#1084#1072
      FieldName = 'COVERDEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrAffinaj: TDataSource
    DataSet = taAffinaj
    Left = 200
    Top = 456
  end
  object taWhAffinaj: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure F_Update')
    InsertSQL.Strings = (
      'execute procedure F_Insert(:ID)')
    RefreshSQL.Strings = (
      
        'select ID, DEPID, SEMISID, SEMISNAME, Q, W, UQ, UW, T, GOOD, OPE' +
        'RID,'
      '         OPERNAME'
      'from WhAffinaj_R(:ID, :DEPID, :SEMISID, :UQ, :UW, :T, :OPERID)')
    SelectSQL.Strings = (
      'select ID, SEMISID, SEMISNAME, Q, W, T, UQ, UW, DEPID, '
      '       DEPNAME, GOOD, OPERID, OPERNAME'
      'from WhAffinaj_S(:T, :DEPID, :MATID1, :MATID2, :FILTEROPERID)'
      'order by SEMISNAME')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taWhAffinajBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 272
    Top = 416
    object taWhAffinajID: TIntegerField
      FieldName = 'ID'
      Origin = 'WHAFFINAJ_S.ID'
    end
    object taWhAffinajDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'WHAFFINAJ_S.DEPID'
    end
    object taWhAffinajDEPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      FieldName = 'DEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taWhAffinajSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'WHAFFINAJ_S.SEMISID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWhAffinajSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'WHAFFINAJ_S.SEMISNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWhAffinajQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'WHAFFINAJ_S.Q'
    end
    object taWhAffinajW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'WHAFFINAJ_S.W'
      DisplayFormat = '0.###'
    end
    object taWhAffinajT: TSmallintField
      FieldName = 'T'
      Origin = 'WHAFFINAJ_S.T'
    end
    object taWhAffinajUQ: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'. '#1082#1086#1083'-'#1074#1072
      FieldName = 'UQ'
      Origin = 'WHAFFINAJ_S.UQ'
      OnGetText = UQGetText
    end
    object taWhAffinajUW: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'. '#1074#1077#1089#1072
      FieldName = 'UW'
      Origin = 'WHAFFINAJ_S.UW'
      OnGetText = UWGetText
    end
    object taWhAffinajGOOD: TSmallintField
      FieldName = 'GOOD'
      Origin = 'WHAFFINAJ_S.GOOD'
    end
    object taWhAffinajOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'WHAFFINAJ_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWhAffinajOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'WHAFFINAJ_S.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
  end
  object dsrWhAffinaj: TDataSource
    DataSet = taWhAffinaj
    Left = 272
    Top = 464
  end
  object taProtocolMat: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select t.NAME TAILNAME, m.NAME MATNAME, sum(e.N) W, cast(3 as sm' +
        'allint) U '
      'from ActTails e, D_Tails t, D_Mat m'
      'where e.ACTID = :ACTID1 and'
      '           e.Tails = t.Id   and'
      '           m.ID='#39'1'#39
      'group by t.NAME, m.NAME'
      ''
      'union'
      ''
      'select cast('#39#39' as char(40)), m.NAME MATNAME,  p.W W, p.U1 U'
      'from ProtocolMat p, D_Mat m'
      'where p.ACTID = :ACTID2 and'
      '           p.MATID=m.ID and'
      '           equal(p.W, 0, 0.001)=0'
      'union'
      ''
      
        'select cast('#39#1058#1086#1083#1083#1080#1085#1075#39' as char(40)), m.NAME MATNAME, sum(it.NW) W' +
        ', it.UW '
      'from MatItem it, Inv i, D_Mat m'
      'where i.ITYPE=21 and'
      '      i.DOCDATE between :BD and :ED and'
      '      i.ISCLOSE=1 and'
      '      i.INVID = it.INVID and'
      '      it.MATID = m.ID and'
      '      it.N <> 0'
      'group by 1, 2, 4'
      'order by 1')
    CacheModelOptions.BufferChunks = 1000
    BeforeClose = PostBeforeClose
    BeforeOpen = taProtocolMatBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 420
    Top = 199
    object taProtocolMatMATNAME: TFIBStringField
      DisplayLabel = #1052#1072#1090#1077#1088#1080#1072#1083
      FieldName = 'MATNAME'
      Origin = 'D_MAT.NAME'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taProtocolMatW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'PROTOCOLMAT.W'
      Required = True
      DisplayFormat = '0.###'
    end
    object taProtocolMatU: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'PROTOCOLMAT.U'
      Required = True
      OnGetText = taProtocolMatUGetText
    end
    object taProtocolMatTAILNAME: TFIBStringField
      DisplayLabel = #1042#1080#1076' '#1086#1090#1093#1086#1076#1072
      FieldName = 'TAILNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrProtocolMat: TDataSource
    DataSet = taProtocolMat
    Left = 422
    Top = 244
  end
  object taCheckProtocolData: TpFIBDataSet
    SelectSQL.Strings = (
      'select t.ID, t.DEPID, t.OPERID, t.WORDER_GET_W,'
      '    t.WORDER_RET_W, t.WORDER_F, t.PROTOCOL_IN,'
      '    t.PROTOCOL_DONE_W, t.PROTOCOL_OUT, t.R,'
      '    o.OPERATION, d.NAME'
      'from Tmp_Compare_Protocol_WOrder t, D_Dep d, D_Oper o'
      'where t.DEPID=d.DEPID and'
      '          t.OPERID=o.OPERID'
      'order by d.SORTIND, o.OPERATION ')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 296
    Top = 304
    object taCheckProtocolDataID: TIntegerField
      FieldName = 'ID'
      Origin = 'TMP_COMPARE_PROTOCOL_WORDER.ID'
      Required = True
    end
    object taCheckProtocolDataDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'TMP_COMPARE_PROTOCOL_WORDER.DEPID'
      Required = True
    end
    object taCheckProtocolDataOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'TMP_COMPARE_PROTOCOL_WORDER.OPERID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taCheckProtocolDataWORDER_GET_W: TFloatField
      DisplayLabel = #1042#1099#1076#1072#1085#1086
      FieldName = 'WORDER_GET_W'
      Origin = 'TMP_COMPARE_PROTOCOL_WORDER.WORDER_GET_W'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckProtocolDataWORDER_RET_W: TFloatField
      DisplayLabel = #1055#1088#1080#1085#1103#1090#1086
      FieldName = 'WORDER_RET_W'
      Origin = 'TMP_COMPARE_PROTOCOL_WORDER.WORDER_RET_W'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckProtocolDataWORDER_F: TFloatField
      DisplayLabel = #1060#1072#1082#1090'. '#1087#1086#1090#1077#1088#1080
      FieldName = 'WORDER_F'
      Origin = 'TMP_COMPARE_PROTOCOL_WORDER.WORDER_F'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckProtocolDataPROTOCOL_IN: TFloatField
      DisplayLabel = #1042#1093#1086#1076'.'
      FieldName = 'PROTOCOL_IN'
      Origin = 'TMP_COMPARE_PROTOCOL_WORDER.PROTOCOL_IN'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckProtocolDataPROTOCOL_DONE_W: TFloatField
      DisplayLabel = #1050' '#1089#1087#1080#1089#1072#1085#1080#1102
      FieldName = 'PROTOCOL_DONE_W'
      Origin = 'TMP_COMPARE_PROTOCOL_WORDER.PROTOCOL_DONE_W'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckProtocolDataPROTOCOL_OUT: TFloatField
      DisplayLabel = #1048#1089#1093'.'
      FieldName = 'PROTOCOL_OUT'
      Origin = 'TMP_COMPARE_PROTOCOL_WORDER.PROTOCOL_OUT'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckProtocolDataR: TFloatField
      DisplayLabel = #1056#1072#1079#1085#1080#1094#1072
      FieldName = 'R'
      Origin = 'TMP_COMPARE_PROTOCOL_WORDER.R'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckProtocolDataOPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Origin = 'D_OPER.OPERATION'
      Required = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taCheckProtocolDataNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'NAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrCheckProtocolData: TDataSource
    DataSet = taCheckProtocolData
    Left = 296
    Top = 352
  end
  object taSChargeList: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Inv set'
      '  DOCNO=:DOCNO, '
      '  DOCDATE=:DOCDATE, '
      '  USERID=:USERID,'
      '  ISCLOSE=:ISCLOSE'
      'where InvId=:OLD_INVID')
    DeleteSQL.Strings = (
      'delete from Inv where InvId=:OLD_INVID')
    InsertSQL.Strings = (
      
        'insert into Inv(INVID, DOCNO, DOCDATE, ITYPE, DEPID, SUPID, USER' +
        'ID, '
      '     ISCLOSE, NDSID, SSF, SSFDT)'
      
        'values (:INVID, :DOCNO, :DOCDATE, :ITYPE, :DEPID, :SUPID,:USERID' +
        ','
      '      0, :NDSID, :SSF, :SSFDT)')
    RefreshSQL.Strings = (
      'select i.INVID, i.DOCNO, i.DOCDATE , i.ITYPE, i.DEPID, i.USERID,'
      
        '    i.SUPID,  i.FIO, i.SUPNAME,  i.ISCLOSE, i.DEPNAME,  i.NDSID,' +
        ' i.REF,'
      
        '    i.Q, i.SSF, i.SSFDT, i.W, i.COST, a.DOCNO REFDOCNO, a.DOCDAT' +
        'E REFDOCDATE'
      'from SList_R(:INVID) i left join Act a on (i.REF=a.ID)')
    SelectSQL.Strings = (
      'select i.INVID, i.DOCNO, i.DOCDATE, i.ITYPE, i.DEPID, i.USERID,'
      
        '    i.SUPID, i.FIO, i.SUPNAME, i.ISCLOSE, i.DEPNAME,  i.NDSID, i' +
        '.REF,'
      
        '    i.Q, i.SSF, i.SSFDT, i.W, i.COST, a.DOCNO REFDOCNO, a.DOCDAT' +
        'E REFDOCDATE'
      
        'from SList_S(:DEPID1, :DEPID2, :BD, :ED,:ITYPE_) i left join Act' +
        ' a on    (i.REF=a.ID)'
      'order by i.DocDate')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taSChargeListBeforeDelete
    BeforeOpen = taSChargeListBeforeOpen
    OnNewRecord = taSChargeListNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 624
    Top = 196
    object taSChargeListINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SLIST_S.INVID'
    end
    object taSChargeListDOCNO: TIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1072#1082#1090#1072
      FieldName = 'DOCNO'
      Origin = 'SLIST_S.DOCNO'
    end
    object taSChargeListDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1072#1082#1090#1072
      FieldName = 'DOCDATE'
      Origin = 'SLIST_S.DOCDATE'
    end
    object taSChargeListITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'SLIST_S.ITYPE'
    end
    object taSChargeListDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SLIST_S.DEPID'
    end
    object taSChargeListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'SLIST_S.USERID'
    end
    object taSChargeListSUPID: TIntegerField
      FieldName = 'SUPID'
      Origin = 'SLIST_S.SUPID'
    end
    object taSChargeListFIO: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'FIO'
      Origin = 'SLIST_S.FIO'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSChargeListSUPNAME: TFIBStringField
      FieldName = 'SUPNAME'
      Origin = 'SLIST_S.SUPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSChargeListISCLOSE: TSmallintField
      FieldName = 'ISCLOSE'
      Origin = 'SLIST_S.ISCLOSE'
    end
    object taSChargeListDEPNAME: TFIBStringField
      DisplayLabel = #1057#1082#1083#1072#1076
      FieldName = 'DEPNAME'
      Origin = 'SLIST_S.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSChargeListNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SLIST_S.NDSID'
    end
    object taSChargeListREF: TIntegerField
      FieldName = 'REF'
      Origin = 'SLIST_S.REF'
    end
    object taSChargeListQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'SLIST_S.Q'
    end
    object taSChargeListSSF: TFIBStringField
      FieldName = 'SSF'
      Origin = 'SLIST_S.SSF'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taSChargeListSSFDT: TDateTimeField
      FieldName = 'SSFDT'
      Origin = 'SLIST_S.SSFDT'
    end
    object taSChargeListW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SLIST_S.W'
    end
    object taSChargeListCOST: TFloatField
      FieldName = 'COST'
      Origin = 'SLIST_S.COST'
    end
    object taSChargeListREFDOCNO: TIntegerField
      DisplayLabel = #1055#1088#1086#1090#1086#1082#1086#1083' '#1085#1086#1084#1077#1088
      FieldName = 'REFDOCNO'
      Origin = 'INV.DOCNO'
    end
    object taSChargeListREFDOCDATE: TDateTimeField
      DisplayLabel = #1055#1088#1086#1090#1086#1082#1086#1083' '#1076#1072#1090#1072
      FieldName = 'REFDOCDATE'
      Origin = 'INV.DOCDATE'
    end
  end
  object taSCharge: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from SIEl where ID=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into SIEl(ID, INVID, SEMIS, Q,  W, TPRICE, PRICE, NDSID, ' +
        'NDS, T, COST, OPERID, FOR_Q_U, REF, DEPID)'
      
        'values (:ID, :INVID, :SEMIS, :Q, :W, :TPRICE, :PRICE, :NDSID, :N' +
        'DS, 8, :COST, :OPERID,:FOR_Q_U, :REF, :DEPID)')
    RefreshSQL.Strings = (
      
        'select se.ID, se.INVID, se.SEMIS,  se.Q, se.W, se.TPRICE, se.PRI' +
        'CE,                '
      '        se.NDSID, se.NDS, se.T, se.COST, se.OPERID, se.FOR_Q_U,'
      '        se.REF, se.UQ, se.UW, se.DEPID'
      'from SIEl se'
      'where se.ID=:ID')
    SelectSQL.Strings = (
      
        'select se.ID, se.INVID, se.SEMIS,  se.Q, se.W, se.TPRICE, se.PRI' +
        'CE,                se.NDSID, se.NDS, se.T, se.COST, se.OPERID, s' +
        'e.FOR_Q_U,'
      '        se.REF, se.UQ, se.UW, se.DEPID'
      'from SIEl se'
      'where se.InvId=:INVID            '
      'order by se.Id')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = taSChargeBeforeDelete
    BeforeEdit = SChargeCheckClose
    BeforeOpen = taSChargeBeforeOpen
    OnNewRecord = taSChargeNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 696
    Top = 196
    object taSChargeID: TIntegerField
      FieldName = 'ID'
      Origin = 'SIEL.ID'
      Required = True
    end
    object taSChargeINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'SIEL.INVID'
      Required = True
    end
    object taSChargeSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Origin = 'SIEL.SEMIS'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSChargeQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'SIEL.Q'
      Required = True
    end
    object taSChargeW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'SIEL.W'
      Required = True
    end
    object taSChargeTPRICE: TFloatField
      FieldName = 'TPRICE'
      Origin = 'SIEL.TPRICE'
      Required = True
    end
    object taSChargePRICE: TFloatField
      FieldName = 'PRICE'
      Origin = 'SIEL.PRICE'
      Required = True
    end
    object taSChargeNDSID: TIntegerField
      FieldName = 'NDSID'
      Origin = 'SIEL.NDSID'
      Required = True
    end
    object taSChargeT: TSmallintField
      FieldName = 'T'
      Origin = 'SIEL.T'
    end
    object taSChargeCOST: TFloatField
      FieldName = 'COST'
      Origin = 'SIEL.COST'
      Required = True
    end
    object taSChargeOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'SIEL.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSChargeFOR_Q_U: TSmallintField
      FieldName = 'FOR_Q_U'
      Origin = 'SIEL.FOR_Q_U'
      Required = True
    end
    object taSChargeREF: TIntegerField
      FieldName = 'REF'
      Origin = 'SIEL.REF'
    end
    object taSChargeSEMISNAME: TStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldKind = fkLookup
      FieldName = 'SEMISNAME'
      LookupDataSet = dm.taSemis
      LookupKeyFields = 'SEMISID'
      LookupResultField = 'SEMIS'
      KeyFields = 'SEMIS'
      Size = 40
      Lookup = True
    end
    object taSChargeOPERNAME: TStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldKind = fkLookup
      FieldName = 'OPERNAME'
      LookupDataSet = dm.taOper
      LookupKeyFields = 'OPERID'
      LookupResultField = 'OPERATION'
      KeyFields = 'OPERID'
      Size = 60
      Lookup = True
    end
    object taSChargeUQ: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'. '#1082#1086#1083'-'#1074#1072
      FieldName = 'UQ'
      Origin = 'SIEL.UQ'
      Required = True
      OnGetText = UQGetText
    end
    object taSChargeUW: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'. '#1074#1077#1089#1072
      FieldName = 'UW'
      Origin = 'SIEL.UW'
      Required = True
      OnGetText = UWGetText
    end
    object taSChargeNDS: TFloatField
      FieldName = 'NDS'
      Origin = 'SIEL.NDS'
      Required = True
    end
    object taSChargeDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'SIEL.DEPID'
    end
    object taSChargeDEPNAME: TStringField
      DisplayLabel = #1042#1080#1085#1086#1074#1085#1080#1082
      FieldKind = fkLookup
      FieldName = 'DEPNAME'
      LookupDataSet = taPsDep
      LookupKeyFields = 'DEPID'
      LookupResultField = 'NAME'
      KeyFields = 'DEPID'
      Size = 40
      Lookup = True
    end
  end
  object dsrSChargeList: TDataSource
    DataSet = taSChargeList
    Left = 624
    Top = 240
  end
  object dsrSCharge: TDataSource
    DataSet = taSCharge
    Left = 696
    Top = 240
  end
  object taSChargeWH: TpFIBDataSet
    UpdateSQL.Strings = (
      'update WhSemis set'
      '  Id=null '
      'where Id is null')
    InsertSQL.Strings = (
      'execute procedure F_INSERT(0)')
    RefreshSQL.Strings = (
      
        'select DEPID, SEMISID, Q, W, UQ, UW, SEMISNAME, OPERID,         ' +
        '           '
      '          OPERNAME, GOOD, DEPNAME, SORTINDDEPID, SORTIND'
      'from WhSemis_R(:DEPID, :SEMISID, :OPERID)')
    SelectSQL.Strings = (
      
        'select DEPID, DEPNAME, SEMISID, Q, W, UQ, UW, SEMISNAME,        ' +
        '   OPERID, OPERNAME, GOOD, SORTINDDEPID, SORTIND'
      
        'from WhSemis_S(:OPERID, :DEPID, :MATID1, :MATID2,  :T,        :F' +
        'ILTEROPERID)'
      'order by SORTINDDEPID, SORTIND')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taSChargeWHBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 764
    Top = 196
    object taSChargeWHDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'WHSEMIS_S.DEPID'
    end
    object taSChargeWHDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Origin = 'WHSEMIS_S.DEPNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSChargeWHSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'WHSEMIS_S.SEMISID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSChargeWHQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'WHSEMIS_S.Q'
    end
    object taSChargeWHW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'WHSEMIS_S.W'
      DisplayFormat = '0.###'
    end
    object taSChargeWHUQ: TSmallintField
      FieldName = 'UQ'
      Origin = 'WHSEMIS_S.UQ'
      OnGetText = UQGetText
    end
    object taSChargeWHUW: TSmallintField
      FieldName = 'UW'
      Origin = 'WHSEMIS_S.UW'
      OnGetText = UWGetText
    end
    object taSChargeWHSEMISNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMISNAME'
      Origin = 'WHSEMIS_S.SEMISNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taSChargeWHOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'WHSEMIS_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taSChargeWHOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Origin = 'WHSEMIS_S.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taSChargeWHGOOD: TSmallintField
      FieldName = 'GOOD'
      Origin = 'WHSEMIS_S.GOOD'
    end
    object taSChargeWHSORTINDDEPID: TIntegerField
      FieldName = 'SORTINDDEPID'
      Origin = 'WHSEMIS_S.SORTINDDEPID'
    end
    object taSChargeWHSORTIND: TIntegerField
      FieldName = 'SORTIND'
      Origin = 'WHSEMIS_S.SORTIND'
    end
  end
  object dsrSChargeWH: TDataSource
    DataSet = taSChargeWH
    Left = 764
    Top = 240
  end
  object taCheckStone: TpFIBDataSet
    SelectSQL.Strings = (
      'select ts.SEMISID, ts.UQ, ts.UW, s.SEMIS SEMISNAME,'
      '    REST_INQ_1, REST_INW_1, INQ_1, INW_1, PRODQ_1, PRODW_1,'
      '    RETPRODQ_1, RETPRODW_1, SCHARGEQ_1,'
      '    SCHARGEW_1, FQ_1, FW_1, REST_INQ_2, REST_INW_2,'
      '    INQ_2, INW_2, WORDERQ_2, WORDERW_2,'
      '    SCHARGEQ_2, SCHARGEW_2, FQ_2, FW_2,'
      '    RQ_1, RW_1, RQ_2, RW_2'
      'from Tmp_Stone ts, D_Semis s, D_Mat m'
      'where ts.USERID=:USERID and'
      '           ts.SEMISID=s.SEMISID and'
      '           s.MAT=m.ID and'
      '           m.IsIns = 1 and'
      '           m.ID between :MATID1 and :MATID2'
      'order by s.SORTIND'
      '           ')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taCheckStoneBeforeOpen
    OnCalcFields = taCheckStoneCalcFields
    Transaction = dm.tr
    Database = dm.db
    Left = 764
    Top = 416
    object taCheckStoneSEMISID: TFIBStringField
      FieldName = 'SEMISID'
      Origin = 'TMP_STONE.SEMISID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taCheckStoneUQ: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'. '#1082#1086#1083'-'#1074#1072
      FieldName = 'UQ'
      Origin = 'TMP_STONE.UQ'
      Required = True
      OnGetText = UQGetText
    end
    object taCheckStoneUW: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'. '#1074#1077#1089#1072
      FieldName = 'UW'
      Origin = 'TMP_STONE.UW'
      Required = True
      OnGetText = UWGetText
    end
    object taCheckStoneSEMISNAME: TFIBStringField
      DisplayLabel = #1042#1089#1090#1072#1074#1082#1072
      FieldName = 'SEMISNAME'
      Origin = 'D_SEMIS.SEMIS'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taCheckStoneREST_INQ_1: TIntegerField
      DisplayLabel = #1042#1093#1086#1076#1103#1097#1080#1077' '#1082#1086#1083'-'#1074#1086
      FieldName = 'REST_INQ_1'
      Origin = 'TMP_STONE.REST_INQ_1'
      Required = True
    end
    object taCheckStoneREST_INW_1: TFloatField
      DisplayLabel = #1042#1093#1086#1076#1103#1097#1080#1077' '#1074#1077#1089
      FieldName = 'REST_INW_1'
      Origin = 'TMP_STONE.REST_INW_1'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneINQ_1: TIntegerField
      DisplayLabel = #1055#1088#1080#1093#1086#1076' '#1082#1086#1083'-'#1074#1086
      FieldName = 'INQ_1'
      Origin = 'TMP_STONE.INQ_1'
      Required = True
    end
    object taCheckStoneINW_1: TFloatField
      DisplayLabel = #1055#1088#1080#1093#1086#1076' '#1074#1077#1089
      FieldName = 'INW_1'
      Origin = 'TMP_STONE.INW_1'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStonePRODQ_1: TIntegerField
      DisplayLabel = #1054#1090#1075#1088#1091#1079#1082#1072' '#1082#1086#1083'-'#1074#1086
      FieldName = 'PRODQ_1'
      Origin = 'TMP_STONE.PRODQ_1'
      Required = True
    end
    object taCheckStonePRODW_1: TFloatField
      DisplayLabel = #1054#1090#1075#1088#1091#1079#1082#1072' '#1074#1077#1089
      FieldName = 'PRODW_1'
      Origin = 'TMP_STONE.PRODW_1'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneRETPRODQ_1: TIntegerField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1087#1088#1086#1076#1091#1082#1094#1080#1080' '#1082#1086#1083'-'#1074#1086
      FieldName = 'RETPRODQ_1'
      Origin = 'TMP_STONE.RETPRODQ_1'
      Required = True
    end
    object taCheckStoneRETPRODW_1: TFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' '#1087#1088#1086#1076#1091#1082#1094#1080#1080' '#1074#1077#1089
      FieldName = 'RETPRODW_1'
      Origin = 'TMP_STONE.RETPRODW_1'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneSCHARGEQ_1: TIntegerField
      DisplayLabel = #1057#1087#1080#1089#1072#1085#1080#1077' '#1082#1086#1083'-'#1074#1086
      FieldName = 'SCHARGEQ_1'
      Origin = 'TMP_STONE.SCHARGEQ_1'
      Required = True
    end
    object taCheckStoneSCHARGEW_1: TFloatField
      DisplayLabel = #1057#1087#1080#1089#1072#1085#1080#1077' '#1074#1077#1089
      FieldName = 'SCHARGEW_1'
      Origin = 'TMP_STONE.SCHARGEW_1'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneFQ_1: TIntegerField
      DisplayLabel = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1082#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072' '#1082#1086#1083'-'#1074#1086
      FieldName = 'FQ_1'
      Origin = 'TMP_STONE.FQ_1'
      Required = True
    end
    object taCheckStoneFW_1: TFloatField
      DisplayLabel = #1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1082#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072' '#1074#1077#1089
      FieldName = 'FW_1'
      Origin = 'TMP_STONE.FW_1'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneREST_INQ_2: TIntegerField
      DisplayLabel = #1042#1093#1086#1076#1103#1097#1080#1077' '#1082#1086#1083'-'#1074#1086' 2'
      FieldName = 'REST_INQ_2'
      Origin = 'TMP_STONE.REST_INQ_2'
      Required = True
    end
    object taCheckStoneREST_INW_2: TFloatField
      DisplayLabel = #1042#1093#1086#1076#1103#1097#1080#1077' '#1074#1077#1089' 2'
      FieldName = 'REST_INW_2'
      Origin = 'TMP_STONE.REST_INW_2'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneINQ_2: TIntegerField
      DisplayLabel = #1055#1088#1080#1093#1086#1076' '#1082#1086#1083'-'#1074#1086' 2'
      FieldName = 'INQ_2'
      Origin = 'TMP_STONE.INQ_2'
      Required = True
    end
    object taCheckStoneINW_2: TFloatField
      DisplayLabel = #1055#1088#1080#1093#1086#1076' '#1074#1077#1089' 2'
      FieldName = 'INW_2'
      Origin = 'TMP_STONE.INW_2'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneWORDERQ_2: TIntegerField
      DisplayLabel = #1053#1072#1088#1103#1076' '#1082#1086#1083'-'#1074#1086' 2 '
      FieldName = 'WORDERQ_2'
      Origin = 'TMP_STONE.WORDERQ_2'
      Required = True
    end
    object taCheckStoneWORDERW_2: TFloatField
      DisplayLabel = #1053#1072#1088#1103#1076' '#1074#1077#1089' 2'
      FieldName = 'WORDERW_2'
      Origin = 'TMP_STONE.WORDERW_2'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneSCHARGEQ_2: TIntegerField
      DisplayLabel = #1057#1087#1080#1089#1072#1085#1080#1077' '#1082#1086#1083'-'#1074#1086'  2 '
      FieldName = 'SCHARGEQ_2'
      Origin = 'TMP_STONE.SCHARGEQ_2'
      Required = True
    end
    object taCheckStoneSCHARGEW_2: TFloatField
      DisplayLabel = #1057#1087#1080#1089#1072#1085#1080#1077' '#1074#1077#1089' 2'
      FieldName = 'SCHARGEW_2'
      Origin = 'TMP_STONE.SCHARGEW_2'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneFQ_2: TIntegerField
      DisplayLabel = #1060#1072#1082#1090'. '#1085#1072#1083#1080#1095#1080#1077' '#1087#1086' '#1087#1088#1086#1090#1086#1082#1086#1083#1091' 2'
      FieldName = 'FQ_2'
      Origin = 'TMP_STONE.FQ_2'
      Required = True
    end
    object taCheckStoneFW_2: TFloatField
      DisplayLabel = #1060#1072#1082#1090'. '#1085#1072#1083#1080#1095#1080#1077' '#1087#1086' '#1087#1088#1086#1090#1086#1082#1086#1083#1091' '#1074#1077#1089'2'
      FieldName = 'FW_2'
      Origin = 'TMP_STONE.FW_2'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneRQ_1: TIntegerField
      DisplayLabel = #1048#1089#1093#1086#1076' '#1082#1086#1083'-'#1074#1086
      FieldName = 'RQ_1'
      Origin = 'TMP_STONE.RQ_1'
      Required = True
    end
    object taCheckStoneRW_1: TFloatField
      DisplayLabel = #1048#1089#1093#1086#1076'. '#1074#1077#1089
      FieldName = 'RW_1'
      Origin = 'TMP_STONE.RW_1'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneRQ_2: TIntegerField
      DisplayLabel = #1048#1089#1093#1086#1076'. '#1082#1086#1083'-'#1074#1086' 2'
      DisplayWidth = 10
      FieldName = 'RQ_2'
      Origin = 'TMP_STONE.RQ_2'
      Required = True
    end
    object taCheckStoneRW_2: TFloatField
      DisplayLabel = #1048#1089#1093#1086#1076'. '#1074#1077#1089' 2'
      FieldName = 'RW_2'
      Origin = 'TMP_STONE.RW_2'
      Required = True
      DisplayFormat = '0.###'
    end
    object taCheckStoneRQ: TIntegerField
      DisplayLabel = #1056#1072#1079#1085#1080#1094#1072' '#1082#1086#1083'-'#1074#1086
      FieldKind = fkCalculated
      FieldName = 'RQ'
      Calculated = True
    end
    object taCheckStoneRW: TFloatField
      DisplayLabel = #1056#1072#1079#1085#1080#1094#1072' '#1074#1077#1089
      FieldKind = fkCalculated
      FieldName = 'RW'
      DisplayFormat = '0.###'
      Calculated = True
    end
  end
  object dsrCheckStone: TDataSource
    DataSet = taCheckStone
    Left = 764
    Top = 464
  end
  object taMatrixProd: TpFIBDataSet
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 344
    Top = 416
  end
  object dsrMatrixProd: TDataSource
    DataSet = taMatrixProd
    Left = 344
    Top = 464
  end
  object taB_UID: TpFIBDataSet
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 408
    Top = 416
  end
  object dsrB_UID: TDataSource
    DataSet = taB_UID
    Left = 412
    Top = 464
  end
  object taWhTails: TpFIBDataSet
    SelectSQL.Strings = (
      'select wh.ID, wh.TAILSID, wh.W, wh.UW, t.NAME TAILNAME,'
      '   case  '
      '     when (wh.DEPID is null) then '#39#1085#1077' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#39
      '     else d.NAME '
      '   end DEPNAME '
      'from WhTails wh left join D_Dep d on (wh.DEPID=d.DEPID), '
      '   D_Tails t'
      'where wh.TAILSID=t.ID '
      'order by t.NAME')
    CacheModelOptions.BufferChunks = 1000
    AfterOpen = CommitRetaininig
    Transaction = dm.tr
    Database = dm.db
    Description = #1057#1082#1083#1072#1076' '#1089#1098#1077#1084#1072
    Left = 376
    Top = 308
    object taWhTailsID: TIntegerField
      FieldName = 'ID'
      Origin = 'WHTAILS.ID'
      Required = True
    end
    object taWhTailsTAILSID: TIntegerField
      FieldName = 'TAILSID'
      Origin = 'WHTAILS.TAILSID'
      Required = True
    end
    object taWhTailsW: TFloatField
      DefaultExpression = '0'
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'WHTAILS.W'
      Required = True
      DisplayFormat = '0.###'
    end
    object taWhTailsUW: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'UW'
      Origin = 'WHTAILS.UW'
      Required = True
      OnGetText = UWGetText
    end
    object taWhTailsTAILNAME: TFIBStringField
      DisplayLabel = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      FieldName = 'TAILNAME'
      Origin = 'D_TAILS.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWhTailsDEPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      FieldName = 'DEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrWhTails: TDataSource
    DataSet = taWhTails
    Left = 376
    Top = 356
  end
  object taInsBuffer: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    USERID,'
      '    INSID,'
      '    Q,'
      '    W,'
      '    COMPID'
      'FROM'
      '    B_INS '
      'where USERID=:USERID')
    BeforeOpen = taInsBufferBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 636
    Top = 52
    poSQLINT64ToBCD = True
    object taInsBufferUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object taInsBufferINSID: TFIBStringField
      FieldName = 'INSID'
      Size = 10
      EmptyStrToNull = True
    end
    object taInsBufferQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object taInsBufferW: TFIBFloatField
      FieldName = 'W'
    end
    object taInsBufferCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
  end
  object taInvSubType: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, NAME, INVTYPEID, T0'
      'from D_INVSUBTYPE'
      'where INVTYPEID=:ITYPE and'
      '      T0 between :SCHEMAID1 and :SCHEMAID2 '
      'order by NAME')
    BeforeOpen = taInvSubTypeBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 780
    Top = 300
    poSQLINT64ToBCD = True
  end
  object dsrInvSubType: TDataSource
    DataSet = taInvSubType
    Left = 780
    Top = 352
  end
  object taDIAssortEl: TpFIBDataSet
    SelectSQL.Strings = (
      'select e.ID, e.INVID, e.ARTID, e.ART2ID, e.SZ, e.Q,'
      
        '       a2.Art, a2.Art2, sz.Name SZNAME, e.T, e.OperId, o.Operati' +
        'on,'
      '       e.OperIdFrom, e.Old_Art2Id, e.U, e.OLD_U, e.CommentId,'
      '       e.Q*e.U Q1'
      'from AEl e, Art2 a2, D_Sz sz, D_Oper o'
      'where e.InvId = :INVID and'
      '      a2.Art2Id=e.Art2Id and'
      '      e.Sz=sz.Id and'
      '      e.operId=o.OperId')
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    OnCalcFields = CalcRecNo
    Transaction = dm.tr
    Database = dm.db
    DataSource = dsrDIAssortInv
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 672
    Top = 100
    poSQLINT64ToBCD = True
    WaitEndMasterScroll = True
    dcForceOpen = True
    object taDIAssortElRecNo: TIntegerField
      DisplayLabel = '#'
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taDIAssortElID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taDIAssortElINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taDIAssortElARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object taDIAssortElART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taDIAssortElSZ: TFIBIntegerField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
    end
    object taDIAssortElQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taDIAssortElART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taDIAssortElART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taDIAssortElT: TFIBSmallIntField
      FieldName = 'T'
    end
    object taDIAssortElOPERID: TFIBStringField
      FieldName = 'OPERID'
      Size = 10
      EmptyStrToNull = True
    end
    object taDIAssortElOPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = True
    end
    object taDIAssortElOPERIDFROM: TFIBStringField
      FieldName = 'OPERIDFROM'
      Size = 10
      EmptyStrToNull = True
    end
    object taDIAssortElOLD_ART2ID: TFIBIntegerField
      FieldName = 'OLD_ART2ID'
    end
    object taDIAssortElU: TFIBSmallIntField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'.'
      FieldName = 'U'
      OnGetText = UGetText
    end
    object taDIAssortElOLD_U: TFIBSmallIntField
      FieldName = 'OLD_U'
    end
    object taDIAssortElCOMMENTID: TFIBSmallIntField
      FieldName = 'COMMENTID'
    end
    object taDIAssortElSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      EmptyStrToNull = True
    end
    object taDIAssortElQ1: TFIBBCDField
      FieldName = 'Q1'
      Size = 0
      RoundByScale = True
    end
  end
  object dsrDIAssort: TDataSource
    DataSet = taDIAssortEl
    Left = 672
    Top = 144
  end
  object taDIAssortInv: TpFIBDataSet
    SelectSQL.Strings = (
      'select INVID, DOCNO, DOCDATE'
      'from Inv'
      'where INVID=:REF')
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeOpen = taDIAssortInvBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 600
    Top = 100
    poSQLINT64ToBCD = True
    object taDIAssortInvDOCNO: TFIBIntegerField
      FieldName = 'DOCNO'
    end
    object taDIAssortInvDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object taDIAssortInvINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
  end
  object dsrDIAssortInv: TDataSource
    DataSet = taDIAssortInv
    Left = 600
    Top = 144
  end
  object dsrPenalty: TDataSource
    Left = 312
    Top = 56
  end
  object taPenalty: TpFIBDataSet
    UpdateSQL.Strings = (
      'update inv set penalty = :penalty where invid = :invid')
    SelectSQL.Strings = (
      'SELECT'
      '    INVID,'
      '    DOCNO,'
      '    DOCDATE,'
      '    COST,'
      '    PENALTY'
      'FROM'
      '    INV$PENALTY$S'
      '    ('
      '      :Mode,'
      '      :SELF$COMPANY$ID,'
      '      :COMPANY$ID,'
      '      :INVOICE$ID'
      '     )')
    AfterPost = taPenaltyAfterPost
    BeforeOpen = taPenaltyBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 312
    Top = 8
    object taPenaltyINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taPenaltyDOCNO: TFIBIntegerField
      FieldName = 'DOCNO'
    end
    object taPenaltyDOCDATE: TFIBDateField
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taPenaltyCOST: TFIBFloatField
      FieldName = 'COST'
      DisplayFormat = '0.00'
    end
    object taPenaltyPENALTY: TFIBFloatField
      FieldName = 'PENALTY'
      DisplayFormat = '0.00'
    end
  end
  object taSOEl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update SIEl set'
      '  SEMIS=:SEMIS,'
      '  Q=:Q,'
      '  W=:W,'
      '  NDS=:NDS,'
      '  NDSID=:NDSID,'
      '  TPRICE=:TPRICE,'
      '  COST=:COST,'
      '  OPERID=:OPERID,'
      '  FOR_Q_U=:FOR_Q_U, '
      '  REF=:REF'
      'where Id=:OLD_Id')
    DeleteSQL.Strings = (
      'delete from SIEl where ID=:OLD_ID')
    InsertSQL.Strings = (
      '')
    RefreshSQL.Strings = (
      
        'select se.ID, se.INVID, se.SEMIS,  se.Q, se.W, se.TPRICE, se.PRI' +
        'CE,'
      
        '          se.NDSID, se.NDS, se.T, se.COST, se.OPERID, se.FOR_Q_U' +
        ', se.REF'
      'from SIEl se'
      'where  se.ID=:ID')
    SelectSQL.Strings = (
      'select '
      
        '   se.ID, se.INVID, se.SEMIS,  se.Q, se.W, se.TPRICE, se.PRICE, ' +
        '               '
      '   se.NDSID, nds.nds, se.T, se.COST, se.OPERID, se.FOR_Q_U,'
      '   se.REF '
      'from SIEl se, D_Nds nds'
      'where se.InvId=:INVID and'
      '      se.NdsId=nds.NdsId'
      'order by se.Id')
    AfterDelete = CommitRetaininig
    AfterPost = CommitRetaininig
    BeforeClose = PostBeforeClose
    BeforeDelete = DelConf
    BeforeEdit = SOCheckClose
    BeforeOpen = taSOElBeforeOpen
    OnCalcFields = taSOElCalcFields
    OnNewRecord = taSOElNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 560
    Top = 192
    object taSOElID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taSOElINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taSOElSEMIS: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMIS'
      Size = 10
      EmptyStrToNull = True
    end
    object taSOElQ: TFIBFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      OnChange = taSOElQChange
    end
    object taSOElW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      OnChange = taSOElWChange
    end
    object taSOElTPRICE: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'TPRICE'
      OnChange = taSOElTPRICEChange
    end
    object taSOElNDSID: TFIBIntegerField
      FieldName = 'NDSID'
    end
    object taSOElPRICE: TFIBFloatField
      FieldName = 'PRICE'
    end
    object taSOElT: TFIBSmallIntField
      FieldName = 'T'
    end
    object taSOElNDS: TFIBFloatField
      DisplayLabel = #1053#1044#1057
      FieldName = 'NDS'
    end
    object taSOElCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taSOElOPERID: TFIBStringField
      FieldName = 'OPERID'
      Size = 10
      EmptyStrToNull = True
    end
    object taSOElREF: TFIBIntegerField
      FieldName = 'REF'
    end
    object taSOElFOR_Q_U: TFIBSmallIntField
      DisplayLabel = #1062#1077#1085#1072' '#1079#1072' '#1082#1086#1083'-'#1074#1086'\'#1074#1077#1089
      FieldName = 'FOR_Q_U'
      OnChange = taSOElFOR_Q_UChange
    end
    object taSOElOPERNAME: TStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldKind = fkLookup
      FieldName = 'OPERNAME'
      LookupDataSet = dm.taOper
      LookupKeyFields = 'OPERID'
      LookupResultField = 'OPERATION'
      KeyFields = 'OPERID'
      Transliterate = False
      Lookup = True
    end
    object taSOElSEMISNAME: TStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldKind = fkLookup
      FieldName = 'SEMISNAME'
      LookupDataSet = dm.taSemis
      LookupKeyFields = 'SEMISID'
      LookupResultField = 'SEMIS'
      KeyFields = 'SEMIS'
      Transliterate = False
      Lookup = True
    end
    object taSOElUQ: TFIBStringField
      FieldKind = fkCalculated
      FieldName = 'UQ'
      EmptyStrToNull = True
      Calculated = True
    end
    object taSOElUW: TFIBStringField
      FieldKind = fkCalculated
      FieldName = 'UW'
      EmptyStrToNull = True
      Calculated = True
    end
    object taSOElFORQUNAME: TFIBStringField
      FieldKind = fkLookup
      FieldName = 'FORQUNAME'
      KeyFields = 'FOR_Q_U'
      EmptyStrToNull = True
      Lookup = True
    end
  end
  object DataSetFilterContract: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  ContractID,'
      '  ContractName,'
      '  AccountingType'
      'from'
      '  filter$contract')
    AllowedUpdateKinds = []
    Transaction = dm.tr
    Database = dm.db
    Left = 888
    Top = 192
    oRefreshAfterPost = False
    oFetchAll = True
    object DataSetFilterContractCONTRACTID: TFIBIntegerField
      FieldName = 'CONTRACTID'
    end
    object DataSetFilterContractCONTRACTNAME: TFIBStringField
      FieldName = 'CONTRACTNAME'
      Size = 80
      EmptyStrToNull = True
    end
    object DataSetFilterContractACCOUNTINGTYPE: TFIBIntegerField
      FieldName = 'ACCOUNTINGTYPE'
    end
  end
  object DataSourceFilterContract: TDataSource
    DataSet = DataSetFilterContract
    Left = 888
    Top = 240
  end
end
