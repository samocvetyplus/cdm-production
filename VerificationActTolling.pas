unit VerificationActTolling;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxEdit,
  dxBar, cxClasses, ImgList, cxVGrid, cxDBVGrid, cxInplaceContainer,
  cxCustomData, cxFilter, cxData, cxDataStorage, DB,
  cxDBData, cxSplitter, dxBarExtItems, ActnList,
  dxStatusBar, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPSCore, FIBDataSet, FIBDatabase, pFIBDatabase,
  pFIBDataSet, FIBQuery, pFIBQuery, Math, FR_DSet, FR_DBSet, FR_Class;

type
  TfmVerificationActTolling = class(TForm)
    GridActHeader: TcxDBVerticalGrid;
    GridActHeaderCategoryRow1: TcxCategoryRow;
    GridActHeaderDBEditorRow1: TcxDBEditorRow;
    GridActHeaderDBEditorRow2: TcxDBEditorRow;
    GridActHeaderDBEditorRow3: TcxDBEditorRow;
    GridActHeaderCategoryRow2: TcxCategoryRow;
    GridActHeaderDBEditorRow4: TcxDBEditorRow;
    GridActHeaderDBEditorRow5: TcxDBEditorRow;
    GridActHeaderCategoryRow3: TcxCategoryRow;
    GridActHeaderDBEditorRow6: TcxDBEditorRow;
    GridActHeaderDBEditorRow7: TcxDBEditorRow;
    BarManager: TdxBarManager;
    ImageList: TImageList;
    BarManagerBar2: TdxBar;
    dxStatusBar1: TdxStatusBar;
    Actions: TActionList;
    acPrint: TAction;
    acExit: TAction;
    acRecalc: TAction;
    dxPrintButton: TdxBarLargeButton;
    dxRecalcActButton: TdxBarLargeButton;
    dxPeriodButton: TdxBarLargeButton;
    PeriodBeginDateCombo: TdxBarDateCombo;
    PeriodEndDateCombo: TdxBarDateCombo;
    dxExitButton: TdxBarLargeButton;
    Splitter: TcxSplitter;
    Transaction: TpFIBTransaction;
    DataSourceActItems: TDataSource;
    MainGridView: TcxGridDBTableView;
    MainGridLevel: TcxGridLevel;
    MainGrid: TcxGrid;
    GridViewDocDate: TcxGridDBColumn;
    GridViewDocNo: TcxGridDBColumn;
    GridViewMovementType: TcxGridDBColumn;
    GridViewWeight: TcxGridDBColumn;
    GridViewCleanWeight: TcxGridDBColumn;
    GridViewLossesPercent: TcxGridDBColumn;
    DataSetActTotal: TpFIBDataSet;
    GridViewSuplierName: TcxGridDBColumn;
    DataSetActTotalWEIGHT: TFIBBCDField;
    DataSetActTotalMOVEMENTTYPE: TFIBStringField;
    DataSetActTotalSUPLIERNAME: TFIBStringField;
    DataSetActTotalCleanWeight: TFloatField;
    DataSetActTotalDOCNO: TFIBSmallIntField;
    DataSetActTotalDOCDATE: TFIBDateTimeField;
    DataSetActTotalLOSSESPERCENT: TFIBFloatField;
    DataSetActTotalWEIGHTBYRETURN: TFIBBCDField;
    MainGridViewWEIGHTBYRETURN: TcxGridDBColumn;
    DataSetActTotalCOST: TFIBBCDField;
    MainGridViewCOST: TcxGridDBColumn;
    procedure acExitExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acRecalcExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure DataSetActTotalBeforeOpen(DataSet: TDataSet);
    procedure MainGridViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure DataSetActTotalCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure Execute;
  end;

var
  fmVerificationActTolling: TfmVerificationActTolling;

implementation

{$R *.dfm}
uses Data, dbUtil, DateUtils, DictData, MainData, PrintData, ListAncestor;



procedure TfmVerificationActTolling.acExitExecute(Sender: TObject);
begin
  if DataSetActTotal.Transaction.Active then  DataSetActTotal.Transaction.Commit;
  DataSetActTotal.Close;
  Close;
end;

procedure TfmVerificationActTolling.acPrintExecute(Sender: TObject);
begin
  with dmPrint do
  begin
    FDocID := 103;
    taInvHeader.close;
    taInvHeader.ParamByName('invid').AsInteger:=dmData.taVerificationListTollingINVID.AsInteger;
    taInvHeader.Open;
    frReport.Dataset := frActTolling;
    taDoc.Open;
    frReport.LoadFromDB(taDoc, FDocID);
    dm.DocPreview:=dm.GetDocPreview;
    if dm.DocPreview=pvDesign
      then
        frReport.DesignReport
      else
        frReport.ShowReport;
    taDoc.Close;
  end;
end;

procedure TfmVerificationActTolling.acRecalcExecute(Sender: TObject);
var ActID: Integer;
begin
  if PeriodBeginDateCombo.Date > PeriodEndDateCombo.Date then
    begin
      MessageDlg('������ ������� �� ����� ���� ����� �����!', mtError, [mbOk], 0);
      SysUtils.Abort;
    end;
  if (PeriodBeginDateCombo.Date = dmData.taVerificationListTollingABD.AsDateTime) and (PeriodEndDateCombo.Date = dmData.taVerificationListTollingAED.AsDateTime) then
    if MessageDlg('����� ������ ������� ����� �������. ������� ����������?', mtInformation,[mbYes, mbNo], 0) <> mrYes then
      SysUtils.Abort;

  ActID := dmData.taVerificationListTollingINVID.AsInteger;

  try
    try
      Screen.Cursor := crSQLWait;
      with dmData.taVerificationListTolling do
        begin
          Edit;
          FieldByName('ABD').AsDateTime := PeriodBeginDateCombo.Date;
          FieldByName('AED').AsDateTime := PeriodEndDateCombo.Date;
          Post;
          Close;
          Open;
          Locate('Invid', ActID,[]);
        end;
        DataSourceActItems.DataSet.Close;
        DataSourceActItems.DataSet.Open;
    finally
      Screen.Cursor := crDefault;
    end;
  except
    On E:Exception do ShowMessage(E.Message);
  end;
end;


procedure TfmVerificationActTolling.DataSetActTotalBeforeOpen(
  DataSet: TDataSet);
var SuplierID: integer;
begin
  SuplierID := dmData.taVerificationListTollingSUPID.AsInteger;

  DataSetActTotal.ParamByName('PeriodBegin').AsDateTime := dmData.taVerificationListTollingABD.AsDatetime;
  DataSetActTotal.ParamByName('PeriodEnd').AsDateTime := dmData.taVerificationListTollingAED.AsDateTime;

  if SuplierID = 0 then
    begin
      DataSetActTotal.ParamByName('SQL$Param').AsString := '';
      DataSetActTotal.SelectSQL.Strings[4] := 'order by Suplier$Name';
    end
  else
    begin
      DataSetActTotal.ParamByName('SQL$Param').AsString := 'and CompID = ' + IntToStr(SuplierID);
      DataSetActTotal.SelectSQL.Strings[4] := 'order by DocDate';
    end;
end;

procedure TfmVerificationActTolling.DataSetActTotalCalcFields(
  DataSet: TDataSet);
begin
  DataSetActTotal.FieldByName('CleanWeight').AsFloat := RoundTo(DataSetActTotal.FieldByName('Weight').AsFloat * 0.585, -3);
end;

class procedure TfmVerificationActTolling.Execute;
begin
  fmVerificationActTolling := nil;
  try
    fmVerificationActTolling := TfmVerificationActTolling.Create(Application);
    fmVerificationActTolling.ShowModal;
  finally
    FreeAndNil(fmVerificationActTolling);
  end;
end;

procedure TfmVerificationActTolling.FormCreate(Sender: TObject);
var
  SupID: Integer;
begin

  PeriodBeginDateCombo.Date := StartOfTheYear(Date);
  PeriodEndDateCombo.Date := Date;

  SupID := dmData.taVerificationListTollingSupID.AsInteger;

  if SupID = 0 then
    begin
      GridViewDocDate.Visible := false;
      GridViewDocNo.Visible := false;
      GridViewLossesPercent.Visible := false;

      GridViewSuplierName.Visible := true;
      GridViewSuplierName.SortIndex := 0;
    end;

   dmPrint.frActTolling.DataSet := DataSourceActItems.DataSet;
end;

procedure TfmVerificationActTolling.MainGridViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  if (AViewInfo.GridRecord.Values[GridViewMovementType.Index] = '�������� �������') then
    ACanvas.Brush.Color := clBtnFace;
  if (AViewInfo.GridRecord.Values[GridViewMovementType.Index] = '��������� �������') then
    ACanvas.Brush.Color := clBtnShadow;
end;

end.
