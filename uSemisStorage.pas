unit uSemisStorage;

interface

type

  TStorage = class;

  TStorageElement = class(TObject)
  private
    procedure Connect;
    procedure Disconnect;
    function GetConnected: Boolean;
    procedure SetCode(const Value: string);
    procedure SetConnected(const Value: Boolean);
    procedure SetOperation(const Value: string);
    procedure SetUnitWeight(const Value: Integer);
    procedure SetUnitQuantity(const Value: Integer);
  public
    FID: Integer;
    FCode: string;
    FOperation: string;
    FUnitQuantity: Integer;
    FUnitWeight: Integer;
    FStorage: TStorage;
  public
    constructor Create(AStorage: TStorage);
    property ID: Integer read FID;
    property Code: string read FCode write SetCode;
    property Operation: string read FOperation write SetOperation;
    property UnitQuantity: Integer read FUnitQuantity write SetUnitQuantity;
    property UnitWeight: Integer read FUnitWeight write SetUnitWeight;
    property Storage: TStorage read FStorage;
    property Connected: Boolean read GetConnected write SetConnected;
  end;


  TSpoilageDelta = record
    StorageElementID: Integer;
    InvoiceElementID: Integer;
    OperationID: Integer;
    Quantity: Integer;
    Weight: Double;
  end;

  TStorage = class(TObject)
  private
    FID: Integer;
    FTitle: string;
    FElement: TStorageElement;
    FConnected: Boolean;
    procedure SetID(const Value: Integer);
    function GetConnected: Boolean;
    procedure SetConnected(const Value: Boolean);
    procedure Connect;
    procedure Disconnect;
  public
    constructor Create;
    destructor Destroy; override;
    property Connected: Boolean read GetConnected write SetConnected;
    property ID: Integer read FID write SetID;
    property Title: string read FTitle;
    property Element: TStorageElement read FElement;
  end;

  TDialogSemisMode = (smOut, smIn);  


implementation

uses SysUtils,
     pFIBQuery, pFIBStoredProc,
     DictData;


{ TStorage }

procedure TStorage.Connect;
var
  Query: TpFIBQuery;
begin
  Query := TpFIBQuery.Create(nil);
  Query.Database := dm.db;
  Query.Transaction := dm.tr;
  Query.SQL.Text := 'select stretrim(name) title from d_dep where depid = ' + IntToStr(ID);
  Query.ExecQuery;
  if Query.RecordCount <> 0 then
  begin
    FConnected := True;
    FTitle := Trim(Query.FldByName['Title'].AsString);
  end
  else FID := FID;
  Query.Free;
end;

constructor TStorage.Create;
begin
  FElement := TStorageElement.Create(Self);
end;

destructor TStorage.Destroy;
begin
  Element.Free;
  inherited Destroy;
end;

procedure TStorage.Disconnect;
begin
  FConnected := False;
  Element.Connected := False;
end;

function TStorage.GetConnected: Boolean;
begin
  Result := FConnected;
end;

procedure TStorage.SetConnected(const Value: Boolean);
begin
  if FID <> 0 then
  begin
    if Value then Connect
    else Disconnect;
  end;
end;

procedure TStorage.SetID(const Value: Integer);
begin
  if Value <> ID then
  begin
    FID := Value;
    FTitle := '';
    FElement.Disconnect;
  end;
end;

{ TStorageElement }

constructor TStorageElement.Create(AStorage: TStorage);
begin
  FStorage := AStorage;
end;

procedure TStorageElement.Connect;
var
  StoredProc: TpFIBStoredProc;
begin
  StoredProc := TpFIBStoredProc.Create(nil);
  StoredProc.Database := dm.db;
  StoredProc.Transaction := dm.tr;
  if (Storage <> nil) and Storage.Connected then
  begin
    StoredProc.StoredProcName := 'CHECK_WHSEMIS';
    StoredProc.ParamByName('i_depid').AsInteger := Storage.ID;
    StoredProc.ParamByName('i_semisid').AsString := Code;
    StoredProc.ParamByName('i_operid').AsString := Operation;
    StoredProc.ParamByName('i_uq').AsInteger := UnitQuantity;
    StoredProc.ParamByName('i_uw').AsInteger := UnitWeight;
    StoredProc.ParamByName('i_selfcompid').AsInteger:= dm.User.SelfCompId;
    StoredProc.ExecProc;
    StoredProc.Transaction.CommitRetaining;
    FID := StoredProc.ParamByName('WHSEMISID').AsInteger;
  end;
  StoredProc.Free;
end;

procedure TStorageElement.Disconnect;
begin
  FID := 0;
end;

function TStorageElement.GetConnected: Boolean;
begin
  Result := FID <> 0;
end;

procedure TStorageElement.SetConnected(const Value: Boolean);
begin
  if Value <> Connected then
  begin
    if Value then Connect
    else Disconnect;
  end;
end;

procedure TStorageElement.SetCode(const Value: string);
begin
  if Value <> Code then
  begin
    Disconnect;
    FCode := Value;
  end;
end;

procedure TStorageElement.SetOperation(const Value: string);
begin
  if Value <> Operation then
  begin
    Disconnect;
    FOperation := Value;
  end;
end;

procedure TStorageElement.SetUnitWeight(const Value: Integer);
begin
  if Value <> UnitWeight then
  begin
    Disconnect;
    FUnitWeight := Value;
  end;
end;

procedure TStorageElement.SetUnitQuantity(const Value: Integer);
begin
  if Value <> UnitQuantity then
  begin
    Disconnect;
    FUnitQuantity := Value;
  end;
end;


end.
