unit dCode;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList, ExtCtrls, ComCtrls,
  StdCtrls, ActnList, Menus, Grids, DBGrids, RXDBCtrl, db,
  TB2Item, TB2Dock, TB2Toolbar, DBGridEh, Mask, DBCtrlsEh,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TDictInfo = class(TObject)
    Id : integer;
    DictId : integer;
    Name : string;
    DictName : string;
    IdName : string;
    DisplayName : string;
    ClDataSet : string;
  end;

  TfmCode = class(TfmAncestor)
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    ppCode: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    acEvent: TActionList;
    acN: TAction;
    acDelN: TAction;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acAddKey: TAction;
    acDelKey: TAction;
    plTree: TPanel;
    TBDock2: TTBDock;
    TBToolbar2: TTBToolbar;
    trvArtCode: TTreeView;
    TBItem3: TTBItem;
    TBItem4: TTBItem;
    gridKey: TDBGridEh;
    cmbxDict: TDBComboBoxEh;
    acEditN: TAction;
    TBItem5: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure acNExecute(Sender: TObject);
    procedure trvArtCodeExpanding(Sender: TObject; Node: TTreeNode;
      var AllowExpansion: Boolean);
    procedure cmbxDictChange(Sender: TObject);
    procedure trvArtCodeChange(Sender: TObject; Node: TTreeNode);
    procedure acDelNExecute(Sender: TObject);
    procedure acAddKeyExecute(Sender: TObject);
    procedure acDelKeyExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acEditNExecute(Sender: TObject);
    procedure acEditNUpdate(Sender: TObject);
    procedure acDelNUpdate(Sender: TObject);
    procedure acNUpdate(Sender: TObject);
  private
    function GetDictId: integer;
    function GetN: smallint;
    procedure RefreshStatement;
  public
    property N : smallint read GetN;
    property DictId : integer read GetDictId;
  end;



var
  fmCode: TfmCode;

implementation

uses DictData, dbUtil, dbTree, MsgDialog, fmUtils, ProductionConsts;

{$R *.dfm}

procedure TfmCode.FormCreate(Sender: TObject);
var
  di : TDictInfo;
  tn : TTreeNode;
begin
  cmbxDict.Items.Clear;
  OpenDataSet(dm.taDict);
  while not dm.taDict.Eof do
  begin
    di := TDictInfo.Create;
    di.Id := dm.taDictID.AsInteger;
    di.Name := dm.taDictNAME.AsString;
    di.DictId := dm.taDictDICTID.AsInteger;
    di.DictName := dm.taDictDICTNAME.AsString;
    di.DisplayName := dm.taDictDISPLAYNAME.AsString;
    di.IdName := dm.taDictIDNAME.AsString;
    di.ClDataSet := dm.taDictCLDATASET.AsString;
    cmbxDict.Items.AddObject(dm.taDictNAME.AsString, di);
    dm.taDict.Next;
  end;
  CloseDataSet(dm.taDict);
  tn := trvArtCode.Items.AddChild(nil, '�������');
  tn.HasChildren := True;
  tn.Expand(True);
end;

procedure TfmCode.acNExecute(Sender: TObject);
var
  nd : TNodeData;
  tn : TTreeNode;
  n : string;
  e : integer;
begin
  if cmbxDict.ItemIndex = -1 then raise Exception.Create('�������� ����������');
  n := InputBox('���������� ����� ��������', '����', '');
  if(n = '')then eXit;
  e := ExecSelectSQL('select count(*) from D_Code where N="'+n+'"', dm.quTmp);
  if(e > 0) then raise EWarning.Create(rsArtCodeError);
  tn := trvArtCode.Items.AddChild(trvArtCode.TopItem, n);
  if not trvArtCode.TopItem.Expanded then trvArtCode.TopItem.Expand(True);
  //tn.EditText;
  ExecSQL('insert into D_Code(Id, Dict, N) values ('+IntToStr(dm.GetId(30))+', '+
    IntToStr(TDictInfo(cmbxDict.Items.Objects[cmbxDict.ItemIndex]).Id)+', "'+n+'")', dm.quTmp);
  nd := TNodeData.Create;
  nd.Code := n;
  nd.Value := TDictInfo(cmbxDict.Items.Objects[cmbxDict.ItemIndex]).Id;
  nd.Loaded := True;
  tn.Data := nd;
end;

procedure TfmCode.trvArtCodeExpanding(Sender: TObject; Node: TTreeNode;
  var AllowExpansion: Boolean);
var
  nd : TNodeData;
  tn : TTreeNode;
begin
  if(Node.Level=0)then
  begin
    Node.DeleteChildren;
    OpenDataSet(dm.taCodeN);
    while not dm.taCodeN.Eof do
    begin
      nd := TNodeData.Create;
      nd.Code := dm.taCodeNN.AsInteger;
      nd.Value := dm.taCodeNDICT.AsInteger;
      tn := trvArtCode.Items.AddChild(Node, dm.taCodeNN.AsString);
      tn.Data := nd;
      tn.HasChildren := False;
      nd.Loaded := False;
      dm.taCodeN.Next;
    end;
    CloseDataSet(dm.taCodeN);
  end;
end;

procedure TfmCode.cmbxDictChange(Sender: TObject);
begin
  if  trvArtCode.Selected.Level = 1 then
  begin
    if MessageDialog('���������� ������� ��������� ��������?', mtConfirmation, [mbOk, mbCancel], 0)=mrOk
    then begin
      ExecSQL('delete from D_Code where N='+IntToStr(TNodeData(trvArtCode.Selected.Data).Code), dm.quTmp);
      TNodeData(trvArtCode.Selected.Data).Value := TDictInfo(cmbxDict.Items.Objects[cmbxDict.ItemIndex]).Id;
      CloseDataSet(dm.taCode);
      RefreshStatement;
      OpenDataSet(dm.taCode);
    end
    else SysUtils.Abort;
  end;
end;

procedure TfmCode.trvArtCodeChange(Sender: TObject; Node: TTreeNode);
var
  i : integer;
begin
  case Node.Level of
    0 : begin
      PostDataSet(dm.taCode);
      dm.taCode.Transaction.CommitRetaining;
      CloseDataSet(dm.taCode);
    end;
    1 : begin
      PostDataSet(dm.taCode);
      dm.taCode.Transaction.CommitRetaining;
      cmbxDict.ItemIndex := -1;
      // ������������ �������
      for i:=0 to Pred(cmbxDict.Items.Count) do
        if TDictInfo(cmbxDict.Items.Objects[i]).Id = TNodeData(Node.Data).Value
        then begin
          cmbxDict.ItemIndex := i;
          break;
        end;

      if cmbxDict.ItemIndex = -1 then raise Exception.Create('internal: �� ������ ����������');
      CloseDataSet(dm.taCode);
      RefreshStatement;
      OpenDataSet(dm.taCode);
    end
  end;
end;

procedure TfmCode.acDelNExecute(Sender: TObject);
begin
  if MessageDialog('�������?', mtConfirmation, [mbOk, mbCancel], 0)=mrOk then
    if Assigned(trvArtCode.Selected)and Assigned(trvArtCode.Selected.Data)then
    begin
      ExecSQL('delete from D_Code where N='+IntToStr(TNodeData(trvArtCode.Selected.Data).Code), dm.quTmp);
      trvArtCode.Selected.Delete;
    end;
end;

procedure TfmCode.acAddKeyExecute(Sender: TObject);
begin
  dm.taCode.Insert;
end;

procedure TfmCode.acDelKeyExecute(Sender: TObject);
begin
  dm.taCode.Delete;
end;

procedure TfmCode.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PostDataSet(dm.taCode);
  dm.taCode.Transaction.CommitRetaining;
  dm.InitArtCode;
  inherited;
end;

function TfmCode.GetDictId: integer;
begin
  with cmbxDict do
    Result := TDictInfo(Items.Objects[ItemIndex]).DictId;
end;

function TfmCode.GetN: smallint;
begin
  if Assigned(trvArtCode.Selected)and Assigned(trvArtCode.Selected.Data)
  then Result := TNodeData(trvArtCode.Selected.Data).Code
  else raise Exception.Create('TODO!');
end;

procedure TfmCode.RefreshStatement;
var
  di : TDictInfo;
  Node: TTreeNode;
begin
  di := TDictInfo(cmbxDict.Items.Objects[cmbxDict.ItemIndex]);
  Node := trvArtCode.Selected;
  dm.taCode.SelectSQL.Text := 'select c.Id, c.Dict, c.N, c.K, c.VAL, C.PCOEF from D_Code c left join '+
    di.DictName+' d on (c.Val=d.'+di.IdName+') where c.N='+IntToStr(TNodeData(Node.Data).Code)+
    ' order by c.K';
  dm.taCodeVALNAME.LookupDataSet := TDataSet(dm.FindComponent(di.ClDataSet));
  dm.taCodeVALNAME.LookupKeyFields := di.IdName;
  dm.taCodeVALNAME.LookupResultField := di.DisplayName;
  dm.taCode.InsertSQL.Text := 'insert into D_Code(Id, N, Dict, K, Val, PCOEF) '+
    'values(:ID, :N, :Dict, :K, :Val, :PCOEF)';
  dm.taCode.UpdateSQL.Text := 'update D_Code set K=:K, VAL=:VAL, PCOEF=:PCOEF where Id=:OLD_Id';
  dm.taCode.DeleteSQL.Text := 'delete from D_Code where Id=:OLD_Id';
  (*dm.taCode.RefreshSQL.Text := 'select c.Id, c.Dict, c.N, c.K, c.VAL from D_Code � left join '+
    di.DictName+' d on (c.Val=d.'+di.IdName+') where c.Id=:ID';*)
end;

procedure TfmCode.acEditNExecute(Sender: TObject);
var
  n : string;
  e : integer;
begin
  n := InputBox('��������� ����� ��������', '����', trvArtCode.Selected.Text);
  if(n = '')or(n = trvArtCode.Selected.Text)then eXit;
  e := ExecSelectSQL('select count(*) from D_Code where N="'+n+'"', dm.quTmp);
  if(e > 0) then raise EWarning.Create(rsArtCodeError);
  ExecSQL('update D_Code set N='+trvArtCode.Selected.Text+' where N="'+n+'"', dm.quTmp);
end;

procedure TfmCode.acEditNUpdate(Sender: TObject);
begin
  acEditN.Enabled := Assigned(trvArtCode.Selected) and
                     (trvArtCode.Selected.Level <> 0);
end;

procedure TfmCode.acDelNUpdate(Sender: TObject);
begin
  acDelN.Enabled := Assigned(trvArtCode.Selected) and
                    (trvArtCode.Selected.Level <> 0);
end;

procedure TfmCode.acNUpdate(Sender: TObject);
begin
  if Assigned(trvArtCode.Selected)and(trvArtCode.Selected.Level = 0) then acN.Enabled := False
  else acN.Enabled := True;
end;

end.
