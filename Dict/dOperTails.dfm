inherited fmOperTails: TfmOperTails
  Caption = #1054#1090#1093#1086#1076#1099' '#1086#1087#1077#1088#1072#1094#1080#1080
  PixelsPerInch = 96
  TextHeight = 13
  inherited plDict: TPanel
    inherited plSelect: TPanel
      inherited btnOk: TSpeedButton
        Left = 242
      end
    end
    inherited gridDict: TDBGridEh
      AllowedOperations = [alopUpdateEh]
      AutoFitColWidths = True
      DataSource = dm.dsrOperTail
      FrozenCols = 2
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Width = 30
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERATION'
          Footers = <>
          Width = 131
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SEMISNAME'
          Footers = <>
          Width = 132
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          DropDownRows = 15
          DropDownSpecRow.ShortCut = 16573
          DropDownSpecRow.Visible = True
          EditButtons = <>
          FieldName = 'MATNAME'
          Footers = <>
          Width = 111
        end>
    end
  end
end
