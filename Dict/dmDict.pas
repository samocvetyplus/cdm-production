unit dmDict;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList, Db, IBDatabase, IBCustomDataSet, IBSQL, AppEvnts;

type
  Tdm = class(TDataModule)
    ilButtons: TImageList;
    db: TIBDatabase;
    tr: TIBTransaction;
    taOper: TIBDataSet;
    dsrOper: TDataSource;
    taOperRecNo: TIntegerField;
    taOperOPERATION: TIBStringField;
    taOperSOPER: TIBStringField;
    taSemis: TIBDataSet;
    dsrSemis: TDataSource;
    dsrMOL: TDataSource;
    taSemisSEMIS: TIBStringField;
    taSemisSNAME: TIBStringField;
    taSemisRecNo: TIntegerField;
    quTmp: TIBSQL;
    quGetID: TIBSQL;
    taDep: TIBDataSet;
    dsrDep: TDataSource;
    taComp: TIBDataSet;
    dsrComp: TDataSource;
    taCompCODE: TIBStringField;
    taCompSNAME: TIBStringField;
    taCompPOSTINDEX: TIBStringField;
    taCompCOUNTRY: TIBStringField;
    taCompCITY: TIBStringField;
    taCompADDRESS: TIBStringField;
    taCompPHONE: TIBStringField;
    taCompFAX: TIBStringField;
    taCompEMAIL: TIBStringField;
    taCompWWW: TIBStringField;
    taCompBOSSFIO: TIBStringField;
    taCompBOSSPHONE: TIBStringField;
    taCompBUHFIO: TIBStringField;
    taCompBUHPHONE: TIBStringField;
    taCompOFIO: TIBStringField;
    taCompOFIOPHONE: TIBStringField;
    taCompINN: TIBStringField;
    taCompBIK: TIBStringField;
    taCompOKPO: TIBStringField;
    taCompOKONH: TIBStringField;
    taCompBANK: TIBStringField;
    taCompBILL: TIBStringField;
    taCompKBANK: TIBStringField;
    taCompKBILL: TIBStringField;
    taCompADRBILL: TIBStringField;
    taCompPASPSER: TIBStringField;
    taCompPASPNUM: TIBStringField;
    taCompDISTRDATE: TDateTimeField;
    taCompDISTRPLACE: TIBStringField;
    taCompNDOG: TIBStringField;
    taCompDOGBD: TDateTimeField;
    taCompDOGED: TDateTimeField;
    taCompCERTN: TIBStringField;
    taCompCERTDATE: TDateTimeField;
    taCompREUED: TDateTimeField;
    taCompSELLER: TSmallintField;
    taCompBUYER: TSmallintField;
    taCompLAWFACE: TSmallintField;
    taCompPHISFACE: TSmallintField;
    taCompRecNo: TIntegerField;
    taCompCOMPID: TIntegerField;
    taRec: TIBDataSet;
    dsrRec: TDataSource;
    taRecRECID: TIntegerField;
    taRecCOMPID: TIntegerField;
    taRecNAME: TIBStringField;
    taDepDEPID: TIntegerField;
    taDepPARENT: TIntegerField;
    taDepCHILD: TIntegerField;
    taDepCOLOR: TIntegerField;
    quDepT: TIBSQL;
    taDepNAME: TIBStringField;
    taDepSNAME: TIBStringField;
    taCompNAME: TIBStringField;
    taRecDOCPREVIEW: TSmallintField;
    taSemisSEMISID: TIBStringField;
    taOperOPERID: TIBStringField;
    quSeq: TIBDataSet;
    quSeqT: TIBSQL;
    dsrSeq: TDataSource;
    quSeqInfo: TIBDataSet;
    dsrSeqInfo: TDataSource;
    quSeqMATID: TIBStringField;
    quSeqOPERATION: TIBStringField;
    quSeqPARENT: TIntegerField;
    quSeqCHILD: TIntegerField;
    quSeqART2ID: TIntegerField;
    quFSeq: TIBSQL;
    quSeqSEQID: TIntegerField;
    quDep: TIBDataSet;
    quDepDEPID: TIntegerField;
    quDepNAME: TIBStringField;
    quDepSNAME: TIBStringField;
    quDepCOLOR: TIntegerField;
    aplev: TApplicationEvents;
    taGr: TIBDataSet;
    dsrGr: TDataSource;
    taGrGRID: TIBStringField;
    taGrGR: TIBStringField;
    taGrRecNo: TIntegerField;
    taSemisGR: TIBStringField;
    taSemisGrName: TStringField;
    taRecDOCPATH: TIBStringField;
    taOperRATE: TFloatField;
    taSemisUQ: TSmallintField;
    taUnit: TIBDataSet;
    taUnitU: TIBStringField;
    dsrUnit: TDataSource;
    taUnitUNITID: TIBStringField;
    taOperS: TFloatField;
    taOperLOSS: TFloatField;
    taSemisUW: TSmallintField;
    taOperISCX: TSmallintField;
    taCxOper: TIBDataSet;
    dsrCxOper: TDataSource;
    taOperU: TIBStringField;
    taCxOperRecNo: TIntegerField;
    taCxOperOPERM: TIBStringField;
    taCxOperOPERATIONM: TIBStringField;
    taCxOperOPERSL: TIBStringField;
    taCxOperOPERATIONSL: TIBStringField;
    taCxOperCXOPERID: TIntegerField;
    taOperSimple: TIBDataSet;
    dsrOperSimple: TDataSource;
    taOperSimpleOPERID: TIBStringField;
    taOperSimpleOPERATION: TIBStringField;
    taOperSimpleSOPER: TIBStringField;
    taOperSimpleRATE: TFloatField;
    taOperSimpleU: TIBStringField;
    taOperSimpleS: TFloatField;
    taOperSimpleLOSS: TFloatField;
    taOperSimpleISCX: TSmallintField;
    taCxOperSEMIS: TIBStringField;
    taMOL: TIBDataSet;
    taMOLMOLID: TIntegerField;
    taMOLFNAME: TIBStringField;
    taMOLLNAME: TIBStringField;
    taMOLMNAME: TIBStringField;
    taMOLPSWD: TIBStringField;
    taMOLSHEETNO: TSmallintField;
    taMOLRecNo: TIntegerField;
    taMOLFIO: TIBStringField;
    taMOLDEPID: TIntegerField;
    taRecBEGINDATE: TDateTimeField;
    taRecENDDATE: TDateTimeField;
    taRecSHOWKINDDOC: TSmallintField;
    quRecTmp: TIBSQL;
    trRec: TIBTransaction;
    quDep1: TIBDataSet;
    quDep1DEPID: TIntegerField;
    quDep1NAME: TIBStringField;
    quDep1SNAME: TIBStringField;
    quDep1COLOR: TIntegerField;
    taMOLDepName: TStringField;
    taOperCLASSOP: TIntegerField;
    quTmpMOL: TIBDataSet;
    dsrTmpMOL: TDataSource;
    quTmpMOLMOLID: TIntegerField;
    quTmpMOLFNAME: TIBStringField;
    quTmpMOLLNAME: TIBStringField;
    quTmpMOLMNAME: TIBStringField;
    quTmpMOLPSWD: TIBStringField;
    quTmpMOLSHEETNO: TSmallintField;
    quTmpMOLFIO: TIBStringField;
    quTmpMOLDEPID: TIntegerField;
    quTmpMOLdepname: TStringField;
    procedure DataModuleCreate(Sender: TObject);
    procedure taMOLNewRecord(DataSet: TDataSet);
    procedure CalcRecNo(DataSet: TDataSet);
    procedure taCompNewRecord(DataSet: TDataSet);
    procedure taDepNewRecord(DataSet: TDataSet);
    procedure taRecNewRecord(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taDepBeforeOpen(DataSet: TDataSet);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure DelConf(DataSet: TDataSet);
    procedure taSemisUQGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taSemisUWGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taSemisUWSetText(Sender: TField; const Text: String);
    procedure taSemisUQSetText(Sender: TField; const Text: String);
    procedure quSeqBeforeOpen(DataSet: TDataSet);
    procedure aplevRestore(Sender: TObject);
    procedure taOperUGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taOperUSetText(Sender: TField; const Text: String);
    procedure taCxOperNewRecord(DataSet: TDataSet);
    procedure taCxOperBeforeOpen(DataSet: TDataSet);
    procedure taCxOperOPERATIONSLSetText(Sender: TField;
      const Text: String);
    procedure SEMISGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure SEMISSetText(Sender: TField; const Text: String);
    procedure aplevException(Sender: TObject; E: Exception);
    procedure taOperCLASSOPGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taOperCLASSOPSetText(Sender: TField; const Text: String);
    procedure quTmpMOLNewRecord(DataSet: TDataSet);
  private
    FConfirmDel : boolean;
    FLogined : boolean;
    FUserId : integer;
    FUserName, FLName, FMName : string;
    FBeginDate, FEndDate : TDate;
    FDepDef : integer;
    function LoginFunction(UsersTable: TIBDataSet; const Password: String): Boolean;
    function GetBeginDate: TDate;
    function GetEndDate: TDate;
    function GetShowKindDoc: integer;
    procedure SetBeginDate(const Value: TDate);
    procedure SetEndDate(const Value: TDate);
    procedure SetShowKindDoc(const Value: integer);
    function GetDepDef: integer;
  public
    D_DepId : integer;
    SeqId : integer;
    property Logined : boolean read FLogined write FLogined;
    property ConfirmDel : boolean read FConfirmDel write FConfirmDel;
    (* ���������� � ������������, ������� ����� � ��������� *)
    property UserName : string read FUserName write FUserName;
    property LastName : string read FLName write FLName;
    property MiddleName : string read FMName write FMName;
    property UserId : integer read FUserId write FUserId;
    property DepDef : integer read GetDepDef write FDepDef;
    (* �������� �� ��������� *)
    property BeginDate : TDate read GetBeginDate write SetBeginDate;
    property EndDate : TDate read GetEndDate write SetEndDate;
    property ShowKindDoc : integer read GetShowKindDoc write SetShowKindDoc;
    function GetId(TableID: integer): integer;
  end;

{const
  clMoneyGreen = RGB(192, 220, 192);
  clCream = RGB(230, 210, 180);
  clPink = RGB(250, 170, 170);}

(*
   ���� ������
     - D_MOL 1 ���������� ����������� ������������ ���
     - WOrder 2 ������� � ������� �������
     - WOItem 3 ������� ������
     - D_Dep 4 ������� �������������
     - D_Comp 5 ������� ����������� � ���������� ���
     - D_Rec 6 ������� �������� �� ���������
     - null 7 ��������� ����������� ������ ��� ������(���������) ����� ��-15
     - D_Seq 8 ������� ������������ ��������
     - null 9 ���. ����. ������ ��� ���������
     - D_CxOper 10 - ������� � ��������� ������� ��������
*)

var
  dm: Tdm;
  clMoneyGreen : TColor;

implementation

uses DBLogin, StrUtils, dbUtil, fmUtils, MainData, ExcptDlg;

{$R *.DFM}

procedure Tdm.DataModuleCreate(Sender: TObject);
var
  y,m,d : word;
begin
  Logined:=False;
  if not LoginDB(db, 'D_MOL', 'FName', LoginFunction, 3, False) then
  begin
    dm.Free;
    exit;
  end;
  db.Connected := True;
  Logined:=True;
  taRec.Open;
  BeginDate := taRecBEGINDATE.AsDateTime;
  EndDate := taRecENDDATE.AsDateTime;
end;

function Tdm.LoginFunction(UsersTable: TIBDataSet; const Password: String): Boolean;
begin
  Result:=False;
  if Password=DelESpace(UsersTable.FieldByName('Pswd').AsString) then
    with UsersTable do
    begin
      UserName := FieldByName('FName').AsString;
      LastName := FieldByName('LName').AsString;
      MiddleName := FieldByName('MName').AsString;
      if (Length(LastName)>0) AND (Length(MiddleName)>0) then
         UserName:=UserName+' '+LastName[1]+'.'+MiddleName[1]+'.';
      UserId:=FieldByName('MOLId').AsInteger;
      if FieldByName('DepId').IsNull then DepDef := -1
      else DepDef := FieldByName('DepId').AsInteger;
      Result:=True;
    end;
end;

function Tdm.GetId(TableID: integer): integer;
begin
  with quGetID do
    begin
      Params[0].AsInteger:=TableID;
      ExecQuery;
      Result:=Fields[0].AsInteger;
      Close;
    end;
end;

procedure Tdm.taMOLNewRecord(DataSet: TDataSet);
begin
  taMOLMOLID.AsInteger := GetID(1);
end;

procedure Tdm.CalcRecNo(DataSet: TDataSet);
begin
  DataSet.FieldByName('RecNo').AsInteger := DataSet.RecNo;
end;


procedure Tdm.taCompNewRecord(DataSet: TDataSet);
begin
  taCompCompId.AsInteger := GetID(5);
end;

procedure Tdm.taDepNewRecord(DataSet: TDataSet);
begin
  taDepDEPID.AsInteger := GetID(4);
end;

procedure Tdm.taRecNewRecord(DataSet: TDataSet);
begin
  taRecRecId.AsInteger := GetID(6);
end;

procedure Tdm.CommitRetaining(DataSet: TDataSet);
begin
  tr.CommitRetaining;
end;

procedure Tdm.taDepBeforeOpen(DataSet: TDataSet);
begin
  taDep.Params[0].AsInteger := D_DepId;
end;

procedure Tdm.PostBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure Tdm.DelConf(DataSet: TDataSet);
begin
  if MessageDlg('������� ������?', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
end;

procedure Tdm.taSemisUQGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
          -1 : Text:='���';
           0 : Text:='��';
           1 : Text:='����';
         end;
end;

procedure Tdm.taSemisUQSetText(Sender: TField; const Text: String);
begin
  if Text='���' then Sender.AsInteger := -1;
  if Text='��' then Sender.AsInteger := 0;
  if Text='����' then Sender.AsInteger := 1;
end;

procedure Tdm.taSemisUWGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
          -1 : Text:='���';
           3 : Text:='��';
           4 : Text:='�����';
         end;
end;

procedure Tdm.taSemisUWSetText(Sender: TField; const Text: String);
begin
  if Text='���' then Sender.AsInteger := -1;
  if Text='��' then Sender.AsInteger:=3;
  if Text='�����' then Sender.AsInteger:=4;
end;

procedure Tdm.quSeqBeforeOpen(DataSet: TDataSet);
begin
  taDep.Params[0].AsInteger := SeqId;
end;

procedure Tdm.aplevRestore(Sender: TObject);
begin
  RestoreApp;
end;

procedure Tdm.taOperUGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  quTmp.Close;
  if Sender.AsString <> '' then
  begin
    quTmp.SQL.Text := 'select U from D_Unit where UnitId="'+Sender.AsString+'"';
    quTmp.ExecQuery;
     if quTmp.Fields[0].AsString = '' then Text := ''
     else Text := quTmp.Fields[0].AsString;
    quTmp.Close;
  end;
  //if quTmp.Fields[0].IsNull then  else Text := quTmp.Fields[0].AsString;
end;

procedure Tdm.taOperUSetText(Sender: TField; const Text: String);
var
   s : string;
begin
  s := dmMain.GetIdByField('D_Unit', 'U', 'UnitId', Text);
  if s <> '' then // ����� ��� �����-��
    Sender.AsString := s
  else //������� �� PickList'a
    Sender.AsString := Text;
end;

procedure Tdm.taCxOperNewRecord(DataSet: TDataSet);
begin
  taCxOperCXOPERID.AsInteger := getid(10);
  taCxOperOperM.AsString := taOperOPERID.AsString;
  taCxOperOperSL.AsVariant := null;
end;

procedure Tdm.taCxOperBeforeOpen(DataSet: TDataSet);
begin
  with TIbDataSet(DataSet).Params do
    ByName('I_OperM').AsString := taOperOPERID.AsString;
end;

procedure Tdm.taCxOperOPERATIONSLSetText(Sender: TField;
  const Text: String);
begin
  Sender.AsString := Text;
end;

procedure Tdm.SEMISGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  quTmp.Close;
  quTmp.SQL.Text := 'select Semis from D_Semis where SemisId="'+Sender.AsString+'"';
  quTmp.ExecQuery;
  if quTmp.Fields[0].IsNull then Text := '' else Text := quTmp.Fields[0].AsString;
  quTmp.Close;
end;

procedure Tdm.SEMISSetText(Sender: TField; const Text: String);
var
   s : string;
begin
  s := dmMain.GetIdByField('D_Semis', 'Semis', 'SemisId', Text);
  if s <> '' then // ����� ��� �����-��
    Sender.AsString := s
  else //������� �� PickList'a
    Sender.AsString := Text;
end;

procedure Tdm.aplevException(Sender: TObject; E: Exception);
begin
  TranslateIbMsg(e);
  Application.ShowException(E);
end;

function Tdm.GetBeginDate: TDate;
begin
  with quRecTmp do
  begin
    Close;
    Transaction.StartTransaction;
    SQL.Text := 'select BeginDate from D_Rec';
    ExecQuery;
    Result := Fields[0].AsDate;
    Close;
    Transaction.Commit;
  end;
end;

function Tdm.GetEndDate: TDate;
begin
  with quRecTmp do
  begin
    Close;
    Transaction.StartTransaction;
    SQL.Text := 'select EndDate from D_Rec';
    ExecQuery;
    Result := Fields[0].AsDate;
    Close;
    Transaction.Commit;
  end;
end;

function Tdm.GetShowKindDoc: integer;
begin
  with quRecTmp do
  begin
    Close;
    Transaction.StartTransaction;
    SQL.Text := 'select ShowKindDoc from D_Rec';
    ExecQuery;
    Result := Fields[0].AsInteger;
    Close;
    Transaction.Commit;
  end;
end;

procedure Tdm.SetBeginDate(const Value: TDate);
begin
  with quRecTmp do
  begin
    Close;
    Transaction.StartTransaction;
    SQL.Text := 'update D_Rec set BeginDate="'+DateToStr(Value)+'"';
    ExecQuery;
    Close;
    Transaction.Commit;
  end;
end;

procedure Tdm.SetEndDate(const Value: TDate);
begin
  with quRecTmp do
  begin
    Close;
    Transaction.StartTransaction;
    SQL.Text := 'update D_Rec set EndDate="'+DateToStr(Value)+'"';
    ExecQuery;
    Close;
    Transaction.Commit;
  end;
end;

procedure Tdm.SetShowKindDoc(const Value: integer);
begin
  with quRecTmp do
  begin
    Close;
    Transaction.StartTransaction;
    SQL.Text := 'update D_Rec set ShowKindDoc='+IntToStr(Value);
    ExecQuery;
    Close;
    Transaction.Commit;
  end;
end;

function Tdm.GetDepDef: integer;
begin
  with quRecTmp do
  begin
    Close;
    Transaction.StartTransaction;
    SQL.Text := 'select DepId from D_MOL where MolId='+IntToStr(UserId);
    ExecQuery;
    Result := Fields[0].AsInteger;
    Transaction.Commit;
    Close;
  end;
end;

procedure Tdm.taOperCLASSOPGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           1 : Text:='������� ���������';
           2 : Text:='������';
           3 : Text:='�������������� �����-��';
         end;
end;

procedure Tdm.taOperCLASSOPSetText(Sender: TField; const Text: String);
begin
  if Text='������� ���������' then Sender.AsInteger := 1;
  if Text='������' then Sender.AsInteger := 2;
  if Text='�������������� �����-��' then Sender.AsInteger := 3;
end;

procedure Tdm.quTmpMOLNewRecord(DataSet: TDataSet);
begin
  quTmpMOLMOLID.AsInteger := getid(1);
end;

initialization
  clMoneyGreen := RGB(192, 220, 192);
end.





