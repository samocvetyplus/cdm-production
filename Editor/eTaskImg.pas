unit eTaskImg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh, ExtDlgs;

type
  TfmeTaskImage = class(TfmEditor)
    Label1: TLabel;
    Label2: TLabel;
    edImgName: TDBEditEh;
    edImgFile: TDBEditEh;
    dlg: TOpenPictureDialog;
    procedure edImgFileEditButtons0Click(Sender: TObject;
      var Handled: Boolean);
    procedure edImgNameChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmeTaskImage: TfmeTaskImage;

implementation

{$R *.dfm}

procedure TfmeTaskImage.edImgFileEditButtons0Click(Sender: TObject;
  var Handled: Boolean);
begin
  inherited;
  with dlg do
    if Execute then begin
      edImgFile.Text:=FileName;
    end;
end;

procedure TfmeTaskImage.edImgNameChange(Sender: TObject);
begin
  inherited;
  btnOk.Enabled:=(edImgName.Text<>'') and ((edImgFile.Text<>'')or(not edImgFile.Visible)); 
end;

end.
