unit dmErrors;

interface

uses
  SysUtils, Windows, Classes, FIB, pFIBErrorHandler, FIBQuery, pFIBQuery,
  FIBDatabase, pFIBDatabase, DB, FIBDataSet, pFIBDataSet;

type
  TErrorHandler = class(TDataModule)
    FIBErrorHandler: TpFibErrorHandler;
    Transaction: TpFIBTransaction;
    DBErrors: TpFIBDataSet;
    DBErrorsERRORID: TFIBIntegerField;
    DBErrorsERRORDATE: TFIBDateTimeField;
    DBErrorsERRORUSER: TFIBStringField;
    DBErrorsERRORSOURCE: TFIBStringField;
    DBErrorsERRORMESSAGE: TFIBStringField;
    DBErrorsERRORCOMPONENT: TFIBStringField;
    DBErrorsERRORSQL: TFIBMemoField;
    procedure OnError(Sender: TObject; ErrorValue: EFIBError;
      KindIBError: TKindIBError; var DoRaise: Boolean);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  ErrorHandler: TErrorHandler;

implementation

{$R *.dfm}

uses

  StrUtils, WinSock,
  IB_ErrorCodes, DictData;

procedure TErrorHandler.DataModuleCreate(Sender: TObject);
begin
  Transaction.StartTransaction;
  DBErrors.Active := True;
end;

procedure TErrorHandler.DataModuleDestroy(Sender: TObject);
begin
  DBErrors.Active := False;
  Transaction.Commit;
end;

procedure TErrorHandler.OnError(Sender: TObject; ErrorValue: EFIBError; KindIBError: TKindIBError; var DoRaise: Boolean);
var
  ErrorMessage: string;
  SQLMessage: string;
  IBMessage: string;
  ERRORUSER: string;
  ERRORSOURCE: string;

procedure HandleUniqueViolation;
begin
end;

procedure HandleForeignKey;
begin
end;

procedure HandleCheck;
begin
end;

procedure HandleSecurity;
begin
end;

procedure HandleException;
begin
end;

procedure HandleOther;
begin
end;

procedure HandleLostConnect;
begin
  KindIBError := keNoError;
end;

procedure HandleNoError;
begin
end;

function GetIPAddress: string;
type
  pu_long = ^u_long;
var
  varTWSAData : TWSAData;
  varPHostEnt : PHostEnt;
  varTInAddr : TInAddr;
  namebuf : Array[0..255] of char;
begin
  If WSAStartup($101,varTWSAData) <> 0 Then
  Result := 'No. IP Address'
  Else Begin
    gethostname(namebuf,sizeof(namebuf));
    varPHostEnt := gethostbyname(namebuf);
    varTInAddr.S_addr := u_long(pu_long(varPHostEnt^.h_addr_list^)^);
    Result := 'IP Address: '+inet_ntoa(varTInAddr);
  End;
  WSACleanup;
end;

begin

  ErrorMessage := '';

  case KindIBError of

    keUniqueViolation:
    begin
      HandleUniqueViolation;
    end;
    keForeignKey:
    begin
      HandleUniqueViolation;
    end;
    keCheck:
    begin
      HandleCheck;
    end;
    keSecurity:
    begin
      HandleSecurity;
    end;
    keException:
    begin
      HandleException;
    end;
    keOther:
    begin
      HandleOther;
    end;
    keLostConnect:
    begin
      HandleLostConnect;
    end;
    keNoError:
    begin
      HandleNoError;
    end;

  end;

  if KindIBError <> keNoError then
  begin
    if ErrorMessage = '' then
    begin
      SQLMessage := Trim(AnsiReplaceStr(ErrorValue.SQLMessage, #13#10, ' '));
      IBMessage := Trim(AnsiReplaceStr(ErrorValue.IBMessage, #13#10, ' '));
      ErrorMessage := SQLMessage + ' ' + IBMessage;
      ErrorMessage := Trim(ErrorMessage)
    end;
    DBErrors.Append;
    DBErrorsERRORUSER.AsString := dm.User.FIO;
    DBErrorsERRORSOURCE.AsString := GetIPAddress;
    DBErrorsERRORMESSAGE.AsString := ErrorMessage;
    if Sender <> nil then
    begin
      DBErrorsERRORCOMPONENT.AsString := ErrorValue.RaiserName;
      if Sender is TFIBQuery then
      begin
        DBErrorsERRORSQL.AsString := TFIBQuery(Sender).SQL.Text;
      end;
    end;
    DBErrors.Post;
    Transaction.CommitRetaining;
  end;
  DoRaise := True;
end;

end.
