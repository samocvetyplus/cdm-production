inherited fmEServMat: TfmEServMat
  Left = 366
  Top = 268
  Caption = #1052#1072#1090#1077#1088#1080#1072#1083#1099' '#1090#1086#1083#1083#1080#1085#1075#1072
  ClientWidth = 595
  ExplicitWidth = 603
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 595
    ExplicitWidth = 595
  end
  inherited plDict: TPanel
    Width = 595
    ExplicitWidth = 595
    inherited plSelect: TPanel
      Width = 591
      ExplicitWidth = 591
      inherited btnOk: TSpeedButton
        Left = 413
        ExplicitLeft = 413
      end
      inherited btnCancel: TSpeedButton
        Left = 497
        ExplicitLeft = 497
      end
    end
    inherited gridDict: TDBGridEh
      Width = 591
      DataSource = dmInv.dsrEServMat
      Columns = <
        item
          EditButtons = <>
          FieldName = 'MATNAME'
          Footers = <>
          Title.EndEllipsis = True
          Width = 121
        end
        item
          EditButtons = <>
          FieldName = 'COMPNAME'
          Footers = <>
          Title.EndEllipsis = True
          Width = 116
        end
        item
          EditButtons = <>
          FieldName = 'N'
          Footers = <>
          Title.EndEllipsis = True
          Width = 61
        end
        item
          EditButtons = <>
          FieldName = 'GETW'
          Footers = <>
          Title.EndEllipsis = True
        end
        item
          EditButtons = <>
          FieldName = 'NW'
          Footers = <>
          Title.EndEllipsis = True
        end
        item
          EditButtons = <>
          FieldName = 'SCOST'
          Footers = <>
          Title.EndEllipsis = True
        end
        item
          EditButtons = <>
          FieldName = 'OUTW'
          Footers = <>
          Title.EndEllipsis = True
        end>
    end
  end
  inherited tb1: TSpeedBar
    Width = 595
    ExplicitWidth = 595
  end
end
