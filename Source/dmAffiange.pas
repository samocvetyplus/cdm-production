unit dmAffiange;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase;

type
  TDataModuleAffiange = class(TDataModule)
    Transaction: TpFIBTransaction;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  DataModuleAffiange: TDataModuleAffiange;

implementation

uses DictData;

{$R *.dfm}

procedure TDataModuleAffiange.DataModuleCreate(Sender: TObject);
begin
  Transaction.StartTransaction;
end;

procedure TDataModuleAffiange.DataModuleDestroy(Sender: TObject);
begin
  if Transaction.Active then Transaction.Commit;
end;

end.
