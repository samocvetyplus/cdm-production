{********************************************}
{  ����� �� ����� �������� � ��������        }
{  Copyrigth (C) 2003-2004 basile for CDM    }
{  v1.10                                     }
{ last update 16.02.2004                     }
{********************************************}
unit WOrderTotal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList,  ImgList,  ExtCtrls,
  ComCtrls, StdCtrls, DBCtrls, DB, pFIBDataSet, FIBDataSet, FR_DSet,
  FR_DBSet, rxPlacemnt, rxSpeedbar;

type
  TfmWOrderTotal = class(TfmAncestor)
    trvMat: TTreeView;
    plMain: TPanel;
    Label1: TLabel;
    txtRej: TDBText;
    txtRet: TDBText;
    txtDone: TDBText;
    lnDone: TLabel;
    lbRet: TLabel;
    lbRej: TLabel;
    txtNk: TDBText;
    txtReWork: TDBText;
    txtGet_W: TDBText;
    lbGet: TLabel;
    lbReWork: TLabel;
    Label8: TLabel;
    lbF: TLabel;
    Bevel1: TBevel;
    taMat: TpFIBDataSet;
    taMatTotal: TpFIBDataSet;
    taMatTotalIN_Q: TIntegerField;
    taMatTotalIN_W: TFloatField;
    taMatTotalGET_Q: TIntegerField;
    taMatTotalGET_W: TFloatField;
    taMatTotalREWORK_Q: TIntegerField;
    taMatTotalREWORK_W: TFloatField;
    taMatTotalRET_Q: TIntegerField;
    taMatTotalRET_W: TFloatField;
    taMatTotalREJ_Q: TIntegerField;
    taMatTotalREJ_W: TFloatField;
    taMatTotalDONE_Q: TIntegerField;
    taMatTotalDONE_W: TFloatField;
    taMatTotalNK_Q: TIntegerField;
    taMatTotalNK_W: TFloatField;
    taMatTotalI_Q: TIntegerField;
    taMatTotalI_W: TFloatField;
    dsrMatTotal: TDataSource;
    txtIn_W: TDBText;
    txtI_W: TDBText;
    lbSelectMat: TLabel;
    Bevel2: TBevel;
    Label2: TLabel;
    Bevel3: TBevel;
    taMatTotalF_Q: TIntegerField;
    taMatTotalF_W: TFloatField;
    txtFQ: TDBText;
    txtFW: TDBText;
    txtIn_Q: TDBText;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    txtGet_Q: TDBText;
    txtReWork_Q: TDBText;
    txtDone_Q: TDBText;
    txtNk_Q: TDBText;
    txtRet_Q: TDBText;
    txtRej_Q: TDBText;
    plArt2Ins: TPanel;
    Label7: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    txtArt2Ins_Q: TDBText;
    txtArt2Ins_W: TDBText;
    taMatTotalART2INS_Q: TIntegerField;
    taMatTotalART2INS_W: TFloatField;
    taMatMATID: TFIBStringField;
    taMatMATNAME: TFIBStringField;
    taMatSORTIND: TIntegerField;
    taMatISINS: TSmallintField;
    spitDetailArt2Ins: TSpeedItem;
    acDetailArt2Ins: TAction;
    taDetailArt2Ins: TpFIBDataSet;
    frDetailArt2Ins: TfrDBDataSet;
    taDetailArt2Ins2: TpFIBDataSet;
    frDetailArt2Ins2: TfrDBDataSet;
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure taMatBeforeOpen(DataSet: TDataSet);
    procedure trvMatChange(Sender: TObject; Node: TTreeNode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure taMatTotalBeforeOpen(DataSet: TDataSet);
    procedure acDetailArt2InsExecute(Sender: TObject);
    procedure taDetailArt2InsBeforeOpen(DataSet: TDataSet);
    procedure taDetailArt2Ins2BeforeOpen(DataSet: TDataSet);
  end;

var
  fmWOrderTotal: TfmWOrderTotal;

implementation

uses MainData, DictData, dbUtil, dbTree, PrintData;

{$R *.dfm}

procedure TfmWOrderTotal.FormCreate(Sender: TObject);
var
  RootNode : TTreeNode;
  nd : TNodeData;
begin
  Self.Caption := '�� - '+dmMain.PsName_WOrderTotal+'. �������� - '+dmMain.OperName_WOrderTotal; 
  RootNode := trvMat.Items.Add(nil, '���������');
  OpenDataSet(taMat);
  while not taMat.Eof do
  begin
    with trvMat.Items.AddChild(RootNode, taMat.Fields[1].AsString) do
    begin
      nd := TNodeData.Create;
      nd.Code := taMatMATID.AsString;
      nd.Value := taMatISINS.AsInteger;
      Data := nd;
    end;
    taMat.Next;
  end;
  CloseDataSet(taMat);
  trvMat.TopItem.Expand(True);
  trvMat.Selected := trvMat.TopItem.getFirstChild;
end;

procedure TfmWOrderTotal.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  Resize := False;
end;

procedure TfmWOrderTotal.taMatBeforeOpen(DataSet: TDataSet);
begin
  with TpFIbDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dmMain.BD_WOrderTotal);
    //ShowMessage('bd: ' + ByName['BD'].AsString);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dmMain.ED_WOrderTotal);
    //ShowMessage('ed: ' + ByName['ED'].AsString);
    ByName['DEPID'].AsInteger := dmMain.PsId_WorderTotal;
    //ShowMessage('DEPID: ' + ByName['DEPID'].AsString);
    ByName['OPERID'].AsString := dmMain.OperId_WOrderTotal;
    //ShowMessage('OPERID: ' + ByName['OPERID'].AsString);
  end;
end;

procedure TfmWOrderTotal.trvMatChange(Sender: TObject; Node: TTreeNode);
var
  i : integer; 
begin
  if not Assigned(trvMat.Selected)then eXit;
  plArt2Ins.Visible := False;
  acDetailArt2Ins.Enabled := False;
  for i:=0 to Pred(Self.ComponentCount) do
    if Self.Components[i].InheritsFrom(TControl) and
       (TControl(Self.Components[i]).Parent.Name = plMain.Name)
    then TControl(Self.Components[i]).Visible := not(trvMat.Selected = trvMat.TopItem);
  lbSelectMat.Visible := trvMat.Selected = trvMat.TopItem;
  if(trvMat.Selected = trvMat.TopItem)then eXit;
  Screen.Cursor := crSQLWait;
  try
    ReOpenDataSet(taMatTotal);
    plArt2Ins.Visible := TNodeData(trvMat.Selected.Data).Value=1;
    acDetailArt2Ins.Enabled := plArt2Ins.Visible;
  finally
    Screen.Cursor := crDefault;
  end
end;

procedure TfmWOrderTotal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(taMatTotal);
  inherited;
end;

procedure TfmWOrderTotal.taMatTotalBeforeOpen(DataSet: TDataSet);
begin
  with TpFIbDataSet(DataSet).Params do
  begin
    //showmessage('������: '+ datetimetostr(dmMain.BD_WOrderTotal) + '; �����: ' + datetimetostr(dmMain.ED_WOrderTotal));
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dmMain.BD_WOrderTotal);

    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dmMain.ED_WOrderTotal);

    ByName['PSID'].AsInteger := dmMain.PsId_WorderTotal;

    ByName['OPERID'].AsString := dmMain.OperId_WOrderTotal;

    ByName['MATID'].AsString := TNodeData(trvMat.Selected.Data).Code;

  end;
end;

procedure TfmWOrderTotal.acDetailArt2InsExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkDetailArt2Ins, Null);
  CloseDataSet(taDetailArt2Ins);
end;

procedure TfmWOrderTotal.taDetailArt2InsBeforeOpen(DataSet: TDataSet);
begin
  with TpFIbDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dmMain.BD_WOrderTotal);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dmMain.ED_WOrderTotal);
    ByName['PSID'].AsInteger := dmMain.PsId_WorderTotal;
    ByName['MATID'].AsString := TNodeData(trvMat.Selected.Data).Code;
  end;
end;

procedure TfmWOrderTotal.taDetailArt2Ins2BeforeOpen(DataSet: TDataSet);
begin
  taDetailArt2Ins2.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dmMain.BD_WOrderTotal);
  taDetailArt2Ins2.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dmMain.ED_WOrderTotal);
  taDetailArt2Ins2.ParamByName('PSID').AsInteger := dmMain.PsId_WorderTotal;
  taDetailArt2Ins2.ParamByName('MATID').AsString := TNodeData(trvMat.Selected.Data).Code;
end;

end.
