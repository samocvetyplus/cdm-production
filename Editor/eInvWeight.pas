unit eInvWeight;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, ComCtrls, Buttons, ExtCtrls;

type
  TfmeInvWeight = class(TfmEditor)
    UpDown1: TUpDown;
    Edit1: TEdit;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmeInvWeight: TfmeInvWeight;

implementation

uses SInv_det;


{$R *.dfm}

procedure TfmeInvWeight.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  fmSInv_det.iw := StrToFloat(Edit1.Text);
end;

end.
