unit Sort;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, TB2Dock, TB2Toolbar, ActnList, ImgList,
  TB2Item, Menus;

type
  TfmSort = class(TForm)
    Panel3: TPanel;
    lb1: TListBox;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    acEvent: TActionList;
    acUp: TAction;
    im: TImageList;
    TBItem1: TTBItem;
    acDown: TAction;
    TBItem2: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem3: TTBItem;
    acSortAsc: TAction;
    acSortDesc: TAction;
    TBItem4: TTBItem;
    ppList: TPopupMenu;
    acChangePosition: TAction;
    N1: TMenuItem;
    procedure FormActivate(Sender: TObject);
    procedure lb1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lb1StartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure lb1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure acUpExecute(Sender: TObject);
    procedure acDownExecute(Sender: TObject);
    procedure acSortAscExecute(Sender: TObject);
    procedure acSortDescExecute(Sender: TObject);
    procedure acChangePositionExecute(Sender: TObject);
    procedure ppListPopup(Sender: TObject);
    procedure lb1MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    CurrIndex : integer;
    DraggingIndex: integer;
  end;

var
  fmSort: TfmSort;

implementation

{$R *.DFM}

procedure TfmSort.FormActivate(Sender: TObject);
begin
  ActiveControl:=lb1;
  lb1.ItemIndex:=0;
end;

procedure TfmSort.lb1DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
var p: TPoint;
    i: integer;
begin
  p.X:=X;
  p.Y:=Y;
  i:=lb1.ItemAtPos(p, True);
  Accept:=(i<>DraggingIndex) AND (i>-1);
end;

procedure TfmSort.lb1StartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
  DraggingIndex:=lb1.ItemIndex;
end;

procedure TfmSort.lb1DragDrop(Sender, Source: TObject; X, Y: Integer);
var i, j, SelCount: integer;
    p: TPoint;
begin
  p.X:=X;
  p.Y:=Y;
  i:=lb1.ItemAtPos(p, True);
  SelCount := lb1.SelCount;
  if i>-1 then
    // multiselection
    if lb1.SelCount=-1 then lb1.Items.Move(DraggingIndex, i)
    else begin
      try
        // ������ � ����� ����������� ����������
        if DraggingIndex > i then
        begin
          for j:=0 to Pred(lb1.Count) do
            if lb1.Selected[j] then
            begin
              lb1.Items.Move(j, i);
              inc(i);
            end;
          for i:=0 to Pred(SelCount) do
            lb1.Selected[lb1.ItemAtPos(p, True)+i]:=True;
        end
        else begin
          for j:=Pred(lb1.Count) downto 0 do
            if lb1.Selected[j] then
            begin
              lb1.Items.Move(j, i);
              dec(i);
            end;
          for i:=0 to Pred(SelCount) do
            lb1.Selected[lb1.ItemAtPos(p, True)-i]:=True;
        end;
      finally 
      end;
    end;
  lb1.ItemIndex := lb1.ItemAtPos(p, True);
end;

procedure TfmSort.acUpExecute(Sender: TObject);
var
  i: integer;
begin
  with lb1, Items do
    if ItemIndex>0 then
      begin
        i:=ItemIndex-1;
        Move(ItemIndex,i);
        ActiveControl:=lb1;
        ItemIndex:=i;
      end

end;

procedure TfmSort.acDownExecute(Sender: TObject);
var
  i : integer;
begin
  with lb1, Items do
    if ItemIndex<Count-1 then
    begin
      i:=ItemIndex+1;
      Move(ItemIndex,i);
      ActiveControl:=lb1;
      ItemIndex:=i;
    end;
end;

procedure TfmSort.acSortAscExecute(Sender: TObject);
begin
  lb1.Items.BeginUpdate;
  try
    lb1.Sorted := False;
    lb1.Sorted := True;
  finally
    lb1.Items.EndUpdate;
  end;
end;

procedure TfmSort.acSortDescExecute(Sender: TObject);
var
  FirstIndex, LastIndex : integer;
  s : string;
begin
  lb1.Items.BeginUpdate;
  try
    lb1.Sorted := False;
    lb1.Sorted := True;
    FirstIndex := 0;
    LastIndex := lb1.Count-1;
    while (LastIndex > FirstIndex)do
    begin
      s := lb1.Items[FirstIndex];
      lb1.Items[FirstIndex] := lb1.Items[LastIndex];
      lb1.Items[LastIndex] := s;
      inc(FirstIndex);
      dec(LastIndex);
    end;
  finally
    lb1.Items.EndUpdate;
  end;
end;

procedure TfmSort.acChangePositionExecute(Sender: TObject);
var
  i, Save_Ind, ind, SelCount: integer;
begin
  Save_Ind := CurrIndex;
  SelCount := lb1.SelCount;
  ind := -1;
  for i:=0 to Pred(lb1.Count) do
    if lb1.Selected[i] then
    begin
      ind := i;
      break;
    end;
  if ind=-1 then eXit;
  if ind > CurrIndex then
  begin
    for i:=0 to Pred(lb1.Count) do
      if lb1.Selected[i] then
      begin
        lb1.Items.Move(i, CurrIndex);
        inc(CurrIndex);
      end;
      for i:=0 to SelCount-1 do
        lb1.Selected[Save_Ind+i] := True;
  end
  else begin
    for i:=Pred(lb1.Count) downto 0 do
      if lb1.Selected[i] then
      begin
        lb1.Items.Move(i, CurrIndex);
        dec(CurrIndex);
      end;
      for i:=0 to SelCount-1 do
       lb1.Selected[Save_Ind-i] := True;
  end;
  lb1.ItemIndex := Save_Ind;
end;

procedure TfmSort.ppListPopup(Sender: TObject);
begin
  //
end;

procedure TfmSort.lb1MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  APoint: TPoint;
begin
  if Button = mbRight then
  begin
    APoint.X := X;
    APoint.Y := Y;
    CurrIndex := lb1.ItemAtPos(APoint, True);
  end;
end;

end.
