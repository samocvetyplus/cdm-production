unit eSQ;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, RXSpin;

type
  TfmeSQ = class(TfmEditor)
    lbQ: TLabel;
    edQ: TRxSpinEdit;
    lbW: TLabel;
    edW: TRxSpinEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmeSQ: TfmeSQ;

implementation

uses WOrder, MainData;

{$R *.dfm}

procedure TfmeSQ.FormCreate(Sender: TObject);
begin
  edQ.Value := dmMain.SemisQ;
  edW.Value := dmMain.SemisW;
  inherited;
  if(dmMain.SemisQ <> 0) then ActiveControl := edQ else ActiveControl := edW;
end;

procedure TfmeSQ.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk : if ActiveControl = edQ then
           begin
             ActiveControl := edW;
             SysUtils.Abort;
           end;
  end;
  dmMain.SemisQ := edQ.AsInteger;
  dmMain.SemisW := edW.Value;
  inherited;
end;

end.
