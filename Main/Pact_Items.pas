unit Pact_Items;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList, ExtCtrls, ComCtrls,
  Grids, DBGrids, RXDBCtrl, StdCtrls, DBCtrls,
  DBGridEh, dbutil, ActnList, DBGridEhGrouping, GridsEh, rxPlacemnt,
  rxSpeedbar;

type
  TfmPact_Items = class(TfmAncestor)
    DBText1: TDBText;
    DBText2: TDBText;
    Label1: TLabel;
    Label2: TLabel;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    DBGridEh1: TDBGridEh;
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPact_Items: TfmPact_Items;

implementation

uses InvData, eUID;

{$R *.dfm}

procedure TfmPact_Items.SpeedItem1Click(Sender: TObject);
begin
  dmInv.taPact_Items.Delete;
end;

procedure TfmPact_Items.SpeedItem2Click(Sender: TObject);
begin
  inherited;
   try
    fmUID:=TfmUID.Create(self);
    if fmUID.ShowModal=mrOk then
      with dminv do
      begin
        sqlAddPact.ParamByName('UID').AsInteger:=fmUID.UID;
        sqlAddPact.ParamByName('ACT_ITEMID').AsInteger:=taPriceACTSACT_ITEMID.AsInteger;
        sqlAddPact.ExecQuery;
        sqlAddPact.Transaction.CommitRetaining;
      end;
   finally
    fmUID.Free;
   end;
end;

end.
