inherited fmOperRej: TfmOperRej
  Left = 357
  Top = 149
  Caption = #1041#1088#1072#1082' '#1076#1083#1103' '#1086#1087#1077#1088#1072#1094#1080#1080
  PixelsPerInch = 96
  TextHeight = 13
  inherited plDict: TPanel
    inherited gridDict: TDBGridEh
      DataSource = dm.dsrOperRej
      FrozenCols = 2
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Width = 31
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERATION'
          Footers = <>
          Title.EndEllipsis = True
          Width = 121
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SEMISNAME'
          Footers = <>
          Title.EndEllipsis = True
          Width = 155
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          DropDownRows = 15
          DropDownSpecRow.ShortCut = 16573
          DropDownSpecRow.Visible = True
          EditButtons = <>
          FieldName = 'MATNAME'
          Footers = <>
          Width = 77
        end>
    end
  end
end
