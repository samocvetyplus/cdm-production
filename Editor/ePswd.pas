{***********************************************}
{  �������� ������ ������������                 }
{  Copyrigth (C) 2003 basile for CDM            }
{  v0.22                                         }
{  last update 02.05.2003                       }
{***********************************************}

unit ePswd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DbEditor, StdCtrls, Mask, DBCtrls, Buttons, ExtCtrls, DBCtrlsEh;

type
  TfmPswd = class(TfmDbEditor)
    Label1: TLabel;
    Label2: TLabel;
    edPswd: TDBEditEh;
    edPswdTest: TDBEditEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPswd: TfmPswd;

implementation

uses DictData, MD5, ProductionConsts;

{$R *.dfm}

procedure TfmPswd.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Password: string;
  APassword: string;
  MD5Digest: TMD5Digest;
begin
  if ModalResult=mrOK then
  begin
    if edPswdTest.Text<>dm.taMOLPSWD.AsString then
    begin
      edPswdTest.Text:='';
      raise Exception.Create(rsRepeatPswd);
    end;
    Password := edPswd.Text;
    MD5Digest := MD5String(Password);
    APassword := MD5DigestToBase64(MD5Digest);
    DataSet.FieldByName('PSWD').AsString := APassword;
  end;
  inherited;
end;

end.



