unit eW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh, ActnList;

type
  TfmeW = class(TfmEditor)
    lbW: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    cmbxOper: TDBComboBoxEh;
    cmbxU: TDBComboBoxEh;
    edW: TDBNumberEditEh;
    acEvent: TActionList;
    acReadW: TAction;
    acOk: TAction;
    lbComment: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acReadWExecute(Sender: TObject);
    procedure acOkExecute(Sender: TObject);

  end;

var
  fmeW: TfmeW;

implementation

uses ApplData, DictData, dbTree, ScaleData, SInvProd, UtilLib;

{$R *.dfm}

procedure TfmeW.FormCreate(Sender: TObject);
var
  i, Ind : integer;
  Art : String;
  Semis : string;
begin
  lbComment.Visible := dm.LocalSetting.Scale_Use;
  Art := dmAppl.taWHApplART.AsString;
  // ���������������� ������ ��������
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.Items.Delete(0);
  Ind := -1;
  for i:=0 to Pred(cmbxOper.Items.Count) do
    if TNodeData(cmbxOper.Items.Objects[i]).Code=dm.LastOperId then
    begin
      Ind := i;
      break;
    end;
  if(Ind = -1)then cmbxOper.ItemIndex := 0
  else cmbxOper.ItemIndex := Ind;
  edW.Text := FloatToStr(dmAppl.ADistrW);

  with dmAppl, quTmp do
    begin
      Close;
      SQL.Text := 'select semis from d_art where d_artid = :ArtID';

      ParamByName('ArtID').AsInteger  := taWhApplARTID.AsInteger;
      ExecQuery;
      Semis := Trim(Fields[0].AsString);
      if (Semis = '�') or (Semis = '1233') or (Semis = '1218') then
        begin
          cmbxU.ItemIndex := dmAppl.ADistrNewU;
        end
      else
        cmbxU.ItemIndex := dmAppl.ADistrNewU-1;
    end;

  inherited;
end;


procedure TfmeW.FormClose(Sender: TObject; var Action: TCloseAction);
var
  DefaultDecimalSeparator : char;

  procedure HandleConvert;
  begin
    if DecimalSeparator = '.' then DecimalSeparator := ','
    else DecimalSeparator := '.';
    try
      dmAppl.ADistrW := StrToFloat(edW.Text);
    except
      ShowMessage('������. ������������ ������');
    end;
  end;

begin
  DefaultDecimalSeparator := DecimalSeparator;
  try
    try
      dmAppl.ADistrNewU := cmbxU.ItemIndex + 1;
      dmAppl.ADistrW := StrToFloat(edW.Text);
      dm.LastOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
    except
      on E:EConvertError do HandleConvert;
    end
  finally
    DecimalSeparator := DefaultDecimalSeparator;
  end;
end;

procedure TfmeW.FormShow(Sender: TObject);
begin
  acReadW.Execute;
end;

procedure TfmeW.acReadWExecute(Sender: TObject);
var
  Weight : double;
begin
  if dm.LocalSetting.Scale_Use then
  begin
    Weight := dmScale.GetWeigth;
    edW.Value := MyRound(Weight);
    if(dm.WorkMode = 'aSInvProd')then
    begin
      fmSInvProd.stbrStatus.Panels[1].Text := FloatToStr(Weight);
      fmSInvProd.stbrStatus.Panels[2].Text := FloatToStr(MyRound(Weight));
    end;
  end
end;

procedure TfmeW.acOkExecute(Sender: TObject);
begin
  ModalResult := mrOk;
end;

end.
