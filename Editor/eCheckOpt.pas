unit eCheckOpt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh, DBLookupEh;

type
  TfmeCheckOpt = class(TfmEditor)
    Label1: TLabel;
    edCost: TDBNumberEditEh;
    Label2: TLabel;
    chbxNDS: TDBCheckBoxEh;
    edNDS: TDBNumberEditEh;
    edGive: TDBNumberEditEh;
    lbComp: TLabel;
    cmbxComp: TDBComboBoxEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edCostChange(Sender: TObject);
    procedure edCostExit(Sender: TObject);
  private
    FOpenDS : boolean;
  end;

var
  fmeCheckOpt: TfmeCheckOpt;

implementation

uses DictData, InvData, dbTree, fmUtils;

{$R *.dfm}

procedure TfmeCheckOpt.FormCreate(Sender: TObject);
begin
  if dmInv.FCassaUseNDS then
  begin
    chbxNDS.Checked := True;
    chbxNDS.Caption := '� �.�. ��� '+FloatToStr(dm.LocalSetting.Cassa_NDS)+'%';
    edNDS.Value := dmInv.FCassaMoney*dm.LocalSetting.Cassa_NDS/100;
  end
  else begin
    chbxNDS.Checked := False;
    edNDS.Value := 0;
  end;
  edCost.Value := dmInv.FCassaMoney;
  edGive.Value := dmInv.FCassaGiveMoney;
  cmbxComp.Items.Assign(dm.dComp);
  cmbxComp.Items.Delete(0);
  if cmbxComp.Items.Count > 0 then cmbxComp.ItemIndex := 0;
  inherited;
end;

procedure TfmeCheckOpt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case  ModalResult of
    mrOk : begin
      if(cmbxComp.ItemIndex = -1)then
      begin
        ActiveControl := cmbxComp;
        raise EWarning.Create('���������� ������� �����������');
      end;
      if(ActiveControl=cmbxComp)then
      begin
        ActiveControl := edCost;
        SysUtils.Abort;
      end;
      if VarIsNull(edCost.Value)or(edCost.Value = 0) then
      begin
        ActiveControl := edCost;
        raise EWarning.Create('����� �� ����� ���� ����� 0');
      end;
      if(ActiveControl = edCost)then
      begin
        if chbxNDS.Checked then ActiveControl := edNDS
        else ActiveControl := edCost;
        SysUtils.Abort;
      end;
      if(ActiveControl = edNDS)then
      begin
        ActiveControl := edGive;
        SysUtils.Abort;
      end;
      if chbxNDS.Checked and (VarIsNull(edNDS.Value) or (edNDS.Value=0))then
      begin
        ActiveControl := edNDS;
        raise EWarning.Create('������� ���');
      end;
      if(edCost.Value > edGive.Value)then
      begin
        ActiveControl := edGive;
        raise EWarning.Create('�������� �� ����� ������ �����');
      end;
      dmInv.FCassaUseNDS := chbxNDS.Checked;
      dmInv.FCassaNDSMoney := edNDS.Value;
      dmInv.FCassaMoney := edCost.Value;
      dmInv.FCassaGiveMoney := edGive.Value;
      dmInv.FCassaCompId := TNodeData(cmbxComp.Items.Objects[cmbxComp.ItemIndex]).Code;
    end;
  end;
  inherited;  
end;

procedure TfmeCheckOpt.edCostChange(Sender: TObject);
begin
  if chbxNDS.Checked then
    edNDS.Value := edCost.Value*dm.LocalSetting.Cassa_NDS/100;
end;

procedure TfmeCheckOpt.edCostExit(Sender: TObject);
begin
  if VarIsNull(edGive.Value)or(edGive.Value=0)then
    edGive.Value := edCost.Value; 
end;

end.
