inherited fmErrWhSemis: TfmErrWhSemis
  Left = 129
  Top = 196
  Width = 803
  Caption = #1057#1082#1083#1072#1076' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 795
  end
  inherited tb1: TSpeedBar
    Width = 795
    object spitDoIt: TSpeedItem
      BtnCaption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      Caption = #1048#1089#1087#1088#1072#1074#1080#1090#1100
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF000080800000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000080800000000000FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000080800000FFFF000000
        0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0000808000000000000000000000000000FFFF
        FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0000808000FFFFFF0000FFFF00FFFFFF0000FF
        FF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF0000808000FFFFFF0000FFFF000000
        0000008080000080800000808000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF000080800000FFFF00FFFFFF0000FF
        FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00008080000000000000000000000000000000000000FFFF00FFFF
        FF0000FFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF0000808000FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
        FF00FFFFFF0000FFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000808000FFFFFF0000FFFF00FFFFFF0000FFFF000000
        0000008080000080800000808000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF000080800000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
        FF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF000080800000FFFF00FFFFFF0000FFFF00FFFF
        FF0000FFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF0000808000FFFFFF0000FFFF00FFFFFF0000FF
        FF00FFFFFF0000FFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF0000808000FFFFFF0000FFFF00FFFF
        FF0000FFFF00FFFFFF0000FFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF000080800000808000008080000080
        80000080800000808000008080000080800000808000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      Hint = #1048#1089#1087#1088#1072#1074#1080#1090#1100'|'
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = spitDoItClick
      SectionName = 'Untitled (0)'
    end
  end
  object gridWhSemis: TDBGridEh [2]
    Left = 0
    Top = 42
    Width = 795
    Height = 200
    Align = alClient
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmMain.dsrErrWhSemis
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Width = 106
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SEMISNAME'
        Footers = <>
        Width = 95
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OPERNAME'
        Footers = <>
        Width = 81
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FQ'
        Footers = <>
        Width = 96
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FW'
        Footers = <>
        Width = 75
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RQ'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RW'
        Footers = <>
        Width = 51
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'UQ'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'UW'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
end
