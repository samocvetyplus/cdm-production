unit ApplData;

interface

uses
  Controls, SysUtils, Classes, DB, pFIBDataSet, DateUtils,
  pFIBQuery, pFIBDatabase, FR_DSet, FR_DBSet, ArtImage, FIBDatabase,
  FIBQuery, FIBDataSet, DBClient, Provider;

const
  sArtU : set of char = ['2'];
    
type
  TADistrType = (
     adtWOrder, // �����
     adtDIEl,   // ���������� ����������� ����� ���������
     adtSOEl,   // ������ ����������
     adtRej,    // ����
     adtProd,   // �������� ������� ���������
     adtMdfArt  // �������� ������� 
     );

  TdmAppl = class(TDataModule)
    taAList: TpFIBDataSet;
    dsrAList: TDataSource;
    taAListDOCNO: TIntegerField;
    taAListDOCDATE: TDateTimeField;
    taAListITYPE: TSmallintField;
    taAListDEPID: TIntegerField;
    taAListUSERID: TIntegerField;
    taAListISCLOSE: TSmallintField;
    taAListISPROCESS: TSmallintField;
    taAListUSERNAME: TStringField;
    taAListRecNo: TIntegerField;
    taAppl: TpFIBDataSet;
    taAListID: TIntegerField;
    dsrAppl: TDataSource;
    taApplID: TIntegerField;
    taApplAPPLID: TIntegerField;
    taApplARTID: TIntegerField;
    taApplART2ID: TIntegerField;
    taApplMW: TFloatField;
    taApplWQ: TIntegerField;
    taApplQ: TIntegerField;
    taApplR: TSmallintField;
    taApplMODEL: TSmallintField;
    taApplINS: TFloatField;
    taApplINW: TFloatField;
    taApplINQ: TIntegerField;
    taApplDEBTORS: TFloatField;
    taApplDEBTORW: TFloatField;
    taApplDEBTORQ: TIntegerField;
    taApplREJS: TFloatField;
    taApplREJW: TFloatField;
    taApplREJQ: TIntegerField;
    taApplSALES: TFloatField;
    taApplSALEW: TFloatField;
    taApplSALEQ: TIntegerField;
    taApplRETS: TFloatField;
    taApplRETW: TFloatField;
    taApplRETQ: TIntegerField;
    taApplOUTS: TFloatField;
    taApplOUTW: TFloatField;
    taApplOUTQ: TIntegerField;
    taAListABD: TDateTimeField;
    taAListAED: TDateTimeField;
    taAListW: TFloatField;
    quTmp: TpFIBQuery;
    trAppl: TpFIBTransaction;
    taApplSZ: TIntegerField;
    taApplART: TFIBStringField;
    taApplART2: TFIBStringField;
    taAListQ: TFloatField;
    taApplFQ: TIntegerField;
    taWHAppl: TpFIBDataSet;
    taAInv: TpFIBDataSet;
    dsrWHAppl: TDataSource;
    dsrAInv: TDataSource;
    taAEl: TpFIBDataSet;
    dsrAEl: TDataSource;
    taAInvINVID: TIntegerField;
    taAInvDOCNO: TIntegerField;
    taAInvDOCDATE: TDateTimeField;
    taAInvDEPID: TIntegerField;
    taAInvFROMDEPID: TIntegerField;
    taAInvUSERID: TIntegerField;
    taAInvJOBID: TIntegerField;
    taWHApplRecNo: TIntegerField;
    taAElRecNo: TIntegerField;
    taWHApplID: TIntegerField;
    taWHApplPSID: TIntegerField;
    taWHApplOPERID: TFIBStringField;
    taWHApplARTID: TIntegerField;
    taWHApplART2ID: TIntegerField;
    taWHApplSZID: TIntegerField;
    taWHApplT: TSmallintField;
    taWHApplQ: TIntegerField;
    taWHApplART2: TFIBStringField;
    taWHApplART: TFIBStringField;
    taWHApplSZ: TFIBStringField;
    taAElID: TIntegerField;
    taAElINVID: TIntegerField;
    taAElARTID: TIntegerField;
    taAElART2ID: TIntegerField;
    taAElSZID: TIntegerField;
    taAElART: TFIBStringField;
    taAElART2: TFIBStringField;
    taAElSZ: TFIBStringField;
    taAListREF: TIntegerField;
    taApplRecNo: TIntegerField;
    taWHApplOPERNAME: TFIBStringField;
    taRejInv: TpFIBDataSet;
    dsrRejInv: TDataSource;
    taRejEl: TpFIBDataSet;
    dsrRejEl: TDataSource;
    taPInv: TpFIBDataSet;
    taPEl: TpFIBDataSet;
    dsrPInv: TDataSource;
    dsrPEl: TDataSource;
    taRejInvINVID: TIntegerField;
    taRejInvDOCNO: TIntegerField;
    taRejInvDOCDATE: TDateTimeField;
    taRejInvDEPID: TIntegerField;
    taRejInvFROMDEPID: TIntegerField;
    taRejInvUSERID: TIntegerField;
    taRejInvJOBID: TIntegerField;
    taRejElRecNo: TIntegerField;
    taPElRecNo: TIntegerField;
    taAInvITYPE: TSmallintField;
    taRejInvITYPE: TSmallintField;
    taRejElID: TIntegerField;
    taRejElINVID: TIntegerField;
    taRejElARTID: TIntegerField;
    taRejElART2ID: TIntegerField;
    taRejElSZID: TIntegerField;
    taRejElART: TFIBStringField;
    taRejElART2: TFIBStringField;
    taRejElSZ: TFIBStringField;
    taRejElQ: TIntegerField;
    taPElID: TIntegerField;
    taPElINVID: TIntegerField;
    taPElARTID: TIntegerField;
    taPElART2ID: TIntegerField;
    taPElSZID: TIntegerField;
    taPElART: TFIBStringField;
    taPElART2: TFIBStringField;
    taPElSZ: TFIBStringField;
    taPElQ: TIntegerField;
    taAInvREF: TIntegerField;
    taRejInvREF: TIntegerField;
    taPInvINVID: TIntegerField;
    taPInvDOCNO: TIntegerField;
    taPInvDOCDATE: TDateTimeField;
    taPInvDEPID: TIntegerField;
    taPInvFROMDEPID: TIntegerField;
    taPInvUSERID: TIntegerField;
    taPInvJOBID: TIntegerField;
    taPInvREF: TIntegerField;
    taAElT: TSmallintField;
    taRejElT: TSmallintField;
    taPElT: TSmallintField;
    taAElOPERID: TFIBStringField;
    taAElOPERATION: TFIBStringField;
    taRejElOPERID: TFIBStringField;
    taRejElOPERATION: TFIBStringField;
    taPElOPERID: TFIBStringField;
    taPElOPERATION: TFIBStringField;
    taPInvITYPE: TSmallintField;
    taAElOPERIDFROM: TFIBStringField;
    taAElOPERATIONFROM: TFIBStringField;
    taAElART2ID_OLD: TIntegerField;
    taAElART2_OLD: TFIBStringField;
    taAInvWORDERID: TIntegerField;
    taRejInvWORDERID: TIntegerField;
    taPInvWORDERID: TIntegerField;
    taTmp: TpFIBDataSet;
    taAIns: TpFIBDataSet;
    dsrAIns: TDataSource;
    taAInsWOITEMID: TIntegerField;
    taAInsWORDERID: TIntegerField;
    taAInsOPERID: TFIBStringField;
    taAInsSEMIS: TFIBStringField;
    taAInsITDATE: TDateTimeField;
    taAInsQ: TFloatField;
    taAInsW: TFloatField;
    taAInsITTYPE: TIntegerField;
    taAInsINVID: TIntegerField;
    taAInsDOCNO: TIntegerField;
    taAInsDOCDATE: TDateTimeField;
    taAInsUSERID: TIntegerField;
    taAInsE: TSmallintField;
    taAInsDEPID: TIntegerField;
    taAInsJOBDEPID: TIntegerField;
    taAInsJOBID: TIntegerField;
    taAInsAINV: TIntegerField;
    taAInsREF: TIntegerField;
    taAInsSEMISNAME1: TFIBStringField;
    taAInsSEMISNAME: TStringField;
    taAInsCOLOR: TStringField;
    taAInsCLEANNES: TStringField;
    taAInsCHROMATICITY: TStringField;
    taAInsEDGSHAPEID: TStringField;
    taAInsEDGTYPEID: TStringField;
    taAInsIGR: TStringField;
    taAElQ: TIntegerField;
    taAInvQ: TFloatField;
    taRejInvQ: TFloatField;
    taPInvQ: TFloatField;
    taAInsART2ID: TIntegerField;
    taAInsART: TStringField;
    taAElU: TSmallintField;
    taRejElU: TSmallintField;
    taPElU: TSmallintField;
    taWHApplU: TSmallintField;
    taAElU_OLD: TSmallintField;
    frAInv: TfrDBDataSet;
    frAEl: TfrDBDataSet;
    taWhA2: TpFIBDataSet;
    taWhA2ART2: TFIBStringField;
    taWhA2ART: TFIBStringField;
    taWhA2SZ: TFIBStringField;
    taWhA2OPERATION: TFIBStringField;
    taWhA2DEPNAME: TFIBStringField;
    taWhA2Q: TIntegerField;
    dsrWhA2: TDataSource;
    taApplSZNAME: TFIBStringField;
    taRejElCOMMENTID: TSmallintField;
    taPElCOMMENTID: TSmallintField;
    taApplSTORES: TFloatField;
    taApplSTOREQ: TIntegerField;
    taApplSTOREW: TFloatField;
    taApplCOMMENT: TFIBStringField;
    taApplPICT: TBlobField;
    taApplNQ: TIntegerField;
    taAListOPERID: TFIBStringField;
    taAListOPERATION: TFIBStringField;
    taAProdList: TpFIBDataSet;
    dsrAProdList: TDataSource;
    taAProdListID: TIntegerField;
    taAProdListDOCNO: TIntegerField;
    taAProdListDOCDATE: TDateTimeField;
    taAProdListW: TFloatField;
    taAProdListQ: TFloatField;
    taAProdListOPERATION: TFIBStringField;
    taAProdListFIO: TFIBStringField;
    taAPInv: TpFIBDataSet;
    dsrAPInv: TDataSource;
    taAPEl: TpFIBDataSet;
    dsrAPEl: TDataSource;
    taAPInvINVID: TIntegerField;
    taAPInvDOCNO: TIntegerField;
    taAPInvDOCDATE: TDateTimeField;
    taAPInvDEPID: TIntegerField;
    taAPInvFROMDEPID: TIntegerField;
    taAPInvUSERID: TIntegerField;
    taAPInvQ: TFloatField;
    taAPInvJOBID: TIntegerField;
    taAPInvITYPE: TSmallintField;
    taAPInvREF: TIntegerField;
    taAPInvWORDERID: TIntegerField;
    taAPElID: TIntegerField;
    taAPElINVID: TIntegerField;
    taAPElARTID: TIntegerField;
    taAPElART2ID: TIntegerField;
    taAPElSZID: TIntegerField;
    taAPElART: TFIBStringField;
    taAPElART2: TFIBStringField;
    taAPElSZ: TFIBStringField;
    taAPElQ: TIntegerField;
    taAPElT: TSmallintField;
    taAPElOPERID: TFIBStringField;
    taAPElOPERATION: TFIBStringField;
    taAPElU: TSmallintField;
    taAProdListISPROCESS: TSmallintField;
    taApplInv: TpFIBDataSet;
    dsrApplInv: TDataSource;
    taApplInvID: TIntegerField;
    taApplInvDOCNO: TIntegerField;
    taApplInvDOCDATE: TDateTimeField;
    taApplInvW: TFloatField;
    taApplInvQ: TFloatField;
    taApplInvOPERATION: TFIBStringField;
    taApplInvFIO: TFIBStringField;
    taApplInvISPROCESS: TSmallintField;
    taApplAQ: TIntegerField;
    taExecAppl: TpFIBDataSet;
    dsrExecAppl: TDataSource;
    taExecApplART: TFIBStringField;
    taExecApplSZNAME: TFIBStringField;
    taExecApplARTID: TIntegerField;
    quGetExecApplId: TpFIBDataSet;
    quGetExecApplIdAPPLID: TIntegerField;
    taExecApplSZID: TIntegerField;
    taRejAdd: TpFIBDataSet;
    dsrRejAdd: TDataSource;
    taRejAddARTID: TIntegerField;
    taRejAddART: TFIBStringField;
    taRejAddSZID: TIntegerField;
    taRejAddSZNAME: TFIBStringField;
    taRejAddITYPE: TSmallintField;
    taRejAddQ: TIntegerField;
    taRejAddCOMMENTID: TIntegerField;
    taRejAddCOMMENTNAME: TFIBStringField;
    taRejAddCOMMENT: TFIBStringField;
    taRejElCOMMENT: TFIBStringField;
    taPElCOMMENT: TFIBStringField;
    taApplNQ_APPL: TIntegerField;
    taRejAddUSERNAME: TFIBStringField;
    taRejAddINVDATE: TDateTimeField;
    taApplPInv: TpFIBDataSet;
    taApplPEl: TpFIBDataSet;
    taApplRejInv: TpFIBDataSet;
    taApplRejEl: TpFIBDataSet;
    dsrApplPInv: TDataSource;
    dsrApplPEl: TDataSource;
    dsrApplRejInv: TDataSource;
    dsrApplRejEl: TDataSource;
    taApplPInvINVID: TIntegerField;
    taApplPInvDOCNO: TIntegerField;
    taApplPInvDOCDATE: TDateTimeField;
    taApplPInvDEPID: TIntegerField;
    taApplPInvFROMDEPID: TIntegerField;
    taApplPInvUSERID: TIntegerField;
    taApplPInvQ: TFloatField;
    taApplPInvJOBID: TIntegerField;
    taApplPInvREF: TIntegerField;
    taApplPInvITYPE: TSmallintField;
    taApplPInvWORDERID: TIntegerField;
    taApplRejInvINVID: TIntegerField;
    taApplRejInvDOCNO: TIntegerField;
    taApplRejInvDOCDATE: TDateTimeField;
    taApplRejInvDEPID: TIntegerField;
    taApplRejInvFROMDEPID: TIntegerField;
    taApplRejInvUSERID: TIntegerField;
    taApplRejInvQ: TFloatField;
    taApplRejInvJOBID: TIntegerField;
    taApplRejInvREF: TIntegerField;
    taApplRejInvITYPE: TSmallintField;
    taApplRejInvWORDERID: TIntegerField;
    taApplRejElID: TIntegerField;
    taApplRejElINVID: TIntegerField;
    taApplRejElARTID: TIntegerField;
    taApplRejElART2ID: TIntegerField;
    taApplRejElSZID: TIntegerField;
    taApplRejElART: TFIBStringField;
    taApplRejElART2: TFIBStringField;
    taApplRejElSZ: TFIBStringField;
    taApplRejElQ: TIntegerField;
    taApplRejElT: TSmallintField;
    taApplRejElOPERID: TFIBStringField;
    taApplRejElOPERATION: TFIBStringField;
    taApplRejElU: TSmallintField;
    taApplRejElCOMMENTID: TSmallintField;
    taApplRejElCOMMENT: TFIBStringField;
    taApplPElID: TIntegerField;
    taApplPElINVID: TIntegerField;
    taApplPElARTID: TIntegerField;
    taApplPElART2ID: TIntegerField;
    taApplPElSZID: TIntegerField;
    taApplPElART: TFIBStringField;
    taApplPElART2: TFIBStringField;
    taApplPElSZ: TFIBStringField;
    taApplPElQ: TIntegerField;
    taApplPElT: TSmallintField;
    taApplPElOPERID: TFIBStringField;
    taApplPElOPERATION: TFIBStringField;
    taApplPElU: TSmallintField;
    taApplPElCOMMENTID: TSmallintField;
    taApplPElCOMMENT: TFIBStringField;
    taApplPSTOREQ: TIntegerField;
    taApplPSTOREW: TFloatField;
    taAInvDEPNAME: TFIBStringField;
    taAInvFROMDEPNAME: TFIBStringField;
    taWHApplQ1: TIntegerField;
    taRejElQ1: TIntegerField;
    taPElQ1: TIntegerField;
    taAElQ1: TIntegerField;
    taAElPATTERN: TFIBStringField;
    taApplKOEF: TFloatField;
    taApplREQUESTQ: TFIBIntegerField;
    taApplARTROOT: TFIBSmallIntField;
    taChangeAssortByAppl: TpFIBDataSet;
    dsrChangeAssortByAppl: TDataSource;
    taChangeAssortByApplJAID: TFIBIntegerField;
    taChangeAssortByApplAARTID: TFIBIntegerField;
    taChangeAssortByApplAART: TFIBStringField;
    taChangeAssortByApplAART2ID: TFIBIntegerField;
    taChangeAssortByApplASZID: TFIBIntegerField;
    taChangeAssortByApplASZNAME: TFIBStringField;
    taChangeAssortByApplAQ: TFIBIntegerField;
    taChangeAssortByApplAOPERID: TFIBStringField;
    taChangeAssortByApplAUNITID: TFIBIntegerField;
    taChangeAssortByApplWHARTID: TFIBIntegerField;
    taChangeAssortByApplWHART2ID: TFIBIntegerField;
    taChangeAssortByApplWHSZID: TFIBIntegerField;
    taChangeAssortByApplWHOPERID: TFIBStringField;
    taChangeAssortByApplWHUNITID: TFIBIntegerField;
    taChangeAssortByApplAOPERNAME: TFIBStringField;
    taChangeAssortByApplWHART: TFIBStringField;
    taChangeAssortByApplWHSZNAME: TFIBStringField;
    taChangeAssortByApplWHOPERNAME: TFIBStringField;
    taChangeAssortByApplAFQ: TFIBIntegerField;
    taChangeAssortByApplWHUNITNAME: TFIBStringField;
    taChangeAssortByApplAUNITNAME: TFIBStringField;
    taChangeAssortByApplWHQ: TFIBIntegerField;
    taChangeAssortByApplQ: TFIBIntegerField;
    taApplREQUESTQ2: TFIBIntegerField;
    taApplSELL0: TFIBIntegerField;
    taApplSELL1: TFIBIntegerField;
    taAListA1BD: TFIBDateTimeField;
    taAListA1ED: TFIBDateTimeField;
    taChangeAssortByApplD: TFIBSmallIntField;
    taWHApplPATTERN: TFIBStringField;
    taWhA2s: TpFIBDataSet;
    taWhA2sSUBMODELTITLE: TFIBStringField;
    taWhA2sMODELTITLE: TFIBStringField;
    taWhA2sSIZETITLE: TFIBStringField;
    taWhA2sOPERATIONTITLE: TFIBStringField;
    taWhA2sPLACEOFSTORAGETITLE: TFIBStringField;
    taWhA2sQUANTITY: TFIBIntegerField;
    dsrWhA22: TDataSource;
    taWhA2sTOTALQUANTITY: TFIBIntegerField;
    taWhA2sDifference: TFIBIntegerField;
    taWha2m: TClientDataSet;
    taWha2d: TClientDataSet;
    dsrWh2m: TDataSource;
    dsrWh2d: TDataSource;
    taWha2dSUBMODELTITLE: TStringField;
    taWha2dMODELTITLE: TStringField;
    taWha2dSIZETITLE: TStringField;
    taWha2dOPERATIONTITLE: TStringField;
    taWha2dPLACEOFSTORAGETITLE: TStringField;
    taWha2dQUANTITY: TIntegerField;
    taWha2dTOTALQUANTITY: TIntegerField;
    taWha2dDifference: TIntegerField;
    taWha2mSUBMODELTITLE: TStringField;
    taWha2mMODELTITLE: TStringField;
    taWha2mSIZETITLE: TStringField;
    taWha2mOPERATIONTITLE: TStringField;
    taWha2mPLACEOFSTORAGETITLE: TStringField;
    taWha2mQUANTITY: TIntegerField;
    taWha2mTOTALQUANTITY: TIntegerField;
    taWha2mDifference: TIntegerField;
    DataSetProvider: TDataSetProvider;
    taAListDEPARTMENT: TFIBStringField;
    taApplResp: TpFIBDataSet;
    dsrApplResp: TDataSource;
    taApplRespINVID: TFIBIntegerField;
    taApplRespSTARTSEMISRESPPERSON: TFIBIntegerField;
    taApplRespFINISHSEMISRESPPERSON: TFIBIntegerField;
    taApplRespREADYPRODRESPPERSON: TFIBIntegerField;
    taApplRespSTARTSEMISDATE: TFIBDateTimeField;
    taApplRespFINISHSEMISDATE: TFIBDateTimeField;
    taApplRespREADYSEMISDATE: TFIBDateTimeField;
    taAListSTARTN: TFIBStringField;
    taAListFINISHN: TFIBStringField;
    taAListREADYN: TFIBStringField;
    taAListSTARTD: TFIBDateTimeField;
    taAListFINISHD: TFIBDateTimeField;
    taAListREADYD: TFIBDateTimeField;
    procedure CalcRecNo(DataSet: TDataSet);
    procedure taAListBeforeOpen(DataSet: TDataSet);
    procedure taAListNewRecord(DataSet: TDataSet);
    procedure taApplBeforeOpen(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure DelConf(DataSet: TDataSet);
    procedure taSListBeforeOpen(DataSet: TDataSet);
    procedure taSListAfterPost(DataSet: TDataSet);
    procedure taAElBeforeOpen(DataSet: TDataSet);
    procedure taAInvBeforeOpen(DataSet: TDataSet);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure taAInvNewRecord(DataSet: TDataSet);
    procedure taAElNewRecord(DataSet: TDataSet);
    procedure taRejInvNewRecord(DataSet: TDataSet);
    procedure taRejElNewRecord(DataSet: TDataSet);
    procedure taRejInvBeforeOpen(DataSet: TDataSet);
    procedure taRejElBeforeOpen(DataSet: TDataSet);
    procedure taPElBeforeOpen(DataSet: TDataSet);
    procedure taPElNewRecord(DataSet: TDataSet);
    procedure taPInvBeforeOpen(DataSet: TDataSet);
    procedure taPInvNewRecord(DataSet: TDataSet);
    procedure taPElBeforePost(DataSet: TDataSet);
    procedure CommitRetainingWithRefreshWh(DataSet: TDataSet);
    procedure taAInsBeforeOpen(DataSet: TDataSet);
    procedure taAInsCalcFields(DataSet: TDataSet);
    procedure taAInsNewRecord(DataSet: TDataSet);
    procedure taAInsBeforePost(DataSet: TDataSet);
    procedure taPElQChange(Sender: TField);
    procedure taAElQChange(Sender: TField);
    procedure taRejElQChange(Sender: TField);
    procedure CancelWh(DataSet: TDataSet);
    procedure InitQValue(DataSet: TDataSet);
    procedure taAElART2_OLDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure CommitRetainingWithRefreshQ(DataSet: TDataSet);
    procedure taWHApplCalcFields(DataSet: TDataSet);
    procedure taAElCalcFields(DataSet: TDataSet);
    procedure taAInsITTYPEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taAInsITTYPESetText(Sender: TField; const Text: String);
    procedure UGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure USetText(Sender: TField; const Text: String);
    procedure taAElAfterOpen(DataSet: TDataSet);
    procedure taAElAfterScroll(DataSet: TDataSet);
    procedure taApplAfterOpen(DataSet: TDataSet);
    procedure taWhA2BeforeOpen(DataSet: TDataSet);
    procedure taApplAfterScroll(DataSet: TDataSet);
    procedure taApplNewRecord(DataSet: TDataSet);
    procedure taApplBeforeEdit(DataSet: TDataSet);
    procedure taRejElCOMMENTIDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taPElCOMMENTIDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taWHApplNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure taApplBeforePost(DataSet: TDataSet);
    procedure taApplAfterPost(DataSet: TDataSet);
    procedure taApplFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure taAProdListBeforeOpen(DataSet: TDataSet);
    procedure taAPInvBeforeOpen(DataSet: TDataSet);
    procedure taAPInvNewRecord(DataSet: TDataSet);
    procedure taAPElNewRecord(DataSet: TDataSet);
    procedure taAPElBeforeOpen(DataSet: TDataSet);
    procedure taAPElPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure taApplInvBeforeOpen(DataSet: TDataSet);
    procedure taAPElQChange(Sender: TField);
    procedure taRejElPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure taPElPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure taExecApplAfterClose(DataSet: TDataSet);
    procedure taRejAddBeforeOpen(DataSet: TDataSet);
    procedure taApplSZNAMEChange(Sender: TField);
    procedure taApplPInvNewRecord(DataSet: TDataSet);
    procedure taApplPInvBeforeOpen(DataSet: TDataSet);
    procedure taApplRejInvNewRecord(DataSet: TDataSet);
    procedure taApplRejInvBeforeOpen(DataSet: TDataSet);
    procedure taApplRejElBeforeOpen(DataSet: TDataSet);
    procedure taApplPElBeforeOpen(DataSet: TDataSet);
    procedure taApplPElPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure taApplRejElPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure taApplPElNewRecord(DataSet: TDataSet);
    procedure taApplRejElNewRecord(DataSet: TDataSet);
    procedure taApplARTValidate(Sender: TField);
    procedure taAInsBeforeDelete(DataSet: TDataSet);
    procedure taWHApplBeforeOpen(DataSet: TDataSet);
    procedure taChangeAssortByApplBeforeOpen(DataSet: TDataSet);
    procedure taChangeAssortByApplAfterOpen(DataSet: TDataSet);
    procedure taChangeAssortByApplAfterScroll(DataSet: TDataSet);
    procedure taWha2dBeforeOpen(DataSet: TDataSet);
    procedure taWha2dAfterOpen(DataSet: TDataSet);
    procedure taWha2dAfterClose(DataSet: TDataSet);
    procedure taWhA2sCalcFields(DataSet: TDataSet);
    procedure taAListAfterOpen(DataSet: TDataSet);
    procedure taApplBeforeClose(DataSet: TDataSet);
    procedure taApplRespBeforeOpen(DataSet: TDataSet);
  protected
    FSaveState : TDataSetState;
  private
    FIsNewArt : boolean;
    FABD: TDateTime;
    FEBD: TDateTime;
    FWhTo: integer;
    FAInvId: integer;
    FaDistrKind: integer;
    FWhFrom: integer;
    FAOperId: string;
    FUserTo: integer;
    FUserFrom: integer;
    FPInvId: integer;
    FRInvId: integer;
    FFilterOperId: string;
    FAElId: integer;
    FItType: integer;
    FWhApplId: integer;
    FADistrQ: integer;
    FADocDate: TDateTime;
    FADocNo: integer;
    FwOrderId: integer;
    FwoItemId: integer;
    FInvId: integer;
    FADistrW: double;
    FNewWhApplId: integer;
    FFilterDepId: integer;
    FADistrNewU: byte;
    FApplNewOperId: string;
    FApplOper: string;
    FAPInvId: integer;
    FApplInvId: integer;
    FADistrType: TADistrType;
    FAInvType: integer;
    FIsWorkWithStone: boolean;
    FApplRejInvId: integer;
    FApplPInvId: integer;
    FApplWh: integer;
    FADistrU: shortint;
    FAInsItType: byte;
    FFilterApplArt: string;
    FFilterChangeAssortByApplOperId: string;
    procedure SetADistrKind(const Value: integer);
    procedure SetADistrType(const Value: TADistrType);
  protected
    FInvQValue, FWhQValue : integer;
    FFieldChange : boolean;
    FOnCalculateAppl: TDataSetNotifyEvent;
  public
    TmpApplID : Variant;
    FArtImage : TfmImage;
    (* ������ ������� *)
    property OnCalculateAppl: TDataSetNotifyEvent read FOnCalculateAppl write FOnCalculateAppl;
    property AnalizBD : TDateTime read FABD write FABD;
    property AnalizED : TDateTime read FEBD write FEBD;
    // 0 - �� �������� �� ������� �����; 1 - � �������� ����� � ��������
    property ADistrKind : integer read FaDistrKind write SetADistrKind;
    property WhFrom : integer read FWhFrom write FWhFrom;
    property WhTo : integer read FWhTo;
    property UserTo : integer read FUserTo;
    property UserFrom : integer read FUserFrom;
    property ItType : integer read FItType write FItType;
    property ADocNo : integer read FADocNo;
    property ADocDate : TDateTime read FADocDate;
    property wOrderId : integer read FwOrderId; // woItem.wOrderId
    property woItemId : integer read FwoItemId; // woItem.Id
    property InvId    : integer read FInvId;    // woItem.InvId

    property AInvId : integer read FAInvId write FAInvId; // woItem.AInvId
    property AInvType : integer read FAInvType write FAInvType;
    property PInvId : integer read FPInvId write FPInvId; // �������� � ������������
    property RInvId : integer read FRInvId write FRInvId; // ����� � ������������
    property APInvId : integer read FAPInvId write FAPInvId; // �� ������
    // id ��������� ������ ��� ���������
    property ApplInvId : integer read FApplInvId write FApplInvId;
    // �������� � ������ ARootInvId
    property AOperId : string read FAOperId;
    // �������� ��� ���������� ������ ��������� ���������
    property FilterOperId : string read FFilterOperId write FFilterOperId;
    property FilterDepId : integer read FFilterDepId write FFilterDepId;
    property FilterChangeAssortByApplOperId : string read FFilterChangeAssortByApplOperId write FFilterChangeAssortByApplOperId;
    // ��������� ������� � ���������
    property AElId : integer read FAElId write FAElId;
    // ��������� ������� � ������ ��������� ���������
    property WhApplId : integer read FWhApplId write FWhApplId;
    property ADistrQ : integer read FADistrQ write FADistrQ;
    property ADistrW : double read FADistrW write FADistrW;
    property ADistrU : shortint read FADistrU write FADistrU;
    property ADistrNewU : byte read FADistrNewU write FADistrNewU;
    property NewWhApplId : integer read FNewWhApplId write FNewWhApplId;
    property ADistrType : TADistrType read FADistrType write SetADistrType;
    property AInsItType : byte read FAInsItType write FAInsItType;

    (* ���������� � ������ � ������ *)
    property ApplPInvId : integer read FApplPInvId write FApplPInvId;
    property ApplRejInvId : integer read FApplRejInvId write FApplRejInvId;
    property ApplWh : integer read FApplWh write FApplWh;

    (* ���������� ��������� � ������ *)
    property FilterApplArt : string read FFilterApplArt write FFilterApplArt;

    // �������� ��� ������� ������� ������ (��� ����������)
    property ApplOper : string read FApplOper write FApplOper;
    property IsWorkWithStone : boolean read FIsWorkWithStone write FIsWorkWithStone;
    // ������
    property ApplNewOperId : string read FApplNewOperId write FApplNewOperId;

                          (* ��������� � ������� *)
    function _ShowEditorQ(Q : integer; U, U_Old : byte; A : byte) : boolean;
    function GenPattern(Art2Id : integer) : string;

    procedure RefreshWh(DataSet : TpFIBDataSet; OperField : string='OPERID');
    procedure ChangeQInv(Sender : TField; OperId, ArtId, Art2Id, SzId, U : TField; T : byte);
     // 0 �������� � ������
     // 1 ����� � ������, �������� � ������
    procedure RejAddPeriod(var Bd, Ed : TDateTime);
  end;

var
  dmAppl: TdmAppl;

implementation

uses MainData, DictData, Forms, Appl, Dialogs, dbUtil, SList, Variants,
  ADistr, AList, eMW, Editor, RxDateUtil, MsgDialog, eQ, ProductionConsts,
  ChangeAssortByAppl, RxstrUtils;

{$R *.dfm}


procedure TdmAppl.taAListAfterOpen(DataSet: TDataSet);
begin
  if VartoStr(TmpApplID) <> '' then
    taAlist.Locate('ID', StrToInt(VarToStr(TmpApplID)), []);
end;

procedure TdmAppl.taAListBeforeOpen(DataSet: TDataSet);
begin
  dm.SetAPeriodParams(TpFIBDataSet(DataSet).Params);
  dm.SetDepParams(TpFIBDataSet(DataSet).Params);
  dm.SetSelfCompParams(TpFIBDataSet(DataSet).Params);

  if ApplNewOperID = '' then
  begin
    taAList.ParamByName('OperID1').AsString := '';
    taAList.ParamByName('OperID2').AsString := '��';
  end
  else
  begin
    taAList.ParamByName('OperID1').AsString := ApplNewOperID;
    taAList.ParamByName('OperID2').AsString := ApplNewOperID;
  end;

  //ShowMessage('dep1: ' + IntToStr(TpFIBDataSet(DataSet).ParamByName('DepID1').AsInteger) + ' dep2: ' + IntToStr(TpFIBDataSet(DataSet).ParamByName('DepID2').AsInteger));
  //ShowMessage('oper1: ' + TpFIBDataSet(DataSet).ParamByName('OperID1').AsString + ' oper2: ' + TpFIBDataSet(DataSet).ParamByName('OperID2').AsString);
  //ShowMessage('ID: ' + IntToStr(taAListID.AsInteger));
end;

procedure TdmAppl.taAListNewRecord(DataSet: TDataSet);
begin
  taAListID.AsInteger := dm.GetId(12);
  taAListISCLOSE.AsInteger := 0;
  taAListISPROCESS.AsInteger := 0;
  taAListDOCNO.AsInteger := dm.getId(19);
  taAListDOCDATE.AsDateTime := Today;
  taAListUSERID.AsInteger := dm.User.UserId;
  taAListDEPID.AsInteger := ApplWh;
  taAListW.AsFloat := 0;
  taAlistITYPE.AsInteger := 1;
  taAListOPERID.AsString := ApplNewOperId;
  taAListA1BD.AsDateTime := StartOfTheDay(IncMonth(Today, -6));
  taAListA1ED.AsDateTime := EndOfTheDay(Today);
end;

procedure TdmAppl.taApplBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    if(dm.WorkMode='aDistr')then
    begin
     taAppl.ParamByName('ID').AsInteger := ApplInvId;

     taAppl.ParamByName('ART_').AsString := '%';

     //ByName['ART_'].AsString:='%';
    end
    else if(dm.WorkMode = 'Appl')then
    begin
      ByName['ID'].AsInteger  := taAListID.AsInteger;
      ByName['ART_'].AsString := FilterApplArt;
    end;
end;

procedure TdmAppl.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TdmAppl.DelConf(DataSet: TDataSet);
begin
  if not dbUtil.ConfirmDelete then SysUtils.Abort;
end;

procedure TdmAppl.taSListBeforeOpen(DataSet: TDataSet);
begin
  dm.SetPeriodParams(TpFIBDataSet(DataSet).Params);
  dm.SetDepParams(TpFIBDataSet(DataSet).Params)
end;

procedure TdmAppl.taSListAfterPost(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TdmAppl.taAElBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['INVID'].AsInteger := taAInvINVID.AsInteger;
  end
end;

procedure TdmAppl.SetADistrKind(const Value: integer);
var
  v : Variant;
begin
  FaDistrKind := Value;
  case ADistrType of
    adtDIEl : begin
      FaDistrKind := 0;
      FWhTo     := dmMain.taDIListDEPID.AsInteger;
      FUserTo   := dmMain.taDIListJOBID.AsInteger;

      FWhFrom   := dmMain.taDIListFROMDEPID.AsInteger;
      FUserFrom := dmMain.taDIListUSERID.AsInteger;

      FAInvId := dmMain.taDIElREF.AsInteger;
      FAInvType := -1;

      {FRInvId := 0;
      FPInvId := 0;}      
      v := ExecSelectSQL('select InvId from Inv where IType=8 and Ref='+dmMain.taDIElID.AsString+' and WOrderId is null', quTmp);
      if VarIsNull(v) then FRInvId := 0 else FRInvId := v;

      v:= Null;
      v := ExecSelectSQL('select InvId from Inv where IType=9 and Ref='+dmMain.taDIElID.AsString+' and WOrderId is null', quTmp);
      if VarIsNull(v) then FPInvId := 0 else FPInvId := v;

      FAOperId := dmMain.taDIElOPERID.AsString;
      FItType  := 0;
      //FADocNo := dmMain.taWOrderDOCNO.AsInteger;
      //FADocDate := dmMain.taWOrderDOCDATE.AsDateTime;
      FInvId := -1;  //dmMain.taWorderINVID.AsInteger;
      FwoItemId := -1; //dmMain.taWOrderWOITEMID.AsInteger;
      FwOrderId := -1; //dmMain.taWOrderWORDERID.AsInteger;
    end;
    adtWOrder : begin
      if(FaDistrKind = 1)then
      begin
        FWhTo :=  dmMain.taWOrderDEPID.AsInteger; // dm.User.wWhId;
        FWhFrom := dmMain.taWOrderJOBDEPID.AsInteger;
        FUserTo := dmMain.taWOrderJOBID.AsInteger;
        FUserFrom := dmMain.taWOrderUSERID.AsInteger;
      end
      else begin
        FWhTo := dmMain.taWOrderJOBDEPID.AsInteger;
        FWhFrom := dm.User.wWhId; //dmMain.taWOrderDEPID.AsInteger;
        FUserTo := dmMain.taWOrderUSERID.AsInteger;
        FUserFrom := dmMain.taWOrderJOBID.AsInteger;
      end;
      FAInvId   := dmMain.taWOrderAINV.AsInteger;

      v := ExecSelectSQL('select IType from Inv where InvId='+
        IntToStr(dmMain.taWOrderAPPLINV.AsInteger), dmAppl.quTmp);
      if VarIsNull(v) then FAInvType :=0 else FAInvType:=v;

      v := Null;
      v := ExecSelectSQL('select InvId from Inv where IType=8 and Ref='+
        IntToStr(dmMain.taWOrderWOITEMID.AsInteger)+' and WOrderId is not null', dmAppl.quTmp);
      if VarIsNull(v) then FRInvId := 0 else FRInvId := v;

      v:= Null;
      v := ExecSelectSQL('select InvId from Inv where IType=9 and Ref='+
        IntToStr(dmMain.taWOrderWOITEMID.AsInteger)+' and WOrderId is not null', dmAppl.quTmp);
      if VarIsNull(v) then FPInvId := 0 else FPInvId := v;

      FAOperId := dmMain.taWOrderOPERID.AsString;
      FItType := dmMain.taWOrderITTYPE.AsInteger;
      FADocNo := dmMain.taWOrderDOCNO.AsInteger;
      FADocDate := dmMain.taWOrderDOCDATE.AsDateTime;
      FInvId := dmMain.taWorderINVID.AsInteger;
      FwoItemId := dmMain.taWOrderWOITEMID.AsInteger;
      FwOrderId := dmMain.taWOrderWORDERID.AsInteger;
    end;
    adtSOEl : begin
      FaDistrKind := 0;
      FWhTo     := -1;//dmMain.taSOListDEPID.AsInteger;
      FUserTo   := -1;//dmMain.taSOListUSERID.AsInteger;

      FWhFrom   := dmMain.taSOListDEPID.AsInteger;
      FUserFrom := dmMain.taSOListUSERID.AsInteger;

      FAInvId := 0;
      FAInvType := -1;
      FRInvId := dmMain.taSOElREF.AsInteger;
      FPInvId := 0;
      FAOperId := dmMain.taSOElOPERID.AsString;
      FItType  := 0;
      FInvId := -1;  //dmMain.taWorderINVID.AsInteger;
      FwoItemId := -1; //dmMain.taWOrderWOITEMID.AsInteger;
      FwOrderId := -1; //dmMain.taWOrderWORDERID.AsInteger;
    end;
    adtRej : begin
      FaDistrKind := 0;
      FWhTo     := -1;//dmMain.taSOListDEPID.AsInteger;
      FUserTo   := -1;//dmMain.taSOListUSERID.AsInteger;

      FWhFrom   := dmMain.taWOrderDEPID.AsInteger;
      FUserFrom := dmMain.taWOrderUSERID.AsInteger;

      FAInvId := 0;
      FAInvType := -1;
      FRInvId := dmMain.taWOrderAINV.AsInteger;
      FPInvId := 0;
      FAOperId := dm.RejOperId;
      FItType  := 5;
      FInvId := -1;  //dmMain.taWorderINVID.AsInteger;
      FwoItemId := -1; //dmMain.taWOrderWOITEMID.AsInteger;
      FwOrderId := -1; //dmMain.taWOrderWORDERID.AsInteger;
    end;
    adtMdfArt : begin
      FaDistrKind := 0;
      FWhTo     := -1;//dmMain.taSOListDEPID.AsInteger;
      FUserTo   := -1;//dmMain.taSOListUSERID.AsInteger;

      FWhFrom   := dmMain.taWOrderDEPID.AsInteger;
      FUserFrom := dmMain.taWOrderUSERID.AsInteger;

      FAInvId := 0;
      FAInvType := -1;
      FRInvId := dmMain.taWOrderAINV.AsInteger;
      v:= Null;
      v := ExecSelectSQL('select InvId from Inv where IType=9 and Ref='+dmMain.taWOrderWOITEMID.AsString+
        ' and WOrderId='+dmMain.taWOrderWORDERID.AsString, dmAppl.quTmp);
      if VarIsNull(v) then FPInvId := 0 else FPInvId := v;
      FAOperId := dm.RejOperId;
      FItType  := 5;
      FInvId := -1;  //dmMain.taWorderINVID.AsInteger;
      FwoItemId := dmMain.taWOrderWOITEMID.AsInteger;
      FwOrderId := dmMain.taWOrderWORDERID.AsInteger;
    end;
    else raise Exception.Create('Internal error: Not initialize ADistrType');
  end;
end;

procedure TdmAppl.taAInvBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ID'].AsInteger := AInvId;
end;

procedure TdmAppl.PostBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure TdmAppl.taAInvNewRecord(DataSet: TDataSet);
begin
  taAInvINVID.AsInteger    := dm.GetId(12);
  taAInvDOCNO.AsInteger    := dm.GetId(23);
  taAInvDOCDATE.AsDateTime := Today;
  case ADistrType of
    adtDIEl : begin
      taAInvDEPID.AsInteger     := dmMain.taDIListDEPID.AsInteger;
      taAInvFROMDEPID.AsInteger := dmMain.taDIListFROMDEPID.AsInteger;
      taAInvJOBID.AsInteger     := dmMain.taDIListJOBID.AsInteger;
      taAInvUSERID.AsInteger    := dmMain.taDIListUSERID.AsInteger;
      taAInvREF.AsInteger       := dmMain.taDIElID.AsInteger;
      taAInvWORDERID.AsVariant  := Null;
      taAInvITYPE.AsInteger     := 6;
    end;
    adtWOrder : begin
      if dmMain.taWOrderITTYPE.AsInteger in [0,6] then
      begin
        taAInvDEPID.AsInteger  :=  dmMain.taWOrderJOBDEPID.AsInteger;
        taAInvFROMDEPID.AsInteger := dmMain.taWOrderDEPID.AsInteger; // dm.User.wWhId;
        taAInvJOBID.AsInteger  := dmMain.taWOrderJOBID.AsInteger;
        taAInvUSERID.AsInteger := dmMain.taWOrderUSERID.AsInteger;
      end
      else begin
        taAInvDEPID.AsInteger  := dmMain.taWOrderDEPID.AsInteger; // dm.User.wWhId
        taAInvFROMDEPID.AsInteger := dmMain.taWOrderJOBDEPID.AsInteger;
        taAInvJOBID.AsInteger  :=  dmMain.taWOrderUSERID.AsInteger;
        taAInvUSERID.AsInteger := dmMain.taWOrderJOBID.AsInteger;
      end;
      taAInvREF.AsInteger      := dmMain.taWOrderWOITEMID.AsInteger;
      taAInvWORDERID.AsInteger := dmMain.taWOrderWORDERID.AsInteger;
      taAInvITYPE.AsInteger    := 6;
    end
    else raise EInternal.Create(Format(rsInternalError, ['209']));
  end;
end;

procedure TdmAppl.taAElNewRecord(DataSet: TDataSet);
begin
  taAElID.AsInteger    := dm.GetId(34); 
  taAElINVID.AsInteger := AInvId;
  taAElARTID.AsInteger := taWHApplARTID.AsInteger;
  taAElART2ID.AsInteger := taWHApplART2ID.AsInteger;
  taAElSZID.AsInteger := taWHApplSZID.AsInteger;
  taAElU.AsInteger := ADistrU;
  taAElU_OLD.AsInteger := taWHApplU.AsInteger;
  taAElQ.AsInteger := ADistrQ;
  taAElT.AsInteger := 1;
  case ItType of
    5 : taAElOPERID.AsString :=  dm.RejOperId;  // ���� ����, ����� ����������� �� �������� �������� �����
    7 : taAElOPERID.AsString := dm.NkOperId;  // ���� ����������, ����� ����������� �� ��������
    3 : taAElOPERID.AsString := taWHApplOPERID.AsString;
    0, 6 : taAElOPERID.AsString := taWHApplOPERID.AsString;
    1 : taAElOPERID.AsString := AOperId;
  end;

  if ADistrKind = 0 then // ������
  begin
    taAElOPERIDFROM.AsString := taWHApplOPERID.AsString;
  end
  else begin
    taAElOPERIDFROM.AsVariant := taWHApplOPERID.AsString;
  end;
  taAElART2ID_OLD.AsVariant := null;
end;

procedure TdmAppl.taRejInvNewRecord(DataSet: TDataSet);
begin
  case ADistrType of
    adtSOEl : begin
      taRejInvDEPID.AsInteger   := dmMain.taSOListDEPID.AsInteger;
      taRejInvUSERID.AsInteger  := dm.User.UserId;
      taRejInvREF.AsInteger       := dmMain.taSOElID.AsInteger;
      taRejInvWORDERID.AsVariant  := Null;
    end;
    adtWOrder, adtRej, adtMdfArt : begin
      taRejInvDEPID.AsInteger := WhFrom;
      taRejInvUSERID.AsInteger := dm.User.UserId;
      taRejInvREF.AsInteger := woItemId;
      taRejInvWORDERID.AsInteger := dmMain.taWOrderListWORDERID.AsInteger;
    end;
    adtProd : begin
      taRejInvDEPID.AsInteger := dmMain.taASListFROMDEPID.AsInteger;
      taRejInvUSERID.AsInteger := dm.User.UserId;
      taRejInvREF.AsInteger := dmMain.taASListINVID.AsInteger;
      taRejInvWORDERID.AsVariant := Null;
    end;
    adtDIEl : begin
      taRejInvDEPID.AsInteger := dmMain.taDIListFROMDEPID.AsInteger;
      taRejInvUSERID.AsInteger := dm.User.UserId;
      taRejInvREF.AsInteger := dmMain.taDIElID.AsInteger;
      taRejInvWORDERID.AsVariant := Null;
      //raise Exception.Create(rsNotImplementation);
    end;
    else raise EInternal.Create(Format(rsInternalError, ['209']));
  end;
  taRejInvINVID.AsInteger := dm.GetId(12);
  taRejInvDOCNO.AsInteger := dm.GetId(23);
  taRejInvDOCDATE.AsDateTime := Today;
  taRejInvITYPE.AsInteger := 8;
  RInvId := taRejInvINVID.AsInteger;
end;

procedure TdmAppl.taRejElNewRecord(DataSet: TDataSet);
begin
  taRejElID.AsInteger := dm.GetId(34);
  //ShowMessage('RInvID:'+ IntToStr(RInvID));
  //ShowMessage('INVID:'+ dmMain.taDIListINVID.AsString);
  taRejElINVID.AsInteger := RInvId;
  taRejElARTID.AsInteger := taWHApplARTID.AsInteger;
  taRejElART2ID.AsInteger := taWHApplART2ID.AsInteger;
  taRejElSZID.AsInteger := taWHApplSZID.AsInteger;
  taRejElQ.AsInteger := ADistrQ;
  taRejElT.AsInteger := 2;
  (* ������� ��������� *)
  taRejElU.AsInteger := taWHApplU.AsInteger;
  taRejElOPERID.AsString := taWHApplOPERID.AsString;
 // ShowMessage('Operid: ' + taRejELOperId.AsString);
  taRejElCOMMENTID.AsInteger := 2;
  //  taRejEl.Post;
end;

procedure TdmAppl.taRejInvBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ID'].AsInteger := RInvId;
end;

procedure TdmAppl.taRejElBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := RInvId;
end;



procedure TdmAppl.taPElBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := PInvId;
   // ShowMessage('PINVID '+ intToStr(PInvID));
end;

procedure TdmAppl.taPElNewRecord(DataSet: TDataSet);
begin
  taPElID.AsInteger := dm.GetId(34);
  taPElINVID.AsInteger := PInvId;
  taPElT.AsInteger := 3;
  taPElU.AsInteger := 1;
  taPElOPERID.AsString := AOperId;
  taPElSZID.AsInteger := -1;
  taPElCOMMENTID.AsInteger := 3;
end;

procedure TdmAppl.taPInvBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ID'].AsInteger := PInvId;
end;

procedure TdmAppl.taPInvNewRecord(DataSet: TDataSet);
begin
  case ADistrType of
    adtWorder, adtMdfArt : begin
      taPInvDEPID.AsInteger := WhFrom;
      taPInvUSERID.AsInteger := dm.User.UserId;
      taPInvREF.AsInteger := woItemId;
      taPInvWORDERID.AsInteger := dmMain.taWOrderListWORDERID.AsInteger;
    end;
    adtProd : begin
      taPInvDEPID.AsInteger := dmMain.taASListFROMDEPID.AsInteger;
      taPInvUSERID.AsInteger := dm.User.UserId;
      taPInvREF.AsVariant := dmMain.taASListINVID.AsInteger;
      taPInvWORDERID.AsVariant := Null;
    end;
    adtSOEl, adtRej : raise Exception.Create(rsNotImplementation);
    adtDIEl : begin
      taPInvDEPID.AsInteger := dmMain.taDIListFROMDEPID.AsInteger;
      taPInvUSERID.AsInteger := dm.User.UserId;
      taPInvREF.AsInteger := dmMain.taDIElID.AsInteger;
      taPInvWORDERID.AsVariant := Null;
    end;
    else raise EInternal.Create(Format(rsInternalError, ['209']));
  end;
  taPInvINVID.AsInteger := dm.GetId(12);
  taPInvDOCNO.AsInteger := dm.GetId(23);
  taPInvDOCDATE.AsDateTime := Today;
  taPInvITYPE.AsInteger := 9;
  PInvId := taPInvINVID.AsInteger;
end;

procedure TdmAppl.taPElBeforePost(DataSet: TDataSet);
begin
  if(taPElARTID.IsNull) then raise Exception.Create('�������� �������');
  if(taPElSZID.IsNull) then raise Exception.Create('�������� ������');
  if(taPElQ.IsNull or (taPElQ.AsInteger = 0)) then raise Exception.Create('������� ���-��');
  if(taPElOPERID.IsNull)then raise Exception.Create('�������� ��������');
end;

procedure TdmAppl.RefreshWh(DataSet: TpFIBDataSet; OperField : string='OPERID');
begin
  PostDataSet(taWhAppl);
  if not Assigned(DataSet.FindField('ART2ID')) then
  begin
    MessageDialog('�� ������� ���� ART2ID � '+DataSet.Name, mtError, [mbOk], 0);
    eXit;
  end;
  if not Assigned(DataSet.FindField('SZID')) then
  begin
    MessageDialog('�� ������� ���� SZID � '+DataSet.Name, mtError, [mbOk], 0);
    eXit;
  end;
  if not Assigned(DataSet.FindField(OperField)) then
  begin
    MessageDialog('�� ������� ���� '+OperField+' � '+DataSet.Name, mtError, [mbOk], 0);
    eXit;
  end;
  if taWhAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([DataSet.FieldByName('ART2ID').AsInteger,
         DataSet.FieldByName('SZID').AsInteger, DataSet.FieldByName(OperField).AsString]), [])
  then taWhAppl.Refresh;
end;

procedure TdmAppl.CommitRetainingWithRefreshWh(DataSet: TDataSet);
begin
  CommitRetaining(DataSet);
  if DataSet.Name = 'taAInv' then taAInv.Refresh; 
  if DataSet.Name = 'taRejInv' then taRejInv.Refresh;
  if DataSet.Name = 'taPInv' then taPInv.Refresh;
  if DataSet.Name = 'taAEl' then RefreshWh(TpFIBDataSet(DataSet), 'OPERIDFROM')
  else RefreshWh(TpFIBDataSet(DataSet));
  FFieldChange := False;
  FWhQValue := 0;
  FInvQValue := 0;
end;

procedure TdmAppl.taAInsBeforeOpen(DataSet: TDataSet);
begin
  dmAppl.IsWorkWithStone := True;
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['WORDERID'].AsInteger := wOrderId;
    ByName['ART2ID'].AsInteger := dm.AArt2Id;
  end
end;

procedure TdmAppl.taAInsCalcFields(DataSet: TDataSet);
var
  r : Variant;
begin
  if taAInsSEMIS.AsString <> '' then
  begin
    r := ExecSelectSQL('select EDGSHAPEID, EDGTID, CLEANNES, CHROMATICITY, IGR, COLOR '+
        'from D_Semis where SemisId="'+taAInsSEMIS.AsString+'"', quTmp);
    try
      try taAInsEDGSHAPEID.AsString := VarToStr(r[0]) except end;
      try taAInsEDGTYPEID.AsString := VarToStr(r[1]); except end;
      try taAInsCLEANNES.AsString := VarToStr(r[2]); except end;
      try taAInsCHROMATICITY.AsString := VarToStr(r[3]); except end;
      try taAInsIGR.AsString := VarToStr(r[4]); except end;
      try taAInsCOLOR.AsString := VarToStr(r[5]); except end;
    except
    end;
  end;
  r := ExecSelectSQL('select Art, Art2 from Art2 where Art2Id='+IntToStr(dm.AArt2Id),
     quTmp);
  taAInsART.AsString := r[0]+' '+r[1];
end;

procedure TdmAppl.taAInsNewRecord(DataSet: TDataSet);
begin
  taAInsWOITEMID.AsInteger := dm.GetId(3);
  taAInsOPERID.AsString := AOperId;
  taAInsWORDERID.AsInteger := wOrderId;
  taAInsINVID.AsInteger := InvId;
  taAInsDOCNO.AsInteger := ADocNo;
  taAInsDOCDATE.AsDateTime := ADocDate;
  taAInsITDATE.AsDateTime := ADocDate;
  taAInsITTYPE.AsInteger := AInsItType;

  case ADistrType of
    adtWOrder : ;
    adtDIEl, adtSOEl, adtRej : raise EInternal.Create(Format(rsInternalError, ['151']));
    else raise EInternal.Create(Format(rsInternalError, ['150']));
  end;

  (* � ����������� �� ���� ����� ��� �������� ���������� *)
  case ItType of
    0, 6 : begin
      taAInsDEPID.AsInteger := WhFrom;
      taAInsUSERID.AsInteger := UserTo;
      taAInsJOBDEPID.AsInteger := WhTo;
      taAInsJOBID.AsInteger := UserFrom;
    end;
    1,3,5,7 : begin
       taAInsDEPID.AsInteger := WhTo;
       taAInsUSERID.AsInteger := UserFrom;
       taAInsJOBDEPID.AsInteger := WhFrom;
       taAInsJOBID.AsInteger := UserTo;
    end;
  end;

  taAInsQ.AsFloat := 0;
  taAInsW.AsFloat := 0;

  taAInsAINV.AsInteger := AInvId;
  taAInsREF.AsInteger := woItemId;
  taAInsE.AsInteger := 0;
  taAInsART2ID.AsInteger := dm.AArt2Id;
end;

procedure TdmAppl.taAInsBeforePost(DataSet: TDataSet);
begin
  if(taAInsQ.IsNull or (taAInsQ.AsInteger = 0)) then raise Exception.Create('������� ���-��!');
  if(taAInsW.IsNull or (taAInsW.AsFloat = 0)) then raise Exception.Create('������� ���!');
end;

procedure TdmAppl.taPElQChange(Sender: TField);
begin
  if not(Sender.DataSet.State in [dsInsert]) then
    ChangeQInv(Sender, taPElOPERID, taPElARTID, taPElART2ID, taPElSZID, taPElU, 0);
  taPInv.Refresh;
end;

procedure TdmAppl.taAElQChange(Sender: TField);
begin
  if not(Sender.DataSet.State in [dsInsert]) then
    if taAElART2ID_OLD.IsNull then ChangeQInv(Sender, taAElOPERIDFROM, taPElARTID, taAElART2ID, taAElSZID, taAElU, 1)
    else ChangeQInv(Sender, taAElOPERIDFROM, taPElARTID, taAElART2ID_OLD, taAElSZID, taAElU, 1);
  taAInv.Refresh;
end;

procedure TdmAppl.taRejElQChange(Sender: TField);
begin
  if not(Sender.DataSet.State in [dsInsert]) then
    ChangeQInv(Sender, taRejElOPERID, taREjElARTID, taRejElART2ID, taRejElSZID, taRejElU, 1);
  taRejInv.Refresh;
end;

procedure TdmAppl.CancelWh(DataSet: TDataSet);
begin
  if taWhAppl.State in [dsEdit, dsInsert] then taWhAppl.Cancel;
end;

procedure TdmAppl.InitQValue(DataSet: TDataSet);
begin
  FFieldChange := False;
  FWhQValue := 0;
  FInvQValue := 0;
end;

procedure TdmAppl.ChangeQInv(Sender : TField; OperId, ArtId, Art2Id, SzId, U : TField; T : byte);
var
  Id : integer;
begin
  if not FFieldChange then
  try
    FFieldChange := True;
    FInvQValue := Sender.OldValue;
    if (taWhAppl.State in [dsEdit, dsInsert]) then
    begin
      FWhQValue := taWhApplQ.AsInteger;
      if T=0 then taWHApplQ.AsInteger := FWhQValue + Sender.NewValue - FInvQValue
      else taWHApplQ.AsInteger := FWhQValue + FInvQValue - Sender.NewValue;
    end
    else
      if not taWhAppl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([Art2Id.AsInteger, SzId.AsInteger, OperId.AsString, U.AsInteger]), []) then
      begin
        taWhAppl.Insert;
        Id := ExecSelectSQL('select WhApplId from Check_WhAppl('+IntToStr(WhFrom)+
          ',"'+OperId.AsString+'", '+Art2Id.AsString+','+SzId.AsString+','+U.AsString+')', quTmp);
        FWhQValue := ExecSelectSQL('select Q from WhAppl where Id='+IntToStr(Id), quTmp);
        taWHApplID.AsInteger := Id;
        taWHApplPSID.AsInteger := WhFrom;
        taWHApplOPERID.AsString := OperId.AsString;
        taWhApplARTID.AsInteger := ArtId.AsInteger;
        taWHApplART2ID.AsInteger := Art2Id.AsInteger;
        taWHApplSZID.AsInteger := SzId.AsInteger;
        taWHApplU.AsInteger := U.AsInteger;
        //taWHApplART2.AsString := Art2.AsString;
        //taWHApplSZ.AsString := DataSet.FieldByName('SZ').AsString;
        //taWHApplOPERNAME.AsString := OperName.AsString;
        //taWHApplART.AsString := DataSet.FieldByName('ART').AsString;
        if T=0 then taWHApplQ.AsInteger := FWhQValue + Sender.NewValue - FInvQValue
        else taWHApplQ.AsInteger := FWhQValue + FInvQValue - Sender.NewValue;
      end
      else begin
        if not(taWhAppl.State in [dsEdit, dsInsert]) then taWhAppl.Edit;
        FWhQValue := taWHApplQ.AsInteger;
        if T=0 then taWHApplQ.AsInteger := FWhQValue + Sender.NewValue - FInvQValue
        else taWHApplQ.AsInteger := FWhQValue + FInvQValue - Sender.NewValue;
      end
  except
    on E:Exception do begin
      MessageDialog('������ � ��������� ChangeQInv', mtError, [mbOk], 0);
      eXit;
    end;
  end
  else try
    if not(taWhAppl.State in [dsEdit, dsInsert]) then taWhAppl.Edit;
    taWHApplQ.AsInteger := FWhQValue + Sender.NewValue - FInvQValue;
  except
    on E:Exception do begin
      MessageDialog('������ � ��������� ChangeQInv Place2', mtError, [mbOk], 0);
      eXit;
    end;
  end
end;


procedure TdmAppl.taAElART2_OLDGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text := '<�����>'
  else Text := Sender.AsString;
end;

procedure TdmAppl.CommitRetainingWithRefreshQ(DataSet: TDataSet);
begin
  CommitRetaining(DataSet);
  if DataSet.Name = 'taAEl' then taAInv.Refresh;
  if DataSet.Name = 'taRejInv' then taRejInv.Refresh;
  if DataSet.Name = 'taPInv' then taPInv.Refresh;
  {if DataSet.Name = 'taAEl' then RefreshWh(TpFIBDataSet(DataSet), 'OPERIDFROM')
  else RefreshWh(TpFIBDataSet(DataSet));}
  FFieldChange := False;
  FWhQValue := 0;
  FInvQValue := 0;
end;

function TdmAppl.GenPattern(Art2Id: integer): string;
begin
  Result := ExecSelectSQL('select Art2 from gen_Art2('+IntToStr(Art2Id)+',0)', quTmp);
end;

procedure TdmAppl.taWHApplCalcFields(DataSet: TDataSet);
begin
  taWHApplRecNo.AsInteger := DataSet.RecNo;
end;

procedure TdmAppl.taAElCalcFields(DataSet: TDataSet);
begin
  taAElRecNo.AsInteger := DataSet.RecNo;
end;

procedure TdmAppl.taAInsITTYPEGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           0: Text := '������';
           1: Text := '�����';
           3: Text := '�������';
           5: Text := '����';
           6: Text := '���������';
         end;
end;

procedure TdmAppl.taAInsITTYPESetText(Sender: TField; const Text: String);
begin
  if Text = '������' then Sender.AsInteger := 0;
  if Text = '�����' then Sender.AsInteger := 1;
  if Text = '�������' then Sender.AsInteger := 3;
  if Text = '����' then Sender.AsInteger := 5;
  if Text = '���������' then Sender.AsInteger := 6;
end;

procedure TdmAppl.UGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  case Sender.AsInteger of
    1 : Text := '��.';
    2 : Text := '����';
    else Text := '�� ����������'
  end;
end;

procedure TdmAppl.USetText(Sender: TField; const Text: String);
begin
  if(Text = '��.')then Sender.AsInteger := 1
  else if(Text = '����')then Sender.AsInteger := 2
  else Sender.Clear;
end;

procedure TdmAppl.taAElAfterOpen(DataSet: TDataSet);
begin
  FFieldChange := False;
  FWhQValue := 0;
  FInvQValue := 0;
  if dm.WorkMode = 'aDistr' then
    if Assigned(fmADistr) and fmADistr.acShowStone.Checked then
    begin
      dm.AArt2Id := taAElART2ID.AsInteger;
      ReOpenDataSet(taAIns);
    end
end;

procedure TdmAppl.taAElAfterScroll(DataSet: TDataSet);
begin
  FFieldChange := False;
  FWhQValue := 0;
  FInvQValue := 0;
  if(dm.WorkMode = 'aDistr')then
    if Assigned(fmADistr) and fmADistr.acShowStone.Checked then
    begin
      dm.AArt2Id := taAElART2ID.AsInteger;
      ReOpenDataSet(taAIns);
    end
end;

procedure TdmAppl.taApplAfterOpen(DataSet: TDataSet);
begin
  if Screen.ActiveForm.InheritsFrom(TfmAppl) and
     TfmAppl(Screen.ActiveForm).ShowWh then
  begin
    dm.AArt2Id := dmAppl.taApplART2ID.AsInteger;
    dm.ASzId := dmAppl.taApplSZ.AsInteger;
    ReOpenDataSet(taWhA2);
    ReOpenDataSet(taWhA2d);
  end;
  if Assigned(FArtImage) then FArtImage.Caption := '�������: '+dmAppl.taApplART.AsString;
end;

procedure TdmAppl.taWhA2BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['ART2ID'].AsInteger := dm.AArt2Id;
    ByName['SZ'].AsInteger := dm.ASzId;
  end;
end;

procedure TdmAppl.taApplAfterScroll(DataSet: TDataSet);
begin
  if Screen.ActiveForm.InheritsFrom(TfmAppl) and
    TfmAppl(Screen.ActiveForm).ShowWh then
  begin
    dm.AArt2Id := dmAppl.taApplART2ID.AsInteger;
    dm.ASzId := dmAppl.taApplSZ.AsInteger;
    ReOpenDataSet(taWhA2);
    ReOpenDataSet(taWhA2d);
  end;
  if Assigned(FArtImage) then FArtImage.Caption := '�������: '+dmAppl.taApplART.AsString;
end;

procedure TdmAppl.taApplNewRecord(DataSet: TDataSet);
begin
  taApplID.AsInteger := dm.GetId(75);
  taApplAPPLID.AsInteger := taAListID.AsInteger;
  taApplR.AsInteger := 0;
  taApplMODEL.AsInteger := 0;
  taApplWQ.AsFloat := 0;
  taApplMW.AsFloat := 0;
  taApplQ.AsInteger := 0;
  taApplINS.AsFloat := 0;
  taApplINW.AsFloat := 0;
  taApplINQ.AsInteger := 0;
  taApplDEBTORS.AsFloat := 0;
  taApplDEBTORW.AsFloat := 0;
  taApplDEBTORQ.AsFloat := 0;
  taApplREJS.AsFloat := 0;
  taApplREJW.AsFloat := 0;
  taApplREJQ.AsFloat := 0;
  taApplRETS.AsFloat := 0;
  taApplRETW.AsFloat := 0;
  taApplRETQ.AsInteger := 0;
  taApplSALES.AsFloat := 0;
  taApplSALEW.AsFloat := 0;
  taApplSALEQ.AsInteger := 0;
  taApplOUTS.AsFloat := 0;
  taApplOUTW.AsFloat := 0;
  taApplOUTQ.AsInteger := 0;
  taApplSALES.AsFloat := 0;
  taApplSALEW.AsFloat := 0;
  taApplSALEQ.AsInteger := 0;
  taApplSTOREW.AsFloat := 0;
  taApplSTOREQ.AsInteger := 0;
  taApplPSTOREW.AsFloat := 0;
  taApplPSTOREQ.AsInteger := 0;
  taApplNQ.AsInteger := 0;
  taApplNQ_APPL.AsInteger := 0;
  taApplKOEF.AsInteger := 0;
  FIsNewArt := False;
end;

procedure TdmAppl.taApplBeforeClose(DataSet: TDataSet);
begin
  TmpApplId := taAppl.ParamByName('ID').AsInteger;
end;

procedure TdmAppl.taApplBeforeEdit(DataSet: TDataSet);
begin
  if(dm.WorkMode='aDistr')then
  begin
    if(taApplInvISPROCESS.AsInteger = 1)then
      raise Exception.Create('������ ����������');  
  end
  else begin

    //ShowMessage('DM, 1482:'+taApplAPPLID.AsString);
    //ShowMessage('DM, 1484: '+ taAListID.AsString);
    if(ApplKind = '������') then
      if taAListISCLOSE.AsInteger = 1 then
        raise Exception.Create('������ �������!');
    if(ApplKind = '������ ������������')then
      if taAListISPROCESS.AsInteger = 1 then
        raise Exception.Create('������ ����������!');
  end;
end;

procedure TdmAppl.taRejElCOMMENTIDGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text := '<���>'
  else case Sender.AsInteger of
    1 : Text := '������� ������';
    2 : Text := '������� �������';
    3 : Text := '���� �������';
    4 : Text := '������������ ����';
    6 : Text := '���� ����';
    7 : Text := '���� ����������';
    9 : Text := '���� � ��������, �� ������� ��������� ���� ������� ��� ��������������';
    10 : Text := '���� ��� ��������� ������� ��������';
    else Text := '<���>';
  end;
end;

procedure TdmAppl.taPElCOMMENTIDGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text := '<���>'
  else case Sender.AsInteger of
    1 : Text := '������� ������';
    2 : Text := '������� �������';
    3 : Text := '�������� �������';
    5 : Text := '�������� ������� �� ������';
    8 : Text := '�������� �� ������';
    9 : Text := '�������� �� ��������, � ����� � ���������� ���� ��� ��������������';
    10 : Text := '�������� ��� ��������� ������� ��������';
    else Text := '<���>';
  end;
end;

procedure TdmAppl.taWHApplNewRecord(DataSet: TDataSet);
begin
  taWHApplID.Asinteger := NewWhApplId;
end;

procedure TdmAppl.DataModuleCreate(Sender: TObject);
begin
  FArtImage := nil;
end;

procedure TdmAppl.taApplBeforePost(DataSet: TDataSet);
begin
  FSaveState := DataSet.State;
end;

procedure TdmAppl.taApplAfterPost(DataSet: TDataSet);
var
  jaId : integer;
begin
  FIsNewArt := False;
  if FSaveState = dsInsert then
  begin
    dbUtil.CommitRetaining(DataSet);
    jaId := taApplID.AsInteger;
    ReOpenDataSet(DataSet);
    DataSet.Locate('ID', jaId, []);
    FSaveState := dsBrowse;
  end
  else begin
    PostDataSet(taAList);
    RefreshDataSet(taAList);
  end;
  dbUtil.CommitRetaining(DataSet);
end;

procedure TdmAppl.taApplFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := taApplQ.AsInteger <> 0;
end;

procedure TdmAppl.taAProdListBeforeOpen(DataSet: TDataSet);
begin
  {with TpFIBDataSet(DataSet).Params do
    ByName('OPERID').AsString := ApplOper;}
     //and i.OPERID =:OPERID
end;

procedure TdmAppl.taAPInvBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ID'].AsInteger := APInvId;
end;

procedure TdmAppl.taAPInvNewRecord(DataSet: TDataSet);
begin
  taAPInvINVID.AsInteger := dm.GetId(12);
  taAPInvDOCNO.AsInteger := dm.GetId(23);
  taAPInvDOCDATE.AsDateTime := Today;

  taAPInvDEPID.AsInteger  := dmMain.taWOrderDEPID.AsInteger; // dm.User.wWhId;
  taAPInvJOBID.AsInteger  := dmMain.taWOrderJOBID.AsInteger;
  taAPInvUSERID.AsInteger := dmMain.taWOrderUSERID.AsInteger;

  taAPInvREF.AsInteger := dmMain.taWOrderWOITEMID.AsInteger;
  taAPInvWORDERID.AsInteger := dmMain.taWOrderWORDERID.AsInteger;
  taAPInvITYPE.AsInteger := 7;
end;

procedure TdmAppl.taAPElNewRecord(DataSet: TDataSet);
begin
  taAPElID.AsInteger := dm.GetId(34);
  taAPElU.AsInteger := 1;
  (* ��������� �� �� ����� ���� ������ ���-�� �������, ���� ������� ��. ���. ���� ��.*)
  taAPElINVID.AsInteger := APInvId;
  taAPElARTID.AsInteger := taApplARTID.AsInteger;
  taAPElART2ID.AsInteger := taApplART2ID.AsInteger;
  taAPElSZID.AsInteger := taApplSZ.AsInteger;

  taAPElQ.AsInteger := ADistrQ;
  taAPElT.AsInteger := 0;
  case dmAppl.ItType of
    7 : taAPElOPERID.AsString := dm.NkOperId;// ����������
    5 : taAPElOPERID.AsString := dm.RejOperId;// ����
    else taAPElOPERID.AsString := ApplOper;
  end;
end;

procedure TdmAppl.taAPElBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := taAPInvINVID.AsInteger;
end;

procedure TdmAppl.taAPElPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  DataSet.Cancel;
  Action := daFail;
end;

procedure TdmAppl.taApplInvBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['APPLID'].AsInteger := ApplInvId;
end;

procedure TdmAppl.taAPElQChange(Sender: TField);
begin
  if not(Sender.DataSet.State in [dsInsert]) then
  begin
    PostDataSet(Sender.DataSet);
    taAppl.Locate('ART2ID;SZ', VarArrayOf([taAPElART2ID.AsInteger, taAPElSZID.Asinteger]), []);
    RefreshDataSet(taAppl);
  end;
end;

procedure TdmAppl.taRejElPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
{  DataSet.Cancel;
  Action := daFail;}
end;

procedure TdmAppl.taPElPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  CancelDataSet(DataSet);
  Action := daFail;
end;

procedure TdmAppl.SetADistrType(const Value: TADistrType);
begin
  FADistrType := Value;
end;

procedure TdmAppl.taExecApplAfterClose(DataSet: TDataSet);
var
  i : integer;
begin
  (* ������� ����������� ���� *)
  i := 0;
  while i<=Pred(DataSet.FieldCount) do
  begin
    if(Pos('_', DataSet.Fields[i].FieldName)<>0)or
      (DataSet.Fields[i].FieldName[1]='T') then DataSet.Fields[i].Free
    else inc(i);
  end;
end;

procedure TdmAppl.taRejAddBeforeOpen(DataSet: TDataSet);
var
  Bd, Ed : TDateTime;
begin
  RejAddPeriod(Bd, Ed);
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(Bd);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(Ed);
  end;
end;

procedure TdmAppl.RejAddPeriod(var Bd, Ed: TDateTime);
var
  InvId : integer;
begin
  InvId := ExecSelectSQL('select max(i.InvId) from Inv i '+
    'where i.InvId <> (select max(InvId) from Inv where IType=1) and '+
    '      i.IType=1', quTmp);
  Bd := ExecSelectSQL('select DocDate from Inv i where InvId='+IntToStr(InvId), quTmp);
  Ed := EndOfTheDay(Today);
end;

procedure TdmAppl.taApplSZNAMEChange(Sender: TField);
var
  SzId : integer;
begin
  if(Sender.DataSet.State = dsInsert)then
  begin
    SzId := ExecSelectSQL('select Id from D_Sz where Name="'+Sender.AsString+'"', quTmp);
    if(SzId = 0)then raise Exception.Create('������� ������ ������');
    taApplSZ.AsInteger := SzId;
    if(taApplART.AsString <> '')and(not FIsNewArt)then
      taApplMW.AsFloat := ExecSelectSQL('select AW from Average_Weight('+
          taApplARTID.AsString+','+taApplSZ.AsString+')', quTmp);
  end;
end;

procedure TdmAppl.taApplPInvNewRecord(DataSet: TDataSet);
begin
  taApplPInvDOCNO.AsInteger := dm.GetId(23);
  taApplPInvDOCDATE.AsDateTime := Today;
  //taApplPInvDEPID.AsInteger := ApplWh; { TODO : !!! }
  taApplPInvUSERID.AsInteger := dm.User.UserId;
  taApplPInvITYPE.AsInteger := 19;
  taApplPInvREF.AsInteger := taAListID.AsInteger; { TODO : !!! }
  taApplPInvWORDERID.AsVariant := Null;
  ApplPInvId := taApplPInvINVID.AsInteger;
end;

procedure TdmAppl.taApplPInvBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ID'].AsInteger := ApplPInvId;
end;

procedure TdmAppl.taApplRejInvNewRecord(DataSet: TDataSet);
begin
  taApplRejInvDOCNO.AsInteger    := dm.GetId(23);
  taApplRejInvDOCDATE.AsDateTime := Today;
  taApplRejInvITYPE.AsInteger    := 18;
  taApplRejInvUSERID.AsInteger   := dm.User.UserId;
  taApplRejInvREF.AsInteger      := dmAppl.taAListID.AsInteger;
  taApplRejInvWORDERID.AsVariant := Null;
  ApplRejInvId                   := taApplRejInvINVID.AsInteger;
end;

procedure TdmAppl.taApplRespBeforeOpen(DataSet: TDataSet);
begin
  taApplResp.ParamByName('ID').AsInteger := taAList.FieldByName('ID').AsInteger;
end;

procedure TdmAppl.taApplRejInvBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ID'].AsInteger := ApplRejInvId;
end;

procedure TdmAppl.taApplRejElBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := ApplRejInvId;
end;

procedure TdmAppl.taApplPElBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := ApplPInvId;
end;

procedure TdmAppl.taApplPElNewRecord(DataSet: TDataSet);
begin
  taApplPElINVID.AsInteger := ApplPInvId;
  taApplPElT.AsInteger := 3;
  taApplPElCOMMENTID.AsInteger := 8; // �������� �� ������
end;

procedure TdmAppl.taApplRejElPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  DataSet.Cancel;
  Action := daFail;
end;

procedure TdmAppl.taApplRejElNewRecord(DataSet: TDataSet);
begin
  taApplRejElID.AsInteger := dm.GetId(34);
  taRejElINVID.AsInteger := ApplRejInvId;
  taRejElT.AsInteger := 2;
  taRejElCOMMENTID.AsInteger := 8; // ���� �� ������
end;

procedure TdmAppl.taApplPElPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  DataSet.Cancel;
  Action := daFail;
end;

procedure TdmAppl.taApplARTValidate(Sender: TField);
var
  ArtId : Variant;
begin
  if(Sender.DataSet.State = dsInsert)then
  begin
    //if not dm.CheckValidArt(Sender.AsString) then raise Exception.Create(rsInvalidArtFormat);
    ArtId := ExecSelectSQL('select D_ArtId from D_Art where Art='+ #39 + Sender.AsString + #39, quTmp);
    if VarIsNull(ArtId)or(ArtId = 0) then
    begin
      taApplARTID.AsInteger := dm.InsertArt(Sender.AsString);
      ShowMWForm(Sender.AsString, taApplARTID.AsInteger);
      taApplMW.AsFloat := Editor.vResult;
      ExecSQL('update D_Art set MW='+StringReplace(taApplMW.AsString, ',', '.', [rfReplaceAll])+
              ' where D_ArtId='+taApplARTID.AsString, quTmp);
      FIsNewArt := True;
    end
    else begin
      FIsNewArt := False;
      taApplARTID.AsInteger := ArtId;
      if(taApplSZNAME.AsString <> '') then
        taApplMW.AsFloat := ExecSelectSQL('select AW from Average_Weight('+VarToStr(ArtId)+','+taApplSZ.AsString+')', quTmp);
    end;
  end
end;

procedure TdmAppl.taAInsBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

function TdmAppl._ShowEditorQ(Q: integer; U, U_Old, A: byte): boolean;
 (*
     A = 0 - �������� �������������� ��. ���. � ����� TfmeQ
     A = 1 - ��������� �������������� ��.���. � ����� TfmeQ
 *)
begin
  Result := False;
  if(U = 2)and(U_Old = 1)then
  begin
    if(Q mod 2 <> 0)then raise Exception.Create('�������� �� ���������. �������� ���-�� �������')
    else Q := Round(Q/2);
    //Q := 1;//???

  end;
  if(U = 1)and(U_Old = 2)then Q := 2*Q;

  Self.ADistrQ := Q;
  Self.ADistrU := U;
  eQ.fmeKind := A;
  case A of
    0 : begin
      if(Q = 1)then
      begin
        if (U = 1)then // ��. ���. = ��.
        begin
          Result := True;
          eXit;
        end
        else Result := ShowEditor(TfmeQ, TfmEditor(fmeQ)) = mrOk;
      end
      else
        Result := ShowEditor(TfmeQ, TfmEditor(fmeQ)) = mrOk
    end;
    1 : begin
      if(Q = 1)then
      begin
        Result := True;
        eXit;
      end
      else Result := ShowEditor(TfmeQ, TfmEditor(fmeQ)) = mrOk;
    end;
    2 : begin
      Result := ShowEditor(TfmeQ, TfmEditor(fmeQ)) = mrOk;
    end;
  end;
  if Result then
  begin
    if(U = 2)and(U_Old = 1)then Self.ADistrQ := 2*Self.ADistrQ;
    if(U = 1)and(U_Old = 2)then
      if(Self.ADistrQ mod 2 <> 0)then raise Exception.Create('�������� �� ���������. �������� ���-�� �������')
      else Self.ADistrQ := Round(Self.ADistrQ/2);
  end;
end;

procedure TdmAppl.taWHApplBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['DEPID'].AsInteger := WhFrom;
    if FilterOperId = ROOT_OPER_CODE then
    begin
      ByName['OPERID1'].AsString := MINSTR;
      ByName['OPERID2'].AsString := MAXSTR;
    end
    else begin
      ByName['OPERID1'].AsString := FilterOperId;
      ByName['OPERID2'].AsString := FilterOperId;
    end
  end;
end;

procedure TdmAppl.taChangeAssortByApplBeforeOpen(DataSet: TDataSet);
begin
  taChangeAssortByAppl.ParamByName('APPLID').AsInteger := ApplInvId;
  taChangeAssortByAppl.ParamByName('DEPID').AsInteger := FWhFrom;
  if (FilterChangeAssortByApplOperId = ROOT_OPER_CODE) then
  begin
    taChangeAssortByAppl.ParamByName('OPERID1').AsString := MINSTR;
    taChangeAssortByAppl.ParamByName('OPERID2').AsString := MAXSTR;
  end
  else begin
    taChangeAssortByAppl.ParamByName('OPERID1').AsString := FilterChangeAssortByApplOperId;
    taChangeAssortByAppl.ParamByName('OPERID2').AsString := FilterChangeAssortByApplOperId;
  end;
end;

procedure TdmAppl.taChangeAssortByApplAfterOpen(DataSet: TDataSet);
begin
  if Assigned(Screen.ActiveForm)and(Screen.ActiveForm.ClassName = 'TfmChangeAssortByAppl')then
    TfmChangeAssortByAppl(Screen.ActiveForm).gridDict.FieldColumns['AFQ'].ReadOnly := (taChangeAssortByApplD.AsInteger<>1);
  dbUtil.CommitRetaining(DataSet);
end;

procedure TdmAppl.taChangeAssortByApplAfterScroll(DataSet: TDataSet);
begin
  if Assigned(Screen.ActiveForm)and(Screen.ActiveForm.ClassName = 'TfmChangeAssortByAppl')then
    TfmChangeAssortByAppl(Screen.ActiveForm).gridDict.FieldColumns['AFQ'].ReadOnly := (taChangeAssortByApplD.AsInteger<>1);
  dbUtil.CommitRetaining(DataSet);
end;

procedure TdmAppl.taWha2dBeforeOpen(DataSet: TDataSet);
begin
  taWha2d.Params.ParamByName('Application$ID').AsInteger := taAListID.AsInteger;
  taWha2d.Params.ParamByName('Sub$Model$ID').AsInteger := dm.AArt2Id;
  taWha2d.Params.ParamByName('Size$ID').AsInteger := dm.ASzId;
end;

procedure TdmAppl.taWha2dAfterOpen(DataSet: TDataSet);
var
  ModelTitle: string;
  AModelTitle: string;
  Q: Integer;
  TotalQ: Integer;
begin
  taWha2m.CreateDataSet;
  taWha2m.DisableControls;
  taWha2d.DisableControls;
  taWha2m.IndexFieldNames := 'Model$Title';
  taWha2d.First;
  if taWha2d.RecordCount <> 0 then
  begin
    Q := taWha2dQUANTITY.AsInteger;
    TotalQ := taWha2dTOTALQUANTITY.AsInteger;
    AModelTitle := taWha2dMODELTITLE.AsString;
    taWhA2d.Next;
    while not taWha2d.Eof do
    begin
      ModelTitle := taWha2dMODELTITLE.AsString;
      if ModelTitle = AModelTitle then Q := Q + taWha2dQUANTITY.AsInteger
      else
      begin
         taWha2m.Append;
         taWha2mMODELTITLE.AsString := AModelTitle;
         taWha2mQUANTITY.AsInteger := Q;
         taWha2mTOTALQUANTITY.AsInteger := TotalQ;
         taWha2mDifference.AsInteger := Q - TotalQ;
         taWha2m.Post;
         AModelTitle := ModelTitle;
         Q := taWha2dQUANTITY.AsInteger;
         TotalQ := taWha2dTOTALQUANTITY.AsInteger;
       end;
       taWha2d.Next;
    end;
    taWha2m.Append;
    taWha2mMODELTITLE.AsString := AModelTitle;
    taWha2mQUANTITY.AsInteger := Q;
    taWha2mTOTALQUANTITY.AsInteger := TotalQ;
    taWha2mDifference.AsInteger := Q - TotalQ;
    taWha2m.Post;
  end;
  taWha2m.First;
  taWha2d.First;
  taWhA2d.EnableControls;
  taWha2m.EnableControls;
end;

procedure TdmAppl.taWha2dAfterClose(DataSet: TDataSet);
begin
  taWha2m.Active := False;
end;

procedure TdmAppl.taWhA2sCalcFields(DataSet: TDataSet);
begin
  taWhA2sDifference.AsInteger := taWhA2sQUANTITY.AsInteger - taWhA2sTOTALQUANTITY.AsInteger;
end;

procedure TdmAppl.CalcRecNo(DataSet: TDataSet);
begin
  DataSet.FieldByName('RecNo').Value := DataSet.RecNo;
  if Assigned(FOnCalculateAppl) then OnCalculateAppl(DataSet);
end;


end.
