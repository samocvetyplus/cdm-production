unit CompLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, DBCtrlsEh, DBLookupEh, DB, FIBDataSet,
  pFIBDataSet, ActnList, DBGridEh;

type
  TfmCompLogin = class(TForm)
    Label1: TLabel;
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
    lcbxComp: TDBLookupComboboxEh;
    chbxTolling: TDBCheckBoxEh;
    lcbxTolling: TDBLookupComboboxEh;
    taComp: TpFIBDataSet;
    cmbxTolling: TDBComboBoxEh;
    dsrComp: TDataSource;
    taTolling: TpFIBDataSet;
    acEvent: TActionList;
    acTolling: TAction;
    dsrTolling: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure acTollingExecute(Sender: TObject);
    procedure acTollingUpdate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure OkBtnClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  end;

var
  fmCompLogin: TfmCompLogin;

implementation

uses DictData, dbUtil, IniFiles, ProductionConsts, fmUtils, uUtils;

{$R *.dfm}

procedure TfmCompLogin.FormCreate(Sender: TObject);
begin
  OpenDataSets([taComp, taTolling]);
end;

procedure TfmCompLogin.acTollingExecute(Sender: TObject);
begin
  exit;
end;

procedure TfmCompLogin.acTollingUpdate(Sender: TObject);
begin
  cmbxTolling.Enabled := chbxTolling.Checked;
  lcbxTolling.Enabled := chbxTolling.Checked;
end;

procedure TfmCompLogin.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Ini : TIniFile;
  SelfCompId, TollingType, TollingCompId: integer;
  UseTolling: boolean;
begin
  case ModalResult of
    mrOk : begin
      if VarIsNull(lcbxComp.KeyValue) then
      begin
        ActiveControl := lcbxComp;
        raise Exception.Create(rsNotDefineCompId);
      end;

      if chbxTolling.Checked then
      begin
        if (cmbxTolling.ItemIndex = -1) then
        begin
          ActiveControl := cmbxTolling;
          raise Exception.Create('�������� ��� ��������');
        end;

        if VarIsNull(lcbxTolling.KeyValue) then
        begin
          ActiveControl := lcbxTolling;
          raise Exception.Create('�������� ����������� ��������');
        end;

        if(lcbxTolling.KeyValue = lcbxComp.KeyValue) then
        begin
          ActiveControl := lcbxTolling;
          raise Exception.Create('�������� ������ ����������� ��������');
        end;
      end;
      SelfCompId := lcbxComp.KeyValue;
      UseTolling := chbxTolling.Checked;
      if UseTolling then
      begin
        TollingType := StrToInt(cmbxTolling.KeyItems[cmbxTolling.ItemIndex]);
        TollingCompId := lcbxTolling.KeyValue;
      end
      else begin
        TollingType := -1;
        TollingCompId := -1;
      end;
      (* �������� � ini-���� *)
      Ini := TIniFile.Create(GetIniFileName);
      try
        Ini.WriteInteger('CompLogin', 'CompId', SelfCompId);
        Ini.WriteString('CompLogin', 'CompName', lcbxComp.Text);
        Ini.WriteBool('CompLogin', 'UseTolling', UseTolling);
        Ini.WriteInteger('CompLogin', 'TollingType', TollingType);
        Ini.WriteInteger('CompLogin', 'TollingCompId', TollingCompId);
        Ini.WriteString('CompLogin', 'TollingCompName', lcbxTolling.Text);
      finally
        Ini.Free;
      end;
    end;
  end;
end;

procedure TfmCompLogin.OkBtnClick(Sender: TObject);
begin
  ModalResult := mrOk; 
end;

procedure TfmCompLogin.Button1Click(Sender: TObject);
begin
  ShowMessage(lcbxComp.DataSource.Name);
  ShowMessage(TpFIBDataSet(lcbxComp.DataSource.DataSet).Sqls.SelectSQL.Text);
end;

end.
