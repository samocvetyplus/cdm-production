object fmSuplierCase: TfmSuplierCase
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088' '#1087#1086#1083#1091#1095#1072#1090#1077#1083#1103
  ClientHeight = 442
  ClientWidth = 321
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 16
    Width = 120
    Height = 13
    Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object DBGridEh1: TDBGridEh
    Left = 0
    Top = 40
    Width = 321
    Height = 353
    DataGrouping.GroupLevels = <>
    DataSource = dsSuplierCase
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'NAME'
        Footers = <>
        Title.Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
        Width = 287
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object btnCansel: TBitBtn
    Left = 200
    Top = 407
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    Kind = bkCancel
  end
  object btnOK: TBitBtn
    Left = 40
    Top = 407
    Width = 75
    Height = 25
    TabOrder = 3
    Kind = bkOK
  end
  object fEdit: TEdit
    Left = 153
    Top = 13
    Width = 158
    Height = 21
    TabOrder = 0
    OnChange = fEditChange
  end
  object taSuplierCase: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  compid,'
      '  name'
      'from'
      '  d_comp')
    AutoUpdateOptions.CanChangeSQLs = True
    Transaction = dm.tr
    Database = dm.db
    Left = 96
    Top = 8
  end
  object dsSuplierCase: TDataSource
    DataSet = taSuplierCase
    Left = 64
    Top = 8
  end
end
