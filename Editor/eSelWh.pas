unit eSelWh;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, DB, FIBDataSet, pFIBDataSet,
  DBGridEh, Mask, DBCtrlsEh, DBLookupEh;

type
  TfmeSelWh = class(TfmEditor)
    Label1: TLabel;
    taDepWh: TpFIBDataSet;
    lcbxDepWh: TDBLookupComboboxEh;
    dsrDepWh: TDataSource;
    taDepWhDEPID: TFIBIntegerField;
    taDepWhDEPNAME: TFIBStringField;
    Label2: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  end;

function ShowSelWh(UserId : integer; UserName: string):integer;

implementation

uses DictData, dbUtil;

{$R *.dfm}

function ShowSelWh(UserId : integer; UserName: string):integer;
var
  fmeSelWh: TfmeSelWh;
begin
  fmeSelWh := TfmeSelWh.Create(Application);
  try
    fmeSelWh.Label2.Caption := Format(fmeSelWh.Label2.Caption, [UserName]);
    fmeSelWh.taDepWh.ParamByName('USERID').AsInteger := UserId;
    OpenDataSet(fmeSelWh.taDepWh);
    fmeSelWh.taDepWh.Last;
    if fmeSelWh.taDepWh.RecordCount = 0 then
    begin
      Result := -1;
    end
    else if fmeSelWh.taDepWh.RecordCount = 1 then
    begin
      Result := fmeSelWh.taDepWhDEPID.AsInteger;
    end
    else begin
      fmeSelWh.taDepWh.First;
      fmeSelWh.ShowModal;
      Result := vResult;
    end;
    CloseDataSet(fmeSelWh.taDepWh);
  except
    FreeAndNil(fmeSelWh);
  end;
end;

procedure TfmeSelWh.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk : vResult := lcbxDepWh.KeyValue;
    else SysUtils.Abort;
  end;
  inherited;
end;

procedure TfmeSelWh.FormShow(Sender: TObject);
begin
  lcbxDepWh.KeyValue := taDepWhDEPID.AsVariant;
end;

end.
