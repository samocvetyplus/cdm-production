unit Export1C_Old;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Mask, ToolEdit, ExtCtrls, dateutils, dbutils,
  dbUtil, xmldom, XMLIntf, msxmldom, XMLDoc, DB, IBCustomDataSet;

type
  TfmExport1C_Old = class(TForm)
    BitBtn1: TBitBtn;
    edFile: TFilenameEdit;
    Label1: TLabel;
    Bevel1: TBevel;
    Label2: TLabel;
    edDate: TDateEdit;
    CheckBox2: TCheckBox;
    XMLDoc: TXMLDocument;
    taComp: TIBDataSet;
    taMat: TIBDataSet;
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BitBtn1Click(Sender: TObject);
  private
    procedure CreateExportXmlFile(FileName : string);
  end;

var
  fmExport1C_Old: TfmExport1C_Old;

implementation

uses PrintData, DictData;

{$R *.dfm}

procedure TfmExport1C_Old.FormCreate(Sender: TObject);
begin
  edDate.Date:=Today;
end;

procedure TfmExport1C_Old.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key=VK_ESCAPE then ModalResult:=mrCancel;
end;

procedure TfmExport1C_Old.BitBtn1Click(Sender: TObject);
var f:TextFile;
begin
  CreateExportXmlFile(edFile.FileName);
  exit;
 try
  AssignFile(f,edFile.FileName);
  if not FileExists(edFile.FileName) then
  begin
     Rewrite(f);
  end
  else if MessageDlg('������������ ����?',mtConfirmation,[mbOk,mbCancel],0)=mrOk then
  begin
     Rewrite(f);
  end;
  dmPrint.taInvChangeReport.ParamByName('ADATE').AsDate:=edDate.Date;
  OpenDataSets([dmPrint.taInvChangeReport]);
  dmPrint.taInvChangeReport.First;
  while not dmPrint.taInvChangeReport.Eof do
  begin
   dmPrint.taInvChangeReport.Next;
   Write(f,INtToStr(dmPrint.taInvChangeReportINVID.AsInteger)+' ');
   write(f,INtToStr(dmPrint.taInvChangeReportDOCNO.AsInteger)+' ');
   write(f,dateToStr(dmPrint.taInvChangeReportDOCDATE.AsDateTime)+' ');
   write(f,dmPrint.taInvChangeReportDEPNAME.Asstring+' ');
   writeln(f,dmPrint.taInvChangeReportSupname.Asstring+' ');
  end;
 //��������� ���������
  if CheckBox2.Checked then
   begin
    writeln(f,'-');
    OpenDataSets([dmPrint.taDelInv]);
    dmPrint.taDelInv.First;
    while not dmPrint.taDelInv.Eof do
    begin
     Write(f,INtToStr(dmPrint.taDelInvINVID.AsInteger)+' ');
     dmPrint.taDelInv.Next;
    end;
  end;
  dmPrint.PrintDocumentA(dkInvReport,VarArrayOf([edDate.date]));
 finally
   CloseFile(F);
 end;
end;

procedure TfmExport1C_Old.CreateExportXmlFile(FileName: string);
var
  Info, Comp, Mat, Item, Att: IXMLNode;
  i : integer;
begin
  XMLDoc.Active := True;
  // ����� ����������
  Info := XMLDoc.DocumentElement.AddChild('����������');
  Att := XMLDoc.CreateNode('��', ntAttribute); Att.NodeValue := dm.db.DatabaseName;
  Info.AttributeNodes.Add(Att);
  Att := XMLDoc.CreateNode('����_��������', ntAttribute); Att.NodeValue := ToDay;
  Info.AttributeNodes.Add(Att);
  Att := XMLDoc.CreateNode('������������', ntAttribute); Att.NodeValue := dm.User.FIO;
  Info.AttributeNodes.Add(Att);

  // �����������
  OpenDataSet(taComp);
  Comp := XMLDoc.DocumentElement.AddChild('�����������');
  while not taComp.Eof do
  begin
    Item := Comp.AddChild('COMP');
    for i:=0 to Pred(taComp.FieldCount) do with taComp.Fields[i] do
      Item.AddChild(FieldName).NodeValue := AsString;
    taComp.Next;
  end;
  CloseDataSet(taComp);

  // ���������
  OpenDataSet(taMat);
  Mat := XMLDoc.DocumentElement.AddChild('���������');
  while not taMat.Eof do
  begin
    Item := Mat.AddChild('MAT');
    for i:=0 to Pred(taMat.FieldCount) do with taMat.Fields[i] do
      Item.AddChild(FieldName).NodeValue := AsString;
    taMat.Next;
  end;
  CloseDataSet(taMat);
  XMLDoc.SaveToFile(FileName);
  XMLDoc.Active := False;
end;

end.
