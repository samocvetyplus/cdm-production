unit eSelAppl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, VirtualTrees, DB,
  FIBDataSet, pFIBDataSet, TB2Dock, TB2Toolbar, ActnList, TB2Item, ImgList;

type
  TfmeSelAppl = class(TfmEditor)
    taSelAppl: TpFIBDataSet;
    vstAppl: TVirtualStringTree;
    TBDock1: TTBDock;
    tlbrFunc: TTBToolbar;
    acEvent: TActionList;
    acChangePeriod: TAction;
    im16: TImageList;
    TBItem1: TTBItem;
    taSelApplID: TFIBIntegerField;
    taSelApplDOCNO: TFIBIntegerField;
    taSelApplDOCDATE: TFIBDateTimeField;
    taSelApplOPERATION: TFIBStringField;
    acCheckAll: TAction;
    acUncheckAll: TAction;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    taSelApplISCLOSE: TFIBSmallIntField;
    taSelApplISPROCESS: TFIBSmallIntField;
    procedure FormCreate(Sender: TObject);
    procedure taSelApplBeforeOpen(DataSet: TDataSet);
    procedure acChangePeriodExecute(Sender: TObject);
    procedure vstApplInitNode(Sender: TBaseVirtualTree; ParentNode,
      Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
    procedure vstApplGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure acCheckAllExecute(Sender: TObject);
    procedure acCheckAllUpdate(Sender: TObject);
    procedure acUncheckAllUpdate(Sender: TObject);
    procedure acUncheckAllExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FBeginDate : TDateTime;
    FEndDate : TDateTime;
    procedure InitTreeList;
    procedure ShowPeriod;
  end;

var
  fmeSelAppl: TfmeSelAppl;
  ApplIds : string;
  ApplDescr : string;

implementation

uses DictData, dbUtil, Period;

{$R *.dfm}

type
  PAppl = ^TAppl;
  TAppl = record
    Id : integer;
    ApplNo : string;
    ApplDate : TDateTime;
    ApplOper : string;
    IsClose : string;
    IsProcess : string;
  end;

var
  ApplEntries : array of TAppl;

procedure TfmeSelAppl.FormCreate(Sender: TObject);
begin
  inherited;
  //tlbrFunc.Skin := dm.TBSkin;
  FBeginDate := dm.BeginDate;
  FEndDate := dm.EndDate;
  ShowPeriod;
  InitTreeList;
end;

procedure TfmeSelAppl.InitTreeList;
var
  i : integer;
begin
  vstAppl.NodeDataSize := SizeOf(TAppl);
  vstAppl.RootNodeCount := 0;
  vstAppl.Clear;
  // ���������� ������
  OpenDataSet(taSelAppl);
  taSelAppl.Last;
  SetLength(ApplEntries, taSelAppl.RecordCount);
  taSelAppl.First;
  i:=0;
  while not taSelAppl.Eof do
  begin
    ApplEntries[i].Id := taSelApplID.AsInteger;
    ApplEntries[i].ApplNo := taSelApplDOCNO.AsString;
    ApplEntries[i].ApplDate := taSelApplDOCDATE.AsDateTime;
    ApplEntries[i].ApplOper := taSelApplOPERATION.AsString;
    if(taSelApplISCLOSE.AsInteger = 1)then ApplEntries[i].IsClose := '��'
    else ApplEntries[i].IsClose := '���';
    if(taSelApplISPROCESS.AsInteger = 1)then ApplEntries[i].IsProcess := '��'
    else ApplEntries[i].IsProcess := '���';
    inc(i);
    taSelAppl.Next;
  end;
  CloseDataSet(taSelAppl);
  vstAppl.NodeDataSize := SizeOf(TAppl);
  vstAppl.RootNodeCount := High(ApplEntries);
end;

procedure TfmeSelAppl.taSelApplBeforeOpen(DataSet: TDataSet);
begin
  taSelAppl.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(FBeginDate);
  taSelAppl.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(FEndDate);
end;

procedure TfmeSelAppl.acChangePeriodExecute(Sender: TObject);
var
  BD, ED : TDateTime;
begin
  BD := FBeginDate;
  ED := FEndDate;
  if(ShowPeriodForm(BD, ED))and((FBeginDate <> BD)or(FEndDate <> ED))then
  begin
    FBeginDate := BD;
    FEndDate := ED;
    ShowPeriod;
    Application.ProcessMessages;
    InitTreeList;
    Application.ProcessMessages;
  end;
end;

procedure TfmeSelAppl.ShowPeriod;
begin
  Self.Caption := '����� ������ ('+DateToStr(FBeginDate)+'-'+DateToStr(FEndDate)+')';
end;

procedure TfmeSelAppl.vstApplInitNode(Sender: TBaseVirtualTree; ParentNode,
  Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
var
  Data: PAppl;
begin
  if ParentNode = nil then
  begin
    //Include(InitialStates, ivsHasChildren);
    Data := Sender.GetNodeData(Node);
    Data^ := ApplEntries[Node.Index mod High(ApplEntries)+1];
    Node.CheckType := ctCheckBox;
  end
end;

procedure TfmeSelAppl.vstApplGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  Data : PAppl;
begin
  if Node.Parent = Sender.RootNode then
  begin
    Data := Sender.GetNodeData(Node);
    case Column of
      0: CellText := Data.ApplNo;
      1: CellText := DateToStr(Data.ApplDate);
      2: CellText := Data.ApplOper;
      3: CellText := Data.IsClose;
      4: CellText := Data.IsProcess;
    end;
  end;
end;

procedure TfmeSelAppl.acCheckAllExecute(Sender: TObject);
var
  Node : PVirtualNode;
begin
  Node := vstAppl.RootNode.FirstChild;
  vstAppl.BeginUpdate;
  try
    while Assigned(Node) do
    begin
      Node.CheckState := csCheckedNormal;
      Application.ProcessMessages;
      Node := Node.NextSibling;
    end;
  finally
    vstAppl.EndUpdate;
  end;
end;

procedure TfmeSelAppl.acCheckAllUpdate(Sender: TObject);
begin
  acCheckAll.Enabled := vstAppl.RootNodeCount <> 0;
end;

procedure TfmeSelAppl.acUncheckAllUpdate(Sender: TObject);
begin
  acUncheckAll.Enabled := (vstAppl.RootNodeCount <> 0);
end;

procedure TfmeSelAppl.acUncheckAllExecute(Sender: TObject);
var
  Node : PVirtualNode;
begin
  Node := vstAppl.RootNode.FirstChild;
  vstAppl.BeginUpdate;
  try
    while Assigned(Node) do
    begin
      Node.CheckState := csUnCheckedNormal;
      Application.ProcessMessages;
      Node := Node.NextSibling;
    end;
  finally
    vstAppl.EndUpdate;
  end;
end;

procedure TfmeSelAppl.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Node : PVirtualNode;
  Data : PAppl;
begin
  case ModalResult of
    mrOk: begin
       Node := vstAppl.RootNode.FirstChild;
       ApplIds := '';
       ApplDescr := '';
       while Assigned(Node) do
       begin
         if(Node.CheckState = csCheckedNormal)then
         begin
           Data := vstAppl.GetNodeData(Node);
           ApplIds := ApplIds+IntToStr(Data.Id)+';';
           ApplDescr := ApplDescr+'#'+Data.ApplNo+'-'+StringReplace(Data.ApplOper, ' ', '_', [rfReplaceAll]) +';';
         end;
         Node := Node.NextSibling;
       end;
       ApplIds := copy(ApplIds, 1, Length(ApplIds)-1);
       ApplDescr := copy(ApplDescr, 1, Length(ApplDescr)-1);
    end;
  end;
  inherited;
end;

end.
