unit eQ;

interface

uses
  Windows, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, DBCtrlsEh, Mask, ExtCtrls;

type
  TfmeQ = class(TfmEditor)
    lbQ: TLabel;
    lbUnit: TLabel;
    cmbxU: TDBComboBoxEh;
    edQ: TDBNumberEditEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);

  end;

var
  fmeQ : TfmeQ;
  fmeKind : byte;

implementation

uses ApplData, MsgDialog;

{$R *.dfm}

procedure TfmeQ.FormCreate(Sender: TObject);
begin
  edQ.Value := dmAppl.ADistrQ;
  cmbxU.ItemIndex := dmAppl.ADistrU - 1;
  case fmeKind of
    0 : ActiveControl := edQ;
    1 : begin
      cmbxU.Enabled := False;
      ActiveControl := edQ;
    end;
    2 : begin
      edQ.Enabled := False;
      cmbxU.Enabled := True;
      ActiveControl := cmbxU;
    end;
  end;
  inherited;
end;



procedure TfmeQ.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk : begin
       if(ActiveControl = btnOk)then ModalResult := mrOk
       else if(ActiveControl = btnCancel)then ModalResult := mrCancel
       else begin
         ActiveControl := FindNextControl(ActiveControl, True, True, False);
         SysUtils.Abort;
       end;
       if(StrToIntDef(edQ.Text, 0) = 0)then
       begin
         ActiveControl := edQ;
         SysUtils.Abort;
       end
       else begin
         if(fmeKind <> 2)then
           if(dmAppl.ADistrQ * dmAppl.ADistrU) < (edQ.Value * StrToInt(cmbxU.KeyItems[cmbxU.ItemIndex]))then
             if MessageDialog('� ������� ��� ������� �������. � ���������� ��������� ������������� �����.'+#13+' ����������?', mtWarning, [mbYes, mbNo], 0) = mrNo then SysUtils.Abort;
         dmAppl.ADistrQ := edQ.Value;
         dmAppl.ADistrU := StrToInt(cmbxU.KeyItems[cmbxU.ItemIndex]);
       end;
    end;
  end;
end;


end.
