{***********************************************}
{  ������ �������                               }
{  Copyrigth (C) 2001-2003 basile for CDM       }
{  v0.65                                        }
{***********************************************}

unit WOrderList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ListAncestor,  ImgList, StdCtrls, Grids, DBGrids, RXDBCtrl,
  ExtCtrls, ComCtrls, Menus, DBCtrls, db,
  RXCtrls, Buttons, DBGridEh, ActnList, TB2Item, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmWOrderList = class(TfmListAncestor)
    chbxClose: TCheckBox;
    spbtnAddRej: TSpeedButton;
    acRejWorder: TAction;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem5: TTBItem;
    acTotal: TAction;
    procedure FormCreate(Sender: TObject);
    procedure chbxCloseClick(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acRejWorderExecute(Sender: TObject);
    procedure acTotalExecute(Sender: TObject);
    procedure acTotalUpdate(Sender: TObject);
  protected
    procedure DepClick(Sender : TObject); override;
    procedure Access;
    procedure SetReadOnly(ReadOnly : boolean); override;
  end;

var
  fmWOrderList: TfmWOrderList;

implementation

uses DictData, MainData, fmUtils, dbUtil, WOrder, DocAncestor, Period,
     AList, ApplData, Variants, WOrderRej, WOrderTotal, RxStrUtils,
  ProductionConsts;

{$R *.DFM}

{ TfmWOrderList }

procedure TfmWOrderList.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  Access;
  inherited;
end;

procedure TfmWOrderList.FormCreate(Sender: TObject);
var
  i, j : integer;
  Item : TMenuItem;
  FindDep : boolean;
begin
  Self.HelpFile := Application.HelpFile;

  j := 0;
  FindDep := False;
  FCurrDep := dm.User.wWhId;
  for i:=0 to Pred(dm.CountDep) do
    if (dm.DepInfo[i].DepTypes and $2)=$2 then
    begin
      if (FCurrDep = dm.DepInfo[i].DepId) then
        FindDep := True;
      Item := TMenuItem.Create(ppDep);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepClick;
      Item.RadioItem := True;
     // if Item.Tag = dm.DepDef then DepClick(Item);
      inc(j);
      if j>30 then
      begin
        Item.Break := mbBreak;
        j:=0;
      end;
      ppDep.Items.Add(Item);
    end;
  chbxClose.OnClick := nil;
  if dm.ShowKindDoc = -1 then chbxClose.Checked := False
  else chbxClose.Checked := True;
  chbxClose.OnClick := chbxCloseClick;
  if FindDep then
  begin
    dm.CurrDep := FCurrDep;
    laDep.Caption := dm.DepInfoById[FCurrDep].Name;
  end
  else begin
    dm.CurrDep := -1;
    laDep.Caption := mnitAllWh.Caption;
  end;

  stbrStatus.Panels[0].Text := '������������: '+dm.User.FIO;
  stbrStatus.Panels[1].Text := dm.User.wWhName;
  inherited;
  Access;
  if dm.User.wWhName <> '' then
  begin
    Self.Caption := Self.Caption + ' - ' + dm.User.wWhName;
  end;
end;

procedure TfmWOrderList.chbxCloseClick(Sender: TObject);
begin
  if chbxClose.Checked then dm.ShowKindDoc := 0 else dm.ShowKindDoc := -1;
  ReOpenDataSets([dmMain.taWOrderList]);
  //if not (dmMain.taWOrderList.State in [dsInsert, dsEdit]) then dmMain.taWOrderList.Edit;
end;

procedure TfmWOrderList.Access;
var
  u : Variant;
begin

 { with stbrStatus.Panels[0] do
    if dm.IsAdm then Text := 'Adm'
    else if((dm.User.AccProd_d and 1) = 1)then Text := 'View'
    else Text := 'Full';
  if dm.IsAdm then eXit;
  if dm.User.Profile <> 1 then // ��������
    SetReadOnly(True)
  else begin
    u := ExecSelectSQL('select MOLID from D_PS where DEPID='+
      IntToStr(dm.CurrDep)+' and MOLID='+IntToStr(dm.User.UserId), dmMain.quTmp);
    SetReadOnly(VarIsNull(u)or(u=0));
  end;}

end;

procedure TfmWOrderList.SetReadOnly(ReadOnly: boolean);
begin
{
  acAddDoc.Enabled := not ReadOnly;
  acDelDoc.Enabled := not ReadOnly;
  gridDocList.ReadOnly := ReadOnly;
                                    }
end;

procedure TfmWOrderList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if(dmMain.taWOrderListIsClose.AsInteger = 0)then
  begin
    Background:=clWindow;
    if(Column.FieldName = 'DEFOPERNAME')and
      (dmMain.taWOrderListDEFOPER.AsString = dm.RejOperId)and
      (dmMain.taWOrderListISCLOSE.AsInteger = 0)
    then Background := clMoneyGreen;
  end
  else Background:=clBtnFace;
end;


procedure TfmWOrderList.acRejWorderExecute(Sender: TObject);
var
  TransfromInfo : TTransformInfo;
begin
  // ��������� ��������� �� ������������ �� ��������
  if(dm.User.wWhId = -1)then raise Exception.Create(Format(rsIsNotMOL, [dm.User.FIO]));
  if(dm.User.wWhId <> dm.CurrDep)then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '��� �������� ������ �� ����� � '+laDep.Caption]));
  if(dm.User.Transform_DepId = -2)then raise Exception.Create(Format(rsSelectPS, ['�������������']));
  acAddDoc.Execute;
  dmMain.WorderMode := wmRej;
  if(not(dmMain.taWOrderList.State in [dsInsert, dsEdit]))then dmMain.taWOrderList.Edit;
  dmMain.taWOrderListDEFOPER.AsString := dm.RejOperId;      // �������� �����
  dmMain.taWOrderListJOBDEPID.AsInteger := dm.User.Transform_DepId;
  dmMain.taWOrderListJOBID.AsInteger := dm.User.Transform_MolId;
  with dmMain do
    ShowDocForm(TfmWOrderRej, taWOrderList, taWOrderListWORDERID.AsInteger, quTmp);
  RefreshDataSet(dmMain.taWOrderList);
end;

procedure TfmWOrderList.acAddDocExecute(Sender: TObject);
begin
  inherited;
  dmMain.WorderMode := wmWorder;
  with dmMain do
    ShowDocForm(TfmWOrder, taWOrderList, taWOrderListWORDERID.AsInteger, quTmp);
  RefreshDataSets([dmMain.taWOrderList]);
end;

procedure TfmWOrderList.acEditDocExecute(Sender: TObject);
begin
  with dmMain do
  begin
    if(taWOrderListDEFOPER.AsString = dm.RejOperId)then
    begin
      dmMain.WorderMode := wmRej;
      ShowDocForm(TfmWOrderRej, taWOrderList, taWOrderListWORDERID.AsInteger, quTmp);
    end
    else begin
      dmMain.WorderMode := wmWorder;
      ShowDocForm(TfmWOrder, taWOrderList, taWOrderListWORDERID.AsInteger, quTmp);
    end
  end;
  RefreshDataSets([dmMain.taWOrderList]);
end;

procedure TfmWOrderList.acTotalExecute(Sender: TObject);
begin
  dmMain.PsId_WorderTotal := dmMain.taWOrderListJOBDEPID.AsInteger;
  dmMain.PsName_WOrderTotal := DelRSpace(dmMain.taWOrderListJOBDEPNAME.AsString);
  dmMain.OperId_WOrderTotal := DelRSpace(dmMain.taWOrderListDEFOPER.AsString);
  dmMain.OperName_WOrderTotal := dmMain.taWOrderListDEFOPERNAME.AsString;
  dmMain.BD_WOrderTotal := dm.BeginDate;
  dmMain.ED_WOrderTotal := dm.EndDate;
  ShowAndFreeForm(TfmWorderTotal, TForm(fmWorderTotal));
end;

procedure TfmWOrderList.acTotalUpdate(Sender: TObject);
begin
  acTotal.Enabled := DataSet.Active and (not DataSet.IsEmpty);
  if acTotal.Enabled then
    acTotal.Caption := '����� �� '+DelRSpace(dmMain.taWOrderListJOBDEPNAME.AsString)+' '+DelRSpace(dmMain.taWOrderListDEFOPERNAME.AsString)
  else acTotal.Caption := '�����';
  acTotal.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

end.
