unit SIList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Printers, Controls, Forms,
  Dialogs, ListAncestor, Menus,  ImgList,  StdCtrls, RXCtrls,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, ComCtrls, db,
  RxMenus, DBGridEh, ActnList, TB2Item, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar, Mask, DBCtrlsEh, pFIBDataSet, FIBQuery,
  FIBDataSet, pFIBQuery, pFIBDataBase, FR_Class, StrUtils, PrnDbgeh, DBLookupEh;

type
  TfmSIList = class(TfmListAncestor)
    ppPrint: TTBPopupMenu;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    acPrintStone: TAction;
    acPrintMetal: TAction;
    acCreateSOInv: TAction;
    TBItem7: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    acPrintActSemisGet: TAction;
    TBItem8: TTBItem;
    acImportDoc: TAction;
    TBItem9: TTBItem;
    acPrintRequirementInvoice: TAction;
    cbComp: TDBComboBoxEh;
    lbComp: TLabel;
    TBItem10: TTBItem;
    TBItem11: TTBItem;
    TBItem12: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    TBItem13: TTBItem;
    TBSeparatorItem4: TTBSeparatorItem;
    TBSeparatorItem5: TTBSeparatorItem;
    TBItem14: TTBItem;
    acAddToExistingInvSOEL: TAction;
    acUnionToOneSOElInv: TAction;
    PrintSIListGrid: TPrintDBGridEh;
    Label1: TLabel;
    cbContract: TDBLookupComboboxEh;
    TBItem15: TTBItem;
    TBItem16: TTBItem;
    acPrintActUnknownCrude: TAction;
    acPrintActGoldForReadyProduct: TAction;
    acPrintActMutualClaims: TAction;
    TBItem17: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure acPrintStoneExecute(Sender: TObject);
    procedure acPrintMetalExecute(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acCreateSOInvExecute(Sender: TObject);
    procedure acCreateSOInvUpdate(Sender: TObject);
    procedure acPrintActSemisGetExecute(Sender: TObject);
    procedure acPrintActSemisGetUpdate(Sender: TObject);
    procedure acPrintStoneUpdate(Sender: TObject);
    procedure acPrintMetalUpdate(Sender: TObject);
    procedure acImportDocExecute(Sender: TObject);
    procedure acPrintRequirementInvoiceExecute(Sender: TObject);
    procedure acPrintRequirementInvoiceUpdate(Sender: TObject);
    procedure cbCompCloseUp(Sender: TObject; Accept: Boolean);
    procedure TBItem11Click(Sender: TObject);
    procedure TBItem13Click(Sender: TObject);
    procedure TBItem14Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbContractCloseUp(Sender: TObject; Accept: Boolean);
    procedure acPrintActGoldForReadyProductExecute(Sender: TObject);
    procedure acPrintActUnknownCrudeExecute(Sender: TObject);
    procedure acPrintActMutualClaimsExecute(Sender: TObject);
  protected
    procedure DepClick(Sender : TObject); override;
  end;

var
  fmSIList: TfmSIList;

implementation

uses MainData, dbUtil, DictData, SIEl, fmUtils, PrintData, InvData, INVCopy, PrintData2, MsgDialog;

{$R *.dfm}

procedure TfmSIList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if dmMain.taSIList.State in [dsEdit] then  dmMAin.taSIList.Post;
end;


procedure TfmSIList.FormCreate(Sender: TObject);
var
  i, j : integer;
  Item : TMenuItem;
  FindDep : boolean;
begin
  cbComp.OnCloseUp := nil;
  cbComp.Items.Clear;
  cbComp.Items.Assign(dm.dComp);
  cbComp.ItemIndex := 0;
  cbComp.OnCloseUp := cbCompCloseUp;
  dmMain.FSupname := ROOT_KNAME_CODE;

  if not dmMain.DataSetFilterContract.Active then
  begin
    dmMain.DataSetFilterContract.Active := true;
    dmMain.DataSetFilterContract.First;
  end;

  cbContract.KeyValue := -1;

  stbrStatus.panels[1].Text := dm.User.wWhName;

  FBD := dm.whBeginDate;
  FED := dm.whEndDate;
  FCurrDep := -1;//dm.User.wWhId;
  j := 0;
  FindDep := False;
  for i:=0 to Pred(dm.CountDep) do
  begin
    if  ((dm.DepInfo[i].DepTypes and $2) = $2) then
    begin
      if (FCurrDep = dm.DepInfo[i].DepId)then FindDep := True;
      Item := TMenuItem.Create(ppDep);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepClick;
      Item.RadioItem := True;
      inc(j);
      if j > 30 then
      begin
        Item.Break := mbBreak;
        j := 0;
      end;
      ppDep.Items.Add(Item);
    end;
  end;
  if FindDep then
  begin
    laDep.Caption := dm.DepInfoById[FCurrDep].Name;
  end
  else begin
    FCurrDep := -1;
    laDep.Caption := mnitAllWh.Caption;
  end;

  dm.CurrDep := FCurrDep;

  inherited;
  
  if dm.User.wWhName <> '' then
  begin
    Self.Caption := Self.Caption + ' - ' + dm.User.wWhName;
  end;

end;

procedure TfmSIList.DepClick(Sender: TObject);
begin
  acAddDoc.Tag := TMenuItem(Sender).Tag;
  dm.CurrDep := TMenuItem(Sender).Tag;
  inherited;
end;

procedure TfmSIList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if AnsiUpperCase(Column.FieldName) = 'RECNO' then Background := clMoneyGreen
  else if DataSet.FieldByName('ISCLOSE').AsInteger = 1 then Background := clCloseDoc
  else Background := clOpenDoc;
  if(Column.FieldName = 'DOCNO')then
    if not dmMain.taSIListCOLOR.IsNull then Background := dmMain.taSIListCOLOR.AsInteger;
end;


procedure TfmSIList.TBItem11Click(Sender: TObject);
var Period: string;
begin
  inherited;
  Period := laPeriod.Caption;
  Delete(Period, 1, 2);
  if dmMain.taSIList.IsEmpty then
      ShowMessage('� ������� ������� ��� ��������� ��� ������!')
  else
    begin
      GridDocList.Columns[3].Visible := false;
      GridDocList.Columns[4].Visible := false;
      GridDocList.Columns[5].Visible := false;
      GridDocList.Columns[7].Visible := false;
      GridDocList.Columns[8].Visible := false;
      PrintSIListGrid.BeforeGridText.text := '������ ��������� ��������� � ' + UpperCase(Period) + ' (' + dmMain.taSIListSELFCOMPNAME.AsString + ')';
      Printer.Orientation := poLandscape;
      PrintSIListGrid.Print;
      GridDocList.Columns[3].Visible := true;
      GridDocList.Columns[4].Visible := true;
      GridDocList.Columns[5].Visible := true;
      GridDocList.Columns[7].Visible := true;
      GridDocList.Columns[8].Visible := true;
    end;
end;

procedure TfmSIList.TBItem13Click(Sender: TObject);
begin
  inherited;
  dmPrint.taSIListToPrint.open;
  dmPrint.PrintDocumentA(dkSIEl_M15, VarArrayOf([VarArrayOf([dmMain.taSIListINVID.asinteger]),VarArrayOf([dmMain.taSIListINVID.asinteger])]));
end;


procedure TfmSIList.TBItem14Click(Sender: TObject);
var Invs : string;
    i : integer;
begin
inherited;
  if (gridDocList.SelectedRows.Count > 1) then
    begin
      gridDocList.DataSource.dataSet.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[0]));
      Invs := gridDocList.DataSource.dataSet.FieldByName('INVID').AsString;
      dmPrint2.InvNumbers := gridDocList.DataSource.dataSet.FieldByName('DOCNO').AsString;
      for i := 1 to gridDocList.SelectedRows.Count - 1 do
        begin
          gridDocList.DataSource.dataSet.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[i]));
          Invs := Invs + ', ' + gridDocList.DataSource.dataSet.FieldByName('INVID').AsString;
          dmPrint2.InvNumbers := dmPrint2.InvNumbers + ', ' + gridDocList.DataSource.dataSet.FieldByName('DOCNO').AsString;
        end;
    end
  else
    begin
      Invs := gridDocList.DataSource.dataSet.FieldByName('INVID').AsString;
      dmPrint2.InvNumbers := gridDocList.DataSource.dataSet.FieldByName('DOCNO').AsString;
    end;
dmPrint2.SListToPrint(Invs);
end;

procedure TfmSIList.acAddDocExecute(Sender: TObject);
begin

  if dm.CurrDep = -1 then
  begin

    if Sender is TAction then
    begin

      dm.CurrDep := (Sender as TAction).Tag;

    end;

  end;

  inherited;
  with dmMain do
  begin
    PostDataSet(taSIList);
    ShowAndFreeForm(TfmSIEl, TForm(fmSIEl));
    PostDataSet(taSIList);
  end;
end;

procedure TfmSIList.acEditDocExecute(Sender: TObject);
begin
  inherited;
  with dmMain do
  begin
    taSIList.Edit;
    ShowAndFreeForm(TfmSIEl, TForm(fmSIEl));
    taSIList.Transaction.CommitRetaining;
  end;
end;

procedure TfmSIList.acPrintDocExecute(Sender: TObject);
begin
  //
end;

procedure TfmSIList.acPrintStoneExecute(Sender: TObject);
begin
  dmPrint.taSIListToPrint.open;
  dmPrint.PrintDocumentA(dkSIList_STONE, VarArrayOf([VarArrayOf([dmMain.taSIListINVID.asinteger]),VarArrayOf([dmMain.taSIListINVID.asinteger])]));
end;

procedure TfmSIList.acPrintMetalExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkSIList_Metall, VarArrayOf([VarArrayOf([dmMain.taSIListINVID.asinteger]),VarArrayOf([dmMain.taSIListINVID.asinteger])]));
end;

procedure TfmSIList.acCreateSOInvExecute(Sender: TObject);
var
  c : TCopyInvInfo;
begin
  c.DefaultComp := -1;
  c.CanChangeComp := True;
  c.DefaultDep := dmMain.taSIListDEPID.AsInteger;
  c.CanChangeDep := False;
  c.DepComp := False;
  c.BetweenOrg := False;
  c.CopyInvId := dmMain.taSIListINVID.AsInteger;
  c.CopyTypeFrom := 13;
  c.CopyType := 15;
  c.FromDep := -1;
  if dmMain.taSIListNDSID.IsNull then c.NDSId := -1
  else c.NDSId := dmMain.taSIListNDSID.AsInteger;
  ShowInvCopy(c);
end;

procedure TfmSIList.acCreateSOInvUpdate(Sender: TObject);
begin
  acCreateSOInv.Enabled := dmMain.taSIList.Active and (not dmMain.taSIList.IsEmpty) and
    (dmMain.taSIListISCLOSE.AsInteger=1);
end;

procedure TfmSIList.acPrintActGoldForReadyProductExecute(Sender: TObject);
var
  InvoiceID: Integer;
begin

  InvoiceID := dmMain.taSIListINVID.AsInteger;

  dmPrint.PrintDocumentA(dkActGoldForReadyProduct, VarArrayOf([VarArrayOf([InvoiceID]),VarArrayOf([InvoiceID]), VarArrayOf([InvoiceID])]));

end;

procedure TfmSIList.acPrintActMutualClaimsExecute(Sender: TObject);
var
  InvoiceID: Integer;
begin

  InvoiceID := dmMain.taSIListINVID.AsInteger;

  dmPrint.PrintDocumentA(dkActMutualClaims, VarArrayOf([VarArrayOf([InvoiceID]),VarArrayOf([InvoiceID])]));

end;

procedure TfmSIList.acPrintActSemisGetExecute(Sender: TObject);
var
  InvoiceID: Integer;
begin

  InvoiceID := dmMain.taSIListINVID.AsInteger;

  dmPrint.PrintDocumentA(dkSIList_ActSemisGet, VarArrayOf([VarArrayOf([InvoiceID]),VarArrayOf([InvoiceID])]));

end;

procedure TfmSIList.acPrintActSemisGetUpdate(Sender: TObject);
begin
  acPrintActSemisGet.Enabled := DataSet.Active and (not DataSet.IsEmpty) and
    (DataSet.FieldByName('SCHEMAID').AsInteger = 1);
end;

procedure TfmSIList.acPrintActUnknownCrudeExecute(Sender: TObject);
var
  InvoiceID: Integer;
begin

  InvoiceID := dmMain.taSIListINVID.AsInteger;

  dmPrint.PrintDocumentA(dkActUnknownCrude, VarArrayOf([VarArrayOf([InvoiceID]), VarArrayOf([InvoiceID])]));

end;

procedure TfmSIList.acPrintStoneUpdate(Sender: TObject);
begin
  acPrintStone.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmSIList.cbCompCloseUp(Sender: TObject; Accept: Boolean);
begin
  if cbComp.ItemIndex>0 then dmMain.FSupname:=cbComp.Value
  else dmMain.FSupname:=ROOT_KNAME_CODE;
  PostDataSet(dmMain.taSIList);
  ReOpenDataSet(dmMain.taSIList);
end;

procedure TfmSIList.cbContractCloseUp(Sender: TObject; Accept: Boolean);
var
  ComboBox: TDBLookupComboBoxEh;
  DataSet: TpFIBDataSet;
begin
  inherited;

  if Sender is TDBLookupComboBoxEh then
  begin
    ComboBox := TDBLookupComboBoxEh(Sender);

    DataSet := TpFIBDataSet(GridDocList.DataSource.DataSet);

    dmMain.ContractFilter(DataSet, ComboBox.KeyField, ComboBox.KeyValue);
  end;

end;

procedure TfmSIList.acPrintMetalUpdate(Sender: TObject);
begin
  acPrintMetal.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmSIList.acImportDocExecute(Sender: TObject);
begin
  //
end;

procedure TfmSIList.acPrintRequirementInvoiceExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkSIList_RequirementInvoice, VarArrayOf([VarArrayOf([dmMain.taSIListINVID.asinteger]),VarArrayOf([dmMain.taSIListINVID.asinteger])]));
end;

procedure TfmSIList.acPrintRequirementInvoiceUpdate(Sender: TObject);
begin
  acPrintStone.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

end.
