inherited fmCleannes: TfmCleannes
  Caption = ''
  PixelsPerInch = 96
  TextHeight = 13
  inherited plDict: TPanel
    inherited gridDict: TDBGridEh
      DataSource = dm.dsrCleannes
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Width = 32
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ID'
          Footers = <>
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NAME'
          Footers = <>
          Width = 174
        end>
    end
  end
  inherited ilButtons: TImageList
    Top = 196
  end
  inherited fmstr: TFormStorage
    Left = 120
    Top = 148
  end
end
