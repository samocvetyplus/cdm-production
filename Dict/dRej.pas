unit dRej;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList,  ImgList,  Grids,
  DBGridEh, Buttons, ExtCtrls, ComCtrls, Db, DBGridEhGrouping, rxPlacemnt,
  rxSpeedbar, GridsEh;

type
  TfmdRej = class(TfmDictAncestor)
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FSaveOnFilterRecord : TFilterRecordEvent;
    procedure FilterDepart(DataSet : TDataSet; var Accept : boolean);
  end;

implementation

uses DictData, dbUtil;

{$R *.dfm}

procedure TfmdRej.FormCreate(Sender: TObject);
begin
  CloseDataSet(dm.taDepart);
  FSaveOnFilterRecord := dm.taDepart.OnFilterRecord;
  dm.taDepart.Filtered := True;
  dm.taDepart.OnFilterRecord := FilterDepart;
  OpenDataSet(dm.taDepart);
  inherited;
end;

procedure TfmdRej.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.taDepart.Filtered := False;
  dm.taDepart.OnFilterRecord := FSaveOnFilterRecord;
  inherited;
  CloseDataSet(dm.taDepart);
end;

procedure TfmdRej.FilterDepart(DataSet: TDataSet; var Accept: boolean);
begin
  Accept := (dm.taDepartDEPTYPES.AsInteger and $1 = $1);
end;

end.
