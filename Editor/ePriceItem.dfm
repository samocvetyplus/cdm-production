object fmPriceItem: TfmPriceItem
  Left = 445
  Top = 292
  BorderStyle = bsDialog
  Caption = #1053#1086#1074#1072#1103' '#1089#1090#1088#1086#1082#1072
  ClientHeight = 124
  ClientWidth = 284
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 281
    Height = 97
    Shape = bsFrame
  end
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 41
    Height = 13
    Caption = #1040#1088#1090#1080#1082#1091#1083
  end
  object Label2: TLabel
    Left = 8
    Top = 32
    Width = 47
    Height = 13
    Caption = #1040#1088#1090#1080#1082#1091#1083'2'
  end
  object Label3: TLabel
    Left = 8
    Top = 56
    Width = 63
    Height = 13
    Caption = #1057#1090#1072#1088#1072#1103' '#1094#1077#1085#1072
  end
  object Label4: TLabel
    Left = 8
    Top = 80
    Width = 59
    Height = 13
    Caption = #1053#1086#1074#1072#1103' '#1094#1077#1085#1072
  end
  object Button1: TButton
    Left = 48
    Top = 98
    Width = 105
    Height = 25
    Caption = 'Ok'
    ModalResult = 1
    TabOrder = 0
  end
  object Button2: TButton
    Left = 152
    Top = 98
    Width = 105
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 1
  end
  object Edit1: TEdit
    Left = 64
    Top = 6
    Width = 209
    Height = 21
    TabOrder = 2
  end
  object Edit2: TEdit
    Left = 64
    Top = 29
    Width = 209
    Height = 21
    TabOrder = 3
  end
  object RxCalcEdit1: TRxCalcEdit
    Left = 152
    Top = 50
    Width = 121
    Height = 21
    AutoSize = False
    NumGlyphs = 2
    TabOrder = 4
  end
  object RxCalcEdit2: TRxCalcEdit
    Left = 152
    Top = 72
    Width = 121
    Height = 21
    AutoSize = False
    NumGlyphs = 2
    TabOrder = 5
  end
end
