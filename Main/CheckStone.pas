unit CheckStone;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList,  ImgList,  ExtCtrls,
  ComCtrls, TB2Dock, TB2Toolbar, TB2Item, StdCtrls, Mask, DBCtrlsEh, Grids,
  DBGridEh, DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmCheckStone = class(TfmAncestor)
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    TBControlItem2: TTBControlItem;
    cmbxMat: TDBComboBoxEh;
    gridCheckStone: TDBGridEh;
    spitCreate: TSpeedItem;
    acCreate: TAction;
    procedure FormCreate(Sender: TObject);
    procedure cmbxMatChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCreateExecute(Sender: TObject);
    procedure gridCheckStoneGetCellParams(Sender: TObject;
      Column: TColumnEh; AFont: TFont; var Background: TColor;
      State: TGridDrawState);
  end;

var
  fmCheckStone: TfmCheckStone;

implementation

uses MainData, dbUtil, DictData, dbTree, DateUtils, Period;

{$R *.dfm}

procedure TfmCheckStone.FormCreate(Sender: TObject);
var
  SaveEvent : TNotifyEvent;
begin
  SaveEvent := cmbxMat.OnChange;
  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMatIns);
  cmbxMat.ItemIndex := 0;
  dmMain.FilterMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := SaveEvent;
  OpenDataSet(dmMain.taCheckStone);
end;

procedure TfmCheckStone.cmbxMatChange(Sender: TObject);
begin
  dmMain.FilterMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taCheckStone);
end;

procedure TfmCheckStone.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmMain.taCheckStone);
end;

procedure TfmCheckStone.acCreateExecute(Sender: TObject);
var
  bd : TDateTime;
  ed : TDateTime;
begin
  bd := StartOfTheMonth(StartOfTheDay(ToDay));
  ed := EndOfTheMonth(EndOfTheDay(ToDay));
  if not ShowPeriodForm(bd, ed) then eXit;
  Application.ProcessMessages;
  Screen.Cursor := crSQLWait;
  try
    ExecSQL('execute procedure Create_Stone_Report('+IntToStr(dm.User.UserId)+', "'+
      DateTimeToStr(bd)+'", "'+DateTimeToStr(ed)+'")', dmMain.quTmp);
    ReOpenDataSet(dmMain.taCheckStone);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmCheckStone.gridCheckStoneGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  with dmMain do
  begin
    if(Column.FieldName = 'RQ_1')and(taCheckStoneRQ_1.AsInteger <> taCheckStoneRQ_2.AsInteger)
    then Background := clRed
    else if(Column.FieldName = 'RW_1')and(Abs(taCheckStoneRW_1.AsFloat - taCheckStoneRW_2.AsFloat)>Eps)
    then Background := clRed
  end;
end;

end.
