unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, PrintData, InvData, Grids, DBGrids, DB, StdCtrls, PrintData2;

type
  TTempForm = class(TForm)
    Grid: TDBGrid;
    ds: TDataSource;
    Label1: TLabel;
    Grid2: TDBGrid;
    ds2: TDataSource;
    label2: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  TempForm: TTempForm;

implementation

{$R *.dfm}

procedure TTempForm.FormCreate(Sender: TObject);
begin
  try
    grid.DataSource := ds;
    grid.DataSource.DataSet := dmInv.taSListCalcPrihod;
    grid2.DataSource := ds2;
    grid2.DataSource.DataSet := dmPrint2.taPT_Ext;
  except
    on e:Exception do
      ShowMessage('���-�� ���������: ' + e.Message);
  end;
end;

end.
