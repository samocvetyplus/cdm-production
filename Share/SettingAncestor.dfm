object fmAncestorSetting: TfmAncestorSetting
  Left = 77
  Top = 74
  Width = 613
  Height = 442
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object plSetup: TPanel
    Left = 0
    Top = 0
    Width = 605
    Height = 408
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object splt: TSplitter
      Left = 169
      Top = 0
      Height = 389
    end
    object lbCase: TLabel
      Left = 172
      Top = 0
      Width = 433
      Height = 389
      Align = alClient
      Alignment = taCenter
      Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1086#1076#1080#1085' '#1080#1079' '#1087#1091#1085#1082#1090#1086#1074
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
    end
    object lstvSetting: TListView
      Left = 0
      Top = 0
      Width = 169
      Height = 389
      Align = alLeft
      Columns = <>
      Constraints.MaxWidth = 250
      Constraints.MinWidth = 150
      TabOrder = 0
      ViewStyle = vsList
      OnClick = lstvSettingClick
    end
    object stbrSetting: TStatusBar
      Left = 0
      Top = 389
      Width = 605
      Height = 19
      Panels = <>
    end
  end
  object fmstr: TFormStorage
    UseRegistry = False
    StoredValues = <>
    Left = 313
    Top = 123
  end
  object ilSetting: TImageList
    Left = 364
    Top = 124
  end
end
