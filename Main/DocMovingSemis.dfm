inherited fmDocMoving: TfmDocMoving
  Left = 225
  Top = 192
  Width = 559
  Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1084
  FormStyle = fsStayOnTop
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 551
  end
  inherited tb1: TSpeedBar
    Width = 551
  end
  object dbgList: TDBGridEh [2]
    Left = 0
    Top = 42
    Width = 551
    Height = 200
    Align = alClient
    AllowedOperations = []
    AllowedSelections = []
    BorderStyle = bsNone
    Ctl3D = False
    DataGrouping.GroupLevels = <>
    DataSource = dsrDocMoving
    EditActions = [geaCopyEh, geaSelectAllEh]
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FrozenCols = 2
    Options = [dgTitles, dgIndicator, dgColumnResize, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    OptionsEh = [dghFixed3D, dghFrozen3D, dghIncSearch, dghPreferIncSearch, dghDblClickOptimizeColWidth, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentCtl3D = False
    ParentShowHint = False
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    ShowHint = True
    SumList.Active = True
    SumList.VirtualRecords = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Alignment = taCenter
      end
      item
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Alignment = taCenter
        Width = 60
      end
      item
        EditButtons = <>
        FieldName = 'INVTYPENAME'
        Footers = <>
        Title.Alignment = taCenter
        Width = 73
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Title.Alignment = taCenter
        Width = 59
      end
      item
        EditButtons = <>
        FieldName = 'PRICE'
        Footers = <>
        Title.Alignment = taCenter
      end
      item
        EditButtons = <>
        FieldName = 'COST'
        Footers = <>
        Title.Alignment = taCenter
      end
      item
        EditButtons = <>
        FieldName = 'COMPNAME'
        Footers = <>
        Title.Alignment = taCenter
        Width = 93
      end
      item
        EditButtons = <>
        FieldName = 'NDS'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Width = 102
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object taDocMoving: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.DOCNO, i.DOCDATE, c.NAME COMPNAME, t.NAME INVTYPENAME,'
      '    (it.Cost/it.W) PRICE, it.W, it.Cost, it.NDS, d.NAME DEPNAME '
      'from Inv i left join D_Dep d on (i.DEPID=d.DEPID), '
      '     SIEl it, D_InvType t, D_Comp c'
      'where i.ITYPE=t.ID and'
      '      i.SELFCOMPID=c.COMPID and      '
      '      i.INVID=it.INVID and'
      '      it.SEMIS=:SEMISID and'
      '      i.ITYPE in (13, 15, 16)      '
      'order by i.DocDate'
      '      ')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 20
    Top = 76
    poSQLINT64ToBCD = True
    object taDocMovingDOCNO: TFIBIntegerField
      DisplayLabel = #1053#1086#1084#1077#1088
      FieldName = 'DOCNO'
    end
    object taDocMovingDOCDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm'
    end
    object taDocMovingCOMPNAME: TFIBStringField
      DisplayLabel = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      FieldName = 'COMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taDocMovingINVTYPENAME: TFIBStringField
      DisplayLabel = #1058#1080#1087
      FieldName = 'INVTYPENAME'
      Size = 80
      EmptyStrToNull = True
    end
    object taDocMovingPRICE: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'PRICE'
      currency = True
    end
    object taDocMovingW: TFIBFloatField
      DefaultExpression = '0'
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
    end
    object taDocMovingCOST: TFIBFloatField
      DisplayLabel = #1057#1090#1086#1080#1084#1086#1089#1090#1100
      FieldName = 'COST'
      currency = True
    end
    object taDocMovingNDS: TFIBFloatField
      DisplayLabel = #1053#1044#1057
      FieldName = 'NDS'
      currency = True
    end
    object taDocMovingDEPNAME: TFIBStringField
      DisplayLabel = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      FieldName = 'DEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrDocMoving: TDataSource
    DataSet = taDocMoving
    Left = 20
    Top = 124
  end
end
