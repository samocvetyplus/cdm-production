unit dInvColor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList,  ImgList,  Grids,
  DBGridEh, Buttons, ExtCtrls, ComCtrls, StdCtrls, Mask, RxToolEdit,
  DBGridEhGrouping, rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmdInvColor = class(TfmDictAncestor)
    procedure gridDictGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure gridDictColumns1EditButtonClick(Sender: TObject;
      var Handled: Boolean);
  end;

var
  fmdInvColor: TfmdInvColor;

implementation

uses DictData, DB, dbUtil;

{$R *.dfm}

procedure TfmdInvColor.gridDictGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(Column.FieldName = '')then
  begin
    //AFont.Color := dm.taInvColorCOLOR.AsInteger;
    Background := dm.taInvColorCOLOR.AsInteger;
  end;
end;

procedure TfmdInvColor.FormCreate(Sender: TObject);
begin
  inherited;
  OpenDataSet(dm.taInvType);
end;

procedure TfmdInvColor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  CloseDataSet(dm.taInvType);
end;

procedure TfmdInvColor.gridDictColumns1EditButtonClick(Sender: TObject; var Handled: Boolean);
var
  cd: TColorDialog;
begin
  cd:=TColorDialog.Create(Self);
  cd.Color:=dm.taInvColorCOLOR.AsInteger;
  cd.Options:=[cdFullOpen, cdAnyColor];
  try
    if cd.Execute then
    begin
      if not (dm.taInvColor.State in [dsInsert, dsEdit]) then dm.taInvColor.Edit;
      dm.taInvColorCOLOR.AsInteger := cd.Color;
    end;
  finally
    cd.Free;
  end;
end;

end.
