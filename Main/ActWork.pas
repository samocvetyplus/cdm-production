unit ActWork;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus, TB2Item, ActnList,  ImgList, Grids,
  DBGridEh, DBCtrlsEh, StdCtrls, Mask, DBCtrls, ExtCtrls,
  ComCtrls, Buttons, DBLookupEh, TB2Dock, TB2Toolbar, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmActWork = class(TfmDocAncestor)
    Label1: TLabel;
    txtCompName: TDBText;
    pgAct: TPageControl;
    shMat: TTabSheet;
    shUID: TTabSheet;
    gridUID: TDBGridEh;
    acAdd: TAction;
    acDel: TAction;
    SpeedButton1: TSpeedButton;
    acCalcMatByUID: TAction;
    spitLogic: TSpeedButton;
    acLogic: TAction;
    Label2: TLabel;
    lcbxSubType: TDBLookupComboboxEh;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acPrintActWork: TAction;
    acPrintProdTolling: TAction;
    acChangeNorma: TAction;
    ppDoc: TTBPopupMenu;
    TBItem3: TTBItem;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    lbAddUID: TLabel;
    edUID: TDBEditEh;
    TBControlItem2: TTBControlItem;
    acAddUID: TAction;
    acDelUID: TAction;
    ppUID: TTBPopupMenu;
    TBItem4: TTBItem;
    EditorNorm: TDBEditEh;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acCalcMatByUIDExecute(Sender: TObject);
    procedure acLogicExecute(Sender: TObject);
    procedure acLogicUpdate(Sender: TObject);
    procedure acPrintActWorkExecute(Sender: TObject);
    procedure acPrintActWorkUpdate(Sender: TObject);
    procedure acPrintProdTollingExecute(Sender: TObject);
    procedure acPrintProdTollingUpdate(Sender: TObject);
    procedure spitCloseClick(Sender: TObject);
    procedure acChangeNormaExecute(Sender: TObject);
    procedure acChangeNormaUpdate(Sender: TObject);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acAddUIDExecute(Sender: TObject);
    procedure acDelUIDExecute(Sender: TObject);
    procedure acDelUIDUpdate(Sender: TObject);
    procedure EditorNormChange(Sender: TObject);
  private
    procedure InitBtn(T : boolean);
  end;

var
  fmActWork: TfmActWork;

implementation

uses Data, dbUtil, DictData, MainData, PrintData, InvData, fmUtils,
  ProductionConsts, DB;

{$R *.dfm}

procedure TfmActWork.FormCreate(Sender: TObject);
begin
  FDocName := '��� ����������� �����';
  //ppDoc.Skin := dm.ppSkin;
  EditorNorm.DataSource := dmData.dsrActWorkList;
  edNoDoc.DataSource := dmData.dsrActWorkList;
  dtedDateDoc.DataSource := dmData.dsrActWorkList;
  gridItem.DataSource := dmData.dsrActWork;
  inherited;
  InitBtn(dmData.taActWorkListISCLOSE.AsInteger=1);
  pgAct.Align := alClient;
  gridItem.Parent := shMat;
  OpenDataSets([dmMain.taInvSubType, dmData.taServItem]);
  //ppPrint.Skin := dm.ppSkin;
end;

procedure TfmActWork.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  CloseDataSets([dmMain.taInvSubType, dmData.taServItem]);
end;

procedure TfmActWork.acAddExecute(Sender: TObject);
begin
  DataSet.Append;
  DataSet.Transaction.CommitRetaining;
  ActiveControl := gridItem;
  gridItem.SelectedField := gridItem.FieldColumns['MATNAME'].Field;
end;

procedure TfmActWork.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := DataSet.Active and (dmData.taActWorkListISCLOSE.AsInteger=0);
end;

procedure TfmActWork.acDelExecute(Sender: TObject);
begin
  DataSet.Delete;
end;

procedure TfmActWork.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := DataSet.Active and (not DataSet.IsEmpty) and (dmData.taActWorkListISCLOSE.AsInteger=0);
end;

procedure TfmActWork.acCalcMatByUIDExecute(Sender: TObject);
begin
  if dmData.taActWorkList.State <> dsBrowse then
  begin
    dmData.taActWorkList.Post;
    dmData.taActWorkList.Transaction.CommitRetaining;
  end;
  ExecSQL('execute procedure Calc_ServMatByUID('+dmData.taActWorkListINVID.AsString+')', dm.quTmp);
  ReOpenDataSet(DataSet);
  ReOpenDataSet(dmData.dsrServItem.DataSet);
  pgAct.ActivePage := shMat;
end;

procedure TfmActWork.acLogicExecute(Sender: TObject);
begin
  beep;
end;

procedure TfmActWork.acLogicUpdate(Sender: TObject);
begin
  edNoDoc.Enabled := dmData.taActWorkListISCLOSE.AsInteger=0;
  dtedDateDoc.Enabled := dmData.taActWorkListISCLOSE.AsInteger=0;
  lcbxSubType.Enabled := False; //(dmData.taActWorkListISCLOSE.AsInteger=0);
end;

procedure TfmActWork.InitBtn(T: boolean);
begin
  if not T then // �������� ������
  begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 6;
  end
  else begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 5;
  end
end;

procedure TfmActWork.acPrintActWorkExecute(Sender: TObject);
begin
  PostDataSets([dmData.taActWork, dmData.taServItem, dmData.taActWorkList]);
  dmPrint.PrintDocumentA(dkActWork, VarArrayOf([VarArrayOf([
       dmData.taActWorkListINVID.AsInteger])]));
end;

procedure TfmActWork.acPrintActWorkUpdate(Sender: TObject);
begin
  acPrintActWork.Enabled := dmData.taActWorkList.Active and dmData.taActWork.Active and
    (not dmData.taActWork.IsEmpty);
end;

procedure TfmActWork.acPrintProdTollingExecute(Sender: TObject);
begin
  PostDataSets([dmData.taActWork, dmData.taServItem, dmData.taActWorkList]);
  dmPrint.PrintDocumentA(dkProdTolling, VarArrayOf([VarArrayOf([
        dmData.taActWorkListINVID.AsInteger])]));
end;

procedure TfmActWork.acPrintProdTollingUpdate(Sender: TObject);
begin
  acPrintProdTolling.Enabled := dmData.taActWorkList.Active and dmData.taActWork.Active and
     (not dmData.taActWork.IsEmpty);
end;

procedure TfmActWork.spitCloseClick(Sender: TObject);
begin
  if dmData.taActWorkListREFINVID.AsInteger <> 0 then
  begin
    if(dmData.taActWorkListREFISCLOSE.AsInteger = 0) then
      raise EWarning.Create('�������� '+dmData.taActWorkListREFNAME.AsString+'#'+
        dmData.taActWorkListREFDOCNO.AsString+' �� '+dmData.taActWorkListREFDOCDATE.AsString+' �� ������');
  end;
  inherited;
end;

procedure TfmActWork.acChangeNormaExecute(Sender: TObject);
var
  Bookmark : Pointer;
  r : Variant;
begin
  Bookmark := dmData.taActWork.GetBookmark;
  dmData.taActWork.DisableControls;
  dmData.taActWork.DisableScrollEvents;
  try
    dmData.taActWork.First;
    while not dmData.taActWork.Eof do
    begin
      r := ExecSelectSQL('select N from D_MAT where ID="'+dmData.taActWorkMATID.AsString+'"', dm.quTmp);
      if VarIsNull(r) then r := 0;
      dmData.taActWork.Edit;
      dmData.taActWorkN.AsFloat := r;
      dmData.taActWork.Post;
      dmData.taActWork.Next;
    end;
  finally
    dmData.taActWork.GotoBookmark(Bookmark);
    dmData.taActWork.EnableControls;
    dmData.taActWork.EnableScrollEvents;
  end;
  acCalcMatByUID.Execute;  
end;

procedure TfmActWork.acChangeNormaUpdate(Sender: TObject);
begin
  acChangeNorma.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmActWork.edUIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : acAddUID.Execute;
  end;
end;

procedure TfmActWork.acAddUIDExecute(Sender: TObject);
var
  r, ServItemId : Variant;
  UID : integer;
begin
  if not dmData.taActWorkListREFINVID.IsNull then
    raise Exception.Create('��������� ������� ����� ������ � ���� ����������� �������');
  UID := StrToIntDef(edUID.Text, -1);
  if UID = -1 then raise Exception.Create('�������� ����� �������');
  // ��������� � ����� �� ��������� ������� � ������ ���
  // ����� ���� ������� �����������, ���� ��� ������ ������ ���
  //r := ExecSelectSQL('select OWNERID from GET_UIDOWNER('+IntToStr(UID)+')', dm.quTmp);
  r := ExecSelectSQL('select selfcompid from TMP_PROD_STORE where UID = '+IntToStr(UID)+'', dm.quTmp);
  if VarIsNull(r) then raise Exception.Create('������� � ����� ������� ��� � ���������');
  if r <> dm.User.SelfCompId then
  begin
    r := ExecSelectSQL('select NAME from D_Comp where COMPID='+VarToStr(r), dm.quTmp);
    raise Exception.Create('������� � ������� #'+IntToStr(UID)+' ����������� '+VarToStr(r));
  end;
  // ��������� ������ ������� (�������, ����������)
  r := ExecSelectSQL('select State from SInfo where UID='+IntToStr(UID), dm.quTmp);
  if VarIsNull(r) then raise Exception.Create('����������� ������ � �������')
  else if r<>0 then raise Exception.Create('������� ��� �� ������');
  r := ExecSelectSQL('select first 1 SITEMID from SITEM where UID='+IntToStr(UID)+' order by SITEMID desc', dm.quTmp);
  if VarIsNull(r) then raise Exception.Create('������� � ����� ������� ��� � ���������');
  ServItemId := ExecSelectSQL('select gen_id(gen_servitem_id, 1) from d_rec', dm.quTmp);
  if VarIsNull(ServItemId) then raise EInternal.Create(Format(rsInternalError, ['006']));
  if not VarIsNull(dmData.taServItem.Lookup('Invid;UID', VarArrayOf([dmData.taActWorkListINVID.AsInteger, UID]), 'ID')) then
     raise Exception.Create('������� � ������� #'+IntToStr(UID)+' ��� ���� � ������ ����!');
  ExecSQL('insert into ServItem(ID, INVID, SITEMID) values('+VarToStr(ServItemId)+','+
    dmData.taActWorkListINVID.AsString+','+VarToStr(r)+')', dm.quTmp);
  dmData.taServItem.Insert;
  dmData.taServItemID.AsInteger := ServItemId;
  dmData.taServItem.Post;
  dmData.taServItem.Refresh;
  edUID.SelectAll;
end;

procedure TfmActWork.acDelUIDExecute(Sender: TObject);
begin
  if not dmData.taActWorkListREFINVID.IsNull then
  raise Exception.Create('��������� ������� ����� ������ � ���� ����������� �������');
  dmData.taServItem.Delete;
end;

procedure TfmActWork.acDelUIDUpdate(Sender: TObject);
begin
  acDelUID.Enabled := (dmData.taServItem.Active) and (not dmData.taServItem.IsEmpty);
end;

procedure TfmActWork.EditorNormChange(Sender: TObject);
begin
  Caption := VarToStr(EditorNorm.Value);
end;

end.
