inherited fmSInvProd: TfmSInvProd
  Left = 74
  Top = 79
  Caption = #1055#1086#1089#1090#1072#1074#1082#1072' '#1089' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072' '#1085#1072' '#1089#1082#1083#1072#1076' '#1075#1086#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
  ClientHeight = 705
  ClientWidth = 1092
  OldCreateOrder = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitTop = -151
  ExplicitWidth = 1100
  ExplicitHeight = 739
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter [0]
    Left = 357
    Top = 42
    Width = 2
    Height = 644
  end
  inherited stbrStatus: TStatusBar
    Top = 686
    Width = 1092
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end
      item
        Width = 50
      end
      item
        Width = 50
      end>
    ExplicitTop = 686
    ExplicitWidth = 1092
  end
  inherited tb1: TSpeedBar
    Width = 1092
    BoundLines = [blTop, blLeft, blRight]
    ExplicitWidth = 1092
    object SpeedButton1: TSpeedButton [0]
      Left = 3
      Top = 2
      Width = 1
      Height = 1
      Action = acCheckRule
      Flat = True
    end
    inherited spitExit: TSpeedItem
      Left = 571
    end
  end
  object plWhAppl: TPanel [3]
    Left = 0
    Top = 42
    Width = 357
    Height = 644
    Align = alLeft
    BevelInner = bvLowered
    TabOrder = 2
    object spbrWhAppl: TSpeedBar
      Left = 2
      Top = 2
      Width = 353
      Height = 25
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      BtnOffsetHorz = 3
      BtnOffsetVert = 3
      BtnWidth = 24
      BtnHeight = 23
      BevelOuter = bvNone
      TabOrder = 0
      InternalVer = 1
      object lbOper: TLabel
        Left = 4
        Top = 4
        Width = 54
        Height = 13
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103':'
        Transparent = True
      end
      object lbArt: TLabel
        Left = 203
        Top = 5
        Width = 47
        Height = 13
        Caption = #1040#1088#1090#1080#1082#1091#1083':'
        Transparent = True
      end
      object cmbxOper: TDBComboBoxEh
        Left = 61
        Top = 2
        Width = 135
        Height = 19
        TabStop = False
        DropDownBox.Rows = 15
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        ShowHint = True
        TabOrder = 0
        Text = 'cmbxOper'
        Visible = True
        OnChange = cmbxOperChange
      end
      object edFilterArt: TDBEditEh
        Left = 262
        Top = 2
        Width = 91
        Height = 19
        TabStop = False
        EditButtons = <>
        Flat = True
        ShowHint = True
        TabOrder = 1
        Visible = True
        OnChange = edFilterArtChange
      end
    end
    object gridWhAppl: TDBGridEh
      Left = 2
      Top = 66
      Width = 353
      Height = 576
      Align = alClient
      AllowedOperations = []
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dmAppl.dsrWHAppl
      Flat = True
      FooterColor = clBtnFace
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterRowCount = 1
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      PopupMenu = ppWh
      RowDetailPanel.Color = clBtnFace
      SumList.Active = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      OnDblClick = gridWhApplDblClick
      OnKeyDown = gridWhApplKeyDown
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART'
          Footers = <>
          ReadOnly = True
          Title.EndEllipsis = True
          Width = 46
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART2'
          Footers = <>
          ReadOnly = True
          Title.EndEllipsis = True
          Width = 33
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'Q'
          Footer.FieldName = 'Q'
          Footer.ValueType = fvtSum
          Footers = <>
          ReadOnly = True
          Title.EndEllipsis = True
          Width = 39
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SZ'
          Footers = <>
          ReadOnly = True
          Title.EndEllipsis = True
          Width = 45
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'U'
          Footers = <>
          ReadOnly = True
          Title.EndEllipsis = True
          Width = 45
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERNAME'
          Footers = <>
          ReadOnly = True
          Title.EndEllipsis = True
          Width = 97
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'PATTERN'
          Footers = <>
          ReadOnly = True
          Title.EndEllipsis = True
          Visible = False
          Width = 86
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object TBDock2: TTBDock
      Left = 2
      Top = 27
      Width = 353
      Height = 39
      object tlbrWh: TTBToolbar
        Left = 0
        Top = 0
        BorderStyle = bsNone
        DockPos = -2
        Images = ilButtons
        Options = [tboImageAboveCaption]
        ProcessShortCuts = True
        TabOrder = 0
        object TBItem6: TTBItem
          Action = acArt2Ins
        end
        object TBSubmenuItem1: TTBSubmenuItem
          Caption = #1048#1079#1084#1077#1085#1080#1090#1100
          DropdownCombo = True
          ImageIndex = 8
          object TBItem7: TTBItem
            Action = acChangeArt
          end
          object TBItem8: TTBItem
            Action = acChangeArt2
          end
          object TBItem21: TTBItem
            Action = acChangeSz
          end
        end
      end
    end
  end
  object plSInvProd: TPanel [4]
    Left = 359
    Top = 42
    Width = 733
    Height = 644
    Align = alClient
    BevelInner = bvLowered
    TabOrder = 3
    object plGrid: TPanel
      Left = 80
      Top = 2
      Width = 651
      Height = 640
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object SpeedBar2: TSpeedBar
        Left = 0
        Top = 39
        Width = 651
        Height = 89
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        BtnOffsetHorz = 3
        BtnOffsetVert = 3
        BtnWidth = 24
        BtnHeight = 23
        BevelOuter = bvNone
        TabOrder = 0
        InternalVer = 1
        object lbDocNo: TLabel
          Left = 4
          Top = 6
          Width = 71
          Height = 13
          Caption = #8470'  '#1085#1072#1082#1083#1072#1076#1085#1086#1081
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object lbDocDate: TLabel
          Left = 4
          Top = 26
          Width = 76
          Height = 13
          Caption = #1044#1072#1090#1072' '#1087#1086#1089#1090#1072#1074#1082#1080
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object lbFromDepName: TLabel
          Left = 200
          Top = 7
          Width = 66
          Height = 13
          Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object lbDepName: TLabel
          Left = 203
          Top = 26
          Width = 59
          Height = 13
          Caption = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object txtDepTo: TDBText
          Left = 281
          Top = 27
          Width = 45
          Height = 13
          AutoSize = True
          DataField = 'DEPNAME'
          DataSource = dmMain.dsrASList
          Transparent = True
        end
        object txtDepFrom: TDBText
          Left = 281
          Top = 4
          Width = 97
          Height = 17
          DataField = 'FROMDEPNAME'
          DataSource = dmMain.dsrASList
          Transparent = True
        end
        object lbOperWh: TLabel
          Left = 343
          Top = 25
          Width = 54
          Height = 13
          Caption = #1054#1087#1077#1088#1072#1094#1080#1103':'
          Transparent = True
          Visible = False
        end
        object Label1: TLabel
          Left = 4
          Top = 46
          Width = 70
          Height = 13
          Caption = #1042#1080#1076' '#1076#1086#1075#1086#1074#1086#1088#1072
        end
        object Label4: TLabel
          Left = 6
          Top = 70
          Width = 49
          Height = 13
          Caption = #1054#1087#1080#1089#1072#1085#1080#1077
        end
        object Label7: TLabel
          Left = 203
          Top = 45
          Width = 49
          Height = 13
          Caption = #1059#1087#1072#1082#1086#1074#1072#1083
        end
        object Label8: TLabel
          Left = 203
          Top = 70
          Width = 48
          Height = 13
          Caption = #1054#1090#1075#1088#1091#1079#1080#1083
        end
        object edDocNo: TDBNumberEditEh
          Left = 84
          Top = 2
          Width = 85
          Height = 19
          currency = False
          DataField = 'DOCNO'
          DataSource = dmMain.dsrASList
          EditButton.Style = ebsUpDownEh
          EditButtons = <>
          Flat = True
          MinValue = 1.000000000000000000
          ShowHint = True
          TabOrder = 0
          Visible = True
        end
        object cmbxSOper: TDBComboBoxEh
          Left = 352
          Top = 0
          Width = 33
          Height = 19
          TabStop = False
          DropDownBox.Rows = 15
          DropDownBox.Width = -1
          EditButtons = <>
          Flat = True
          ShowHint = True
          TabOrder = 2
          Visible = False
        end
        object deSDate: TDBDateTimeEditEh
          Left = 84
          Top = 22
          Width = 85
          Height = 19
          DataField = 'DOCDATE'
          DataSource = dmMain.dsrASList
          EditButton.Glyph.Data = {
            36010000424D360100000000000076000000280000001C0000000C0000000100
            040000000000C000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD777777777
            77DDFFFFFFFFFFFF0000DD004044040047D777777777777F0000DDFFFFFFFFFF
            47D7FDFFFDFFFD7F0000DDF000F0008F47D7F777D777DF7F0000DDFF0FF8F80F
            47D7FD7FDDDD7F7F0000DDFF0FFFFF0F47D7FD7FDDFF7D7F0000DDFF0FF0008F
            47D7FD7FD777DD7F0000DDF00FF0FFFF47D7F77FD7FFFF7F0000DDFF0FF0000F
            47D7FD7DD7777D7F0000DDFFFFFFFFFF47D7FDDDDDDDDD7F0000DDF88888888F
            47D7FFFFFFFFFF7F0000DD0000000000DDD777777777777D0000}
          EditButton.NumGlyphs = 2
          EditButton.Style = ebsGlyphEh
          EditButton.Width = 18
          EditButtons = <>
          Flat = True
          Kind = dtkDateEh
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object lcbxContract: TDBLookupComboboxEh
          Left = 86
          Top = 45
          Width = 111
          Height = 19
          DataField = 'CONTRACTID'
          DataSource = dmMain.dsrASList
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          Flat = True
          KeyField = 'ID'
          ListField = 'NAME'
          ListSource = dm.dsrContract
          ShowHint = True
          TabOrder = 3
          Visible = True
          OnUpdateData = lcbxContractUpdateData
        end
        object lcbxInvColor: TDBLookupComboboxEh
          Left = 84
          Top = 68
          Width = 105
          Height = 19
          DataField = 'INVCOLORID'
          DataSource = dmMain.dsrASList
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          DropDownBox.Columns = <
            item
              FieldName = 'NAME'
            end>
          DropDownBox.ListSource = dm.dsrInvColor
          DropDownBox.SpecRow.CellsText = '<'#1085#1077' '#1079#1072#1076#1072#1085#1086'>'
          DropDownBox.SpecRow.Visible = True
          EditButtons = <>
          Flat = True
          KeyField = 'ID'
          ListField = 'NAME'
          ListSource = dm.dsrInvColor
          ShowHint = True
          TabOrder = 4
          Visible = True
        end
        object lcbxJobId: TDBLookupComboboxEh
          Left = 272
          Top = 46
          Width = 129
          Height = 19
          DataField = 'JOBID'
          DataSource = dmMain.dsrASList
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.Width = -1
          EditButtons = <>
          Flat = True
          KeyField = 'MOLID'
          ListField = 'FNAME;LNAME;MNAME'
          ListSource = dm.dsrMOL
          ShowHint = True
          TabOrder = 5
          Visible = True
        end
        object lcbxShipedBy: TDBLookupComboboxEh
          Left = 272
          Top = 68
          Width = 129
          Height = 19
          DataField = 'ShipedBy'
          DataSource = dmMain.dsrASList
          DropDownBox.Width = -1
          EditButtons = <>
          Flat = True
          KeyField = 'MOLID'
          ListField = 'FNAME;LNAME;MNAME'
          ListSource = dm.dsrMOL
          ShowHint = True
          TabOrder = 6
          Visible = True
        end
        object GroupBox1: TGroupBox
          Left = 431
          Top = 6
          Width = 202
          Height = 77
          TabOrder = 7
          object cbForeignItems: TcxCheckBox
            Left = 24
            Top = 12
            Caption = #1044#1088#1091#1075#1086#1081' '#1080#1079#1075#1086#1090#1086#1074#1080#1090#1077#1083#1100
            Properties.OnEditValueChanged = cbForeignItemsPropertiesEditValueChanged
            TabOrder = 0
            Width = 145
          end
          object lcbxProducers: TDBLookupComboBox
            Left = 11
            Top = 39
            Width = 180
            Height = 21
            DataField = 'PRODUCER$ID'
            DataSource = dmMain.dsrASList
            TabOrder = 1
          end
        end
      end
      object TBDock1: TTBDock
        Left = 0
        Top = 0
        Width = 651
        Height = 39
        FixAlign = True
        object tlbrInv: TTBToolbar
          Left = 0
          Top = 0
          Align = alTop
          BorderStyle = bsNone
          Caption = 'tlbrInv'
          DockPos = -4
          Images = ilButtons
          Options = [tboImageAboveCaption]
          TabOrder = 0
          object TBItem1: TTBItem
            Action = acOpen
            DisplayMode = nbdmImageAndText
          end
          object TBItem2: TTBItem
            Action = acPrint
            DisplayMode = nbdmImageAndText
          end
          object TBItem11: TTBItem
            Action = acPrice
            ImageIndex = 14
            Images = ilButtons
          end
          object TBItem24: TTBItem
            Action = acCalcPrice
          end
          object TBItem31: TTBItem
            Action = acCheckInv
          end
          object TBSubmenuItem3: TTBSubmenuItem
            Caption = #1056#1072#1079#1085#1086#1077
            DropdownCombo = True
            ImageIndex = 16
            object TBItem33: TTBItem
              Action = acPhoto
            end
          end
        end
      end
      object pglView: TPageControl
        Left = 0
        Top = 128
        Width = 651
        Height = 512
        ActivePage = tshGrid
        Align = alClient
        TabOrder = 2
        TabPosition = tpBottom
        object tshGrid: TTabSheet
          Caption = #1057#1090#1072#1085#1076#1072#1088#1090#1085#1099#1081' '#1074#1080#1076
          object plPInv: TPanel
            Left = 0
            Top = 185
            Width = 643
            Height = 142
            Align = alBottom
            BevelInner = bvLowered
            TabOrder = 0
            object SpeedBar1: TSpeedBar
              Left = 2
              Top = 2
              Width = 639
              Height = 29
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              BtnOffsetHorz = 3
              BtnOffsetVert = 3
              BtnWidth = 24
              BtnHeight = 23
              BevelOuter = bvNone
              TabOrder = 0
              InternalVer = 1
              object Label5: TLabel
                Left = 4
                Top = 15
                Width = 70
                Height = 13
                Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object txtPDocNo: TDBText
                Left = 73
                Top = 15
                Width = 37
                Height = 13
                DataField = 'DOCNO'
                DataSource = dmAppl.dsrPInv
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object Label6: TLabel
                Left = 107
                Top = 15
                Width = 11
                Height = 13
                Caption = #1086#1090
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object txtPDocDate: TDBText
                Left = 121
                Top = 15
                Width = 65
                Height = 14
                DataField = 'DOCDATE'
                DataSource = dmAppl.dsrPInv
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object Panel1: TPanel
                Left = 0
                Top = 0
                Width = 639
                Height = 14
                Align = alTop
                BevelOuter = bvNone
                Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1085#1099#1077' '#1074' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086' '#1072#1088#1090#1080#1082#1091#1083#1099
                Color = clMoneyGreen
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object gridPEl: TDBGridEh
              Left = 2
              Top = 31
              Width = 639
              Height = 109
              Align = alClient
              AllowedOperations = []
              ColumnDefValues.Title.TitleButton = True
              DataGrouping.GroupLevels = <>
              DataSource = dmAppl.dsrPEl
              Flat = True
              FooterColor = clBtnFace
              FooterFont.Charset = DEFAULT_CHARSET
              FooterFont.Color = clWindowText
              FooterFont.Height = -11
              FooterFont.Name = 'MS Sans Serif'
              FooterFont.Style = []
              FooterRowCount = 1
              FrozenCols = 1
              Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
              OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
              ParentShowHint = False
              PopupMenu = ppPInv
              RowDetailPanel.Color = clBtnFace
              ShowHint = True
              SumList.Active = True
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              UseMultiTitle = True
              OnGetCellParams = gridPElGetCellParams
              Columns = <
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'RecNo'
                  Footer.ValueType = fvtCount
                  Footers = <>
                  Width = 22
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'ART'
                  Footers = <>
                  Width = 68
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'ART2'
                  Footers = <>
                  Width = 32
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'SZ'
                  Footers = <>
                  Width = 49
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'Q'
                  Footer.FieldName = 'Q1'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Width = 43
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'U'
                  Footers = <>
                  Width = 36
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'OPERATION'
                  Footers = <>
                  Width = 62
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'COMMENTID'
                  Footers = <>
                  Title.Caption = #1057#1080#1089#1090'. '#1082#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
                  ToolTips = True
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'COMMENT'
                  Footers = <>
                  Width = 108
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'ART2ID'
                  Footers = <>
                  Visible = False
                end>
              object RowDetailData: TRowDetailPanelControlEh
              end
            end
          end
          object plRej: TPanel
            Left = 0
            Top = 327
            Width = 643
            Height = 159
            Align = alBottom
            BevelInner = bvLowered
            TabOrder = 1
            object spbrRej: TSpeedBar
              Left = 2
              Top = 2
              Width = 639
              Height = 29
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Tahoma'
              Font.Style = []
              BtnOffsetHorz = 3
              BtnOffsetVert = 3
              BtnWidth = 24
              BtnHeight = 23
              BevelOuter = bvNone
              TabOrder = 0
              InternalVer = 1
              object Label2: TLabel
                Left = 4
                Top = 16
                Width = 70
                Height = 13
                Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object txtRejDocNo: TDBText
                Left = 76
                Top = 16
                Width = 25
                Height = 14
                DataField = 'DOCNO'
                DataSource = dmAppl.dsrRejInv
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object Label3: TLabel
                Left = 103
                Top = 16
                Width = 11
                Height = 13
                Caption = #1086#1090
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clNavy
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object txtRejDocDate: TDBText
                Left = 115
                Top = 16
                Width = 65
                Height = 15
                DataField = 'DOCDATE'
                DataSource = dmAppl.dsrRejInv
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                Transparent = True
              end
              object plRejName: TPanel
                Left = 0
                Top = 0
                Width = 639
                Height = 16
                Align = alTop
                BevelOuter = bvNone
                Caption = #1057#1085#1103#1090#1099#1077' '#1089' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072' '#1072#1088#1090#1080#1082#1091#1083#1099
                Color = clMoneyGreen
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
              end
            end
            object gridRejEl: TDBGridEh
              Left = 2
              Top = 31
              Width = 639
              Height = 126
              Align = alClient
              AllowedOperations = []
              ColumnDefValues.Title.TitleButton = True
              DataGrouping.GroupLevels = <>
              DataSource = dmAppl.dsrRejEl
              Flat = True
              FooterColor = clBtnFace
              FooterFont.Charset = DEFAULT_CHARSET
              FooterFont.Color = clWindowText
              FooterFont.Height = -11
              FooterFont.Name = 'MS Sans Serif'
              FooterFont.Style = []
              FooterRowCount = 1
              FrozenCols = 1
              Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
              OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
              ParentShowHint = False
              PopupMenu = ppRejInv
              RowDetailPanel.Color = clBtnFace
              ShowHint = True
              SumList.Active = True
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              UseMultiTitle = True
              OnGetCellParams = gridRejElGetCellParams
              Columns = <
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'RecNo'
                  Footer.ValueType = fvtCount
                  Footers = <>
                  Width = 22
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'ART'
                  Footers = <>
                  Title.EndEllipsis = True
                  Width = 50
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'ART2'
                  Footers = <>
                  Title.EndEllipsis = True
                  Width = 35
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'SZ'
                  Footers = <>
                  Title.EndEllipsis = True
                  Width = 41
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'Q'
                  Footer.FieldName = 'Q1'
                  Footer.ValueType = fvtSum
                  Footers = <>
                  Title.EndEllipsis = True
                  Width = 48
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'U'
                  Footers = <>
                  Title.EndEllipsis = True
                  Width = 32
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'OPERATION'
                  Footers = <>
                  Title.EndEllipsis = True
                  Width = 67
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'COMMENTID'
                  Footers = <>
                  Title.Caption = #1057#1080#1089#1090'. '#1082#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
                  Title.EndEllipsis = True
                  ToolTips = True
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'COMMENT'
                  Footers = <>
                  Title.Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
                  Title.EndEllipsis = True
                  Width = 120
                end
                item
                  DropDownBox.ColumnDefValues.Title.TitleButton = True
                  EditButtons = <>
                  FieldName = 'ART2ID'
                  Footers = <>
                  Visible = False
                end>
              object RowDetailData: TRowDetailPanelControlEh
              end
            end
          end
          object gridItem: TDBGridEh
            Left = 0
            Top = 0
            Width = 643
            Height = 185
            Align = alClient
            AllowedOperations = [alopUpdateEh]
            ColumnDefValues.Title.TitleButton = True
            DataGrouping.GroupLevels = <>
            DataSource = dmMain.dsrASItem
            Flat = True
            FooterColor = clBtnFace
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            FooterRowCount = 1
            FrozenCols = 5
            Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
            OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
            PopupMenu = ppInv
            RowDetailPanel.Color = clBtnFace
            SumList.Active = True
            TabOrder = 2
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            UseMultiTitle = True
            OnGetCellParams = gridItemGetCellParams
            OnKeyDown = gridItemKeyDown
            Columns = <
              item
                Color = clBtnFace
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'SORTIND'
                Footers = <>
                Title.Caption = #8470#1087'.'#1087'.'
                Width = 36
              end
              item
                Color = clBtnFace
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'UID'
                Footer.ValueType = fvtCount
                Footers = <>
                ReadOnly = True
                Title.EndEllipsis = True
                Width = 45
              end
              item
                Color = clBtnFace
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'ART'
                Footers = <>
                ReadOnly = True
                Title.EndEllipsis = True
                Width = 45
              end
              item
                Color = clBtnFace
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'ART2'
                Footers = <>
                ReadOnly = True
                Title.EndEllipsis = True
                Width = 36
              end
              item
                Color = clBtnFace
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'SZNAME'
                Footers = <>
                ReadOnly = True
                Title.EndEllipsis = True
                Width = 39
              end
              item
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'W'
                Footer.FieldName = 'W'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.EndEllipsis = True
                Width = 33
              end
              item
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'PRICE'
                Footers = <>
                Title.EndEllipsis = True
                Width = 51
              end
              item
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'COST'
                Footer.Color = clMoneyGreen
                Footer.FieldName = 'COST'
                Footer.ValueType = fvtSum
                Footers = <>
                Title.EndEllipsis = True
                Width = 56
              end
              item
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'U'
                Footers = <>
                KeyList.Strings = (
                  '0'
                  '1')
                PickList.Strings = (
                  #1064#1090'.'
                  #1055#1072#1088#1072)
                Title.Caption = #1045#1076'. '#1080#1079#1084'.|'#1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
                Title.EndEllipsis = True
                Title.Hint = #1045#1076'. '#1080#1079#1084'. '#1087#1086#1083#1091#1092'-'#1072' '#1072#1088#1090#1080#1082#1091#1083#1072
                Width = 78
              end
              item
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'UA'
                Footers = <>
                Title.Caption = #1045#1076'. '#1080#1079#1084'.|'#1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
                Title.EndEllipsis = True
                Title.Hint = #1045#1076'. '#1080#1079#1084'. '#1072#1088#1090#1080#1082#1091#1083#1072
                Width = 71
              end
              item
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'S_OPERNAME'
                Footers = <>
                Title.Caption = #1054#1087#1077#1088#1072#1094#1080#1103'|'#1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
                Title.EndEllipsis = True
                Title.Hint = 
                  #1054#1087#1077#1088#1072#1094#1080#1103', '#1089' '#1082#1086#1090#1086#1088#1086#1081' '#1089#1085#1080#1084#1072#1077#1090#1089#1103' '#1087#1086#1083#1091#1092'-'#1090' ('#1079#1072#1076#1072#1077#1090#1089#1103' '#1074' '#1089#1087#1088#1072#1074#1086#1095#1085#1080#1082#1077' '#1072#1088 +
                  #1090#1080#1082#1091#1083#1086#1074')'#13#10#1089#1086' '#1089#1082#1083#1072#1076#1072' '#1087#1086#1083#1091#1092'-'#1090#1086#1074
                Width = 83
              end
              item
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'OPERNAME'
                Footers = <>
                Title.Caption = #1054#1087#1077#1088#1072#1094#1080#1103'|'#1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
                Title.EndEllipsis = True
                Title.Hint = #1054#1087#1077#1088#1072#1094#1080#1103', '#1089' '#1082#1086#1090#1086#1088#1086#1081' '#1089#1085#1080#1084#1072#1077#1090#1089#1103' '#1072#1088#1090#1080#1082#1091#1083' '#1089#1086' '#1089#1082#1083#1072#1076#1072' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
                Width = 78
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
        object tshMatrix: TTabSheet
          Caption = #1052#1072#1090#1088#1080#1095#1085#1099#1081' '#1074#1080#1076
          ImageIndex = 1
          object TBDock3: TTBDock
            Left = 0
            Top = 0
            Width = 643
            Height = 26
            object tlbrSheet: TTBToolbar
              Left = 2
              Top = 0
              BorderStyle = bsNone
              DockPos = 2
              TabOrder = 0
            end
          end
          object gridMatrixProd: TDBGridEh
            Left = 0
            Top = 26
            Width = 643
            Height = 460
            Align = alClient
            AllowedOperations = []
            AllowedSelections = [gstRectangle, gstColumns]
            AutoFitColWidths = True
            ColumnDefValues.Title.TitleButton = True
            DataGrouping.GroupLevels = <>
            DataSource = dmMain.dsrMatrixProd
            DrawMemoText = True
            Flat = True
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
            PopupMenu = ppMatrixInv
            RowDetailPanel.Color = clBtnFace
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnGetCellParams = gridMatrixProdGetCellParams
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
      end
    end
    object TBDock4: TTBDock
      Left = 2
      Top = 2
      Width = 78
      Height = 640
      Position = dpLeft
      object tlbrFunc: TTBToolbar
        Left = 0
        Top = 0
        BorderStyle = bsNone
        Caption = 'tlbrFunc'
        DockMode = dmCannotFloat
        Images = ilButtons
        Options = [tboImageAboveCaption]
        TabOrder = 0
        object TBSubmenuItem2: TTBSubmenuItem
          Action = acAdd
          DropdownCombo = True
          Options = [tboDropdownArrow]
          object TBItem18: TTBItem
            Action = acAdd
            Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1082#1086#1085#1077#1094
            Options = [tboDefault]
          end
          object TBItem17: TTBItem
            Action = acAddToFirstFreePlace
          end
        end
        object TBItem16: TTBItem
          Action = acDel
          Caption = '  '#1059#1076#1072#1083#1080#1090#1100'    '
        end
      end
    end
  end
  inherited ilButtons: TImageList
    Left = 126
    Top = 144
    Bitmap = {
      494C010112001300040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000005000000001002000000000000050
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000020A1C9002CAACF001082AC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002BA3
      C90024A5CC000F84AE00149AC30024AED60033B1D500188BB4001787AF0043AB
      CC003DA8CB000000000000000000000000000000000000000000414141006F6E
      6E00414141004141410041414100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000032A5
      C90037B8DC0014AED90011A1CB001DC7F00048D7F80034A6CA005CC1DD0067C4
      DE003FA4C6000000000000000000000000003635350052505000A4A2A100EBEA
      EA0066666600A3A1A100969494006A686900504E4E0041414100414141000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000208CB4002C98BD004EB5
      D50085DBEF0051C0DE0039C8EC001ED7FF003ADBFF005FD4F10075C6DF00B0E1
      EC0090CEE1001B8BB50000000000000000005B575600C6BDBB00E5E1E000F2F1
      F00066666600B9B8B900F8F5F500F0EDEE00D3D0D000918D8D00747271006E6D
      6C004B4B4A004040400041414100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000060BAD70048B0D20053BE
      E00095EDFF008DEFFF005AE5FF0027DAFF001CD8FF0052E2FF0079E8FF007DEB
      FF003FCBEE0031B1D9002CA4CE0000000000726C6A00C5BBB900D0CDCD00F3F1
      F10066666600BEBDBE00F9F9F900DFDEDF009795950071707000797978006E6C
      6C00A3A09E00908D8B006B666400373736000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001391C00022A6D7004EC3
      EA0081DFF70092EFFF0071D3E8005CB8CC0050B2C9002AC7EB0007D1FF0004CF
      FE0009D0FE0008C9F5000EB4E10000000000605C5A00918B8A009E9C9C00F5F4
      F30066666600C5C5C500FEFEFF00928E8E00615F5F00DAD9DB00B4B1B100938E
      8E0069696700B0A8A600B3A5A200393939000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001C8EB80028ADDE003FBB
      E7006DD7F60091C7D3009796960097969600979696009796960045BCD80002D2
      FF0014D3FE001ED2FB000C9ECB0000000000625D5B00B3ACAA00A6A4A300F4F3
      F30066666600C9CBCB00DDDCDE007D797A00A9A9AB00B6A49200CFA77B008A7B
      720078727100AEA4A200B0A39F003B3A3A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001E92BC0027ADDF0031B4
      E3005ACFF40097969600E9E3E200B2B3B300ACA2A200E1B7B5009796960059DC
      FA009CF0FF00B5EFFC003D9BBD0000000000615C5B00B3ABA900A5A3A200F5F3
      F30066666600C5C5C600D4D3D4009492920098989A00C1AE9B00FFF0C800A98D
      7100857D7A009B939100B1A3A0003B3A3A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000148CB8001F9DCD0027AC
      DD0047C5EF0097969600E9E2E000B1B2B200ACA2A200DCB5B400979696007CE4
      FB00ACECF9008ACBE00048A5C40000000000605B5A00B2AAA800A6A4A300F5F3
      F30066666600BDBCBC00DDDCDC008A8989009B98980090888400CAB49A009381
      6F00867E7B00998F8E00AFA19D003B3A3A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000E80AA0020A2
      D30036BCEB0097969600E9E2E000B1B2B200ACA2A200DDB6B4009796960065E1
      FD0068CAE6000579A400000000000000000067616000B2ABA900A19F9E00F9F7
      F70066666600B1AFB000F1EEEE00A3A1A100727272007D7573007F757100887E
      7A007B747200AEA29F00AA9C97003B3A3A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001389
      B4000D8FBE0097969600ECE5E200B1B2B200ACA2A200E0B8B600979696001E9A
      C1002293BA00000000000000000000000000635E5C008C858500CECBCB00E6E5
      E50066666600BDB8B900DEDADA00D5CFD000969392005B5A5A0064605F006A66
      64009A918F00998D8A00A69892003B3A3A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000097969600E5E3E200AEAFAF00ABA1A100D8B6B600979696000000
      00000000000000000000000000000000000065605F00D1C7C600D3CFCF007373
      7300A5A0A000D1CACA00CEC7C700CFC8C800D1C9C800C3BAB800BFB5B400B8AC
      AA00A9A3A200F4F3F400A29D9A00393939000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000097969600B2B2B200A0A0A00099969600ABA0A000979696000000
      0000000000000000000000000000000000003E3D3D007975740061606000605F
      5F00A4A4A400898787009D9A9A00A49F9E009D979700958D8C009B928F009C91
      8E00A29A9700D3D2D300D3D2D300393939000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000097969600E1E0E000C6C7C700A5A3A300B3A5A500979696000000
      0000000000000000000000000000000000000000000000000000000000003939
      3900333333005F5F5F00515252005F5F5F00727272007B797A007F7C7C007E79
      78006B656300534E4D005D575500343434000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000097969600E5E4E400EAE9E900BCBCBC00A39E9E00979696000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000343434003333330064636400403F3F004B49
      4900444242002F30300037363600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000097969600979696009796960097969600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B58C8C008C5A5A008C5A5A008C5A5A008C5A5A008C5A5A008C5A
      5A008C5A5A008C5A5A008C5A5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000B5000000B5000000000000000000000000000000
      0000000000000000000080808000808080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009862
      5100C5897200AF7B6B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B58C8C00FFFFEF00FFF7E700FFF7DE00F7EFDE00F7EFDE00F7EF
      DE00FFEFDE00F7E7D6008C5A5A0000000000000000000000B5000000B5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000B5000000B500000000000000000000000000000000000000
      000000000000FF000000FF000000808080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000D4B6AC00FCB2
      9E00FFDEBC00CE8F7500DA8C7200FBAD8C00B87C6500AB7A6900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B58C8C00FFF7E700F7E7CE00F7E7CE00F7DECE00F7DEC600F7DE
      C600F7E7CE00EFDECE008C5A5A0000000000000000000000B5000000B5000000
      B500000000000000000000000000000000000000000000000000000000000000
      00000000B5000000B50000000000000000000000000000000000000000000000
      0000FF0000000080000000800000FF0000008080800080808000000000000000
      0000000000000000000000000000000000000000000000000000D4B6AC00F2AF
      9C00FFD1B200D5A59200FFBE9E00D59A8300BB766000FCB09000D78E7400D680
      6200E5CBC1000000000000000000000000000000000000000000000000000000
      000000000000B58C8C00FFF7E700FFD6A500FFD6A500FFD6A500FFD6A500FFD6
      A500FFD6A500EFDECE008C5A5A0000000000000000000000B5000000B5000000
      B5000000B5000000000000000000000000000000000000000000000000000000
      B5000000B50000000000000000000000000000000000000000000000000000FF
      000000800000008000000080000000800000FF00000080808000808080000000
      0000000000000000000000000000000000000000000000000000D4B6AC00F7B5
      A200FFD0AF00E8C1AC00E8CDB100D6A28E00FFFFFA00D29C8400EAB5A000E69F
      8100EDCDC10000000000000000000000000000000000B58C8C008C5A5A008C5A
      5A008C5A5A00B58C8C00FFF7EF00FFE7CE00FFE7C600FFDEC600FFDEC600F7DE
      BD00F7E7D600EFDECE009C6B63000000000000000000000000000000D6000000
      BD000000B5000000B500000000000000000000000000000000000000B5000000
      B50000000000000000000000000000000000000000000000000000FF00000080
      00000080000000800000008000000080000000800000FF000000808080008080
      8000000000000000000000000000000000000000000000000000D4B6AC00FAB9
      A800FFDAB900C2846C00C37A6400FFE2BB00A0736000FFD8B700E2C2A900E9BF
      A200EBCABD0000000000000000000000000000000000B58C8C00FFFFEF00FFF7
      E700FFF7DE00B58C8C00FFFFF700FFE7CE00FFE7CE00FFE7C600FFDEC600FFDE
      C600F7E7D600EFE7D6009C6B6B00000000000000000000000000000000000000
      00000000B5000000B5000000B500000000000000B5000000B5000000B5000000
      000000000000000000000000000000000000000000000000000000FF00000080
      0000008000000080000000FF0000008000000080000000800000FF0000008080
      8000808080000000000000000000000000000000000000000000D4B6AC00FBBF
      B000FFD1B200DAB09E00FFDFBF00B8837000EBA38900E0A38700C8867100DE95
      7800ECD0C30000000000000000000000000000000000B58C8C00FFF7E700F7E7
      CE00F7E7CE00B58C8C00FFFFF700FFD6A500FFD6A500FFD6A500FFD6A500FFD6
      A500FFD6A500F7E7DE00A57B7300000000000000000000000000000000000000
      0000000000000000B5000000C6000000C6000000CE000000B500000000000000
      000000000000000000000000000000000000000000000000000000FF00000080
      000000800000FF0000008080800000FF0000008000000080000000800000FF00
      0000808080000000000000000000000000000000000000000000D4B6AC00FCC1
      B100FFD7B500E6C1AA00E3C7AA00F3C0AA00FFFAE100E1B49A00F6C7B000E6B3
      9800E9CABE0000000000000000000000000000000000B58C8C00FFF7E700FFD6
      A500FFD6A500B58C8C00FFFFFF00FFEFDE00FFE7D600FFE7D600FFE7D600FFEF
      D600FFF7E700EFE7DE00A57B7300000000000000000000000000000000000000
      000000000000000000000000C6000000C6000000DE0000000000000000000000
      000000000000000000000000000000000000000000000000000000FF00000080
      000000800000FF000000808080000000000000FF00000080000000800000FF00
      0000808080008080800000000000000000000000000000000000D4B6AC00FFC8
      B900FFDCBC00BC806B00CA826B00FFD4B100A46B5800FFD5B100DBB59600FFD1
      A000F5D9CC0000000000000000000000000000000000B58C8C00FFF7EF00FFE7
      CE00FFE7C600B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFDE
      DE00D6C6C600BDADAD00B5847300000000000000000000000000000000000000
      0000000000000000B5000000D6000000CE000000DE000000EF00000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000800000FF00000000000000000000000000000000FF0000008000000080
      0000FF0000008080800080808000000000000000000000000000D4B6AC00FFCC
      C100FFDABA00D6AF9F00FFEECD00C6958400FFD2B600EFAC8900286AD1004775
      B900FFE8CE0000000000000000000000000000000000B58C8C00FFFFF700FFE7
      CE00FFE7CE00B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B58C
      8C00B58C8C00B58C8C00B58C8C00000000000000000000000000000000000000
      00000000E7000000DE000000D60000000000000000000000E7000000EF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF000000FF00000000000000000000000000000000000000FF00000080
      000000800000FF00000080808000808080000000000000000000D5BDB600FFD4
      C900FFD7B700F0CEB600DCB9A100FCD2B800DCC7B100F7C19B0063AAE90078A2
      EB00FCD2B80000000000000000000000000000000000B58C8C00FFFFF700FFD6
      A500FFD6A500B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B58C
      8C00EFB56B00C68C7B0000000000000000000000000000000000000000000000
      FF000000DE000000EF00000000000000000000000000000000000000FF000000
      F700000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      00000080000000800000FF000000808080000000000000000000D7C0B800FFDA
      D000FFDFC000F1BF9F00E1AE9300EABFA100FFDFC000FFEAC700D4CFC000D6CE
      C400EDC9B60000000000000000000000000000000000B58C8C00FFFFFF00FFEF
      DE00FFE7D600B58C8C00B58C8C00B58C8C00B58C8C00B58C8C00B58C8C00B58C
      8C00BD84840000000000000000000000000000000000000000000000F7000000
      F7000000FF000000000000000000000000000000000000000000000000000000
      F7000000F7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF00000080000000800000FF0000000000000000000000D9C0B900FFE0
      DA00F6CBAE00E89E7800FFBF9000FCB28900E1A27F00DDA08100E9AA8900F4BF
      9B00E8CFC20000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00EFDEDE00D6C6C600BDADAD00B58473000000
      000000000000000000000000000000000000000000000000F7000000F7000000
      F700000000000000000000000000000000000000000000000000000000000000
      0000000000000000F70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000800000FF0000000000000000000000DAC0B800FFE6
      E300F5CDAD00EE844E00FF965600FF975B00FF9F6500FFA56C00FFA97200F099
      6C00DABEB10000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B58C8C00B58C8C00B58C8C00B58C8C000000
      0000000000000000000000000000000000000000F7000000F7000000F7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FF0000FF0000000000000000000000D4B6AC00FFE4
      E300FFE3C400E0A08000E18A5F00E2875C00DE7C4E00D87D5500F6844D00DC7D
      4F00D9B7A50000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B58C8C00EFB56B00C68C7B00000000000000
      0000000000000000000000000000000000000000F7000000F700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DABDB400FFFF
      FF00FFFBE000FFFFDE00FFFFE400FFFEE000FFFFE300FFF8DA00F3D1B700FFE0
      C400F6DAC60000000000000000000000000000000000B58C8C00B58C8C00B58C
      8C00B58C8C00B58C8C00B58C8C00B58C8C00BD84840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EAD8
      CB00EFDFD100E8D2C100E5CCB700E6CAAD00D5B39200EFD6AE00F8E3BA00FFFD
      CB00D7B8A1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000002C3A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009933000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000273300008DB90000CEFC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099330000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000628100003D61000073920000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000993300009933
      0000993300000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000427A00B5E4950000403F0000375000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009933
      0000AA5F1F009933000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000004B850036D03E00A0FF9D0000B60000005E5800003D58000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000099330000BA7D480099330000993300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000004B8E008DD0A4006EFF9E00A1CF9D0050CF500000B1000000525800003F
      5800000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CDA27C00D8B596009933000099330000000000000000
      0000000000000000000000000000000000000000000000273300006381000042
      840000D0000024FF4600B2F5AA0000AB00005883580058AA5800007B0000004E
      48000026370000D4FF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000E1C6B000ECDCCD00EDDDD100993300000000
      000000000000000000000000000000000000002C3A00008DB900003D6100B5E1
      9400A3FFA200B9FFC80000A30000009B0000004E00003765370096C896009ED4
      8E0053788F000464700029F6FF00000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000000
      000000000000000000000000000099330000F4E9E200FDF9F500FBF4EC009933
      0000993300000000000000000000000000000000000000CEFC00007192000058
      3F0000CA00004ED44C0058935800007E0000001D0000D9F8D900DAFFDA0097CC
      7F000D888F0038F1F0000E38370000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000099330000E1C0AB00F7E9DA00F4E0CC00E1BA
      9C00993300009933000000000000000000000000000000000000000000000034
      5000005F580000D4000058BC5800375E3700D9F8D900D9F8D90087C46F000997
      A600000000001250500000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000F6E6D600F3DEC800F0D5BA00E3B995009933
      0000000000000000000000000000000000000000000000000000000000000000
      0000003B5800006858000077000097C59700DAFFDA0087C46F000997A6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000099330000EDCAA800EAC19900E7B98B00DFA8
      7500993300000000000000000000000000000000000000000000000000000000
      0000003B5800003D5800004C48009FEC8F0097C97F000997A600006281000062
      8100000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000099330000E3AE7900E0A56B00DD9C
      5C00DA944F00993300000000000000000000000000000000000000000000027D
      A30000628100006281000026370053768F000D888F00B3EBFC00003D61000062
      8100027DA3000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099330000993300009933
      0000993300009933000099330000000000000000000000000000008DB9000000
      0000027DA3000000000000D4FF000464700038F1F0001250500000000000008D
      B90000000000008DB90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000628100027D
      A30000000000000000000000000029F7FF000E38370000000000000000000000
      0000027DA3000062810000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000062810000719200008DB9000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008DB90002749700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B5B5B5000000000000000000000000009C9C9C009C9C
      9C00A5A5A500000000000000000000000000000000000000000000000000069D
      CC0049B7E00060C1E80054BCE3003DB2DC004DB9E10000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5B5B500B5B5B500DEDEDE00ADADAD00525252006B6B6B00ADADAD00D6D6
      D600E7DEDE009C9C9C000000000000000000000000000000000008A1CE000EA5
      D1007FD2F100A0DFFE0097DAFB0093D9FA007CCDEF0073CAEE0048B7E0000000
      000000000000000000000000000000000000000000000000000000000000069D
      CC0049B7E00060C1E80054BCE3003DB2DC004DB9E10000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000B5B5B500B5B5
      B500FFFFFF00FFFFFF00E7DEDE00B5B5B5005A5A630031313100313131004A4A
      4A00848484009C9C9C00A5A5A50000000000000000000000000013A8D4001DAF
      D80083D8EE00A0E3FB0093DAF90082D0F20059BBE0008FD7F7009ADCFB0076CB
      F00000000000000000000000000000000000000000000000000008A1CE000EA5
      D1007FD2F100A0DFFE0097DAFB0093D9FA007CCDEF0073CAEE0048B7E0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000000000000000000000000000A5A5A500B5B5B500F7F7F700FFFF
      FF00F7F7F700D6D6D600B5B5B500A5A5A500ADADAD009C9C9C007B7B7B005252
      520031313900313131008C8C8C0000000000000000000000000020AFD8002EB7
      DE0094DFEF00B1EEFA00A2E7FD0064B2D100369BC0006FC7EB0095DAFB009ADC
      FB0034B1DA00000000000000000000000000000000000000000013A8D4001DAF
      D80083D8EE00A0E3FB0093DAF90082D0F20059BBE0008FD7F7009ADCFB0076CB
      F000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00000000000000000000000000B5ADAD00EFEFEF00EFEFEF00CECE
      CE00BDB5B500C6C6C600D6CECE00B5B5B500ADADAD00ADA5A500ADADAD00ADAD
      AD00A5A5A500848484009C9C9C000000000000000000000000002DB5DD003FBE
      E400B1E5F100CFF8FD00B6F4FF0083C9DE00287FA10069BDE00094DBFC0095DA
      FA0033B0D900000000000000000000000000000000000000000020AFD8002EB7
      DE0094DFEF00B1EEFA00A2E7FD0064B2D100369BC0006FC7EB0095DAFB009ADC
      FB0034B1DA000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00BDBDBD0000000000ADA5AD00BDBDBD00B5B5B500C6C6
      C600D6D6D600DEDEDE00F7F7F700F7F7F700E7E7E700D6D6D600BDBDBD00ADAD
      AD00ADA5A500ADADAD00A5A5A50000000000000000000000000039BBE30048C1
      E800C5E8F400ECFAFD00CAF5FB00B3ECF50063A9BF008BCFE80099DFFE0094D8
      FA0033B0D90000000000000000000000000000000000000000002DB5DD003FBE
      E400B1E5F100CFF8FD00B6F4FF0083C9DE00287FA10069BDE00094DBFC0095DA
      FA0033B0D9000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000A5A5A500C6BDC600D6D6D600DEDE
      DE00D6D6D600EFEFEF00DEDEDE00B5BDB500CECECE00D6DEDE00DEDEDE00DEDE
      DE00D6D6D600C6C6C600ADADAD000000000000000000000000002EB3DB0044C1
      E2005EC0DB006CC8E00062C8E20060C7E2006ECFE70086D8EE009DE2F900A2E1
      FE0034B1DA00000000000000000000000000000000000000000039BBE30048C1
      E800C5E8F400ECFAFD00CAF5FB00B3ECF50063A9BF008BCFE80099DFFE0094D8
      FA0033B0D9000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000000000000000B5ADAD00DEDEDE00DEDE
      DE00E7E7E700D6D6D600C6C6C600BDDEBD00CED6CE00D6BDB500BDB5B500BDBD
      BD00C6C6C600D6CECE00BDBDBD0000000000000000000000000029B3D80075DC
      E700328BA5003A9AB30069E1F00044CCE70021B5D90014AAD3002CAFD60079D0
      ED0037B3DA0000000000000000000000000000000000000000002EB3DB0044C1
      E2005EC0DB006CC8E00062C8E20060C7E2006ECFE70086D8EE009DE2F900A2E1
      FE0034B1DA000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000B5ADAD00CECE
      CE00C6BDBD00C6C6C600EFEFEF00FFF7F700F7F7F700F7EFE700E7E7E700DEDE
      DE00C6C6C600ADA5A500000000000000000000000000000000000000000030B2
      D6004FBBD10074D7E30098FDFF0080F1FA0050CFE60012A1C8000397C30013A4
      CF0018A5D00000000000000000000000000000000000000000002BB1D80058CD
      E40040B8D70046BEDB0045C2DF0037B7D9004AC2E2006BCFEA009AE1F80086D4
      F40025AAD40000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF000000000000000000000000000000000000000000B5AD
      AD00E7E7E700D6D6D600B5B5B500CECECE00DEDEE700DEE7E700DEDEDE00D6D6
      D600C6BDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000054CFE7004ACAE40045C1DD00129AC3000F98C20011A5
      D00011A2CF00000000000000000000000000000000000000000028B1D7005CCC
      DF000C9BC4003CB6D1008AF5FB0055D7EC0024B6DA00049DCA0025ABD20052C0
      E0001FA7D2000000000000000000000000000000000000000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFEFEF00FFE7DE00EFD6CE00EFD6CE00EFDED600E7DEDE00DEDEDE00ADAD
      AD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000109CC5000894C0000000
      0000000000000000000000000000000000000000000000000000000000000C94
      C1000091BF002FAECF0087F0F8007AECF80047C2DC000B96C000119DC4001CA9
      D00000000000000000000000000000000000000000000000000000000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000078CB8000587B300000000000000000000000000089AC6000B91BC000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000B99C600199BC300000000003ABFE00020A7CD000A95C0000D96C2000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B0000FFFF007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00002EB0DA001E9FCA00000000000000000000000000099CC8000B92BD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004FC3E6002A9DC6000000000000000000000000000994BF000C8AB5000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF00000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004CB5D50068D0EA000000000000000000000000000999C5000A92BE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00003BA6C60077DEEE003DAAC80000000000148AB6000C9DC800000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF0000000000000000007B7B7B0000FFFF007B7B7B0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEB5
      B500FFE7DE00FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005BC4D90078DFEB004FB9D4003EAED2001DA6D100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004AB5CD007BE0EC006BD1E6004ABCE0001898C300000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEB5
      B500DEB5B500DEB5B500DEB5B500F7BDB500F7BDB50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000055BFD40055C0D6004FBAD80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009A6666006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000000
      00000000000000000000C6C6C60000000000000000000000000000000000C6C6
      C600C6C6C60000000000C6C6C600000000000000000000000000000000000000
      00009A6666009A666600B9666600BB6868006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C6000000000000000000C6C6
      C600C6C6C6000000000000000000C6C6C6000000000000000000000000000000
      000000000000C6C6C600000000000000000000000000000000009A6666009A66
      6600C66A6B00D06A6B00D2686900C3686900693334009A6666009A6666009A66
      66009A6666009A6666009A666600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000660000006600000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C600C6C6
      C6000000000000000000C6C6C600000000000000000000000000848400008484
      00000000000000000000000000000000000000000000000000009A666600DE73
      7400D7707100D56F7000D56D6E00C76A6D0069333400FEA2A300FCAFB000FABC
      BD00F9C5C600F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      0000808000008080000080800000000000000000000000000000000000000000
      000000000000006600001EB231001FB133000066000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848400008484
      00008484000000000000000000000000000000000000000000009A666600E077
      7800DB757600DA747500DA727300CC6E71006933340039C5650025CF630029CC
      630019CB5B00F9C5C6009A666600000000000000000080808000808080008080
      800080808000808080008080800080808000808080000000000080800000FFFF
      0000FFFF0000FFFFFF0000000000000000000000000000000000000000000000
      00000066000031C24F0022B738001AB02D0021B4370000660000000000000000
      000000000000000000000000000000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C6000000000000000000000000000000000000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009A666600E57D
      7E00E07A7B00DF797A00DF777800D07275006933340042C4680030CD670033CB
      670024CB6000F9C5C6009A666600000000000000000000000000FFFF00008080
      0000808000008080000080800000808000000000000080800000FFFF0000FFFF
      0000FFFFFF000000000000000000000000000000000000000000000000000066
      000047D36D003BCB5E0025A83B00006600001BA92E001DB13200006600000000
      000000000000000000000000000000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C6000000000084848400848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009A666600EA82
      8300E57F8000E37D7E00E6808100D3747600693334003DC2640029CB63002FCA
      640020CA5E00F9C5C6009A66660000000000000000000000000000000000FFFF
      00008080000080800000808000000000000080800000FFFF0000FFFF0000FFFF
      FF00000000008080000000000000000000000000000000000000006600004FD6
      790053DE7F0031B54D00006600000000000000660000179D27001EAE31000066
      000000000000000000000000000000000000C6C6C600C6C6C600C6C6C600FF00
      0000FFFF0000FF00000084848400848484008484840000000000848400000000
      00008484000000000000C6C6C600C6C6C60000000000000000009A666600F087
      8800E9818200EC969700FBDDDE00D8888A0069333400B8E1AC006BDC89005DD5
      800046D47300F9C5C6009A666600000000000000000000000000000000000000
      0000FFFF0000808000000000000080800000FFFF0000FFFF0000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000066
      000041C5630000660000CE6C6C00B73D3D0000000000000000000066000019AA
      2B0000660000000000000000000000000000C6C6C600FF000000FF000000FF00
      0000FFFF0000FFFF000084840000848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009A666600F58C
      8D00EE868700F0999A00FDDCDD00DA888A0069333400FFF5D800FFFFE000FFFF
      DE00ECFDD400F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000080800000FFFF0000FFFF0000FFFFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000B73D
      3D0000660000CF6F6F00CD696900CF6E6E00B73D3D0000000000000000000066
      0000149D2100006600000000000000000000FF000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FF000000848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009A666600FA91
      9200F48E8F00F28B8C00F48C8D00DC7F800069333400FDF3D400FFFFDF00FFFF
      DD00FFFFE000F9C5C6009A666600000000000000000000000000000000000000
      00000000000080800000FFFF0000FFFF0000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000B73D3D00DA90
      9000D7868600CD696900B73D3D00CC666600CE6B6B00B73D3D00000000000000
      000000000000006600000066000000000000FF000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FF000000848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009A666600FE97
      9800F9939400F8929300F9909200E085850069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A666600000000000000000000000000000000000000
      000080800000FFFF0000FFFF0000FFFFFF000000000080800000808080000000
      00000000000000000000000000000000000000000000B73D3D00DC959500DE9B
      9B00D2777700B73D3D0000000000B73D3D00CA5F5F00CD696900B73D3D000000
      000000000000000000000066000000660000C6C6C600C6C6C600C6C6C600FF00
      0000FFFF0000FF00000084848400848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009A666600FF9B
      9C00FD979800FC969700FE979800E388890069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A666600000000000000000000000000000000008080
      0000FFFF0000FFFF0000FFFFFF0000000000FFFF000080800000808000008080
      8000000000000000000000000000000000000000000000000000B73D3D00D786
      8600B73D3D0000000000000000000000000000000000B73D3D00CC656500B73D
      3D0000000000000000000000000000000000C6C6C600C6C6C600C6C6C600FF00
      0000FF0000000000000084848400848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009A666600FF9F
      A000FF9A9B00FF999A00FF9A9B00E78C8D0069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A66660000000000000000000000000080800000FFFF
      0000FFFF0000FFFFFF00000000000000000000000000FFFF0000808000008080
      000080808000000000000000000000000000000000000000000000000000B73D
      3D00000000000000000000000000000000000000000000000000B73D3D00C95C
      5C00B73D3D00000000000000000000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C6000000000084848400848484008484840084848400FFFF00008484
      00008484000000000000C6C6C600C6C6C60000000000000000009A6666009A66
      6600E98E8F00FE999A00FF9D9E00EB8F900069333400FBF0D200FDFCDC00FDFC
      DA00FDFCDC00F9C5C6009A6666000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B73D3D00B73D3D000000000000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600000000008484840084848400848484008484840000000000FFFF
      00008484000000000000C6C6C600C6C6C6000000000000000000000000000000
      00009A666600B0717200D7868700DA888800693334009A6666009A6666009A66
      66009A6666009A6666009A666600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B73D3D00B73D3D0000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C6000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C6000000000000000000000000000000
      000000000000000000009A6666009A6666006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600424D3E000000000000003E000000
      2800000040000000500000000100010000000000800200000000000000000000
      000000000000000000000000FFFFFF00FE3FFFFF00000000E007C1FF00000000
      E007001F00000000800300010000000080010000000000008001000000000000
      800100000000000080010000000000008001000000000000C003000000000000
      E007000000000000F81F000000000000F81F000000000000F81FE00000000000
      F81FFE0100000000FC3FFFFF00000000F801FFFCFCFFE3FFF8019FF9F87FC03F
      F8018FF3F03FC007F80187E7E01FC0078001C3CFC00FC0078001F11FC007C007
      8001F83FC007C0078001FC7FC103C0078001F83FE381C0078001F19FF3C0C007
      8003E3CFFFE0C0078007C7E7FFF0C007801F8FFBFFF8C007801F1FFFFFFCC007
      803F3FFFFFFFC007807FFFFFFFFFE007FFFFFEFFFFFFFFFF7FFFFC7FFFFFFFFF
      9FFFFC7FFFFFFFFFC7FFFC3FFFFFFF3FE3FFF81FFCFFFC3FF0FFF00FFC3FF03F
      F83F8003FC0FC000FC1F000100030000FE0780010000C000FE03E00B0003F03F
      FC0FF01FFC0FFC3FFE07F00FFC3FFF3FFF03E007FCFFFFFFFF81D42BFFFFFFFF
      FFFFCE73FFFFFFFFFFFF1FF9FFFFFFFFFDC7E07FFFFFFE7FF003C01FE07FFE1F
      C001C00FC01FFC070001C007C00FFC010001C007C007F8000001C007C007F800
      0001C007C00700008001C007C0070000C003E007C0070001E007FC07C0070032
      F00FFF9FE00F003EF03FF39FF21F003EF03FF39FF39F003EF03FF39FF13F001D
      E03FF83FF83F0023E07FFC7FFFFF003FFE7FFFFFFFFFDDA5F07FFFFFFFFF669B
      C001FFE0FCFFCD87C0010000F87F0380C0010001F03F0000C0018003E01F0000
      C001C001C10F0000C001E001E0C70000C001F01FE0630000C001F01FC0390000
      C001E00F821C0000C001C007C78F0000C0018103EFC70000C0010381FFF30000
      F00107C1FFF90000FC7FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  inherited fmstr: TFormStorage
    Active = False
    Options = [fpState]
    StoredProps.Strings = (
      'plWhAppl.Width')
    Left = 128
    Top = 204
  end
  inherited ActionList2: TActionList
    Left = 68
  end
  object acEvent: TActionList
    Images = ilButtons
    Left = 68
    Top = 260
    object acAdd: TAction
      Tag = 1
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 10
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = '  '#1059#1076#1072#1083#1080#1090#1100'  '
      ImageIndex = 11
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acOpen: TAction
      Category = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      Caption = #1054#1090#1082#1088#1099#1090
      ImageIndex = 5
      OnExecute = acOpenExecute
      OnUpdate = acOpenUpdate
    end
    object acPrint: TAction
      Tag = 1
      Category = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 4
      OnExecute = acPrintExecute
    end
    object acExcel: TAction
      Category = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      ImageIndex = 1
      OnExecute = acExcelExecute
    end
    object acArt2Ins: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1042#1089#1090#1072#1074#1082#1080
      ImageIndex = 9
      OnExecute = acArt2InsExecute
      OnUpdate = acArt2InsUpdate
    end
    object acChangeArt: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
      ShortCut = 24641
      OnExecute = acChangeArtExecute
      OnUpdate = acChangeArtUpdate
    end
    object acChangeArt2: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083' 2'
      ShortCut = 24626
      OnExecute = acChangeArt2Execute
      OnUpdate = acChangeArt2Update
    end
    object acChangeSz: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1088#1072#1079#1084#1077#1088
      ShortCut = 24666
      OnExecute = acChangeSzExecute
      OnUpdate = acChangeSzUpdate
    end
    object acShowID: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = 'ShowID'
      ShortCut = 24649
      OnExecute = acShowIDExecute
    end
    object acPDel: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      OnExecute = acPDelExecute
      OnUpdate = acPDelUpdate
    end
    object acRejDel: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      OnExecute = acRejDelExecute
      OnUpdate = acRejDelUpdate
    end
    object acPrice: TAction
      Category = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      Caption = #1053#1072#1079#1085#1072#1095#1080#1090#1100' '#1094#1077#1085#1099
      OnExecute = acPriceExecute
    end
    object acInvArt2Ins: TAction
      Category = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      Caption = #1042#1089#1090#1072#1074#1082#1080
      ImageIndex = 9
      OnExecute = acInvArt2InsExecute
      OnUpdate = acInvArt2InsUpdate
    end
    object acAddToFirstFreePlace: TAction
      Tag = 2
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1087#1077#1088#1074#1086#1077' '#1089#1074#1086#1073#1086#1076#1085#1086#1077' '#1084#1077#1089#1090#1086
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acMultiSelect: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1052#1085#1086#1078#1077#1089#1090#1074#1077#1085#1085#1072#1103' '#1074#1099#1073#1086#1088#1082#1072
      ShortCut = 116
      OnExecute = acMultiSelectExecute
    end
    object acCopyBuffer: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1074' '#1073#1091#1092#1077#1088
      ImageIndex = 12
      OnExecute = acCopyBufferExecute
      OnUpdate = acCopyBufferUpdate
    end
    object acClearBuffer: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '#1073#1091#1092#1077#1088
      ImageIndex = 13
      OnExecute = acClearBufferExecute
      OnUpdate = acClearBufferUpdate
    end
    object acShowBuffer: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1073#1091#1092#1077#1088
      OnExecute = acShowBufferExecute
      OnUpdate = acShowBufferUpdate
    end
    object acPrintBuffer: TAction
      Tag = 2
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1055#1077#1095#1072#1090#1100' '#1073#1091#1092#1077#1088#1072
      OnExecute = acPrintExecute
      OnUpdate = acPrintBufferUpdate
    end
    object acFillSortInd: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1087#1086#1083#1077' SortInd'
      OnExecute = acFillSortIndExecute
      OnUpdate = acFillSortIndUpdate
    end
    object acCalcPrice: TAction
      Category = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1094#1077#1085#1091
      ImageIndex = 15
      OnExecute = acCalcPriceExecute
    end
    object acCheckRule: TAction
      Category = #1057#1077#1088#1074#1080#1089
      OnUpdate = acCheckRuleUpdate
    end
    object acCheckBarCodeReady: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1043#1086#1090#1086#1074' '#1083#1080' '#1087#1088#1080#1085#1090#1077#1088'?'
      OnExecute = acCheckBarCodeReadyExecute
    end
    object acPrintSticker: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1055#1077#1095#1072#1090#1100' '#1073#1080#1088#1082#1080
      ShortCut = 16421
      OnExecute = acPrintStickerExecute
      OnUpdate = acPrintStickerUpdate
    end
    object acStickerForward: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1042#1087#1077#1088#1077#1076
      ShortCut = 16422
      OnExecute = acStickerForwardExecute
    end
    object acStickerBackward: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1053#1072#1079#1072#1076
      ShortCut = 16424
      OnExecute = acStickerBackwardExecute
    end
    object acSetNullSticker_Count: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1057#1095#1077#1090#1095#1080#1082' '#1073#1080#1088#1086#1082
      OnExecute = acSetNullSticker_CountExecute
      OnUpdate = acSetNullSticker_CountUpdate
    end
    object acRefresh_Step: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1087#1088#1072#1074#1080#1083#1086' '#1091#1087#1088#1072#1074#1083#1077#1085#1080#1103
      OnExecute = acRefresh_StepExecute
      OnUpdate = acRefresh_StepUpdate
    end
    object acCheckInv: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1055#1088#1086#1074#1077#1088#1080#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      ImageIndex = 2
      OnExecute = acCheckInvExecute
    end
    object acScale_GetW: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1055#1086#1083#1091#1095#1080#1090#1100' '#1074#1077#1089
      OnExecute = acScale_GetWExecute
    end
    object acPhoto: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1057#1087#1080#1089#1086#1082' '#1072#1088#1090#1080#1082#1091#1083#1086#1074' '#1073#1077#1079' '#1092#1086#1090#1086#1075#1088#1072#1092#1080#1080
      ImageIndex = 17
      OnExecute = acPhotoExecute
    end
  end
  object ppInv: TTBPopupMenu
    Images = ilButtons
    Left = 640
    Top = 164
    object TBItem12: TTBItem
      Action = acInvArt2Ins
      Visible = False
    end
    object TBItem20: TTBItem
      Action = acFillSortInd
    end
    object TBItem25: TTBItem
      Action = acCheckBarCodeReady
      Enabled = False
      Visible = False
    end
    object TBItem26: TTBItem
      Action = acPrintSticker
    end
    object TBItem28: TTBItem
      Action = acStickerForward
      Enabled = False
      Visible = False
    end
    object TBItem27: TTBItem
      Action = acStickerBackward
      Enabled = False
      Visible = False
    end
    object TBItem29: TTBItem
      Action = acSetNullSticker_Count
    end
    object TBItem30: TTBItem
      Action = acRefresh_Step
      Enabled = False
      Visible = False
    end
    object TBItem32: TTBItem
      Action = acScale_GetW
    end
    object TBSeparatorItem1: TTBSeparatorItem
    end
    object TBItem3: TTBItem
      Action = acExcel
    end
  end
  object ppWh: TTBPopupMenu
    Left = 68
    Top = 204
    object TBItem22: TTBItem
      Action = acChangeArt
    end
    object TBItem5: TTBItem
      Action = acChangeArt2
    end
    object TBItem23: TTBItem
      Action = acChangeSz
    end
    object TBItem4: TTBItem
      Action = acArt2Ins
      Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1072#1088#1090'2'
    end
  end
  object ppRejInv: TTBPopupMenu
    Left = 832
    Top = 308
    object TBItem10: TTBItem
      Action = acRejDel
    end
  end
  object ppPInv: TTBPopupMenu
    Left = 836
    Top = 360
    object TBItem9: TTBItem
      Action = acPDel
    end
  end
  object ppMatrixInv: TTBPopupMenu
    Images = ilButtons
    Left = 672
    Top = 156
    object TBItem13: TTBItem
      Action = acPrint
      Caption = #1055#1077#1095#1072#1090#1100' '#1083#1080#1089#1090#1072
    end
    object TBSeparatorItem2: TTBSeparatorItem
    end
    object TBItem14: TTBItem
      Action = acCopyBuffer
    end
    object TBItem19: TTBItem
      Action = acShowBuffer
    end
    object TBItem15: TTBItem
      Action = acClearBuffer
    end
  end
  object quDDL: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    ParamCheck = False
    Left = 180
    Top = 148
  end
end
