unit Store_SUM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList, ExtCtrls, ComCtrls,
  StdCtrls, DBCtrls, dbUtil, ActnList, rxPlacemnt, rxSpeedbar;

type
  TfmStore_SUM = class(TfmAncestor)
    Label1: TLabel;
    Label2: TLabel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    lbAssort1: TLabel;
    lbQ1: TLabel;
    lbW1: TLabel;
    lbSUM1: TLabel;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    lbAssort2: TLabel;
    lbQ2: TLabel;
    lbW2: TLabel;
    lbSUM2: TLabel;
    GroupBox3: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    lbAssort3: TLabel;
    lbQ3: TLabel;
    lbW3: TLabel;
    lbSum3: TLabel;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmStore_SUM: TfmStore_SUM;

implementation

uses StoreByItems, InvData, DictData;

{$R *.dfm}

procedure TfmStore_SUM.FormShow(Sender: TObject);
begin
  inherited;
  Screen.Cursor:=crSQLWait;
  Label2.Caption:=fmStorebyItems.laDep.Caption;
  with dmINV do
  begin
   if FCurrDep=-1 then
   begin
    taSUM.ParamByName('DEPID1').Asinteger:=-MAXINT;
    taSUM.ParamByName('DEPID2').Asinteger:=MAXINT;
   end else
   begin
    taSUM.ParamByName('DEPID1').Asinteger:=FCurrDEp;
    taSUM.ParamByName('DEPID2').Asinteger:=FCurrDEp;
   end;
   taSUM.ParamByName('BD').AsTimeStamp:=DateTimeToTimeStamp(dm.whBeginDate);
   taSUM.ParamByName('ED').AsTimeStamp:=DateTimeToTimeStamp(dm.whEndDate);
   taSUM.ParamByName('State').Asinteger:=0;
   ReOpenDataSets([taSUM]);
   lbAssort1.Caption:=taSUM.fieldbyNAme('ASSORT').AsString;
   lbQ1.Caption:=taSUM.fieldbyNAme('Q').AsString;
   lbW1.Caption:=taSUM.fieldbyNAme('W').AsString;
   lbSUM1.Caption:=FloatTOSTR(round(taSUM.fieldbyNAme('SUMM').Asfloat*100)/100);
   taSUM.ParamByName('State').Asinteger:=1;
   ReOpenDataSets([taSUM]);
   lbAssort2.Caption:=taSUM.fieldbyNAme('ASSORT').AsString;
   lbQ2.Caption:=taSUM.fieldbyNAme('Q').AsString;
   lbW2.Caption:=taSUM.fieldbyNAme('W').AsString;
   lbSUM2.Caption:=FloatTOSTR(round(taSUM.fieldbyNAme('SUMM').Asfloat*100)/100);

//   taSUM.ParamByName('State').Asinteger:=0;
  // ReOpenDataSets([taSUM]);
  end;
   Screen.Cursor:=crDefault;
end;

procedure TfmStore_SUM.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CloseDataSets([dmINV.taSum]);
end;

end.
