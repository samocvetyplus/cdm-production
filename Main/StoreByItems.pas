unit StoreByItems;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList, ExtCtrls, ComCtrls, ListAncestor, DB,
  StdCtrls, Menus, Grids, DBGrids, RXDBCtrl, pFIBQuery, dbUtil, Period,
  Mask, RxToolEdit, ActnList, fmUtils, DBGridEh, MsgDialog,  eIns, dbeditor,
  DBCtrlsEh, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, PrnDbgeh, cxGraphics, DBGridEhGrouping, GridsEh,
  rxPlacemnt, rxSpeedbar, cxLookAndFeels, cxLookAndFeelPainters;
          
type
  TfmStorebyItems = class(TfmAncestor)
    tb2: TSpeedBar;
    laDep: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    spitDep: TSpeedItem;
    spitPeriod: TSpeedItem;
    ppDep: TPopupMenu;
    mnitAllWh: TMenuItem;
    mnitSep: TMenuItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedBar1: TSpeedBar;
    SpeedbarSection3: TSpeedbarSection;
    Label1: TLabel;
    Label2: TLabel;
    edArt: TComboEdit;
    edUID: TComboEdit;
    SpeedBar2: TSpeedBar;
    SpeedbarSection4: TSpeedbarSection;
    lbState: TLabel;
    SpeedItem3: TSpeedItem;
    pmState: TPopupMenu;
    Dct1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    SpeedItem4: TSpeedItem;
    N6: TMenuItem;
    grStore: TDBGridEh;
    SpeedItem5: TSpeedItem;
    ActionList1: TActionList;
    acSum: TAction;
    acPrint: TAction;
    acHistory: TAction;
    acIns: TAction;
    lb1: TLabel;
    cmbxSelfComp: TDBComboBoxEh;
    Label3: TLabel;
    cmbxBuh: TcxComboBox;
    PrintDBGridEh1: TPrintDBGridEh;
    Label4: TLabel;
    cmbxMaterial: TDBComboBoxEh;
    procedure spitPeriodClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Dct1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure edArtButtonClick(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acSumExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acHistoryExecute(Sender: TObject);
    procedure acInsExecute(Sender: TObject);
    procedure cmbxSelfCompChange(Sender: TObject);
    procedure cmbxBuhPropertiesChange(Sender: TObject);
    procedure cmbxMaterialChange(Sender: TObject);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  protected
    FCurrDep : integer;
    FQuery : TpFIBQuery;
    FBD, FED: TDateTime;
    FOnChangePeriod : TOnChangePeriod;
    SearchEnable: boolean;
    StopFlag: boolean;
    procedure DepClick(Sender : TObject);virtual;
    procedure ShowPeriod;virtual;
  public
  end;

var
  fmStorebyItems: TfmStorebyItems;

implementation

uses DictData, InvData, ItemHistory, Store_SUM, PrintData, eArtIns, dbTree;

{$R *.dfm}

procedure TfmStorebyItems.DepClick(Sender: TObject);
begin
  laDep.Caption := TMenuItem(Sender).Caption;
  dm.CurrDep := TMenuItem(Sender).Tag;
  FCurrDep:= TMenuItem(Sender).Tag;
  ReOpenDataSets([grStore.DataSource.DataSet]);
end;

procedure TfmStorebyItems.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(FBD) + ' �� ' +DateToStr(FED);
end;

procedure TfmStorebyItems.spitPeriodClick(Sender: TObject);
begin
  if MessageDialog('����������� �����?', mtInformation, [mbYes, mbNo], 0)=mrNo then Exit;
  if ShowPeriodForm(FBD, FED) then
  begin
    ShowPeriod;
    Application.ProcessMessages;
    Screen.Cursor:=crSQLWait;
    try    
      dmInv.sqlCreateProdStore.ParamByName('BDATE').AsTimeStamp:=DateTimeToTimeStamp(FBD);
      //ShowMessage('BD: ' + dateTimeToStr(FBD));
      dmInv.sqlCreateProdStore.ParamByName('EDATE').AsTimeStamp:=DateTimeToTimeStamp(FED);
      //showMessage('ED: ' + DateTimeToStr(FED));
      dmInv.sqlCreateProdStore.ParamByName('USERID').AsInteger := 0;
      dmInv.sqlCreateProdStore.ExecQuery;
      dmInv.sqlCreateProdStore.Transaction.CommitRetaining;
      ReOpenDataSets([grStore.DataSource.DataSet]);
    finally
      Screen.Cursor:=crDefault;
    end;
    Application.ProcessMessages;
  end;
end;

procedure TfmStorebyItems.FormCreate(Sender: TObject);
var
  i, j : integer;
  Item : TMenuItem;
  r : Variant;
  nd : TNodeData;
  CompanyIndex: Integer;
begin
  SearchEnable:=true;
  dm.CurrDep := 23;
  dmINV.FCurrDep:=ROOT_COMP_CODE;
  dmINV.FCurrState:=ROOT_STATE_CODE;
  inherited;
  // ��������
  cmbxSelfComp.OnChange := nil;
  cmbxSelfComp.Items.Clear;
  OpenDataSet(dm.taCompLogin);
  CompanyIndex := 0;
  nd := TNodeData.Create;
  nd.Code := -1;
  cmbxSelfComp.Items.AddObject('*���', nd);
  while not dm.taCompLogin.Eof do
  begin
    nd := TNodeData.Create;
    nd.Code := dm.taCompLoginCOMPID.AsInteger;
    if nd.Code = dm.User.SelfCompId then CompanyIndex := cmbxSelfComp.Items.Count;
    cmbxSelfComp.Items.AddObject(dm.taCompLoginNAME.AsString, nd);
    dm.taCompLogin.Next;
  end;
  CloseDataSet(dm.taCompLogin);

  cmbxSelfComp.ItemIndex := CompanyIndex;
  cmbxSelfComp.OnChange := cmbxSelfCompChange;

// ��������
  cmbxMaterial.OnChange := nil;
  cmbxMaterial.Items.Clear;
  ReOpenDataSet(dm.taMat);
  nd := TNodeData.Create;
  nd.Code := '';
  cmbxMaterial.Items.AddObject('*���', nd);
  while not dm.taMat.Eof do
  begin
    if dm.taMatGOOD.AsInteger = 1 then
    begin
      nd := TNodeData.Create;
      nd.Code := dm.taMatID.AsString;
      cmbxMaterial.Items.AddObject(Trim(dm.taMatNAME.AsString), nd);
    end;
    dm.taMat.Next;
  end;
  CloseDataSet(dm.taMat);

  cmbxMaterial.ItemIndex := 0;
  cmbxMaterial.OnChange := cmbxMaterialChange;

  //���������� PopupMenu �� FDepInfo;
  j := 0;
  for i:=0 to Pred(dm.CountDep) do
  begin
  if dm.DepInfo[i].Wh then
  begin
    Item := TMenuItem.Create(ppDep);
    Item.Caption := dm.DepInfo[i].SName;
    Item.Tag := dm.DepInfo[i].DepId;
    Item.OnClick := DepClick;
    Item.RadioItem := True;
    if Item.Tag = dm.DepDef then DepClick(Item);
    inc(j);
    if j>30 then
    begin
      Item.Break := mbBreak;
      j:=0;
    end;
    ppDep.Items.Add(Item);
  end;
  end;
  r := ExecSelectSQL('select STORE_BDATE from D_Rec', dm.quTmp);
  if VarIsNull(r) then FBD:= StrToDateTime('01.01.2000') else FBD := VarToDateTime(r);
  r := ExecSelectSQL('select STORE_EDATE from D_Rec', dm.quTmp);
  if VarIsNull(r) then FED := StrToDateTime('01.01.2000') else FED := VarToDateTime(r);
  mnitAllWh.OnClick := DepClick;
  laDep.Caption := mnitAllWh.Caption;
  inherited;
  // ���������� ������
  ShowPeriod;
  Screen.Cursor:=crSQLWait;

  dmINV.FCurrState := 0;
  lbState.Caption:= '�� ������';

  with dmINV do
  begin
    taStoreByItems2.ParamByName('ART_').AsString:='';
    OpenDataSets([taStoreByItems2]);
  end;
  if CompanyIndex <> 0 then
  cmbxSelfCompChange(nil);
  Screen.Cursor:=crDefault;
end;

procedure TfmStorebyItems.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  CloseDataSets([dmInv.taStoreByItems2]);
  // ������� ����� �� ������������ ���������
  if Assigned(fmArtIns) then
  begin
    fmArtIns.Close;
    FreeAndNil(fmArtIns);
  end;
end;

procedure TfmStorebyItems.edUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
if Key = 13 then
  begin
    try
    if SearchEnable   then
    begin
      StopFlag:=False;
      with dmINV, taStoreByItems2 do
       if Active then Locate('UID', StrToInt(edUID.Text), [loPartialKey]);
     end;
  except
    ShowMessage('dmInv.taStoreByItems2: Locate by "UID" error');
  end;
  end;
end;

procedure TfmStorebyItems.Dct1Click(Sender: TObject);
begin
  inherited;
  Screen.Cursor:=crSQLWait;
  case TMenuItem(Sender).Tag  of
   -1: dmINV.FCurrState:=ROOT_STATE_CODE;
    0,1,2,4,5: dmINV.FCurrState:=TMenuItem(Sender).Tag;
  end;
  ReopenDatasets([dmINV.taStoreByItems2]);
  lbState.Caption:=TMenuItem(Sender).Caption;
  Screen.Cursor:=crDefault;
end;

procedure TfmStorebyItems.FormKeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
begin
  inherited;
  if (Shift=[ssCtrl]) and (Key=Ord('F')) then edUID.SetFocus;
end;

procedure TfmStorebyItems.edArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key=VK_RETURN then
  begin
   dmINv.taStoreByItems2.ParamByName('ART_').AsString:=edArt.Text;
   ReOpenDataSets([dmINv.taStoreByItems2]);
  end;
end;

procedure TfmStorebyItems.DBGridEh1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
    case dmINV.taStoreByItems2STATE.AsInteger of
     0: Background:=clStore;
     1: Background:=clSell;
     2: Background:=clReturn;
     4: Background:=clOut;
     5: Background:=clOut2;
    end;
    if Column.FIELD.FieldName='TPRICE' then Background:=clAqua;
    if Column.FIELD.FieldName='ART2' then Background:=clBtnFace;
    if Column.FIELD.FieldName='UID' then Background:=clBtnFace;
end;

procedure TfmStorebyItems.edArtButtonClick(Sender: TObject);
begin
  inherited;
   dmINv.taStoreByItems2.ParamByName('ART_').AsString:='';
   ReOpenDataSets([dmINv.taStoreByItems2]);
end;

procedure TfmStorebyItems.SpeedItem5Click(Sender: TObject);
begin
  inherited;
  DM.AArt2Id:=dmInv.taStoreByItems2ART2ID.asinteger;
  ShowDBEditor(TfmeIns,dm.dsrA2Ins,embrowse);
end;

procedure TfmStorebyItems.acSumExecute(Sender: TObject);
begin
  inherited;
  ShowAndFreeForm(TfmStore_SUM,Self, TForm(fmStore_SUM),True, False);
end;

procedure TfmStorebyItems.acPrintExecute(Sender: TObject);
var
  Id1: integer;
  Id2: integer;
  Mat1: string;
  Mat2: string;
begin
  if cmbxSelfComp.ItemIndex = 0 then
  begin
    id1 := -MAXINT;
    id2 := MAXINT;
  end
  else
  begin
    id1 := TNodeData(cmbxSelfComp.Items.Objects[cmbxSelfComp.ItemIndex]).Code;
    id2 := id1;
  end;
  if cmbxMaterial.ItemIndex = 0 then
  begin
    Mat1 := #32;
    Mat2 := #255
  end
  else
  begin
    Mat1 := TNodeData(cmbxMaterial.Items.Objects[cmbxMaterial.ItemIndex]).Code;
    Mat2 := Mat1;
  end;
  dmPrint.PrintDocumentA(dkStoreByItems, VarArrayOf([VarArrayOf([Id1, Id2, Mat1, Mat2])]));
end;

procedure TfmStorebyItems.acHistoryExecute(Sender: TObject);
begin
  inherited;
  dmINV.FCurrUId:=dmINV.taStoreByItems2UID.ASinteger;
  OpenDataSets([dmINV.taItemHistory]);
  ShowAndFreeForm(TfmItemHistory, Self, TForm(fmItemHistory), True, False);
  CloseDataSets([dmINV.taItemHistory]);
end;

procedure TfmStorebyItems.acInsExecute(Sender: TObject);
begin
  inherited;
  dmInv.CurrArt2Id := dmInv.taStoreByItems2ART2ID.AsInteger;
  if fmArtIns=nil then
  begin
    fmArtIns:=TfmArtIns.Create(Self);
    fmArtIns.Show;
  end
end;

procedure TfmStorebyItems.cmbxSelfCompChange(Sender: TObject);
begin
  if cmbxSelfComp.ItemIndex = -1 then eXit;
  //if cmbxSelfComp.ItemIndex = 0 then grStore.DataSource.DataSet.Filtered := False
  //else
  with dmInv.taStoreByItems2 do
  begin
    DisableScrollEvents;
    DisableControls;
    try
      Filtered := False;

      Filter := '';

      if (cmbxSelfComp.ItemIndex <> -1) and (cmbxSelfComp.ItemIndex<>0) then
      begin
        Filter := 'SELFCOMPID='+IntToStr(TNodeData(cmbxSelfComp.Items.Objects[cmbxSelfComp.ItemIndex]).Code);
      end;

      if (cmbxBuh.ItemIndex <> -1) and (cmbxBuh.ItemIndex<>0) then
      begin
        if Filter<>'' then Filter:=Filter+' and BUH_10='+IntToStr(cmbxBuh.ItemIndex-1)
        else Filter:= 'BUH_10='+IntToStr(cmbxBuh.ItemIndex-1);
      end;

      //Filter := 'SELFCOMPID='+IntToStr(TNodeData(cmbxSelfComp.Items.Objects[cmbxSelfComp.ItemIndex]).Code);
      if Filter <> '' then
      Filtered := True;
      First;
    finally
      EnableScrollEvents;
      EnableControls;
    end;
  end;
  grStore.SumList.RecalcAll;
end;

procedure TfmStorebyItems.cmbxBuhPropertiesChange(Sender: TObject);
begin
  if cmbxBuh.ItemIndex = -1 then eXit;
  //if cmbxBuh.ItemIndex = 0 then grStore.DataSource.DataSet.Filtered := False
  //else
  with dmInv.taStoreByItems2 do
  begin
    DisableScrollEvents;
    DisableControls;
    try
      Filtered := False;
      Filter := '';
      if (cmbxSelfComp.ItemIndex <> -1) and (cmbxSelfComp.ItemIndex<>0) then
        Filter := 'SELFCOMPID='+IntToStr(TNodeData(cmbxSelfComp.Items.Objects[cmbxSelfComp.ItemIndex]).Code);

      if (cmbxBuh.ItemIndex <> -1) and (cmbxBuh.ItemIndex<>0) then
      begin
        if Filter<>'' then Filter:=Filter+' and BUH_10='+IntToStr(cmbxBuh.ItemIndex-1)
        else Filter:= 'BUH_10='+IntToStr(cmbxBuh.ItemIndex-1);
      end;
      //if Filter<>'' then Filter := copy(Filter, 0, Length(Filter)-1);
      Filtered := True;
    finally
      EnableScrollEvents;
      EnableControls;
    end;
  end;
  grStore.SumList.RecalcAll;
end;

procedure TfmStorebyItems.cmbxMaterialChange(Sender: TObject);
begin
  Screen.Cursor:=crSQLWait;
  if cmbxMaterial.ItemIndex = 0 then dmInv.FCurrMat := ''
  else dmInv.FCurrMat := TNodeData(cmbxMaterial.Items.Objects[cmbxMaterial.ItemIndex]).Code;
  ReopenDatasets([dmINV.taStoreByItems2]);
  Screen.Cursor:=crDefault;
end;

end.
