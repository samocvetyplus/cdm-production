{***********************************************}
{  ����� ����������                             }
{  Copyrigth (C) 2003 basile for CDM            }
{  v0.41                                        }
{***********************************************}
unit WhSemis;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList, ExtCtrls, ComCtrls,
  Grids, DBGridEh, StdCtrls, Mask, DBCtrlsEh, TB2Item, TB2Dock, TB2Toolbar,
  ActnList, db, DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmWHSemis = class(TfmAncestor)
    gridWhSemis: TDBGridEh;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    cmbxSemis: TDBComboBoxEh;
    TBControlItem2: TTBControlItem;
    TBControlItem3: TTBControlItem;
    Label2: TLabel;
    cmbxDep: TDBComboBoxEh;
    TBControlItem4: TTBControlItem;
    TBControlItem5: TTBControlItem;
    Label3: TLabel;
    cmbxOper: TDBComboBoxEh;
    TBControlItem6: TTBControlItem;
    TBControlItem7: TTBControlItem;
    Label4: TLabel;
    cmbxMat: TDBComboBoxEh;
    TBControlItem8: TTBControlItem;
    spitPrint: TSpeedItem;
    acEvent: TActionList;
    acPrint: TAction;
    procedure FormCreate(Sender: TObject);
    procedure cmbxDepChange(Sender: TObject);
    procedure cmbxMatChange(Sender: TObject);
    procedure cmbxOperChange(Sender: TObject);
    procedure cmbxSemisChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPrintExecute(Sender: TObject);
  end;

var
  fmWHSemis: TfmWHSemis;

implementation

uses MainData, DictData, dbUtil, dbTree, PrintData;

{$R *.dfm}

procedure TfmWHSemis.FormCreate(Sender: TObject);
var
  SaveEvent : TNotifyEvent;
begin
  dm.WorkMode := 'aWhSemis';
  // ����������
  with dm.tr do
  begin
    if Active then Commit;
    StartTransaction;
  end;
  // ���������������� ������� ������ ������������
  SaveEvent := cmbxOper.OnChange;
  cmbxOper.OnChange := nil;
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.ItemIndex := 0;
  cmbxOper.Items.Insert(1, sNoOperation);

  dmMain.FilterOperId := ROOT_OPER_CODE;
  cmbxOper.OnChange := SaveEvent;

  SaveEvent := cmbxDep.OnChange;
  cmbxDep.OnChange := nil;
  cmbxDep.Items.Assign(dm.dPSWhProd);
  cmbxDep.ItemIndex := 0;
  dmMain.FilterDepId := ROOT_PS_CODE;
  cmbxDep.OnChange := SaveEvent;

  SaveEvent := cmbxSemis.OnChange;
  cmbxSemis.OnChange := nil;
  cmbxSemis.Items.Assign(dm.dSemis);
  cmbxSemis.ItemIndex := 0;
  dmMain.FilterSemisId := ROOT_SEMIS_CODE;
  cmbxSemis.OnChange := SaveEvent;

  SaveEvent := cmbxMat.OnChange;
  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMat);
  cmbxMat.ItemIndex := 0;
  dmMain.FilterMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := SaveEvent;
  // ������� ������ ������
  OpenDataSet(dmMain.taWhS);
end;

procedure TfmWHSemis.cmbxDepChange(Sender: TObject);
begin
  if cmbxDep.ItemIndex = -1 then eXit;
  dmMain.FilterDepId := TNodeData(cmbxDep.Items.Objects[cmbxDep.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taWHS);
end;

procedure TfmWHSemis.cmbxMatChange(Sender: TObject);
begin
  if cmbxMat.ItemIndex = -1 then eXit;
  dmMain.FilterMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taWHS);
end;

procedure TfmWHSemis.cmbxOperChange(Sender: TObject);
begin
  if cmbxOper.ItemIndex = -1 then eXit;
  with cmbxOper, Items do
    if Items[ItemIndex] = sNoOperation then dmMain.FilterOperId := '���'
    else dmMain.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taWHS);
end;

procedure TfmWHSemis.cmbxSemisChange(Sender: TObject);
begin
  if cmbxSemis.ItemIndex = -1 then eXit;
  dmMain.FilterSemisId := TNodeData(cmbxSemis.Items.Objects[cmbxSemis.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taWHS);
end;

procedure TfmWHSemis.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmMain.taWhS);
  inherited;
end;

procedure TfmWHSemis.acPrintExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkWhS, Null);
end;

end.
