unit WhTails;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList,  ImgList,  ExtCtrls,
  ComCtrls, Grids, DBGridEh, Buttons, TB2Item, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, StdCtrls, TB2Dock,
  TB2Toolbar, cxPC, cxControls, DB, FIBDataSet, pFIBDataSet, DBGridEhGrouping, GridsEh,
  rxPlacemnt, rxSpeedbar, cxGraphics, cxLookAndFeels, cxLookAndFeelPainters;

type
  TfmWhTails = class(TfmAncestor)
    acRefresh: TAction;
    spbtnRefresh: TSpeedButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    gridWhTails: TDBGridEh;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    edCalcDate: TcxDateEdit;
    TBControlItem2: TTBControlItem;
    gridWhTailsDate: TDBGridEh;
    TBDock2: TTBDock;
    TBToolbar2: TTBToolbar;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acCalcWh_Date: TAction;
    taWhTailsDate: TpFIBDataSet;
    taWhTailsDateID: TFIBIntegerField;
    taWhTailsDateTAILSID: TFIBIntegerField;
    taWhTailsDateW: TFIBFloatField;
    taWhTailsDateUW: TFIBSmallIntField;
    taWhTailsDateTAILNAME: TFIBStringField;
    taWhTailsDateDEPNAME: TFIBStringField;
    dsrWhTailsDate: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acRefreshExecute(Sender: TObject);
    procedure acRefreshUpdate(Sender: TObject);
    procedure acCalcWh_DateExecute(Sender: TObject);
    procedure taWhTailsDateUWGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
  end;

var
  fmWhTails: TfmWhTails;

implementation

uses MainData, dbUtil, DictData, MsgDialog, DateUtils;

{$R *.dfm}

procedure TfmWhTails.FormCreate(Sender: TObject);
begin
  inherited;
  ExecSQL('delete from Tmp_WhTails', dm.quTmp);
  OpenDataSets([dmMain.taWhTails, taWhTailsDate]);
  edCalcDate.Date := ToDay;
end;

procedure TfmWhTails.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dmMain.taWhTails, taWhTailsDate]);
  inherited;
end;

procedure TfmWhTails.acRefreshExecute(Sender: TObject);
begin
  if MessageDialog('����������� ����� �����?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  ExecSQL('execute procedure ReCalc_WHTails', dmMain.quTmp);
  ReOpenDataSet(dmMain.taWhTails);
  MessageDialogA('����� ����� ����������', mtInformation);
end;

procedure TfmWhTails.acRefreshUpdate(Sender: TObject);
begin
  acRefresh.Visible := dm.User.Profile=-1; // �������������
end;

procedure TfmWhTails.acCalcWh_DateExecute(Sender: TObject);
begin
  ExecSQL('execute procedure Build_WhTails_Date(:ED)', VarArrayOf([edCalcDate.Date]), dm.quTmp);
  ReOpenDataSet(taWhTailsDate);
end;

procedure TfmWhTails.taWhTailsDateUWGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
          -1 : Text:='���';
           3 : Text:='��';
           4 : Text:='�����';
         end;
end;

end.
