inherited fmACTbyItem: TfmACTbyItem
  Left = 217
  Top = 186
  Width = 817
  Height = 473
  Caption = #1040#1082#1090' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 420
    Width = 809
  end
  inherited tb1: TSpeedBar
    Width = 809
    inherited spitExit: TSpeedItem
      Left = 707
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Hint = #1055#1077#1095#1072#1090#1100'|'
      ImageIndex = 4
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1055#1088#1086#1089#1084#1086#1090#1088
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088
      Hint = #1055#1088#1086#1089#1084#1086#1090#1088'|'
      ImageIndex = 5
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100'|'
      ImageIndex = 1
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = SpeedItem3Click
      SectionName = 'Untitled (0)'
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 42
    Width = 809
    Height = 378
    Align = alClient
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
    AutoFitColWidths = True
    Color = clWhite
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmInv.dsrPriceACTS
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    FrozenCols = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = SpeedItem2Click
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RecNo'
        Footers = <>
        Width = 25
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Width = 100
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ART2'
        Footers = <>
        Width = 62
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OLD_PRICE'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'NEW_PRICE'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footers = <>
      end
      item
        Color = 15658734
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SUM_Q0'
        Footer.FieldName = 'SUM_Q0'
        Footer.Font.Charset = RUSSIAN_CHARSET
        Footer.Font.Color = clTeal
        Footer.Font.Height = -11
        Footer.Font.Name = 'Times New Roman'
        Footer.Font.Style = [fsBold]
        Footer.ValueType = fvtSum
        Footers = <>
        Width = 83
      end
      item
        Color = 12382157
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'IN_SUM'
        Footers = <>
        Width = 83
      end
      item
        Color = 12910591
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OUT_SUM'
        Footers = <>
        Width = 90
      end
      item
        Color = 16641237
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RAZ_SUM'
        Footer.DisplayFormat = '0.00'
        Footer.FieldName = 'RAZ_SUM'
        Footer.Font.Charset = RUSSIAN_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Times New Roman'
        Footer.Font.Style = [fsBold]
        Footer.ValueType = fvtSum
        Footers = <>
        Width = 71
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited fmstr: TFormStorage
    Left = 168
    Top = 120
  end
end
