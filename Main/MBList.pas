{*****************************************}
{  ������ �������� ����������             }
{  Copyrigth (C) 2005 Kornejchouk Basile  }
{  v0.1                                   }
{*****************************************}
unit MBList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor,  TB2Item, ActnList, Menus, 
  ImgList, Grids, DBGridEh, StdCtrls, ExtCtrls, ComCtrls, Mask,
  DBCtrlsEh, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmMBList = class(TfmListAncestor)
    Label1: TLabel;
    edYear: TDBNumberEditEh;
    procedure acAddDocExecute(Sender: TObject);
    procedure edYearChange(Sender: TObject);
    procedure edYearKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure acDelDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acPeriodUpdate(Sender: TObject);
  end;

var
  fmMBList: TfmMBList;

implementation

uses DictData, Data, MatBalance, MainData, eMonth, DateUtils, UtilLib,
  ProductionConsts, MsgDialog, DbUtil, DocAncestor;

{$R *.dfm}

procedure TfmMBList.acAddDocExecute(Sender: TObject);
var
  DefaultMonth : smallint;
  Bookmark : Pointer;
  Msg : string;
begin
  DefaultMonth := -1;
  // ����� �����
  Bookmark := dmData.taMBList.GetBookmark;
  dmData.taMBList.DisableControls;
  try
    dmData.taMBList.First;
    while not dmData.taMBList.Eof do
    begin
      if MonthOf(dmData.taMBListABD.AsDateTime) > DefaultMonth then
        DefaultMonth := MonthOf(dmData.taMBListABD.AsDateTime);
      dmData.taMBList.Next;
    end;
  finally
    dmData.taMBList.GotoBookmark(Bookmark);
    dmData.taMBList.EnableControls;
  end;
  if DefaultMonth = -1 then DefaultMonth := 1
  else if DefaultMonth = 12 then raise EWarning.Create(Format(rsMB_AlreadyCreate, [edYear.Text]))
  else inc(DefaultMonth);
  Msg := '������� ������ ���������� �� '+cMonth[DefaultMonth]+'?';
  if MessageDialog(Msg, mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  inherited;
  dmData.taMBList.Edit;
  dmData.taMBListABD.AsDateTime := StartOfAMonth(edYear.Value, DefaultMonth);
  dmData.taMBListAED.AsDateTime := EndOfAMonth(edYear.Value, DefaultMonth);
  dmData.taMBList.Post;
  Screen.Cursor := crSQLWait;
  try
    // �������� ������� ����������
    ExecSQL('execute procedure Create_MatBalance('+dmData.taMBListINVID.AsString+')', dm.quTmp);
  finally
    Screen.Cursor := crDefault;
  end;
  ShowDocForm(TfmMatBalance, dmData.taMBList, dmData.taMBListINVID.AsInteger, dm.quTmp);
  dmData.taMBList.Refresh;
end;

procedure TfmMBList.edYearChange(Sender: TObject);
begin
  dmMain.DocListYear := edYear.Value;
  ReOpenDataSet(DataSet);
end;

procedure TfmMBList.edYearKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : begin
      dmMain.DocListYear := edYear.Value;
      ReOpenDataSet(DataSet);
    end;
  end;
end;

procedure TfmMBList.FormCreate(Sender: TObject);
begin
  dm.WorkMode := 'MatBalance';
  // ���������� ���
  edYear.OnChange := nil;
  edYear.Value := dmMain.DocListYear;
  edYear.OnChange := edYearChange;
  inherited;    
end;

procedure TfmMBList.acDelDocExecute(Sender: TObject);
begin
  inherited;
  DataSet.Transaction.CommitRetaining;
end;

procedure TfmMBList.acEditDocExecute(Sender: TObject);
begin
  ShowDocForm(TfmMatBalance, dmData.taMBList, dmData.taMBListINVID.AsInteger, dm.quTmp);
  dmData.taMBList.Refresh;
end;

procedure TfmMBList.acPeriodUpdate(Sender: TObject);
begin
  acPeriod.Visible := False;
end;

end.
