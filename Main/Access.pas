unit Access;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, CheckLst, ComCtrls, M207DBCtrls;

type
  TfmAccess = class(TfmEditor)
    pgctrlAccess: TPageControl;
    tbshProd: TTabSheet;
    btsAccProd: TM207DBBits;
    tbshWh: TTabSheet;
    tbshDict: TTabSheet;
    btsAccDict: TM207DBBits;
    btsAccWh: TM207DBBits;
    tbshAppl: TTabSheet;
    tbshWhSemis: TTabSheet;
    btsAccMat: TM207DBBits;
    btsAccAppl: TM207DBBits;
    rgrProd: TRadioGroup;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure btsAccProdClick(Sender: TObject);
    procedure pgctrlAccessChange(Sender: TObject);
    procedure btsAccProdClickCheck(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    
  private
    CurIndex : integer;
  end;

var
  fmAccess: TfmAccess;
  FIO : string;

implementation

uses DictData, dbUtil;

{$R *.dfm}

procedure TfmAccess.FormCreate(Sender: TObject);
begin
  inherited;
  Caption := '����� ������� '+FIO;
  pgctrlAccess.ActivePage := tbshProd;
  // ���������� ��������
  if((dm.taMOLACCPROD_D.AsInteger and 1) = 0) then rgrProd.ItemIndex := 0
  else rgrProd.ItemIndex := 1;
end;

procedure TfmAccess.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOK : PostDataSet(dm.taMOL);
    mrCancel : CancelDataSet(dm.taMOL);
  end;
end;

procedure TfmAccess.pgctrlAccessChange(Sender: TObject);
begin
  if(pgctrlAccess.ActivePage = tbshDict)then ActiveControl := btsAccDict
  else if(pgctrlAccess.ActivePage = tbshProd)then ActiveControl := btsAccProd
  else if(pgctrlAccess.ActivePage = tbshWh)then ActiveControl := btsAccWh
  else if(pgctrlAccess.ActivePage = tbshWhSemis)then ActiveControl := btsAccMat
  else if(pgctrlAccess.ActivePage = tbshAppl)then ActiveControl := btsAccAppl;
  CurIndex := 0;
end;


procedure TfmAccess.btnOkClick(Sender: TObject);
var l: longint;
begin
  if btsAccProd.Checked[CurIndex] then
  begin
    dm.taMOL.Edit;
    l := 1;
    if(rgrProd.ItemIndex = 0)
      then
        dm.taMOLACCPROD_D.AsInteger := dm.taMOLACCPROD_D.AsInteger and not (l shl CurIndex)
    else
      dm.taMOLACCPROD_D.AsInteger := dm.taMOLACCPROD_D.AsInteger or (l shl CurIndex);
    dm.taMOL.Post;
  end;
  inherited;
end;

procedure TfmAccess.btsAccProdClick(Sender: TObject);
var
  l: longint;
begin
  // ��������� ��������� � ������� �������
  if btsAccProd.Checked[CurIndex] then
  begin
    dm.taMOL.Edit;
    l := 1;
    if(rgrProd.ItemIndex = 0)
      then
        dm.taMOLACCPROD_D.AsInteger := dm.taMOLACCPROD_D.AsInteger and not (l shl CurIndex)
    else
      dm.taMOLACCPROD_D.AsInteger := dm.taMOLACCPROD_D.AsInteger or (l shl CurIndex);
    dm.taMOL.Post;
  end;

  CurIndex := btsAccProd.ItemIndex;
  //
  rgrProd.Visible := btsAccProd.Checked[CurIndex];
  if rgrProd.Visible then
  begin
    rgrProd.Caption := '��� ������� '+btsAccProd.Items[CurIndex];
    // ���������� ��������
    l:=1;
    if(dm.taMOLACCPROD_D.AsInteger and (l shl CurIndex) = 0)
      then rgrProd.ItemIndex := 0
    else rgrProd.ItemIndex := 1;
  end;
end;

procedure TfmAccess.btsAccProdClickCheck(Sender: TObject);
begin
  rgrProd.Visible := btsAccProd.Checked[btsAccProd.ItemIndex];
end;

initialization
  FIO := '';

end.







