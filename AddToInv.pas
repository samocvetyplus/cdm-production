unit AddToInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGridEh, DBCtrls, DB, FIBDataSet, pFIBDataSet, StdCtrls, Mask,
  DBCtrlsEh, DBLookupEh, Buttons;

type
  TfmAddToInv = class(TForm)
    lNum: TLabel;
    lDate: TLabel;
    lDep: TLabel;
    lKlad: TLabel;
    lNDS: TLabel;
    AddButton: TBitBtn;
    CancelButton: TBitBtn;
    NumCBox: TDBLookupComboboxEh;
    taINV: TpFIBDataSet;
    dsrINV: TDataSource;
    tDate: TDBText;
    tDep: TDBText;
    tKlad: TDBText;
    tNDS: TDBText;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmAddToInv: TfmAddToInv;


implementation

{$R *.dfm}
uses MainData, DictData, dbUtil, fmUtils, ApplData, ADistr, InvData, ProductionConsts,
SOList, SOEl;



procedure TfmAddToInv.FormCreate(Sender: TObject);
begin
taInv.Active:=false;
taInv.ApplyUpdates;
taInv.Active:=true;
end;

end.
