{****************************************}
{  ������������ ���������                }
{  Copyrigth (C) 2003 basile for CDM     }
{  v0.50                                 }
{****************************************}
unit VSheet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus,  ImgList, Grids, DBGridEh,
  RxToolEdit, RXDBCtrl, StdCtrls, Mask, DBCtrls, ExtCtrls, ComCtrls,
  DBCtrlsEh, TB2Item, TB2Dock, TB2Toolbar, ActnList, DB, pFIBDataSet,
  FIBDataSet, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmVSheet = class(TfmDocAncestor)
    TBDock1: TTBDock;
    TBToolbar: TTBToolbar;
    TBControlItem3: TTBControlItem;
    TBControlItem4: TTBControlItem;
    TBControlItem5: TTBControlItem;
    TBControlItem6: TTBControlItem;
    TBControlItem7: TTBControlItem;
    TBControlItem8: TTBControlItem;
    TBSeparatorItem4: TTBSeparatorItem;
    TBItem5: TTBItem;
    lbOper: TLabel;
    lbSemis: TLabel;
    Label1: TLabel;
    cmbxOper: TDBComboBoxEh;
    cmbxSemis: TDBComboBoxEh;
    cmbxMat: TDBComboBoxEh;
    acEvent: TActionList;
    acCopyQToFQ: TAction;
    ppProtocol: TTBPopupMenu;
    ppitDetailSemis: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem4: TTBItem;
    TBItem2: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    TBItem3: TTBItem;
    acDetailSemis: TAction;
    spitDetail: TSpeedItem;
    acPrintVReport: TAction;
    acExcel: TAction;
    acReCalc: TAction;
    acPrintSlVReport: TAction;
    itZero: TTBItem;
    acZero: TAction;
    SpeedItem1: TSpeedItem;
    TBItem1: TTBItem;
    acAssort: TAction;
    TBSeparatorItem2: TTBSeparatorItem;
    acCopyQAtoFQA: TAction;
    TBItem7: TTBItem;
    taTmpWhSemis: TpFIBDataSet;
    taTmpWhSemisDEPID: TIntegerField;
    taTmpWhSemisSEMISID: TFIBStringField;
    taTmpWhSemisOPERID: TFIBStringField;
    taTmpWhSemisQ: TIntegerField;
    taTmpWhSemisW: TFloatField;
    taTmpWhSemisUQ: TSmallintField;
    taTmpWhSemisUW: TSmallintField;
    taTmpWhArt: TpFIBDataSet;
    taTmpWhArtPSID: TIntegerField;
    taTmpWhArtOPERID: TFIBStringField;
    taTmpWhArtART2ID: TIntegerField;
    taTmpWhArtSZ: TIntegerField;
    taTmpWhArtT: TSmallintField;
    taTmpWhArtQ: TIntegerField;
    taTmpWhArtU: TSmallintField;
    acAdd: TAction;
    acDel: TAction;
    TBItem6: TTBItem;
    TBItem8: TTBItem;
    TBItem9: TTBItem;
    acPrintAssort: TAction;
    tbiCopyOutToFact: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure cmbxOperChange(Sender: TObject);
    procedure cmbxSemisChange(Sender: TObject);
    procedure cmbxMatChange(Sender: TObject);
    procedure acCopyQToFQExecute(Sender: TObject);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acDetailSemisExecute(Sender: TObject);
    procedure acPrintVReportExecute(Sender: TObject);
    procedure acExcelExecute(Sender: TObject);
    procedure acReCalcExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPrintSlVReportExecute(Sender: TObject);
    procedure acZeroExecute(Sender: TObject);
    procedure acAssortExecute(Sender: TObject);
    procedure acCopyQAtoFQAExecute(Sender: TObject);
    procedure spitCloseClick(Sender: TObject);
    procedure taTmpWhSemisBeforeOpen(DataSet: TDataSet);
    procedure taTmpWhArtBeforeOpen(DataSet: TDataSet);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure gridItemColumns0UpdateData(Sender: TObject; var Text: String;
      var Value: Variant; var UseText, Handled: Boolean);
    procedure acPrintAssortExecute(Sender: TObject);
    procedure tbiCopyOutToFactClick(Sender: TObject);
  private
    procedure Access; 
  end;

var
  fmVSheet: TfmVSheet;

implementation

uses MainData, InvData, dbUtil, dbTree, ProtWhItem, PrintData, fmUtils,
  DictData, m207Export, VArt, MsgDialog, DateUtils,   ProductionConsts;

{$R *.dfm}

procedure TfmVSheet.FormCreate(Sender: TObject);
begin
  gridItem.DataSource := dmInv.dsrSemisReport;
  edNoDoc.DataSource := dmMain.dsrVList;
  dtedDateDoc.DataSource := dmMain.dsrVList;
  Access;
  OpenDataSets([dm.taOper, dm.taSemis]);
  cmbxOper.OnChange := nil;
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.Items.Insert(1, sNoOperation);
  cmbxOper.ItemIndex := 0;
  dmInv.CurrOper := cmbxOper.Items[cmbxOper.ItemIndex];
  dmMain.FilterOperId := ROOT_OPER_CODE;
  cmbxOper.OnChange := cmbxOperChange;

  cmbxSemis.OnChange := nil;
  cmbxSemis.Items.Assign(dm.dSemis);
  cmbxSemis.ItemIndex:=0;
  dmInv.CurrSemis := cmbxSemis.Items[cmbxSemis.ItemIndex];
  cmbxSemis.OnChange := cmbxSemisChange;

  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMat);
  cmbxMat.ItemIndex := 0;
  dmMain.FilterMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := cmbxMatChange;

  FDocName := '���������';

  inherited;

  Caption := '������������ ���������.'+dmMain.taVListDEPNAME.AsString;

  InitBtn(Boolean(dmMain.taVListISCLOSE.AsInteger));

  //ppPrint.Skin := dm.ppSkin;
end;

procedure TfmVSheet.cmbxOperChange(Sender: TObject);
begin
  with cmbxOper, Items, dmInv do
  begin
    PostDataSet(taSemisReport);
    CurrOper := cmbxOper.Items[cmbxOper.itemIndex];
    if Items[ItemIndex] = sNoOperation then dmMain.FilterOperId := '���'
    else dmMain.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
    ReOpenDataSet(taSemisReport);
  end
end;

procedure TfmVSheet.cmbxSemisChange(Sender: TObject);
begin

  with dmInv do
  begin

    PostDataSet(taSemisReport);

    CurrSemis:=cmbxSemis.Items[cmbxSemis.itemIndex];

    ReOpenDataSet(taSemisReport);

  end
end;

procedure TfmVSheet.cmbxMatChange(Sender: TObject);
begin
  PostDataSet(dmInv.taSemisReport);
  dmMain.FilterMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSet(dmInv.taSemisReport);
end;

procedure TfmVSheet.acCopyQToFQExecute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  with dmInv do
  begin
    Bookmark := taSemisReport.GetBookmark;
    taSemisReport.DisableControls;
    try
      taSemisReport.First;
      while not taSemisReport.Eof do
      begin
        taSemisReport.Edit;
        taSemisReportFQ.AsFloat := taSemisReportREST_OUT_Q.AsFloat;
        taSemisReportFW.AsFloat := taSemisReportREST_OUT_W.AsFloat;
        taSemisReport.Post;
        taSemisReport.Next;
      end;
    finally
      taSemisReport.EnableControls;
      taSemisReport.GotoBookmark(Bookmark);
    end;
  end;
end;

procedure TfmVSheet.gridItemGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if(Column.FieldName = 'SEMIS')or(Column.FieldName = 'OPERATION')or
    (Column.FieldName = 'FQ')or(Column.FieldName = 'FW') then Background := clWhite;
end;

procedure TfmVSheet.acDetailSemisExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmProtWhItem, TForm(fmProtWhItem));
end;

procedure TfmVSheet.acPrintVReportExecute(Sender: TObject);
begin
  PostDataSets([dmInv.taSemisReport, dmMain.taVList]);
  dmPrint.PrintDocumentA(dkSemisReport, VarArrayOf([VarArrayOf([dmMain.taVListID.AsInteger]),
     VarArrayOf([dmMain.taVListID.AsInteger]),
     VarArrayOf([dmMain.taVListID.AsInteger])]));
end;

procedure TfmVSheet.acExcelExecute(Sender: TObject);
var
  f, c : string;
begin
  c := '������������ ��������� '+dmMain.taVListDOCNO.AsString+'  �� �������� '+dmMain.taVListDEPNAME.AsString;
  f := ExtractFileDir(ParamStr(0))+'\'+c+'.xls';
  GridToXLS(gridItem, f, c, True, False);
end;

procedure TfmVSheet.acReCalcExecute(Sender: TObject);
begin
  Screen.Cursor:=crSQLWait;
  PostDataSets([dmMain.taVList, dmInv.taSemisReport]);
  with dmMain do
  try
    case MasterDataSet.FieldByName('BUH').AsInteger of
      0 : CreateV_for_Wh(taVListID.AsString);
      1 : CreateV_for_Buh(taVListID.AsString);
    end;
    ReOpenDataSet(dmInv.taSemisReport);
    dmMain.taVList.Edit;
    dmMain.taVListCREATED.AsDateTime := Now;
    dmMain.taVList.Post;
  finally
    Screen.Cursor:=crDefault;
  end;
  MessageDialog('�������� �� ���������� � ������������ �����������', mtInformation, [mbOk], 0);
end;

procedure TfmVSheet.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmMain.FilterMatId := ROOT_MAT_CODE;
  inherited;
  CloseDataSets([dm.taSemis, dm.taOper]);
end;

procedure TfmVSheet.acPrintSlVReportExecute(Sender: TObject);
begin
  PostDataSets([dmInv.taSemisReport, dmMain.taVList]);
  dmPrint.PrintDocumentA(dkVSheet, VarArrayOf([VarArrayOf([dmMain.taVListID.AsInteger]),
     VarArrayOf([dmMain.taVListID.AsInteger]),
     VarArrayOf([dmMain.taVListID.AsInteger]) ]));
end;


procedure TfmVSheet.acAssortExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmVArt, TForm(fmVArt));
end;

procedure TfmVSheet.acCopyQAtoFQAExecute(Sender: TObject);
begin
  if MessageDialog('��������� �������� ����������� �������� ���-�� "������� ���-��" � "����������� ���-��"?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then eXit;
  Screen.Cursor := crSQLWait;
  try
    ExecSQL('update AIn_Art set FQ=Q where VItemId in (select Id from Semis_Report_Dep where Act_Id='+dmMain.taVListID.AsString+')', dmMain.quTmp);
    ExecSQL('execute procedure Semis_Report_UpdA('+dmMain.taVListID.AsString+')', dmMain.quTmp);
    ReOpenDataSet(dmInv.taSemisReport);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmVSheet.spitCloseClick(Sender: TObject);
var
  OperId : string;
  v : Variant;
begin
  if(dmMain.taVListBUH.AsInteger = 1)then
    raise EWarning.Create('��������� �� ����� ��������� ������������ ���������');
  // ��������� ���� ������ ������ ��������� ��������� � ������ ��������� �������� ���������
  if(dmMain.taVListISCLOSE.AsInteger = 0)then
  begin
    v := ExecSelectSQL('select max(DocDate) from Act where T=2 and IsClose = 1 and DepId ='+dmMain.taVListDEPID.AsString, dmMain.quTmp);
    if not VarIsNull(v) then
      if StartOfTheDay(VarToDateTime(v)+1)<>StartOfTheDay(dmMain.taVListBD.AsDateTime) then raise Exception.Create(rsInvalidPeriod);
    end;
  inherited;
  if(dmMain.taVListISCLOSE.AsInteger = 1)then
  begin
    //MessageDialog('������ ����� ����������� ������� �� ���������� � '+#13+'������������ ��� '+dmMain.taVListDEPNAME.AsString+'. ', mtInformation, [mbYes], 0);
    Screen.Cursor := crSQLWait;
    try
      ExecSQL('execute procedure Build_WhSemis_Dep('+dmMain.taVListDEPID.AsString+')', dmMain.quTmp);
      ExecSQL('delete from WhSemis where DepId='+dmMain.taVListDEPID.AsString, dmMain.quTmp);
      ReOpenDataSet(taTmpWhSemis);
      while not taTmpWhSemis.Eof do
      begin
        if taTmpWhSemisOPERID.IsNull then OperId := 'null' else OperId := '"'+taTmpWhSemisOPERID.AsString+'"';
        ExecSQL('insert into WhSemis(ID, DEPID, SEMISID, OPERID, Q, W, UQ, UW, SELFCOMPID) '+
          ' values(gen_id(gen_whsemis_id, 1), '+taTmpWhSemisDEPID.AsString+', "'+
          taTmpWhSemisSEMISID.AsString+'", '+OperId+', '+
          StringReplace(taTmpWhSemisQ.AsString, ',', '.', [rfReplaceAll])+', '+
          StringReplace(taTmpWhSemisW.AsString,',', '.', [rfReplaceAll])+', '+
          taTmpWhSemisUQ.AsString+', '+taTmpWhSemisUW.AsString+','+IntToStr(dm.User.SelfCompId)+')', dmMain.quTmp);
        taTmpWhSemis.Next;
      end;
      CloseDataSet(taTmpWhSemis);
      ExecSQL('execute procedure Build_WhArt_Dep('+dmMain.taVListDEPID.AsString+')', dmMain.quTmp);
      ExecSQL('delete from WhAppl where PsId='+dmMain.taVListDEPID.AsString, dmMain.quTmp);
      ReOpenDataSet(taTmpWhArt);
      while not taTmpWhArt.Eof do
      begin
        ExecSQL('insert into WhAppl(ID, PSID, OPERID, ART2ID, SZ, T, Q, U, SELFCOMPID)'+
          ' values(gen_id(gen_whappl_id, 1), '+taTmpWhArtPSID.AsString+' , "'+taTmpWhArtOPERID.AsString+'", '+
          taTmpWhArtART2ID.AsString+','+taTmpWhArtSZ.AsString+',0,'+taTmpWhArtQ.AsString+','+
          taTmpWhArtU.AsString+','+IntToStr(dm.User.SelfCompId)+')', dmMain.quTmp);
          taTmpWhArt.Next;
      end;
      CloseDataSet(taTmpWhArt);
      MessageDialog('������� ���������� � ������������ ��� '+dmMain.taVListDEPNAME.AsString+' �����������.', mtInformation, [mbOk], 0);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TfmVSheet.taTmpWhSemisBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['DEPID'].AsInteger := dmMain.taVListDEPID.AsInteger;
end;

procedure TfmVSheet.taTmpWhArtBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['DEPID'].AsInteger := dmMain.taVListDEPID.AsInteger;
end;

procedure TfmVSheet.Access;
begin
  if dm.IsAdm then eXit;
  if(dm.User.Profile = 1)then spitClose.Enabled := True
  else spitClose.Enabled := False;
  if((dm.User.Profile = 1)or(dm.User.Profile = -1))then
  begin
    gridItem.ReadOnly := False;
    acCopyQToFQ.Enabled := True;
    acCopyQAtoFQA.Enabled := True;
  end
  else begin
    gridItem.ReadOnly := True;
    acCopyQToFQ.Enabled := False;
    acCopyQAtoFQA.Enabled := False;
  end;
end;

procedure TfmVSheet.acAddExecute(Sender: TObject);
begin
  DataSet.Insert;
  ActiveControl := gridItem;  
end;

procedure TfmVSheet.acDelExecute(Sender: TObject);
begin
  DataSet.Delete;
end;

procedure TfmVSheet.gridItemColumns0UpdateData(Sender: TObject;
  var Text: String; var Value: Variant; var UseText, Handled: Boolean);
begin
  with dmInv do
  begin
    taSemisReportUQ.AsInteger := ExecSelectSQL('select UQ from D_Semis where SemisId="'+Value+'"', dmMain.quTmp);
    taSemisReportUW.AsInteger := ExecSelectSQL('select UW from D_Semis where SemisId="'+Value+'"', dmMain.quTmp);
    taSemisReportUQ_NAME.AsString := ExecSelectSQL('select U from D_Unit where UnitId='+taSemisReportUQ.AsString, dmMain.quTmp);
    taSemisReportUW_NAME.AsString := ExecSelectSQL('select U from D_Unit where UnitId='+taSemisReportUW.AsString, dmMain.quTmp);
  end;
end;

procedure TfmVSheet.acPrintAssortExecute(Sender: TObject);
begin
  PostDataSets([dmInv.taSemisReport, dmMain.taVList]);
  dmPrint.PrintDocumentA(dkVSheetAssort, VarArrayOf([VarArrayOf([dmMain.taVListID.AsInteger])]));
end;

procedure TfmVSheet.tbiCopyOutToFactClick(Sender: TObject);
begin
  if MessageDialog('��������� �������� �� ����������� ���������� ���� � ���-�� � �����������?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then eXit;
  with dmInv do
  begin
    Screen.Cursor := crSQLWait;
    try
      ExecSQL('Update SEMIS_REPORT_DEP set fq = rest_out_q, fw = rest_out_w where act_id = ' + IntToStr(dmMain.taVListID.AsInteger), dmMain.quTmp);
      ReOpenDataSet(dmInv.taSemisReport);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;


procedure TfmVSheet.acZeroExecute(Sender: TObject);
begin
  if MessageDialog('�������� ����������� ��� � ���-�� ?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then eXit;
  with dmInv do
  begin
    Screen.Cursor := crSQLWait;
    try
      ExecSQL('Update SEMIS_REPORT_DEP set fq = 0, fw = 0 where act_id = ' + IntToStr(dmMain.taVListID.AsInteger), dmMain.quTmp);
      ReOpenDataSet(dmInv.taSemisReport);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;




end.
