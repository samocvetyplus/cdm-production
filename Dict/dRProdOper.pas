unit dRProdOper;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, Placemnt, ImgList, Grids, DBGrids, RXDBCtrl,
  SpeedBar, ExtCtrls, ComCtrls, StdCtrls, RXSpin,
  ActnList, DBGridEh, Buttons;

type
  TfmRProdOper = class(TfmDictAncestor)
    SpeedBar1: TSpeedBar;
    lbYear: TLabel;
    edYear: TRxSpinEdit;
    lbMonth: TLabel;
    cmbxMonth: TComboBox;
    spitOper: TSpeedItem;
    procedure cmbxMonthChange(Sender: TObject);
    procedure edYearChange(Sender: TObject);
    procedure spitOperClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRProdOper: TfmRProdOper;

implementation

uses DictData, dbUtil;

{$R *.dfm}

procedure TfmRProdOper.cmbxMonthChange(Sender: TObject);
begin
//  ReOpenDataSets([dm.taRProd]);
end;

procedure TfmRProdOper.edYearChange(Sender: TObject);
begin
//  ReOpenDataSets([dm.taRProd]);
end;

procedure TfmRProdOper.spitOperClick(Sender: TObject);
begin
  with dm.quTmp do
  begin
    if Transaction.Active then Transaction.Commit;
    Transaction.StartTransaction;
    SQL.Text := 'execute procedure Do_Oper_for_RProd('+IntToStr(edYear.AsInteger)+', '+
      IntToStr(cmbxMonth.ItemIndex+1)+')';
    ExecQuery;
    Transaction.Commit;
//    ReOpenDataSets([dm.taRProd]);
  end
end;

end.
