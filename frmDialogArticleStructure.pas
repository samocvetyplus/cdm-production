unit frmDialogArticleStructure;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxLookAndFeelPainters, cxContainer,
  cxLabel, StdCtrls, cxButtons, ExtCtrls, ActnList, ImgList, dxBar,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  FIBDataSet, pFIBDataSet, cxDBLookupComboBox, cxTextEdit, FIBQuery,
  pFIBQuery, dxmdaset, pFIBStoredProc, Menus, cxLookAndFeels;

type
  TDialogArticleStructure = class(TForm)
    ViewMask: TcxGridDBTableView;
    GridMaskLevel1: TcxGridLevel;
    GridMask: TcxGrid;
    GridArticle: TcxGrid;
    ViewArticle: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    dxBarManager1: TdxBarManager;
    dxBarDockControl1: TdxBarDockControl;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    ActionList1: TActionList;
    ImageList1: TImageList;
    ActionArticleInsert: TAction;
    ActionArticleDelete: TAction;
    ActionMaskCopy: TAction;
    ActionMaskInsert: TAction;
    ActionMaskDelete: TAction;
    dxBarDockControl2: TdxBarDockControl;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    ButtonMaskCopy: TdxBarButton;
    Panel1: TPanel;
    Bevel1: TBevel;
    ButtonClose: TcxButton;
    _DataSetArticles: TpFIBDataSet;
    DataSetMasks: TpFIBDataSet;
    DataSetMaskArticles: TpFIBDataSet;
    _DataSetArticlesARTICLE: TFIBStringField;
    DataSetMasksID: TFIBIntegerField;
    DataSetMasksMASK: TFIBStringField;
    DataSetMaskArticlesMASKID: TFIBIntegerField;
    DataSetMaskArticlesARTICLEID: TFIBIntegerField;
    DataSetMaskArticlesArticle: TStringField;
    DataSourceMasks: TDataSource;
    DataSourceMaskArticles: TDataSource;
    ViewMaskMASK: TcxGridDBColumn;
    _DataSetArticlesARTICLEID: TFIBIntegerField;
    ViewArticleArticle: TcxGridDBColumn;
    DataSetMaskArticlesQUANTITY: TFIBIntegerField;
    ViewArticleQuantity: TcxGridDBColumn;
    ActionMaskPost: TAction;
    ActionMaskCancel: TAction;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    ActionArticlePost: TAction;
    ActionArticleCancel: TAction;
    dxBarButton9: TdxBarButton;
    dxBarButton10: TdxBarButton;
    ActionMaskSearch: TAction;
    EditorMaskSearch: TdxBarEdit;
    dxBarButton11: TdxBarButton;
    DataSetMaskValidate: TpFIBDataSet;
    DataSetMaskValidateMASK: TFIBStringField;
    DataSetArticles: TdxMemData;
    SizeEditor: TdxBarCombo;
    DataSetSize: TpFIBDataSet;
    DataSetMaskSize: TpFIBDataSet;
    DataSetSizeID: TFIBIntegerField;
    DataSetSizeTITLE: TFIBStringField;
    DataSetMaskArticlesSIZEID: TFIBIntegerField;
    ActionSizeCopy: TAction;
    ButtonSizeCopy: TdxBarButton;
    DataSetMaskArticlesID: TFIBIntegerField;
    DataSetMaskSizeSIZEID: TFIBIntegerField;
    DataSetMaskSizeSIZETITLE: TFIBStringField;
    ProcedureMaskCopy: TpFIBStoredProc;
    ProcedureSizeCopy: TpFIBStoredProc;
    ProcedureClearGarbage: TpFIBStoredProc;
    Styles: TcxStyleRepository;
    StyleDisabled: TcxStyle;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DataSourceMasksDataChange(Sender: TObject; Field: TField);
    procedure DataSetMaskArticlesBeforeOpen(DataSet: TDataSet);
    procedure DataSetMaskArticlesNewRecord(DataSet: TDataSet);
    procedure ActionArticleInsertUpdate(Sender: TObject);
    procedure ActionArticleDeleteUpdate(Sender: TObject);
    procedure ActionMaskCopyUpdate(Sender: TObject);
    procedure ActionMaskInsertUpdate(Sender: TObject);
    procedure ActionMaskDeleteUpdate(Sender: TObject);
    procedure ActionMaskPostUpdate(Sender: TObject);
    procedure ActionMaskCancelUpdate(Sender: TObject);
    procedure ActionArticlePostUpdate(Sender: TObject);
    procedure ActionArticleCancelUpdate(Sender: TObject);
    procedure ActionArticleInsertExecute(Sender: TObject);
    procedure ActionArticleDeleteExecute(Sender: TObject);
    procedure ActionArticlePostExecute(Sender: TObject);
    procedure ActionArticleCancelExecute(Sender: TObject);
    procedure ActionMaskCopyExecute(Sender: TObject);
    procedure ActionMaskInsertExecute(Sender: TObject);
    procedure ActionMaskDeleteExecute(Sender: TObject);
    procedure ActionMaskPostExecute(Sender: TObject);
    procedure ActionMaskCancelExecute(Sender: TObject);
    procedure DataSetMasksBeforeEdit(DataSet: TDataSet);
    procedure DataSetMasksBeforeInsert(DataSet: TDataSet);
    procedure DataSetMasksAfterPost(DataSet: TDataSet);
    procedure DataSetMasksAfterCancel(DataSet: TDataSet);
    procedure DataSetMaskArticlesBeforeEdit(DataSet: TDataSet);
    procedure DataSetMaskArticlesBeforeInsert(DataSet: TDataSet);
    procedure DataSetMaskArticlesAfterCancel(DataSet: TDataSet);
    procedure DataSetMaskArticlesAfterPost(DataSet: TDataSet);
    procedure EditorMaskSearchKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ActionMaskSearchUpdate(Sender: TObject);
    procedure ActionMaskSearchExecute(Sender: TObject);
    procedure DataSetMasksAfterDelete(DataSet: TDataSet);
    procedure DataSetMaskArticlesAfterDelete(DataSet: TDataSet);
    procedure DataSetMasksBeforePost(DataSet: TDataSet);
    procedure DataSetMaskValidateBeforeOpen(DataSet: TDataSet);
    procedure DataSetMaskArticlesBeforePost(DataSet: TDataSet);
    procedure ViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure ViewMaskKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ViewArticleKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SizeEditorChange(Sender: TObject);
    procedure ActionSizeCopyUpdate(Sender: TObject);
    procedure ActionSizeCopyExecute(Sender: TObject);
    procedure SizeEditorDropDown(Sender: TObject);
    procedure DataSetMaskSizeBeforeOpen(DataSet: TDataSet);
    procedure SizeEditorExit(Sender: TObject);
  private
    Buffer: TStringList;
    Size: string;
    Article: string;
    StoredMask: string;
    CurrentSizeID: Integer;
    CurrentSizeTitle: string;
    CurrentMaskID: Integer;
    BufferSourceMask: string;    
    BufferSourceMaskID: Integer;
    BufferTargetMaskID: Integer;
    SizeCoping: Boolean;
    procedure InitilizeSizeEditor;
    procedure InternalExecute; overload;
  public
    class procedure Execute(Article: string = ''; Size: string = '-'); overload;
  end;

implementation

uses DictData, dbUtil, uUtils, Main;

{$R *.dfm}

var
  DialogArticleStructure: TDialogArticleStructure;

procedure TDialogArticleStructure.InternalExecute;
begin
  if Article <> '' then
  begin
    EditorMaskSearch.Text := Article;
    if ActionMaskSearch.Enabled then
    ActionMaskSearch.Execute;
    Size := '-';
  end;
  DialogArticleStructure.ShowModal;
  ProcedureClearGarbage.ExecProc;
  ProcedureClearGarbage.Transaction.CommitRetaining;
end;

class procedure TDialogArticleStructure.Execute(Article: string; Size: string);
begin
  DialogArticleStructure := TDialogArticleStructure.Create(nil);
  DialogArticleStructure.Article := Article;
  DialogArticleStructure.Size := Size;
  DialogArticleStructure.InternalExecute;
  DialogArticleStructure.Free;
end;

procedure TDialogArticleStructure.FormCreate(Sender: TObject);
begin
  DataSetSize.Active := True;
  DataSetSize.Locate('ID', -1, []);

  _DataSetArticles.Active := True;
  DataSetArticles.LoadFromDataSet(_DataSetArticles);
  DataSetArticles.Indexes.Add.FieldName := 'ARTICLE$ID';
  DataSetArticles.Indexes.Add.FieldName := 'ARTICLE';
  DataSetArticles.SortedField := 'ARTICLE';

  _DataSetArticles.Active := False;

  CurrentMaskID := MaxInt;
  DataSetMasks.Active := True;
  BufferSourceMaskID := MaxInt;
  BufferTargetMaskID := MaxInt;
end;

procedure TDialogArticleStructure.FormDestroy(Sender: TObject);
begin
  DataSetMasks.Active := False;
  DataSetArticles.Active := False;
end;

procedure TDialogArticleStructure.DataSourceMasksDataChange(Sender: TObject; Field: TField);
var
  MaskID: Integer;
begin
  if DataSetMasks.Active and (Field = nil) then
  begin
    if (DataSetMasks.State in [dsBrowse, dsInsert]) then
    begin
      MaskID := DataSetMasksID.AsInteger;
      if MaskID <> CurrentMaskID then
      begin
        CurrentMaskID := MaskID;
        InitilizeSizeEditor;
        DataSetMaskArticles.Active := False;
        DataSetMaskArticles.Active := True;
      end;
    end;
  end;
end;

procedure TDialogArticleStructure.DataSetMaskArticlesBeforeOpen(DataSet: TDataSet);
begin
  DataSetMaskArticles.ParamByName('mask$id').AsInteger := CurrentMaskID;
  DataSetMaskArticles.ParamByName('size$id').AsInteger := CurrentSizeID;  
end;

procedure TDialogArticleStructure.DataSetMaskArticlesNewRecord(
  DataSet: TDataSet);
begin
  DataSetMaskArticlesMASKID.AsInteger := DataSetMasksID.AsInteger;
  DataSetMaskArticlesSIZEID.AsInteger := CurrentSizeID;
  DataSetMaskArticlesQUANTITY.AsInteger := 1;
end;

procedure TDialogArticleStructure.ActionArticleInsertUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := DataSetMasks.Active and (DataSetMasks.State = dsBrowse) and (DataSetMasksID.AsInteger <> 0);
  if Enabled then
  Enabled := DataSetMaskArticles.Active and (DataSetMaskArticles.State = dsBrowse);

  ActionArticleInsert.Enabled := Enabled;
end;

procedure TDialogArticleStructure.ActionArticleDeleteUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin

  Enabled := DataSetMasks.Active and (DataSetMasks.State = dsBrowse) and (DataSetMasksID.AsInteger <> 0);

  if Enabled then
  Enabled := DataSetMaskArticles.Active and (DataSetMaskArticles.State = dsBrowse) and (DataSetMaskArticlesMaskID.AsInteger <> 0);

  ActionArticleDelete.Enabled := Enabled;
end;

procedure TDialogArticleStructure.ActionArticlePostUpdate(Sender: TObject);
begin
  ActionArticlePost.Enabled := DataSetMaskArticles.Active and (DataSetMaskArticles.State <> dsBrowse);
end;

procedure TDialogArticleStructure.ActionArticleCancelUpdate(Sender: TObject);
begin
  ActionArticleCancel.Enabled := DataSetMaskArticles.Active and (DataSetMaskArticles.State <> dsBrowse);
end;

procedure TDialogArticleStructure.ActionMaskInsertUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := DataSetMaskArticles.Active and (DataSetMaskArticles.State = dsBrowse);

  if Enabled then
  Enabled := DataSetMasks.Active and (DataSetMasks.State = dsBrowse);

  ActionMaskInsert.Enabled := Enabled;
end;

procedure TDialogArticleStructure.ActionMaskDeleteUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := DataSetMaskArticles.Active and (DataSetMaskArticles.State = dsBrowse);

  if Enabled then
  Enabled := DataSetMasks.Active and (DataSetMasks.State = dsBrowse) and (DataSetMasksID.AsInteger <> 0);

  ActionMaskDelete.Enabled := Enabled;
end;

procedure TDialogArticleStructure.ActionMaskPostUpdate(Sender: TObject);
begin
  ActionMaskPost.Enabled := DataSetMasks.Active and (DataSetMasks.State <> dsBrowse);
end;

procedure TDialogArticleStructure.ActionMaskCancelUpdate(Sender: TObject);
begin
  ActionMaskCancel.Enabled := DataSetMasks.Active and (DataSetMasks.State <> dsBrowse);
end;


procedure TDialogArticleStructure.ActionArticleInsertExecute(Sender: TObject);
begin
  DataSetMaskArticles.Insert;
end;

procedure TDialogArticleStructure.ActionArticleDeleteExecute(Sender: TObject);
begin
  if QuestionYesNoMessage('������� �������.') then
  DataSetMaskArticles.Delete;
end;

procedure TDialogArticleStructure.ActionArticlePostExecute(Sender: TObject);
begin
  DataSetMaskArticles.Post;
end;

procedure TDialogArticleStructure.ActionArticleCancelExecute(Sender: TObject);
begin
  DataSetMaskArticles.Cancel;
end;


procedure TDialogArticleStructure.ActionMaskInsertExecute(Sender: TObject);
begin
  GridMask.SetFocus;
  ViewMask.DataController.Insert;
  ViewMaskMASK.Editing := True;
end;

procedure TDialogArticleStructure.ActionMaskDeleteExecute(Sender: TObject);
begin
  if QuestionYesNoMessage('������� �����.') then
  DataSetMasks.Delete;
end;

procedure TDialogArticleStructure.ActionMaskPostExecute(Sender: TObject);
begin
  DataSetMasks.Post;
end;

procedure TDialogArticleStructure.ActionMaskCancelExecute(Sender: TObject);
begin
  DataSetMasks.Cancel;
end;

procedure TDialogArticleStructure.DataSetMasksBeforeEdit(DataSet: TDataSet);
begin
  StoredMask := Trim(DataSetMasksMASK.AsString);
  ViewArticle.Styles.Content := StyleDisabled;
  GridArticle.Enabled := False;
end;

procedure TDialogArticleStructure.DataSetMasksBeforeInsert(DataSet: TDataSet);
begin
  if not ActionMaskInsert.Enabled then Abort;
  StoredMask := '';
  ViewArticle.Styles.Content := StyleDisabled;  
  GridArticle.Enabled := False;
end;

procedure TDialogArticleStructure.DataSetMasksAfterPost(DataSet: TDataSet);
var
  ABufferSourceMaskID: Integer;
  ABufferTargetMaskID: Integer;
begin
  ViewArticle.Styles.Content := nil;
  GridArticle.Enabled := True;
  DataSetMasks.Transaction.CommitRetaining;
  if (BufferSourceMaskID <> MaxInt) and (BufferTargetMaskID <> MaxInt) then
  begin
    ABufferSourceMaskID := BufferSourceMaskID;
    ABufferTargetMaskID := BufferTargetMaskID;
    BufferSourceMaskID := MaxInt;
    BufferTargetMaskID := MaxInt;
    try
      ProcedureMaskCopy.ParamByName('Source$Mask$ID').AsInteger := ABufferSourceMaskID;
      ProcedureMaskCopy.ParamByName('Target$Mask$ID').AsInteger := ABufferTargetMaskID;
      ProcedureMaskCopy.ExecProc;
      ProcedureMaskCopy.Transaction.CommitRetaining;
      InitilizeSizeEditor;
      DataSetMaskArticles.Active := False;
      DataSetMaskArticles.Active := True;
    finally
    
    end;
  end;
  ButtonMaskCopy.Down := False;
end;

procedure TDialogArticleStructure.DataSetMasksAfterCancel(DataSet: TDataSet);
begin
  BufferSourceMaskID := MaxInt;
  BufferTargetMaskID := MaxInt;
  ButtonMaskCopy.Down := False;
  ViewArticle.Styles.Content := nil;    
  GridArticle.Enabled := True;
end;

procedure TDialogArticleStructure.DataSetMaskArticlesBeforeEdit(DataSet: TDataSet);
begin
  ViewMask.Styles.Content := StyleDisabled;
  GridMask.Enabled := False;
end;

procedure TDialogArticleStructure.DataSetMaskArticlesBeforeInsert(DataSet: TDataSet);
begin
  if not ActionArticleInsert.Enabled then Abort;
  ViewMask.Styles.Content := StyleDisabled;
  GridMask.Enabled := False;
  ViewArticleArticle.Options.Editing := True;
  ViewArticleArticle.Options.ShowEditButtons := isebDefault;
end;

procedure TDialogArticleStructure.DataSetMaskArticlesAfterCancel(DataSet: TDataSet);
begin
  ViewMask.Styles.Content := nil;
  GridMask.Enabled := True;
  ViewArticleArticle.Options.Editing := False;
  ViewArticleArticle.Options.ShowEditButtons := isebNever;
end;

procedure TDialogArticleStructure.DataSetMaskArticlesAfterPost(DataSet: TDataSet);
begin
  ViewMask.Styles.Content := nil;
  GridMask.Enabled := True;
  ViewArticleArticle.Options.Editing := False;
  ViewArticleArticle.Options.ShowEditButtons := isebNever;
  DataSetMaskArticles.Transaction.CommitRetaining;
end;

procedure TDialogArticleStructure.EditorMaskSearchKeyDown(Sender: TObject;  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    if ActionMaskSearch.Enabled then
    ActionMaskSearch.Execute;
  end;
end;

procedure TDialogArticleStructure.ActionMaskSearchUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := DataSetMaskArticles.Active and (DataSetMaskArticles.State = dsBrowse);

  if Enabled then
  Enabled := DataSetMasks.Active and (DataSetMasks.State = dsBrowse);

  ActionMaskSearch.Enabled := Enabled;
  SizeEditor.Enabled := Enabled;
end;

procedure TDialogArticleStructure.ActionMaskSearchExecute(Sender: TObject);
var
  MaskID: Variant;
  Article: string;
  Index: Integer;
begin
  Article := EditorMaskSearch.Text;
  MaskID := ExecSelectSQL('select id from article$mask where ' + '''' + Article + '''' + ' like mask', dm.quTmp);
  if VarIsNull(MaskID) then
  begin
   if QuestionYesNoMessage('����� �� �������.'#13#10'��������.') then
   begin
      DataSetMasks.Insert;
      DataSetMasksMASK.AsString := Article;
      DataSetMasks.Post;
   end;
  end
  else
  begin
    DataSetMasks.DisableControls;
    DataSetMasks.First;
    if not DataSetMasks.Locate('ID', MaskID, []) then
    begin
      DataSetMasks.Active := False;
      DataSetMasks.Active := True;
      DataSetMasks.First;
      if DataSetMasks.Locate('ID', MaskID, []) then
      begin
        DataSetMasks.EnableControls;
        if Size <> '-' then
        begin
          Index := SizeEditor.Items.IndexOf(Size);
          if Index <> -1 then SizeEditor.ItemIndex := Index
          else
          begin
            SizeEditor.Text := Size;
          end;
        end;
      end
      else DataSetMasks.EnableControls;
    end
    else
    begin
      DataSetMasks.EnableControls;
      if Size <> '-' then
      begin
        Index := SizeEditor.Items.IndexOf(Size);
        if Index <> -1 then SizeEditor.ItemIndex := Index
        else
        begin
          SizeEditor.Text := Size;
        end;
      end;
    end;
  end;
end;

procedure TDialogArticleStructure.DataSetMasksAfterDelete(DataSet: TDataSet);
begin
  DataSetMasks.Transaction.CommitRetaining;
end;

procedure TDialogArticleStructure.DataSetMaskArticlesAfterDelete(
  DataSet: TDataSet);
begin
  DataSetMaskArticles.Transaction.CommitRetaining;
end;

procedure TDialogArticleStructure.DataSetMasksBeforePost(DataSet: TDataSet);
var
  i: Integer;
  Mask: string;
  Masks: string;
  MaskID: Variant;
  Value: Variant;
begin
  Mask := Trim(DataSetMasksMASK.AsString);
  if Mask = '' then
  begin
    ErrorOkMessage('����� �� ����� ���� ������');
    Abort;
  end
  else
  if Mask <> StoredMask then
  begin
    Value := ExecSelectSQL('select first 1 d_artid from d_art where stretrim(art) like ' + '''' + Mask + '''', dm.quTmp);
    if VarIsNull(Value) then
    begin
      ErrorOkMessage('��� ����� �� ���������� �� ������ ��������.');
      Abort;
    end;

    Value := ExecSelectSQL('select first 1 id from article$mask where mask = ' + '''' + Mask + '''', dm.quTmp);
    if not VarIsNull(Value) then
    begin
      ErrorOkMessage('����� ��� ����������.');
      Abort;
    end;

    DataSetMaskValidate.Active := True;
    if DataSetMaskValidate.RecordCount <> 0 then
    begin
      i := 0;
      Masks := '';
      DataSetMaskValidate.First;
      while (not DataSetMaskValidate.Eof) and (i < 16) do
      begin
        if Masks = '' then Masks := DataSetMaskValidateMask.AsString
        else Masks := Masks + ',' + #13#10  + DataSetMaskValidateMask.AsString;
        DataSetMaskValidate.Next;
        Inc(i);
      end;
      if i = 16 then Masks := Masks + ',' + #13#10 + '...';
      DataSetMaskValidate.Active := False;
      ErrorOkMessage('����� "' + Mask +  '" ������������'#13#10'�� ���������� �������:'#13#10 + Masks);
      Abort;
    end;

    DataSetMaskValidate.Active := False;
  end;
  if DataSetMasks.State = dsInsert then
  begin
    MaskID := ExecSelectSQL('select gen_id(GEN_MASK$ID, 1) from rdb$database', dm.quTmp);
    DataSetMasksID.AsInteger := MaskID;
    BufferTargetMaskID := MaskID;
  end;
end;

procedure TDialogArticleStructure.DataSetMaskValidateBeforeOpen(DataSet: TDataSet);
begin
  if DataSetMasks.State = dsInsert then DataSetMaskValidate.ParamByName('Mask$ID').AsInteger := 0
  else DataSetMaskValidate.ParamByName('Mask$ID').AsInteger := DataSetMasksID.AsInteger;

  DataSetMaskValidate.ParamByName('Mask').AsString := Trim(DataSetMasksMask.AsString);
end;

procedure TDialogArticleStructure.DataSetMaskArticlesBeforePost(DataSet: TDataSet);
var
  ID: Integer;
  MaskID: Integer;
  ArticleID: Integer;
  SQL: string;
  Value: Variant;
begin
  if DataSetMaskArticlesQUANTITY.AsInteger < 0 then
  begin
    ErrorOkMessage('���-�� ������ ���� ������ ��� ����� ����.');
    Abort;
  end;
  if DataSetMaskArticlesARTICLEID.IsNull then
  begin
    ErrorOkMessage('���������� �������.');
    Abort;
  end;

  if DataSetMaskArticles.State = dsInsert then
  begin

    MaskID := DataSetMaskArticlesMASKID.AsInteger;
    ArticleID := DataSetMaskArticlesARTICLEID.AsInteger;
    SQL := 'select article$id from article$structure where';
    SQL := SQL + ' mask$id = ' + IntToStr(MaskID);
    SQL := SQL + ' and Article$ID = ' + IntToStr(ArticleID);

    Value := ExecSelectSQL(SQL, dm.quTmp);
    if not VarIsNull(Value) then
    begin
      ErrorOkMessage('������� ��� ����������.');
      Abort;
    end;

    ID := ExecSelectSQL('select gen_id(GEN_ARTICLE$STRUCTURE$ID, 1) from rdb$database', dm.quTmp);
    DataSetMaskArticlesID.AsInteger := ID;
  end;
end;

procedure TDialogArticleStructure.ViewEditKeyDown(Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_DELETE) and (ssCtrl in Shift) then
  begin
    if Sender = ViewMask then
    begin
      if not ActionMaskDelete.Enabled then Key := 0;
      if not QuestionYesNoMessage('������� �����.') then Key := 0;
    end
    else
    if Sender = ViewArticle then
    begin
      if not ActionArticleDelete.Enabled then Key := 0;
      if not QuestionYesNoMessage('������� �������.') then Key := 0;
    end;
  end;
end;

procedure TDialogArticleStructure.ViewMaskKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if (Key = VK_DELETE) then
  begin
    if not ActionMaskDelete.Enabled then Key := 0 else
    if not QuestionYesNoMessage('������� �����.') then Key := 0;
  end;
end;

procedure TDialogArticleStructure.ViewArticleKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if (Key = VK_DELETE) then
  begin
    if not ActionArticleDelete.Enabled then Key := 0 else
    if not QuestionYesNoMessage('������� �������.') then Key := 0;
  end;
end;

procedure TDialogArticleStructure.InitilizeSizeEditor;
var
  ID: Integer;
  Title: string;
  List: TStrings;

procedure AddSize(ID: Integer; Title: string);
begin
  if List.IndexOf(Title) = -1 then
  List.Add(Title);
end;

begin
  SizeEditor.OnChange := nil;

  SizeEditor.Items.Clear;
  List := SizeEditor.Items;

  AddSize(-1, '-');

  DataSetMaskSize.Active := False;
  DataSetMaskSize.Active := True;
  DataSetMaskSize.First;
  while not DataSetMaskSize.Eof do
  begin
    ID := DataSetMaskSizeSIZEID.AsInteger;
    Title := Trim(DataSetMaskSizeSIZETITLE.AsString);
    AddSize(ID, Title);
    DataSetMaskSize.Next;
  end;
  DataSetMaskSize.Active := False;

  SizeEditor.ItemIndex := 0;
  CurrentSizeID := -1;
  CurrentSizeTitle :='-';
  SizeEditor.OnChange := SizeEditorChange;
end;


procedure TDialogArticleStructure.SizeEditorChange(Sender: TObject);
var
  SizeTitle: string;
begin
  SizeEditor.OnChange := nil;

  SizeTitle := Trim(SizeEditor.Text);
  if SizeTitle <> '' then
  begin
    if SizeTitle <> CurrentSizeTitle then
    begin
      DataSetSize.First;
      if not DataSetSize.Locate('Title', SizeTitle, []) then
      begin
        if QuestionYesNoMessage('������ �� ����������.'#13#10'������� ���������� ��������.') then
        begin
          fmMain.acSz.Execute;
          DataSetSize.Active := False;
          DataSetSize.Active := True;
          DataSetSize.First;
          if not DataSetSize.Locate('Title', SizeTitle, []) then
          begin
            SizeEditor.Text := CurrentSizeTitle;
          end
          else
          begin
            if SizeCoping then
            begin
              ProcedureSizeCopy.ParamByName('Mask$ID').AsInteger := CurrentMaskID;
              ProcedureSizeCopy.ParamByName('Source$Size$ID').AsInteger := CurrentSizeID;
              ProcedureSizeCopy.ParamByName('Target$Size$ID').AsInteger := DataSetSizeID.AsInteger;
              ProcedureSizeCopy.ExecProc;
              ProcedureSizeCopy.Transaction.CommitRetaining;
            end;

            CurrentSizeTitle := SizeEditor.Text;
            CurrentSizeID := DataSetSizeID.AsInteger;
            DataSetMaskArticles.Active := False;
            DataSetMaskArticles.Active := True;

          end;
        end
        else
        begin
          SizeEditor.Text := CurrentSizeTitle;
        end
      end
      else
      begin
        if SizeCoping then
        begin
          ProcedureSizeCopy.ParamByName('Mask$ID').AsInteger := CurrentMaskID;
          ProcedureSizeCopy.ParamByName('Source$Size$ID').AsInteger := CurrentSizeID;
          ProcedureSizeCopy.ParamByName('Target$Size$ID').AsInteger := DataSetSizeID.AsInteger;
          ProcedureSizeCopy.ExecProc;
          ProcedureSizeCopy.Transaction.CommitRetaining;
        end;
        CurrentSizeTitle := SizeEditor.Text;
        CurrentSizeID := DataSetSizeID.AsInteger;
        DataSetMaskArticles.Active := False;
        DataSetMaskArticles.Active := True;
      end;
    end;
  end
  else SizeEditor.Text := CurrentSizeTitle;
  SizeEditor.OnChange := SizeEditorChange;
  SizeCoping := False;
end;

procedure TDialogArticleStructure.ActionMaskCopyExecute(Sender: TObject);
var
  Mask: string;
  Editor: TcxTextEdit;
begin
  Mask := Trim(DataSetMasksMASK.AsString);
  if Length(Mask) > 4 then SetLength(Mask, 4);
  BufferSourceMaskID := CurrentMaskID;
  GridMask.SetFocus;
  ViewMask.DataController.Insert;
  DataSetMasksMASK.AsString := Mask;
  ViewMaskMASK.Editing := True;
end;


procedure TDialogArticleStructure.ActionMaskCopyUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin

  Enabled := DataSetMasks.Active and (DataSetMasks.State = dsBrowse) and (DataSetMasksID.AsInteger <> 0);

  if Enabled then
  Enabled := DataSetMaskArticles.Active and (DataSetMaskArticles.State = dsBrowse);

  ActionMaskCopy.Enabled := Enabled;
end;

procedure TDialogArticleStructure.ActionSizeCopyUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := not SizeCoping;

  if Enabled then
  Enabled := DataSetMasks.Active and (DataSetMasks.State = dsBrowse) and (DataSetMasksID.AsInteger <> 0);

  if Enabled then
  Enabled := DataSetMaskArticles.Active and (DataSetMaskArticles.State = dsBrowse);

  ActionSizeCopy.Enabled := Enabled;
end;


procedure TDialogArticleStructure.SizeEditorDropDown(Sender: TObject);
var
  Index: Integer;
  Size: string;
  SizeID: Integer;
begin
  Size := CurrentSizeTitle;
  SizeID := CurrentSizeID;
  InitilizeSizeEditor;
  CurrentSizeID := SizeID;
  CurrentSizeTitle := Size;
  Index := SizeEditor.Items.IndexOf(Size);
  if Index = -1 then
  begin
    SizeEditor.Items.Append(Size);
    Index := SizeEditor.Items.IndexOf(Size);
  end;
  SizeEditor.OnChange := nil;
  SizeEditor.ItemIndex := Index;
  SizeEditor.OnChange := SizeEditorChange;
end;

procedure TDialogArticleStructure.DataSetMaskSizeBeforeOpen(DataSet: TDataSet);
begin
  DataSetMaskSize.ParamByName('mask$id').AsInteger := CurrentMaskID;
end;

procedure TDialogArticleStructure.SizeEditorExit(Sender: TObject);
begin
  SizeCoping := False;
end;

procedure TDialogArticleStructure.ActionSizeCopyExecute(Sender: TObject);
begin
  SizeEditor.SetFocus;
  SizeEditor.CurText := '����� "' + CurrentSizeTitle + '"';
  SizeCoping := True;
end;


end.


