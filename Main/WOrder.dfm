inherited fmWOrder: TfmWOrder
  Left = 134
  Top = 137
  ActiveControl = gridItem
  Align = alClient
  Caption = #1053#1072#1088#1103#1076
  ClientHeight = 581
  ClientWidth = 909
  OnActivate = FormActivate
  ExplicitWidth = 917
  ExplicitHeight = 615
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 562
    Width = 909
    Panels = <
      item
        Width = 160
      end
      item
        Width = 90
      end
      item
        Width = 50
      end>
    ExplicitTop = 562
    ExplicitWidth = 909
  end
  inherited tb1: TSpeedBar
    Width = 909
    ExplicitWidth = 909
    inherited spitAdd: TSpeedItem [1]
      Action = acAddItem
      Hint = ''
      Left = 67
      Visible = False
      OnClick = acAddItemExecute
    end
    inherited spitDel: TSpeedItem [2]
      Action = acDelSemis
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF008000000080000000800000008000
        00008000000080000000800000008000000080000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000FFFFFF00000000000000
        0000000000000000000000000000FFFFFF0080000000FF00FF00000000000000
        00000000000000000000000000000000000080000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FFFFFF00000000000000
        0000000000000000000000000000FFFFFF0080000000FF00FF0000000000FFFF
        FF000000000000000000000000000000000080000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FFFFFF00000000000000
        0000FFFFFF0080000000800000008000000080000000FF00FF0000000000FFFF
        FF000000000000000000000000000000000080000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0080000000FFFFFF0080000000FF00FF00FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF008000000080000000FF00FF00FF00FF00FF00FF0000000000FFFF
        FF000000000000000000FFFFFF00000000008000000080000000800000008000
        00008000000080000000FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
        000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      Hint = ''
      Left = 131
      Visible = False
      OnClick = acDelSemisExecute
    end
    inherited spitPrint: TSpeedItem [3]
      Tag = 1
      Action = acPrint
      DropDownMenu = nil
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF000000
        0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
        C600C6C6C60000000000C6C6C60000000000FF00FF00FF00FF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000C6C6C60000000000FF00FF0000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600FFFFFF00FFFFFF00FFFF
        FF00C6C6C600C6C6C600000000000000000000000000FF00FF0000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000FF000000FF000000
        FF00C6C6C600C6C6C60000000000C6C6C60000000000FF00FF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000C6C6C600C6C6C6000000000000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
        C600C6C6C60000000000C6C6C60000000000C6C6C60000000000FF00FF000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000C6C6C60000000000C6C6C6000000000000000000FF00FF00FF00
        FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0000000000C6C6C60000000000C6C6C60000000000FF00FF00FF00
        FF00FF00FF0000000000FFFFFF00FF000000FF000000FF000000FF000000FF00
        0000FFFFFF0000000000000000000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000FFFFFF00FF000000FF000000FF000000FF00
        0000FF000000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
        000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      Hint = ''
      OnClick = acPrintExecute
    end
    inherited spitClose: TSpeedItem [4]
    end
    inherited spitExit: TSpeedItem [5]
      Left = 651
    end
    object spitCalcLoss: TSpeedItem
      BtnCaption = #1056#1072#1089#1095#1077#1090
      Caption = #1056#1072#1089#1095#1077#1090
      Hint = #1056#1072#1089#1095#1077#1090' '#1089#1098#1105#1084#1072' '#1080' '#1085#1086#1088#1084' '#1087#1086#1090#1077#1088#1100'|'
      ImageIndex = 8
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = spitSumClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      Action = acAddNew
      BtnCaption = #1053#1086#1074#1072#1103
      Caption = #1053#1086#1074#1072#1103
      ImageIndex = 9
      Spacing = 1
      Left = 3
      Top = 3
      OnClick = acAddNewExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acTotalWOrder
      BtnCaption = #1048#1090#1086#1075#1086
      Caption = 'SpeedItem2'
      Glyph.Data = {
        42020000424D4202000000000000420000002800000010000000100000000100
        1000030000000002000000000000000000000000000000000000007C0000E003
        00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C00000000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C0000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C0000
        1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C1F7C00000000
        1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
        1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
        1F7C1F7C1F7C}
      ImageIndex = 3
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = acTotalWOrderExecute
      SectionName = 'Untitled (0)'
    end
  end
  inherited plMain: TPanel
    Width = 909
    Height = 171
    Anchors = [akTop, akRight]
    ExplicitWidth = 909
    ExplicitHeight = 171
    inherited lbNoDoc: TLabel
      FocusControl = edNoDoc
    end
    inherited lbDateDoc: TLabel
      Top = 28
      FocusControl = dtedDateDoc
      ExplicitTop = 28
    end
    inherited lbDep: TLabel
      Left = 8
      Top = 61
      Visible = False
      ExplicitLeft = 8
      ExplicitTop = 61
    end
    inherited txtDep: TDBText
      Left = 104
      Top = 49
      Width = 33
      Alignment = taRightJustify
      DataField = 'DEPNAME'
      Font.Color = clBlack
      Visible = False
      ExplicitLeft = 104
      ExplicitTop = 49
      ExplicitWidth = 33
    end
    object lbOper: TLabel [4]
      Left = 192
      Top = 47
      Width = 53
      Height = 13
      Caption = #1054#1087#1077#1088#1072#1094#1080#1103' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbExec: TLabel [5]
      Left = 192
      Top = 28
      Width = 67
      Height = 13
      Caption = #1048#1089#1087#1086#1083#1085#1080#1090#1077#1083#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbGet: TLabel [6]
      Left = 12
      Top = 93
      Width = 42
      Height = 13
      Caption = #1042#1099#1076#1072#1085#1086':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lnDone: TLabel [7]
      Left = 144
      Top = 93
      Width = 46
      Height = 13
      Caption = #1057#1076#1077#1083#1072#1085#1086':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbRet: TLabel [8]
      Left = 144
      Top = 109
      Width = 45
      Height = 13
      Caption = #1042#1086#1079#1074#1088#1072#1090':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbRej: TLabel [9]
      Left = 144
      Top = 125
      Width = 28
      Height = 13
      Caption = #1041#1088#1072#1082':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbReWork: TLabel [10]
      Left = 12
      Top = 109
      Width = 59
      Height = 13
      Caption = #1044#1086#1088#1072#1073#1086#1090#1082#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtReWork: TDBText [11]
      Left = 80
      Top = 109
      Width = 61
      Height = 17
      DataField = 'REWORK'
      DataSource = dmMain.dsrWOrder_T
    end
    object txtRej: TDBText [12]
      Left = 192
      Top = 124
      Width = 49
      Height = 17
      DataField = 'REJ'
      DataSource = dmMain.dsrWOrder_T
    end
    object txtRet: TDBText [13]
      Left = 192
      Top = 109
      Width = 49
      Height = 17
      DataField = 'RET'
      DataSource = dmMain.dsrWOrder_T
    end
    object txtDone: TDBText [14]
      Left = 192
      Top = 93
      Width = 49
      Height = 17
      DataField = 'DONE'
      DataSource = dmMain.dsrWOrder_T
    end
    object txtGet: TDBText [15]
      Left = 80
      Top = 93
      Width = 57
      Height = 17
      DataField = 'GET'
      DataSource = dmMain.dsrWOrder_T
    end
    object bvlT: TM207Bevel [16]
      Left = 9
      Top = 69
      Width = 456
      Height = 14
      Shape = bsTopLine
      Caption = #1048#1090#1086#1075#1086' '#1087#1086' '#1085#1072#1088#1103#1076#1091' '#1079#1072' '#1087#1077#1088#1080#1086#1076
      CaptionOffs = 6
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      BorderWidth = 0
    end
    object lbStone: TLabel [17]
      Left = 268
      Top = 81
      Width = 33
      Height = 13
      Caption = #1050#1072#1084#1085#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbF: TLabel [18]
      Left = 8
      Top = 142
      Width = 82
      Height = 13
      Caption = #1048#1090#1086#1075#1086' '#1087#1086' '#1079#1086#1083#1086#1090#1091
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtF_T: TDBText [19]
      Left = 107
      Top = 143
      Width = 65
      Height = 13
      DataField = 'F'
      DataSource = dmMain.dsrWOrder_T
    end
    object Label3: TLabel [20]
      Left = 244
      Top = 93
      Width = 19
      Height = 13
      Caption = #1042#1077#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [21]
      Left = 244
      Top = 109
      Width = 34
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtStoneW: TDBText [22]
      Left = 288
      Top = 93
      Width = 65
      Height = 17
      DataField = 'STONEW'
      DataSource = dmMain.dsrWOrder_T
    end
    object txtStoneQ: TDBText [23]
      Left = 288
      Top = 109
      Width = 65
      Height = 17
      DataField = 'STONEQ'
      DataSource = dmMain.dsrWOrder_T
    end
    object Label5: TLabel [24]
      Left = 108
      Top = 81
      Width = 36
      Height = 13
      Caption = #1047#1086#1083#1086#1090#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel [25]
      Left = 8
      Top = 154
      Width = 80
      Height = 13
      Caption = #1058#1077#1082'. '#1085#1072#1082#1083#1072#1076#1085#1072#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbCurrInv: TLabel [26]
      Left = 105
      Top = 154
      Width = 42
      Height = 13
      Caption = 'lbCurrInv'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel [27]
      Left = 192
      Top = 8
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Bevel1: TBevel [28]
      Left = 8
      Top = 139
      Width = 457
      Height = 9
      Shape = bsTopLine
    end
    object txtADocNo: TDBText [29]
      Left = 656
      Top = 6
      Width = 61
      Height = 17
      DataField = 'DOCNO'
      DataSource = dmMain.dsrAssortInv
    end
    object lbAInv: TLabel [30]
      Left = 508
      Top = 6
      Width = 145
      Height = 13
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072' '#8470
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbAInvFrom: TLabel [31]
      Left = 715
      Top = 6
      Width = 11
      Height = 13
      Caption = #1086#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtADocDt: TDBText [32]
      Left = 732
      Top = 6
      Width = 66
      Height = 14
      DataField = 'DOCDATE'
      DataSource = dmMain.dsrAssortInv
    end
    object Label2: TLabel [33]
      Left = 368
      Top = 81
      Width = 62
      Height = 13
      Caption = #1041#1088#1080#1083#1083#1080#1072#1085#1090#1099
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtBrW: TDBText [34]
      Left = 368
      Top = 93
      Width = 65
      Height = 17
      DataField = 'BRW'
      DataSource = dmMain.dsrWOrder_T
    end
    object txtBrQ: TDBText [35]
      Left = 368
      Top = 109
      Width = 65
      Height = 17
      DataField = 'BRQ'
      DataSource = dmMain.dsrWOrder_T
    end
    object Label8: TLabel [36]
      Left = 12
      Top = 124
      Width = 66
      Height = 13
      Caption = #1053#1077#1082#1086#1084#1087#1083#1077#1082#1090':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtNk: TDBText [37]
      Left = 80
      Top = 124
      Width = 58
      Height = 14
      DataField = 'NK'
      DataSource = dmMain.dsrWOrder_T
    end
    object lbDate: TLabel [38]
      Left = 8
      Top = 48
      Width = 40
      Height = 13
      Caption = #1053#1072#1095#1072#1083#1086':'
    end
    object txtDefOper: TDBText [39]
      Left = 276
      Top = 48
      Width = 165
      Height = 17
      DataField = 'DEFOPERNAME'
      DataSource = dmMain.dsrWOrderList
    end
    object txtExec: TDBText [40]
      Left = 276
      Top = 28
      Width = 165
      Height = 17
      DataField = 'JOBFACE'
      DataSource = dmMain.dsrWOrderList
    end
    object txtJobPS: TDBText [41]
      Left = 276
      Top = 8
      Width = 165
      Height = 17
      DataField = 'JOBDEPNAME'
      DataSource = dmMain.dsrWOrderList
    end
    inherited edNoDoc: TDBEditEh
      DataField = 'DOCNO'
    end
    inherited dtedDateDoc: TDBDateTimeEditEh
      DataField = 'DOCDATE'
    end
    object tlbrT: TTBToolbar
      Left = 153
      Top = 148
      Width = 331
      Height = 19
      ProcessShortCuts = True
      TabOrder = 2
      object it0: TTBItem
        Action = ac0
        GroupIndex = 1
      end
      object it6: TTBItem
        Action = ac6
        GroupIndex = 1
      end
      object TBSeparatorItem1: TTBSeparatorItem
      end
      object it1: TTBItem
        Action = ac1
        GroupIndex = 1
      end
      object it3: TTBItem
        Action = ac3
        GroupIndex = 1
      end
      object it5: TTBItem
        Action = ac5
        GroupIndex = 1
      end
      object it7: TTBItem
        Action = ac7
        GroupIndex = 1
      end
    end
    object lcbxJobPS: TDBLookupComboboxEh
      Left = 276
      Top = 4
      Width = 149
      Height = 19
      DataField = 'JOBDEPID'
      DataSource = dmMain.dsrWOrderList
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      DropDownBox.Rows = 15
      EditButtons = <>
      Flat = True
      KeyField = 'DEPID'
      ListField = 'NAME'
      ListSource = dmMain.dsrPsDep
      TabOrder = 3
      Visible = True
      OnCloseUp = lcbxJobPSCloseUp
      OnKeyValueChanged = lcbxJobPSKeyValueChanged
    end
    object lcbxExec: TDBLookupComboboxEh
      Left = 276
      Top = 24
      Width = 149
      Height = 19
      DataField = 'JOBID'
      DataSource = dmMain.dsrWOrderList
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      DropDownBox.ListSource = dmMain.dsrPsMol
      DropDownBox.AutoDrop = True
      EditButtons = <>
      Flat = True
      KeyField = 'MOLID'
      ListField = 'FIO'
      ListSource = dmMain.dsrPsMol
      TabOrder = 4
      Visible = True
      OnEnter = lcbxExecEnter
    end
    object lcbxOper: TDBLookupComboboxEh
      Left = 276
      Top = 44
      Width = 149
      Height = 19
      DataField = 'DEFOPER'
      DataSource = dmMain.dsrWOrderList
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      DropDownBox.AutoDrop = True
      DropDownBox.Rows = 15
      EditButtons = <>
      Flat = True
      KeyField = 'OPERID'
      ListField = 'OPERATION'
      ListSource = dmMain.dsrPsOper
      TabOrder = 5
      Visible = True
      OnEnter = lcbxOperEnter
    end
    object gridAEl: TDBGridEh
      Left = 501
      Top = 25
      Width = 408
      Height = 145
      AllowedOperations = []
      AllowedSelections = []
      Anchors = [akLeft, akTop, akRight]
      BorderStyle = bsNone
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dmMain.dsrAssortEl
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterRowCount = 1
      FrozenCols = 1
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFrozen3D, dghFooter3D, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghColumnResize, dghColumnMove]
      PopupMenu = ppA
      ReadOnly = True
      RowDetailPanel.Color = clBtnFace
      SumList.Active = True
      TabOrder = 6
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnGetCellParams = gridAElGetCellParams
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footer.ValueType = fvtCount
          Footers = <>
          Title.EndEllipsis = True
          Width = 22
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clNavy
          Footer.Font.Height = -11
          Footer.Font.Name = 'MS Sans Serif'
          Footer.Font.Style = []
          Footer.Value = #1048#1090#1086#1075#1086' '#1096#1090'.'
          Footer.ValueType = fvtStaticText
          Footers = <>
          Title.EndEllipsis = True
          Width = 49
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART2'
          Footers = <>
          Title.EndEllipsis = True
          Width = 39
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SZ'
          Footers = <>
          Title.EndEllipsis = True
          Width = 46
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'Q'
          Footer.FieldName = 'Q1'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clNavy
          Footer.Font.Height = -11
          Footer.Font.Name = 'MS Sans Serif'
          Footer.Font.Style = []
          Footer.ValueType = fvtSum
          Footers = <>
          Title.EndEllipsis = True
          Width = 61
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERATION'
          Footers = <>
          Title.EndEllipsis = True
          Width = 121
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'U'
          Footers = <>
          Title.EndEllipsis = True
          Width = 46
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object dtADocDate: TDBDateTimeEditEh
      Left = 804
      Top = 4
      Width = 74
      Height = 19
      DataField = 'DOCDATE'
      DataSource = dmMain.dsrAssortInv
      EditButton.Style = ebsGlyphEh
      EditButtons = <>
      Flat = True
      Kind = dtkDateEh
      TabOrder = 7
      Visible = True
    end
  end
  object plExt: TPanel [3]
    Left = 0
    Top = 213
    Width = 289
    Height = 349
    Align = alLeft
    BevelInner = bvLowered
    TabOrder = 3
    object plExtTiitle: TPanel
      Left = 2
      Top = 2
      Width = 285
      Height = 43
      Align = alTop
      BevelOuter = bvNone
      Constraints.MinHeight = 43
      Constraints.MinWidth = 273
      TabOrder = 0
      object lbMat: TLabel
        Left = 3
        Top = 24
        Width = 23
        Height = 13
        Caption = #1052#1072#1090'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label7: TLabel
        Left = 116
        Top = 24
        Width = 56
        Height = 13
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103'  '
      end
      object lbWhSemisName: TLabel
        Left = 3
        Top = 1
        Width = 52
        Height = 13
        Caption = #1050#1083#1072#1076#1086#1074#1072#1103':'
      end
      object cmbxMat: TDBComboBoxEh
        Left = 32
        Top = 22
        Width = 81
        Height = 19
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        TabOrder = 0
        Visible = True
        OnChange = cmbxMatChange
      end
      object cmbxOper: TDBComboBoxEh
        Left = 170
        Top = 22
        Width = 102
        Height = 19
        DropDownBox.Rows = 20
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        TabOrder = 1
        Visible = True
        OnChange = cmbxOperChange
      end
      object chbxW: TDBCheckBoxEh
        Left = 216
        Top = 0
        Width = 53
        Height = 17
        Caption = #1042#1077#1089#1099
        Flat = True
        TabOrder = 2
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
    end
    object gridSemis: TDBGridEh
      Left = 2
      Top = 45
      Width = 285
      Height = 302
      Align = alClient
      AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
      Color = clWhite
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dmMain.dsrWhSemis
      Flat = True
      FooterColor = clBtnFace
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterRowCount = 1
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      ReadOnly = True
      RowDetailPanel.Color = clBtnFace
      RowLines = 2
      RowSizingAllowed = True
      SumList.Active = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      OnKeyDown = gridSemisKeyDown
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SEMISNAME'
          Footers = <>
          Width = 99
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'Q'
          Footer.FieldName = 'Q'
          Footer.ValueType = fvtSum
          Footers = <>
          Width = 38
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'W'
          Footer.FieldName = 'W'
          Footer.ValueType = fvtSum
          Footers = <>
          Width = 60
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERNAME'
          Footers = <>
          Width = 82
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'UQ'
          Footers = <>
          Title.Caption = #1045#1076'. '#1080#1079#1084'|'#1082#1086#1083'-'#1074#1072
          Width = 43
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'UW'
          Footers = <>
          Title.Caption = #1045#1076'. '#1080#1079#1084'|'#1074#1077#1089#1072
          Width = 39
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SEMISID'
          Footers = <>
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERID'
          Footers = <>
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'GOOD'
          Footers = <>
          Visible = False
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object tbdcSemis: TTBDock [4]
    Left = 289
    Top = 213
    Width = 66
    Height = 349
    Position = dpLeft
    object TBToolbar2: TTBToolbar
      Left = 0
      Top = 0
      Align = alLeft
      DockMode = dmCannotFloat
      DockPos = 0
      Images = ilButtons
      Options = [tboImageAboveCaption]
      TabOrder = 0
      object TBItem2: TTBItem
        Action = acAddSemisNew
        Caption = ' '#1050' '#1085#1086#1074#1086#1081'  '
      end
      object TBItem6: TTBItem
        Action = acAddSemis
      end
      object TBItem5: TTBItem
        Action = acDelSemis
        Caption = ' '#1059#1076#1072#1083#1080#1090#1100' '
      end
      object TBItem1: TTBItem
        Action = acTransform0
        Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      end
    end
  end
  inherited gridItem: TDBGridEh
    Left = 355
    Top = 213
    Width = 554
    Height = 349
    AllowedOperations = [alopUpdateEh]
    AllowedSelections = []
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    PopupMenu = ppInv
    TabOrder = 5
    UseMultiTitle = True
    OnExit = gridItemExit
    OnGetCellParams = gridItemGetCellParams
    OnKeyDown = gridItemKeyDown
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RecNo'
        Footers = <>
        Width = 22
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1053#1086#1084#1077#1088
        Title.EndEllipsis = True
        Width = 40
      end
      item
        DisplayFormat = 'd.mm.yy'
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1044#1072#1090#1072
        Title.EndEllipsis = True
        Width = 36
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ADOCNO'
        Footers = <>
        Title.EndEllipsis = True
        Width = 48
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OPERNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 73
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ITTYPE'
        Footers = <>
        Title.EndEllipsis = True
        Width = 60
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        DropDownRows = 20
        EditButtons = <>
        FieldName = 'SEMISNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 82
      end
      item
        ButtonStyle = cbsEllipsis
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footers = <>
        Title.EndEllipsis = True
        Width = 43
        OnEditButtonClick = gridItemColumns7EditButtonClick
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Title.EndEllipsis = True
        Width = 39
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
        Title.Caption = #1042#1099#1076#1072#1085#1086'|'#1042#1099#1076#1072#1083
        Title.EndEllipsis = True
        Width = 66
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Title.Caption = #1042#1099#1076#1072#1085#1086'|'#1052#1061'1'
        Title.EndEllipsis = True
        Width = 56
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'JOBNAME'
        Footers = <>
        Title.Caption = #1055#1086#1083#1091#1095#1077#1085#1086'|'#1055#1086#1083#1091#1095#1080#1083
        Title.EndEllipsis = True
        Width = 61
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'JOBDEPNAME'
        Footers = <>
        Title.Caption = #1055#1086#1083#1091#1095#1077#1085#1086'|'#1052#1061'2'
        Title.EndEllipsis = True
        Width = 49
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FROMOPERNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 67
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INVID'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WOITEMID'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WORDERID'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ITDATE'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1042#1099#1076#1072#1085#1086
        Title.EndEllipsis = True
        Visible = False
        Width = 66
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REJPSNAME'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
        Width = 122
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REJREF'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FULLART'
        Footers = <>
        Title.EndEllipsis = True
        Width = 50
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REF'
        Footers = <>
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        DropDownRows = 15
        DropDownWidth = -1
        EditButtons = <>
        FieldName = 'REJNAME'
        Footers = <>
        Title.Caption = #1041#1088#1072#1082'|'#1055#1088#1080#1095#1080#1085#1072
        Title.EndEllipsis = True
        Width = 60
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REJPSNAME'
        Footers = <>
        Title.Caption = #1041#1088#1072#1082'|'#1042#1080#1085#1086#1074#1085#1080#1082
        Title.EndEllipsis = True
        Width = 53
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ITDATE'
        Footers = <>
        Visible = False
      end>
  end
  inherited ilButtons: TImageList
    Left = 362
    Top = 316
    Bitmap = {
      494C010111001300040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000005000000001002000000000000050
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009A9D9E009A9D9E009A9D
      9E009A9D9E009A9D9E009A9D9E009A9D9E009A9D9E009A9D9E009A9D9E009A9D
      9E009A9D9E009A9D9E0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A6A8A900FEFEFD00FEFEFD00FEFE
      FD00FEFEFD00FEFEFD00FCFCFB00F9F9F800F5F5F400F1F1F000EDEDEC00E9E9
      E800E0E0E000D2D2D2009A9D9E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A6A8A900FEFEFD00FEFEFD00FEFE
      FD00FEFEFD00FEFEFD00FEFEFD00FCFCFB00F9F9F800F5F5F400F1F1F000EDED
      EC00E9E9E800E0E0E0009A9D9E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A6A8A900FEFEFD00FEFEFD00FEFE
      FD002D2D2D0058534E00DCDCDC00FEFEFD00FCFCFB00F9F9F800F5F5F400F1F1
      F000EDEDEC00E9E9E8009A9D9E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A6A8A900FEFEFD00DCDCDC00C9C9
      C90058534E004544420051606100DCDCDC00FEFEFD00FCFCFB00F9F9F800F5F5
      F400F1F1F000EDEDEC009A9D9E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A6A8A900FEFEFD00FEFEFD00FEFE
      FD00FEFEFD0054777B001A9CC20018556F00425A9400FEFEFD00FCFCFB00F9F9
      F800F5F5F400F1F1F0009A9D9E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A6A8A900FEFEFD00DCDCDC00C9C9
      C900C9C9C90054777B0029799A00947E75001A9CC200425A9400C9C9C900C9C9
      C900C9C9C900F5F5F4009A9D9E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A6A8A900FEFEFD00FEFEFD00FEFE
      FD00FEFEFD00FEFEFD0098B9D70027D1FC0004B8EC001A9CC200425A9400FEFE
      FD00FEFEFD00FAFAF9009A9D9E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A6A8A900FEFEFD00FEFEFD00FEFE
      FD00FEFEFD00FEFEFD0098B9D700ACEDFC0027D1FC0004B8EC001A9CC200425A
      9400FEFEFD00FEFEFD009A9D9E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A6A8A900108C3100108C3100108C
      3100108C3100108C3100108C310098B9D700ACEDFC0027D1FC0004B8EC001A9C
      C200425A9400108C31009A9D9E00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000010B5310073D69C0010B5310010B5
      310010B5310010B5310010B5310010B5310098B9D700ACEDFC0027D1FC0004B8
      EC001A9CC200425A940000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000010B5310073D69C002BBD
      5F002BBD5F0021A5420000000000000000000000000098B9D700ACEDFC0027D1
      FC0035A8F500222F9B0011087500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000010B5310010B5
      310020A43900000000000000000000000000000000000000000098B9D7004A9E
      ED00455FC400455FC400222F9B00110875000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000455F
      C400889FE1008997DF00455FC400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000455FC400455FC40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000084840000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000848400000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000848400FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00BDBDBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000848400FFFFFF0000FFFF0000000000008484000084
      8400008484000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084840000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      84000000000000000000000000000000000000FFFF00FFFFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000084
      8400FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000848400FFFFFF0000FFFF00FFFFFF0000FFFF0000000000008484000084
      8400008484000000000000000000000000000000000000000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000084840000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B0000FFFF007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000848400FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF0000000000000000000000000000000000000000000000FF000000FF000000
      FF00000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000848400FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000FFFF00000000000000000000000000000000000000FF000000FF000000
      FF0000000000000000007B7B7B0000FFFF007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000848400008484000084840000848400008484000084
      840000848400008484000084840000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B58C8C008C5A5A008C5A5A008C5A5A008C5A5A008C5A5A008C5A
      5A008C5A5A008C5A5A008C5A5A0000000000000000000000000000000000299C
      DE00299CDE00A57B7300A57B7300A57B7300A57B7300A57B7300A57B7300A57B
      7300A57B7300A57B7300A57B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      000000000000B58C8C00FFFFEF00FFF7E700FFF7DE00F7EFDE00F7EFDE00F7EF
      DE00FFEFDE00F7E7D6008C5A5A00000000000000000000000000299CDE008CD6
      EF0084D6F700CEC6BD00FFEFDE00FFF7E700FFF7E700F7EFDE00F7EFDE00F7EF
      DE00F7EFDE00FFEFDE00A57B73000000000000000000AD632900AD632900AD63
      2900AD632900AD632900AD632900AD632900AD632900AD632900AD632900AD63
      2900AD632900AD632900AD632900000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      000000000000B58C8C00FFF7E700F7E7CE00F7E7CE00F7DECE00F7DEC600F7DE
      C600F7E7CE00EFDECE008C5A5A000000000000000000299CDE00A5EFFF0094F7
      FF008CF7FF00CEC6BD00F7E7DE00F7E7D600F7DEC600F7DEC600F7DEC600F7DE
      BD00F7DEC600F7E7D600A57B730000000000DEA53900FFD69400EFB57300E7AD
      6300FFBD6300F7AD4A00DE9C3900FFAD3100FFAD2100FF9C1000FF9C0000FF9C
      0000FF9C0000FF9C0000FF9C0000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000000000000000000000
      000000000000B58C8C00FFF7E700FFD6A500FFD6A500FFD6A500FFD6A500FFD6
      A500FFD6A500EFDECE008C5A5A000000000000000000299CDE00A5E7FF0094EF
      FF0084EFFF00CEC6BD00FFEFDE00FFE7CE00FFDEBD00FFDEBD00FFDEBD00FFDE
      BD00F7DEC600F7E7D600A57B730000000000E7AD1800EFC694005A4A4200524A
      4200D69C5A00846B420042393900BD843900D694290052423100524229005242
      310052422900634A2900F7940000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FF000000FF000000FF
      000000FF000000FF000000FF00000000000000000000B58C8C008C5A5A008C5A
      5A008C5A5A00B58C8C00FFF7EF00FFE7CE00FFE7C600FFDEC600FFDEC600F7DE
      BD00F7E7D600EFDECE009C6B63000000000000000000299CDE00ADEFFF00A5F7
      FF0094F7FF00CEC6BD00FFEFE700FFE7D600FFDEC600FFDEC600FFDEC600F7DE
      BD00F7DEC600F7E7D600A57B730000000000E7AD1800FFD6A500CEA57300C694
      6B00FFBD6B00D69C5A00BD8C4200EFA54200EFA54200AD7B5200C6843900E78C
      1000B57B3100BD7B3100FF9C0000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000FF
      000000FF000000000000000000000000000000000000B58C8C00FFFFEF00FFF7
      E700FFF7DE00B58C8C00FFFFF700FFE7CE00FFE7CE00FFE7C600FFDEC600FFDE
      C600F7E7D600EFE7D6009C6B6B000000000000000000299CDE00B5EFFF00ADF7
      FF00A5F7FF00CEC6BD00FFEFEF00FFEFDE00FFE7D600FFE7CE00FFE7CE00FFE7
      CE00F7E7D600F7EFDE00A57B730000000000E7AD1800EFC69C0063524A005A52
      4200D6A56B008C734A004A423900B5843900947B94002942DE006B63A500CE8C
      4A003142D600314ACE00EF941000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FF
      000000FF000000000000000000000000000000000000B58C8C00FFF7E700F7E7
      CE00F7E7CE00B58C8C00FFFFF700FFD6A500FFD6A500FFD6A500FFD6A500FFD6
      A500FFD6A500F7E7DE00A57B73000000000000000000299CDE00BDEFFF00BDF7
      FF00B5F7FF00CEC6BD00FFF7F700FFEFD600FFDEBD00FFDEBD00FFDEBD00FFDE
      B500FFE7CE00F7EFE700A57B730000000000E7AD1800FFD6A500BD9C7B00B594
      6B00FFC67B00D6A56300A57B4A00EFAD4A00EFA552008C739400C68C5200FFA5
      10009C736B00A5735A00FF9C0000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000B58C8C00FFF7E700FFD6
      A500FFD6A500B58C8C00FFFFFF00FFEFDE00FFE7D600FFE7D600FFE7D600FFEF
      D600FFF7E700EFE7DE00A57B73000000000000000000299CDE00C6EFFF00CEF7
      FF00BDF7FF00CEC6BD00FFF7F700FFF7F700FFF7E700FFEFE700FFEFE700FFEF
      E700FFF7E700EFE7DE00A57B730000000000E7AD1800EFBD9400635A4A005A52
      4A00CEA573008C73520042423900B5844A009C8494002139E7006B63A500CE94
      4A00314AD600314ACE00E7941800AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000B58C8C00FFF7EF00FFE7
      CE00FFE7C600B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFDE
      DE00D6C6C600BDADAD00B58473000000000000000000299CDE00CEEFFF00DEFF
      FF00CEFFFF00CEC6BD00FFFFF700FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7E7
      E700D6BDB500C6ADA500A57B730000000000E7AD1800FFD6A500FFC69400F7C6
      9400FFCE9400FFC67B00EFB56B00FFBD6300FFB54A00E7A55A00FFAD3900FFAD
      2100EF9C2900F79C1800FF9C0000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000B58C8C00FFFFF700FFE7
      CE00FFE7CE00B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B58C
      8C00B58C8C00B58C8C00B58C8C000000000000000000299CDE00D6F7FF00EFFF
      FF00DEFFFF00CEC6BD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00DECE
      C600E7AD7300F7945A000000000000000000E7AD1800F7CEA500E7DEC600E7E7
      C600DEDEBD00E7DEBD00E7DEB500DED6AD00E7D69C00FFBD5A00FFAD3900F7A5
      3100F79C2100FF9C1000FF9C0000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000B58C8C00FFFFF700FFD6
      A500FFD6A500B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B58C
      8C00EFB56B00C68C7B00000000000000000000000000299CDE00DEF7FF00FFFF
      FF00EFFFFF00CEC6BD00FFF7EF00FFF7F700FFF7F700FFF7F700FFF7F700E7C6
      BD00C6AD8C00299CDE000000000000000000E7AD1800D6AD8C006BD6CE006BD6
      CE006BD6CE006BD6CE006BD6CE0063D6D6008CE7DE00FFC67B00CE8C3900B584
      3900AD7B3100C6842100FFA50800AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFEF
      DE00FFE7D600B58C8C00B58C8C00B58C8C00B58C8C00B58C8C00B58C8C00B58C
      8C00BD84840000000000000000000000000000000000299CDE00DEF7FF00FFFF
      FF00F7FFFF00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEC6
      BD0084C6DE00299CDE000000000000000000EFB53900FFCEA500D6AD8400D6AD
      8400D6AD8400D6AD8400D6AD7B00CEA56B00DEAD6B00FFBD6B00F7AD5200FFAD
      4200E79C3100F7A52900FFA51000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00EFDEDE00D6C6C600BDADAD00B58473000000
      00000000000000000000000000000000000000000000299CDE00DEF7FF00F7F7
      F700B5C6CE00ADC6CE00A5C6CE00A5C6CE00A5C6CE00A5C6CE00B5D6D600DEFF
      FF008CDEF700299CDE00000000000000000000000000E7A50800EFB53900F7BD
      4A00EFB54A00EFB54A00F7BD6300F7B55A00EFB54A00EFAD4200EFA53900E79C
      3100EF9C2100E7941800D684180000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B58C8C00B58C8C00B58C8C00B58C8C000000
      00000000000000000000000000000000000000000000299CDE00E7FFFF00DECE
      C600BDA59C00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEBDB500BD9C9400E7EF
      E70094DEF700299CDE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B58C8C00EFB56B00C68C7B00000000000000
      0000000000000000000000000000000000000000000000000000299CDE00B5D6
      E700949C9C00E7DED600FFFFFF00FFFFF700FFFFF700D6C6BD00849CA5008CCE
      E700299CDE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00B58C8C00B58C
      8C00B58C8C00B58C8C00B58C8C00B58C8C00BD84840000000000000000000000
      000000000000000000000000000000000000000000000000000000000000299C
      DE00299CDE009C948C009C948C009C948C009C948C009C948C00299CDE00299C
      DE00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B5B5B5000000000000000000000000009C9C9C009C9C
      9C00A5A5A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000069D
      CC0049B7E00060C1E80054BCE3003DB2DC004DB9E10000000000000000000000
      0000000000000000000000000000000000007B7B7B004A637B00BD9494000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5B5B500B5B5B500DEDEDE00ADADAD00525252006B6B6B00ADADAD00D6D6
      D600E7DEDE009C9C9C000000000000000000000000000000000000000000069D
      CC0049B7E00060C1E80054BCE3003DB2DC004DB9E10000000000000000000000
      000000000000000000000000000000000000000000000000000008A1CE000EA5
      D1007FD2F100A0DFFE0097DAFB0093D9FA007CCDEF0073CAEE0048B7E0000000
      0000000000000000000000000000000000006B9CC600188CEF004A7BA500CE94
      9400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5B5B500B5B5
      B500FFFFFF00FFFFFF00E7DEDE00B5B5B5005A5A630031313100313131004A4A
      4A00848484009C9C9C00A5A5A50000000000000000000000000008A1CE000EA5
      D1007FD2F100A0DFFE0097DAFB0093D9FA007CCDEF0073CAEE0048B7E0000000
      000000000000000000000000000000000000000000000000000013A8D4001DAF
      D80083D8EE00A0E3FB0093DAF90082D0F20059BBE0008FD7F7009ADCFB0076CB
      F000000000000000000000000000000000004AB5FF0052B5FF00218CEF004A7B
      A500C69494000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A5A5A500B5B5B500F7F7F700FFFF
      FF00F7F7F700D6D6D600B5B5B500A5A5A500ADADAD009C9C9C007B7B7B005252
      520031313900313131008C8C8C0000000000000000000000000013A8D4001DAF
      D80083D8EE00A0E3FB0093DAF90082D0F20059BBE0008FD7F7009ADCFB0076CB
      F00000000000000000000000000000000000000000000000000020AFD8002EB7
      DE0094DFEF00B1EEFA00A2E7FD0064B2D100369BC0006FC7EB0095DAFB009ADC
      FB0034B1DA000000000000000000000000000000000052B5FF0052B5FF001884
      E7004A7BA500CE94940000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B5ADAD00EFEFEF00EFEFEF00CECE
      CE00BDB5B500C6C6C600D6CECE00B5B5B500ADADAD00ADA5A500ADADAD00ADAD
      AD00A5A5A500848484009C9C9C0000000000000000000000000020AFD8002EB7
      DE0094DFEF00B1EEFA00A2E7FD0064B2D100369BC0006FC7EB0095DAFB009ADC
      FB0034B1DA0000000000000000000000000000000000000000002DB5DD003FBE
      E400B1E5F100CFF8FD00B6F4FF0083C9DE00287FA10069BDE00094DBFC0095DA
      FA0033B0D900000000000000000000000000000000000000000052B5FF004AB5
      FF00188CE7004A7BA500BD949400000000000000000000000000000000000000
      000000000000000000000000000000000000ADA5AD00BDBDBD00B5B5B500C6C6
      C600D6D6D600DEDEDE00F7F7F700F7F7F700E7E7E700D6D6D600BDBDBD00ADAD
      AD00ADA5A500ADADAD00A5A5A5000000000000000000000000002DB5DD003FBE
      E400B1E5F100CFF8FD00B6F4FF0083C9DE00287FA10069BDE00094DBFC0095DA
      FA0033B0D900000000000000000000000000000000000000000039BBE30048C1
      E800C5E8F400ECFAFD00CAF5FB00B3ECF50063A9BF008BCFE80099DFFE0094D8
      FA0033B0D90000000000000000000000000000000000000000000000000052BD
      FF004AB5FF002184DE005A6B730000000000AD7B7300C6A59C00D6B5A500D6A5
      9C0000000000000000000000000000000000A5A5A500C6BDC600D6D6D600DEDE
      DE00D6D6D600EFEFEF00DEDEDE00B5BDB500CECECE00D6DEDE00DEDEDE00DEDE
      DE00D6D6D600C6C6C600ADADAD0000000000000000000000000039BBE30048C1
      E800C5E8F400ECFAFD00CAF5FB00B3ECF50063A9BF008BCFE80099DFFE0094D8
      FA0033B0D90000000000000000000000000000000000000000002EB3DB0044C1
      E2005EC0DB006CC8E00062C8E20060C7E2006ECFE70086D8EE009DE2F900A2E1
      FE0034B1DA000000000000000000000000000000000000000000000000000000
      000052BDFF00B5D6EF00A5948C00B59C8C00F7E7CE00FFFFDE00FFFFDE00FFFF
      DE00EFDEC600CEADA500000000000000000000000000B5ADAD00DEDEDE00DEDE
      DE00E7E7E700D6D6D600C6C6C600BDDEBD00CED6CE00D6BDB500BDB5B500BDBD
      BD00C6C6C600D6CECE00BDBDBD000000000000000000000000002EB3DB0044C1
      E2005EC0DB006CC8E00062C8E20060C7E2006ECFE70086D8EE009DE2F900A2E1
      FE0034B1DA00000000000000000000000000000000000000000029B3D80075DC
      E700328BA5003A9AB30069E1F00044CCE70021B5D90014AAD3002CAFD60079D0
      ED0037B3DA000000000000000000000000000000000000000000000000000000
      000000000000CEB5B500DEBDA500FFF7C600FFFFD600FFFFDE00FFFFDE00FFFF
      DE00FFFFEF00F7F7EF00B58C8C00000000000000000000000000B5ADAD00CECE
      CE00C6BDBD00C6C6C600EFEFEF00FFF7F700F7F7F700F7EFE700E7E7E700DEDE
      DE00C6C6C600ADA5A500000000000000000000000000000000002BB1D80058CD
      E40040B8D70046BEDB0045C2DF0037B7D9004AC2E2006BCFEA009AE1F80086D4
      F40025AAD40000000000000000000000000000000000000000000000000030B2
      D6004FBBD10074D7E30098FDFF0080F1FA0050CFE60012A1C8000397C30013A4
      CF0018A5D0000000000000000000000000000000000000000000000000000000
      000000000000C6948C00F7E7B500FFDEAD00FFF7D600FFFFDE00FFFFE700FFFF
      F700FFFFFF00FFFFFF00DED6BD0000000000000000000000000000000000B5AD
      AD00E7E7E700D6D6D600B5B5B500CECECE00DEDEE700DEE7E700DEDEDE00D6D6
      D600C6BDBD00000000000000000000000000000000000000000028B1D7005CCC
      DF000C9BC4003CB6D1008AF5FB0055D7EC0024B6DA00049DCA0025ABD20052C0
      E0001FA7D2000000000000000000000000000000000000000000000000000000
      0000000000000000000054CFE7004ACAE40045C1DD00129AC3000F98C20011A5
      D00011A2CF000000000000000000000000000000000000000000000000000000
      000000000000DEBDAD00FFE7AD00F7CE9400FFF7CE00FFFFDE00FFFFE700FFFF
      FF00FFFFFF00FFFFEF00F7EFD600C69C94000000000000000000000000000000
      0000FFEFEF00FFE7DE00EFD6CE00EFD6CE00EFDED600E7DEDE00DEDEDE00ADAD
      AD00000000000000000000000000000000000000000000000000000000000C94
      C1000091BF002FAECF0087F0F8007AECF80047C2DC000B96C000119DC4001CA9
      D000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000109CC5000894C0000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7C6AD00FFE7AD00EFBD8400FFE7B500FFFFDE00FFFFDE00FFFF
      EF00FFFFEF00FFFFE700FFFFD600C6AD9C000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000B99C600199BC300000000003ABFE00020A7CD000A95C0000D96C2000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000078CB8000587B300000000000000000000000000089AC6000B91BC000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DEC6AD00FFEFB500EFBD8400F7CE9C00FFEFC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00F7F7D600CEA59C000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004FC3E6002A9DC6000000000000000000000000000994BF000C8AB5000000
      0000000000000000000000000000000000000000000000000000000000000000
      00002EB0DA001E9FCA00000000000000000000000000099CC8000B92BD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CEA59C00FFF7C600FFEFC600F7D6A500F7D69C00FFE7BD00FFF7
      CE00FFFFD600FFFFDE00E7DEBD00000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00003BA6C60077DEEE003DAAC80000000000148AB6000C9DC800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004CB5D50068D0EA000000000000000000000000000999C5000A92BE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DEC6AD00FFFFFF00FFF7EF00F7CE9C00F7BD8C00F7CE
      9C00FFE7B500FFF7CE00BD9C8C0000000000000000000000000000000000DEB5
      B500FFE7DE00FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004AB5CD007BE0EC006BD1E6004ABCE0001898C300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005BC4D90078DFEB004FB9D4003EAED2001DA6D100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6BDBD00F7F7DE00FFF7C600FFE7B500FFEF
      B500F7DEB500D6AD9C000000000000000000000000000000000000000000DEB5
      B500DEB5B500DEB5B500DEB5B500F7BDB500F7BDB50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000055BFD40055C0D6004FBAD80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CEAD9400CEAD9C00DEBDAD00DEBD
      AD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C6363006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009C6363009C636300BD636300BD6B6B006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000008780E0008780E0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C6363009C63
      6300C66B6B00D66B6B00D66B6B00C66B6B006B3131009C6363009C6363009C63
      63009C6363009C6363009C636300000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000DA31B0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300DE73
      7300D6737300D66B7300D66B6B00C66B6B006B313100FFA5A500FFADB500FFBD
      BD00FFC6C600FFC6C6009C636300000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000EAA1D0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300E773
      7B00DE737300DE737300DE737300CE6B73006B31310039C6630021CE630029CE
      630018CE5A00FFC6C6009C636300000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000EA81C0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300E77B
      7B00E77B7B00DE7B7B00DE737B00D67373006B31310042C66B0031CE630031CE
      630021CE6300FFC6C6009C636300000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70010AA1F0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300EF84
      8400E77B8400E77B7B00E7848400D67373006B31310039C6630029CE630029CE
      630021CE5A00FFC6C6009C63630000000000000000000000000008780E000878
      0E0008780E0008780E0008780E0076F9A70019B02C0008780E0008780E000878
      0E0008780E0008780E0000000000000000000000000000000000000000000104
      A2000104A2000104A2000104A2000104A2000104A2000104A2000104A2000104
      A2000104A2000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300F784
      8C00EF848400EF949400FFDEDE00DE8C8C006B313100BDE7AD006BDE8C005AD6
      840042D67300FFC6C6009C636300000000000000000008780E0076F9A70055E3
      830049DA720042D3680037C856002AB9430022B337001CB2300016AF27000FA8
      1D000EA91B000DA21B0008780E000000000000000000000000000104A2005983
      FF000026FF000030FF000030FB00002FF200002FE900002EE1000030D8000031
      D0000034CB000104A20000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300F78C
      8C00EF848400F79C9C00FFDEDE00DE8C8C006B313100FFF7DE00FFFFE700FFFF
      DE00EFFFD600FFC6C6009C636300000000000000000008780E0076F9A70076F9
      A70076F9A70076F9A70076F9A70076F9A7002CBB480076F9A70076F9A70076F9
      A70076F9A70076F9A70008780E000000000000000000000000000104A200ABC2
      FF006480FF006688FF006688FF006687FA006787F5006787F0005779E9004D70
      E4004D74E2000104A20000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300FF94
      9400F78C8C00F78C8C00F78C8C00DE7B84006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFE700FFC6C6009C63630000000000000000000000000008780E000878
      0E0008780E0008780E0008780E0076F9A7003CCB5D0008780E0008780E000878
      0E0008780E0008780E0000000000000000000000000000000000000000000104
      A2000104A2000104A2000104A2000104A2000104A2000104A2000104A2000104
      A2000104A2000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300FF94
      9C00FF949400FF949400FF949400E78484006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C636300000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70049D9720008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300FF9C
      9C00FF949C00FF949400FF949C00E78C8C006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C636300000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70055E2820008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300FF9C
      A500FF9C9C00FF9C9C00FF9C9C00E78C8C006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C636300000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70063F0970008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C6363009C63
      6300EF8C8C00FF9C9C00FF9C9C00EF8C94006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C636300000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70076F9A70008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009C636300B5737300D6848400DE8C8C006B3131009C6363009C6363009C63
      63009C6363009C6363009C636300000000000000000000000000000000000000
      000000000000000000000000000008780E0008780E0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C6363009C6363006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000500000000100010000000000800200000000000000000000
      000000000000000000000000FFFFFF00FFFF0000000000008003000000000000
      0001000000000000000100000000000000010000000000000001000000000000
      0001000000000000000100000000000000010000000000000001000000000000
      000100000000000000030000000000008381000000000000C7C0000000000000
      FFE1000000000000FFF3000000000000FCFFFE7FFFFFFFFFFE7FFE1FFFFFFFFF
      FE3FFC07FFFFFFFFF81FFC01FFFFFF3FF80FF800FCFFFC3FFC07F800FC3FF03F
      FC1F0000FC0FC000E00F000000030000E00700010000C000F00700320003F03F
      F01F003EFC0FFC3FF80F003EFC3FFF3FF807003EFCFFFFFFFC03001DFFFFFFFF
      FC010023FFFFFFFFFFFF003FFFFFFFFFFFFFFFC3F801E001FFFFFFC3F801C001
      8001C000F80180010000C000F80180010000C000800180010000C00080018001
      0000C003800180010000C003800180010000C007800180010000C00780018003
      0000C007800380030000C007800780030000C00F801F80038001C01F801F8003
      FFFFC03F803FC007FFFFFFFF807FE00FFDC7FFFFE07F1FFFF003E07FC01F0FFF
      C001C01FC00F07FF0001C00FC00783FF0001C007C007C1FF0001C007C007E10F
      0001C007C007F0038001C007C007F801C003C007E007F801E007C007FC07F800
      F00FE00FFF9FF800F03FF21FF39FF800F03FF39FF39FF801F03FF13FF39FFC01
      E03FF83FF83FFE03E07FFFFFFC7FFF0FFE7FFFFFFFFFFFFFF07FFE7FFFFFFFFF
      C001FC3FFFFFC007C001FC3FFFFFE7E7C001FC3FFFFFF3F7C001FC3FFFFFF9F7
      C001C003E007FCFFC0018001C003FE7FC0018001C003FF3FC001C003E007FE7F
      C001FC3FFFFFFCFFC001FC3FFFFFF9F7C001FC3FFFFFF3F7C001FC3FFFFFE7E7
      F001FE7FFFFFC007FC7FFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  inherited ppPrint: TTBPopupMenu [7]
    Left = 536
    Top = 300
  end
  inherited fmstr: TFormStorage [8]
    Active = False
    PreventResize = True
    Left = 364
    Top = 348
  end
  inherited ActionList2: TActionList [9]
    Left = 436
    Top = 60
  end
  object ppInv: TPopupMenu
    Images = ilButtons
    Left = 584
    Top = 300
    object mnitAddToInv: TMenuItem
      Action = acAddItem
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object mnitAddToNew: TMenuItem
      Action = acAddNew
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082' '#1085#1086#1074#1086#1081' '#1085#1072#1082#1083'.'
    end
    object mnitDelItem: TMenuItem
      Action = acDelSemis
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object mnitPrint: TMenuItem
      Tag = 1
      Action = acPrint
      Caption = #1055#1077#1095#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object N7: TMenuItem
      Action = acPrintPreview
    end
    object N8: TMenuItem
      Action = acPrintInvWithAssort
    end
    object N6: TMenuItem
      Action = acPrintGrid
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object Excel1: TMenuItem
      Action = acExcel
    end
    object mnitSep1: TMenuItem
      Caption = '-'
      Visible = False
    end
    object mnitCopy: TMenuItem
      Action = acSCopy
    end
    object mnitIns: TMenuItem
      Action = acSPaste0
    end
    object mnitInsOrd: TMenuItem
      Tag = 1
      Action = acSPaste1
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Action = acTransform0
      Visible = False
    end
    object N2: TMenuItem
      Action = acE
    end
    object N4: TMenuItem
      Action = acArt
    end
  end
  object acEvent: TActionList
    Images = ilButtons
    Left = 400
    Top = 348
    object acAddSemisNew: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = '  '#1050' '#1085#1086#1074#1086#1081'  '
      ImageIndex = 9
      OnExecute = acAddSemisNewExecute
    end
    object acAddSemis: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 14
      OnExecute = acAddSemisExecute
    end
    object acDelSemis: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      ImageIndex = 15
      OnExecute = acDelSemisExecute
    end
    object acAddItem: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      OnExecute = acAddItemExecute
    end
    object acAddNew: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1053#1086#1074#1072#1103
      ImageIndex = 9
      OnExecute = acAddNewExecute
    end
    object acACopy: TAction
      Category = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 10
      OnExecute = acACopyExecute
      OnUpdate = acACopyUpdate
    end
    object acAPaste: TAction
      Category = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100
      ImageIndex = 11
      OnExecute = acAPasteExecute
      OnUpdate = acAPasteUpdate
    end
    object acSCopy: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 10
      OnExecute = acSCopyExecute
    end
    object acSPaste0: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' ('#1074#1099#1076#1072#1095#1072')'
      ImageIndex = 11
      OnExecute = acPasteExecute
      OnUpdate = acSPaste0Update
    end
    object acSPaste1: TAction
      Tag = 1
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' ('#1085#1072#1088#1103#1076')'
      ImageIndex = 11
      OnExecute = acPasteExecute
      OnUpdate = acSPaste1Update
    end
    object acTransform0: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1058#1088#1072#1085#1089#1092#1086#1088#1084#1072#1094#1080#1103
      ImageIndex = 12
      ShortCut = 117
      OnExecute = acTransform0Execute
    end
    object acArt: TAction
      Category = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      ImageIndex = 13
      ShortCut = 16449
      OnExecute = acArtExecute
      OnUpdate = acArtUpdate
    end
    object acPrint: TAction
      Tag = 1
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 4
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
    object acPrintGrid: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1055#1077#1095#1072#1090#1100' '#1090#1072#1073#1083#1080#1094#1099
      OnExecute = acPrintGridExecute
    end
    object Action1: TAction
    end
    object acE: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1069#1089#1082#1087#1077#1088#1080#1084#1077#1085#1090#1072#1083#1100#1085#1072#1103' '#1088#1072#1073#1086#1090#1072
      ShortCut = 120
      Visible = False
      OnExecute = acEExecute
    end
    object acExcel: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      OnExecute = acExcelExecute
    end
    object acMultiSelect: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = 'MultiSelect'
      ShortCut = 116
      OnExecute = acMultiSelectExecute
    end
    object acShowID: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = 'ShowID'
      ShortCut = 8260
      OnExecute = acShowIDExecute
    end
    object ac0: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1042#1099#1076#1072#1095#1072
      ShortCut = 32817
      OnExecute = ac0Execute
    end
    object ac6: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1044#1086#1088#1072#1073#1086#1090#1082#1072
      ShortCut = 32818
      OnExecute = ac6Execute
    end
    object ac1: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1053#1072#1088#1103#1076
      ShortCut = 32819
      OnExecute = ac1Execute
    end
    object ac3: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1042#1086#1079#1074#1088#1072#1090
      ShortCut = 32820
      OnExecute = ac3Execute
    end
    object ac5: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1041#1088#1072#1082
      ShortCut = 32821
      OnExecute = ac5Execute
    end
    object ac7: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1053#1077#1082#1086#1084#1087#1083#1077#1082#1090
      ShortCut = 32822
      OnExecute = ac7Execute
    end
    object acPrintPreview: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1055#1077#1095#1072#1090#1100' '#1087#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1086#1081' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      OnExecute = acPrintPreviewExecute
    end
    object acPrintInvWithAssort: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1055#1077#1095#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1089' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1086#1084
      OnExecute = acPrintInvWithAssortExecute
    end
    object acTotalWOrder: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1048#1090#1086#1075#1086
      ImageIndex = 3
      OnExecute = acTotalWOrderExecute
    end
    object acProcessAppl: TAction
      Category = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      Caption = #1054#1073#1088#1072#1073#1086#1090#1072#1090#1100' '#1079#1072#1082#1072#1079
      ImageIndex = 16
      OnExecute = acProcessApplExecute
    end
  end
  object ppA: TPopupMenu
    Images = ilButtons
    Left = 608
    Top = 108
    object mnitCopyA: TMenuItem
      Action = acACopy
    end
    object mnitPasteA: TMenuItem
      Action = acAPaste
    end
  end
  object printItem: TPrintDBGridEh
    DBGridEh = gridItem
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 444
    Top = 316
  end
  object PrinterSetupDialog: TPrinterSetupDialog
    Left = 608
    Top = 400
  end
end
