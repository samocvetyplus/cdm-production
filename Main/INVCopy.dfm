object fmInvCopy: TfmInvCopy
  Left = 274
  Top = 252
  BorderStyle = bsDialog
  Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1085#1080#1077' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
  ClientHeight = 145
  ClientWidth = 269
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object bvl: TBevel
    Left = 2
    Top = 1
    Width = 264
    Height = 107
    Shape = bsFrame
  end
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 71
    Height = 13
    Caption = #8470' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '
  end
  object Label2: TLabel
    Left = 8
    Top = 28
    Width = 86
    Height = 13
    Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '
  end
  object lbDep: TLabel
    Left = 8
    Top = 48
    Width = 67
    Height = 13
    Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
  end
  object Label4: TLabel
    Left = 8
    Top = 88
    Width = 63
    Height = 13
    Caption = #1057#1090#1072#1074#1082#1072' '#1053#1044#1057
  end
  object Label3: TLabel
    Left = 8
    Top = 68
    Width = 49
    Height = 13
    Caption = #1050#1083#1072#1076#1086#1074#1072#1103
  end
  object btnCreate: TBitBtn
    Left = 88
    Top = 114
    Width = 85
    Height = 25
    Caption = #1057#1086#1079#1076#1072#1090#1100
    Default = True
    TabOrder = 5
    OnClick = btnCreateClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object edDocNo: TDBNumberEditEh
    Left = 96
    Top = 4
    Width = 109
    Height = 19
    DecimalPlaces = 0
    EditButton.Style = ebsUpDownEh
    EditButton.Visible = True
    EditButtons = <>
    Flat = True
    TabOrder = 0
    Visible = True
  end
  object edDocDate: TDBDateTimeEditEh
    Left = 96
    Top = 24
    Width = 109
    Height = 19
    EditButton.Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
      262FA31D20B5FF00FF4B4B4B5B5B5B87817B87817BFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FF82776F5F61BC2126B2666059A3A3A4FAFAFAE3
      E0DCD1CEC99A979582807E7874715D5A58FF00FFFF00FFFF00FFFF00FFA89381
      7B75C42024B2BA9775A3A3A4FFFFFFFFFFFFFFFFFDD6D0CAC8C0B8F6EBDDEDE0
      D1A59B916B6864FF00FFFF00FFA893817873C32024B2B99B7EA3A3A4FEFFFFFF
      FFFF9F9FA07371713535377E7B76FFF5E7FFF2E1878685FF00FFFF00FFA89483
      7873C52024B3BAA18AA3A3A4FEFFFFFFFFFFCACACCF5F3F2FFFDF9353537F3EA
      E0FDF0E2878685FF00FFFF00FFA8978B7876CA2024B2BBA896A3A3A4FEFFFFFF
      FFFFB3B3B4828283A2A1A2353537F8F2EBFDF2E9878685FF00FFFF00FF918984
      7779CF2024B2BCADA0A3A3A4FEFFFFFFFFFF9A9A9B3535377C7C7ECCCCCBFFFE
      FBFCF6EF878685FF00FFFF00FF8E8888787BD42024B1BDB2A9A3A3A4FEFEFFFF
      FFFFBCBCBD353537353537353537FFFEFDFCF9F5878685FF00FFFF00FF8E8C8E
      787EDA2024B1B4B3B9A3A3A4FEFEFEE9E9E9E2E2E2BCBCBCA2A2A3DFDFE0FFFF
      FFFCFBFB878685FF00FFFF00FF8E8F947880DE2023B1B4B3B9A3A3A4FFFFFFA5
      A5A5BBBBBB9F9FA0C6C6C7C5C5C6E2E2E3FEFEFE878685FF00FFFF00FF8E8F95
      7880DF2023B1B4B3B9A3A3A4FFFFFFB9B9B9B3B3B4ACACADA8A8A9B9B9BAE0E0
      E1FFFFFF878685FF00FFFF00FF9091977B83E22023B0B6B5B6A3A3A4FFFFFF79
      7979E1E2E2FBFBFCEDEDEDB9B9B9DBDBDCFFFFFF878685FF00FFFF00FF83848A
      646AD21D23BA6F71919E9DAFBBB9BC7F7D738D8C88EDEAE0E7E7E36465629090
      90FFFFFF878685FF00FFFF00FFFF00FF4141632E317D3034833F43933A3D9440
      42964444605655996D6893736E885E5B61918D8A616161FF00FFFF00FFFF00FF
      FF00FFFF00FFFF00FF4B4B4BFF00FF3B3D624B4B4B373765FF00FF3333833838
      65141477FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4B4B4B4B
      4B4BFF00FFFF00FF4B4B4E4C4C4CFF00FFFF00FFFF00FFFF00FF}
    EditButton.Style = ebsGlyphEh
    EditButton.Width = 18
    EditButtons = <>
    Flat = True
    Kind = dtkDateTimeEh
    TabOrder = 1
    Visible = True
  end
  object btnCancel: TBitBtn
    Left = 180
    Top = 114
    Width = 85
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 6
    Kind = bkCancel
  end
  object lcSup: TDBLookupComboboxEh
    Left = 96
    Top = 44
    Width = 161
    Height = 19
    DropDownBox.ColumnDefValues.Title.TitleButton = True
    DropDownBox.Rows = 15
    DropDownBox.Width = -1
    EditButtons = <>
    Flat = True
    KeyField = 'COMPID'
    ListField = 'NAME'
    ListSource = dsrComp
    TabOrder = 2
    Visible = True
  end
  object lcNDS: TDBLookupComboboxEh
    Left = 96
    Top = 84
    Width = 161
    Height = 19
    DropDownBox.ColumnDefValues.Title.TitleButton = True
    DropDownBox.Width = -1
    EditButtons = <>
    Flat = True
    KeyField = 'NDSID'
    ListField = 'LONGNAME'
    ListSource = dm.dsrNDS
    TabOrder = 4
    Visible = True
  end
  object lcDep: TDBLookupComboboxEh
    Left = 96
    Top = 64
    Width = 161
    Height = 19
    DropDownBox.ColumnDefValues.Title.TitleButton = True
    DropDownBox.Rows = 15
    DropDownBox.Width = -1
    EditButtons = <>
    Flat = True
    KeyField = 'DEPID'
    ListField = 'NAME'
    ListSource = dsrWhDep
    TabOrder = 3
    Visible = True
  end
  object taWhDep: TpFIBDataSet
    SelectSQL.Strings = (
      'select DEPID, NAME'
      'from D_DEP'
      'where bit(DEPTYPES,1)=1 and'
      '      SELFCOMPID between :SELFCOMPID1 and :SELFCOMPID2'
      ''
      'union'
      '-- '#1089#1082#1083#1072#1076#1099' '#1075#1086#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
      'select DEPID, NAME'
      'from D_DEP'
      'where bit(DEPTYPES,2)=1'
      '')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 224
    Top = 8
    poSQLINT64ToBCD = True
  end
  object dsrWhDep: TDataSource
    DataSet = taWhDep
    Left = 224
    Top = 52
  end
  object taComp: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from D_Comp'
      'order by SORTIND')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 168
    Top = 8
    poSQLINT64ToBCD = True
  end
  object dsrComp: TDataSource
    DataSet = taComp
    Left = 168
    Top = 52
  end
  object sqlInvCopy: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'Execute procedure '
      
        'COPY_INV(:INVID,:OUTTYPE,:DOCNO, :DEPID,:FROMDEPID,:SUPID,:DOCDA' +
        'TE,:NDSID, :USERID)')
    Left = 117
    Top = 8
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
