unit frmSpoilage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, FIBDataSet, cxControls,
  cxGridCustomView, pFIBDataSet, cxClasses, cxGridLevel, cxGrid,
  cxLookAndFeelPainters, ExtCtrls, StdCtrls, cxButtons;

type
  TDialogNormLosses = class(TForm)
    GridLevel: TcxGridLevel;
    Grid: TcxGrid;
    DataSource: TDataSource;
    DataSet: TpFIBDataSet;
    DataSetMATERIALID: TFIBIntegerField;
    DataSetBEGINDATE: TFIBDateField;
    DataSetNORM: TFIBFloatField;
    GridDBTableView: TcxGridDBTableView;
    DataSetMaterial: TpFIBDataSet;
    DataSetMaterialID2: TFIBIntegerField;
    DataSetMaterial2: TStringField;
    DataSetMaterialTITLE: TFIBStringField;
    GridDBTableViewBEGINDATE: TcxGridDBColumn;
    GridDBTableViewNORM: TcxGridDBColumn;
    GridDBTableViewMaterial: TcxGridDBColumn;
    DataSetCOMPANYID: TFIBIntegerField;
    ButtonClose: TcxButton;
    Bevel: TBevel;
    procedure DataSetNewRecord(DataSet: TDataSet);
    procedure DataSetBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure Execute;
  end;


implementation

uses DictData;

{$R *.dfm}

procedure TDialogNormLosses.DataSetNewRecord(DataSet: TDataSet);
begin
  DataSetCOMPANYID.AsInteger := dm.User.SelfCompId;
  DataSetBEGINDATE.AsDateTime := Date;
  DataSetMATERIALID.AsInteger := 1;
end;

class procedure TDialogNormLosses.Execute;
var
  DialogNormLosses: TDialogNormLosses;
begin
  DialogNormLosses := TDialogNormLosses.Create(Application);
  DialogNormLosses.ShowModal;
  DialogNormLosses.Free;
end;

procedure TDialogNormLosses.DataSetBeforeOpen(DataSet: TDataSet);
begin
  Self.DataSet.ParamByName('Company$ID').AsInteger := dm.User.SelfCompId;
end;

end.
