unit CrudeStore;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, FIBDataSet, pFIBDataSet, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGridCustomPopupMenu, cxGridPopupMenu, Menus,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxPSCore, dxPScxCommon,
  dxPScxGrid6Lnk;

type
  TfmCrudeStore = class(TForm)
    Level: TcxGridLevel;
    Grid: TcxGrid;
    taCrudeStore: TpFIBDataSet;
    dsCrudeStore: TDataSource;
    taCrudeStoreMOVEMENTTYPENAME: TFIBStringField;
    taCrudeStoreMOVEMENTSUBTYPENAME: TFIBStringField;
    taCrudeStoreTOTALWEIGHT: TFIBBCDField;
    taCrudeStoreOPENEDINVOICECOUNT: TFIBSmallIntField;
    taCrudeStoreOPENEDINVOICENUMBERS: TFIBStringField;
    taCrudeStoreOPENEDINVOICEDATES: TFIBStringField;
    View: TcxGridDBBandedTableView;
    ViewMOVEMENTTYPENAME: TcxGridDBBandedColumn;
    ViewMOVEMENTSUBTYPENAME: TcxGridDBBandedColumn;
    ViewTOTALWEIGHT: TcxGridDBBandedColumn;
    ViewOPENEDINVOICECOUNT: TcxGridDBBandedColumn;
    ViewOPENEDINVOICENUMBERS: TcxGridDBBandedColumn;
    ViewOPENEDINVOICEDATES: TcxGridDBBandedColumn;
    taCrudeStoreTOTALWEIGHTPURE: TFIBBCDField;
    ViewTOTALWEIGHTPURE: TcxGridDBBandedColumn;
    GridPopupMenu: TPopupMenu;
    N1: TMenuItem;
    dxPrinter: TdxComponentPrinter;
    dxPrinterLink: TdxGridReportLink;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    procedure taCrudeStoreBeforeOpen(DataSet: TDataSet);
    procedure taCrudeStoreCalcFields(DataSet: TDataSet);
    procedure N1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    MaterialID: Integer;
  public
    class procedure Execute(AMaterialID: integer; ADateTime: TDateTime);
  end;

var
  fmCrudeStore: TfmCrudeStore;

implementation

{$R *.dfm}

uses DictData, Math, DateUtils;


class procedure TfmCrudeStore.Execute(AMaterialID: Integer; ADateTime: TDateTime);
var
  CompanyName: String;
begin

  fmCrudeStore := nil;

  try

    fmCrudeStore := TfmCrudeStore.Create(Application);

    fmCrudeStore.MaterialID := AMaterialID;

    CompanyName := dm.db.QueryValue('select first 1 name from d_comp where compid = :compid', 0, [dm.User.SelfCompId]);

    CompanyName := trim(CompanyName);

    fmCrudeStore.Caption := fmCrudeStore.Caption + CompanyName;

    fmCrudeStore.Caption := fmCrudeStore.Caption + ' ' + DateToStr(ADateTime);

    fmCrudeStore.taCrudeStore.ParamByName('Current$Date').AsDateTime := ADateTime;

    fmCrudeStore.taCrudeStore.Active := true;

    fmCrudeStore.ShowModal;

  finally

    FreeAndNil(fmCrudeStore);

  end;

end;

procedure TfmCrudeStore.FormShow(Sender: TObject);
begin

    if MaterialID = 1 then
    begin

      Self.Caption := fmCrudeStore.Caption + ' (������)';

      viewTotalWeight.Caption := viewTotalWeight.Caption + ' (Au 585)';

      viewTotalWeightPure.Caption := viewTotalWeightPure.Caption + ' (Au 1000)';

    end;

    if MaterialID = 3 then
    begin

      Self.Caption := fmCrudeStore.Caption + ' (�������)';

      viewTotalWeight.Caption := viewTotalWeight.Caption + ' (Ag 925)';

      viewTotalWeightPure.Caption := viewTotalWeightPure.Caption + ' (Ag 1000)';

    end;

   View.DataController.Groups.FullExpand;

end;

procedure TfmCrudeStore.N1Click(Sender: TObject);
var
  Link : TdxGridReportLink;
begin

  View.Bands[1].Visible := false;

  Link := TdxGridReportLink(dxPrinter.LinkByName('dxPrinterLink'));

  Link.ReportTitle.Text := fmCrudeStore.Caption;

  Link.ReportDocument.Caption := Link.ReportTitle.Text;

  Link.ReportDocument.CreationDate := Date;

  Link.ReportDocument.Creator := dm.User.FIO;

  Link.Preview;

  View.Bands[1].Visible := true;

end;

procedure TfmCrudeStore.taCrudeStoreBeforeOpen(DataSet: TDataSet);
begin
  taCrudeStore.ParamByName('Material$ID').AsInteger := MaterialID;
end;

procedure TfmCrudeStore.taCrudeStoreCalcFields(DataSet: TDataSet);
var
  PureKoefficient: Currency;
begin

  if MaterialID = 1 then
  begin
    PureKoefficient := 585;
  end;

  if MaterialID = 3 then
  begin
    PureKoefficient := 925;
  end;

  taCrudeStoreTOTALWEIGHTPURE.AsCurrency := RoundTo((taCrudeStoreTOTALWEIGHT.AsCurrency * PureKoefficient) / 1000, -3);

end;

end.
