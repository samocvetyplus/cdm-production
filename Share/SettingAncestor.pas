unit SettingAncestor; 

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, DBCtrls, StdCtrls, RxToolEdit, Mask,
  RxDBComb, db, Menus, Buttons, ImgList, rxPlacemnt;

type
  TfmAncestorSetting = class(TForm)
    plSetup: TPanel;
    splt: TSplitter;
    fmstr: TFormStorage;
    lstvSetting: TListView;
    lbCase: TLabel;
    stbrSetting: TStatusBar;
    ilSetting: TImageList;
    procedure FormCreate(Sender: TObject);
    procedure lstvSettingClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  public
    constructor Create(AOwner : TComponent); override;
  end;

(* ��� ���������� ����� ��������� ��������� ��������� ����������
     1. � lstvSetting �������� �������
     2. � ����������� � ����� ��������� ������������ ������ TextNode
     3. ���� Tag ����������� ������� ���������� �������� ������ ������� � �������
*)


var
  SettingTextNodes : array of string;

implementation

{$R *.DFM}

uses fmUtils;

{ TfmSetting }

procedure TfmAncestorSetting.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then MinimizeApp
  else inherited;
end;

procedure TfmAncestorSetting.FormCreate(Sender: TObject);
var
  i : integer;
begin
  (* ��������� ������ �� SettingTextNodes *)
  lstvSetting.Items.BeginUpdate;
  try
    lstvSetting.Items.Clear;
    for i:=Low(SettingTextNodes) to High(SettingTextNodes) do
      lstvSetting.Items.Add.Caption := SettingTextNodes[i];
  finally
    lstvSetting.Items.EndUpdate;
  end;

  with plSetup do
    for i:=0 to Pred(ControlCount) do
      if Controls[i].InheritsFrom(TPanel) then
        if Controls[i].Tag = 1 then
        begin
          Controls[i].Visible := True;
          Controls[i].Align := alClient;
        end
        else Controls[i].Visible := False;
end;

procedure TfmAncestorSetting.lstvSettingClick(Sender: TObject);
  function Find(s : string) : integer;
  var
    i : integer;
  begin
    Result := -1;
    for i := Low(SettingTextNodes) to High(SettingTextNodes) do
      if SettingTextNodes[i] = s then
      begin
        Result := i+1;
        break;
      end;
  end;
var
  Ind, i : integer;
begin
  if not Assigned(lstvSetting.Selected) then eXit;
  Ind := Find(lstvSetting.Selected.Caption);
  with plSetup do
    for i:=0 to Pred(ControlCount) do
      if Controls[i].InheritsFrom(TPanel) then
        if Controls[i].Tag = Ind then
        begin
          TCustomPanel(Controls[i]).Visible := True;
          TCustomPanel(Controls[i]).Align := alClient;
        end
        else TCustomPanel(Controls[i]).Visible := False;
end;

constructor TfmAncestorSetting.Create(AOwner: TComponent);
begin
  inherited;
end;

procedure TfmAncestorSetting.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree; 
end;

end.
