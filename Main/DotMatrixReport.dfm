object fmDotMatrixReport: TfmDotMatrixReport
  Left = 131
  Top = 144
  Width = 827
  Height = 421
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object zrInv16In: TZReport
    Left = 16
    Top = 8
    Width = 80
    Height = 26
    DataSet = dmPrint.quInv16Item
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Options.Escapes.Model = emCustom
    Options.Escapes.Values = (
      ''
      ''
      ''
      ''
      #15
      #18
      #27'E'
      #27'F'
      #27'4'
      #27'5'
      ''
      ''
      #27'S0'
      #27'T'
      #27'S1'
      #27'T'
      #27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'
      #10#10#10#10#10#10#10#10#10#10#10#10#10
      ''
      '')
    Options.PageFrom = 1
    Options.PageTo = 1
    Options.PaperType = zptContinuous
    Options.PreviewMode = zpmWholeReport
    Margins.Left = 4
    Margins.Top = 0
    PrintIfEmpty = True
    Minimized = True
    object zrvRecNo: TZRField
      Format.Width = 10
      DataField = 'RecNo'
      DataSet = dmPrint.quInv16Item
    end
    object zrvSemisName: TZRField
      Format.Width = 40
      DataField = 'SEMIS'
      DataSet = dmPrint.quInv16Item
    end
    object zrvQ: TZRField
      Format.Width = 10
      DataField = 'Q'
      DataSet = dmPrint.quInv16Item
    end
    object zrvW: TZRField
      Format.Width = 10
      DataField = 'W'
      DataSet = dmPrint.quInv16Item
    end
    object zrvOperName: TZRField
      Format.Width = 60
      DataField = 'OPERATION'
      DataSet = dmPrint.quInv16Item
    end
    object zrvUQName: TZRField
      Format.Width = 10
      DataField = 'UQNAME'
      DataSet = dmPrint.quInv16Item
    end
    object zrvUWName: TZRField
      Format.Width = 10
      DataField = 'UWNAME'
      DataSet = dmPrint.quInv16Item
    end
    object zrvItType: TZRField
      Format.Width = 10
      DataField = 'ITTYPE'
      DataSet = dmPrint.quInv16Item
    end
    object zrvItTypeName: TZRField
      Format.Width = 10
      DataField = 'ITTYPENAME'
      DataSet = dmPrint.quInv16Item
    end
    object zrInv16InGroup: TZRGroup
    end
    object zrInv16InHeader: TZRBand
      Left = 4
      Top = 10
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtHeader
    end
    object zrInv16InSubDetail: TZRSubDetail
      Left = 4
      Top = 11
      Width = 75
      Height = 3
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 0
      object zrvDocNo: TZRField
        Format.Width = 10
        DataField = 'DOCNO'
        DataSet = dmPrint.quInv16
      end
      object zrvWOrderNo: TZRField
        Format.Width = 10
        DataField = 'WNO'
        DataSet = dmPrint.quInv16
      end
      object zrvDocDate: TZRField
        Format.Width = 18
        DataField = 'DOCDATE'
        DataSet = dmPrint.quInv16
      end
      object zrvDepName: TZRField
        Format.Width = 40
        DataField = 'DEPNAME'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel1: TZRLabel
        Left = 11
        Top = 2
        Width = 6
        Height = 1
        Caption = #1057#1082#1083#1072#1076':'
      end
      object ZRLabel9: TZRLabel
        Left = 18
        Top = 2
        Width = 7
        Height = 1
        AutoSize = zasWidth
        Caption = 'ZRLabel9'
        Variable = zrvDepName
      end
      object ZRLabel7: TZRLabel
        Left = 1
        Top = 0
        Width = 7
        Height = 1
        Caption = #1053#1072#1088#1103#1076' '#8470
      end
      object ZRLabel8: TZRLabel
        Left = 9
        Top = 0
        Width = 8
        Height = 1
        Caption = 'ZRLabel8'
        Variable = zrvWOrderNo
      end
      object ZRLabel3: TZRLabel
        Left = 11
        Top = 1
        Width = 11
        Height = 1
        Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
      end
      object ZRLabel4: TZRLabel
        Left = 23
        Top = 1
        Width = 7
        Height = 1
        Variable = zrvDocNo
      end
      object ZRLabel5: TZRLabel
        Left = 31
        Top = 1
        Width = 2
        Height = 1
        Caption = #1086#1090
      end
      object ZRLabel6: TZRLabel
        Left = 34
        Top = 1
        Width = 10
        Height = 1
        Variable = zrvDocDate
      end
      object ZRLabel2: TZRLabel
        Left = 48
        Top = 0
        Width = 11
        Height = 1
        Caption = #1060#1086#1088#1084#1072' '#1057#1070'-16'
      end
    end
    object zrInv16InColumnHeader: TZRBand
      Left = 4
      Top = 14
      Width = 75
      Height = 5
      Frame.Left = 1
      Frame.Top = 1
      Frame.Right = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnHeader
      object ZRLabel11: TZRLabel
        Left = 1
        Top = 1
        Width = 24
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Alignment.Y = zahCenter
        Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090', '#13#10#1084#1072#1090#1077#1088#1080#1072#1083
      end
      object ZRLabel12: TZRLabel
        Left = 35
        Top = 1
        Width = 6
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1050#1086#1083'-'#13#10#1074#1086
      end
      object ZRLabel16: TZRLabel
        Left = 41
        Top = 1
        Width = 9
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1052#1072#1089#1089#1072
      end
      object ZRLabel22: TZRLabel
        Left = 50
        Top = 1
        Width = 19
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
      end
      object ZRLabel10: TZRLabel
        Left = 30
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1074#1077#1089#1072
      end
      object ZRLabel13: TZRLabel
        Left = 25
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1082'-'#1074#1072
      end
      object ZRLabel211: TZRLabel
        Left = 69
        Top = 1
        Width = 3
        Height = 3
        Caption = #1058#1080#1087
      end
    end
    object zrInv16InDetail: TZRBand
      Left = 4
      Top = 19
      Width = 75
      Height = 1
      Frame.Left = 1
      Frame.Right = 1
      Stretch = False
      BandType = zbtDetail
      object ZRLabel14: TZRLabel
        Left = 1
        Top = 0
        Width = 24
        Height = 1
        Frame.Right = 1
        Variable = zrvSemisName
      end
      object ZRLabel15: TZRLabel
        Left = 35
        Top = 0
        Width = 6
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = zrvQ
      end
      object ZRLabel17: TZRLabel
        Left = 41
        Top = 0
        Width = 9
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = zrvW
      end
      object ZRLabel23: TZRLabel
        Left = 50
        Top = 0
        Width = 19
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        AutoSize = zasHeight
        Variable = zrvOperName
      end
      object ZRLabel24: TZRLabel
        Left = 25
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = zrvUQName
      end
      object ZRLabel25: TZRLabel
        Left = 30
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = zrvUWName
      end
      object ZRLabel212: TZRLabel
        Left = 69
        Top = 0
        Width = 3
        Height = 1
        Variable = zrvItTypeName
      end
    end
    object zrInv16InColumnFooter: TZRBand
      Left = 4
      Top = 20
      Width = 75
      Height = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnFooter
    end
    object zrInv16InFooter: TZRBand
      Left = 4
      Top = 21
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtFooter
    end
    object zrInv16InSubDetail0: TZRSubDetail
      Left = 4
      Top = 22
      Width = 75
      Height = 3
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 5
      object zrvJobFace: TZRField
        Format.Width = 60
        DataField = 'JOBFACE'
        DataSet = dmPrint.quInv16
      end
      object zrvMolFace: TZRField
        Format.Width = 60
        DataField = 'MOLID'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel18: TZRLabel
        Left = 1
        Top = 0
        Width = 27
        Height = 1
        Caption = #1042#1099#1076#1072#1083'('#1072') ________________'
      end
      object ZRLabel19: TZRLabel
        Left = 31
        Top = 1
        Width = 30
        Height = 1
        Variable = zrvJobFace
      end
      object ZRLabel20: TZRLabel
        Left = 31
        Top = 0
        Width = 30
        Height = 1
        Caption = #1055#1088#1080#1085#1103#1083'('#1072') ________________________'
      end
      object ZRLabel21: TZRLabel
        Left = 1
        Top = 1
        Width = 27
        Height = 1
        Variable = zrvMolFace
      end
    end
  end
  object zrInv16Out: TZReport
    Left = 64
    Top = 8
    Width = 80
    Height = 26
    DataSet = dmPrint.quInv16Item
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Options.Escapes.Model = emCustom
    Options.Escapes.Values = (
      ''
      ''
      ''
      ''
      #15
      #18
      #27'E'
      #27'F'
      #27'4'
      #27'5'
      ''
      ''
      #27'S0'
      #27'T'
      #27'S1'
      #27'T'
      #27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'
      #10#10#10#10#10#10#10#10#10#10#10#10#10
      ''
      '')
    Options.Font.Style = zfsElite
    Options.PageFrom = 1
    Options.PageTo = 1
    Options.PaperType = zptContinuous
    Options.PreviewMode = zpmWholeReport
    Margins.Left = 4
    Minimized = True
    object ZRField14: TZRField
      Format.Width = 10
      DataField = 'RecNo'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField15: TZRField
      Format.Width = 40
      DataField = 'SEMIS'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField16: TZRField
      Format.Width = 10
      DataField = 'Q'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField17: TZRField
      Format.Width = 10
      DataField = 'W'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField18: TZRField
      Format.Width = 60
      DataField = 'OPERATION'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField19: TZRField
      Format.Width = 10
      DataField = 'UQNAME'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField20: TZRField
      Format.Width = 10
      DataField = 'UWNAME'
      DataSet = dmPrint.quInv16Item
    end
    object zrvItTypeNameOut: TZRField
      Format.Width = 2
      DataField = 'ITTYPENAME'
      DataSet = dmPrint.quInv16Item
    end
    object ZRGroup2: TZRGroup
    end
    object ZRBand6: TZRBand
      Left = 4
      Top = 11
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtHeader
    end
    object ZRSubDetail3: TZRSubDetail
      Left = 4
      Top = 12
      Width = 75
      Height = 3
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 0
      object ZRField21: TZRField
        Format.Width = 10
        DataField = 'DOCNO'
        DataSet = dmPrint.quInv16
      end
      object ZRField22: TZRField
        Format.Width = 10
        DataField = 'WNO'
        DataSet = dmPrint.quInv16
      end
      object ZRField23: TZRField
        Format.Width = 18
        DataField = 'DOCDATE'
        DataSet = dmPrint.quInv16
      end
      object ZRField24: TZRField
        Format.Width = 40
        DataField = 'DEPNAME'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel51: TZRLabel
        Left = 11
        Top = 2
        Width = 6
        Height = 1
        Caption = #1057#1082#1083#1072#1076':'
      end
      object ZRLabel52: TZRLabel
        Left = 18
        Top = 2
        Width = 7
        Height = 1
        AutoSize = zasWidth
        Caption = 'ZRLabel9'
        Variable = ZRField24
      end
      object ZRLabel53: TZRLabel
        Left = 1
        Top = 0
        Width = 7
        Height = 1
        Caption = #1053#1072#1088#1103#1076' '#8470
      end
      object ZRLabel54: TZRLabel
        Left = 9
        Top = 0
        Width = 8
        Height = 1
        Caption = 'ZRLabel8'
        Variable = ZRField22
      end
      object ZRLabel55: TZRLabel
        Left = 11
        Top = 1
        Width = 11
        Height = 1
        Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
      end
      object ZRLabel56: TZRLabel
        Left = 23
        Top = 1
        Width = 7
        Height = 1
        Variable = ZRField21
      end
      object ZRLabel57: TZRLabel
        Left = 31
        Top = 1
        Width = 2
        Height = 1
        Caption = #1086#1090
      end
      object ZRLabel58: TZRLabel
        Left = 34
        Top = 1
        Width = 10
        Height = 1
        Variable = ZRField23
      end
      object ZRLabel59: TZRLabel
        Left = 48
        Top = 0
        Width = 11
        Height = 1
        Caption = #1060#1086#1088#1084#1072' '#1057#1070'-16'
      end
    end
    object ZRBand7: TZRBand
      Left = 4
      Top = 15
      Width = 75
      Height = 5
      Frame.Left = 1
      Frame.Top = 1
      Frame.Right = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnHeader
      object ZRLabel60: TZRLabel
        Left = 1
        Top = 1
        Width = 24
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Alignment.Y = zahCenter
        Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090', '#13#10#1084#1072#1090#1077#1088#1080#1072#1083
      end
      object ZRLabel61: TZRLabel
        Left = 35
        Top = 1
        Width = 6
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1050#1086#1083'-'#13#10#1074#1086
      end
      object ZRLabel62: TZRLabel
        Left = 41
        Top = 1
        Width = 9
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1052#1072#1089#1089#1072
      end
      object ZRLabel63: TZRLabel
        Left = 50
        Top = 1
        Width = 19
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
      end
      object ZRLabel64: TZRLabel
        Left = 30
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1074#1077#1089#1072
      end
      object ZRLabel65: TZRLabel
        Left = 25
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1082'-'#1074#1072
      end
      object ZRLabel213: TZRLabel
        Left = 69
        Top = 1
        Width = 3
        Height = 3
        Caption = #1058#1080#1087
      end
    end
    object ZRBand8: TZRBand
      Left = 4
      Top = 20
      Width = 75
      Height = 1
      Frame.Left = 1
      Frame.Right = 1
      Stretch = False
      BandType = zbtDetail
      object ZRLabel66: TZRLabel
        Left = 1
        Top = 0
        Width = 24
        Height = 1
        Frame.Right = 1
        Variable = ZRField15
      end
      object ZRLabel67: TZRLabel
        Left = 35
        Top = 0
        Width = 6
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = ZRField16
      end
      object ZRLabel68: TZRLabel
        Left = 41
        Top = 0
        Width = 9
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = ZRField17
      end
      object ZRLabel69: TZRLabel
        Left = 50
        Top = 0
        Width = 19
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        AutoSize = zasHeight
        Variable = ZRField18
      end
      object ZRLabel70: TZRLabel
        Left = 25
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = ZRField19
      end
      object ZRLabel71: TZRLabel
        Left = 30
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = ZRField20
      end
      object ZRLabel214: TZRLabel
        Left = 69
        Top = 0
        Width = 3
        Height = 1
        Variable = zrvItTypeNameOut
      end
    end
    object ZRBand9: TZRBand
      Left = 4
      Top = 21
      Width = 75
      Height = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnFooter
    end
    object ZRBand10: TZRBand
      Left = 4
      Top = 22
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtFooter
    end
    object ZRSubDetail4: TZRSubDetail
      Left = 4
      Top = 23
      Width = 75
      Height = 2
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 5
      object ZRField25: TZRField
        Format.Width = 60
        DataField = 'JOBFACE'
        DataSet = dmPrint.quInv16
      end
      object ZRField26: TZRField
        Format.Width = 60
        DataField = 'MOLID'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel72: TZRLabel
        Left = 1
        Top = 0
        Width = 27
        Height = 1
        Caption = #1055#1088#1080#1085#1103#1083'('#1072') ________________'
      end
      object ZRLabel73: TZRLabel
        Left = 31
        Top = 1
        Width = 30
        Height = 1
        Variable = ZRField25
      end
      object ZRLabel74: TZRLabel
        Left = 31
        Top = 0
        Width = 30
        Height = 1
        Caption = #1057#1076#1072#1083'('#1072') ________________________'
      end
      object ZRLabel75: TZRLabel
        Left = 1
        Top = 1
        Width = 27
        Height = 1
        Variable = ZRField26
      end
    end
    object ZReport3: TZReport
      Left = -494
      Top = -319
      Width = 75
      Height = 26
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16Item
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      Options.Escapes.Model = emEpsonLXSeries
      Options.Font.Style = zfsElite
      Options.PageFrom = 1
      Options.PageTo = 1
      Options.PaperType = zptContinuous
      Options.PreviewMode = zpmWholeReport
      Margins.Left = 4
      Minimized = True
      object ZRField27: TZRField
        Format.Width = 10
        DataField = 'RecNo'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField28: TZRField
        Format.Width = 40
        DataField = 'SEMIS'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField29: TZRField
        Format.Width = 10
        DataField = 'Q'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField30: TZRField
        Format.Width = 10
        DataField = 'W'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField31: TZRField
        Format.Width = 60
        DataField = 'OPERATION'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField32: TZRField
        Format.Width = 10
        DataField = 'UQNAME'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField33: TZRField
        Format.Width = 10
        DataField = 'UWNAME'
        DataSet = dmPrint.quInv16Item
      end
      object ZRGroup3: TZRGroup
      end
      object ZRBand11: TZRBand
        Left = 4
        Top = 11
        Width = 70
        Height = 1
        Stretch = False
        BandType = zbtHeader
      end
      object ZRSubDetail5: TZRSubDetail
        Left = 4
        Top = 12
        Width = 70
        Height = 3
        DataOptions.AutoOpen = True
        DataSet = dmPrint.quInv16
        Stretch = False
        MasterIndex = 0
        object ZRField34: TZRField
          Format.Width = 10
          DataField = 'DOCNO'
          DataSet = dmPrint.quInv16
        end
        object ZRField35: TZRField
          Format.Width = 10
          DataField = 'WNO'
          DataSet = dmPrint.quInv16
        end
        object ZRField36: TZRField
          Format.Width = 18
          DataField = 'DOCDATE'
          DataSet = dmPrint.quInv16
        end
        object ZRField37: TZRField
          Format.Width = 40
          DataField = 'DEPNAME'
          DataSet = dmPrint.quInv16
        end
        object ZRLabel76: TZRLabel
          Left = 11
          Top = 2
          Width = 6
          Height = 1
          Caption = #1057#1082#1083#1072#1076':'
        end
        object ZRLabel77: TZRLabel
          Left = 18
          Top = 2
          Width = 7
          Height = 1
          AutoSize = zasWidth
          Caption = 'ZRLabel9'
          Variable = ZRField37
        end
        object ZRLabel78: TZRLabel
          Left = 1
          Top = 0
          Width = 7
          Height = 1
          Caption = #1053#1072#1088#1103#1076' '#8470
        end
        object ZRLabel79: TZRLabel
          Left = 9
          Top = 0
          Width = 8
          Height = 1
          Caption = 'ZRLabel8'
          Variable = ZRField35
        end
        object ZRLabel80: TZRLabel
          Left = 11
          Top = 1
          Width = 11
          Height = 1
          Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
        end
        object ZRLabel81: TZRLabel
          Left = 23
          Top = 1
          Width = 7
          Height = 1
          Variable = ZRField34
        end
        object ZRLabel82: TZRLabel
          Left = 31
          Top = 1
          Width = 2
          Height = 1
          Caption = #1086#1090
        end
        object ZRLabel83: TZRLabel
          Left = 34
          Top = 1
          Width = 10
          Height = 1
          Variable = ZRField36
        end
        object ZRLabel84: TZRLabel
          Left = 48
          Top = 0
          Width = 11
          Height = 1
          Caption = #1060#1086#1088#1084#1072' '#1057#1070'-16'
        end
      end
      object ZRBand12: TZRBand
        Left = 4
        Top = 15
        Width = 70
        Height = 5
        Frame.Left = 1
        Frame.Top = 1
        Frame.Right = 1
        Frame.Bottom = 1
        Stretch = False
        BandType = zbtColumnHeader
        object ZRLabel85: TZRLabel
          Left = 1
          Top = 1
          Width = 24
          Height = 3
          Frame.Right = 1
          Alignment.X = zawCenter
          Alignment.Y = zahCenter
          Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090', '#13#10#1084#1072#1090#1077#1088#1080#1072#1083
        end
        object ZRLabel86: TZRLabel
          Left = 35
          Top = 1
          Width = 6
          Height = 3
          Frame.Right = 1
          Alignment.X = zawCenter
          Caption = #1050#1086#1083'-'#13#10#1074#1086
        end
        object ZRLabel87: TZRLabel
          Left = 41
          Top = 1
          Width = 8
          Height = 3
          Frame.Right = 1
          Alignment.X = zawCenter
          Caption = #1052#1072#1089#1089#1072
        end
        object ZRLabel88: TZRLabel
          Left = 49
          Top = 1
          Width = 20
          Height = 3
          Alignment.X = zawCenter
          Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        end
        object ZRLabel89: TZRLabel
          Left = 30
          Top = 1
          Width = 5
          Height = 3
          Frame.Right = 1
          Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1074#1077#1089#1072
        end
        object ZRLabel90: TZRLabel
          Left = 25
          Top = 1
          Width = 5
          Height = 3
          Frame.Right = 1
          Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1082'-'#1074#1072
        end
      end
      object ZRBand13: TZRBand
        Left = 4
        Top = 20
        Width = 70
        Height = 1
        Frame.Left = 1
        Frame.Right = 1
        Stretch = False
        BandType = zbtDetail
        object ZRLabel91: TZRLabel
          Left = 1
          Top = 0
          Width = 24
          Height = 1
          Frame.Right = 1
          Variable = ZRField28
        end
        object ZRLabel92: TZRLabel
          Left = 35
          Top = 0
          Width = 6
          Height = 1
          Frame.Right = 1
          Alignment.X = zawRight
          Variable = ZRField29
        end
        object ZRLabel93: TZRLabel
          Left = 41
          Top = 0
          Width = 8
          Height = 1
          Frame.Right = 1
          Alignment.X = zawRight
          Variable = ZRField30
        end
        object ZRLabel94: TZRLabel
          Left = 49
          Top = 0
          Width = 20
          Height = 1
          Alignment.X = zawRight
          AutoSize = zasHeight
          Variable = ZRField31
        end
        object ZRLabel95: TZRLabel
          Left = 25
          Top = 0
          Width = 5
          Height = 1
          Frame.Right = 1
          Variable = ZRField32
        end
        object ZRLabel96: TZRLabel
          Left = 30
          Top = 0
          Width = 5
          Height = 1
          Frame.Right = 1
          Variable = ZRField33
        end
      end
      object ZRBand14: TZRBand
        Left = 4
        Top = 21
        Width = 70
        Height = 1
        Frame.Bottom = 1
        Stretch = False
        BandType = zbtColumnFooter
      end
      object ZRBand15: TZRBand
        Left = 4
        Top = 22
        Width = 70
        Height = 1
        Stretch = False
        BandType = zbtFooter
      end
      object ZRSubDetail6: TZRSubDetail
        Left = 4
        Top = 23
        Width = 70
        Height = 2
        DataOptions.AutoOpen = True
        DataSet = dmPrint.quInv16
        Stretch = False
        MasterIndex = 5
        object ZRField38: TZRField
          Format.Width = 60
          DataField = 'JOBFACE'
          DataSet = dmPrint.quInv16
        end
        object ZRField39: TZRField
          Format.Width = 60
          DataField = 'MOLID'
          DataSet = dmPrint.quInv16
        end
        object ZRLabel97: TZRLabel
          Left = 1
          Top = 0
          Width = 27
          Height = 1
          Caption = #1042#1099#1076#1072#1083'('#1072') ________________'
        end
        object ZRLabel98: TZRLabel
          Left = 31
          Top = 1
          Width = 30
          Height = 1
          Variable = ZRField38
        end
        object ZRLabel99: TZRLabel
          Left = 31
          Top = 0
          Width = 30
          Height = 1
          Caption = #1055#1088#1080#1085#1103#1083'('#1072') ________________________'
        end
        object ZRLabel100: TZRLabel
          Left = 1
          Top = 1
          Width = 27
          Height = 1
          Variable = ZRField39
        end
      end
    end
  end
  object zrInv16InAssort: TZReport
    Left = 108
    Top = 8
    Width = 80
    Height = 28
    DataOptions.AutoOpen = True
    DataSet = dmPrint.quInv16Item
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Options.Escapes.Model = emCustom
    Options.Escapes.Values = (
      ''
      ''
      ''
      ''
      #15
      #18
      #27'E'
      #27'F'
      #27'4'
      #27'5'
      ''
      ''
      #27'S0'
      #27'T'
      #27'S1'
      #27'T'
      #27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'
      #10#10#10#10#10#10#10#10#10#10#10#10#10
      ''
      '')
    Options.Font.Style = zfsElite
    Options.PageFrom = 1
    Options.PageTo = 1
    Options.PaperType = zptContinuous
    Options.PreviewMode = zpmWholeReport
    Margins.Left = 4
    Minimized = True
    object ZRField1: TZRField
      Format.Width = 10
      DataField = 'RecNo'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField2: TZRField
      Format.Width = 40
      DataField = 'SEMIS'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField3: TZRField
      Format.Width = 10
      DataField = 'Q'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField4: TZRField
      Format.Width = 10
      DataField = 'W'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField5: TZRField
      Format.Width = 60
      DataField = 'OPERATION'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField6: TZRField
      Format.Width = 10
      DataField = 'UQNAME'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField7: TZRField
      Format.Width = 10
      DataField = 'UWNAME'
      DataSet = dmPrint.quInv16Item
    end
    object zrvField8: TZRField
      Format.Width = 2
      DataField = 'ITTYPENAME'
      DataSet = dmPrint.quInv16Item
    end
    object ZRGroup1: TZRGroup
    end
    object ZRBand1: TZRBand
      Left = 4
      Top = 11
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtHeader
    end
    object ZRSubDetail1: TZRSubDetail
      Left = 4
      Top = 12
      Width = 75
      Height = 3
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 0
      object ZRField8: TZRField
        Format.Width = 10
        DataField = 'DOCNO'
        DataSet = dmPrint.quInv16
      end
      object ZRField9: TZRField
        Format.Width = 10
        DataField = 'WNO'
        DataSet = dmPrint.quInv16
      end
      object ZRField10: TZRField
        Format.Width = 18
        DataField = 'DOCDATE'
        DataSet = dmPrint.quInv16
      end
      object ZRField11: TZRField
        Format.Width = 40
        DataField = 'DEPNAME'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel26: TZRLabel
        Left = 11
        Top = 2
        Width = 6
        Height = 1
        Caption = #1057#1082#1083#1072#1076':'
      end
      object ZRLabel27: TZRLabel
        Left = 18
        Top = 2
        Width = 7
        Height = 1
        AutoSize = zasWidth
        Caption = 'ZRLabel9'
        Variable = ZRField11
      end
      object ZRLabel28: TZRLabel
        Left = 1
        Top = 0
        Width = 7
        Height = 1
        Caption = #1053#1072#1088#1103#1076' '#8470
      end
      object ZRLabel29: TZRLabel
        Left = 9
        Top = 0
        Width = 8
        Height = 1
        Caption = 'ZRLabel8'
        Variable = ZRField9
      end
      object ZRLabel30: TZRLabel
        Left = 11
        Top = 1
        Width = 11
        Height = 1
        Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
      end
      object ZRLabel31: TZRLabel
        Left = 23
        Top = 1
        Width = 7
        Height = 1
        Variable = ZRField8
      end
      object ZRLabel32: TZRLabel
        Left = 31
        Top = 1
        Width = 2
        Height = 1
        Caption = #1086#1090
      end
      object ZRLabel33: TZRLabel
        Left = 34
        Top = 1
        Width = 10
        Height = 1
        Variable = ZRField10
      end
      object ZRLabel34: TZRLabel
        Left = 48
        Top = 0
        Width = 11
        Height = 1
        Caption = #1060#1086#1088#1084#1072' '#1057#1070'-16'
      end
    end
    object ZRBand2: TZRBand
      Left = 4
      Top = 15
      Width = 75
      Height = 5
      Frame.Left = 1
      Frame.Top = 1
      Frame.Right = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnHeader
      object ZRLabel35: TZRLabel
        Left = 1
        Top = 1
        Width = 24
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Alignment.Y = zahCenter
        Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090', '#13#10#1084#1072#1090#1077#1088#1080#1072#1083
      end
      object ZRLabel36: TZRLabel
        Left = 35
        Top = 1
        Width = 6
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1050#1086#1083'-'#13#10#1074#1086
      end
      object ZRLabel37: TZRLabel
        Left = 41
        Top = 1
        Width = 10
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1052#1072#1089#1089#1072
      end
      object ZRLabel38: TZRLabel
        Left = 51
        Top = 1
        Width = 19
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
      end
      object ZRLabel39: TZRLabel
        Left = 30
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1074#1077#1089#1072
      end
      object ZRLabel40: TZRLabel
        Left = 25
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1082'-'#1074#1072
      end
      object ZRLabel215: TZRLabel
        Left = 70
        Top = 1
        Width = 3
        Height = 3
        Caption = #1058#1080#1087
      end
    end
    object ZRBand3: TZRBand
      Left = 4
      Top = 20
      Width = 75
      Height = 1
      Frame.Left = 1
      Frame.Right = 1
      Stretch = False
      BandType = zbtDetail
      object ZRLabel41: TZRLabel
        Left = 1
        Top = 0
        Width = 24
        Height = 1
        Frame.Right = 1
        Variable = ZRField2
      end
      object ZRLabel42: TZRLabel
        Left = 35
        Top = 0
        Width = 6
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = ZRField3
      end
      object ZRLabel43: TZRLabel
        Left = 41
        Top = 0
        Width = 10
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = ZRField4
      end
      object ZRLabel44: TZRLabel
        Left = 51
        Top = 0
        Width = 19
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        AutoSize = zasHeight
        Variable = ZRField5
      end
      object ZRLabel45: TZRLabel
        Left = 25
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = ZRField6
      end
      object ZRLabel46: TZRLabel
        Left = 30
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = ZRField7
      end
      object ZRLabel219: TZRLabel
        Left = 70
        Top = 0
        Width = 3
        Height = 1
        Variable = zrvField8
      end
    end
    object zrInv16InAssortSubDetail: TZRSubDetail
      Left = 4
      Top = 21
      Width = 75
      Height = 1
      DataOptions.AutoOpen = True
      DataSet = dmPrint.taInv16Assort
      Frame.Left = 1
      Frame.Right = 1
      Stretch = False
      MasterIndex = 3
      object zrvArt: TZRField
        DataField = 'ART'
        DataSet = dmPrint.taInv16Assort
      end
      object zrvArt2: TZRField
        DataField = 'ART2'
        DataSet = dmPrint.taInv16Assort
      end
      object zrvName: TZRField
        DataField = 'NAME'
        DataSet = dmPrint.taInv16Assort
      end
      object zrvQArt: TZRField
        DataField = 'Q'
        DataSet = dmPrint.taInv16Assort
      end
      object ZRLabel101: TZRLabel
        Left = 2
        Top = 0
        Width = 10
        Height = 1
        Variable = zrvArt
      end
      object ZRLabel102: TZRLabel
        Left = 13
        Top = 0
        Width = 6
        Height = 1
        Caption = 'ZRLabel102'
        Variable = zrvArt2
      end
      object ZRLabel103: TZRLabel
        Left = 34
        Top = 0
        Width = 6
        Height = 1
        Alignment.X = zawRight
        Caption = 'ZRLabel103'
        Variable = zrvQArt
      end
      object ZRLabel104: TZRLabel
        Left = 20
        Top = 0
        Width = 8
        Height = 1
        Caption = 'ZRLabel104'
        Variable = zrvName
      end
    end
    object ZRBand4: TZRBand
      Left = 4
      Top = 22
      Width = 75
      Height = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnFooter
    end
    object ZRBand5: TZRBand
      Left = 4
      Top = 23
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtFooter
    end
    object ZRSubDetail2: TZRSubDetail
      Left = 4
      Top = 24
      Width = 75
      Height = 2
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 6
      object ZRField12: TZRField
        Format.Width = 60
        DataField = 'JOBFACE'
        DataSet = dmPrint.quInv16
      end
      object ZRField13: TZRField
        Format.Width = 60
        DataField = 'MOLID'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel47: TZRLabel
        Left = 1
        Top = 0
        Width = 27
        Height = 1
        Caption = #1042#1099#1076#1072#1083'('#1072') ________________'
      end
      object ZRLabel48: TZRLabel
        Left = 31
        Top = 1
        Width = 30
        Height = 1
        Variable = ZRField12
      end
      object ZRLabel49: TZRLabel
        Left = 31
        Top = 0
        Width = 30
        Height = 1
        Caption = #1055#1088#1080#1085#1103#1083'('#1072') ________________________'
      end
      object ZRLabel50: TZRLabel
        Left = 1
        Top = 1
        Width = 27
        Height = 1
        Variable = ZRField13
      end
    end
  end
  object zrInv16OutAssort: TZReport
    Left = 152
    Top = 8
    Width = 80
    Height = 28
    DataOptions.AutoOpen = True
    DataSet = dmPrint.quInv16Item
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Options.Escapes.Model = emCustom
    Options.Escapes.Values = (
      ''
      ''
      ''
      ''
      #15
      #18
      #27'E'
      #27'F'
      #27'4'
      #27'5'
      ''
      ''
      #27'S0'
      #27'T'
      #27'S1'
      #27'T'
      #27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'
      #10#10#10#10#10#10#10#10#10#10#10#10#10
      ''
      '')
    Options.Font.Style = zfsElite
    Options.PageFrom = 1
    Options.PageTo = 1
    Options.PaperType = zptContinuous
    Options.PreviewMode = zpmWholeReport
    Margins.Left = 4
    Minimized = True
    object ZRField40: TZRField
      Format.Width = 10
      DataField = 'RecNo'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField41: TZRField
      Format.Width = 40
      DataField = 'SEMIS'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField42: TZRField
      Format.Width = 10
      DataField = 'Q'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField43: TZRField
      Format.Width = 10
      DataField = 'W'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField44: TZRField
      Format.Width = 60
      DataField = 'OPERATION'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField45: TZRField
      Format.Width = 10
      DataField = 'UQNAME'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField46: TZRField
      Format.Width = 10
      DataField = 'UWNAME'
      DataSet = dmPrint.quInv16Item
    end
    object zrvField47: TZRField
      Format.Width = 2
      DataField = 'ITTYPENAME'
      DataSet = dmPrint.quInv16Item
    end
    object ZRGroup4: TZRGroup
    end
    object ZRBand16: TZRBand
      Left = 4
      Top = 11
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtHeader
    end
    object ZRSubDetail7: TZRSubDetail
      Left = 4
      Top = 12
      Width = 75
      Height = 3
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 0
      object ZRField47: TZRField
        Format.Width = 10
        DataField = 'DOCNO'
        DataSet = dmPrint.quInv16
      end
      object ZRField48: TZRField
        Format.Width = 10
        DataField = 'WNO'
        DataSet = dmPrint.quInv16
      end
      object ZRField49: TZRField
        Format.Width = 18
        DataField = 'DOCDATE'
        DataSet = dmPrint.quInv16
      end
      object ZRField50: TZRField
        Format.Width = 40
        DataField = 'DEPNAME'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel105: TZRLabel
        Left = 11
        Top = 2
        Width = 6
        Height = 1
        Caption = #1057#1082#1083#1072#1076':'
      end
      object ZRLabel106: TZRLabel
        Left = 18
        Top = 2
        Width = 7
        Height = 1
        AutoSize = zasWidth
        Caption = 'ZRLabel9'
        Variable = ZRField50
      end
      object ZRLabel107: TZRLabel
        Left = 1
        Top = 0
        Width = 7
        Height = 1
        Caption = #1053#1072#1088#1103#1076' '#8470
      end
      object ZRLabel108: TZRLabel
        Left = 9
        Top = 0
        Width = 8
        Height = 1
        Caption = 'ZRLabel8'
        Variable = ZRField48
      end
      object ZRLabel109: TZRLabel
        Left = 11
        Top = 1
        Width = 11
        Height = 1
        Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
      end
      object ZRLabel110: TZRLabel
        Left = 23
        Top = 1
        Width = 7
        Height = 1
        Variable = ZRField47
      end
      object ZRLabel111: TZRLabel
        Left = 31
        Top = 1
        Width = 2
        Height = 1
        Caption = #1086#1090
      end
      object ZRLabel112: TZRLabel
        Left = 34
        Top = 1
        Width = 10
        Height = 1
        Variable = ZRField49
      end
      object ZRLabel113: TZRLabel
        Left = 48
        Top = 0
        Width = 11
        Height = 1
        Caption = #1060#1086#1088#1084#1072' '#1057#1070'-16'
      end
    end
    object ZRBand17: TZRBand
      Left = 4
      Top = 15
      Width = 75
      Height = 5
      Frame.Left = 1
      Frame.Top = 1
      Frame.Right = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnHeader
      object ZRLabel114: TZRLabel
        Left = 1
        Top = 1
        Width = 24
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Alignment.Y = zahCenter
        Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090', '#13#10#1084#1072#1090#1077#1088#1080#1072#1083
      end
      object ZRLabel115: TZRLabel
        Left = 35
        Top = 1
        Width = 6
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1050#1086#1083'-'#13#10#1074#1086
      end
      object ZRLabel116: TZRLabel
        Left = 41
        Top = 1
        Width = 10
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1052#1072#1089#1089#1072
      end
      object ZRLabel117: TZRLabel
        Left = 51
        Top = 1
        Width = 19
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
      end
      object ZRLabel118: TZRLabel
        Left = 30
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1074#1077#1089#1072
      end
      object ZRLabel119: TZRLabel
        Left = 25
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1082'-'#1074#1072
      end
      object ZRLabel216: TZRLabel
        Left = 70
        Top = 1
        Width = 3
        Height = 3
        Caption = #1058#1080#1087
      end
    end
    object ZRBand18: TZRBand
      Left = 4
      Top = 20
      Width = 75
      Height = 1
      Frame.Left = 1
      Frame.Right = 1
      Stretch = False
      BandType = zbtDetail
      object ZRLabel120: TZRLabel
        Left = 1
        Top = 0
        Width = 24
        Height = 1
        Frame.Right = 1
        Variable = ZRField41
      end
      object ZRLabel121: TZRLabel
        Left = 35
        Top = 0
        Width = 6
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = ZRField42
      end
      object ZRLabel122: TZRLabel
        Left = 41
        Top = 0
        Width = 10
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = ZRField43
      end
      object ZRLabel123: TZRLabel
        Left = 51
        Top = 0
        Width = 19
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        AutoSize = zasHeight
        Variable = ZRField44
      end
      object ZRLabel124: TZRLabel
        Left = 25
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = ZRField45
      end
      object ZRLabel125: TZRLabel
        Left = 30
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = ZRField46
      end
      object ZRLabel220: TZRLabel
        Left = 70
        Top = 0
        Width = 3
        Height = 1
        Variable = zrvField47
      end
    end
    object ZRSubDetail8: TZRSubDetail
      Left = 4
      Top = 21
      Width = 75
      Height = 1
      DataOptions.AutoOpen = True
      DataSet = dmPrint.taInv16Assort
      Frame.Left = 1
      Frame.Right = 1
      Stretch = False
      MasterIndex = 3
      object ZRField51: TZRField
        DataField = 'ART'
        DataSet = dmPrint.taInv16Assort
      end
      object ZRField52: TZRField
        DataField = 'ART2'
        DataSet = dmPrint.taInv16Assort
      end
      object ZRField53: TZRField
        DataField = 'NAME'
        DataSet = dmPrint.taInv16Assort
      end
      object ZRField54: TZRField
        DataField = 'Q'
        DataSet = dmPrint.taInv16Assort
      end
      object ZRLabel126: TZRLabel
        Left = 2
        Top = 0
        Width = 10
        Height = 1
        Variable = ZRField51
      end
      object ZRLabel127: TZRLabel
        Left = 13
        Top = 0
        Width = 6
        Height = 1
        Caption = 'ZRLabel102'
        Variable = ZRField52
      end
      object ZRLabel128: TZRLabel
        Left = 34
        Top = 0
        Width = 6
        Height = 1
        Alignment.X = zawRight
        Caption = 'ZRLabel103'
        Variable = ZRField54
      end
      object ZRLabel129: TZRLabel
        Left = 20
        Top = 0
        Width = 8
        Height = 1
        Caption = 'ZRLabel104'
        Variable = ZRField53
      end
    end
    object ZRBand19: TZRBand
      Left = 4
      Top = 22
      Width = 75
      Height = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnFooter
    end
    object ZRBand20: TZRBand
      Left = 4
      Top = 23
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtFooter
    end
    object ZRSubDetail9: TZRSubDetail
      Left = 4
      Top = 24
      Width = 75
      Height = 2
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 6
      object ZRField55: TZRField
        Format.Width = 60
        DataField = 'JOBFACE'
        DataSet = dmPrint.quInv16
      end
      object ZRField56: TZRField
        Format.Width = 60
        DataField = 'MOLID'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel130: TZRLabel
        Left = 1
        Top = 0
        Width = 27
        Height = 1
        Caption = #1055#1088#1080#1085#1103#1083'('#1072') ________________'
      end
      object ZRLabel131: TZRLabel
        Left = 31
        Top = 1
        Width = 30
        Height = 1
        Variable = ZRField55
      end
      object ZRLabel132: TZRLabel
        Left = 31
        Top = 0
        Width = 30
        Height = 1
        Caption = #1057#1076#1072#1083'('#1072') ________________________'
      end
      object ZRLabel133: TZRLabel
        Left = 1
        Top = 1
        Width = 27
        Height = 1
        Variable = ZRField56
      end
    end
  end
  object zrInv16In_Preview: TZReport
    Left = 202
    Top = 8
    Width = 80
    Height = 26
    DataSet = dmPrint.quInv16Item
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Options.Escapes.Model = emCustom
    Options.Escapes.Values = (
      ''
      ''
      ''
      ''
      #15
      #18
      #27'E'
      #27'F'
      #27'4'
      #27'5'
      ''
      ''
      #27'S0'
      #27'T'
      #27'S1'
      #27'T'
      #27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'
      #10#10#10#10#10#10#10#10#10#10#10#10#10
      ''
      '')
    Options.Font.Style = zfsElite
    Options.PageFrom = 1
    Options.PageTo = 1
    Options.PaperType = zptContinuous
    Options.PreviewMode = zpmWholeReport
    Margins.Left = 4
    Minimized = True
    object ZRField57: TZRField
      Format.Width = 10
      DataField = 'RecNo'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField58: TZRField
      Format.Width = 40
      DataField = 'SEMIS'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField59: TZRField
      Format.Width = 10
      DataField = 'Q'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField60: TZRField
      Format.Width = 10
      DataField = 'W'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField61: TZRField
      Format.Width = 60
      DataField = 'OPERATION'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField62: TZRField
      Format.Width = 10
      DataField = 'UQNAME'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField63: TZRField
      Format.Width = 10
      DataField = 'UWNAME'
      DataSet = dmPrint.quInv16Item
    end
    object zrvField64: TZRField
      Format.Width = 2
      DataField = 'ITTYPENAME'
      DataSet = dmPrint.quInv16Item
    end
    object ZRGroup5: TZRGroup
    end
    object ZRBand21: TZRBand
      Left = 4
      Top = 11
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtHeader
    end
    object ZRSubDetail10: TZRSubDetail
      Left = 4
      Top = 12
      Width = 75
      Height = 3
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 0
      object ZRField64: TZRField
        Format.Width = 10
        DataField = 'DOCNO'
        DataSet = dmPrint.quInv16
      end
      object ZRField65: TZRField
        Format.Width = 10
        DataField = 'WNO'
        DataSet = dmPrint.quInv16
      end
      object ZRField66: TZRField
        Format.Width = 18
        DataField = 'DOCDATE'
        DataSet = dmPrint.quInv16
      end
      object ZRField67: TZRField
        Format.Width = 40
        DataField = 'DEPNAME'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel134: TZRLabel
        Left = 11
        Top = 2
        Width = 6
        Height = 1
        Caption = #1057#1082#1083#1072#1076':'
      end
      object ZRLabel135: TZRLabel
        Left = 18
        Top = 2
        Width = 7
        Height = 1
        AutoSize = zasWidth
        Caption = 'ZRLabel9'
        Variable = ZRField67
      end
      object ZRLabel136: TZRLabel
        Left = 1
        Top = 0
        Width = 7
        Height = 1
        Caption = #1053#1072#1088#1103#1076' '#8470
      end
      object ZRLabel137: TZRLabel
        Left = 9
        Top = 0
        Width = 8
        Height = 1
        Caption = 'ZRLabel8'
        Variable = ZRField65
      end
      object ZRLabel138: TZRLabel
        Left = 11
        Top = 1
        Width = 11
        Height = 1
        Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
      end
      object ZRLabel139: TZRLabel
        Left = 23
        Top = 1
        Width = 7
        Height = 1
        Variable = ZRField64
      end
      object ZRLabel140: TZRLabel
        Left = 31
        Top = 1
        Width = 2
        Height = 1
        Caption = #1086#1090
      end
      object ZRLabel141: TZRLabel
        Left = 34
        Top = 1
        Width = 10
        Height = 1
        Variable = ZRField66
      end
      object ZRLabel142: TZRLabel
        Left = 48
        Top = 0
        Width = 11
        Height = 1
        Caption = #1060#1086#1088#1084#1072' '#1057#1070'-16'
      end
      object ZRLabel159: TZRLabel
        Left = 20
        Top = 0
        Width = 24
        Height = 1
        FontStyles = [zfsBold]
        Caption = #1055#1056#1045#1044#1042#1040#1056#1048#1058#1045#1051#1068#1053#1067#1049' '#1044#1054#1050#1059#1052#1045#1053#1058
      end
    end
    object ZRBand22: TZRBand
      Left = 4
      Top = 15
      Width = 75
      Height = 5
      Frame.Left = 1
      Frame.Top = 1
      Frame.Right = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnHeader
      object ZRLabel143: TZRLabel
        Left = 1
        Top = 1
        Width = 24
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Alignment.Y = zahCenter
        Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090', '#13#10#1084#1072#1090#1077#1088#1080#1072#1083
      end
      object ZRLabel144: TZRLabel
        Left = 35
        Top = 1
        Width = 6
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1050#1086#1083'-'#13#10#1074#1086
      end
      object ZRLabel145: TZRLabel
        Left = 41
        Top = 1
        Width = 10
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1052#1072#1089#1089#1072
      end
      object ZRLabel146: TZRLabel
        Left = 51
        Top = 1
        Width = 19
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
      end
      object ZRLabel147: TZRLabel
        Left = 30
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1074#1077#1089#1072
      end
      object ZRLabel148: TZRLabel
        Left = 25
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1082'-'#1074#1072
      end
      object ZRLabel217: TZRLabel
        Left = 70
        Top = 1
        Width = 3
        Height = 3
        Caption = #1058#1080#1087
      end
    end
    object ZRBand23: TZRBand
      Left = 4
      Top = 20
      Width = 75
      Height = 1
      Frame.Left = 1
      Frame.Right = 1
      Stretch = False
      BandType = zbtDetail
      object ZRLabel149: TZRLabel
        Left = 1
        Top = 0
        Width = 24
        Height = 1
        Frame.Right = 1
        FontStyles = [zfsBold]
        Variable = ZRField58
      end
      object ZRLabel150: TZRLabel
        Left = 35
        Top = 0
        Width = 6
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = ZRField59
      end
      object ZRLabel151: TZRLabel
        Left = 41
        Top = 0
        Width = 10
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = ZRField60
      end
      object ZRLabel152: TZRLabel
        Left = 51
        Top = 0
        Width = 19
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        AutoSize = zasHeight
        Variable = ZRField61
      end
      object ZRLabel153: TZRLabel
        Left = 25
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = ZRField62
      end
      object ZRLabel154: TZRLabel
        Left = 30
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = ZRField63
      end
      object ZRLabel221: TZRLabel
        Left = 70
        Top = 0
        Width = 3
        Height = 1
        Variable = zrvField64
      end
    end
    object ZRBand24: TZRBand
      Left = 4
      Top = 21
      Width = 75
      Height = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnFooter
    end
    object ZRBand25: TZRBand
      Left = 4
      Top = 22
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtFooter
    end
    object ZRSubDetail11: TZRSubDetail
      Left = 4
      Top = 23
      Width = 75
      Height = 2
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 5
      object ZRField68: TZRField
        Format.Width = 60
        DataField = 'JOBFACE'
        DataSet = dmPrint.quInv16
      end
      object ZRField69: TZRField
        Format.Width = 60
        DataField = 'MOLID'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel155: TZRLabel
        Left = 1
        Top = 0
        Width = 27
        Height = 1
        Caption = #1042#1099#1076#1072#1083'('#1072') ________________'
      end
      object ZRLabel156: TZRLabel
        Left = 31
        Top = 1
        Width = 30
        Height = 1
        Variable = ZRField68
      end
      object ZRLabel157: TZRLabel
        Left = 31
        Top = 0
        Width = 30
        Height = 1
        Caption = #1055#1088#1080#1085#1103#1083'('#1072') ________________________'
      end
      object ZRLabel158: TZRLabel
        Left = 1
        Top = 1
        Width = 27
        Height = 1
        Variable = ZRField69
      end
    end
  end
  object zrInv16Out_Preview: TZReport
    Left = 260
    Top = 8
    Width = 80
    Height = 26
    DataSet = dmPrint.quInv16Item
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    Options.Escapes.Model = emCustom
    Options.Escapes.Values = (
      ''
      ''
      ''
      ''
      #15
      #18
      #27'E'
      #27'F'
      #27'4'
      #27'5'
      ''
      ''
      #27'S0'
      #27'T'
      #27'S1'
      #27'T'
      #27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'#27'j2'
      #10#10#10#10#10#10#10#10#10#10#10#10#10
      ''
      '')
    Options.Font.Style = zfsElite
    Options.PageFrom = 1
    Options.PageTo = 1
    Options.PaperType = zptContinuous
    Options.PreviewMode = zpmWholeReport
    Margins.Left = 4
    Minimized = True
    object ZRField70: TZRField
      Format.Width = 10
      DataField = 'RecNo'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField71: TZRField
      Format.Width = 40
      DataField = 'SEMIS'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField72: TZRField
      Format.Width = 10
      DataField = 'Q'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField73: TZRField
      Format.Width = 10
      DataField = 'W'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField74: TZRField
      Format.Width = 60
      DataField = 'OPERATION'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField75: TZRField
      Format.Width = 10
      DataField = 'UQNAME'
      DataSet = dmPrint.quInv16Item
    end
    object ZRField76: TZRField
      Format.Width = 10
      DataField = 'UWNAME'
      DataSet = dmPrint.quInv16Item
    end
    object zrvField77: TZRField
      Format.Width = 2
      DataField = 'ITTYPENAME'
      DataSet = dmPrint.quInv16Item
    end
    object ZRGroup6: TZRGroup
    end
    object ZRBand26: TZRBand
      Left = 4
      Top = 11
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtHeader
    end
    object ZRSubDetail12: TZRSubDetail
      Left = 4
      Top = 12
      Width = 75
      Height = 3
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 0
      object ZRField77: TZRField
        Format.Width = 10
        DataField = 'DOCNO'
        DataSet = dmPrint.quInv16
      end
      object ZRField78: TZRField
        Format.Width = 10
        DataField = 'WNO'
        DataSet = dmPrint.quInv16
      end
      object ZRField79: TZRField
        Format.Width = 18
        DataField = 'DOCDATE'
        DataSet = dmPrint.quInv16
      end
      object ZRField80: TZRField
        Format.Width = 40
        DataField = 'DEPNAME'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel160: TZRLabel
        Left = 11
        Top = 2
        Width = 6
        Height = 1
        Caption = #1057#1082#1083#1072#1076':'
      end
      object ZRLabel161: TZRLabel
        Left = 18
        Top = 2
        Width = 7
        Height = 1
        AutoSize = zasWidth
        Caption = 'ZRLabel9'
        Variable = ZRField80
      end
      object ZRLabel162: TZRLabel
        Left = 1
        Top = 0
        Width = 7
        Height = 1
        Caption = #1053#1072#1088#1103#1076' '#8470
      end
      object ZRLabel163: TZRLabel
        Left = 9
        Top = 0
        Width = 8
        Height = 1
        Caption = 'ZRLabel8'
        Variable = ZRField78
      end
      object ZRLabel164: TZRLabel
        Left = 11
        Top = 1
        Width = 11
        Height = 1
        Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
      end
      object ZRLabel165: TZRLabel
        Left = 23
        Top = 1
        Width = 7
        Height = 1
        Variable = ZRField77
      end
      object ZRLabel166: TZRLabel
        Left = 31
        Top = 1
        Width = 2
        Height = 1
        Caption = #1086#1090
      end
      object ZRLabel167: TZRLabel
        Left = 34
        Top = 1
        Width = 10
        Height = 1
        Variable = ZRField79
      end
      object ZRLabel168: TZRLabel
        Left = 52
        Top = 0
        Width = 11
        Height = 1
        Caption = #1060#1086#1088#1084#1072' '#1057#1070'-16'
      end
      object ZRLabel210: TZRLabel
        Left = 21
        Top = 0
        Width = 24
        Height = 1
        FontStyles = [zfsBold]
        Caption = #1055#1056#1045#1044#1042#1040#1056#1048#1058#1045#1051#1068#1053#1067#1049' '#1044#1054#1050#1059#1052#1045#1053#1058
      end
    end
    object ZRBand27: TZRBand
      Left = 4
      Top = 15
      Width = 75
      Height = 5
      Frame.Left = 1
      Frame.Top = 1
      Frame.Right = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnHeader
      object ZRLabel169: TZRLabel
        Left = 1
        Top = 1
        Width = 24
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Alignment.Y = zahCenter
        Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090', '#13#10#1084#1072#1090#1077#1088#1080#1072#1083
      end
      object ZRLabel170: TZRLabel
        Left = 35
        Top = 1
        Width = 6
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1050#1086#1083'-'#13#10#1074#1086
      end
      object ZRLabel171: TZRLabel
        Left = 41
        Top = 1
        Width = 10
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1052#1072#1089#1089#1072
      end
      object ZRLabel172: TZRLabel
        Left = 51
        Top = 1
        Width = 19
        Height = 3
        Frame.Right = 1
        Alignment.X = zawCenter
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
      end
      object ZRLabel173: TZRLabel
        Left = 30
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1074#1077#1089#1072
      end
      object ZRLabel174: TZRLabel
        Left = 25
        Top = 1
        Width = 5
        Height = 3
        Frame.Right = 1
        Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1082'-'#1074#1072
      end
      object ZRLabel218: TZRLabel
        Left = 70
        Top = 1
        Width = 3
        Height = 3
        Caption = #1058#1080#1087
      end
    end
    object ZRBand28: TZRBand
      Left = 4
      Top = 20
      Width = 75
      Height = 1
      Frame.Left = 1
      Frame.Right = 1
      Stretch = False
      BandType = zbtDetail
      object ZRLabel175: TZRLabel
        Left = 1
        Top = 0
        Width = 24
        Height = 1
        Frame.Right = 1
        FontStyles = [zfsBold]
        Variable = ZRField71
      end
      object ZRLabel176: TZRLabel
        Left = 35
        Top = 0
        Width = 6
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = ZRField72
      end
      object ZRLabel177: TZRLabel
        Left = 41
        Top = 0
        Width = 10
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        Variable = ZRField73
      end
      object ZRLabel178: TZRLabel
        Left = 51
        Top = 0
        Width = 19
        Height = 1
        Frame.Right = 1
        Alignment.X = zawRight
        AutoSize = zasHeight
        Variable = ZRField74
      end
      object ZRLabel179: TZRLabel
        Left = 25
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = ZRField75
      end
      object ZRLabel180: TZRLabel
        Left = 30
        Top = 0
        Width = 5
        Height = 1
        Frame.Right = 1
        Variable = ZRField76
      end
      object ZRLabel222: TZRLabel
        Left = 70
        Top = 0
        Width = 3
        Height = 1
        Variable = zrvField77
      end
    end
    object ZRBand29: TZRBand
      Left = 4
      Top = 21
      Width = 75
      Height = 1
      Frame.Bottom = 1
      Stretch = False
      BandType = zbtColumnFooter
    end
    object ZRBand30: TZRBand
      Left = 4
      Top = 22
      Width = 75
      Height = 1
      Stretch = False
      BandType = zbtFooter
    end
    object ZRSubDetail13: TZRSubDetail
      Left = 4
      Top = 23
      Width = 75
      Height = 2
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16
      Stretch = False
      MasterIndex = 5
      object ZRField81: TZRField
        Format.Width = 60
        DataField = 'JOBFACE'
        DataSet = dmPrint.quInv16
      end
      object ZRField82: TZRField
        Format.Width = 60
        DataField = 'MOLID'
        DataSet = dmPrint.quInv16
      end
      object ZRLabel181: TZRLabel
        Left = 1
        Top = 0
        Width = 27
        Height = 1
        Caption = #1055#1088#1080#1085#1103#1083'('#1072') ________________'
      end
      object ZRLabel182: TZRLabel
        Left = 31
        Top = 1
        Width = 30
        Height = 1
        Variable = ZRField81
      end
      object ZRLabel183: TZRLabel
        Left = 31
        Top = 0
        Width = 30
        Height = 1
        Caption = #1057#1076#1072#1083'('#1072') ________________________'
      end
      object ZRLabel184: TZRLabel
        Left = 1
        Top = 1
        Width = 27
        Height = 1
        Variable = ZRField82
      end
    end
    object ZReport2: TZReport
      Left = -494
      Top = -319
      Width = 75
      Height = 26
      DataOptions.AutoOpen = True
      DataSet = dmPrint.quInv16Item
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      Options.Escapes.Model = emEpsonLXSeries
      Options.Font.Style = zfsElite
      Options.PageFrom = 1
      Options.PageTo = 1
      Options.PaperType = zptContinuous
      Options.PreviewMode = zpmWholeReport
      Margins.Left = 4
      Minimized = True
      object ZRField83: TZRField
        Format.Width = 10
        DataField = 'RecNo'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField84: TZRField
        Format.Width = 40
        DataField = 'SEMIS'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField85: TZRField
        Format.Width = 10
        DataField = 'Q'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField86: TZRField
        Format.Width = 10
        DataField = 'W'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField87: TZRField
        Format.Width = 60
        DataField = 'OPERATION'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField88: TZRField
        Format.Width = 10
        DataField = 'UQNAME'
        DataSet = dmPrint.quInv16Item
      end
      object ZRField89: TZRField
        Format.Width = 10
        DataField = 'UWNAME'
        DataSet = dmPrint.quInv16Item
      end
      object ZRGroup7: TZRGroup
      end
      object ZRBand31: TZRBand
        Left = 4
        Top = 11
        Width = 70
        Height = 1
        Stretch = False
        BandType = zbtHeader
      end
      object ZRSubDetail14: TZRSubDetail
        Left = 4
        Top = 12
        Width = 70
        Height = 3
        DataOptions.AutoOpen = True
        DataSet = dmPrint.quInv16
        Stretch = False
        MasterIndex = 0
        object ZRField90: TZRField
          Format.Width = 10
          DataField = 'DOCNO'
          DataSet = dmPrint.quInv16
        end
        object ZRField91: TZRField
          Format.Width = 10
          DataField = 'WNO'
          DataSet = dmPrint.quInv16
        end
        object ZRField92: TZRField
          Format.Width = 18
          DataField = 'DOCDATE'
          DataSet = dmPrint.quInv16
        end
        object ZRField93: TZRField
          Format.Width = 40
          DataField = 'DEPNAME'
          DataSet = dmPrint.quInv16
        end
        object ZRLabel185: TZRLabel
          Left = 11
          Top = 2
          Width = 6
          Height = 1
          Caption = #1057#1082#1083#1072#1076':'
        end
        object ZRLabel186: TZRLabel
          Left = 18
          Top = 2
          Width = 7
          Height = 1
          AutoSize = zasWidth
          Caption = 'ZRLabel9'
          Variable = ZRField93
        end
        object ZRLabel187: TZRLabel
          Left = 1
          Top = 0
          Width = 7
          Height = 1
          Caption = #1053#1072#1088#1103#1076' '#8470
        end
        object ZRLabel188: TZRLabel
          Left = 9
          Top = 0
          Width = 8
          Height = 1
          Caption = 'ZRLabel8'
          Variable = ZRField91
        end
        object ZRLabel189: TZRLabel
          Left = 11
          Top = 1
          Width = 11
          Height = 1
          Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
        end
        object ZRLabel190: TZRLabel
          Left = 23
          Top = 1
          Width = 7
          Height = 1
          Variable = ZRField90
        end
        object ZRLabel191: TZRLabel
          Left = 31
          Top = 1
          Width = 2
          Height = 1
          Caption = #1086#1090
        end
        object ZRLabel192: TZRLabel
          Left = 34
          Top = 1
          Width = 10
          Height = 1
          Variable = ZRField92
        end
        object ZRLabel193: TZRLabel
          Left = 48
          Top = 0
          Width = 11
          Height = 1
          Caption = #1060#1086#1088#1084#1072' '#1057#1070'-16'
        end
      end
      object ZRBand32: TZRBand
        Left = 4
        Top = 15
        Width = 70
        Height = 5
        Frame.Left = 1
        Frame.Top = 1
        Frame.Right = 1
        Frame.Bottom = 1
        Stretch = False
        BandType = zbtColumnHeader
        object ZRLabel194: TZRLabel
          Left = 1
          Top = 1
          Width = 24
          Height = 3
          Frame.Right = 1
          Alignment.X = zawCenter
          Alignment.Y = zahCenter
          Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090', '#13#10#1084#1072#1090#1077#1088#1080#1072#1083
        end
        object ZRLabel195: TZRLabel
          Left = 35
          Top = 1
          Width = 6
          Height = 3
          Frame.Right = 1
          Alignment.X = zawCenter
          Caption = #1050#1086#1083'-'#13#10#1074#1086
        end
        object ZRLabel196: TZRLabel
          Left = 41
          Top = 1
          Width = 8
          Height = 3
          Frame.Right = 1
          Alignment.X = zawCenter
          Caption = #1052#1072#1089#1089#1072
        end
        object ZRLabel197: TZRLabel
          Left = 49
          Top = 1
          Width = 20
          Height = 3
          Alignment.X = zawCenter
          Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        end
        object ZRLabel198: TZRLabel
          Left = 30
          Top = 1
          Width = 5
          Height = 3
          Frame.Right = 1
          Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1074#1077#1089#1072
        end
        object ZRLabel199: TZRLabel
          Left = 25
          Top = 1
          Width = 5
          Height = 3
          Frame.Right = 1
          Caption = #1045#1076'.'#13#10#1080#1079#1084'.'#13#10#1082'-'#1074#1072
        end
      end
      object ZRBand33: TZRBand
        Left = 4
        Top = 20
        Width = 70
        Height = 1
        Frame.Left = 1
        Frame.Right = 1
        Stretch = False
        BandType = zbtDetail
        object ZRLabel200: TZRLabel
          Left = 1
          Top = 0
          Width = 24
          Height = 1
          Frame.Right = 1
          Variable = ZRField84
        end
        object ZRLabel201: TZRLabel
          Left = 35
          Top = 0
          Width = 6
          Height = 1
          Frame.Right = 1
          Alignment.X = zawRight
          Variable = ZRField85
        end
        object ZRLabel202: TZRLabel
          Left = 41
          Top = 0
          Width = 8
          Height = 1
          Frame.Right = 1
          Alignment.X = zawRight
          Variable = ZRField86
        end
        object ZRLabel203: TZRLabel
          Left = 49
          Top = 0
          Width = 20
          Height = 1
          Alignment.X = zawRight
          AutoSize = zasHeight
          Variable = ZRField87
        end
        object ZRLabel204: TZRLabel
          Left = 25
          Top = 0
          Width = 5
          Height = 1
          Frame.Right = 1
          Variable = ZRField88
        end
        object ZRLabel205: TZRLabel
          Left = 30
          Top = 0
          Width = 5
          Height = 1
          Frame.Right = 1
          Variable = ZRField89
        end
      end
      object ZRBand34: TZRBand
        Left = 4
        Top = 21
        Width = 70
        Height = 1
        Frame.Bottom = 1
        Stretch = False
        BandType = zbtColumnFooter
      end
      object ZRBand35: TZRBand
        Left = 4
        Top = 22
        Width = 70
        Height = 1
        Stretch = False
        BandType = zbtFooter
      end
      object ZRSubDetail15: TZRSubDetail
        Left = 4
        Top = 23
        Width = 70
        Height = 2
        DataOptions.AutoOpen = True
        DataSet = dmPrint.quInv16
        Stretch = False
        MasterIndex = 5
        object ZRField94: TZRField
          Format.Width = 60
          DataField = 'JOBFACE'
          DataSet = dmPrint.quInv16
        end
        object ZRField95: TZRField
          Format.Width = 60
          DataField = 'MOLID'
          DataSet = dmPrint.quInv16
        end
        object ZRLabel206: TZRLabel
          Left = 1
          Top = 0
          Width = 27
          Height = 1
          Caption = #1042#1099#1076#1072#1083'('#1072') ________________'
        end
        object ZRLabel207: TZRLabel
          Left = 31
          Top = 1
          Width = 30
          Height = 1
          Variable = ZRField94
        end
        object ZRLabel208: TZRLabel
          Left = 31
          Top = 0
          Width = 30
          Height = 1
          Caption = #1055#1088#1080#1085#1103#1083'('#1072') ________________________'
        end
        object ZRLabel209: TZRLabel
          Left = 1
          Top = 1
          Width = 27
          Height = 1
          Variable = ZRField95
        end
      end
    end
  end
end
