inherited fmPswd: TfmPswd
  Left = 337
  Top = 237
  ActiveControl = edPswd
  Caption = #1055#1072#1088#1086#1083#1100
  ClientHeight = 91
  ClientWidth = 226
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 226
    Height = 91
    ExplicitWidth = 226
    ExplicitHeight = 91
    object Label1: TLabel [0]
      Left = 8
      Top = 12
      Width = 38
      Height = 13
      Caption = #1055#1072#1088#1086#1083#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel [1]
      Left = 8
      Top = 32
      Width = 81
      Height = 13
      Caption = #1055#1086#1076#1090#1074#1077#1088#1078#1076#1077#1085#1080#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    inherited btnOk: TBitBtn
      Left = 44
      Top = 56
      Anchors = [akRight, akBottom]
      ExplicitLeft = 44
      ExplicitTop = 56
    end
    inherited btnCancel: TBitBtn
      Left = 136
      Top = 56
      Anchors = [akRight, akBottom]
      ExplicitLeft = 136
      ExplicitTop = 56
    end
    object edPswd: TDBEditEh
      Left = 92
      Top = 8
      Width = 121
      Height = 19
      DataField = 'PSWD'
      DataSource = dm.dsrMOL
      EditButtons = <>
      Flat = True
      PasswordChar = '*'
      TabOrder = 2
      Visible = True
    end
    object edPswdTest: TDBEditEh
      Left = 92
      Top = 28
      Width = 121
      Height = 19
      EditButtons = <>
      Flat = True
      PasswordChar = '*'
      TabOrder = 3
      Visible = True
    end
  end
end
