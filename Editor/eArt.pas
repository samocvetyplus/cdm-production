unit eArt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, RxDBComb, Mask, DBCtrls,
  DBCtrlsEh;

type
  TfmeArt = class(TfmEditor)
    lbArt: TLabel;
    edArt: TDBEditEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmeArt: TfmeArt;

implementation

uses DictData, dbUtil, MsgDialog;

{$R *.dfm}

procedure TfmeArt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk: begin
      vResult := ExecSelectSQL('select D_ArtId from D_Art where Art="'+edArt.Text+'"', dm.quTmp);
      if VarIsNull(vResult)or(vResult=0) then
      begin
        ActiveControl := edArt;
        MessageDialog('�������� '+edArt.Text+' ��� � �����������', mtError, [mbOk], 0);
        SysUtils.Abort;
      end;
    end;
  end;
  inherited;
end;

end.
