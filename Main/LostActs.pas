unit LostActs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, ActnList, Menus,  ImgList, Grids,
  DBGridEh,  StdCtrls, ExtCtrls, ComCtrls, //bsUtils,
  fmUtils,
  TB2Item, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmLostActs = class(TfmListAncestor)
    procedure acEditDocExecute(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure acPrintDocUpdate(Sender: TObject);
  end;

var
  fmLostActs: TfmLostActs;

implementation

uses InvData, LostStones_det, PrintData, DB;

{$R *.dfm}

procedure TfmLostActs.acEditDocExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmLostStones_det,TForm(fmLostStones_det));
end;

procedure TfmLostActs.acPrintDocExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkLostStones,null);
end;

procedure TfmLostActs.acPrintDocUpdate(Sender: TObject);
begin
  acPrintDoc.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

end.
