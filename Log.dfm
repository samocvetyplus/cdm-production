inherited fmLog: TfmLog
  Left = 213
  Top = 302
  Caption = #1051#1086#1075
  ClientWidth = 432
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnDeactivate = FormDeactivate
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 432
    BevelInner = bvNone
    BevelOuter = bvNone
    inherited btnOk: TBitBtn
      Left = 344
      OnClick = btnOkClick
    end
    inherited btnCancel: TBitBtn
      Left = 100
      Visible = False
      OnClick = btnCancelClick
    end
    object lbxLog: TListBox
      Left = 0
      Top = 0
      Width = 432
      Height = 125
      Align = alTop
      BevelKind = bkFlat
      BorderStyle = bsNone
      ItemHeight = 13
      TabOrder = 2
    end
  end
end
