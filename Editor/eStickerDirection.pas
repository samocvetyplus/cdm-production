unit eStickerDirection;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls;

type
  TfmeStikerDirection = class(TfmEditor)
    lbQ: TLabel;
    edQ: TEdit;
    procedure edQKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  public
  end;

var
  fmeStikerDirection: TfmeStikerDirection;

implementation

{$R *.dfm}

procedure TfmeStikerDirection.edQKeyPress(Sender: TObject; var Key: Char);
begin
  case Key of
    '0'..'9': eXit;
    else SysUtils.Abort;
  end;
end;

procedure TfmeStikerDirection.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk: vResult := StrToIntDef(edQ.Text, 0);
  end;
  inherited;
end;

end.
