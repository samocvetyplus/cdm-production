unit AWMatOff;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus, TB2Item, ActnList, ImgList, Grids,
  DBGridEh, DBCtrlsEh, StdCtrls, Mask, DBCtrls, ExtCtrls,
  ComCtrls, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmAWMatOff = class(TfmDocAncestor)
    spitRefresh: TSpeedItem;
    acRefresh: TAction;
    Label1: TLabel;
    txtBD: TDBText;
    txtED: TDBText;
    Label3: TLabel;
    Label2: TLabel;
    txtSupName: TDBText;
    Label4: TLabel;
    Label5: TLabel;
    txtP: TDBText;
    txtPE: TDBText;
    lbKC: TLabel;
    txtKC: TDBText;
    Label6: TLabel;
    txtPB: TDBText;
    Label7: TLabel;
    Label8: TLabel;
    txtS: TDBText;
    txtR: TDBText;
    Label9: TLabel;
    Label10: TLabel;
    txtTI: TDBText;
    txtTO: TDBText;
    procedure acRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure gridItemColumns7EditButtonClick(Sender: TObject; var Handled: Boolean);
  end;

var
  fmAWMatOff: TfmAWMatOff;

implementation

uses Data, dbUtil, DictData, AWMatOff_UID, fmUtils;

{$R *.dfm}

procedure TfmAWMatOff.acRefreshExecute(Sender: TObject);
begin
  PostDataSets([dmData.taAWMO, dmData.taAWMOList]);
  CloseDataSet(dmData.taAWMO);
  ExecSQL('execute procedure CREATE_AWMATOFF('+dmData.taAWMOListINVID.AsString+')', dm.quTmp);
  dmData.taAWMOList.Refresh;
  ReOpenDataSet(dmData.taAWMO);
end;

procedure TfmAWMatOff.FormCreate(Sender: TObject);
begin
  edNoDoc.DataSource := dmData.dsrAWMOList;
  dtedDateDoc.DataSource := dmData.dsrAWMOList;
  gridItem.DataSource := dmData.dsrAWMO;
  FDocName := '�������� ���������� �������';
  inherited;
  InitBtn(Boolean(dmData.taAWMOListISCLOSE.AsInteger));
end;

procedure TfmAWMatOff.gridItemColumns7EditButtonClick(Sender: TObject;
  var Handled: Boolean);
begin
  ShowAndFreeForm(TfmAWMatOff_UID, TForm(fmAWMatOff_UID));

end;

end.
