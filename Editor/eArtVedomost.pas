unit eArtVedomost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Period, StdCtrls, Mask, DBCtrlsEh, Buttons, ExtCtrls, DateUtils,
  dbUtil;

type
  TfmEArtVedomost = class(TfmPeriod)
    Label3: TLabel;
    edNo: TDBEditEh;
    Bevel1: TBevel;
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  end;

var
  fmEArtVedomost: TfmEArtVedomost;

implementation

uses DictData, InvData, pFIBQuery;

{$R *.dfm}

procedure TfmEArtVedomost.FormCreate(Sender: TObject);
var r:Variant;
begin
  inherited;
  edNo.Value:=dm.GetId(45);
//  deBD.Value:=StartOfTheMonth(Today);
//  deED.Value:=EndOfTheMonth(Today);
   r := ExecSelectSQL('select max(DOCDATE)+1 from ACT where T=4', dminv.sqlPublic);
   deBD.Value:=r;
   deBD.ReadOnly:=true;
   deED.Value:=EndOfTheMonth(r);
end;

procedure TfmEArtVedomost.BitBtn1Click(Sender: TObject);
begin
  inherited;
  with dminv.sqlPublic do
  begin
    SQL.Clear;
    SQL.add('execute procedure Create_ArtVedomost(:Docno,:BD,:ED,:userid,:DEPID)');
    ParamByName('Docno').AsInteger:=edNo.Value;
    ParamByName('USERID').AsInteger:=dm.User.UserId;
    ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(VarToDateTime(deBD.Value));
    ParamByName('ED').AsTimeStamp :=  DateTimeToTimeStamp(EndOfTheDay(VarToDateTime(deED.Value)));
    { TODO : переделать }
    ParamByName('DEPID').AsInteger:=23;
    ExecQuery;
    Transaction.CommitRetaining;
  end;
    ReOpenDataSet(dminv.taAVList);
end;

end.
