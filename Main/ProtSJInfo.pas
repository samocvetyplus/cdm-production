unit ProtSJInfo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGridEh, ActnList, DBGridEhGrouping, GridsEh, rxPlacemnt,
  rxSpeedbar;

type
  TfmProtSJInfo = class(TfmAncestor)
    gridHistSemis: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmProtSJInfo: TfmProtSJInfo;

implementation

uses MainData, dbUtil;

{$R *.dfm}

procedure TfmProtSJInfo.FormCreate(Sender: TObject);
begin
  OpenDataSet(dmMain.taProtSJInfo);
  inherited;
end;

procedure TfmProtSJInfo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmMain.taProtSJInfo);
  inherited;
end;

end.
