object fmMain: TfmMain
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086' '#1102#1074#1077#1083#1080#1088#1085#1099#1093' '#1080#1079#1076#1077#1083#1080#1081
  ClientHeight = 166
  ClientWidth = 745
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCanResize = FormCanResize
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 139
    Width = 745
    Height = 27
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
  end
  object TBDock1: TTBDock
    Left = 0
    Top = 0
    Width = 745
    Height = 140
    Background = TBBackground
    object tlbrWhProd: TTBToolbar
      Left = 0
      Top = 101
      Cursor = crHandPoint
      BorderStyle = bsNone
      Caption = 'tlbrWhProd'
      DockPos = 0
      DockRow = 3
      Images = im16
      Options = [tboImageAboveCaption]
      TabOrder = 0
      object TBItem7: TTBItem
        Action = acInv
      end
      object TBItem11: TTBItem
        Action = acSell
      end
      object TBItem6: TTBItem
        Action = acRet
      end
      object TBItem10: TTBItem
        Action = acRetProd
      end
      object TBItem89: TTBItem
        Action = acActWork
      end
      object TBItem90: TTBItem
        Action = acActVerification
      end
      object TBItem113: TTBItem
        Action = acVerificationActTolling
      end
      object TBItem112: TTBItem
        Action = acPaymentsAndBalance
      end
    end
    object tlbrAppl: TTBToolbar
      Left = 552
      Top = 62
      Cursor = crHandPoint
      BorderStyle = bsNone
      Caption = #1047#1072#1103#1074#1082#1080
      DockPos = 552
      DockRow = 2
      Images = im16
      Options = [tboImageAboveCaption]
      TabOrder = 1
      object TBItem8: TTBItem
        Action = acAppl
      end
      object TBItem91: TTBItem
        Action = acRequest
      end
      object TBItem100: TTBItem
        Caption = #1046#1091#1088#1085#1072#1083
        Visible = False
        OnClick = TBItem100Click
      end
    end
    object tlbrProd: TTBToolbar
      Left = 0
      Top = 23
      Cursor = crHandPoint
      BorderStyle = bsNone
      DockPos = 0
      DockRow = 1
      Images = im16
      Options = [tboImageAboveCaption]
      TabOrder = 2
      object TBItem1: TTBItem
        Action = acWOrder
      end
      object TBItem2: TTBItem
        Action = acSInvProd
        Caption = #1054#1090#1075#1088#1091#1079#1082#1072
      end
      object TBItem16: TTBItem
        Action = acBulkList
      end
      object ButtonAffinage: TTBSubmenuItem
        Caption = #1057#1098#1105#1084
        DropdownCombo = True
        ImageIndex = 84
        object TBItem67: TTBItem
          Action = acAffinaj1
          Caption = #1051#1086#1084
          Options = [tboDropdownArrow]
        end
        object TBItem103: TTBItem
          Action = acAffinaj2
          Caption = #1047#1086#1083#1086#1090#1086
        end
        object TBItem110: TTBItem
          Action = acAffinaj3
          Caption = #1057#1077#1088#1076#1077#1095#1085#1080#1082
        end
      end
      object TBItem13: TTBItem
        Action = acWhArt
      end
      object TBSeparatorItem1: TTBSeparatorItem
      end
      object TBItem5: TTBItem
        Action = acProtocol
        Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
      end
      object TBItem101: TTBItem
        Action = acProtocol_2
      end
      object TBItem15: TTBItem
        Action = acVList
        Caption = #1042#1077#1076#1086#1084#1086#1089#1090#1100
      end
      object TBSeparatorItem11: TTBSeparatorItem
      end
      object TBItem4: TTBItem
        Action = acZp
      end
    end
    object tlbrWhMat: TTBToolbar
      Left = 0
      Top = 62
      Cursor = crHandPoint
      BorderStyle = bsNone
      Caption = 'tlbrWhMat'
      DockPos = 0
      DockRow = 2
      Images = im16
      Options = [tboImageAboveCaption]
      TabOrder = 3
      object TBItem9: TTBItem
        Action = acSInv
      end
      object TBItem12: TTBItem
        Action = acTransfer
      end
      object TBItem3: TTBItem
        Action = acSOProd
      end
      object TBSeparatorItem2: TTBSeparatorItem
      end
      object TBItem14: TTBItem
        Action = acWhSemis
      end
    end
    object tlbrMain: TTBToolbar
      Left = 0
      Top = 0
      Align = alTop
      CloseButton = False
      DockMode = dmCannotFloat
      DockPos = 0
      FullSize = True
      Images = im16
      MenuBar = True
      ProcessShortCuts = True
      ShrinkMode = tbsmWrap
      TabOrder = 4
      object sitProd: TTBSubmenuItem
        Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
        object TBItem19: TTBItem
          Action = acWOrder
        end
        object TBItem17: TTBItem
          Action = acSInvProd
        end
        object TBItem30: TTBItem
          Action = acSInv
        end
        object TBItem29: TTBItem
          Action = acTransfer
        end
        object TBItem31: TTBItem
          Action = acSOProd
        end
        object TBItem23: TTBItem
          Action = acBulkList
        end
        object TBItem76: TTBItem
          Action = acSCharge
        end
        object TBItem68: TTBItem
          Action = acAffinaj1
        end
        object TBSeparatorItem4: TTBSeparatorItem
        end
        object TBItem88: TTBItem
          Action = acActWork
        end
        object TBSeparatorItem26: TTBSeparatorItem
        end
        object TBItem33: TTBItem
          Action = acAppl
        end
        object TBSeparatorItem3: TTBSeparatorItem
        end
        object TBItem32: TTBItem
          Action = acWhSemis
          ImageIndex = 33
        end
        object TBItem21: TTBItem
          Action = acWhArt
        end
        object TBItem118: TTBItem
          Action = acAssayOfficeStickersPrint
        end
        object TBSeparatorItem20: TTBSeparatorItem
        end
        object TBItem18: TTBItem
          Action = acProtocol
          Caption = #1055#1088#1086#1090#1086#1082#1086#1083' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
        end
        object ItemProtocolPS: TTBItem
          Action = acProtocolPS
          ImageIndex = 15
          Visible = False
        end
        object TBItem22: TTBItem
          Action = acVList
        end
        object TBItem98: TTBItem
          Action = acMatBalance
        end
        object TBSeparatorItem23: TTBSeparatorItem
        end
        object TBItem20: TTBItem
          Action = acZp
        end
      end
      object TBSeparatorItem15: TTBSeparatorItem
      end
      object sitWh: TTBSubmenuItem
        Caption = #1057#1082#1083#1072#1076' '#1075#1086#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
        Images = im16
        object TBItem65: TTBItem
          Action = acInv
        end
        object TBItem63: TTBItem
          Action = acSell
        end
        object TBItem28: TTBItem
          Action = acRet
        end
        object TBItem27: TTBItem
          Action = acRetProd
        end
        object TBSeparatorItem19: TTBSeparatorItem
        end
        object TBItem34: TTBItem
          Action = acPriceAct
        end
        object TBSeparatorItem5: TTBSeparatorItem
        end
        object TBSubmenuItem5: TTBSubmenuItem
          Caption = #1057#1082#1083#1072#1076
          object TBItem97: TTBItem
            Action = acStoreByItems
          end
          object TBItem96: TTBItem
            Action = acStoreByArt
          end
          object TBItem95: TTBItem
            Action = acHistory
          end
          object TBSeparatorItem29: TTBSeparatorItem
          end
          object TBSubmenuItem9: TTBSubmenuItem
            Caption = #1053#1072#1083#1080#1095#1080#1077' '#1084#1077#1090#1072#1083#1083#1072' '#1074' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
            object TBItem120: TTBItem
              Tag = 1
              Caption = #1047#1086#1083#1086#1090#1086
              ImageIndex = 70
              Images = im16
              OnClick = TBItem120Click
            end
            object TBItem121: TTBItem
              Tag = 3
              Caption = #1057#1077#1088#1077#1073#1088#1086
              ImageIndex = 70
              Images = im16
              OnClick = TBItem121Click
            end
          end
        end
        object TBSeparatorItem6: TTBSeparatorItem
        end
        object TBItem84: TTBItem
          Action = acCassaPays
          ImageIndex = 41
        end
        object TBItem26: TTBItem
          Action = acCashBank
        end
        object TBSeparatorItem24: TTBSeparatorItem
        end
        object TBItem36: TTBItem
          Action = acReport
        end
        object TBSubmenuItem6: TTBSubmenuItem
          Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086' '#1085#1072#1082#1083#1072#1076#1085#1099#1084
          ImageIndex = 24
          object TBItem35: TTBItem
            Action = acInvOborot0
          end
          object TBItem104: TTBItem
            Action = acInvOborot1
          end
          object TBItem105: TTBItem
            Action = acInvOborot2
          end
        end
        object TBItem102: TTBItem
          Action = acInvOborotExt
        end
        object TBSubmenuItem7: TTBSubmenuItem
          Caption = #1054#1073#1086#1088#1086#1090#1082#1072' '#1087#1086' '#1076#1072#1074#1072#1083#1100#1095#1077#1089#1082#1080#1084' '#1085#1072#1082#1083#1072#1076#1085#1099#1084
          ImageIndex = 24
          object TBItem93: TTBItem
            Action = acTolling0
            ImageIndex = 24
          end
          object TBItem106: TTBItem
            Action = acTolling1
            ImageIndex = 24
          end
          object TBItem107: TTBItem
            Action = acTolling3
            ImageIndex = 24
          end
        end
        object TBSeparatorItem7: TTBSeparatorItem
        end
        object TBItem37: TTBItem
          Action = acImportStore
        end
        object TBItem64: TTBItem
          Action = acPriceDict
        end
        object TBItem71: TTBItem
          Action = acLostStones
        end
        object TBItem72: TTBItem
          Action = acReportKolie
        end
        object TBItem74: TTBItem
          Action = acOutREport
        end
        object TBItem92: TTBItem
          Action = acPriceList
        end
        object TBItem94: TTBItem
          Action = acRevision
        end
        object TBItem99: TTBItem
          Action = acAWMatOff
        end
      end
      object TBSeparatorItem16: TTBSeparatorItem
      end
      object TBSubmenuItem2: TTBSubmenuItem
        Caption = #1057#1077#1088#1074#1080#1089
        object TBSubmenuItem4: TTBSubmenuItem
          Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
          ImageIndex = 76
          object TBItem47: TTBItem
            Action = acSemis
          end
          object TBItem69: TTBItem
            Action = acSemisAff
          end
          object TBItem46: TTBItem
            Action = acMat
          end
          object TBItem45: TTBItem
            Action = acOperation
          end
          object TBItem44: TTBItem
            Action = acPrill
          end
          object TBItem43: TTBItem
            Action = acTails
          end
          object TBItem42: TTBItem
            Action = acTech
          end
          object TBItem114: TTBItem
            Action = acOperOrder
            Caption = #1058#1077#1093#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1080#1077' '#1082#1072#1088#1090#1099
            ImageIndex = 43
          end
          object TBItem70: TTBItem
            Action = acRej
          end
          object TBSeparatorItem8: TTBSeparatorItem
          end
          object TBItem52: TTBItem
            Action = acArt
          end
          object TBItem59: TTBItem
            Action = acArtCode
          end
          object TBItem50: TTBItem
            Action = acSz
          end
          object TBItem81: TTBItem
            Action = acSzDiamond
          end
          object TBItem51: TTBItem
            Action = acIns
          end
          object TBSubmenuItem1: TTBSubmenuItem
            Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080' '#1074#1089#1090#1072#1074#1086#1082
            object TBItem41: TTBItem
              Action = acEdgShape
            end
            object TBItem40: TTBItem
              Action = acEdgT
            end
            object TBItem39: TTBItem
              Action = acColor
            end
            object TBItem49: TTBItem
              Action = acCleannes
            end
            object TBItem48: TTBItem
              Action = acChromaticity
            end
            object TBItem53: TTBItem
              Action = acIGr
            end
          end
          object TBSeparatorItem10: TTBSeparatorItem
          end
          object TBItem54: TTBItem
            Action = acComp
          end
          object TBItem85: TTBItem
            Action = acClient
            Visible = False
          end
          object TBItem86: TTBItem
            Action = acContractType
          end
          object TBItem57: TTBItem
            Action = acNDS
          end
          object TBItem77: TTBItem
            Action = acPaytype
          end
          object TBItem56: TTBItem
            Action = acDiscount
            Visible = False
          end
          object TBSeparatorItem13: TTBSeparatorItem
          end
          object TBItem55: TTBItem
            Action = acPriceDict
          end
          object TBSeparatorItem21: TTBSeparatorItem
          end
          object TBItem111: TTBItem
            Caption = #1058#1077#1093#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1080#1077' '#1087#1072#1088#1072#1084#1077#1090#1088#1099' '#1094#1077#1087#1077#1081' '#1080' '#1073#1088#1072#1089#1083#1077#1090#1086#1074
            OnClick = TBItem111Click
          end
        end
        object TBItem58: TTBItem
          Action = acChangePswd
        end
        object TBSeparatorItem12: TTBSeparatorItem
        end
        object TBItem66: TTBItem
          Action = acZpK
        end
        object TBSeparatorItem9: TTBSeparatorItem
        end
        object TBItem75: TTBItem
          Action = acInitStone
        end
        object TBItem78: TTBItem
          Action = acCheckStone
          Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1088#1072#1089#1093#1086#1076#1086#1074#1072#1085#1080#1103' '#1076#1088#1072#1075'. '#1082#1072#1084#1085#1077#1081
        end
        object TBItem61: TTBItem
          Action = acMWArt
        end
        object TBItem60: TTBItem
          Action = acCalcSemisPrice
        end
        object TBItem87: TTBItem
          Action = acInvColor
        end
        object TBSeparatorItem22: TTBSeparatorItem
        end
        object TBItem79: TTBItem
          Action = acExportMC
        end
        object TBItem80: TTBItem
          Action = acImportMC
        end
        object TBSeparatorItem30: TTBSeparatorItem
        end
        object TBItem123: TTBItem
          Tag = 1
          Caption = #1057#1086#1079#1076#1072#1090#1100' '#1096#1072#1073#1083#1086#1085#1099' ('#1079#1086#1083#1086#1090#1086')'
          OnClick = TBItem123Click
        end
        object TBItem122: TTBItem
          Tag = 3
          Caption = #1057#1086#1079#1076#1072#1090#1100' '#1096#1072#1073#1083#1086#1085#1099' ('#1089#1077#1088#1077#1073#1088#1086')'
          OnClick = TBItem123Click
        end
        object TBSeparatorItem14: TTBSeparatorItem
        end
        object TBItem73: TTBItem
          Action = acInvReportExecute
        end
        object TBSeparatorItem25: TTBSeparatorItem
        end
        object TBItem38: TTBItem
          Action = acMOL
          Images = im16
        end
        object TBItem62: TTBItem
          Action = acSetting
        end
        object TBItem109: TTBItem
          Caption = #1053#1086#1088#1084#1099' '#1087#1086#1090#1077#1088#1100
        end
      end
      object TBSeparatorItem17: TTBSeparatorItem
      end
      object TBSubmenuItem3: TTBSubmenuItem
        Caption = #1040#1085#1072#1083#1080#1079
        object TBItem82: TTBItem
          Action = acNeedStone
        end
        object TBItem83: TTBItem
          Action = acAnlzRej
        end
        object TBItem108: TTBItem
          Caption = #1055#1088#1086#1076#1072#1078#1080
          OnClick = TBItem108Click
        end
        object TBItem119: TTBItem
          Action = acBuyReport
        end
        object TBSeparatorItem28: TTBSeparatorItem
        end
        object TBSubmenuItem8: TTBSubmenuItem
          Caption = #1060#1080#1085#1072#1085#1089#1086#1074#1099#1081' '#1084#1086#1085#1080#1090#1086#1088#1080#1085#1075
          object TBItem116: TTBItem
            Action = acFinMonitoringSells
          end
          object TBItem115: TTBItem
            Action = acFinMonitoringReturns
          end
          object TBItem117: TTBItem
            Action = acFinMonitoringPayments
          end
        end
      end
      object TBSeparatorItem18: TTBSeparatorItem
      end
      object TBItem25: TTBItem
        Action = acExit
      end
      object TBSeparatorItem27: TTBSeparatorItem
      end
      object TBItem24: TTBItem
        Action = acWhatsNew
        Visible = False
      end
    end
  end
  object mmMain: TMainMenu
    Images = dm.ilButtons
    Left = 708
    Top = 76
    object mnitProd: TMenuItem
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      object mnitWOrder: TMenuItem
        Action = acWOrder
      end
      object N36: TMenuItem
        Caption = #1047#1072#1103#1074#1082#1080
        ImageIndex = 60
        OnClick = acApplProdExecute
      end
      object N22: TMenuItem
        Action = acSInvProd
      end
      object N28: TMenuItem
        Caption = '-'
      end
      object N56: TMenuItem
        Action = acZp
      end
      object N57: TMenuItem
        Action = acProtocol
      end
    end
    object mnitWh: TMenuItem
      Caption = #1057#1082#1083#1072#1076' '#1075#1086#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
      object N12: TMenuItem
        Action = acInv
      end
      object N14: TMenuItem
        Action = acRet
      end
      object N15: TMenuItem
        Action = acSell
      end
      object N16: TMenuItem
        Action = acRetProd
      end
      object N21: TMenuItem
        Action = acStoreByArt
      end
      object N25: TMenuItem
        Action = acStoreByItems
      end
      object N19: TMenuItem
        Action = acPriceAct
      end
      object N66: TMenuItem
        Caption = '-'
      end
      object N20: TMenuItem
        Action = acReport
      end
      object N60: TMenuItem
        Action = acInvOborot0
      end
      object N67: TMenuItem
        Caption = '-'
      end
      object N68: TMenuItem
        Action = acImportStore
      end
      object N3: TMenuItem
        Action = acReportKolie
      end
      object N6: TMenuItem
        Action = acOutREport
      end
    end
    object mnitDict: TMenuItem
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      object N40: TMenuItem
        Action = acSemis
      end
      object N41: TMenuItem
        Action = acMat
      end
      object N38: TMenuItem
        Action = acOperation
      end
      object N42: TMenuItem
        Action = acPrill
      end
      object N43: TMenuItem
        Action = acTails
      end
      object N70: TMenuItem
        Action = acTech
      end
      object N31: TMenuItem
        Caption = '-'
      end
      object N45: TMenuItem
        Action = acEdgShape
      end
      object N46: TMenuItem
        Action = acEdgT
      end
      object N33: TMenuItem
        Action = acColor
      end
      object N34: TMenuItem
        Action = acChromaticity
      end
      object N1: TMenuItem
        Action = acCleannes
      end
      object N4: TMenuItem
        Action = acIGr
      end
      object N47: TMenuItem
        Caption = '-'
      end
      object N39: TMenuItem
        Action = acArt
      end
      object N37: TMenuItem
        Action = acIns
      end
      object N5: TMenuItem
        Action = acSz
      end
      object acPriceDict1: TMenuItem
        Action = acPriceDict
      end
      object N32: TMenuItem
        Caption = '-'
      end
      object N49: TMenuItem
        Action = acComp
      end
      object N50: TMenuItem
        Action = acNDS
      end
      object N51: TMenuItem
        Action = acDiscount
      end
      object N30: TMenuItem
        Caption = '-'
      end
      object N35: TMenuItem
        Action = acArtCode
      end
    end
    object N64: TMenuItem
      Caption = #1057#1077#1088#1074#1080#1089
      object N65: TMenuItem
        Action = acChangePswd
      end
    end
    object mnitReport: TMenuItem
      Caption = #1054#1090#1095#1077#1090#1099
      Visible = False
      object N62: TMenuItem
        Caption = #1055#1086' '#1084#1077#1089#1090#1072#1084' '#1093#1088#1072#1085#1077#1085#1080#1103
        object N13: TMenuItem
          Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086' '#1079#1086#1083#1086#1090#1091
        end
        object mnitJGroup: TMenuItem
          Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086' '#1075#1088#1091#1087#1087#1077
        end
        object N44: TMenuItem
          Caption = '-'
        end
        object N48: TMenuItem
          Caption = #1057#1074#1086#1076#1085#1072#1103' '#1086#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086' '#1079#1086#1083#1086#1090#1091
        end
        object N53: TMenuItem
          Caption = #1057#1074#1086#1076#1085#1072#1103' '#1086#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086' '#1073#1088#1080#1083#1083#1080#1072#1085#1090#1072#1084
        end
        object N52: TMenuItem
          Caption = #1057#1074#1086#1076#1085#1072#1103' '#1086#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086' '#1075#1088#1091#1087#1087#1077
        end
        object N54: TMenuItem
          Caption = '-'
        end
      end
      object N63: TMenuItem
        Caption = #1055#1086' '#1082#1083#1072#1076#1086#1074#1099#1084
      end
    end
    object mnitAdmin: TMenuItem
      Caption = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1080#1088#1086#1074#1072#1085#1080#1077
      object mnitUser: TMenuItem
        Action = acMOL
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object N58: TMenuItem
        Action = acMWArt
      end
      object N69: TMenuItem
        Action = acCalcSemisPrice
      end
      object N29: TMenuItem
        Caption = #1054#1096#1080#1073#1082#1080
        object woItemwOrderIdInvwOrderId1: TMenuItem
          Action = acErrWOrder
        end
        object N55: TMenuItem
          Action = acErrART
        end
        object N11: TMenuItem
          Action = acErrWhArt
        end
        object N27: TMenuItem
          Action = acErrWhSemis
        end
        object UAOperIdSItem1: TMenuItem
          Action = acErrSInvProd
        end
      end
      object N61: TMenuItem
        Action = acTmpWhSemis
      end
      object N59: TMenuItem
        Caption = '-'
      end
      object N17: TMenuItem
        Action = acSetting
      end
    end
    object N18: TMenuItem
      Action = acExit
    end
  end
  object acEvent: TActionList
    Images = im16
    Left = 708
    Top = 108
    object acTolling0: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1052#1072#1090#1077#1088#1080#1072#1083': '#1042#1089#1077
      OnExecute = acTollingExecute
    end
    object acTolling1: TAction
      Tag = 1
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1052#1072#1090#1077#1088#1080#1072#1083': '#1047#1086#1083#1086#1090#1086
      OnExecute = acTollingExecute
    end
    object acAffinaj1: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1051#1086#1084' ...'
      ImageIndex = 84
      OnExecute = acAffinaj1Execute
    end
    object acInvOborot0: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1052#1072#1090#1077#1088#1080#1072#1083': '#1042#1089#1077
      ImageIndex = 24
      OnExecute = acInvOborotExecute
    end
    object acAffinaj2: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1057#1098#1077#1084' '#1079#1086#1083#1086#1090#1072' ...'
      ImageIndex = 84
      OnExecute = acAffinaj2Execute
    end
    object acAffinaj3: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1057#1098#1077#1084' '#1089#1077#1088#1076#1077#1095#1085#1080#1082#1072
      ImageIndex = 84
      OnExecute = acAffinaj3Execute
    end
    object acTolling3: TAction
      Tag = 3
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1052#1072#1090#1077#1088#1080#1072#1083': '#1057#1077#1088#1077#1073#1088#1086
      OnExecute = acTollingExecute
    end
    object acExit: TAction
      Caption = #1042#1099#1093#1086#1076
      ShortCut = 123
      OnExecute = acExitExecute
    end
    object acInvOborot1: TAction
      Tag = 1
      Category = #1057#1082#1083#1072#1076
      Caption = #1052#1072#1090#1077#1088#1080#1072#1083': '#1047#1086#1083#1086#1090#1086
      ImageIndex = 24
      OnExecute = acInvOborotExecute
    end
    object acInvOborot2: TAction
      Tag = 3
      Category = #1057#1082#1083#1072#1076
      Caption = #1052#1072#1090#1077#1088#1080#1072#1083': '#1057#1077#1088#1077#1073#1088#1086
      ImageIndex = 24
      OnExecute = acInvOborotExecute
    end
    object acOperation: TAction
      Tag = 1
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      ImageIndex = 12
      OnExecute = acOperationExecute
    end
    object acSemis: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      ImageIndex = 87
      OnExecute = acSemisExecute
    end
    object acMOL: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1080
      ImageIndex = 3
      OnExecute = acMOLExecute
    end
    object acWOrder: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1053#1072#1088#1103#1076
      ImageIndex = 0
      OnExecute = acWOrderExecute
    end
    object acProtocol_2: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1055#1088#1086#1090#1086#1082#1086#1083
      ImageIndex = 15
      OnExecute = acProtocol_2Execute
    end
    object acComp: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
      ImageIndex = 36
      OnExecute = acCompExecute
    end
    object acSetting: TAction
      Category = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
      ImageIndex = 1
      OnExecute = acSettingExecute
    end
    object acArt: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1040#1088#1090#1080#1082#1091#1083#1099
      ImageIndex = 13
      OnExecute = acArtExecute
    end
    object acMat: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1052#1072#1090#1077#1088#1080#1072#1083#1099
      ImageIndex = 85
      OnExecute = acMatExecute
    end
    object acPrill: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1055#1088#1086#1073#1099
      Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1087#1088#1086#1073
      OnExecute = acPrillExecute
    end
    object acTails: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1042#1080#1076#1099' '#1086#1090#1093#1086#1076#1086#1074
      Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1074#1080#1076#1086#1074' '#1086#1090#1093#1086#1076#1086#1074
      ImageIndex = 17
      OnExecute = acTailsExecute
    end
    object acNDS: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1053#1044#1057
      Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1053#1044#1057
      ImageIndex = 68
      OnExecute = acNDSExecute
    end
    object acDiscount: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1057#1082#1080#1076#1082#1080
      Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1089#1082#1080#1076#1086#1082
      OnExecute = acDiscountExecute
    end
    object acIns: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1042#1089#1090#1072#1074#1082#1080
      Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1074#1089#1090#1072#1074#1086#1082
      ImageIndex = 74
      OnExecute = acInsExecute
    end
    object acEdgShape: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1060#1086#1088#1084#1072' '#1086#1075#1088#1072#1085#1082#1080
      Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1092#1086#1088#1084' '#1086#1075#1088#1072#1085#1082#1080
      ImageIndex = 72
      OnExecute = acEdgShapeExecute
    end
    object Action1: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = 'acOperOrder'
    end
    object acEdgT: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1058#1080#1087' '#1086#1075#1088#1072#1085#1082#1080
      Hint = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1090#1080#1087#1086#1074' '#1086#1075#1088#1072#1085#1082#1080
      ImageIndex = 37
      OnExecute = acEdgTExecute
    end
    object acAppl: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1047#1072#1082#1072#1079
      Hint = #1057#1087#1080#1089#1086#1082' '#1079#1072#1103#1074#1086#1082
      ImageIndex = 60
      OnExecute = acApplExecute
    end
    object acInv: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1055#1086#1089#1090#1072#1074#1082#1072
      HelpKeyword = #1057#1087#1080#1089#1086#1082' '#1087#1086#1089#1090#1072#1074#1086#1082
      ImageIndex = 51
      OnExecute = acInv1Execute
      OnUpdate = acInvUpdate
    end
    object acZp: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1047#1072#1088#1087#1083#1072#1090#1072
      ImageIndex = 71
      OnExecute = acZpExecute
    end
    object acProtocol: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1055#1088#1086#1090#1086#1082#1086#1083
      ImageIndex = 15
      OnExecute = acProtocolExecute
    end
    object acSell: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1055#1088#1086#1076#1072#1078#1072
      HelpKeyword = #1057#1087#1080#1089#1086#1082' '#1087#1088#1086#1076#1072#1078
      ImageIndex = 50
      OnExecute = acSellExecute
    end
    object acSz: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1056#1072#1079#1084#1077#1088#1099
      ImageIndex = 45
      OnExecute = acSzExecute
    end
    object acRet: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1042#1086#1079#1074#1088#1072#1090
      ImageIndex = 48
      OnExecute = acRetExecute
    end
    object acRetProd: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1042#1086#1079#1074#1088#1072#1090' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      ImageIndex = 49
      OnExecute = acRetProdExecute
    end
    object acStoreByArt: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076' '#1087#1086'-'#1072#1088#1090#1080#1082#1091#1083#1100#1085#1086
      ImageIndex = 10
      OnExecute = acStoreByArtExecute
    end
    object acStoreByItems: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1057#1082#1083#1072#1076' '#1087#1086'-'#1080#1079#1076#1077#1083#1100#1085#1086
      ImageIndex = 31
      OnExecute = acStoreByItemsExecute
    end
    object acColor: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1062#1074#1077#1090#1072
      ImageIndex = 73
      OnExecute = acColorExecute
    end
    object acCleannes: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1063#1080#1089#1090#1086#1090#1072
      OnExecute = acCleannesExecute
    end
    object acChromaticity: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1062#1074#1077#1090#1085#1086#1089#1090#1100
      ImageIndex = 78
      OnExecute = acChromaticityExecute
    end
    object acIGr: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1043#1088#1091#1087#1087#1072
      OnExecute = acIGrExecute
    end
    object acReport: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086' '#1072#1088#1090#1080#1082#1091#1083#1072#1084
      ImageIndex = 29
      OnExecute = acReportExecute
    end
    object acSInvProd: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1054#1090#1075#1088#1091#1079#1082#1072' '#1075#1086#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
      ImageIndex = 44
      OnExecute = acSInvProdExecute
    end
    object acPriceDict: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1094#1077#1085
      ImageIndex = 30
      OnExecute = acPriceDictExecute
    end
    object acPriceAct: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1040#1082#1090#1099' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
      ImageIndex = 28
      OnExecute = acPriceActExecute
    end
    object acErrWOrder: TAction
      Category = #1054#1096#1080#1073#1082#1080
      Caption = #1042' '#1085#1072#1088#1103#1076#1072' woItem.wOrderId->Inv.wOrderId '
      OnExecute = acErrWOrderExecute
    end
    object acArtCode: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1050#1086#1076#1080#1092#1080#1082#1072#1094#1080#1103' '#1072#1088#1090#1080#1082#1091#1083#1072
      OnExecute = acArtCodeExecute
    end
    object acSInv: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1055#1086#1089#1090#1091#1087#1083#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      ImageIndex = 57
      OnExecute = acSInvExecute
    end
    object acErrART: TAction
      Category = #1054#1096#1080#1073#1082#1080
      Caption = #1054#1096#1080#1073#1082#1080' '#1074' '#1072#1088#1090#1080#1082#1091#1083#1072#1093
      OnExecute = acErrARTExecute
    end
    object acTransfer: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      ImageIndex = 69
      OnExecute = acTransferExecute
    end
    object acErrWhArt: TAction
      Category = #1054#1096#1080#1073#1082#1080
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090' '#1089#1082#1083#1072#1076#1072' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      OnExecute = acErrWhArtExecute
    end
    object acErrWhSemis: TAction
      Category = #1054#1096#1080#1073#1082#1080
      Caption = #1055#1088#1077#1089#1095#1077#1090' '#1089#1082#1083#1072#1076#1072' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
      OnExecute = acErrWhSemisExecute
    end
    object acErrSInvProd: TAction
      Category = #1054#1096#1080#1073#1082#1080
      Caption = #1053#1077' '#1079#1072#1087#1086#1083#1085#1077#1085#1099' '#1087#1086#1083#1103' UA, OperId '#1074' '#1090#1072#1073#1083#1080#1094#1077' SItem'
      OnExecute = acErrSInvProdExecute
    end
    object acWhArt: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1057#1082#1083#1072#1076' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      ImageIndex = 77
      OnExecute = acWhArtExecute
    end
    object acWhSemis: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1057#1082#1083#1072#1076' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
      ImageIndex = 32
      OnExecute = acWhSemisExecute
    end
    object acMWArt: TAction
      Category = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090' '#1089#1088#1077#1076#1085#1077#1075#1086' '#1074#1077#1089#1072' '#1072#1088#1090#1080#1082#1091#1083#1086#1074
      ImageIndex = 4
      Visible = False
      OnExecute = acMWArtExecute
    end
    object acSOProd: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1056#1072#1089#1093#1086#1076' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      ImageIndex = 58
      OnExecute = acSOProdExecute
    end
    object acTmpWhSemis: TAction
      Category = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1057#1082#1083#1072#1076' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      OnExecute = acTmpWhSemisExecute
    end
    object acChangePswd: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1087#1072#1088#1086#1083#1100
      ImageIndex = 8
      OnExecute = acChangePswdExecute
    end
    object acVList: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1057#1083#1080#1095#1080#1090#1077#1083#1100#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100
      ImageIndex = 54
      OnExecute = acVListExecute
    end
    object acImportStore: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1047#1072#1075#1088#1091#1079#1080#1090#1100' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1102' '#1086' '#1089#1082#1083#1072#1076#1077
      ImageIndex = 27
      OnExecute = acImportStoreExecute
    end
    object acBulkList: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1040#1082#1090' '#1089#1089#1099#1087#1082#1080
      ImageIndex = 56
      OnExecute = acBulkListExecute
    end
    object acCalcSemisPrice: TAction
      Category = #1040#1076#1084#1080#1085#1080#1089#1090#1088#1080#1088#1086#1074#1072#1085#1080#1077
      Caption = #1055#1077#1088#1077#1089#1095#1077#1090' '#1089#1088#1077#1076#1085#1080#1093' '#1094#1077#1085' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      ImageIndex = 4
      Visible = False
      OnExecute = acCalcSemisPriceExecute
    end
    object acTech: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1058#1077#1093#1085#1086#1083#1086#1075#1080#1080
      ImageIndex = 52
      OnExecute = acTechExecute
    end
    object acZpK: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090#1099' '#1076#1083#1103' '#1088#1072#1089#1095#1077#1090#1072' '#1079#1072#1088#1087#1083#1072#1090#1099
      OnExecute = acZpKExecute
    end
    object acSemisAff: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099', '#1089#1086#1076#1077#1088#1078#1072#1097#1080#1077' '#1076#1088#1072#1075'.'#1084#1077#1090#1072#1083#1083#1099
      OnExecute = acSemisAffExecute
    end
    object acRej: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1055#1088#1080#1095#1080#1085#1099' '#1073#1088#1072#1082#1072
      OnExecute = acRejExecute
    end
    object acLostStones: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1055#1086#1090#1077#1088#1103#1085#1085#1099#1077' '#1082#1072#1084#1085#1080
      ImageIndex = 86
      OnExecute = acLostStonesExecute
    end
    object acReportKolie: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1054#1090#1095#1077#1090' '#1087#1086' '#1096#1085#1091#1088#1082#1072#1084
      OnExecute = acReportKolieExecute
    end
    object acInvReportExecute: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1042#1099#1075#1088#1091#1079#1082#1072' '#1074' 1'#1057
      ImageIndex = 88
      OnExecute = acInvReportExecuteExecute
    end
    object acOutREport: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1054#1090#1095#1077#1090' '#1087#1086' '#1087#1088#1086#1076#1072#1078#1072#1084' '
      ImageIndex = 80
      OnExecute = acOutREportExecute
    end
    object acInitStone: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1053#1072#1095#1072#1083#1100#1085#1099#1077' '#1076#1072#1085#1085#1099#1077' '#1087#1086' '#1076#1088#1072#1075'. '#1082#1072#1084#1085#1103#1084
      OnExecute = acInitStoneExecute
      OnUpdate = acInitStoneUpdate
    end
    object acSCharge: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1040#1082#1090' '#1089#1087#1080#1089#1072#1085#1080#1103' '#1076#1088#1072#1075'.'#1082#1072#1084#1085#1077#1081
      OnExecute = acSChargeExecute
    end
    object acPaytype: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1042#1080#1076#1099' '#1086#1087#1083#1072#1090#1099
      ImageIndex = 35
      OnExecute = acPaytypeExecute
    end
    object acCheckStone: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1057#1074#1077#1088#1082#1072' '#1074#1089#1090#1072#1074#1086#1082
      OnExecute = acCheckStoneExecute
    end
    object acExportMC: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' '#1052#1086#1073#1080#1083#1100#1085#1099#1081' '#1082#1083#1080#1077#1085#1090
      ImageIndex = 89
      OnExecute = acExportMCExecute
    end
    object acImportMC: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1048#1084#1087#1086#1088#1090' '#1080#1079' '#1052#1086#1073#1080#1083#1100#1085#1086#1075#1086' '#1082#1083#1080#1077#1085#1090#1072
      ImageIndex = 90
      OnExecute = acImportMCExecute
    end
    object acSzDiamond: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1056#1072#1079#1084#1077#1088#1099' '#1073#1088#1080#1083#1083#1080#1072#1085#1090#1086#1074
      OnExecute = acSzDiamondExecute
    end
    object acNeedStone: TAction
      Category = #1040#1085#1072#1083#1080#1079
      Caption = #1055#1086#1090#1088#1077#1073#1085#1086#1089#1090#1100' '#1074' '#1076#1088#1072#1075#1086#1094#1077#1085#1085#1099#1093' '#1082#1072#1084#1085#1103#1093
      OnExecute = acNeedStoneExecute
    end
    object acAnlzRej: TAction
      Category = #1040#1085#1072#1083#1080#1079
      Caption = #1041#1088#1072#1082' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072
      OnExecute = acAnlzRejExecute
    end
    object acCassaPays: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1050#1072#1089#1089#1072' - '#1086#1087#1090
      OnExecute = acCassaPaysExecute
    end
    object acClient: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1056#1086#1079#1085#1080#1095#1085#1099#1077' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1080
      ImageIndex = 91
      OnExecute = acClientExecute
    end
    object acContractType: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1042#1080#1076#1099' '#1076#1086#1075#1086#1074#1086#1088#1086#1074
      OnExecute = acContractTypeExecute
    end
    object acInvColor: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1062#1074#1077#1090#1086#1074#1072#1103' '#1089#1093#1077#1084#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
      ImageIndex = 96
      OnExecute = acInvColorExecute
    end
    object acActWork: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1040#1082#1090' '#1074#1099#1087#1086#1083#1085#1077#1085#1085#1099#1093' '#1088#1072#1073#1086#1090
      ImageIndex = 92
      OnExecute = acActWorkExecute
    end
    object acActVerification: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1040#1082#1090' '#1089#1074#1077#1088#1082#1080
      ImageIndex = 93
      OnExecute = acActVerificationExecute
    end
    object acRequest: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1047#1072#1103#1074#1082#1072
      ImageIndex = 94
      OnExecute = acRequestExecute
    end
    object acPriceList: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1055#1088#1072#1081#1089' '#1051#1080#1089#1090
      OnExecute = acPriceListExecute
    end
    object acRevision: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
      ImageIndex = 56
      OnExecute = acRevisionExecute
    end
    object acHistory: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1048#1089#1090#1086#1088#1080#1103' '#1080#1079#1076#1077#1083#1080#1081
      ImageIndex = 95
      OnExecute = acHistoryExecute
    end
    object acWhatsNew: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1063#1090#1086' '#1085#1086#1074#1086#1075#1086'?'
      OnExecute = acWhatsNewExecute
    end
    object acCashBank: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1041#1072#1085#1082
      OnExecute = acCashBankExecute
    end
    object acMatBalance: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1041#1072#1083#1072#1085#1089' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      OnExecute = acMatBalanceExecute
    end
    object acAWMatOff: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1057#1087#1080#1089#1072#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074' '#1087#1086' '#1072#1082#1090#1072#1084' '#1074#1099#1087#1086#1083#1085#1077#1085#1085#1099#1093' '#1088#1072#1073#1086#1090
      OnExecute = acAWMatOffExecute
    end
    object acInvOborotExt: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1054#1073#1086#1088#1086#1090#1085#1072#1103' '#1074#1077#1076#1086#1084#1086#1089#1090#1100' '#1087#1086' '#1074#1085#1077#1096#1085#1077#1084#1091' '#1090#1086#1083#1083#1080#1085#1075#1091
      ImageIndex = 24
      OnExecute = acInvOborotExtExecute
    end
    object acProtocolPS: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1084#1077#1089#1090#1072#1084' '#1093#1088#1072#1085#1077#1085#1080#1103
      OnExecute = acProtocolPSExecute
    end
    object acPaymentsAndBalance: TAction
      Category = #1057#1082#1083#1072#1076
      Caption = #1042#1079#1072#1080#1084#1086#1088#1072#1089#1095#1105#1090#1099
      ImageIndex = 65
      OnExecute = acPaymentsAndBalanceExecute
      OnUpdate = acPaymentsAndBalanceUpdate
    end
    object acVerificationActTolling: TAction
      Caption = #1040#1082#1090' '#1090#1086#1083#1083#1080#1085#1075#1072
      ImageIndex = 34
      OnExecute = acVerificationActTollingExecute
    end
    object acOperOrder: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082#1080
      Caption = #1055#1086#1088#1103#1076#1086#1082' '#1086#1087#1077#1088#1072#1094#1080#1081' '
      ImageIndex = 82
      OnExecute = acOperOrderExecute
      OnUpdate = acOperOrderUpdate
    end
    object acFinMonitoringSells: TAction
      Category = #1040#1085#1072#1083#1080#1079
      Caption = #1055#1088#1086#1076#1072#1078#1080
      OnExecute = acFinMonitoringSellsExecute
    end
    object acFinMonitoringReturns: TAction
      Category = #1040#1085#1072#1083#1080#1079
      Caption = #1042#1086#1079#1074#1088#1072#1090#1099
      OnExecute = acFinMonitoringReturnsExecute
    end
    object acFinMonitoringPayments: TAction
      Category = #1040#1085#1072#1083#1080#1079
      Caption = #1054#1087#1083#1072#1090#1099
      OnExecute = acFinMonitoringPaymentsExecute
    end
    object acAssayOfficeStickersPrint: TAction
      Category = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086
      Caption = #1041#1080#1088#1082#1080' '#1076#1083#1103' '#1087#1088#1086#1073#1080#1088#1082#1080
      ImageIndex = 16
      OnExecute = acAssayOfficeStickersPrintExecute
    end
    object acBuyReport: TAction
      Category = #1040#1085#1072#1083#1080#1079
      Caption = #1054#1090#1095#1077#1090' '#1087#1086' '#1089#1082#1091#1087#1082#1077
      OnExecute = acBuyReportExecute
    end
    object acCrudeStore: TAction
      Category = #1057#1082#1083#1072#1076
      ImageIndex = 70
      OnExecute = acCrudeStoreExecute
    end
    object acCreatePatterns: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1064#1072#1073#1083#1086#1085#1099' '#1080#1079' '#1087#1088#1072#1081#1089#1072
      OnExecute = acCreatePatternsExecute
    end
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.nlz'
    Filter = #1040#1085#1072#1083#1080#1079'(*.nlz)|*.nlz|'#1042#1089#1077'(*.*)|*.*'
    Left = 596
    Top = 108
  end
  object im32: TImageList
    Height = 32
    Width = 32
    Left = 668
    Top = 76
    Bitmap = {
      494C01010B000E00040020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      00000000000036000000280000008000000060000000010020000000000000C0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000031080000A5310800290800000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000003163000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000003163000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001063730018C6E700316B7300CE391000D64A2900B53100003108
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000316300003163009CCEFF00639CCE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000316300003163009CCEFF00639CCE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000010180031D6EF0031100000BD421000B55A3900CE735200C6421800B531
      0800310800000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000031
      6300003163009CCEFF009CCEFF009CCEFF0031639C00639CCE00639CCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000031
      6300003163009CCEFF009CCEFF009CCEFF0031639C00639CCE00639CCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000042DEF70084392100B54221009C391800AD5A4200E77B5A00B531
      0800BD3910003108000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000316300003163009CCE
      FF009CCEFF00639CCE00639CCE009CCEFF0031639C0031639C0031639C00639C
      CE00639CCE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000316300003163009CCE
      FF009CCEFF00639CCE00639CCE009CCEFF0031639C0031639C0031639C00639C
      CE00639CCE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001063730018C6
      E70031636B00B55A390073E7EF0063E7EF009C523900D6735A00E78C6B00CE5A
      3100CE4A2100BD39080031080000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000316300003163009CCEFF009CCEFF00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C00639CCE00639CCE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000316300003163009CCEFF009CCEFF00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C00639CCE00639CCE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000010180031D6EF003110
      0000BD421800B55A3900D67B6300E7846300E77B5A00E7846300E7846300E784
      6300BD421800E7633900BD390800290800000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000316300003163009CCEFF009CCEFF00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C00639CCE00639CCE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000316300003163009CCEFF009CCEFF00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C00639CCE00639CCE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000042DEF7008439
      2100B54221009C391800AD523900E78C6B00E7846B00E77B5A00E7846300E784
      6300CE5A3900DE7B5A00E75A3900BD3908003108000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000031
      6300003163009CCEFF009CCEFF00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C0031639C00639CCE00639CCE000000
      0000000000000000000000000000000000000000000000000000000000000031
      6300003163009CCEFF009CCEFF00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C0031639C00639CCE00639CCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000001063730018C6E70031636B00B55A390073E7
      EF0063E7F7009C5A3900D6735200E7846300E7846B00E7846300E77B5A00E784
      5A00E77B6300BD4A2100FFC6B500DE5A3100BD39080031080000000000000000
      0000000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639C9C003163630031636300639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639C9C003163630031636300639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000010180031D6EF0031100000BD421800B55A3900D673
      5A00E7846300E78C7300E78C6B00E77B6300E7846300E7846300E7846300E77B
      5A00E7846300D6633900D6947B00FFCEBD00DE523100BD390800290800000000
      0000000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639C9C009CCECE00CEFFFF0031636300639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639C9C009CCECE00CEFFFF0031636300639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000042DEF70084392100B54221009C391800AD5A
      4200E7846300E7846B00E78C6B00E7846B00E7845A00E7846300E7846300E784
      6300E77B5A00E7845A00BD391000FFE7CE00FFC6B500DE523100BD3908003108
      0000000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639C9C00CEFFFF009CCECE00639C9C00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C009CCEFF0031639C0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639C9C00CEFFFF009CCECE00639C9C00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00001063730018C6E70031636B00B55A390073E7EF0063E7EF009C523900D67B
      6B00E78C7300E7846300E7846B00E7846B00E7846B00E77B5A00E77B6300E784
      6300E7846300E7845A00BD391000F7C6A500FFEFD600FFC6B500DE523100BD39
      0800290800000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639C9C00639C9C00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C009CCEFF00639CCE0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639C9C00639C9C00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000010
      180031D6EF0031100000BD421800B55A3900D67B6300E7847300E7846B00E784
      6300E78C7300E78C7300E77B5A00E7846300E7846B00E7846B00E7846300E784
      6300E7846B00CE5A3900CE734A00FFDEBD00FFCEAD00FFE7CE00FFBDAD00DE4A
      29009C2900000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C009CCEFF00639CCE0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000042DEF70084392100B54A21009C391800AD523900E78C7B00E78C7300E784
      6300E7846300E78C7300E78C7300E77B5A00E78C6B00E78C7300E78C7300E78C
      6B00CE5A3900CE735200FFE7C600FFD6AD00FFD6AD00FFEFE700F79C8400CE42
      18007B2100000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C009CCEFF00639CCE0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE003163
      9C0031639C00639CCE00639CCE009CCEFF0031639C0031639C0031639C000031
      63000031630031639C0031639C0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000052630018C6E7003163
      6B00B55A390073E7EF0063E7F7009C5A4200D67B6300E7846300E78C7300E78C
      7300E7846300E7846300E78C7300E78C7300E78C6B00E7947300EF947B00D663
      4200CE734A00FFE7C600FFDEBD00FFD6B500FFF7E700F79C8400CE4218008421
      0000000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C009CCEFF00639CCE0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639CCE00639CCE00639CCE0031639C0031639C00CEFF
      FF009CCEFF000031630031639C009CCEFF0031639C000031630031639C009CCE
      FF00CEFFFF00003163000031630031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008494940029CEE70031100000BD42
      1800B55A4200D67B6300E77B5A00E78C7300E78C7300E7846B00E7846300E78C
      7300E78C7300E78C7300CE846B00B5635200BD736300D6846B00CE633900CE73
      5200FFEFDE00FFDEB500FFD6B500FFF7EF00F79C8400CE421800842100000000
      0000000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE009CCEFF009CCEFF0031639C00639CCE0031639C0031639C003163
      9C0031639C009CCEFF00639CCE0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639CCE0031639C0031639C00CEFFFF009CCEFF009CCE
      FF009CCEFF009CCEFF00639CCE0031639C000031630031639C009CCEFF009CCE
      FF009CCEFF009CCEFF00CEFFFF00003163000031630031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000039DEEF0084392100B54A
      21009C391800AD5A4200E78C7300E77B5A00E78C7300E78C7300E7846B00E78C
      6B00EFA59400EFA59400D6947B00A56B5A00C69C9C00B55A4A00845A5200738C
      9400A58C7B00D6BDA500FFF7E700F79C8400CE42180084210000000000000000
      0000000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE009CCE
      FF009CCEFF0031639C0031639C00639CCE00639CCE00639CCE00639CCE003163
      9C0031639C009CCEFF00639CCE0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF0031639C0031639C00CEFFFF009CCEFF009CCEFF009CCEFF009CCE
      FF00639CCE0031639C0031639C0031639C00639CCE00639CCE0031639C00639C
      CE009CCEFF009CCEFF009CCEFF009CCEFF00CEFFFF000031630000316300639C
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEB5A500A54A290073E7E70063E7
      EF009C523900D67B6300E78C7300E7846B00E7846300E79C8C00EFAD9C00EFAD
      9C00EFA59400EFAD9C00EFAD9C00C6A5A500C6634A009C736B0084DEFF0052BD
      FF002194F7005A738400CE846300CE4218008421000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639CCE00639CCE00639CCE009CCEFF009CCEFF003163
      9C0031639C00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE009CCEFF00639CCE0031639C0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000000031
      630031639C00CEFFFF009CCEFF009CCEFF009CCEFF009CCEFF00639CCE003163
      9C0031639C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639C
      CE0031639C00639CCE009CCEFF009CCEFF009CCEFF009CCEFF00CEFFFF000031
      6300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CE8C7300C6422100EF8C7300EFA5
      8C00EFAD9C00EFAD9C00EFAD9C00EFB5AD00EFB5AD00EFAD9C00EFB5AD00EFB5
      AD00EFB5A500EFB59C00F7BDAD00C66B52009C736B008CDEFF0039C6FF001894
      F700218CF7002194FF004A394200631000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF00639CCE00639CCE009CCEFF009CCEFF0031639C0031639C00639C
      CE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00CEFF
      FF00CEFFFF009CCEFF009CCEFF0031639C0031639C0031639C0031639C00639C
      CE0000000000000000000000000000000000000000000031630031639C00CEFF
      FF009CCEFF009CCEFF009CCEFF009CCEFF00639CCE0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE0031639C00639CCE009CCEFF009CCEFF009CCEFF009CCE
      FF00CEFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7BDAD00B5290000DE8C7300EFAD
      9400F7BDB500F7BDB500F7BDB500F7BDB500F7BDB500F7BDAD00F7B5A500F7BD
      B500F7BDB500F7C6B500D66B4A00A573630073D6FF0039CEFF0000BDFF0000BD
      FF002194F700218CF7002194FF00002142000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF009CCEFF009CCEFF0031639C0031639C00639CCE00639CCE003163
      9C0031639C00639CCE00639CCE00639CCE00639CCE00CEFFFF00CEFFFF009CCE
      FF009CCEFF00639CCE00639CCE00639CCE00639CCE0031639C0031639C00639C
      CE000000000000000000000000000000000000316300CEFFFF009CCEFF009CCE
      FF009CCEFF009CCEFF00639CCE0031639C0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE00639CCE0031639C00639CCE009CCEFF009CCE
      FF009CCEFF009CCEFF00CEFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094210000D6634200E7AD
      9C00EFB5A500F7C6BD00F7C6BD00F7C6BD00F7CEBD00F7CEBD00F7C6BD00F7C6
      B500F7CEC600D6735A00CE7B5A00FFE7D6004ACEFF0010EFFF0000BDFF0000BD
      FF0000BDFF002194F700218CF7002194FF000821420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003163
      9C009CCEFF0031639C0031639C00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE0031639C0031639C00CEFFFF00CEFFFF009CCEFF009CCEFF00639C
      CE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639C
      CE00000000000000000000000000000000000000000000316300003163009CCE
      FF00639CCE0031639C0031639C0031639C0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE0031639C00639C
      CE009CCEFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000010000000BD310000EFAD
      9C00EFB5A500F7C6B500F7CEC600F7D6C600F7CEC600F7D6C600F7D6CE00F7D6
      CE00D6735200CE846B00FFFFFF00FFEFDE0042E7EF0010EFFF0018EFFF0000BD
      FF0000BDFF0000BDFF002194F700218CF7002994FF0008214200000000000000
      0000000000000000000000000000000000000000000000000000000000003163
      9C0031639C00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE00CEFFFF00CEFFFF009CCEFF009CCEFF00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE000031
      6300003163000000000000000000000000000000000000000000000000000031
      6300639CCE0031639C0031639C0031639C0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639C
      CE00003163000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084210000CE5A
      3900F7DED600EFC6B500F7CEBD00F7D6D600F7DED600F7DED600FFE7DE00D67B
      5A00CE7B6300FFE7CE00FFFFFF00FFFFF700EFF7F70021EFFF0018EFFF0018EF
      FF0000BDFF0000BDFF0000BDFF002194F700218CF7002194FF00082142000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000031639C0031639C00639CCE00639CCE00639CCE00639CCE00CEFF
      FF00CEFFFF009CCEFF009CCEFF00639CCE00639CCE0031639C0031639C00639C
      CE00639CCE00639CCE00639CCE00639CCE00639CCE0000316300003163000000
      000000000000000000000000000000000000000000000031630000316300639C
      CE00639CCE00639CCE00639CCE0031639C0031639C0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE00639CCE00639CCE00CEFFFF009CCEFF009CCE
      FF009CCEFF000031630000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B529
      0000EFB5A500F7DEDE00EFCEC600F7D6CE00FFE7DE00FFEFE700D67B6300CE84
      6300FFFFFF00FFFFF700FFDEBD00FFFFFF00FFAD8C00BD52310021EFFF0018EF
      FF0018EFFF0000BDFF0000BDFF0000BDFF002194F700218CF7002994FF000821
      4200000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000031639C0031639C00CEFFFF00CEFFFF009CCE
      FF009CCEFF00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE003163
      9C0031639C00639CCE00639CCE00003163000031630000000000000000000000
      000000000000000000000000000000000000003163009CCEFF00639CCE00639C
      CE00639CCE00639CCE00639CCE00639CCE00639CCE0031639C0031639C003163
      9C0031639C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE00CEFFFF009CCEFF009CCEFF009CCEFF009CCE
      FF009CCEFF00639CCE00639CCE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006B18
      0000CE522900FFEFEF00FFE7E700F7DED600FFEFE700D67B6300CE846B00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFAD9400CE4218008418000008424A0018FF
      FF0018EFFF0018EFFF0000BDFF0000BDFF0000BDFF002194F700218CF7002994
      FF00082142000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE000031630000316300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000316300003163009CCE
      FF00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE003163
      9C0031639C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639C
      CE00639CCE00CEFFFF009CCEFF009CCEFF009CCEFF009CCEFF009CCEFF00639C
      CE00639CCE000031630000316300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5290000EFAD9C00FFF7F700FFF7F700D66B5200C6522100FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFB59400CE4218008421000000000000000000000852
      520018FFFF0018EFFF0018EFFF0000BDFF0000BDFF0000BDFF002194F700218C
      F7002994FF000821420000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003163
      9C0031639C00639CCE00639CCE00639CCE00639CCE00639CCE00639CCE000031
      6300003163000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000031
      6300003163009CCEFF00639CCE00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE0031639C0031639C0031639C00639CCE00639CCE00639CCE00CEFF
      FF009CCEFF009CCEFF009CCEFF009CCEFF009CCEFF00639CCE00639CCE000031
      6300003163000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005A180000C6421800FFFFFF00DE7B630094210000B5310800EF8C6300FFFF
      F700FFFFFF00FFB59400CE421800842100000000000000000000000000000000
      00000852520018FFFF0018EFFF0018EFFF0000BDFF0000BDFF0000BDFF002194
      F700218CF7002194FF0008294200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000031639C0031639C00639CCE00639CCE0000316300003163000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000316300003163009CCEFF00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE00639CCE0031639C00639CCE00CEFFFF009CCEFF009CCE
      FF009CCEFF009CCEFF009CCEFF00639CCE00639CCE0000316300003163000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000AD310000CE522900942100000000000052100000B5310000EF8C
      6300F7AD8400CE42180084210000000000000000000000000000000000000000
      0000000000000852520018FFFF0018EFFF0018EFFF0000BDFF0000BDFF0000BD
      FF0029A5FF0073CEFF005AADDE00101008000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000031639C0031639C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000316300003163009CCEFF00639CCE00639C
      CE00639CCE00639CCE00639CCE000031630031639C009CCEFF009CCEFF009CCE
      FF009CCEFF00639CCE00639CCE00003163000031630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004A1000006318000000000000000000000000000042080000B529
      0000BD3108008421000000000000000000000000000000000000000000000000
      00000000000000000000004A520018FFFF0018EFFF0018EFFF0000BDFF0021CE
      FF00426373007BC6EF0008294200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000316300003163009CCE
      FF00639CCE00003163000031630000000000000000000031630000316300639C
      CE00639CCE000031630000316300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000734A
      39009C5239000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000428C940010F7FF0018EFFF0021E7FF00426B
      8400395A6300317BA50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000031
      6300003163000000000000000000000000000000000000000000000000000031
      6300003163000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADFFFF0000EFFF006BD6FF0084CE
      F700317BA5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B5FFFF004AADEF004A73
      8C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6D6D60073737300737373007373730073737300737373007373
      7300C6A5AD00AD7373009C634A00292929004A4A4A0063636300737373007373
      7300737373007373730073737300737373007373730073737300737373007373
      7300737373007373730073737300ADADAD000000000000000000000000000000
      000073737300393939008C8C8C00000000000000000000000000000000000000
      0000000000000000000042424200393939004242420042424200424242004242
      4200393939004242420042424200424242000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6C6E700ADB5CE00BDC6
      D600FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000949494006B6B6B00737373007373730073737300737373007373
      7300DECECE00DEB5A500B55A63007B4A3900292929004A4A4A005A5A5A006B6B
      6B00737373007373730073737300737373007373730073737300737373007373
      7300737373007B7B7B00393939007B7B7B000000000063636300000000000000
      0000181818001010100000000000000000000000000000000000000000000000
      0000000000000000000008080800181818002929290031313100313131003131
      3100292929001818180000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDDE001029AD000829C6000829
      8C007394A500FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADADAD00F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7E7DE00F7D6D600E7BDBD00BD737B006B524A0063635A0084848400B5B5
      B500EFEFEF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007B7B7B0073737300000000004A4A4A00D6D6D600B5B5
      AD00949494009C9C94002121210000000000000000000000000039393900736B
      6B009C9C9C00C6C6C600D6D6D600DEDEDE00E7E7E700EFEFEF00EFEFEF00EFEF
      EF00E7E7E700DEDEDE00D6D6D600BDBDBD009494940063636300292929000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B5B5D6001829AD00319CF700298CFF000052
      EF0000319C00849CB500FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADADAD00F7F7F700EFEFEF00D6D6D600D6D6D600D6D6D600D6D6
      D600D6D6D600D6DECE00DEE7D600EFCEDE00AD523900CE732100736352007373
      7300B5B5B500EFEFEF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00737373007373730000000000EFEFEF00EFEFEF00FFFF
      FF00A5A5A5009C9C9C00949494000000000052525200A5A59C00ADADAD00B5B5
      B500BDBDBD00C6C6C600CECECE00D6D6D600DEDEDE00E7E7E700E7EFEF00E7E7
      E700DEDEDE00D6D6D600CECECE00C6C6C600BDBDBD00B5B5B500ADADAD009C9C
      9C0039393900000000007B7B7B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CECEE7001829A5003184F70042D6FF00109CFF00006B
      FF000042E70000187B00CED6DE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADADAD00F7F7F700D6D6D600DEDEDE00EFEFEF00F7F7F700D6D6
      D600CECECE00F7F7F700DEE7DE00C67B5200B5522100CE630000CE6B10005A4A
      3900424242006B6B6B008C8C8C00949494009494940094949400949494009494
      940094949400F7F7F7007373730073737300C6C6C600E7E7E7009C9494009494
      9400E7E7E7009C9C9C00ADADAD006B6B6B00737373009C9C9C00A5A59C00ADAD
      AD00BDBDBD00CED6D600DEDEDE00DEDEDE00DEDEDE00E7DEDE00E7D6D600E7D6
      D600E7DED600DEDEDE00DEDEDE00CECECE00BDBDBD00ADADAD00A5A5A5009C9C
      9C009C9C9C00313131007B7B7B00000000000000000000000000848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      8400848484008484840084848400848484008484840084848400848484008484
      84008484840063636B0018219C002184EF004ACEFF00088CFF00004AFF000029
      E7000042EF0000219C005A6B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADADAD00F7F7F700DEDEDE00EFEFEF00FFFFFF00FFFFFF00E7E7
      E700DEDEDE00FFFFFF00FFFFFF00DEAD7B00D66B1000D6843100CE630000CE6B
      100063524200525252007B7B7B009C9C9C00BDBDBD00BDBDBD00B5B5B5009C9C
      9C00EFEFEF00FFFFFF007373730073737300DEDEDE00CECECE00A5A5A5008C8C
      8C00ADADAD00BDBDBD00ADADAD00ADADAD0052524A00BDBDBD00DED6D600CEAD
      9C00BD8C7B00B57B6B00B5735A00BD7B6300CE947B00CE9C8C00CE9C8C00CE9C
      8400C68C7300BD735A00B5634A00B5846B00C69C9400D6CEC600D6D6D600B5B5
      B50094948C00424242007B7B7B0000000000000000008C5A42007B4A31007342
      3100734231007342310073423100734A3100734A3100734A3100734A3100734A
      3100734A3100734A3100734A3100734A3100734A3100734A3100734A3100734A
      310063392900101073002973F70042CEFF001094FF00004AFF000031EF000031
      D6000010A50000186B005A6B7B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADADAD00F7F7F700DEDEDE00C6C6C600CECECE00CECECE00C6C6
      C600BDBDBD00CECECE00CECECE00C6CEC600BD7B2100D66B0800D6843900CE63
      0000C66308006B5A4A004A4A4A0073737300C6C6C600D6D6D600BDBDBD009494
      9400FFFFFF00FFFFFF007373730073737300E7E7E700C6C6C600B5B5AD008C8C
      8C0084848400D6D6D600B5B5B500BDBDBD008C8C8C00B5846B00A5422100AD4A
      2900AD5A3100B56B4200C6846300CE9C8400DEB59C00E7BDAD00E7C6B500DEBD
      AD00D6AD9400CE947B00BD7B5A00B5633900AD522900A54A2900A5634A00E7BD
      AD00EFEFEF00424242007B7B7B0000000000A54A1800A5522900A54A2100A54A
      2100A54A2100A54A2100A54A2100A54A2100A54A2100A54A2100A54A2100A54A
      2100A54A2100A54A2100A54A2100A54A2100A54A2100A54A2100A54A21009C63
      420018187B00186BF7004ACEFF00089CFF000052FF000029EF000031D6000010
      A50000186B00394A52007B7B7B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADA59C0094736300947B73007B73
      7300847B7B006B6B6B00BDBDBD00BDBDBD00DEDEDE00F7F7F700FFFFFF00E7E7
      E700DEDEDE00FFFFFF00FFFFFF00EFEFEF00CED6CE00DE7B2100CE6B1000D684
      3100CE630000CE630000635239004A4A4A0094949400BDBDBD00BDBDBD009494
      9400FFFFFF00FFFFFF007373730073737300E7E7E700C6C6C600BDBDBD009494
      940084848400B5B5B500C6C6C600C6C6C600A59C94007B311000BD6B4200BD6B
      4A00C6735200C67B5A00CE947300DEAD9400E7BDAD00E7CEBD00EFCEBD00E7C6
      BD00DEB5A500D6A58400CE8C6B00C67B5200C6734A00BD6B4200BD5A3900AD42
      1800FFE7DE006B6B73007B7B7B0000000000B56B4200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7FFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7F7FF00424A
      B500215AE70042CEFF00189CFF000052FF000031EF000031D6000018AD000018
      6B00736B6B005A524A008484840000000000FFFFFF00CECED600B5BDC600BDBD
      C600BDBDC600B5BDC600B5BDC600B5BDC600B5B5C600B5BDC600B5BDC600B5BD
      C600B5BDC600B5B5C600B5B5C600B5BDC600ADBDC600ADB5C600ADBDC600ADB5
      C600B5BDC600B5BDC600B5BDC600ADB5C600ADB5C600A5B5C600A5B5C600A5B5
      C600A5B5C6009CB5C600ADBDC600D6D6DE00BD8C7B00E7AD9400E7A58C00D694
      7B00B57B6B008C635A007B635A006B636300847B7B00ADADAD00CECECE00C6C6
      C600D6D6D600FFFFFF00FFFFFF00EFEFEF00CECECE00FFF7F700CE631000D673
      1800D6843100CE5A0000CE6B1000635A4200525252008C8C8C009C9C9C008484
      8400F7F7F700FFFFFF007373730073737300EFEFEF00CECECE00C6C6C6009C9C
      9C00848484008C8C8C00DEDEDE00CECECE00CECECE0073392900C6734A00C67B
      5200C67B5200CE8C6300D69C8400E7B59C00EFCEBD00EFD6C600EFD6CE00EFD6
      C600E7C6AD00DEAD9400D6947300CE846300CE7B5200C67B5200C6734A00C663
      3900C6A59C00101010000000000000000000B56B4200FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7F700636BC600084A
      D6004ACEFF0010A5FF00005AFF000029EF000031D6000018AD00001073008CA5
      B500C6947B005A524A008484840000000000BDBDC6006BC6DE0052CEEF005AD6
      EF005AD6F7005AD6F7005AD6F7005AD6F7005AD6F7005ADEF7005ADEF70063E7
      F7005ADEF7005ADEF70063DEF70063E7F70063DEF70063DEF70063E7F70063DE
      F70063DEF7006BE7F7006BDEF70052D6EF004AD6EF0052CEEF004AD6EF004AD6
      F70042D6F70042D6F70052CEEF00BDC6CE00D6948400FFCEBD00FFCEAD00F7BD
      9C00E79C8400F7AD9400EFAD9400DE948400B5846B008C635A0073635A00635A
      5A006B636B0094949400C6C6C600C6C6C600BDBDBD00CECECE00DEE7CE00C66B
      0000D67B2100D6842900CE5A0000D67B18006B5A4A00525252005A5A5A005A5A
      5A00C6C6C600E7E7E7007373730073737300F7F7F700CECECE00CECECE00A5A5
      A500848484007B847B00CECECE00D6D6D600DEDEDE007B5A4A00C6734A00CE84
      5A00CE845A00CE8C6B00D69C8400E7B59C00EFCEBD00EFD6C600EFD6CE00EFD6
      C600E7C6AD00DEAD9400D6947300D68C6300CE845A00CE845A00CE734A00C652
      290052180000000000000000000000000000B56B3900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFEFF7004242A500104AD60042C6
      FF0021A5FF00005AFF000031EF000031D6000018AD00001073007B9CAD00FFFF
      FF00CE9473005A524A008484840000000000ADB5BD0063DEF70063E7FF006BE7
      FF006BEFFF0073EFFF006BCED6004A8C9C00429CB5004AA5B500529CA5005AB5
      C60052ADC6005294A5005A94A5005A9CAD005294A5007BD6E7005AADBD005A94
      9C005AA5B50052848C005AA5B50063B5C6005AADBD007BC6CE0084F7FF0084EF
      FF0084EFFF0084F7FF0073E7F700B5C6CE00D6948400FFCEB500FFC6A500D694
      8400DE9C8400DE947B00F7BD9C00D6947B00DE9C7B00FFBD9C00EFA58C00D694
      7B00BD84730084737300D6D6D600F7F7F700CECECE00FFFFFF00FFFFFF00EFEF
      DE00D66B0800D67B2100D67B2100CE630000B55A1800BD7B63007B6B5A003139
      390073737300ADADAD00636363007373730000000000D6D6D600CECECE00BDBD
      B5008484840084848400B5B5B500DEDEDE00E7E7E70094847B00A54A2100CE63
      3100CE6B3900D67B5200DE9C7300E7B59C00EFCEB500EFD6C600F7D6CE00EFD6
      C600EFC6AD00E7AD8C00D68C6B00CE7B4A00CE6B3100CE633100CE632900C64A
      21009C634A00000000000000000000000000B56B3100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7FFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00E7EFEF005A5AA5001052DE005AD6FF0018AD
      FF00005AFF000031F7000031D6000018B500001073006B8CA500F7FFFF00FFFF
      FF00CE9473005A524A008484840000000000ADB5BD00ADEFF700ADF7FF00B5F7
      FF00B5F7FF00BDF7FF00C6F7FF00738C8400738C9400739494006B7B7B006363
      63006B7B7B00737B7B007B848400737B7300737B7B00C6E7E700849494007384
      84007B949C00737B73007B9494007B94940094B5B500D6FFFF00CEF7FF00C6F7
      FF00CEFFFF00C6FFFF00BDEFF700B5BDCE00D6948400FFCEBD00FFC6A500DEAD
      9C00FFE7CE00F7C6A500CE947B00E7A58C00E79C8400E7AD8C00D6947B00D68C
      7300FFBD9C00C69C8C00A5A5A500E7E7E700C6C6C600F7F7F700FFFFFF00FFFF
      FF00F7EFE700D6731000D67B2100D6732100B5736300C6736300DE8C5A009C63
      6B005A5A5A006B6B6B004A4A4A006B6B6B0000000000DEDEDE00D6D6CE00CECE
      CE00848484008484840094949400E7E7E700E7E7E700BDB5AD008C421800CE6B
      3100CE6B3900D67B4A00DE947300E7B59400EFCEB500F7D6C600F7D6CE00EFD6
      C600EFC6AD00E7A58400D6845A00CE734200CE633100CE633100CE632900BD4A
      2100E7AD9400000000000000000000000000B56B3100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6CECE0073736B00EFEFE700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF005A639C004A8CDE0063E7FF0031BDFF00005A
      FF000031F7000031DE000021B500001073006B8CA500F7F7FF00FFFFFF00FFFF
      FF00CE9473005A524A008484840000000000B5B5BD00DEEFEF00E7FFFF00EFF7
      FF00F7FFFF00F7FFFF00F7FFFF00DEE7E700E7E7E700EFF7F700EFEFF700E7EF
      EF00E7E7E700EFEFEF00EFEFEF00EFEFEF00EFF7EF00F7F7F700F7F7F700EFEF
      EF00EFF7F700E7E7E700EFF7F700E7EFEF00EFEFEF00EFEFEF00FFFFFF00F7FF
      FF00E7E7E700EFF7F700EFF7F700BDBDC600DE9C8400FFD6BD00FFCEAD00F7BD
      9C00E7CEB500E7C6A500E7AD9400EFD6BD00FFE7C600D69C8400E7AD9C00F7BD
      9C00EFA58C00CE9C8C0094949400CECECE00BDBDBD00D6D6D600FFFFFF00FFFF
      FF00FFFFFF00BDA58400CE734A00EFDEE700DEB5BD00C65A0000C65A0000C663
      6300947B84004A525200313131005252520000000000EFEFEF00D6D6D600DEDE
      DE00848484008484840084848400E7E7E700E7E7E700DEDEDE0073311000CE63
      3100CE6B3900D67B4A00DE947300E7B59400EFCEB500F7D6C600F7D6CE00EFD6
      C600EFC6AD00E7A58C00D6846300CE734200CE633100CE633100CE632900BD4A
      2100DEA58C00000000000000000000000000BD6B3100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00D6DEDE00B5ADA500524242009CB5B500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00527BDE0073D6FF0063EFFF00004AEF000031
      F7000031DE000021BD0000107B00527B9400EFF7F700FFFFFF00FFFFFF00FFFF
      FF00CE946B005A524A008484840000000000B5B5C600EFEFEF00FFFFFF00D6D6
      CE009C948C00F7F7F700FFFFFF00FFFFFF00FFFFFF00E7E7E7009C8C8C00DEDE
      DE00FFFFFF00A59494008C736B00EFE7E700DEDED6007B635A009C8C8400FFF7
      F700C6B5B500846B6300BDB5AD00FFF7F7008C7B7300735A5200D6CECE00FFFF
      FF008C847B00E7DEDE00FFFFFF00BDBDC600DE9C8C00FFD6BD00FFCEAD00EFB5
      9400C68C7300E7AD8C00FFCEAD00DEB59C00DEBDA500EFB59C00E7BDAD00F7E7
      C600EFB59C00CEA58C00ADADAD00F7F7F700CECECE00FFFFFF00FFFFFF00FFFF
      FF00D6D6D600FFFFFF00EFCED600CE7B4200E7944A00E7944200D6630000A563
      21006B5A6300947B7B00292929003939390000000000F7F7F700D6D6D600DEDE
      DE0094949400848484007B847B00D6D6D600DEDEDE00DEE7E70073422900CE63
      3100CE6B3900D67B4A00DE947300E7B59400EFCEB500F7D6C600F7D6CE00EFD6
      C600EFC6AD00E7A58C00D6846300CE734200CE633100CE633100CE632900BD4A
      2100DEA58C00000000000000000000000000BD6B3100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00E7EFEF00A5A5A500FFF7D600736B6B00ADBDB500FFFF
      FF00FFFFFF00FFFFFF00F7FFFF00847BAD007363A5004ACEEF00087BE7000029
      D6000021BD0000087B006384A500E7EFF700FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00D69C6B005A524A008484840000000000B5B5C600E7EFEF00FFFFFF00A59C
      9400100000009C949400FFFFFF00FFFFFF00FFFFFF009C948C00ADA5A5009C8C
      8400CECEC6008C7B7300A59494009C8C840094848400ADA59C008C7B7300C6B5
      B5008C7B7300CEC6BD0084736B00BDB5B50094847B00B5ADA50094848400FFFF
      FF00ADA5A500EFEFE700F7F7F700BDBDC600E79C8C00FFD6C600FFCEAD00D69C
      8400EFA58C00DE9C8400EFB59C00C68C7300CE847300FFC6A500EFB59400D69C
      8400FFC6AD00CEA58C009C949C00D6D6D600C6C6C600DEDEDE00DEDEDE00DEDE
      DE00BDBDBD00DEDEDE00DEDEDE00DEC69C00C6630000E79C4A00DE8C3900CE63
      00007B4A18007B6B6B007B7373002929290000000000FFFFFF00D6D6D600D6D6
      D600ADADAD008484840084848400CECECE00D6D6D600D6D6DE007B524200C663
      3100CE6B3900D67B4A00DE947300E7B59400EFCEB500F7D6C600F7D6CE00EFD6
      C600EFC6AD00E7A58C00D6846300CE734200CE633100CE633100CE632900BD4A
      2100DEA58C00000000000000000000000000C6733100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7E7E700BDBDC600F7F7EF00E7CEB500634A42008C635200E7EF
      EF00F7FFFF00EFFFFF009C8C9C00B5847300E7AD9400636B9400188CFF000029
      CE0000088400426B8C00EFF7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00D69C6B005A524A008484840000000000B5B5BD00EFEFEF00EFEFEF00847B
      7300524239007B6B6300FFFFFF00FFFFFF00FFFFFF0094848400EFEFEF009C8C
      8400C6BDB500A59C9400DED6D60094847B0094847B00D6CECE00AD9C9C00B5AD
      A50094848400EFEFEF008C847B00B5ADAD00AD9C9C00CEC6C60094848400FFFF
      FF00D6D6D600FFFFFF00F7F7F700BDBDC600E7A58C00FFD6C600FFC6AD00E7B5
      A500FFEFCE00F7CEAD00CE9C8400EFB5A500F7B59C00DEA58C00CE947B00D68C
      7300EFB59400CEA58C00A59C9C00DEDEDE00C6C6C600E7E7E700E7E7E700E7E7
      E700C6C6C600E7E7E700E7E7E700E7E7E700BDBDB500D66B1000E7944A00E78C
      3100D66B00006B4A3100845A5A00524239000000000000000000D6D6D600CECE
      CE00C6C6C6008484840084848400BDBDBD00CECECE00CECECE0084635200BD5A
      2900CE6B3900D67B4A00DE947300E7B59400EFCEB500F7D6C600F7D6CE00EFD6
      C600EFC6AD00E7A58C00D6846300CE734200CE633100CE633100CE632900BD4A
      2100DEA58C00000000000000000000000000C6732900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CECECE00FFFFEF00DEDEC600B5948400BDA59C007B4242006363
      63006B7373006B525200C6948400FFF7C600F7CEBD005221420029428400294A
      94005A739C00DEEFEF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00D69C6B005A524A008484840000000000B5B5BD00EFEFEF00E7E7DE00847B
      7300BDB5B500948C840094847B00E7E7E700FFFFFF009C8C8C00EFEFEF00AD9C
      9C00C6BDBD009C8C8400D6D6CE0094847B0094848400CEC6BD00A59C9400B5AD
      A50094848400E7E7E70094847B00B5ADAD00A59C9400C6BDBD009C8C8400FFFF
      FF00A59C9400E7E7E700F7F7F700BDBDC600E7A59400FFDEC600FFCEAD00F7BD
      A500E7C6AD00E7C6A500EFB59C00EFCEBD00FFE7CE00D6A58C00E7BDA500FFD6
      BD00EFB59C00CEA58C00ADADAD00EFEFEF00CECECE00FFFFFF00FFFFFF00FFFF
      FF00CECECE00FFFFFF00FFFFFF00FFFFFF00CECECE00F7EFDE00D67B2100EF94
      4200DE7B2100C6630800BD7B6B00BDA5A5000000000000000000E7E7E700C6C6
      C600CECECE008C8C8C0084848400BDBDBD00C6C6C600C6C6C600846B6300BD5A
      2900CE6B3900D67B4A00DE947300E7B59400EFCEB500F7D6C600F7D6CE00EFD6
      C600EFC6AD00E7A58C00D6846300CE734200CE633100CE633100CE632900BD4A
      2100DEA58C00000000000000000000000000C6732900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7E7E700CECEBD009C8C8400E7CEC600FFFFEF009C847B00845A
      5A005A424A00B5847300FFFFCE00F7E7BD0084635A0042393900C6CECE00DEEF
      EF00F7FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00D69C6B005A524A008484840000000000BDBDC600EFEFEF00E7E7E700A59C
      9400D6D6CE0039211800736B6300D6D6D600FFFFFF00DEDEDE00CEC6C600EFEF
      EF00D6D6CE0094847B00DED6D60094847B009C8C8400CEBDBD00AD9C9C00B5AD
      A50094848400E7E7E70094847B00B5ADAD00AD9C9400C6BDBD009C8C8400FFFF
      FF00A59C9C00E7E7DE00F7FFF700B5B5C600E7A59400FFDECE00FFD6B500E7B5
      9400BD846B00E7AD8C00FFD6B500DEB59400D6AD9400FFC6AD00EFC6A500F7D6
      B500F7C6A500CEA58C00948C8C00CECECE00BDBDBD00D6D6D600CECECE00CECE
      CE00BDBDBD00D6D6D600CECECE00D6D6D600BDBDBD00CECECE00D6CEB500C66B
      1000E7944200D66B0000D67B390084737B000000000000000000E7E7E700CECE
      CE00C6C6C600A5A5A50084848400BDBDBD00B5BDB500BDBDBD00846B6300BD5A
      2900CE6B3900D67B4A00DE947300E7B59400EFCEB500F7D6C600F7D6CE00EFD6
      C600EFC6AD00E7A58C00D6846300CE734200CE633100CE633100CE632900BD4A
      2100DEA58C00000000000000000000000000CE732900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00F7FFFF00C6C6C600A5A5A500C6ADB500C6BDB500B59C9400E7BD
      A500ADA5A500F7DEBD00F7E7C60084635A0029292900CECECE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00DE9C6B005A524A008484840000000000B5B5C600DEDEDE00F7F7F700B5AD
      AD009C948C005A4A42008C848400EFEFEF00FFFFFF009C948C008C7B73009C94
      8C00D6CECE00947B7B00DED6D600947B7B009C8C8C00CEC6BD00ADA59C00B5A5
      A5009C8C8400EFEFE70094848400B5A5A500ADA59C00C6BDBD009C8C8400FFFF
      FF00ADA59C00E7E7E700F7F7F700B5B5C600EFAD9400FFDED600FFCEAD00D69C
      8C00F7B59400E7A58C00E7B59C00C68C7B00D68C7300FFC6A500C6ADA5008C8C
      9400F7D6B500CEAD9400A5A5A500E7E7E700CECECE00F7F7F700F7F7F700F7F7
      F700C6C6C600F7F7F700F7F7F700F7F7F700C6C6C600EFEFEF00F7F7F700D6D6
      CE00CE8C3100DE8C2900E77B29005A525200000000000000000000000000DEDE
      DE00B5B5B500B5B5B5007B847B00C6C6C600ADADAD00ADB5AD0084635A00C65A
      2900CE6B3900D67B4A00DE947300E7B59400EFCEB500F7D6C600F7D6CE00EFD6
      C600EFC6AD00E7A58C00D6846300CE734200CE633100CE633100CE632900BD4A
      2100DEA58C00000000000000000000000000CE732900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00F7FFFF00A5A5A500CEC6BD00F7EFDE00EFBD
      A500DEB59C00D6B5AD009C8484001818180073737300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00DE9C6B005A524A008484840000000000BDBDC600CEC6C600FFFFFF008C84
      7B00ADADA500BDBDB5009C948C00F7F7F700FFFFFF00948C8400948484008473
      6B00D6D6CE008C7B7300E7E7E7008C7B73009C8C8400C6BDBD00ADA59C00ADA5
      9C008C7B7B00E7DEDE008C7B7300B5ADA5009C8C8400AD9C9C009C948C00FFFF
      FF00A59C9C00EFEFEF00F7F7F700B5B5C600EFAD9C00FFDED600FFCEAD00E7BD
      AD00FFEFD600F7D6B500CE9C8C00F7C6AD00FFCEAD00D6AD94005A84CE004A7B
      F700A5ADBD00D6AD9400A5A5A500EFEFEF00CECECE00FFFFFF00FFFFFF00FFFF
      FF00CECECE00FFFFFF00FFFFFF00FFFFFF00CECECE00FFFFFF00FFFFFF00FFFF
      FF00D6D6C600D6B56B00AD7B6B006B6B6B00000000000000000000000000DEDE
      DE00C6C6C600ADADAD008C8C8C00D6D6D600A5A5A500A5A5A500845A4A00CE63
      3100CE6B3900D67B4A00DE947300E7B59C00EFCEB500F7D6C600F7DECE00F7D6
      C600EFC6AD00E7A58C00D68C6300D6734200CE633100CE633100CE632900BD4A
      2100DEA58C00000000000000000000000000CE7B2100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CED6D600E7DECE00FFFFFF00FFEF
      DE00E7BDA500E7AD9C00947B7B004218180073737300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00DEA563005A524A008484840000000000C6C6CE00BDBDBD00FFFFFF00ADAD
      A500ADA5A500948C8400A59C9400FFFFFF00FFFFFF009C8C84006B5A52007363
      5A00EFEFEF008C7B6B007B635A00A5948C00CEC6C600735A52007B635A00DEDE
      D600A5948C007B635A00A59C9400EFEFE7007B6B63007B6B6300E7DEDE00F7F7
      FF009C949400EFEFEF00F7F7F700BDBDC600EFB59C00FFE7D600FFD6B500EFC6
      A500E7C6B500E7C6A500EFC6A500E7CEB500F7DEC600D6B59C007BADD60084B5
      FF0094ADCE00CEAD94008C8C8C00C6C6C600BDBDBD00CECECE00CECECE00CECE
      CE00BDBDBD00CECECE00CECECE00CECECE00BDBDBD00C6C6C600CECECE00CECE
      CE00BDBDBD00F7F7F70073737300737373000000000000000000000000000000
      0000E7E7E700B5B5B500A5A5A500EFEFEF00949494009C9C9C009C523100CE63
      3100CE6B3100C66B4200C6845A00C6947B00C6A59400C6AD9C00C6AD9C00C6A5
      9C00C69C8C00BD8C6B00BD735200C6633900CE633100CE633100CE632900BD4A
      2100DEA59400000000000000000000000000D67B2100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7FFFF00CECECE00E7E7C600FFFF
      FF00FFF7DE00EFBDA500CE9C94006331390029212100ADB5B500C6C6C600EFEF
      EF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00DEA563005A524A008484840000000000D6D6DE00B5B5BD00F7F7F700EFEF
      EF00F7F7F700EFEFEF00EFE7E700FFFFFF00FFFFFF00D6CEC600AD9C9400CEC6
      C600FFFFFF00E7E7DE00B5AD9C00F7F7EF00FFFFFF00D6D6C600DEDED600FFFF
      FF00F7F7F700DED6CE00FFF7F700FFFFF700F7F7EF00F7F7EF00FFFFFF00FFF7
      F700F7EFEF00FFF7F700EFEFEF00B5BDC600EFB5A500FFE7DE00FFD6B500FFD6
      B500F7CEAD00FFCEB500FFDEBD00F7C6AD00EFBDA500FFD6B500BDC6BD007BAD
      CE00C6C6C600D6B59C00A5A5A500EFEFEF00CECECE00FFFFFF00FFFFFF00FFFF
      FF00CECECE00FFFFFF00FFFFFF00FFFFFF00CECECE00FFFFFF00FFFFFF00FFFF
      FF00D6D6D600F7F7F70073737300737373000000000000000000000000000000
      0000E7E7E700F7F7F700FFFFFF00D6D6D6009494940094847B009C4218008442
      2100845242008C7363009C8C8400A59C9C00B5ADAD00BDBDBD00C6BDBD00BDBD
      B500B5ADAD00A59C940094847B0084635A00734A31007B3918009C421800B542
      1800D69C8400000000000000000000000000D67B2100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CECECE00E7E7
      C600FFFFF700FFF7DE00E7BDAD00948C84005A3131000800000039393900424A
      4A00DEE7E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7A563005A524A008484840000000000E7E7EF00ADADAD00F7E7CE00F7E7
      CE00F7E7CE00F7E7CE00F7E7CE00FFE7CE00FFEFD600FFF7DE00FFFFEF00FFF7
      DE00FFEFD600FFF7DE00FFFFE700FFF7DE00FFEFD600FFF7DE00FFF7DE00FFEF
      DE00FFEFD600FFF7DE00FFEFD600FFEFD600FFE7BD00F7DEB500FFDEAD00F7D6
      AD00F7D6AD00F7D6AD00E7D6BD00BDBDCE00F7B5A500FFE7DE00FFD6B500EFBD
      9C00E7B59400E7B59C00E7B59C00EFBD9C00F7C6AD00F7CEAD00F7CEAD00F7CE
      B500FFE7C600CEB59C00A5A5A500E7E7E700CECECE00FFFFFF00F7F7F700F7F7
      F700C6C6C600F7F7F700F7F7F700FFFFFF00C6C6C600EFEFEF00F7F7F700FFFF
      FF00CECECE00F7F7F70073737300737373000000000000000000000000000000
      000000000000DEDEDE00F7F7F7009CA5A5004A21100084524200948C8400ADAD
      AD00B5BDBD00C6C6C600CECECE00D6D6D600DEDEDE00E7E7E700E7E7E700E7E7
      E700DEDEDE00D6D6D600CECECE00C6C6C600BDBDBD00ADADAD00847B73006B42
      310021000000000000000000000000000000D67B2100FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7FFFF00CECE
      CE00E7DEC600FFFFF700EFE7DE00C6C6BD0094949400735A5A00848484006B6B
      6B00A5ADAD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7A563005A524A008484840000000000F7F7F700ADA5A500EFB56B00E7B5
      6300EFB56B00EFBD6B00E7B56B00CE9C5A00F7CE9400F7DEBD00F7D6AD00F7D6
      AD00F7D6AD00F7D6AD00F7D6AD00F7D6A500F7D69C00F7CE9400F7CE9C00F7CE
      9400F7CE9400EFD6A500EFC68C00EFC67300DEA54A00D69C4200EFB54A00EFAD
      4A00EFAD4A00EFAD4200D6BD9400CED6DE00F7BDAD00FFEFE700F7CEAD00E79C
      7B00FFB58C00FFB58C00F7BD9400F7B59400EFB58C00EFB59400EFB59400E7B5
      9400F7CEB500D6B5A5008C848C00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00FFFFFF0073737300737373000000000000000000000000000000
      000000000000000000000000000084848400737373009C9C9C00A5A59C00ADA5
      A500B5B5B500BDBDBD00C6C6C600C6CECE00CED6D600D6D6D600D6D6D600CED6
      D600CED6D600CECECE00C6C6C600BDBDBD00B5B5B500ADADAD00A5A5A5009C9C
      9C0052525200292929000000000000000000DE842900FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CECECE00DED6C600EFEFEF00D6C6CE00D6CECE00CEC6C6008C8C8C008C8C
      8C00F7F7F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7AD63005A524A00848484000000000000000000B5B5BD00C69C5A00D6A5
      5200D6A55200DEAD5200DEAD5200CE9C5200E7B56B00E7BD7B00E7BD7B00E7BD
      7B00E7BD7B00EFBD7300EFC67B00EFC67B00E7BD7B00EFC67B00E7BD7B00E7BD
      7300E7BD7B00DEB57B00D6B57B00CEAD7B00C6AD8400BDA58400BDAD8C00BDAD
      9400BDAD9400B5A59C00C6C6CE0000000000F7BDAD00FFEFE700F7CEAD00E794
      6300FF9C6300FFA56B00FFA56B00FFA57300FFAD7300FFAD7B00FFB58400FFAD
      8400EFAD8C00D6AD9C00A5A5A500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00737373007B7B7B000000000000000000000000000000
      00000000000000000000000000009C9C9C008C8C8C0094949400B5B5B500C6CE
      CE00BDBDBD00C6ADA500C69C8400C68C7300C6846300C67B5A00BD7B5200C684
      6300C6846B00C6947B00C69C8C00C6B5AD00C6C6C600C6CECE00ADADAD009494
      940084848400313131000000000000000000D67B1800DEDECE00DED6BD00DED6
      BD00DED6BD00DED6BD00DED6BD00DED6BD00DED6BD00DED6BD00DED6BD00DED6
      BD00D6CEBD00C6C6B500BDB5AD00B58C8400BD7B7300BD7B7300AD847300C6BD
      AD00DECEB500D6CEB500DED6B500DECEB500DECEB500DED6B500E7D6AD00DEDE
      C600D68C31005A5A5A0084848400000000000000000000000000DEDEDE00BDB5
      C600C6BDCE00BDBDCE00C6C6CE00C6C6CE00C6BDCE00C6BDC600C6BDC600C6BD
      C600BDBDC600BDBDCE00BDBDC600BDBDC600BDBDCE00BDBDCE00BDBDCE00C6C6
      CE00C6C6D600CECED600CECEDE00D6D6E700EFEFF700EFEFF700EFEFF700EFEF
      F700EFF7F700FFFFFF000000000000000000F7BDAD00FFEFE700FFDEBD00DE9C
      7300EF844A00FF844A00FF844A00FF8C4A00FF8C5200FF945200FF945A00FF94
      5A00EF9C6B00CEAD9C00A5A5A500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7E7E700ADADAD00ADADAD00ADAD
      AD00B5B5B500C6C6C6006B6B6B00C6C6C6000000000000000000000000000000
      000000000000000000000000000094949400A5A5A500EFEFEF00A59C94008C52
      3900B5522100C65A2100CE632900CE632900CE632900CE633100D6633100CE63
      3100CE632900CE632900CE632900C65A2100B552290094634A00C6BDBD00E7E7
      E70084848400313131000000000000000000B55A0000AD4A0000A54200009C42
      00009C4200009C4200009C4200009C4200009C4200009C4200009C4200009C42
      00009C420000A54200009C4200009C4200009C4200009C4200009C4200009C42
      0000A54A0000AD5200009C420000A54A0000A54A0000A54200008C4A0800A54A
      0000BD5A00006363630084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7C6B500FFEFE700FFDEBD00FFD6
      B500EFB59400E7A57B00E79C7300E7947300E7946B00EF8C5A00EF845200EF7B
      4A00EFAD8C00DEBDAD00A59C9C00FFFFFF00E7E7E700CECECE00CECECE00CECE
      CE00D6D6D600CECECE00D6D6D600FFFFFF009C9C9C00B5B5B500C6C6C600BDBD
      BD00A5A5A500848484004A4A4A00EFEFEF000000000000000000000000000000
      00000000000000000000000000009C9C9C00FFFFFF00C6C6C60094949400847B
      730073635A006B5242006B4229006B3921007339180073311000733110007331
      18007339210073422900734A31007B5A4A0084736B00948C8C009C9C9C00E7E7
      E700E7DEDE00292929000000000000000000BD630000CE6B0000D66B0000D66B
      0000D66B0000D66B0000D66B0000D66B0000D6730000D6730000D6730000D673
      0000D66B0000D66B0000CE6B0000D66B0000D66B0000D66B0000CE6B0000CE6B
      0000E7944200F7AD6300CE6B0000EFA55200F7A542009C633900296BFF007B73
      8400CE6B00007373730084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFE7DE00FFFFFF00FFF7DE00FFF7
      D600FFF7D600FFEFCE00FFEFCE00FFEFCE00FFE7C600F7DEBD00F7D6B500EFCE
      B500FFE7CE00E7C6B500A59C9C00FFFFFF00F7F7F700EFEFEF00EFEFEF00EFEF
      EF00F7F7F700EFEFEF00EFEFEF00FFFFFF00A5A5A500F7F7F700FFFFFF00E7E7
      E700ADADAD0052525200EFEFEF00000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700BDBDBD00EFEFEF00ADADAD00ADAD
      A500B5B5B500BDBDBD00CECECE00CECED600D6D6D600D6D6D600D6D6D600D6D6
      D600D6DEDE00D6D6D600CECECE00C6C6C600BDBDBD00ADADAD00BDBDBD00F7F7
      F7006B6B6B00000000000000000000000000D6730800DE8C2900EFA55200F79C
      3900F79C3900F79C3900F79C3100F79C3100F79C3900EF942900EF942900F79C
      3900F79C3100EF9C3100F79C3100EF942900EF942100F7942900EF942100EF8C
      2100F7942900F79C3100EF942100F79C3100F79C3100EF943100C68C5A00D68C
      3900DE7B0000ADA59C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D6BDAD00EFE7CE00FFEFCE00FFF7
      CE00FFFFD600FFFFDE00FFFFDE00FFF7CE00FFF7CE00FFF7D600FFFFD600FFFF
      D600FFFFD600E7CEB500B5B5B500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A5A5A500F7F7F700DEDEDE009C9C
      9C0052525200F7F7F70000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6D6D600D6D6D600C6C6
      C600BDBDBD00BDBDBD00BDBDBD00BDBDC600C6C6C600CECECE00D6D6D600CECE
      CE00CECECE00C6C6C600BDBDBD00BDBDBD00C6C6C600C6C6C600A5A5A5008C8C
      8C000000000000000000000000000000000000000000D67B0800DE8C2900DE84
      2100DE841800DE841800DE841000DE841000DE841800DE841000DE841000DE84
      1800DE841800DE841800DE841800DE841800DE841000DE841800DE841000DE84
      1000DE7B0800DE7B0800DE841000DE7B0800DE7B0800E7840800EF840800E77B
      0000E7AD73000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7F7F700EFE7E700D6D6D600C6BD
      BD00BDB5AD008C847B00B5A59C00D6C6BD00E7CEC600E7CEB500E7C6AD00E7C6
      AD00E7CEB500BDB5AD00EFEFEF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A5A5A500CECECE00A5A5A5004A4A
      4A00EFEFEF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7EFE7000000000000000000EFEF
      EF00D6D6D600CECECE00C6C6C600CECECE00CECECE00C6C6C600C6C6C600C6C6
      C600CECECE00CECECE00BDBDBD00CECECE00C6C6C60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADADAD00EFF7EF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A5A5A5009494940052525200F7F7
      F700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADADAD009C9C9C00A5A5A500A5A5A500A5A5A500A5A5A500A5A5
      A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5A500A5A5
      A500A5A5A500A5A5A500A5A5A500A5A5A500848484004A4A4A00EFEFEF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004A6373001839520021395200213152000008390000104A000021
      6B0000397B00004A8400004A840000397B000031730000297300002173000029
      730000297B00003984000039840018528C006B8CAD00F7F7F700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00D6E7FF00B5CEF7008CB5EF006B9CEF006B9CEF008CB5EF00B5CEF700D6E7
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00F7F7F700E7E7E700DEDEDE00D6D6D600D6D6D600D6D6
      D600D6D6D600DEDEDE00DEDEDE00E7E7E700EFEFEF00F7F7F700FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000001829000021390000103900000839000010520000216B000031
      8C00004A9C00004A9C000039940000398C0000398C0000399400003994000042
      A5000052AD00005AAD000063AD00005AA5000042940000216B0042528400C6C6
      CE00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6E7FF0084ADEF00296B
      DE000052D6000042D600004AD6000852DE000852DE00004AD600004AD600004A
      D600296BDE007BA5EF00D6E7FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00F7F7F700DEDEDE00BDBDBD009C9C9C008C8C8C00848484007B7B7B007B7B
      7B008484840084848400949494009C9C9C00ADADAD00C6C6C600DEDEDE00EFEF
      EF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000102900002942000021420000184A0000185A0000217B0000399C000052
      AD000052B5000042A5000042A500004AA500005AB5000063BD00006BC600007B
      CE000084CE00008CCE000084CE000063BD000042AD0000217B0000084A000000
      1000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EFF7FF006394E7000052D600004AD600105A
      DE003184E7004294EF005AA5EF0063B5EF0063B5F70052A5EF004294EF003184
      E700105ADE00004AD6000052D6006394E700EFEFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00F7F7F700E7E7E700E7E7E700EFEFEF00EFEF
      EF00C6C6C600949494007B635A008452420094523900A5524200AD5A4200AD5A
      4200A55242009C524200944A3900844A39007B5242007B635A008C848400BDBD
      BD00E7E7E700FFFFFF0000000000000000000000000000000000000000000000
      0000000000005A5A520031313100313131003131310031313100313131003131
      3100313131003131310031313100313131003131310031313100313131003131
      31003131310031313100313131003131310031313100313131004A4A4A004A42
      4200313131004242420000000000000000000000000000000000000000000000
      0000002131000029420000214A0000216300003184000052AD00006BC6000073
      C6000063BD00005AB500006BBD00007BCE000094D60000A5DE0000ADDE0000B5
      E70000B5E70000ADE7000094DE00006BCE000039A50000187300000842000000
      0800000000000000000000000000000000000000000000000000000000000000
      00000000000000000000ADCEF7001863DE00004AD600186BDE004A9CEF0073C6
      F7007BCEF7007BD6F70084D6FF0084D6F70084D6FF0084D6F7007BD6F7007BCE
      F70073C6F70052A5EF001863DE00004AD6001863DE00ADC6F700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00EFEFEF00C6C6C600A5A5A500A5A5A500B5B5B500ADAD
      AD008C7B7B008C524200B5634A00BD6B5200C67B6300C67B6B00CE846B00CE84
      6B00CE846B00C67B6300C6736300BD735A00B5634A00AD5242008C4229007B52
      4200BDBDB500F7F7F70000000000000000000000000000000000000000000000
      0000000000000010290008183900082142000821420008214200082142000821
      4200082142000821420008214200082142000821420008214200082142000821
      4200082142000821420008214200082142000821390000102900000000000818
      2900082139000000000000000000000000000000000000000000000000000831
      4200002139000021420000215A0000398C00006BB500008CCE0000A5DE000094
      D6000084CE00008CD600009CDE0000B5E70000C6EF0000D6EF0000D6EF0000D6
      EF0000D6EF0000BDEF000094E7000063CE000029940000106300001042000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000094B5EF000052D600004AD6004294EF0073CEF7007BD6FF007BCE
      F70073C6F7007BCEF7006BBDF7005AADEF0063B5F70073C6F70073CEF70073C6
      F7007BCEF7007BD6F7007BCEF7004294EF000052D6000052D6008CADEF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00D6D6D6008C736B00844A3900844A39007B5A52007B6B
      6300A5634A00BD735A00CE846B00D68C7B00D6948400DE9C8400DE9C8C00DE9C
      8C00DE9C8C00DE9C8400D6948400D6947B00CE846B00C6736300B5634A00A54A
      31007B392900E7E7E700FFFFFF00000000000000000000000000081021001831
      5A00102952001839630018426B0018426B0018395A0018395A0018395A001839
      5A0018395A0018395A0018395A0018395A0018395A0018395A0018395A001839
      5A0018395A0018395A0018395A001839630018426B0018396300102952001829
      420029527B001839630000000000000000000000000000000000000000000008
      21000018390000184A0000317B00006BAD00009CCE0000BDDE0000BDE70000AD
      E70000ADE70000B5E70000C6EF0000D6EF0000E7F70010E7F70010EFF70010E7
      F70000DEF70000C6F7000094E700005ABD0000318C0000216B0000294A000008
      1000000000000000000000000000000000000000000000000000000000000000
      000094B5EF00004AD600105ADE005AADEF007BCEF70073CEF70073C6F70073C6
      F7007BCEF70084D6F7002973E7000042D600004AD6004294EF0084D6F7007BC6
      F70073CEF70073C6F7007BCEF7007BCEF7005AADEF00105ADE00004AD6008CAD
      EF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00CEBDBD009C4A3100A54A3900B55A4200BD6B5200BD73
      5A00CE7B6B00D68C7300D69C8400DEA58C00E7AD9400E7AD9400E7AD9C00E7AD
      9C00E7AD9C00E7AD9400E7AD9400DEA58C00DE9C8400D68C7B00C67B6300B563
      4A0073311800B5B5B500DEDEDE00F7F7F7000000000008214A00295284002142
      6B0010315A00214A730021426B0010294A00082142001031520010315A001031
      5A0010315A0010315A0010315A0010315A0010315A0010315A0010315A001031
      5A0010315A0010315A0010294A000821420010314A00214A7300183963000010
      21000821390029527B0000081000393939000000000000000000000000000000
      18000010390000215A00004A8C00008CBD0000B5D60000C6DE0000C6E70000BD
      E70000C6EF0000D6EF0000DEF70008EFF70021EFF70031F7F70029EFF70010EF
      F70000DEF70000C6EF000094DE00005AB5000042940000397300003952000008
      100000000000000000000000000000000000000000000000000000000000B5CE
      F7000052DE00105ADE0063B5F7007BD6F70073C6F70073C6F70073C6F7007BCE
      F70084D6F7006BB5F7000852DE000052DE000042D600216BDE008CDEFF007BCE
      F7007BCEF7007BCEF70073C6F70073C6F7007BD6F7006BBDF7001063DE000052
      DE00ADC6F7000000000000000000000000000000000000000000000000000000
      0000FFFFFF00F7F7F700C6BDBD009C523900B5635200C6736300CE846B00D68C
      7300D6947B00DE9C8400DEA58C00E7AD9400E7AD9C00E7AD9C00E7AD9C00E7AD
      9C00E7AD9C00E7AD9C00E7AD9C00E7AD9400E7A59400DE9C8400D68C7300C673
      5A006B392900736B63009C9C9400E7E7DE00949CAD0021427300214263000818
      310018396300214A73001031520021426B00315A840018315200214A7300214A
      7300214A7300214A7300214A7300214A7300214A7300214A7300214A7300214A
      7300214A730021426B00315A8400183152001031520018395A0018426B000010
      210029313900214A730008182900393939000000000000000000000000000000
      10000010390000216300005A94000094BD0000BDD60000CEE70000C6E70000CE
      EF0000D6EF0000E7F70018EFF70039F7F70052F7F7004AF7F70031F7F70008EF
      F70000DEF70000BDEF00008CCE00005AAD00004A8C00004A730000394A000000
      0000000000000000000000000000000000000000000000000000EFF7FF001863
      DE00004AD6005AADEF007BCEF70073C6F70073C6F7007BCEF7007BCEF7007BCE
      F7008CDEFF006BB5F700004AD6000852DE00004AD6001863DE008CDEFF0084CE
      F7007BCEF7007BCEF7007BCEF70073C6F70073C6F7007BCEF7005AADEF000052
      D600185ADE00EFF7FF000000000000000000FFFFFF00F7F7F700FFFFFF00EFEF
      EF00D6D6D600C6C6C600A5A5A500A5635200C67B6300CE846B00D6947B00DE94
      7B00DE9C8400DE9C8400E7A58C00E7A59400E7A59400E7A59400E7A59400E7A5
      9400E7A59400E7AD9400E7A59400E7A59400E7A58C00DE9C8400D6947B00CE7B
      6300B5634A009C4A31008C392100B5947B00949CB500214A7300081021000810
      210018426B00214A6B0018395A0021426B0039638C0021426B0018396300214A
      7300183963001039630010315A0010315A0010315A001039630018396300214A
      7300214A730031527B00315A8C00183152001839630018395A0021426B000810
      210000102100294A7B0000102100636363000000000000000000000000000000
      1000000839000029630000639C00009CBD0000BDD60000C6E70000CEE70000D6
      EF0000E7F70018EFF70042F7F70063F7F70063F7FF004AF7F70018EFF70000E7
      F70000CEEF0000A5DE000073BD000052A50000528400004A6B00002129003939
      3900000000000000000000000000000000000000000000000000739CEF00004A
      D6004294EF007BCEF70073C6F70073C6F7007BCEF7007BCEF7007BCEF70084CE
      F7008CDEFF004A94EF00004AD600085ADE000052D600105ADE0084CEF7008CD6
      F70084D6F70084CEF7007BCEF7007BCEF7007BC6F70073C6F7007BCEF7004294
      EF00004AD6006394E7000000000000000000D6D6D600C6C6C600C6C6C600B5B5
      B5008C8C8C0073635A007B635A00A56B5200CE7B6300D68C7300D6947B00DE94
      7B00DE9C8400DE9C8400DE9C8400DE9C8400DE9C8400DE9C8400DE9C8400DE9C
      8400DE9C8400DE9C8400DE9C8400DE9C8400DE9C8400DE9C8400D6947300CE84
      6B00BD6B5200AD5239009439210094522900949CB500214A7300081018000810
      210018426B00214A73001839630021426B00315A8C00294A6B0018314A001842
      6B0018426B00A5B5BD00BDCED600BDC6D600BDCED600ADB5BD0018426B001842
      6B0021426B0029527B0031527B0010294A001839630018395A0021426B000810
      210008182900294A7B0000000800000000000000000000000000000000000000
      10000008310000296300005A94000094BD0000B5D60000BDDE0000CEE70000DE
      EF0010EFF70039F7F7005AF7F7006BF7FF0052F7F70029F7F70000E7F70000D6
      EF0000B5E700008CCE00005AAD00004A8C00004A730000314A00000000000000
      00000000000000000000000000000000000000000000DEEFFF000852DE001863
      DE0073CEF70073CEF70073C6F7007BC6F7007BCEF7007BCEF70084CEF7008CD6
      F7008CDEFF00216BDE00004AD600085ADE000052DE00105ADE0084CEF7008CDE
      FF0084D6F70084D6F70084CEF7007BCEF7007BCEF70073C6F7007BCEF7007BCE
      F700186BDE000052D600D6E7FF000000000094847B00735A5200736B63007363
      5A008C422900A54A3100B5634200C6735200CE7B6300D68C6B00D68C7300DE94
      7300D6947300DE947300DE947300DE947300DE947300DE947300DE947300DE94
      7300DE947300DE947300DE947B00DE947300DE947B00DE947300D68C7300CE84
      6300BD6B5200AD52390094392100A57B5A0094A5B50021427300081829000810
      210021426B00214A730018395A0029527B00315A8400214A6B00214263001839
      630010426B00D6D6D600D6D6D600D6D6D600D6D6D600CECECE0010426B001842
      6B0029527B0029527B00294A730021426B001031520018395A00214273000810
      21001831520021426B0000000000000000000000000000000000000000000000
      08000008290000215A0000528C000084B50000A5CE0000B5DE0000CEE70000DE
      EF0018EFF70042F7F7005AF7F7004AF7F70029EFF70000E7F70000DEEF0000C6
      E700009CD600006BB500004A94000039730000314A0000081800313131000000
      000000000000000000000000000000000000000000008CADEF00004AD6004A9C
      EF007BD6F70073C6F70073C6F7007BCEF7007BCEF70084CEF70084D6F7008CDE
      FF008CDEFF00105ADE000052D6000852DE000052D600085ADE0084CEF70094DE
      FF008CD6F70084D6F70084D6F70084CEF7007BCEF7007BCEF70073C6F7007BD6
      FF004A9CEF00004AD6007BA5EF0000000000843121008C3118008C3921009442
      2900AD523900B55A4200BD6B4A00C6735200CE7B5A00CE846300D6846B00D684
      6B00D6846B00D6846B00D6846B00D6846B00D6846B00D6846B00D6846B00D684
      6B00D6846B00D6846B00D6846B00D6846B00D6846B00D6846B00D6846300C67B
      5A00BD6B4A00AD5239008C391800C6ADA5000000000018396B0018395A000010
      210021426B00214A73001839630029527B00296B9400215A7B0021395A001842
      6B0010396B00E7E7DE00DEDEDE00DEDEDE00DEDEDE00DEDED60010426B001842
      6B0029527B002963940029638C0021395A001839630018395A00214273000818
      310021426B000818290052525200000000000000000000000000000000000000
      00000008210000184A0000427B000073A500008CC60000ADD60000CEE70000DE
      EF0010EFF70031EFF70031F7F70018EFF70000E7F70000D6F70000C6EF0000A5
      DE00007BC60000529C000039730000294A000010180000000000000000000000
      000000000000000000000000000000000000FFFFFF003173DE00085ADE006BBD
      F7007BCEF70073C6F7007BCEF7007BCEF70084CEF70084D6F70084D6F70094DE
      FF0084CEF7000852DE00004AD6000052DE00004AD600085ADE0084CEF7009CE7
      FF008CD6FF008CD6FF0084D6F70084CEF70084CEF7007BCEF70073C6F7007BCE
      F7006BC6F700105ADE00316BDE00FFFFFF008C31210094392100A54A2900AD52
      3900B55A4200BD634A00C66B4A00C6735200CE735200CE7B5A00CE7B5A00CE7B
      5A00CE7B5A00CE7B5A00CE7B5A00CE7B5A00CE7B5A00CE7B5A00CE7B5A00CE7B
      5A00CE7B5A00CE7B5A00CE7B5A00CE7B5A00CE7B5A00CE7B5A00CE735A00BD6B
      5200B55A42009C4231007B311000EFE7E7000000000010294A00214A73000818
      310021426B00214A730018396300216B94004AA5C60029B5DE00183152001842
      6B0010426B00D6D6D600EFEFEF00EFEFEF00EFEFEF00D6D6D60018426B001842
      6B002952840031BDE70039B5DE001831520021426B0018395A00214273001029
      420021426B000000000000000000000000000000000000000000000000000000
      1000000018000010390000296B00005A9C00007BB500009CD60000C6E70000DE
      EF0008E7F70010EFF70008EFF70000E7F70000D6F70000BDEF0000A5DE000084
      C600005AAD000042840000295A00001029000000000000000000000000000000
      000000000000000000000000000000000000C6D6F700004AD6002173E7007BCE
      F70073C6F70073C6F7007BCEF7007BCEF70084CEF70084D6F7008CD6F7009CE7
      FF006BBDF700004AD600085ADE0073B5F700428CE7000042D6003984E70094DE
      FF0094DEFF008CD6FF008CD6F70084D6F70084CEF7007BCEF7007BCEF70073CE
      F7007BCEF7003184E700004AD600D6E7FF008C4A310094392100A54A3100B55A
      3900BD634200C66B4A00C66B4A00C66B4A00C6735200CE735200CE735200CE73
      5200CE735200CE735200C6735200C6735200CE735200C6735200CE735200CE73
      5200CE735200CE735200CE735200C6735200C6735200C66B4A00BD634A00B55A
      4200A54A31008C311800947B6B00FFFFFF000000000000102900294A73001031
      5A0021426B00294A7B0021396B0029739C005A8CAD0031ADD60029426B002952
      7B0010396300103163000831630008316300083163001031630010396300214A
      7300295A840042ADD6004AA5C6002142630021426B0021396300214A73001029
      4A00082139001818180000000000000000000000000000000000000000005252
      5A0000001000000831000021630000428C00006BAD000094CE0000BDE70000D6
      EF0000DEF70000DEF70000DEF70000D6F70000BDEF0000A5DE000084CE000063
      B500004A940000316B0000183900000008000000000000000000000000000000
      0000000000000000000000000000000000008CB5EF00004AD600398CE7007BCE
      F70073C6F70073CEF7007BCEF70084CEF70084D6F7008CD6F7008CD6F700A5EF
      FF004A94E7000039D6002173DE00C6FFFF006BB5EF000039D6000852DE009CE7
      FF009CDEFF008CD6FF008CD6FF0084D6F70084CEF7007BCEF7007BCEF70073C6
      F7007BD6F7004294EF000042D600B5CEF700A584730094392100A54A2900B552
      3900BD634200BD634200C66B4200C66B4A00C66B4A00C66B4A00C66B4A00C66B
      4A00C66B4A00C66B4A00C66B4A00C66B4A00C66B4A00C66B4A00C66B4A00C66B
      4A00C66B4A00C66B4A00C66B4A00BD6B4A00BD634200B55A4200AD523900A54A
      3100943921006B392100F7F7F700FFFFFF000000000000000000183963002142
      6B0018315A0010294A001029420010395A002994BD00217BA500102142001031
      4A001031520010314A0010314A0010314A0010314A0010314A00103152001031
      4A00103152002994BD00218CB5001021390010294A0010214200102942001021
      3900000008000000000000000000000000000000000000000000000000000000
      0000000010000008310000185A0000398400005AAD00008CCE0000B5E70000CE
      EF0000D6F70000D6F70000CEF70000B5EF00009CDE000084CE00006BB5000052
      9C0000397B000029520000102100000008000000000000000000000000000000
      0000000000000000000000000000000000005A8CE700085ADE0063BDF7007BCE
      F70073C6F7007BCEF7007BCEF70084CEF70084D6F7008CD6F7008CD6F700A5EF
      FF005294EF000039D600216BDE00BDFFFF0063ADEF000042D6000852DE0084CE
      F7009CE7FF008CDEFF008CD6F70084D6F70084CEF7007BCEF7007BCEF70073C6
      F7007BD6F7005AADEF00004AD6008CB5EF00DED6D600843921009C422100A54A
      3100B55A3900BD5A3900BD634200BD634200BD634200BD634200BD634200BD63
      4200BD634200BD634200BD634200BD634200BD634200BD634200BD634200BD63
      4200BD634200BD634200BD633900B55A3900B5523100AD4A31009C4229008C31
      210063392100EFEFEF0000000000000000000000000000000000081831001831
      5A0010315A0018426B0018426B0018396300395A8C003152840010294A001842
      6B0018426B0018426B0018426B0018426B0018426B0018426B0018426B001842
      6B0018396300315A8400315A840010294A0018426B0018426B00183963000821
      4200000000000000000000000000000000000000000000000000000000000000
      0000000018000010390000185A00003184000052AD000084CE0000A5DE0000BD
      EF0000C6EF0000BDEF0000B5EF00009CDE000084D600006BBD00005AA500004A
      8C00003963000018390000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A8CE700085ADE0063B5F70073CE
      F70073C6F7007BCEF70084D6FF008CD6FF008CDEFF0094DEFF009CE7FF00A5EF
      FF004A94EF000042D6004A9CEF00CEFFFF007BC6F700004AD600004AD60063AD
      EF00ADF7FF009CE7FF0094DEFF0094DEFF008CDEFF0084D6F7007BCEF70073C6
      F7007BCEF70063B5EF000852DE006B9CEF00FFFFFF00BD9C94008C3118009C42
      2900A54A3100AD523100B55A3900B55A3900BD633900BD5A3900BD633900BD5A
      3900B55A3900B55A3900B55A3900B55A3900B55A3900BD5A3900BD5A3900BD5A
      3900BD5A3900B55A3900B55A3900AD523100A54A29009C392100843118007352
      3900EFEFEF000000000000000000000000000000000000000000000810001031
      5A00214A7300214A7300214A730029527B0039638C0039638C0018395A00214A
      7300214A7300214A7300214A7300214A7300214A7300214A7300214A7300214A
      7300294A730039638C0039638C0021426300214A6B00214A7300214A73001839
      6B00000010000000000000000000000000000000000000000000000000000000
      000000002100001039000021630000398400005AAD00007BC60000A5DE0000B5
      E70000B5EF0000ADE70000A5E700008CD6000073C600006BB500005A9C00004A
      7B00003152000010210008101000000000000000000000000000000000000000
      0000000000000000000000000000000000005A8CE700085ADE0063B5F70073CE
      F7007BCEF7007BC6F7005AADEF006BBDF70063ADEF003184E7003984E700428C
      E7000052D6000052DE00105ADE0063ADEF00428CE7000052D600004AD600216B
      DE0073BDF7006BB5F7006BBDF70073BDF7006BB5EF007BC6F7007BCEF70073C6
      F7007BCEF70063B5EF000852DE006B9CEF0000000000FFFFFF00AD847B008C31
      18009C422100A54A2900AD523100B5523100B55A3100B55A3900B55A3900B552
      3100AD523100AD523100AD523100AD523100AD523100B5523100B55A3900B55A
      3900B55A3900B5523100AD4A3100A54229008C39180073392900B5B5AD00FFFF
      FF00000000000000000000000000000000000000000000000000000008001842
      6B00214A7300214A7B00214A7300315A840039638C0039638C0021426300214A
      7300214A7B00214A7B00214A7B00214A7B00214A7B00214A7B00214A7B00214A
      730029527B0039638C0039638C00294A730018396300214A7B00214A73002142
      7300001029000000000000000000000000000000000000000000000000000000
      210000102900001842000029630000398C00005AAD00007BC600009CDE0000AD
      E70000ADE70000A5E7000094DE00007BCE000073BD00006BAD00006394000052
      730000314A000010210000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005A94E700085ADE0063BDF7007BCE
      F7007BCEF7003184E700004AD6000852DE000052D600004AD600004AD6000052
      DE000852DE00085ADE000052DE00004AD6000052DE000852DE000852DE000052
      DE000052DE00004ADE000052DE00085ADE00004AD6002973E7007BCEF7007BCE
      F7007BCEF7005AA5EF00004AD6008CB5EF000000000000000000FFFFFF00BD9C
      94008C39210094392100A54A2900AD523100AD523100AD523100AD523100AD4A
      3100A54A2900A5422900A542290094523900A54A3100AD523100AD523100B55A
      3100B5523100AD523100A54A29007B3918008C7B6B00F7F7F700000000000000
      0000000000000000000000000000000000000000000000000000000008002142
      6B0029527B0029527B00214A73003152840039638C0039638C0021426300294A
      730029527B0021527B0021527B0029527B0029527B0021527B0029527B00214A
      7B00294A730039638C0039638C00294A730021426B0029527B0029527B00214A
      7300001031000000000000000000000000000000000000000000000000000000
      100000103100001842000029630000428400005AA5000073C6000094D60000A5
      DE0000A5E700009CDE00008CD600007BC6000073BD000073AD00006B94000052
      7300003142000000080000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094B5EF00004AD6003984E70084D6
      F70052A5EF00004AD6000052D6000052D600004AD600004AD600004AD6000042
      D6000042D6000042D6000042D6000042D6000042D6000042D600004AD6000042
      D6000042D6000042D6000042D600004AD6000052D600004AD6005AA5EF0084D6
      FF007BD6F7004294EF00004AD600B5CEF700000000000000000000000000FFFF
      FF00DECECE009452420094392100A54A2900AD4A3100AD523100A54A2900A54A
      29009C4221008C4229009C847B00ADADAD0084635200A54A2900AD523100AD52
      3100AD523100A54A29007B31180073736B00DEDEDE00FFFFFF00000000000000
      00000000000000000000000000000000000000000000000000009CA5AD001039
      630029527B0029527B0029527B00214A730039638C0031527B0021426B002952
      7B0029527B0029527B0029527B0029527B0029527B0029527B0029527B002952
      7B00214A7300315A8400315A8C002142630029527B0029527B0029527B00214A
      7300001031000000000000000000000000000000000000000000000000000810
      18000008210000214200003163000042840000529C00006BBD000084CE000094
      DE00009CDE000094DE000084D600007BC6000073BD00007BAD00006B9400005A
      7300002131000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6D6F700004AD6002973E70084D6
      FF00398CE700004AD6000052D6000852DE00317BE700398CE700317BE7003984
      E700428CE700428CE700428CE700428CE700428CE700428CE7003984E700398C
      E7003184E700317BE7003184E700186BDE00004ADE00004AD600297BE7007BCE
      F7007BD6F700317BE700004AD600DEE7FF000000000000000000000000000000
      0000FFFFFF00DED6D6008C5A52009C422900A54A2900A54A2900A54A29008C42
      290094736B00E7EFEF00CECECE00846B63008C422900A54A2900AD523100AD52
      3100AD523100A54A29009439210063291800A5A5A500EFEFEF00000000000000
      0000000000000000000000000000000000000000000000000000CED6DE001039
      630029527B0029527B0029527B0029527B0021426B0021426B0029527B002952
      7B0029527B0029527B0029527B0029527B0029527B0029527B0029527B002952
      7B0029527B00214A730021426B0029527B0029527B0029527B0029527B00214A
      7B00001831000000000000000000000000000000000000000000000000000000
      0000000000000018390000295A0000397300004A9400005AAD000073C600008C
      D6000094D6000094D6000084D600007BC6000073BD000073AD00006B9400005A
      7B0000394A000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF003173E700085ADE0073CE
      F7002973E700004AD600004AD6004A94EF0094E7FF0094DEFF0094DEFF009CE7
      FF009CE7FF00A5EFFF00A5EFFF00A5EFFF00A5EFFF00A5EFFF009CE7FF009CE7
      FF009CE7FF0094DEFF0094DEFF0084D6F7001863DE00004AD6000852DE006BBD
      F70073C6F700085ADE00316BDE00FFFFFF00000000000000000000000000FFFF
      FF00EFEFEF00ADADAD0073524200943921009C422900A54229009C4221006B42
      3100BDBDBD00DEDEDE009C9494008C3921009C422100A54A2900A54A2900AD52
      3100AD4A2900A54A29009C4221008C3118005A210800CECEC600FFFFFF000000
      0000000000000000000000000000000000000000000000000000CED6DE001839
      630029528400295284002952840029528400295A8400295A8400295284002952
      8400295284002952840029528400295284002952840029528400295284002952
      840029528400295A8400295A8400295284002952840029528400295A84002952
      7B00001831000000000000000000000000000000000000000000000000000000
      0000313942000010310000294A00003163000042840000529C000063B500007B
      C600008CD600008CD6000084D600007BCE000073BD00006BAD00006B9400005A
      7B00004A63000031520021526300000000000000000000000000000000000000
      000000000000000000000000000000000000000000008CB5EF00004AD60052A5
      EF002173E7000042D600004AD60073C6F70084D6F70084CEF70084D6F70084D6
      F7008CD6F7008CD6F7008CD6F7008CDEFF008CD6FF008CD6F7008CD6FF008CD6
      F7008CD6F70084D6F70084CEF70094DEFF00297BE7000039D6000852DE0073C6
      F70052A5EF00004AD60084ADEF0000000000000000000000000000000000FFFF
      FF00D6D6D6007B5A4A008C311800943921009C4229009C4229009C4221008C39
      18007B5A5200BDBDBD00845A4A008C391800944221009C4A2900A54A2900AD4A
      2900A54A2900A54A29009C4221008C3118007321100084635200F7EFEF000000
      0000000000000000000000000000000000000000000000000000CED6DE001842
      6B00395A840039638C0039638C0039638C0039638C0039638C0039638C003963
      8C0039638C0039638C0039638C0039638C0039638C0039638C0039638C003963
      8C0039638C0039638C0039638C0039638C0039638C0039638C00315A84002952
      7B00001831000000000000000000000000000000000000000000000000000000
      0000394252000008210000183900002952000031630000428400004A9C000063
      B5000073C6000084CE000084CE00007BCE00006BBD000063AD00005A9C000052
      84000042630000314A0018394A00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7EFFF000852DE00186B
      DE004A9CEF00105ADE003984E7007BCEF7007BCEF7007BCEF70084CEF70084CE
      F70084D6F70084D6FF008CD6F7008CD6F7008CD6FF008CD6F70084D6F70084D6
      F70084CEF70084CEF7007BCEF70084CEF7005AADEF004A9CEF0052A5EF0073C6
      F700186BDE000052DE00DEE7FF0000000000000000000000000000000000F7F7
      F700BDB5B500732918008C311800943921009C4221009C422100944221008C39
      18007B291000ADADAD00843929008C391800944221009C422900A54A2900A54A
      2900A54A2900A54A29009C4221008C3918007B29100063291800DEDEDE000000
      0000000000000000000000000000000000000000000000000000CED6DE003152
      7B006B8CAD007B94B5007B94B5007B94B5007B94B5007B94B5007B94B5007B94
      B5007B94B5007B94B5007B94B5007B94B5007B94B5007B94B5007B94B5007B94
      B5007B94B5007B94B5007B94B5007B94B5007B94B5007B94AD005A7B9C00315A
      8400001831000000000000000000000000000000000000000000000000000000
      00000000000000000000000821000018310000214A0000295A0000397B00004A
      9400005AAD00006BBD000073C6000073C600006BBD00005AAD0000529400004A
      84000039630000294A0000082100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000739CEF00004A
      D6004294EF007BCEF7007BCEF70073C6F70073C6F7007BCEF7007BCEF7007BCE
      F70084CEF70084D6F70084CEF70084D6F70084D6F70084D6F70084D6F70084CE
      F70084CEF7007BCEF7007BCEF7007BC6F7007BCEF7007BD6F70084D6FF004294
      EF00004AD6006394E7000000000000000000000000000000000000000000F7F7
      F700A5948C007B2110008C3918009C4A2900A5523100A55231009C4A31009439
      21007B291000ADADAD00843921008C391800943921009C422100A54A2900A54A
      2900A54A2900A54A29009C4221008C3918008429100063210800C6B5AD000000
      0000000000000000000000000000000000000000000000000000CECED6003152
      7B008CA5BD009CB5C6009CB5C6009CB5C6009CB5C6009CB5C6009CB5C6009CB5
      C6009CB5C6009CB5C6009CB5C6009CB5C6009CB5C6009CB5C6009CB5C6009CB5
      C6009CB5C6009CB5C6009CB5C6009CB5C6009CB5C6009CADC600738CAD00315A
      8400001029000000000000000000000000000000000000000000000000000000
      0000000000002121210000000000000818000010290000183900002152000029
      730000398C000052A5000063AD000063B5000063B5000052A500004A94000039
      7B000029630000214A0000002100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F7F7FF002163
      DE00004AD6005AADEF007BD6F70073C6F70073C6F70073C6F7007BCEF7007BCE
      F7007BCEF70084CEF70084CEF70084CEF70084CEF70084CEF70084CEF70084CE
      F7007BCEF7007BCEF7007BCEF70073C6F70073C6F7007BCEF7005AADEF00004A
      D6001863DE00EFF7FF000000000000000000000000000000000000000000FFFF
      FF00AD9C940073211000AD736300BD847300BD847300BD847300B5846B00B573
      63007B291000C6BDBD00843921008C3118009C4A3100AD5A4200B5634A00B563
      4A00B5634A00AD634A00AD5A4200944A31007B29100063180800C6ADA5000000
      0000000000000000000000000000000000000000000000000000000000002142
      6300426B8C005A7B9C005A7B9C005A7B9C005A7B9C005A7B9C005A7B9C005A7B
      9C005A7B9C005A7B9C005A7B9C005A7B9C005A7B9C005A7B9C005A7B9C005A7B
      9C005A7B9C005A7B9C005A7B9C005A7B9C005A7B9C005A7B9C00426B8C001839
      6300182129000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000080000082100001031000010
      4A000021630000317B00004294000052A5000052A500004A9C0000428C000031
      7B000021630000184A0000001800000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B5CE
      F7000052DE00085ADE0063B5F7007BCEF70073C6F70073C6F70073C6F7007BCE
      F7007BCEF7007BCEF7007BCEF7007BCEF7007BCEF7007BCEF7007BCEF7007BCE
      F7007BCEF70073CEF70073C6F70073C6F7007BCEF70063B5F700105ADE000052
      D600ADC6F700000000000000000000000000000000000000000000000000FFFF
      FF00D6CECE0073291800C69C8C00E7D6CE00E7CEC600E7CEC600E7D6CE00CEA5
      9C0073311800E7E7E7008C524A0084311800BD8C7B00C6948400CE9C8400CE9C
      8400CE9C8400CE948400C6948400BD8C7B007B2910005A211000E7DEDE000000
      000000000000000000000000000000000000000000000000000000000000E7E7
      EF004A637B0010315A001839630018396B001839630018396B0018396B001839
      6B0018396B0018396B0018396B0018396B0018396B0018396B0018396B001839
      6B0018396B0018396B0018396B0018396B00183963001839630008214A005A63
      6B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000018000008
      29000008390000185A0000296B000039840000428C0000428C00003984000029
      730000215A000010420000001800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009CBDEF00004AD600085ADE0052A5EF007BCEF70073CEF70073C6F70073C6
      F70073C6F70073CEF70073C6F7007BCEF7007BCEF7007BC6F70073C6F70073C6
      F70073C6F70073C6F70073CEF7007BCEF7005AADEF00105ADE00004AD60094B5
      EF00000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF009C7363008C423100C6A59400EFDEDE00EFE7E700CEADA5008C52
      4200BDADA500FFFFFF00C6ADAD007B211000BD948400E7CECE00EFDED600E7D6
      CE00E7D6CE00EFD6D600E7D6CE00C6948C007321080073524A00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6CED600CECED600CECED600CECED600CECED600CECED600CECE
      D600CECED600CECED600CECED600CECED600CECED600CECED600CECED600CECE
      D600CECED600CECED600CECED600CECED600CECED600CECED600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      1000000018000008310000104A0000215A0000296B0000317300003173000029
      6300001852000010390000001000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000094B5EF000052D600004AD600398CEF0073C6F7007BD6F70073CE
      F70073C6F70073C6F70073C6F70073C6F70073C6F70073C6F70073C6F70073CE
      F70073CEF7007BD6F70073CEF7004294EF00004AD6000052D60094B5EF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F7EFEF00946B5A006B211000732108006B2110006B291800AD9C
      9C00FFFFFF0000000000F7F7F70094635A0084392100B5847300DEC6BD00F7EF
      EF00F7EFEF00DECEC600BD8C7B00843929004A180800EFEFEF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000001000000021000008390000184A0000215A0000215A000021
      5200001842000008290000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B5CEF7002163DE00004AD6001863DE004A9CEF006BBD
      F7007BCEF7007BCEF7007BCEF7007BCEF70073CEF7007BCEF7007BCEF7007BCE
      F7006BC6F7004A9CEF001863DE00004AD6001863DE00B5CEF700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00DED6CE00B59C9400BDA5A500F7F7F7000000
      0000000000000000000000000000F7EFEF0094635200732108007B2910007B29
      10007B2910007B291000732108004A291000D6CECE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000800000018000008310000103900001042000010
      4200001031000000100018181800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EFF7FF006B9CEF000852DE00004AD600085A
      DE002173E700398CE70063B5F70063B5F70063B5F70063B5F700398CE7002973
      E700085ADE00004AD6000852DE006B9CEF00EFF7FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00BDA59C00845242006B29
      10006B2110006331210094847B00F7F7F7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000003131310000000000000000000000080000001800000821000008
      1800000008000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7EFFF008CB5EF003173
      DE00004AD600004AD600085ADE00085ADE00085ADE00085ADE00004AD600004A
      D6003173DE0084ADEF00DEE7FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000002121180000000000000000000000
      00003939390000000000000000000000000042424A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6D6F70094B5EF005A94E7005A8CE7005A8CE7005A94E70094B5EF00C6D6
      F700FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000600000000100010000000000000600000000000000000000
      000000000000000000000000FFFFFF00FFFF9FFFFFFFFFFFFFFFFFFF00000000
      FFFC0FFFFFFE7FFFFFFE7FFF00000000FFF807FFFFF81FFFFFF81FFF00000000
      FFF003FFFFE007FFFFE007FF00000000FFE001FFFF8001FFFF8001FF00000000
      FFC000FFFE00007FFE00007F00000000FF80007FF800001FF800001F00000000
      FF00003FE0000007E000000700000000FE00001FE0000007E000000700000000
      FC00000FE0000007E000000700000000F8000007E0000007E000000700000000
      F0000003E0000007E000000700000000E0000003E0000007E000000700000000
      C0000003E0000007E00000070000000080000007E0000007E000000700000000
      0000000FE0000007E0000007000000008000001FE0000007E000000700000000
      0000003FE0000007E0000007000000000000007FE00000078000000100000000
      0000007FE000000700000000000000008000003FE00000078000000100000000
      8000001FE0000007E000000700000000C000000FF800001F8000000100000000
      E0000007FE00007F0000000000000000E0000003FF8001FF8000000100000000
      F0002001FFE007FFE000000700000000F0007000FFF81FFFF800001F00000000
      F880F800FFFE7FFFFE00007F00000000F9C1FC01FFFFFFFFFF8181FF00000000
      FFE7FE03FFFFFFFFFFE7E7FF00000000FFFFFF07FFFFFFFFFFFFFFFF00000000
      FFFFFF8FFFFFFFFFFFFFFFFF00000000F8000000F1FC00FFFFFFFF87FFFFFFFF
      F800000080C0000FFFFFFF03FFFFFFFFF800000080000003FFFFFE01FFFFFFFF
      F800000080000001FFFFFC01FFFFFFFFF800000000000001C0000001FFFFFFFF
      F80000000000000180000001FFFFFFFFF80000000000000100000001FFFFFFFF
      0000000000000001000000010000000000000000000000030000000100000000
      0000000000000007000000010000000000000000800000070000000100000000
      0000000080000007000000010000000000000000800000070000000100000000
      0000000080000007000000010000000000000000800000070000000100000000
      00000000C0000007000000010000000000000000C00000070000000100000000
      00000000C0000007000000010000000000000000E00000070000000100000000
      00000000E0000007000000010000000000000000F00000070000000100000000
      00000000F0000007000000010000000000000000F80000070000000100000000
      00000000FE000003000000018000000100000000FE00000300000001C0000003
      00000000FE00000300000001FFFFFFFF00000000FE00000300000001FFFFFFFF
      00000001FE00000700000003FFFFFFFF00000003FF80000F80000007FFFFFFFF
      00000007FF60007FFFFFFFFFFFFFFFFFF800000FFFFFFFFFFFFFFFFFFFFFFFFF
      F800001FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF800003FFFE007FFFFF8001F
      FFFFFFFFF800000FFF8001FFFFE00007FFFFFFFFF000000FFE00007FFC000003
      F8000003F000000FFC00003FF8000003E0000001E000000FF800001FF8000001
      80000001E000000FF000000FF000000080000000E000000FE0000007F0000000
      00000000E000000FC00000030000000000000000E000000FC000000300000000
      00000001E000001F800000010000000000000001E000001F8000000100000000
      80000001E000003F000000000000000080000003E000007F0000000000000000
      80000003E00000FF0000000000000000C0000007F00000FF0000000000000003
      C0000007F00001FF0000000000000007C0000007F00001FF000000008000000F
      C0000007E00003FF00000000C000003FC0000007E00003FF00000000E000003F
      C0000007E00007FF00000000F000003FC0000007F00007FF00000000E000001F
      C0000007F00001FF80000001E000001FC0000007F00001FF80000001E000001F
      C0000007F80001FFC0000003E000001FC0000007F80001FFC0000003E000001F
      E0000007FE0001FFE0000007E000001FE000000FFF8001FFF000000FF000001F
      F800003FFFC001FFF800001FF804003FFFFFFFFFFFE001FFFC00003FFC1E007F
      FFFFFFFFFFF001FFFE00007FFFFF00FFFFFFFFFFFFF803FFFF8001FFFFFFFFFF
      FFFFFFFFFFFF077FFFF007FFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object im16: TImageList
    Left = 672
    Top = 44
    Bitmap = {
      494C010161006300040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000009001000001002000000000000090
      0100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000873AD00218CC600218CC6000873AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000086B
      A5002984AD002194C60021A5DE00189CD600108CC600087BAD00007BAD000063
      9C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000086BA5008C8C
      AD00B55A5A007B5A6300318CB50029ADE700108CAD0008B5BD0008ADBD00008C
      BD000094CE0000639C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000086BA50073BDEF00C673
      7B00DE635A00D65A5A0073525A0018A5CE0000FFFF0000FFFF0000FFFF0000BD
      DE00187B9C00007B9C0000639C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000107BB5005AB5EF004AADDE00739C
      BD00A5849C008494B5005A94BD0021C6EF0018DEF70010CEEF0010B5E700739C
      7B00EF8C0000BD7B1000316B63000073AD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000002994CE00296BF7000839E700185A
      BD00399CCE004AC6FF0042BDFF0042B5F70039ADEF0021ADEF004AA5BD00F79C
      0800FF9C0000FF9C0000947B3100007BBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000218CC6004294FF000031FF000021
      FF002152B5005ABDFF0052C6FF004ABDF70042B5EF0031B5EF0029ADE70042AD
      C6005AA5A50084A57300299CB5000073AD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001884BD0052ADFF004294
      FF004294E70039948C0042A5CE004AB5EF0039ADE70039B5EF00219CD6000884
      BD0000639C000084C60000639C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001884BD0052BDF70063CE
      F70039AD7B0029BD5A0029945200317BAD0000000000298CBD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000873AD00298C
      C6004ACEB50042CE9C004ACEB50052BDF70042ADE700319CD6000873AD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000873AD001884BD002994CE002994CE002184BD000873AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000094ADAD009CA5A50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005A84AD0021528400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000849C9C00849C9C00ADC6C6009CC6C60094ADAD009CA5A500A5B5BD000000
      000000000000000000000000000000000000000000005A9CD600316BAD00316B
      AD00316BAD00316BAD00316BAD00316BAD00316BAD00316BAD00316BAD00316B
      AD00316BAD00316BAD00316BAD00000000000000000000000000000000000000
      000000000000000000008C8C8C00000000000000000000000000000000000000
      0000000000008C8C8C0000000000000000000000000000000000000000006B8C
      AD00316B9C004A84B5004A7BAD00295284000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9C9C0094AD
      AD0073949400738C8C008CA5A5006B9494008CA5A500B5CECE00A5C6CE000000
      000000000000000000000000000000000000000000004AA5EF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00316BAD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A84AD0031639C00528C
      BD0073A5D6006B9CD6004A84B500316394002952840000000000000000000000
      00000000000000000000000000000000000000000000000000006B847B00738C
      8C007B949C00738C94007B9494008CADAD007B94940094ADAD0094B5B5009CAD
      AD00A5ADB500ADBDBD000000000000000000000000004AA5EF00F7FFFF00296B
      3100296B3100296B3100296B3100296B3100296B3100296B3100296B3100296B
      3100296B3100F7FFFF00316BAD00000000000000000000000000000000000000
      00005A4A4A005A4A4A005A4A4A005A4A4A005A4A4A00524A4A00524A4A00524A
      4A004A4A42004A4A42004A424200845A5A005A84AD004A7BB500639CCE0073A5
      D6005A94C60073A5DE004A84B50031639C003163940029528400000000000000
      000000000000000000000000000000000000000000008C9C9C00738C94006B84
      84006B8C8C00849C9C0084A5A50084ADA50094BDB5009CBDC6009CBDB50094AD
      AD00B5CECE00000000000000000000000000000000004AA5EF00F7FFFF00296B
      3100F7FFFF007BADD600F7FFFF007BADD600F7FFFF00F7FFFF007BADD600F7FF
      FF00296B3100F7FFFF00316BAD00000000000000000000000000000000005A52
      4A00845A5A000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000031639C004A7BB500316B9C00295A
      9400396B9C006394CE00638CBD006B8CB500396B9C0031639C00295284000000
      000000000000000000000000000000000000000000008C9494006B848400738C
      8C008CA5A5008CA5A50084ADA5009CC6C6009CC6C60084ADA50073948C008CA5
      A500A5C6C6009CADAD00A5A5A50000000000000000004AA5EF00F7FFFF00296B
      31004ABD6B007BADD6004ABD6B007BADD6004ABD6B004ABD6B007BADD6004ABD
      6B00296B3100F7FFFF00316BAD00000000000000000000000000000000006352
      4A00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000031639C004273A50018528400396B
      A5005A8CC60073ADDE00A5CEF7008C94A5005A84B500316B9C00316394002952
      840000000000000000000000000000000000A5A5A5008CA5A500738C8C00738C
      8C00738C8C00738C8C007B9494008CADAD0084ADA5007B9C9C008CADAD00A5C6
      CE008CADB50094B5B500B5C6C60000000000000000004AA5EF00F7FFFF00296B
      3100F7FFFF007BADD600F7FFFF007BADD600F7FFFF00F7FFFF007BADD600F7FF
      FF00296B3100F7FFFF00316BAD0000000000000000000000000000000000BD9C
      940063524A0063524A00A57B7300AD848400BD9C940000000000000000000000
      00000000000000000000000000000000000031639C006394CE00639CCE007BAD
      DE00A5CEF700FFFFFF00FFFFFF00BDBDBD0039424A005A84AD00316B9C003163
      9C00295284000000000000000000000000009CA5A5008C9C9C007B949400849C
      9C00849C9C007B948C008C9C9400848C8C0084A5A5008CADAD009CC6BD0094BD
      BD00638C8400A5BDBD000000000000000000000000004AA5EF00F7FFFF00296B
      31004ABD6B007BADD6004ABD6B007BADD6004ABD6B004ABD6B007BADD6004ABD
      6B00296B3100F7FFFF00316BAD0000000000000000000000000000000000AD84
      84006B5252004ACEEF004ACEEF004ACEEF0063524A00A57B7300AD848400BD9C
      940000000000000000000000000000000000316B9C006B9CCE00A5CEF700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00C6C6C600313131004A5A63005A8CBD00316B
      9C003163940029528400000000000000000000000000949C9C009CB5AD008CA5
      A5007B949400738C8C00849494009CA5A5007B948C0094B5B500B5D6D600BDDE
      DE009CB5BD0094B5B500ADC6CE00ADADAD00000000004AA5EF00F7FFFF00296B
      3100F7FFFF007BADD600F7FFFF007BADD600F7FFFF00F7FFFF007BADD600F7FF
      FF00296B3100F7FFFF00316BAD0000000000000000000000000000000000A57B
      73009C6B63009C6B63009C6B63009C6B63009C6B63009C6B63009C6B63005A4A
      4A00A57B7300AD848400BD9C9400000000005A84AD00315A7B00CECECE00FFFF
      FF00CEEFDE00EFFFF700CECECE00A5A5A5004A4A4A003939390063737B005A84
      B50031639C00316394002152840000000000000000008C9C9C009CB5B5007B94
      9400738C8C006B8484006B8484006B7B7B006B84840094B5AD00BDE7E700C6E7
      E7009CBDBD008CB5B500C6E7E700BDCECE00000000004AA5EF00F7FFFF00296B
      3100296B3100296B3100296B3100296B3100296B3100296B3100296B3100296B
      3100296B3100F7FFFF00316BAD00000000000000000000000000BD9C9400735A
      520018A5D6004ACEEF004ACEEF004ACEEF0018A5D600F77B4200F77B4200F77B
      4200F77B4200D6730000AD848400000000000000000042424200ADC6BD0063BD
      7300319439007B947B00B5B5B500C6C6C6006B6B6B00525252005A5252004252
      5A005284B50031639C0021528C0000000000000000008C9494009CADAD008CAD
      A5005A7B7B0052737300426363004A636300637B7B008CADA500C6E7E700ADCE
      CE00739C94008CA5A500ADBDBD0000000000000000004AA5EF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00316BAD00000000000000000000000000AD8484007B5A
      52009C6B63009C6B63009C6B63009C6B63009C6B63009C6B63009C6B63009C6B
      63009C6B63009C6B6300A57B7300000000000000000042424200106B10004273
      2100B56B4200DE6B3900B5A5AD00B5ADB500CECECE006B6B6B00737373003939
      39006B7384005A84AD00295A8C000000000000000000000000009CA5A50094B5
      B5005A7B7B00425A6300425A630042635A006B8C840094ADAD00ADD6D60094B5
      B5008CADAD009CBDC600A5B5B50000000000000000004AA5EF00F7FFFF00F7FF
      FF00296B3100296B3100296B3100296B3100296B3100296B3100296B3100296B
      3100F7FFFF00F7FFFF00316BAD00000000000000000000000000A57B73000000
      000018A5D60018A5D60018A5D60018A5D60018A5D600F77B4200F77B4200D673
      00001010D6001010D6009C6B630000000000000000000000000042424200A552
      3900AD393900A542630094429400AD94AD00BDBDBD00CECECE006B6B6B004242
      4200525A5A00294A73003973A5000000000000000000000000008C9494007B94
      94008CADAD00849C9C006B848400738C8C007B9C9C0084A5A5006B8484006B8C
      84009CBDBD00BDDEDE000000000000000000000000004AA5EF00F7FFFF00F7FF
      FF004ABD6B004ABD6B0042B55A0042B55A00319C3900319C3900319C39003184
      3900F7FFFF00F7FFFF00316BAD000000000000000000BD9C9400845A5A000000
      0000D6730000FFB59400FFB59400F77B4200F77B4200F77B4200D67300000000
      00001010D6001010D600845A5A00000000000000000000000000000000004242
      4200424242007B297B00527B8C0052B5BD00A5ADAD009C9C9C00CECECE008484
      84006B6B7300103963004A73AD00000000000000000000000000000000000000
      00009C9C9C00849C9C00849C9C00849C9C00738C8C00637B7B008CA5A50094B5
      B5009CB5B500ADADAD000000000000000000000000004AA5EF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FFFF00F7FF
      FF00F7FFFF00F7FFFF00316BAD000000000000000000AD848400845A5A00845A
      5A00845A5A007B5A5200735A5200735A52006B52520063524A0063524A005A52
      4A005A4A4A005A4A4A00845A5A00000000000000000000000000000000000000
      0000424242004242420052737300845A5A00A54242009C8C9C00DEDEDE00BDBD
      BD00424242000000000000000000000000000000000000000000000000000000
      0000000000008C9C9C00000000007B8C8C00A5B5B500849494009CB5AD00B5C6
      C60000000000000000000000000000000000000000004AA5EF004AA5EF004AA5
      EF004AA5EF004AA5EF004AA5EF004AA5EF004AA5EF004AA5EF004AA5EF004AA5
      EF004AA5EF004AA5EF005A9CD60000000000AD848400A57B7300000000000000
      00000000000000000000D6730000FFB59400D673000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000424242006B315200B542B50084218400424242004242
      4200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000949C9C00A5ADA50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B5A5A00845A5A00000000000000
      0000000000000000000000000000D67300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004242420052295A0042424200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008C8C8C007373
      73007B7B7B009494940094949400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000297B9400297B
      9400297B9400297B9400297B9400297B9400297B9400297B9400297B9400297B
      9400297B9400297B9400297B9400000000000000000000000000000000000000
      0000000000000000000000000000E7E7E700DEDEDE00D6D6D600D6D6D600E7E7
      E700EFEFEF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006B6B6B00315A8C00296B
      8C00294A5A00424242005A5A5A006B6B6B007B7B7B0094949400000000000000
      000000000000000000000000000000000000000000001884AD001894C6001894
      C6001894C6001894C6001894C6001894C6001894C6001894C6001894C6001894
      C6001894C6001894C6001894C600297B9400000000000000000000000000C6C6
      C600A5A5A500ADADAD008C7B7B00BD6B5200C67B6300CE846B00CE846B00BD73
      5A00B5634A007B524200BDBDB50000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000094949400296B8C004273C6003984
      DE00398CE7002194E700297BAD00295A7B00294A5A00424242005A5A5A006B6B
      6B00848484000000000000000000000000001894C60063CEFF001894C6009CFF
      FF006BD6FF006BD6FF006BD6FF006BD6FF006BD6FF006BD6FF006BD6FF006BD6
      FF0031ADD6009CFFFF001894C600297B94000000000000000000000000008C73
      6B00844A39007B6B6300A5634A00D68C7B00D6948400DE9C8C00DE9C8C00D694
      7B00CE846B00A54A31007B392900000000000000000000FFFF0000FFFF00C6C6
      C600C6C6C60000FFFF0000FFFF0000FFFF000000000000000000000000000000
      00000000000000000000000000000000000094949400296B8C00427BCE00427B
      D6003984DE00398CE700398CEF003194F700299CFF002194E700297BAD001863
      7300314A5A006363630000000000000000002184BD0063CEFF001884BD009CFF
      FF006BD6FF006BD6FF006BD6FF0063CEFF0063CEFF0063CEFF0063CEFF0063CE
      FF0039A5D6009CFFFF001894C600297B9400FFFFFF00EFEFEF00D6D6D600A563
      5200C67B6300DE947B00DE9C8400E7A59400E7A59400E7A59400E7A59400E7A5
      9400E7A58C00CE7B6300B5634A00B5947B0000FFFF0000FFFF00FF0000008484
      84000000FF00C6C6C60000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF000000000000000000000000000000000094949400296B8C00427BCE00427B
      CE00427BD6003984DE00398CE700398CEF003194F700319CFF00319CFF00319C
      FF00219CEF00424242000000000000000000218CBD0063CEFF00218CBD009CFF
      FF007BE7FF007BE7FF007BE7FF0063CEFF0063CEFF0063CEFF0063CEFF0063CE
      FF0042ADDE009CFFFF001894C600297B9400D6D6D600B5B5B5008C8C8C00A56B
      5200CE7B6300DE947B00DE9C8400DE9C8400DE9C8400DE9C8400DE9C8400DE9C
      8400DE9C8400CE846B00BD6B520094522900FFFFFF00C6C6C600FF0000000000
      FF0084848400FFFFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000094949400296B8C00427BD6004273
      C600427BCE00427BD6003984DE00398CE700398CEF003194F700319CFF00319C
      FF00219CF700424242000000000000000000218CC60063CEFF002994C6009CFF
      FF0084EFFF0084EFFF0084EFFF0084EFFF0084EFFF0000DE4A0000DE4A0084EF
      FF004AB5E7009CFFFF001894C600297B94008C312100AD523900B55A4200C673
      5200CE735200CE7B5A00CE7B5A00CE7B5A00CE7B5A00CE7B5A00CE7B5A00CE7B
      5A00CE7B5A00BD6B5200B55A4200EFE7E70084848400FF000000848484000000
      FF00C6C6C600C6C6C600FF000000FF00000084848400C6C6C60000FFFF0000FF
      FF0000FFFF0000FFFF00000000000000000094949400296B9C00427BD6004A6B
      BD004273C600427BCE00427BD6003984DE00398CE700398CEF003194F700319C
      FF00219CF700424242000000000000000000298CC60063CEFF00319CCE009CFF
      FF0094F7FF0094F7FF0094F7FF009CFFFF0000DE4A008CFFAD008CFFAD0000DE
      4A0052BDEF009CFFFF001894C600297B94008C4A3100B55A3900BD634200C66B
      4A00C6735200CE735200CE735200C6735200CE735200CE735200CE735200C673
      5200C6735200B55A4200A54A3100FFFFFF00FF000000848484000000FF00C6C6
      C600FF00000084848400C6C6C60084008400FF000000FF0000008484840000FF
      FF0000FFFF0000FFFF0000FFFF00000000009494940031639C00315A8C003952
      7B00394A7300426BBD00427BCE00427BD6003984DE00398CE700398CEF003194
      F700219CF7004242420000000000000000002994C6006BD6FF00319CCE009CFF
      FF009CFFFF009CFFFF009CFFFF0000DE4A008CFFAD008CFFAD008CFFAD008CFF
      AD0000DE4A009CFFFF001894C600297B9400FFFFFF009C422900A54A3100B55A
      3900BD633900BD5A3900B55A3900B55A3900B55A3900BD5A3900BD5A3900AD52
      3100A54A290073523900EFEFEF00000000000000FF000000FF0084848400C6C6
      C600848484000000FF0084848400848484000000FF0084008400FF0000008484
      8400C6C6C60000FFFF0000FFFF0000FFFF00949494003973B500394A73004A63
      B5004A63AD00425A8C00425A8C00425A8C003963A500396BAD003984D6003994
      F7002994EF004242420000000000000000002994C6007BE7FF002994C600FFFF
      FF00FFFFFF00FFFFFF0000DE4A004AFF73004AFF73004AFF73004AFF73004AFF
      73004AFF730000DE4A001894C600297B9400000000008C3118009C422100B552
      3100B55A3100B5523100AD523100AD523100AD523100B55A3900B55A3900A542
      29008C391800FFFFFF000000000000000000FFFFFF000000FF00C6C6C600C6C6
      C600848484000000FF0084848400FFFFFF00FFFFFF000000FF000000FF00FF00
      0000FF00000084848400C6C6C60000FFFF0094949400426BB500394A73004273
      C6004273C6004273C6004273C60084DEF7008CEFFF0052A5EF003963A5003163
      8C00396BAD003942520000000000000000003194CE0084EFFF0084E7FF002994
      C6002994C600319CCE00319CCE00319CCE00319CCE00319CCE00319CCE00319C
      CE00319CCE00319CCE00319CCE00000000000000000000000000FFFFFF009C42
      2900A54A29008C42290094736B00846B63008C422900AD523100AD5231006329
      1800A5A5A5000000000000000000000000000000000000FFFF0000FFFF00FFFF
      FF00FF0000000000FF000000FF000000FF0084848400C6C6C600C6C6C6000000
      FF0084008400FF000000840000008484840094949400395273003973B500397B
      D600397BD600397BD600397BD6007BDEFF007BDEFF0052ADF700397BD600397B
      D600397BD600425A8C000000000000000000319CCE0094F7FF008CF7FF008CF7
      FF008CF7FF0000630000006300000063000021BD420021BD420021BD420021BD
      42000063000000630000006300000000000000000000FFFFFF00EFEFEF009439
      21009C4229006B423100BDBDBD008C3921009C422100AD523100AD4A29008C31
      18005A2108000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF00C6C6C600840000008484840084848400FF00000084848400FFFFFF0000FF
      FF000000FF000000FF00FF000000FF000000949494004A84CE008CC6F700BDEF
      FF00ADD6F7008CC6F7008CC6F70052A5EF00529CE700398CEF00398CE700398C
      E700398CE700315263000000000000000000319CCE00FFFFFF009CFFFF009CFF
      FF009CFFFF009CFFFF00FFFFFF000063000010AD210010AD210010AD210010AD
      21000063000000000000000000000000000000000000F7F7F700A5948C009C4A
      2900A5523100943921007B2910008C39180094392100A54A2900A54A29008C39
      18008429100000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF00FFFFFF00FF000000FF000000C6C6C600FFFFFF0000FFFF0000FF
      FF0000FFFF00FFFFFF000000FF0000FFFF00000000009494940084848400426B
      A500216BCE003984D6005A94CE006BC6F7007BCEFF0094E7FF007BCEFF007BDE
      FF006BCEFF00394A6B00000000000000000000000000319CCE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00319CCE000063000000A5080000A5080000A5080000A5
      08000063000000000000000000000000000000000000FFFFFF00AD9C9400BD84
      7300BD847300B57363007B2910008C3118009C4A3100B5634A00B5634A00944A
      31007B29100000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000009494
      9400184AB50021398400000000000000000018215A0010186B0031638C00396B
      AD007B7B7B000000000000000000000000000000000000000000319CCE00319C
      CE003194CE00319CCE0000000000006300000063000000630000006300000063
      0000006300000000000000000000000000000000000000000000000000006B21
      100073210800AD9C9C00FFFFFF0094635A0084392100F7EFEF00F7EFEF008439
      29004A1808000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000009494
      94004A8CD600296BDE0021398400213984001839940010218400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DED6
      CE00B59C94000000000000000000F7EFEF00946352007B2910007B2910004A29
      1000D6CECE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094949400A5A5A500426B9400426B9400425A8C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF000000FF000000FF000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000527B
      4A000052000000630000000000000000000000000000087B0800000000000000
      0000000000000000000000000000000000000000000000000000000000004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C000000FF000000FFFFCE00FFFF0000FF9C3100FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A6B42000042
      0000005A0000007B00000000000000000000007B0000007B00006BBD63006BB5
      6B008CBD8400000000000000000000000000000000004A4A4A000000000063BD
      F70063BDF70063BDF700004A7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFCE00FFFF0000FF9C3100FF003100FF0031000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000052BD4200003100000039
      0000005200000073000000000000107B080010A500000084000018BD000010A5
      0000108C00008CBD840000000000000000004A4A4A004A4A4A006BC6FF005AAD
      E7006BC6FF003984AD0052A5DE00214252000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF000000FF9C3100FF003100FFFFFF00FF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000052C6420018AD000000310000295A
      29000000000000000000189C0800187B1800109C00000084000018BD000021C6
      000010AD0000429C420000000000000000000000000000000000398CB5004A9C
      CE004A9CCE004294BD0052A5DE002142520018394A0000000000000000000000
      0000000000000000000000000000000000000000000000009C000000FF0031FF
      FF000000AD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000029395A002942
      7B000000000000000000000000000000000018BD000018AD0000395A39000000
      00007BAD7B0000000000000000000000000018841800007B0000000000000000
      0000086B0000105210003963390000000000000000000000000052A5DE006BC6
      FF006BC6FF0052A5DE00007BBD00184A63002142520018394A00000000000000
      000000000000000000000000000000000000000000000000000031FFFF00FFFF
      FF000000AD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000029427B00315294003152
      94003152940000000000000000000000000021B5000021CE000021A51000187B
      1800529C52000000000000000000000000000000000021842100000000000000
      000008520000004200000042000000000000000000000000000063BDF70052A5
      DE0052A5DE0063BDF700184A6300184A6300184A630018394A00183142000000
      00000000000000000000000000000000000000000000000000000000AD000000
      AD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000029395A0031529400315AA5004A73
      C600738CC60031529400000000000000000039BD210018B50000088C0000088C
      00005A9C5A00000000000000000000000000000000000000000000000000219C
      390000730000086B1000317B39000000000000000000000000000000000063BD
      F700398CB500104A6B00184A6300184A630018425A001831420018394A002142
      5200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FF0000008C0000CEFF9C0000BD0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000029395A00315294004A73C6006384
      C6006384C600315AA500000000000000000000000000007B000008940000109C
      0000529C5200000000000000000000000000000000000000000039A53900109C
      100000940000429C4A0000000000000000000000000000000000000000000000
      000000000000184A6300181818009C9C9C000000000018425A00184A63001842
      5A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CEFF9C0000BD000000520000008C00000000
      42000000000000000000000000000000000000000000000000005A4A29006B5A
      42007B7363007B735A0000000000000000000000000029427B0029427B000000
      0000315AA500000000000000000000000000318C3100318C3100399439003994
      39003994390000000000000000000000000000000000188C1800000000000000
      000031AD310073BD7B0000000000000000000000000000000000000000000000
      0000ADADAD00ADADAD00ADADAD00B5B5B500B5B5B500DEDEDE0000000000184A
      6300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000639C00008C000000520000FFFFFF00000042000000
      4200000000000000000000000000000000004A4231004A4231005A5239006B5A
      42007B7363008C7B6B00948C7B00948C7B00000000000000000000000000B5B5
      B500E7E7E7000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000188C100018AD0000088408000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009C9C9C00ADADAD00ADADAD00ADADAD00ADADAD00B5B5B500B5B5B500DEDE
      DE00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000420000004200000042000000000000004200009C
      CE0000004200000000000000420000000000000000004A4231005A5239006B5A
      42007B7363008C7B6B00948C7B009C94840000000000000000007B7B7B00ADAD
      AD00CECECE00E7E7E70000000000000000000000000063B5630000840000217B
      2100000000000000000000000000007B00000084000008940000008400000073
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C9C9C00ADADAD00ADADAD00ADADAD00ADADAD00B5B5
      B5004A639C000000000031426300000000000000000000000000000000005252
      5200525252000000000000000000000000000000000000639C00009CCE000000
      000000639C0000000000000042000000000000000000000000005A5239005A52
      39008C7B6B00948C7B009C94840000000000000000000000000000000000ADAD
      AD00E7E7E70000000000000000000000000000000000005A0000005A0000086B
      00002963210000000000189C0800189C080010841800088C0000189C0800189C
      0800189C08000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00ADADAD00ADADAD003131
      3100314263000000000031426300000000000000000000000000000000000000
      0000000000000000420000639C0000639C00000000000000000000639C00009C
      CE00009CCE00009CCE00009CCE00000000000000000000000000000000005A52
      3900948C7B009C9484006B6B6B0000000000000000000000000094949400C6C6
      C60000000000000000000000000000000000000000005A735A0000390000084A
      000000390000214A21000000000000000000219C100021C6000029D608000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006B6B6B004A63
      9C00000000000000000039528400000000000000000000000000000000000000
      0000000000000000000000639C0000639C0000639C0031639C00009CCE00009C
      CE00009CCE000000000000000000000000000000000000000000000000000000
      00009C9484000000000000000000949494006B6B6B00ADADAD00C6C6C6009C9C
      9C00000000000000000000000000000000000000000000000000638463000039
      000008520000104A000000730000089C000021C6000021CE0000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000394A7B00394A7B0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000639C00009CCE00009CCE003163
      9C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B7B7B0094949400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000313131004A4A4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C600C6C6C60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000212121004A4A
      4A0063636300848484006B6B6B0052525200393939000000000000C6EF003131
      3100313131004A4A4A0000000000000000000000000000000000000000000000
      00000000000000000000000000008C8C8C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008400000084848400848484008484840084000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000737373007373
      7300000000000000000000000000000000000000000000000000313131005252
      52006B6B6B00848484006B6B6B00525252002121210000000000848484004242
      42004A4A4A000000000000000000000000000000000000000000000000000000
      000000000000FF313100FF9C9C00FF3131008C8C8C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000840000008400000084000000840000008400000084000000840000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000073737300ADADAD00ADADAD00ADAD
      AD00ADADAD0073737300000000000000000000000000000000004A4A4A005252
      52006B6B6B00848484006B6B6B00525252000000000000BDE700424242003131
      310000000000000000004A4A4A0000000000000000000000000000000000FF31
      3100FF9C9C00FFCECE00FF313100FFCECE00FF3131008C8C8C00000000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C6008400000084000000FFFF000084000000840000008400000084000000C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADADAD008C8C8C00525252005252
      52008C8C8C00ADADAD00000000000000000000000000000000004A4A4A005252
      52006B6B6B00848484006B6B6B00525252000000000000A5C600525252004A4A
      4A0052525200313131002121210000000000FF313100FF9C9C00FF9C9C00FFCE
      CE00FF31310000000000FF31310000000000FFCECE00FF313100000000008C8C
      8C0000000000000000000000000000000000000000000000000000000000C6C6
      C60084000000FFFF0000FFFF000084000000840000008400000084000000C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000073737300ADADAD0052525200BDBDBD00BDBD
      BD0052525200ADADAD00737373000000000000000000000000004A4A4A005252
      52006B6B6B00848484006B6B6B00525252000000000000000000636363006363
      630052525200000000006363630000000000FF313100FF313100FF3131000000
      0000FF636300FF313100FF313100FF31310000000000FFCECE00EF0000000000
      000000000000000000000000000000000000000000000000000000000000C6C6
      C600FF000000FFFF0000FFFF0000FF000000FF000000FF000000FF000000C6C6
      C600000000000000000000000000000000000000000000000000000000000000
      000073737300000000000000000073737300ADADAD0052525200BDBDBD00BDBD
      BD0052525200ADADAD00737373000000000000000000000000004A4A4A005252
      52006B6B6B00848484006B6B6B00525252000000000000C6EF006B6B6B006B6B
      6B005252520031313100000000000000000000000000FFCECE00FF9C9C00FF9C
      9C00FF636300FF636300FF313100FF313100EF00000000000000EF0000000000
      00008C8C8C000000000000000000000000000000000000000000000000000000
      0000C6C6C600FF000000FFFF0000FF000000FF000000FF000000C6C6C6000000
      0000000000000000000000000000000000000000000073737300000000008C8C
      8C00ADADAD008C8C8C000000000073737300ADADAD008C8C8C00525252005252
      52008C8C8C00ADADAD00000000000000000000000000000000004A4A4A005252
      52006B6B6B00848484006B6B6B00525252002121210000A5C6006B6B6B006363
      63005252520039393900000000000000000000000000FFCECE00FF9C9C00FF9C
      9C00FF636300FF636300FF3131000000000000000000FF313100000000000000
      0000FF3131008C8C8C0000000000000000000000000000000000000000000000
      000000000000C6C6C600FFFFFF00C6C6C600C6C6C600C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000ADADAD008C8C
      8C00737373008C8C8C00ADADAD000000000073737300ADADAD00ADADAD00ADAD
      AD00ADADAD0073737300000000000000000000000000000000004A4A4A005252
      52006B6B6B00848484006B6B6B00525252003131310000000000525252006363
      6300525252003131310000000000000000000000000000000000FFCECE00FF9C
      9C00FF636300FF636300FF313100FF313100FF31310000FFFF00000000000000
      0000FFCECE00FF3131008C8C8C00000000000000000000000000000000000000
      00000000000000000000C6C6C600FFFFFF00C6C6C60000000000000000000000
      000000000000000000000000000000000000000000008C8C8C008C8C8C005252
      520000000000525252008C8C8C008C8C8C000000000000000000737373007373
      73000000000000000000000000000000000000000000000000004A4A4A003131
      310021212100000000000000000000000000313131000000000000C6EF005252
      52004A4A4A00000000000000000000000000000000000000000000000000FF9C
      9C00FF9C9C00FF636300FF313100FF313100FF313100EF000000EF000000EF00
      000000000000FFCECE00FF3131008C8C8C000000000000000000000000000000
      00000000000000000000C6C6C600FFFFFF00C6C6C60000000000000000000000
      00000000000000000000000000000000000073737300ADADAD00737373000000
      0000000000000000000073737300ADADAD007373730000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006363
      63007B7B7B007B7B7B00636363004A4A4A003939390031313100181818001818
      1800000000000000000000000000000000000000000000000000000000000000
      0000FF9C9C00FF636300FF636300FF313100FF313100EF000000EF000000EF00
      0000DE000000000000008C8C8C00000000000000000000000000000000000000
      00000000000000000000C6C6C600FFFFFF00C6C6C60000000000000000000000
      000000000000000000000000000000000000000000008C8C8C008C8C8C005252
      520000000000525252008C8C8C008C8C8C000000000000000000000000000000
      0000000000000000000000000000000000000000000018181800525252005252
      5200313131001818180018181800181818001818180031313100313131000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF636300FF636300FF313100FF313100FF313100EF000000EF00
      0000DE000000CE00000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600FFFFFF00C6C6C60000000000000000000000
      0000000000000000000000000000000000000000000000000000ADADAD008C8C
      8C00737373008C8C8C00ADADAD00000000000000000000000000000000000000
      000000000000000000000000000000000000000000001818180021212100A5A5
      A500E7E7E700C6C6C60000C6EF0000C6EF0000C6EF0000738C00181818000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FF636300FF313100FF313100FF313100EF0000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C6008484840000000000000000000000
      0000000000000000000000000000000000000000000073737300000000008C8C
      8C00ADADAD008C8C8C0000000000737373000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A5A5A500F7F7
      F700C6C6C600D6FFFF00DEDEDE0000C6EF0000C6EF0000A5C6004A4A4A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FF3131000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C6C6C600848484008484840084848400C6C6C600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000737373000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A5A5A500C6C6
      C600D6FFFF00D6FFFF00DEDEDE0029E7FF0000A5C600DEDEDE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B5B5
      B500D6FFFF00B5B5B500B5B5B500DEDEDE00DEDEDE00B5B5B500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000042424200000000004242420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007373730000000000000000000000
      0000000000000000000000000000000000000000310000003100000031000000
      3100000031000000310000003100000031000000310000003100000031000000
      3100000031000000310000003100000031000000000000000000000000000000
      00004242420000000000639C0000FFFFFF006363000000000000424242000000
      0000000000000000000000000000000000000000000000000000000000006373
      7300526B73005A73730063737B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000063CE000000000042424200525252000000
      000000000000000000000000000000000000009CFF000063CE000063CE00009C
      FF00009CFF00009CFF00009CFF00009CFF00009CFF00009CFF00009CFF00009C
      FF00009CFF00009CFF00009CFF00000031000000000000000000424242000000
      0000639C0000FFFFFF00FFFFFF00CECECE00DEDEDE00EFEFEF00636300000000
      0000424242000000000000000000000000000000000000000000004A5A000094
      AD0039B5C600399CAD00319CB500006B7B00214A5A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000009CCE00009CCE000063CE0000319C0000319C00000000004242
      4200525252000000000000000000000000000000310000003100000031000000
      3100000031000000310000003100000031000000310000003100000031000000
      3100000031000000310000003100000031004242420000000000639C0000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00ADADAD00BDBDBD00CECECE00DEDEDE00EFEF
      EF006363000000000000424242000000000000000000086B730000CEE700BDFF
      FF00007B9400004263000063840052C6DE005AD6EF00317B940010636B000000
      000000000000000000000000000000000000000000000000000000000000009C
      CE00009CCE00009CCE00009CCE000063CE0000319C0000319C0000319C000031
      9C0000000000424242005252520000000000009CFF0000316300003163003163
      9C0031639C000031630000316300003163000031630031639C0031639C000031
      63000031630031639C000000310000003100639C0000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00737373008C8C8C00ADADAD00BDBDBD00CECE
      CE00DEDEDE00EFEFEF0063630000000000000000000000637B0052FFFF000042
      5200000000000008180000214200005A840000FFFF007BFFFF00A5FFFF007B8C
      8C000000000000000000000000000000000000000000009CCE00009CCE00009C
      CE00009CCE00009CCE00009CCE000063CE0000319C0000319C0000319C000031
      9C0000319C0000319C000000000052525200009CFF0031639C00639CCE003163
      9C0031639C00639CCE00639CCE0031639C0031639C0031639C0031639C003163
      9C00639CCE0031639C000000310000003100639C0000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00DEDEDE0063630000639C000031310000737373008C8C8C00ADAD
      AD00BDBDBD00CECECE0000000000000000006B8C8C0000B5D600ADF7FF008C73
      73000000000000000000000000005A7B7B00007B8C0094FFFF00FFFFFF00FFFF
      FF0031848C00000000000000000000000000009CCE00009CCE00009CCE00009C
      CE00009CCE00009CCE0063FFFF00009CFF0063CEFF0000319C0000319C000031
      9C0000319C0000319C000000000000000000009CFF0000316300003163000031
      630000316300003163000031630000003100000031000031630031639C000031
      63000031630000316300000031000000310063630000FFFFFF00FFFFFF00DEDE
      DE0063630000639C0000639C0000639C0000639C9C00639C0000313100007373
      73008C8C8C00ADADAD0000000000000000006B7373008CE7F7008CFFFF006B8C
      8C0000000000000000000000000000000000000000009C9494004AF7FF0039E7
      FF008CFFFF00186373000000000000000000009CCE00009CCE00009CCE00009C
      CE0063FFFF00009CFF003100FF009C63FF000000BD00009CFF0063CEFF000031
      9C0000319C0000319C0000000000000000009CFFFF0031639C0031639C003163
      9C00639CCE00639CCE00000031000000310000319C0031639C00639CCE003163
      9C0031639C00639CCE000000310000003100000000006331000063630000639C
      0000639C0000639C9C00639C0000639C0000639C9C00639C0000639C0000639C
      00003131000073737300000000000000000094948C0000D6EF00007B84005AA5
      AD00A59494000000000000000000000000000000000000000000005A6B0000A5
      CE007BFFFF00216B7B000000000000000000009CCE00009CCE0063FFFF00009C
      FF000042000000AD00009C63FF003100FF003100FF000000DE003100FF00009C
      FF0063CEFF0000319C0000000000000000009CFFFF0000316300003163003163
      9C0000316300003163000000310000CEFF000063CE0000316300003163003163
      9C000031630000316300000031000000310000000000000000000000000063CE
      31009C9C6300639C0000639C0000639C0000639C9C00639C0000639C00006363
      0000639C0000639C000000000000000000000000000000425A007B9CA5008CC6
      CE00217B8C00BDB5B50000000000000000000000000000000000000000000084
      940000F7FF004ABDD600426363000000000063FFFF00009CFF00000052000000
      520000AD000000AD00009C63FF00CECEFF003100FF009C63FF003100FF00FF00
      630000319C00009CFF0000000000000000000000310000003100000031000000
      3100000031000000310000003100009CFF000000310000003100000031000000
      3100000031000000310000003100000031000000000000000000000000000000
      00000000000063CE31009C9C6300639C0000639C000063630000636300006363
      0000639C000063630000000000000000000000000000007B8C0000849400846B
      6B0000424A005A5A5A0000526300000000000000000000000000000000006373
      7B00002952004AC6DE00005A63000000000000000000009CFF00000052000042
      000000AD0000CEFFCE0063FF31009C63FF009C63FF009C31FF009C3131009C00
      0000000000000000000000000000000000000000310000003100000031000000
      31000000310000003100000031009CFFFF000000310000003100000031000000
      3100000031000000310000003100000031000000000000000000000000000000
      000000000000000000000000000063CE31009C9C630063630000639C9C006363
      00000000000000000000000000000000000000000000000000000094AD00008C
      9400AD948C00FFFFFF000052630000526300000000000000000000000000737B
      7B000000000063BDD600006B7B000000000000000000000000000000000000AD
      000000EF000000EF000063FF310000319C0000319C00CECE63009C313100CE31
      6300FF006300000000000000000000000000009CFF0000003100003163003163
      9C0031639C000031630000316300000031000000310000316300003163000031
      63000031630000316300009CFF00000031000000000000000000000000000000
      0000000000000000000000000000000000000000000063CE3100000000000000
      0000000000000000000000000000000000000000000000000000005A63009CD6
      DE00FFFFFF00FFFFFF00FFFFFF000000080052636B00949494009C8C8C003939
      4200397B8C00ADFFFF00005263000000000000000000000000000042000000AD
      0000CEFFCE0063FF310000AD0000009CFF0000000000FF0063009C0000009C31
      31009C0000000000000000000000000000009CFFFF000000310031639C00639C
      CE0031639C0031639C0031639C0031639C0031639C0031639C0031639C00639C
      CE0031639C0031639C0000CEFF00000031000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000052
      6300FFFFFF00FFFFFF00D6CECE005A5A5A00294A52007B84840042C6D60042FF
      FF0063EFFF00009CC600637B840000000000008C00000000000000AD000000EF
      000000EF000063FF310000000000000000000000000000000000CECE63009C31
      3100FF316300FF00630000000000000000000000310000003100000031000000
      3100000031000000310000003100000031000000310000003100000031000000
      3100000031000000310000003100000031000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000526300A59494000000080000526300316B6B00314A5200007B9C00008C
      B50000426B0018636B000000000000000000000000000000000000EF0000CEFF
      CE00CEFFCE0000AD000000000000000000000000000000000000FF0063009C00
      0000FFFFCE00FF00630000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000736B6B004A737B00005A730000394A000008180000213900636B
      6B0000000000000000000000000000000000000000000000000000AD000063FF
      310000AD0000000000000000000000000000000000000000000000000000FF00
      6300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000EF00000000
      00000000000000000000000000000000000000000000000000009C0031000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000002939000000000000000000000000000000
      0000000000000000000000000000000000004AF729007BE71800B5D61800F7AD
      1800FF8C3900FF7B4A00FF5A6B00FF317B00FF10A500C610C600A510D6005A10
      F7003910FF00394AFF00299CFF0018D6FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000031FF000063FF000063FF000031FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000213100008CBD0000CEFF0000000000000000000000
      00000000000000000000000000000000000039FF18007BE71800B5D61800F7BD
      1800FF9C2900FF7B4A00FF4A6B00FF219400FF10A500D610B500A510D6006B10
      F7003910FF00395AFF00298CFF0018D6FF000000000000009400000094000000
      BD000000FF000000FF000000000000000000000000000000000052FF0000B5FF
      B5008CFF8C004ADE000000000000000000000000000000000000000000000031
      FF00009CFF0000CEFF0000CEFF00009CFF000031FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000638400003963000073940000000000000000000000
      00000000000000000000000000000000000039FF18007BE71800B5D61800F7AD
      1800FF9C2900FF7B3900FF4A6B00FF317B00FF10A500D610B500A510D6005A10
      F7003910FF00295AFF00298CFF0018D6FF000000000000009400000094000000
      BD000000FF000000FF006B6BFF000000000000000000319400004ADE00008CFF
      8C0052FF000000BD000000840000000000000000000000000000000000000063
      FF0000CEFF000031FF000031FF00009CFF000063FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000427B00B5E794000042390000315200000000000000
      0000000000000000000000000000000000004AF718007BE71800B5D61800F7BD
      1800FF8C3900FF7B4A00FF5A6B00FF219400FF10A500C610C600A510D6006B10
      E7003910FF00394AFF00299CFF0018D6FF000000000000009400000094000000
      BD000000FF000000FF006B6BFF004A4AFF00000000000084000000BD000052FF
      000052FF000000BD000000840000000000000000000000000000000000000063
      FF0000CEFF0000CEFF000063FF009C000000FF000000FF000000FF0000009C00
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000004A840031D63900A5FF9C0000B50000005A5A0000395A000000
      0000000000000000000000000000000000004AF718007BE71800B5D61800F7AD
      1800FF8C3900FF7B4A00FF4A6B00FF317B00F710B500D600C600A510D6005A10
      F7003910FF00394AFF00298CFF0018D6FF000000000000009400000094000000
      BD000000FF000000FF006B6BFF004A4AFF000000000000840000319400004ADE
      00004ADE00003194000000840000000000000000000000000000000000000031
      FF000063FF000063FF00FF000000FF310000CE000000CE000000CE000000FF31
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000004A8C008CD6A5006BFF9C00A5CE9C0052CE520000B5000000525A000039
      5A000000000000000000000000000000000039FF18007BE71800B5D61800F7AD
      1800FF9C2900FF7B4A00FF4A6B00FF317B00FF10A500D610B500A510D6005A10
      F7003910FF00295AFF00299CFF0018D6FF000000000000000000000073000000
      730000007300000084006B6BFF004A4AFF000000000000000000319400004ADE
      00004ADE00003194000000000000000000000000000000000000000000000000
      0000000000009C000000FF310000CE000000FF000000FF000000FF000000CE00
      0000FF3100009C00000000000000000000000000000000213100006384000042
      840000D6000021FF4200B5F7AD0000AD00005A845A005AAD5A00007B0000004A
      4A000021310000D6FF00000000000000000039FF18007BE71800B5D61800F7BD
      1800FF9C2900FF7B4A00FF4A6B00FF219400FF10A500D610B500A510E7007B00
      F7003910FF00395AFF00299CFF0018D6FF000000000000000000000000000000
      52000000730000007300000073004A4AFF0000000000000000003194000000BD
      000000BD00003194000000000000000000000000000000000000000000000000
      000000000000FF000000CE000000FF00000052000000FF00000052000000FF00
      0000CE000000FF000000000000000000000000293900008CBD0000396300B5E7
      9400A5FFA500BDFFCE0000A50000009C0000004A00003163310094CE94009CD6
      8C00527B8C000063730029F7FF00000000004AF718007BE71800B5D61800F7AD
      1800FF9C2900FF7B4A00FF4A6B00FF317B00FF10A500C610C600A510D6005A10
      F7003910FF00295AFF00298CFF0018D6FF000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000084000000BD
      000000BD00000084000000000000000000000000000000000000000000000000
      000000000000FF000000CE000000FF000000FF000000FF000000FF000000FF00
      0000CE000000FF00000000000000000000000000000000CEFF0000739400005A
      390000CE00004AD64A005A945A00007B000000180000DEFFDE00DEFFDE0094CE
      7B00088C8C0039F7F70008393100000000004AF729007BE71800B5D61800F7AD
      1800FF8C3900FF7B4A00FF5A6B00FF317B00F710B500C610C600A510D6005A10
      F7003910FF00394AFF00189CFF0018D6FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008400003194
      0000319400000084000000000000000000000000000000000000000073000000
      AD000000AD00FF000000CE000000FF00000052000000FF00000052000000FF00
      0000CE000000FF00000000000000000000000000000000000000000000000031
      5200005A5A0000D600005ABD5A00315A3100DEFFDE00DEFFDE0084C66B000894
      A500000000001052520000000000000000004AF729007BE71800B5D61800F7AD
      1800FF8C3900FF7B4A00FF5A6B00FF317B00F710B500C610C600A510D6005A10
      F7003910FF00394AFF00189CFF0018D6FF000000000000000000000000000000
      000084000000DE000000FF000000FF0000000000000000000000000000003194
      000031940000000000000000000000000000000000000000AD000000FF000000
      DE000000DE009C000000FF310000CE000000CE000000CE000000CE000000CE00
      0000FF3100009C00000000520000000000000000000000000000000000000000
      000000395A00006B5A000073000094C69400DEFFDE0084C66B000894A5000000
      0000000000000000000000000000000000004AF718007BE71800B5D61800F7BD
      1800FF8C3900FF7B4A00FF4A6B00FF219400FF10A500C610C600A510D6006B10
      F7003910FF00394AFF00298CFF0018D6FF000000000000000000000000008400
      0000DE000000FF000000FF000000FF0000000000000000000000000000003194
      000031940000000000000000000000000000000073000000FF000000FF000000
      8C00000052000000DE009C000000FF000000FF310000FF310000FF310000FF00
      00009C000000008C000000BD0000000000000000000000000000000000000000
      000000395A0000395A00004A4A009CEF8C0094CE7B000894A500006384000063
      84000000000000000000000000000000000039FF18007BE71800B5D61800F7AD
      1800FF9C2900FF7B3900FF4A6B00FF317B00FF10A500D610B500A510D6005A10
      F7003910FF00295AFF00299CFF0018D6FF00000000000000000000000000BD00
      0000FF000000FF6B6B00FFD6D600FF000000FF00000000000000000000000084
      000000840000000000000000000000000000000073000000FF000000FF000000
      8C00000052000000FF000000FF00000073009C0000009C0000009C000000008C
      0000008C0000008C000000BD000000000000000000000000000000000000007B
      A50000638400006384000021310052738C00088C8C00B5EFFF00003963000063
      8400007BA50000000000000000000000000039FF18007BE71800B5D61800F7BD
      1800FF9C2900FF7B4A00FF4A6B00FF219400FF10A500D610B500A510D6006B10
      F7003910FF00395AFF00299CFF0018D6FF000000000000000000000000008400
      0000DE000000FF6B6B00FF6B6B00FF000000FF00000000000000000000000084
      000000840000000000000000000000000000000000000000AD000000DE000000
      FF000000FF000000DE000000AD0000000000000000000052000000BD00000073
      00000073000000BD000000520000000000000000000000000000008CBD000000
      0000007BA5000000000000D6FF000063730039F7F7001052520000000000008C
      BD0000000000008CBD0000000000000000004AF718007BE71800B5D61800F7AD
      1800FF8C3900FF7B4A00FF4A6B00FF317B00F710B500D600C600A510D6005A10
      F7003910FF00394AFF00298CFF0018D6FF000000000000000000000000008400
      0000BD000000FF000000FF000000FF0000000000000000000000000000000000
      00000000000000000000000000000000000000000000000073000000AD000000
      FF000000FF000000AD0000007300000000000000000000000000000000000052
      000000520000000000000000000000000000000000000000000000638400007B
      A50000000000000000000000000029F7FF000839310000000000000000000000
      0000007BA5000063840000000000000000004AF718007BE71800B5D61800F7AD
      1800FF8C3900FF7B4A00FF5A6B00FF317B00FF10A500C610C600A510D6005A10
      F7003910FF00394AFF00299CFF0018D6FF000000000000000000000000000000
      000000000000BD000000FF000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      7300000073000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000063840000739400008CBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008CBD00007394000000000039FF18007BE71800B5D61800F7BD
      1800FF9C2900FF7B4A00FF4A6B00FF219400FF10A500D610B500A510D6006B10
      F7003910FF00395AFF00298CFF0018D6FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F7F7F700C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600E7E7E700E7E7E70000000000000000000000000000000000000000000000
      000000000000FFFFFF00BDBDBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DEDEDE00ADADAD00BDBDBD0000000000000000000000
      0000000000000000000000000000000000000000000000000000F7F7F700ADAD
      AD004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A009494
      9400949494009494940094949400ADADAD00000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00BDBDBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000003131FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEDEDE00DEDEDE00DEDEDE00ADADAD00BDBDBD00BDBDBD00BDBDBD000000
      000000000000000000000000000000000000000000000000000000000000ADAD
      AD0094949400ADADAD00ADADAD000000000000520000ADADAD00949494009494
      9400ADADAD0000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBDBD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000003131FF000000
      9C00000000000000000000000000000000000000000000000000DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00ADADAD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00000000000000000000000000000000000000000094949400C6C6
      C600E7E7E700CECECE000000000000BD000000000000CECECE00C6C6C600ADAD
      AD009494940000000000000000000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBDBD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      CE0000009C000000000000000000000000000000000000000000DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00ADADAD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00000000000000000000000000000000004A4A4A009C9C9C00CECE
      CE00E7E7E7000000000000BD0000009400000000000000000000E7E7E700C6C6
      C6009C9C9C004A4A4A0000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBDBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000042424200737373008C8C8C00525252000000
      00000000CE0000009C0000000000000000000000000000000000DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00ADADAD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00000000000000000000000000000000004A4A4A00ADADAD00CECE
      CE00F7F7F700000000000000000000BD00000000000000000000E7E7E700CECE
      CE009C9C9C004A4A4A000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00BDBD
      BD00000000000000000000000000000000000000000000000000000000000000
      000000009C00000000000000000042424200737373008C8C8C00525252002121
      21000000CE000000CE0000009C00000000000000000000000000DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00ADADAD00BDBDBD00ADADAD00ADADAD00BDBD
      BD00BDBDBD00000000000000000000000000000000004A4A4A009C9C9C00CECE
      CE00E7E7E7000000000000BD00000094000000BD0000F7F7F700E7E7E700CECE
      CE009C9C9C004A4A4A0000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00BDBDBD000000000000000000000000000000000000000000000000000000
      9C0000009C000000000000000000000000000000000000000000525252000000
      CE000000CE0000009C0000000000000000000000000000000000DEDEDE00DEDE
      DE00DEDEDE00CECECE00BDBDBD0031639C0031639C0031639C009C9C9C009C9C
      9C00ADADAD00000000000000000000000000000000004A4A4A00ADADAD00C6C6
      C600E7E7E7000000000000940000009400000000000000000000E7E7E700ADAD
      AD00949494004A4A4A0000000000000000000000000000000000000000000000
      000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000063CE00FFFFFF00FFFF
      FF00EFEFEF00000000000000000000000000000000000000000000009C000000
      CE000000000073737300000000000000000000000000000000003131FF000000
      CE0000009C000000000000000000000000000000000000000000DEDEDE00CECE
      CE00BDBDBD00BDBDBD0031639C00639CCE00639CCE009CCEFF009CCEFF003163
      9C009C9C9C00000000000000000000000000000000000000000094949400ADAD
      AD00E7E7E700F7F7F70000BD00000094000000BD0000E7E7E700CECECE00ADAD
      AD00949494000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000063CE00000000000063CE00FFFF
      FF00EFEFEF000000000000000000000000000000000000009C000000CE000000
      000042424200737373008C8C8C005252520021212100000000003131FF000000
      9C00000000000000000000000000000000000000000000000000BDBDBD00BDBD
      BD00EFEFEF0031639C0031639C00639CCE00639CCE00639CCE00639CCE009CCE
      FF009CCEFF000000000000000000000000000000000000000000313131009494
      9400CECECE00CECECE00CECECE0000BD000000520000C6C6C600ADADAD009494
      9400313131000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      000000000000000000000000000000000000000000003131FF000000CE000000
      CE0042424200737373008C8C8C00525252002121210000000000000000000000
      0000000000000000000000000000000000000000000000000000EFEFEF003163
      9C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639CCE00639C
      CE00639CCE000000000000000000000000000000000000000000000000004A4A
      4A0094949400ADADAD00ADADAD00ADADAD00ADADAD00ADADAD00949494004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000003131FF000000
      CE0000009C000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000031639C009CCEFF003163
      9C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639CCE009CCE
      FF009CCEFF0031639C0000000000000000000000000000000000000000000000
      0000000000004A4A4A004A4A4A004A4A4A004A4A4A004A4A4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008C8C8C000000000000000000000000000000000000000000000000003131
      FF0000009C000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009CCEFF009CCEFF009CCE
      FF009CCEFF0031639C0031639C00639CCE00639CCE009CCEFF009CCEFF009CCE
      FF009CCEFF009CCEFF0031639C00000000000000000000000000000000000000
      0000000000000000000094949400ADADAD004A4A4A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00003131FF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000009CCEFF009CCEFF009CCE
      FF009CCEFF009CCEFF009CCEFF00000000009CCEFF009CCEFF009CCEFF009CCE
      FF009CCEFF000000000000000000000000000000000000000000000000000000
      0000000000004A4A4A004A4A4A00F7F7F700F7F7F7004A4A4A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009CCE
      FF009CCEFF009CCEFF000000000000000000000000009CCEFF009CCEFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004A4A4A00ADADAD00E7E7E700ADADAD00E7E7E70094949400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008C8C8C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004A4A4A004A4A4A004A4A4A004A4A4A00949494004A4A4A004A4A4A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000630000008C000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF00DEDEDE00DEDEDE00DEDEDE00DEDEDE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000006300000063000000630000008C000000420000000000
      0000000000000000000000000000000000006B6B6B006B6B6B006B6B6B006B6B
      6B006B6B6B006B6B6B00DEDEDE00DEDEDE00DEDEDE00DEDEDE006B6B6B006B6B
      6B006B6B6B006B6B6B006B6B6B00000000000000000000000000000000000000
      00000000000000000000007BBD00003194000063940000319400003152000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B00B5B5B5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000063000000630000006300000063000000630000008C000000420000004200
      000000000000000000000000000000000000FFFFFF00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE0000000000000000000000000000000000007B
      BD00007BBD00006394000094DE00007BBD000094DE0000639400006394000031
      9400003152000000000000000000000000000000000000000000000000000000
      00008C8C8C008C8C8C00848484007B7B7B00E7E7E70000000000000000000000
      0000000000000000000000000000000000000000000000000000630000006300
      000063000000630000006300000063000000630000008C000000420000004200
      000042000000000000000000000000000000FFFFFF00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00000000002121
      2100DEDEDE00DEDEDE00DEDEDE00000000000000000000319400006394000094
      DE000094DE000094DE000094DE000094DE000063940000315200006394000063
      9400006394000031940000315200000000000000000000000000A5A5A5009C9C
      9C00949494008C8C8C00848484007B7B7B00B5B5B50000000000000000000000
      0000000000004A73000000000000000000000000000000000000630000006300
      000063000000424242008C8C8C00630000006300000073000000420000004200
      000042000000420000000000000000000000FFFFFF00DEDEDE00DEDEDE00DEDE
      DE00ADADAD006B6B6B006B6B6B006B6B6B00DEDEDE00DEDEDE00000000000000
      000021212100DEDEDE00DEDEDE000000000000000000007BBD000094DE000094
      DE000094DE000094DE000094DE00007BBD000094DE0000639400006394000063
      940000639400006394000031520000000000B5B5B500B5B5B500ADADAD009C9C
      9C009C9C9C008C8C8C0084848400848484004A7300004A7300004A7300004A73
      00004A7300004A7300004A730000000000000000000000000000630000006300
      0000630000004242420063000000630000006300000000639C00420000004200
      000042000000420000000000000000000000FFFFFF00DEDEDE00DEDEDE00ADAD
      AD006B6B6B00ADADAD00DEDEDE00DEDEDE00DEDEDE0039393900080808000000
      00000000000021212100DEDEDE0000000000000000006BC6FF000094DE000094
      DE000094DE000094DE000094DE000094DE000094DE0000639400006394000063
      94000063940000639400006394000000000000000000B5B5B500ADADAD00A5A5
      A5007BBD00007BBD00007BBD0000848484004A7300007BBD00007BBD00007BBD
      00007BBD00007BBD00007BBD00004A7300000000000000000000630000006300
      000063000000630000006300000063CE630000CECE008C0000008C0000004200
      000042000000420000000000000000000000FFFFFF00DEDEDE00DEDEDE003939
      390052525200DEDEDE0000000000DEDEDE000000000000000000393939002121
      210021212100DEDEDE00DEDEDE000000000000000000007BBD000094DE000094
      DE000094DE000094DE000094DE00007BBD000063940000315200006394000063
      94000063940000639400003152000000000000000000B5B5B500ADADAD00ADAD
      AD009C9C9C009C9C9C00949494004A7300007BBD00007BBD00007BBD00007BBD
      00007BBD00007BBD00007BBD0000000000000000000000000000630000006300
      000063000000FFCE9C00FFFF31008C0000008C00000063000000420000004200
      000042000000420000000000000000000000FFFFFF00DEDEDE00DEDEDE002121
      210039393900CECECE000000000000000000DEDEDE0000000000CECECE003939
      390021212100DEDEDE00DEDEDE000000000000000000007BBD000094DE000094
      DE000094DE000094DE000094DE00007BBD000094DE0000315200006394000063
      9400006394000063940000319400000000000000000000000000B5B5B500ADAD
      AD00A5A5A500A5A5A5009C9C9C00949494008C8C8C007B7B7B00B5B5B5000000
      0000000000004A73000000000000000000000000000000000000000000006300
      000063009C00DEDEDE008C8C8C00630000006300000073000000420000004200
      000042000000420000000000000000000000FFFFFF00DEDEDE00DEDEDE002121
      210021212100393939000000000000000000DEDEDE0000000000DEDEDE005252
      520039393900DEDEDE00DEDEDE0000000000000000006BC6FF000094DE000094
      DE000094DE00006394006BC6FF000094DE000094DE006BC6FF00006394000031
      9400006394000063940000319400000000000000000000000000BDBDBD00B5B5
      B500ADADAD00A5A5A500A5A5A5007B7B7B009C9C9C007B7B7B007B7B9400BDBD
      D600000000000000000000000000000000000000000000000000000000000000
      00008C0000008C8C8C008C8C8C00730000007300000073000000420000004200
      000042000000420000000000000000000000FFFFFF00DEDEDE00212121000000
      0000000000000808080039393900DEDEDE00DEDEDE00DEDEDE00ADADAD006B6B
      6B00ADADAD00DEDEDE00DEDEDE000000000000000000007BBD00006394006BC6
      FF006BC6FF000094DE00007BBD0000319400003194000031940000319400007B
      BD006BC6FF00006394000031940000000000000000000000000000000000BDBD
      BD00B5B5B5007B7B7B00A5A5A5007B7B7B00A5848400A58484007B7B94007B7B
      9400000000000000000000000000000000000000000000000000000000006300
      000063000000DEDEDE00730000008C0000008C0000008C000000730000004200
      000042000000420000000000000000000000FFFFFF00DEDEDE00DEDEDE002121
      21000000000000000000DEDEDE00DEDEDE006B6B6B006B6B6B006B6B6B00ADAD
      AD00DEDEDE00DEDEDE00DEDEDE000000000000000000000000000094DE006BC6
      FF000094DE0000319400007BBD00003194000031940000315200003194000094
      DE006BC6FF000000000000000000000000000000000000000000000000007B7B
      7B00BDBDBD007B7B7B00A5848400A5848400A5848400A5848400BDBDD6000000
      0000000000000000000000000000000000000000000000000000630000007300
      0000730000008C0000008C0000008C0000007300000073000000520000004200
      000000000000000000000000000000000000FFFFFF00DEDEDE00DEDEDE00DEDE
      DE002121210000000000DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00000000000000000000000000000000000000
      00000000000000000000000000000094DE006BC6FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A5848400A5848400A5848400A5848400EFC6C60000000000000000000000
      0000000000000000000000000000000000000000000000000000730000008C00
      00008C0000007300000052000000520000004200000000000000000000000000
      000000000000000000000000000000000000FFFFFF00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A5848400A5848400EFC6C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000005200
      0000520000004200000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00DEDEDE00DEDEDE00DEDE
      DE0000000000000000000000000000000000000000000000000000000000FFFF
      FF00DEDEDE00DEDEDE00DEDEDE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004242
      4200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000212121004242420042424200000000000000000000000000D6A5
      9C00CE9C8C00C69C8C00C6948C00C6948C00C6948C00C6948C00C6948C00C694
      8C00C6948C00C6948C0063636300000000000000000000000000000000000000
      0000000000000000000000000000000000002121210010101000313163000031
      3100101010005252520000000000000000000000000000000000000000000000
      0000000000000000000000000000003163000031630000313100000000000000
      0000000000000000000000000000000000000000100000000000737373007373
      7300737373008C8C8C008C8C8C008C8C8C00737373008C8C8C00737373000000
      000000000000000000002121210042424200000000000000000000000000E7E7
      E700FFDECE00FFDEC600FFDEC600FFDEBD00FFDEBD00FFD6BD00FFD6B500FFD6
      B500FFD6AD00FFD6BD0063636300000000000000000000000000000000000000
      0000000000000000000021212100003131003131630031639C00316363003131
      3100000042000031310010101000000000000000000000000000000000000000
      0000000000007373730000316300003163000031630031639C0000319C000000
      0000000000000000000000000000000000000000000000000000737373007373
      73008C8C8C000000EF000000AD008C8C8C008C8C8C008C8C8C00737373000000
      000073737300000000000000000000000000000000000000000000000000E7E7
      E700FFE7CE00FFE7CE00FFDEC600FFDEC600FFDEC600FFDEBD00FFD6BD00FFD6
      B500FFD6B500FFD6BD0063636300000000000000000000000000000000000000
      0000003131002121210031639C0063639C0031639C003163630063639C000031
      3100313131000031310000003100000000000000000000000000000000000000
      0000000000000031310000000000003163000031630000319C0000319C000031
      9C00003163000000000000000000000000000000000000000000737373008C8C
      8C008C8C8C000000EF008C8C8C008C8C8C008C8C8C008C8C8C008C8C8C000000
      000000000000000000000000100000000000000000000000000000000000E7E7
      E700FFE7CE00FFE7CE00FFE7CE00FFDEC600FFDEC600FFDEC600FFDEBD00FFDE
      BD00FFD6B500FFD6BD0063636300000000000000000000000000003131003131
      3100316363006363CE0031639C00319CCE005252520031639C0031639C003131
      6300003163003131630000002100000000000000000000000000000000000000
      000000000000000000000031630000003100000000000031630000319C000000
      00000000000000003100000010000000000000000000000000008C8C8C008C8C
      8C008C8C8C00FF9C0000FF9C0000FF9C0000FF9C0000FF9C0000FF6300007373
      730073737300737373000000000000000000000000000000000000000000EFEF
      EF00FFE7D600FFE7D600FFE7CE00FFE7CE00FFDEC600FFDEC600FFDEC600FFDE
      BD00FFDEBD00FFD6BD006363630000000000000000000000000063639C00639C
      CE00639CCE00639C9C0063639C0031639C00319CCE0063639C0031639C004242
      4200313163000063630031003100000000000000000000000000000000000000
      0000000000000000210000000000000021000000100000003100000031000000
      00000000000000000000003163000000000000000000000000008C8C8C008C8C
      8C0073737300FF9C0000FF9C0000FF9C0000FF9C0000FF9C0000FF6300008C8C
      8C008C8C8C00000000000000000000000000000000000000000000000000EFEF
      EF00FFEFDE00FFE7D600FFE7D600FFE7CE00FFE7CE00FFE7CE00FFDEC600FFDE
      C600FFDEC600FFD6BD0063636300000000000000000010101000639CCE00639C
      CE00639C9C006363CE00319CCE0031639C0063639C0031639C00639C9C000031
      630031639C003131630000313100000000000000000000000000000000000000
      0000000000000000000000316300009CCE0000319C000063CE00000031000000
      00000031310000316300003163000000000000000000000000008C8C8C008C8C
      8C008C8C8C00FF9C0000FF9C0000FF9C0000FF9C0000FF9C000094009C009400
      9C008C8C8C00737373000000000000000000000000000000000000000000EFEF
      EF00FFEFDE00FFEFDE00FFE7D600FFE7D600FFE7D600FFE7CE00FFE7CE00FFDE
      C600FFDEC600FFD6BD0063636300000000000000000010101000639CCE00639C
      CE00639CCE00639C9C006363CE00639CCE0031639C0031639C00639C9C003131
      6300316363003131630000316300000000000000000000313100000000000000
      210000000000000000000031310000316300009CCE00009CFF00009CFF000031
      3100000021000000000000003100000000000000000000000000737373008C8C
      8C0000FFFF0000FFFF008C8C8C00737373007373730073737300CE31CE00CE31
      CE008C8C8C00000000000000000000000000000000000000000000000000D6D6
      D600F7E7D600FFEFDE00FFEFDE00FFEFDE00FFE7D600FFE7D600FFE7CE00FFE7
      CE00FFDECE00FFD6BD00636363000000000000000000000010009C9CCE00639C
      CE00639CCE00639CCE009C9CCE00319C9C00639CCE00639CCE0063639C003131
      63003131630031639C0021212100000000000000000000316300000000000031
      310000313100000000000000000000002100003163000063CE0000CEFF000063
      CE000031630000316300000010000000000000000000000000007373730000CE
      FF0063FFFF0063FFFF0000CEFF00737373007373730073737300CE31CE00CE31
      CE008C8C8C0073737300000000000000000000000000DEDEDE00EFEFEF00184A
      F700C6BDB500D6CEBD00E7DECE00FFEFDE00FFEFDE00FFE7D600FFE7D600FFE7
      D600FFE7CE00FFD6BD00636363000000000000000000101010009CCECE009C9C
      CE009CCECE009C9CCE00639CCE009C9CCE0063639C0031636300316363003131
      6300316363003131630000313100000000000031630000000000003131000031
      3100003131000031310000313100000000000000000000313100003163000000
      3100000010000000000000000000000000000000000000000000737373007373
      730000CEFF0000CEFF0073737300737373007373730073737300CE31CE00CE31
      CE008C8C8C00000000000000000000000000FFFFFF002142F700737373000031
      FF008C848C000029EF00CEC6BD00FFEFDE00FFEFDE00FFEFDE00FFEFD600FFE7
      D600FFE7D600FFD6BD0063636300000000000000000021212100639CCE00CECE
      FF009CCECE009C9CFF00639C9C0063639C0031639C0031636300313163003163
      9C00313163000031310031313100000000000031630000002100003131000031
      3100003131000031310000313100003131000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008C8C8C008C8C
      8C008C8C8C00737373008C8C8C008C8C8C008C8C8C0073737300737373007373
      73008C8C8C00737373000000000000000000C6C6C600316BEF00008CF70000D6
      FF000042FF00524A5200C6BDB500EFDED600FFEFE700FFEFDE00FFEFDE00FFEF
      DE00FFE7D600FFD6BD0063636300000000000000000010101000CECEFF009CCE
      CE009CCECE00639C9C0063639C0031639C0063639C0063639C00316363000000
      2100313131000000000000000000000000000000000000002100003131000031
      3100003131000031310000313100003131000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000029FF000031FF0000CEFF008CFF
      FF0000CEFF000031FF000031E700DED6CE00FFF7E700FFEFE700FFEFDE00FFEF
      DE00FFEFDE00FFD6BD0063636300000000000000000031313100639C9C009C9C
      CE00ADADAD006363CE009C9CCE009C9C9C00639C9C00639CCE00313131003131
      3100000000000000000000000000000000000000000000000000000000000031
      3100003131000031310000313100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084849C0000D6FF0000CE
      FF000084F7006B6B6B00E7DEDE00FFF7EF00FFF7EF00FFF7E700FFEFE700E7B5
      9400E7943900D684310063636300000000000000000000000000737373003131
      310063639C0063CECE00639CCE0063639C004242420031313100000000000000
      000000000000000000000000000000000000000000000000000000000000BDBD
      BD00000031000000000000003100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000031FF00A59CAD000031
      FF003952D6000031F700DEDED600FFF7F700FFF7EF00FFF7EF00FFF7E700EFC6
      AD00FFB552006363630000000000000000000000000000000000000000000000
      0000636363003131310021212100525252000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000010000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000031
      FF00DEDEDE00F7FFF700F7F7F700EFF7F700EFEFEF00EFEFEF00E7E7E700EFD6
      B500636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000AD847B00B58C
      7B00B58C7B00B58C7B00B58C7B00B58C7B00B58C7B00B58C7B00B58C7B00B58C
      7B00B58C7B00B58C7B00B58C7B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000AD847B00FFE7
      CE00F7E7CE00F7E7C600F7E7BD00F7DEBD00F7DEB500F7DEB500F7DEAD00F7D6
      AD00F7D6A500F7D6A500B58C7B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000004A000000B50000006300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000003163009CCEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5847B00FFEF
      D600FFEFCE00F7E7CE00F7E7C600009C0000F7DEBD00F7DEB500F7DEB500F7DE
      AD00F7D6AD00F7D6A500B58C7B00000000000000000000000000000000000000
      0000000000000000000000000000000042000808FF000000E70000008C000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000C6000000390000009C0000009C000000A5000000AD0000008C00000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000003163009CCEFF00639CCE009CCEFF0031639C00639CCE00000000000000
      0000000000000000000000000000000000000000000000000000B58C7B00FFEF
      DE00FFEFD600F7EFCE00009C0000009C0000009C0000F7DEBD00F7DEBD00F7DE
      B500F7DEAD00F7D6AD00B58C7B00000000000000000000000000000000000000
      00000000AD000808FF000808FF000808FF000808FF000000BD0000007B000000
      FF000000000000000000000000000000000000000000000000000000000000A5
      0000008C0000005A000000A500000000000010211000005A000000A5000000A5
      0000007300000094000000210000000000000000000000000000003163009CCE
      FF00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000BD8C7B00FFEF
      DE00FFEFDE00009C00004A9442007BAD6300009C00006B9C5200F7DEBD00F7DE
      BD00F7DEB500F7DEB500B58C7B0000000000000000000800FF000808FF000800
      FF000000FF000000AD00000000001010000000009C000808FF00000073000000
      E7000000BD00000000000000000000000000000000000000000000DE000000D6
      0000008C00002110210000000000635A6300BDB5BD00BDBDBD00CEC6CE003921
      3900003900000094000000840000000000000000000031639C00639CCE003163
      6300639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C00639CCE0000000000000000000000000000000000BD8C7B00FFF7
      E700FFF7DE00009C0000F7EFD600FFEFD600BDCE9C00009C0000009C0000F7DE
      BD00F7DEBD00F7DEB500B58C7B0000000000000000004A63FF000000FF005252
      2900CECEBD00BDBDB500B5B5B500BDBDBD000000000000000000525231000000
      D6000000D6000000730000000000000000000000000000BD000000D6000000A5
      0000CEBDCE00B5B5B500BDBDBD00B5B5B500B5B5B500B5B5B500B5B5B500BDBD
      BD007363730000AD0000009C0000000000000000000031639C00639C9C009CCE
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C00639CCE0000000000000000000000000000000000C6947B00FFF7
      EF00FFF7E700FFF7DE00FFEFDE00FFEFD600FFEFD600EFE7C600009C0000009C
      0000F7E7BD00F7DEBD00B58C7B000000000000003100A5D6FF00000021004A4A
      4200BDBDBD00B5B5B500B5B5B500B5B5B500BDBDB500B5B5B500BDBDB5007373
      63000000E7000000DE00000010000000000000AD000000D6000000D60000DECE
      DE00BDBDBD00B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500BDBDBD000000
      0000524A52000052000000B50000000000000000000031639C00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C00639CCE0000000000000000000000000000000000C6947B00FFF7
      EF00FFF7EF00FFF7E700FFF7E700FFEFDE00FFEFD600FFEFD600FFEFCE00F7E7
      CE00F7E7C600F7E7BD00B58C7B000000000000005A0000002100292921004A4A
      4A0031313100BDBDBD00B5B5B500B5B5B500B5B5B500B5B5B500B5B5B500C6C6
      BD0084846B000000DE000000E7000000000031FF310000CE00007B6B7B000808
      0800000000004A4A4A00C6C6C600BDBDBD00B5B5B500C6C6C60000000000CECE
      CE00F7F7F700FFFFFF00FFFFFF00000000000000000031639C00639CCE00639C
      CE0031639C00CEFFFF00003163009CCEFF00003163009CCEFF00003163003163
      9C0031639C00639CCE0000000000000000000000000000000000CE9C7B00FFFF
      F700DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C
      3900DE9C3900F7E7C600B58C7B0000000000000000006B6B63007B7B7B007B7B
      7B006B6B6B0000000000D6D6CE0000000000CECECE006B6B6B00000000000000
      0000A5A59C00A5A584000000E7000000000000FF0000A58CA5009C949C009C9C
      9C008C8C8C00B5B5B5008C848C00181818000000000000000000FFFFFF00EFEF
      EF00FFFFFF006363630000000000000000000000000031639C0031639C00CEFF
      FF009CCEFF009CCEFF0031639C0031639C00639CCE00639CCE009CCEFF009CCE
      FF0000316300639CCE0000000000000000000000000000000000CE9C8400FFFF
      F700FFFFF700FFF7EF00FFF7EF00FFF7E700FFF7E700FFF7DE00FFEFDE00FFEF
      D600FFEFD600F7E7CE00B58C7B00000000000000000000000000080808008484
      84007B7B7B008C8C8400000000000000730000000000FFFFFF00FFFFFF00EFEF
      EF00EFEFEF00F7F7F700A5A5A50000000000100810004A4A4A009C9C9C00A5A5
      A5004A4A4A0000840000006300000000000042394200736B7300F7F7F700E7E7
      E7000000000000000000000000000000000000316300CEFFFF009CCEFF009CCE
      FF0031639C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639C
      CE009CCEFF009CCEFF0000000000000000000000000000000000D69C8400FFFF
      FF00FFFFF700FFFFF700FFF7EF00FFF7EF00FFF7E700FFF7E700FFEFDE00FFEF
      DE00FFEFD600FFEFD600B58C7B00000000000000000000000000000000000000
      000084848400080800000000FF0000009C000000F70000000000D6D6D600FFFF
      FF00BDBDBD004A4A4A0000000000000000000000000000000000000000000000
      00000000000000AD000000D600000052000000630000847B8400000000000000
      000000000000000000000000000000000000003163009CCEFF0031639C003163
      9C0031639C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639C
      CE00639CCE00639CCE0000000000000000000000000000000000D6A58400FFFF
      FF00DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C
      3900DE9C3900FFEFD600B58C7B00000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000D6000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000210000009C000000CE000000BD0000008C000000000000000000000000
      00000000000000000000000000000000000000316300639CCE00639CCE003163
      9C0031639C0031639C0031639C0031639C00639CCE00639CCE00639CCE00639C
      CE00CEFFFF009CCEFF0000316300000000000000000000000000DEA58400FFFF
      FF00FFFFFF00FFFFFF00FFFFF700FFFFF700FFFFF700FFF7EF00FFF7EF00FFF7
      E700FFF7E700FFEFDE00B58C7B00000000000000000000000000000000000000
      000000004A00000042000000FF000000FF0000007B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000100000CEFFCE0000CE000000CE000000DE000000B50000000000000000
      000000000000000000000000000000000000003163009CCEFF00639CCE00639C
      CE00639CCE0031639C0031639C0031639C00639CCE00639CCE00CEFFFF009CCE
      FF009CCEFF00639CCE0000316300000000000000000000000000DEA58400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFFFF700FFF7EF00FFF7
      EF00FFF7E700FFF7E700B58C7B00000000000000000000000000000000000000
      00000000000000002900739CFF000000FF000000840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000005200007BF77B0000CE000000DE000000100000000000000000
      0000000000000000000000000000000000000000000000000000003163009CCE
      FF00639CCE00639CCE00639CCE0031639C00CEFFFF009CCEFF009CCEFF00639C
      CE00003163000000000000000000000000000000000000000000DEA58400DEA5
      8400DEA58400DEA58400DEA58400DEA58400DEA58400DEA58400DEA58400DEA5
      8400DEA58400DEA58400DEA58400000000000000000000000000000000000000
      000000000000000039000000FF000000D6000000390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000BD000000DE00000010000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000003163009CCEFF00003163000000000000316300639CCE00003163000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000001800000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A5636B00A5636B00A5636B00A5636B00A563
      6B00A5636B00A5636B00A5636B00A5636B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A4A4A004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      000000215200004ABD00006BE700007BFF000073F700005AD6000042AD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C636300F7E7C600F7DEB500EFD6A500EFCE
      9400F7C68400EFC68400F7D68400A5636B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000636363004A4A4A009C9C9C00ADAD
      AD00525252008484840000000000000000000000000000000000000000000042
      BD00008CFF000084FF000084FF00008CFF000084FF000084FF000084FF00006B
      E700002163000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008C5A5A00DECEB500D6C6A500D6BD9C00DEBD
      9400E7C69400E7BD8400F7C68400A5636B000000000000000000000000000000
      0000000000000000000000630000006300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004A4A4A00CECECE00CECECE00ADAD
      AD00E7E7E7004242420000000000000000000000000000000000004ABD00008C
      FF00008CFF00008CFF00008CFF000031AD00007BF700008CFF00008CFF00008C
      FF000084FF0000184A0000000000000000000000000000000000000000000000
      000000000000000000000000000073525200A59C94009C948400A5947B00C6AD
      8C00DEBD9400E7C69400F7CE8400A5636B000000000000000000000000000000
      0000000000000063000018B5310018B531000063000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000052525200A5A5A500F7F7F700080808000000
      00009C9C9C00E7E7E7005A5A5A0000000000000000000039A500008CFF00008C
      FF00008CFF000094FF000031AD00FFFFFF009CB5DE000094FF00008CFF00008C
      FF00008CFF000063D60000000000000000000000000000000000000000000000
      00000000000000000000005AFF00524239006B6B6B00005AFF00736B5A00948C
      7300C6AD8C00DEBD9400EFCE9400A5636B000000000000000000000000000000
      00000063000031C64A0021B5390018B5290021B5310000630000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000393939006B6B6B00E7E7E700313131002121
      21009C9C9C00848484002929290000000000000000000073E700008CFF000094
      FF000094FF00004ABD00FFFFFF00FFFFFF00FFFFFF004A7BC600009CFF000094
      FF00008CFF00008CFF000029840000000000A56B6B00A56B6B00A56B6B00A563
      63008C5A5A006B424200005AFF00005AFF00005AFF00005AFF005A525200736B
      5A00A5947B00D6B59C00F7D6A500A5636B000000000000000000000000000063
      000042D66B0039CE5A0021AD39000063000018AD290018B53100006300000000
      0000000000000000000000000000000000000000000000000000000000005252
      520042424200000000004A4A4A005252520042424200FFFFFF00BDBDBD00ADAD
      AD00C6C6C600525252000000000000000000315284000094FF000094FF000094
      FF000063D600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00104AB500009C
      FF000094FF000094FF00004ABD0000000000A5636B00FFE7C600EFCEA500E7C6
      9400005AFF00005AFF00005AFF00005AFF00005AFF00005AFF00005AFF00005A
      FF009C948400DECEAD00EFDEB500A5636B000000000000000000006300004AD6
      7B0052DE7B0031B54A00006300000000000000630000109C210018AD31000063
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B00ADADAD0094949400CECECE004A4A4A0052525200393939005A5A5A008484
      840031313100525252000000000000000000316BBD000094FF00009CFF000084
      EF00F7F7FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000031
      AD00009CFF000094FF00006BDE0000000000A56B6B00FFEFD600F7D6B500EFCE
      A500CEAD8400005AFF00005AFF005242310073737300005AFF00005AFF006B6B
      6300A59C94008C5A52009C635A00A5636B000000000000000000000000000063
      000042C6630000630000CE6B6B00B539390000000000000000000063000018AD
      290000630000000000000000000000000000000000005252520042424200BDBD
      BD00E7E7E700FFFFFF00E7E7E700FFFFFF004242420052525200313131005252
      520000000000000000000000000000000000316BBD00009CFF00009CFF00BDCE
      E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000052C600009CFF00006BDE0000000000A5736B00FFF7E700F7DEC600EFD6
      B500D6B59400005AFF00005AFF005242310063636300005AFF00005AFF007373
      7300A5A59C008C5A5200DE944200BD734200000000000000000000000000B539
      390000630000CE6B6B00CE6B6B00CE6B6B00B539390000000000000000000063
      0000109C2100006300000000000000000000D6D6D60084848400B5B5B500F7F7
      F7005A5A5A00313131007B7B7B00E7E7E700EFEFEF0063636300000000000000
      00000000000000000000000000000000000031528C00009CFF00009CFF00A5DE
      FF00A5DEFF00E7F7FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A5E7FF00A5DE
      FF005AC6FF00009CFF00005ACE0000000000A5736B00FFFFF700F7E7D600EFDE
      C600005AFF00005AFF00005AFF00005AFF00005AFF00005AFF00005AFF00005A
      FF00C6BDBD0094635200C6846300000000000000000000000000B5393900DE94
      9400D6848400CE6B6B00B5393900CE636300CE6B6B00B5393900000000000000
      0000000000000063000000630000000000000000000010101000C6C6C600EFEF
      EF00000000000000000021212100C6C6C600CECECE0000000000000000000000
      000000000000000000000000000000000000000000000094F70000A5FF0008AD
      FF0018B5FF00B5E7FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0010B5FF0008AD
      FF0000A5FF0000A5FF000039AD0000000000BD846B00FFFFFF00FFEFE700F7E7
      D600EFDEC600DEC6A500005AFF00005AFF00005AFF00005AFF00D6946B00D694
      6B00D6946B009C635A00000000000000000000000000B5393900DE949400DE9C
      9C00D6737300B539390000000000B5393900CE5A5A00CE6B6B00B53939000000
      0000000000000000000000630000006300000000000042424200D6D6D600CECE
      CE00393939000000000042424200ADADAD00BDBDBD0042424200000000000000
      000000000000000000000000000000000000000000000042BD0000A5FF0010AD
      FF0021B5FF00BDE7FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0021B5FF0010B5
      FF0000A5FF000094F7000000000000000000BD846B00FFFFFF00FFFFF700FFF7
      E700F7E7D600EFDEC600005AFF00CEB594008C5A5A00005AFF00000000000000
      0000000000000000000000000000000000000000000000000000B5393900D684
      8400B539390000000000000000000000000000000000B5393900CE636300B539
      39000000000000000000000000000000000000000000A5A5A500B5B5B500FFFF
      FF00B5B5B500949494009C9C9C00BDBDBD00848484006B6B6B00000000000000
      0000000000000000000000000000000000000000000000000000007BDE0018B5
      FF0031BDFF0042C6FF005ACEFF0063D6FF005ACEFF004AC6FF0031BDFF0018B5
      FF0008ADFF00003194000000000000000000D6946B00FFFFFF00FFFFFF00FFFF
      F700FFEFE700F7E7D600A56B5A00A56B5A009C63630000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B539
      3900000000000000000000000000000000000000000000000000B5393900CE5A
      5A00B53939000000000000000000000000000000000000000000000000008484
      8400DEDEDE00CECECE00BDBDBD00525252000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000010000873
      D60031BDFF0042C6FF005ACEFF006BD6FF005AD6FF004AC6FF0031BDFF0018BD
      FF000039B500000000000000000000000000DE9C7300FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7D6CE00A56B5A00CE844200AD6B520000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5393900B539390000000000000000000000000000000000000000008484
      840063636300181818006B6B6B00636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000042BD00299CE70052D6FF005AD6FF0052CEFF0042C6FF00005AC6000021
      520000000000000000000000000000000000E7AD7B00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00E7D6DE00A56B5A00D68C5A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B5393900B5393900000000000000000000000000000000000000
      00008C8C8C00000000008C8C8C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B8CA5007B9CD6007B9CDE0000000000000000000000
      000000000000000000000000000000000000E7AD7B00D6946B00D6946B00D694
      6B00D6946B00D6946B00A56B5A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000004210000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF420000CE290000CE290000EF
      3900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000EF390000DE31000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000EF390000D63100009C180000F7
      4200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000009C0000006B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000DE310000DE3100005A000000F739000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000C6000000FF000000
      00000000000000000000000000000000000000CE310000CE310000B5210000B5
      2900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000000000000000000000000000
      000000000000000000000000420000000000000000000000000000C62100A5FF
      B50000CE310000CE310000A5180000C631000021080000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF0000009C000000
      FF00000000000000000000000000000000000063180000D6310000D631000063
      1800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000D6000000FF000000FF000000FF00000073000000E7000000FF000000
      FF000000FF000000FF0000008400000000000000000000DE3100E7FFDE0042DE
      630000CE290000CE310000D63100009C210000BD290000EF390000EF39000021
      08000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000D60000009C000000FF000000FF000000
      A5000000FF000000000000000000000000000000000000DE310000CE31000094
      210000000000000000000000000000000000008C210000000000000000000000
      0000000000000000000000000000000000000000000000000000000029000000
      FF000000FF000000FF000000FF000000FF000000FF000000BD000000C6000000
      A50000009C000000CE00000000000000000000000000000000000018000029EF
      5A009CF7AD0000CE290000D6310000210800000000000000000000DE310000DE
      310000BD290000000000000000000000000000000000000000006384FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      B5000000DE000000FF0000000000000000000000000000DE310000CE310000E7
      39000000000000000000000000000000000000E7390000EF3900000000000000
      000000000000000000000000000000000000000000000000FF00739CFF00526B
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      000000D62900BDFFC60000D631000000000000000000000000000000000000DE
      310000D63100007B18000000000000000000000000000000D600C6FFFF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      F70000008C000000FF000000210000000000000000000000000000E7390000D6
      31000008000000000000000000000000000000DE310000CE310000DE3100008C
      21000000000000000000000000000000000000000000000000000000FF00BDFF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000031080000EF39000000000000000000000000000000000000B5
      290000CE310000DE31000000000000000000000000000000FF00BDF7FF00526B
      FF005A7BFF002931FF002939FF000000FF000000FF000000FF000000FF000000
      B5000000FF0000000000000000000000000000000000000000000000000000EF
      390000E73900008C2100000000000000000063EF840000CE310000CE310000D6
      310000E739000000000000000000000000000000000000000000000000001018
      FF005A73FF000000FF000000DE00000000000000290000002900000084000000
      7B000000D60000007B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000063
      180000CE310000D631000042100000000000000000000000FF000000FF000000
      FF000000D600000084000000210000007B000000FF000000FF000000FF000000
      7300000000000000000000000000000000000000000000000000000000000000
      000000100000009C210000F7390000E73100A5FFAD0000CE310000CE310000CE
      310000DE310000A5290000000000000000000000000000000000000000000000
      00001821FF000000FF0000004200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000A5
      290000CE310000CE310000CE3100000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DEFFDE0000CE210000CE310000E7
      3900000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000E7
      310000CE310000CE310000EF3900000000000000000000000000000000000000
      00000000000000000000000000000000FF000000AD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000180000F7FFEF00E7FFDE0000CE29000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A5FF
      B50000CE290000CE310000E73900000000000000000000000000000000000000
      000000000000000000000000B500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000063100042F7630000310000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000420800FFFF
      F70073EF8C0039E7630000EF3900000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000007B18000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000E731000084
      1000005208000021000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00A5ADCE00BDC6C60000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A56384006B08390084395A0084315A0073104200AD738C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF005A84C60000CEFF0000BDFF009C9CC600FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000944A
      73008C426300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF008431
      5A00AD7394000000000000000000000000000000000000000000000000000000
      0000000000000000000084291800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF003173CE0008E7FF0000F7FF0000F7FF0000CEFF005273C600FFFF
      FF000000000000000000000000000000000000000000000000006B003100FFFF
      FF00FFFFFF00CEADBD0073184A00630029006300310084315200E7CED600FFFF
      FF00EFE7E7008C39630000000000000000000000000000000000000000000000
      00006B292100DEB5A500FFEFDE00AD4239000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE633100525252004242
      420000000000000000000000000000000000000000000000000000000000F7F7
      F7000063D60000C6FF0000D6FF0000D6FF0000DEFF0008D6FF0000C6FF002163
      D600FFFFFF00000000000000000000000000000000006B103900FFFFFF00FFFF
      FF005A0021007B294A006B0831006300310063003100630031007B294A007318
      4200FFFFFF00F7EFEF009C5A7B00000000000000000000000000000000008C31
      2900FFFFFF00FFEFE700C6847B00FFEFE700CE8C7B0039101000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000CE630000CE630000CE633100525252006331
      0000424242004242420000000000000000000000000000000000DEE7DE002973
      F70031A5F70021A5FF00009CFF0000A5FF0000A5FF0000A5FF0000A5FF0000A5
      FF000052E700F7F7EF000000000000000000DEC6D600E7D6DE00FFFFFF005A00
      210063002900FFF7F700FFFFFF005A002100630031009C5A7B00FFFFFF006B10
      39006B103900FFFFFF00AD7B94000000000000000000000000006B292100FFFF
      FF00FFFFFF00BD736300FFF7E700FFDED600BD6B5A00B56352008C3931000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FF9C3100CE9C3100CE633100CE633100CE633100525252006331
      00004242420042424200424242000000000000000000FFFFFF007B94EF008CA5
      EF0073B5F70084CEFF008CCEFF006BC6FF00189CFF000094FF000094FF001073
      EF006B94EF006B84EF00FFFFFF000000000073184200FFFFFF0084395A006300
      310063002900EFDEE700FFFFFF00C694AD0063002900FFFFFF00FFFFFF006300
      310063003100C69CAD00FFFFFF008C3963000000000000000000E7C6C600FFFF
      FF009C393100FFFFFF00DE9C9400EFC6B500EFC6B500F7CEBD00F7CEBD00F7CE
      BD00F7CEBD00F7CEBD009C312900000000000000000000000000000000000000
      0000FF9C3100CE9C3100CE9C3100CE633100CE630000CE633100525252006331
      000042424200000000000000000000000000000000000000000000000000FFFF
      FF007B9CDE00ADDEFF00ADE7FF009CDEFF008CDEFF006BCEFF0039B5FF00396B
      CE00FFFFFF00000000000000000000000000BD8CA500FFFFFF005A0021006300
      3100630031005A002900FFFFFF00FFFFFF00EFE7EF00FFFFFF00BD8CA5006300
      31006300310063003100FFFFFF007B21520000000000BD423100ADC6F700FFFF
      FF00F7EFEF00CE8C8C00FFE7D600FFDED600FFDED600FFDED600FFDED600FFDE
      D600FFDED600FFEFDE0008000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100CE9C3100FF9C3100FF9C0000CE633100424242006331
      000000000000000000000000000000000000000000000000000000000000CECE
      C60094ADD6008C9CBD008C9CB500C6EFFF00ADDEF7008CCEF7006BBDFF004A63
      AD00D6D6D600FFFFFF000000000000000000DECED600EFE7EF00630031006300
      310063003100630031009C638400FFFFFF00FFFFFF00FFF7F7005A0029006300
      3100630031005A002100FFFFFF0094426B00000000001039C6000042D6007B31
      4200FFFFFF009C392900FFEFE700FFEFE700FFEFE700FFEFE700FFEFE700FFEF
      E700FFEFE700D6948C0000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF633100CE633100525252006331
      0000000000000000000000000000000000000000000000000000FFFFFF00E721
      00007394DE00FF9C4A00A5634A00A58484008CADCE009C849400A5949C005A84
      D600E77B3100FFFFFF000000000000000000E7D6DE00DEC6D600630031006300
      3100630031006300310063002900FFFFFF00FFFFFF00BD94A500630031006300
      3100630031005A002900FFFFFF00944A6B009C635A002942AD000039DE00CE84
      7B00FFFFFF00CE948C00FFF7F700FFF7F700FFF7F700FFF7F700FFF7F700FFF7
      F700FFF7F700AD4A390000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100CE633100424242006331
      0000000000000000000000000000000000000000000000000000CEBDBD00FF5A
      18006B8CDE00FF8C390094A5C600BD4A2100948C94007BADDE00BDAD9C005A73
      DE00FFAD5200DEE7E7000000000000000000D6B5C600FFF7FF00630029006300
      31006300310063002900EFDEE700FFFFFF00FFFFFF00FFFFFF006B1039006300
      3100630031005A002100FFFFFF0084396300000000001000000084425A00FFEF
      EF00FFFFFF00E7CECE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009C39290000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C3100CE633100525252006331
      0000000000000000000000000000000000000000000000000000B57B7300FF4A
      210094B5EF009C421000BDCEEF008C849C0084A5CE0084B5E700CEAD84004A73
      DE00FFC66300DEC6BD00000000000000000084315A00FFFFFF00630029006300
      3100630029009C637B00FFFFFF00FFFFFF0094527300FFFFFF00FFFFFF005A00
      29006300310084315A00FFFFFF006B083900000000000000000018080800007B
      FF00007BFF00F7E7E700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007B29210000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C3100FF9C3100FF9C310000000000525252006331
      0000000000000000000000000000000000000000000000000000DE9C7B00FFBD
      52007B9CEF00DEB56B008C8494009C9CA5008442730094425200D6421800FF52
      2100F7210800DECECE00000000000000000084295A00FFFFFF00D6B5C6005A00
      2900F7EFF700FFFFFF00FFFFFF006B08310063002900EFE7EF00FFFFFF006B10
      39005A002900FFFFFF00EFE7E700DEBDCE00000000000000000052211800007B
      FF00007BFF00EFCECE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF009431290000000000000000000000000000000000000000000000
      0000FF9C3100FF9C3100FF9C310000000000BDBDBD008C8C8C00BDBDBD006331
      0000000000000000000000000000000000000000000000000000F7EFE700F773
      0800FFBD7300F7C68C00E7AD8C00FFBD7B00F7635200735A9400EF634A00F742
      3100C6100000FFFFFF0000000000000000000000000084295200FFFFFF009452
      7300EFDEE700FFF7F700AD739400630031006300310063002900FFFFFF006300
      2900D6B5C600FFFFFF007318420000000000000000000000000021080800BD42
      29005263A5006B7B940029BDFF0029BDFF0029BDFF0029BDFF0029BDFF005A84
      A5006B738C00A542390000000000000000000000000000000000000000000000
      0000FF9C310000000000BDBDBD008C8C8C00BDBDBD0063310000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00CE4A
      1000FFC68C00FFD6AD00F7D6AD00FFCEAD00FFA59400A584AD00FFA59C00FF6B
      52007B39310000000000000000000000000000000000F7EFEF00BD94AD00FFFF
      FF00CEADBD005A0021006300290063003100630031006300290063083100F7E7
      EF00FFFFFF008429520000000000000000000000000000000000000000000000
      000000000000A54A42004ACEFF004ACEFF004ACEFF004ACEFF004ACEFF009C39
      3100FFC6B500BD635A0000000000000000000000000000000000000000000000
      0000633100008C8C8C00BDBDBD00633100000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00CE420000FFFFFF00FFEFE700FFF7DE00FFDEDE00FFE7E700FFF7E7006308
      0800FFFFFF000000000000000000000000000000000000000000F7EFEF008429
      5A00FFFFFF00FFFFFF00FFF7F700D6BDCE00DEC6CE00FFFFFF00FFFFFF00FFF7
      FF007B214A000000000000000000000000000000000000000000000000000000
      0000000000006B2118007B8494007B8494007B8494007B8494007B849C00BD63
      5200AD4A42000800000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF00DE734A00E7844A00F7CEBD00DEBDAD008C3939008C636300FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      00008C426B0084315A00D6BDCE00EFDEE700E7D6DE00CEA5B5007B215200A56B
      8C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF9C63009C63
      31009C633100FF9C630000000000000000000000000000000000000000000000
      0000000000000000000031636300000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000949C
      9400C6C6AD000000000000000000000000008C6B4A0000000000000000000000
      000000000000000000000000000000000000B5B5B500B5BDBD00B5BDBD00B5BD
      BD00B5BDBD00B5BDBD00B5BDBD00BDBDBD00BDBDBD00B5BDBD00B5BDBD00B5BD
      BD00B5BDBD00BDBDBD00B5BDBD00F7F7F7000000000000000000000000000000
      000000000000000000005252520000000000000000009C633100FFCE9C00FFCE
      9C00FFCE9C00FFCE9C009C633100000000000000000000000000000000000000
      0000316363000000000000313100639C9C003163630000000000000000000000
      000000000000000000000000000000000000000000000000000000000000944A
      2100944A2900B5B5A50094846B009C522900AD5A3900AD522900000000000000
      00000000000000000000000000000000000000000000EF734200F7845A00F794
      6B00F7947300F79C7300FF9C7B0039211800FF5A2900DE522100DE522100DE52
      2100D64A1800D6421800C6310800B5BDBD000000000000000000000000000000
      00008C8C8C0063636300525252006363630052525200FFFFCE00FFFFCE00FFCE
      9C00FFFFFF00FFFFCE00FFFFFF00000000000000000000000000316363000000
      0000003131000031310000313100639C9C003163630031636300424242000000
      00000000000000000000000000000000000000000000000000007B736B00A552
      2900844A2900AD5A3900AD5A3100AD6B4200A5633900A55A3900844218007B73
      6300946B42008C4A2900000000000000000000000000E7946B00E7A58C00EFB5
      9C00EFBDA500F7BDA500F7BDAD0031312900FF945200FF8C4A00F7734200EF73
      4200EF6B3900E7633100D6421800BDBDBD0000000000000000008C8C8C00ADAD
      AD00ADADAD008C8C8C00000000007373730052525200FFFFCE00FFFFCE00FFCE
      9C00FFFFCE00FFFFCE00FFFFCE00000000000000000000000000316363004242
      420000313100639C9C0073737300CECECE00FFFFFF0000000000424242003131
      3100212121000000000000000000000000000000000000000000000000008442
      1800A56339008C522900AD633900D67B4A00C66B4200BD734A00A55A3900A552
      31009C5A3100AD6B42007B4A29000000000000000000EFA58400EFBDA500F7CE
      BD00FFD6C600FFD6C600FFD6C6002929210029080000312921009C4A2100FF84
      5200F77B4A00EF6B3900D64A2100B5BDBD008C8C8C0063636300636363008C8C
      8C000000000031313100000000003131310000000000FFFFCE00FFFFCE00FFCE
      9C00FFFFCE00FFFFCE00FFFFCE00000000000000000031636300316363004242
      420000313100639C9C003163630052525200CECECE00CECECE00FFFFFF000000
      000021212100000010000000000000000000000000009C9C7B0084422100A563
      39009C5A3100D67B520010100800101821004A5A5A004A424A00A5634200AD63
      390094523100A56B4A008C4A21000000000000000000EFAD9400F7C6B500FFDE
      CE00FFEFDE00FFE7DE00FFE7DE00FFF7E700FFF7E700FFFFF70000000000FF94
      6300F7844A00EF734200DE522100B5BDBD00ADADAD008C8C8C00000000003131
      3100212121008C8C8C00525252006363630000000000FFFFCE00FFFFCE00FFCE
      9C00FFFFCE00FFFFCE00FFFFCE00000000000000000031636300316363000031
      310000313100639C9C0031636300316363004242420031313100CECECE00CECE
      CE00FFFFFF00000000000000000000000000944A18009C4A2900AD634200A55A
      3900BD6B420029394A00524A4200736352005A4A42005A524A00394242007B5A
      4200B56B4A00AD633900000000000000000000000000EFB59C00FFF7DE000000
      00000000000073736B00FFEFE700CEBDB500FFEFDE00FFFFFF0000000000FF9C
      6300FF845200F7734200DE522100B5BDBD000000000031313100212121008C8C
      8C00BDBDBD0042424200525252007373730052525200FFFFCE00FFCE9C00FFFF
      CE00FFFFFF00FFFFCE00FFFFCE00000000000000000031636300424242000031
      310000313100639C9C0000000000316363004242420031313100212121002121
      2100CECECE006363630000000000000000008C4218008C5229009C523100C673
      420000000000524A4200524A39001810100018101000292121004A4239002929
      29009C523100B56B4200AD9484000000000000000000F7B59C0029292100FFA5
      5A00FFB584005A180800FFFFDE0031211800A542100021000000FFAD6B00FF8C
      5A00FF845200F7734200DE522100B5BDBD00212121008C8C8C00BDBDBD00008C
      000000BD000042424200525252006363630052525200FF9C6300FFFFCE00FFFF
      CE00FFFFCE00FFFFCE00FF9C6300000000000000000031636300424242009C9C
      9C00316363004242420031636300316363000000000031313100319C63000000
      1000000000000000000000000000000000000000000073523900B56B4A00BD6B
      3900393139005A4A4A001010100042393100635A52006B5A5200635A5200524A
      4200734A42008C522900A55A31007B4A210000000000FFD6B500FFAD8C00E76B
      3900FFAD730031080000FFFFFF0039312900FFC67300FFAD7300FF9C6300FF8C
      5A00FF9C5A00FF8C4A00FF5A2900BDBDBD008C8C8C00008C000000BD000031FF
      630000BD00004242420052525200636363009C633100FFFFFF00FFCE9C00FFCE
      9C00FFCE9C00FFFFCE00FFFFFF009C6331000000000031636300003131000031
      3100639C9C00FFFFFF00316363004242420031636300316363000000000031CE
      63000000000000000000000000000000000094A5AD00B552210094523100B55A
      31007B7B8400423129002921210063524A009C8C8400C6BDB500B5A59C00635A
      5A005A524A00B5634200AD523100BDB5A500000000003108000031080000B552
      2900FFAD7B003918080031100000080000002100000021080000A55A3100FFA5
      7300000000001800000010000000BDBDBD008C8C8C0031FF630000BD0000008C
      000000BD00004242420052525200737373009C633100FFFFCE00FFCE9C00FFCE
      9C00FFCE9C00FFCE9C00FFFFCE009C6331000000000042424200003131003163
      63003131310031639C00639CCE00EFEFEF003163630000313100316363003163
      630000000000000000000000000000000000000000009C4A2100AD633900B563
      31004A4A4A003931310029212100736B6300BDADA500FFFFFF00DEDED600947B
      7300945231009C523100B56B4200B5B5940000000000F7845200F78C5A00FF9C
      6B00FF9C6B00FFAD7300FFAD6B0039211800E75A100000000000FFBD8400FF94
      5A0039180800D64A0800A5290000BDBDBD008C8C8C00008C000031FF6300008C
      0000424242008C8C8C00ADADAD007373730073737300636363009C633100FFCE
      9C00FFCE9C00FF9C63009C633100000000000000000000313100000000003163
      630000000000316363004242420031639C00313131002121210021212100FFFF
      FF00000000000000000000000000000000000000000000000000A5A59C00AD6B
      420073524A005A5A52002918180073635A00BDADAD00DED6D6007B7373007B7B
      7300D67B4A00945A3900945231009C52290000000000F77B5200F78C5A00FF94
      6300FFA56B00000000003910000000000000DE5210004A10000073391800B56B
      390000000000C64208009C290000BDBDBD008C8C8C00008C0000424242008C8C
      8C00ADADAD00EFEFEF00DEDEDE00DEDEDE00ADADAD0073737300636363004242
      420000000000FFCE9C009C633100000000000000000000313100316363000000
      00003131310031636300000000003163630042424200FFFFFF009C9C9C002121
      2100000000000000000000000000000000000000000000000000000000000000
      0000AD734200634A420039313100524A42006B635A00947B730073737300BD63
      3100AD634200B56342009C633900944A290000000000F77B5200F78C5A00FF94
      6300FF9452008C310000BD4A1000D6521000BD4A1000C64A1000BD420800BD42
      0000CE4A1000B5390800A5290000BDBDBD008C8C8C008C8C8C00ADADAD00FFFF
      FF00DEDEDE00EFEFEF00DEDEDE00DEDEDE00DEDEDE00DEDEDE00ADADAD004242
      420000000000FFFFCE009C633100000000000000000000000000316363002121
      21009C9C9C00CECECE0031313100316363000000000031636300424242000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000946B4A00AD633900BD634200634229007B5A4A008C523100AD5A2900A55A
      3900AD6B4200944A2900944A29009473520000000000EF734A00F7845200F794
      6300FF844A0029080000E75A100052180000C64A1000BD421000BD421000B542
      1000B5420800AD390800A5210000BDBDBD0073737300DEDEDE00DEDEDE00FFFF
      FF00DEDEDE00EFEFEF00DEDEDE00DEDEDE00DEDEDE00DEDEDE00737373000000
      00009C633100FF9C6300FFFFCE00000000000000000000000000316363000031
      310031636300212121009C9C9C00CECECE003131310031636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000948C7B00B5634200AD633900B56B4200AD633900A5633900A5633900A563
      39009C5A290000000000000000000000000000000000EF6B4200EF7B4A00F784
      5200FF8C5A009C4A29003118100042291800D64A0800B5420800B5420800B542
      0800AD390800AD3108009C210000BDBDBD00000000000000000073737300FFFF
      FF00DEDEDE00EFEFEF00DEDEDE00DEDEDE007373730000000000000000000000
      0000000000009C633100FF9C6300000000000000000000000000000000000000
      0000316363000031310031636300212121003163630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000944A29008C523100B552290094735200B56B4A00944A29008C42
      18000000000000000000000000000000000000000000EF633100EF6B4200F77B
      4A00F77B5200F7845200F784520039180800D6420800B5390800B5390800B539
      0800B5310800AD310800A5210000BDBDBD000000000000000000000000000000
      000073737300EFEFEF0073737300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000031636300003131003163630000313100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ADA58C009C8C7300A5A5940000000000000000008C5229008431
      0800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000008C00ADADAD00000052000000000000000000000000000000
      DE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000ADADAD004242
      4200212121004242420000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFCE
      6300FF9C6300FFCE6300000000000000000000000000CE630000CE630000CE63
      0000CE630000CE630000CE630000CE630000CE630000CE630000CE630000CE63
      0000CE630000CE630000CE630000CE6300000000000000000000000000000000
      8C0000008C0000008C00ADADAD000000520000005200000052000000DE000000
      DE00424242007373730000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADADAD00DEDEDE00DEDEDE008C8C
      8C00424242004242420042424200424242000000000000000000000000000000
      00000000000000000000000000000000000000000000FFCE6300FF9C6300FFCE
      9C00FFCE9C00FFCE6300000000000000000000000000CE630000FFFFFF00FFFF
      FF00FFFFF700FFF7E700FFEFD600FFE7C600FFDEB500FFD6AD00FFD6AD00FFD6
      AD00FFD6AD00FFD6AD00FFD6AD00CE6300000000000000008C0000008C00ADAD
      AD00CE63630000008C00ADADAD00000052006331310000005200CE6363000000
      DE00424242007373730000000000000000000000000000000000000000000000
      00000000000000000000ADADAD00DEDEDE00DEDEDE00DEDEDE00424242000000
      0000424242004242420042424200000000000000000000000000000000000000
      0000000000000000000000000000FFCE6300FF9C6300FFCE9C00FFFFCE00FFFF
      CE00FFCE9C00FFCE6300000000000000000000000000CE630000FFFFFF00FFFF
      FF00FFFFFF00FFFFF700FFF7E700FFEFD600FFE7C600FFDEB500FFD6AD00FFD6
      AD00FFD6AD00FFD6AD00FFD6AD00CE6300000000000000008C00CE636300ADAD
      AD00ADADAD0000008C00ADADAD00000052006331310000005200CE6363000000
      DE00424242007373730000000000000000000000000000000000000000000000
      0000ADADAD00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE004242
      4200000000000000000000000000000000000000000000000000000000000000
      000000000000FFCE6300FF9C6300FFCE9C00FFFFCE00FFFFCE00CE636300FFFF
      CE00FFCE9C00FFCE6300000000000000000000000000CE630000FFFFFF004273
      FF004273FF004273FF00FFFFF700A5390800A5390800A5390800FFDEB500009C
      CE00009CCE00009CCE00FFD6AD00CE6300000000000000008C00ADADAD000000
      8C0000008C0000008C00ADADAD000000520000005200000052000000DE000000
      DE00424242007373730000000000000000000000000000000000ADADAD00DEDE
      DE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00BDBDBD00DEDEDE008C8C8C000000
      000000000000000000000000000000000000000000000000000000000000FFCE
      6300FF9C6300FFCE9C00FFFFCE00FFFFCE00CE636300FFFFCE00CE636300FFFF
      CE00FFCE9C00FFCE6300000000000000000000000000CE630000FFFFFF004273
      FF004273FF004273FF00FFFFFF00A5390800A5390800A5390800FFE7C600009C
      CE00009CCE00009CCE00FFD6AD00CE6300000000000000008C0000008C000000
      8C0000008C0000008C0000000000000000000000520000000000424242000000
      000000008C00424242000000000000000000ADADAD00DEDEDE00DEDEDE00DEDE
      DE00DEDEDE00DEDEDE00BDBDBD0063636300DEDEDE00DEDEDE00DEDEDE004242
      4200000000000000000000000000000000000000000000000000FF9C6300FFCE
      9C00FFFFCE00FFFFCE00FFFFCE00FFFFCE00CE636300FFFFCE00FFFFCE00FFFF
      CE00FFCE9C00FFCE6300000000000000000000000000CE630000FFFFFF004273
      FF004273FF004273FF00FFFFFF00A5390800A5390800A5390800FFEFD600009C
      CE00009CCE00009CCE00FFD6AD00CE6300000000000000008C00ADADAD00CE63
      6300ADADAD000000000021212100212121002121210042424200424242000000
      000000008C0000008C00000000000000000000000000DEDEDE00DEDEDE00DEDE
      DE00BDBDBD0063636300DEDEDE00DEDEDE00ADADAD00DEDEDE00DEDEDE008C8C
      8C00000000000000000000000000000000000000000000000000FF9C6300FFFF
      CE00FFFFCE009C63CE00FFFFCE00FFFFCE00FFFFCE00FFCE6300FF633100FFFF
      CE00FFCE9C00FFCE6300000000000000000000000000CE630000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7E700FFEF
      D600FFE7C600FFDEB500FFD6AD00CE6300000000000000008C00ADADAD00ADAD
      AD0000008C002121210010101000212121002121210021212100424242004242
      42000000520000008C00000000000000000000000000DEDEDE00BDBDBD006363
      6300DEDEDE00DEDEDE00ADADAD0021212100DEDEDE00DEDEDE00424242000000
      0000000000000000000000000000000000000000000000000000FF9C6300FFFF
      CE00CE63FF009C63CE00FFFFCE00FFCE6300FF633100FFFFCE00FFFFCE00FFFF
      FF00FF9C6300FFCE6300000000000000000000000000CE630000FFFFFF00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00E77B0000E77B0000E77B0000FFFFF700009C
      0000009C0000009C0000FFDEB500CE630000000000000000000000008C000000
      8C00000000002121210021212100212121004242420042424200424242002121
      210000000000000000007373730000000000DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00ADADAD0021212100DEDEDE00DEDEDE008C8C8C00DEDEDE00DEDEDE004242
      4200000000000000000000000000000000000000000000000000FF9C6300FFFF
      CE00CE9CFF009C63CE00FFFFCE00FFFFCE00FFFFCE00FFFFFF00FF9C6300FFCE
      6300FFFFCE0000000000000000000000000000000000CE630000FFFFFF00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00E77B0000E77B0000E77B0000FFFFFF00009C
      0000009C0000009C0000FFE7C600CE6300000000000000000000000000000000
      0000212121001010100021212100212121004242420021212100424242004242
      42000000000021212100101010000000000000000000DEDEDE00ADADAD002121
      2100DEDEDE00DEDEDE008C8C8C0000000000DEDEDE00DEDEDE00BDBDBD008C8C
      8C00000000000000000000000000000000000000000000000000FF9C6300FFFF
      CE0000CECE00FFFFCE00FFFFCE00FFFFFF00FFCE6300FFCE6300FFFFCE000000
      00000000000000000000000000000000000000000000CE630000FFFFFF00CE9C
      9C00CE9C9C00CE9C9C00FFFFFF00E77B0000E77B0000E77B0000FFFFFF00009C
      0000009C0000009C0000FFEFD600CE6300000000000000000000000000000000
      0000212121002121210021212100101010004242420042424200000000002121
      21001010100021212100000000000000000000000000DEDEDE00DEDEDE00DEDE
      DE008C8C8C0000000000DEDEDE00DEDEDE00BDBDBD008C8C8C00000000000000
      0000000000000000000000000000000000000000000000000000FF9C6300FFFF
      CE00FFFFCE00FFFFFF00FF9C6300FFFFCE00CE63630000000000000000000000
      00000000000000000000000000000000000000000000CE630000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFF700FFF7E700CE6300000000000000000000000000003163
      FF0031319C000000000021212100212121000000000010101000212121002121
      21002121210021212100000000000000000000000000DEDEDE008C8C8C000000
      0000DEDEDE00DEDEDE00BDBDBD008C8C8C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FF9C6300FFFF
      FF00FF9C6300FFCE6300FFCE6300FFCE6300FFCE630000000000000000000000
      00000000000000000000000000000000000000000000CE630000CE630000CE63
      0000CE630000CE630000CE630000CE630000CE630000CE630000CE630000CE63
      0000CE630000CE630000CE630000CE6300000000000000000000000000000000
      000031319C000000000000000000000000002121210021212100212121001010
      10002121210000000000000000000000000000000000DEDEDE00DEDEDE00DEDE
      DE00BDBDBD008C8C8C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFCE6300FFCE
      6300FFFFCE000000000000000000CE636300FFCE630000000000000000000000
      0000000000000000000000000000000000000000000000000000CE630000CE63
      0000CE630000CE630000CE630000CE630000CE630000CE630000CE630000CE63
      0000CE630000CE630000CE630000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000212121002121
      210000000000000000000000000000000000DEDEDE00DEDEDE00BDBDBD008C8C
      8C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFCE000000
      0000000000000000000000000000FF9C6300FFCE630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD008C8C8C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFCE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001818180021212100181818000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD007373730000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ADADAD007373730000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000018181800EFEFEF00B5B5B500ADADAD003131310018181800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000042424200ADADAD007373730063636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000042424200ADADAD007373730063636300000000000000
      0000000000000000000000000000000000000000000000000000181818005252
      5200FFFFFF00E7E7E700ADADAD00BDBDBD00BDBDBD00BDBDBD00525252001818
      1800000000000000000000000000000000000000000018181800B5B5B5009C9C
      9C00181818000000000000000000182129007BDEFF0073D6FF00181818000000
      0000000000000000000000000000000000000000000000000000000000000000
      000042424200ADADAD0073737300ADADAD007373730073737300737373006363
      6300000000006363630000000000000000000000000000000000737373000000
      000042424200ADADAD008C8C8C00ADADAD007373730073737300737373006363
      6300000000006363630000000000000000000000000018181800D6D6D600F7F7
      F700F7F7F700EFEFEF00ADADAD00ADADAD00C6C6C600BDBDBD00BDBDBD00C6C6
      C6007373730018181800000000000000000018181800FFFFFF00CECECE00FFFF
      FF00FFFFFF0018181800000000002984C600296B9C00316B8C00294A5A000000
      00000000000000000000000000000000000000000000BDBDBD0042424200ADAD
      AD00737373009C9C9C009C9C9C00ADADAD007373730073737300737373007373
      73007373730063636300000000000000000000000000BDBDBD00737373000000
      00008C8C8C00CECECE00ADADAD00ADADAD007373730073737300737373007373
      73007373730063636300000000000000000021212100FFFFFF00F7F7F700F7F7
      F700F7F7F700EFEFEF00ADADAD009C9C9C002121210073737300C6C6C600BDBD
      BD00BDBDBD00C6C6C600949494001818180029292900E7D6DE00BDBDBD007B7B
      7B00ADA5A500212121000000000018314200218CEF0039ADFF00181818000000
      00000000000000000000000000000000000000000000BDBDBD00737373009C9C
      9C00ADADAD009C9C9C009C9C9C00ADADAD007373730073737300737373007373
      7300737373007373730000000000000000000000000042424200737373000000
      0000ADADAD009C9C9C008C8C8C00ADADAD007373730073737300737373007373
      73007373730073737300000000000000000039393900F7F7F700F7F7F700F7F7
      F70039393900EFEFEF00ADADAD00BDBDBD00BDBDBD00BDBDBD005A5A5A004A4A
      4A00BDBDBD00BDBDBD00C6C6C6001818180018181800DEADB500EFF7F700CEB5
      BD0029739C0084E7FF0052B5DE00000000001818180018181800181818001818
      1800526B520018211800000000000000000000000000BDBDBD0073737300ADAD
      AD0042424200009CCE008C8C8C00ADADAD007373730073737300737373007373
      73007373730073737300000000000000000000000000BDBDBD00737373000000
      0000636363006363630042424200ADADAD007373730073737300737373007373
      73007373730073737300000000000000000042424200FFFFFF0084848400CECE
      CE0094949400EFEFEF00ADADAD00ADADAD00C6C6C600BDBDBD00BDBDBD00C6C6
      C6007B7B7B0021212100C6C6C60018181800000000005A4A4A00FFC6C600AD84
      7B002984C60031739C00428CAD001818180021292100738C7300BDE7B500ADDE
      A5006B8C63005A7B5200000000000000000000000000BDBDBD0073737300ADAD
      AD00ADADAD00424242009C9C9C00ADADAD007373730073737300737373007373
      73007373730073737300000000000000000000000000BDBDBD00737373000000
      0000212121002121210000000000ADADAD007373730073737300737373007373
      730073737300737373000000000000000000424242004A4A4A00FFFFFF00FFFF
      FF0094949400E7E7E700ADADAD008C8C8C001818180084848400C6C6C600BDBD
      BD00BDBDBD00C6C6C600BDBDBD00181818000000000000000000000000001818
      1800086BCE00219CFF0018528400D6F7CE009CBD9C00526352006384630094C6
      8C0094D684008CCE7B00181818000000000000000000BDBDBD0073737300ADAD
      AD00ADADAD00ADADAD00ADADAD00ADADAD007373730073737300737373007373
      73007373730073737300000000000000000000000000BDBDBD0073737300ADAD
      AD00000000000000000000000000ADADAD007373730073737300737373007373
      7300737373007373730000000000000000004242420039393900FFFFFF00ADAD
      AD0094949400F7F7F700ADADAD00BDBDBD00C6C6C600A5A5A500393939006363
      6300C6C6C600BDBDBD00BDBDBD00212121002929290073737300BDC6BD00EFFF
      EF00CEE7C600ADBD9C007B8C73006B846B00637B6300ADE79C009CD68C008CCE
      7B0084CE73007BC66300293929000000000000000000BDBDBD0073737300ADAD
      AD00DEDEDE0073737300ADADAD00ADADAD00ADADAD0073737300737373007373
      73007373730073737300000000000000000000000000BDBDBD0073737300ADAD
      AD000000000073737300ADADAD00ADADAD00ADADAD0073737300737373007373
      730073737300737373000000000000000000424242002929290073737300FFFF
      FF00AD9C9C0039849C004294BD0063636300ADADAD00BDBDBD00BDBDBD00C6C6
      C6004A4A4A0039393900B5B5B50029292900C6CEC6008C948C00636B63008494
      8400B5D6B5006B846B00E7FFE7007BA57B00CEFFCE00293121007BB56B007BC6
      6B007BC6630073BD63006BB563000000000000000000BDBDBD00737373007373
      7300ADADAD00ADADAD00FFFFFF00DEDEDE00ADADAD00DEDEDE00ADADAD007373
      73007373730073737300000000000000000000000000BDBDBD00737373005252
      5200ADADAD00ADADAD00FFFFFF00DEDEDE00ADADAD00DEDEDE00ADADAD007373
      7300737373007373730000000000000000004A4A4A00FFFFFF00BDB5B5004273
      840084EFFF006BDEFF00425A6300FFFFFF00DEADAD0029424200847B7B00C6C6
      C600BDBDBD00C6C6C6009C9C9C003131310042424200D6F7D600C6E7C600BDE7
      BD006B846B00526B52009CC69C0063846300BDEFBD0094B594006394520073C6
      63006BBD63006BBD630063BD6B001818180000000000BDBDBD00ADADAD00ADAD
      AD00FFFFFF00DEDEDE00ADADAD00DEDEDE00DEDEDE00DEDEDE00ADADAD00DEDE
      DE00ADADAD0073737300000000000000000000000000BDBDBD00ADADAD00ADAD
      AD00FFFFFF00DEDEDE00ADADAD00DEDEDE00DEDEDE00DEDEDE00ADADAD00DEDE
      DE00ADADAD0073737300000000000000000031313100293939008CEFFF008CEF
      FF0042525A00FFF7F7008C8C8C00B58484004A7B7B00ADFFFF00B5ADAD006363
      63005A5A5A00BDBDBD00BDBDBD004242420018181800CEEFCE00B5DEB500ADDE
      A50084A57B0042523900E7FFE700637B6300739473007B9C7B007BCE6B0063AD
      5A00528C5200396B4200396339002131210000000000BDBDBD00FFFFFF00DEDE
      DE00ADADAD00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00ADADAD00DEDEDE00424242000000000000000000BDBDBD00FFFFFF00DEDE
      DE00ADADAD00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDE
      DE00ADADAD00DEDEDE0042424200000000000000000039636B0094F7FF007BEF
      F700B5B5B5009C7B7B006B6363007BEFEF0084D6D60063B5B500ADA5A500526B
      6B0052737300DEE7E7005A52420039393900000000007B9C7B00A5D6A5009CD6
      94009CDE8C007BB57300425A3900B5E7B5008CAD8C0063A55A005AAD5A005AAD
      63004A945A0039734A00315A390018292100000000000000000042424200EFEF
      EF00ADADAD00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00ADADAD00DEDE
      DE0042424200000000000000000000000000000000000000000042424200EFEF
      EF00ADADAD00DEDEDE00DEDEDE00DEDEDE00DEDEDE00DEDEDE00ADADAD00DEDE
      DE0042424200000000000000000000000000000000003952520084EFF7002131
      3100AD8484006BCECE004A737300F7F7F7004A424200737373006BBDBD00BDBD
      BD00B5BDBD008C6B5200312929001818180000000000181818008CC68400425A
      42005A844A0084CE6B0073B563004A7342002131210018181800181818000000
      0000000000000000000000000000000000000000000000000000000000000000
      000042424200EFEFEF00ADADAD00DEDEDE00ADADAD00DEDEDE00424242000000
      0000000000000000000000000000000000000000000000000000000000000000
      000042424200EFEFEF00ADADAD00DEDEDE00ADADAD00DEDEDE00424242000000
      0000000000000000000000000000000000000000000018181800000000000000
      00000000000018181800393939005A5A5A0029393900B5FFFF0031525A007B63
      52004A42310018181800000000000000000000000000000000007BAD6B005A84
      4A00212921001818180000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000042424200DEDEDE004242420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000042424200DEDEDE004242420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000018181800314A4A0018181800735A42001818
      1800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000181818001818180000000000000000000000
      0000000000000000000000000000000000000000000000000800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000104AA5004A4A4A006B6B630039393900181818000000
      00000000000000000000000000000000000000000000396B8C00083952000042
      6300003152000000080000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000003163009CCEFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B00000000007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000104AA5001052A5004A4A4A00B5ADA500B5B5AD007B7B7B003939
      3100292929000000000000000000000000000000000018293900294A63000829
      3900105A840010182100081829000810210000315A0000081800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000003163009CCEFF00639CCE009CCEFF0031639C00639CCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BDBDBD00000000007B7B7B00000000007B7B7B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000104AA5001052A5001884FF004A4A4A00B5ADA500B5B5B500B5B5B500BDBD
      BD0084848400524A4A0021212100000000000000000021314A00426B8C00295A
      840010293900101829001029420010426B0010294A000810100008395A000818
      3100000000000000000000000000000000000000000000000000003163009CCE
      FF00639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C00639C
      CE00000000000000000000000000000000000000000000000000000000000000
      0000BDBDBD0000BD000000000000007B00000000000000FF0000003900000000
      000000000000000000000000000000000000000000000000000000000000104A
      A5001052A5001884FF001863BD0008295200525A6300948C8400BDB5B500B5B5
      B500BDBDB500BDBDBD00181818002929290000000000294A730039637B001821
      310029527300216B9C001863940010639400105A8C0010426B00101831000839
      6300004273000000000000000000000000000000000031639C00639CCE003163
      6300639CCE00639CCE00639CCE009CCEFF0031639C0031639C0031639C003163
      9C0031639C00639CCE0000000000000000000000000000000000000000000000
      0000007B000000BD0000000000000000000000000000397B390000FF00007B7B
      7B000000000000000000000000000000000000000000000000001052A5001052
      A5001884FF001863BD009CBDE7001863BD00105AC6000829520042525A008C84
      8400B5B5AD00C6BDBD0018181800000000000000080039638C00314A6B004284
      AD003173A500316B9C001829390018314200318CC6002173A50010639C000818
      290000426B000000000000000000000000000000000031639C00639C9C009CCE
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C009CCEFF003163
      9C0031639C00639CCE0000000000000000000000000000000000000000000000
      00007B7B7B00BDBDBD000000000000000000000000007B7B7B0000FF00007B7B
      7B0000000000000000000000000000000000000000001052A5001052A5001884
      FF001863BD009CBDE7001863BD002184FF002184FF001884FF00106BDE000829
      52004252630084847B0018181800000000000008180042739C004273A5005294
      BD002952730021314A00316B9C00213952001018290018294200184263000821
      3100000000003184B50000000000000000000000000031639C00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C009CCEFF003163
      9C0031639C00639CCE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000BD0000397B39000000
      0000000000000000000000000000000000001052A5001052A50031A5FF001863
      BD009CBDE7001863B5002994FF00218CFF00187BFF00187BFF001884FF00218C
      FF00106BE700082952000829520000000000000818004A7BB5004A7BB5003152
      6B0042739C0031526B00294A630010182100297394004A6B5A0021522900104A
      5A0000528400004A730000000000000000000000000031639C00639CCE00639C
      CE00639CCE00639CCE00639CCE009CCEFF0031639C0031639C009CCEFF003163
      9C0031639C00639CCE0000000000000000000000000000000000000000000000
      0000000000000000000000000000397B390000000000397B3900000000000000
      000000000000000000000000000000000000104A9C005AC6FF00216BB5009CBD
      E700216BBD0031A5FF00319CFF00218CFF004A9CFF00429CFF002184FF001884
      FF001884FF001884FF001063D600084A940000183100528CC600528CBD004A84
      BD004A7BAD00396384004294520052847300395A390052C6BD00396342004284
      730052BDC6001884630000000000000000000000000031639C00639CCE00639C
      CE00639CCE009CCEFF0031639C00639CCE00639CCE0031639C009CCEFF003163
      9C0031639C00639CCE0000000000000000000000000000000000000000000000
      0000BDBDBD00003900000000000000BD00000000000000000000000000000000
      000000000000000000000000000000000000104A9C00317BC6009CBDE7002173
      BD0042B5FF0039A5FF002994FF0052ADFF00FFFFFF00BDDEFF001884FF001884
      FF001884FF00186BDE00104A940000000000001839005A9CDE005294D600528C
      CE005284BD0010A510004AC652004ACE520052AD9C004273940031845A004294
      73002952290063CEBD00217B7B00000000000000000031639C00639CCE009CCE
      FF0031639C00639CCE00639CCE00639CCE00639CCE00CEFFFF009CCEFF003163
      9C0031639C00639CCE0000000000000000000000000000000000000000000000
      0000397B390000FF000000000000BDBDBD000000000000000000000000000000
      000000000000000000000000000000000000104A9C009CBDE7002973BD004ABD
      FF005AC6FF00CEEFFF008CCEFF00E7F7FF00DEEFFF00318CFF00187BFF001884
      FF001873DE0008428C00000000000000000008295A0063ADEF0063A5E7005A9C
      DE005294CE0008842100088C2100088C2100088421001042290021BD310029BD
      310031B54A00299C4A0010392100000000000000000031639C0031639C00639C
      CE00639CCE00639CCE0031639C00CEFFFF009CCEFF00639CCE00639CCE00639C
      CE00639CCE00639CCE0000000000000000000000000000000000000000000000
      0000007B0000397B3900000000000000000000000000BDBDBD00BDBDBD000000
      000000000000000000000000000000000000185AAD00317BBD005ACEFF0052C6
      FF004ABDFF0063C6FF00CEEFFF00FFFFFF009CCEFF001884FF002184FF00187B
      EF001052A500000000000000000000000000000821006BB5FF006BADFF0063A5
      EF005A9CDE00528CC600214A5A00109C180000B5100008B5180008B5180008B5
      180008B51800009C08000000000000000000000000000000000031639C00639C
      CE00639CCE00CEFFFF009CCEFF00639CCE0031639C00639CCE00639CCE00639C
      CE00003163000000000000000000000000000000000000000000000000000000
      0000397B390000BD0000000000007B7B7B000000000000BD0000397B39000000
      00000000000000000000000000000000000000000000104A9C00297BCE003994
      E7004ABDFF0039B5FF0031A5FF0063B5FF00BDDEFF004A9CFF00187BEF001052
      A50000000000000000000000000000000000000000005294CE006BB5FF0063AD
      F7005A9CE7000063210084DE8C007BD684007BD684008CDE940031C64200005A
      00000000000042394200A5A5A500000000000000000000000000000000000000
      0000639CCE00639CCE00639CCE00639CCE00639CCE00639CCE00003163000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000397B39000000000000FF000000000000397B3900000000000000
      000000000000000000000000000000000000000000000000000000000000104A
      9C00185AB5003194E70039ADFF00319CFF002994FF002984EF001052A5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000018005AD66B0094DE9C0094DE9C009CE7A5000000000039C64A007BDE
      84007BDE840084E78C0000000000000000000000000000000000000000000000
      0000000000000000000031639C00639CCE000031630000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000010529C001863BD00319CFF00217BD600105AA500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000107B1800BDEFBD00B5E7B500ADE7B50063D66B00B5E7B500ADE7
      B500B5E7BD0029BD390000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B00000000007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000104A9C00104A9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000529C520000730000D6F7D6005AC663000873
      10007B947B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000005200000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD3939008C5A5A00BDAD
      AD00EFEFE700DEDEDE008C3939008C2929000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000520000005200000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5310000AD4200008C21
      000000000000000000000000000000000000B5525200BD4A4A008C4A4A008429
      2900DECECE00FFFFFF009439390094212100A542420000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000085AE7001852CE003163BD00425A9C001042B5000000
      00000000000000000000000000000000000000000000005A0000005A00000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000094290000BD4A0000AD42
      00008C210000000000000000000000000000B5525200BD424200A55A5A008C39
      39009C8C8C00EFE7E70000520000005200000063000000520000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000317BEF005A8CD6009CA5B500C6B5AD00DEAD8C00F79C5A00A5948C00215A
      C600000000000000000000000000000000000000000000630800086B10000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000094290000C652
      0000A5390000842100000000000000000000B5525200B5394200BD525200BD63
      6300B5525200BD5A5A00B5525200BD4A4A000052000000840800007308000052
      00000000000000000000000000000000000000000000000000001073FF005A9C
      E700FFD6A500FFD6A500FFDEA500FFD6A500E7A57B00DE844A00FFC68C00EFD6
      C6005A8CD6001863DE00000000000000000000000000006B0800189429000052
      0000000000000000000000000000000000000000000000520000000000000000
      000000000000000000000000000000000000942900009C310000942900009429
      00009C3100009C3100009C3100009C3100008C29000000000000000000009C31
      0000C6520000942900008421000000000000A54A4200C6848400EFD6D600EFD6
      CE00EFCECE00EFCECE00EFDEDE00C67373009C31310000520000008C0800007B
      0800005200000000000000000000000000000000000000000000296BBD00CEB5
      9C00FFEFC600FFE7C600FFE7C600EFCEAD00DE946B00E79C6B00FFD6B500FFEF
      D600FFFFEF00A5C6EF002973EF000000000000000000006B080029BD4A00086B
      1000000000000000000000000000000000000000000000520000005200000000
      000000000000000000000000000000000000AD420800EF841800D6630800BD4A
      0000BD4A0000BD4A0000BD4A0000BD4A00009429000000000000000000000000
      0000A5390000BD4A00008C2100008C210000A54A4200DEBDBD00EFF7F700E7E7
      E700E7E7E700E7E7E700EFF7F700C67373009C313100A5636B00005200000094
      100000520000A5636B00A5636B00A5636B00000000001873FF00AD733900FFDE
      A500FFF7EF00FFF7E700FFE7D600EFBD9C00D6845200EFAD8400FFE7CE00FFF7
      EF00FFFFF700FFFFFF009CB5E700105AE70000000000005A000029B54A0021AD
      4200005200000000000000000000000000000000000000520000008408000052
      000000000000000000000000000000000000AD4A0800FF9C3100E77B1800D663
      08008C2900008C2900008C2900008C2900008C29000000000000000000000000
      00008C290000BD4A00009C31000084210000A54A4200DEB5B500DEE7DE00D6CE
      CE00D6CECE00D6CECE00E7E7E700C67373009C313100FFE7B5000052000008A5
      180000520000EFC68400F7D68400A5636B0000000000186BC6006BB55A00FFF7
      E700FFFFF700FFFFF700F7E7D600E7A58400E7946300F7CEAD00B5B5AD00DED6
      D600FFFFFF00FFFFFF00B5949400296BD6000000000000000000108C290042E7
      7300189C31000052000000520000005200000052000000520000089C1000007B
      080000520000000000000000000000000000AD4A0800FFA53100B5521000EF84
      2100C65A08008421000000000000000000000000000000000000000000000000
      000000000000B5420000AD39000084210000A54A4200DEB5B500EFEFEF00DEDE
      D600DEDEDE00DEDEDE00EFEFEF00CE7B730000520000005200000052000018B5
      3900005200000052000000520000A5636B0000000000186B730063D68C00FFFF
      FF00FFFFFF00FFFFF700EFD6BD00DE946B00E79C7300FFE7D600F7EFE700BDBD
      BD00C6C6C600F7F7EF00847BA500217BFF0000000000000000000052000029B5
      4A0039E76B0029B54A00109C2900109C2100109C29000894180008941000008C
      080000840800005200000000000000000000AD4A0800FFAD39009C310000AD42
      0800FF9C2900C65A080084210000000000000000000000000000000000000000
      000000000000AD420000AD4200008421000000000000C6A5A500DEE7E700DED6
      D600DED6D600DED6D600DEDEDE00B56B6B0000520000189C310039D6630029C6
      520018AD2900088C180000520000A5636B00000000000839C600C6D6EF00FFFF
      FF00FFFFFF00FFFFFF00E7CEBD00DE946B00F7C69C00E7DECE00FFF7F700FFFF
      FF00EFEFE700A5C6F700217BFF0000000000000000000000000000000000005A
      000021AD420031D6630029CE520021BD420018AD310010A52100089C1800008C
      080000940800008410000052000000000000AD4A0800FFAD3900A53908007B18
      00008C210000FF9C2900CE5A10008C2100000000000000000000000000000000
      00008C210000BD4A0000AD390000842100000000000000000000000000000000
      0000000000000000000000000000BD8C8400FFFFF7000052000029BD520042E7
      7B00189C310000520000F7D69400A5636B001863E7001842EF00FFFFFF00DEEF
      FF00A5CEFF0084B5FF00318CFF005A94E700D6D6DE00D6CEC600B5B5B500E7E7
      E700FFFFFF00529CFF001073FF00000000000000000000000000000000000000
      0000005200000873180018A5390021B5390018B5310018AD2900089C1800089C
      1000087B1000005200000000000000000000AD4A0800FFAD3900A53908000000
      0000000000008C210000FF9C2900DE7318009429000084210000842100008421
      0000A5390000C6520000942900008C2100000000000000000000000000000000
      0000000000000000000000000000C6947B00FFFFFF00FFF7F7000052000021AD
      420000520000F7D6B500FFDEA500A5636B001873FF00398CFF004A9CFF005A9C
      FF00000000000000000000000000000000001873FF009CCEFF00EFEFEF00D6CE
      C600CEE7FF00297BFF0000000000000000000000000000000000000000000000
      0000000000000000000000520000005A0000005A00000052000010AD2900087B
      100000520000000000000000000000000000B54A1000FFB53900A53908000000
      000000000000000000008C210000E7842100EF841800C65A0800AD420000B542
      0000C64A00009C31000094290000000000000000000000000000000000000000
      0000000000000000000000000000DEA58400FFFFFF00FFFFFF00FFF7F7000052
      0000F7E7CE00FFE7CE00F7DEB500A5636B000000000000000000000000000000
      00000000000000000000000000000000000000000000000000001873FF00ADD6
      FF005AA5FF001873FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000520000087310000052
      000000000000000000000000000000000000A5390800E78C29009C3100000000
      00000000000000000000000000008C210000AD4A0800C65A1000BD520000A539
      0000942900008421000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7AD8400FFFFFF00FFFFFF00FFFFFF00FFF7
      F700FFEFE700A56B5A00A56B5A00A5636B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00001873FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000520000005200000000
      000000000000000000000000000000000000A5390800942900008C2100000000
      0000000000000000000000000000000000008C2100008C2100008C2100008C21
      00008C2100000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7B58C00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFF7F700A56B5A00E7944200BD7342000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000520000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EFBD8C00FFF7EF00FFF7F700FFF7F700FFF7
      F700F7EFEF00A56B5A00CE846300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E7B58400D69C7B00D69C7B00D6946B00D694
      6B00D6946B00A56B5A0000000000000000007B7B7B004A637B00BD9494000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000003110000042180000522100005221000042180000311000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000210000003100000039000000390000003100000029000000
      0000000000000000000000000000000000006B9CC600188CEF004A7BA500CE94
      9400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094290000B542000094290000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004A18
      00004A18000084310000A5420000AD420000AD420000A5420000843100005218
      0000521800000000000000000000000000000000000000000000000000000000
      390000003900000063000000840000008C0000008C0000008C00000063000000
      3900000039000000000000000000000000004AB5FF0052B5FF00218CEF004A7B
      A500C69494000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008C21
      0000B5420000BD4A00008C290000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000005A2100006B29
      0000AD420000B5420000AD420000A5420000AD420000AD420000AD420000B542
      0000732900003110000000000000000000000000000000000000000042000000
      5200000094000000940000008C000000840000008C0000008C00000094000000
      9400000052000000210000000000000000000000000052B5FF0052B5FF001884
      E7004A7BA500CE94940000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008C210000AD42
      0000BD4A00008C29000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000005A2100007B310000C64A
      0000BD4A0000AD420000A5420000A5420000A5420000A5420000A5420000AD42
      0000B5420000732900005218000000000000000000000008520000005A000000
      A50000009C0000008C00000084000000840000008C0000008C0000008C000000
      8C0000009400000052000000390000000000000000000000000052B5FF004AB5
      FF00188CE7004A7BA500BD949400000000000000000000000000000000000000
      000000000000000000000000000000000000000000008C210000AD420000C652
      00009429000000000000000000008C2900009431000094310000943100009431
      00009429000094290000943100008C290000000000005A210000D6520000CE52
      0000BD4A0000AD420000B5521000F7E7D600FFEFE700C67B4200A5420000A542
      0000AD420000B5420000521800000000000000000000000852000000B5000000
      AD0000009C0000008C002942B500E7EFFF00D6DEF7000008940000008C000000
      8C0000008C0000009400000039000000000000000000000000000000000052BD
      FF004AB5FF002184DE005A6B730000000000AD7B7300C6A59C00D6B5A500D6A5
      9C0000000000000000000000000000000000000000009C310000D66B08009C31
      0000000000000000000000000000A5390000C64A0000BD4A0000BD4A0000BD4A
      0000BD4A0000BD4A0000B54200008C2900005A210000A5390000EF5A0000CE52
      0000B5420000AD420000AD420000DEAD7B00FFFFFF00FFFFF700C67B3900A542
      0000A5420000AD420000843100004218000000085A0000008C000000CE000000
      AD00000094001839AD00F7F7FF00FFFFFF007384CE0000008C0000008C000000
      8C0000008C000000940000006300000031000000000000000000000000000000
      000052BDFF00B5D6EF00A5948C00B59C8C00F7E7CE00FFFFDE00FFFFDE00FFFF
      DE00EFDEC600CEADA500000000000000000084210000C6631000CE6310008421
      0000000000000000000000000000942900009C3100009C310000942900009C31
      0000C64A0000C64A0000B54200008C2900005A210000D6520000EF5A0000D652
      0000B5420000AD420000AD420000AD420000D6A56B00FFFFFF00FFF7F700C673
      3900A5420000AD4200009C39000042180000000042000000BD000000CE000000
      B5001831AD00F7F7FF00FFFFFF006373CE0000008C0000008C0000008C000000
      8C0000008C0000008C0000008400000031000000000000000000000000000000
      000000000000CEB5B500DEBDA500FFF7C600FFFFD600FFFFDE00FFFFDE00FFFF
      DE00FFFFEF00F7F7EF00B58C8C00000000008C290000EF8C2900AD4208000000
      00000000000000000000000000000000000000000000000000008C210000B542
      0000BD4A0000AD390000B54200008C29000073290000F7630000FF630000FFD6
      AD00FFD6AD00F7CEAD00FFD6AD00F7CEA500F7CEAD00FFFFF700FFFFFF00FFF7
      EF00CE8C4A00AD420000AD4200004A18000000005A000000DE000000DE00425A
      EF00EFF7FF00FFFFFF00F7F7FF00ADBDF700A5B5F700ADB5E700ADB5E700ADB5
      E700ADBDEF0000008C0000008C00000039000000000000000000000000000000
      000000000000C6948C00F7E7B500FFDEAD00FFF7D600FFFFDE00FFFFE700FFFF
      F700FFFFFF00FFFFFF00DED6BD000000000094290000F7943100A53908000000
      000000000000000000000000000000000000000000008C210000AD420000C652
      000094290000A5310000BD4A00008C2900008C310000FF7B1000FF6B0000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00E7C6A500AD420000AD4200005221000000006B000018E7000008E7009CAD
      F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0000008C0000008C00000039000000000000000000000000000000
      000000000000DEBDAD00FFE7AD00F7CE9400FFF7CE00FFFFDE00FFFFE700FFFF
      FF00FFFFFF00FFFFEF00F7EFD600C69C940094290000F7943100B55210008418
      0000000000000000000000000000000000008C210000B5420000C65200009C31
      000084210000A5390000BD4A00008C2900008C310000FF8C2900FF841800F78C
      3100FF8C3100FF8C3100FF8C3100FF842100EFA56300FFFFFF00FFFFFF00EFBD
      8C00C65A0800BD4A0000A54200004A18000000006B001839EF000021E7000008
      D6007B94EF00FFFFFF00FFFFFF006384FF000829C6001831BD001831BD001031
      B5001031B50000009C0000008400000031000000000000000000000000000000
      000000000000E7C6AD00FFE7AD00EFBD8400FFE7B500FFFFDE00FFFFDE00FFFF
      EF00FFFFEF00FFFFE700FFFFD600C6AD9C0084210000D6732100F79429009431
      000084210000842100008421000094310000C6520000C6520000A53100000000
      000000000000A5390000BD4A00008C2900008C310000FF841800FFAD6300FF63
      0000EF5A0000FF630000FF630000FF8C3100FFEFDE00FFFFFF00EFAD7B00CE52
      0000C64A0000BD4A0000943900004A18000000006B001039F7006384FF000000
      E7000000D6006B84EF00FFFFFF00DEE7FF001839D6000000B5000000B5000000
      B5000000A50000009C0000007300000039000000000000000000000000000000
      000000000000DEC6AD00FFEFB500EFBD8400F7CE9C00FFEFC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00F7F7D600CEA59C000000000094290000EF942900F79C
      3100BD521000B54A1000CE631800EF8C2100D663100094310000000000000000
      000000000000A5390000BD4A00008C29000000000000E75A0000FFC69400FF9C
      4200E75A0000EF5A0000FF731000FFFFF700FFFFFF00F7AD7300CE520000CE4A
      0000C64A0000C64A00007329000000000000000000000000CE0094ADFF00395A
      F7000000C6000000CE005A7BEF00FFFFFF00F7F7FF000010C6000000B5000000
      AD000000A5000000AD0000005A00000000000000000000000000000000000000
      000000000000CEA59C00FFF7C600FFEFC600F7D6A500F7D69C00FFE7BD00FFF7
      CE00FFFFD600FFFFDE00E7DEBD0000000000000000000000000094290000C65A
      1800EF8C2900F7943100EF8C2900BD5210008C29000000000000000000000000
      000000000000A5310000BD4A00008C29000000000000E75A0000FF8C2900FFDE
      B500FF944200FF630000EF630000FFC69400FFBD7B00E75A0000DE520000D652
      0000D6520000B54200007329000000000000000000000008D6001842F700B5C6
      FF003152F7000000DE000000CE006B84EF0094A5FF000000C6000000BD000000
      B5000000BD000000940000005A00000000000000000000000000000000000000
      00000000000000000000DEC6AD00FFFFFF00FFF7EF00F7CE9C00F7BD8C00F7CE
      9C00FFE7B500FFF7CE00BD9C8C00000000000000000000000000000000008C21
      0000942900009C31000094290000842100000000000000000000000000000000
      0000000000008C290000942900008C2900000000000000000000C64A0000FF9C
      4200FFE7C600FFBD7B00FF842900FF730800FF6B0000FF6B0800FF6B0800FF63
      0000CE4A000073290000000000000000000000000000000000000000AD003152
      EF00C6D6FF007B94FF000831E7000010E7000008E7000010E7000010E7000008
      E7000000AD000000520000000000000000000000000000000000000000000000
      0000000000000000000000000000D6BDBD00F7F7DE00FFF7C600FFE7B500FFEF
      B500F7DEB500D6AD9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FF84
      1800FF841800FFBD7B00FFD6AD00FFC69400FFB57300FFA55A00FF842100D652
      0000D65200000000000000000000000000000000000000000000000000000829
      EF000829EF007394FF00ADBDFF008CA5FF006384F7004263EF000029E7000000
      BD000000BD000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CEAD9400CEAD9C00DEBDAD00DEBD
      AD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E75A0000FF730800FF7B1800FF7B1000FF630000B54200000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000C6000010E7000021E7000018E7000000DE0000009C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000393939000000
      0000000000000000000000000000393939004242420042424200424242000000
      0000181818000808080000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004A4A4A00B5B5AD009C9C94000000
      000000000000736B6B00C6C6C600DEDEDE00EFEFEF00D6D6D600CECECE009494
      94008C8C8C007373730000000000393939000000000000000000000000000000
      0000000000000000000000000000087B0800087B080000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7E7E700949494009C9C9C006B6B
      6B009C9C9C00ADADAD00CED6D600DEDEDE00E7DEDE00EFE7E700CECECE00C6C6
      C600D6D6D600C6C6C600CECECE00292929000000000000000000000000000000
      00000000000000000000087B080073FFA50008A51800087B0800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6C6C6008C8C8C00D6D6D600BDBD
      BD00B5846B00AD4A2900B56B4A00D6A58C00DEBDB500D6B5AD00C6C6C600D6D6
      D600BDBDBD00D6D6D600E7E7E700000000000000000000000000000000000000
      00000000000000000000087B080073FFA50008AD1800087B0800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006BC6DE005AD6EF005AD6F7005AD6
      F7005ADEF70063E7F7005ADEF70063E7F70063DEF70063DEF7006BE7F70052D6
      EF0052CEEF004AD6F70042D6F700BDC6CE00CECECE009C9C9C008C8C8C00CECE
      CE0073392900B57B5A00CE8C73009CBD8400C6D6B500B5CEAD00BDBDBD00DEDE
      DE00E7E7E700DEDEDE00DEDEDE00000000000000000000000000000000000000
      00000000000000000000087B080073FFA50008AD1800087B0800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADEFF700B5F7FF00BDF7FF00738C
      84007394940063636300737B7B00737B7300C6E7E70073848400737B73007B94
      9400D6FFFF00C6F7FF00C6FFFF00B5BDCE00D6D6D600BDBDB50084848400DEDE
      DE009C8C8400D6846300529C390094CE8400C6DEB500BDD6AD009CB59C00CECE
      CE00BDC6C6009C39080000000000000000000000000000000000000000000000
      00000000000000000000087B080073FFA50010AD1800087B0800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFEFEF00D6D6CE00F7F7F700FFFF
      FF00E7E7E700DEDEDE00A5949400EFE7E7007B635A00FFF7F700846B6300FFF7
      F700735A5200FFFFFF00E7DEDE00BDBDC600EFEFEF00DEDEDE0084848400E7E7
      E700DEDEDE00D68463005AAD4200CEB59C00EFD6CE00EFD6C60084B573004294
      3100C6845A00BD4A180000000000000000000000000000000000087B0800087B
      0800087B0800087B0800087B080073FFA50018B52900087B0800087B0800087B
      0800087B0800087B080000000000000000000000000000000000000000000000
      A5000000A5000000A5000000A5000000A5000000A5000000A5000000A5000000
      A5000000A500000000000000000000000000EFEFEF00847B73007B6B6300FFFF
      FF00948484009C8C8400A59C940094847B00D6CECE00B5ADA500EFEFEF00B5AD
      AD00CEC6C600FFFFFF00FFFFFF00BDBDC600FFFFFF00D6D6D60084848400CECE
      CE00D6D6DE00C67B5200529C3900DEB5A500EFD6CE00EFD6C6007BBD7300AD84
      5200CE846300BD4A1800000000000000000000000000087B080073FFA50052E7
      84004ADE730042D66B0031CE520029BD420021B5310018B5310010AD210008AD
      180008AD180008A51800087B08000000000000000000000000000000A5005A84
      FF000021FF000031FF000031FF000029F7000029EF000029E7000031DE000031
      D6000031CE000000A5000000000000000000EFEFEF00A59C940039211800D6D6
      D600DEDEDE00EFEFEF0094847B0094847B00CEBDBD00B5ADA500E7E7E700B5AD
      AD00C6BDBD00FFFFFF00E7E7DE00B5B5C60000000000C6C6C6008C8C8C00BDBD
      BD00C6C6C600B56B4A00AD845A0094CE8400E7CEC600BDDEB500ADAD8400CE8C
      6300CE7B5A00BD4A2100000000000000000000000000087B080073FFA50073FF
      A50073FFA50073FFA50073FFA50073FFA50029BD4A0073FFA50073FFA50073FF
      A50073FFA50073FFA500087B08000000000000000000000000000000A500ADC6
      FF006384FF00638CFF00638CFF006384FF006384F7006384F700527BEF004A73
      E7004A73E7000000A5000000000000000000CEC6C6008C847B00BDBDB500F7F7
      F700948C840084736B008C7B73008C7B7300C6BDBD00ADA59C00E7DEDE00B5AD
      A500AD9C9C00FFFFFF00EFEFEF00B5B5C60000000000DEDEDE00B5B5B500C6C6
      C600ADB5AD00B56B4A00C68463008CBD7B00BDD6B500BDDEB500DEB59C00CE8C
      6B00CE845A00BD4A180000000000000000000000000000000000087B0800087B
      0800087B0800087B0800087B080073FFA50039CE5A00087B0800087B0800087B
      0800087B0800087B080000000000000000000000000000000000000000000000
      A5000000A5000000A5000000A5000000A5000000A5000000A5000000A5000000
      A5000000A500000000000000000000000000B5B5BD00EFEFEF00EFEFEF00FFFF
      FF00D6CEC600CEC6C600E7E7DE00F7F7EF00D6D6C600FFFFFF00DED6CE00FFFF
      F700F7F7EF00FFF7F700FFF7F700B5BDC6000000000000000000B5B5B500EFEF
      EF009C9C9C00CE6B3900B5735200BD948400C6ADA500C6ADA500C6947B00C67B
      5A00D6845A00BD5A290000000000000000000000000000000000000000000000
      00000000000000000000087B080073FFA5004ADE7300087B0800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADA5A500E7B56300EFBD6B00CE9C
      5A00F7DEBD00F7D6AD00F7D6AD00F7D6A500F7CE9400F7CE9400EFD6A500EFC6
      7300D69C4200EFAD4A00EFAD4200CED6DE000000000000000000DEDEDE009CA5
      A50084524200ADADAD00C6C6C600D6D6D600E7E7E700E7E7E700D6D6D600C6C6
      C600ADADAD006B42310000000000000000000000000000000000000000000000
      00000000000000000000087B080073FFA50052E78400087B0800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDB5C600BDBDCE00C6C6
      CE00C6BDC600C6BDC600BDBDCE00BDBDC600BDBDCE00C6C6CE00CECED600D6D6
      E700EFEFF700EFEFF700FFFFFF00000000000000000000000000000000009C9C
      9C0094949400C6CECE00C6BDBD00E7E7E700CECECE00C6C6C600CECECE00CECE
      CE00C6C6C6009494940031313100000000000000000000000000000000000000
      00000000000000000000087B080073FFA50063F79400087B0800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009C9C
      9C00C6C6C600847B7300CECECE00B5B5B500BDBDBD00CECECE00CECECE00D6D6
      D60094848400E7E7E70029292900000000000000000000000000000000000000
      00000000000000000000087B080073FFA50073FFA500087B0800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D6D6D600C6C6C600B5B5B50094949400D6D6D600F7F7F700E7E7E700E7E7
      E700E7E7E7008C8C8C0000000000000000000000000000000000000000000000
      0000000000000000000000000000087B0800087B080000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EFEFEF00E7E7E700E7E7E700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6D6D6007373
      73007373730073737300AD737300292929006363630073737300737373007373
      7300737373007373730073737300ADADAD000000000000000000000000000000
      0000000000000000000000000000000000003131630000316300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000ADADAD00FFFF
      FF00FFFFFF00FFFFFF00F7D6D600BD737B0063635A00B5B5B500FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00737373000000000000000000000000000000
      00000000000000000000000000006363CE003131630000316300000000002121
      2100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000ADADAD00D6D6
      D600EFEFEF00D6D6D600F7F7F700C67B5200CE6300005A4A39006B6B6B009494
      94009494940094949400F7F7F700737373000000000000000000000000000000
      00000000000000000000000000006363CE003131630000316300000000000000
      0000000000000000000000000000000000000000000000000000000000001008
      0800100808000800000008000000000000000000000008000000100808001008
      0800000000000000000000000000000000000000000000000000000000006B94
      CE00317BC6003994C60094DED600A5DEDE005AC6C6007BCEC60073BDBD000000
      0000000000000000000000000000000000000000000000000000ADADAD00DEDE
      DE00CECECE00C6C6C600CECECE00C6CEC600D66B0800CE6300006B5A4A007373
      7300D6D6D60094949400FFFFFF00737373000000000000000000000000000000
      00000000000000000000000000006363CE003131630000004200000000000000
      0000000000000000000000000000000000000000000008000000100808000042
      730021BDFF0031CEFF0029BDFF0039CEFF0039CEFF0029BDFF0029C6FF00088C
      DE0008000800100808000000000000000000000000000000000094F7E7000084
      D600188CEF00107BE7006B9CD6000000000000000000000000000000000073CE
      CE0084BDBD00000000000000000000000000E7AD9400D6947B008C635A006B63
      6300ADADAD00C6C6C600FFFFFF00EFEFEF00FFF7F700D6731800CE5A0000635A
      42008C8C8C0084848400FFFFFF00737373000000000000000000000000000000
      00000000000000000000000000006363CE003131630000000000000000000000
      0000000000000000000000000000000000000000000008000000008CF700086B
      E700089CFF0018B5FF004ACEFF0073E7FF0063DEFF0039CEFF0018B5FF000073
      EF00006BEF00004AA5000808080000000000000000008CE7DE005A9CD600108C
      EF0039B5FF00219CF700217BC6004A8CC6005A8CCE004A8CCE005A9CD6000000
      0000000000007BCECE00A5BDB50000000000FFCEB500D6948400DE947B00D694
      7B00FFBD9C00D6947B0084737300F7F7F700FFFFFF00EFEFDE00D67B2100CE63
      0000BD7B630031393900ADADAD00737373000000000000000000000000000000
      00000000000000000000000000006363CE003131630000000000000000000000
      00000000000000000000000000000000000008000000086BE700087BEF0018AD
      FF004AD6FF007BEFFF00A5FFFF00C6F7FF00B5FFFF008CF7FF0063E7FF0039CE
      FF00109CF700085ADE0000295A00000000009CDEDE00000000008CB5E7000884
      E70029A5FF00188CEF00187BD6001884DE001084E700188CE700006BD600ADCE
      E70000000000000000002184D600318CE700FFD6BD00F7BD9C00E7C6A500EFD6
      BD00D69C8400F7BD9C00CE9C8C00CECECE00D6D6D600FFFFFF00BDA58400EFDE
      E700C65A0000C66363004A525200525252000000000000000000000000000000
      00000000000000000000000000006363CE003131630000000000000000000000
      00000000000000000000000000000000000000296300315AB500007BEF005AD6
      FF00DEF7FF00EFEFF700F7F7F700F7F7FF00F7F7FF00EFEFF700E7F7F700B5F7
      FF00009CFF001052C6003184EF0000000000000000000000000000000000428C
      D6000873D600187BCE00188CEF00188CF700188CF70018B5FF00188CF700297B
      C600000000000000000073A5DE009CC6CE00FFD6C600D69C8400DE9C8400C68C
      7300FFC6A500D69C8400CEA58C00D6D6D600DEDEDE00DEDEDE00DEDEDE00DEC6
      9C00E79C4A00CE6300007B6B6B00292929000000000000000000BDBDBD00ADAD
      AD000000000000000000000000006363CE000031630000000000000000000000
      0000000000000000000000000000000000002142A500005AC600B5FFFF007B73
      7300000000000000000000000000000000000000000000000000000000004239
      39009CB5B50084EFFF007B9CDE00000000000000000000000000000000000000
      0000000000001084DE001894F70029B5FF000084F70094DEFF00218CEF00317B
      C60000000000000000000000000094DEDE00FFDEC600F7BDA500E7C6A500EFCE
      BD00D6A58C00FFD6BD00CEA58C00EFEFEF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7EFDE00EF944200C6630800BDA5A5000000000000000000EFEFEF00BDBD
      BD00636363000000000000000000313163000000000000000000000000000000
      000000000000000000000000000000000000000831008CFFFF00183184000021
      5200100808001008080018101000181818001810100010101000100808000800
      0000104A9400397BC600B5D6E7000000000094BDBD0000000000000000000000
      0000000000000073D600219CFF00218CDE001884DE00106BBD000073D6000000
      0000000000000000000063C6BD009CDEDE00FFDED600D69C8C00E7A58C00C68C
      7B00FFC6A5008C8C9400CEAD9400E7E7E700F7F7F700F7F7F700F7F7F700F7F7
      F700EFEFEF00D6D6CE00DE8C29005A5252000000000000000000BDBDBD00ADAD
      AD00CECECE006363630063636300525252000000000000000000000000000000
      000000000000000000000000000000000000000000006BEFFF00397BBD000894
      FF000063D600008CEF000084AD00107B9C00087B9C00008CC600007BE700087B
      F700187BE7001039A5006BADD600000000000000000073ADAD00000000000000
      0000000000002984CE00187BD600188CE700107BE7001884DE005294D6000000
      00000000000084D6D60084DEE70000000000FFE7D600EFC6A500E7C6A500E7CE
      B500D6B59C0084B5FF00CEAD9400C6C6C600CECECE00CECECE00CECECE00CECE
      CE00C6C6C600CECECE00F7F7F700737373000000000000000000000000000000
      0000DEDEDE00BDBDBD008C8C8C00636363005252520000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF004284AD00008C
      FF00107BE70010A5F70000C6FF0010E7FF0021DEFF0010BDFF00108CEF000884
      EF00084AB5006BE7F7000000000000000000000000000000000073C6C60094C6
      C60094C6C600000000003173D6001084E7006BCEFF001894F7005294CE0094DE
      DE007BDEDE00000000000000000000000000FFE7DE00EFBD9C00E7B59C00EFBD
      9C00F7CEAD00F7CEB500CEB59C00E7E7E700FFFFFF00F7F7F700F7F7F700FFFF
      FF00EFEFEF00FFFFFF00F7F7F700737373000000000000000000000000000000
      0000ADADAD00DEDEDE00DEDEDE00ADADAD006363630000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B5EF
      F70042ADDE00008CE70000A5F70000C6FF0000B5FF000094EF002194DE0073D6
      EF00F7FFFF000000000000000000000000000000000000000000000000000000
      00000000000084DEDE0000000000006BCE00188CE7000063CE00000000000000
      000000000000000000000000000000000000FFEFE700E7946300FFA56B00FFA5
      7300FFAD7B00FFAD8400D6AD9C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B7B7B000000000000000000000000000000
      000000000000EFEFEF00003163006363CE009C9C9C009C9C9C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFEFE700FFD6B500E7A57B00E794
      7300EF8C5A00EF7B4A00DEBDAD00FFFFFF00CECECE00CECECE00CECECE00FFFF
      FF00B5B5B500BDBDBD0084848400EFEFEF000000000000000000000000000000
      00000000000000000000CECECE0000000000BDBDBD009C9C9C00636363006363
      6300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFE7CE00FFF7CE00FFFFDE00FFF7
      CE00FFF7D600FFFFD600E7CEB500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F7F7F7009C9C9C00F7F7F700000000000000000000000000000000000000
      000000000000000000000000000000000000DEDEDE0063636300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000ADADAD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0094949400F7F7F7000000000000000000000000000884AD000884AD000884
      AD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000001073
      AD0000639400000000000000000000000000000000000000000000000000006B
      A500086BA5000000000000000000000000000000000000000000000000000000
      0000C6948400C6948400C6948400C69484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A5948400A594
      8400A59484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000884AD00A5D6E700D6FFFF0094E7
      F7001884AD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000086B9C0052CE
      EF0031B5D600005A9400000000000000000000000000000000001084B50084E7
      F700088CBD00086B9C000000000000000000000000000000000000000000C694
      8400E7B59400F7CEAD00EFD6B500E7C6A500C6948400C6948400C6948400C694
      84000000000000000000000000000000000018731800CEC6AD00DECECE00DED6
      C600F7DECE00A5948400A5948400A5948400A594840000000000000000000000
      0000000000000000000000000000000000000884AD005ABDDE00A5CEDE00B5FF
      FF006BD6EF000884AD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002994
      BD005AF7FF0039B5DE0000639C000000000000000000188CBD00A5EFFF0042D6
      F7002994BD000000000000000000000000000000000000000000C6948400EFC6
      A500F7C69400F7D6B500F7DEC600F7DEB500D6AD9400946363009C736B00C694
      8400C6948400C69484000000000000000000108C2900CEDEBD00DED6CE00D6CE
      CE00FFF7E700FFF7DE00FFEFD600FFEFC600FFE7BD00A5948400A5948400A594
      840000000000000000000000000000000000000000000884AD004ABDDE009CCE
      DE009CF7FF0029A5CE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000863
      9C004AD6F70052EFFF004AC6E700006B9C002194BD00B5F7FF0052EFFF004AD6
      F700005A940000000000000000000000000000000000C6948400EFCEA500F7D6
      A500EFC69400F7D6B500F7E7D600F7DEC600D6B59C009463630094636300DEB5
      9400EFCEA500DEBD9C00C69484000000000000730800BDD6B500D6CECE00D6CE
      CE00F7E7DE00736B5A00736B5A00D6BDA500E7C6AD00E7C6A500FFDEAD00A594
      84000000000000000000000000000000000000000000000000000884AD0039BD
      DE00A5D6E7000884AD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000008639C0031DEFF0052E7FF005AD6F700A5EFFF0084EFFF0042DEFF000863
      9C0000000000000000000000000000000000C6948400F7DEAD00FFDEAD00F7D6
      A500EFC69400F7DEBD00FFF7E700FFEFD600D6BDA50094636300946B6300DEB5
      9400EFCEA500E7C69C00C694840000000000006B00009CBD9400DECED600D6CE
      CE00EFE7DE00D6CEBD00D6CEB500736B5A00736B5A00736B5A00FFD6AD00A594
      8400000000000000000000000000000000000000000000000000000000000884
      AD0029BDDE009CCEDE000884AD00088CBD000884AD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000008639C0031D6FF0052E7FF0073E7FF0084F7FF0008639C000000
      000000000000000000000000000000000000C6948400FFE7B500FFDEAD00F7D6
      A500F7C68C00F7DEC600FFFFF700FFF7E700DEBDAD0094635A0094636300DEB5
      9400EFCEA500E7C69C00C694840000000000006B000084AD8400D6CED600D6CE
      CE00EFE7E700736B5A00397B3100105A1000105A100018631800105A1000085A
      0800085A08000000000000000000000000000000000000000000000000000000
      00000884AD0018BDE7009CD6E7004AE7FF0010DEFF000884AD000884AD000884
      AD000884AD000884AD0000000000000000000000000000000000000000000063
      940008639C0008C6FF0018CEFF0031D6FF0052DEFF007BEFFF0063CEE7000863
      9C0008639C00000000000000000000000000C6948400FFE7B500FFDEA500EFCE
      A500C6B5A500FFDEC600FFFFFF00FFFFF700E7D6C600A57B73009C6B6300DEB5
      9C00EFCEA500E7C69C00C694840000000000006300006B9C6B00D6CED600D6CE
      CE00DED6D6008CA58400086B0800087B080000940000008C0000007B00000873
      080008630800085A0800085A0800086308000000000000000000000000000000
      0000000000000884AD0018BDDE0029C6E70018D6FF0021DEFF0031DEFF0039E7
      FF0042E7FF0052EFFF000884AD000000000000000000005A9400187BAD0052C6
      E70042E7FF0018CEFF0008C6FF0018CEFF0039D6FF005AE7FF0084EFFF0073E7
      F70029A5CE0029A5CE00005A940000000000C6948400FFE7AD00DED6BD006BBD
      EF00319CF7009CBDE700FFFFF700FFFFF700FFF7EF00F7E7D600E7CEBD00EFCE
      B500EFCEA500E7C69C00C6948400000000000000000042844200D6CECE00D6CE
      CE00D6D6D600217B2900086B100008A51000009C0800009C0000009C0000009C
      0000009C0000009C0000007B0000006300000000000000000000000000000000
      000000000000000000000884AD0039E7FF004ADEFF0052DEFF005ADEFF0063E7
      FF006BE7FF007BEFFF008CF7FF000884AD00106BA5006BBDD600D6FFFF00BDFF
      FF0084F7FF0052E7FF0018CEFF0008C6FF0018CEFF0039DEFF0063EFFF008CFF
      FF008CFFFF004AD6EF006BBDD600005A9400C6948400B5CED6005AC6FF004ABD
      FF004AADFF00319CFF009CCEFF00FFFFF700FFFFF700FFF7EF00FFEFDE00F7DE
      C600F7D6B500E7C6A500BD949400000000000000000031733100D6CECE00FFFF
      FF006B9C6B000863100018AD310010AD290008A5180000A50800009C0000009C
      0000009C00000084000000630000000000000000000000000000000000000000
      000000000000000000000884AD006BEFFF006BE7FF0073E7FF0084E7FF008CEF
      FF00A5F7FF009CE7F700ADF7FF000884AD00005A9400005A9400005A9400005A
      9400005A9400005A940052CEF70018CEFF0008CEFF0010A5DE00005A9400005A
      9400005A9400005A9400005A9400005A940063B5FF005AB5FF005AC6FF0063C6
      FF0052BDFF0042ADFF00319CFF00A5CEFF00FFFFF700FFFFF700FFF7E700FFEF
      D600EFD6C600AD9CAD00AD949C000000000000000000085A0800D6CECE00EFF7
      EF001063100021A5390021BD4A0018B5390010B5290010AD210008A5100000A5
      0800008C00000063000000000000000000000000000000000000000000000000
      000000000000000000000884AD0094F7FF009CEFFF00A5EFFF00ADEFFF00C6FF
      FF00D6F7F700298CB500D6FFFF000884AD000000000000000000000000000000
      00000000000000000000005A94004AE7FF0010CEFF00006BA500000000000000
      000000000000000000000000000000000000000000005AB5FF005ABDFF0063C6
      FF005AC6FF0052BDFF0042ADFF00319CFF00A5CEFF00FFFFF700FFFFF700DEDE
      DE008C94BD00948CAD00000000000000000000000000005A0000BDB5BD003984
      3900107B210031CE6B0029C65A0021BD4A0021BD420018B5310010B52900089C
      1000006B00000000000000000000000000000000000000000000000000000000
      000000000000000000000884AD00BDF7FF00BDF7FF00C6F7FF00DEFFFF00DEF7
      F700218CB5000884AD00DEFFFF000884AD000000000000000000000000000000
      00000000000000000000005A940073EFFF0031CEF700006BA500000000000000
      00000000000000000000000000000000000000000000000000005AB5FF005ABD
      FF0063C6FF005AC6FF0052B5FF0042A5FF00319CFF00ADD6FF00C6D6EF00638C
      CE006B84BD00000000000000000000000000000000000063000052734A001063
      100029C6630031CE630031CE6B0029CE630029CE520021BD4A0018AD3100006B
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000884AD00EFFFFF00CEF7FF00CEFFFF00ADDEE7002184
      AD000884AD00007BA500D6FFFF000884AD000000000000000000000000000000
      00000000000000000000005A940094EFFF0042BDE70000639400000000000000
      0000000000000000000000000000000000000000000000000000000000005AB5
      FF005ABDFF0063C6FF005AC6FF004AB5FF0039A5FF00319CFF004A84EF004A7B
      D600000000000000000000000000000000000000000000000000106B18002973
      390039A5630039945A00399C5A0031AD630031A55A0029C65A00087B10000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000884AD00F7FFFF00DEFFFF00A5DEEF00A5DE
      EF00B5EFF700CEF7FF00D6FFFF000884AD000000000000000000000000000000
      00000000000000000000005A9400ADEFFF0042A5CE00005A9400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005AB5FF0063BDFF0063C6FF0052C6FF0052ADF700314AC600526BE7000000
      0000000000000000000000000000000000000000000000000000424A42003952
      4200295A3100296331003163390031734200316B420010842100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000884AD000884AD000884AD000884
      AD000884AD000884AD000884AD00000000000000000000000000000000000000
      00000000000000000000005A9400BDE7EF0042A5C600005A9400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005ABDFF0063C6FF0000000000000000003129AD00526BE7000000
      000000000000000000000000000000000000000000000000000000000000424A
      4200424A4200424A420042524200425242003952390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000001873A500086B9C0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003131B500526BE7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000002129A5001821
      B500000000004A4A4A005A5A5A0084847B0084847B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000087B0000087B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084736B005A63BD002121
      B50063635A00A5A5A500FFFFFF00E7E7DE00D6CECE009C94940084847B007B73
      73005A5A5A000000000000000000000000000884AD000884AD00000000000000
      00000000000000000000000000000884AD000884AD000884AD000884AD000884
      AD000884AD000884AD0000000000000000000000000000000000000000000008
      84000029CE000029B50000087B00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD632900AD632900AD63
      2900AD632900AD632900AD632900AD632900AD632900AD632900AD632900AD63
      2900AD632900AD632900AD6329000000000000000000AD9484007B73C6002121
      B500BD947300A5A5A500FFFFFF00FFFFFF00FFFFFF00D6D6CE00CEC6BD00F7EF
      DE00EFE7D600A59C94006B6B6300000000000884AD004AB5DE00188CB5000000
      000029394A000000000029394A00217B9C00B5DEF700B5DEF700007BA5007BBD
      D600BDE7F7000884AD0000000000000000000000000000000000000000000008
      8C00738CFF000031DE000029BD0000087B000000000000000000000000000000
      000000000000000000000000000000000000DEA53900FFD69400EFB57300E7AD
      6300FFBD6300F7AD4A00DE9C3900FFAD3100FFAD2100FF9C1000FF9C0000FF9C
      0000FF9C0000FF9C0000FF9C0000AD63290000000000AD9484007B73C6002121
      B500BD9C7B00A5A5A500FFFFFF00FFFFFF009C9CA50073737300313131007B7B
      7300FFF7E700FFF7E70084848400000000000884AD008CCEEF004AADCE006B84
      8C0052737B00295A7300426B7B002994BD00C6E7F700CEEFF700007BA50094CE
      E700EFFFFF000884AD0000000000000000000000000000000000000000000000
      000000087B00738CFF000031DE000029BD0000087B0000000000000000000000
      000000000000000000000000000000000000E7AD1800EFC694005A4A4200524A
      4200D69C5A00846B420042393900BD843900D694290052423100524229005242
      310052422900634A2900F7940000AD63290000000000AD9484007B73C6002121
      B500BDA58C00A5A5A500FFFFFF00FFFFFF00CECECE00F7F7F700FFFFFF003131
      3100F7EFE700FFF7E70084848400000000000884AD00BDDEEF000884AD002939
      4A000000000029394A00000000000884AD007BC6E7000884AD00007BA5000884
      AD008CD6F7000884AD0000000000000000000000000000000000000000000000
      00000000000000087B00738CFF000031DE000029BD0000087B00000000000000
      000000000000000000000000000000000000E7AD1800FFD6A500CEA57300C694
      6B00FFBD6B00D69C5A00BD8C4200EFA54200EFA54200AD7B5200C6843900E78C
      1000B57B3100BD7B3100FF9C0000AD63290000000000AD948C007B73CE002121
      B500BDAD9400A5A5A500FFFFFF00FFFFFF00B5B5B50084848400A5A5A5003131
      3100FFF7EF00FFF7EF0084848400000000000884AD00007BA500000000000000
      00000000000000000000218CBD0063B5D6000884AD0000000000000000000000
      00000884AD0063B5DE000884AD00000000000000000000000000000000000000
      0000000000000000000000087B00738CFF000031DE000029BD0000087B000000
      000000000000000000000000000000000000E7AD1800EFC69C0063524A005A52
      4200D6A56B008C734A004A423900B5843900947B94002942DE006B63A500CE8C
      4A003142D600314ACE00EF941000AD63290000000000948C8400737BCE002121
      B500BDADA500A5A5A500FFFFFF00FFFFFF009C9C9C00313131007B7B7B00CECE
      CE00FFFFFF00FFF7EF0084848400000000000000000000000000000000000000
      0000000000000884AD00BDDEEF000884AD000000000000000000000000000000
      0000000000000884AD00B5DEEF000884AD000000000000000000000000000000
      000000000000000000000000000000087B00738CFF000031DE000029BD000008
      7B0000000000000000000000000000000000E7AD1800FFD6A500BD9C7B00B594
      6B00FFC67B00D6A56300A57B4A00EFAD4A00EFA552008C739400C68C5200FFA5
      10009C736B00A5735A00FF9C0000AD632900000000008C8C8C007B7BD6002121
      B500BDB5AD00A5A5A500FFFFFF00FFFFFF00BDBDBD0031313100313131003131
      3100FFFFFF00FFFFF70084848400000000000000000000000000000000000000
      0000000000000884AD00D6EFF7000884AD0000000000B5848400B5848400B584
      840000000000007BAD00DEEFF7000884AD000000000000000000000000003973
      DE00009CCE0000000000000000000000000000087B00738CFF000031DE000029
      BD0000087B00000000000000000000000000E7AD1800EFBD9400635A4A005A52
      4A00CEA573008C73520042423900B5844A009C8494002139E7006B63A500CE94
      4A00314AD600314ACE00E7941800AD632900000000008C8C8C007B7BDE002121
      B500B5B5BD00A5A5A500FFFFFF00EFEFEF00E7E7E700BDBDBD00A5A5A500DEDE
      E700FFFFFF00FFFFFF0084848400000000000000000000000000000000000000
      0000000000000884AD00C6E7EF000884AD0000000000B5848400F7D6AD00B584
      8400000000000084AD00CEEFF7000884AD0000000000000000000894CE0000BD
      EF0000BDEF00009CCE0000000000000000000000000000087B00738CFF000021
      B500004AAD00007BC6000000000000000000E7AD1800FFD6A500FFC69400F7C6
      9400FFCE9400FFC67B00EFB56B00FFBD6300FFB54A00E7A55A00FFAD3900FFAD
      2100EF9C2900F79C1800FF9C0000AD632900000000008C8C94007B84DE002121
      B500B5B5BD00A5A5A500FFFFFF00A5A5A500BDBDBD009C9CA500C6C6C600C6C6
      C600E7E7E700FFFFFF0084848400000000000000000000000000000000000000
      0000000000000884AD00B5E7EF000884AD0000000000B5848400FFE7BD00B584
      8400000000000084AD00BDE7EF000884AD00000000000000000000000000009C
      CE00009CCE00000000000000000000000000000000000000000000087B000063
      BD0000D6FF00009CCE00009CCE0000000000E7AD1800F7CEA500E7DEC600E7E7
      C600DEDEBD00E7DEBD00E7DEB500DED6AD00E7D69C00FFBD5A00FFAD3900F7A5
      3100F79C2100FF9C1000FF9C0000AD632900000000008C8C94007B84DE002121
      B500B5B5BD00A5A5A500FFFFFF00BDBDBD00B5B5B500ADADAD00ADADAD00BDBD
      BD00E7E7E700FFFFFF0084848400000000000000000000000000000000000000
      0000000000000884AD00BDE7F7000884AD0000000000BD8C8400FFEFCE00B584
      8400000000000084AD00C6E7F7000884AD000000000000000000000000000000
      00000000000000000000009CCE0000000000009CCE000000000000000000009C
      DE00009CCE0000D6FF00009CCE0000000000E7AD1800D6AD8C006BD6CE006BD6
      CE006BD6CE006BD6CE006BD6CE0063D6D6008CE7DE00FFC67B00CE8C3900B584
      3900AD7B3100C6842100FFA50800AD63290000000000949494007B84E7002121
      B500B5B5B500A5A5A500FFFFFF007B7B7B00E7E7E700FFFFFF00EFEFEF00BDBD
      BD00DEDEDE00FFFFFF0084848400000000000000000000000000000000000000
      0000000000000884AD0073BDD6000884AD0000000000CE9C8400FFF7E700B584
      8400000000000084B5009CCEE7000884AD000000000000000000000000000000
      00000000000000000000009CCE00009CCE00009CCE0000000000000000000000
      0000009CCE00009CCE000000000000000000EFB53900FFCEA500D6AD8400D6AD
      8400D6AD8400D6AD8400D6AD7B00CEA56B00DEAD6B00FFBD6B00F7AD5200FFAD
      4200E79C3100F7A52900FFA51000AD6329000000000084848C00636BD6001821
      BD006B7394009C9CAD00BDBDBD007B7B73008C8C8C00EFEFE700E7E7E7006363
      630094949400FFFFFF0084848400000000000000000000000000000000000000
      000000000000000000000884AD000884AD0000000000DEAD8400FFFFF700B584
      8400000000000884AD001084AD00000000000000000000000000000000000000
      000000000000009CCE0000CEFF0000CEFF0000C6F700009CCE00000000000000
      00000000000000000000000000000000000000000000E7A50800EFB53900F7BD
      4A00EFB54A00EFB54A00F7BD6300F7B55A00EFB54A00EFAD4200EFA53900E79C
      3100EF9C2100E7941800D6841800000000000000000000000000424263002931
      7B00313184003942940039399400424294004242630052529C006B6B9400736B
      8C005A5A6300948C8C0063636300000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7B58C00F7EFEF00BD84
      7B00000000000000000000000000000000000000000000000000000000000000
      0000009CCE00009CCE00009CCE0000BDF700009CD600009CCE00009CCE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004A4A4A0000000000393963004A4A4A0031316300000000003131
      8400393963001010730000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFBD9400CE9463000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000009CCE0000BDF700009CCE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004A4A4A004A4A4A0000000000000000004A4A4A004A4A
      4A00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DEAD8400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000009CCE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000021A5CE0029ADCE001084AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000004A
      000000000000000000000000000000000000000000000000000000000000004A
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000029A5
      CE0021A5CE000884AD00109CC60021ADD60031B5D600188CB5001084AD0042AD
      CE0039ADCE000000000000000000000000008C3110007B3110007B3110007B31
      10007B3110007B3110007B3110007B3110007B3110007B3110007B3110007B31
      10007B3110007B3110007B3110008C3110000000000000000000004A000010A5
      2100004A00000000000000000000000000000000000000000000004A000010A5
      2100004A0000000000000000000000000000A5310000B5390800E7422900F74A
      3100F74A31000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000031A5
      CE0031BDDE0010ADDE0010A5CE0018C6F7004AD6FF0031A5CE005AC6DE0063C6
      DE0039A5C6000000000000000000000000006B291000FFF7F700FFF7EF00FFF7
      E700FFEFDE00FFEFD600FFE7CE00FFE7C600FFE7BD00FFDEBD00FFDEB500FFD6
      AD00FFD6A500FFD6A500FFCE9C007B31100000000000004A000021BD420018AD
      310010A52100004A0000000000000000000000000000004A000021BD420018AD
      310010A52100004A00000000000000000000A5310000BD391000EF422900F752
      3900F75A4200F7634A00F7523900EF4229008C42100000000000000000000000
      00000000000000000000000000000000000000000000218CB500299CBD004AB5
      D60084DEEF0052C6DE0039CEEF0018D6FF0039DEFF005AD6F70073C6DE00B5E7
      EF0094CEE700188CB500000000000000000063291000FFFFF700FFF7EF00FFE7
      CE00F7D6A500F7D6A500FFFFFF00FFFFFF00FFD6AD00FFE7BD00FFDEB500FFDE
      AD00FFD6AD00FFD6A500FFCE9C0073291000004A000042DE730029C6520021B5
      420018AD290010A52100004A000000000000004A000042DE730029C6520021B5
      420018AD290010A52100004A000000000000AD310000C6391800F75A3900FF6B
      4A00FF6B4A00FFC6BD00FF8C7B00E73921008439100000730000089C180010AD
      310031BD4A0042B53900007B0000006300000000000063BDD6004AB5D60052BD
      E70094EFFF008CEFFF005AE7FF0021DEFF0018DEFF0052E7FF007BEFFF007BEF
      FF0039CEEF0031B5DE0029A5CE000000000063291000FFFFF700D6C6BD006342
      1800636363006363630094949400FFFFFF00FFFFFF00FFE7C600FFDEBD00FFDE
      B500FFD6AD00FFD6A500FFD6A5007329100000000000004A000039DE6B0029C6
      520021BD4200004A0000000000000000000000000000004A000039DE6B0029C6
      520021BD4200004A00000000000000000000B5310800D6422100FF6B4A00FF6B
      4200FF947B00FFFFFF00FFBDAD00EF4A29007B390800008C100018C6520018C6
      42009CF79C009CEF9C0008941000006B0000000000001094C60021A5D6004AC6
      EF0084DEF70094EFFF0073D6EF005ABDCE0052B5CE0029C6EF0000D6FF0000CE
      FF0008D6FF0008CEF70008B5E7000000000063291000FFFFFF006B4A31005A84
      7B0000BDFF0000BDFF0094EFFF0094949400FFFFFF00FFE7C600FFDEBD00FFDE
      B500FFDEB500FFD6AD00FFD6A500732910000000000000000000004A000039DE
      7300004A00000000000000000000000000000000000000000000004A000039DE
      7300004A000000000000000000000000000000000000D6422100FF734A00B552
      4200948C8C00B5BDBD00E78C7300FF5A3900944A100008A5210010B53900298C
      390094B58C006BBD630010AD31000073000000000000188CBD0029ADDE0039BD
      E7006BD6F70094C6D6009494940094949400949494009494940042BDDE0000D6
      FF0010D6FF0018D6FF00089CCE000000000063290800FFFFFF005A31100000BD
      FF00004A0000004A000000BDFF0063636300F7D6A500FFE7CE00D6842100D684
      2100D6842100D6842100FFD6AD0073291000000000000000000000000000004A
      000000000000000000000000000000000000000000000000000000000000004A
      000000000000000000000000000000000000000000000000000042426300184A
      94001052AD00104A9400734A5200000000000000000000210000001800000818
      4A0008186300082939001094310000000000000000001894BD0021ADDE0031B5
      E7005ACEF70094949400EFE7E700B5B5B500ADA5A500E7B5B500949494005ADE
      FF009CF7FF00B5EFFF00399CBD000000000063290800FFFFFF005A31100084FF
      FF00004A0000004A000000BDFF0063636300F7D6A500FFE7CE00D6842100D684
      2100D6842100D6842100FFD6AD00732910000000000000000000000000000042
      0000000000000000000000000000000000000000000000000000000000000042
      000000000000000000000000000000000000000000001063A500086BD600188C
      FF001884FF00187BF7001863C600000000000000080000000000000829000829
      9C0000299C0000188C00002973000000000000000000108CBD00189CCE0021AD
      DE0042C6EF0094949400EFE7E700B5B5B500ADA5A500DEB5B500949494007BE7
      FF00ADEFFF008CCEE7004AA5C6000000000063291000FFFFFF006B5231009C9C
      9C00FFFFFF0000BDFF009C8C7B0063421800FFDEBD00FFEFD600FFE7CE00FFE7
      C600FFDEBD00FFDEB500FFDEB50073291000000000000000000000000000004A
      0000004A00000052000000520000004A00000052000000520000004A0000004A
      000000000000000000000000000000000000186B8C002984C6002994FF00299C
      FF00299CFF00299CFF002994FF00106BC6000000000000000000083194000842
      C6000842B5000842B5000831B50000108C0000000000000000000884AD0021A5
      D60031BDEF0094949400EFE7E700B5B5B500ADA5A500DEB5B5009494940063E7
      FF006BCEE700007BA500000000000000000063290800FFFFFF00D6CEC6006B52
      31005A3110005A3110006B4A3100CEC6B500FFEFDE00FFEFDE00FFEFD600FFE7
      CE00FFE7C600FFDEBD00FFDEB500732910000000000000000000000000000000
      0000000000000000000000000000003108000000000000000000000000000000
      000000000000000000000000000000000000186B8C00298CD60031A5FF00319C
      FF00299CF700299CF70031A5FF00217BCE000000000000001000104ABD001052
      D600104AC6001052CE00104AC6000831A500000000000000000000000000108C
      B500088CBD0094949400EFE7E700B5B5B500ADA5A500E7BDB50094949400189C
      C6002194BD000000000000000000000000006B291000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFF700FFF7EF00FFF7EF00FFF7E700FFEFDE00FFEFD600FFE7
      CE00FFE7C600FFE7BD00FFDEBD007B3110000000000000000000000000000000
      0000000084000000840000008400000084000000840000008400000084000000
      000000000000000000000000000000000000186B8C00186B8C00298CC6002994
      E700319CFF00319CFF0031A5FF001873C6000000000000000000104AA500187B
      FF001873F700187BF700105AD60010429C000000000000000000000000000000
      00000000000094949400E7E7E700ADADAD00ADA5A500DEB5B500949494000000
      0000000000000000000000000000000000009C522100CE944200C68C4200C68C
      4200C68C4200C68C4200C68C4200C68C4200C68C4200C68C4200CE9C5200CE94
      4200CE9C5200C6944A00BD945A00A55221000000000000000000000000000000
      0000000084000029F7000021FF000018FF000010DE000008AD00000084000000
      000000000000000000000000000000000000186B8C00186B8C00186B8C002994
      D60031A5FF0031A5FF00319CF700104A8C00000008001010080010213900084A
      9C001063BD00186BC6000831730010429C000000000000000000000000000000
      00000000000094949400B5B5B500A5A5A5009C949400ADA5A500949494000000
      0000000000000000000000000000000000009C421000CE631000D6631000CE63
      1000CE631000CE631000CE631000CE631000CE631000D66B1800F7AD6300E773
      1000F7AD63009C6339003152C600A54208000000000000000000000000000000
      0000000084001039EF000831FF000021FF000018FF000008D600000084000000
      00000000000000000000000000000000000000000000186B8C00186B8C00186B
      8C00186B8C00186B8C00186B8C000000000000000000101010004A4A42004242
      4200101018000008100000000800000000000000000000000000000000000000
      00000000000094949400E7E7E700C6C6C600A5A5A500B5A5A500949494000000
      0000000000000000000000000000000000000000000094421800944218009442
      18009442180094421800944218009442180094421800944218009C4218009439
      10009C4218008C39180084392100000000000000000000000000000000000000
      000000008400214AEF00214AFF000839FF000029FF000010E700000084000000
      0000000000000000000000000000000000000000000000000000186B8C00186B
      8C00186B8C00186B8C0000000000000000000000000000000000393939004242
      3900100808000000000000000000000000000000000000000000000000000000
      00000000000094949400E7E7E700EFEFEF00BDBDBD00A59C9C00949494000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000084002952F700316BFF00215AFF001042FF000029F700000084000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000094949400949494009494940094949400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000084000000840000008400000084000000840000008400000084000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000900100000100010000000000800C00000000000000000000
      000000000000000000000000FFFFFF00FFFF000000000000FFFF000000000000
      FC3F000000000000E00F000000000000C0030000000000008001000000000000
      0000000000000000000000000000000000000000000000008001000000000000
      80BF000000000000C01F000000000000F03F000000000000FFFF000000000000
      FFFF000000000000FFFF000000000000FE7FFFFFFDFBF9FFF01F8001F8F1E0FF
      C01F8001FDFB807FC0038001F000003F80078001E7FF001F80018001EFFF000F
      00018001E07F000700038001E00F000380008001E001000180008001C0018001
      80018001C0018001C0018001D001C001C00380019011E001F00380018001F007
      FA0F80013C7FFC0FFE7FFFFF3EFFFE3FFEBFC1FFC001FE07FFBF803F8000E001
      C3BF00070000E00180FF000300000000000F0003000000000001000300000000
      000300030000000000010003000000010000000300008003000000030001C007
      8000000300018007800000030007800700008003800780070001E307C207E007
      FF81E03FFFFFE607FFFFF07FFFFFFFFFFFFFFFFFFE3FFFFFE3BFE3FFF81FFFFF
      C30781FFFC1FFFFF820300FFFC1FFFFF0C03807F87FFFF871731803FC7FFFF03
      07B1C01FCFFFFF0307E1C00FFC27E70387C3F007BE07810307B3E007D8010047
      FF1FF001D00000838E0FF800E00181C78407FE04F001C0C7831FFF81F803E40F
      C03FFFE1FC07FE1FFFFFFFF3FFFFFFFFFFFFFFFFFFFFF807FFFFF83FFFCFC003
      FCFFF01FFF4BC003F07FE00FFE01C001803FE00FFF03C001000FE00FFC00C001
      000FE00FF400C0010007E00F8003C0038003F01F8001C003C001F83F80CBC007
      E000FC7F084FC007F001FC7F80FF800FF801FC7F80FF800FFC0FF83F80FFC01F
      FE7FF83FF7FFC03FFFFFF83FFFFFE03FFC7FFFFFFF7F0000F01FE1FFFC1F0000
      C007C07FF00700000001801FC00100000000800F0000000000030E0700010000
      00030F8300010000000307C300010000C00183E100010000F00181E100070000
      FC07C0E1C0070000FF1FC001C0070000FFFFE00141C30000FFFFF003C3C3FFFF
      FFFFF80FC7EFFFFFFFFFFFFFDFDFFFFFFFFFF9FFFEFF0000FFFFE07FFC7F0000
      83C3E07FFC7F00008181C01FFC3F00008081C007F81F00008081E003F00F0000
      C0C3F00380030000E0C3F00100010000FFC3E00180010000FFC38001E00B0000
      F0E70001F01F0000E0E70000F00F0000E0670000E0070000E0670081D42B0000
      E0FF81C3CE730000F9FFC3FF1FF90000FFFFFFFFFEFFC003F1FFFFFFF83FC000
      C0FFFFCFE00FE007007FFFC78007C007003FFFC380078003801FF00180078003
      C00FE00180078003E007C20380078003F00780478007C007F847004F8003C007
      FC0F805F8000E00FFF3FC3FF0003F83FFFF7E3FF0001FC7FFFFFF3FF0003F83F
      FFFFFFFFC30FF03FFFFEFFFFF7BFF01FFF1FF81FFFFFFFFFFC0FF81FFC1FFEFF
      F0070000E007F87FC00300008001E07F800100000000803B8001000000000001
      80010000000000008001000000008001800100000000800B800100000000C00F
      C00100000000C007800100000000E00F80030000C007E03F80070000FE7FF0FF
      C03F0000FFFFF3FFC1FF07E0FFFFFFFFFFFFFFCFFF3F8000E001FF03F80F0000
      E001FC01F0078001E001F001F0010001E001C001F0008000E0018001F0000001
      E0018001C0008000E001800100000001E0018001000080008001800100010001
      000180010007800000018007001F00010001800F003FFFFF8001C03FE07FFFFF
      8003F0FFFCFFFFFFE007FFFFFFFFFFFFC001FFFFF9FFFFFFC001FF9FF03FFC7F
      C001F80FE007F01FC0018007C001C007C001000380018003C001000380008003
      C001000100008003C001000000008003C001000000018003C001800000070001
      C001E000E01F0001C001F00FE03F0001C001F03FF01F0001C001E83FF03FC007
      C001F83FF87FF11FFFFFF8FFFCFFFFFFFFFFFFFFFE00FFFFFFCBF01FFE00FFFF
      FF03E007FE00FCFFFF03C003FE00F87FFE118003FC00F03FFE0180010000E01F
      E40300010000C10FE00300010000E0C7800F00010000E063003F00010001C039
      847F80010003821C843F8001003FC78F803FC003007FEFC7E0FFC007007FFFF3
      E0FFF00F00FFFFF9F5FFFC7F01FFFFFFFFFFFF7FFFFFFFFFFFFFFE7FFFFF07FF
      FFBFF87FFF9F07FFFE7FE07FFF8F07FFFC7EC07FFF0707FFF000001F8003067F
      C00000078003063F800180838001860F0001E0C1000082038001F0E10003C001
      8003FCE00007F001C1FFFEE03C1FFE03E1FFFFE0FC3FFE0FF1FFFFC0FCFFFE1F
      FBFFFFC0F9FFFE7FFFFFFF80FFFFFEFFFC7FF81FFFFFFFFFF81FE007FDFFFFFF
      F00FC003F0FFFF0FE0078001E03FFC03C0030001C01FF00180010000C001E007
      E00700008001E00FE00300008003E00FC00300000003E00FC00300008003E00F
      C0030000C003E00FC0030000C003E00FC0038001C003E01FC0078003F803F07F
      E007C007F803F1FFF00FF00FFFFFFFFFFFC3FCFFE77F0000F881F03FE03F0000
      E001C00FC003000080018003E001000000018001800100000001800100030000
      0001800300010000000180078000000000008007000000000000800780000000
      0001800FC00000000009800FF00000000009800FF00000000011C01FF0070000
      C079F03FF80F0000F1FFFC3FF8CF0000FDFFFFFFFFF3FFFFF047FF83FFC38000
      8001FE00FF0380000001F801FC0380000001E007F00380000001800FC0038000
      0001000F800380000001000F800380000001800F800380008001000780038000
      C0018007800F8000E001001F803F8000E003007F807F8000E60781FF807FC001
      EF8707FF8C7FFFFFFFEF1FFFFC7FFFFFFFFFFFFFF8FFFFFFFE3FFE3FF03FFFFF
      FC1FFC1FC00F861FD001C0018003021F800180010000021F8001800100000103
      8001800100008003800180010000E00180018001000000018001800100000001
      800180010000000080018001000000008001800180008000C007C0078000801F
      F01FF01FB803C3FFFC7FFC7FFE0FFFFFFFFFFE7F8FFFFFFFFFFFFC1F80FFFC7F
      FD7FF807800FF01FF83FF0010007C007F01FE00000078003F10FC00100078003
      F10F800100038003FD1F000100038003FC3F000000018003F07F000100018003
      F07F000300018003F11F00070003C007F01F800F8001F01FF83FE01FE001FC7F
      FC7FF83FF803FFFFFD7FFE7FFE07FFFFFFFFDFFFFFFF80FFFFFF9FFFFF8F007F
      FC1F9FFFFF87003FF00F9FFFFFC3000FC0038FBF00610007C0018F9F00700000
      8000878F007000008000C00703F800008000C00301F880008001E00100F0FE00
      0001F0031800FE000F03FC071C01FE00FFC3FF8F1E03FE00FFF7FF9F1F07FE00
      FFFFFFBFFFFFFE01FFFFFFFFFFFFFE031FFFFFFFF81FF81F0FFFF1FFE007E007
      07FFE1FFC003C00383FFC3FF80018001C1FF860080018001E10F8E0000000000
      F0030E0000000000F8011FC000000000F8011F8000000000F8000F0000000000
      F800001800000000F800803880018001F801C07880018001FC01E0F8C003C003
      FE03FFFFE007E007FF0FFFFFF81FF81FFFFFDE03FFFFFFFFFFFF0000FE7FFFFF
      FFFF0000FC3FFFFFFFFF0001FC3FFFFF00000000FC3FFFFF00000003FC3FFFFF
      00000003C003E007000000038001C003000080038001C00300008003C003E007
      0000C003FC3FFFFF0000C003FC3FFFFF8001E001FC3FFFFFFFFFE001FC3FFFFF
      FFFFF003FE7FFFFFFFFFFE3FFFFFFFFFFF3FFFFFFFFFC000FE1FFFFFFFFFC000
      FE0FF83FFFFFC000FE0FC007E01FC000FE3F8001C1E70000FE3F000180190000
      FE3F0000400C0000CE7F0000E00C0000867F0FE0F80E0000807F0001781C0000
      C07F8001B8190000E07F8003C4070000F03FE007FA3F0000F01FFFFFFFFF0000
      F807FFFFFFFF0001FE1FFFFFFFFFC0038FFFE7E7F0FFC7FF07FFC3C3E00F007F
      03FFE187C003000F83FFE0078001000FC3FFF00F0001000FE07FF81F00010007
      F003E00700010000F801800100018000FC00000000018001FC00000000018003
      FC00FC3F80038007FC00FC3FC007800FFC00FC3FE00FC01FFE00FC3FF01FC03F
      FF01FC3FF99FE07FFFFFFE7FFF9FFFFFFFFFC87FFFFFF3FFFFFF80073E03E1FF
      800180011403E0FF000080010003F07F000080010A03F83F000080013C71FC1F
      00008001F8F8FE0F00008001F888E70700008001F888C38300008001F888E7C1
      00008001F888FD6100008001F888FC7300008001FC89F83F8001C001FF8FF01F
      FFFFFA23FF9FFC7FFFFFFCCFFFBFFEFFFE3FFFFFEFEFFFFFE0070000C7C707FF
      E00700008383007F8003000001010000800100008383000080010000C7C78000
      80010000EFEFC18180010000EFEF810180010000E00F0000C0030000FEFF0000
      E0070000F01F0000F81F0000F01F0000F81F0000F01F8181F81F8001F01FC3C3
      F81FFFFFF01FFFFFFC3FFFFFF01FFFFF00000000000000000000000000000000
      000000000000}
  end
  object im24: TImageList
    Height = 24
    Width = 24
    Left = 708
    Top = 44
    Bitmap = {
      494C010104000900040018001800FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000600000003000000001002000000000000048
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000884AD001894
      BD0021A5CE000884AD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A5CE0039D6
      F70042CEEF0031B5D6000884AD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000884AD0039B5D60029ADCE00108CB5000884AD001094BD0018B5DE001094
      BD0029ADCE001894BD001084B5000884AD00218CB50042ADCE0052BDDE000884
      AD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00002194BD0039B5DE0031BDDE0029CEF70018BDEF0010ADDE00109CC60021D6
      FF004ADEFF0042BDDE0031A5C60073D6EF009CEFFF006BCEE70042ADD6000884
      AD0000000000000000000000000000000000AD4A0000C6521800E75A3900F763
      4A00F7634A00FF735200FF7B5A00FF7B5A00F7634A00F7634A00EF634A00D65A
      3900000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AD632900AD632900AD63
      2900AD632900AD632900AD632900AD632900AD632900AD632900AD632900AD63
      2900AD632900AD632900AD632900AD632900AD632900AD632900AD632900AD63
      2900AD632900AD632900AD63290000000000A5522900A5522900A5522900A552
      2900A5522900A5522900A5522900A5522900A5522900A5522900A5522900A552
      2900A5522900A5522900A5522900A5522900A5522900A5522900A5522900A552
      2900A5522900A5522900A5522900A55229000000000000000000000000000000
      00000884AD004AB5D6005ABDDE002194BD00109CC6001094BD0029D6FF0018D6
      FF0039DEFF005AE7FF00299CBD0031A5C6002994BD0063B5CE008CD6E7000884
      AD0000000000000000000000000000000000B54A0000CE522100EF5A4200F76B
      5200FF846300FF8C6B00FF845A00FFB5A500FFB5AD00F75A4200EF634200CE5A
      3100AD4A2100007B00000084000000840000007B0000007B0000007B0000007B
      000000000000000000000000000000000000BD840000EFB56B00FFC68400FFC6
      7B00FFBD7300FFBD6300FFBD5A00FFB55200FFB54A00FFAD3900FFAD3100FFAD
      2900FFA52100FFA51800FF9C0800FF9C0000FF9C0000FF9C0000FF9C0000FF9C
      0000FF9C0000FF9C0000EF8C0800AD632900A5522900FFF7EF00FFF7EF00FFEF
      CE00F7DEBD00F7DEB500FFFFFF00FFFFFF00FFE7BD00FFEFD600FFE7CE00FFE7
      CE00FFE7C600FFE7C600FFDEBD00FFDEBD00FFDEB500FFDEB500FFDEB500FFDE
      AD00FFDEAD00FFD6AD00FFD6AD00A5522900000000000884AD001884B50031A5
      C600188CB50073DEF70094EFFF008CEFFF0063D6EF004ADEFF0031DEFF0018D6
      FF0029D6FF0052DEFF0073E7FF0084DEEF00C6F7FF00CEF7FF00CEF7FF003194
      BD002194BD000884AD000000000000000000B54A0800D6522900EF6B4A00FF84
      6300FF8C6300FF846300FF846B00FFEFEF00FFF7F700F7735A00E75A4200C652
      2900AD4A210000940000009C0000009C000008A5100010B5290008A50800009C
      000000940000007B00000000000000000000CE941800FFCE9400FFC68C00F7BD
      7B00CE9C6300E7AD6300FFBD6300FFB55A00E7A54A00CE944200E79C3900FFAD
      3100FFA52100FFA51800FFA51000FF9C0800FF9C0000FF9C0000FF9C0000FF9C
      0000FF9C0000FF9C0000FF9C0000AD632900A5522900FFFFF700DECEC6007B5A
      2900845A3100845A3100947B6300F7EFEF00FFFFFF00FFEFD600FFE7CE00FFE7
      CE00FFE7C600FFE7C600FFE7BD00FFDEBD00FFDEBD00FFDEB500FFDEB500FFDE
      B500FFDEAD00FFDEAD00FFD6AD00A5522900000000002994BD00ADEFFF007BD6
      EF002194BD006BD6F7008CE7FF0094EFFF007BEFFF005AE7FF0042DEFF0021D6
      FF0018D6FF0042DEFF006BE7FF008CEFFF00ADEFFF009CEFFF006BE7FF001894
      BD005ACEEF0042BDE700108CB50000000000BD4A0800DE5A3100F77B5A00FF8C
      6B00FF8C6300FF7B5A00FFB5AD00FFFFFF00FFFFFF00FF947300EF734A00BD52
      2900AD4A2100008C0000009C000018B5390031CE6B0029C652008CE78C0021AD
      210000940000008C00000073000000000000D69C2100FFCE9400DEAD7B003131
      31003131310031313100A5845200DEA55200313131003131310031313100CE8C
      3100FFAD29004239310031313100313131003131310031313100313131003131
      310042393100E78C0800FF9C0000AD632900A5522900FFFFF700846B4A007B7B
      5A00FFFFFF00FFFFFF0094A59400947B6300FFFFFF00FFEFD600FFEFD600FFEF
      CE00FFE7CE00FFE7C600FFE7C600FFE7BD00FFDEBD00FFDEBD00FFDEB500FFDE
      B500FFDEAD00FFDEAD00FFD6AD00A5522900000000001884AD00188CB5001894
      BD0029A5D60063CEEF007BDEF7009CEFFF0084EFFF006BE7FF004ADEFF0029DE
      FF0018D6FF0021D6FF0031DEFF0021D6FF0010D6FF0010D6FF0010D6FF0010C6
      EF0008ADD60010A5CE00108CB5000000000000000000EF633900FF8C6B00FF8C
      6B00FF845A00F78C7300FFEFEF00FFFFFF00FFFFF700FF8C6B00FF846300C65A
      3100AD4A18000094000021BD4A0039D6730029C65A0052CE6300DEFFDE006BD6
      6B0000940000008C00000073000000000000D69C2100FFCE9C00FFCE9400A58C
      63009C7B5A009C7B5200F7B56B00F7B56300A57B4A009C734200A57B4200F7A5
      3900FFAD3100A57331009C6B29009C6B21009C6B21009C6B18009C6318009C63
      1800A56B1800FF9C0000FF9C0000AD632900A5522900FFFFF700734A2100DEE7
      E700EFEFF700FFFFFF00FFFFFF00845A3100F7DEB500FFEFDE00DE9C3900DE9C
      3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C
      3900DE9C3900FFDEAD00FFDEAD00A5522900000000000884AD0021ADDE0029AD
      E70039B5E70052C6EF0073D6F70094EFFF0094EFFF007BDEF70073B5C60063AD
      BD0073A5AD0052B5C60021CEF70010D6FF0010D6FF0010D6FF0010D6FF0010D6
      FF0010D6FF0010D6FF0008A5CE000000000000000000000000009C4A3900EF7B
      5A00B5634A008C7B84008C949C00A5ADAD00CEB5AD00F77B5A00FF8C6B00DE73
      4A000073000010A5210031D66B0031CE6B0029C64A00ADF7AD00E7FFE7007BDE
      7B0010B53900009408000073000000000000D69C2100FFCE9C00FFCE9C00E7B5
      8400CEA57300DEAD6B00FFC67300FFBD6B00DEA55200CE944A00DE9C4200FFAD
      4200FFAD3900CE945200BD8C5A00CE8C4200FF9C1000FF9C0000CE843100BD84
      4200CE843100FF9C0000FF9C0000AD632900A5522900FFFFF700734A2100DEE7
      E700DEE7E700EFF7F700FFFFFF00845A3100F7DEB500FFEFDE00FFEFD600FFEF
      D600FFEFCE00FFE7CE00FFE7C600FFE7C600FFE7BD00FFDEBD00FFDEBD00FFDE
      B500FFDEB500FFDEB500FFDEAD00A5522900000000000884AD00108CBD0029AD
      E70029B5E7004AC6EF006BD6F70084E7FF0094CED600A5A5A500BDBDBD00ADAD
      AD00A5A5A500B5A5A5009CA5A50042BDD60010D6FF0010D6FF0010D6FF0010D6
      FF0010D6FF0010BDEF000884AD0000000000000000000000000000000000294A
      73002163A500186BB5001863B500185A9C00395A8400735A6300000000000000
      000000000000088C180031CE630021AD42004A944A008CAD8C00A5C69C0052B5
      520029CE630018AD3100006B000000000000DE9C2100FFCE9C00DEB584003131
      31003131310031313100A5845A00CE9C6300313131003131310031313100C68C
      42009C8484000031FF000031FF000031FF00BD8452008C6B7B000031FF000031
      FF000031FF00CE843100FF9C0000AD632900A5522900FFFFFF00846B4A009C84
      6B00DEE7E700DEE7E7009C7B5A007B5A2900FFE7CE00FFEFDE00FFEFDE00FFEF
      D600FFEFD600FFE7CE00FFE7CE00FFE7C600FFE7C600FFE7BD00FFDEBD00FFDE
      B500FFDEB500FFDEB500FFDEAD00A5522900000000000884AD00319CBD0021A5
      D60029ADE70039BDE7005ACEEF007BDEF700949C9C00E7E7E700CECECE00ADAD
      AD00A5A5A500C6ADAD00DEBDBD00A5A5A50018CEF70010D6FF0031DEFF0052DE
      FF0052CEEF001084B5000884AD00000000000000000000000000105A94002184
      DE002994FF003194FF003194FF002994F700187BD6001063AD00000000000000
      00000000000000000000003908000029080010216B000821730010295A00084A
      3100108C2900088408000000000000000000DEA52100FFCE9C00FFCE9C00B594
      73009C846300A5846300F7BD7B00FFC67300B58C5A009C7B4A00B5844A00F7AD
      4A00EFAD4A009C7B8400846B9C009C7B7B00FFA51800EF9C21008C6B73008463
      84009C736300FF9C0000FF9C0000AD632900A5522900FFFFFF00DED6CE00846B
      4A00734A2100734A2100846B4A00DECEBD00FFF7E700FFEFDE00FFEFDE00FFEF
      D600FFEFD600FFEFCE00FFE7CE00FFE7C600FFE7C600FFE7C600FFE7BD00FFDE
      BD00FFDEB500FFDEB500FFDEB500A5522900000000000884AD001894C60029AD
      E70029ADE70031B5E70052C6EF006BD6F700ADADAD00EFEFEF00CECECE00ADAD
      AD00A5A5A500C6ADAD00DEBDBD00C6ADAD006BC6DE0094EFFF00BDF7FF00CEF7
      FF00CEF7FF006BBDD6000884AD000000000000000000398CB500318CDE0039A5
      FF0039A5FF0039A5FF0039A5FF00399CF700399CF7002984D6002173BD000000
      000000000000000000000000000008184A001042AD001042AD001031A5000829
      8C0000000000000000000000000000000000DEA52100FFCE9C00FFCE9C00DEB5
      8400A58C6B00CEA57300FFC68400FFC67B00CE9C63009C7B4A00CE945200FFB5
      5200FFB54A00BD8C6B00846B9C00BD8C6300FFA52100FFA51800BD844A008463
      8400BD844200FF9C0000FF9C0000AD632900A5522900FFFFFF00FFFFFF00FFEF
      DE00F7DEBD00F7DEB500FFFFFF00FFFFFF00FFE7CE00FFF7E700FFEFDE00FFEF
      DE00FFEFD600FFEFD600FFE7CE00FFE7CE00FFE7C600FFE7C600FFE7BD00FFDE
      BD00FFDEBD00FFDEB500FFDEB500A5522900000000000884AD001894BD0021A5
      D60029ADE70029B5E70042BDEF0063CEF700ADADAD00EFEFEF00CECECE00ADAD
      AD00A5A5A500C6ADAD00DEBDBD00C6ADAD006BCEE70084EFFF00ADEFFF00C6F7
      FF00A5DEEF0063B5CE000884AD0000000000000000003994C60042A5F70042AD
      FF0042ADFF0042ADFF0042ADFF0042ADFF0042ADFF0042ADFF00297BBD000000
      0000000000000000000000102900184AB5001852BD00184AB500184AAD00104A
      AD0010319C00000000000000000000000000DEA52100FFCE9C00CEAD84003131
      310031313100313131009C7B5A00CEA56B00313131003131310031313100B584
      4A009C8494000031FF000031FF000031FF00BD8C5A009C7B73000031FF000031
      FF000031FF00BD844200FF9C0000AD632900A5522900FFFFFF00DED6CE007B5A
      31007B7B7B007B7B7B00A5A5A500FFFFFF00FFFFFF00FFF7E700FFF7DE00FFEF
      DE00FFEFDE00FFEFD600FFEFD600FFE7CE00FFE7CE00FFE7C600FFE7C600FFE7
      BD00FFDEBD00FFDEB500FFDEB500A55229000000000000000000000000000884
      AD00108CBD0029ADE70039BDE7005ACEEF00ADADAD00EFEFEF00CECECE00ADAD
      AD00A5A5A500C6ADAD00DEBDBD00C6ADAD0063CEE70073E7FF009CEFFF002994
      BD000884AD000000000000000000000000004294BD00429CD60042ADFF004AB5
      FF004AB5FF004AB5FF004AB5FF004AB5FF004AB5FF004AB5FF003994DE000000
      0000000000000000000010397B001863D600185AC600185AC600185AC6001863
      CE001852BD0010319C000000000000000000E7A52100FFCE9C00FFCE9C00DEB5
      84009C846B00C69C7B00FFCE8C00FFC68400C69463009C7B5200C6945A00FFBD
      5A00FFB55200AD8C84008473A500BD8C6B00FFAD2900FFA52100AD7B6300846B
      8C00BD844200FF9C0000FF9C0000AD632900A5522900FFFFFF00846B52007B9C
      940000CEFF0000CEFF00ADEFFF00A5A5A500FFFFFF00FFF7E700FFF7E700FFEF
      DE00FFEFDE00FFEFD600FFEFD600FFEFCE00FFE7CE00FFE7C600FFE7C600FFE7
      C600FFDEBD00FFDEBD00FFDEB500A55229000000000000000000000000000000
      0000108CBD0029ADE70031B5E7004AC6EF00ADADAD00EFEFEF00CECECE00ADAD
      AD00A5A5A500C6ADAD00DEBDBD00C6ADAD005ACEE7006BE7FF008CEFFF001084
      AD00000000000000000000000000000000004294BD0042A5D6004AB5FF004AB5
      FF004AB5FF004AADFF0042A5F70042A5F7004AB5FF004AB5FF00399CDE000000
      00000000000000000000184A9C002173E700216BD600216BD600216BD600216B
      DE00216BDE0010399C000000000000000000E7AD2100FFCE9C00FFCE9C00FFCE
      9C00FFCE9C00FFCE9C00FFCE9400FFCE8C00FFC67B00FFC67300FFBD6B00FFBD
      6300FFB55A00FFB55200FFB54200FFAD3900FFAD3100FFA52900FFA51800FFA5
      1000FF9C0800FF9C0000FF9C0000AD632900A5522900FFFFFF007352210000CE
      FF00006300000063000000CEFF007B7B7B00F7DEB500FFF7EF00DE9C3900DE9C
      3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C3900DE9C
      3900DE9C3900FFDEBD00FFDEBD00A55229000000000000000000000000000000
      00000884AD00108CBD00108CBD00219CC600ADADAD00EFEFEF00CECECE00ADAD
      AD00A5A5A500C6ADAD00DEBDBD00C6ADAD00399CBD00219CBD00299CBD000884
      AD00000000000000000000000000000000004294BD00429CC60042A5D60042A5
      D60042A5DE004AB5FF004AADFF004AADFF004AB5FF0042ADFF003994D6000000
      00000000000000000000184A8400298CF700297BE7002173D6002173DE00297B
      E7002173D60010319C000000000000000000E7AD2100FFCE9C00FFD69C00FFDE
      B500FFDEB500FFDEB500FFDEB500FFD6AD00FFD6AD00FFD69C00FFD69400FFCE
      8C00FFCE8400FFC67300FFB54A00FFAD4200FFAD3900FFAD2900FFA52100FFA5
      1800FF9C1000FF9C0800FF9C0000AD632900A5522900FFFFFF00735221009CFF
      FF00006300000063000000CEFF007B7B7B00F7DEBD00FFF7EF00FFF7E700FFF7
      E700FFF7DE00FFEFDE00FFEFDE00FFEFD600FFEFD600FFE7CE00FFE7CE00FFE7
      C600FFE7C600FFDEBD00FFDEBD00A55229000000000000000000000000000000
      000000000000000000000000000000000000ADADAD00EFEFEF00CECECE00ADAD
      AD00A5A5A500C6ADAD00DEBDBD00C6ADAD000000000000000000000000000000
      0000000000000000000000000000000000004294BD004294BD004294BD004294
      B5004AA5DE004AB5FF004AB5FF004AB5FF004AB5FF004ABDFF00297BB5000000
      00000000000008080800082131002994F700319CFF00319CFF00319CFF00319C
      FF00297BDE00215AAD000000000000000000E7AD2100FFCE9C00CEAD84008CE7
      DE008CE7DE008CE7DE008CE7DE008CE7DE008CE7DE008CE7DE008CE7DE008CE7
      DE008CE7DE00DEE7CE00FFB55200E7A54200DE943900CE8C3100CE8C2900CE8C
      2100DE8C1800FF9C0800FF9C0000AD632900A5522900FFFFFF00846B5200B5B5
      AD00FFFFFF0000CEFF00ADA594007B5A3100FFEFDE00FFF7EF00FFF7EF00FFF7
      E700FFF7E700FFEFDE00FFEFDE00FFEFD600FFEFD600FFEFCE00FFE7CE00FFE7
      C600FFE7C600FFE7BD00FFDEBD00A55229000000000000000000000000000000
      000000000000000000000000000000000000ADADAD00EFEFEF00CECECE00ADAD
      AD00A5A5A500C6ADAD00DEBDBD00C6ADAD000000000000000000000000000000
      000000000000000000000000000000000000000000004294BD004294BD004294
      BD0042A5D6004AB5F7004AB5FF004AB5FF004AB5FF00399CDE00000000000000
      0000000000002929290029212100103152001863A500217BCE00298CE7002984
      DE0010396B00000000000000000000000000EFAD2100FFCE9C00BD9C73006BD6
      CE006BD6CE006BD6CE006BD6CE006BD6CE006BD6CE006BD6CE006BD6CE006BD6
      CE006BD6CE00DEE7CE00FFB55200C68C4200B5843900A57B3900B57B31009C6B
      2900CE842100FF9C1000FF9C0800AD632900A5522900FFFFFF00DED6D600846B
      52007352210073522100846B5200DED6CE00FFFFF700FFF7EF00FFF7EF00FFF7
      EF00FFF7E700FFF7E700FFEFDE00FFEFDE00FFEFD600FFEFD600FFE7CE00FFE7
      CE00FFE7C600FFE7C600FFE7BD00A55229000000000000000000000000000000
      000000000000000000000000000000000000ADADAD00C6C6C6009C9C9C009494
      94009494940094949400ADA5A500BDA5A5000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004294BD004294
      BD004294BD00429CC600429CCE0042A5D600429CCE00297BB500000000000000
      000000000000212121006B6B6B00635A5A003939390010101800000008000000
      080000001800000000000000000000000000E7AD1800FFCE9C00EFC68C00BD9C
      7300BD9C7300BD9C7300BD9C7300BD9C7300BD9C7300BD946B00BD946300BD94
      5A00BD8C5200EFBD7300FFBD5A00F7AD5200F7AD4200FFAD3900E79C3100CE8C
      2900FFA52100FFA51800FF9C0800AD632900A5522900EFC67B00EFC67B00EFC6
      7B00EFC67B00EFC67B00EFC67B00EFC67B00EFC67B00EFC67B00EFC67B00EFC6
      7B00EFC67B00EFC67B00EFC67B00EFC67B00EFC67B00EFC67B00EFC67B00EFC6
      7B00EFC67B00EFC67B00EFC67B00A55229000000000000000000000000000000
      00000000000000000000000000000000000094949400B5ADAD00CEC6C600B5B5
      B500A59C9C00B5A5A500B5A5A500949494000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004294
      BD004294BD004294BD004294BD004294BD000000000000000000000000000000
      000000000000000000003131310084848400A59C9C004A4A4A00000000000000
      000000000000000000000000000000000000E7A50000FFC67B00FFCE9C00FFCE
      9C00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE9C00FFCE9400FFC68C00FFC6
      7B00FFC67300FFBD6B00FFBD6300FFB55A00FFB54A00FFB54200FFAD3900FFAD
      3100FFA52100FFA51800EF941000A5632100A5522900CE6B1800D6731000D673
      1000D6731000D6731000D6731000D6731000D6731000D6731000D6731000D673
      1000D6731000D6731000D6731000FFC68400EF9C4200E7841000FFC68400EF9C
      4200AD7B4A004A6BD600CE6B1800A55229000000000000000000000000000000
      000000000000000000000000000000000000A5A5A500F7F7F700DEDEDE00C6C6
      C600ADADAD00A59C9C00BDADAD00AD9C9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000080808001818180008080800000000000000
      00000000000000000000000000000000000000000000E7A50800EFB53900F7BD
      4A00EFB54A00EFB54A00EFB54A00EFB54A00EFB54A00E7AD4A00E7AD4200E7AD
      4200E7A53900E7A53900DE9C3100DE9C2900DE9C2900DE942100DE942100DE8C
      1800D68C1800C67B1800AD6B180000000000A5522900C6733900D6843900D684
      3900D6843900D6843900D6843900D6843900D6843900D6843900D6843900D684
      3900D6843900D6843900D6843900F7BD8400E7944200D6731000FFC68400E794
      42009C6B4A004A6BD600C6733900A55229000000000000000000000000000000
      0000000000000000000000000000000000009C9C9C00F7F7F700EFEFEF00D6D6
      D600BDBDBD00A5A5A500ADA5A500A59C9C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5522900A5522900A552
      2900A5522900A5522900A5522900A5522900A5522900A5522900A5522900A552
      2900A5522900A5522900A5522900A5522900A5522900A5522900A5522900A552
      2900A5522900A5522900A5522900000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5A5A500E7E7E700E7E7
      E700CECECE00ADADAD0094949400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000949494009494
      9400949494009494940000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000060000000300000000100010000000000400200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFC3FFFFFFFFFFFFFFFFFFFFFFC1FFFF
      FFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFF0000F000FFF800001000000
      F0000F00000F0000000000008000030000030000000000008000010000010000
      00000000800001800001000000000000800001C00001000000000000800001E0
      3801000000000000800001C03C0300000000000080000180180F000000000000
      800001801007000000000000E00007000003000000000000F0000F0000030000
      00000000F0000F000003000000000000FF00FF000003000000000000FF00FF80
      2007000000000000FF00FFC03007000000000000FF00FFE0F80F000000000000
      FF00FFFFFE1F800001000000FF00FFFFFFFFFFFFFF800001FF81FFFFFFFFFFFF
      FFFFFFFFFFC3FFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object CommPortDriver11: TCommPortDriver
    ComPort = pnCOM1
    OnReceiveData = CommPortDriver11ReceiveData
    Left = 568
    Top = 104
  end
  object PopupMenuAffiange: TPopupMenu
    Left = 672
    Top = 104
    object N7: TMenuItem
      Caption = #1051#1086#1084#1072
    end
    object N8: TMenuItem
      Caption = #1057#1098#1077#1084' '#1079#1086#1083#1086#1090#1086
    end
    object N9: TMenuItem
      Caption = #1057#1098#1077#1084' '#1089#1077#1088#1076#1077#1095#1085#1080#1082
    end
  end
  object SaveDialog: TSaveDialog
    DefaultExt = 'xls'
    Filter = #1060#1072#1081#1083' Excel (*.xls)|*.xls'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing, ofDontAddToRecent]
    Title = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1082#1072#1082' ...'
    Left = 632
    Top = 80
  end
  object TBBackground: TTBBackground
    BkColor = 16776176
    Transparent = True
    Left = 616
    Top = 72
  end
end
