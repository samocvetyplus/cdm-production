inherited fmMBList: TfmMBList
  Left = 104
  Top = 161
  Caption = #1057#1087#1080#1089#1086#1082' '#1073#1072#1083#1072#1085#1089#1086#1074' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
  PixelsPerInch = 96
  TextHeight = 13
  inherited tb2: TSpeedBar
    inherited laDep: TLabel
      Visible = False
    end
    inherited laPeriod: TLabel
      Visible = False
    end
    object Label1: TLabel [2]
      Left = 8
      Top = 7
      Width = 73
      Height = 13
      Caption = #1042#1099#1073#1080#1088#1080#1090#1077' '#1075#1086#1076' '
      Transparent = True
    end
    object edYear: TDBNumberEditEh [3]
      Left = 81
      Top = 4
      Width = 60
      Height = 19
      DecimalPlaces = 0
      EditButton.Style = ebsUpDownEh
      EditButton.Visible = True
      EditButtons = <>
      Flat = True
      MaxValue = 2050.000000000000000000
      MinValue = 2000.000000000000000000
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnChange = edYearChange
      OnKeyDown = edYearKeyDown
    end
    inherited spitPeriod: TSpeedItem [5]
      Visible = False
    end
    inherited spitDep: TSpeedItem [6]
      Visible = False
    end
  end
  inherited tb1: TSpeedBar
    inherited spitPrint: TSpeedItem [4]
    end
    inherited spitEdit: TSpeedItem [5]
      OnClick = acEditDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    DataSource = dmData.dsrMBList
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    UseMultiTitle = True
    Visible = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1053#1086#1084#1077#1088
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1044#1072#1090#1072
        Title.EndEllipsis = True
        Width = 88
      end
      item
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 130
      end
      item
        EditButtons = <>
        FieldName = 'ABD'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076'|'#1053#1072#1095#1072#1083#1086
        Title.EndEllipsis = True
        Width = 79
      end
      item
        EditButtons = <>
        FieldName = 'AED'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076'|'#1054#1082#1086#1085#1095#1072#1085#1080#1077
        Title.EndEllipsis = True
        Width = 74
      end>
  end
  inherited ilButtons: TImageList
    Left = 22
    Top = 152
  end
  inherited fmstr: TFormStorage
    Left = 128
    Top = 96
  end
  inherited ActionList2: TActionList
    Left = 72
    Top = 152
  end
  inherited acAEvent: TActionList
    Left = 72
    Top = 96
    inherited acPeriod: TAction
      Visible = False
      OnUpdate = acPeriodUpdate
    end
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
  end
  inherited ppDoc: TTBPopupMenu
    Left = 184
    Top = 96
  end
end
