unit dCoverDepart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList, Placemnt, ImgList, SpeedBar, ExtCtrls,
  ComCtrls, StdCtrls, CheckLst, DB, FIBDataSet, pFIBDataSet;

type
  TfmdCoverDepart = class(TfmAncestor)
    Panel1: TPanel;
    taPS: TpFIBDataSet;
    taPSDEPID: TFIBIntegerField;
    taPSNAME: TFIBStringField;
    taPSCOVERID: TFIBIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure taPSBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmdCoverDepart: TfmdCoverDepart;

implementation

uses DictData, dbTree;

{$R *.dfm}

procedure TfmdCoverDepart.FormCreate(Sender: TObject);
var
  nd : TNodeData;
begin
  OpenDataSet(taPS);
  i := 0;
  while not taPS.Eof do
  begin
    nd := TNodeData.Create;
    nd.Code := taPSDEPID.AsInteger;
    nd.Name := taPSNAME.AsString;
    nd.Params := taPSCOVERID.AsVariant;
    chlistPS.Items.Add(taPSNAME.AsString, nd);
    inc(i);
    chlistPS.Checked[i] := taPSCOVERID.IsNull;
    taPS.Next;
  end;
  CloseDatSet(taPS);
end;

procedure TfmdCoverDepart.taPSBeforeOpen(DataSet: TDataSet);
begin
  taPS.ParamByName('DEPID').AsString := dm.taDepDEPID.AsInteger;
end;

end.
