unit ZpAssort;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGridEh, DB, pFIBDataSet, FIBDataSet, ActnList, DBGridEhGrouping,
  GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmZpAssort = class(TfmAncestor)
    gridZpAssort: TDBGridEh;
    quT: TpFIBDataSet;
    quTK: TFloatField;
    quTQ: TFIBBCDField;
    spitPrint: TSpeedItem;
    acPrint: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure quTBeforeOpen(DataSet: TDataSet);
    procedure gridZpAssortSumListAfterRecalcAll(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
  end;

var
  fmZpAssort: TfmZpAssort;

implementation

uses MainData, dbUtil, DictData, PrintData;

{$R *.dfm}

procedure TfmZpAssort.FormCreate(Sender: TObject);
begin
  OpenDataSet(dmMain.taZpAssort);
end;

procedure TfmZpAssort.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmMain.taZpAssort);
end;

procedure TfmZpAssort.quTBeforeOpen(DataSet: TDataSet);
begin
  with TpFIbDataSet(quT).Params do
    ByName['ZITEMID'].AsInteger := dmMain.taZItemID.AsInteger;
end;

procedure TfmZpAssort.gridZpAssortSumListAfterRecalcAll(Sender: TObject);
begin
  OpenDataSet(quT);
  try
    gridZpAssort.FieldColumns['Q'].Footer.Value := quTQ.AsString;
    if(quTQ.AsInteger = 0)then gridZpAssort.FieldColumns['KT'].Footer.Value := '100'
    else gridZpAssort.FieldColumns['KT'].Footer.Value := FormatFloat('0.###', quTK.AsFloat/quTQ.AsInteger);
  except
  end;
  CloseDataSet(quT);
end;

procedure TfmZpAssort.acPrintExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkZpAssort, VarArrayOf([VarArrayOf([dmMain.taZItemID.AsInteger])]));
end;

end.


//sum((K+ARTK)*Q)/ sum(Q)


