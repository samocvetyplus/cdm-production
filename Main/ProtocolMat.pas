unit ProtocolMat;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGridEh, ActnList, PrnDbgeh, DBGridEhGrouping, GridsEh,
  rxPlacemnt, rxSpeedbar;

type
  TfmProtocolMat = class(TfmAncestor)
    gridProtocolMat: TDBGridEh;
    spitRefresh: TSpeedItem;
    acEvent: TActionList;
    acRefresh: TAction;
    SpeedItem1: TSpeedItem;
    acPrint: TAction;
    prnProtocolMat: TPrintDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acRefreshExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acPrintUpdate(Sender: TObject);
    procedure prnProtocolMatBeforePrint(Sender: TObject);
  end;

var
  fmProtocolMat: TfmProtocolMat;

implementation

uses MainData, dbUtil;

{$R *.dfm}

procedure TfmProtocolMat.FormCreate(Sender: TObject);
begin
  inherited;
  OpenDataSet(dmMain.taProtocolMat);
end;

procedure TfmProtocolMat.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  CloseDataSet(dmMain.taProtocolMat);
end;

procedure TfmProtocolMat.acRefreshExecute(Sender: TObject);
begin
  Screen.Cursor := crSQLWait;
  try
    // ������ � ���� �� ������
    ExecSQL('execute procedure Create_ActTails('+dmMain.taPListID.AsString+')', dmMain.quTmp);
    Application.ProcessMessages;
    // ������ �� ��������� �������
    ExecSQL('execute procedure Create_ProtocolMat('+dmMain.taPListID.AsString+')', dmMain.quTmp);
    Application.ProcessMessages;
    ReOpenDataSet(dmMain.taProtocolMat);
    Application.ProcessMessages;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmProtocolMat.acPrintExecute(Sender: TObject);
begin
  if prnProtocolMat.PrinterSetupDialog then prnProtocolMat.Print;
end;

procedure TfmProtocolMat.acPrintUpdate(Sender: TObject);
begin
  with gridProtocolMat.DataSource.DataSet do
    acPrint.Enabled := Active and (not IsEmpty);
end;

procedure TfmProtocolMat.prnProtocolMatBeforePrint(Sender: TObject);
begin
  prnProtocolMat.Title.Text := '�������� �������������� �'+dmMain.taPListDOCNO.AsString+' �� ������ c '+
     dmMain.taPListBD.AsString+' �� '+dmMain.taPListDOCDATE.AsString;

  prnProtocolMat.PageHeader.RightText.Text := '���������' + #13 + '����������� ��������';
  //'�������� �������������� �'+dmMain.taPListDOCNO.AsString+' �� ������ c '+
  //   dmMain.taPListBD.AsString+' �� '+dmMain.taPListDOCDATE.AsString + '                  ���������';
end;

end.
