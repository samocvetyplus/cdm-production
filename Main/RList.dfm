inherited fmRList: TfmRList
  Left = 161
  Top = 191
  Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103
  ClientHeight = 429
  ClientWidth = 619
  ExplicitWidth = 627
  ExplicitHeight = 463
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 410
    Width = 619
    ExplicitTop = 410
    ExplicitWidth = 619
  end
  inherited tb2: TSpeedBar
    Width = 619
    ExplicitWidth = 619
  end
  inherited tb1: TSpeedBar
    Width = 619
    ExplicitWidth = 619
    inherited spitPrint: TSpeedItem [4]
      OnClick = acPrintDocExecute
    end
    inherited spitEdit: TSpeedItem [5]
      OnClick = acEditDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    Width = 619
    Height = 339
    Color = clWhite
    DataSource = dmInv.dsrSList
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    UseMultiTitle = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Width = 162
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Width = 150
      end>
  end
  inherited ppDep: TPopupMenu
    Top = 80
  end
  inherited acAEvent: TActionList
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
    inherited acPrintDoc: TAction
      OnExecute = acPrintDocExecute
    end
  end
end
