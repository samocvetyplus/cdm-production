unit TechnologyGroups;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,  cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  ImgList, cxSplitter, dxBar, cxClasses, dxStatusBar, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ActnList, FIBDataSet, pFIBDataSet,
  FIBDatabase, pFIBDatabase, cxDBLookupComboBox, FIBQuery, pFIBQuery, Menus,
  cxGridBandedTableView, cxGridDBBandedTableView, FR_DSet, FR_DBSet, dxRibbon, FR_Class,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxPSCore, dxPScxCommon,
  dxPScxGrid6Lnk, cxTextEdit, cxLabel, cxBarEditItem, cxCurrencyEdit, cxHint,
  DBClient;

type

  TfmTechnologyGroups = class(TForm)
    BarManager: TdxBarManager;
    StatusBar: TdxStatusBar;
    BarGroups: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    ImageList1: TImageList;
    Actions: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acCustomize: TAction;
    acExit: TAction;
    GroupsView: TcxGridDBTableView;
    GroupsLevel: TcxGridLevel;
    GridGroups: TcxGrid;
    ArticlesLevel: TcxGridLevel;
    ArticlesView: TcxGridDBTableView;
    Groups: TpFIBDataSet;
    GroupArticles: TpFIBDataSet;
    Operations: TpFIBDataSet;
    Articles: TpFIBDataSet;
    SourceArticles: TDataSource;
    SourceOperations: TDataSource;
    SourceGroupOperations: TDataSource;
    SourceGroupArticles: TDataSource;
    SourceGroups: TDataSource;
    OperationsLevel: TcxGridLevel;
    GridOperations: TcxGrid;
    Splitter: TcxSplitter;
    Transaction: TpFIBTransaction;
    OperationsID: TFIBIntegerField;
    OperationsOPERATION: TFIBStringField;
    ArticlesD_ARTID: TFIBIntegerField;
    ArticlesART: TFIBStringField;
    ArticlesViewName: TcxGridDBColumn;
    GroupArticlesID: TFIBIntegerField;
    GroupArticlesARTICLEID: TFIBIntegerField;
    GroupArticlesARTICLENAME: TFIBStringField;
    GroupArticlesGROUPID2: TFIBIntegerField;
    acOperationsCopy: TAction;
    gridGroupsPopupMenu: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    SmallImages: TImageList;
    acOperationsPaste: TAction;
    CloneGroupOperations: TpFIBQuery;
    GroupOperations: TpFIBDataSet;
    GroupOperationsID: TFIBIntegerField;
    GroupOperationsGROUPID: TFIBIntegerField;
    GroupOperationsOPERATIONID: TFIBIntegerField;
    GroupOperationsOPERATIONORDER: TFIBIntegerField;
    GroupOperationsOPERATION: TFIBStringField;
    GroupOperationsN: TFIBFloatField;
    GroupOperationsNS: TFIBFloatField;
    GroupOperationsRATE: TFIBFloatField;
    GroupOperationsEXCLUDEDOPERATIONS: TFIBStringField;
    GroupOperationsQUOTA: TFIBFloatField;
    GridOperationsDBBandedTableView1: TcxGridDBBandedTableView;
    OperationsView: TcxGridDBBandedTableView;
    OperationsViewColumn1: TcxGridDBBandedColumn;
    OperationsViewColumn2: TcxGridDBBandedColumn;
    OperationsViewColumn3: TcxGridDBBandedColumn;
    OperationsViewColumn4: TcxGridDBBandedColumn;
    OperationsViewColumn5: TcxGridDBBandedColumn;
    OperationsViewColumn6: TcxGridDBBandedColumn;
    OperationsViewColumn7: TcxGridDBBandedColumn;
    dxBarLargeButton5: TdxBarLargeButton;
    acPrintTechnologyCard: TAction;
    frGroupOperations: TfrDBDataSet;
    Semis: TpFIBDataSet;
    SourceSemis: TDataSource;
    BarPopupMenu: TdxBarPopupMenu;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    acPrintCardsArticles: TAction;
    acPrintCardsWithOperation: TAction;
    acPrintForeignMaterialReport: TAction;
    acPrintForeignMaterialSemisReport: TAction;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarSubItem1: TdxBarSubItem;
    dxBarButton9: TdxBarButton;
    dxBarButton10: TdxBarButton;
    frGroupArticles: TfrDBDataSet;
    GroupArticlesGROUPNAME: TFIBStringField;
    SemisID: TFIBIntegerField;
    SemisSEMIS: TFIBStringField;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton11: TdxBarButton;
    acCalcCreationCosts: TAction;
    SemisLevel: TcxGridLevel;
    GroupSemis: TpFIBDataSet;
    SourceGroupSemis: TDataSource;
    GroupSemisID: TFIBIntegerField;
    GroupSemisGROUPOPERATIONSID: TFIBIntegerField;
    GroupSemisCOST: TFIBBCDField;
    GroupSemisUNITID: TFIBStringField;
    GroupSemisSEMISID: TFIBIntegerField;
    SemisView: TcxGridDBTableView;
    SemisViewColumn2: TcxGridDBColumn;
    SemisViewColumn3: TcxGridDBColumn;
    SemisViewColumn4: TcxGridDBColumn;
    SemisViewColumn5: TcxGridDBColumn;
    dxBarLargeButton6: TdxBarLargeButton;
    Print: TpFIBDataSet;
    frPrint: TfrDBDataSet;
    frGroups: TfrDBDataSet;
    acPrintGroups: TAction;
    Printer: TdxComponentPrinter;
    PrinterLink: TdxGridReportLink;
    Tmp: TpFIBQuery;
    DataSetUnit: TpFIBDataSet;
    UnitSource: TDataSource;
    SemisViewColumn1: TcxGridDBColumn;
    GroupSemisQUANTITY: TFIBBCDField;
    Consts: TpFIBDataSet;
    ConstsGOLDCOST: TFIBBCDField;
    ConstsLOSSESPERCENT: TFIBFloatField;
    ConstsSTARTD: TFIBDateTimeField;
    ConstsENDD: TFIBDateTimeField;
    SourceConsts: TDataSource;
    cxLabel: TcxBarEditItem;
    GroupsViewColumn1: TcxGridDBColumn;
    GroupsViewColumn2: TcxGridDBColumn;
    GroupsViewColumn3: TcxGridDBColumn;
    GroupsViewColumn4: TcxGridDBColumn;
    GroupsViewColumn5: TcxGridDBColumn;
    GroupsViewColumn6: TcxGridDBColumn;
    GroupsViewColumn7: TcxGridDBColumn;
    GroupsViewColumn8: TcxGridDBColumn;
    GroupsViewColumn9: TcxGridDBColumn;
    GroupsViewColumn11: TcxGridDBColumn;
    GroupSemisWEIGHTPERCENT: TFIBBCDField;
    GroupsID: TFIBIntegerField;
    GroupsNAME: TFIBStringField;
    GroupsAVERAGEWEIGHT: TFIBBCDField;
    GroupsWORKSCOST: TFIBBCDField;
    GroupsLOSSESPERCENT: TFIBBCDField;
    GroupsWASTEPERCENT: TFIBBCDField;
    GroupsWEIGHTPART: TFIBBCDField;
    GroupsQUANTITYPART: TFIBBCDField;
    GroupsFMCOST: TFIBBCDField;
    GroupsOVERNORMLOSSESCOST: TFIBBCDField;
    GroupsSELFCOST: TFIBBCDField;
    GroupsSUMMARYWEIGHTPERCENT: TFIBBCDField;
    GroupsViewColumn10: TcxGridDBColumn;
    GroupsFMCLEARCOST: TFIBBCDField;
    GroupsViewColumn12: TcxGridDBColumn;
    GroupsPRICEPOSITION: TFIBSmallIntField;
    GroupsViewColumn13: TcxGridDBColumn;
    acPrintSemisExpense: TAction;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton12: TdxBarButton;
    cxlbInfo: TcxBarEditItem;
    GoldPriceEdit: TcxBarEditItem;
    cxBarEditItem2: TcxBarEditItem;
    dxBarButton13: TdxBarButton;
    acChangePrice: TAction;
    cxHintStyleController: TcxHintStyleController;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    dxBarButton14: TdxBarButton;
    acPrintOperationFrequency: TAction;
    dxBarButton15: TdxBarButton;
    acPrintPricereport: TAction;
    GroupsViewColumn14: TcxGridDBColumn;
    GroupsGROUPMATERIALID: TFIBStringField;
    dxBarEdit1: TdxBarEdit;
    cxBarEditItem1: TcxBarEditItem;
    SilverPriceEdit: TcxBarEditItem;
    ConstsSILVERCOST: TFIBBCDField;
    ConstsSILVERLOSSESPERCENT: TFIBFloatField;
    acPrintMissingArticles: TAction;
    dxBarSubItem4: TdxBarSubItem;
    dxBarButton16: TdxBarButton;
    dxBarButton17: TdxBarButton;
    dxBarButton18: TdxBarButton;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton19: TdxBarButton;
    dxBarButton20: TdxBarButton;
    acCalcAdditionalLosses: TAction;
    dxBarButton21: TdxBarButton;
    procedure AfterPost(DataSet: TDataSet);
    procedure GroupsNewRecord(DataSet: TDataSet);
    procedure acExitExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acCustomizeExecute(Sender: TObject);
    procedure GroupArticlesNewRecord(DataSet: TDataSet);
    procedure GroupOperationsNewRecord(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure acOperationsCopyExecute(Sender: TObject);
    procedure acOperationsPasteExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acCustomizeUpdate(Sender: TObject);
    procedure OperationsViewEditValueChanged(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem);
    procedure GroupsViewDblClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure acPrintTechnologyCardExecute(Sender: TObject);

    procedure acPrintCardsArticlesExecute(Sender: TObject);
    procedure acPrintCardsWithOperationExecute(Sender: TObject);
    procedure acPrintForeignMaterialSemisReportExecute(Sender: TObject);
    procedure acPrintForeignMaterialReportExecute(Sender: TObject);
    procedure GroupSemisNewRecord(DataSet: TDataSet);
    procedure GroupSemisAfterDelete(DataSet: TDataSet);
    procedure acCalcCreationCostsExecute(Sender: TObject);
    procedure GroupSemisAfterPost(DataSet: TDataSet);
    procedure acPrintGroupsExecute(Sender: TObject);
    procedure OperationsViewDataControllerDetailExpanding(
      ADataController: TcxCustomDataController; ARecordIndex: Integer;
      var AAllow: Boolean);
    procedure SemisViewColumn5PropertiesEditValueChanged(Sender: TObject);
    procedure ConstsAfterOpen(DataSet: TDataSet);
    procedure acPrintSemisExpenseExecute(Sender: TObject);
    procedure acChangePriceExecute(Sender: TObject);
    procedure acChangePriceUpdate(Sender: TObject);
    procedure acCalcCreationCostsUpdate(Sender: TObject);
    procedure acPrintOperationFrequencyExecute(Sender: TObject);
    procedure acPrintPricereportExecute(Sender: TObject);
    procedure acPrintMissingArticlesExecute(Sender: TObject);
    procedure acCalcAdditionalLossesExecute(Sender: TObject);
  private

  public
    allGroupArticles: AnsiString;
    procedure printDoc(DocID : integer; GetValue: TDetailEvent; ReportDataset: TfrDBDataSet);
    class procedure Execute;
  end;

var
  fmTechnologyGroups: TfmTechnologyGroups;

implementation

{$R *.dfm}
uses
  MainData, DictData, uUtils, StrUtils, TechnologyGroupCustomization,
  uDialogs, PrintData, uDialogSetParams, fmDialogTechnologyRecalc, DateUtils,
  DialogTechnologyOperationReport, dmEventAlerts;

procedure TfmTechnologyGroups.acAddExecute(Sender: TObject);
begin
  try
    Groups.Insert;
    Groups.Post;
  except
    On E:Exception do
      begin
       ShowMessage(E.Message);
       Groups.Cancel;
      end;
  end;
end;

procedure TfmTechnologyGroups.acCalcAdditionalLossesExecute(Sender: TObject);
begin
  TDlgTechnologyOperationReport.Execute;
end;


procedure TfmTechnologyGroups.acCalcCreationCostsExecute(Sender: TObject);
var StartTime, EndTime : TDateTime;
    Diffrence : Integer;
begin

  with Tmp do
    begin
      Close;
      SQL.Text := 'select recalc$start$time, recalc$end$time from technology$consts';
      ExecQuery;

      StartTime := Fields[0].AsDateTime;
      EndTime := Fields[1].AsdateTime;
    end;

  Diffrence := MinutesBetween(StartTime, EndTime);

  InformationMessage('��������! ��������� �������� ����� ' + IntToStr(Diffrence div 60) + ' �. ' + IntToStr(Diffrence mod 60) + ' ���.!' + #13#10 +
                     '�� ����� ��������� ��������� ������, ������������ � ��������, ����� ����������');
                     
  if TDialogTechnologyRecalc.Execute = mrYes then
    begin
      Screen.Cursor := crSQLWait;
      try
        try
          with Tmp do
            begin

              EventAlertsData.Alert(GROUPS_ALERT, amStart);

              Close;
              SQL.Clear;
              SQL.Text := 'update technology$consts set recalc$start$time = ' + #39 + 'now' + #39 + ' where id = 1';
              ExecQuery;

              Close;
              SQL.Clear;
              SQL.Text := 'execute procedure technology$Koefficient$Recalc';
              ExecQuery;

              Close;
              SQL.Clear;
              SQL.Text := 'update technology$consts set recalc$end$time = ' + #39 + 'now' + #39 + ' where id = 1';
              ExecQuery;

              EventAlertsData.Alert(GROUPS_ALERT, amEnd);

            end;
        except
          On E: Exception do
          begin
            ShowMessage(E.Message);
            Tmp.Transaction.RollbackRetaining;
          end;
        end;
      finally
        Tmp.Transaction.CommitRetaining;
        Groups.CloseOpen(false);
        Consts.CloseOpen(false);
        Screen.Cursor := crDefault;
      end;
    end
  else Exit;
end;

procedure TfmTechnologyGroups.acCalcCreationCostsUpdate(Sender: TObject);
begin
  acCalcCreationCosts.Enabled := (dm.User.Profile = -1) or (dm.User.Profile = 3);
end;

procedure TfmTechnologyGroups.acChangePriceExecute(Sender: TObject);
var
  CursorPosition: Integer;
begin
if QuestionYesNoMessage('���� ����� ��������� � ������������ � ����������.' + #13#10 +
                        '������� ����������?') then
  begin

    Consts.Edit;

    ConstsGOLDCOST.AsCurrency := Currency(GoldPriceEdit.EditValue);
    ConstsSILVERCOST.AsCurrency := Currency(SilverPriceEdit.EditValue);

    Consts.Post;

    CursorPosition := GroupsID.AsInteger;

    Groups.CloseOpen(false);

    Groups.Locate('ID', CursorPosition, []);
  end;
end;

procedure TfmTechnologyGroups.acChangePriceUpdate(Sender: TObject);
begin
  acChangePrice.Enabled := (dm.User.Profile = -1) or (dm.User.Profile = 3);
end;

procedure TfmTechnologyGroups.acCustomizeExecute(Sender: TObject);
begin

  if Groups.State in [dsEdit, dsInsert] then
  begin
    Groups.Post;
  end;

  GroupArticles.Filter := 'GroupID = ' + trim(Groups.FieldByName('ID').AsString);
  GroupArticles.Filtered := true;

  with Articles, Operations, GroupArticles do
  begin
    CloseOpen(false);
  end;

  TfmTechnologyGroupCustomization.Execute;
  
end;

procedure TfmTechnologyGroups.acCustomizeUpdate(Sender: TObject);
begin
  acCustomize.Enabled := (Groups.RecordCount > 0) and (GroupsView.Controller.SelectedRowCount <> 0);
end;

procedure TfmTechnologyGroups.acDelExecute(Sender: TObject);
begin
if DialogQuestionYesNoMessage('������� ����� � �� � ����������?') = mrYes then
  try
    Groups.Delete;
    Groups.Transaction.CommitRetaining;
  except
    On E:Exception do ShowMessage(E.Message);
  end;
end;

procedure TfmTechnologyGroups.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := (Groups.RecordCount > 0) and (GroupsView.Controller.SelectedRowCount <> 0);
end;

procedure TfmTechnologyGroups.acExitExecute(Sender: TObject);
begin
  with Groups, GroupOperations do
    begin
      if State in [dsEdit, dsInsert] then Post;
      Close;
    end;
  Close;
end;

procedure TfmTechnologyGroups.acOperationsCopyExecute(Sender: TObject);
begin
  CloneGroupOperations.ParamByName('SourceGroupID').AsInteger := Groups.FieldByName('ID').AsInteger;
  StatusBar.Panels[0].Text := '� ������ ����������� ����� �' + Groups.FieldByName('ID').AsString + '. �������� �������.';
end;

procedure TfmTechnologyGroups.acOperationsPasteExecute(Sender: TObject);
var SelectedGroupID : Integer;
begin
SelectedgroupID := Groups.FieldByName('ID').AsInteger;
  if CloneGroupOperations.ParamByName('SourceGroupID').AsInteger <> SelectedGroupID then
    begin
      GroupsView.DataController.BeginUpdate;
      OperationsView.dataController.BeginUpdate;
      CloneGroupOperations.ParamByName('DestGroupID').AsInteger := SelectedGroupID;
      try
        CloneGroupOperations.ExecQuery;
      except
        on E: Exception do
          begin
            DialogErrorOkMessage('������ ��� ������� ������� ��������.'+#10#13+
                                 '��������� Clone$Groups$Operations.'+#10#13+
                                 '����� ������: ' + E.Message);
            CloneGroupOperations.Transaction.RollbackRetaining;
          end;
      end;

      GroupsView.DataController.EndUpdate;
      OperationsView.dataController.EndUpdate;
      CloneGroupOperations.Transaction.CommitRetaining;
      Groups.CloseOpen(false);
      GroupOperations.CloseOpen(false);
      Operations.CloseOpen(false);
      Articles.CloseOpen(false);
      Groups.Locate('ID', SelectedGroupID, []);
      StatusBar.Panels[0].Text := '������� �����������';

    end
  else DialogErrorOkMessage('���������� ������� �������������� �������� ������ �����! �������� ��������.');

end;

procedure TfmTechnologyGroups.acPrintCardsArticlesExecute(Sender: TObject);
begin
  printDoc(106, nil, frGroupArticles);
end;

procedure TfmTechnologyGroups.acPrintCardsWithOperationExecute(Sender: TObject);
begin
  if Print.Active then Print.Close;
  Print.SelectSQL.Clear;
  Print.SelectSQL.Text := 'select * from Technology$Operation$Report(:Operation$ID) order by Group$name';
  TDialogSetParams.Execute(1);
  Print.Open;
  printDoc(107, nil, frPrint);
  Print.Close;
end;

procedure TfmTechnologyGroups.acPrintForeignMaterialReportExecute(
  Sender: TObject);
begin
  if Print.Active then Print.Close;
  Print.SelectSQL.Clear;
  Print.SelectSQL.Text := 'select semis$name, quantity, weight, full$cost, average$price, U  from Technology$Full$Semis$Report(:Start$Date, :End$Date)';
  TDialogSetParams.Execute(2);
  Print.Open;
  printDoc(108, dmPrint.TechnologySemisGetValue, frPrint);
  Print.Close;
end;

procedure TfmTechnologyGroups.acPrintForeignMaterialSemisReportExecute(
  Sender: TObject);
begin
  if Print.Active then Print.Close;
  Print.SelectSQL.Clear;
  Print.SelectSQL.Text := 'select semis$name, quantity, weight, full$cost, average$price, U  from Technology$Semis$Report(:Semis, :Start$Date, :End$Date)';
  TDialogSetParams.Execute(3);
  Print.Open;
  printDoc(109, dmPrint.TechnologySemisGetValue, frPrint);
  Print.Close;
end;

procedure TfmTechnologyGroups.acPrintGroupsExecute(Sender: TObject);
var
  Link: TdxgridReportLink;
begin
  Link := TdxGridReportLink(Printer.LinkByName('PrinterLink'));
  Link.ReportTitle.Text := '����� �� ���. ������';
  Link.ReportDocument.Caption := Link.ReportTitle.Text;
  Link.ReportDocument.CreationDate := Date;
  Link.Preview;
end;

procedure TfmTechnologyGroups.acPrintMissingArticlesExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  if Print.Active then Print.Close;
  Print.SelectSQL.Clear;
  Print.SelectSQL.Text := 'select Article$ID, Article  from technology$missing$articles(:Start$Date, :End$Date)';
  TDialogSetParams.Execute(2);
  try
    try
      Print.Open;
      printDoc(117, dmPrint.TechnologySemisGetValue, frPrint);
    except
      on E:Exception do
        DialogErrorOKMessage(E.Message);
    end;
  finally
    Print.Close;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmTechnologyGroups.acPrintOperationFrequencyExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  if Print.Active then Print.Close;
  Print.SelectSQL.Clear;
  Print.SelectSQL.Text := 'select operation$name, cards$weight, orders$weight, result, average$koefficient, average$next$koefficient' + #13#10 +
                          'from technology$Operation$Frequency' + #13#10 +
                          'order by operation$name';
  try
    try
      Print.Open;
      printDoc(114, nil, frPrint);
    except
      on E:Exception do
        DialogErrorOKMessage(E.Message);
    end;
  finally
    Print.Close;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmTechnologyGroups.acPrintPriceReportExecute(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  if Print.Active then Print.Close;
  Print.SelectSQL.Clear;
  Print.SelectSQL.Text := 'select price$name, price$position, price$sum, price$max$cost  from technology$price$print';
  try
    try
      Print.Open;
      printDoc(115, nil, frPrint);
    except
      on E:Exception do
        DialogErrorOKMessage(E.Message);
    end;
  finally
    Print.Close;
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmTechnologyGroups.acPrintSemisExpenseExecute(Sender: TObject);
begin
  InformationMessage('��������! ������������� ����������� ������ ' + #13#10 +
                     '�� ������, ������������� �� ���������!');
  Screen.Cursor := crSQLWait;
  if Print.Active then Print.Close;
  Print.SelectSQL.Clear;
  Print.SelectSQL.Text := 'select semis$name, summary$semis$weight, summary$semis$quantity from Semis$Expense$Report(:Start$Date, :End$Date)';
  TDialogSetParams.Execute(0);
  Print.Open;
  printDoc(113, dmPrint.TechnologySemisGetValue, frPrint);
  Print.Close;
  Screen.Cursor := crDefault;
end;

procedure TfmTechnologyGroups.acPrintTechnologyCardExecute(Sender: TObject);
begin

allGroupArticles := '';

with GroupArticles do
  begin
    DisableScrollEvents;
    DisableControls;
    Filter := 'GroupID = ' + trim(Groups.FieldByName('ID').AsString);
    Filtered := true;
    First;
    while not Eof do
      begin
        allGroupArticles := allGroupArticles + trim(GroupArticlesARTICLENAME.AsString) + ', ';
        Next;
      end;
    Filter := '';
    Filtered := false;
    EnableControls;
    EnableScrollEvents;
  end;

Delete(AllGroupArticles, Length(AllGroupArticles) - 1, 2);

printDoc(105, dmPrint.TechnologyGroupsGetValue, frGroupOperations);
end;

procedure TfmTechnologyGroups.printDoc(DocID: Integer; GetValue: TDetailEvent; ReportDataset: TfrDBDataset);
begin
with dmPrint do
  begin
    FDocID := DocID;
    frReport.Dataset := ReportDataset;
    frReport.OnGetValue := GetValue;
    taDoc.Open;
    frReport.LoadFromDB(taDoc, FDocID);
    dm.DocPreview:=dm.GetDocPreview;
    if dm.DocPreview = pvDesign
      then
        frReport.DesignReport
      else
        frReport.ShowReport;
    taDoc.Close;
  end;
end;

procedure TfmTechnologyGroups.SemisViewColumn5PropertiesEditValueChanged(
  Sender: TObject);
begin
  groupSemis.Post;
end;

class procedure TfmTechnologyGroups.Execute;
begin
  fmTechnologyGroups := nil;
  try
    fmTechnologyGroups := TfmTechnologyGroups.Create(Application);
    fmTechnologyGroups.ShowModal;
  finally
    FreeAndNil(fmTechnologyGroups);
  end;
end;

procedure TfmTechnologyGroups.FormCreate(Sender: TObject);
begin

  Transaction.StartTransaction;

  if not dm.db.DefaultTransaction.Active then
  begin
    dm.db.DefaultTransaction.StartTransaction;
  end;

  Groups.Open;
  GroupArticles.Open;
  GroupSemis.Open;
  Consts.Open;
  Operations.Open;
  Semis.Open;
  Articles.Open;
  DataSetUnit.Open;

  with GoldPriceEdit, SilverPriceEdit do
  begin
    Properties.ReadOnly := not ((dm.User.Profile = -1) or (dm.User.Profile = 3));
  end;

  with dm.taMat do
    begin
      if Active then Close;
      Filter := 'good = 1';
      Filtered := true;
      Open;
    end;

end;

procedure TfmTechnologyGroups.FormDestroy(Sender: TObject);
begin
  with Groups, GroupArticles, Operations, Articles, DataSetUnit do Active := false;
  with dm.taMat do
    begin
      Filter := '';
      Filtered := false;
      Close;
    end;

  if Transaction.InTransaction then
  begin
    Transaction.Commit;
  end;

end;

procedure TfmTechnologyGroups.GroupArticlesNewRecord(DataSet: TDataSet);
begin
  GroupArticlesID.AsInteger := dm.GetID(101);
  GroupArticles.FieldByName('GroupID').AsInteger := Groups.FieldByName('ID').AsInteger;
  GroupArticles.FieldByName('Article$ID').AsInteger := Articles.FieldByName('D_ArtID').AsInteger;
end;

procedure TfmTechnologyGroups.GroupOperationsNewRecord(DataSet: TDataSet);
begin
  GroupOperationsID.AsInteger := dm.GetID(102);
  GroupOperations.FieldByName('Group$ID').AsInteger := Groups.FieldByName('ID').AsInteger;
  GroupOperations.FieldByName('Operation$ID').AsInteger := Operations.FieldByName('ID').AsInteger;
  GroupOperations.FieldByName('Quota').AsInteger := 100;
  GroupOperations.FieldByName('Excluded$Operations').AsString := '';
end;

procedure TfmTechnologyGroups.GroupSemisAfterDelete(DataSet: TDataSet);
begin
  StatusBar.Panels[1].Text := '�������� ���������';
end;

procedure TfmTechnologyGroups.GroupSemisAfterPost(DataSet: TDataSet);
begin
  GroupSemis.Transaction.CommitRetaining;
  GroupSemis.Refresh;
end;

procedure TfmTechnologyGroups.GroupSemisNewRecord(DataSet: TDataSet);
begin
  GroupSemisID.AsInteger := dm.GetId(103);
  GroupSemisGROUPOPERATIONSID.AsInteger := GroupOperationsID.AsInteger;
end;

procedure TfmTechnologyGroups.AfterPost(DataSet: TDataSet);
begin
  (DataSet as TpFIBDataSet).Transaction.CommitRetaining;
end;

procedure TfmTechnologyGroups.ConstsAfterOpen(DataSet: TDataSet);
begin
    cxlbInfo.Caption := '������ ���������� �� ������ � ' + Consts.FieldByName('StartD').AsString + ' �� '
    + Consts.FieldByName('EndD').AsString + '. ';
    GoldPriceEdit.EditValue := ConstsGOLDCOST.AsCurrency;
    SilverPriceEdit.EditValue := ConstsSILVERCOST.AsCurrency;
end;


procedure TfmTechnologyGroups.GroupsNewRecord(DataSet: TDataSet);
begin
  Groups.FieldByName('ID').AsInteger := dm.GetId(100);
  Groups.FieldByName('Name').AsString := '';
end;

procedure TfmTechnologyGroups.GroupsViewDblClick(Sender: TObject);
begin
  acCustomize.Execute;
end;

procedure TfmTechnologyGroups.OperationsViewDataControllerDetailExpanding(
  ADataController: TcxCustomDataController; ARecordIndex: Integer;
  var AAllow: Boolean);
begin
  ADataController.FocusSelectedRow(ARecordIndex);
end;

procedure TfmTechnologyGroups.OperationsViewEditValueChanged(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem);
begin
  GroupOperations.Post;
end;


end.
