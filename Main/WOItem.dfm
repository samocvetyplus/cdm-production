inherited fmWOItem: TfmWOItem
  Left = 79
  Top = 156
  Width = 848
  Height = 376
  Caption = '����� ������'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbr: TStatusBar
    Top = 330
    Width = 840
  end
  object gr: TM207IBGrid [1]
    Left = 0
    Top = 96
    Width = 840
    Height = 234
    Align = alClient
    Color = clBtnFace
    DataSource = dmMain.dsrWOItem
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    FixedCols = 1
    IniStorage = fmstr
    TitleButtons = True
    OnGetCellParams = grGetCellParams
    MultiShortCut = 116
    ColorShortCut = 0
    MultiSelectMode = msmTeapot
    PrintDataSet = 16464
    ClearHighlight = False
    SortOnTitleClick = True
    Columns = <
      item
        Expanded = False
        FieldName = 'RecNo'
        Width = 27
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Mol'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 119
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'Operation'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 114
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MATGET'
        PickList.Strings = (
          'AU 585'
          '���������')
        Visible = True
      end
      item
        Color = cl3DLight
        Expanded = False
        FieldName = 'MatNameGet'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 132
        Visible = True
      end
      item
        Color = cl3DLight
        Expanded = False
        FieldName = 'QGET'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 68
        Visible = True
      end
      item
        Color = cl3DLight
        Expanded = False
        FieldName = 'WGET'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clSilver
        Expanded = False
        FieldName = 'MatNamePut'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 112
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'ITDATE'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 84
        Visible = True
      end
      item
        Color = clSilver
        Expanded = False
        FieldName = 'QPUT'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 76
        Visible = True
      end
      item
        Color = clSilver
        Expanded = False
        FieldName = 'WPUT'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'FACTO'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 76
        Visible = True
      end
      item
        Color = clInfoBk
        Expanded = False
        FieldName = 'QUOTA'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 73
        Visible = True
      end
      item
        Color = clWhite
        Expanded = False
        FieldName = 'QRET'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clWhite
        Expanded = False
        FieldName = 'WRET'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Visible = True
      end
      item
        Color = clWhite
        Expanded = False
        FieldName = 'MatNameRet'
        Title.Alignment = taCenter
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clNavy
        Title.Font.Height = -11
        Title.Font.Name = 'MS Sans Serif'
        Title.Font.Style = []
        Width = 127
        Visible = True
      end>
  end
  object plWOrder: TPanel [2]
    Left = 0
    Top = 42
    Width = 840
    Height = 54
    Align = alTop
    BevelInner = bvLowered
    TabOrder = 2
    object lbNoOrder: TLabel
      Left = 8
      Top = 8
      Width = 73
      Height = 13
      Caption = '����� ������'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbDocDate: TLabel
      Left = 8
      Top = 32
      Width = 65
      Height = 13
      Caption = '���� ������'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 192
      Top = 9
      Width = 31
      Height = 13
      Caption = '�����'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbDep: TDBText
      Left = 232
      Top = 9
      Width = 153
      Height = 16
      DataField = 'DepName'
      DataSource = dmMain.dsrWOrderList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object edDocNo: TDBEdit
      Left = 88
      Top = 6
      Width = 97
      Height = 21
      Color = clInfoBk
      DataField = 'DOCNO'
      DataSource = dmMain.dsrWOrderList
      TabOrder = 0
    end
    object dtedDocDate: TDBDateEdit
      Left = 88
      Top = 27
      Width = 97
      Height = 20
      DataField = 'DOCDATE'
      DataSource = dmMain.dsrWOrderList
      Color = clInfoBk
      NumGlyphs = 2
      TabOrder = 1
    end
  end
  inherited tb1: TSpeedBar
    Width = 840
    TabOrder = 3
    object spitAdd: TSpeedItem
      BtnCaption = '��������'
      Caption = '��������'
      Hint = '��������|'
      ImageIndex = 1
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = spitAddClick
      SectionName = 'Untitled (0)'
    end
    object spitDel: TSpeedItem
      BtnCaption = '�������'
      Caption = '�������'
      Hint = '�������|'
      ImageIndex = 2
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = spitDelClick
      SectionName = 'Untitled (0)'
    end
    object spitPrint: TSpeedItem
      BtnCaption = '��������'
      Caption = '��������'
      DropDownMenu = ppPrint
      Hint = '��������|'
      ImageIndex = 67
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
  end
  inherited ilButtons: TImageList
    Left = 214
    Top = 188
  end
  inherited fmstr: TFormStorage
    Left = 112
    Top = 184
  end
  object ppPrint: TPopupMenu
    Left = 360
    Top = 184
    object mnitItOrder: TMenuItem
      Tag = 2
      Caption = '������� ������'
      OnClick = mnitPrintClick
    end
    object mnitItInv: TMenuItem
      Tag = 1
      Caption = '������� ���������'
      OnClick = mnitPrintClick
    end
    object mnitSep: TMenuItem
      Caption = '-'
    end
    object mnitOrder15: TMenuItem
      Tag = 5
      Caption = '����� ��-15'
      OnClick = mnitPrintClick
    end
    object mnitInv15: TMenuItem
      Tag = 4
      Caption = '��������� ��-15'
      OnClick = mnitPrintClick
    end
    object mnitCU15: TMenuItem
      Tag = 3
      Caption = '����� ��-15'
      OnClick = mnitPrintClick
    end
  end
end
