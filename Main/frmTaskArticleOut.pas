unit frmTaskArticleOut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, FIBQuery, pFIBQuery, FIBDatabase, pFIBDatabase,
  FIBDataSet, pFIBDataSet, cxPC, StdCtrls, ExtCtrls, ActnList, ToolWin,
  ComCtrls, cxLookAndFeels, cxGridBandedTableView, cxGridDBBandedTableView,
  Grids, DBGrids, Provider, DBClient, cxContainer, cxCheckBox;

const

  CONST_InvoiceAddedID        = 12;
  CONST_InvoiceAddedDOCNO     = 23;
  CONST_InvoiceAddedItemsID   = 34;
  CONST_InvoiceRemovedID      = 12;
  CONST_InvoiceRemovedDOCNO   = 23;
  CONST_InvoiceRemovedItemsID = 34;
  CONST_SubStorageItemsID     = 34;

  CONST_InvoiceRemovedITYPE   = 8;
  CONST_InvoiceAddedITYPE     = 9;

type

  TDialogTaskArticleOut = class(TForm)
    GridDemandDBTableView: TcxGridDBTableView;
    GridDemandLevel: TcxGridLevel;
    GridDemand: TcxGrid;
    DataSourceDemand: TDataSource;
    Transaction: TpFIBTransaction;
    DataSetDemand: TpFIBDataSet;
    PageControl: TcxPageControl;
    TabSheet1: TcxTabSheet;
    TabSheet2: TcxTabSheet;
    GridDemandDBTableViewDOCNO: TcxGridDBColumn;
    GridDemandDBTableViewDOCDATE: TcxGridDBColumn;
    GridDemandDBTableViewW: TcxGridDBColumn;
    GridDemandDBTableViewQ: TcxGridDBColumn;
    GridDemandDBTableViewOPERATION: TcxGridDBColumn;
    GridDemandDBTableViewFIO: TcxGridDBColumn;
    GridDemandAndTaskLevel: TcxGridLevel;
    GridDemandAndTask: TcxGrid;
    Panel1: TPanel;
    PanelPrior: TPanel;
    ButtonPrior: TButton;
    PanelNext: TPanel;
    ButtonNext: TButton;
    PanelOK: TPanel;
    ButtonOK: TButton;
    PanelCancel: TPanel;
    ButtonCancel: TButton;
    ActionList: TActionList;
    ActionNext: TAction;
    ActionPrior: TAction;
    ActionOK: TAction;
    ActionCancel: TAction;
    DataSetDemandID: TFIBIntegerField;
    DataSetDemandDOCNO: TFIBIntegerField;
    DataSetDemandDOCDATE: TFIBDateTimeField;
    DataSetDemandW: TFIBFloatField;
    DataSetDemandQ: TFIBFloatField;
    DataSetDemandOPERATION: TFIBStringField;
    DataSetDemandFIO: TFIBStringField;
    DataSetDemandISPROCESS: TFIBSmallIntField;
    DataSourceArticles: TDataSource;
    cxLookAndFeelController: TcxLookAndFeelController;
    GridDemandAndTaskMaster: TcxGridDBBandedTableView;
    DataSetInvoiceRemoved: TpFIBDataSet;
    DataSetInvoiceAdded: TpFIBDataSet;
    QueryTmp: TpFIBQuery;
    DataSetInvoiceAddedINVID: TFIBIntegerField;
    DataSetInvoiceAddedDOCNO: TFIBIntegerField;
    DataSetInvoiceAddedDOCDATE: TFIBDateTimeField;
    DataSetInvoiceAddedDEPID: TFIBIntegerField;
    DataSetInvoiceAddedFROMDEPID: TFIBIntegerField;
    DataSetInvoiceAddedUSERID: TFIBIntegerField;
    DataSetInvoiceAddedQ: TFIBFloatField;
    DataSetInvoiceAddedJOBID: TFIBIntegerField;
    DataSetInvoiceAddedREF: TFIBIntegerField;
    DataSetInvoiceAddedITYPE: TFIBSmallIntField;
    DataSetInvoiceAddedWORDERID: TFIBIntegerField;
    DataSetInvoiceRemovedINVID: TFIBIntegerField;
    DataSetInvoiceRemovedDOCNO: TFIBIntegerField;
    DataSetInvoiceRemovedDOCDATE: TFIBDateTimeField;
    DataSetInvoiceRemovedDEPID: TFIBIntegerField;
    DataSetInvoiceRemovedFROMDEPID: TFIBIntegerField;
    DataSetInvoiceRemovedUSERID: TFIBIntegerField;
    DataSetInvoiceRemovedQ: TFIBFloatField;
    DataSetInvoiceRemovedJOBID: TFIBIntegerField;
    DataSetInvoiceRemovedITYPE: TFIBSmallIntField;
    DataSetInvoiceRemovedREF: TFIBIntegerField;
    DataSetInvoiceRemovedWORDERID: TFIBIntegerField;
    DataSetInvoiceAddedItems: TpFIBDataSet;
    DataSetInvoiceRemovedItems: TpFIBDataSet;
    DataSetInvoiceAddedItemsID: TFIBIntegerField;
    DataSetInvoiceAddedItemsINVID: TFIBIntegerField;
    DataSetInvoiceAddedItemsARTID: TFIBIntegerField;
    DataSetInvoiceAddedItemsART2ID: TFIBIntegerField;
    DataSetInvoiceAddedItemsSZID: TFIBIntegerField;
    DataSetInvoiceAddedItemsART: TFIBStringField;
    DataSetInvoiceAddedItemsART2: TFIBStringField;
    DataSetInvoiceAddedItemsSZ: TFIBStringField;
    DataSetInvoiceAddedItemsQ: TFIBIntegerField;
    DataSetInvoiceAddedItemsT: TFIBSmallIntField;
    DataSetInvoiceAddedItemsOPERID: TFIBStringField;
    DataSetInvoiceAddedItemsOPERATION: TFIBStringField;
    DataSetInvoiceAddedItemsU: TFIBSmallIntField;
    DataSetInvoiceAddedItemsCOMMENTID: TFIBSmallIntField;
    DataSetInvoiceAddedItemsCOMMENT: TFIBStringField;
    DataSetInvoiceAddedItemsQ1: TFIBIntegerField;
    DataSetInvoiceRemovedItemsID: TFIBIntegerField;
    DataSetInvoiceRemovedItemsINVID: TFIBIntegerField;
    DataSetInvoiceRemovedItemsARTID: TFIBIntegerField;
    DataSetInvoiceRemovedItemsART2ID: TFIBIntegerField;
    DataSetInvoiceRemovedItemsSZID: TFIBIntegerField;
    DataSetInvoiceRemovedItemsART: TFIBStringField;
    DataSetInvoiceRemovedItemsART2: TFIBStringField;
    DataSetInvoiceRemovedItemsSZ: TFIBStringField;
    DataSetInvoiceRemovedItemsQ: TFIBIntegerField;
    DataSetInvoiceRemovedItemsT: TFIBSmallIntField;
    DataSetInvoiceRemovedItemsOPERID: TFIBStringField;
    DataSetInvoiceRemovedItemsOPERATION: TFIBStringField;
    DataSetInvoiceRemovedItemsU: TFIBSmallIntField;
    DataSetInvoiceRemovedItemsCOMMENTID: TFIBSmallIntField;
    DataSetInvoiceRemovedItemsCOMMENT: TFIBStringField;
    DataSetInvoiceRemovedItemsQ1: TFIBIntegerField;
    DataSetSubStorageItems: TpFIBDataSet;
    DataSetSubStorageItemsID: TFIBIntegerField;
    DataSetSubStorageItemsINVID: TFIBIntegerField;
    DataSetSubStorageItemsARTID: TFIBIntegerField;
    DataSetSubStorageItemsART2ID: TFIBIntegerField;
    DataSetSubStorageItemsSZID: TFIBIntegerField;
    DataSetSubStorageItemsART: TFIBStringField;
    DataSetSubStorageItemsART2: TFIBStringField;
    DataSetSubStorageItemsSZ: TFIBStringField;
    DataSetSubStorageItemsQ: TFIBIntegerField;
    DataSetSubStorageItemsT: TFIBSmallIntField;
    DataSetSubStorageItemsOPERID: TFIBStringField;
    DataSetSubStorageItemsOPERATION: TFIBStringField;
    DataSetSubStorageItemsOPERIDFROM: TFIBStringField;
    DataSetSubStorageItemsOPERATIONFROM: TFIBStringField;
    DataSetSubStorageItemsART2ID_OLD: TFIBIntegerField;
    DataSetSubStorageItemsART2_OLD: TFIBStringField;
    DataSetSubStorageItemsU: TFIBSmallIntField;
    DataSetSubStorageItemsU_OLD: TFIBSmallIntField;
    DataSetSubStorageItemsQ1: TFIBIntegerField;
    DataSetSubStorageItemsPATTERN: TFIBStringField;
    TransactionTmp: TpFIBTransaction;
    StyleRepository: TcxStyleRepository;
    cxStyle1: TcxStyle;
    DBDataArticlesMaster: TpFIBDataSet;
    DBDataArticlesDetail: TpFIBDataSet;
    DBDataArticlesMasterJAID: TFIBIntegerField;
    DBDataArticlesMasterAARTID: TFIBIntegerField;
    DBDataArticlesMasterAART: TFIBStringField;
    DBDataArticlesMasterAART2ID: TFIBIntegerField;
    DBDataArticlesMasterASZID: TFIBIntegerField;
    DBDataArticlesMasterASZNAME: TFIBStringField;
    DBDataArticlesMasterAQ: TFIBIntegerField;
    DBDataArticlesMasterAFQ: TFIBIntegerField;
    DBDataArticlesMasterAOPERID: TFIBStringField;
    DBDataArticlesMasterAOPERNAME: TFIBStringField;
    DBDataArticlesMasterAUNITID: TFIBIntegerField;
    DBDataArticlesMasterAUNITNAME: TFIBStringField;
    DBDataArticlesMasterQ: TFIBIntegerField;
    DBDataArticlesDetailJAID: TFIBIntegerField;
    DBDataArticlesDetailWHARTID: TFIBIntegerField;
    DBDataArticlesDetailWHART: TFIBStringField;
    DBDataArticlesDetailWHART2ID: TFIBIntegerField;
    DBDataArticlesDetailWHSZID: TFIBIntegerField;
    DBDataArticlesDetailWHSZNAME: TFIBStringField;
    DBDataArticlesDetailWHOPERID: TFIBStringField;
    DBDataArticlesDetailWHOPERNAME: TFIBStringField;
    DBDataArticlesDetailWHUNITID: TFIBIntegerField;
    DBDataArticlesDetailWHUNITNAME: TFIBStringField;
    DBDataArticlesDetailWHQ: TFIBIntegerField;
    DataArticlesMaster: TClientDataSet;
    SourceArticlesMaster: TDataSource;
    ProviderArticlesMaster: TDataSetProvider;
    SourceArticlesDetail: TDataSource;
    DataArticlesDetail: TClientDataSet;
    ProviderArticlesDetail: TDataSetProvider;
    DataArticlesMasterJAID: TIntegerField;
    DataArticlesMasterAARTID: TIntegerField;
    DataArticlesMasterAART: TStringField;
    DataArticlesMasterAART2ID: TIntegerField;
    DataArticlesMasterASZID: TIntegerField;
    DataArticlesMasterASZNAME: TStringField;
    DataArticlesMasterAQ: TIntegerField;
    DataArticlesMasterAFQ: TIntegerField;
    DataArticlesMasterAOPERID: TStringField;
    DataArticlesMasterAOPERNAME: TStringField;
    DataArticlesMasterAUNITID: TIntegerField;
    DataArticlesMasterAUNITNAME: TStringField;
    DataArticlesMasterQ: TIntegerField;
    GridDemandAndTaskLevel1: TcxGridLevel;
    GridDemandAndTaskDetail: TcxGridDBTableView;
    GridDemandAndTaskDetailWHART: TcxGridDBColumn;
    GridDemandAndTaskDetailWHSZNAME: TcxGridDBColumn;
    GridDemandAndTaskDetailWHOPERNAME: TcxGridDBColumn;
    GridDemandAndTaskDetailWHUNITNAME: TcxGridDBColumn;
    GridDemandAndTaskDetailWHQ: TcxGridDBColumn;
    GridDemandAndTaskMasterAART: TcxGridDBBandedColumn;
    GridDemandAndTaskMasterASZNAME: TcxGridDBBandedColumn;
    GridDemandAndTaskMasterAQ: TcxGridDBBandedColumn;
    GridDemandAndTaskMasterAFQ: TcxGridDBBandedColumn;
    GridDemandAndTaskMasterQ: TcxGridDBBandedColumn;
    DataArticlesDetailJAID: TIntegerField;
    DataArticlesDetailWHARTID: TIntegerField;
    DataArticlesDetailWHART: TStringField;
    DataArticlesDetailWHART2ID: TIntegerField;
    DataArticlesDetailWHSZID: TIntegerField;
    DataArticlesDetailWHSZNAME: TStringField;
    DataArticlesDetailWHOPERID: TStringField;
    DataArticlesDetailWHOPERNAME: TStringField;
    DataArticlesDetailWHUNITID: TIntegerField;
    DataArticlesDetailWHUNITNAME: TStringField;
    DataArticlesDetailWHQ: TIntegerField;
    cxStyle2: TcxStyle;
    cxStyle3: TcxStyle;
    DataArticlesMasterMaxQ: TIntegerField;
    GridDemandAndTaskMasterMaxQ: TcxGridDBBandedColumn;
    Detail: TClientDataSet;
    DetailJAID: TIntegerField;
    DetailWHARTID: TIntegerField;
    DetailWHART: TStringField;
    DetailWHART2ID: TIntegerField;
    DetailWHSZID: TIntegerField;
    DetailWHSZNAME: TStringField;
    DetailWHOPERID: TStringField;
    DetailWHOPERNAME: TStringField;
    DetailWHUNITID: TIntegerField;
    DetailWHUNITNAME: TStringField;
    DetailWHQ: TIntegerField;
    Master: TClientDataSet;
    MasterJAID: TIntegerField;
    MasterAARTID: TIntegerField;
    MasterAART: TStringField;
    MasterAART2ID: TIntegerField;
    MasterASZID: TIntegerField;
    MasterASZNAME: TStringField;
    MasterAQ: TIntegerField;
    MasterAFQ: TIntegerField;
    MasterAOPERID: TStringField;
    MasterAOPERNAME: TStringField;
    MasterAUNITID: TIntegerField;
    MasterAUNITNAME: TStringField;
    MasterQ: TIntegerField;
    MasterMaxQ: TIntegerField;
    DBDataArticlesMasterMAXQ: TFIBIntegerField;
    cxStyle4: TcxStyle;
    DataArticlesMasterMoreQ: TIntegerField;
    GridDemandAndTaskMasterColumn1: TcxGridDBBandedColumn;
    cbOnlyValidEntries: TcxCheckBox;
    cxStyle5: TcxStyle;
    DBDataArticlesDetailQ: TFIBIntegerField;
    DataArticlesDetailQ: TIntegerField;
    DetailQ: TIntegerField;
    procedure ActionCancelUpdate(Sender: TObject);
    procedure ActionOKUpdate(Sender: TObject);
    procedure ActionPriorUpdate(Sender: TObject);
    procedure ActionNextUpdate(Sender: TObject);
    procedure ActionNextExecute(Sender: TObject);
    procedure ActionPriorExecute(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure DataSetInvoiceAddedBeforeOpen(DataSet: TDataSet);
    procedure DataSetInvoiceRemovedBeforeOpen(DataSet: TDataSet);
    procedure DataSetInvoiceAddedNewRecord(DataSet: TDataSet);
    procedure DataSetInvoiceRemovedNewRecord(DataSet: TDataSet);
    procedure DataSetInvoiceAddedItemsBeforeOpen(DataSet: TDataSet);
    procedure DataSetInvoiceRemovedItemsBeforeOpen(DataSet: TDataSet);
    procedure DataSetInvoiceAddedItemsNewRecord(DataSet: TDataSet);
    procedure DataSetInvoiceRemovedItemsNewRecord(DataSet: TDataSet);
    procedure DataSetInvoiceAddedItemsBeforePost(DataSet: TDataSet);
    procedure DataSetInvoiceRemovedItemsBeforePost(DataSet: TDataSet);
    procedure DataSetSubStorageItemsBeforeOpen(DataSet: TDataSet);
    procedure DataSetSubStorageItemsNewRecord(DataSet: TDataSet);
    procedure DataSetSubStorageItemsBeforePost(DataSet: TDataSet);
    procedure DataArticlesMasterBeforeOpen(DataSet: TDataSet);
    procedure DataArticlesDetailBeforeOpen(DataSet: TDataSet);
    procedure GridDemandAndTaskMasterQStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure DataArticlesMasterBeforeEdit(DataSet: TDataSet);
    procedure DataArticlesMasterBeforePost(DataSet: TDataSet);
    procedure DataArticlesMasterAfterPost(DataSet: TDataSet);
    procedure DataArticlesMasterCalcFields(DataSet: TDataSet);

  private
    FAllQ: Integer;
    FDemandID: Integer;
    FQ: Integer;
    FJaID: Integer;
    FArtID: Integer;
    FDeltaQ: Integer;
    procedure DoJob;
    procedure Validate;
    procedure ExecuteSQL(Text: string; CommitRetaining: Boolean = False);
    function GetNewID(Index: Integer): Integer;
  public
    UserID: Integer;
    OrderID: Integer;
    OrderItemID: Integer;
    InvoiceAddedID: Integer;
    InvoiceRemovedID: Integer;
    SourceStorageID: Integer;
    TargetStorageID: Integer;
    TargetSubStorageID: Integer;
    TargetSubStorageOperationID: string;
    function Execute: Boolean;
  end;

var
  DialogTaskArticleOut: TDialogTaskArticleOut;

implementation

uses DictData, DateUtils, uUtils, Math;


{$R *.dfm}

{ TDialogTaskArticleOut }

function TDialogTaskArticleOut.Execute: Boolean;
var
  DemandCount: Integer;
begin
  Result := False;
  if Transaction.Active then Transaction.Commit;
  Transaction.StartTransaction;
  FDemandID := 0;
  DataSetDemand.Active := True;
  DemandCount := DataSetDemand.RecordCount;
  if DemandCount <> 0 then
  begin
    if DemandCount = 1 then
    PageControl.ActivePageIndex := PageControl.PageCount - 1;
    if ShowModal = mrOK then
    begin
      try
        DoJob;
        Result := True;
      except
        Transaction.RollbackRetaining;
      end;
    end;
    DataArticlesDetail.Active := False;
    DataArticlesMaster.Active := False;
  end
  else InformationMessage('������ ����������.');
  DataSetDemand.Active := False;
  if Transaction.Active then Transaction.Commit;
  if TransactionTmp.Active then TransactionTmp.Commit;
end;


procedure TDialogTaskArticleOut.ActionOKUpdate(Sender: TObject);
begin
  ActionOK.Enabled := FAllQ <> 0;
end;


procedure TDialogTaskArticleOut.ActionCancelUpdate(Sender: TObject);
begin
  PanelCancel.Visible := True;
  PanelOK.Visible := PageControl.ActivePageIndex = PageControl.PageCount - 1;
  PanelNext.Visible := PageControl.ActivePageIndex <> PageControl.PageCount - 1;
  PanelPrior.Visible := PageControl.ActivePageIndex = PageControl.PageCount - 1;
  cbOnlyValidEntries.Visible := PageControl.ActivePageIndex <> PageControl.PageCount - 1;
  ActionCancel.Enabled := True;
end;


procedure TDialogTaskArticleOut.ActionPriorUpdate(Sender: TObject);
begin
  ActionPrior.Enabled := True;
end;

procedure TDialogTaskArticleOut.ActionNextUpdate(Sender: TObject);
begin
  ActionNext.Enabled := True;
end;

procedure TDialogTaskArticleOut.ActionNextExecute(Sender: TObject);
var
  ActivePageIndex: Integer;
begin
  ActivePageIndex := PageControl.ActivePageIndex;
  PageControl.Pages[ActivePageIndex].TabVisible := False;
  ActivePageIndex := ActivePageIndex + 1;
  PageControl.Pages[ActivePageIndex].TabVisible := True;
  PageControl.ActivePageIndex := ActivePageIndex;
end;

procedure TDialogTaskArticleOut.ActionPriorExecute(Sender: TObject);
var
  ActivePageIndex: Integer;
begin
  ActivePageIndex := PageControl.ActivePageIndex;
  PageControl.Pages[ActivePageIndex].TabVisible := False;
  ActivePageIndex := ActivePageIndex - 1;
  PageControl.Pages[ActivePageIndex].TabVisible := True;
  PageControl.ActivePageIndex := ActivePageIndex;
end;


procedure TDialogTaskArticleOut.DoJob;
var
  JaID: Integer;

  DeltaQ: Integer;
  OldArtID: integer;
  OldArt2ID: integer;
  OldSzID: integer;
  OldU: integer;
  OldOperId: string;
  NewArtID: integer;
  NewArt2ID: integer;
  NewSzID: integer;
  NewU: integer;
  NewOperId: string;
  FQ: Integer;
begin

  ExecuteSQL('Select InvID from inv where IType = 9 and Ref = ' + IntToStr(OrderItemID) + ' and WOrderID is not null');
  InvoiceAddedID := QueryTmp.Fields[0].AsInteger;

  ExecuteSQL('Select InvID from inv where IType = 8 and Ref = ' + IntToStr(OrderItemID) + ' and WOrderID is not null');
  InvoiceRemovedID := QueryTmp.Fields[0].AsInteger;

  DataSetInvoiceAdded.Active := True;
  if (InvoiceAddedID = 0) or DataSetInvoiceAdded.IsEmpty then
  begin
    DataSetInvoiceAdded.Append;
    DataSetInvoiceAdded.Post;
    DataSetInvoiceAdded.Transaction.CommitRetaining;
    DataSetInvoiceAdded.Active := False;
    DataSetInvoiceAdded.Active := True;
  end;

  DataSetInvoiceRemoved.Active := True;
  if (InvoiceRemovedID = 0) or DataSetInvoiceRemoved.IsEmpty then
  begin
    DataSetInvoiceRemoved.Append;
    DataSetInvoiceRemoved.Post;
    DataSetInvoiceRemoved.Transaction.CommitRetaining;
    DataSetInvoiceRemoved.Active := False;
    DataSetInvoiceRemoved.Active := True;
  end;

  DataSetSubStorageItems.Active := True;
  DataSetInvoiceRemovedItems.Active := True;
  DataSetInvoiceAddedItems.Active := True;

  if DataArticlesMaster.State = dsEdit then
  DataArticlesMaster.Post;

  Master.CloneCursor(DataArticlesMaster, True, False);
  Detail.CloneCursor(DataArticlesDetail, True, False);
  Detail.IndexFieldNames := 'JAID';

  if Master.Active and Detail.Active then
  begin
    Master.First;
    while not Master.Eof do
    begin
      DeltaQ := MasterQ.AsInteger;
      if DeltaQ <> 0 then
      begin
        JaID := MasterJAID.AsInteger;
        FQ := MasterAFQ.AsInteger + DeltaQ;
        ExecuteSQL('Update JA set FQ = ' + IntToStr(FQ) + ' where ID = '  + IntToStr(JaID), False);
        Detail.SetRange([JaID],[JaID]);
        Detail.First;

        NewArtID  := MasterAARTID.AsInteger;
        NewArt2ID := MasterAART2ID.AsInteger;
        NewSzID   := MasterASZID.AsInteger;
        NewU      := DetailWHUNITID.AsInteger;
        NewOperID := DetailWHOPERID.AsString;


        while not Detail.Eof do
        begin

          OldArtID  := DetailWHARTID.AsInteger;
          OldArt2ID := DetailWHART2ID.AsInteger;
          OldSzID   := DetailWHSZID.AsInteger;
          OldU      := DetailWHUNITID.AsInteger;
          OldOperID := DetailWHOPERID.AsString;

          DataSetInvoiceRemovedItems.First;
          if DataSetInvoiceRemovedItems.Locate('ART2ID;SZID;OPERID;U', VarArrayOf( [OldArt2Id, OldSzId, OldOperId, OldU]), []) then
          begin
            DataSetInvoiceRemovedItems.Edit;
          end
          else DataSetInvoiceRemovedItems.Insert;
          DataSetInvoiceRemovedItems.Post;

          Detail.Next;
        end;
        DataSetInvoiceAddedItems.First;
        if DataSetInvoiceAddedItems.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([NewArt2Id, NewSzId, NewOperId, NewU]), []) then
        begin
          DataSetInvoiceAddedItems.Edit;
        end
        else DataSetInvoiceAddedItems.Insert;
        DataSetInvoiceAddedItems.Post;

        DataSetSubStorageItems.First;
        if DataSetSubStorageItems.Locate('ART2ID;SZID;OPERIDFROM;U_OLD', VarArrayOf( [NewArt2Id, NewSzId, NewOperId, NewU]), []) then
        begin
          DataSetSubStorageItems.Edit;
        end
        else DataSetSubStorageItems.Append;
        DataSetSubStorageItems.Post;
      end;
      Master.Next;
    end;
  end;
  Detail.Active := False;
  Master.Active := False;

  DataSetSubStorageItems.Active := False;
  DataSetInvoiceRemovedItems.Active := False;
  DataSetInvoiceAddedItems.Active := False;
  DataSetInvoiceRemoved.Active := False;
  DataSetInvoiceAdded.Active := False;
end;

procedure TDialogTaskArticleOut.ExecuteSQL(Text: string; CommitRetaining: Boolean = False);
begin
  if Text <> '' then
  begin
    if QueryTmp.Open then
    begin
      QueryTmp.Close;
    end;
    if QueryTmp.Transaction.Active then
    begin
      if CommitRetaining then
      QueryTmp.Transaction.CommitRetaining;
    end;
    QueryTmp.Transaction.StartTransaction;
    QueryTmp.SQL.Text := Text;
    QueryTmp.ExecQuery;
  end;
end;

procedure TDialogTaskArticleOut.DataSetInvoiceAddedBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('ID').AsInteger := InvoiceAddedID;
end;

procedure TDialogTaskArticleOut.DataSetInvoiceRemovedBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('ID').AsInteger := InvoiceRemovedID;
end;

procedure TDialogTaskArticleOut.DataSetInvoiceAddedNewRecord(DataSet: TDataSet);
begin
  InvoiceAddedID := GetNewID(CONST_InvoiceAddedID);

  DataSetInvoiceAddedINVID.AsInteger := InvoiceAddedID;
  DataSetInvoiceAddedDOCNO.AsInteger := GetNewID(CONST_InvoiceAddedDOCNO);
  DataSetInvoiceAddedDOCDATE.AsDateTime := ToDay;
  DataSetInvoiceAddedITYPE.AsInteger := CONST_InvoiceAddedITYPE;
  DataSetInvoiceAddedDEPID.AsInteger := SourceStorageID;
  DataSetInvoiceAddedUSERID.AsInteger := UserID;
  DataSetInvoiceAddedREF.AsInteger := OrderItemID;
  DataSetInvoiceAddedWORDERID.AsInteger := OrderID;
end;

procedure TDialogTaskArticleOut.DataSetInvoiceRemovedNewRecord(DataSet: TDataSet);
begin
  InvoiceRemovedID := GetNewID(CONST_InvoiceRemovedID);

  DataSetInvoiceRemovedINVID.AsInteger := InvoiceRemovedID;
  DataSetInvoiceRemovedDOCNO.AsInteger := GetNewID(CONST_InvoiceRemovedDOCNO);
  DataSetInvoiceRemovedDOCDATE.AsDateTime := ToDay;
  DataSetInvoiceRemovedITYPE.AsInteger := CONST_InvoiceRemovedITYPE;
  DataSetInvoiceRemovedDEPID.AsInteger := SourceStorageID;
  DataSetInvoiceRemovedUSERID.AsInteger := UserID;
  DataSetInvoiceRemovedREF.AsInteger := OrderItemID;
  DataSetInvoiceRemovedWORDERID.AsInteger := OrderID;
end;

procedure TDialogTaskArticleOut.DataSetInvoiceAddedItemsBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('INVID').AsInteger := InvoiceAddedID;
end;

procedure TDialogTaskArticleOut.DataSetInvoiceRemovedItemsBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('INVID').AsInteger := InvoiceRemovedID;
end;

procedure TDialogTaskArticleOut.DataSetInvoiceAddedItemsNewRecord(DataSet: TDataSet);
var
  NewArtID: integer;
  NewArt2ID: integer;
  NewSzID: integer;
  NewU: integer;
  NewOperId: string;
begin
  NewArtID  := MasterAARTID.AsInteger;
  NewArt2ID := MasterAART2ID.AsInteger;
  NewSzID   := MasterASZID.AsInteger;
  NewU      := DetailWHUNITID.AsInteger;
  NewOperID := DetailWHOPERID.AsString;

  DataSetInvoiceAddedItemsID.AsInteger := GetNewID(CONST_InvoiceAddedItemsID);
  DataSetInvoiceAddedItemsINVID.AsInteger := InvoiceAddedID;
  DataSetInvoiceAddedItemsT.AsInteger := 3;
  DataSetInvoiceAddedItemsARTID.AsInteger := NewArtID;
  DataSetInvoiceAddedItemsART2ID.AsInteger := NewArt2ID;
  DataSetInvoiceAddedItemsSZID.AsInteger := NewSzID;
  DataSetInvoiceAddedItemsU.AsInteger := NewU;
  DataSetInvoiceAddedItemsOPERID.AsString := NewOperId;
  DataSetInvoiceAddedItemsCOMMENTID.AsInteger := 3;
  DataSetInvoiceAddedItemsCOMMENT.AsString := '�� ����������������� ������';
end;

procedure TDialogTaskArticleOut.DataSetInvoiceRemovedItemsNewRecord(DataSet: TDataSet);
var
  OldArtID: integer;
  OldArt2ID: integer;
  OldSzID: integer;
  OldU: integer;
  OldOperId: string;
begin
  OldArtID  := DetailWHARTID.AsInteger;
  OldArt2ID := DetailWHART2ID.AsInteger;
  OldSzID   := DetailWHSZID.AsInteger;
  OldU      := DetailWHUNITID.AsInteger;
  OldOperID := DetailWHOPERID.AsString;

  DataSetInvoiceRemovedItemsID.AsInteger := GetNewID(CONST_InvoiceRemovedItemsID);
  DataSetInvoiceRemovedItemsINVID.AsInteger := InvoiceRemovedID;
  DataSetInvoiceRemovedItemsT.AsInteger := 2;
  DataSetInvoiceRemovedItemsARTID.AsInteger := OldArtID;
  DataSetInvoiceRemovedItemsART2ID.AsInteger := OldArt2ID;
  DataSetInvoiceRemovedItemsSZID.AsInteger := OldSzID;
  DataSetInvoiceRemovedItemsU.AsInteger := OldU;
  DataSetInvoiceRemovedItemsOPERID.AsString := OldOperId;
  DataSetInvoiceRemovedItemsCOMMENTID.AsInteger := 2;
  DataSetInvoiceRemovedItemsCOMMENT.AsString := '�� ����������������� ������';
end;

procedure TDialogTaskArticleOut.DataSetInvoiceAddedItemsBeforePost(DataSet: TDataSet);
var
  OldQ: Integer;
  NewQ: Integer;
  DeltaQ: Integer;
begin
  DeltaQ := MasterQ.AsInteger;
  if DataSet.State = dsInsert then
  begin
    NewQ := DeltaQ;
  end
  else
  if DataSet.State = dsEdit then
  begin
    OldQ := DataSetInvoiceAddedItemsQ.AsInteger;
    NewQ := OldQ + DeltaQ;
  end;
  DataSetInvoiceAddedItemsQ.AsInteger := NewQ;
end;

procedure TDialogTaskArticleOut.DataSetInvoiceRemovedItemsBeforePost(DataSet: TDataSet);
var
  OldQ: Integer;
  NewQ: Integer;
  DeltaQ: Integer;
begin
  DeltaQ := MasterQ.AsInteger;
  if DataSet.State = dsInsert then
  begin
    NewQ := DeltaQ;
  end
  else
  if DataSet.State = dsEdit then
  begin
    OldQ := DataSetInvoiceRemovedItemsQ.AsInteger;
    NewQ := OldQ + DeltaQ;
  end;
  DataSetInvoiceRemovedItemsQ.AsInteger := NewQ;
end;

function TDialogTaskArticleOut.GetNewID(Index: Integer): Integer;
begin
  Result := dm.GetId(Index);
end;

procedure TDialogTaskArticleOut.DataSetSubStorageItemsBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('INVID').AsInteger := TargetSubStorageID;
end;

procedure TDialogTaskArticleOut.DataSetSubStorageItemsNewRecord(DataSet: TDataSet);
var
  NewArtID: integer;
  NewArt2ID: integer;
  NewSzID: integer;
  NewU: integer;
  NewOperId: string;
begin
  NewArtID  := MasterAARTID.AsInteger;
  NewArt2ID := MasterAART2ID.AsInteger;
  NewSzID   := MasterASZID.AsInteger;
  NewU      := DetailWHUNITID.AsInteger;
  NewOperID := DetailWHOPERID.AsString;

  DataSetSubStorageItemsID.AsInteger := GetNewID(CONST_SubStorageItemsID);
  DataSetSubStorageItemsT.AsInteger := 1;
  DataSetSubStorageItemsINVID.AsInteger := TargetSubStorageID;
  DataSetSubStorageItemsARTID.AsInteger := NewArtID;
  DataSetSubStorageItemsART2ID.AsInteger := NewArt2ID;
  DataSetSubStorageItemsSZID.AsInteger := NewSzID;
  DataSetSubStorageItemsU.AsInteger := NewU;
  DataSetSubStorageItemsU_OLD.AsInteger := NewU;
  DataSetSubStorageItemsQ.AsInteger := 0;

  DataSetSubStorageItemsOPERID.AsString := TargetSubStorageOperationID;
  DataSetSubStorageItemsOPERIDFROM.AsString := NewOperId;
  DataSetSubStorageItemsART2ID_OLD.AsVariant := NewArt2ID;

end;

procedure TDialogTaskArticleOut.DataSetSubStorageItemsBeforePost(DataSet: TDataSet);
var
  OldQ: Integer;
  NewQ: Integer;
  DeltaQ: Integer;
begin
  DeltaQ := MasterQ.AsInteger;
  if DataSet.State = dsInsert then
  begin
    NewQ := DeltaQ;
  end
  else
  if DataSet.State = dsEdit then
  begin
    OldQ := DataSetSubStorageItemsQ.AsInteger;
    NewQ := OldQ + DeltaQ;
  end;
  DataSetSubStorageItemsQ.AsInteger := NewQ;
end;

procedure TDialogTaskArticleOut.DataArticlesMasterBeforeOpen(DataSet: TDataSet);
begin
  DataArticlesMaster.Params.ParamByName('DemandID').AsInteger := FDemandID;
end;

procedure TDialogTaskArticleOut.DataArticlesDetailBeforeOpen(DataSet: TDataSet);
begin
  DataArticlesDetail.Params.ParamByName('DemandID').AsInteger := FDemandID;
  DataArticlesDetail.Params.ParamByName('SourceStorageID').AsInteger := SourceStorageID;
end;

procedure TDialogTaskArticleOut.GridDemandAndTaskMasterQStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  MaxQ: Variant;
  Q: Variant;
begin
  MaxQ := ARecord.Values[0];
  Q := ARecord.Values[5];
  if VarIsNull(Q) or  VarIsNull(MaxQ) then
  begin
    Exit;
  end;

  if MaxQ < 0 then AStyle := cxStyle2 // Red
  else
  begin
    if MaxQ = 0 then AStyle := cxStyle3 // Yellow
    else AStyle := cxStyle4; // Green
  end;
end;

procedure TDialogTaskArticleOut.DataArticlesMasterBeforeEdit(DataSet: TDataSet);
begin
  FQ := DataArticlesMasterQ.AsInteger;
  FJaID := DataArticlesMasterJaID.AsInteger;
  FArtID := DataArticlesMasterAARTID.AsInteger;
end;

procedure TDialogTaskArticleOut.DataArticlesMasterBeforePost(DataSet: TDataSet);
var
  Q: Integer;
  MaxQ: Integer;
begin
  Q := DataArticlesMasterQ.AsInteger;
  MaxQ := DataArticlesMasterMaxQ.AsInteger;
  if Q < 0 then Q := 0;
  //if Q > MaxQ then Q := MaxQ;
  DataArticlesMasterQ.AsInteger := Q;
  FDeltaQ := Q - FQ;
end;

procedure TDialogTaskArticleOut.DataArticlesMasterAfterPost(DataSet: TDataSet);
var
  Art2ID: Integer;
  Source: TClientDataSet;
  Target: TClientDataSet;
  MasterArticleID: Integer;
  ChildArticleID: Integer;
begin
  if FDeltaQ <> 0 then
  begin
    FAllQ := FAllQ + FDeltaQ;

    Source := TClientDataSet.Create(nil);
    Source.CloneCursor(DataArticlesDetail, True, False);
    Source.IndexFieldNames := 'JaID';

    Target := TClientDataSet.Create(nil);
    Target.CloneCursor(DataArticlesDetail, True, False);
    Target.IndexFieldNames := 'WHART2ID';

    GridDemandAndTaskDetail.BeginUpdate;

    Source.SetRange([FJaID], [FJaID]);
    Source.First;
    while not Source.Eof do
    begin
      Art2ID := Source.FieldByName('WHART2ID').AsInteger;
      Target.SetRange([Art2ID], [Art2ID]);
      Target.First;
      while not Target.Eof do
      begin
        Target.Edit;
        Target.FieldByName('WHQ').AsInteger := Target.FieldByName('WHQ').AsInteger - FDeltaQ*Target.FieldByName('Q').AsInteger;
        Target.Post;
        Target.Next;
      end;
      Source.Next;
    end;
    Target.Free;
    Source.Free;
    GridDemandAndTaskDetail.EndUpdate;
    Validate;
  end;
end;


procedure TDialogTaskArticleOut.PageControlChange(Sender: TObject);
var
  DemandID: Integer;

begin
  if PageControl.ActivePageIndex = PageControl.PageCount - 1 then
  begin
    DemandID := DataSetDemandID.AsInteger;
    //if DemandID <> FDemandID then
    begin
       FDemandID := DemandID;

       DataArticlesDetail.Active := False;
       DataArticlesMaster.Active := False;


       GridDemandAndTaskMaster.DataController.DataSource := nil;
       GridDemandAndTaskDetail.DataController.DataSource := nil;

       DataArticlesMaster.ProviderName := 'ProviderArticlesMaster';
       DataArticlesDetail.ProviderName := 'ProviderArticlesDetail';
       DataArticlesMaster.Active := True;
       DataArticlesDetail.Active := True;
       DataArticlesMaster.LogChanges := False;
       DataArticlesDetail.LogChanges := False;
       DataArticlesMaster.ProviderName := '';
       DataArticlesDetail.ProviderName := '';

       Validate;

       GridDemandAndTaskMaster.DataController.DataSource := SourceArticlesMaster;
       GridDemandAndTaskDetail.DataController.DataSource := SourceArticlesDetail;

       //GridDemandAndTaskMaster.ViewData.Expand(False);
    end;
  end;
end;


procedure TDialogTaskArticleOut.Validate;
var
  JaID: Integer;
  i: Integer;
  c: Integer;
  Q: Integer;
  StorageQ: Integer;
  MaxQ: Integer;
  Master: TClientDataSet;
  Detail: TClientDataSet;
  Flag: Boolean;
  Partial: TList;
begin
  Partial := TList.Create;

  GridDemandAndTaskMaster.BeginUpdate;

  Master := TClientDataSet.Create(nil);
  Master.CloneCursor(DataArticlesMaster, True, False);

  Detail := TClientDataSet.Create(nil);
  Detail.CloneCursor(DataArticlesDetail, True, False);
  Detail.IndexFieldNames := 'JAID';

  Master.First;
  while not Master.Eof do
  begin
    JaID := Master.FieldByName('JAID').AsInteger;
    Q := Master.FieldByName('Q').AsInteger;

    MaxQ := MaxInt;

    Detail.SetRange([JaID],[JaID]);
    Detail.First;
    Flag := False;
    while not Detail.Eof do
    begin
      StorageQ := Detail.FieldByName('WHQ').AsInteger div Detail.FieldByName('Q').AsInteger;
      if StorageQ > 0 then Flag := True;
      MaxQ := Min(StorageQ, MaxQ);
      Detail.Next;
    end;
    if MaxQ <> MaxInt then
    begin
      if Flag then
      begin
        if Partial.IndexOf(Pointer(JaID)) = -1 then
        Partial.Add(Pointer(JaID));
      end;

      Master.Edit;
      Master.FieldByName('MaxQ').AsInteger := {Q + }MaxQ;
      Master.Post;
    end
    else
    begin
      Master.Edit;
      Master.FieldByName('MaxQ').AsInteger := 0;
      Master.Post;
    end;
    Master.Next;
  end;

  GridDemandAndTaskMaster.EndUpdate;

  if GridDemandAndTaskMaster.DataController.DataSource = nil then
  begin
    if cbOnlyValidEntries.Checked then
    begin
      c := Master.RecordCount;
      for i := c downto 1 do
      begin
        Master.RecNo := i;
        JaID := Master.FieldByName('JAID').AsInteger;
        if Master.FieldByName('MaxQ').AsInteger <= 0 then
        begin
          if Partial.IndexOf(Pointer(JaID)) = -1 then
          Master.Delete;
        end;
      end;
    end;
  end;
  Master.Free;
  Detail.Free;

  Partial.Free;
end;

procedure TDialogTaskArticleOut.DataArticlesMasterCalcFields(
  DataSet: TDataSet);
begin
  DataArticlesMasterMoreQ.AsInteger := DataArticlesMasterMaxQ.AsInteger - DataArticlesMasterQ.AsInteger;
end;

end.
