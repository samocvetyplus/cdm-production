unit dPrice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ImgList, Grids, DBGrids, RXDBCtrl,DB,
   ExtCtrls, ComCtrls, dbUtil,  //bsUtils,
  StdCtrls, Menus, fmUtils, ActnList, Buttons, DBGridEh, DBCtrlsEh, Mask,
  ArtImage, RxToolEdit, DateUtils, TB2Item, InvData, DBGridEhGrouping,
  rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmPriceDict = class(TfmDictAncestor)
    tb2: TSpeedBar;
    laDep: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    spitDep: TSpeedItem;
    ppDep: TPopupMenu;
    mnitAllWh: TMenuItem;
    mnitSep: TMenuItem;
    acNewPrice: TAction;
    SpeedItem4: TSpeedItem;
    acNewPriceForU: TAction;
    SpeedItem5: TSpeedItem;
    grArt: TDBGridEh;
    SpeedItem6: TSpeedItem;
    SpeedItem7: TSpeedItem;
    grArt2: TDBGridEh;
    cbAll: TDBCheckBoxEh;
    acRet: TAction;
    acCalc: TAction;
    SpeedItem8: TSpeedItem;
    acDeal: TAction;
    SpeedItem9: TSpeedItem;
    acImage: TAction;
    Label2: TLabel;                    
    DBEditEh2: TDBEditEh;
    Label1: TLabel;
    DBEditEh1: TDBEditEh;
    SpeedItem10: TSpeedItem;
    acCode: TAction;
    cbKind: TDBComboBoxEh;
    SpeedItem11: TSpeedItem;
    acDelPArt: TAction;
    TBPopupMenu1: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    TBItem4: TTBItem;
    SpeedItem12: TSpeedItem;
    acIns: TAction;
    SpeedItem13: TSpeedItem;
    acAll: TAction;
    edDate: TDBDateTimeEditEh;
    SpeedBar1: TSpeedBar;
    SpeedbarSection3: TSpeedbarSection;
    edART: TDBEditEh;
    Label3: TLabel;
    SpeedItem14: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure grGetCellParams(Sender: TObject; Field: TField; AFont: TFont;
      var Background: TColor; Highlight: Boolean);
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acNewPriceForUExecute(Sender: TObject);
    procedure acRetExecute(Sender: TObject);
    procedure acCalcExecute(Sender: TObject);
    procedure grArtGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acDealExecute(Sender: TObject);
    procedure DBEditEh1Exit(Sender: TObject);
    procedure acImageExecute(Sender: TObject);
    procedure DBEditEh2Exit(Sender: TObject);
    procedure acCodeExecute(Sender: TObject);
    procedure cbAllClick(Sender: TObject);
    procedure cbKindChange(Sender: TObject);
    procedure acInsExecute(Sender: TObject);
    procedure acDelPArtExecute(Sender: TObject);
    procedure SpeedItem13Click(Sender: TObject);
    procedure grArt2Exit(Sender: TObject);
    procedure edARTChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedItem14Click(Sender: TObject);
  protected
   procedure DepClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPriceDict: TfmPriceDict;

implementation

uses DictData, NewPrice, eNewPriceU, ApplData, dCode,
   eArtIns, dArt;

{$R *.dfm}

procedure TfmPriceDict.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  laDep.Caption := TMenuItem(Sender).Caption;
  dmINV.FCurrDep := TMenuItem(Sender).Tag;
//  ReOpenDataSets([dmINV.taPriceDict]);
end;

procedure TfmPriceDict.FormCreate(Sender: TObject);
var
  i, j : integer;
  Item : TMenuItem;
begin
  dm.CurrDep := 23;
  inherited;
  mnitAllWh.OnClick := DepClick;
  laDep.Caption := mnitAllWh.Caption;
  //���������� PopupMenu �� FDepInfo;
  j := 0;
//  spitExport.Visible:=true;
  for i:=0 to Pred(dm.CountDep) do
  begin
  if dm.DepInfo[i].Wh then
  begin
    Item := TMenuItem.Create(ppDep);
    Item.Caption := dm.DepInfo[i].SName;
    Item.Tag := dm.DepInfo[i].DepId;
    Item.OnClick := DepClick;
    Item.RadioItem := True;
    if Item.Tag = 23{ dm.DepDef } then DepClick(Item);
    inc(j);
    if j>30 then
    begin
      Item.Break := mbBreak;
      j:=0;
    end;
    ppDep.Items.Add(Item);
  end;
  end;
  dmINV.FCurrDep:=ROOT_COMP_CODE;
  dmINV.FCurrActPrice:=0; // ���� �� ����������
  REOpenDataSets([{dmINV.taPriceDict,} dminv.taARTPrice, dminv.taARTPriceDet, dmInv.taConst, dmInv.taConst2]);
  dminv.taConst.Locate('NAME','���� �� �����',[loCaseInsensitive]);
  dminv.taConst2.Locate('NAME','���������� ����',[loCaseInsensitive]);
  grArt.Align:=alLeft;
  grArt2.Align:=alClient;
  edDate.Value:=Today;
  cbKind.ItemIndex:=0;
  dminv.FCurrU:=0;
end;

procedure TfmPriceDict.grGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  inherited;
  if FIELD.FieldName='PRICE' then Background:=clAqua;
  if FIELD.FieldName='TPRICE' then Background:=clAqua;
  if (dmINV.taPriceDictUNIT.AsString='��.') then Background:=clMoneyGreen;
  if (dmINV.taPriceDictUNIT.asstring='��.') then Background:=clSell;
end;

procedure TfmPriceDict.SpeedItem1Click(Sender: TObject);
begin
  inherited;
  if dmINV.FCurrDep<>ROOT_COMP_CODE then
  ShowAndFreeForm(TfmNewPrice,TForm(fmNewPrice))
  else showmessage('�������� ����� ��� �������� ����� ����������')
end;

procedure TfmPriceDict.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
{  with dminv do
  begin
   try
    fmWorkingSplash:=TfmWorkingSplash.Create(self);
    fmWorkingSplash.rxlabel1.Caption:='����������� ��� ����������';
    fmWorkingSplash.Show;
    fmWorkingSplash.Update;
    Application.ProcessMessages;
    sqlCreateActPrice.ParamByName('NEW_DATE').AsDateTime:=edDate.Value;
    sqlCreateActPrice.ParamByName('USERID').AsInteger:=dm.User.UserId;
    sqlCreateActPrice.ParamByName('DEPID').AsInteger:=dm.CurrDep; //dmINV.FCurrDep;
    sqlCreateActPrice.ExecQuery;
    sqlCreateActPrice.Transaction.CommitRetaining;
    fmWorkingSplash.Close;
   finally
     fmWorkingSplash.Free;
   end;
  end;}
  PostDataSet(dminv.taARTPriceDet);
  dmInv.sqlClearSelectUp.ExecQuery;
  dmInv.sqlClearSelectUp.Transaction.CommitRetaining;
  CloseDataSets([dmINV.taPriceDict,dminv.taARTPrice, dminv.taARTPriceDet, dmInv.taConst, dmInv.taConst2]);
end;

procedure TfmPriceDict.acNewPriceForUExecute(Sender: TObject);
begin
  inherited;
  if dmINV.FCurrDep<>ROOT_COMP_CODE then
  ShowAndFreeForm(TfmNewPriceU,TForm(fmNewPriceU))
  else showmessage('�������� ����� ��� �������� ����� ����������')
end;

procedure TfmPriceDict.acRetExecute(Sender: TObject);
begin
  inherited;
  with dminv do
  begin
    sqlReturnPrice.ParamByName('ART2ID').AsInteger:=taARTPriceDetART2ID.AsInteger;
    if cball.Checked then    sqlReturnPrice.ParamByName('ALL_').AsInteger:=0
    else sqlReturnPrice.ParamByName('ALL_').AsInteger:=CbKind.ItemIndex+1;
    sqlReturnPrice.ExecQuery;
    sqlReturnPrice.Transaction.CommitRetaining;
    RefreshDataSet(taARTPriceDet);
  end;
end;

procedure TfmPriceDict.acCalcExecute(Sender: TObject);
var art2:string;
begin
  inherited;
  Screen.Cursor:=crSqLWait;
  try
  with dminv do
  begin
    if taConst.State in [dsEdit] then taConst.Post;
    if taConst2.State in [dsEdit] then taConst2.Post;
    sqlCalcPrice.ParamByName('ART2ID').AsInteger:=taARTPriceDetART2ID.AsInteger;
    if cball.Checked then    sqlCalcPrice.ParamByName('ALL_').AsInteger:=0
    else sqlCalcPrice.ParamByName('ALL_').AsInteger:=CbKind.ItemIndex+1;
    sqlCalcPrice.ExecQuery;
    sqlCalcPrice.Transaction.CommitRetaining;
    art2:=taARTPriceDetART2.AsString;
    if cbAll.Checked then  RefreshDataSet(taARTPriceDet)
    else ReOpenDataSet(taARTPriceDet);
    taARTPriceDet.Locate('ART2',art2,[loCaseInsensitive]);
//    ReopenDataSet(taARTPriceDet);
  end;
  finally
   Screen.Cursor:=crDefault;
  end;
end;

procedure TfmPriceDict.grArtGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if (Column.FieldName='K') and (dmInv.taARTPriceK.AsFloat>0) then Background:=clMoneyGreen;
end;

procedure TfmPriceDict.acDealExecute(Sender: TObject);
var art2:string;
begin
  inherited;
   with dminv do
  begin
    sqlDealPrice.ParamByName('ART2ID').AsInteger:=taARTPriceDetART2ID.AsInteger;
    if cball.Checked
    then sqlDealPrice.ParamByName('ALL_').AsInteger:=0
    else sqlDealPrice.ParamByName('ALL_').AsInteger:=CbKind.ItemIndex+1;
    sqlDealPrice.ExecQuery;
    sqlDealPrice.Transaction.CommitRetaining;
    art2:=taARTPriceDetART2.AsString;
    ReopenDataSet(taARTPriceDet);
    taARTPriceDet.Locate('ART2',art2,[loCaseInsensitive]);
  end;
end;

procedure TfmPriceDict.DBEditEh1Exit(Sender: TObject);
begin
  inherited;
  try
  if dminv.taConst.State in [dsEdit] then dminv.taConst.Post;
//  dminv.taConst.Transaction.CommitRetaining;
  except

  end;
end;

procedure TfmPriceDict.acImageExecute(Sender: TObject);
begin
  inherited;
 if Assigned(dmAppl.FArtImage) then
  begin
    dmAppl.FArtImage.Close;
    dmAppl.FArtImage := nil;
  end
  else begin
    dmAppl.FArtImage := TfmImage.Create(Self);
//  dmAppl.FArtImage.Assign(TPicture(dmInv.taARTPricePict));
    dmAppl.FArtImage.Caption := '�������: '+dmAppl.taApplART.AsString;
    dmAppl.FArtImage.Show;
  end;
  acImage.Checked := Assigned(dmAppl.FArtImage);
end;

procedure TfmPriceDict.DBEditEh2Exit(Sender: TObject);
begin
  inherited;
   try
  if dminv.taConst2.State in [dsEdit] then dminv.taConst2.Post;
//  dminv.taConst.Transaction.CommitRetaining;
  except

  end;
end;

procedure TfmPriceDict.acCodeExecute(Sender: TObject);
begin
  inherited;
  ShowAndFreeForm(TfmCode, TForm(fmCode));
  ReOpenDataSets([dmInv.taArtPrice,dmInv.taARTPriceDet]);
end;
procedure TfmPriceDict.cbAllClick(Sender: TObject);
begin
  inherited;
  if cbAll.Checked then cbKind.Enabled:=false
  else cbKind.Enabled:=True;
end;

procedure TfmPriceDict.cbKindChange(Sender: TObject);
begin
  inherited;
  dminv.FCurrU:=cbKind.ItemIndex;
  dmInv.taARTPrice.Filtered:=true;
  ReOpenDataSets([dmInv.taARTPrice,dmInv.taARTPriceDet]);
end;

procedure TfmPriceDict.acInsExecute(Sender: TObject);
begin
  dmInv.CurrArt2Id:=dminv.taARTPriceDetART2ID.AsInteger;
  if fmArtIns=nil then
  begin
    fmArtIns:=TfmArtIns.Create(self);
    fmArtIns.Show;
    fmArtIns.Caption := '�������: '+dmInv.taARTPriceART.AsString+' '+dmInv.taARTPriceDetART2.AsString;
  end;
end;

procedure TfmPriceDict.acDelPArtExecute(Sender: TObject);
begin
//  inherited;
   if MessageDlg('������� ������?',mtWarning,[mbOk,mbCancel],0)=mrOk then
    begin
     dminv.taARTPriceDet.Delete;
     dminv.taARTPriceDet.Transaction.CommitRetaining;
  {   dminv.sqlDeleteArt2.ParamByName('Art2id').asInteger:=dminv.taARTPriceDetART2ID.asinteger;;
     dminv.sqlDeleteArt2.Prepare;
     dminv.sqlDeleteArt2.ExecQuery;
     dminv.sqlDeleteArt2.Transaction.CommitRetaining;}
     ReOpenDataSet(dminv.taARTPriceDet);
    end;
end;

procedure TfmPriceDict.SpeedItem13Click(Sender: TObject);
begin
  inherited;
  if cbKind.ItemIndex =0
  then  dmInv.sqlCheckArt.ParamByName('All').AsInteger:=1
  else  dmInv.sqlCheckArt.ParamByName('All').AsInteger:=0;
  dmInv.sqlCheckArt.ParamByName('Check').AsInteger:=1;
  dmInv.sqlCheckArt.ParamByName('Artid').asinteger:=dminv.taARTPriceARTID.AsInteger;
  dmInv.sqlCheckArt.ExecQuery;
  dmInv.sqlCheckArt.Transaction.CommitRetaining;
  ReOpenDataSet(dminv.taARTPriceDet);
end;

procedure TfmPriceDict.grArt2Exit(Sender: TObject);
begin
  if dmInv.taARTPriceDet.State in [dsEdit] then  dmInv.taARTPriceDet.post;
end;

procedure TfmPriceDict.edARTChange(Sender: TObject);
begin
  inherited;
    with dmINV, taARTPrice do
     if Active then Locate('ART', edART.Text, [loPartialKey]);
end;

procedure TfmPriceDict.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
//  if ((Shift in [ssCtrl]) and (Key=Key_F)) then ActiveControl=edART;
end;

procedure TfmPriceDict.SpeedItem14Click(Sender: TObject);
begin
  inherited;
 if cbKind.ItemIndex =0
  then  dmInv.sqlCheckArt.ParamByName('All').AsInteger:=1
  else  dmInv.sqlCheckArt.ParamByName('All').AsInteger:=0;
  dmInv.sqlCheckArt.ParamByName('Check').AsInteger:=0;
  dmInv.sqlCheckArt.ParamByName('Artid').asinteger:=dminv.taARTPriceARTID.AsInteger;
  dmInv.sqlCheckArt.ExecQuery;
  dmInv.sqlCheckArt.Transaction.CommitRetaining;
  ReOpenDataSet(dminv.taARTPriceDet);
end;

end.
