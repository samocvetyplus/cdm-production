unit frmLink;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, FIBDataSet,
  pFIBDataSet, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, cxGridBandedTableView,
  cxGridDBBandedTableView, Provider, DBClient, ActnList,
  dxBar, ImgList, DBActns, dxPScxGrid6Lnk, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPSCore, dxPScxCommon, FR_DSet, FR_DBSet, FR_Class,
  FR_Desgn;

type

  TDialogLink = class(TForm)
    ViewParent: TcxGridDBTableView;
    GridLevel1: TcxGridLevel;
    Grid: TcxGrid;
    DBDataSetParent: TpFIBDataSet;
    DBDataSetChild: TpFIBDataSet;
    GridLevel2: TcxGridLevel;
    ViewParentARTICLE: TcxGridDBColumn;
    ViewChild: TcxGridDBBandedTableView;
    ViewChildARTICLE: TcxGridDBBandedColumn;
    ViewChildSIZES: TcxGridDBBandedColumn;
    ViewChildP1: TcxGridDBBandedColumn;
    ViewChildP2: TcxGridDBBandedColumn;
    ViewChildP3: TcxGridDBBandedColumn;
    ViewChildP4: TcxGridDBBandedColumn;
    ViewChildP5: TcxGridDBBandedColumn;
    ViewChildP6: TcxGridDBBandedColumn;
    ViewChildP7: TcxGridDBBandedColumn;
    ViewChildP8: TcxGridDBBandedColumn;
    ViewChildP9: TcxGridDBBandedColumn;
    ViewChildP10: TcxGridDBBandedColumn;
    DBArticles: TpFIBDataSet;
    DataSetArticles: TClientDataSet;
    ProviderArticles: TDataSetProvider;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    Images: TcxImageList;
    Actions: TActionList;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionInsert: TAction;
    ActionDelete: TAction;
    dxBarLargeButton2: TdxBarLargeButton;
    DataSetParent: TClientDataSet;
    DataSetChild: TClientDataSet;
    DataSetArticlesARTICLEID: TIntegerField;
    DataSetArticlesARTICLE: TStringField;
    ProviderParent: TDataSetProvider;
    ProviderChild: TDataSetProvider;
    DataSetChildPARENTARTICLEID: TIntegerField;
    DataSetChildARTICLEID: TIntegerField;
    DataSetChildARTICLE: TStringField;
    DataSetChildSIZES: TStringField;
    DataSetChildP1: TFloatField;
    DataSetChildP2: TStringField;
    DataSetChildP3: TStringField;
    DataSetChildP4: TStringField;
    DataSetChildP5: TStringField;
    DataSetChildP6: TIntegerField;
    DataSetChildP7: TFloatField;
    DataSetChildP8: TFloatField;
    DataSetChildP9: TFloatField;
    DataSetChildP10: TFloatField;
    DataSourceParent: TDataSource;
    DataSourceChild: TDataSource;
    DataSetParentARTICLEID: TIntegerField;
    DataSetParentSORTING: TStringField;
    DataSetParentARTICLE: TStringField;
    DataSetChildLINKID: TIntegerField;
    ActionPost: TDataSetPost;
    ActionCancel: TDataSetCancel;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    DBDataSetChildLINKID: TFIBIntegerField;
    DBDataSetChildPARENTARTICLEID: TFIBIntegerField;
    DBDataSetChildARTICLEID: TFIBIntegerField;
    DBDataSetChildARTICLE: TFIBStringField;
    DBDataSetChildSIZES: TFIBStringField;
    DBDataSetChildP1: TFIBFloatField;
    DBDataSetChildP2: TFIBStringField;
    DBDataSetChildP3: TFIBStringField;
    DBDataSetChildP4: TFIBStringField;
    DBDataSetChildP5: TFIBStringField;
    DBDataSetChildP6: TFIBIntegerField;
    DBDataSetChildP7: TFIBFloatField;
    DBDataSetChildP8: TFIBFloatField;
    DBDataSetChildP9: TFIBFloatField;
    DBDataSetChildP10: TFIBFloatField;
    DBDataSetParentARTICLEID: TFIBIntegerField;
    DBDataSetParentSORTING: TFIBStringField;
    ButtonPrint: TdxBarLargeButton;
    frReport: TfrReport;
    frDataSetParent: TfrDBDataSet;
    frDataSetChild: TfrDBDataSet;
    procedure ActionInsertExecute(Sender: TObject);
    procedure ActionDeleteExecute(Sender: TObject);
    procedure ActionDeleteUpdate(Sender: TObject);
    procedure DataSetParentBeforePost(DataSet: TDataSet);
    procedure DataSetParentApplyChanges(DataSet: TDataSet);
    procedure DataSetChildAfterPost(DataSet: TDataSet);
    procedure DBDataSetChildAfterPost(DataSet: TDataSet);
    procedure DBDataSetParentAfterDelete(DataSet: TDataSet);
    procedure DBDataSetParentAfterPost(DataSet: TDataSet);
    procedure ButtonPrintClick(Sender: TObject);
  private
    procedure InternalExecute;
  public
    class procedure Execute;
  end;

var
  DialogLink: TDialogLink;

implementation

uses Data, DictData, uUtils, dbUtil;

{$R *.dfm}

{ TDialogLink }

procedure TDialogLink.ActionDeleteExecute(Sender: TObject);
begin
  DataSetParent.Delete;
end;

procedure TDialogLink.ActionDeleteUpdate(Sender: TObject);
begin
  ActionDelete.Enabled := not DataSetParent.IsEmpty;
  ViewParent.OptionsData.Editing := DataSetParent.State = dsInsert;
end;

procedure TDialogLink.ActionInsertExecute(Sender: TObject);
begin
  DataSetParent.Append;
end;

procedure TDialogLink.DataSetChildAfterPost(DataSet: TDataSet);
begin
  DataSetChild.ApplyUpdates(0);
end;

procedure TDialogLink.DataSetParentApplyChanges(DataSet: TDataSet);
begin
  DataSetParent.ApplyUpdates(-1);
end;

procedure TDialogLink.DataSetParentBeforePost(DataSet: TDataSet);
var
  Article: string;
begin
  Article := DataSetParentARTICLE.AsString;
  if Article = '' then
  begin
    ErrorOkMessage('�� ������ �������.');
    Abort;
  end;
  DataSetParentSORTING.AsString := Article;
end;

procedure TDialogLink.DBDataSetChildAfterPost(DataSet: TDataSet);
begin
  DBDataSetChild.Transaction.CommitRetaining;
end;

procedure TDialogLink.DBDataSetParentAfterDelete(DataSet: TDataSet);
begin
  DBDataSetParent.Transaction.CommitRetaining;
end;

procedure TDialogLink.DBDataSetParentAfterPost(DataSet: TDataSet);
begin
  DBDataSetParent.Transaction.CommitRetaining;
end;

procedure TDialogLink.ButtonPrintClick(Sender: TObject);
begin
  frReport.ShowReport;
end;

class procedure TDialogLink.Execute;
begin
  ExecSQL('execute procedure link$init', dm.quTmp, True);
  DialogLink := TDialogLink.Create(nil);
  DialogLink.InternalExecute;
  DialogLink.Free;
end;

procedure TDialogLink.InternalExecute;
begin
  DataSetArticles.Active := True;
  DataSetParent.Active := True;
  DataSetChild.Active := True;
  ShowModal;
end;

end.
