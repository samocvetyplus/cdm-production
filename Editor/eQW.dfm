object fmeQW: TfmeQW
  Left = 262
  Top = 253
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077' '
  ClientHeight = 107
  ClientWidth = 222
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 220
    Height = 81
    Shape = bsFrame
  end
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 77
    Height = 13
    Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090':'
    Transparent = True
  end
  object DBText1: TDBText
    Left = 96
    Top = 8
    Width = 42
    Height = 13
    AutoSize = True
    DataField = 'SEMISNAME'
    DataSource = dmInv.dsrWH_items
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Transparent = True
  end
  object Label2: TLabel
    Left = 9
    Top = 32
    Width = 59
    Height = 13
    Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
    Transparent = True
  end
  object Label3: TLabel
    Left = 8
    Top = 56
    Width = 19
    Height = 13
    Caption = #1042#1077#1089
  end
  object BitBtn1: TBitBtn
    Left = 40
    Top = 82
    Width = 75
    Height = 25
    Caption = #1054#1082
    ModalResult = 1
    TabOrder = 0
    OnClick = BitBtn1Click
  end
  object edQ: TRxSpinEdit
    Left = 94
    Top = 27
    Width = 121
    Height = 21
    Alignment = taRightJustify
    TabOrder = 1
  end
  object edW: TRxCalcEdit
    Left = 94
    Top = 49
    Width = 121
    Height = 21
    AutoSize = False
    NumGlyphs = 2
    TabOrder = 2
  end
  object BitBtn2: TBitBtn
    Left = 115
    Top = 82
    Width = 75
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 3
    OnClick = BitBtn1Click
  end
end
