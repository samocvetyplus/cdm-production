unit dDepT1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList,  ImgList, Buttons,
  Grids, DBGrids, RXDBCtrl, ExtCtrls, ComCtrls, DBGridEh, db,
  DBGridEhGrouping, rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmDepT1 = class(TfmDictAncestor)
    Splitter1: TSplitter;
    gridPS: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure gridDictGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure gridPSGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  private
    FSaveWorkMode : string;
    FSaveActiveDepart, FSaveActiveDataSet : boolean;
    procedure FilterDepart(DataSet : TDataSet; var Accept : boolean);
  end;

var
  fmDepT1: TfmDepT1;
  RejPsId : Variant;

implementation

uses DictData, dbUtil;

{$R *.dfm}

procedure TfmDepT1.FormCreate(Sender: TObject);
begin
  FSaveActiveDepart := dm.taDepart.Active;
  FSaveActiveDataSet := DataSet.Active;
  CloseDataSet(dm.taDepart);
  dm.taDepart.Filtered := True;
  dm.taDepart.OnFilterRecord := FilterDepart;
  OpenDataSet(dm.taDepart);
  dm.taDepart.Insert;
  dm.taDepartDEPID.AsInteger := -1;
  dm.taDepartNAME.AsString := '�� �����';
  dm.taDepartSORTIND.AsInteger := -1;
  dm.taDepartDEPTYPES.AsInteger := 1;
  dm.taDepart.Post;
  FSaveWorkMode := dm.WorkMode;
  dm.WorkMode := 'DepT1';
  inherited;
  DataSet.Locate('ID', 10, []);
end;

procedure TfmDepT1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if(dm.taDepartDEPID.AsInteger = -1)then RejPsId := Null
  else RejPsId := dm.taDepartDEPID.AsInteger;
  CloseDataSet(dm.taDepart);
  dm.taDepart.Filtered := False;
  dm.taDepart.OnFilterRecord := nil;
  dm.WorkMode := FSaveWorkMode;
  dm.taDepart.Active := FSaveActiveDepart;
  DataSet.Active := FSaveActiveDataSet;
end;



procedure TfmDepT1.FilterDepart(DataSet: TDataSet; var Accept: boolean);
begin
  Accept := (dm.taDepartDEPTYPES.AsInteger and $1 = $1) and
            (dm.taDepartDEPTYPES.AsInteger and $10 <> $10);
  if Accept then Accept := dm.taDepartNAME.AsString <> '�� �����';          
end;

procedure TfmDepT1.gridDictGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(dm.taRejPSIDMUSTHAVEVALUE.AsInteger = 1)then AFont.Color := clRed
  else AFont.Color := clWindowText;
end;

procedure TfmDepT1.gridPSGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(dm.taDepartNAME.AsString='�� �����')then AFont.Color := clRed
  else AFont.Color := clWindowText;
end;

end.
