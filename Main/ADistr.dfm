inherited fmADistr: TfmADistr
  Left = 97
  Top = 131
  Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
  ClientHeight = 595
  ClientWidth = 1001
  OldCreateOrder = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 1009
  ExplicitHeight = 629
  PixelsPerInch = 96
  TextHeight = 13
  object spitAdd: TSpeedButton [0]
    Left = 257
    Top = 0
    Width = 141
    Height = 34
    Action = acAdd
    Flat = True
    Glyph.Data = {
      76010000424D7601000000000000760000002800000020000000100000000100
      0400000000000001000000000000000000001000000000000000000000000000
      8000008000000080800080000000800080008080000080808000C0C0C0000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333FF3333333333333003333
      3333333333773FF3333333333309003333333333337F773FF333333333099900
      33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
      99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
      33333333337F3F77333333333309003333333333337F77333333333333003333
      3333333333773333333333333333333333333333333333333333333333333333
      3333333333333333333333333333333333333333333333333333}
    Layout = blGlyphTop
    NumGlyphs = 2
  end
  inherited stbrStatus: TStatusBar
    Top = 576
    Width = 1001
    SimplePanel = True
    ExplicitTop = 576
    ExplicitWidth = 1001
  end
  inherited tb1: TSpeedBar
    Width = 1001
    ExplicitWidth = 1001
    inherited spitExit: TSpeedItem
      ImageIndex = 5
      Left = 635
    end
    object spitMode: TSpeedItem
      Action = acWhKind
      BtnCaption = #1056#1077#1078#1080#1084
      Caption = #1056#1077#1078#1080#1084
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acWhKindExecute
      SectionName = 'Untitled (0)'
    end
    object spitAppl: TSpeedItem
      Action = acNewAppl
      BtnCaption = #1047#1072#1103#1074#1082#1072
      Caption = #1047#1072#1103#1074#1082#1072
      Spacing = 1
      Left = 131
      Top = 3
      OnClick = acNewApplExecute
      SectionName = 'Untitled (0)'
    end
  end
  object plWH: TPanel [3]
    Left = 0
    Top = 42
    Width = 369
    Height = 534
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 2
    object TBDock: TTBDock
      Left = 0
      Top = 37
      Width = 369
      Height = 39
      object tlbrWh: TTBToolbar
        Left = 0
        Top = 0
        BorderStyle = bsNone
        Caption = #1055#1072#1085#1077#1083#1100' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
        DockMode = dmCannotFloat
        Images = ilButtons
        Options = [tboImageAboveCaption, tboShowHint]
        ParentShowHint = False
        ProcessShortCuts = True
        ShowHint = True
        TabOrder = 0
        object TBItem9: TTBItem
          Action = acPAdd
        end
        object TBItem8: TTBItem
          Action = acRej
        end
        object titChange: TTBSubmenuItem
          Caption = #1048#1079#1084#1077#1085#1080#1090#1100
          DropdownCombo = True
          ImageIndex = 12
          object TBItem7: TTBItem
            Action = acChangeSz
          end
          object TBItem4: TTBItem
            Action = acChangeArt
          end
          object TBItem3: TTBItem
            Action = acChangeArt2
          end
          object TBItem29: TTBItem
            Action = acChangeByAppl
            Visible = False
          end
        end
        object TBItemDemand: TTBItem
          Action = ActionDemand
          Caption = #1047#1072#1082#1072#1079
          Images = dm.ilButtons
        end
        object TBItem27: TTBItem
          Action = acWhArt2Ins
          Caption = #1042#1089#1090#1072#1074#1082#1080
        end
        object TBSubmenuItem2: TTBSubmenuItem
          Caption = #1055#1077#1095#1072#1090#1100
          DropdownCombo = True
          ImageIndex = 2
          object TBItem10: TTBItem
            Action = acPrintWh
          end
        end
      end
    end
    object gridWHAppl: TDBGridEh
      Left = 0
      Top = 76
      Width = 369
      Height = 458
      Align = alClient
      AllowedOperations = []
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dmAppl.dsrWHAppl
      DrawMemoText = True
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterRowCount = 1
      FrozenCols = 1
      Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghFooter3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      ParentShowHint = False
      PopupMenu = ppWh
      RowDetailPanel.Color = clBtnFace
      ShowHint = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      OnGetCellParams = gridWHApplGetCellParams
      OnKeyDown = gridWHApplKeyDown
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Width = 24
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERNAME'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clNavy
          Footer.Font.Height = -11
          Footer.Font.Name = 'MS Sans Serif'
          Footer.Font.Style = []
          Footer.Value = #1048#1090#1086#1075#1086' '#1096#1090'.'
          Footer.ValueType = fvtStaticText
          Footers = <>
          Title.EndEllipsis = True
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clBlack
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 70
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART'
          Footers = <>
          Title.EndEllipsis = True
          Width = 52
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART2'
          Footers = <>
          Title.EndEllipsis = True
          Width = 38
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SZ'
          Footers = <>
          Title.EndEllipsis = True
          Width = 34
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'Q'
          Footer.FieldName = 'Q1'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clNavy
          Footer.Font.Height = -11
          Footer.Font.Name = 'MS Sans Serif'
          Footer.Font.Style = []
          Footers = <>
          Title.EndEllipsis = True
          Title.Font.Charset = DEFAULT_CHARSET
          Title.Font.Color = clBlack
          Title.Font.Height = -11
          Title.Font.Name = 'MS Sans Serif'
          Title.Font.Style = []
          Width = 32
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'U'
          Footers = <>
          Title.Caption = #1045#1076'. '#1080#1079#1084'.'
          Title.EndEllipsis = True
          Width = 32
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'PATTERN'
          Footers = <>
          Title.EndEllipsis = True
          Title.ToolTips = True
          Width = 65
        end
        item
          EditButtons = <>
          FieldName = 'ARTID'
          Footers = <>
        end
        item
          EditButtons = <>
          FieldName = 'ART2ID'
          Footers = <>
        end
        item
          EditButtons = <>
          FieldName = 'OPERID'
          Footers = <>
        end
        item
          EditButtons = <>
          FieldName = 'SZID'
          Footers = <>
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object plDepFrom: TPanel
      Left = 0
      Top = 0
      Width = 369
      Height = 16
      Align = alTop
      BevelOuter = bvNone
      Caption = #1057#1086#1089#1090#1086#1103#1085#1080#1077' '#1082#1083#1072#1076#1086#1074#1086#1081
      Color = clMoneyGreen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object Panel5: TPanel
      Left = 0
      Top = 16
      Width = 369
      Height = 21
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object lbOperFrom: TLabel
        Left = 8
        Top = 3
        Width = 111
        Height = 13
        Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1086#1087#1077#1088#1072#1094#1080#1080': '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object cmbxOper: TDBComboBoxEh
        Left = 120
        Top = 1
        Width = 137
        Height = 19
        TabStop = False
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        TabOrder = 0
        Visible = True
        OnChange = cmbxOperChange
      end
    end
  end
  object plInv: TPanel [4]
    Left = 453
    Top = 42
    Width = 548
    Height = 534
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object spltStone: TSplitter
      Left = 0
      Top = 174
      Width = 548
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      Visible = False
    end
    object plRej: TPanel
      Left = 0
      Top = 375
      Width = 548
      Height = 159
      Align = alBottom
      BevelInner = bvLowered
      TabOrder = 0
      object gridRejEl: TDBGridEh
        Left = 2
        Top = 34
        Width = 544
        Height = 123
        Align = alClient
        AllowedOperations = []
        Color = clBtnHighlight
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dmAppl.dsrRejEl
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFooter3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
        ParentShowHint = False
        PopupMenu = ppRejInv
        RowDetailPanel.Color = clBtnFace
        ShowHint = True
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnExit = GridPost
        OnGetCellParams = gridRejElGetCellParams
        Columns = <
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'RecNo'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtCount
            Footers = <>
            Width = 24
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.Value = #1048#1090#1086#1075#1086' '#1096#1090'.'
            Footer.ValueType = fvtStaticText
            Footers = <>
            Title.EndEllipsis = True
            Width = 49
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART2'
            Footers = <>
            Title.EndEllipsis = True
            Width = 36
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'SZ'
            Footers = <>
            Title.EndEllipsis = True
            Width = 44
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q1'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.EndEllipsis = True
            Width = 39
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'OPERATION'
            Footers = <>
            Title.EndEllipsis = True
            Width = 59
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'U'
            Footers = <>
            Title.EndEllipsis = True
            Width = 46
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'COMMENTID'
            Footers = <>
            Title.EndEllipsis = True
            ToolTips = True
            Width = 73
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'COMMENT'
            Footers = <>
            Title.EndEllipsis = True
            Width = 72
          end
          item
            EditButtons = <>
            FieldName = 'ARTID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'ART2ID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'SZID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'OPERID'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object plRejName: TPanel
        Left = 2
        Top = 2
        Width = 544
        Height = 16
        Align = alTop
        BevelOuter = bvNone
        Caption = #1057#1085#1103#1090#1099#1077' '#1089' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072' '#1072#1088#1090#1080#1082#1091#1083#1099
        Color = clMoneyGreen
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object Panel4: TPanel
        Left = 2
        Top = 18
        Width = 544
        Height = 16
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Label2: TLabel
          Left = 4
          Top = 1
          Width = 70
          Height = 13
          Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object txtRejDocNo: TDBText
          Left = 76
          Top = 1
          Width = 25
          Height = 13
          DataField = 'DOCNO'
          DataSource = dmAppl.dsrRejInv
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label3: TLabel
          Left = 103
          Top = 1
          Width = 11
          Height = 13
          Caption = #1086#1090
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object txtRejDocDate: TDBText
          Left = 115
          Top = 1
          Width = 134
          Height = 14
          DataField = 'DOCDATE'
          DataSource = dmAppl.dsrRejInv
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object plPInv: TPanel
      Left = 0
      Top = 245
      Width = 548
      Height = 130
      Align = alBottom
      BevelInner = bvLowered
      TabOrder = 1
      object Panel2: TPanel
        Left = 2
        Top = 2
        Width = 544
        Height = 14
        Align = alTop
        BevelOuter = bvNone
        Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1085#1099#1077' '#1074' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086' '#1072#1088#1090#1080#1082#1091#1083#1099
        Color = clMoneyGreen
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object gridPEl: TDBGridEh
        Left = 2
        Top = 32
        Width = 544
        Height = 96
        Align = alClient
        AllowedOperations = []
        Color = clBtnHighlight
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dmAppl.dsrPEl
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 1
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFooter3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
        ParentShowHint = False
        PopupMenu = ppPInv
        RowDetailPanel.Color = clBtnFace
        ShowHint = True
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnExit = GridPost
        OnGetCellParams = gridPElGetCellParams
        Columns = <
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'RecNo'
            Footer.ValueType = fvtCount
            Footers = <>
            Width = 24
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.Value = #1048#1090#1086#1075#1086' '#1096#1090'.'
            Footer.ValueType = fvtStaticText
            Footers = <>
            Title.EndEllipsis = True
            Width = 49
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART2'
            Footers = <>
            Title.EndEllipsis = True
            Width = 41
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'SZ'
            Footers = <>
            Title.EndEllipsis = True
            Width = 48
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q1'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clNavy
            Footer.Font.Height = -11
            Footer.Font.Name = 'MS Sans Serif'
            Footer.Font.Style = []
            Footer.ValueType = fvtSum
            Footers = <>
            Title.EndEllipsis = True
            Width = 43
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'OPERATION'
            Footers = <>
            Title.EndEllipsis = True
            Width = 57
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'U'
            Footers = <>
            Title.EndEllipsis = True
            Width = 43
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'COMMENTID'
            Footers = <>
            Title.EndEllipsis = True
            ToolTips = True
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'COMMENT'
            Footers = <>
            Title.EndEllipsis = True
            Width = 82
          end
          item
            EditButtons = <>
            FieldName = 'ARTID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'ART2ID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'OPERID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'SZID'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel3: TPanel
        Left = 2
        Top = 16
        Width = 544
        Height = 16
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object Label5: TLabel
          Left = 4
          Top = 1
          Width = 70
          Height = 13
          Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#8470
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object txtPDocNo: TDBText
          Left = 73
          Top = 1
          Width = 37
          Height = 13
          DataField = 'DOCNO'
          DataSource = dmAppl.dsrPInv
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object Label6: TLabel
          Left = 111
          Top = 1
          Width = 11
          Height = 13
          Caption = #1086#1090
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object txtPDocDate: TDBText
          Left = 123
          Top = 1
          Width = 102
          Height = 14
          DataField = 'DOCDATE'
          DataSource = dmAppl.dsrPInv
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object gridAEl: TDBGridEh
      Left = 0
      Top = 72
      Width = 548
      Height = 102
      Align = alClient
      AllowedOperations = [alopUpdateEh]
      Color = clBtnHighlight
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dmAppl.dsrAEl
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterRowCount = 1
      FrozenCols = 1
      Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghFooter3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      ParentShowHint = False
      PopupMenu = ppInv
      RowDetailPanel.Color = clBtnFace
      ShowHint = True
      SumList.Active = True
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      OnExit = GridPost
      OnGetCellParams = gridAElGetCellParams
      OnKeyDown = gridAElKeyDown
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footer.ValueType = fvtCount
          Footers = <>
          Width = 24
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clNavy
          Footer.Font.Height = -11
          Footer.Font.Name = 'MS Sans Serif'
          Footer.Font.Style = []
          Footer.Value = #1048#1090#1086#1075#1086' '#1096#1090'.'
          Footer.ValueType = fvtStaticText
          Footers = <>
          Title.EndEllipsis = True
          Width = 50
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART2'
          Footers = <>
          Title.EndEllipsis = True
          Width = 34
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SZ'
          Footers = <>
          Title.EndEllipsis = True
          Width = 43
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'Q'
          Footer.FieldName = 'Q1'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clNavy
          Footer.Font.Height = -11
          Footer.Font.Name = 'MS Sans Serif'
          Footer.Font.Style = []
          Footer.ValueType = fvtSum
          Footers = <>
          Title.EndEllipsis = True
          Width = 37
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERATION'
          Footers = <>
          Title.EndEllipsis = True
          Width = 58
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERATIONFROM'
          Footers = <>
          Title.EndEllipsis = True
          Width = 66
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART2_OLD'
          Footers = <>
          Title.EndEllipsis = True
          Width = 55
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'PATTERN'
          Footers = <>
          Title.EndEllipsis = True
          ToolTips = True
          Width = 65
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'U'
          Footers = <>
          Title.EndEllipsis = True
          Width = 37
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'U_OLD'
          Footers = <>
          Title.EndEllipsis = True
          Width = 37
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
    object plDepTo: TPanel
      Left = 0
      Top = 0
      Width = 548
      Height = 16
      Align = alTop
      BevelOuter = bvNone
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Color = clMoneyGreen
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
    end
    object plHeaderInv: TPanel
      Left = 0
      Top = 16
      Width = 548
      Height = 17
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 4
      object lbAInv: TLabel
        Left = 6
        Top = 2
        Width = 60
        Height = 13
        Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object txtDocNo: TDBText
        Left = 68
        Top = 2
        Width = 33
        Height = 12
        DataField = 'DOCNO'
        DataSource = dmAppl.dsrAInv
        Transparent = True
      end
      object lbOt: TLabel
        Left = 106
        Top = 2
        Width = 12
        Height = 13
        Caption = #1086#1090
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object txtDocDate: TDBText
        Left = 120
        Top = 2
        Width = 65
        Height = 13
        DataField = 'DOCDATE'
        DataSource = dmAppl.dsrAInv
        Transparent = True
      end
      object lbOperTo: TLabel
        Left = 188
        Top = 2
        Width = 54
        Height = 13
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object tbdkInv: TTBDock
      Left = 0
      Top = 33
      Width = 548
      Height = 39
      object tlbrInv: TTBToolbar
        Left = 0
        Top = 0
        BorderStyle = bsNone
        DockMode = dmCannotFloat
        DockPos = 0
        Images = ilButtons
        Options = [tboImageAboveCaption]
        ParentShowHint = False
        ProcessShortCuts = True
        ShowHint = True
        TabOrder = 0
        object TBItem25: TTBItem
          Action = acInvArt2Ins
        end
        object TBSubmenuItem3: TTBSubmenuItem
          Action = acStone
          DropdownCombo = True
          object TBItem26: TTBItem
            Action = acShowStone
          end
        end
        object TBSubmenuItem4: TTBSubmenuItem
          Caption = #1055#1077#1095#1072#1090#1100
          DropdownCombo = True
          ImageIndex = 2
          object TBItem28: TTBItem
            Action = acPrintInv
          end
        end
      end
    end
    object gridStone: TDBGridEh
      Left = 0
      Top = 177
      Width = 548
      Height = 68
      Align = alBottom
      AllowedOperations = []
      Color = clBtnHighlight
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dmAppl.dsrAIns
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterRowCount = 1
      FrozenCols = 1
      Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghFooter3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      RowDetailPanel.Color = clBtnFace
      TabOrder = 6
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      Visible = False
      OnGetCellParams = gridStoneGetCellParams
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART'
          Footers = <>
          Title.EndEllipsis = True
          Width = 87
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SEMISNAME'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Title.EndEllipsis = True
          Width = 88
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'Q'
          Footer.FieldName = 'Q'
          Footer.ValueType = fvtSum
          Footers = <>
          Title.EndEllipsis = True
          Width = 37
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'W'
          Footer.FieldName = 'W'
          Footer.ValueType = fvtSum
          Footers = <>
          Title.EndEllipsis = True
          Width = 35
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ITTYPE'
          Footers = <>
          Title.EndEllipsis = True
          Width = 58
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'CLEANNES'
          Footers = <>
          Title.EndEllipsis = True
          Width = 42
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'COLOR'
          Footers = <>
          Title.EndEllipsis = True
          Width = 32
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'CHROMATICITY'
          Footers = <>
          Title.EndEllipsis = True
          Width = 28
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'EDGSHAPEID'
          Footers = <>
          Title.EndEllipsis = True
          Width = 33
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'EDGTYPEID'
          Footers = <>
          Title.EndEllipsis = True
          Width = 37
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'IGR'
          Footers = <>
          Title.EndEllipsis = True
          Width = 55
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object plBtn: TPanel [5]
    Left = 373
    Top = 42
    Width = 80
    Height = 534
    Align = alLeft
    TabOrder = 4
    object spbrBtn: TSpeedBar
      Left = 1
      Top = 1
      Width = 78
      Height = 532
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      BoundLines = [blTop, blBottom]
      Position = bpCustom
      Align = alClient
      Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
      BtnOffsetHorz = 3
      BtnOffsetVert = 3
      BtnWidth = 75
      BtnHeight = 35
      Wallpaper.Data = {
        07544269746D6170760F0000424D760F00000000000076000000280000008000
        00003C0000000100040000000000000F00000000000000000000100000000000
        0000000000000000800000800000008080008000000080008000808000008080
        8000C0C0C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
        FF00888888888888888888888888787878888888888888788887888888888888
        888088F8FF8FFFF888F88F88F8F8F8F8F8F8F8F8F88888888888788878888888
        8888888888888888888888888887878878888888888888888878888888888888
        888888888FFFFF8F8F8F88F8F8F8F8F8F8F8F8F8888888888888887878888888
        8888888888888888888888888888887878888888888888888788888888888888
        88888888888FF8FF8F88888888F8F8FF8F8F88F88F8888888888888878888888
        8888888888888888888888888888788788888888888888888788888888888888
        8888888888888FFFFF8F8888F8F8F8F8F8F88F88888888888888888787888888
        8888888888888888888888888888887888888888888888888888888888888888
        888888888888888FF8F8F8F888F8F8F8F8F8F8F8F88888888888888878888888
        8888888888888888888888888888888888888888888888888888888888888888
        8888888888888888FFFF8F8F8F8F8F8F8F8F888888888F888888888888888888
        8888888888888888888888888888888888888888888888888888888888888888
        88888888888888888FF8F8F8F8F8F8FFFFF8F8F8F8F888888888888887887878
        8888888888888888888888888888888888888888888888888888888888888888
        888888888888888788FFFFF8F8888F88F8F8F88F88888F888888888888878788
        8888888888888888888888888888878888888888888888888888888888888888
        8888888888888888888FFFFFF8F8F88F8F8FF8F8F88F88888888888888888878
        8888888888888888888888888888888888788888888888888888888888888888
        88888888888888888788FFFF8F8F88F8F8FF8FF8F8F8F88F8888888888878787
        8888888888888888888888888888887888788888888888888888888888888888
        888888888888888888888FFFFF8F8888F8F8FF8FF8F88F888888888888888888
        8888887888888888888888888888888788888888888888888888888888888888
        8888888888888888888888FFFF8F88F8F8F8F8F8F8F8F88F8F88888888888787
        8887888788788888888888888888888888888888888888888888888888888888
        8888888888888888888888FFFFFF8F88F8F8F8FF8FF8F8F8888F888888888888
        7878878888888888888888888888888878888888888888888888888888888888
        88888888888888888888888FFFF8F8F888F8F8F8F8F8F888F8888F8888888887
        8887888888888888888888888888888887888888888888888888888888888888
        88888888888888888888888F8FFF8F88F8F88F8FFF8F8F8F888F888888888878
        7878878888888F88888888888888888888888788888888888888888888888888
        88888888888888888888888FFFF8F8888888F8F8F8F8F8F88F88F88888888888
        87887888888F8888F8F888888888888888788888888878888888888888888888
        888888888888888888888788FFFFF8F888F88F8F8FF8F8F8F88F88F888888888
        787878788888F888888F8F888888888888878878888787888888888888888888
        888888888888888888888888FFFF8F888888F8F8FF8F8F8F8F8F8F8888888888
        888787887888888888888F8F8888888888887888788888888888888888888888
        8888888888888888888888788FFFF8F8F88F8F8FF8F8FF8F8F8F8F8F88888888
        8878788788888888788888888888888888888787887888878888888888888888
        8888888888888888888888888FFFF8F88888888F8F8FF8F8F8F8F888F8888888
        8887887878888887888888888888788888888878888788888888888888888888
        888888888888888888888887888F8F8F88888F8F8F8F8F8F8F8F8F8F88888888
        8888887878878878888888888888888888888788888888888888888888888888
        8888888888888888888888888888F8F8F8F88F8F8F8F8F8F8F8F8888888F8888
        8888878878787878888888888878788888887888888888888888888888888888
        888888888888888888888888888888F8F88888888F8F8F8F8F8F8F8F8F888888
        8888888888878888888888888888878888888788888888888888888888888888
        8888888888888888888888888888888F8F88888F8F8F8F8F8F8F8F888888F88F
        8888887888878888888888888888888888888778888888888888888888888888
        88888888888888888788888888888888F8F8888888F88F8F8FF8F8F8F8F88888
        8888888888878888888888888888888888888888888888888888888888888888
        88888888888888888888888888888888888888888F88F8F8F8F8F8F88888F888
        8888888887888888888888888888888888888787888888888888888888888888
        8888888888888888888888788888887888888888888F8F8F8F8F8F8F8F8F888F
        8888888888878888888888888888888888888878788888888888888888888888
        888888888888888887888888888888888888888888F8F8F8F8F8F8F8F8F88F88
        8888888888788888888888888888888888888878888888888888888888888888
        888888888888887888888888888888878888888888888F88F8F8F8F8F8F8F888
        888888888888888888888888888888888888888888888888888F888888888888
        88888888888887878787888888888888787888888888F88F8F8F8F8F8F8F8F88
        8888888888888888887888888888888888888888888888888788F88888888888
        8888888888888878787888788888888878887888888888F8F8F8F8FF8F8F88F8
        88888888888887878888888888888888888888888888888888888F8888888888
        888888888888888888878788888888888787878788888F88F8F8F8F8F8F8F88F
        88888888888887878788888788888888888888888888888888888FF8F8888788
        88888888888888887878888888888888887888888888888F8F8F8F8F8F8F88F8
        F888888888888888888888887888888888888888888888888888888888888888
        8888888888888888878888888888888888888888878888F88F8F8F8F8F8F8F8F
        88F8888888888887878888888888888888888888888888888888788888888888
        888888888888888888788888888888888888888888888888F8F88F8F8F8F8F8F
        F888F88888888878888888F88888888888888888888888888888888887878887
        888888888888888888788888888888888888888887888888888F88F8F8F8F8F8
        88F8888888888888787888F8F888888888888878888888888888888888888888
        8888888888888888888888888888888888888888788788888F88F8F8F8F8F8F8
        F8F8F8F8888888888888888F8888888888888888888888888888888888888888
        888888888888888888888888888888888888888887888888888F88F8F8F8F8F8
        F8F88888F88888888878888F8888888888888887878888888888888888888888
        88888888888888888888888888888888888888888878888888888F8F8F8F8F8F
        8888F8F888888888888888888F88888888888888887888888888888888888888
        8888888888888888888888888888888888888888888888888888F8F8F8F8F8F8
        F8F8F88888888888888888888888888888888888788888888888888888888888
        888888888888888888888888888888888888888888878887888888F8F88F8F8F
        8F8F88F888888888888888888888888888888888887878888888888788888888
        88888888888888888888888888888888888888888888788888888F8F88F8F8F8
        F8F88F888F888888888888888888878888888888888888888888887878888888
        888888888888888888888888888888888788888888878787888888888F8F8FF8
        F8F8F88F88888888888888888887888888888888887878888888888888888888
        8888888888888888888888888888888888788888888877878888888F8F8F8F8F
        8F88F8F8F8F88888888888888878787878888888888888888888888887888888
        888F888888888888888888888888888888888888888778778888888888F88F8F
        8F8F88F88888F888888888887878888888888888888888888888888888888888
        88F8F8888888888888888888888888888887888888887778788888888F88F8F8
        F888F8F8F8F88888888888888878788888888888888888888888888888888888
        8888888888888888888888888888888888888888888887888888888888F88F8F
        8F8F8F8F888F8888888888887878888888888888888888888888888888888888
        888888888888888888888888888888888888888888888888888788888888F88F
        8F88F8F8F8F88F88888888888878788888888888888888888888888888888888
        88888888888888888888888888888888888888888888888888788888888F8F8F
        8F8F8F8F888F888888888888888888888F8F8888887888888888888888888888
        88888888878888888888888888888888888888888888888888878888888888F8
        F8F88F8F8F8F88F888888888888888888F8F8F88888888888888888888888888
        888888788888888888888888788888888888888888887888888878888888F8F8
        F88F8F8F8F88F888888888888888888888FFF88F888887888888888888888888
        8888888888788888888888888888888888888888888878888888878888888888
        88F8F8F8F88F88F888F8888888888888888888F8888888888888888888888888
        888888888888888888888888888888888888888888888888888888787888888F
        8F88F8F8F8F88F88F88888888888888888888888F8F888888888888888888888
        8888888888888888888888888878888888888888888888888888878888888888
        888F8F8F8F88F88F88F88888888F88888878888888F888888888888888888888
        8888888888888888888888888887888888888888888888888888888887888888
        88F8F8F88F8F88F88F88F88888888F8888887888888888888888888888888888
        8888888888888888888888888888788888888888888888888888888888888888
        8F88F8F8F888F888F88F88888888888888788788888888788888888888888888
        8888888888888888888888888888888887888888888888888888888888788888
        888F8F8F8F8F88F8F8F888888888888888887878788888888888888888888888
        8888888888888888888888888888878888888888888887888888888888878888
        88888888F888F888888888888888888888878787878888788888888888888888
        8888888888888888888888888888888888888888888888888888888888888888
        88888F8F8F8F8888F88F88F88888888888888788787878888888888888888888
        8888888888888888888888888888888888888888888888888888888888878888
        8888}
      BevelOuter = bvNone
      TabOrder = 0
      InternalVer = 1
      object SpeedbarSection2: TSpeedbarSection
        Caption = 'Untitled (0)'
      end
      object spitAdd1: TSpeedItem
        Action = acAdd
        BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
        Caption = #1044#1086#1073#1072#1074#1080#1090#1100
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333FF3333333333333003333
          3333333333773FF3333333333309003333333333337F773FF333333333099900
          33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
          99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
          33333333337F3F77333333333309003333333333337F77333333333333003333
          3333333333773333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
        Spacing = 1
        Left = 3
        Top = 3
        Visible = True
        OnClick = acAddExecute
        SectionName = 'Untitled (0)'
      end
      object spitDel: TSpeedItem
        Action = acDel
        BtnCaption = #1059#1076#1072#1083#1080#1090#1100
        Caption = #1059#1076#1072#1083#1080#1090#1100
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333FF3333333333333003333333333333F77F33333333333009033
          333333333F7737F333333333009990333333333F773337FFFFFF330099999000
          00003F773333377777770099999999999990773FF33333FFFFF7330099999000
          000033773FF33777777733330099903333333333773FF7F33333333333009033
          33333333337737F3333333333333003333333333333377333333333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
        Spacing = 1
        Left = 3
        Top = 73
        Visible = True
        OnClick = acDelExecute
        SectionName = 'Untitled (0)'
      end
      object spitArt2: TSpeedItem
        Action = acCreateArt2
        BtnCaption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333FF3333333333333003333
          3333333333773FF3333333333309003333333333337F773FF333333333099900
          33333FFFFF7F33773FF30000000999990033777777733333773F099999999999
          99007FFFFFFF33333F7700000009999900337777777F333F7733333333099900
          33333333337F3F77333333333309003333333333337F77333333333333003333
          3333333333773333333333333333333333333333333333333333333333333333
          3333333333333333333333333333333333333333333333333333}
        NumGlyphs = 2
        Spacing = 1
        Left = 3
        Top = 38
        Visible = True
        OnClick = acCreateArt2Execute
        SectionName = 'Untitled (0)'
      end
      object spitChangeArt: TSpeedItem
        Action = acChangeArt
        BtnCaption = #1048#1079#1084'. '#1072#1088#1090#1080#1082#1091#1083
        Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
        Glyph.Data = {
          36040000424D3604000000000000360000002800000010000000100000000100
          2000000000000004000000000000000000000000000000000000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009933
          000099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00993300009933000099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF0099330000AA5F1F0099330000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF0099330000BA7D48009933000099330000FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF0099330000CDA27C00D8B59600993300009933
          0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000E1C6B000ECDCCD00EDDD
          D10099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000F4E9E200FDF9
          F500FBF4EC009933000099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000E1C0AB00F7E9
          DA00F4E0CC00E1BA9C009933000099330000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000F6E6D600F3DEC800F0D5
          BA00E3B9950099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000EDCAA800EAC1
          9900E7B98B00DFA8750099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000E3AE
          7900E0A56B00DD9C5C00DA944F0099330000FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009933
          00009933000099330000993300009933000099330000FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
          FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
        Spacing = 1
        Left = 3
        Top = 108
        Visible = True
        OnClick = acChangeArtExecute
        SectionName = 'Untitled (0)'
      end
    end
  end
  object spltLeft: TRxSplitter [6]
    Left = 369
    Top = 42
    Width = 4
    Height = 534
    ControlFirst = plWH
    Align = alLeft
    BevelInner = bvLowered
  end
  object plApplProd: TPanel [7]
    Left = 13
    Top = 224
    Width = 228
    Height = 350
    BevelInner = bvLowered
    TabOrder = 6
    object spltGrids: TSplitter
      Left = 469
      Top = 2
      Width = 4
      Height = 346
    end
    object plAppl: TPanel
      Left = 2
      Top = 2
      Width = 467
      Height = 346
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object plMain: TPanel
        Left = 0
        Top = 0
        Width = 467
        Height = 29
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object lbNoDoc: TLabel
          Left = 8
          Top = 8
          Width = 37
          Height = 13
          Caption = #1047#1072#1103#1074#1082#1080
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbDateDoc: TLabel
          Left = 84
          Top = 8
          Width = 11
          Height = 13
          Caption = #1086#1090
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object SpeedButton6: TSpeedButton
          Left = 1016
          Top = 4
          Width = 133
          Height = 22
          Flat = True
        end
        object txtApplNo: TDBText
          Left = 48
          Top = 8
          Width = 33
          Height = 17
          DataField = 'DOCNO'
          DataSource = dmAppl.dsrApplInv
        end
        object txtApplDate: TDBText
          Left = 98
          Top = 8
          Width = 65
          Height = 17
          DataField = 'DOCDATE'
          DataSource = dmAppl.dsrApplInv
        end
      end
      object gridAppl: TDBGridEh
        Left = 0
        Top = 68
        Width = 467
        Height = 278
        Align = alClient
        AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dmAppl.dsrAppl
        Flat = True
        FooterColor = clBtnFace
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 6
        OptionsEh = [dghFixed3D, dghFooter3D, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghRowHighlight, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = gridApplDblClick
        OnKeyDown = gridApplKeyDown
        Columns = <
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART'
            Footer.Value = #1042#1089#1077#1075#1086
            Footer.ValueType = fvtStaticText
            Footers = <>
            ReadOnly = True
            Width = 55
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART2'
            Footers = <>
            ReadOnly = True
            Width = 29
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'SZNAME'
            Footers = <>
            ReadOnly = True
            Width = 46
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.ValueType = fvtSum
            Footers = <>
            ReadOnly = True
            Title.Caption = #1047#1072#1103#1074#1083#1077#1085#1086
            Width = 58
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'FQ'
            Footer.FieldName = 'FQ'
            Footer.ValueType = fvtSum
            Footers = <>
            ReadOnly = True
            Width = 48
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'NQ'
            Footers = <>
            ReadOnly = True
            Width = 69
          end
          item
            Checkboxes = True
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'R'
            Footer.FieldName = 'R'
            Footer.ValueType = fvtSum
            Footers = <>
            KeyList.Strings = (
              '1'
              '0')
            Width = 29
          end
          item
            Checkboxes = True
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'MODEL'
            Footers = <>
            KeyList.Strings = (
              '1'
              '0')
            Width = 31
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'COMMENT'
            Footers = <>
            Width = 92
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object TBDock1: TTBDock
        Left = 0
        Top = 29
        Width = 467
        Height = 39
        object tlbrAppl: TTBToolbar
          Left = 0
          Top = 0
          BorderStyle = bsNone
          DockMode = dmCannotFloat
          DockPos = 0
          Images = ilButtons
          Options = [tboImageAboveCaption, tboShowHint]
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          object TBItem2: TTBItem
            Action = acCloseAppl
          end
          object TBSeparatorItem1: TTBSeparatorItem
          end
          object TBItem30: TTBItem
            Action = acPrintAppl
          end
        end
      end
    end
    object TBDock2: TTBDock
      Left = 473
      Top = 2
      Width = 66
      Height = 346
      BoundLines = [blTop]
      Position = dpLeft
      object TBToolbar2: TTBToolbar
        Left = 0
        Top = 0
        Align = alLeft
        BorderStyle = bsNone
        DockMode = dmCannotFloat
        DockPos = -12
        Images = ilButtons
        ShrinkMode = tbsmWrap
        TabOrder = 0
        object TBItem6: TTBItem
          Action = acAddAppl
          Options = [tboImageAboveCaption]
        end
        object TBItem5: TTBItem
          Action = acDelAppl
          Options = [tboImageAboveCaption]
        end
      end
    end
    object plAPInv: TPanel
      Left = 539
      Top = 2
      Width = 187
      Height = 346
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object Panel1: TPanel
        Left = 0
        Top = 39
        Width = 187
        Height = 25
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label8: TLabel
          Left = 4
          Top = 4
          Width = 59
          Height = 13
          Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103':'
        end
        object txtAPDocNo: TDBText
          Left = 68
          Top = 4
          Width = 49
          Height = 17
          DataField = 'DOCNO'
          DataSource = dmAppl.dsrAPInv
        end
        object Label9: TLabel
          Left = 128
          Top = 4
          Width = 11
          Height = 13
          Caption = #1086#1090
        end
        object txtAPDocDate: TDBText
          Left = 143
          Top = 4
          Width = 74
          Height = 17
          DataField = 'DOCDATE'
          DataSource = dmAppl.dsrAPInv
        end
      end
      object gridAPInv: TDBGridEh
        Left = 0
        Top = 64
        Width = 187
        Height = 282
        Align = alClient
        AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
        Color = clBtnFace
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dmAppl.dsrAPEl
        Flat = True
        FooterColor = clBtnFace
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        OptionsEh = [dghFixed3D, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghRowHighlight, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART'
            Footers = <>
            ReadOnly = True
            Width = 49
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART2'
            Footers = <>
            ReadOnly = True
            Width = 35
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'SZ'
            Footers = <>
            ReadOnly = True
            Width = 43
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 41
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'OPERATION'
            Footers = <>
            Width = 74
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'U'
            Footers = <>
            Width = 48
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object TBDock3: TTBDock
        Left = 0
        Top = 0
        Width = 187
        Height = 39
        object tlbrApplInv: TTBToolbar
          Left = 0
          Top = 0
          BorderStyle = bsNone
          DockMode = dmCannotFloat
          Images = ilButtons
          Options = [tboImageAboveCaption, tboShowHint]
          TabOrder = 0
          object TBItem1: TTBItem
            Action = acPrintAPInv
            Images = ilButtons
          end
        end
      end
    end
  end
  inherited ilButtons: TImageList
    Left = 14
    Top = 168
    Bitmap = {
      494C01010E001300040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000002C3A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009933000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000273300008DB90000CEFC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099330000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000628100003D61000073920000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000993300009933
      0000993300000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000427A00B5E4950000403F0000375000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009933
      0000AA5F1F009933000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000004B850036D03E00A0FF9D0000B60000005E5800003D58000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000099330000BA7D480099330000993300000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000004B8E008DD0A4006EFF9E00A1CF9D0050CF500000B1000000525800003F
      5800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000099330000CDA27C00D8B596009933000099330000000000000000
      0000000000000000000000000000000000000000000000273300006381000042
      840000D0000024FF4600B2F5AA0000AB00005883580058AA5800007B0000004E
      48000026370000D4FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000E1C6B000ECDCCD00EDDDD100993300000000
      000000000000000000000000000000000000002C3A00008DB900003D6100B5E1
      9400A3FFA200B9FFC80000A30000009B0000004E00003765370096C896009ED4
      8E0053788F000464700029F6FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000099330000F4E9E200FDF9F500FBF4EC009933
      0000993300000000000000000000000000000000000000CEFC00007192000058
      3F0000CA00004ED44C0058935800007E0000001D0000D9F8D900DAFFDA0097CC
      7F000D888F0038F1F0000E383700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000099330000E1C0AB00F7E9DA00F4E0CC00E1BA
      9C00993300009933000000000000000000000000000000000000000000000034
      5000005F580000D4000058BC5800375E3700D9F8D900D9F8D90087C46F000997
      A600000000001250500000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000F6E6D600F3DEC800F0D5BA00E3B995009933
      0000000000000000000000000000000000000000000000000000000000000000
      0000003B5800006858000077000097C59700DAFFDA0087C46F000997A6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000099330000EDCAA800EAC19900E7B98B00DFA8
      7500993300000000000000000000000000000000000000000000000000000000
      0000003B5800003D5800004C48009FEC8F0097C97F000997A600006281000062
      8100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000099330000E3AE7900E0A56B00DD9C
      5C00DA944F00993300000000000000000000000000000000000000000000027D
      A30000628100006281000026370053768F000D888F00B3EBFC00003D61000062
      8100027DA3000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099330000993300009933
      0000993300009933000099330000000000000000000000000000008DB9000000
      0000027DA3000000000000D4FF000464700038F1F0001250500000000000008D
      B90000000000008DB90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000628100027D
      A30000000000000000000000000029F7FF000E38370000000000000000000000
      0000027DA3000062810000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000062810000719200008DB9000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008DB90002749700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00007B7B7B007B7B7B00BDBDBD007B7B7B00000000007B7B7B00BDBDBD007B7B
      7B007B7B7B000000000000000000000000000000000000000000000000000000
      00007B7B7B007B7B7B00BDBDBD007B7B7B00000000007B7B7B00BDBDBD007B7B
      7B007B7B7B0000000000000000000000000005710A0005710A0005710A000571
      0A000000000000009A0000009A0000009A0000009A0000009A0000009A000000
      9A0000009A0000009A0000009A0000009A000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BDBD
      BD00BDBDBD00BDBDBD00BDBDBD007B7B7B00000000007B7B7B00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD000000000000000000000000000000000000000000BDBD
      BD00BDBDBD00BDBDBD00BDBDBD007B7B7B00000000007B7B7B00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00000000000000000005710A0045D36C0032C350000571
      0A000000000000009A000333F6000D3EFC002551FC00496DFD007A95FE00B5C4
      FF00F5F8FF00FFFFFF00FFFFFF0000009A000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B007B7B7B007B7B7B00BDBDBD00BDBDBD0000000000BDBDBD00BDBDBD007B7B
      7B007B7B7B007B7B7B0000000000000000000000000000000000000000007B7B
      7B007B7B7B007B7B7B00BDBDBD00BDBDBD0000000000BDBDBD00BDBDBD007B7B
      7B007B7B7B007B7B7B00000000000000000005710A0059E2870049D571000571
      0A000000000000009A000336FC000D3FFD002652FD004A6FFE007B97FF00B8C7
      FF00F8FBFF00FFFFFF00FFFFFF0000009A000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00000000000000000000000000BDBDBD00BDBD
      BD00BDBDBD00BDBDBD000000000000000000000000000000000000000000BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00000000000000000000000000BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00000000000000000005710A0005710A0005710A000571
      0A000000000000009A0000009A0000009A0000009A0000009A0000009A000000
      9A0000009A0000009A0000009A0000009A000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B007B7B7B007B7B7B007B7B7B000000000000000000000000007B7B7B007B7B
      7B007B7B7B007B7B7B0000000000000000000000000000000000000000007B7B
      7B007B7B7B007B7B7B007B7B7B000000000000000000000000007B7B7B007B7B
      7B007B7B7B007B7B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000000000000000000000000000BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00000000000000000000FFFF000000000000000000BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD0000000000000000000000000000000000000000000571
      0A0005710A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000005710A005CE7
      8D0040BE620005710A0005710A00000000000000000000000000000000000000
      00000000000008750E0005710A00000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BDBDBD000000000000000000000000000000000000000000BDBD
      BD0000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF000000000000000000000000000000000000000000BDBD
      BD0000000000000000000000000000000000000000000000000005710A0045D1
      6B004ED978002BA5440005710A00000000000000000000000000000000000000
      00000C7A14002BA4430005710A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BDBDBD000000000000000000000000000000000000000000BDBD
      BD000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF007B7B7B00000000007B7B7B0000FFFF0000FFFF000000000000000000BDBD
      BD00000000000000000000000000000000000000000005710A001DA530002DBD
      4A0038C558000A79110000000000000000000000000000000000000000000E7D
      170046C76B0005710A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BDBDBD000000000000000000000000000000000000000000BDBD
      BD0000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000000000BDBDBD000000000000000000000000000000000000000000BDBD
      BD00000000000000000000000000000000000000000005710A0005710A000A7F
      120022B6380028B741000B7B120006730B0005720B000B7A13001D912D0048CB
      700005710A000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00007B7B7B007B7B7B00BDBDBD00000000000000000000000000BDBDBD007B7B
      7B007B7B7B000000000000000000000000000000000000FFFF000000000000FF
      FF0000000000BDBDBD000000000000000000000000000000000000000000BDBD
      BD00000000000000000000000000000000000000000000000000000000000571
      0A00077D0E0016AD27001EB434001BA02E0020A4340031B94D0042CC65000571
      0A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD000000
      00000000000000000000000000000000000000FFFF00000000000000000000FF
      FF007B7B7B007B7B7B00BDBDBD00000000000000000000000000BDBDBD007B7B
      7B007B7B7B000000000000000000000000000000000000000000000000000000
      000005710A0005710A000D9C190013A422001BAC2E001BAC2E0005710A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF000000000000000000BDBDBD00BDBDBD00BDBDBD00BDBDBD00BDBDBD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000005710A0005710A0005710A0005710A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000808080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009A6666006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000008000000080000000
      8000000080000000800000008000000080000000000000000000000000000000
      00009A6666009A666600B9666600BB6868006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000FF0000008000000080000000
      80000000800000008000000080000000800000000000000000009A6666009A66
      6600C66A6B00D06A6B00D2686900C3686900693334009A6666009A6666009A66
      66009A6666009A6666009A66660000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF0000000000000000009A666600DE73
      7400D7707100D56F7000D56D6E00C76A6D0069333400FEA2A300FCAFB000FABC
      BD00F9C5C600F9C5C6009A66660000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000808080000000000000000000000000000000000000000000808080000000
      00000000000000000000808080000000000000000000000000009A666600E077
      7800DB757600DA747500DA727300CC6E71006933340039C5650025CF630029CC
      630019CB5B00F9C5C6009A66660000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00000084000000000000000000FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000808080000000000000000000808080000000000000000000808080008080
      80008080800080808000808080000000000000000000000000009A666600E57D
      7E00E07A7B00DF797A00DF777800D07275006933340042C4680030CD670033CB
      670024CB6000F9C5C6009A66660000000000000000000000000084848400FFFF
      FF00FFFFFF000000840000008400000084000000000000000000FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000008080800000000000000000009A666600EA82
      8300E57F8000E37D7E00E6808100D3747600693334003DC2640029CB63002FCA
      640020CA5E00F9C5C6009A66660000000000000000000000000084848400FFFF
      FF000000840000008400FFFFFF00000084000000840000000000FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000008080800000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00808080000000
      00000000000000000000000000008080800000000000000000009A666600F087
      8800E9818200EC969700FBDDDE00D8888A0069333400B8E1AC006BDC89005DD5
      800046D47300F9C5C6009A66660000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000084008484840000000000FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000008080800000000000000000000000
      0000FFFFFF0000000000808080000000000000000000FFFFFF00808080000000
      00000000000000000000000000008080800000000000000000009A666600F58C
      8D00EE868700F0999A00FDDCDD00DA888A0069333400FFF5D800FFFFE000FFFF
      DE00ECFDD400F9C5C6009A66660000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000008400000000000000
      0000FFFFFF00000000000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000008080800000000000000000000000
      0000FFFFFF000000000080808000808080000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009A666600FA91
      9200F48E8F00F28B8C00F48C8D00DC7F800069333400FDF3D400FFFFDF00FFFF
      DD00FFFFE000F9C5C6009A66660000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000084000000
      0000FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      00000000000080808000000000000000000000000000000000009A666600FE97
      9800F9939400F8929300F9909200E085850069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A66660000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      8400FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      00008080800000000000FFFFFF00FFFFFF000000000000000000808080008080
      80008080800000000000808080000000000000000000000000009A666600FF9B
      9C00FD979800FC969700FE979800E388890069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A66660000000000000000000000000084848400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      00000000000080808000000000000000000000000000000000009A666600FF9F
      A000FF9A9B00FF999A00FF9A9B00E78C8D0069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A66660000000000000000000000000084848400FFFF
      FF00FFFFFF00000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000808080000000000000000000000000000000000000000000808080000000
      00008080800000000000000000000000000000000000000000009A6666009A66
      6600E98E8F00FE999A00FF9D9E00EB8F900069333400FBF0D200FDFCDC00FDFC
      DA00FDFCDC00F9C5C6009A666600000000000000000000000000000000008484
      84008484840000000000FFFFFF00000000000000000000000000000000008484
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009A666600B0717200D7868700DA888800693334009A6666009A6666009A66
      66009A6666009A6666009A666600000000000000000000000000000000000000
      0000000000000000000084848400848484008484840084848400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000808080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009A6666009A6666006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B5B5B5000000000000000000000000009C9C9C009C9C
      9C00A5A5A5000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000808080008080800000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000B5B5B500B5B5B500DEDEDE00ADADAD00525252006B6B6B00ADADAD00D6D6
      D600E7DEDE009C9C9C0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000FF0000000000000000000000000000000000000000000000FFFF00FFFF
      FF0000FFFF0000FFFF00FFFFFF0000FFFF000000000000FF000000FF00000000
      0000FFFFFF0000FFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000B5B5B500B5B5
      B500FFFFFF00FFFFFF00E7DEDE00B5B5B5005A5A630031313100313131004A4A
      4A00848484009C9C9C00A5A5A500000000000000000000000000000000008080
      80000000000000000000000000000000000000000000000000000000000000FF
      000000FF00000000000000000000000000000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF0000000000000000000000000000FF000000FF00000000
      000000000000000000007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00A5A5A500B5B5B500F7F7F700FFFF
      FF00F7F7F700D6D6D600B5B5B500A5A5A500ADADAD009C9C9C007B7B7B005252
      520031313900313131008C8C8C00000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000FF000000FF000000FF
      000000FF000000FF000000FF000000000000000000000000000000FFFF00FFFF
      FF0000FFFF0000FFFF000000000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF00007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00B5ADAD00EFEFEF00EFEFEF00CECE
      CE00BDB5B500C6C6C600D6CECE00B5B5B500ADADAD00ADA5A500ADADAD00ADAD
      AD00A5A5A500848484009C9C9C00000000000000000080808000000000000000
      0000808080000000000000000000000000000000000000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF000000000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF00007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00ADA5AD00BDBDBD00B5B5B500C6C6
      C600D6D6D600DEDEDE00F7F7F700F7F7F700E7E7E700D6D6D600BDBDBD00ADAD
      AD00ADA5A500ADADAD00A5A5A500000000000000000000000000808080000000
      00008080800000000000000000008080800000000000000000000000000000FF
      000000FF00000000000000000000000000000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF0000000000000000000000000000FF000000FF00000000
      000000000000000000007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00A5A5A500C6BDC600D6D6D600DEDE
      DE00D6D6D600EFEFEF00DEDEDE00B5BDB500CECECE00D6DEDE00DEDEDE00DEDE
      DE00D6D6D600C6C6C600ADADAD00000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF000000000000FF
      000000FF0000000000000000000000000000000000000000000000FFFF00FFFF
      FF0000FFFF0000FFFF00FFFFFF0000FFFF000000000000FF000000FF00000000
      0000FFFFFF0000FFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF000000000000000000B5ADAD00DEDEDE00DEDE
      DE00E7E7E700D6D6D600C6C6C600BDDEBD00CED6CE00D6BDB500BDB5B500BDBD
      BD00C6C6C600D6CECE00BDBDBD00000000008080800000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00000000000000
      0000000000000000000000000000808080000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF000000000000FF000000FF00000000
      000000FFFF00FFFFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000B5ADAD00CECE
      CE00C6BDBD00C6C6C600EFEFEF00FFF7F700F7F7F700F7EFE700E7E7E700DEDE
      DE00C6C6C600ADA5A50000000000000000008080800000000000000000000000
      0000FFFFFF0000000000808080000000000000000000FFFFFF00808080000000
      000000000000000000000000000080808000000000000000000000FFFF00FFFF
      FF0000FFFF0000FFFF00FFFFFF0000FFFF000000000000000000000000000000
      0000FFFFFF0000FFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000000000000000000000000000B5AD
      AD00E7E7E700D6D6D600B5B5B500CECECE00DEDEE700DEE7E700DEDEDE00D6D6
      D600C6BDBD000000000000000000000000008080800000000000000000000000
      0000FFFFFF000000000080808000808080000000000000000000000000000000
      000000000000000000000000000080808000000000000000000000FFFF00FFFF
      FF0000FFFF0000FFFF00FFFFFF0000FFFF0000FFFF00FFFFFF0000FFFF0000FF
      FF00FFFFFF0000FFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000FFEFEF00FFE7DE00EFD6CE00EFD6CE00EFDED600E7DEDE00DEDEDE00ADAD
      AD00000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFF
      FF0000FFFF00FFFFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008080800000000000FFFFFF00FFFFFF000000000000000000808080008080
      80008080800000000000808080000000000000000000000000007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B0000FFFF0000FFFF00FFFFFF0000FFFF0000FF
      FF00FFFFFF0000FFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF0000FFFF007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B0000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000808080000000000000000000000000000000000000000000808080000000
      0000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000000000000000000000000000DEB5
      B500FFE7DE00FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEB5
      B500DEB5B500DEB5B500DEB5B500F7BDB500F7BDB50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080808000808080008080800000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF00FFFFFEFF000000007FFFFC7F00000000
      9FFFFC7F00000000C7FFFC3F00000000E3FFF81F00000000F0FFF00F00000000
      F83F800300000000FC1F000100000000FE07800100000000FE03E00B00000000
      FC0FF01F00000000FE07F00F00000000FF03E00700000000FF81D42B00000000
      FFFFCE7300000000FFFF1FF900000000FFFFFC1FFC1FFFFFFFFFF007F007FFFF
      FFFFE003E0030800FF3FC001C0010800FC3FC001C0010800F03FC001C0010800
      C000C001C001FFFF0000C0014001E7FFC000E003A003C1F9F03FF1C7C3C7C1F1
      FC3FF1C7004783E3FF3FF1C7C1C78007FFFFF007A1C7E00FFFFFF80F6007F01F
      FFFFFC1FE80FFC3FFFFFFFFFEC1FFFFFFC7FFE7FFFFFFFFFFC00F07FE007FFFF
      EC00C001C003FFFFC780C001C303FFFF9191C001C183FCFFD641C001C0C3FC3F
      EC20C001C043FC0F2B8CC001C0230003309CC001C033000000A3C001C0130003
      E863C001C003FC0FD4C1C001C003FC3F9133C001C003FCFFC787C001E1C7FFFF
      EC3FF001FC3FFFFFFC7FFC7FFFFFFFFFFFFFFFFFFDC7FC43FF0F8001F003FC43
      C0018001C001EC00C00180000001C700C001800000019100C00180000001D600
      C00180000001EC03C001800180012B00C0018001C003309CC0018001E00700A0
      C0018001F00FE863C0018001F03FF4C1C0018001F03F9133C0038001F03FC787
      FFFFFFC1E03FEC3FFFFFC07FE07FFC7F00000000000000000000000000000000
      000000000000}
  end
  inherited fmstr: TFormStorage
    StoredProps.Strings = (
      'plWH.Width')
    Left = 12
    Top = 216
  end
  inherited ActionList2: TActionList
    Left = 168
    Top = 172
    object ActionDemand: TAction
      Caption = #1047#1072#1103#1074#1082#1072
      ImageIndex = 22
      OnExecute = ActionDemandExecute
      OnUpdate = ActionDemandUpdate
    end
  end
  object acEvent: TActionList
    Images = ilButtons
    Left = 60
    Top = 168
    object acAdd: TAction
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = acAddExecute
      OnUpdate = acAddUpdate
    end
    object acDel: TAction
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = acDelExecute
    end
    object acAdd2: TAction
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = #1057#1076#1072#1090#1100' '#1072#1088#1090'2'
      OnExecute = acAdd2Execute
      OnUpdate = acAdd2Update
    end
    object acDel2: TAction
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = #1059#1076#1072#1083#1077#1085#1080#1077' '#1072#1088#1090'2'
      OnExecute = acDel2Execute
    end
    object acPAdd: TAction
      Category = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1074' '#1088#1072#1073#1086#1090#1091
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1088#1072#1073#1086#1090#1091' '#1085#1086#1074#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
      ImageIndex = 3
      OnExecute = acPAddExecute
    end
    object acPDel: TAction
      Category = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1074' '#1088#1072#1073#1086#1090#1091
      OnExecute = acPDelExecute
    end
    object acRej: TAction
      Category = #1057#1085#1103#1090#1080#1077' '#1089' '#1088#1072#1073#1086#1090#1099
      Caption = '  '#1057#1085#1103#1090#1100'  '
      Hint = #1057#1085#1103#1090#1100' '#1089' '#1088#1072#1073#1086#1090#1099' '#1072#1088#1090#1080#1082#1091#1083
      ImageIndex = 4
      OnExecute = acRejExecute
    end
    object acRejDel: TAction
      Category = #1057#1085#1103#1090#1080#1077' '#1089' '#1088#1072#1073#1086#1090#1099
      OnExecute = acRejDelExecute
    end
    object acStone: TAction
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1082#1072#1084#1085#1077#1081
      OnExecute = acStoneExecute
      OnUpdate = acStoneUpdate
    end
    object acCreateArt2: TAction
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = #1040#1088#1090#1080#1082#1091#1083' 2'
      OnExecute = acCreateArt2Execute
      OnUpdate = acCreateArt2Update
    end
    object acChangeSz: TAction
      Category = #1057#1082#1083#1072#1076' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1088#1072#1079#1084#1077#1088
      ShortCut = 24666
      OnExecute = acChangeSzExecute
    end
    object acChangeArt: TAction
      Category = #1057#1082#1083#1072#1076' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
      ShortCut = 24641
      OnExecute = acChangeArtExecute
    end
    object acPrintInv: TAction
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = #1055#1077#1095#1072#1090#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
      OnExecute = acPrintInvExecute
    end
    object acChangeArt2: TAction
      Category = #1057#1082#1083#1072#1076' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083' 2'
      ShortCut = 24626
      OnExecute = acChangeArt2Execute
      OnUpdate = acChangeArt2Update
    end
    object acChangeByAppl: TAction
      Category = #1057#1082#1083#1072#1076' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1087#1086' '#1079#1072#1082#1072#1079#1091
      OnExecute = acChangeByApplExecute
      OnUpdate = acChangeByApplUpdate
    end
    object acPrintWh: TAction
      Category = #1057#1082#1083#1072#1076' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = 'C'#1086#1089#1090#1086#1103#1085#1080#1077' '#1084#1077#1089#1090#1072' '#1093#1088#1072#1085#1077#1085#1080#1103
      ImageIndex = 2
      OnExecute = acPrintWhExecute
    end
    object acWhKind: TAction
      Category = #1057#1082#1083#1072#1076' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = #1060#1072#1082#1090'.'
      OnExecute = acWhKindExecute
    end
    object acAddAppl: TAction
      Category = #1047#1072#1103#1074#1082#1072
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 7
      OnExecute = acAddApplExecute
    end
    object acDelAppl: TAction
      Category = #1047#1072#1103#1074#1082#1072
      Caption = ' '#1059#1076#1072#1083#1080#1090#1100' '
      ImageIndex = 8
      OnExecute = acDelApplExecute
    end
    object acPrintAPInv: TAction
      Category = #1047#1072#1103#1074#1082#1072
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103
      ImageIndex = 2
    end
    object acCloseAppl: TAction
      Category = #1047#1072#1103#1074#1082#1072
      Caption = #1054#1090#1082#1088#1099#1090
      ImageIndex = 10
      OnExecute = acCloseApplExecute
    end
    object acNewAppl: TAction
      Category = #1047#1072#1103#1074#1082#1072
      Caption = #1047#1072#1103#1074#1082#1072
      OnExecute = acNewApplExecute
    end
    object acInvArt2Ins: TAction
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = #1042#1089#1090#1072#1074#1082#1080
      ImageIndex = 13
      OnExecute = acInvArt2InsExecute
      OnUpdate = acInvArt2InsUpdate
    end
    object acWhArt2Ins: TAction
      Category = #1057#1082#1083#1072#1076' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = #1042#1089#1090#1072#1074#1082#1080' '#1072#1088#1090#1080#1082#1091#1083#1072' 2'
      ImageIndex = 13
      OnExecute = acWhArt2InsExecute
      OnUpdate = acWhArt2InsUpdate
    end
    object acShowStone: TAction
      Category = #1055#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1076#1074#1080#1078#1077#1085#1080#1077' '#1082#1072#1084#1085#1077#1081
      OnExecute = acShowStoneExecute
    end
    object acShowID: TAction
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' ID'
      ShortCut = 24649
      OnExecute = acShowIDExecute
    end
    object acPrintAppl: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 2
      OnExecute = acPrintApplExecute
      OnUpdate = acPrintApplUpdate
    end
  end
  object ppWh: TTBPopupMenu
    Images = ilButtons
    Left = 115
    Top = 169
    object TBItem14: TTBItem
      Action = acPAdd
    end
    object TBItem15: TTBItem
      Action = acRej
    end
    object TBSeparatorItem4: TTBSeparatorItem
    end
    object TBItem16: TTBItem
      Action = acChangeSz
    end
    object TBItem17: TTBItem
      Action = acChangeArt
    end
    object TBItem13: TTBItem
      Action = acChangeArt2
    end
    object TBSeparatorItem5: TTBSeparatorItem
    end
    object TBItem12: TTBItem
      Action = acWhArt2Ins
    end
    object TBSeparatorItem3: TTBSeparatorItem
    end
    object TBItem11: TTBItem
      Action = acPrintWh
    end
  end
  object ppPInv: TTBPopupMenu
    Left = 520
    Top = 372
    object TBItem18: TTBItem
      Action = acPAdd
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
    end
    object TBItem19: TTBItem
      Action = acPDel
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
  end
  object ppRejInv: TTBPopupMenu
    Left = 528
    Top = 516
    object TBItem20: TTBItem
      Action = acRej
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1074' '#1085#1072#1082#1083#1072#1076#1085#1091#1102
    end
    object TBItem21: TTBItem
      Action = acRejDel
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
  end
  object ppInv: TTBPopupMenu
    Left = 808
    Top = 168
    object TBItem24: TTBItem
      Action = acStone
    end
    object TBItem23: TTBItem
      Action = acInvArt2Ins
    end
    object TBSeparatorItem6: TTBSeparatorItem
    end
    object TBItem22: TTBItem
      Action = acPrintInv
    end
  end
  object frReportAppl: TfrReport
    InitialZoom = pzOnePage
    PreviewButtons = [pbZoom, pbPrint, pbFind, pbExit]
    StoreInDFM = True
    RebuildPrinter = False
    Left = 40
    Top = 368
    ReportForm = {
      1900000022080000190000000001000100FFFFFFFFFF09000000340800009A0B
      00000000000000000000000000000000000000FFFF00000000FFFF0000000000
      00000000000000030400466F726D000F000080DC000000780000007C0100002C
      010000040000000200CB0000000C005265706F72745469746C65310002010000
      00002C000000F5020000140000003000000001000000000000000000FFFFFF1F
      00000000000000000000000000FFFF0000000000020000000100000000000000
      01000000C800000014000000010000000000000200430100000B004D61737465
      724461746131000201000000009C000000F50200001400000030000500010000
      00000000000000FFFFFF1F000000000D006672446174615365744170706C0000
      0000000000FFFF000000000002000000010000000000000001000000C8000000
      14000000010000000000000200AE0100000B0050616765486561646572310002
      010000000060000000F5020000140000003000020001000000000000000000FF
      FFFF1F00000000000000000000000000FFFF0000000000020000000100000000
      00000001000000C8000000140000000100000000000000004102000005004D65
      6D6F31000200100000009C000000600000001400000003000700010000000000
      00000000FFFFFF1F2C020000000000010015005B646D4170706C2E7461417070
      6C2E22415254225D00000000FFFF000000000002000000010000000005004172
      69616C000A000000000000000000000000000100020000000000FFFFFF000000
      0002000000000000000000D502000005004D656D6F32000200700000009C0000
      0060000000140000000300070001000000000000000000FFFFFF1F2C02000000
      0000010016005B646D4170706C2E74614170706C2E2241525432225D00000000
      FFFF00000000000200000001000000000500417269616C000A00000000000000
      0000000000000100020000000000FFFFFF00000000020000000000000000006B
      03000005004D656D6F33000200D00000009C0000003400000014000000030007
      0001000000000000000000FFFFFF1F2C020000000000010018005B646D417070
      6C2E74614170706C2E22535A4E414D45225D00000000FFFF0000000000020000
      0001000000000500417269616C000A0000000000000000000000000001000200
      00000000FFFFFF0000000002000000000000000000FC03000005004D656D6F34
      000200040100009C0000006C0000001400000003000700010000000000000000
      00FFFFFF1F2C020000000000010013005B646D4170706C2E74614170706C2E22
      51225D00000000FFFF00000000000200000001000000000500417269616C000A
      000000000000000000000000000100020000000000FFFFFF0000000002000000
      0000000000008E04000005004D656D6F35000200700100009C0000006C000000
      140000000300070001000000000000000000FFFFFF1F2C020000000000010014
      005B646D4170706C2E74614170706C2E224651225D00000000FFFF0000000000
      0200000001000000000500417269616C000A0000000000000000000000000001
      00020000000000FFFFFF00000000020000000000000000001305000005004D65
      6D6F360002001000000060000000600000001400000003000E00010000000000
      00000000FFFFFF1F2C02000000000001000700C0F0F2E8EAF3EB00000000FFFF
      00000000000200000001000000000500417269616C000A000000000000000000
      000000000100020000000000FFFFFF0000000002000000000000000000950500
      0005004D656D6F370002007000000060000000600000001400000003000E0001
      000000000000000000FFFFFF1F2C02000000000001000400C0F0F23200000000
      FFFF00000000000200000001000000000500417269616C000A00000000000000
      0000000000000100020000000000FFFFFF000000000200000000000000000019
      06000005004D656D6F38000200D000000060000000340000001400000003000E
      0001000000000000000000FFFFFF1F2C02000000000001000600D0E0E7ECE5F0
      00000000FFFF00000000000200000001000000000500417269616C000A000000
      000000000000000000000100020000000000FFFFFF0000000002000000000000
      0000009F06000005004D656D6F3900020004010000600000006C000000140000
      0003000E0001000000000000000000FFFFFF1F2C02000000000001000800C7E0
      FFE2EBE5EDEE00000000FFFF0000000000020000000100000000050041726961
      6C000A000000000000000000020000000100020000000000FFFFFF0000000002
      0000000000000000002E07000006004D656D6F31300002007001000060000000
      6C0000001400000003000F0001000000000000000000FFFFFF1F2C0200000000
      0001001000CFF0E8EDFFF2EE20E220F0E0E1EEF2F300000000FFFF0000000000
      0200000001000000000500417269616C000A0000000000000000000000000001
      00020000000000FFFFFF0000000002000000000000000000F007000006004D65
      6D6F3131000200100000002C000000D402000014000000030000000100000000
      0000000000FFFFFF1F2C02000000000001004300C7E0FFE2EAE020B9205B646D
      4170706C2E74614170706C496E762E22444F434E4F225D20EEF2205B646D4170
      706C2E74614170706C496E762E22444F4344415445225D00000000FFFF000000
      00000200000001000000000500417269616C000A000000020000000000020000
      000100020000000000FFFFFF000000000200000000000000FEFEFF0000000000
      00000000000000FC000000000000000000000000000000005800BC27E80C5294
      E3407C3C07894BBDE440}
  end
  object frDataSetAppl: TfrDBDataSet
    DataSource = dmAppl.dsrAppl
    Left = 40
    Top = 400
  end
  object frDBDataSet1: TfrDBDataSet
    DataSource = dmAppl.dsrApplInv
    Left = 40
    Top = 432
  end
end
