object DialogTechnologyRecalc: TDialogTechnologyRecalc
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1077#1088#1077#1089#1095#1105#1090#1072
  ClientHeight = 214
  ClientWidth = 233
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxDBVerticalGrid
    Left = 0
    Top = 0
    Width = 233
    Height = 177
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    OptionsView.ScrollBars = ssVertical
    OptionsView.CategoryExplorerStyle = True
    OptionsView.PaintStyle = psDelphi
    OptionsView.GridLineColor = clBtnShadow
    OptionsView.RowHeaderWidth = 114
    TabOrder = 0
    DataController.DataSource = fmTechnologyGroups.SourceConsts
    Version = 1
    object GridCategoryRow1: TcxCategoryRow
      Properties.Caption = #1047#1086#1083#1086#1090#1086
      ID = 0
      ParentID = -1
      Index = 0
      Version = 1
    end
    object GridDBEditorRow1: TcxDBEditorRow
      Properties.Caption = #1062#1077#1085#1072' '#1079#1072' '#1075#1088#1072#1084#1084
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.DataBinding.FieldName = 'GOLD$COST'
      ID = 1
      ParentID = -1
      Index = 1
      Version = 1
    end
    object GridDBEditorRow2: TcxDBEditorRow
      Properties.Caption = #1055#1088#1086#1094#1077#1085#1090' '#1087#1086#1090#1077#1088#1100
      Properties.DataBinding.FieldName = 'LOSSES$PERCENT'
      ID = 2
      ParentID = -1
      Index = 2
      Version = 1
    end
    object GridCategoryRow2: TcxCategoryRow
      Properties.Caption = #1057#1077#1088#1077#1073#1088#1086
      ID = 3
      ParentID = -1
      Index = 3
      Version = 1
    end
    object GridDBEditorRow3: TcxDBEditorRow
      Options.CanResized = False
      Options.Moving = False
      Properties.Caption = #1062#1077#1085#1072' '#1079#1072' '#1075#1088#1072#1084#1084
      Properties.EditPropertiesClassName = 'TcxCurrencyEditProperties'
      Properties.DataBinding.FieldName = 'SILVER$COST'
      Properties.Options.Filtering = False
      ID = 4
      ParentID = -1
      Index = 4
      Version = 1
    end
    object GridDBEditorRow4: TcxDBEditorRow
      Properties.Caption = #1055#1088#1086#1094#1077#1085#1090' '#1087#1086#1090#1077#1088#1100
      Properties.DataBinding.FieldName = 'SILVER$LOSSES$PERCENT'
      ID = 5
      ParentID = -1
      Index = 5
      Version = 1
    end
    object GridCategoryRow3: TcxCategoryRow
      Properties.Caption = #1055#1077#1088#1080#1086#1076
      ID = 6
      ParentID = -1
      Index = 6
      Version = 1
    end
    object GridDBEditorRow5: TcxDBEditorRow
      Properties.Caption = #1053#1072#1095#1072#1083#1086
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.SaveTime = False
      Properties.EditProperties.ShowTime = False
      Properties.DataBinding.FieldName = 'STARTD'
      ID = 7
      ParentID = -1
      Index = 7
      Version = 1
    end
    object GridDBEditorRow6: TcxDBEditorRow
      Properties.Caption = #1050#1086#1085#1077#1094
      Properties.EditPropertiesClassName = 'TcxDateEditProperties'
      Properties.EditProperties.SaveTime = False
      Properties.EditProperties.ShowTime = False
      Properties.DataBinding.FieldName = 'ENDD'
      ID = 8
      ParentID = -1
      Index = 8
      Version = 1
    end
  end
  object btnConfirm: TcxButton
    Left = 99
    Top = 183
    Width = 60
    Height = 25
    Caption = ' '#1044#1072#1083#1077#1077
    ModalResult = 6
    TabOrder = 1
    OnClick = btnConfirmClick
    LookAndFeel.Kind = lfUltraFlat
    PaintStyle = bpsCaption
  end
  object btnCancel: TcxButton
    Left = 165
    Top = 183
    Width = 60
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    ModalResult = 2
    TabOrder = 2
    OnClick = btnCancelClick
    LookAndFeel.Kind = lfUltraFlat
  end
end
