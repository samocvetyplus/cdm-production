unit StoreItems;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, RxPlacemnt,InvData, ImgList, RxSpeedBar, ExtCtrls, ComCtrls,
  Grids, DBGrids, RXDBCtrl,  DB, dbUtil, DBGridEh, ActnList,
  DBGridEhGrouping, GridsEh ;

type
  TfmStoreItems = class(TfmAncestor)
    grItems: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmStoreItems: TfmStoreItems;

implementation



{$R *.dfm}

procedure TfmStoreItems.FormCreate(Sender: TObject);
begin
  inherited;
//  OpenDataSets([dmINV.tastoreItems]);
end;

procedure TfmStoreItems.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
//  CloseDataSets([dmINV.taStoreItems]);
end;

end.
