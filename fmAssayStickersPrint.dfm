object fmPrintAssayStickers: TfmPrintAssayStickers
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = #1041#1080#1088#1082#1080' '#1076#1083#1103' '#1087#1088#1086#1073#1080#1088#1082#1080
  ClientHeight = 128
  ClientWidth = 305
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbUnit: TLabel
    Left = 287
    Top = 11
    Width = 9
    Height = 13
    Caption = #1075'.'
  end
  object Label1: TLabel
    Left = 232
    Top = 65
    Width = 9
    Height = 13
    Caption = #1075'.'
  end
  object Label2: TLabel
    Left = 71
    Top = 65
    Width = 18
    Height = 13
    Caption = #1096#1090'.'
  end
  object cbGood: TComboBox
    Left = 8
    Top = 8
    Width = 145
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 0
    Text = #1050#1086#1083#1100#1094#1072
    Items.Strings = (
      #1050#1086#1083#1100#1094#1072
      #1057#1077#1088#1100#1075#1080
      #1041#1088#1072#1089#1083#1077#1090#1099
      #1055#1086#1076#1074#1077#1089#1099
      #1050#1086#1083#1100#1077
      #1062#1077#1087#1080)
  end
  object cbCondition: TComboBox
    Left = 159
    Top = 8
    Width = 82
    Height = 21
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 1
    Text = #1076#1086
    Items.Strings = (
      #1076#1086
      #1089#1074#1099#1096#1077)
  end
  object edWeightLimit: TEdit
    Left = 247
    Top = 8
    Width = 34
    Height = 21
    TabOrder = 2
    Text = '2'
  end
  object cbWhiteInsertions: TComboBox
    Left = 8
    Top = 35
    Width = 273
    Height = 21
    ItemHeight = 13
    TabOrder = 3
    Items.Strings = (
      #1089' '#1101#1083#1077#1084'. '#1073#1077#1083'. '#1079#1086#1083#1086#1090#1072)
  end
  object edTotalQ: TEdit
    Left = 8
    Top = 62
    Width = 57
    Height = 21
    TabOrder = 4
  end
  object edTotalW: TEdit
    Left = 144
    Top = 62
    Width = 82
    Height = 21
    TabOrder = 5
  end
  object btnPrint: TButton
    Left = 222
    Top = 95
    Width = 75
    Height = 25
    Caption = #1055#1077#1095#1072#1090#1100
    TabOrder = 6
    OnClick = btnPrintClick
  end
end
