inherited fmqArtStone: TfmqArtStone
  Left = 310
  Top = 188
  ClientHeight = 152
  ClientWidth = 268
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 268
    Height = 152
    object lbText: TLabel [0]
      Left = 8
      Top = 3
      Width = 256
      Height = 26
      Caption = 
        #1042#1084#1077#1089#1090#1077' '#1089' '#1074#1099#1073#1088#1072#1085#1085#1099#1084' '#1072#1088#1090#1080#1082#1091#1083#1086#1084' '#1074#1099#1076#1072#1074#1072#1083#1080#1089#1100#13#10#1082#1072#1084#1085#1080'. '#1042#1099#1073#1077#1088#1080#1090#1077' '#1095#1090#1086' '#1085#1077#1086 +
        #1073#1093#1086#1076#1080#1084#1086' '#1089' '#1085#1080#1084#1080' '#1089#1076#1077#1083#1072#1090#1100' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Layout = tlCenter
    end
    inherited btnOk: TBitBtn
      Left = 76
      Top = 116
    end
    inherited btnCancel: TBitBtn
      Left = 172
      Top = 116
    end
    object rdgrQ: TRadioGroup
      Left = 8
      Top = 36
      Width = 245
      Height = 71
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemIndex = 0
      Items.Strings = (
        #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
        #1042#1077#1088#1085#1091#1090#1100' '#1087#1086' '#1085#1072#1082#1083#1072#1076#1085#1086#1081)
      ParentFont = False
      TabOrder = 2
    end
  end
end
