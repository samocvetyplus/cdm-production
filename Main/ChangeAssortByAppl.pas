unit ChangeAssortByAppl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList, RxPlacemnt, ImgList, RxSpeedBar, Grids,
  DBGridEh, Buttons, ExtCtrls, ComCtrls, StdCtrls, TB2Item, TB2Dock,
  TB2Toolbar, Mask, DBCtrlsEh, DBGridEhGrouping, GridsEh;

type
  TfmChangeAssortByAppl = class(TfmDictAncestor)
    acShowID: TAction;
    acChangeArt: TAction;
    chbxGet: TCheckBox;
    chbxAQ: TCheckBox;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    TBControlItem2: TTBControlItem;
    cmbxOper: TDBComboBoxEh;
    procedure acShowIDExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure gridDictGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acChangeArtExecute(Sender: TObject);
    procedure cmbxOperChange(Sender: TObject);
  end;

implementation

uses ApplData, InvData, FIBDataSet, DB, ADistr, dbUtil, MsgDialog, DictData,
  dbTree;

{$R *.dfm}

procedure TfmChangeAssortByAppl.acShowIDExecute(Sender: TObject);
begin
  gridDict.FieldColumns['JAID'].Visible := not gridDict.FieldColumns['JAID'].Visible;
  gridDict.FieldColumns['AARTID'].Visible := not gridDict.FieldColumns['AARTID'].Visible;
  gridDict.FieldColumns['AART2ID'].Visible := not gridDict.FieldColumns['AART2ID'].Visible;
  gridDict.FieldColumns['ASZID'].Visible := not gridDict.FieldColumns['ASZID'].Visible;
  gridDict.FieldColumns['AOPERID'].Visible := not gridDict.FieldColumns['AOPERID'].Visible;
  gridDict.FieldColumns['AUNITID'].Visible := not gridDict.FieldColumns['AUNITID'].Visible;
  gridDict.FieldColumns['WHARTID'].Visible := not gridDict.FieldColumns['WHARTID'].Visible;
  gridDict.FieldColumns['WHART2ID'].Visible := not gridDict.FieldColumns['WHART2ID'].Visible;
  gridDict.FieldColumns['WHSZID'].Visible := not gridDict.FieldColumns['WHSZID'].Visible;
  gridDict.FieldColumns['WHOPERID'].Visible := not gridDict.FieldColumns['WHOPERID'].Visible;
  gridDict.FieldColumns['WHUNITID'].Visible := not gridDict.FieldColumns['WHUNITID'].Visible;
end;

procedure TfmChangeAssortByAppl.FormCreate(Sender: TObject);
var
  i : integer;
begin
  gridDict.DataSource := dmAppl.dsrChangeAssortByAppl;
  gridDict.FieldColumns['JAID'].Visible := False;
  gridDict.FieldColumns['AARTID'].Visible := False;
  gridDict.FieldColumns['AART2ID'].Visible := False;
  gridDict.FieldColumns['ASZID'].Visible := False;
  gridDict.FieldColumns['AOPERID'].Visible := False;
  gridDict.FieldColumns['AUNITID'].Visible := False;
  gridDict.FieldColumns['WHARTID'].Visible := False;
  gridDict.FieldColumns['WHART2ID'].Visible := False;
  gridDict.FieldColumns['WHSZID'].Visible := False;
  gridDict.FieldColumns['WHOPERID'].Visible := False;
  gridDict.FieldColumns['WHUNITID'].Visible := False;
  cmbxOper.OnChange := nil;
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.ItemIndex := 0;
  if not VarIsNull(dm.Rec.ChangeApplOperId) then
    with cmbxOper, Items do
      for i:=0 to Pred(Count) do
        if (TNodeData(Objects[i]).Code = dm.Rec.ChangeApplOperId)then
        begin
          ItemIndex := i;
          break;
        end;
  dmAppl.FilterChangeAssortByApplOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  cmbxOper.OnChange := cmbxOperChange;
  inherited;
  gridDict.ReadOnly := False;
end;

procedure TfmChangeAssortByAppl.gridDictGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if (Column.FieldName = 'AART')or(Column.FieldName = 'ASZNAME')or
     (Column.FieldName = 'AOPERNAME')or(Column.FieldName = 'AQ')or
     (Column.FieldName = 'AFQ')
  then Background := clBtnFace2;
end;

procedure TfmChangeAssortByAppl.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : ;
    else inherited;
  end;
end;

procedure TfmChangeAssortByAppl.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  case  ModalResult of
    mrOk : acChangeArt.Execute;
  end;
  inherited;
end;

procedure TfmChangeAssortByAppl.acChangeArtExecute(Sender: TObject);

  procedure _changeart;
  var
    NewArtId, NewArt2Id, NewSzId, NewU, OldArtId, OldArt2Id, OldSzId, OldU, Q : integer;
    NewOperId, OldOperId : string;
  begin
    NewArtId := dmAppl.taChangeAssortByApplAARTID.AsInteger;
    NewArt2Id := dmAppl.taChangeAssortByApplAART2ID.AsInteger;
    NewSzId := dmAppl.taChangeAssortByApplASZID.AsInteger;
    NewOperId := dmAppl.taChangeAssortByApplWHOPERID.AsString; // �������� �� ��������, ������� �� ������
    NewU := dmAppl.taChangeAssortByApplWHUNITID.AsInteger; // ��.���. ������� �� ������

    OldArtId := dmAppl.taChangeAssortByApplWHARTID.AsInteger;
    OldArt2Id := dmAppl.taChangeAssortByApplWHART2ID.AsInteger;
    OldSzId := dmAppl.taChangeAssortByApplWHSZID.AsInteger;
    OldOperId := dmAppl.taChangeAssortByApplWHOPERID.AsString;
    OldU := dmAppl.taChangeAssortByApplWHUNITID.AsInteger;

    Q := dmAppl.taChangeAssortByApplQ.AsInteger;

    (* ����� � ������ ������� *)
    if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty) then
    begin
      fmADistr.plRej.Visible := True;
      dmAppl.taRejInv.Append;
      dmAppl.taRejInv.Post;
      ReOpenDataSet(dmAppl.taRejInv);
    end;
    if not dmAppl.taRejEl.Locate('ART2ID;SZID;OPERID;U',
      VarArrayOf([OldArt2Id, OldSzId, OldOperId, OldU]), [])then
    begin
      dmAppl.taRejEl.Append;
      try
        dmAppl.taRejElQ.AsInteger := Q;
        dmAppl.taRejElARTID.AsInteger := OldArtId;
        dmAppl.taRejElOPERID.AsString := OldOperId;
        dmAppl.taRejElSZID.AsInteger := OldSzId;
        dmAppl.taRejElART2ID.AsInteger := OldArt2Id;
        dmAppl.taRejElCOMMENTID.AsInteger := 2;
        dmAppl.taRejElCOMMENT.AsString := '�� ����������������� ������';
        try
          dmAppl.taRejEl.Post;
        except
          on E:Exception do begin
            dmAppl.taRejEl.Cancel;
            MessageDialog('������ ��� ���������� ������. dmAppl.taRejEl.Post'#13+E.Message, mtError, [mbOk], 0);
          end;
        end;
      except
        on E : Exception do begin
          dmAppl.taRejEl.Cancel;
          MessageDialog('������ ��� ������ �������� � ������'#13+E.Message, mtError, [mbOk], 0);
        end;
      end;
    end
    else begin
      dmAppl.taRejEl.Edit;
      dmAppl.taRejElQ.AsInteger := dmAppl.taRejElQ.AsInteger + Q;
      try
        dmAppl.taRejEl.Post;
      except
        on E:Exception do begin
           dmAppl.taRejEl.Cancel;
           MessageDialog('������ ��� ���������� ������. dmAppl.taRejEl.Post 2'#13+E.Message, mtError, [mbOk], 0);
        end;
      end;
    end;
    (* �������� � ������ ������� *)
    if(dmAppl.taPInv.IsEmpty or (dmAppl.PInvid = 0)) then
    begin
      fmADistr.plPInv.Visible := True;
      dmAppl.taPInv.Append;
      dmAppl.taPInv.Post;
      ReOpenDataSets([dmAppl.taPInv]);
    end;
    if not dmAppl.taPEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([NewArt2Id, NewSzId, NewOperId, NewU]), []) then
    begin
      dmAppl.taPEl.Append;
      try
        dmAppl.taPElQ.AsInteger := Q;
        dmAppl.taPElARTID.AsInteger := NewArtId;
        dmAppl.taPElOPERID.AsString := NewOperId;
        dmAppl.taPElSZID.AsInteger := NewSzId;
        dmAppl.taPElART2ID.AsInteger := NewArt2Id;
        dmAppl.taPElU.AsInteger := NewU;
        dmAppl.taPElCOMMENTID.AsInteger := 2;
        dmAppl.taPElCOMMENT.AsString := '�� ����������������� ������';
        try
          dmAppl.taPEl.Post;
        except
          on E:Exception do begin
            dmAppl.taPEl.Cancel;
            MessageDialog('������ ��� ���������� ������. dmAppl.taPEl.Post'#13+E.Message, mtError, [mbOk], 0);
          end;
        end;
      except
        on E : Exception do begin
          dmAppl.taPEl.Cancel;
          MessageDialog('������ ��� ���������� �������� � ������'#13+E.Message, mtError, [mbOk], 0);
        end;
      end;
    end
    else begin
      dmAppl.taPEl.Edit;
      dmAppl.taPElQ.AsInteger := dmAppl.taPElQ.AsInteger + Q;
      try
        dmAppl.taPEl.Post;
      except
        on E:Exception do begin
          dmAppl.taPEl.Cancel;
          MessageDialog('������ ��� ���������� ������. dmAppl.taPEl.Post 2'#13+E.Message, mtError, [mbOk], 0);
        end;
      end;
    end;
    if not chbxGet.Checked then eXit;
    (* ������ �� ����� �������� *)
    if not dmAppl.taAEl.Locate('ART2ID;SZID;OPERIDFROM;U_OLD',
      VarArrayOf([NewArt2Id, NewSzId, NewOperId, NewU]), []) then
    begin
      dmAppl.taAEl.Append;
      try
        dmAppl.taAElARTID.AsInteger := NewArtId;
        dmAppl.taAElART2ID.AsInteger := NewArt2Id;
        dmAppl.taAElSZID.AsInteger := NewSzId;
        dmAppl.taAElOPERID.AsString := dmAppl.AOperId;
        dmAppl.taAElU.AsInteger := NewU;

        dmAppl.taAElART2ID_OLD.AsInteger := NewArt2Id;
        dmAppl.taAElOPERIDFROM.AsString := NewOperId;
        dmAppl.taAElU_OLD.AsInteger := NewU;

        dmAppl.taAElQ.AsInteger := Q;
        try
          dmAppl.taAEl.Post;
        except
          on E:Exception do begin
            dmAppl.taAEl.Cancel;
            MessageDialog('������ ��� ���������� ������. dmAppl.taAEl.Post'#13+E.Message, mtError, [mbOk], 0);
          end;
        end;
       except
         on E: Exception do begin
            dmAppl.taAEl.Cancel;
            MessageDialog('������ ��� ������ ��������'#13+E.Message, mtError, [mbOk], 0);
         end;
       end;
    end
    else begin
      dmAppl.taAEl.Edit;
      dmAppl.taAElQ.AsInteger := dmAppl.taAElQ.AsInteger + Q;
      try
        dmAppl.taAEl.Post;
      except
        on E:Exception do begin
          dmAppl.taAEl.Cancel;
          MessageDialog('������ ��� ���������� ������. dmAppl.taAEl.Post'#13+E.Message, mtError, [mbOk], 0);
        end;
      end;  
    end
  end; // procedure _changeart

var
  bm: Pointer;
  q : integer;
begin
  with dmAppl.taChangeAssortByAppl do
  begin
    if State in [dsEdit, dsInsert] then Post;
    bm := GetBookmark;
    DisableScrollEvents;
    DisableControls;
    try
      First;
      while not Eof do
      begin
        q := FieldByName('Q').AsInteger;
        if(q <> 0) then
        begin
          _changeart;
          if State in [dsEdit] then Post;
          Edit;
          if chbxAQ.Checked then FieldByName('AFQ').AsInteger := FieldByName('AFQ').AsInteger+q;
          Post;
        end;
        Next;
      end;
    finally
      GotoBookmark(bm);
      EnableScrollEvents;
      EnableControls;
    end;
  end;
end;

procedure TfmChangeAssortByAppl.cmbxOperChange(Sender: TObject);
begin
  dmAppl.FilterChangeAssortByApplOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSet(dmAppl.taChangeAssortByAppl);
end;

end.
