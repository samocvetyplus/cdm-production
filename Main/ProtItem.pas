unit ProtItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList, ExtCtrls, ComCtrls,
  Grids, DBGridEh, StdCtrls, DBCtrlsEh, TB2Item, Menus, DBSumLst, DB,
  pFIBDataSet, ActnList, FIBDataSet, TB2Dock, TB2Toolbar, Mask,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmProtItem = class(TfmAncestor)
    gridSemis: TDBGridEh;
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;
    ppSemis: TTBPopupMenu;
    TBItem1: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    itDetail: TTBItem;
    taPIt_TW: TpFIBDataSet;
    taPIt_TWINW: TFloatField;
    taPIt_TWGETW: TFloatField;
    taPIt_TWREWORKW: TFloatField;
    taPIt_TWDONEW: TFloatField;
    taPIt_TWRETW: TFloatField;
    taPIt_TWREJW: TFloatField;
    taPIt_TWNKW: TFloatField;
    taPIt_TWIW: TFloatField;
    taPIt_TWFW: TFloatField;
    taPIt_TWRW: TFloatField;
    taPIt_TQ: TpFIBDataSet;
    acEvent: TActionList;
    acDelSemis: TAction;
    acAddSemis: TAction;
    acDetail: TAction;
    plInfo: TPanel;
    taPIt_TQINQ: TFIBBCDField;
    taPIt_TQGETQ: TFIBBCDField;
    taPIt_TQREWORKQ: TFIBBCDField;
    taPIt_TQDONEQ: TFIBBCDField;
    taPIt_TQRETQ: TFIBBCDField;
    taPIt_TQREJQ: TFIBBCDField;
    taPIt_TQNKQ: TFIBBCDField;
    taPIt_TQIQ: TFIBBCDField;
    taPIt_TQFQ: TFIBBCDField;
    taPIt_TQRQ: TFIBBCDField;
    TBDock1: TTBDock;
    tlbrFilter: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label4: TLabel;
    TBControlItem2: TTBControlItem;
    cmbxMat: TDBComboBoxEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure itDetailClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gridSemisSumListAfterRecalcAll(Sender: TObject);
    procedure taPIt_TQBeforeOpen(DataSet: TDataSet);
    procedure acAddSemisExecute(Sender: TObject);
    procedure acDelSemisExecute(Sender: TObject);
    procedure acDetailExecute(Sender: TObject);
    procedure cmbxMatChange(Sender: TObject);
  private
    procedure SetDetailField(bDetail : boolean);
  end;

var
  fmProtItem: TfmProtItem;

implementation

uses MainData, dbUtil, DictData, ProtSJInfo, fmUtils, dbTree;

{$R *.dfm}

procedure TfmProtItem.FormCreate(Sender: TObject);
var
  SaveEvent : TNotifyEvent;
begin
  if dm.tr.Active then dm.tr.CommitRetaining
  else dm.tr.StartTransaction;
  // ����������
  dmMain.FilterMatId := ROOT_MAT_CODE;
  SaveEvent := cmbxMat.OnChange;
  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMat);
  cmbxMat.ItemIndex := 0;
  dmMain.FilterMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := SaveEvent;
  OpenDataSets([dm.taSemis, dmMain.taPIt_InSemis_d]);
  //ppSemis.Skin := dm.ppSkin;
  plInfo.Caption := '����� ��������: '+dmMain.taProtocol_dPSNAME.AsString+'   '+
    '��������: '+dmMain.taProtocol_dOPERNAME.AsString;
end;

procedure TfmProtItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PostDataSet(dmMain.taPIt_InSemis_d);
  CloseDataSets([dm.taSemis, dmMain.taPIt_InSemis_d]);
  if dm.tr.Active then dm.tr.CommitRetaining;
  inherited;
end;

procedure TfmProtItem.itDetailClick(Sender: TObject);
begin
  SetDetailField(not itDetail.Checked);
end;

procedure TfmProtItem.FormShow(Sender: TObject);
begin
  SetDetailField(False);
end;

procedure TfmProtItem.SetDetailField(bDetail: boolean);
begin
  gridSemis.FieldColumns['INQ'].Visible := bDetail;
  gridSemis.FieldColumns['INW'].Visible := bDetail;
  gridSemis.FieldColumns['GETW'].Visible := bDetail;
  gridSemis.FieldColumns['GETQ'].Visible := bDetail;
  gridSemis.FieldColumns['REWORKW'].Visible := bDetail;
  gridSemis.FieldColumns['REWORKQ'].Visible := bDetail;
  gridSemis.FieldColumns['DONEW'].Visible := bDetail;
  gridSemis.FieldColumns['DONEQ'].Visible := bDetail;
  gridSemis.FieldColumns['RETW'].Visible := bDetail;
  gridSemis.FieldColumns['RETQ'].Visible := bDetail;
  gridSemis.FieldColumns['REJW'].Visible := bDetail;
  gridSemis.FieldColumns['REJQ'].Visible := bDetail;
  gridSemis.FieldColumns['NKW'].Visible := bDetail;
  gridSemis.FieldColumns['NKQ'].Visible := bDetail;
  itDetail.Checked := bDetail;
end;

procedure TfmProtItem.gridSemisSumListAfterRecalcAll(Sender: TObject);
begin
  OpenDataSet(taPIt_TW);
  try
    gridSemis.FieldColumns['INW'].Footers[0].Value     := FormatFloat('0.###', taPIt_TWINW.AsFloat);
    gridSemis.FieldColumns['GETW'].Footers[0].Value    := FormatFloat('0.###', taPIt_TWGETW.AsFloat);
    gridSemis.FieldColumns['REWORKW'].Footers[0].Value := FormatFloat('0.###', taPIt_TWREWORKW.AsFloat);
    gridSemis.FieldColumns['DONEW'].Footers[0].Value   := FormatFloat('0.###', taPIt_TWDONEW.AsFloat);
    gridSemis.FieldColumns['RETW'].Footers[0].Value    := FormatFloat('0.###', taPIt_TWRETW.AsFloat);
    gridSemis.FieldColumns['REJW'].Footers[0].Value    := FormatFloat('0.###', taPIt_TWREJW.AsFloat);
    gridSemis.FieldColumns['NKW'].Footers[0].Value     := FormatFloat('0.###', taPIt_TWNKW.AsFloat);
    gridSemis.FieldColumns['W'].Footers[0].Value       := FormatFloat('0.###', taPIt_TWIW.AsFloat + taPIt_TWINW.AsFloat);
    gridSemis.FieldColumns['FW'].Footers[0].Value      := FormatFloat('0.###', taPIt_TWFW.AsFloat);
    gridSemis.FieldColumns['RW'].Footers[0].Value      := FormatFloat('0.###', taPIt_TWIW.AsFloat  + taPIt_TWINW.AsFloat - taPIt_TWFW.AsFloat);
  except
  end;
  CloseDataSet(taPIt_TW);

  OpenDataSet(taPIt_TQ);
  try
    gridSemis.FieldColumns['INQ'].Footers[0].Value     := taPIt_TQINQ.AsString;
    gridSemis.FieldColumns['GETQ'].Footers[0].Value    := taPIt_TQGETQ.AsString;
    gridSemis.FieldColumns['REWORKQ'].Footers[0].Value := taPIt_TQREWORKQ.AsString;
    gridSemis.FieldColumns['DONEQ'].Footers[0].Value   := taPIt_TQDONEQ.AsString;
    gridSemis.FieldColumns['RETQ'].Footers[0].Value    := taPIt_TQRETQ.AsString;
    gridSemis.FieldColumns['REJQ'].Footers[0].Value    := taPIt_TQREJQ.AsString;
    gridSemis.FieldColumns['NKQ'].Footers[0].Value     := taPIt_TQNKQ.AsString;
    gridSemis.FieldColumns['Q'].Footers[0].Value       := IntToStr(taPIt_TQIQ.AsInteger + taPIt_TQINQ.AsInteger) ;
    gridSemis.FieldColumns['FQ'].Footers[0].Value      := taPIt_TQFQ.AsString;
    gridSemis.FieldColumns['RQ'].Footers[0].Value      := IntToStr(taPIt_TQIQ.AsInteger + taPIt_TQINQ.AsInteger - taPIt_TQFQ.AsInteger);
  except
  end;
  CloseDataSet(taPIt_TQ);
end;

procedure TfmProtItem.taPIt_TQBeforeOpen(DataSet: TDataSet);
begin
  with TpFIbDataSet(DataSet).Params do
  begin
    ByName['ID'].AsInteger := dmMain.taProtocol_dID.AsInteger;
    if(dmMain.FilterMatId = ROOT_MAT_CODE)then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := dmMain.FilterMatId;
      ByName['MATID2'].AsString := dmMain.FilterMatId;
    end;
  end;
end;


procedure TfmProtItem.acAddSemisExecute(Sender: TObject);
begin
  dmMain.taPIt_InSemis_d.Insert;
end;

procedure TfmProtItem.acDelSemisExecute(Sender: TObject);
begin
  dmMain.taPIt_InSemis_d.Delete;
end;

procedure TfmProtItem.acDetailExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmProtSJInfo, TForm(fmProtSJInfo));
end;

procedure TfmProtItem.cmbxMatChange(Sender: TObject);
begin
  dmMain.FilterMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taPIt_InSemis_d);
end;

end.
