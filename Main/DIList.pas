{***********************************************}
{  ���������� ����������� ��������������        }
{  Copyrigth (C) 2002-2003 basile for CDM       }
{  v1.17                                        }
{  last update 24.12.2003                       }
{***********************************************}

unit DIList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, Menus,  ImgList,  StdCtrls,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, ComCtrls, db,
  DBGridEh, ActnList, TB2Item, DBGridEhGrouping, rxPlacemnt,
  GridsEh, rxSpeedbar;

type
  TfmDIList = class(TfmListAncestor)
    ppDepFrom: TPopupMenu;
    mnitAllDepFrom: TMenuItem;
    mnitSepDepFrom: TMenuItem;
    lbDepFrom: TLabel;
    spitDepFrom: TSpeedItem;
    ppPrint: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure acPrintDocUpdate(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure N2Click(Sender: TObject);
  protected
    procedure DepFromClick(Sender : TObject);
    procedure DepClick(Sender : TObject);override; // ����
  end;

var
  fmDIList: TfmDIList;

implementation

uses MainData, DIEl, DictData, dbUtil, fmUtils, PrintData, MsgDialog, SIList, PrintData2;

{$R *.dfm}

{ TfmDIList }

procedure TfmDIList.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  inherited;
end;

procedure TfmDIList.DepFromClick(Sender: TObject);
begin
  dm.CurrFromDep := TMenuItem(Sender).Tag;
  lbDepFrom.Caption := TMenuItem(Sender).Caption;
  ReOpenDataSet(DataSet);
end;

procedure TfmDIList.FormCreate(Sender: TObject);
var
  i, j1, j2 : integer;
  Item : TMenuItem;
begin
  dm.CurrDep := -1;
  dm.CurrFromDep := dm.User.wWhId;
  mnitAllDepFrom.OnClick := DepFromClick;
  lbDepFrom.Caption := mnitAllDepFrom.Caption;

  inherited;

  //���������� PopupMenu �� FDepInfo;
  j1 := 0; j2 := 0;
  for i:=0 to Pred(dm.CountDep) do
  begin
    (* �����-����������� *)
    if (dm.DepInfo[i].DepTypes and $2)=$2 then
    begin
      Item := TMenuItem.Create(ppDepFrom);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepFromClick;
      Item.RadioItem := True;
      if Item.Tag = dm.User.wWhId then lbDepFrom.Caption := dm.DepInfo[i].SName;
      inc(j1);
      if j1>30 then
      begin
        Item.Break := mbBreak;
        j1 := 0;
      end;
      ppDepFrom.Items.Add(Item);
    end;
    (* �����-���������� *)
    if (dm.DepInfo[i].DepTypes and $2)=$2 then
    begin
      Item := TMenuItem.Create(ppDep);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepClick;
      Item.RadioItem := True;
      inc(j2);
      if j2>30 then
      begin
        Item.Break := mbBreak;
        j2:=0;
      end;
      ppDep.Items.Add(Item);
    end
  end
end;


procedure TfmDIList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmMain.taDIList);
  if dm.tr.Active then dm.tr.Commit;
end;

procedure TfmDIList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if dmMain.taDIListISCLOSE.AsInteger=0 then Background:=clWindow else Background:=clBtnFace;
  if(Column.FieldName = 'DOCNO')then
    if not dmMain.taDIListCOLOR.IsNull then Background := dmMain.taDIListCOLOR.AsInteger;
end;

procedure TfmDIList.N2Click(Sender: TObject);
var Invs: String;
       i: integer;
begin
inherited;
    if (gridDocList.SelectedRows.Count > 1) then
    begin
      gridDocList.DataSource.dataSet.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[0]));
      Invs := gridDocList.DataSource.dataSet.FieldByName('INVID').AsString;
      dmPrint2.InvNumbers := gridDocList.DataSource.dataSet.FieldByName('DOCNO').AsString;
      for i := 1 to gridDocList.SelectedRows.Count - 1 do
        begin
          gridDocList.DataSource.dataSet.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[i]));
          Invs := Invs + ', ' + gridDocList.DataSource.dataSet.FieldByName('INVID').AsString;
          dmPrint2.InvNumbers := dmPrint2.InvNumbers + ', ' + gridDocList.DataSource.dataSet.FieldByName('DOCNO').AsString;
        end;
    end
  else
    begin
      Invs := gridDocList.DataSource.dataSet.FieldByName('INVID').AsString;
      dmPrint2.InvNumbers := gridDocList.DataSource.dataSet.FieldByName('DOCNO').AsString;
    end;
dmPrint2.SListToPrint(Invs);
end;

procedure TfmDIList.acAddDocExecute(Sender: TObject);
begin
  if(dm.CurrDep = -1)then
  begin
    MessageDialog('���������� ������� �����-����������', mtWarning, [mbOK], 0);
    Abort;
  end;
  if(dm.CurrFromDep = -1)then
  begin
    MessageDialog('���������� ������� �����-�����������', mtWarning, [mbOK], 0);
    Abort;
  end;
  if(dm.CurrDep = dm.CurrFromDep) then
  begin
    MessageDialog('�����-����������� � �����-���������� ���������', mtWarning, [mbOK], 0);
    Abort;
  end;
  inherited;
  with dmMain do
    ShowAndFreeForm(TfmDIEl, TForm(fmDIEl));
  RefreshDataSet(dmMain.taDIList);
end;

procedure TfmDIList.acEditDocExecute(Sender: TObject);
var
  SaveCurrFromDep, SaveCurrDep : integer;
begin
  if dmMain.taDIList.IsEmpty then eXit;
  SaveCurrFromDep := dm.CurrFromDep;
  SaveCurrDep := dm.CurrDep;
  try
    dm.CurrDep := dmMain.taDIListDEPID.AsInteger;
    dm.CurrFromDep := dmMain.taDIListFROMDEPID.AsInteger;
    ShowAndFreeForm(TfmDIEl, TForm(fmDIEl));
  finally
    dm.CurrDep := SaveCurrDep;
    dm.CurrFromDep := SaveCurrFromDep;
  end;
  RefreshDataSet(dmMain.taDIList);
end;

procedure TfmDIList.acPrintDocExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkWHSemis_Transfer, VarArrayOf([VarArrayOf([dmMain.taDIListINVID.AsInteger]),VarArrayOf([dmMain.taDIListINVID.AsInteger])]));
end;

procedure TfmDIList.acPrintDocUpdate(Sender: TObject);
begin
  acPrintDoc.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

end.
