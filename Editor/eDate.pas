unit eDate;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, Menus, StdCtrls, cxButtons, cxLabel, cxTextEdit,
  cxMaskEdit, cxDropDownEdit, cxCalendar;

type
  TfmEDate = class(TForm)
    DateEdit: TcxDateEdit;
    cxLabel: TcxLabel;
    bContinue: TcxButton;
    bCancel: TcxButton;
  private

  public
    class function Execute: Variant;
  end;

var
  fmEDate: TfmEDate;

implementation

{$R *.dfm}


class function TfmEDate.Execute: Variant;
var
  Answer: TModalResult;
begin

  fmEDate := nil;

  try

    fmEDate := TfmEDate.Create(nil);

    fmEDate.DateEdit.Date := Date;

    Answer := fmEDate.ShowModal;

    if Answer = mrCancel then
    begin
      Result := null;
    end else
    begin
      Result := fmEDate.DateEdit.Date;
    end;

  finally

    FreeAndNil(fmEdate);

  end;

end;

end.
