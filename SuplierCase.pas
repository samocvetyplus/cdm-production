unit SuplierCase;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGridEhGrouping, StdCtrls, Buttons, GridsEh, DBGridEh, DB,
  FIBDataSet, pFIBDataSet;

type
  TfmSuplierCase = class(TForm)
    taSuplierCase: TpFIBDataSet;
    dsSuplierCase: TDataSource;
    DBGridEh1: TDBGridEh;
    btnCansel: TBitBtn;
    btnOK: TBitBtn;
    Label1: TLabel;
    fEdit: TEdit;
    procedure fEditChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSuplierCase: TfmSuplierCase;


implementation


uses MainData, DictData, dbUtil, fmUtils;

{$R *.dfm}


procedure TfmSuplierCase.fEditChange(Sender: TObject);
var tmpstr: string[70];
begin
tmpstr:=AnsiUpperCase(trim(fEdit.Text));
  with taSupliercase do
    begin
     Close;
     with sqls do
       begin
         SelectSQL.Clear;
         SelectSQL.text:='select compid, name from d_comp where upper(name) like ''%'+tmpstr+'%''';
       end;
     Open;
  end;

end;

procedure TfmSuplierCase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if fmSuplierCase.ModalResult=mrOk
  then
    try
      dmMain.taSOList.Edit;
      dmMain.taSOList.FieldByName('SUPID').AsInteger:=taSuplierCase.FieldByName('COMPID').AsInteger;
      dmMain.taSOList.Post;
    except
      on E: Exception do
        ShowMessage(E.Message);
    end;
    dmMain.taSOList.Refresh;
end;

procedure TfmSuplierCase.FormCreate(Sender: TObject);
begin
  taSuplierCase.Open;
end;

end.
