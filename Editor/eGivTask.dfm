inherited fmeGivTask: TfmeGivTask
  Left = 453
  Top = 197
  Caption = 'fmeGivTask'
  ClientHeight = 125
  ClientWidth = 299
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 299
    Height = 125
    object Label1: TLabel [0]
      Left = 8
      Top = 11
      Width = 29
      Height = 13
      Caption = #1044#1072#1090#1072' '
    end
    object Label2: TLabel [1]
      Left = 8
      Top = 35
      Width = 67
      Height = 13
      Caption = #1048#1089#1087#1086#1083#1085#1080#1090#1077#1083#1100
    end
    object Label3: TLabel [2]
      Left = 8
      Top = 59
      Width = 50
      Height = 13
      Caption = #1054#1087#1077#1088#1072#1094#1080#1103
    end
    inherited btnOk: TBitBtn
      Left = 116
      Top = 90
      Anchors = [akRight, akBottom]
    end
    inherited btnCancel: TBitBtn
      Left = 208
      Top = 90
      Anchors = [akRight, akBottom]
    end
    object edDate: TDBDateTimeEditEh
      Left = 88
      Top = 8
      Width = 87
      Height = 19
      AlwaysShowBorder = True
      EditButton.Style = ebsEllipsisEh
      EditButtons = <>
      Flat = True
      Kind = dtkDateTimeEh
      TabOrder = 2
      Visible = True
    end
    object edDeps: TDBLookupComboboxEh
      Left = 88
      Top = 32
      Width = 201
      Height = 19
      AlwaysShowBorder = True
      EditButtons = <>
      Flat = True
      KeyField = 'DEPID'
      ListField = 'NAME'
      ListSource = dmData.dsDeps
      TabOrder = 3
      Visible = True
    end
    object edOper: TDBLookupComboboxEh
      Left = 88
      Top = 56
      Width = 201
      Height = 19
      AlwaysShowBorder = True
      EditButtons = <>
      Flat = True
      KeyField = 'OPERID'
      ListField = 'TASKOPERNAME'
      ListSource = dmData.dsOperToGive
      TabOrder = 4
      Visible = True
    end
  end
end
