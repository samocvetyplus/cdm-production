inherited fmPriceList: TfmPriceList
  Left = 361
  Top = 153
  Width = 632
  Height = 364
  Caption = #1055#1088#1072#1081#1089#1051#1080#1089#1090
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 311
    Width = 624
  end
  inherited tb1: TSpeedBar
    Width = 624
    object Label1: TLabel [0]
      Left = 8
      Top = 2
      Width = 41
      Height = 13
      Caption = #1040#1088#1090#1080#1082#1091#1083
      Transparent = True
    end
    object Label2: TLabel [1]
      Left = 146
      Top = 3
      Width = 26
      Height = 13
      Caption = #1062#1077#1085#1072
      Transparent = True
    end
    object SpeedButton1: TSpeedButton [2]
      Left = 269
      Top = 16
      Width = 113
      Height = 21
      Action = acSetPrice
    end
    object edArt: TDBEditEh [3]
      Left = 6
      Top = 18
      Width = 121
      Height = 19
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnChange = edArtChange
    end
    object edPrice: TDBEditEh [4]
      Left = 144
      Top = 17
      Width = 121
      Height = 19
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 1
      Visible = True
    end
    inherited spitExit: TSpeedItem
      Left = 515
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 42
    Width = 624
    Height = 269
    Align = alClient
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmInv.dsrPriceList
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Color = clWhite
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Title.Alignment = taCenter
        Width = 109
      end
      item
        Color = 13103866
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'PRICE'
        Footers = <>
        Title.Alignment = taCenter
        Width = 94
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ilButtons: TImageList
    Left = 502
    Top = 44
  end
  inherited fmstr: TFormStorage
    Left = 544
    Top = 48
  end
  inherited ActionList2: TActionList
    Left = 500
    Top = 96
    object acSetPrice: TAction
      Caption = #1053#1072#1079#1085#1072#1095#1080#1090#1100' '#1094#1077#1085#1091
      OnExecute = acSetPriceExecute
    end
    object acFilter: TAction
      Caption = #1054#1090#1092#1080#1083#1100#1090#1088#1086#1074#1072#1090#1100
      OnExecute = acFilterExecute
    end
  end
end
