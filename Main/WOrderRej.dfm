inherited fmWOrderRej: TfmWOrderRej
  Left = 145
  Top = 132
  Width = 902
  Height = 626
  Align = alClient
  Caption = #1053#1072#1088#1103#1076' '#1087#1086' '#1073#1088#1072#1082#1091
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 573
    Width = 894
    Panels = <
      item
        Width = 80
      end
      item
        Width = 50
      end>
  end
  inherited tb1: TSpeedBar
    Width = 894
    inherited spitAdd: TSpeedItem [1]
      Hint = ''
      Left = 67
      Visible = False
    end
    inherited spitDel: TSpeedItem [2]
      Action = acDelSemis
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF008000000080000000800000008000
        00008000000080000000800000008000000080000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF0080000000FFFFFF00000000000000
        0000000000000000000000000000FFFFFF0080000000FF00FF00000000000000
        00000000000000000000000000000000000080000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FFFFFF00000000000000
        0000000000000000000000000000FFFFFF0080000000FF00FF0000000000FFFF
        FF000000000000000000000000000000000080000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FFFFFF00000000000000
        0000FFFFFF0080000000800000008000000080000000FF00FF0000000000FFFF
        FF000000000000000000000000000000000080000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0080000000FFFFFF0080000000FF00FF00FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080000000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF008000000080000000FF00FF00FF00FF00FF00FF0000000000FFFF
        FF000000000000000000FFFFFF00000000008000000080000000800000008000
        00008000000080000000FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF0000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000FFFF
        FF00FFFFFF00FFFFFF00FFFFFF000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000000000
        000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      Hint = ''
      Left = 131
      Visible = False
      OnClick = acDelSemisExecute
    end
    inherited spitPrint: TSpeedItem [3]
      Tag = 1
      Action = acPrint
      DropDownMenu = nil
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00FF000000
        0000C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
        C600C6C6C60000000000C6C6C60000000000FF00FF00FF00FF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000C6C6C60000000000FF00FF0000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600FFFFFF00FFFFFF00FFFF
        FF00C6C6C600C6C6C600000000000000000000000000FF00FF0000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C6000000FF000000FF000000
        FF00C6C6C600C6C6C60000000000C6C6C60000000000FF00FF00000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000C6C6C600C6C6C6000000000000000000C6C6
        C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
        C600C6C6C60000000000C6C6C60000000000C6C6C60000000000FF00FF000000
        0000000000000000000000000000000000000000000000000000000000000000
        000000000000C6C6C60000000000C6C6C6000000000000000000FF00FF00FF00
        FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF0000000000C6C6C60000000000C6C6C60000000000FF00FF00FF00
        FF00FF00FF0000000000FFFFFF00FF000000FF000000FF000000FF000000FF00
        0000FFFFFF0000000000000000000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000FFFFFF00FF000000FF000000FF000000FF00
        0000FF000000FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF0000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00000000000000000000000000000000000000
        000000000000000000000000000000000000FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      Hint = ''
      Left = 67
      OnClick = acPrintExecute
    end
    inherited spitClose: TSpeedItem [4]
      Left = 131
    end
    inherited spitExit: TSpeedItem [5]
      Left = 651
    end
    object spitCalcLoss: TSpeedItem
      BtnCaption = #1056#1072#1089#1095#1077#1090
      Caption = #1056#1072#1089#1095#1077#1090
      Hint = #1056#1072#1089#1095#1077#1090' '#1089#1098#1105#1084#1072' '#1080' '#1085#1086#1088#1084' '#1087#1086#1090#1077#1088#1100'|'
      ImageIndex = 8
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = spitSumClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1053#1086#1074#1072#1103
      Caption = #1053#1086#1074#1072#1103
      ImageIndex = 9
      Spacing = 1
      Left = 3
      Top = 3
      SectionName = 'Untitled (0)'
    end
  end
  inherited plMain: TPanel
    Width = 894
    Height = 171
    Anchors = [akTop, akRight]
    inherited lbNoDoc: TLabel
      FocusControl = edNoDoc
    end
    inherited lbDateDoc: TLabel
      Top = 28
      FocusControl = dtedDateDoc
    end
    inherited lbDep: TLabel
      Left = 312
      Visible = False
    end
    inherited txtDep: TDBText
      Left = 104
      Top = 49
      Width = 33
      Alignment = taRightJustify
      DataField = 'DEPNAME'
      Font.Color = clBlack
      Visible = False
    end
    object lbOper: TLabel [4]
      Left = 192
      Top = 50
      Width = 53
      Height = 13
      Caption = #1054#1087#1077#1088#1072#1094#1080#1103' '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbExec: TLabel [5]
      Left = 192
      Top = 28
      Width = 67
      Height = 13
      Caption = #1048#1089#1087#1086#1083#1085#1080#1090#1077#1083#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel [6]
      Left = 8
      Top = 70
      Width = 80
      Height = 13
      Caption = #1058#1077#1082'. '#1085#1072#1082#1083#1072#1076#1085#1072#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbCurrInv: TLabel [7]
      Left = 92
      Top = 70
      Width = 42
      Height = 13
      Caption = 'lbCurrInv'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object Label6: TLabel [8]
      Left = 192
      Top = 8
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtADocNo: TDBText [9]
      Left = 616
      Top = 4
      Width = 61
      Height = 17
      DataField = 'DOCNO'
      DataSource = dmMain.dsrAssortInv
    end
    object lbAInv: TLabel [10]
      Left = 468
      Top = 4
      Width = 145
      Height = 13
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072' '#8470
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbAInvFrom: TLabel [11]
      Left = 680
      Top = 4
      Width = 11
      Height = 13
      Caption = #1086#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtADocDt: TDBText [12]
      Left = 693
      Top = 4
      Width = 66
      Height = 14
      DataField = 'DOCDATE'
      DataSource = dmMain.dsrAssortInv
    end
    object lbDate: TLabel [13]
      Left = 8
      Top = 48
      Width = 40
      Height = 13
      Caption = #1053#1072#1095#1072#1083#1086':'
    end
    object txtDefOper: TDBText [14]
      Left = 276
      Top = 48
      Width = 165
      Height = 17
      DataField = 'DEFOPERNAME'
      DataSource = dmMain.dsrWOrderList
    end
    object txtExec: TDBText [15]
      Left = 276
      Top = 28
      Width = 165
      Height = 17
      DataField = 'JOBFACE'
      DataSource = dmMain.dsrWOrderList
    end
    object txtJobPS: TDBText [16]
      Left = 276
      Top = 8
      Width = 165
      Height = 17
      DataField = 'JOBDEPNAME'
      DataSource = dmMain.dsrWOrderList
    end
    object Label2: TLabel [17]
      Left = 8
      Top = 84
      Width = 106
      Height = 13
      Caption = #1058#1077#1082'. '#1084#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbCurrPS: TLabel [18]
      Left = 120
      Top = 84
      Width = 41
      Height = 13
      Caption = 'lbCurrPS'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object M207Bevel1: TM207Bevel [19]
      Left = 4
      Top = 66
      Width = 425
      Height = 2
      Shape = bsTopLine
      CaptionOffs = 6
      BorderWidth = 0
    end
    inherited edNoDoc: TDBEditEh
      DataField = 'DOCNO'
    end
    inherited dtedDateDoc: TDBDateTimeEditEh
      DataField = 'DOCDATE'
    end
    object tlbrT: TTBToolbar
      Left = 5
      Top = 148
      Width = 331
      Height = 19
      ProcessShortCuts = True
      TabOrder = 2
      object it0: TTBItem
        Action = ac0
        GroupIndex = 1
      end
      object it6: TTBItem
        Action = ac6
        GroupIndex = 1
      end
      object TBSeparatorItem1: TTBSeparatorItem
      end
      object it1: TTBItem
        Action = ac1
        GroupIndex = 1
      end
      object it3: TTBItem
        Action = ac3
        GroupIndex = 1
      end
      object it5: TTBItem
        Action = ac5
        GroupIndex = 1
      end
      object it7: TTBItem
        Action = ac7
        GroupIndex = 1
      end
    end
    object lcbxJobPS: TDBLookupComboboxEh
      Left = 276
      Top = 26
      Width = 149
      Height = 19
      DataField = 'JOBDEPID'
      DataSource = dmMain.dsrWOrderList
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      DropDownBox.Rows = 15
      EditButtons = <>
      Flat = True
      KeyField = 'DEPID'
      ListField = 'NAME'
      ListSource = dmMain.dsrPsDep
      TabOrder = 3
      Visible = True
      OnCloseUp = lcbxJobPSCloseUp
      OnKeyValueChanged = lcbxJobPSKeyValueChanged
    end
    object lcbxExec: TDBLookupComboboxEh
      Left = 276
      Top = 6
      Width = 149
      Height = 19
      DataField = 'JOBID'
      DataSource = dmMain.dsrWOrderList
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      DropDownBox.ListSource = dmMain.dsrPsMol
      DropDownBox.AutoDrop = True
      EditButtons = <>
      Flat = True
      KeyField = 'MOLID'
      ListField = 'FIO'
      ListSource = dmMain.dsrPsMol
      TabOrder = 4
      Visible = True
      OnEnter = lcbxExecEnter
    end
    object lcbxOper: TDBLookupComboboxEh
      Left = 276
      Top = 46
      Width = 149
      Height = 19
      DataField = 'DEFOPER'
      DataSource = dmMain.dsrWOrderList
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      DropDownBox.AutoDrop = True
      DropDownBox.Rows = 15
      EditButtons = <>
      Flat = True
      KeyField = 'OPERID'
      ListField = 'OPERATION'
      ListSource = dmMain.dsrPsOper
      TabOrder = 5
      Visible = True
      OnEnter = lcbxOperEnter
    end
    object gridAEl: TDBGridEh
      Left = 444
      Top = 20
      Width = 435
      Height = 145
      AllowedOperations = []
      AllowedSelections = []
      Anchors = [akLeft, akTop, akRight]
      BorderStyle = bsNone
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dmMain.dsrAssortEl
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterRowCount = 1
      FrozenCols = 1
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFrozen3D, dghFooter3D, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghColumnResize, dghColumnMove]
      PopupMenu = ppA
      ReadOnly = True
      RowDetailPanel.Color = clBtnFace
      SumList.Active = True
      TabOrder = 6
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnGetCellParams = gridAElGetCellParams
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footer.ValueType = fvtCount
          Footers = <>
          Title.EndEllipsis = True
          Width = 22
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clNavy
          Footer.Font.Height = -11
          Footer.Font.Name = 'MS Sans Serif'
          Footer.Font.Style = []
          Footer.Value = #1048#1090#1086#1075#1086' '#1096#1090'.'
          Footer.ValueType = fvtStaticText
          Footers = <>
          Title.EndEllipsis = True
          Width = 49
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART2'
          Footers = <>
          Title.EndEllipsis = True
          Width = 39
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SZ'
          Footers = <>
          Title.EndEllipsis = True
          Width = 46
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'Q'
          Footer.FieldName = 'Q1'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clNavy
          Footer.Font.Height = -11
          Footer.Font.Name = 'MS Sans Serif'
          Footer.Font.Style = []
          Footer.ValueType = fvtSum
          Footers = <>
          Title.EndEllipsis = True
          Width = 61
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERATION'
          Footers = <>
          Title.EndEllipsis = True
          Width = 69
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'U'
          Footers = <>
          Title.EndEllipsis = True
          Width = 46
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object plExt: TPanel [3]
    Left = 0
    Top = 213
    Width = 289
    Height = 360
    Align = alLeft
    BevelInner = bvLowered
    TabOrder = 3
    object plExtTiitle: TPanel
      Left = 2
      Top = 2
      Width = 285
      Height = 43
      Align = alTop
      BevelOuter = bvNone
      Constraints.MinHeight = 43
      Constraints.MinWidth = 273
      TabOrder = 0
      object lbMat: TLabel
        Left = 3
        Top = 20
        Width = 23
        Height = 13
        Caption = #1052#1072#1090'.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbWhSemisName: TLabel
        Left = 3
        Top = 1
        Width = 52
        Height = 13
        Caption = #1050#1083#1072#1076#1086#1074#1072#1103':'
      end
      object cmbxMat: TDBComboBoxEh
        Left = 32
        Top = 18
        Width = 97
        Height = 19
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Width = -1
        EditButtons = <>
        Flat = True
        TabOrder = 0
        Visible = True
        OnChange = cmbxMatChange
      end
      object chbxW: TDBCheckBoxEh
        Left = 232
        Top = 0
        Width = 49
        Height = 17
        Caption = #1042#1077#1089#1099
        Flat = True
        TabOrder = 1
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
    end
    object gridSemis: TDBGridEh
      Left = 2
      Top = 45
      Width = 285
      Height = 313
      Align = alClient
      AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
      Color = clWhite
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dmMain.dsrWhSemis
      Flat = True
      FooterColor = clBtnFace
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      FooterRowCount = 1
      Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      ReadOnly = True
      RowDetailPanel.Color = clBtnFace
      RowLines = 2
      RowSizingAllowed = True
      SumList.Active = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      OnKeyDown = gridSemisKeyDown
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'DEPNAME'
          Footers = <>
          Width = 90
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SEMISNAME'
          Footers = <>
          Width = 99
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'Q'
          Footer.FieldName = 'Q'
          Footer.ValueType = fvtSum
          Footers = <>
          Width = 38
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'W'
          Footer.FieldName = 'W'
          Footer.ValueType = fvtSum
          Footers = <>
          Width = 60
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERNAME'
          Footers = <>
          Width = 82
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'UQ'
          Footers = <>
          Title.Caption = #1045#1076'. '#1080#1079#1084'|'#1082#1086#1083'-'#1074#1072
          Width = 43
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'UW'
          Footers = <>
          Title.Caption = #1045#1076'. '#1080#1079#1084'|'#1074#1077#1089#1072
          Width = 39
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object tbdcSemis: TTBDock [4]
    Left = 289
    Top = 213
    Width = 68
    Height = 360
    Position = dpLeft
    object TBToolbar2: TTBToolbar
      Left = 0
      Top = 0
      Align = alLeft
      DockMode = dmCannotFloat
      DockPos = 0
      Images = ilButtons
      Options = [tboImageAboveCaption]
      TabOrder = 0
      object TBItem2: TTBItem
        Action = acAddSemis
      end
      object TBItem4: TTBItem
        Action = acAddSemisNew
      end
      object TBItem5: TTBItem
        Action = acDelSemis
        Caption = ' '#1059#1076#1072#1083#1080#1090#1100' '
      end
      object TBItem1: TTBItem
        Action = acTransform0
        Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      end
      object TBItem3: TTBItem
        Action = acTransformAssort
        Caption = #1040#1089#1089#1086#1088#1090#1080#1084
      end
    end
  end
  inherited gridItem: TDBGridEh
    Left = 357
    Top = 213
    Width = 537
    Height = 360
    AllowedOperations = [alopUpdateEh]
    AllowedSelections = []
    DataSource = dmMain.dsrWOrder
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    PopupMenu = ppInv
    TabOrder = 5
    UseMultiTitle = True
    OnExit = gridItemExit
    OnGetCellParams = gridItemGetCellParams
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RecNo'
        Footers = <>
        Width = 22
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1053#1086#1084#1077#1088
        Title.EndEllipsis = True
        Width = 40
      end
      item
        DisplayFormat = 'd.mm.yy'
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1044#1072#1090#1072
        Title.EndEllipsis = True
        Width = 36
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ADOCNO'
        Footers = <>
        Title.EndEllipsis = True
        Width = 48
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OPERNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 73
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ITTYPE'
        Footers = <>
        Title.EndEllipsis = True
        Width = 60
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        DropDownRows = 20
        EditButtons = <>
        FieldName = 'SEMISNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 82
      end
      item
        ButtonStyle = cbsEllipsis
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footers = <>
        Title.EndEllipsis = True
        Width = 43
        OnEditButtonClick = gridItemColumns7EditButtonClick
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Title.EndEllipsis = True
        Width = 39
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
        Title.Caption = #1042#1099#1076#1072#1085#1086'|'#1042#1099#1076#1072#1083
        Title.EndEllipsis = True
        Width = 66
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Title.Caption = #1042#1099#1076#1072#1085#1086'|'#1052#1061'1'
        Title.EndEllipsis = True
        Width = 56
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'JOBNAME'
        Footers = <>
        Title.Caption = #1055#1086#1083#1091#1095#1077#1085#1086'|'#1055#1086#1083#1091#1095#1080#1083
        Title.EndEllipsis = True
        Width = 61
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'JOBDEPNAME'
        Footers = <>
        Title.Caption = #1055#1086#1083#1091#1095#1077#1085#1086'|'#1052#1061'2'
        Title.EndEllipsis = True
        Width = 49
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FROMOPERNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 67
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INVID'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WOITEMID'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WORDERID'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ITDATE'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1042#1099#1076#1072#1085#1086
        Title.EndEllipsis = True
        Visible = False
        Width = 66
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REJPSNAME'
        Footers = <>
        Title.Caption = #1041#1088#1072#1082'|'#1042#1080#1085#1086#1074#1085#1080#1082
        Title.EndEllipsis = True
        Width = 73
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REJNAME'
        Footers = <>
        Title.Caption = #1041#1088#1072#1082'|'#1055#1088#1080#1095#1080#1085#1072
        Width = 58
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REJREF'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FULLART'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
        Width = 50
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REF'
        Footers = <>
        Visible = False
      end>
  end
  inherited ilButtons: TImageList
    Left = 362
    Top = 300
    Bitmap = {
      494C010110001300040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000005000000001002000000000000050
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008484000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000084840000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000848400000000000000000000000000FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000848400FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00BDBDBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000848400FFFFFF0000FFFF0000000000008484000084
      8400008484000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000084840000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      84000000000000000000000000000000000000FFFF00FFFFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF00000000000000000000000000000000000084
      8400FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF000000000000000000000000000000000000000000FF000000FF000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000848400FFFFFF0000FFFF00FFFFFF0000FFFF0000000000008484000084
      8400008484000000000000000000000000000000000000000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000084840000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      000000000000000000000000000000000000000000000000000000000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B0000FFFF007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000848400FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FF
      FF0000000000000000000000000000000000000000000000FF000000FF000000
      FF00000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000848400FFFFFF0000FFFF00FFFFFF0000FFFF00FFFF
      FF0000FFFF00000000000000000000000000000000000000FF000000FF000000
      FF0000000000000000007B7B7B0000FFFF007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000848400008484000084840000848400008484000084
      840000848400008484000084840000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B58C8C008C5A5A008C5A5A008C5A5A008C5A5A008C5A5A008C5A
      5A008C5A5A008C5A5A008C5A5A0000000000000000000000000000000000299C
      DE00299CDE00A57B7300A57B7300A57B7300A57B7300A57B7300A57B7300A57B
      7300A57B7300A57B7300A57B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      000000000000B58C8C00FFFFEF00FFF7E700FFF7DE00F7EFDE00F7EFDE00F7EF
      DE00FFEFDE00F7E7D6008C5A5A00000000000000000000000000299CDE008CD6
      EF0084D6F700CEC6BD00FFEFDE00FFF7E700FFF7E700F7EFDE00F7EFDE00F7EF
      DE00F7EFDE00FFEFDE00A57B73000000000000000000AD632900AD632900AD63
      2900AD632900AD632900AD632900AD632900AD632900AD632900AD632900AD63
      2900AD632900AD632900AD632900000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      000000000000B58C8C00FFF7E700F7E7CE00F7E7CE00F7DECE00F7DEC600F7DE
      C600F7E7CE00EFDECE008C5A5A000000000000000000299CDE00A5EFFF0094F7
      FF008CF7FF00CEC6BD00F7E7DE00F7E7D600F7DEC600F7DEC600F7DEC600F7DE
      BD00F7DEC600F7E7D600A57B730000000000DEA53900FFD69400EFB57300E7AD
      6300FFBD6300F7AD4A00DE9C3900FFAD3100FFAD2100FF9C1000FF9C0000FF9C
      0000FF9C0000FF9C0000FF9C0000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000000000000000000000
      000000000000B58C8C00FFF7E700FFD6A500FFD6A500FFD6A500FFD6A500FFD6
      A500FFD6A500EFDECE008C5A5A000000000000000000299CDE00A5E7FF0094EF
      FF0084EFFF00CEC6BD00FFEFDE00FFE7CE00FFDEBD00FFDEBD00FFDEBD00FFDE
      BD00F7DEC600F7E7D600A57B730000000000E7AD1800EFC694005A4A4200524A
      4200D69C5A00846B420042393900BD843900D694290052423100524229005242
      310052422900634A2900F7940000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FF000000FF000000FF
      000000FF000000FF000000FF00000000000000000000B58C8C008C5A5A008C5A
      5A008C5A5A00B58C8C00FFF7EF00FFE7CE00FFE7C600FFDEC600FFDEC600F7DE
      BD00F7E7D600EFDECE009C6B63000000000000000000299CDE00ADEFFF00A5F7
      FF0094F7FF00CEC6BD00FFEFE700FFE7D600FFDEC600FFDEC600FFDEC600F7DE
      BD00F7DEC600F7E7D600A57B730000000000E7AD1800FFD6A500CEA57300C694
      6B00FFBD6B00D69C5A00BD8C4200EFA54200EFA54200AD7B5200C6843900E78C
      1000B57B3100BD7B3100FF9C0000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000FF
      000000FF000000000000000000000000000000000000B58C8C00FFFFEF00FFF7
      E700FFF7DE00B58C8C00FFFFF700FFE7CE00FFE7CE00FFE7C600FFDEC600FFDE
      C600F7E7D600EFE7D6009C6B6B000000000000000000299CDE00B5EFFF00ADF7
      FF00A5F7FF00CEC6BD00FFEFEF00FFEFDE00FFE7D600FFE7CE00FFE7CE00FFE7
      CE00F7E7D600F7EFDE00A57B730000000000E7AD1800EFC69C0063524A005A52
      4200D6A56B008C734A004A423900B5843900947B94002942DE006B63A500CE8C
      4A003142D600314ACE00EF941000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FF
      000000FF000000000000000000000000000000000000B58C8C00FFF7E700F7E7
      CE00F7E7CE00B58C8C00FFFFF700FFD6A500FFD6A500FFD6A500FFD6A500FFD6
      A500FFD6A500F7E7DE00A57B73000000000000000000299CDE00BDEFFF00BDF7
      FF00B5F7FF00CEC6BD00FFF7F700FFEFD600FFDEBD00FFDEBD00FFDEBD00FFDE
      B500FFE7CE00F7EFE700A57B730000000000E7AD1800FFD6A500BD9C7B00B594
      6B00FFC67B00D6A56300A57B4A00EFAD4A00EFA552008C739400C68C5200FFA5
      10009C736B00A5735A00FF9C0000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      00000000000000000000000000000000000000000000B58C8C00FFF7E700FFD6
      A500FFD6A500B58C8C00FFFFFF00FFEFDE00FFE7D600FFE7D600FFE7D600FFEF
      D600FFF7E700EFE7DE00A57B73000000000000000000299CDE00C6EFFF00CEF7
      FF00BDF7FF00CEC6BD00FFF7F700FFF7F700FFF7E700FFEFE700FFEFE700FFEF
      E700FFF7E700EFE7DE00A57B730000000000E7AD1800EFBD9400635A4A005A52
      4A00CEA573008C73520042423900B5844A009C8494002139E7006B63A500CE94
      4A00314AD600314ACE00E7941800AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000B58C8C00FFF7EF00FFE7
      CE00FFE7C600B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFDE
      DE00D6C6C600BDADAD00B58473000000000000000000299CDE00CEEFFF00DEFF
      FF00CEFFFF00CEC6BD00FFFFF700FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7E7
      E700D6BDB500C6ADA500A57B730000000000E7AD1800FFD6A500FFC69400F7C6
      9400FFCE9400FFC67B00EFB56B00FFBD6300FFB54A00E7A55A00FFAD3900FFAD
      2100EF9C2900F79C1800FF9C0000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000B58C8C00FFFFF700FFE7
      CE00FFE7CE00B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B58C
      8C00B58C8C00B58C8C00B58C8C000000000000000000299CDE00D6F7FF00EFFF
      FF00DEFFFF00CEC6BD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00DECE
      C600E7AD7300F7945A000000000000000000E7AD1800F7CEA500E7DEC600E7E7
      C600DEDEBD00E7DEBD00E7DEB500DED6AD00E7D69C00FFBD5A00FFAD3900F7A5
      3100F79C2100FF9C1000FF9C0000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF000000000000000000000000000000000000000000B58C8C00FFFFF700FFD6
      A500FFD6A500B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B58C
      8C00EFB56B00C68C7B00000000000000000000000000299CDE00DEF7FF00FFFF
      FF00EFFFFF00CEC6BD00FFF7EF00FFF7F700FFF7F700FFF7F700FFF7F700E7C6
      BD00C6AD8C00299CDE000000000000000000E7AD1800D6AD8C006BD6CE006BD6
      CE006BD6CE006BD6CE006BD6CE0063D6D6008CE7DE00FFC67B00CE8C3900B584
      3900AD7B3100C6842100FFA50800AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFEF
      DE00FFE7D600B58C8C00B58C8C00B58C8C00B58C8C00B58C8C00B58C8C00B58C
      8C00BD84840000000000000000000000000000000000299CDE00DEF7FF00FFFF
      FF00F7FFFF00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEC6
      BD0084C6DE00299CDE000000000000000000EFB53900FFCEA500D6AD8400D6AD
      8400D6AD8400D6AD8400D6AD7B00CEA56B00DEAD6B00FFBD6B00F7AD5200FFAD
      4200E79C3100F7A52900FFA51000AD632900000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00EFDEDE00D6C6C600BDADAD00B58473000000
      00000000000000000000000000000000000000000000299CDE00DEF7FF00F7F7
      F700B5C6CE00ADC6CE00A5C6CE00A5C6CE00A5C6CE00A5C6CE00B5D6D600DEFF
      FF008CDEF700299CDE00000000000000000000000000E7A50800EFB53900F7BD
      4A00EFB54A00EFB54A00F7BD6300F7B55A00EFB54A00EFAD4200EFA53900E79C
      3100EF9C2100E7941800D684180000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B58C8C00B58C8C00B58C8C00B58C8C000000
      00000000000000000000000000000000000000000000299CDE00E7FFFF00DECE
      C600BDA59C00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEBDB500BD9C9400E7EF
      E70094DEF700299CDE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B58C8C00EFB56B00C68C7B00000000000000
      0000000000000000000000000000000000000000000000000000299CDE00B5D6
      E700949C9C00E7DED600FFFFFF00FFFFF700FFFFF700D6C6BD00849CA5008CCE
      E700299CDE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00B58C8C00B58C
      8C00B58C8C00B58C8C00B58C8C00B58C8C00BD84840000000000000000000000
      000000000000000000000000000000000000000000000000000000000000299C
      DE00299CDE009C948C009C948C009C948C009C948C009C948C00299CDE00299C
      DE00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B5B5B5000000000000000000000000009C9C9C009C9C
      9C00A5A5A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000009C
      CE004AB5E70063C6EF0052BDE70039B5DE004ABDE70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5B5B500B5B5B500DEDEDE00ADADAD00525252006B6B6B00ADADAD00D6D6
      D600E7DEDE009C9C9C000000000000000000000000000000000000000000009C
      CE004AB5E70063C6EF0052BDE70039B5DE004ABDE70000000000000000000000
      000000000000000000000000000000000000000000000000000008A5CE0008A5
      D6007BD6F700A5DEFF0094DEFF0094DEFF007BCEEF0073CEEF004AB5E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5B5B500B5B5
      B500FFFFFF00FFFFFF00E7DEDE00B5B5B5005A5A630031313100313131004A4A
      4A00848484009C9C9C00A5A5A50000000000000000000000000008A5CE0008A5
      D6007BD6F700A5DEFF0094DEFF0094DEFF007BCEEF0073CEEF004AB5E7000000
      000000000000000000000000000000000000000000000000000010ADD60018AD
      DE0084DEEF00A5E7FF0094DEFF0084D6F7005ABDE7008CD6F7009CDEFF0073CE
      F700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A5A5A500B5B5B500F7F7F700FFFF
      FF00F7F7F700D6D6D600B5B5B500A5A5A500ADADAD009C9C9C007B7B7B005252
      520031313900313131008C8C8C0000000000000000000000000010ADD60018AD
      DE0084DEEF00A5E7FF0094DEFF0084D6F7005ABDE7008CD6F7009CDEFF0073CE
      F70000000000000000000000000000000000000000000000000021ADDE0029B5
      DE0094DEEF00B5EFFF00A5E7FF0063B5D600319CC6006BC6EF0094DEFF009CDE
      FF0031B5DE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B5ADAD00EFEFEF00EFEFEF00CECE
      CE00BDB5B500C6C6C600D6CECE00B5B5B500ADADAD00ADA5A500ADADAD00ADAD
      AD00A5A5A500848484009C9C9C0000000000000000000000000021ADDE0029B5
      DE0094DEEF00B5EFFF00A5E7FF0063B5D600319CC6006BC6EF0094DEFF009CDE
      FF0031B5DE00000000000000000000000000000000000000000029B5DE0039BD
      E700B5E7F700CEFFFF00B5F7FF0084CEDE00297BA5006BBDE70094DEFF0094DE
      FF0031B5DE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADA5AD00BDBDBD00B5B5B500C6C6
      C600D6D6D600DEDEDE00F7F7F700F7F7F700E7E7E700D6D6D600BDBDBD00ADAD
      AD00ADA5A500ADADAD00A5A5A50000000000000000000000000029B5DE0039BD
      E700B5E7F700CEFFFF00B5F7FF0084CEDE00297BA5006BBDE70094DEFF0094DE
      FF0031B5DE00000000000000000000000000000000000000000039BDE7004AC6
      EF00C6EFF700EFFFFF00CEF7FF00B5EFF70063ADBD008CCEEF009CDEFF0094DE
      FF0031B5DE000000000000000000000000000000000000000000000000007B7B
      7B000000000000000000000000007B7B7B000000000000FFFF007B7B7B000000
      000000000000000000000000000000000000A5A5A500C6BDC600D6D6D600DEDE
      DE00D6D6D600EFEFEF00DEDEDE00B5BDB500CECECE00D6DEDE00DEDEDE00DEDE
      DE00D6D6D600C6C6C600ADADAD0000000000000000000000000039BDE7004AC6
      EF00C6EFF700EFFFFF00CEF7FF00B5EFF70063ADBD008CCEEF009CDEFF0094DE
      FF0031B5DE00000000000000000000000000000000000000000029B5DE0042C6
      E7005AC6DE006BCEE70063CEE70063C6E7006BCEE70084DEEF009CE7FF00A5E7
      FF0031B5DE000000000000000000000000000000000000000000000000007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B00000000000000000000FFFF000000
      00000000000000000000000000000000000000000000B5ADAD00DEDEDE00DEDE
      DE00E7E7E700D6D6D600C6C6C600BDDEBD00CED6CE00D6BDB500BDB5B500BDBD
      BD00C6C6C600D6CECE00BDBDBD0000000000000000000000000029B5DE0042C6
      E7005AC6DE006BCEE70063CEE70063C6E7006BCEE70084DEEF009CE7FF00A5E7
      FF0031B5DE00000000000000000000000000000000000000000029B5DE0073DE
      E700318CA500399CB5006BE7F70042CEE70021B5DE0010ADD60029ADD6007BD6
      EF0031B5DE0000000000000000000000000000000000000000007B7B7B00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD00FFFFFF007B7B7B0000000000000000000000
      0000000000000000000000000000000000000000000000000000B5ADAD00CECE
      CE00C6BDBD00C6C6C600EFEFEF00FFF7F700F7F7F700F7EFE700E7E7E700DEDE
      DE00C6C6C600ADA5A5000000000000000000000000000000000029B5DE005ACE
      E70042BDD60042BDDE0042C6DE0031B5DE004AC6E7006BCEEF009CE7FF0084D6
      F70021ADD60000000000000000000000000000000000000000000000000031B5
      D6004ABDD60073D6E7009CFFFF0084F7FF0052CEE70010A5CE000094C60010A5
      CE0018A5D6000000000000000000000000007B7B7B007B7B7B00FFFFFF00BDBD
      BD00FFFFFF000000FF00FFFFFF00BDBDBD00FFFFFF007B7B7B007B7B7B000000
      000000000000000000000000000000000000000000000000000000000000B5AD
      AD00E7E7E700D6D6D600B5B5B500CECECE00DEDEE700DEE7E700DEDEDE00D6D6
      D600C6BDBD00000000000000000000000000000000000000000029B5D6005ACE
      DE00089CC60039B5D6008CF7FF0052D6EF0021B5DE00009CCE0021ADD60052C6
      E70018A5D6000000000000000000000000000000000000000000000000000000
      0000000000000000000052CEE7004ACEE70042C6DE00109CC600089CC60010A5
      D60010A5CE00000000000000000000000000000000007B7B7B00BDBDBD00FFFF
      FF00BDBDBD000000FF00BDBDBD00FFFFFF00BDBDBD007B7B7B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFEFEF00FFE7DE00EFD6CE00EFD6CE00EFDED600E7DEDE00DEDEDE00ADAD
      AD00000000000000000000000000000000000000000000000000000000000894
      C6000094BD0029ADCE0084F7FF007BEFFF0042C6DE000894C600109CC60018AD
      D600000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000109CC6000894C6000000
      000000000000000000000000000000000000000000007B7B7B00FFFFFF000000
      FF000000FF000000FF000000FF000000FF00FFFFFF007B7B7B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000089CC600189CC6000000000039BDE70021A5CE000894C6000894C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008CBD000084B500000000000000000000000000089CC6000894BD000000
      000000000000000000000000000000000000000000007B7B7B00BDBDBD00FFFF
      FF00BDBDBD000000FF00BDBDBD00FFFFFF00BDBDBD007B7B7B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004AC6E700299CC6000000000000000000000000000894BD00088CB5000000
      0000000000000000000000000000000000000000000000000000000000000000
      000029B5DE00189CCE00000000000000000000000000089CCE000894BD000000
      0000000000000000000000000000000000007B7B7B007B7B7B00FFFFFF00BDBD
      BD00FFFFFF000000FF00FFFFFF00BDBDBD00FFFFFF007B7B7B007B7B7B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000039A5C60073DEEF0039ADCE0000000000108CB500089CCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004AB5D6006BD6EF00000000000000000000000000089CC6000894BD000000
      00000000000000000000000000000000000000000000000000007B7B7B00FFFF
      FF00BDBDBD00FFFFFF00BDBDBD00FFFFFF007B7B7B0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEB5
      B500FFE7DE00FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004AB5CE007BE7EF006BD6E7004ABDE700189CC600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005AC6DE007BDEEF004ABDD60039ADD60018A5D600000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEB5
      B500DEB5B500DEB5B500DEB5B500F7BDB500F7BDB50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000052BDD60052C6D6004ABDDE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007B7B
      7B000000000000000000000000007B7B7B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C6363006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C6C6C6000000
      00000000000000000000C6C6C60000000000000000000000000000000000C6C6
      C600C6C6C60000000000C6C6C600000000000000000000000000000000000000
      00009C6363009C636300BD636300BD6B6B006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000C6C6C6000000000000000000C6C6
      C600C6C6C6000000000000000000C6C6C6000000000000000000000000000000
      000000000000C6C6C600000000000000000000000000000000009C6363009C63
      6300C66B6B00D66B6B00D66B6B00C66B6B006B3131009C6363009C6363009C63
      63009C6363009C6363009C63630000000000000000000000000000FFFF00FFFF
      FF0000FFFF0000FFFF00FFFFFF0000FFFF000000000000FF000000FF00000000
      0000FFFFFF0000FFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000C6C6C600C6C6
      C6000000000000000000C6C6C600000000000000000000000000848400008484
      00000000000000000000000000000000000000000000000000009C636300DE73
      7300D6737300D66B7300D66B6B00C66B6B006B313100FFA5A500FFADB500FFBD
      BD00FFC6C600FFC6C6009C636300000000000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF0000000000000000000000000000FF000000FF00000000
      000000000000000000007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848400008484
      00008484000000000000000000000000000000000000000000009C636300E773
      7B00DE737300DE737300DE737300CE6B73006B31310039C6630021CE630029CE
      630018CE5A00FFC6C6009C63630000000000000000000000000000FFFF00FFFF
      FF0000FFFF0000FFFF000000000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF00007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C6000000000000000000000000000000000000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009C636300E77B
      7B00E77B7B00DE7B7B00DE737B00D67373006B31310042C66B0031CE630031CE
      630021CE6300FFC6C6009C636300000000000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF000000000000FF000000FF000000FF000000FF000000FF
      000000FF000000FF00007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C6000000000084848400848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009C636300EF84
      8400E77B8400E77B7B00E7848400D67373006B31310039C6630029CE630029CE
      630021CE5A00FFC6C6009C636300000000000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF0000000000000000000000000000FF000000FF00000000
      000000000000000000007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF00C6C6C600C6C6C600C6C6C600FF00
      0000FFFF0000FF00000084848400848484008484840000000000848400000000
      00008484000000000000C6C6C600C6C6C60000000000000000009C636300F784
      8C00EF848400EF949400FFDEDE00DE8C8C006B313100BDE7AD006BDE8C005AD6
      840042D67300FFC6C6009C63630000000000000000000000000000FFFF00FFFF
      FF0000FFFF0000FFFF00FFFFFF0000FFFF000000000000FF000000FF00000000
      0000FFFFFF0000FFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000C6C6C600FF000000FF000000FF00
      0000FFFF0000FFFF000084840000848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009C636300F78C
      8C00EF848400F79C9C00FFDEDE00DE8C8C006B313100FFF7DE00FFFFE700FFFF
      DE00EFFFD600FFC6C6009C636300000000000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF000000000000FF000000FF00000000
      000000FFFF00FFFFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000FF000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FF000000848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009C636300FF94
      9400F78C8C00F78C8C00F78C8C00DE7B84006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFE700FFC6C6009C63630000000000000000000000000000FFFF00FFFF
      FF0000FFFF0000FFFF00FFFFFF0000FFFF000000000000000000000000000000
      0000FFFFFF0000FFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000FF000000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF0000FF000000848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009C636300FF94
      9C00FF949400FF949400FF949400E78484006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C63630000000000000000000000000000FFFF00FFFF
      FF0000FFFF0000FFFF00FFFFFF0000FFFF0000FFFF00FFFFFF0000FFFF0000FF
      FF00FFFFFF0000FFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000C6C6C600C6C6C600C6C6C600FF00
      0000FFFF0000FF00000084848400848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009C636300FF9C
      9C00FF949C00FF949400FF949C00E78C8C006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C636300000000000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFFFF0000FFFF00FFFFFF00FFFF
      FF0000FFFF00FFFFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000C6C6C600C6C6C600C6C6C600FF00
      0000FF0000000000000084848400848484008484840000000000848400008484
      00008484000000000000C6C6C600C6C6C60000000000000000009C636300FF9C
      A500FF9C9C00FF9C9C00FF9C9C00E78C8C006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C6363000000000000000000000000007B7B7B007B7B
      7B007B7B7B007B7B7B007B7B7B0000FFFF0000FFFF00FFFFFF0000FFFF0000FF
      FF00FFFFFF0000FFFF007B7B7B00000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C6000000000084848400848484008484840084848400FFFF00008484
      00008484000000000000C6C6C600C6C6C60000000000000000009C6363009C63
      6300EF8C8C00FF9C9C00FF9C9C00EF8C94006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C636300000000000000000000000000FFFFFF0000FF
      FF00FFFFFF00FFFFFF0000FFFF007B7B7B007B7B7B007B7B7B007B7B7B007B7B
      7B007B7B7B007B7B7B0000000000000000000000000000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600000000008484840084848400848484008484840000000000FFFF
      00008484000000000000C6C6C600C6C6C6000000000000000000000000000000
      00009C636300B5737300D6848400DE8C8C006B3131009C6363009C6363009C63
      63009C6363009C6363009C636300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C6000000000000000000000000000000000000000000000000000000
      00000000000000000000C6C6C600C6C6C6000000000000000000000000000000
      000000000000000000009C6363009C6363006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000000000000000000000
      000000000000000000000000000000000000C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6C600C6C6
      C600C6C6C600C6C6C600C6C6C600C6C6C600424D3E000000000000003E000000
      2800000040000000500000000100010000000000800200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FCFFFE7FFFFFFFFFFE7FFE1FFFFFFFFF
      FE3FFC07FFFFFFFFF81FFC01FFFFFF3FF80FF800FCFFFC3FFC07F800FC3FF03F
      FC1F0000FC0FC000E00F000000030000E00700010000C000F00700320003F03F
      F01F003EFC0FFC3FF80F003EFC3FFF3FF807003EFCFFFFFFFC03001DFFFFFFFF
      FC010023FFFFFFFFFFFF003FFFFFFFFFFFFFFFC3F801E001FFFFFFC3F801C001
      8001C000F80180010000C000F80180010000C000800180010000C00080018001
      0000C003800180010000C003800180010000C007800180010000C00780018003
      0000C007800380030000C007800780030000C00F801F80038001C01F801F8003
      FFFFC03F803FC007FFFFFFFF807FE00FFDC7FFFFE07FFFFDF003E07FC01FFFF8
      C001C01FC00FFFF10001C00FC007FFE30001C007C007FFC70001C007C007E08F
      0001C007C007C01F8001C007C007803FC003C007E007001FE007C007FC07001F
      F00FE00FFF9F001FF03FF21FF39F001FF03FF39FF39F001FF03FF13FF39F803F
      E03FF83FF83FC07FE07FFFFFFC7FE0FFFE7FFFFFFFFFDDA5F07FFF0F8001669B
      C001C0018001CD87C001C00180000380C001C00180000000C001C00180000000
      C001C00180000000C001C00180010000C001C00180010000C001C00180010000
      C001C00180010000C001C00180010000C001C00180010000C001C00380010000
      F001FFFFFFC10000FC7FFFFFC07F000000000000000000000000000000000000
      000000000000}
  end
  inherited ppPrint: TTBPopupMenu [7]
    Left = 536
    Top = 300
  end
  inherited fmstr: TFormStorage [8]
    Active = False
    PreventResize = True
    Left = 364
    Top = 364
  end
  inherited ActionList2: TActionList [9]
    Left = 152
    Top = 216
  end
  object ppInv: TPopupMenu
    Images = ilButtons
    Left = 708
    Top = 312
    object mnitAddToNew: TMenuItem
      Action = acAddSemisNew
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082' '#1085#1086#1074#1086#1081'  '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object mnitAddToInv: TMenuItem
      Action = acAddSemis
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object mnitDelItem: TMenuItem
      Action = acDelSemis
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object mnitPrint: TMenuItem
      Tag = 1
      Action = acPrint
      Caption = #1055#1077#1095#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object N7: TMenuItem
      Action = acPrintPreview
    end
    object N8: TMenuItem
      Action = acPrintInvWithAssort
    end
    object Excel1: TMenuItem
      Action = acExcel
    end
    object mnitSep1: TMenuItem
      Caption = '-'
      Visible = False
    end
    object mnitCopy: TMenuItem
      Action = acSCopy
    end
    object mnitIns: TMenuItem
      Action = acSPaste0
    end
    object mnitInsOrd: TMenuItem
      Tag = 1
      Action = acSPaste1
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object N5: TMenuItem
      Action = acTransform0
      Visible = False
    end
    object N2: TMenuItem
      Caption = #1069#1089#1082#1087#1077#1088#1080#1084#1077#1085#1090#1072#1083#1100#1085#1072#1103' '#1088#1072#1073#1086#1090#1072
      ShortCut = 120
      Visible = False
    end
    object N4: TMenuItem
      Action = acArt
    end
  end
  object acEvent: TActionList
    Images = ilButtons
    Left = 404
    Top = 364
    object acAddSemisNew: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = '  '#1050' '#1085#1086#1074#1086#1081'  '
      ImageIndex = 9
      OnExecute = acAddSemisNewExecute
      OnUpdate = acAddSemisUpdate
    end
    object acAddSemis: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 14
      OnExecute = acAddSemisExecute
      OnUpdate = acAddSemisUpdate
    end
    object acDelSemis: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 15
      OnExecute = acDelSemisExecute
      OnUpdate = acDelSemisUpdate
    end
    object acACopy: TAction
      Category = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 10
      OnExecute = acACopyExecute
      OnUpdate = acACopyUpdate
    end
    object acAPaste: TAction
      Category = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100
      ImageIndex = 11
      OnExecute = acAPasteExecute
      OnUpdate = acAPasteUpdate
    end
    object acSCopy: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 10
      OnExecute = acSCopyExecute
    end
    object acSPaste0: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' ('#1074#1099#1076#1072#1095#1072')'
      ImageIndex = 11
      OnExecute = acPasteExecute
      OnUpdate = acSPaste0Update
    end
    object acSPaste1: TAction
      Tag = 1
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' ('#1085#1072#1088#1103#1076')'
      ImageIndex = 11
      OnExecute = acPasteExecute
      OnUpdate = acSPaste1Update
    end
    object acTransform0: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1058#1088#1072#1085#1089#1092#1086#1088#1084#1072#1094#1080#1103
      ImageIndex = 12
      ShortCut = 117
      OnExecute = acTransform0Execute
      OnUpdate = acTransform0Update
    end
    object acTransformDel: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1090#1088#1072#1085#1089#1092#1086#1088#1084#1072#1094#1080#1102
      ImageIndex = 12
      OnExecute = acTransformDelExecute
    end
    object acArt: TAction
      Category = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      ImageIndex = 13
      ShortCut = 16449
      OnExecute = acArtExecute
      OnUpdate = acArtUpdate
    end
    object acPrint: TAction
      Tag = 1
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1055#1077#1095#1072#1090#1100
      ImageIndex = 4
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
    object Action1: TAction
    end
    object acExcel: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' Excel'
      OnExecute = acExcelExecute
    end
    object acMultiSelect: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = 'MultiSelect'
      ShortCut = 116
      OnExecute = acMultiSelectExecute
    end
    object acShowID: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = 'ShowID'
      ShortCut = 24649
      OnExecute = acShowIDExecute
    end
    object ac0: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1042#1099#1076#1072#1095#1072
      ShortCut = 32817
      OnExecute = ac0Execute
    end
    object ac6: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1044#1086#1088#1072#1073#1086#1090#1082#1072
      ShortCut = 32818
      OnExecute = ac6Execute
    end
    object ac1: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1053#1072#1088#1103#1076
      ShortCut = 32819
      OnExecute = ac1Execute
    end
    object ac3: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1042#1086#1079#1074#1088#1072#1090
      ShortCut = 32820
      OnExecute = ac3Execute
    end
    object ac5: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1041#1088#1072#1082
      ShortCut = 32821
      OnExecute = ac5Execute
    end
    object ac7: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1053#1077#1082#1086#1084#1087#1083#1077#1082#1090
      ShortCut = 32822
      OnExecute = ac7Execute
    end
    object acPrintPreview: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1055#1077#1095#1072#1090#1100' '#1087#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1086#1081' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      OnExecute = acPrintPreviewExecute
    end
    object acPrintInvWithAssort: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1055#1077#1095#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1089' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1086#1084
      OnExecute = acPrintInvWithAssortExecute
    end
    object acActiveWH: TAction
      Category = #1057#1077#1088#1074#1080#1089
      Caption = #1040#1082#1090#1080#1074#1080#1079#1080#1088#1086#1074#1072#1090#1100' '#1082#1083#1072#1076#1086#1074#1091#1102
      ShortCut = 114
      OnExecute = acActiveWHExecute
    end
    object acTransformAssort: TAction
      Category = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      ImageIndex = 12
      OnExecute = acTransformAssortExecute
      OnUpdate = acTransformAssortUpdate
    end
  end
  object ppA: TPopupMenu
    Images = ilButtons
    Left = 608
    Top = 108
    object mnitCopyA: TMenuItem
      Action = acACopy
    end
    object mnitPasteA: TMenuItem
      Action = acAPaste
    end
  end
  object printItem: TPrintDBGridEh
    DBGridEh = gridItem
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 448
    Top = 300
  end
end
