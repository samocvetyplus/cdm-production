inherited fmdSimpleDepart: TfmdSimpleDepart
  Left = 191
  Top = 194
  Width = 381
  Caption = #1052#1077#1089#1090#1072' '#1093#1088#1072#1085#1077#1085#1080#1103
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 373
  end
  inherited plDict: TPanel
    Width = 373
    inherited plSelect: TPanel
      Width = 369
      inherited btnOk: TSpeedButton
        Left = 191
      end
      inherited btnCancel: TSpeedButton
        Left = 275
      end
    end
    inherited gridDict: TDBGridEh
      Width = 369
      AllowedOperations = [alopUpdateEh]
      AutoFitColWidths = True
      DataSource = dm.dsrDepart
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NAME'
          Footers = <>
          Width = 263
        end>
    end
  end
  inherited tb1: TSpeedBar
    Width = 373
  end
end
