inherited fmRejAdd: TfmRejAdd
  Left = 65
  Top = 181
  Width = 597
  Height = 421
  Caption = #1054#1090#1095#1077#1090' '#1086' '#1089#1085#1103#1090#1099#1093' '#1080' '#1076#1086#1073#1072#1074#1083#1077#1085#1085#1099#1093' '#1072#1088#1090#1080#1082#1091#1083#1072#1093
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 368
    Width = 589
    SimplePanel = True
  end
  inherited tb1: TSpeedBar
    Width = 589
    object SpeedItem1: TSpeedItem
      Action = acPrint
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = 'SpeedItem1'
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acPrintExecute
      SectionName = 'Untitled (0)'
    end
  end
  object gridRejAdd: TDBGridEh [2]
    Left = 0
    Top = 42
    Width = 589
    Height = 326
    Align = alClient
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmAppl.dsrRejAdd
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Width = 56
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SZNAME'
        Footers = <>
        Width = 45
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footers = <>
        Width = 40
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'COMMENTNAME'
        Footers = <>
        Width = 158
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'COMMENT'
        Footers = <>
        Width = 100
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
        Title.Caption = ' |'#1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
        Width = 81
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INVDATE'
        Footers = <>
        Title.Caption = ' |'#1044#1072#1090#1072' '#1076#1077#1081#1089#1090#1074#1080#1103
        Width = 69
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited fmstr: TFormStorage
    Top = 92
  end
  object acEvent: TActionList
    Left = 24
    Top = 93
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      OnExecute = acPrintExecute
    end
  end
  object PrintGrid: TPrintDBGridEh
    DBGridEh = gridRejAdd
    Options = []
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 160
    Top = 92
  end
end
