{***********************************************}
{  �������� ������������                        }
{  Copyrigth (C) 2002 basile for CDM            }
{  v3.10                                        }
{  create 05.10.2002                            }
{  last update 12.05.2004                       }
{  ���� ������� ������                          }
{    - ������ adtWOrder                         }
{    - ���������� ����������� adtDIEl           }
{    - ������ ���������� adtSOEl                }
{    - ���� �� ������� adtRej                   }
{    - ��������� ������������ adtMdfArt         }
{    - ��������� ������������ (�������)         }
{    - update for FIB5.2                        }
{***********************************************}
unit ADistr;

interface


uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls, ComCtrls,
  Grids, DBGrids, RXDBCtrl, StdCtrls, RXSplit,
  Buttons, DBCtrls, ActnList, db, Menus, ApplProd, DBGridEh, DBCtrlsEh,
  Mask, TB2Item, TB2Dock, TB2Toolbar, pFIBDataSet, FR_DSet, FR_DBSet,
  FR_Class, DBGridEhGrouping, GridsEh;

type
  TfmADistr = class(TfmAncestor)
    plWH: TPanel;
    plInv: TPanel;
    plBtn: TPanel;
    spltLeft: TRxSplitter;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    spitAdd: TSpeedButton;
    spbrBtn: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    spitAdd1: TSpeedItem;
    spitDel: TSpeedItem;
    plRej: TPanel;
    plPInv: TPanel;
    acPAdd: TAction;
    acRej: TAction;
    acStone: TAction;
    spitArt2: TSpeedItem;
    acCreateArt2: TAction;
    acRejDel: TAction;
    acPDel: TAction;
    acChangeArt: TAction;
    spltStone: TSplitter;
    acAdd2: TAction;
    acPrintWh: TAction;
    acPrintInv: TAction;
    acChangeSz: TAction;
    spitMode: TSpeedItem;
    acWhKind: TAction;
    plApplProd: TPanel;
    plAppl: TPanel;
    plMain: TPanel;
    lbNoDoc: TLabel;
    lbDateDoc: TLabel;
    SpeedButton6: TSpeedButton;
    txtApplNo: TDBText;
    txtApplDate: TDBText;
    gridAppl: TDBGridEh;
    TBDock1: TTBDock;
    tlbrAppl: TTBToolbar;
    TBDock2: TTBDock;
    TBToolbar2: TTBToolbar;
    TBItem6: TTBItem;
    TBItem5: TTBItem;
    spltGrids: TSplitter;
    acAddAppl: TAction;
    acDelAppl: TAction;
    plAPInv: TPanel;
    Panel1: TPanel;
    Label8: TLabel;
    txtAPDocNo: TDBText;
    Label9: TLabel;
    txtAPDocDate: TDBText;
    gridAPInv: TDBGridEh;
    TBDock3: TTBDock;
    tlbrApplInv: TTBToolbar;
    TBItem1: TTBItem;
    acPrintAPInv: TAction;
    acCloseAppl: TAction;
    TBItem2: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    spitAppl: TSpeedItem;
    acNewAppl: TAction;
    acInvArt2Ins: TAction;
    acWhArt2Ins: TAction;
    acDel2: TAction;
    acChangeArt2: TAction;
    TBDock: TTBDock;
    tlbrWh: TTBToolbar;
    titChange: TTBSubmenuItem;
    TBItem3: TTBItem;
    TBItem4: TTBItem;
    TBItem7: TTBItem;
    TBItem8: TTBItem;
    TBItem9: TTBItem;
    TBSubmenuItem2: TTBSubmenuItem;
    TBItem10: TTBItem;
    ppWh: TTBPopupMenu;
    TBItem11: TTBItem;
    TBItem12: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    TBItem13: TTBItem;
    TBItem14: TTBItem;
    TBItem15: TTBItem;
    TBSeparatorItem4: TTBSeparatorItem;
    TBItem16: TTBItem;
    TBItem17: TTBItem;
    TBSeparatorItem5: TTBSeparatorItem;
    gridWHAppl: TDBGridEh;
    plDepFrom: TPanel;
    gridRejEl: TDBGridEh;
    plRejName: TPanel;
    Panel2: TPanel;
    gridPEl: TDBGridEh;
    ppPInv: TTBPopupMenu;
    ppRejInv: TTBPopupMenu;
    TBItem18: TTBItem;
    TBItem19: TTBItem;
    Panel3: TPanel;
    Label5: TLabel;
    txtPDocNo: TDBText;
    Label6: TLabel;
    txtPDocDate: TDBText;
    TBItem20: TTBItem;
    TBItem21: TTBItem;
    Panel4: TPanel;
    Label2: TLabel;
    txtRejDocNo: TDBText;
    Label3: TLabel;
    txtRejDocDate: TDBText;
    gridAEl: TDBGridEh;
    plDepTo: TPanel;
    ppInv: TTBPopupMenu;
    TBItem22: TTBItem;
    TBItem23: TTBItem;
    TBSeparatorItem6: TTBSeparatorItem;
    plHeaderInv: TPanel;
    lbAInv: TLabel;
    txtDocNo: TDBText;
    lbOt: TLabel;
    txtDocDate: TDBText;
    tbdkInv: TTBDock;
    tlbrInv: TTBToolbar;
    TBItem24: TTBItem;
    TBItem25: TTBItem;
    lbOperTo: TLabel;
    acShowStone: TAction;
    gridStone: TDBGridEh;
    TBSubmenuItem3: TTBSubmenuItem;
    TBItem26: TTBItem;
    TBSubmenuItem4: TTBSubmenuItem;
    TBItem28: TTBItem;
    Panel5: TPanel;
    lbOperFrom: TLabel;
    cmbxOper: TDBComboBoxEh;
    TBItem27: TTBItem;
    spitChangeArt: TSpeedItem;
    acChangeByAppl: TAction;
    TBItem29: TTBItem;
    acShowID: TAction;
    TBItemDemand: TTBItem;
    ActionDemand: TAction;
    TBItem30: TTBItem;
    acPrintAppl: TAction;
    frReportAppl: TfrReport;
    frDataSetAppl: TfrDBDataSet;
    frDBDataSet1: TfrDBDataSet;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acPDelExecute(Sender: TObject);
    procedure acRejExecute(Sender: TObject);
    procedure acRejDelExecute(Sender: TObject);
    procedure acPAddExecute(Sender: TObject);
    procedure acCreateArt2Execute(Sender: TObject);
    procedure acStoneExecute(Sender: TObject);
    procedure cmbxOper1Change(Sender: TObject);
    procedure GridPost(Sender: TObject);
    procedure acChangeArtExecute(Sender: TObject);
    procedure acAdd2Execute(Sender: TObject);
    procedure acPrintWhExecute(Sender: TObject);
    procedure acPrintInvExecute(Sender: TObject);
    procedure acChangeSzExecute(Sender: TObject);
    procedure acWhKindExecute(Sender: TObject);
    procedure acAddApplExecute(Sender: TObject);
    procedure acDelApplExecute(Sender: TObject);
    procedure gridApplDblClick(Sender: TObject);
    procedure gridApplKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acCloseApplExecute(Sender: TObject);
    procedure edFindArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acNewApplExecute(Sender: TObject);
    procedure acStoneUpdate(Sender: TObject);
    procedure acInvArt2InsExecute(Sender: TObject);
    procedure acInvArt2InsUpdate(Sender: TObject);
    procedure acWhArt2InsExecute(Sender: TObject);
    procedure acWhArt2InsUpdate(Sender: TObject);
    procedure acDel2Execute(Sender: TObject);
    procedure acAdd2Update(Sender: TObject);
    procedure acCreateArt2Update(Sender: TObject);
    procedure acChangeArt2Execute(Sender: TObject);
    procedure acChangeArt2Update(Sender: TObject);
    procedure gridWHApplGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure gridWHApplKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure gridRejElGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure gridPElGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure gridAElGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acShowStoneExecute(Sender: TObject);
    procedure gridStoneGetCellParams(Sender: TObject; Column: TColumnEh; AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure FormResize(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure cmbxOperChange(Sender: TObject);
    procedure gridAElKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acChangeByApplExecute(Sender: TObject);
    procedure acChangeByApplUpdate(Sender: TObject);
    procedure acShowIDExecute(Sender: TObject);
    procedure ActionDemandUpdate(Sender: TObject);
    procedure ActionDemandExecute(Sender: TObject);
    procedure acPrintApplUpdate(Sender: TObject);
    procedure acPrintApplExecute(Sender: TObject);
  private
    FDepFrom, FDepTo : string;
    FJobFrom: string;
    FJobTo: string;
    FApplMode: boolean;
    FSaveOrderBy : string;
    //procedure InsertWhRecord(DataSet: TDataSet; OperId, OperName: TField; T : byte; ArtId, Art2Id, Art2, U : TField);
    procedure InsertWhRecord(DataSet: TDataSet; OperId, ArtId, Art2Id, SzId, U: TField; T: byte);
    procedure SetApplMode(const Value: boolean);
  public
    property DepFrom : string read FDepFrom;
    property DepTo : string read FDepTo;
    property JobFrom : string read FJobFrom;
    property JobTo : string read FJobTo;
    property ApplMode : boolean read FApplMode write SetApplMode;
    procedure InitBtn(T : boolean);
  end;

var
  fmADistr : TfmADistr;

implementation

uses ApplData, dbUtil, DictData, dbTree, Editor,  DbEditor, ePArt, AIns, fmUtils,
  MainData, eIns, DictAncestor, dArt, PrintData, eSz, qArtStone, dAList, MsgDialog,
  FIBDataSet, ChangeAssortByAppl, ProductionConsts, eArt, frmTaskArticleOut;

{$R *.dfm}


procedure HandlePost_A(E : Exception);
begin
  dmAppl.taAEl.Cancel;
  MessageDialog(E.Message, mtError, [mbOk], 0);
end;

procedure HandlePost_P(E : Exception);
begin
  dmAppl.taPEl.Cancel;
  MessageDialog(E.Message, mtError, [mbOk], 0);
end;

procedure HandlePost_Rej(E:Exception);
begin
  dmAppl.taRejEl.Cancel;
  MessageDialog(E.Message, mtError, [mbOk], 0);
end;

procedure HandlePost_AP(E: Exception);
begin
  dmAppl.taAPEl.Cancel;
  MessageDialog(E.Message, mtError, [mbOk], 0);
end;

procedure TfmADistr.FormCreate(Sender: TObject);
var
  i : integer;
begin
  TBItemDemand.Visible := (dmAppl.ADistrType = adtWOrder) and (dmAppl.ADistrKind <> 0);


  dm.WorkMode := 'aDistr';
  gridWHAppl.FieldColumns['ARTID'].Visible := False;
  gridWHAppl.FieldColumns['ART2ID'].Visible := False;
  gridWHAppl.FieldColumns['SZID'].Visible := False;
  gridWHAppl.FieldColumns['OPERID'].Visible := False;

  gridPEl.FieldColumns['ARTID'].Visible := False;
  gridPEl.FieldColumns['ART2ID'].Visible := False;
  gridPEl.FieldColumns['SZID'].Visible := False;
  gridPEl.FieldColumns['OPERID'].Visible := False;

  gridRejEl.FieldColumns['ARTID'].Visible := False;
  gridRejEl.FieldColumns['ART2ID'].Visible := False;
  gridRejEl.FieldColumns['SZID'].Visible := False;
  gridRejEl.FieldColumns['OPERID'].Visible := False;

  // ���������������� ����� ������
  FApplMode := False;
  plApplProd.Visible := False;
  // ���������������� ����� ������
  acShowStone.Checked := False;
  // ������������� ��������
  dmAppl.FilterOperId := TNodeData(dm.dOper.Objects[0]).Code; // *��� ��������
  cmbxOper.OnChange := nil;
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.ItemIndex := 0;
  cmbxOper.OnChange := cmbxOperChange;
  // ������������� ������
  //tlbrWh.Skin := dm.TBSkin;
  //tlbrAppl.Skin := dm.TBSkin;
  //tlbrApplInv.Skin := dm.TBSkin;
  //tlbrInv.Skin := dm.TBSkin;
  //ppWh.Skin := dm.ppSkin;
  //ppInv.Skin := dm.ppSkin;

  // ��������� ����� �� ��������� ����� ������ � ���
  case dmAppl.ADistrType of
    // ���������� ����������� ����� ���������
    adtDIEl : begin
      acCreateArt2.Visible := False;
      acRej.Visible := False;
      acRejDel.Visible := True;
      acPAdd.Visible := False;
      acPDel.Visible := True;
      acAdd2.Visible := False;
      acDel2.Visible := False;
      acWhKind.Visible := False;
      acChangeSz.Visible := True;
      acChangeArt.Visible := True;
      acChangeArt2.Visible := True;
      acStone.Visible := False;
      spitChangeArt.Visible := False;
      OpenDataSets([dmAppl.taWHAppl, dmAppl.taAInv, dmAppl.taAEl, dmAppl.taRejInv, dmAppl.taRejEl, dmAppl.taPInv, dmAppl.taPEl]);
      FDepFrom := ExecSelectSQL('select Name from D_Dep where DepId='+IntToStr(dmAppl.WhFrom), dmAppl.quTmp);
      FDepTo := ExecSelectSQL('select Name from D_Dep where DepId='+IntToStr(dmAppl.WhTo), dmAppl.quTmp);
      plDepFrom.Caption := '��������� ��������: '+FDepFrom;
      plDepTo.Caption := '��������� ������������ ��� '+FDepTo;
      FJobTo := dmMain.taDIListJOBNAME.AsString;
      FJobFrom := dmMain.taDIListFIO.AsString;
      gridWhAppl.FieldColumns['OPERNAME'].Title.Caption := '��������';
      gridAEl.FieldColumns['OPERATION'].Title.Caption := '��������';
      gridAEl.FieldColumns['OPERATIONFROM'].Visible := False;
      // ����������� ����������
      Caption := '��������� �'+dmAppl.taAInvDOCNO.AsString+' ����������� ������������: ' + FDepFrom+ ' -> ' + FDepTo;
      lbOperTo.Caption := '��������: '+ExecSelectSQL('select Operation from D_Oper where OperId="'+dmAppl.AOperId+'"', dmAppl.quTmp);
      if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty) then plRej.Visible := False;
      if((dmAppl.PInvId = 0) or dmAppl.taPInv.IsEmpty) then plPInv.Visible := False;
    end;

    adtWOrder : begin
      OpenDataSets([dmAppl.taWHAppl, dmAppl.taAInv, dmAppl.taAEl, dmAppl.taRejInv, dmAppl.taRejEl, dmAppl.taPInv, dmAppl.taPEl]);
      FDepFrom := ExecSelectSQL('select Name from D_Dep where DepId='+IntToStr(dmAppl.WhFrom), dmAppl.quTmp);
      FDepTo := ExecSelectSQL('select Name from D_Dep where DepId='+IntToStr(dmAppl.WhTo), dmAppl.quTmp);
      spitChangeArt.Visible := False;
      if dmAppl.ADistrKind = 0 then
      begin
        titChange.Visible := True;
        plDepFrom.Caption := '��������� ��������: '+FDepFrom;
        plDepTo.Caption   := '��������� ������������ ��� '+FDepTo;
        FJobTo   := dmMain.taWOrderJOBNAME.AsString;
        FJobFrom := dmMain.taWOrderUserName.AsString;
        //spbrWh.Height := 81;
        //spbrAInv.Height := 61;
        gridWhAppl.FieldColumns['OPERNAME'].Title.Caption := '����� ��������';
        gridAEl.FieldColumns['OPERATION'].Title.Caption := '�� ��������';
        gridAEl.FieldColumns['OPERATIONFROM'].Visible := True;
        acCreateArt2.Visible := True;
        acRej.Visible := True;
        acPAdd.Visible := True;
        acStone.Visible := True;
      end
      else begin
        titChange.Visible := False;
        plDepFrom.Caption := '��������� ����� ��������: '+FDepFrom;
        plDepTo.Caption := '��������� ������������ ��� '+FDepTo;
        gridWhAppl.FieldColumns['OPERNAME'].Title.Caption := '�� ��������';
        gridAEl.FieldColumns['OPERATION'].Title.Caption := '����� ��������';
        gridAEl.FieldColumns['OPERATIONFROM'].Visible := False;
        FJobTo := dmMain.taWOrderUserName.AsString;
        FJobFrom := dmMain.taWOrderJOBNAME.AsString;
        acChangeSz.Visible := False;
        acChangeArt.Visible := False;
        acChangeArt2.Visible := False;
        acCreateArt2.Visible := True;
        acStone.Visible := False;
        case dmAppl.ItType of
          1 : begin // �����
            acRej.Visible := False;
            acPAdd.Visible := True;
          end;
          5 : begin // ����
            acRej.Visible := True;
            acPAdd.Visible := False;
           end;
           else begin
             //spbrWh.Height := 58;
             //spbrAInv.Height := 37;
           end;
        end;
      end;
      // ����������� ����������
      Caption := '��������� �'+dmAppl.taAInvDOCNO.AsString+' ����������� ������������: ' + FDepFrom+ ' -> ' + FDepTo;
      lbOperTo.Caption := '��������: '+ExecSelectSQL('select Operation from D_Oper where OperId="'+dmAppl.AOperId+'"', dmAppl.quTmp);
      if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty) then plRej.Visible := False;
      if((dmAppl.PInvId = 0) or dmAppl.taPInv.IsEmpty) then plPInv.Visible := False;
      // ��������� ����� ������
      case dmAppl.AInvType of
        7 : acWhKind.Execute;
      end;
    end;

    adtSOEl : begin
      acCreateArt2.Visible := False;
      acRej.Visible := False;
      acRejDel.Visible := False;
      acPAdd.Visible := False;
      acPDel.Visible := False;
      acAdd2.Visible := False;
      acDel2.Visible := False;
      acChangeSz.Visible := False;
      acWhKind.Visible := False;
      acChangeArt.Visible := False;
      acChangeArt2.Visible := False;
      spitChangeArt.Visible := False;

      plRej.Visible := True;
      plRej.Align := alClient;

      plPInv.Visible := False;

      acStone.Visible := False;
      acShowStone.Visible := False;

      gridAEl.Visible := False;

      OpenDataSets([dmAppl.taWHAppl, dmAppl.taRejInv, dmAppl.taRejEl]);

      FDepFrom := ExecSelectSQL('select Name from D_Dep where DepId='+
        IntToStr(dmAppl.WhFrom), dmAppl.quTmp);
      plDepFrom.Caption := '��������� ��������: '+FDepFrom;
      FJobFrom := dmMain.taSOListFIO.AsString;
      //spbrWh.Height := 58;
      //spbrAInv.Height := 37;
      gridWhAppl.FieldColumns['OPERNAME'].Title.Caption := '��������';
    end;
    // ����
    adtRej : begin
      acCreateArt2.Visible := False;
      acRej.Visible       := False;
      acRejDel.Visible    := False;
      acPAdd.Visible      := False;
      acPDel.Visible      := False;
      acAdd2.Visible      := False;
      acDel2.Visible      := False;
      acChangeSz.Visible  := False;
      acWhKind.Visible    := False;
      acChangeArt.Visible := False;
      acChangeArt2.Visible := False;
      tbdkInv.Visible      := False;
      plDepTo.Visible      := False;
      plHeaderInv.Visible  := False;
      titChange.Visible    := False;
      spitChangeArt.Visible := False;

      plRej.Visible := True;
      plRej.Align := alClient;

      plPInv.Visible := False;

      acStone.Visible := False;
      acShowStone.Visible := False;

      gridAEl.Visible := False;

      OpenDataSets([dmAppl.taWHAppl, dmAppl.taRejInv, dmAppl.taRejEl]);

      FDepFrom := ExecSelectSQL('select Name from D_Dep where DepId='+IntToStr(dmAppl.WhFrom), dmAppl.quTmp);
      plDepFrom.Caption := '��������� ��������: '+FDepFrom;
      FJobFrom := dmMain.taWOrderUserName.AsString;
      (* ������� �������� �������� ����� - �� ��������� *)
      with cmbxOper, Items do
        for i:=0 to Pred(Count) do
          if (TNodeData(Objects[i]).Code = dm.RejOperId)
          then begin
            ItemIndex := i;
            dmAppl.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
            ReOpenDataSet(dmAppl.taWHAppl);
            break;
          end;
      gridWhAppl.FieldColumns['OPERNAME'].Title.Caption := '��������';
    end;
    // �������� �������
    adtMdfArt : begin
      acDel.Visible := False;
      acAdd.Visible := False;
      acCreateArt2.Visible := False;
      acRej.Visible       := False;
      acRejDel.Visible    := True;
      acPAdd.Visible      := False;
      acPDel.Visible      := True;
      acAdd2.Visible      := False;
      acDel2.Visible      := False;
      acChangeSz.Visible  := False;
      acWhKind.Visible    := False;
      acChangeArt.Visible := False;
      acChangeArt2.Visible := False;
      tbdkInv.Visible      := False;
      plDepTo.Visible      := False;
      plHeaderInv.Visible  := False;
      titChange.Visible    := False;
      spitChangeArt.Visible := True;

      plRej.Visible := True;
      plRej.Align := alClient;

      plPInv.Visible := True;

      acStone.Visible := False;
      acShowStone.Visible := False;

      gridAEl.Visible := False;

      OpenDataSets([dmAppl.taWHAppl, dmAppl.taRejInv, dmAppl.taRejEl, dmAppl.taPInv, dmAppl.taPEl]);

      FDepFrom := ExecSelectSQL('select Name from D_Dep where DepId='+IntToStr(dmAppl.WhFrom), dmAppl.quTmp);
      plDepFrom.Caption := '��������� ��������: '+FDepFrom;
      FJobFrom := dmMain.taWOrderUserName.AsString;
      (* ������� �������� �������� ����� - �� ��������� *)
      with cmbxOper, Items do
        for i:=0 to Pred(Count) do
          if (TNodeData(Objects[i]).Code = dm.RejOperId)
          then begin
            ItemIndex := i;
            dmAppl.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
            ReOpenDataSet(dmAppl.taWHAppl);
            break;
          end;
      gridWhAppl.FieldColumns['OPERNAME'].Title.Caption := '��������';
    end;
    else EInternal.Create(Format(rsInternalError, ['209']));
  end;
  Self.Resize;
end;

procedure TfmADistr.FormClose(Sender: TObject; var Action: TCloseAction);
var
  s : integer;
begin
  (* �������� ���-�� ��� ������������� *)
  case dmAppl.ADistrType of
    adtWOrder : with dmAppl do begin
      taAEl.DisableControls;
      try
        taAEl.First;
        s := 0;
        while not taAEl.Eof do
        begin
          s := s + taAElQ.AsInteger*taAElU.AsInteger;
          taAEl.Next;
        end
      finally
        taAEl.EnableControls;
      end;
      // �������� �� ���������-������
      if taAPEl.Active then
      begin
        taAPEl.DisableControls;
        try
          taAPEl.First;
          while not taAPEl.Eof do
          begin
            s := s + taAPElQ.AsInteger;
            taAPEl.Next;
          end
        finally
          taAPEl.EnableControls;
        end;
      end
    end;
    adtDIEl : with dmAppl do begin
      taAEl.DisableControls;
      try
        taAEl.First;
        s := 0;
        while not taAEl.Eof do
        begin
          s := s + taAElQ.AsInteger*taAElU.AsInteger;
          taAEl.Next;
        end
      finally
        taAEl.EnableControls;
      end;
    end;
    adtSOEl : with dmAppl do begin
      taRejEl.DisableControls;
      try
        taRejEl.First;
        s := 0;
        while not taRejEl.Eof do
        begin
          s := s + taRejElQ.AsInteger*taRejElU.AsInteger;
          taRejEl.Next;
        end
      finally
        taRejEl.EnableControls;
      end;
    end;
    adtRej, adtMdfArt : with dmAppl do begin
      taRejEl.DisableControls;
      try
        taRejEl.First;
        s := 0;
        while not taRejEl.Eof do
        begin
          s := s + taRejElQ.AsInteger*taRejElU.AsInteger;
          taRejEl.Next;
        end;
      finally
        taRejEl.EnableControls;
      end;
    end;
    else raise EInternal.Create(Format(rsInternalError, ['209']));
  end;

  case dmAppl.ADistrType of
    adtDIEl : with dmMain do begin
      taDIEl.Edit;
      taDIElQ.AsInteger := s;
      taDIEl.Post;
    end;
    adtWOrder, adtRej, adtMdfArt : with dmMain do begin
      taWOrder.Edit;
      taWOrderQ.AsInteger := s;
      taWOrder.Post;
    end;
    adtSOEl : with dmMain do begin
      taSOEl.Edit;
      taSOElQ.AsInteger := s;
      taSOEl.Post;
    end;
    else raise Exception.Create('Internal error: Not initialize ADistrType');
  end;

  if dmAppl.taAEl.IsEmpty then
    if not dmAppl.taAInv.IsEmpty then dmAppl.taAInv.Delete;
  if dmAppl.taRejEl.IsEmpty then
    if not dmAppl.taRejInv.IsEmpty then dmAppl.taRejInv.Delete;
  if dmAppl.taPEl.IsEmpty then
    if not dmAppl.taPInv.IsEmpty then dmAppl.taPInv.Delete;
  with dmAppl do
    CloseDataSets([taWHAppl, taAInv, taAEl, taRejInv, taRejEl, taPInv, taPEl,
     taApplInv, taAPInv, taAPEl]);
  dmAppl.trAppl.Commit;
  inherited;
  //dmAppl.taAppl.SelectSQL[8] := FSaveOrderBy;
end;

procedure TfmADistr.acAddExecute(Sender: TObject);
var
  Id : Variant;
begin
  if ApplMode then raise Exception.Create(rsNotImplementation);
  if dmAppl.taWHAppl.IsEmpty then raise Exception.Create(Format(rsNoData, ['��� �����������']));

  case dmAppl.ADistrType of
    adtSOEl, adtRej : begin
      acRej.Execute;
      eXit;
    end;
    adtWOrder, adtDIEl : begin
      if(dmAppl.ADistrType = adtWorder)and(dmAppl.taWHApplART2.AsString <> '-')then
      begin
        acAdd2.Execute;
        eXit;
      end;

      if not dmAppl._ShowEditorQ(dmAppl.taWHApplQ.AsInteger, dmAppl.taWHApplU.AsInteger, dmAppl.taWHApplU.AsInteger, 0) then eXit;
      (* ��������� ���� �� ��������� ������� � ��������� *)

      Id := ExecSelectSQL('select Id from AEl where Art2Id='+IntToStr(dmAppl.taWHApplART2ID.AsInteger)+
         ' and InvId='+IntToStr(dmAppl.AInvId)+' and Sz='+IntToStr(dmAppl.taWHApplSZID.AsInteger)+' and '+
         ' OperIdFrom="'+dmAppl.taWHApplOPERID.AsString +'"', dmAppl.quTmp);
      if VarIsNull(Id)then Id:=0;
      with dmAppl do
      begin
        if(Id <> 0)then
        begin
          PostDataSet(taAEl);
          if taAEl.Locate('ART2ID;SZID;INVID', VarArrayOf([taWhApplART2ID.AsInteger,
                taWhApplSZID.AsInteger, AInvId]), []) then
          begin
            taAEl.Edit;
            taAElQ.AsInteger := taAElQ.AsInteger + dmAppl.ADistrQ;
            taAEl.Post;
          end
          else raise Exception.Create('null basile');
        end
        else begin
          taAEl.Append;
          try
            taAEl.Post;
          except
             on E: Exception do HandlePost_A(E);
          end;
        end;

        taAEl.Transaction.CommitRetaining;

        if taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([taAElART2ID.Asinteger,
             taAElSZID.AsInteger, taAElOPERIDFROM.AsString]), [])
        then taWHAppl.Refresh;
        
        ADistrQ := 0;
      end;
    end
  end;
end;

procedure TfmADistr.acDelExecute(Sender: TObject);
var
  Art2Id, Sz : integer;
  OperId : string;
begin
  if ApplMode then raise Exception.Create(rsNotImplementation);
  if (dmAppl.ADistrType = adtSOEl)or(dmAppl.ADistrType = adtRej) then
  begin
    acRejDel.Execute;
    eXit;
  end;
  PostDataSet(dmAppl.taAEl);
  if dmAppl.taAEl.IsEmpty then raise EWarning.Create(Format(rsNoData, ['']));
  (* ��������� ���� �� �������� ������? *)
  if ExecSelectSQL('select Result from Check_StoneArt('+dmAppl.taAElART2ID.AsString +', '+
          dmAppl.taAInvINVID.AsString+')', dmAppl.quTmp) <> 0
  then begin
    acDel2.Execute;
    eXit;
  end;

  if not dmAppl._ShowEditorQ(dmAppl.taAElQ.AsInteger, dmAppl.taAElU_OLD.AsInteger, dmAppl.taAElU.AsInteger, 1) then Exit;


  if dmAppl.taAElART2ID_OLD.IsNull then Art2Id := dmAppl.taAElART2ID.AsInteger
  else Art2Id := dmAppl.taAElART2ID_OLD.AsInteger;
  Sz := dmAppl.taAElSZID.AsInteger;
  OperId := dmAppl.taAElOPERIDFROM.AsString;
  if not dmAppl.taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([Art2Id, Sz, OperId]), []) then
    with dmAppl do
    begin
      if taAElART2ID_OLD.IsNull then
        InsertWhRecord(taAEl, taAElOPERIDFROM, taAElARTID, taAElART2ID, taAElSZID, taAElU_OLD, 0)
      else InsertWhRecord(taAEl, taAElOPERIDFROM, taAElARTID, taAElART2ID_OLD, taAElSZID, taAElU_OLD, 0);
    end;
  
  if(dmAppl.ADistrQ = dmAppl.taAElQ.AsInteger)then dmAppl.taAEl.Delete
  else begin
    dmAppl.taAEl.Edit;
    dmAppl.taAElQ.AsInteger := dmAppl.taAElQ.AsInteger - dmAppl.ADistrQ;
    dmAppl.taAEl.Post;
  end;
  dmAppl.taWHAppl.Refresh;
end;

procedure TfmADistr.acRejExecute(Sender: TObject);
begin
  if ApplMode then raise Exception.Create(rsNotImplementation);
  case dmAppl.ADistrType of
    adtDIEl : raise Exception.Create(rsNotImplementation);
    adtWOrder : begin
      case dmAppl.ItType of
        0 : // ������
          if MessageDialog('����� � ������������ ������� '+dmAppl.taWHApplART.AsString+' '+
            dmAppl.taWHApplART2.AsString+', ������ '+dmAppl.taWHApplSZ.AsString+' ?',
            mtConfirmation, [mbYes, mbNo], 0)<>mrYes then eXit;
        5 : // ����
          if MessageDialog('����� '+dmAppl.taWHApplART.AsString+' '+
            dmAppl.taWHApplART2.AsString+', ������ '+dmAppl.taWHApplSZ.AsString+
            ' ��� ������������ ����?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then eXit;
        else eXit;
      end;
    end;
    adtSOEl, adtRej : ;
  end;

  if not dmAppl._ShowEditorQ(dmAppl.taWHApplQ.AsInteger, dmAppl.taWHApplU.AsInteger, dmAppl.taWHApplU.AsInteger, 1) then eXit;
  if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty) then
  begin
    plRej.Visible := True;
    dmAppl.taRejInv.Append;
    dmAppl.taRejInv.Post;
    dmAppl.taRejInv.Transaction.CommitRetaining;
    ReOpenDataSets([dmAppl.taRejInv]);
  end;

  dmAppl.taRejEl.Append;
  try
    dmAppl.taRejElQ.AsInteger := dmAppl.ADistrQ;
    case dmAppl.ItType of
      0 : if(dmAppl.taWHApplOPERID.AsString = dm.RejOperId) then dmAppl.taRejElCOMMENTID.AsInteger := 6
          else if(dmAppl.taWHApplOPERID.AsString = dm.NkOperId)then dmAppl.taRejElCOMMENTID.AsInteger := 7
          else dmAppl.taRejElCOMMENTID.AsInteger := 3;
      5 : dmAppl.taRejElCOMMENTID.AsInteger := 4;
    end;
    dmAppl.taRejEl.Post;
  except
     on E : Exception do HandlePost_Rej(E);
  end;
  with dmAppl do
    if taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([taRejElART2ID.Asinteger,
       taRejElSZID.AsInteger, taRejElOPERID.AsString]), [])
    then taWHAppl.Refresh
    else raise EInternal.Create(Format(rsInternalError, ['200']));
end;

procedure TfmADistr.cmbxOper1Change(Sender: TObject);
begin
  dmAppl.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSets([dmAppl.taWHAppl]);
end;

procedure TfmADistr.acPAddExecute(Sender: TObject);
begin
  if ApplMode then raise Exception.Create(rsNotImplementation);
  case dmAppl.ADistrType of
    adtDIEl, adtSOEl, adtRej : raise Exception.Create(rsNotImplementation);
  end;
  case dmAppl.ItType of
    0 : // ������
      if MessageDialog('�������� � ������ �������?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then eXit;
    1 : // �����
      if MessageDialog('�������� ����� ���������?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then eXit;
    else eXit;
  end;

  if(dmAppl.taPInv.IsEmpty or (dmAppl.PInvid = 0)) then
  begin
    plPInv.Visible := True;
    dmAppl.taPInv.Append;
    dmAppl.taPInv.Post;
    ReOpenDataSet(dmAppl.taPInv);
  end;
  if ShowDbEditor(TfmePArt, dmAppl.dsrPEl, emBrowse, False) <> mrOk then eXit;

  with dmAppl do
    if taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([taPElART2ID.Asinteger,
         taPElSZID.AsInteger, taPElOPERID.AsString]), [])
    then taWHAppl.Refresh
    else begin
      { TODO : �������� � WhAppl ����� ������� }
      ReOpenDataSet(dmAppl.taWHAppl);
      taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([taPElART2ID.Asinteger,
          taPElSZID.AsInteger, taPElOPERID.AsString]), []);
    end;
end;

procedure TfmADistr.acAdd2Execute(Sender: TObject);
begin
  if ApplMode then raise Exception.Create(rsNotImplementation);
  case dmAppl.ADistrType of
    adtDIEl, adtSOEl, adtRej : raise Exception.Create(rsNotImplementation);
  end;
  if dmAppl.taWHAppl.IsEmpty then raise Exception.Create(Format(rsNoData, ['��� �����������']));
  { TODO : ��������� ������ � ��. ���. }
  //dm.AArtId := dmAppl.taWHApplARTID.AsInteger;
  if not dmAppl._ShowEditorQ(dmAppl.taWHApplQ.AsInteger, dmAppl.taWHApplU.AsInteger, dmAppl.taWHApplU.AsInteger, 0) then eXit;

  dm.AArt2Id := dmAppl.taWHApplART2ID.AsInteger;
  dmAppl.WhApplId := dmAppl.taWHApplID.AsInteger;
  (* �������� ������ ������ ������ *)
  if dm.LocalSetting.UseStoneDialog then ShowAndFreeForm(TfmAIns);
  case dmAppl.ADistrKind of
    1 : begin
      (* ��������� ������� ������� ���� �� ��������� ������������ �� ������� �������� 2 *)
      ExecSQL('execute procedure CopyStoneToIns('+IntToStr(dmAppl.wOrderId)+','+
        dmAppl.taWHApplART2ID.AsString+')', dmAppl.quTmp);
      (* �������� ������ �� ��������� *)
      if ShowDbEditor(TfmeIns, dm.dsrA2Ins) <> mrOk then eXit;
    end;
  end;

  with dmAppl do
  begin
    PostDataSet(taAEl);
    taAEl.Insert;
    try
      taAElART2ID.AsInteger := dm.AArt2Id;
      dmAppl.taAElU_OLD.AsInteger := dmAppl.taWHApplU.AsInteger;
      dmAppl.taAEl.Post;
    except
      on E: Exception do HandlePost_A(E);
    end;
    if taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([dm.AArt2Id,
          taAElSZID.AsInteger, taAElOPERIDFROM.AsString]), [])
    then taWHAppl.Refresh;
  end;

{  (* �������� ������ �� ��������� *)
  if ShowDbEditor(TfmeIns, dm.dsrA2Ins) = mrOk then
    with dmAppl do
    begin
      PostDataSet(taAEl);
      dmAppl.taAEl.Insert;
      try
        dmAppl.taAElART2ID.AsInteger := dm.AArt2Id;
        //dmAppl.taAElART2ID_OLD.AsInteger := dmAppl.taWHApplART2ID.AsInteger;
        dmAppl.taAElU_OLD.AsInteger := dmAppl.taWHApplU.AsInteger;
        dmAppl.taAEl.Post;
      except
        on E: Exception do HandlePost(E);
      end;
      if taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([dm.AArt2Id,
          taAElSZID.AsInteger, taAElOPERIDFROM.AsString]), [])
      then taWHAppl.Refresh;
    end;}
end;

procedure TfmADistr.acStoneExecute(Sender: TObject);
begin
  if ApplMode then raise Exception.Create('No impementation');
  case dmAppl.ADistrType of
    adtDIEl, adtSOEl, adtRej : raise Exception.Create('No impementation');
  end;
  if(dmAppl.taAEl.IsEmpty) then raise Exception.Create('��������� �����');
  dmAppl.WhApplId := dmAppl.taWHApplID.AsInteger;
  dm.AArt2Id := dmAppl.taAElART2ID.AsInteger;
  if dm.LocalSetting.UseStoneDialog then ShowAndFreeForm(TfmAIns);
  if acShowStone.Checked then ReOpenDataSet(dmAppl.taAIns);
end;

procedure TfmADistr.GridPost(Sender: TObject);
begin
  PostDataSet(TDbGridEh(Sender).DataSource.DataSet);
end;

procedure TfmADistr.acPDelExecute(Sender: TObject);
var
  Art2Id, Sz : integer;
  OperId : string;
begin
  if ApplMode then raise Exception.Create(rsNotImplementation);
  case dmAppl.ADistrType of
    adtDIEl, adtSOEl, adtRej : raise Exception.Create(rsNotImplementation);
  end;
  PostDataSet(dmAppl.taPEl);
  if dmAppl.taPEl.IsEmpty then
    raise Exception.Create('��������� �����');
  Art2Id := dmAppl.taPElART2ID.AsInteger;
  Sz := dmAppl.taPElSZID.AsInteger;
  OperId := dmAppl.taPElOPERID.AsString;
  dmAppl.ADistrQ := dmAppl.taPElQ.AsInteger;
  if not dmAppl.taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([Art2Id, Sz, OperId]), [])
  then
    with dmAppl do
      InsertWhRecord(taPEl, taPElOPERID, taPElARTID, taPElART2ID, taPElSZID, taPElU, 1);
  dmAppl.taPEl.Delete;
  dmAppl.taWHAppl.Refresh;
end;

procedure TfmADistr.acRejDelExecute(Sender: TObject);
var
  Art2Id, Sz : integer;
  OperId : string;
begin
  if ApplMode then raise Exception.Create('No impementation');
  case dmAppl.ADistrType of
    adtDIEl : raise Exception.Create('No impementation');
  end;
  PostDataSet(dmAppl.taRejEl);
  Art2Id := dmAppl.taRejElART2ID.AsInteger;
  Sz := dmAppl.taRejElSZID.AsInteger;
  OperId := dmAppl.taRejElOPERID.AsString;
  dmAppl.ADistrQ := dmAppl.taRejElQ.AsInteger;
  if not dmAppl.taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([Art2Id, Sz, OperId]), [])
  then
    with dmAppl do
      InsertWhRecord(taRejEl, taRejElOPERID, taRejElARTID, taRejElART2ID, taRejElSZID, taRejElU, 0);
  dmAppl.taRejEl.Delete;
  dmAppl.taWHAppl.Refresh;
end;

procedure TfmADistr.InsertWhRecord(DataSet: TDataSet; OperId, ArtId, Art2Id, SzId, U : TField; T : byte);
var
  r : Variant;
begin
  (* ��������� ������ *)
  if OperId.IsNull then raise Exception.Create('Debug. InsertWhRecord OperId is null');
  if ArtId.IsNull then raise Exception.Create('Debug. InsertWHRecord ArtId is null');
  if Art2Id.IsNull then raise Exception.Create('Debug. InsertWHRecord Art2Id is null');
  if SzId.IsNull then raise Exception.Create('Debug. InsertWhRecord SzId is null');
  if U.IsNull then raise Exception.Create('Debug. InsertWhRecord UnitId is null');
  (* �������� Id � ������ ��������� *)
  r := ExecSelectSQL('select Id from WhAppl where PsId='+IntToStr(dmAppl.WhFrom)+
     ' and OperId="'+OperId.AsString+'" and Art2Id='+Art2Id.AsString+' and Sz='+
     DataSet.FieldByName('SZID').AsString+' and U='+U.AsString, dmAppl.quTmp);
  if VarIsNull(r) then
    try
      dmAppl.NewWhApplId := dm.GetId(76);
      dm.quTmp.Close;
      dm.quTmp.SQL.Text := 'insert into WhAppl(ID,PSID,OPERID,ART2ID,SZ,T,Q,U,SELFCOMPID)'+
                           'values(:ID,:PSID,:OPERID,:ART2ID,:SZ,:T,:Q,:U,:SELFCOMPID)';
      dm.quTmp.Prepare;
      dm.quTmp.ParamByName('ID').AsInteger := dmAppl.NewWhApplId;
      dm.quTmp.ParamByName('PSID').AsInteger := dmAppl.WhFrom;
      dm.quTmp.ParamByName('OPERID').AsString := OperId.AsString;
      dm.quTmp.ParamByName('ART2ID').AsInteger := Art2Id.AsInteger;
      dm.quTmp.ParamByName('SZ').AsInteger := DataSet.FieldByName('SZID').AsInteger;
      dm.quTmp.ParamByName('T').AsInteger := T;
      dm.quTmp.ParamByName('Q').AsInteger := 0;
      dm.quTmp.ParamByName('U').AsInteger := U.AsInteger;
      dm.quTmp.ParamByName('SELFCOMPID').AsInteger := dm.User.SelfCompId;
      dm.quTmp.ExecQuery;
      dm.quTmp.Transaction.CommitRetaining;
      dm.quTmp.Close;
    except
      Exception.Create('Debug. InsertWhRecord Add new record');
    end
  else dmAppl.NewWhApplId := r;

  with dmAppl do
  try
    taWhAppl.Insert;
    taWHApplPSID.AsInteger := dmAppl.WhFrom;
    if OperId.IsNull then
      raise Exception.Create('������ ��� ��������� ���������� ������ � dmAppl.taWHAppl OperId is null');
    taWHApplOPERID.AsString := OperId.AsString;
    //taWHApplOPERNAME.AsString := OperName.AsString;
    if ArtId.IsNull then
      raise Exception.Create('������ ��� ��������� ���������� ������ � dmAppl.taWHAppl ArtId is null');
    taWhApplARTID.AsInteger := ArtId.AsInteger;
    //taWHApplART.AsString := DataSet.FieldByName('ART').AsString;
    if Art2Id.IsNull then
      raise Exception.Create('������ ��� ��������� ���������� ������ � dmAppl.taWHAppl ArtId is null');
    taWHApplART2ID.AsInteger := Art2Id.AsInteger;
    //taWHApplART2.AsString := Art2.AsString;
    if SzId.IsNull then
      raise Exception.Create('������ ��� ��������� ���������� ������ � dmAppl.taWhAppl SzId is null');
    taWHApplSZID.AsInteger := SzId.AsInteger;// DataSet.FieldByName('SZID').AsInteger;
    //taWHApplSZ.AsString := DataSet.FieldByName('SZ').AsString;
    if U.IsNull then
      raise Exception.Create('������ ��� ��������� ���������� ������ � dmAppl.taWHAppl U is null');
    taWHApplU.AsInteger := U.AsInteger;
    if T=0 then taWHApplQ.AsInteger := dmAppl.ADistrQ // �������� � ������
    else if T=1 then taWHApplQ.AsInteger := taWHApplQ.AsInteger - dmAppl.ADistrQ // ����� � ������, ����� �����������
    else raise Exception.Create('Debug. InsertWhRecord T value is '+IntToStr(T));
    taWHAppl.Post;
  except
    on E:Exception do begin
      MessageDialog('������ ��� ��������� ���������� ������ � ������� dmAppl.WHAppl'#13+E.Message, mtError, [mbOk], 0);
      raise;
    end;
  end;
end;

procedure TfmADistr.acChangeArtExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ARTID'; sName: 'ART'; sTable: 'D_ART'; sWhere: '');
var
  NewArtId, NewSzId, NewU : integer;
  NewOperId : string;
  NewArt2Id : Variant;
begin
  if ApplMode then raise Exception.Create(rsNotImplementation);
  case dmAppl.ADistrType of
    {adtDIEl,} adtSOEl, adtRej : raise Exception.Create(rsNotImplementation);
  end;
  {dArt.FindArtId := dmAppl.taWHApplARTID.AsInteger;
  dArt.FindArt := dmAppl.taWHApplART.AsString;
  if ShowDictForm(TfmArt, dm.dsrArt, Rec, '���������� ���������',  dmSelRecordEdit, nil)<>mrOk then eXit;}
  // ��������� �� ���������� ���������� �����, �� ���� ���� ����� ������������ �����
  if dmAppl.taWHApplARTID.IsNull then raise EInternal.Create(Format(rsInternalError, ['205']));
  if dmAppl.taWHApplART2ID.IsNull then raise EInternal.Create(Format(rsInternalError, ['201']));
  if dmAppl.taWHApplSZID.IsNull then raise EInternal.Create(Format(rsInternalError, ['202']));
  if dmAppl.taWHApplOPERID.IsNull then raise EInternal.Create(Format(rsInternalError, ['203']));
  if dmAppl.taWHApplU.IsNull then raise EInternal.Create(Format(rsInternalError, ['204']));

  if ShowEditor(TfmeArt, TfmEditor(fmeArt))<>mrOk then eXit;
  NewArtId := Editor.vResult;
  if(dmAppl.taWHApplART2.AsString = '-')then
    NewArt2Id := ExecSelectSQL('select Art2Id from Art2 where D_ArtId='+IntToStr(NewArtId)+' and Art2="-"', dmAppl.quTmp)
  else
    NewArt2Id := ExecSelectSQL('select Art2Id from Dubl_A2_2('+dmAppl.taWHApplART2ID.AsString+', '+IntToStr(NewArtId)+')', dmMain.quTmp);
  if VarIsNull(NewArt2Id)or(NewArt2Id = 0) then raise EInternal.Create(Format(rsInternalError, ['001']));
  NewSzId := dmAppl.taWHApplSZID.AsInteger;
  if (dmAppl.ADistrType = adtMdfArt) then
  NewOperId := dm.ModifyAssortOperId
  else NewOperId := dmAppl.taWHApplOPERID.AsString;
  NewU := dmAppl.taWHApplU.AsInteger;

  if not dmAppl._ShowEditorQ(dmAppl.taWhApplQ.AsInteger, dmAppl.taWhApplU.AsInteger, dmAppl.taWhApplU.AsInteger, 1) then eXit;
  (* ����� � ������ ������� WhApplArt2Id *)
  if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty) then
  begin
    plRej.Visible := True;
    dmAppl.taRejInv.Append;
    dmAppl.taRejInv.Post;
    ReOpenDataSet(dmAppl.taRejInv);
  end;
  if not dmAppl.taRejEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([dmAppl.taWHApplART2ID.AsInteger, dmAppl.taWHApplSZID.AsInteger,
    dmAppl.taWHApplOPERID.AsString, dmAppl.taWHApplU.AsInteger]), [])then
  begin
    dmAppl.taRejEl.Append;
    try
//      dmAppl.taRejElQ.AsInteger := dmAppl.ADistrQ;
//      dmAppl.taRejElOPERID.AsString := dmAppl.taWHApplOPERID.AsString;
//      dmAppl.taRejElARTID.AsInteger := dmAppl.taWHApplARTID.AsInteger;
//      dmAppl.taRejElART2ID.AsInteger := dmAppl.taWHApplART2ID.AsInteger;
//      dmAppl.taRejElSZID.AsInteger := dmAppl.taWHApplSZID.AsInteger;
//      dmAppl.taRejElCOMMENTID.AsInteger := 2; 
//      showmessage(dmAppl.taRejELOperID.AsString);
//      Form:= TForm5.Create(nil);
//      Form.ShowModal;
      dmAppl.taRejEl.Post;
    except
      on E : Exception do HandlePost_Rej(E);
    end;
  end
  else
  try
    dmAppl.taRejEl.Edit;
    dmAppl.taRejElQ.AsInteger := dmAppl.taRejElQ.AsInteger+dmAppl.ADistrQ;
    dmAppl.taRejEl.Post;
  except
    on E: Exception do HandlePost_Rej(E);
  end;
  dmAppl.taWHAppl.Refresh;
  (* �������� � ������ ������� *)
  if(dmAppl.taPInv.IsEmpty or (dmAppl.PInvid = 0)) then
  begin
    plPInv.Visible := True;
    dmAppl.taPInv.Append;
    dmAppl.taPInv.Post;
    ReOpenDataSets([dmAppl.taPInv]);
  end;
  if not dmAppl.taPEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([NewArt2Id, NewSzId, NewOperId, NewU]), []) then
  begin
    dmAppl.taPEl.Append;
    try
      dmAppl.taPElQ.AsInteger := dmAppl.ADistrQ;
      dmAppl.taPElARTID.AsInteger := NewArtId;
      dmAppl.taPElOPERID.AsString := NewOperId;
      dmAppl.taPElSZID.AsInteger := NewSzId;
      dmAppl.taPElART2ID.AsInteger := NewArt2Id;
      dmAppl.taPElU.AsInteger := NewU;
      //Form.ShowModal;
      dmAppl.taPElCOMMENTID.AsInteger := 2;
      dmAppl.taPEl.Post;
    except
      on E : Exception do HandlePost_P(E);
    end;
  end
  else try
    dmAppl.taPEl.Edit;
    dmAppl.taPElQ.AsInteger := dmAppl.taPElQ.AsInteger + dmAppl.ADistrQ;
    dmAppl.taPEl.Post;
  except
    on E:Exception do HandlePost_P(E);
  end;
  with dmAppl do
    if not taWHAppl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([NewArt2Id, NewSzId, NewOperId, NewU]), [])
    then InsertWhRecord(taPEl, taPElOPERID, taPElARTID, taPElART2ID, taPElSZID, taPElU, 0)
    else taWHAppl.Refresh;
end;

procedure TfmADistr.acCreateArt2Execute(Sender: TObject);
var
  U : Variant;
begin
  if ApplMode then raise Exception.Create(rsNotImplementation);
  case dmAppl.ADistrType of
    adtDIEl, adtSOEl, adtRej : raise Exception.Create(rsNotImplementation);
  end;
  if dmAppl.taWHAppl.IsEmpty then raise Exception.Create(Format(rsNoData, ['��� �����������']));
  {U := ExecSelectSQL('select UnitId from D_Art where D_ArtId='+dmAppl.taWHApplARTID.AsString, dmAppl.quTmp);
  if VarIsNull(U) then raise EInternal.Create(Format(rsInternalError, ['002']));
  inc(U);}
  if not dmAppl._ShowEditorQ(1, 1, 1, 2) then eXit;
  (* ������� ����� ������� Art2Id *)
  dm.AArt2Id := ExecSelectSQL('select Art2Id from Dubl_A2('+IntToStr(dmAppl.taWHApplART2ID.AsInteger)+')', dmAppl.quTmp);
  PostDataSet(dmAppl.taAEl);
  dmAppl.taAEl.Insert;
  try
    dmAppl.taAElART2ID.AsInteger := dm.AArt2Id;
    dmAppl.taAElART2ID_OLD.AsInteger := dmAppl.taWHApplART2ID.AsInteger;
    dmAppl.taAEl.Post;
    dmAppl.taWHAppl.Refresh;
  except
    on E: Exception do ShowMessage(E.Message);
  end;
  (* �������� ������ �������� ������ *)
  if dm.LocalSetting.UseStoneDialog then ShowAndFreeForm(TfmAIns);
end;

procedure TfmADistr.acPrintWhExecute(Sender: TObject);
begin
  if ApplMode then raise Exception.Create('No impementation');
  dmPrint.PrintDocumentA(dkWhAppl, Null);
end;

procedure TfmADistr.acPrintInvExecute(Sender: TObject);
begin
  dm.tr.CommitRetaining;
  dmPrint.PrintDocumentA(dkAInv, null);
end;

procedure TfmADistr.acChangeSzExecute(Sender: TObject);
var
  NewArtId, NewArt2Id, NewSzId, NewU : integer;
  NewOperId : string;
begin
  if ApplMode then raise Exception.Create(rsNotImplementation);
  case dmAppl.ADistrType of
    {adtDIEl,} adtSOEl, adtRej : raise Exception.Create(rsNotImplementation);
  end;
  if ShowEditor(TfmeSz, TfmEditor(fmeSz))<>mrOk then eXit;
  if(ResultSz = dmAppl.taWHApplSZID.AsInteger)then raise EWarning.Create('������ �� �������!');

  // ��������� �� ���������� ���������� �����, �� ���� ���� ����� ������������ �����
  if dmAppl.taWHApplARTID.IsNull then raise EInternal.Create(Format(rsInternalError, ['205']));
  if dmAppl.taWHApplART2ID.IsNull then raise EInternal.Create(Format(rsInternalError, ['201']));
  if dmAppl.taWHApplSZID.IsNull then raise EInternal.Create(Format(rsInternalError, ['202']));
  if dmAppl.taWHApplOPERID.IsNull then raise EInternal.Create(Format(rsInternalError, ['203']));
  if dmAppl.taWHApplU.IsNull then raise EInternal.Create(Format(rsInternalError, ['204']));

  NewArtId := dmAppl.taWHApplARTID.AsInteger;
  NewArt2Id := dmAppl.taWHApplART2ID.AsInteger;
  NewSzId := ResultSz;
  NewOperId := dmAppl.taWHApplOPERID.AsString;
  NewU := dmAppl.taWHApplU.AsInteger;
  if not dmAppl._ShowEditorQ(dmAppl.taWHApplQ.AsInteger, dmAppl.taWHApplU.AsInteger, dmAppl.taWHApplU.AsInteger, 1) then eXit;
  (* ����� � ������ ������� WhApplArt2Id *)
  if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty) then
  begin
    plRej.Visible := True;
    dmAppl.taRejInv.Append;
    dmAppl.taRejInv.Post;
    dmAppl.taRejInv.Transaction.CommitRetaining;
    ReOpenDataSet(dmAppl.taRejInv);
  end;

  // ���� ������ � ���������
  if not dmAppl.taRejEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([dmAppl.taWHApplART2ID.AsInteger,
    dmAppl.taWHApplSZID.AsInteger, dmAppl.taWHApplOPERID.AsString, dmAppl.taWHApplU.AsInteger]), []) then
  begin
    dmAppl.taRejEl.Insert;
    try
      dmAppl.taRejElQ.AsInteger := dmAppl.ADistrQ;
      dmAppl.taRejElARTID.AsInteger := dmAppl.taWHApplARTID.AsInteger;
      dmAppl.taRejElOPERID.AsString := dmAppl.taWHApplOPERID.AsString;
      dmAppl.taRejElSZID.AsInteger := dmAppl.taWHApplSZID. AsInteger;
      dmAppl.taRejElART2ID.AsInteger := dmAppl.taWHApplART2ID.AsInteger;
      dmAppl.taRejElCOMMENTID.AsInteger := 1;
      dmAppl.taRejEl.Post;
    except
      on E : Exception do HandlePost_Rej(E);
    end;
  end
  else try
    dmAppl.taRejEl.Edit;
    dmAppl.taRejElQ.AsInteger := dmAppl.taRejElQ.AsInteger + dmAppl.ADistrQ;
    dmAppl.taRejEl.Post;
  except
    on E:Exception do HandlePost_Rej(E);
  end;
  dmAppl.taWHAppl.Refresh;
  (* �������� � ������ ������� *)
  if(dmAppl.taPInv.IsEmpty or (dmAppl.PInvid = 0)) then
  begin
    plPInv.Visible := True;
    dmAppl.taPInv.Append;
    dmAppl.taPInv.Post;
    ReOpenDataSets([dmAppl.taPInv]);
  end;
  if not dmAppl.taPEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([NewArt2Id, NewSzId, NewOperId, NewU]), []) then
  begin
    dmAppl.taPEl.Append;
    try
      dmAppl.taPElQ.AsInteger := dmAppl.ADistrQ;
      dmAppl.taPElARTID.AsInteger := NewArtId;
      dmAppl.taPElART2ID.AsInteger := NewArt2Id;
      dmAppl.taPElOPERID.AsString := NewOperId;
      dmAppl.taPElSZID.AsInteger := NewSzId;
      dmAppl.taPElU.AsInteger := NewU;
      dmAppl.taPElCOMMENTID.AsInteger := 1;
      dmAppl.taPEl.Post;
    except
      on E : Exception do HandlePost_P(E);
    end;
  end
  else try
    dmAppl.taPEl.Edit;
    dmAppl.taPElQ.AsInteger := dmAppl.taPElQ.AsInteger + dmAppl.ADistrQ;
    dmAppl.taPEl.Post;
  except
    on E:Exception do HandlePost_P(E);
  end;
  // �������� �����
  with dmAppl do
    if taWHAppl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([NewArt2Id, NewSzId, NewOperId, NewU]), []) then taWHAppl.Refresh
    else try
      InsertWhRecord(taPEl, taPElOPERID, taPElARTID, taPElART2ID, taPElSZID, taPElU, 0);
    except
      on E:Exception do
        MessageDialog('������ ��� ������ ������� InsertWhRecord � ������ acChangeSzExecute', mtError, [mbOk], 0);
    end;
end;

procedure TfmADistr.acWhKindExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'DOCNO'; sTable: ''; sWhere: '');
var
  v : Variant;
begin
  if not(dmAppl.ItType in [1,3,5,7])then eXit;
  case dmAppl.ADistrType of
    adtDIEl, adtSOEl, adtRej : raise Exception.Create(rsNotImplementation);
  end;

  if ApplMode then
  begin
    acWhKind.Caption := '�����';
    acWhKind.ImageIndex := -1;
    with dmAppl do
      CloseDataSets([taAppl, taApplInv, taAPEl, taAPInv]);
  end
  else with dmAppl do begin
    ApplOper := dmMain.taWOrderListDEFOPER.AsString;
    (* ������� ������ *)
    if(dmMain.taWOrderAPPLINV.IsNull)then
    begin
      OpenDataSet(taAProdList);
      if(taAProdList.IsEmpty)then raise EWarning.Create('��� ������');
      taAProdList.Last;
      if(taAProdList.RecordCount > 1)then
        if ShowDictForm(TfmdAList, dsrAProdList, Rec, '����� ������',
           dmSelRecordReadOnly)<>mrOk then eXit
        else ApplInvId := vResult
      else ApplInvId := taAProdListID.AsInteger;
      CloseDataSet(taAProdList);
    end
    else ApplInvId := ExecSelectSQL('select Ref from Inv where InvId='+
           dmMain.taWOrderAPPLINV.AsString, quTmp);

    (* ������� �� ���������? *)
    v := ExecSelectSQL('select InvId from Inv where InvId='+
        IntToStr(dmMain.taWOrderAPPLINV.AsInteger)+' and Ref='+IntToStr(ApplInvId), quTmp);
    if VarIsNull(v) then APInvId := 0 else APInvId := v; 
    if(APInvId = 0)
    then begin
      dmMain.taWOrder.Edit;
      dmMain.taWOrderAPPLINV.AsVariant := Null;
      dmMain.taWOrder.Post;
      dmMain.taWOrder.Transaction.CommitRetaining;
    end;
    if dmMain.taWOrderAPPLINV.IsNull then
    begin
      if dmAppl.trAppl.Active then dmAppl.trAppl.CommitRetaining
      else dmAppl.trAppl.StartTransaction;
      OpenDataSet(dmAppl.taAPInv);
      taAPInv.Append;
      taAPInvREF.AsInteger := ApplInvId;
      dmMain.taWOrder.Edit;
      dmMain.taWOrderAPPLINV.AsInteger := dmAppl.taAPInvINVID.AsInteger;
      dmAppl.APInvId := dmAppl.taAPInvINVID.AsInteger;

      dmMain.taWOrder.Post;
      dmAppl.taAPInv.Post;
      dmAppl.taAPInv.Transaction.CommitRetaining;
      dmMain.taWOrder.Transaction.CommitRetaining;
    end;
    CloseDataSets([taAPInv, taAPEl]);

    OpenDataSet(taApplInv);
    taAppl.Filtered := True;
    ReOpenDataSet(taAppl);   // ������� ������� ������
    InitBtn(Boolean(taApplInvISPROCESS.AsInteger));
    // ������� ���������
    OpenDataSets([taAPInv, taAPEl]);
    acWhKind.Caption := '�����';
    acWhKind.ImageIndex := 6;
  end;
  ApplMode := not ApplMode;
end;

procedure TfmADistr.SetApplMode(const Value: boolean);
begin
  if(Value = FApplMode)then eXit;
  if Value then
  begin
    plInv.Visible := False;
    plBtn.Visible := False;
    plWh.Visible := False;
    acNewAppl.Visible := True;
    spitAppl.Visible := True;
    plApplProd.Align := alClient;
    plApplProd.Visible := True;
  end
  else begin
    plApplProd.Visible := False;
    plWh.Visible := True;
    plBtn.Visible := True;
    plInv.Visible := True;
    acNewAppl.Visible := False;
    spitAppl.Visible := False;
  end;
  FApplMode := Value;
end;

procedure TfmADistr.acAddApplExecute(Sender: TObject);
begin
  if not ApplMode then raise Exception.Create(rsNotImplementation);
  case dmAppl.ADistrType of
    adtDIEl : raise Exception.Create(rsNotImplementation);
  end;
  PostDataSets([dmAppl.taAppl, dmAppl.taApplInv, dmAppl.taAPEl]);
  if(dmAppl.taApplInvISPROCESS.AsInteger = 1)then raise Exception.Create('������ ����������');
  if dmAppl.taAppl.IsEmpty then raise Exception.Create('��� ������ ��� �����������');

  {dmAppl.ADistrQ := (dmAppl.taApplQ.AsInteger - dmAppl.taApplFQ.AsInteger);
  if(ShowEditor(TfmeQ, TfmEditor(fmeQ)) <> mrOk)then eXit;}
  if not dmAppl._ShowEditorQ(dmAppl.taApplQ.AsInteger - dmAppl.taApplFQ.AsInteger, 1, 1, 1) then eXit;
  with dmAppl do
  begin
    (* ��������� ���� �� ��������� ������� � ��������� *)
    PostDataSet(taAPEl);
    if taAPEl.Locate('ART2ID;SZID;INVID', VarArrayOf([taApplART2ID.AsInteger,
           taApplSZ.AsInteger, APInvId]), [])
    then begin
      // ������������� ���������
      taAPEl.Edit;
      taAPElQ.AsInteger := taAPElQ.AsInteger + ADistrQ;
      taAPEl.Post;
    end
    else begin
      // �������� � ���������
      taAPEl.Append;
      //taAPElOPERID.AsString := dmAppl.ApplOper;
      try
        taAPEl.Post;
      except
         on E: Exception do HandlePost_AP(E);
      end;
    end;
    trAppl.CommitRetaining;
    taAppl.Refresh;
    ADistrQ := 0;
  end;
end;

procedure TfmADistr.acDelApplExecute(Sender: TObject);
var
  Art2Id, Sz : integer;
begin
  if(not ApplMode)then raise Exception.Create('No impementation');
  case dmAppl.ADistrType of
    adtDIEl : raise Exception.Create('No impementation');
  end;
  PostDataSets([dmAppl.taAppl, dmAppl.taApplInv, dmAppl.taAPEl]);
  if(dmAppl.taApplInvISPROCESS.AsInteger = 1)then raise Exception.Create('������ ����������');
  if(dmAppl.taAPEl.IsEmpty)then raise Exception.Create('��� ������ ��� �����������');
  with dmAppl do
  begin
    PostDataSet(taAPEl);
    {ADistrQ := taAPElQ.AsInteger;
    if(ADistrQ <> 1)and(ShowEditor(TfmeQ, TfmEditor(fmeQ)) <> mrOk)then eXit;}
    if _ShowEditorQ(taAPElQ.AsInteger, taAPElU.AsInteger, taAPElU.AsInteger, 1) then eXit;
    Art2Id := taAPElART2ID.AsInteger;
    Sz := taAPElSZID.AsInteger;
    // �������� ���������
    if(ADistrQ = taAPElQ.AsInteger)then taAPEl.Delete
    else begin
      taAPEl.Edit;
      taAPElQ.AsInteger := taAPElQ.AsInteger - ADistrQ;
      taAPEl.Post;
    end;
    // �������� ������
    if taAppl.Locate('ART2ID;SZ', VarArrayOf([Art2Id, Sz]), []) then taAppl.Refresh
    else raise Exception.Create('acDelApplExecute 1');
    ADistrQ := 0;
  end;
end;

procedure TfmADistr.gridApplDblClick(Sender: TObject);
begin
  acAddAppl.Execute;
end;

procedure TfmADistr.gridApplKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_SPACE  : acAddAppl.Execute;
  end;
end;

procedure TfmADistr.acCloseApplExecute(Sender: TObject);
begin
  if(not ApplMode)then raise Exception.Create('No impementation');
  case dmAppl.ADistrType of
    adtDIEl : raise Exception.Create('No impementation');
  end;
  with dmAppl do
  begin
    PostDataSets([taAppl, taAProdList]);
    if(acCloseAppl.Caption = '������')then
      try
        taApplInv.Edit;
        taApplInvISPROCESS.AsInteger := 1;
        taApplInv.Post;
        acCloseAppl.Caption := '������';
        acCloseAppl.Hint := '������� ��������';
        acCloseAppl.ImageIndex := 9;
        RefreshDataSet(taApplInv);
      except
        on E: Exception do
        { TODO : !!! }
        //if not TranslateIbMsg(E, taApplInv.Database, taApplInv.Transaction)
            // then
            ShowMessage(E.Message);
      end
    else
      try
        taApplInv.Edit;
        taApplInvISPROCESS.AsInteger := 0;
        taApplInv.Post;
        acCloseAppl.Caption := '������';
        acCloseAppl.Hint := '������� ��������';
        acCloseAppl.ImageIndex := 10;
        RefreshDataSet(taApplInv);
      except
        on E: Exception do
        { TODO : !!! }
        //if not TranslateIbMsg(E, taApplInv.Database, taApplInv.Transaction)
          //   then
          ShowMessage(E.Message);
      end
  end
end;

procedure TfmADistr.InitBtn(T: boolean);
begin
  if not T then // �������� ������
  begin
    acCloseAppl.Caption := '������';
    acCloseAppl.Hint := '������� ��������';
    acCloseAppl.ImageIndex := 10;
  end
  else begin
    acCloseAppl.Caption := '������';
    acCloseAppl.Hint := '������� ��������';
    acCloseAppl.ImageIndex := 9;
  end
end;

procedure TfmADistr.edFindArtKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_DOWN, VK_RETURN : ActiveControl := gridAppl;
  end;
end;

procedure TfmADistr.acNewApplExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'DOCNO'; sTable: ''; sWhere: '');
begin
  if not ApplMode then exit;
  {if(dmAppl.taAPEl.RecordCount<>0)or(not dmAppl.taAPEl.IsEmpty) then
    raise EWarning.Create('���������� ������� ������� �� ���������');}
  if MessageDialog('������� ����� ������?', mtConfirmation, [mbOK, mbCancel], 0)<>mrOk then eXit;
  with dmMain, dmAppl do
  begin
    if not(taWOrder.State in [dsEdit, dsInsert]) then taWOrder.Edit;
    taWOrderAPPLINV.AsVariant := Null;
    taWOrder.Post;
    OpenDataSet(taAProdList);
    if(taAProdList.IsEmpty)then raise EWarning.Create(rsNoAppl);
    taAProdList.Last;
    if(taAProdList.RecordCount > 1)then
      if ShowDictForm(TfmdAList, dsrAProdList, Rec, '����� ������', dmSelRecordReadOnly)<>mrOk then eXit
      else ApplInvId := vResult
    else ApplInvId := taAProdListID.AsInteger;
    CloseDataSet(taAProdList);
  end;
end;

procedure TfmADistr.acStoneUpdate(Sender: TObject);
begin
  (Sender as TAction).Visible := not ( ApplMode or
                                       (dmAppl.ADistrType in [adtDIEl, adtSOEl, adtRej])
                                      );
  (Sender as TAction).Enabled := not (dmAppl.taAEl.IsEmpty or dmAppl.taAEl.IsEmpty);
end;

procedure TfmADistr.acInvArt2InsExecute(Sender: TObject);
begin
  dm.AArt2Id := dmAppl.taAElART2ID.AsInteger;
  ShowDbEditor(TfmeIns, dm.dsrA2Ins);
end;

procedure TfmADistr.acInvArt2InsUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (dmAppl.taAElART2.AsString <> '-') and
                                 (not dmAppl.taAEl.IsEmpty);
end;

procedure TfmADistr.acWhArt2InsExecute(Sender: TObject);
begin
  dm.AArt2Id := dmAppl.taWHApplART2ID.AsInteger;
  ShowDbEditor(TfmeIns, dm.dsrA2Ins);
end;

procedure TfmADistr.acWhArt2InsUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (dmAppl.taWhApplART2.AsString <> '-') and
                                 (not dmAppl.taWhAppl.IsEmpty);
end;

procedure TfmADistr.acDel2Execute(Sender: TObject);
var
  Art2Id, Sz : integer;
  OperId : string;
begin
  if ApplMode then raise Exception.Create(rsNotImplementation);
  case dmAppl.ADistrType of
    adtDIEl, adtSOEl, adtRej : raise Exception.Create(rsNotImplementation);
  end;
  if dmAppl.taAEl.IsEmpty then raise Exception.Create(Format(rsNoData, ['']));

  with dmAppl do
  begin
    if not _ShowEditorQ(taAElQ.AsInteger, taAElU_OLD.AsInteger, dmAppl.taAElU.AsInteger, 1) then eXit;

    (* �������� ������ �������� ������ *)
    {if dmAppl.taAElART2ID_OLD.IsNull then dm.AArt2Id := dmAppl.taAElART2ID.AsInteger
    else dm.AArt2Id := dmAppl.taAElART2ID_OLD.AsInteger;}
    dm.AArt2Id := dmAppl.taAElART2ID.AsInteger;
    ShowAndFreeForm(TfmAIns);

    (* ��������� ������� �� �������� ������ � ������ �� ������� �������� 2 *)
    ExecSQL('execute procedure CopyStoneToIns('+IntToStr(dmAppl.wOrderId)+','+IntToStr(Art2Id)+')', dmAppl.quTmp);

    (* �������� ������ �� ��������� *)
    if ShowDbEditor(TfmeIns, dm.dsrA2Ins) <> mrOk then eXit;

    dm.AArtId := dmAppl.taAElARTID.AsInteger;
    Sz := dmAppl.taAElSZID.AsInteger;
    OperId := dmAppl.taAElOPERIDFROM.AsString;
    if not dmAppl.taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([Art2Id, Sz, OperId]), []) then
      if taAElART2ID_OLD.IsNull then
        InsertWhRecord(taAEl, taAElOPERIDFROM, taAElARTID, taAElART2ID, taAElSZID, taAElU_OLD, 0)
      else InsertWhRecord(taAEl, taAElOPERIDFROM, taAElARTID, taAElART2ID_OLD, taAElSZID, taAElU_OLD, 0);

    if(ADistrQ = taAElQ.AsInteger)then taAEl.Delete
    else begin
      taAEl.Edit;
      taAElQ.AsInteger := taAElQ.AsInteger - ADistrQ;
      taAEl.Post;
    end;
    taWHAppl.Refresh;
  end;
end;

procedure TfmADistr.acAdd2Update(Sender: TObject);
begin
  case dmAppl.ADistrType of
    adtWOrder : (Sender as TAction).Enabled := dmAppl.taWHApplART2.AsString <> '-';
  end;
end;

procedure TfmADistr.acCreateArt2Update(Sender: TObject);
begin
  case dmAppl.ADistrType of
    adtWOrder : (Sender as TAction).Enabled := (dmAppl.taWHApplART2.AsString = '-')and
                                               (not dmAppl.taWHAppl.IsEmpty);
  end;
end;

procedure TfmADistr.acChangeArt2Execute(Sender: TObject);
var
  NewArtId, NewArt2Id, NewSzId, NewU : integer;
  NewOperId : string;
begin
  if ApplMode then raise Exception.Create(rsNotImplementation);
  case dmAppl.ADistrType of
    {adtDIEl,} adtSOEl, adtRej : raise Exception.Create(rsNotImplementation);
  end;
  PostDataSets([dmAppl.taRejEl, dmAppl.taRejEl, dmAppl.taPInv, dmAppl.taPEl]);  
  // ��������� �� ���������� ���������� �����, �� ���� ���� ����� ������������ �����
  if dmAppl.taWHApplARTID.IsNull then raise EInternal.Create(Format(rsInternalError, ['205']));
  if dmAppl.taWHApplART2ID.IsNull then raise EInternal.Create(Format(rsInternalError, ['201']));
  if dmAppl.taWHApplSZID.IsNull then raise EInternal.Create(Format(rsInternalError, ['202']));
  if dmAppl.taWHApplOPERID.IsNull then raise EInternal.Create(Format(rsInternalError, ['203']));
  if dmAppl.taWHApplU.IsNull then raise EInternal.Create(Format(rsInternalError, ['204']));

  NewArtId := dmAppl.taWHApplARTID.AsInteger;
  NewArt2Id := ExecSelectSQL('select Art2Id from Dubl_A2('+dmAppl.taWHApplART2ID.AsString+')', dmMain.quTmp);
  NewSzId := dmAppl.taWHApplSZID.AsInteger;
  NewOperId := dmAppl.taWHApplOPERID.AsString;
  NewU := dmAppl.taWHApplU.AsInteger;
  if not dmAppl._ShowEditorQ(dmAppl.taWhApplQ.AsInteger, dmAppl.taWhApplU.AsInteger, dmAppl.taWhApplU.AsInteger, 1) then eXit;
  with dmAppl do
  begin
    (* ����� � ������ ������� WhApplArt2Id *)
    if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty) then
    begin
      plRej.Visible := True;
      taRejInv.Append;
      taRejInv.Post;
      ReOpenDataSet(taRejInv);
    end;
    
    if not taRejEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([taWHApplART2ID.AsInteger, taWHApplSZID.AsInteger,
       taWhApplOPERID.AsString, taWHApplU.AsInteger]), []) then
    begin
      taRejEl.Append;
      try
        taRejElQ.AsInteger := dmAppl.ADistrQ;
        taRejElARTID.AsInteger := taWHApplARTID.AsInteger;
        taRejElART2ID.AsInteger := taWHApplART2ID.AsInteger;
        taRejElOPERID.AsString := taWHApplOPERID.AsString;
        taRejElSZID.AsInteger := taWHApplSZID. AsInteger;
        taRejElCOMMENTID.AsInteger := 10;
        taRejEl.Post;
      except
        on E : Exception do HandlePost_Rej(E);
      end;
    end
    else try
      taRejEl.Edit;
      taRejElQ.AsInteger := dmAppl.taRejElQ.AsInteger+dmAppl.ADistrQ;
      taRejEl.Post;
    except
      on E: Exception do HandlePost_Rej(E);
    end;
    taWHAppl.Refresh;
    (* �������� � ������ ������� *)
    if(dmAppl.taPInv.IsEmpty or (dmAppl.PInvid = 0)) then
    begin
      plPInv.Visible := True;
      taPInv.Append;
      taPInv.Post;
      ReOpenDataSet(dmAppl.taPInv);
    end;
    if not taPEl.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([NewArt2Id, NewSzId, NewOperId, NewU]), [])then
    begin
      taPEl.Append;
      try
        taPElQ.AsInteger := dmAppl.ADistrQ;
        taPElARTID.AsInteger := NewArtId;
        taPElOPERID.AsString := NewOperId;
        taPElSZID.AsInteger := NewSzId;
        taPElART2ID.AsInteger := NewArt2Id;
        taPElU.AsInteger := NewU;
        taPElCOMMENTID.AsInteger := 10;
        taPEl.Post;
      except
        on E : Exception do HandlePost_P(E);
      end;
    end
    else try
      taPEl.Edit;
      taPElQ.AsInteger := taPElQ.AsInteger + dmAppl.ADistrQ;
    except
      on E: Exception do HandlePost_P(E);
    end;
    InsertWhRecord(taPEl, taPElOPERID, taPElARTID, taPElART2ID, taPElSZID, taPElU, 0);
  end;
end;

procedure TfmADistr.acChangeArt2Update(Sender: TObject);
begin
  (Sender as TAction).Enabled := (dmAppl.taWHApplART2.AsString <> '-') and
                                 (not dmAppl.taWHAppl.IsEmpty);
end;

procedure TfmADistr.gridWHApplGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(AnsiUpperCase(Column.FieldName) = 'RECNO')then Background := clMoneyGreen
  else if(Column.FieldName = 'Q')and(dmAppl.taWHApplQ.AsFloat < 0)then Background := clRed;
end;

procedure TfmADistr.gridWHApplKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_SPACE  : if Shift = [] then acAdd.Execute
                else if Shift = [ssCtrl] then acCreateArt2.Execute; 
    VK_DELETE : acRej.Execute;
    VK_INSERT : acPAdd.Execute;
  end;
end;

procedure TfmADistr.gridRejElGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(AnsiUpperCase(Column.FieldName) = 'RECNO')then Background := clMoneyGreen;
end;

procedure TfmADistr.gridPElGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(AnsiUpperCase(Column.FieldName) = 'RECNO')then Background := clMoneyGreen;
end;

procedure TfmADistr.gridAElGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(AnsiUpperCase(Column.FieldName) = 'RECNO')then Background := clMoneyGreen;
end;

procedure TfmADistr.acShowStoneExecute(Sender: TObject);
begin
  if(dmAppl.ADistrType = adtDIEl)then eXit;
  acShowStone.Checked := not acShowStone.Checked;
  gridStone.Visible := acShowStone.Checked;
  spltStone.Visible := acShowStone.Checked;
  if acShowStone.Checked then
  begin
    dm.AArt2Id := dmAppl.taAElArt2Id.AsInteger;
    ReOpenDataSet(dmAppl.taAIns);
  end
  else CloseDataSet(dmAppl.taAIns);
end;

procedure TfmADistr.gridStoneGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(AnsiUpperCase(Column.FieldName) = 'ART') then Background := clMoneyGreen;
end;

procedure TfmADistr.FormResize(Sender: TObject);
begin
  spitExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmADistr.acAddUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not dmAppl.taWHAppl.IsEmpty;
end;

procedure TfmADistr.cmbxOperChange(Sender: TObject);
begin
  dmAppl.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSets([dmAppl.taWHAppl]);
end;

procedure TfmADistr.gridAElKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_SPACE  : if acDel.Visible then acDel.Execute;
  end;
end;

procedure TfmADistr.acChangeByApplExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'DOCNO'; sTable: ''; sWhere: '');
begin
  OpenDataSet(dmAppl.taAProdList);
  if (dmAppl.taAProdList.IsEmpty)then raise EWarning.Create(rsNoAppl);
  dmAppl.taAProdList.Last;
  if(dmAppl.taAProdList.RecordCount > 1)then
    if ShowDictForm(TfmdAList, dmAppl.dsrAProdList, Rec, '����� ������', dmSelRecordReadOnly)<>mrOk then eXit
    else dmAppl.ApplInvId := vResult
  else dmAppl.ApplInvId := dmAppl.taAProdListID.AsInteger;
  CloseDataSet(dmAppl.taAProdList);
  // �������� ������
  ShowDictForm(TfmChangeAssortByAppl, dmAppl.dsrChangeAssortByAppl, Rec, '�������� ����������� �� ������', dmSelRecordReadOnly);
end;

procedure TfmADistr.acChangeByApplUpdate(Sender: TObject);
begin
  acChangeByAppl.Visible := (dmAppl.ADistrType = adtWOrder)and(dmAppl.ADistrKind = 0);
end;

procedure TfmADistr.acShowIDExecute(Sender: TObject);
begin
  gridWHAppl.FieldColumns['ARTID'].Visible := not gridWHAppl.FieldColumns['ARTID'].Visible;
  gridWHAppl.FieldColumns['ART2ID'].Visible := not gridWHAppl.FieldColumns['ART2ID'].Visible;
  gridWHAppl.FieldColumns['SZID'].Visible := not gridWHAppl.FieldColumns['SZID'].Visible;
  gridWHAppl.FieldColumns['OPERID'].Visible := not gridWHAppl.FieldColumns['OPERID'].Visible;

  gridPEl.FieldColumns['ARTID'].Visible := not gridPEl.FieldColumns['ARTID'].Visible;
  gridPEl.FieldColumns['ART2ID'].Visible := not gridPEl.FieldColumns['ART2ID'].Visible;
  gridPEl.FieldColumns['SZID'].Visible := not gridPEl.FieldColumns['SZID'].Visible;
  gridPEl.FieldColumns['OPERID'].Visible := not gridPEl.FieldColumns['OPERID'].Visible;

  gridRejEl.FieldColumns['ARTID'].Visible := not gridRejEl.FieldColumns['ARTID'].Visible;
  gridRejEl.FieldColumns['ART2ID'].Visible := not gridRejEl.FieldColumns['ART2ID'].Visible;
  gridRejEl.FieldColumns['SZID'].Visible := not gridRejEl.FieldColumns['SZID'].Visible ;
  gridRejEl.FieldColumns['OPERID'].Visible := not gridRejEl.FieldColumns['OPERID'].Visible;
end;

procedure TfmADistr.ActionDemandUpdate(Sender: TObject);
begin
  ActionDemand.Enabled := True;
end;

procedure TfmADistr.ActionDemandExecute(Sender: TObject);
var
  UserID: Integer;
  OrderID: Integer;
  OrderItemID: Integer;
  InvoiceAddedID: Integer;
  InvoiceRemovedID: Integer;
  SourceStorageID: Integer;
  TargetStorageID: Integer;
  TargetSubStorageID: Integer;
  TargetSubStorageOperationID: string;
begin

  UserID := dm.User.UserId;
  OrderID := dmMain.taWOrderListWORDERID.AsInteger;
  OrderItemID := dmAppl.woItemId;
  InvoiceAddedID := dmAppl.PInvId;
  InvoiceRemovedID := dmAppl.RInvId;
  SourceStorageID := dmAppl.WhFrom;
  TargetStorageID := dmAppl.WhTo;
  TargetSubStorageID := dmAppl.AInvId;
  TargetSubStorageOperationID := dmAppl.AOperId;


  DialogTaskArticleOut := TDialogTaskArticleOut.Create(Self);

  DialogTaskArticleOut.UserID := UserID;
  DialogTaskArticleOut.OrderID := OrderID;
  DialogTaskArticleOut.OrderItemID := OrderItemID;
  DialogTaskArticleOut.InvoiceAddedID := InvoiceAddedID;
  DialogTaskArticleOut.InvoiceRemovedID := InvoiceRemovedID;
  DialogTaskArticleOut.SourceStorageID := SourceStorageID;
  DialogTaskArticleOut.TargetStorageID := TargetStorageID;
  DialogTaskArticleOut.TargetSubStorageID := TargetSubStorageID;
  DialogTaskArticleOut.TargetSubStorageOperationID := TargetSubStorageOperationID;

  if DialogTaskArticleOut.Execute then
  begin
    dmAppl.taAEl.Active := False;
    dmAppl.taAEl.Active := True;
    dmAppl.taWHAppl.Active := False;
    dmAppl.taWHAppl.Active := True;

    dmAppl.taPEl.Active := False;
    dmAppl.taPEl.Active := True;

    dmAppl.taRejEl.Active := False;
    dmAppl.taRejEl.Active := True;

    plRej.Visible := True;
    plPInv.Visible := True;

  end;
  FreeAndNil(DialogTaskArticleOut);
end;

procedure TfmADistr.acPrintApplUpdate(Sender: TObject);
begin
  acPrintAppl.Enabled := True;
end;

procedure TfmADistr.acPrintApplExecute(Sender: TObject);
var
  DataSource: TDataSource;
  DataSet: TpFIBDataSet;
  BookMark: TBookmark;
begin
          DataSource := gridAppl.DataSource;
  DataSet := TpFIBDataSet(DataSource.DataSet);
  DataSet.DisableControls;
  BookMark := DataSet.GetBookmark;
  DataSet.First;
  frReportAppl.PrepareReport;
  frReportAppl.ShowPreparedReport;
  DataSet.GotoBookmark(BookMark);
  DataSet.EnableControls;
end;

end.


