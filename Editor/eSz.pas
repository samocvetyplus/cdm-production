unit eSz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh;

type
  TfmeSz = class(TfmEditor)
    lbSz: TLabel;
    cmbxSz: TDBComboBoxEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmeSz: TfmeSz;
  ResultSz : integer;

implementation

uses DictData, dbTree;

{$R *.dfm}

procedure TfmeSz.FormCreate(Sender: TObject);
begin
  cmbxSz.Items.Assign(dm.dSz);
  cmbxSz.Items.Delete(0);
  cmbxSz.ItemIndex := 0;
  inherited;
end;

procedure TfmeSz.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  ResultSz := TNodeData(cmbxSz.Items.Objects[cmbxSz.ItemIndex]).Code;
end;

end.
