{***********************************************}
{  ������� ����� ������� ������������           }
{  Copyrigth (C) 2001-2004 basile for CDM       }
{  v2.21                                        }
{***********************************************}
unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, Menus, ImgList, RxSpeedBar, ExtCtrls, ComCtrls, StdCtrls, DateUtils,
  TB2Item, TB2Dock, TB2Toolbar, Grids, DBGridEh, ComDrv32, DB,
  PrnDbgeh, EhLibFIB, SIBEABase, SIBFIBEA;

type
  TDigit= set of char;

  TfmMain = class(TForm)
    mmMain: TMainMenu;
    mnitProd: TMenuItem;
    acEvent: TActionList;
    acExit: TAction;
    acOperation: TAction;
    acSemis: TAction;
    acMOL: TAction;
    acWOrder: TAction;
    mnitWOrder: TMenuItem;
    StatusBar1: TStatusBar;
    acComp: TAction;
    acSetting: TAction;
    acArt: TAction;
    mnitReport: TMenuItem;
    N13: TMenuItem;
    acMat: TAction;
    mnitJGroup: TMenuItem;
    acPrill: TAction;
    acTails: TAction;
    acNDS: TAction;
    acDiscount: TAction;
    acIns: TAction;
    acEdgShape: TAction;
    acEdgT: TAction;
    N36: TMenuItem;
    mnitDict: TMenuItem;
    N38: TMenuItem;
    N39: TMenuItem;
    N40: TMenuItem;
    N41: TMenuItem;
    N42: TMenuItem;
    N43: TMenuItem;
    N45: TMenuItem;
    N46: TMenuItem;
    N47: TMenuItem;
    N49: TMenuItem;
    N50: TMenuItem;
    N51: TMenuItem;
    N17: TMenuItem;
    acAppl: TAction;
    acInv: TAction;
    acZp: TAction;
    acProtocol: TAction;
    acSell: TAction;
    acSz: TAction;
    N5: TMenuItem;
    acRet: TAction;
    mnitUser: TMenuItem;
    acRetProd: TAction;
    acStoreByArt: TAction;
    acStoreByItems: TAction;
    mnitAdmin: TMenuItem;
    N31: TMenuItem;
    N32: TMenuItem;
    N33: TMenuItem;
    N34: TMenuItem;
    acColor: TAction;
    acCleannes: TAction;
    acChromaticity: TAction;
    acIGr: TAction;
    N1: TMenuItem;
    N4: TMenuItem;
    mnitWh: TMenuItem;
    acReport: TAction;
    acSInvProd: TAction;
    acPriceDict: TAction;
    N12: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N21: TMenuItem;
    N25: TMenuItem;
    acPriceDict1: TMenuItem;
    acPriceAct: TAction;
    N29: TMenuItem;
    woItemwOrderIdInvwOrderId1: TMenuItem;
    acErrWOrder: TAction;
    acArtCode: TAction;
    N30: TMenuItem;
    N35: TMenuItem;
    N37: TMenuItem;
    N44: TMenuItem;
    N48: TMenuItem;
    N52: TMenuItem;
    N53: TMenuItem;
    N54: TMenuItem;
    acSInv: TAction;
    acErrART: TAction;
    N55: TMenuItem;
    TBDock1: TTBDock;
    tlbrWhProd: TTBToolbar;
    tlbrAppl: TTBToolbar;
    tlbrProd: TTBToolbar;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem4: TTBItem;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    TBItem10: TTBItem;
    TBItem11: TTBItem;
    TBItem7: TTBItem;
    TBItem8: TTBItem;
    tlbrWhMat: TTBToolbar;
    TBItem9: TTBItem;
    acTransfer: TAction;
    TBItem12: TTBItem;
    N2: TMenuItem;
    acErrWhArt: TAction;
    N11: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    acErrWhSemis: TAction;
    N27: TMenuItem;
    N22: TMenuItem;
    N28: TMenuItem;
    N56: TMenuItem;
    N57: TMenuItem;
    acErrSInvProd: TAction;
    UAOperIdSItem1: TMenuItem;
    acWhArt: TAction;
    TBItem13: TTBItem;
    acWhSemis: TAction;
    TBItem14: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    acMWArt: TAction;
    N58: TMenuItem;
    N59: TMenuItem;
    acInvOborot0: TAction;
    N60: TMenuItem;
    acSOProd: TAction;
    TBItem3: TTBItem;
    acTmpWhSemis: TAction;
    N61: TMenuItem;
    N62: TMenuItem;
    N63: TMenuItem;
    N64: TMenuItem;
    N65: TMenuItem;
    acChangePswd: TAction;
    acVList: TAction;
    TBItem15: TTBItem;
    N66: TMenuItem;
    N67: TMenuItem;
    acImportStore: TAction;
    N68: TMenuItem;
    OpenDialog1: TOpenDialog;
    acBulkList: TAction;
    TBItem16: TTBItem;
    im32: TImageList;
    acCalcSemisPrice: TAction;
    N69: TMenuItem;
    acTech: TAction;
    N70: TMenuItem;
    im16: TImageList;
    tlbrMain: TTBToolbar;
    sitProd: TTBSubmenuItem;
    TBItem17: TTBItem;
    TBItem19: TTBItem;
    TBItem18: TTBItem;
    TBItem20: TTBItem;
    TBItem21: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    TBItem22: TTBItem;
    TBItem23: TTBItem;
    TBItem29: TTBItem;
    TBItem30: TTBItem;
    TBItem31: TTBItem;
    TBItem32: TTBItem;
    TBSeparatorItem4: TTBSeparatorItem;
    TBSubmenuItem2: TTBSubmenuItem;
    sitWh: TTBSubmenuItem;
    TBItem25: TTBItem;
    TBItem27: TTBItem;
    TBItem28: TTBItem;
    TBItem34: TTBItem;
    TBSeparatorItem5: TTBSeparatorItem;
    TBSeparatorItem6: TTBSeparatorItem;
    TBItem35: TTBItem;
    TBItem36: TTBItem;
    TBSeparatorItem7: TTBSeparatorItem;
    TBItem37: TTBItem;
    TBItem38: TTBItem;
    TBSubmenuItem4: TTBSubmenuItem;
    TBItem39: TTBItem;
    TBItem40: TTBItem;
    TBItem41: TTBItem;
    TBItem42: TTBItem;
    TBItem43: TTBItem;
    TBItem44: TTBItem;
    TBItem45: TTBItem;
    TBItem46: TTBItem;
    TBItem47: TTBItem;
    TBSeparatorItem8: TTBSeparatorItem;
    TBItem48: TTBItem;
    TBItem49: TTBItem;
    TBItem50: TTBItem;
    TBItem51: TTBItem;
    TBItem52: TTBItem;
    TBItem53: TTBItem;
    TBItem54: TTBItem;
    TBItem55: TTBItem;
    TBSeparatorItem10: TTBSeparatorItem;
    TBItem56: TTBItem;
    TBItem57: TTBItem;
    TBSeparatorItem13: TTBSeparatorItem;
    TBItem59: TTBItem;
    TBItem58: TTBItem;
    TBSeparatorItem12: TTBSeparatorItem;
    TBItem60: TTBItem;
    TBItem61: TTBItem;
    TBSeparatorItem14: TTBSeparatorItem;
    TBItem62: TTBItem;
    TBSeparatorItem15: TTBSeparatorItem;
    TBSeparatorItem16: TTBSeparatorItem;
    TBSeparatorItem17: TTBSeparatorItem;
    TBSeparatorItem18: TTBSeparatorItem;
    TBSeparatorItem19: TTBSeparatorItem;
    TBItem63: TTBItem;
    TBItem64: TTBItem;
    TBItem65: TTBItem;
    im24: TImageList;
    TBSeparatorItem20: TTBSeparatorItem;
    TBItem33: TTBItem;
    TBSubmenuItem1: TTBSubmenuItem;
    TBSeparatorItem21: TTBSeparatorItem;
    acZpK: TAction;
    TBItem66: TTBItem;
    TBSeparatorItem9: TTBSeparatorItem;
    acAffinaj1: TAction;
    TBItem67: TTBItem;
    TBItem68: TTBItem;
    TBSeparatorItem23: TTBSeparatorItem;
    acSemisAff: TAction;
    TBItem69: TTBItem;
    TBSeparatorItem11: TTBSeparatorItem;
    acRej: TAction;
    TBItem70: TTBItem;
    acLostStones: TAction;
    TBItem71: TTBItem;
    acReportKolie: TAction;
    N3: TMenuItem;
    TBItem72: TTBItem;
    acInvReportExecute: TAction;
    TBItem73: TTBItem;
    acOutREport: TAction;
    N6: TMenuItem;
    TBItem74: TTBItem;
    acInitStone: TAction;
    TBItem75: TTBItem;
    acSCharge: TAction;
    TBItem76: TTBItem;
    acPaytype: TAction;
    TBItem77: TTBItem;
    acCheckStone: TAction;
    TBItem78: TTBItem;
    acExportMC: TAction;
    acImportMC: TAction;
    TBSeparatorItem22: TTBSeparatorItem;
    TBItem79: TTBItem;
    TBItem80: TTBItem;
    acSzDiamond: TAction;
    TBItem81: TTBItem;
    acNeedStone: TAction;
    TBSubmenuItem3: TTBSubmenuItem;
    TBItem82: TTBItem;
    acAnlzRej: TAction;
    TBItem83: TTBItem;
    acCassaPays: TAction;
    TBSeparatorItem24: TTBSeparatorItem;
    TBItem84: TTBItem;
    TBSeparatorItem25: TTBSeparatorItem;
    acClient: TAction;
    TBItem85: TTBItem;
    acContractType: TAction;
    TBItem86: TTBItem;
    acInvColor: TAction;
    TBItem87: TTBItem;
    acActWork: TAction;
    TBItem88: TTBItem;
    TBSeparatorItem26: TTBSeparatorItem;
    TBItem89: TTBItem;
    acActVerification: TAction;
    TBItem90: TTBItem;
    acRequest: TAction;
    TBItem91: TTBItem;
    acPriceList: TAction;
    TBItem92: TTBItem;
    TBItem93: TTBItem;
    acTolling0: TAction;
    CommPortDriver11: TCommPortDriver;
    acRevision: TAction;
    TBItem94: TTBItem;
    TBSubmenuItem5: TTBSubmenuItem;
    TBItem95: TTBItem;
    TBItem96: TTBItem;
    TBItem97: TTBItem;
    acHistory: TAction;
    TBItem24: TTBItem;
    acWhatsNew: TAction;
    TBSeparatorItem27: TTBSeparatorItem;
    acCashBank: TAction;
    TBItem26: TTBItem;
    acMatBalance: TAction;
    TBItem98: TTBItem;
    acAWMatOff: TAction;
    TBItem99: TTBItem;
    TBItem100: TTBItem;
    TBItem101: TTBItem;
    acProtocol_2: TAction;
    TBItem102: TTBItem;
    acInvOborotExt: TAction;
    PopupMenuAffiange: TPopupMenu;
    N7: TMenuItem;
    N8: TMenuItem;
    ButtonAffinage: TTBSubmenuItem;
    TBItem103: TTBItem;
    acAffinaj2: TAction;
    TBSubmenuItem6: TTBSubmenuItem;
    TBItem104: TTBItem;
    TBItem105: TTBItem;
    acInvOborot1: TAction;
    acInvOborot2: TAction;
    TBSubmenuItem7: TTBSubmenuItem;
    TBItem106: TTBItem;
    TBItem107: TTBItem;
    acTolling1: TAction;
    acTolling3: TAction;
    SaveDialog: TSaveDialog;
    TBItem108: TTBItem;
    TBItem109: TTBItem;
    ItemProtocolPS: TTBItem;
    acProtocolPS: TAction;
    N9: TMenuItem;
    acAffinaj3: TAction;
    TBItem110: TTBItem;
    TBBackground: TTBBackground;
    TBItem111: TTBItem;
    TBItem112: TTBItem;
    acPaymentsAndBalance: TAction;
    TBItem113: TTBItem;
    acVerificationActTolling: TAction;
    Action1: TAction;
    acOperOrder: TAction;
    TBItem114: TTBItem;
    TBSeparatorItem28: TTBSeparatorItem;
    acFinMonitoringSells: TAction;
    TBSubmenuItem8: TTBSubmenuItem;
    TBItem115: TTBItem;
    TBItem116: TTBItem;
    acFinMonitoringReturns: TAction;
    TBItem117: TTBItem;
    acFinMonitoringPayments: TAction;
    TBItem118: TTBItem;
    acAssayOfficeStickersPrint: TAction;
    TBItem119: TTBItem;
    acBuyReport: TAction;
    TBSeparatorItem29: TTBSeparatorItem;
    acCrudeStore: TAction;
    TBSubmenuItem9: TTBSubmenuItem;
    TBItem120: TTBItem;
    TBItem121: TTBItem;
    TBItem122: TTBItem;
    acCreatePatterns: TAction;
    TBItem123: TTBItem;
    TBSeparatorItem30: TTBSeparatorItem;
    procedure acExitExecute(Sender: TObject);
    procedure acOperationExecute(Sender: TObject);
    procedure acSemisExecute(Sender: TObject);
    procedure acMOLExecute(Sender: TObject);
    procedure acWOrderExecute(Sender: TObject);
    procedure spitExitClick(Sender: TObject);
    procedure acCompExecute(Sender: TObject);
    procedure acSettingExecute(Sender: TObject);
    procedure acArtExecute(Sender: TObject);
    procedure spitInvClick(Sender: TObject);
    procedure acMatExecute(Sender: TObject);
    procedure acPrillExecute(Sender: TObject);
    procedure acTailsExecute(Sender: TObject);
    procedure acNDSExecute(Sender: TObject);
    procedure acDiscountExecute(Sender: TObject);
    procedure acEdgShapeExecute(Sender: TObject);
    procedure acEdgTExecute(Sender: TObject);
    procedure acInsExecute(Sender: TObject);
    procedure acApplExecute(Sender: TObject);
    procedure acInv1Execute(Sender: TObject);
    procedure acZpExecute(Sender: TObject);
    procedure acProtocolExecute(Sender: TObject);
    procedure acApplProdExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acSzExecute(Sender: TObject);
    procedure acRetExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure N23Click(Sender: TObject);
    procedure acStorebyArtExecute(Sender: TObject);
    procedure acStorebyItemsExecute(Sender: TObject);
    procedure acRetProdExecute(Sender: TObject);
    procedure acSellExecute(Sender: TObject);
    procedure acColorExecute(Sender: TObject);
    procedure acCleannesExecute(Sender: TObject);
    procedure acChromaticityExecute(Sender: TObject);
    procedure acIGrExecute(Sender: TObject);
    procedure acReportExecute(Sender: TObject);
    procedure acSInvProdExecute(Sender: TObject);
    procedure acPriceDictExecute(Sender: TObject);
    procedure acPriceActExecute(Sender: TObject);
    procedure acErrWOrderExecute(Sender: TObject);
    procedure acArtCodeExecute(Sender: TObject);
    procedure acSInvExecute(Sender: TObject);
    procedure acErrARTExecute(Sender: TObject);
    procedure acTransferExecute(Sender: TObject);
    procedure acErrWhArtExecute(Sender: TObject);
    procedure acErrWhSemisExecute(Sender: TObject);
    procedure acErrSInvProdExecute(Sender: TObject);
    procedure acWhArtExecute(Sender: TObject);
    procedure acWhSemisExecute(Sender: TObject);
    procedure acMWArtExecute(Sender: TObject);
    procedure acInvOborotExecute(Sender: TObject);
    procedure acSOProdExecute(Sender: TObject);
    procedure acTmpWhSemisExecute(Sender: TObject);
    procedure acChangePswdExecute(Sender: TObject);
    procedure acVListExecute(Sender: TObject);
    procedure acImportStoreExecute(Sender: TObject);
    procedure acBulkListExecute(Sender: TObject);
    procedure acCalcSemisPriceExecute(Sender: TObject);
    procedure acTechExecute(Sender: TObject);
    procedure acZpKExecute(Sender: TObject);
    procedure acAffinaj1Execute(Sender: TObject);
    procedure acSemisAffExecute(Sender: TObject);
    procedure acRejExecute(Sender: TObject);
    procedure acLostStonesExecute(Sender: TObject);
    procedure FormCanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
    procedure acReportKolieExecute(Sender: TObject);
    procedure acInvReportExecuteExecute(Sender: TObject);
    procedure acOutREportExecute(Sender: TObject);
    procedure acInitStoneExecute(Sender: TObject);
    procedure acInitStoneUpdate(Sender: TObject);
    procedure acSChargeExecute(Sender: TObject);
    procedure acPaytypeExecute(Sender: TObject);
    procedure acCheckStoneExecute(Sender: TObject);
    procedure acExportMCExecute(Sender: TObject);
    procedure acSzDiamondExecute(Sender: TObject);
    procedure acNeedStoneExecute(Sender: TObject);
    procedure acAnlzRejExecute(Sender: TObject);
    procedure acCassaPaysExecute(Sender: TObject);
    procedure acImportMCExecute(Sender: TObject);
    procedure acClientExecute(Sender: TObject);
    procedure acContractTypeExecute(Sender: TObject);
    procedure acInvColorExecute(Sender: TObject);
    procedure acActWorkExecute(Sender: TObject);
    procedure acInvUpdate(Sender: TObject);
    procedure acActVerificationExecute(Sender: TObject);
    procedure acRequestExecute(Sender: TObject);
    procedure acPriceListExecute(Sender: TObject);
    procedure acTollingExecute(Sender: TObject);
    procedure CommPortDriver11ReceiveData(Sender: TObject; DataPtr: Pointer;
      DataSize: Integer);
    procedure Insert_Record(UID:integer);
    procedure acRevisionExecute(Sender: TObject);
    procedure acHistoryExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acWhatsNewExecute(Sender: TObject);
    procedure acCashBankExecute(Sender: TObject);
    procedure acMatBalanceExecute(Sender: TObject);
    procedure acAWMatOffExecute(Sender: TObject);
    procedure TBItem100Click(Sender: TObject);
    procedure acProtocol_2Execute(Sender: TObject);
    procedure acInvOborotExtExecute(Sender: TObject);
    procedure acAffinaj2Execute(Sender: TObject);
    procedure TBItem108Click(Sender: TObject);
    procedure acProtocolPSExecute(Sender: TObject);
    procedure acAffinaj3Execute(Sender: TObject);
    procedure TBItem111Click(Sender: TObject);
    procedure acPaymentsAndBalanceExecute(Sender: TObject);
    procedure acVerificationActTollingExecute(Sender: TObject);
    procedure acOperOrderExecute(Sender: TObject);
    procedure acFinMonitoringSellsExecute(Sender: TObject);
    procedure acFinMonitoringReturnsExecute(Sender: TObject);
    procedure acFinMonitoringPaymentsExecute(Sender: TObject);
    procedure acOperOrderUpdate(Sender: TObject);
    procedure acPaymentsAndBalanceUpdate(Sender: TObject);
    procedure acAssayOfficeStickersPrintExecute(Sender: TObject);
    procedure acBuyReportExecute(Sender: TObject);
    procedure TBItem120Click(Sender: TObject);
    procedure acCrudeStoreExecute(Sender: TObject);
    procedure TBItem121Click(Sender: TObject);
    procedure acCreatePatternsExecute(Sender: TObject);
    procedure TBItem123Click(Sender: TObject);
  private
    FBlockAppHotKey : Atom;

    procedure WMHotKey(var Message : TMessage); message WM_HOTKEY;
    procedure OnPrint(Grid: TDBGridEh);
  published
    procedure OnScanCode(ScanCode: string);
  end;

var
  fmMain: TfmMain;


implementation

uses fmUtils, dOper, dSemis, dMOL, DictData, WOrderList, dComp, Setting,
  MainData, dArt, PrintData, DictAncestor, ListAncestor, SList,
  Rprod, dTails, PList, RxDateUtil, ZList,
  dPrill, dMat, dNDS, dDiscount, dEdgShape, dEdgT, dIns, AList, ApplData,
  InvData, dSz, Variants, Store, StoreByItems, dColor, dChromaticity,
  dCleannes, dIGr, Month, dPrice, PriceACT, errWOrderId, dCode,
  dbUtil, Err_View, SIList, errWhArt, ASList, errWhSemis,
  errSInvProd, WhArt, WhSemis, DIList, SOList, TmpWhSemis, eCasePS, Editor,
  DbEditor, ePswd, VList, MsgDialog, BulkList, dTech, dZpK, AFList,
  Protocol_PS, dSemisAff, AVList, dRej, LostActs, Export1C, OutReport,
  InitStone, SChargeList, dPaytype, CheckStone, ExportMC, dSzDiamond,
  NeedStone, AnlzRej, Cash, ImportMC, Progress, dClient,
  dContract, dInvColor, Data, ActWorkList, VerificationList, RequestList,
  Log_Function, ProductionConsts, dPriceList, SInv_det, RList,
  Rev_Det, ItemHistory, ProductionHelp, IniFiles, WhatsNew, CashBank, eUIDInfo,
  MBList, AWMatOffList, TaskList, frmAffiangePassport, M207Export,
  frmAnalizeSell, frmTaskArticleOut, uUtils, frmDialogArticleStructure,
  frmAffiangePassport2, dmReportInventory, frmLink, BalanceAndPayments,
  VerificationListTolling, TechnologyGroups, FinMonitoring, fmAssayStickersPrint,
  fmDialogBuyReport, CrudeStore, PatternsData, eDate;

{$R *.DFM}

procedure TfmMain.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmMain.acOperationExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'OPERID'; sName: 'OPERATION'; sTable: 'D_Oper'; sWhere: '');
begin
  ShowDictForm(TfmOper, dm.dsrOper, Rec, '���������� ��������', dmDictionary, nil, hcDOper);
end;

procedure TfmMain.acOperOrderExecute(Sender: TObject);
begin
   TfmTechnologyGroups.Execute;
end;

procedure TfmMain.acOperOrderUpdate(Sender: TObject);
begin
  acOperOrder.Enabled := (dm.User.Profile in [1, 3]) or (dm.User.UserId = 158) or (dm.User.Profile = -1); //or (dm.User.Profile = 3) or (dm.User.Profile = 1);
  acOperOrder.Visible := acOperOrder.Enabled;
end;

procedure TfmMain.acSemisExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'SEMISID'; sName: 'SEMIS'; sTable: 'D_Semis'; sWhere: '');
begin
  ShowDictForm(TfmSemis, dm.dsrSemis, Rec, '���������� ��������������', dmDictionary, nil, hcDSemis);
end;

procedure TfmMain.acMatExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_MAT'; sWhere: '');
begin
  ShowDictForm(TfmMat, dm.dsrMat, Rec, '���������� ����������', dmDictionary, nil, hcDMat);
end;

procedure TfmMain.acPrillExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'PRILL'; sTable: 'D_PRILL'; sWhere: '');
begin
  ShowDictForm(TfmPrill, dm.dsrPrill, Rec, '���������� ����', dmDictionary, nil, hcDPrill);
end;

procedure TfmMain.acTailsExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_TAILS'; sWhere: '');
begin
  ShowDictForm(TfmTails, dm.dsrTails, Rec, '���������� ����� �������', dmDictionary, nil, hcDTails);
end;

procedure TfmMain.acMOLExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'MOLID'; sName: 'FIO'; sTable: 'D_MOL'; sWhere: '');
begin
  ShowDictForm(TfmMOL, dm.dsrMOL, Rec, '���������� �����������-������������� ���', dmDictionary, nil, hcDMOL);
end;

procedure TfmMain.acCompExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'COMPID'; sName: 'NAME'; sTable: 'D_COMP'; sWhere: '');
begin
  ShowDictForm(TfmComp, dm.dsrComp, Rec, '���������� ����������� � ���������� ���', dmDictionary, nil, hcDComp);
end;

procedure TfmMain.acNDSExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'NDSID'; sName: 'NAME'; sTable: 'D_NDS'; sWhere: '');
begin
  ShowDictForm(TfmNDS, dm.dsrNDS, Rec, '���������� ���', dmDictionary, nil, hcDNDS);
end;

procedure TfmMain.acDiscountExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'D_DISCOUNTID'; sName: 'NAME'; sTable: 'D_DISCOUNT'; sWhere: '');
begin
  ShowDictForm(TfmDiscount, dm.dsrDiscount, Rec, '���������� ������', dmDictionary, nil, hcDDiscount);
end;

procedure TfmMain.acArtExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'D_ARTID'; sName: 'ART'; sTable: 'D_ART'; sWhere: '');
begin
  ShowDictForm(TfmArt, dm.dsrArt, Rec, '���������� ���������', dmDictionary, nil, hcDArt);
end;

procedure TfmMain.acAssayOfficeStickersPrintExecute(Sender: TObject);
begin
  TfmPrintAssayStickers.Execute;
end;

procedure TfmMain.acEdgShapeExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_EDGSHAPE'; sWhere: '');
begin
  ShowDictForm(TfmEdgShape, dm.dsrEdgShape, Rec, '���������� ���� �������', dmDictionary, nil, hcDCharacter);
end;

procedure TfmMain.acEdgTExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_EDGT'; sWhere: '');
begin
  ShowDictForm(TfmEdgT, dm.dsrEdgT, Rec, '���������� ����� �������', dmDictionary, nil, hcDCharacter);
end;

procedure TfmMain.acInsExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'D_INSID'; sName: 'NAME'; sTable: 'D_INS'; sWhere: '');
begin
  ShowDictForm(TfmIns, dm.dsrIns, Rec, '���������� �������', dmDictionary, nil, hcDIns);
end;

procedure TfmMain.acSzExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_SZ'; sWhere: '');
begin
  ShowDictForm(TfmSz, dm.dsrSz, Rec, '���������� ��������', dmDictionary, nil, hcDSz);
end;


procedure TfmMain.spitExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmMain.acSettingExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmSetting, Self, TForm(fmSetting), True, False);
end;

procedure TfmMain.spitInvClick(Sender: TObject);
begin
  ShowAndFreeForm(TfmSList, Self, TForm(fmSList), True, False);
end;

procedure TfmMain.acInv1Execute(Sender: TObject);
begin
  dmINV.ITYPE:=4;
  dmINV.SType := 0;
  ShowListForm(TfmSList, dmInv.taSList, dm.DepDef, dm.BeginDate, dm.EndDate,
    dmMain.ChangePeriod, dmMain.quTmp);
end;

procedure TfmMain.acApplExecute(Sender: TObject);
begin           
  ApplKind := '������';
  ShowListForm(TfmAList, dmAppl.taAList, dm.CurrDep, dm.aBeginDate, dm.aEndDate,
    dmMain.ChangeAPeriod, dmAppl.quTmp);
end;

procedure TfmMain.acZpExecute(Sender: TObject);
begin
  ShowListForm(TfmZList, dmMain.taZList, dm.CurrDep, dm.zBeginDate, dm.zEndDate,
    dmMain.ChangeZPeriod, dmMain.quTmp);
end;

procedure TfmMain.acProtocolExecute(Sender: TObject);
begin
  {if(dm.User.Profile = -1)or(dm.User.Profile = 3)or(dm.User.Profile = 4)
    then
      ShowListForm(TfmPList, dmMain.taPList, dm.CurrDep, dm.BeginDate, dm.EndDate, dm.InitPeriod, dmMain.quTmp)
  else}
    ShowAndFreeForm(TfmProtocol_PS, TForm(fmProtocol_PS));
end;

procedure TfmMain.acApplProdExecute(Sender: TObject);
begin
  ApplKind := '������ ������������';
  ShowListForm(TfmAList, dmAppl.taAList, dm.CurrDep, dm.aBeginDate, dm.aEndDate,
    dmMain.ChangeAPeriod, dmAppl.quTmp);
end;

procedure TfmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  UnregisterHotKey(Self.Handle, FBlockAppHotKey);
  //CommPortDriver1.Disconnect;
  Application.Terminate;

end;

procedure TfmMain.acRetExecute(Sender: TObject);
begin
 dmINV.ITYPE:=3;
  ShowListForm(TfmSList, dmInv.taSList, dm.CurrDep, dm.BeginDate, dm.EndDate,
   dmMain.ChangePeriod, dmMain.quTmp);
end;

procedure TfmMain.FormCreate(Sender: TObject);
begin
  DBGridEh.OnPrint := Self.OnPrint;

  StatusBar1.Panels[0].Text := '������������: '+dm.User.FIO;
  StatusBar1.Panels[1].Text := '���� ������: '+dm.db.DatabaseName + '                    '
                             + dm.User.wWhName;

  TBSubmenuItem9.Visible := dm.User.SelfCompId = 1;

  if not dm.IsAdm then
  begin

    mnitReport.Visible := False;
    with dm.User do
    begin
      if((AccDict = $0) and (AccProd = $0) and
         (AccWh = $0) and (AccAppl = $0) and
         (AccMat = $0)) then
      begin
        ShowMessage('� ��� ��� ����. ���������� � ��������������.');
        Application.Terminate;
      end;
      // �����������
      if AccDict = $0 then mnitDict.Visible := False
      else begin
        acArt.Visible          := (AccDict and 1) = 1; //���������� ���������
        acTails.Visible        := (AccDict and 2) = 2; //���������� ����� �������
        acMat.Visible          := (AccDict and 4) = 4; //���������� ����������
        acNDS.Visible          := (AccDict and 8) = 8; //���������� ���
        acOperation.Visible    := (AccDict and 16) = 16; //���������� ��������
        acComp.Visible         := (AccDict and 32) = 32; //���������� �����������
        acSemis.Visible        := (AccDict and 64) = 64; //���������� ��������������
        acPrill.Visible        := (AccDict and 128) = 128; //���������� ����
        acSz.Visible           := (AccDict and 256) = 256; //���������� ��������
        acDiscount.Visible     := (AccDict and 512) = 512; //���������� ������
        acEdgT.Visible         := (AccDict and 1024) = 1024; //���������� ����� �������
        acEdgShape.Visible     := (AccDict and 2048) = 2048; //���������� ���� �������
        acChromaticity.Visible := (AccDict and 4096) = 4096; //���������� ���������
        acCleannes.Visible     := (AccDict and 8192) = 8192; //���������� �������
        acIGr.Visible          := (AccDict and 16384) = 16384; //���������� ����� �������
        acColor.Visible        := (AccDict and 32768) = 32768; //���������� �����
        acIns.Visible          := (AccDict and 65536) = 65536; //���������� �������
        acArtCode.Visible      := (AccDict and 131072) = 131072; //����������� ��������
        acPriceDict.Visible    := (AccDict and 262144) = 262144; //���������� ���
        acRej.Visible          := (AccDict and 524288) = 524288; // ���������� ������ �����
        acSemisAff.Visible     := (AccDict and 1048576) = 1048576; // ���������� ���������� ������. ����. ������
        acPaytype.Visible      := (AccDict and 2097152) = 2097152; // ���������� �����
        acClient.Visible       := (AccDict and 4194304) = 4194304; // ���������� ��������� �����������
        acContractType.Visible := (AccDict and 8388608) = 8388608; // ���������� ����� ���������
      end;



      // ������������
      if AccProd = $0  then
      begin
        tlbrProd.Visible := False;
        mnitProd.Visible := False;
        sitProd.Visible := False;
      end
      else begin
        //acWOrder.Visible       := (AccProd and 1) = 1;     //�����
        acZp.Visible           := (AccProd and 4) = 4;     //��������
        acProtocol.Visible     := (AccProd and 8)  = 8;     //��������� ��������������
        acSInvProd.Visible     := (AccProd and 16) = 16;   //��������
        acWhArt.Visible        := (AccProd and 32) = 32;   //����� ������������
        acVList.Visible        := (AccProd and 64) = 64;   //������������ ���������
        acBulkList.Visible     := (AccProd and 128) = 128; //��� ������
        ButtonAffinage.Visible := (AccProd and 256) = 256; //�������
        acZpK.Visible          := (AccProd and 512) = 512; //������������ ��� ������� ��
      end;


      // ������ � ��������
      if AccAppl = $0 then tlbrAppl.Visible := False
      else begin
        acAppl.Visible    := (AccAppl and 1)=1; // ���������������� �����
        acMOL.Visible     := (AccAppl and 2)=2;  // ������������
        acSetting.Visible := (AccAppl and 4)=4; // ��������� ����������
        acRequest.Visible := (AccAppl and 8)=8; // ������ �� �����������
      end;

      // ����� ��������������
      if AccMat = $0 then tlbrWhMat.Visible := False
      else begin
        acSInv.Visible     := (AccMat and 1) = 1;  //�����������
        acTransfer.Visible := (AccMat and 2 )= 2; //�����������
        acSOProd.Visible   := (AccMat and 4) = 4; //������
        acWhSemis.Visible  := (AccMat and 8) = 8; //����� ��������������
      end;
      // ����� ������� ���������
      if AccWh = $0 then begin
        tlbrWhProd.Visible := False;
        mnitWh.Visible     := False;
        sitWh.Visible      := False;
      end
      else
        begin
          acInv.Visible             := (AccWh and 1)=1;       //
          acSell.Visible            := (AccWh and 2)=2;       //�������
          acRet.Visible             := (AccWh and 4)=4;       //�������
          acRetProd.Visible         := (AccWh and 8)=8;       //������� �� ��-��
          acStoreByArt.Visible      := (AccWh and 16)=16;     //����� ��-����������
          acStoreByItems.Visible    := (AccWh and 32)=32;     //����� ��-��������
          acActWork.Visible         := (AccWh and 64)=64;     //��� ���. �����
          acActVerification.Visible := (AccWh and 128)=128;   //��� ������
          acPriceAct.Visible        := (AccWh and 256)=256;   //���� ����������
        end;
      // ������
      acAnlzRej.Visible := True;
    end;
  end
  else N12.Visible:=true;

  // ������������ Hot Key
  FBlockAppHotKey := AddAtom('BlockApp#0');
  RegisterHotKey(Self.Handle, FBlockAppHotKey, MOD_CONTROL or MOD_SHIFT, Ord('Q'));
  if AnsiUpperCase(dm.DbPath) <> AnsiUpperCase(dm.db.DatabaseName) then
                    begin
                    MessageDialogA('�� ������������ � ��������� ��!', mtWarning);
                    Application.Title := '�� ������� �� '+dm.User.SelfCompName
                    end
  else              Application.Title := '������� �� '+dm.User.SelfCompName;

  // comport
  //CommPortDriver1.ComPort:=TComPortNumber( dm.localsetting.barcodescaner_port);
  if dm.LocalSetting.BarCodeScaner_Use then
  begin
    //CommPortDriver1.Disconnect;
    //CommPortDriver1.Connect;
  end;

//  if (dm.User.Profile = 2) or (dm.User.Profile = 4) then
  if dm.User.Profile in [2, 4, 5] then
    acProtocol_2.Visible := False
  else
    acProtocol_2.Visible := true; //(dm.User.AccProd and 8)=8;
  ItemProtocolPS.Visible := true;//((dm.User.Profile = -1) or (dm.User.Profile = 3));
  if dm.User.wWhId in [250, 251] then
    begin
      tlbrProd.Color := clMoneyGreen;
      tlbrWhMat.Color := clMoneyGreen;
      tlbrAppl.Color := clMoneyGreen;
      tlbrWhProd.Color := clMoneyGreen;
      StatusBar1.Color := clMoneyGreen;
      TBDock1.Color := clMoneyGreen;
      tlbrMain.Color := clMoneyGreen;
    end;
end;

procedure TfmMain.SpeedItem5Click(Sender: TObject);
begin
  ShowAndFreeForm(TfmStore, Self, TForm(fmStore), True, False);
end;

procedure TfmMain.N23Click(Sender: TObject);
begin
   ShowAndFreeForm(TfmStorebyItems, Self, TForm(fmStorebyItems), True, False);
end;


procedure TfmMain.acStorebyArtExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmStore, Self, TForm(fmStorebyItems), True, False);
end;

procedure TfmMain.acStorebyItemsExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmStorebyItems, Self, TForm(fmStorebyItems), True, False);
end;

procedure TfmMain.acRetProdExecute(Sender: TObject);
begin
  dmINV.ITYPE:=12;
  ShowListForm(TfmSList, dmInv.taSList, dm.CurrDep, dm.BeginDate, dm.EndDate,
  dmMain.ChangePeriod, dmMain.quTmp);
end;

procedure TfmMain.acSellExecute(Sender: TObject);
begin
  dmINV.ITYPE:=2;
  ShowListForm(TfmSList, dmInv.taSList, dm.CurrDep, dm.BeginDate, dm.EndDate,
   dmMain.ChangePeriod, dmMain.quTmp);
end;

procedure TfmMain.acColorExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_COLOR'; sWhere: '');
begin
  ShowDictForm(TfmColor, dm.dsrColor, Rec, '���������� ������', dmDictionary, nil, hcDCharacter);
end;

procedure TfmMain.acCleannesExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_CLEANNES'; sWhere: '');
begin
  ShowDictForm(TfmCleannes, dm.dsrCleannes, Rec, '���������� �������', dmDictionary, nil, hcDCharacter);
end;

procedure TfmMain.acChromaticityExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_CHROMATICITY'; sWhere: '');
begin
  ShowDictForm(TfmChromaticity, dm.dsrChromaticity, Rec, '���������� ���������', dmDictionary, nil, hcDCharacter);
end;

procedure TfmMain.acIGrExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_IGR'; sWhere: '');
begin
  ShowDictForm(TfmIGr, dm.dsrIGr, Rec, '���������� ����� �������', dmDictionary, nil, hcDCharacter);
end;

procedure TfmMain.acReportExecute(Sender: TObject);
begin
  dmPrint.Ntype:=0;
  ShowListForm(TfmAVList, dmInv.taAVList,dm.CurrDep, dm.whBeginDate, dm.whEndDate,
               dmMain.ChangeWhPeriod, dmMain.quTmp);
end;

procedure TfmMain.acSInvProdExecute(Sender: TObject);
begin
  ShowListForm(TfmASList, dmMain.taASList, dm.CurrDep, dm.whBeginDate, dm.whEndDate,
    dmMain.ChangeWhPeriod, dmMain.quTmp);
end;

procedure TfmMain.acPriceDictExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmPriceDict, Self, TForm(fmPriceDict), True, False);
end;

procedure TfmMain.acPriceActExecute(Sender: TObject);
begin
  dmINV.ITYPE:=11;// ���� ����������
  ShowListForm(TfmPriceACT, dmInv.taSList, dm.CurrDep, dm.BeginDate, dm.EndDate,
     dmMain.ChangePeriod, dmMain.quTmp);
end;

procedure TfmMain.acErrWOrderExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmErrWOrderId, TForm(fmErrWOrderId));
end;

procedure TfmMain.acArtCodeExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmCode, TForm(fmCode));
end;

procedure TfmMain.acSInvExecute(Sender: TObject);
begin
  ShowListForm(TfmSIList, dmMain.taSIList, dm.DepDef, dm.whBeginDate, dm.whEndDate,
    dmMain.ChangeWhPeriod, dmMain.quTmp);
end;

procedure TfmMain.acErrARTExecute(Sender: TObject);
begin
 ShowAndFreeForm(TfmErrArt, TForm(fmErrArt))
end;

procedure TfmMain.acTransferExecute(Sender: TObject);
begin
  ShowListForm(TfmDIList, dmMain.taDIList, dm.DepDef, dm.whBeginDate,
    dm.whEndDate, dmMain.ChangeWhPeriod, dmMain.quTmp);
end;

procedure TfmMain.acErrWhArtExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmErrWhArt, TForm(fmErrWhArt));
end;

procedure TfmMain.acErrWhSemisExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmErrWhSemis, TForm(fmErrWhSemis));
end;

procedure TfmMain.acErrSInvProdExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmErrSInvProd, TForm(fmErrSInvProd));
end;

procedure TfmMain.acWhArtExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmWhArt, TForm(fmWhArt));
end;

procedure TfmMain.acWhSemisExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmWhSemis, TForm(fmWhSemis));
end;

procedure TfmMain.acMWArtExecute(Sender: TObject);
begin
  //ExecSQL('execute procedure BUILD_MW_ART', dmMain.quTmp);
end;

procedure TfmMain.acInvOborotExecute(Sender: TObject);
begin
  dmPrint.Ntype:=1;
  dmPrint.NSubType := TAction(Sender).Tag;
  dmPrint.T0:=0;
  ShowAndFreeForm(TfmMonth,TForm(fmMonth))
end;

procedure TfmMain.acSOProdExecute(Sender: TObject);
begin
  ShowListForm(TfmSOList, dmMain.taSOList, dm.DepDef, dm.whBeginDate, dm.whEndDate,
    dmMain.ChangeWhPeriod, dmMain.quTmp);
end;

procedure TfmMain.acTmpWhSemisExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmTmpWhSemis, TForm(fmTmpWhSemis));
end;

procedure TfmMain.acChangePswdExecute(Sender: TObject);
begin
  OpenDataSet(dm.taMOL);
  if dm.taMOL.Locate('MOLID', dm.User.UserId, [])then
    ShowDbEditor(TfmPswd, dm.dsrMOL, emEdit, True)
  else raise EInternal.Create(Format(rsInternalError, ['004']));
end;

procedure TfmMain.acVerificationActTollingExecute(Sender: TObject);
var Form: TfmVerificationListTolling;
begin
  try
    Form := nil;
    Form := TfmVerificationListTolling.Create(Application, dmData.taVerificationListTolling, -1,
                                 StartOfTheYear(Now), EndOfTheYear(Now), nil, dm.quTmp); 
    Form.ShowModal;
  finally
    FreeAndNil(Form);
  end;
end;

procedure TfmMain.acVListExecute(Sender: TObject);
begin
  ShowListForm(TfmVList, dmMain.taVList, dm.CurrDep, dm.vBeginDate, dm.vEndDate,
    dmMain.ChangeVPeriod, dmMain.quTmp);
end;

procedure TfmMain.WMHotKey(var Message: TMessage);
begin
  dm.BlockApp;
end;

procedure TfmMain.acImportStoreExecute(Sender: TObject);
begin
 if OpenDialog1.Execute then
 begin
  dmInv.ImportStore(OpenDialog1.FileName);
 end;
end;

procedure TfmMain.acBulkListExecute(Sender: TObject);
begin
  ShowListForm(TfmBulkList, dmMain.taBulkList, dm.CurrDep, dm.whBeginDate, dm.whEndDate,
     dmMain.ChangeWhPeriod, dmMain.quTmp);
end;

procedure TfmMain.acBuyReportExecute(Sender: TObject);
begin
  TDialogBuyReport.Execute;
end;

procedure TfmMain.acCalcSemisPriceExecute(Sender: TObject);
begin
  { TODO : ����� �� ���� ���? }
  {
  Screen.Cursor := crSQLWait;
  try
    ExecSQL('execute procedure Calc_SemisPrice('#39+DateToStr(StartOfTheYear(Now))+#39' ,'#39+
      DateToStr(EndOfTheYear(Now))+#39', -1)', dmMain.quTmp);
  finally
    Screen.Cursor := crDefault;
  end;
  }
end;

procedure TfmMain.acTechExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_Tech'; sWhere: '');
begin
  ShowDictForm(TfmTech, dm.dsrTech, Rec, '���������� ����������', dmDictionary, nil, hcDProdTech);
end;

procedure TfmMain.acZpKExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'ZpK'; sWhere: '');
begin
  ShowDictForm(TfmdZpK, dm.dsrZpK, Rec, '������������ ��� ������� ���������� �����', dmDictionary, nil, hcZPKoef);
end;


procedure TfmMain.acSemisAffExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_SemisAff'; sWhere: '');
begin
  ShowDictForm(TfmdSemisAff, dm.dsrSemisAff, Rec, '��������� ���������� ����������� �������', dmDictionary, nil, hcDSemisAff);
end;

procedure TfmMain.acRejExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_Rej'; sWhere: '');
begin
  ShowDictForm(TfmdRej, dm.dsrRej, Rec, '���������� ������ �����', dmDictionary, nil, hcDProdRej);
end;

procedure TfmMain.acLostStonesExecute(Sender: TObject);
begin
    ShowListForm(TfmLostActs, dmInv.taLostActs, dm.CurrDep, dm.BeginDate, dm.EndDate,
     dmMain.ChangePeriod, dmMain.quTmp);
end;

procedure TfmMain.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  Resize := False;
end;

procedure TfmMain.acReportKolieExecute(Sender: TObject);
begin
  dmPrint.Ntype:=2;
  ShowAndFreeForm(TfmMonth,TForm(fmMonth))
end;

procedure TfmMain.acInvReportExecuteExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmExport1C, TForm(fmExport1C));
end;

procedure TfmMain.acOutREportExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmOutReport,TForm(fmOutReport));
end;

procedure TfmMain.acInitStoneExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'SEMISNAME'; sTable: 'Init_Stone'; sWhere: '');
begin
  ShowDictForm(TfmInitStone, dm.dsrInitStone, Rec, '��������� ������� �� ������');
end;

procedure TfmMain.acInitStoneUpdate(Sender: TObject);
begin
  acInitStone.Visible := (dm.User.Profile = -1) or (dm.User.Profile = 3) or
                         (dm.User.Profile = 1);
end;

procedure TfmMain.acSChargeExecute(Sender: TObject);
begin
  ShowListForm(TfmSChargeList, dmMain.taSChargeList, dm.CurrDep,
    dm.BeginDate, dm.EndDate, dmMain.ChangePeriod, dmMain.quTmp);
end;

procedure TfmMain.acPaymentsAndBalanceExecute(Sender: TObject);
begin
  TfmPaymentsAndBalance.Execute;
end;

procedure TfmMain.acPaymentsAndBalanceUpdate(Sender: TObject);
begin
  acPaymentsAndBalance.Enabled := (dm.User.Profile = 2) or (dm.User.Profile = 4) or (dm.User.Profile = -1);
  acPaymentsAndBalance.Visible := acPaymentsAndBalance.Enabled;
end;

procedure TfmMain.acPaytypeExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'PAYTYPEID'; sName: 'NAME'; sTable: 'D_PayType'; sWhere: '');
begin
  ShowDictForm(TfmPaytypeDict, dm.dsrPaytype, Rec, '���������� ����� ������', dmDictionary, nil, hcDPayTtype);
end;

procedure TfmMain.acCheckStoneExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmCheckStone, TForm(fmCheckStone));
end;

procedure TfmMain.acExportMCExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmExportMC, TForm(fmExportMC));
end;

procedure TfmMain.acFinMonitoringPaymentsExecute(Sender: TObject);
begin
  TfmFinMonitoring.Execute(-1);
end;

procedure TfmMain.acFinMonitoringReturnsExecute(Sender: TObject);
begin
  TfmFinMonitoring.Execute(3);
end;

procedure TfmMain.acFinMonitoringSellsExecute(Sender: TObject);
begin
  TfmFinMonitoring.Execute(2);
end;

procedure TfmMain.acSzDiamondExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'SZ'; sName: 'SZ'; sTable: 'D_SzDiamond'; sWhere: '');
begin
  ShowDictForm(TfmdSzDiamond, dm.dsrSzDiamond, Rec, '���������� �������� �����������', dmDictionary, nil, hcDSzDiamond);
end;

procedure TfmMain.acNeedStoneExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmNeedStone, TForm(fmNeedStone));
end;

procedure TfmMain.acAnlzRejExecute(Sender: TObject);
begin
  //Application.MessageBox('� ����������', '���������', MB_OK or MB_ICONINFORMATION);
  ShowAndFreeForm(TfmAnlzRej, TForm(fmAnlzRej));
end;

procedure TfmMain.acCassaPaysExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmCash, TForm(fmCash));
end;

procedure TfmMain.acImportMCExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmImportMC, TForm(fmImportMC));
end;

procedure TfmMain.acClientExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'CLIENTID'; sName: 'FNAME'; sTable: 'D_CLIENT'; sWhere: '');
begin
  ShowDictForm(TfmdClient, dm.dsrClient, Rec, '���������� ��������� �����������', dmDictionary, nil, hcDClient);
end;

procedure TfmMain.acContractTypeExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_CONTRACT'; sWhere: '');
begin
  ShowDictForm(TfmdContract, dm.dsrContract, Rec, '���������� ����� ���������', dmDictionary, nil, hcDContract);
end;

procedure TfmMain.acCreatePatternsExecute(Sender: TObject);
begin
  TPatternsImport.Import((Sender as TAction).Tag);
end;

procedure TfmMain.acCrudeStoreExecute(Sender: TObject);
var
  AResult: Variant;
  ADateTime: TDateTime;
begin

  if Sender is TAction then
  begin

    AResult := TfmEDate.Execute;

    if VarIsNull(AResult) then
    begin
      Exit;
    end;

    AdateTime := VarToDateTime(AResult);

    ADateTime := EndOfTheDay(ADateTime);

    TfmCrudeStore.Execute((Sender as TAction).Tag, ADateTime);

  end;

end;

procedure TfmMain.acInvColorExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_INVCOLOR'; sWhere: '');
begin
  ShowDictForm(TfmdInvColor, dm.dsrInvColor, Rec, '�������� ����� ����������', dmDictionary, nil, hcDInvColor);
end;

procedure TfmMain.acActWorkExecute(Sender: TObject);
begin
  ShowListForm(TfmActWorkList, dmData.taActWorkList, dm.DepDef,
     dm.BeginDate, dm.EndDate, dmMain.ChangePeriod, dmMain.quTmp);
end;

procedure TfmMain.acInvUpdate(Sender: TObject);
begin
  acInv.Visible := False;
end;

procedure TfmMain.acActVerificationExecute(Sender: TObject);
begin
  ShowListForm(TfmVerificationList, dmData.taVerificationList, dm.CurrDep,
     dm.BeginDate, dm.EndDate, dmMain.ChangePeriod, dmMain.quTmp);
end;

procedure TfmMain.acRequestExecute(Sender: TObject);
begin
  ShowListForm(TfmRequestList, dmData.taRequestList, -1, dm.aBeginDate, dm.aEndDate,
    dmMain.ChangeAPeriod, dm.quTmp);
end;

procedure TfmMain.acPriceListExecute(Sender: TObject);
begin
   ShowAndFreeForm(TfmPriceList, TForm(fmPriceList));
end;

procedure TfmMain.acTollingExecute(Sender: TObject);
begin
  dmPrint.Ntype:=1;
  dmPrint.NSubType := TAction(Sender).Tag;
  dmPrint.T0:=1;
  ShowAndFreeForm(TfmMonth,TForm(fmMonth))
end;

procedure TfmMain.CommPortDriver11ReceiveData(Sender: TObject;
  DataPtr: Pointer; DataSize: Integer);
var
  p: pchar;
  i,j: longint;
  ValScan: string;
const DigitSet: TDigit = ['1','2','3','4','5','6','7','8','9','0'];
begin
  p := dataptr;
  for i:=1 to DataSize do
  begin
   if chr(ord(p^))<>#13  then  dmInv.SScanZ:=dmInv.SScanZ+chr(ord(p^))
   else
    begin
     try
      j:=1; ValScan:='';
      while (dmInv.SScanZ[j]='0') and (j<=length(dmInv.SScanZ)) do inc(j);
      while ( j<=length(dmInv.SScanZ))
      do begin if (dmInv.SScanZ[j] in DigitSet) then ValScan:=ValScan+dmInv.SScanZ[j]; inc(j) end;
      dmInv.SScanZ:='';

      Insert_Record (StrToInt(ValScan));
      if AppDebug then ShowMessage(ValScan);
     finally
     end;
   end;
   inc(p);
  end;
end;

procedure TfmMain.Insert_Record(UID: integer);
begin
  if Screen.ActiveForm=fmSInv_det then
    with fmSInv_det do
    begin
      OnInvoiceArticleScaned(UID);
    end;

  if Screen.ActiveForm.Name='fmNotInInv' then
  begin
    if dmInv.taNotInInv.Locate('UID',UID,[loCaseInsensitive]) then
    begin
      dmInv.taNotInInv.Delete;
      dmInv.taNotInInv.Transaction.CommitRetaining;
    end
    else ShowUIDInfo(UID, '������� ��� � ���������!');
  end;

  if Screen.ActiveForm.InheritsFrom(TfmRev_Det) then
  begin
    TfmRev_Det(Screen.ActiveForm).lbScanUID.Caption := IntToStr(UID);
    TfmRev_Det(Screen.ActiveForm).Update;
    TfmRev_Det(Screen.ActiveForm).InsertUID(UID);
  end;
end;

procedure TfmMain.acRevisionExecute(Sender: TObject);
begin
  dmINV.ITYPE:=24;
  ShowListForm(TfmRList, dmInv.taSList, dm.DepDef, dm.BeginDate, dm.EndDate,
    dmMain.ChangePeriod, dmMain.quTmp);
end;

procedure TfmMain.acHistoryExecute(Sender: TObject);
begin
  CloseDataSet(dmInv.taItemHistory);
  dmInv.FCurrUID := 0;
  ShowAndFreeForm(TfmItemHistory, Self, TForm(fmItemHistory), True, False);
  CloseDataSet(dmInv.taItemHistory);
end;

procedure TfmMain.FormShow(Sender: TObject);
var
  IniVer : string;
  Ini : TIniFile;
  sect : TStringList;
begin
  
  // ��������� �� ���������� �� ������ exe
  Ini := TIniFile.Create(GetIniFileName);
  sect := TStringList.Create;
  try
    Ini.ReadSectionValues('LocalSetting', sect);
    IniVer := sect.Values['CurrentVersion'];
    {
    if CompareVer(AppVersion, IniVer)=1 then
    begin
      dm.taWhatsNew.ParamByName('VER1').AsString := IniVer;
      dm.taWhatsNew.ParamByName('VER2').AsString := AppVersion;
      OpenDataSet(dm.taWhatsNew);
      ShowWhatsNew(dm.taWhatsNew);
      CloseDataSet(dm.taWhatsNew);
    end;
    }
    Ini.WriteString('LocalSetting', 'CurrentVersion', AppVersion);
  finally
    Ini.Free;
    sect.Free;
  end;


end;

procedure TfmMain.acWhatsNewExecute(Sender: TObject);
begin
//  dm.taWhatsNew.ParamByName('VER1').AsString := '!';
//  dm.taWhatsNew.ParamByName('VER2').AsString := '�';
//  OpenDataSet(dm.taWhatsNew);
//  ShowWhatsNew(dm.taWhatsNew);
//  CloseDataSet(dm.taWhatsNew);
end;

procedure TfmMain.acCashBankExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmCashBank, TForm(fmCashBank));
end;

procedure TfmMain.acMatBalanceExecute(Sender: TObject);
begin
  ShowListForm(TfmMBList, dmData.taMBList, -2, dm.BeginDate, dm.EndDate, dmMain.ChangePeriod, dm.quTmp);
end;

procedure TfmMain.acAWMatOffExecute(Sender: TObject);
begin
  ShowListForm(TfmAWMatOffList, dmData.taAWMOList, -2, dm.BeginDate, dm.EndDate, dmMain.ChangePeriod, dm.quTmp);
end;

procedure TfmMain.TBItem100Click(Sender: TObject);
{������}
begin
  ShowListForm(TfmTaskList, dmData.taTaskList, -2, dm.BeginDate, dm.EndDate, dmMain.ChangePeriod, dm.quTmp);
end;

procedure TfmMain.acProtocol_2Execute(Sender: TObject);
begin
  ShowListForm(TfmPList, dmMain.taPList, dm.CurrDep, dm.BeginDate, dm.EndDate, dm.InitPeriod, dmMain.quTmp)
end;

procedure TfmMain.acInvOborotExtExecute(Sender: TObject);
begin
  dmPrint.Ntype:=1;
  dmPrint.NSubtype:=0;
  dmPrint.T0:=2;
  ShowAndFreeForm(TfmMonth,TForm(fmMonth))
end;

procedure TfmMain.acAffinaj1Execute(Sender: TObject);
begin
  ShowAndFreeForm(TfmAFList, TForm(fmAFList));
end;



procedure TfmMain.OnPrint(Grid: TDBGridEh);
begin
  if SaveDialog.Execute then
  begin
    GridToXLS(Grid, SaveDialog.FileName, '', True, True);
  end;
end;

procedure TfmMain.TBItem108Click(Sender: TObject);
begin
  DialogAnalizeSell := TDialogAnalizeSell.Create(Self);
  DialogAnalizeSell.ShowModal;
  DialogAnalizeSell.Free;
end;


procedure TfmMain.TBItem111Click(Sender: TObject);
begin

  TDialogLink.Execute;
  exit;

end;


procedure TfmMain.TBItem120Click(Sender: TObject);
begin

  acCrudeStore.Tag := TBItem120.Tag;

  acCrudeStore.Execute;

end;

procedure TfmMain.TBItem121Click(Sender: TObject);
begin
  acCrudeStore.Tag := TBItem121.Tag;

  acCrudeStore.Execute;
end;

procedure TfmMain.TBItem123Click(Sender: TObject);
begin
  acCreatePatterns.Tag := (Sender as TTBItem).Tag;
  acCreatePatterns.Execute;
end;

procedure TfmMain.acWOrderExecute(Sender: TObject);
var
  LogOperationID: string;
begin

  LogOperationID := Insert_operation(lsWOrderList, '-1000', IntToStr(dm.User.LogId), dm.quLog);
  ShowListForm(TfmWOrderList, dmMain.taWOrderList, dm.DepDef,
    dm.BeginDate, dm.EndDate, dmMain.ChangePeriod, dmMain.quTmp);
  update_operation(LogOperationID, dm.quLog);

end;


procedure TfmMain.acProtocolPSExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmProtocol_PS, TForm(fmProtocol_PS));
end;

procedure TfmMain.OnScanCode(ScanCode: string);

// EAN-8 check digit compare

function IsScanCodeValid(ScanCode: string): Boolean;
var
  i, o, e, v, c, l: integer;
begin
  l := Length(ScanCode);
  if (l >= 2) then
  begin
    o := 0;
    e := 0;
    for i := 1 to l - 1 do
    begin
      v := Ord(ScanCode[i]) - ord('0');
      if Odd(i) then o := o + v
      else e := e + v;
    end;
    c := 10 - ((o *3 + e) mod 10);
    if c = 10 then c := 0;
    v := Ord(ScanCode[l]) - ord('0');
    Result := v = c;
  end
  else Result := False;
end;

begin
  //Insert_Record(StrToInt(ScanCode));
  //Exit;

  if IsScanCodeValid(ScanCode) then
  begin
    SetLength(ScanCode, Length(ScanCode) - 1);
    //InformationMessage('C����� ����� "' + ScanCode + '"');
    Insert_Record(StrToInt(ScanCode));
  end
  else
  begin
    if Length(ScanCode) >= 2 then
    SetLength(ScanCode, Length(ScanCode) - 1);
    ErrorOkMessage('������� ������ ����� "' + ScanCode + '"');
  end;
end;

procedure TfmMain.acAffinaj3Execute(Sender: TObject);
begin
  TDialogAffinage2.Execute(False);
end;

procedure TfmMain.acAffinaj2Execute(Sender: TObject);
begin
  TDialogAffinage.Execute(False);
end;


end.
