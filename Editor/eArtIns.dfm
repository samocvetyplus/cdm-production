object fmArtIns: TfmArtIns
  Left = 98
  Top = 203
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = #1042#1089#1090#1072#1074#1082#1080
  ClientHeight = 175
  ClientWidth = 717
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object InsGrid: TDBGridEh
    Left = 0
    Top = 23
    Width = 717
    Height = 152
    Align = alClient
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
    AutoFitColWidths = True
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmInv.dsrArtIns
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        Color = 15461869
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SEMIS'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'QUANTITY'
        Footer.FieldName = 'QUANTITY'
        Footer.ValueType = fvtSum
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WEIGHT'
        Footer.FieldName = 'WEIGHT'
        Footer.ValueType = fvtSum
        Footers = <>
      end
      item
        Color = 13434879
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'PRICE'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SUM'
        Footer.FieldName = 'SUM'
        Footer.ValueType = fvtSum
        Footers = <>
      end
      item
        Checkboxes = True
        Color = 11594959
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'E'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'NAME'
        Footers = <>
        Visible = False
        Width = 130
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object TBDock1: TTBDock
    Left = 0
    Top = 0
    Width = 717
    Height = 23
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      BorderStyle = bsNone
      DockMode = dmCannotFloat
      DockPos = 0
      TabOrder = 0
      object itDocHist: TTBItem
        Action = acDocMovingSemis
      end
    end
  end
  object acEvent: TActionList
    Left = 36
    Top = 44
    object acDocMovingSemis: TAction
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086' '#1076#1086#1082#1091#1084#1077#1085#1090#1072#1084
      OnExecute = acDocMovingSemisExecute
      OnUpdate = acDocMovingSemisUpdate
    end
  end
end
