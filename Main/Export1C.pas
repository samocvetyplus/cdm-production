unit Export1C;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, xmldom, XMLIntf, DB,
  pFIBDataSet, msxmldom, XMLDoc, DBCtrlsEh, Mask, RxToolEdit,
  pFIBStoredProc, CheckLst, FIBQuery, pFIBQuery, FIBDataSet;

type
  TfmExport1C = class(TfmEditor)
    Label1: TLabel;
    edFile: TFilenameEdit;
    Label2: TLabel;
    edDate: TDBDateTimeEditEh;
    XMLDoc: TXMLDocument;
    taMat: TpFIBDataSet;
    taComp: TpFIBDataSet;
    taDocument: TpFIBDataSet;
    taDelDocument: TpFIBDataSet;
    taDocItem: TpFIBDataSet;
    stpBuild_1C: TpFIBStoredProc;
    ta1CGroup: TpFIBDataSet;
    edBD: TDBDateTimeEditEh;
    Label3: TLabel;
    Label4: TLabel;
    chlbx1CGroup: TCheckListBox;
    Label5: TLabel;
    taRec: TpFIBDataSet;
    taRecEXP1C: TDateTimeField;
    edExp1C: TDBDateTimeEditEh;
    dsrRec: TDataSource;
    Losses: TpFIBDataSet;
    LossesID: TFIBIntegerField;
    LossesINVID: TFIBIntegerField;
    LossesMATID: TFIBStringField;
    LossesCOMPID: TFIBIntegerField;
    LossesNW: TFIBFloatField;
    LossesSCOST: TFIBFloatField;
    LossesN: TFIBFloatField;
    LossesGETW: TFIBFloatField;
    LossesOUTW: TFIBFloatField;
    taDocItemID: TFIBIntegerField;
    taDocItemHEADERID: TFIBIntegerField;
    taDocItemMATID: TFIBStringField;
    taDocItemW: TFIBFloatField;
    taDocItemU: TFIBSmallIntField;
    taDocItemCOST: TFIBFloatField;
    taDocItemNDSCOST: TFIBFloatField;
    taDocItemINCOST: TFIBFloatField;
    taDocItemW2: TFIBFloatField;
    taDocItemSERVCOST: TFIBFloatField;
    taDocItemMATOWNER: TFIBSmallIntField;
    Label6: TLabel;
    cbContract: TComboBox;
    procedure FormCreate(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure taDelDocumentBeforeOpen(DataSet: TDataSet);
    procedure taDocItemBeforeOpen(DataSet: TDataSet);
    procedure taDocumentAfterOpen(DataSet: TDataSet);
    procedure taDocumentAfterScroll(DataSet: TDataSet);
    procedure taRecBeforeClose(DataSet: TDataSet);
    procedure taRecAfterPost(DataSet: TDataSet);
    procedure chlbx1CGroupClickCheck(Sender: TObject);
    procedure taDocumentFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure edExp1CChange(Sender: TObject);
    procedure edExp1CButtonClick(Sender: TObject; var Handled: Boolean);
    procedure taDocumentBeforeOpen(DataSet: TDataSet);
    procedure cbContractExit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FModified : boolean;
    SchemaID : byte;
  end;

var
  fmExport1C: TfmExport1C;

implementation

uses DictData, DateUtils, dbUtil, MsgDialog, dbTree;

const
  cOnlyDict = '������ ���������� ����������';


{$R *.dfm}


procedure TfmExport1C.FormCreate(Sender: TObject);
var
  nd : TNodeData;
begin
  FModified := False;
  edBD.Value:= StartOfTheMonth(Today);
  edDate.Value := Today;
  edFile.FileName := 'C:\Program Files\������������\to1c.xml';
  OpenDataSets([ta1CGroup, taRec]);
  chlbx1CGroup.Clear;
  if ta1CGroup.IsEmpty then chlbx1CGroup.Enabled := False
  else begin
    chlbx1CGroup.Items.Add('*���');
    chlbx1CGroup.Checked[0] := True;
    chlbx1CGroup.Items.Add(cOnlyDict);
    chlbx1CGroup.Checked[1] := False; 
    while not ta1CGroup.Eof do
    begin
      nd := nil;
      chlbx1CGroup.Items.AddObject(ta1CGroup.Fields[0].AsString, nd);
      ta1CGroup.Next;
    end;
  end;
  CloseDataSet(ta1CGroup);
  cbContract.Items.Add('*���');
  cbContract.Items.Add('�������');
  cbContract.Items.Add('������������ ����������');
  cbContract.Items.Add('������������ �������');
//  with dm.taContract do
//  begin
//    Open;
//    First;
//    while not Eof do
//    begin
//      cbContract.Items.Add(FieldByName('Name').AsString);
//      Next;
//    end;
//  end;
  cbContract.ItemIndex := 0;

  //edBD.Value := taRec.Fields[0].AsDateTime;
end;

procedure TfmExport1C.FormDestroy(Sender: TObject);
begin
  inherited;
  CloseDataSet(taRec);
end;

procedure TfmExport1C.btnOkClick(Sender: TObject);
var
  Info, Comp, Mat, DelDoc, Doc, Item, Att, TablePart: IXMLNode;
  s : string;
  i : integer;
  ADecimalSeparator: Char;
  N: Double;
  W: Double;
  a: TDateTimeField;
  INN: Variant;
begin

    INN := dm.db.QueryValue('select stretrim(inn) from d_comp where compid = :company$id', 0, [dm.User.SelfCompId]);

    if VarIsNull(INN) then
    begin
      MessageDialogA('� ������� ����������� �� ������ ���!', mtError);
      Exit;
    end;

  // ������������ ������ ��� ��������
  stpBuild_1C.Params[0].AsTimeStamp := DateTimeToTimeStamp(StartOfTheDay(VarToDateTime(edBD.Value)));
  stpBuild_1C.Params[1].AsTimeStamp := DateTimeToTimeStamp(EndOfTheDay(VarToDateTime(edDate.Value)));
  stpBuild_1C.ExecProc;

  stpBuild_1C.Close;

  // ������� xml-���� ��� �������� � 1�
  XMLDoc.Active := True;
  // ����� ����������

  Info := XMLDoc.DocumentElement.AddChild('����������');
    Att := XMLDoc.CreateNode('��', ntAttribute);
      Att.NodeValue := dm.db.DatabaseName;
      Info.AttributeNodes.Add(Att);
    Att := XMLDoc.CreateNode('����_��������', ntAttribute);
      Att.NodeValue := ToDay;
      Info.AttributeNodes.Add(Att);
    Att := XMLDoc.CreateNode('������������', ntAttribute);
      Att.NodeValue := dm.User.FIO;
      Info.AttributeNodes.Add(Att);

    s := ExecSelectSQL('select Exp1C from D_Rec', dm.quTmp);

    Att := XMLDoc.CreateNode('���������_����', ntAttribute);
      Att.NodeValue := s;
      Info.AttributeNodes.Add(Att);
    Att := XMLDoc.CreateNode('��������_����', ntAttribute);
      Att.NodeValue := edDate.Text;
      Info.AttributeNodes.Add(Att);

    Att := XMLDoc.CreateNode('���', ntAttribute);
      Att.NodeValue := INN;
      Info.AttributeNodes.Add(Att);

  // �����������
  OpenDataSet(taComp);
  Comp := XMLDoc.DocumentElement.AddChild('�����������');
  while not taComp.Eof do
  begin
    Item := Comp.AddChild('COMP');
    for i:=0 to Pred(taComp.FieldCount) do with taComp.Fields[i] do
      Item.AddChild(FieldName).NodeValue := AsString;
    taComp.Next;
  end;
  CloseDataSet(taComp);

  // ���������
  OpenDataSet(taMat);
  Mat := XMLDoc.DocumentElement.AddChild('���������');
  while not taMat.Eof do
  begin
    Item := Mat.AddChild('MAT');
    for i:=0 to Pred(taMat.FieldCount) do with taMat.Fields[i] do
      Item.AddChild(FieldName).NodeValue := AsString;
    taMat.Next;
  end;
  CloseDataSet(taMat);

  // ��������� ���������
  {OpenDataSet(taDelDocument);
  DelDoc := XMLDoc.DocumentElement.AddChild('���������_���������');
  while not taDelDocument.Eof do
  begin
    Item := DelDoc.AddChild('DOCUMENT');
    for i:=0 to Pred(taDelDocument.FieldCount) do with taDelDocument.Fields[i] do
      Item.AddChild(FieldName).NodeValue := AsString;
    taDelDocument.Next;
  end;
  CloseDataSet(taDelDocument);}

  // ���������
  OpenDataSet(taDocument);
  Doc := XMLDoc.DocumentElement.AddChild('���������');
  while not taDocument.Eof do
  begin                  
    Item := Doc.AddChild('DOCUMENT');
    for i:=0 to Pred(taDocument.FieldCount) do with taDocument.Fields[i] do
      Item.AddChild(FieldName).NodeValue := AsString;


   if taDocument.FieldByName('SCHEMAID').AsInteger = 2 then
   begin
     if taDocument.FieldByName('ITYPE').AsInteger = 2 then
     begin
       Losses.Active := False;
       Losses.ParamByName('INVID').AsInteger := taDocument.FieldByName('DOCID').AsInteger;
       Losses.ParamByName('COMPID').AsInteger := taDocument.FieldByName('SUPID').AsInteger;
       Losses.Active := True;
       Losses.FetchAll;
       if Losses.RecordCount <> 0 then
       begin
         N := Losses.FieldByName('N').AsFloat;
         taDocItem.First;
         if taDocItem.RecordCount <> 0 then W := taDocItem.FieldByName('W').AsFloat
         else W := 0;
         taDocItem.First;
         ADecimalSeparator := DecimalSeparator;
         DecimalSeparator := '.';
         Item.AddChild('Losses').NodeValue := FloatToStr((100 * W/(100 - N) - W));
         DecimalSeparator := ADecimalSeparator;
       end
       else Item.AddChild('Losses').NodeValue := '0';
       Losses.Active := False;
     end
     else Item.AddChild('Losses').NodeValue := '0';
   end
   else Item.AddChild('Losses').NodeValue := '0';

    TablePart := Item.AddChild('���������_�����');
    // ������� ���������
    while not taDocItem.Eof do
    begin
      Item := TablePart.AddChild('ITEM');
      for i:=0 to Pred(taDocItem.FieldCount) do with taDocItem.Fields[i] do
      if (DataType=ftCurrency) or (DataType=ftFloat) then
        Item.AddChild(FieldName).NodeValue := StringReplace(ASString,',','.',[])
        else Item.AddChild(FieldName).NodeValue := AsString;
      taDocItem.Next;
    end;
    taDocument.Next;
  end;
  CloseDataSets([taDocument, taDocItem]);
  XMLDoc.SaveToFile(edFile.FileName);
  XMLDoc.Active := False;
  MessageDialogA('������ ���� �������� � 1�', mtInformation);
 
end;

procedure TfmExport1C.taDelDocumentBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ADATE'].AsTimeStamp := DateTimeToTimeStamp(VarToDateTime(edDate.Value));
end;

procedure TfmExport1C.taDocItemBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ID'].AsInteger := taDocument.FieldByName('ID').AsInteger;
end;

procedure TfmExport1C.taDocumentAfterOpen(DataSet: TDataSet);
begin
  ReOpenDataSet(taDocItem);
end;

procedure TfmExport1C.taDocumentAfterScroll(DataSet: TDataSet);
begin
  ReOpenDataSet(taDocItem);
end;

procedure TfmExport1C.taRecBeforeClose(DataSet: TDataSet);
begin
  DataSet.Post;
end;

procedure TfmExport1C.taRecAfterPost(DataSet: TDataSet);
begin
  taRec.Transaction.CommitRetaining;
end;

procedure TfmExport1C.cbContractExit(Sender: TObject);
begin
  inherited;
  if cbContract.ItemIndex <> 0 then
    SchemaID := cbContract.ItemIndex - 1;
end;

procedure TfmExport1C.chlbx1CGroupClickCheck(Sender: TObject);
var
  i : integer;
begin
  if(chlbx1CGroup.ItemIndex = 0)then
    for i:=1 to Pred(chlbx1CGroup.Count) do
      chlbx1CGroup.Checked[i] := False
  else begin
    chlbx1CGroup.Checked[0] := False;
    chlbx1CGroup.Checked[1] := True;
  end;
end;

procedure TfmExport1C.taDocumentFilterRecord(DataSet: TDataSet; var Accept: Boolean);
var
  ind : integer;
  isContractSatisfy : boolean;
begin
  if cbContract.ItemIndex = 0  then
    isContractSatisfy := true
  else
    isContractSatisfy := (SchemaID = DataSet.FieldByName('SchemaID').AsInteger) or (DataSet.FieldByName('SchemaID').IsNull);    

  if chlbx1CGroup.Checked[0] then Accept := isContractSatisfy
  else begin
    ind := chlbx1CGroup.Items.IndexOf(DataSet.FieldByName('GROUP_1C').AsString);
    Accept := chlbx1CGroup.Checked[ind] and isContractSatisfy;
  end;
end;

procedure TfmExport1C.edExp1CChange(Sender: TObject);
begin
  if FModified then
  begin
    PostDataSet(taRec);
    FModified := False;
  end
end;

procedure TfmExport1C.edExp1CButtonClick(Sender: TObject; var Handled: Boolean);
begin
  FModified := True;
  Handled := True;
end;

procedure TfmExport1C.taDocumentBeforeOpen(DataSet: TDataSet);
begin
  taDocument.ParamByName('SELFCOMPID').AsInteger := dm.User.SelfCompId;
end;

end.
