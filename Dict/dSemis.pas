unit dSemis;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DictAncestor, RxPlacemnt, ImgList, Grids, DBGrids, RXDBCtrl,
  ComCtrls, RxSpeedBar, ExtCtrls, db, Buttons, ActnList, StdCtrls,
  Mask, DBCtrlsEh, DBLookupEh, Variants, TB2Item, TB2Dock, TB2Toolbar,
  DBGridEh, Menus, DBGridEhGrouping, GridsEh;

//const
    (* S:string=
    'select SemisId, Semis, SName, UQ, UW, Q0, MAT, PRILL, GOOD,'+
    'EDGSHAPEID, EDGTID, CLEANNES, CHROMATICITY, IGR,'+
    'COLOR, SZ, O1, PRICE, TYPES '+
    'from D_SEMIS '+
    'where MAT between :MAT1 and :MAT2 '+
    'and GOOD between :GOOD1 and :GOOD2 ';*)
     //s_ord:string=' order by SortInd';

type
  TfmSemis = class(TfmDictAncestor)
    spbtnIns: TSpeedButton;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    TBControlItem2: TTBControlItem;
    lcbxMat: TDBLookupComboboxEh;
    TBControlItem3: TTBControlItem;
    Label5: TLabel;
    TBControlItem4: TTBControlItem;
    cbGood: TDBCheckBoxEh;
    TBControlItem5: TTBControlItem;
    lbO1: TLabel;
    TBControlItem6: TTBControlItem;
    edO1: TDBEditEh;
    TBControlItem7: TTBControlItem;
    lbSZ: TLabel;
    TBControlItem8: TTBControlItem;
    edSZ: TDBEditEh;
    TBSeparatorItem1: TTBSeparatorItem;
    acIns: TAction;
    acDocMovingSemis: TAction;
    ppSemis: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    TBItem4: TTBItem;
    TBItem5: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    function grGetCellCheckBox(Sender: TObject; Field: TField; var StateCheckBox: Integer): Boolean;
    procedure DBLookupComboboxEh1KeyValueChanged(Sender: TObject);
    procedure cbGoodClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edO1Change(Sender: TObject);
    procedure edSZChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddExecute(Sender: TObject);
    procedure acInsExecute(Sender: TObject);
    procedure acInsUpdate(Sender: TObject);
    procedure acDocMovingSemisExecute(Sender: TObject);
    procedure acDocMovingSemisUpdate(Sender: TObject);
  private
    FSemisSzId : shortstring; // �������������� �������������
    FO1Id      : shortstring; // �����
    FGoodId    : byte; // �����
    procedure Filter(DataSet : TDataSet; var Accept : boolean);
  end;

implementation

{$R *.DFM}

uses dbUtil, DbEditor, eSemisProp, DictData, DocMovingSemis;

procedure TfmSemis.FormCreate(Sender: TObject);
begin
  dm.AMatId  := ROOT_MAT_CODE;
  edO1.Enabled := False;
  edSZ.Enabled := False;
  lbSZ.Enabled := False;
  lbO1.Enabled := False;
  edO1.Text    := '';
  edSZ.Text    := '';
  FGoodId := High(Byte);
  FO1Id   := '';
  FSemisSzId := '';
  DataSet.Filtered := True;
  DataSet.OnFilterRecord := Filter;
  inherited;
end;

procedure TfmSemis.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.AMatId  := ROOT_MAT_CODE;
  DataSet.Filtered := False;
  DataSet.OnFilterRecord := nil;
  inherited;
  dm.LoadArtSL(GOOD_DICT);
  dm.LoadArtSL(SEMIS_DICT);
end;

function TfmSemis.grGetCellCheckBox(Sender: TObject; Field: TField; var StateCheckBox: Integer): Boolean;
begin
  Result := (Field.FieldName='GOOD');
end;

procedure TfmSemis.DBLookupComboboxEh1KeyValueChanged(Sender: TObject);
begin
  if(lcbxMat.KeyValue = 10)
  then begin
    edO1.Enabled := True;
    edSZ.Enabled := True;
    edO1.Text    := '';
    edSZ.Text    := '';
    lbSZ.Enabled := True;
    lbO1.Enabled := True;
  end
  else begin
    edO1.Enabled := False;
    edSZ.Enabled := False;
    lbSZ.Enabled := False;
    lbO1.Enabled := False;
  end;

  if(VarIsNull(lcbxMat.KeyValue)) then dm.AMatId := ROOT_MAT_CODE
  else dm.AMatId := VarToStr(lcbxMat.KeyValue);
  ReOpenDataSet(dm.taSemis);
end;

procedure TfmSemis.cbGoodClick(Sender: TObject);
begin
  case cbGood.State of
    cbChecked   : FGoodId := 1;
    cbUnChecked : FGoodId := 0;
    else FGoodId := High(Byte);
  end;
  ReOpenDataSet(dm.taSemis);
end;

procedure TfmSemis.edO1Change(Sender: TObject);
begin
  FO1Id := edO1.Text;
  ReOpenDataSet(dm.taSemis);
end;

procedure TfmSemis.edSZChange(Sender: TObject);
begin
  FSemisSzId := edSZ.Text;
  ReOpenDataSet(dm.taSemis);
end;

procedure TfmSemis.Filter(DataSet: TDataSet; var Accept: boolean);
begin
  Accept := True;
  case FGoodId of
    0, 1 : Accept := DataSet.FieldByName('GOOD').AsInteger = FGoodId;
    High(Byte) : Accept := True;
  end;

  if(dm.AMatId = '10') then
  begin
    if edO1.Text <> '' then Accept := Pos(edO1.Text, DataSet.FieldByName('O1').AsString) = 1;
    if Accept and (edSZ.Text <> '')then Accept := Pos(edSZ.Text, DataSet.FieldByName('SZ').AsString) =1;
  end;
end;

procedure TfmSemis.acAddExecute(Sender: TObject);
begin
  DataSet.Insert;
  //DataSet.Post;
  //DataSet.Edit;
  gridDict.SelectedField := DataSet.FieldByName('SEMISID');
end;

procedure TfmSemis.acInsExecute(Sender: TObject);
begin
  if DataSet.State in [dsInsert] then
  begin
    DataSet.FieldByName('O1').AsString := edO1.Text;
    DataSet.FieldByName('SZ').AsString := edSz.Text;
  end;
  PostDataSet(DataSet);
  ShowDbEditor(TfmeSemisProp, dm.dsrSemis);
end;

procedure TfmSemis.acInsUpdate(Sender: TObject);
begin
  acIns.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmSemis.acDocMovingSemisExecute(Sender: TObject);
begin
  ShowDocMovingSemis(dm.taSemisSEMISID.AsString);
end;

procedure TfmSemis.acDocMovingSemisUpdate(Sender: TObject);
begin
  acDocMovingSemis.Enabled := dm.taSemis.Active and (not dm.taSemis.IsEmpty);
end;

end.
