object fmUID: TfmUID
  Left = 666
  Top = 371
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'UID'
  ClientHeight = 89
  ClientWidth = 181
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object RxSpeedButton1: TRxSpeedButton
    Left = 11
    Top = 64
    Width = 76
    Height = 25
    Caption = 'Ok'
    ModalResult = 1
    OnClick = RxSpeedButton1Click
  end
  object RxSpeedButton2: TRxSpeedButton
    Left = 94
    Top = 64
    Width = 76
    Height = 25
    Caption = 'Cancel'
    ModalResult = 2
  end
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 177
    Height = 62
    Shape = bsFrame
  end
  object RxLabel1: TRxLabel
    Left = 37
    Top = 5
    Width = 101
    Height = 13
    Caption = #1059#1085#1080#1082#1072#1083#1100#1085#1099#1081' '#1085#1086#1084#1077#1088
  end
  object edUID: TDBNumberEditEh
    Left = 8
    Top = 32
    Width = 161
    Height = 19
    DecimalPlaces = 0
    EditButtons = <>
    Flat = True
    TabOrder = 0
    Value = 0
    Visible = True
    OnKeyDown = FormKeyDown
  end
end
