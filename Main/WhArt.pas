{******************************************}
{  ����� ������������                      }
{  Copyrigth (C) 2002-2004 basile for CDM  }
{  v1.05                                   }
{  last update  15.04.2004                 }
{******************************************}
unit WhArt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList, ExtCtrls, ComCtrls,
  Grids, DBGridEh, TB2Dock, TB2Toolbar, StdCtrls, Mask, DBCtrlsEh, TB2Item,
  ActnList, db, Menus, DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmWhArt = class(TfmAncestor)
    gridWhArt: TDBGridEh;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    acEvent: TActionList;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    cmbxDep: TDBComboBoxEh;
    TBControlItem2: TTBControlItem;
    TBControlItem3: TTBControlItem;
    Label2: TLabel;
    cmbxOper: TDBComboBoxEh;
    TBControlItem4: TTBControlItem;
    TBControlItem6: TTBControlItem;
    Label4: TLabel;
    edFilterArt: TDBEditEh;
    TBControlItem7: TTBControlItem;
    spitPrint: TSpeedItem;
    acPrintWhArt: TAction;
    spitHistory: TSpeedItem;
    ppHistory: TPopupMenu;
    N1: TMenuItem;
    acHistoryArt: TAction;
    acHistoryArtFromFind: TAction;
    N2: TMenuItem;
    spitArt2Ins: TSpeedItem;
    acArt2Ins: TAction;
    ppWhArt: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem3: TTBItem;
    procedure cmbxOperChange(Sender: TObject);
    procedure cmbxDepChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure edFilterArtChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPrintWhArtExecute(Sender: TObject);
    procedure acHistoryArtExecute(Sender: TObject);
    procedure acHistoryArtFromFindExecute(Sender: TObject);
    procedure acHistoryArtUpdate(Sender: TObject);
    procedure acArt2InsExecute(Sender: TObject);
    procedure acArt2InsUpdate(Sender: TObject);
  private
    procedure FilterArt(DataSet: TDataSet; var Accept: Boolean);
  end;

var
  fmWhArt: TfmWhArt;

implementation

uses MainData, DictData, dbTree, dbUtil, PrintData, hWhArt,
     fmUtils, eIns, DbEditor;

{$R *.dfm}

procedure TfmWhArt.cmbxOperChange(Sender: TObject);
begin
  dmMain.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taWHArt);
end;

procedure TfmWhArt.cmbxDepChange(Sender: TObject);
begin
  dmMain.FilterDepId := TNodeData(cmbxDep.Items.Objects[cmbxDep.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taWHArt);
end;

procedure TfmWhArt.FormCreate(Sender: TObject);
var
  SaveEvent : TNotifyEvent;
begin
  //ppWhArt.Skin := dm.ppSkin;
  dm.WorkMode := 'aWhArt';
  dmMain.FilterArt := '%';
  // ����������
  with dm.tr do
  begin
    if Active then Commit;
    StartTransaction;
  end;
  // ���������������� ������� ������ ������������
  SaveEvent := cmbxOper.OnChange;
  cmbxOper.OnChange := nil;
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.ItemIndex := 0;
  dmMain.FilterOperId := ROOT_OPER_CODE;
  cmbxOper.OnChange := SaveEvent;

  edFilterArt.Text := '';

  SaveEvent := cmbxDep.OnChange;
  cmbxDep.OnChange := nil;
  cmbxDep.Items.Assign(dm.dPSWhProd);
  cmbxDep.ItemIndex := 0;
  dmMain.FilterDepId := ROOT_PS_CODE;
  cmbxDep.OnChange := SaveEvent;
  // ������� ������ ������
  OpenDataSet(dmMain.taWhArt);
end;

procedure TfmWhArt.edFilterArtChange(Sender: TObject);
begin
  dmMain.FilterArt := edFilterArt.Text+'%';
  ReOpenDataSet(dmMain.taWhArt);
  {with dmMain.taWHArt do
    if edFilterArt.Text = '' then
    begin
      Filtered := False;
      OnFilterRecord := nil;
    end
    else if not Filtered then
    begin
      Filtered :=
      OnFilterRecord := FilterArt;
      Filtered := True;
    end}
end;

procedure TfmWhArt.FilterArt(DataSet: TDataSet; var Accept: Boolean);
begin
  if copy(DataSet.FieldByName('ART').AsString, 0, Length(edFilterArt.Text))=edFilterArt.Text
  then Accept := True else Accept := False;
end;

procedure TfmWhArt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmMain.taWhArt);
  inherited;
end;

procedure TfmWhArt.acPrintWhArtExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkWhArt, Null);
end;

procedure TfmWhArt.acHistoryArtExecute(Sender: TObject);
begin
  dmMain.hArt2Id := dmMain.taWhArtART2ID.AsInteger;
  dmMain.hSzId := ROOT_SZ_CODE;
  ShowAndFreeForm(TfmHWhArt, TForm(fmHWhArt));
end;

procedure TfmWhArt.acHistoryArtFromFindExecute(Sender: TObject);
//var Art2Id : Variant;
begin
  {Art2Id := ExecSelectSQL('select Art2Id from Art2 where Art="'+edFindArt.Text+'" and Art2="-"', dmMain.quTmp);
  if VarIsNull(Art2Id)or(Art2Id = 0)then raise EWarning.Create(Format(rsArtNotFound, [edFindArt.Text, '� �����������']));
  dmMain.hSzId := ROOT_SZ_CODE;
  ShowAndFreeForm(TfmHWhArt, TForm(fmHWhArt));}
end;

procedure TfmWhArt.acHistoryArtUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not dmMain.taWhArtART2ID.IsNull) and
                                 (not dmMain.taWhArt.IsEmpty);
end;

procedure TfmWhArt.acArt2InsExecute(Sender: TObject);
begin
  dm.AArt2Id := dmMain.taWhArtART2ID.AsInteger;
  ShowDbEditor(TfmeIns, dm.dsrA2Ins);
end;

procedure TfmWhArt.acArt2InsUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not dmMain.taWhArt.IsEmpty)and
                                 (dmMain.taWhArtART2.AsString <> '-');

end;

end.
