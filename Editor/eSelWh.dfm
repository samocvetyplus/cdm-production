inherited fmeSelWh: TfmeSelWh
  Left = 413
  Top = 287
  Caption = #1042#1099#1073#1086#1088' '#1082#1083#1072#1076#1086#1074#1086#1081
  ClientHeight = 103
  ClientWidth = 275
  OldCreateOrder = True
  OnClose = FormClose
  OnShow = FormShow
  ExplicitWidth = 281
  ExplicitHeight = 135
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 275
    Height = 103
    ExplicitWidth = 275
    ExplicitHeight = 103
    object Label1: TLabel [0]
      Left = 12
      Top = 52
      Width = 49
      Height = 13
      Caption = #1050#1083#1072#1076#1086#1074#1072#1103
    end
    object Label2: TLabel [1]
      Left = 12
      Top = 8
      Width = 222
      Height = 39
      Caption = 
        #1059' '#1042#1072#1089' %s '#1077#1089#1090#1100' '#1087#1088#1072#1074#1072' '#1076#1086#1089#1090#1091#1087#1072' '#1074' '#1073#1086#1083#1077#1077' '#1095#1077#1084' '#13#10#1086#1076#1085#1091' '#1082#1083#1072#1076#1086#1074#1091#1102'. '#1044#1083#1103' '#1088#1072#1073 +
        #1086#1090#1099' '#1085#1091#1078#1085#1086' '#1074#1099#1073#1088#1072#1090#1100#13#10#1090#1086#1083#1100#1082#1086' '#1086#1076#1085#1091' '#1082#1083#1072#1076#1086#1074#1091#1102
    end
    inherited btnOk: TBitBtn
      Left = 180
      Top = 72
      ExplicitLeft = 180
      ExplicitTop = 72
    end
    inherited btnCancel: TBitBtn
      Left = 92
      Top = 72
      Visible = False
      ExplicitLeft = 92
      ExplicitTop = 72
    end
    object lcbxDepWh: TDBLookupComboboxEh
      Left = 68
      Top = 49
      Width = 197
      Height = 19
      DropDownBox.Sizable = True
      EditButtons = <>
      Flat = True
      KeyField = 'DEPID'
      ListField = 'DEPNAME'
      ListSource = dsrDepWh
      TabOrder = 2
      Visible = True
    end
  end
  object taDepWh: TpFIBDataSet
    SelectSQL.Strings = (
      'select p.DEPID, d.NAME DEPNAME'
      'from D_PS p, D_Dep d '
      'where band(d.DEPTYPES, 2)=2 and '
      '      p.MOLID=:USERID and '
      '      p.DEPID=d.DEPID')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 28
    Top = 16
    poSQLINT64ToBCD = True
    object taDepWhDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taDepWhDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrDepWh: TDataSource
    DataSet = taDepWh
    Left = 64
    Top = 12
  end
end
