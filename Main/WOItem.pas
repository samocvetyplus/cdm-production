unit WOItem;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Ancestor, Placemnt, ImgList, SpeedBar, ExtCtrls, ComCtrls, Grids,
  DBGrids, RXDBCtrl, M207Grid, StdCtrls, Mask, DBCtrls,
  ToolEdit, pFIBDataSet, db, Menus;

type
  TfmWOItem = class(TfmAncestor)
    gr: TM207IBGrid;
    plWOrder: TPanel;
    edDocNo: TDBEdit;
    lbNoOrder: TLabel;
    lbDocDate: TLabel;
    dtedDocDate: TDBDateEdit;
    Label1: TLabel;
    lbDep: TDBText;
    ppPrint: TPopupMenu;
    mnitCU15: TMenuItem;
    mnitSep: TMenuItem;
    mnitItOrder: TMenuItem;
    mnitItInv: TMenuItem;
    mnitInv15: TMenuItem;
    mnitOrder15: TMenuItem;
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;
    spitPrint: TSpeedItem;
    procedure spitAddClick(Sender: TObject);
    procedure spitDelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure grGetCellParams(Sender: TObject; Field: TField; AFont: TFont;
      var Background: TColor; Highlight: Boolean);
    procedure mnitPrintClick(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
  private
    function GetDataSet: TpFIbDataSet;
    procedure SetDataSet(const Value: TpFIbDataSet);
  public
    property DataSet : TpFIbDataSet read GetDataSet write SetDataSet;
  end;

var
  fmWOItem: TfmWOItem;

implementation

uses MainData, dmDict, dbUtil, PrintData;

{$R *.DFM}

{ TfmWOItem }

function TfmWOItem.GetDataSet: TpFIbDataSet;
begin
  Result := gr.DataSource.DataSet as TpFIbDataSet
end;

procedure TfmWOItem.SetDataSet(const Value: TpFIbDataSet);
begin

end;

procedure TfmWOItem.spitAddClick(Sender: TObject);
begin
  DataSet.Append;
end;

procedure TfmWOItem.spitDelClick(Sender: TObject);
begin
  DataSet.Delete;
end;

procedure TfmWOItem.FormCreate(Sender: TObject);
begin
  inherited;
  with dm, dmMain do
  begin
    if not tr.Active then tr.StartTransaction;
    OpenDataSets([taWOItem, taOper, taMOL, taSemis, quDep]);
  end;
end;

procedure TfmWOItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  with dm, dmMain do
  begin
    PostDataSets([taWOrderList, taWOItem]);
    CloseDataSets([taWOItem, taOper, taMOL, taSemis, quDep]);
    if tr.Active then tr.CommitRetaining;
  end;
  Action := caFree;
end;

procedure TfmWOItem.grGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  inherited;
  if Assigned(Field)and(Field.FieldName='RecNo') then Background:= clMoneyGreen;
end;

procedure TfmWOItem.mnitPrintClick(Sender: TObject);
var
  arr : TarrDoc;
  p : integer;
begin
  gen_arr(arr, gr, dmMain.taWOItemWOItemId);
  case TMenuItem(Sender).Tag of
    1 : PrintDocument(arr, dkItInv15);
    2 : PrintDocument(arr, dkItOrder15);
    3 : begin
          p := dmMain.taWOrderListWORDERID.AsInteger;
          PrintDocument(p, dkCU15);
        end;
    4 : begin
          p := dmMain.taWOrderListWORDERID.AsInteger;
          PrintDocument(p, dkInv15);
        end;
    5 : begin
          p := dmMain.taWOrderListWORDERID.AsInteger;
          PrintDocument(p, dkOrder15);
        end;
  end;
end;

procedure TfmWOItem.spitPrintClick(Sender: TObject);
begin
  inherited;
  //
end;

end.
