inherited fmNDS: TfmNDS
  Width = 450
  Caption = ''
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 442
  end
  inherited plDict: TPanel
    Width = 442
    inherited plSelect: TPanel
      Width = 438
      inherited btnOk: TSpeedButton
        Left = 260
      end
      inherited btnCancel: TSpeedButton
        Left = 344
      end
    end
    inherited gridDict: TDBGridEh
      Width = 438
      DataSource = dm.dsrNDS
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Title.EndEllipsis = True
          Width = 29
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'LONGNAME'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077'|'#1055#1086#1083#1085#1086#1077
          Title.EndEllipsis = True
          Width = 128
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NAME'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077'|'#1050#1088#1072#1090#1082#1086#1077
          Title.EndEllipsis = True
          Width = 94
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NDS'
          Footers = <>
          Title.EndEllipsis = True
          Width = 46
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ID1C'
          Footers = <>
          Title.EndEllipsis = True
          Width = 81
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'T'
          Footers = <>
          KeyList.Strings = (
            '0'
            '1')
          PickList.Strings = (
            #1089#1074#1077#1088#1093#1091
            #1074' '#1090#1086#1084' '#1095#1080#1089#1083#1077)
          Title.EndEllipsis = True
        end>
    end
  end
  inherited tb1: TSpeedBar
    Width = 442
  end
end
