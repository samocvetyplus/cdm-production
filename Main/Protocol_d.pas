{*********************************************}
{  �������� �������������� ���� ��������      }
{  Copyrigth (C) 2001-2004 basile for CDM     }
{  v2.51                                      }
{*********************************************}
unit Protocol_d;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus, ImgList, Grids, DBGrids, RXDBCtrl,
  RxToolEdit, StdCtrls, Mask, DBCtrls, ExtCtrls,
  ComCtrls, db, DBGridEh, TB2Item, ActnList, DBCtrlsEh, TB2Dock,
  TB2Toolbar, Buttons, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmProtocol_d = class(TfmDocAncestor)
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem3: TTBItem;
    acExcel: TAction;
    spitDetail: TSpeedItem;
    ppProtocol: TTBPopupMenu;
    ppitDetailSemis: TTBItem;
    acEvent: TActionList;
    acDetail: TAction;
    TBSeparatorItem1: TTBSeparatorItem;
    acPrintProtocol: TAction;
    acPrintSJ: TAction;
    pitProtocol: TTBItem;
    itSJ: TTBItem;
    acPrintOb: TAction;
    TBItem1: TTBItem;
    SpeedItem1: TSpeedItem;
    acArt: TAction;
    TBItem2: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    TBItem4: TTBItem;
    acUpdProtocol: TAction;
    TBItem5: TTBItem;
    TBItem7: TTBItem;
    acShowId: TAction;
    spitRefresh: TSpeedItem;
    acCorrectProtocol: TAction;
    Label1: TLabel;
    txtUpd: TDBText;
    spit1C: TSpeedItem;
    acProtocolMat: TAction;
    TBSeparatorItem4: TTBSeparatorItem;
    acCheckData: TAction;
    TBItem8: TTBItem;
    acPsCover: TAction;
    SpeedButton1: TSpeedButton;
    procedure FormCreate(Sender: TObject);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh; AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acDetailExecute(Sender: TObject);
    procedure acPrintProtocolExecute(Sender: TObject);
    procedure acPrintSJExecute(Sender: TObject);
    procedure acPrintObExecute(Sender: TObject);
    procedure acExcelExecute(Sender: TObject);
    procedure acArtExecute(Sender: TObject);
    procedure acDetailUpdate(Sender: TObject);
    procedure acUpdProtocolExecute(Sender: TObject);
    procedure acShowIdExecute(Sender: TObject);
    procedure acCorrectProtocolExecute(Sender: TObject);
    procedure acProtocolMatExecute(Sender: TObject);
    procedure acCheckDataExecute(Sender: TObject);
    procedure acUpdProtocolUpdate(Sender: TObject);
    procedure acArtUpdate(Sender: TObject);
    procedure acProtocolMatUpdate(Sender: TObject);
    procedure spitCloseClick(Sender: TObject);
    procedure acPsCoverExecute(Sender: TObject);

  public
    procedure Access;
  end;

var
  fmProtocol_d: TfmProtocol_d;

implementation

uses MainData, ProtItem, dbUtil, fmUtils, DictData, PrintData, InvData, 
     m207Export, dbTree, ProtWhItem, MsgDialog, PArt, Math, ProtocolMat,
  CheckProtocolData, Protocol_PsCover;

{$R *.dfm}

procedure TfmProtocol_d.FormCreate(Sender: TObject);
begin
  Access;
  FDocName := '��������';
  inherited;
  InitBtn(Boolean(dmMain.taPListISCLOSE.AsInteger));
  acArt.Visible := dmMain.taPListA.AsInteger = 1;
  //ppProtocol.Skin := dm.ppSkin;
  //ppPrint.Skin := dm.ppSkin;
  gridItem.FieldColumns['ID'].Visible := False;
  gridItem.FieldColumns['PSID'].Visible := False;
  gridItem.FieldColumns['OPERID'].Visible := False;
  gridItem.FieldColumns['TRANSID'].Visible := False;
end;

procedure TfmProtocol_d.gridItemGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  Background := clInfoBk;
  if(dmMain.taProtocol_dMATID.AsString <> '1')then
  begin
    Background := clSkyBlue;
    if(Column.FieldName='S')and(Abs(RoundTo(Column.Field.AsFloat, -3))<>0) then Background := clRed;
  end
  else begin
    if dmMain.taProtocol_dID.IsNull then
    begin
      if(Column.FieldName='S')and(Abs(RoundTo(Column.Field.AsFloat, -3))<>0) then Background := clRed
      else Background := clMoneyGreen;
    end
    else if(dmMain.taProtocol_dPSNAME.AsString = '')then Background := clWhite
    else if(Column.FieldName='DONEW')then Background := clMoneyGreen;
  end;
end;

procedure TfmProtocol_d.acDetailExecute(Sender: TObject);
var
  Bookmark : TBookmark;
begin
  with dmMain do
  begin
    if(taProtocol_dID.IsNull)or(taProtocol_dOPERID.AsString='') then eXit;
    ShowAndFreeForm(TfmProtItem, TForm(fmProtItem));
    dm.tr.CommitRetaining;
    // ������� ��������
    Bookmark := dmMain.taProtocol_d.GetBookmark;
    try
      while not dmMain.taProtocol_dID.IsNull do
      begin
        if(dmMain.taProtocol_dMATID.AsString='1')then dmMain.taProtocol_d.Refresh;
        dmMain.taProtocol_d.Next;
      end;
      dmMain.taProtocol_d.Refresh;
    finally
      dmMain.taProtocol_d.GotoBookmark(Bookmark);
    end;
  end;
end;

procedure TfmProtocol_d.acPrintProtocolExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taProtocol_d, dmMain.taPList]);
  dmPrint.ProtocolId := dmMain.taPListID.AsInteger;
  dmPrint.PrintDocumentA(dkProtocol_d, VarArrayOf([VarArrayOf([dmMain.taPListID.AsInteger]),
                VarArrayOf([dmMain.taPListID.AsInteger, 1])]));
end;

procedure TfmProtocol_d.acPrintSJExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taProtocol_d, dmMain.taPList]);
  //ExecSQL('execute procedure G_Tails('+dmMain.taPListID.AsString+')', dmMain.quTmp);
  dmPrint.ProtocolId := dmMain.taPListID.AsInteger;
  dmPrint.PrintDocumentA(dkSJ_d, Null);
end;

procedure TfmProtocol_d.acPrintObExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taProtocol_d, dmMain.taPList]);
  dmPrint.PrintDocumentA(dkOb_d, VarArrayOf([VarArrayOf([dmMain.taPListID.AsInteger])]));
end;

procedure TfmProtocol_d.acExcelExecute(Sender: TObject);
var
  f, c : string;
begin
  c := '�������� �������������� '+dmMain.taPListDOCNO.AsString+'  ������ ��������';
  f := ExtractFileDir(ParamStr(0))+'\'+c+'.xls';
  GridToXLS(gridItem, f, c, True, False);
end;

procedure TfmProtocol_d.acArtExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmPArt, TForm(fmPArt));
end;

procedure TfmProtocol_d.acDetailUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not FReadOnly) and ((not dmMain.taProtocol_dID.IsNull)or
                                 (dmMain.taProtocol_dOPERID.AsString <> ''));
end;

procedure TfmProtocol_d.acUpdProtocolExecute(Sender: TObject);
begin
  Screen.Cursor:=crSQLWait;
  with dmMain do
  try
    quTmp.Transaction.CommitRetaining;

    // ������ ����
    ExecSQL('execute procedure G_Norma("'+DateToStr(taPListBD.AsDateTime)+'", "'+DateTimeToStr(taPListDOCDATE.AsDateTime)+'")', quTmp, False);
    // �������� ���� ��������������
    ExecSQL('execute procedure Create_Protocol_PS('+taPListID.AsString+')', quTmp, False);
    // �������� ���� �������������� �� ������������
    ExecSQL('execute procedure Create_PArt('+taPListID.AsString+')', quTmp, False);
    // ������ � ���� �� ������
    ExecSQL('execute procedure Create_ActTails('+taPListID.AsString+')', dmMain.quTmp, False);
    // ������ �� ��������� �������
    ExecSQL('execute procedure Create_ProtocolMat('+taPListID.AsString+')', dmMain.quTmp, False);

    quTmp.Transaction.CommitRetaining;

    Application.ProcessMessages;
    ReOpenDataSet(DataSet);
    RefreshDataSet(dmMain.taPList);

    Screen.Cursor:=crDefault;
  except
    on E: Exception do
    begin
      quTmp.Transaction.RollbackRetaining;
      Screen.Cursor:=crDefault;
      Application.MessageBox(PChar(E.Message) , '������', MB_OK);
    end;
  end;
  MessageDialogA('�������� �� ���������� � ������������ �����������', mtInformation);
end;

procedure TfmProtocol_d.acShowIdExecute(Sender: TObject);
begin
  gridItem.FieldColumns['ID'].Visible := not gridItem.FieldColumns['ID'].Visible;
  gridItem.FieldColumns['PSID'].Visible := not gridItem.FieldColumns['PSID'].Visible;
  gridItem.FieldColumns['OPERID'].Visible := not gridItem.FieldColumns['OPERID'].Visible;
  gridItem.FieldColumns['TRANSID'].Visible := not gridItem.FieldColumns['TRANSID'].Visible;
end;

procedure TfmProtocol_d.acCorrectProtocolExecute(Sender: TObject);
begin
  PostDataSet(dmMain.taProtocol_d);
  ExecSQL('execute procedure Correct_Protocol_PS('+dmMain.taPListID.AsString+')', dmMain.quTmp);
  ReOpenDataSet(dmMain.taProtocol_d);
end;

procedure TfmProtocol_d.acProtocolMatExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmProtocolMat, TForm(fmProtocolMat));
end;

procedure TfmProtocol_d.acCheckDataExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmCheckProtocolData, TForm(fmCheckProtocolData));
end;

procedure TfmProtocol_d.acUpdProtocolUpdate(Sender: TObject);
begin
  acUpdProtocol.Enabled :=  (not FReadOnly) and (dmMain.taPListISCLOSE.AsInteger = 0);
end;

procedure TfmProtocol_d.Access;
begin
  with stbrStatus.Panels[0] do
    if dm.IsAdm then
      Text := 'Adm'
    else if((dm.User.AccProd_d and 8) = 1)then
    begin
     Text := 'View';
     FReadOnly := True;
    end
    else
      begin
        Text := 'Full';
        FreadOnly := False;
      end;
end;

procedure TfmProtocol_d.acArtUpdate(Sender: TObject);
begin
  acArt.Enabled := (not FReadOnly);
end;

procedure TfmProtocol_d.acProtocolMatUpdate(Sender: TObject);
begin
  acProtocolMat.Enabled := (not FReadOnly);
end;

procedure TfmProtocol_d.spitCloseClick(Sender: TObject);
var
  DialogResult: TModalResult;
begin

  if(FReadOnly)then
    begin
      ShowMessage('� ��� ��� ���� ��� ���������� ������ ��������.');
      eXit;
    end;

  if dmMain.taPListISCLOSE.AsInteger = 0 then
  begin

    DialogResult := MessageDlg('��������! ����� ��������� ����� ����������� ���������� ���������!', mtInformation, [mbOk, mbCancel], 0);

    if DialogResult = mrOk then
    begin
      acUpdProtocol.Execute;
    end else
    begin
      Exit;
    end;

  end;

  inherited;
  
end;

procedure TfmProtocol_d.acPsCoverExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmProtocol_PsCover, TForm(fmProtocol_PsCover));
end;

end.
