unit fmAssayStickersPrint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, WinSpool, Printers;

type
  TfmPrintAssayStickers = class(TForm)
    cbGood: TComboBox;
    cbCondition: TComboBox;
    edWeightLimit: TEdit;
    lbUnit: TLabel;
    cbWhiteInsertions: TComboBox;
    edTotalQ: TEdit;
    edTotalW: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    btnPrint: TButton;
    procedure btnPrintClick(Sender: TObject);
  private
    { Private declarations }
    procedure PrintAssayStickers;
    function IncomeValuesCorrect: Boolean;
  public
    { Public declarations }
    class procedure Execute;
  end;

var
  fmPrintAssayStickers: TfmPrintAssayStickers;

implementation

{$R *.dfm}

procedure TfmPrintAssayStickers.btnPrintClick(Sender: TObject);
begin
  if IncomeValuesCorrect then
    PrintAssayStickers
  else
    Exit;
end;

function TfmPrintAssayStickers.IncomeValuesCorrect: Boolean;
begin
  Result := true;

  if (trim(edWeightLimit.Text) = '') or (trim(edTotalQ.Text) = '') or (trim(edTotalW.Text) = '') then
  begin
    raise Exception.Create('�� ��������� ���� �� �������� ���������� (����)!'#13#10 +
                '����������, ��������� ��� ��������!');
    Result := false;
  end;

  try
    StrToInt(edTotalQ.Text);
  except
    raise Exception.Create('������ � �������� ������ ���-�� �������!');
    edTotalQ.SetFocus;
    Result := false;
  end;

  try
    StrToFloat(edTotalW.Text);
  except
    raise Exception.Create('������ � �������� ������ ���� �������!');
    edTotalW.SetFocus;
    Result := false;
  end;

  try
    StrToFloat(edWeightLimit.Text);
  except
    raise Exception.Create('������ � �������� ������������ ���� �������!');
    edWeightLimit.SetFocus;
    Result := false;
  end;

end;

procedure TfmPrintAssayStickers.PrintAssayStickers;
const
  Zebra_dpi = 203;
  LP_COMMAND_DELIM = '' + #10;
var
  lPrinter: Cardinal;
  lDocInfo: TDocInfo1;
  lPrintData: String;
  lPrintedBytes: Cardinal;
  lPrintDataLength: Cardinal;
  ScalingFactor: Double;
  PrinterName: string;
begin
  PrinterName := 'ZDesigner LP 2824';

  if not OpenPrinter(PChar(PrinterName), lPrinter, nil) then raise Exception.Create('������ ����� � ���������!');
  try
    lDocInfo.pDocName := PCHAR('����� ��� ��������� ���������'  + ' - ' +  '1 ��.');
    lDocInfo.pOutputFile := nil;
    lDocInfo.pDatatype := 'RAW';
    ScalingFactor := Zebra_dpi/25.1;
    lPrintData :=  LP_COMMAND_DELIM
                + 'O' + LP_COMMAND_DELIM
                + 'N' + LP_COMMAND_DELIM
                + 'W' + LP_COMMAND_DELIM
                + 'D7' + LP_COMMAND_DELIM
                + 'Q250' + LP_COMMAND_DELIM
                + 'q430' + LP_COMMAND_DELIM
                + 'I8,C' + LP_COMMAND_DELIM
                + 'A75,30,0,3,1,1,N,"' + cbGood.Text + '"' + LP_COMMAND_DELIM
                + 'A195,30,0,3,1,1,N,"' + cbCondition.Text + '"' + LP_COMMAND_DELIM
                + 'A275,30,0,3,1,1,N,"' + edWeightLimit.Text + '"' + LP_COMMAND_DELIM
                + 'A305,30,0,3,1,1,N,"�."' +  LP_COMMAND_DELIM
                + 'A75,70,0,3,1,1,N,"' + cbWhiteInsertions.Text + '"' + LP_COMMAND_DELIM
                + 'A75,110,0,3,1,1,N,"' + edTotalQ.Text + '"' + LP_COMMAND_DELIM
                + 'A125,110,0,3,1,1,N,"��."' + LP_COMMAND_DELIM
                + 'A175,110,0,3,1,1,N,"' + edTotalW.Text + '"' + LP_COMMAND_DELIM
                + 'A275,110,0,3,1,1,N,"�."' + LP_COMMAND_DELIM
                + 'P1' + LP_COMMAND_DELIM;
    lPrintDataLength := Length(lPrintData);
    if StartDocPrinter(lPrinter, 1, @lDocInfo) = 0 then raise Exception.Create('������ ������ ������!');
    if not WritePrinter(lPrinter, PAnsiChar(AnsiString(lPrintData)), lPrintDataLength, lPrintedBytes) then raise Exception.Create('������ �������� ������ � ���� ��������!');
    if lPrintedBytes < lPrintDataLength then raise Exception.Create('������� ������� �������!');
    if not EndDocPrinter(lPrinter) then raise Exception.Create('���������� ��������� ������!');
  finally
    ClosePrinter(lPrinter);
  end;
end;

class procedure TfmPrintAssayStickers.Execute;
begin
  fmPrintAssayStickers := nil;
  try
    fmPrintAssayStickers := TfmPrintAssayStickers.Create(Application);
    fmPrintAssayStickers.ShowModal;
  finally
    FreeAndNil(fmPrintAssayStickers);
  end;
end;

end.
