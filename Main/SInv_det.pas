unit SInv_det;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls, ComCtrls,
  DBCtrls, RxToolEdit, RXDBCtrl, StdCtrls, Mask,  Grids, DBGrids,
  db, Menus, pFIBQuery, Buttons, DBCtrlsEh,
  DBGridEh, DBLookupEh, jpeg, ActnList, RxCurrEdit, TB2Item, ListAncestor,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxGridCardView, dxBar, Provider, FIBDataSet, pFIBDataSet, DBClient,
  FR_DSet, FR_DBSet, FR_Class, DBGridEhGrouping, GridsEh,
  cxLookAndFeels, cxLookAndFeelPainters;

const
  JpegClipboardFormat = $1200;
  
type
  TfmSInv_det = class(TfmAncestor)
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;
    spitId: TSpeedItem;
    paHeader: TPanel;
    Label1: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    txtDep: TDBText;
    Label2: TLabel;
    spitClose: TSpeedItem;
    spitIns: TSpeedItem;
    pmA: TPopupMenu;
    miAddArt: TMenuItem;
    miInsFromSearch: TMenuItem;
    miAEdit: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N28: TMenuItem;
    N27: TMenuItem;
    N26: TMenuItem;
    N25: TMenuItem;
    N36: TMenuItem;
    pmA2: TPopupMenu;
    miIns: TMenuItem;
    N4: TMenuItem;
    N9: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N34: TMenuItem;
    plAdditional: TPanel;
    plFilter: TPanel;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    SpeedItem2: TSpeedItem;
    lbSemis: TListBox;
    lbMat: TListBox;
    Splitter3: TSplitter;
    lbPrill: TListBox;
    Splitter4: TSplitter;
    Splitter5: TSplitter;
    lbIns: TListBox;
    Panel2: TPanel;
    sbAll: TSpeedButton;
    sbFilter: TSpeedButton;
    pcSTORE: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Panel5: TPanel;
    lbFind: TLabel;
    edArt: TComboEdit;
    Panel1: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    Label5: TLabel;
    PopupMenu1: TPopupMenu;
    lbNDS: TLabel;
    cbIsBrack: TDBCheckBoxEh;
    grEL: TDBGridEh;
    gridUID: TDBGridEh;
    dgA2: TDBGridEh;
    gridArt: TDBGridEh;
    DBGridEh3: TDBGridEh;
    Label8: TLabel;
    DBText1: TDBText;
    lcbxDepFrom: TDBLookupComboboxEh;
    edSN: TDBEditEh;
    lcbxNDS: TDBLookupComboboxEh;
    deSDate: TDBDateTimeEditEh;
    cbCom: TDBComboBoxEh;
    lbInvKind: TLabel;
    edUpPrice: TDBNumberEditEh;
    lbUpprice: TLabel;
    imArt: TImage;
    ActionList1: TActionList;
    acPriceDict: TAction;
    acUpdatePrice: TAction;
    acAdd: TAction;
    acDel: TAction;
    acDet: TAction;
    acIns: TAction;
    acPrint: TAction;
    acOutAnailz: TAction;
    lbDog: TLabel;
    mmDescr: TDBMemo;
    lbPaytype: TLabel;
    lcbxPaytype: TDBLookupComboboxEh;
    spitAnalize: TSpeedItem;
    grTransport: TGroupBox;
    edTransPrice: TDBNumberEditEh;
    Label12: TLabel;
    Label13: TLabel;
    edTransPercent: TDBNumberEditEh;
    ppAddToInv: TPopupMenu;
    acAddAllBrill: TAction;
    N1: TMenuItem;
    SpeedItem8: TSpeedItem;
    acAddAllNoBrill: TAction;
    N2: TMenuItem;
    btINS: TButton;
    acShowID: TAction;
    acRefreshMemo: TAction;
    Label14: TLabel;
    lcbxContract: TDBLookupComboboxEh;
    acAdd2: TAction;
    Label15: TLabel;
    lcbxInvColor: TDBLookupComboboxEh;
    ppInv: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    ppPrice: TTBPopupMenu;
    TBItem4: TTBItem;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    acReturnPrice: TAction;
    spitPrice: TSpeedItem;
    TBItem3: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    acSetPrice2: TAction;
    cbPrice: TDBCheckBoxEh;
    acInvCopy: TAction;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    acLoadInv: TAction;
    acCheck: TAction;
    SpeedItem4: TSpeedItem;
    TBItem7: TTBItem;
    acPrice2Price: TAction;
    acOpenSellUID: TAction;
    ppUID: TTBPopupMenu;
    TBItem8: TTBItem;
    acUIDInfo: TAction;
    acUIDHistory: TAction;
    SpeedItem1: TSpeedItem;
    SpeedItem3: TSpeedItem;
    spitETolling: TSpeedItem;
    acEServParams: TAction;
    ppETolling: TPopupMenu;
    N3: TMenuItem;
    acEServMat: TAction;
    N5: TMenuItem;
    acAddW: TAction;
    N6: TMenuItem;
    SpeedButton1: TSpeedButton;
    DBEditEh1: TDBEditEh;
    Label7: TLabel;
    ed_Prihod: TEdit;
    Label9: TLabel;
    edNeedW: TDBEditEh;
    edDiffW: TDBEditEh;
    Label10: TLabel;
    TBItem9: TTBItem;
    acSetPrice: TAction;
    TabSheetPenalty: TTabSheet;
    GridPenaltyDBTableView1: TcxGridDBTableView;
    GridPenaltyLevel1: TcxGridLevel;
    GridPenalty: TcxGrid;
    GridPenaltyDBTableView1DOCNO: TcxGridDBColumn;
    GridPenaltyDBTableView1DOCDATE: TcxGridDBColumn;
    GridPenaltyDBTableView1COST: TcxGridDBColumn;
    GridPenaltyDBTableView1PENALTY: TcxGridDBColumn;
    BarDockControl: TdxBarDockControl;
    BarManager: TdxBarManager;
    dxBarButton2: TdxBarButton;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    PenaltyFilter: TdxBarCombo;
    InvoiceArticles: TClientDataSet;
    DBInvoiceArticles: TpFIBDataSet;
    DBInvoiceArticlesUID: TIntegerField;
    DBInvoiceArticlesSTATE: TIntegerField;
    DBInvoiceArticlesART: TFIBStringField;
    DBInvoiceArticlesART2: TFIBStringField;
    DBInvoiceArticlesDOCDATE: TDateTimeField;
    DBInvoiceArticlesSZ: TFIBStringField;
    DBInvoiceArticlesW: TFloatField;
    DBInvoiceArticlesU: TFIBStringField;
    DBInvoiceArticlesPRICE: TFloatField;
    DBInvoiceArticlesSUM_: TFloatField;
    DBInvoiceArticlesSTATE_NAME: TFIBStringField;
    DBInvoiceArticlesART2ID: TIntegerField;
    DBInvoiceArticlesARTID: TIntegerField;
    DBInvoiceArticlesSZID: TIntegerField;
    DBInvoiceArticlesPRICE2: TFIBFloatField;
    DBInvoiceArticlesSELFCOMPID: TFIBIntegerField;
    DBInvoiceArticlesSELFCOMPNAME: TFIBStringField;
    ProviderInvoiceArticles: TDataSetProvider;
    DataSourceInvoiceArticles: TDataSource;
    InvoiceArticlesRecNo: TIntegerField;
    InvoiceArticlesUID: TIntegerField;
    InvoiceArticlesSTATE: TIntegerField;
    InvoiceArticlesART: TStringField;
    InvoiceArticlesART2: TStringField;
    InvoiceArticlesDOCDATE: TDateTimeField;
    InvoiceArticlesSZ: TStringField;
    InvoiceArticlesW: TFloatField;
    InvoiceArticlesU: TStringField;
    InvoiceArticlesPRICE: TFloatField;
    InvoiceArticlesSUM_: TFloatField;
    InvoiceArticlesSTATE_NAME: TStringField;
    InvoiceArticlesART2ID: TIntegerField;
    InvoiceArticlesARTID: TIntegerField;
    InvoiceArticlesSZID: TIntegerField;
    InvoiceArticlesPRICE2: TFloatField;
    InvoiceArticlesSELFCOMPID: TIntegerField;
    InvoiceArticlesSELFCOMPNAME: TStringField;
    InvoiceArticlesView: TClientDataSet;
    EditorSearchArticleUID: TEdit;
    EditorSearchArticleClass: TEdit;
    DBInvoiceArticlesSEARCHUID: TFIBStringField;
    InvoiceArticlesSEARCHUID: TStringField;
    DBInvoiceArticle: TpFIBDataSet;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    DateTimeField1: TDateTimeField;
    FIBStringField3: TFIBStringField;
    FloatField1: TFloatField;
    FIBStringField4: TFIBStringField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FIBStringField5: TFIBStringField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    FIBFloatField1: TFIBFloatField;
    FIBIntegerField1: TFIBIntegerField;
    FIBStringField6: TFIBStringField;
    FIBStringField7: TFIBStringField;
    Label11: TLabel;
    EditorSearchArticleSubClass: TEdit;
    N7: TMenuItem;
    N8: TMenuItem;
    spitLabels: TSpeedItem;
    ActionLabels: TAction;
    ReportLabels: TfrReport;
    frDataSetLabels: TfrDBDataSet;
    DataSetLabels: TpFIBDataSet;
    DataSetLabelsCOST: TFIBFloatField;
    DataSetLabelsSZ: TFIBStringField;
    DataSetLabelsART: TFIBStringField;
    DataSetLabelsART2: TFIBStringField;
    DataSourceLabels: TDataSource;
    ProviderInvoiceArticlesView: TDataSetProvider;
    Label16: TLabel;
    labelStateDate: TLabel;
    procedure spitCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure lbFilterClick(Sender: TObject);
    procedure sbAllClick(Sender: TObject);
    procedure sbFilterClick(Sender: TObject);
    procedure edArtEnter(Sender: TObject);
    procedure edArtChange(Sender: TObject);
    procedure edArtButtonClick(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure N27Click(Sender: TObject);
    procedure N26Click(Sender: TObject);
    procedure dgArtGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure dgArtCellClick(Column: TColumn);
    procedure dgArtKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure btINSClick(Sender: TObject);
    procedure lcbxDepFromClick(Sender: TObject);
    procedure acPriceDictExecute(Sender: TObject);
    procedure acUpdatePriceExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDetExecute(Sender: TObject);
    procedure lcbxPaytypeChange(Sender: TObject);
    procedure acOutAnailzExecute(Sender: TObject);
    procedure edTransPercentExit(Sender: TObject);
    procedure acAddAllBrillExecute(Sender: TObject);
    procedure acShowIDExecute(Sender: TObject);
    procedure acAdd2Execute(Sender: TObject);
    procedure acInsExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acReturnPriceExecute(Sender: TObject);
    procedure lcbxDepFromEnter(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure edUpPriceExit(Sender: TObject);
    procedure acSetPrice2Execute(Sender: TObject);
    procedure acInvCopyExecute(Sender: TObject);
    procedure acLoadInvExecute(Sender: TObject);
    procedure deSDateExit(Sender: TObject);
    procedure deSDateEnter(Sender: TObject);
    procedure deSDateUpdateData(Sender: TObject; var Handled: Boolean);
    procedure acCheckExecute(Sender: TObject);
    procedure acPrice2PriceExecute(Sender: TObject);
    procedure edUpPriceEditButtons0Click(Sender: TObject; var Handled: Boolean);
    procedure acOpenSellUIDExecute(Sender: TObject);
    procedure acOpenSellUIDUpdate(Sender: TObject);
    procedure lcbxDepFromKeyValueChanged(Sender: TObject);
    procedure lcbxDepFromExit(Sender: TObject);
    procedure acUIDInfoExecute(Sender: TObject);
    procedure acUIDInfoUpdate(Sender: TObject);
    procedure acUIDHistoryExecute(Sender: TObject);
    procedure acEServParamsExecute(Sender: TObject);
    procedure acEServMatExecute(Sender: TObject);
    procedure acAddWUpdate(Sender: TObject);
    procedure acAddWExecute(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure DBEditEh1Change(Sender: TObject);
    procedure acSetPriceExecute(Sender: TObject);
    procedure PenaltyFilterChange(Sender: TObject);
    procedure GridPenaltyExit(Sender: TObject);
    procedure pcSTOREChange(Sender: TObject);
    procedure InvoiceArticlesCalcFields(DataSet: TDataSet);
    procedure InvoiceArticlesBeforeOpen(DataSet: TDataSet);
    procedure EditorSearchArticleUIDChange(Sender: TObject);
    procedure EditorSearchArticleClassChange(Sender: TObject);
    procedure EditorSearchArticleUIDKeyPress(Sender: TObject;
      var Key: Char);
    procedure EditorSearchArticleClassKeyPress(Sender: TObject;
      var Key: Char);
    procedure gridUIDDblClick(Sender: TObject);
    procedure EditorSearchArticleSubClassKeyPress(Sender: TObject;
      var Key: Char);
    procedure EditorSearchArticleSubClassChange(Sender: TObject);
    procedure gridUIDTitleBtnClick(Sender: TObject; ACol: Integer;
      Column: TColumnEh);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure ActionLabelsExecute(Sender: TObject);
    procedure DataSetLabelsBeforeOpen(DataSet: TDataSet);
    procedure lcbxContractExit(Sender: TObject);
    procedure lcbxContractKeyValueChanged(Sender: TObject);
    procedure DBInvoiceArticlesAfterOpen(DataSet: TDataSet);
    procedure lcbxContractEnter(Sender: TObject);
    procedure acPriceDictUpdate(Sender: TObject);
    procedure acSetPriceUpdate(Sender: TObject);
    procedure acEServParamsUpdate(Sender: TObject);
    procedure acEServMatUpdate(Sender: TObject);
    procedure acLoadInvUpdate(Sender: TObject);
  protected
    {$ifdef IBX}
    FQuery : TIBSQL;
    {$elseif FIBP}
    FQuery : TpFIBQuery;
    {$ifend}
  private
    FReadOnly: Boolean;
    SearchEnable: boolean;
    StopFlag: boolean;
    FChangeSDate : boolean;
    FChangeFrom : boolean;
    PerviousContractID : Integer;
    procedure ListBoxKeyPress(Sender: TObject;Var  Key: char);
    procedure FilterInvColor(DataSet: TDataSet; var Accept: Boolean);
    procedure InvColorGetCellParams(Sender: TObject; EditMode: Boolean; Params: TColCellParamsEh);
    function StopFindArt:boolean;
    procedure LoadInv(const FileName: string);
    procedure ReCalcGridTotalRecord;
    procedure OpenInvoiceArticles;
    procedure CloseInvoiceArticles;
    procedure ReOpenInvoiceArticles;
    procedure Access;
  public
    //FId : integer; //
    FDocName : string; // ������ ������������� ��� ���� � ������������ �� �����
                       // ����� ���������
    FEnableScroll:boolean;
    ContractChange : boolean;
    OldContractID: String;

    iw: double;
    procedure OnInvoiceArticleScaned(UID: Integer);
    procedure DeleteInvoiceArticle(ArticleID: Integer);
    procedure InsertInvoiceArticle(ArticleID: Integer);
  end;

var
  fmSInv_det: TfmSInv_det;


implementation

uses dbTree, MainData, DictData, dbUtil, DateUtils, {Ins,} fmUtils,
  dComp, ApplData, InvData, SItem, dMat, dPrill, dSemis, DictAncestor,
  PrintData, INV_STORE, SList, MsgDialog, dPrice, eArtIns,
  eLostStones, SelectInv, Inv_Store4ArtID,
  ProductionConsts, eNotInInv, FIBQuery, eUIDInfo, ItemHistory, EServParams,
  EServMat, Editor, eInvWeight, SInv_Det_SIList,
  frmSetPrice, uUtils, uDialogs;

{$R *.dfm}

procedure TfmSInv_det.spitCloseClick(Sender: TObject);
var AFound: Variant;
begin

  PostDataSet(dmInv.taSList);

  if spitClose.BtnCaption = '������'  then
  begin
    if dmINV.taINVItems.RecordCount=0 then
       raise EWarning.Create(rsInvEmpty);

    if (dmInv.taINVItemsPRICE2.AsCurrency < 400) and (dmInv.taSListITYPE.AsInteger in [2, 3]) then
    if MessageDialog('��������! ������ ����! ������� � ������� ����� ��������?', mtWarning, [mbYes, mbNo], 0) = mrNo then
      eXit;

    if (dmInv.IType = 2) and (StrToInt(VarToStr(lcbxContract.Value)) = 5) then
      begin
        AFound := ExecSelectSql('select id from eserv_params where invid =' + dmInv.taSListINVID.AsString, dmMain.quTmp);
        if VarIsNull(AFound) then
          begin
            DialogErrorOkMessage('��������� ��������� ��������!');
            eXit;
          end;
      end;

    if (dmINV.ITYPE = 12) then
      if (not ((dm.User.DepId = 55) and (dm.User.Profile = 1))) then
        begin
          MessageDialog(dm.User.FName + ' ' + dm.User.LName + ', � ��� ��� ���� ��� �������� ' + #10#13 +'��������� �������� �� ������������!', mtWarning, [mbOk], 0);
          eXit;
        end;

    ExecSQL('execute procedure CloseDoc("'+FDocName+'",'+dmInv.taSListINVID.AsString+')', dm.quTmp);

    dmInv.taSList.Refresh;
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 5;
  end
  else begin
      if (dmINV.ITYPE = 12) and (not dm.IsAdm) then
      if not ((dm.User.DepId = 55) and (dm.User.Profile = 1)) then
        begin
          MessageDialog(dm.User.FName + ' ' + dm.User.LName + ', � ��� ��� ���� ��� �������� ' + #10#13 +' � ��������� ��������� �������� �� ������������!', mtWarning, [mbOk], 0);
          eXit;
        end;
    ExecSQL('execute procedure OpenDoc("'+FDocName+'",'+dmInv.taSListINVID.AsString+')', dm.quTmp);
    dm.quTmp.Transaction.CommitRetaining;
    dmInv.taSList.Refresh;
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 6;
  end;
  if dmINV.taSListISCLOSE.ASinteger = 1 then grEL.Options := grEL.Options - [dgEditing]
  else grEL.Options := grEL.Options + [dgEditing];

  labelStateDate.Caption := FormatDateTime('DD/MM/YYYY', dmInv.taSListCREATED.AsDateTime);
end;

procedure TfmSInv_det.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dgA2.FieldColumns['ART2ID'].Visible := False;
  dgA2.FieldColumns['PRICE'].Visible := False;
  gridArt.FieldColumns['ARTID'].Visible := False;
  inherited;
  with dmInv do
  begin
    PostDataSets([dmInv.taSList, dmInv.taINVItems]);
    QSEL:=1;
    // ������� �������� �����
    CloseDataSet(dm.taInvColor);
    dm.taInvColor.Filtered := False;
    dm.taInvColor.OnFilterRecord := nil;
    CloseDataSets([taINVItems, taComp, taDepname, taSItem, taOutStore, dm.taContract,
      dm.taArt, dm.taIns, dm.taDep, dm.taNDS, dm.taPaytype]);
    if taINVItems.Transaction.Active then taINVItems.Transaction.CommitRetaining;
    dm.taArt.Filtered := False;
    dm.taArt.Filter := '';
    // ������� ����� � ������������ ���������
    if Assigned(fmArtIns) then
    begin
      fmArtIns.Close;
      FreeAndNil(fmArtIns);
    end;
  end;

  if dmMain.taPenalty.State = dsEdit then
  dmMain.taPenalty.Post;
  CloseDataSet(dmMain.taPenalty);
  CloseInvoiceArticles;
end;

procedure TfmSInv_det.Access;
var
  ReadOnly: Boolean;
  i:integer;
  Control: TControl;
begin

  if dm.IsAdm then Exit; 

  ReadOnly := (dmInv.taSListISCLOSE.AsInteger = 1) or (dm.User.Profile <> 2)
               or  not (dmInv.taINVItems.Active and dm.taArt.Active);

  FReadOnly := ReadOnly;

  spitETolling.Enabled := not ReadOnly;

  spitPrice.Enabled := not ReadOnly;

  SpeedItem8.Enabled := not ReadOnly;

  Control := nil;

  for i := 0 to paHeader.ControlCount - 1 do
  begin

    Control := paHeader.Controls[i];

    if not (Control is TLabel) then
    begin

      Control.Enabled := not ReadOnly;

    end;

  end;

  plAdditional.Visible := not ReadOnly;

  if ReadOnly then
  begin

    grEl.Align := alClient;

  end;


  spitClose.Enabled := (not ReadOnly) or (dm.User.Profile = 2) or ((dmInv.ITYPE = 12) and (dm.User.Profile = 1));

end;

procedure TfmSInv_det.FormShow(Sender: TObject);
begin
  inherited;
  with dmINV do
  begin
    if taSListISCLOSE.AsInteger=0   then
    begin
     spitClose.BtnCaption:='������';
     spitClose.ImageIndex:=6;
    end
    else
    begin
      spitClose.BtnCaption:='������';
      spitClose.ImageIndex:=5;
    end;
  end;
  ReOpenDataSets([dmInv.taComp, dmInv.taOutStore]);
  ContractChange := false;
  grEL.Refresh;
end;

procedure TfmSInv_det.FormCreate(Sender: TObject);
var ControlDate:TDateTime;
    ControlDate_Store:TDateTime;
//    Column: TColumnEh;
begin
  fmSInv_det := Self;
  FDocName:='��������� ���';
  FChangeFrom := False;
//  lcbxDepFrom.Enabled := False;
  FChangeSDate := False;
  inherited;

  if Not dmInv.taSListCREATED.IsNull then
  begin

    labelStateDate.Caption := FormatDateTime('DD/MM/YYYY', dmInv.taSListCREATED.AsDateTime);

  end;

  ControlDate_store:=dm.GetStoreBeginDate;
  ControlDate:=dm.GetDepBeginDate(dminv.taSListINVID.AsInteger);
  //ppInv.Skin := dm.ppSkin;
  if ControlDate<ControlDate_store then ControlDate:=ControlDate_store;
  FEnableScroll:=true;
  SearchEnable:=False;
  edArt.Text:='';
  SearchEnable:=True;
  StopFlag:=False;
  pcStore.ActivePageIndex:=0;
  imArt.Picture.Assign(LoadDbPicture(dm.taArtPICT, TJPEGImage));
  with dmINV do
  begin
    FCurrDep:=taSListDEPID.AsInteger;
    // ������� �������� �����
    dm.taInvColor.OnFilterRecord := FilterInvColor;
    dm.taInvColor.Filtered := True;
    OpenDataSet(dm.taInvColor);
    lcbxInvColor.DropDownBox.Columns[0].OnGetCellParams := InvColorGetCellParams;
    case IType of
      2 : begin // �������
        FCurrState:=0; // �� ������
        Self.Caption:='��������� �������';
        label6.Caption:='�����������';
        label2.Caption:='����������';
        CloseDataSet(dmInv.taDepname);
        dmInv.taDepname.SelectSQL.Clear;
        dmInv.taDepname.SelectSQL.Add('select DEPID,Name from D_DEP where band(DEPTYPES,4)=4 and SelfCompid=:SELFCOMPID order by SortInd');
        OpenDataSet(dminv.taDepname);
        lcbxDepFrom.ListSource:=dmINV.dsrComp;
        lcbxDepFrom.DataField:='SUPID';
        lcbxDepFrom.KeyField:='COMPID';
        lcbxDepFrom.ListField:='Name';
        txtDep.DataField:='DEPNAME';

        lbUpprice.Visible:=true;
        edUpPrice.Visible:=true;
        if (dminv.taSListDOCDATE.AsDateTime < ControlDate_Store) then
        begin
          acAdd.Enabled:=false;
          acDEl.Enabled:=false;
          acUpdatePrice.Enabled:=false;
          dminv.taSListDOCDATE.ReadOnly:=true;
          dminv.taSListFROMDEPID.ReadOnly:=true;
        end else
        begin
          acAdd.Enabled:=true;
          acDEl.Enabled:=true;
          acUpdatePrice.Enabled:=true;
          dminv.taSListDOCDATE.ReadOnly:=false;
          dminv.taSListFROMDEPID.ReadOnly:=false;
        end;
       end;
   3: begin // ������� �� �����������
        FCurrState := 1; // �������
        Self.Caption := '������� �� �����������';
        label2.Caption := '�����������';
        label6.Caption := '����������';
        CloseDataSet(dmInv.taDepname);
        dmInv.taDepname.SelectSQL.Clear;
        dmInv.taDepname.SelectSQL.Add('select DEPID,Name from D_DEP where band(DEPTYPES,4)=4 and SelfCompid=:SELFCOMPID order by SortInd');
        OpenDataSet(dminv.taDepname);

        lcbxDepFrom.DataField:='SUPID';
        lcbxDepFrom.KeyField:='COMPID';
        lcbxDepFrom.ListField:='Name';
        lcbxDepFrom.ListSource:=dmINV.dsrCOMP;

        txtDep.DataField:='DEPNAME';

        spitLabels.Enabled := False;
        spitPrice.Enabled := False;
        spitETolling.Enabled := False;

        if (dminv.taSListDOCDATE.AsDateTime < ControlDate_Store) then
        begin
          acAdd.Enabled := false;
          acDEl.Enabled := false;
          acUpdatePrice.Enabled := false;
          dminv.taSListDOCDATE.ReadOnly := true;
          dminv.taSListFROMDEPID.ReadOnly := true;
        end else
        begin
          acAdd.Enabled:=true;
          acDEl.Enabled:=true;
          acUpdatePrice.Enabled:=true;
          dminv.taSListDOCDATE.ReadOnly:=false;
          dminv.taSListFROMDEPID.ReadOnly:=false;
        end;
      end;
   12: begin //������� �� ������������
        FCurrState := 0; // �� ������
        Self.Caption := '������� �� ������������';
        label6.Caption := '�����������';
        label2.Caption := '����������';
        dmInv.taDepname.SelectSQL.Clear;
        dmInv.taDepname.SelectSQL.Add('select DEPID,Name from D_DEP where band(DEPTYPES,2) = 2 and SelfCompid = '+IntToStr(dm.User.SelfCompId)+' order by SortInd');
        ReOpenDataSet(dminv.taDepName);

        lcbxDepFrom.DataField:='FROMDEPID';
        lcbxDepFrom.KeyField:='DEPID';
        lcbxDepFrom.ListField:='NAME';
        lcbxDepFrom.ListSource:=dmINV.dsrDEPNAME;
        txtDep.DataField:='DEPNAME';

        spitAnalize.Enabled := False;

        spitLabels.Enabled := False;
        spitPrice.Enabled := False;
        spitETolling.Enabled := False;

        grTransport.Visible := False;
        if (dminv.taSListDOCDATE.AsDateTime<ControlDate) then
        begin
          acAdd.Enabled:=false;
          acDEl.Enabled:=false;
          dminv.taSListDOCDATE.ReadOnly:=true;
          dminv.taSListFROMDEPID.ReadOnly:=true;
        end else
        begin
          acAdd.Enabled:=true;
          acDEl.Enabled:=true;
          dminv.taSListDOCDATE.ReadOnly:=false;
          dminv.taSListFROMDEPID.ReadOnly:=false;
        end;
       end;
     else raise Exception.Create(Format(rsInternalError, ['220']));
   end
 end;

  (* ��������� ������� ����� *)
  grEL.FieldColumns['PRICE2'].Visible:= dmInv.IType in [2,3];
  grEL.FieldColumns['NDSNAME'].Visible:= dmInv.IType in [2,3];
  grEL.FieldColumns['AKCIZ'].Visible:= dmInv.IType in [2,3];
  grEL.FieldColumns['FULLSUM'].Visible:= dmInv.IType in [2,3];
  grEL.FieldColumns['SSUM'].Visible := dmInv.IType in [2,3];
  grEL.FieldColumns['UA'].Visible:= dmInv.IType in [12];
  grEL.FieldColumns['OLDUA'].Visible:= dmInv.IType in [12];
  grEl.FieldColumns['AKCIZ'].Visible := dmInv.IType in [2,3];
  grEl.FieldColumns['SERVCOST'].Visible := dmInv.IType in [2,3];
  grEl.FieldColumns['UPPRICE'].Visible := dmInv.IType in [2];
  grEl.FieldColumns['CHECK_NO'].Visible := dmInv.IType in [2];

  (* ��������� ��������� *)
  cbPrice.Visible := dmInv.IType in [2];
  lbDog.Visible := dmInv.IType in [2];
  mmDescr.Visible := dmInv.IType in [2];
  cbIsBrack.Visible := dmInv.IType in [12];
  lbInvKind.Visible := dmInv.IType in [2];
  cbCom.Visible := dmInv.IType in [2]; // ��� ���������
  lbPaytype.Visible := dmInv.IType in [2, 3];
  lcbxPaytype.Visible := dmInv.IType in [2, 3];
  lbNDS.Visible := dmInv.IType in [2, 3];
  lcbxNDS.Visible := dmInv.IType in [2, 3];

  (* ���������������� ������� *)
  lbMat.Items.Assign(dm.dMatGOOD);
  lbIns.Items.Assign(dm.dIns);
  lbSemis.Items.Assign(dm.dGood);
  lbPrill.Items.Assign(dm.dPrill);
  dm.InitArtParams;
  lbPrill.ItemIndex := 0;
  lbMat.ItemIndex  := 0;
  lbSemis.ItemIndex := 0;
  lbIns.ItemIndex  := 0;

  OpenDataSets([dm.taIns, dm.taDep, dm.taNDS, dm.taPayType, dm.taContract,
   dmInv.taComp, dmInv.taDepName, dmInv.taOutStore]);

  dmInv.FCurrINVID:= dmInv.taSListINVID.AsInteger;
  ReOpenDataSets([dmInv.taINVItems]);
  ReOpenDataSets([dm.taArt]);
  ReOpenDataSets([dmInv.taA2]);

 if dmInv.ITYPE in [2,3] then
 begin
   TabSheetPenalty.TabVisible := True;
   if dminv.ITYPE = 2 then
   begin
     dmMain.PenaltyMode := 2;
     PenaltyFilter.Items.Delete(0);
     PenaltyFilter.ItemIndex := 1;
     CloseDataSet(dmMain.taPenalty);
     OpenDataSet(dmMain.taPenalty);
     if not dmMain.taPenalty.IsEmpty then
     pcSTORE.ActivePageIndex := TabSheetPenalty.PageIndex;
   end
   else
   if dminv.ITYPE = 3 then
   begin
     dmMain.PenaltyMode := 0;
     PenaltyFilter.ItemIndex := 0;
     CloseDataSet(dmMain.taPenalty);
     OpenDataSet(dmMain.taPenalty);
   end;
 end
 else
 begin
   TabSheetPenalty.TabVisible := False;
 end;

  with dm.quTmp do
  begin
    SQL.Clear;
    SQL.Text := 'select Address from D_Comp where Compid = :SupID';
    ParamByName('SupID').AsInteger := dmInv.taSListSUPID.AsInteger;
    ExecQuery;
    mmDescr.Text := Fields[0].AsString;

    Close;
    SQL.Clear;
  end;

 if GridUID.SortMarkedColumns.Count <> 0 then
 GridUID.SortMarkedColumns[0].Title.SortMarker := smNoneEh;
 GridUID.Columns[1].Title.SortMarker := smDownEh;

 if dmINV.taSListISCLOSE.ASinteger = 1 then grEL.Options := grEL.Options - [dgEditing]
 else grEL.Options := grEL.Options + [dgEditing];

 OldContractID := trim(VarToStr(lcbxContract.Value));

 Access;

end;

procedure TfmSInv_det.lbFilterClick(Sender: TObject);
begin
  with dm do
  begin
    AMatId := TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
    ASemisId := TNodeData(lbSemis.Items.Objects[lbSemis.ItemIndex]).Code;
    AInsId  := TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
    APrillId  := TNodeData(lbPrill.Items.Objects[lbPrill.ItemIndex]).Code;
  end;
end;

procedure TfmSInv_det.ListBoxKeyPress(Sender: TObject;var  Key: char);
var
  c, i, j: integer;
  k: byte;
begin
  if Key in ['*', '0'..'9', 'A'..'Z', 'a'..'z', '�'..'�', '�'..'�'] then
    with TListBox(Sender) do
    begin
      k:=byte(Key) and not byte(32);
      c:=Items.Count;
      i:=ItemIndex;
      j:=i;
      if (byte(Items[i][1]) and not byte(32) = k) then Inc(i);
      while (i<c) and (byte(Items[i][1]) and not byte(32) <>k) do Inc(i);
      if i<c then ItemIndex:=i
      else begin
        i:=0;
        while (i<ItemIndex) and (byte(Items[i][1]) and not byte(32)<>k) do Inc(i);
        if i<ItemIndex then ItemIndex:=i;
      end;
      if j<>ItemIndex then TListBox(Sender).OnClick(NIL);
    end;
end;

procedure TfmSInv_det.sbAllClick(Sender: TObject);
begin
  lbSemis.ItemIndex:=0;
  lbMat.ItemIndex:=0;
  lbPrill.ItemIndex:=0;
  lbIns.ItemIndex:=0;
  with dmInv, dm do
    begin
      ASemisID:=TNodeData(lbSemis.Items.Objects[lbSemis.ItemIndex]).Code;
      AMatId:=TNodeData(lbMat.Items.Objects[lbMat.ItemIndex]).Code;
      APrillId:=TNodeData(lbPrill.Items.Objects[lbPrill.ItemIndex]).Code;
      AInsId:=TNodeData(lbIns.Items.Objects[lbIns.ItemIndex]).Code;
      Old_ASemisId:=ASemisId;
      Old_AMatId:=AMatId;
      Old_APrillId:=APrillId;
      Old_AInsId:=AInsId;
    end;
  ReopenDataSet(dm.taArt);
end;

procedure TfmSInv_det.sbFilterClick(Sender: TObject);
begin
  ReopenDataSet(dm.taArt);
end;

procedure TfmSInv_det.edArtEnter(Sender: TObject);
begin
  with dm do
    if (Old_ASemisId<>ASemisId) or
       (Old_AMatId<>AMatId) or
       (Old_APrillId<>APrillId) or
       (Old_AInsId<>AInsId) then
       begin
         Old_ASemisId:=ASemisId;
         Old_AMatId:=AMatId;
         Old_APrillId:=APrillId;
         Old_AInsId:=AInsId;
         taArt.DisableScrollEvents;
         taArt.DisableControls;
         try
           ReOpenDataSet(taArt);
         finally
           taArt.EnableScrollEvents;
           taArt.EnableControls;
         end;
       end;
  imArt.Picture.Assign(LoadDbPicture(dm.taArtPICT, TJPEGImage));
end;

procedure TfmSInv_det.edArtChange(Sender: TObject);
begin
  if(edArt.Text='')then dm.taArt.Filtered := False
  else begin
    dm.taArt.DisableScrollEvents;
    dm.taArt.DisableControls;
    try
      dm.taArt.Filtered := False;
      dm.taArt.Filter := 'ART='#39+edArt.Text+'*'+#39;
      dm.taArt.Filtered := True;
      ReOpenDataSets([dmInv.taA2, dmInv.taOutStore]);
    finally
      dm.taArt.EnableScrollEvents;
      dm.taArt.EnableControls;
    end;
  end;

end;

procedure TfmSInv_det.edArtButtonClick(Sender: TObject);
begin
  StopFlag:=True;
end;

procedure TfmSInv_det.edArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
     VK_RETURN : acAdd2.Execute
  end;
end;

procedure TfmSInv_det.N27Click(Sender: TObject);
begin
  inherited;
  ShowAndFreeForm(TfmMat, Self, TForm(fmMat), True, False);
  lbMat.Items.Assign(dm.dMatGOOD);
//  dm.FillListBoxes(nil, lbMat, nil, nil);
end;

procedure TfmSInv_det.N26Click(Sender: TObject);
const
  Rec : TRecName = (sId: 'SEMISID'; sName: 'SEMIS'; sTable: 'D_Semis'; sWhere: '');
begin
  ShowDictForm(TfmSemis, dm.dsrSemis, Rec, '���������� ��������������');
end;

procedure TfmSInv_det.dgArtGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
 if(Field.FieldName = 'Art') or (Field.FieldName = 'UNITID') then Background := clMoneyGreen;
end;

procedure TfmSInv_det.dgArtCellClick(Column: TColumn);
begin
  inherited;
  ReOpenDataSets([dmInv.taA2]);
end;

procedure TfmSInv_det.dgArtKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
    ReOpenDataSets([dmInv.taA2,dmInv.taOutStore]);
end;

procedure TfmSInv_det.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
   if (Shift=[ssCtrl])and(Key=VK_INSERT) then ExecuteAction(acAdd);

  if (Shift=[ssCtrl])and(Key=Ord('F')) then
  case pcSTORE.ActivePageIndex of
  0:
  begin
   EditorSearchArticleClass.SetFocus;
  end;
  1:
  begin
   EditorSearchArticleUID.SetFocus;
  end;
  end;
end;

procedure TfmSInv_det.btINSClick(Sender: TObject);
begin
  dminv.CurrArt2Id:=dminv.taA2ART2ID.AsInteger;
  if fmArtIns=nil then
  begin
    fmArtIns:=TfmArtIns.Create(self);
    fmArtIns.Show;
  end
end;

procedure TfmSInv_det.lcbxContractEnter(Sender: TObject);
begin
  inherited;
  PerviousContractID := lcbxContract.KeyValue;
end;

procedure TfmSInv_det.lcbxContractExit(Sender: TObject);
const Msg: string = '�� �������, ��� ������ ������� ��� �������� �� "������� ����������� (�������)"?';
var AResult: Variant;
begin
  inherited;
  if (ContractChange) and (StrToInt(VarToStr(lcbxContract.Value)) = 5 ) and (dmInv.taSListITYPE.AsInteger = 2) then
  begin
    if MessageDialog(Msg, mtConfirmation, [mbOk, mbCancel], 0) <> mrOk then Exit
    else
      with dmInv, sqlPublic do
        begin
          AResult := ExecSelectSQL('select id from eserv_params where invid = ' + taSListINVID.AsString, dm.QuTmp);
          if VarIsNull(AResult) then
            begin
              ErrorOkMessage('���������� ��������� ��������� ��������!');
              lcbxContract.KeyValue := PerviousContractID;
              Exit;
            end;
          Screen.Cursor := crHourGlass;
          try
            SQL.Clear;
            SQL.Add('execute procedure InvSellToInvTolling(:INVID)');
            ParamByName('INVID').AsInteger := taSListINVID.AsInteger;
            Prepare;
            ExecQuery;
          except
            on E: Exception do
              begin
                Transaction.RollbackRetaining;
                ShowMessage('������ ' + E.ClassName + ' ��� ���������� ��������� InvSellToInvTolling: ' + E.Message);
                Screen.Cursor := crDefault;
              end;
          end;
          Transaction.CommitRetaining;
          Screen.Cursor := crDefault;
        end;
  end;
end;

procedure TfmSInv_det.lcbxContractKeyValueChanged(Sender: TObject);
begin
  inherited;
  ContractChange := true;
end;

procedure TfmSInv_det.lcbxDepFromClick(Sender: TObject);
begin
  ReOpenDataSet(dmInv.taOutStore);
end;

procedure TfmSInv_det.acPriceDictExecute(Sender: TObject);
begin
  try
    fmPriceDict:=TfmPriceDict.Create(Application);
    fmPriceDict.ShowModal;
  finally
    fmPriceDict.Free;
  end;
end;

procedure TfmSInv_det.acPriceDictUpdate(Sender: TObject);
begin
  inherited;
  acPriceDict.Enabled := not FreadOnly;
end;

procedure TfmSInv_det.acUpdatePriceExecute(Sender: TObject);
begin
  inherited;
  dmInv.sqlUpdateInvPrice.ParamByName('INVID').AsInteger:=dmInv.taSListINVID.AsInteger;
  dmInv.sqlUpdateInvPrice.ExecQuery;
  dmInv.sqlUpdateInvPrice.Transaction.CommitRetaining;
  ReOpenDataSets([dminv.taInvitems]);
end;

procedure TfmSInv_det.acAddExecute(Sender: TObject);
var
//  Pos: integer;
  recordExists: boolean;
begin
  if dmINV.taSList.State in [dsInsert,dsEdit] then dmINV.taSList.Post;
  RefreshDataSets([dmINV.taSList]);
  if dmINV.taSListISCLOSE.ASinteger = 1 then
  begin
    ShowMessage('������ �������� �������� ���������!');
    SysUtils.Abort;
  end;
// ���� ����� �� �������������� ������
  if pcSTORE.ActivePageIndex=0 then
  with dmInv do
  begin
    PostDataSets([taINVItems]);
    if taInvItems.Locate('ART2ID', taA2Art2Id.AsInteger, []) then acDet.Execute
    else begin
       FCurrARTID:=dm.taArtARTID.Asinteger;
       FCurrART2ID:=taA2ART2ID.Asinteger;

       FCurrSz:=dm.taSzID.Asinteger;
       FCurrFULLART:=dm.taArtFullArt.AsString;

       if cbPrice.Checked
       then begin
           ReOpenDataSet(taPriceList);
           if taPriceList.Locate('ARTID', dm.taArtARTID.AsInteger, [loCaseInsensitive]) then FCurrPrice:=taPriceListPRICE.AsFloat;
       end;

       FCurrPrice := taA2TPRICE.AsFloat;

       FCurrArt:=taA2Art2.AsString;
       if not taA2Art2Id.IsNull then taInvItems.Append;
       if taA2Art2Id.IsNull then raise Exception.CreateRes(ErrorResId + 6);

       dm.AArt2    := taA2Art2.AsString;
       dm.AArt2Id  := taA2Art2Id.AsInteger;
       PostDataSets([taInvItems]);
    // ��� ���������
    try
      case dmINV.ITYPE of
      2,3,12:
      begin
       dm.AArt2Id:=dmINV.taA2ART2ID.Asinteger;
       ShowAndFreeForm(TfmINV_STORE, Self, TForm(fmINV_Store), True, False);
      end;
      4: begin
         ShowAndFreeForm(TfmSItem, Self, TForm(fmSItem), True, False);
         end;
      end;
      if (Q = 0)and(dminv.taINVItems.Active) then taINVItems.Delete;
      taINVItems.Transaction.CommitRetaining;
    except
      showmessage('������');
    end;
  end;
  end
 else // �� ��������
 with dmInv do
  begin
    if ITYPE = 3  then
      if dm.User.SelfCompId = 0 then
      else
      if InvoiceArticlesSELFCOMPID.AsInteger <> dm.User.SelfCompId then
        begin
          ErrorOkMessage('������� ����������� "' + InvoiceArticlesSELFCOMPNAME.AsString + '".');
          Exit;
        end;

   ReOpenDataSets([taSItem]);

    if cbPrice.Checked
    then begin
      ReOpenDataSet(taPriceList);
      if taPriceList.Locate('ARTID',InvoiceArticlesARTID.Asinteger,[loCaseInsensitive]) then FCurrPrice:=taPriceListPRICE.AsFloat;
    end
    else
      FCurrPrice := taA2TPRICE.AsFloat;

    FCurrPrice := InvoiceArticlesPRICE2.AsFloat;

    FCurrArt:=InvoiceArticlesART.AsString;

    FCurrUID := InvoiceArticlesUID.AsInteger;
    FCurrW:=InvoiceArticlesW.AsFloat;
    FCurrARTID:=InvoiceArticlesARTID.Asinteger;
    FCurrART2ID:=InvoiceArticlesART2ID.Asinteger;
    FCurrSz:=InvoiceArticlesSZID.Asinteger;
    PostDataSets([taInvItems]);

    if InvoiceArticles.RecordCount > 0 then
    begin
      if IType = 3 then
      begin
        RecordExists := taInvItems.Locate('ART2ID;PRICE2', VarArrayOf([InvoiceArticlesART2ID.AsInteger, InvoiceArticlesPRICE2.AsFloat]),[loCaseInsensitive])
      end
      else
      begin
        RecordExists := taInvItems.Locate('ART2ID', InvoiceArticlesART2ID.AsInteger,[loCaseInsensitive]);
      end;

      if not RecordExists then
        begin
          taINVItems.Append;
          PostDataSet(taINVItems);
        end;

      taSItem.Append;
      PostDataSet(taSItem);
      InvoiceArticles.Delete;
    end;
    dmInv.taSItem.Transaction.CommitRetaining;
  end;

  RefreshDataSets([dmInv.taSItem, dmINV.taInvItems, dminv.taSlist]);

  ReCalcGridTotalRecord;
end;

procedure TfmSInv_det.acDelExecute(Sender: TObject);
var
  arr : array of integer;
  i, j : integer;
begin
  if dmINV.taSListISCLOSE.AsInteger = 1 then
  begin
    ShowMessage('������ �������� �������� ���������!');
    SysUtils.Abort;
  end;
  if MessageDialog('������� ������ ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
  Screen.Cursor := crHourGlass;
  pcSTORE.ActivePageIndex := 1;
  (* ������� ������� ������� ���� � ��������� ������ *)
  OpenDataSet(dmInv.taSItem);
  dmInv.taSItem.Last;
  SetLength(arr, dmInv.taSItem.RecordCount);
  dmInv.taSItem.First;
  i:=0;
  while not dmInv.taSItem.Eof do
  begin
    arr[i] := dmInv.taSItemUID.AsInteger;
    inc(i);
    dmInv.taSItem.Next;
  end;
  CloseDataSet(dmInv.taSItem);
  // ������� ������
  dmInv.taINVItems.Delete;
  InvoiceArticles.open;
  InvoiceArticles.Insert;
  // ������� ������� � �������� ����� �� ��������
  for i:=Low(arr) to High(arr) do
  begin
    DBInvoiceArticle.ParamByName('UID').AsInteger := arr[i];
    DBInvoiceArticle.Active := True;
    if not DBInvoiceArticle.IsEmpty  then
    begin
      for j := 1 to InvoiceArticles.Fields.Count - 1 do
      InvoiceArticles.Fields[j].Value := DBInvoiceArticle.Fields[j-1].Value;
    end;
    DBInvoiceArticle.Active := False;
  end;
  try
    try
      InvoiceArticles.Post;
      InvoiceArticles.Close;
      InvoiceArticlesView.Open;
      InvoiceArticlesView.FindKey([arr[i]]);
      ReCalcGridTotalRecord;
    except
      on E: Exception do
        begin
          Screen.Cursor := crDefault;
          ShowMessage(E.Message);
        end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;

end;

procedure TfmSInv_det.acDetExecute(Sender: TObject);
begin
  with dmInv do
  begin
    PostDataSet(taINVItems);
    dmInv.FCurrARTID := dmInv.taINVItemsD_ARTID.AsInteger;
    dmInv.FCurrFullArt := dmInv.taINVItemsFULLART.AsString;
    case dmInv.ITYPE of
      2, 3, 12 : ShowAndFreeForm(TfmInv_Store4ArtId, TForm(fmInv_Store4ArtId));
      else raise Exception.Create(rsNotImplementation);
    end;
    taINVItems.Transaction.CommitRetaining;
    RefreshDataSets([taInvItems, taSList]);
  end;
  ReCalcGridTotalRecord;
end;

procedure TfmSInv_det.lcbxPaytypeChange(Sender: TObject);
begin
  if abs(dm.taPaytypeEXTRA.AsFloat)>0.01 then
  edUpPrice.Value := dm.taPaytypeEXTRA.AsFloat;
end;

procedure TfmSInv_det.acOutAnailzExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmSelectInv, TForm(fmSelectInv));
  ReOpenDataSet(dminv.taOutStore);
end;

procedure TfmSInv_det.edTransPercentExit(Sender: TObject);
begin
  inherited;
  if dminv.taSList.State in [dsEdit,dsInsert] then dmInv.taSList.Post; 
  dmInv.taSList.Transaction.CommitRetaining;
  RefreshDataSet(dmInv.taSList);
end;

procedure TfmSInv_det.acAddAllBrillExecute(Sender: TObject);
begin
  PostDataSet(dmInv.taSList);
  ExecSQL('execute procedure Add_ALL_IN_INV('+dmInv.taSListINVID.AsString+',0, 0)', dm.quTmp);
  ReOpenDataSet(dmInv.taINVItems);
end;

procedure TfmSInv_det.acShowIDExecute(Sender: TObject);
begin
  dgA2.FieldColumns['ART2ID'].Visible := not dgA2.FieldColumns['ART2ID'].Visible;
  dgA2.FieldColumns['PRICE'].Visible := not dgA2.FieldColumns['PRICE'].Visible;
  gridArt.FieldColumns['ARTID'].Visible := not gridArt.FieldColumns['ARTID'].Visible;
end;

function TfmSInv_det.StopFindArt:boolean;
begin
   Result := StopFlag;
end;

procedure TfmSInv_det.acAdd2Execute(Sender: TObject);
//var
//  pos : integer;
begin
  PostDataSet(dmINV.taSList);
  if(dmINV.taSListISCLOSE.AsInteger=1) then
    raise Exception.Create(Format(rsInvClose, [dmINV.taSListDOCNO.AsString]));
  // ������ ��� ������ �� �������������� ������
  if pcSTORE.ActivePageIndex<>0 then acAdd.Execute;
  PostDataSet(dmInv.taInvItems);
  dmInv.FCurrARTID:=dm.taArtARTID.AsInteger;
  dmInv.FCurrFullArt:=dm.taArtFULLART.AsString;
  case dmInv.ITYPE of
    2, 3, 12 : ShowAndFreeForm(TfmInv_Store4ArtId, TForm(fmInv_Store4ArtId));
    else raise Exception.Create(rsNotImplementation);
  end;
  ReCalcGridTotalREcord;
end;

procedure TfmSInv_det.FilterInvColor(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := dm.taInvColorINVTYPEID.AsInteger = dmInv.IType;
end;

procedure TfmSInv_det.InvColorGetCellParams(Sender: TObject;
  EditMode: Boolean; Params: TColCellParamsEh);
begin
  Params.Background := dm.taInvColorCOLOR.AsInteger;
end;

procedure TfmSInv_det.acInsExecute(Sender: TObject);
begin
  dmInv.CurrArt2Id:=dminv.taINVItemsART2ID.AsInteger;
  if dmInv.taSListITYPE.Asinteger<>12 then
  begin
    if fmArtIns=nil then
    begin
      fmArtIns:=TfmArtIns.Create(Self);
      fmArtIns.Show;
    end
  end
  else
   try
     fmLostStones:=TfmLostStones.Create(self);
     fmLostStones.ShowModal;
   finally
     fmLostStones.Free;
   end;
end;



procedure TfmSInv_det.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := not FreadOnly;
end;



procedure TfmSInv_det.acReturnPriceExecute(Sender: TObject);
begin
  with dminv.sqlPublic do
  begin
    SQL.Clear;
    sql.Add('execute procedure SET_LAST_PRICE_FOR_INV(:invid)');
    ParamByName('INVID').AsInteger:=dmInv.taSListINVID.AsInteger;
    ExecQuery;
    Transaction.CommitRetaining;
  end;
  ReOpenDataSet(dminv.taINVItems);
end;

procedure TfmSInv_det.lcbxDepFromEnter(Sender: TObject);
begin
  FChangeFrom := False;
end;

procedure TfmSInv_det.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := not FReadOnly;
end;

procedure TfmSInv_det.edUpPriceExit(Sender: TObject);
begin
  inherited;
  if dminv.taSList.State in [dsEdit,dsInsert] then dminv.taSList.Post;
  ReOpenDataSet(dminv.taInvItems);
end;

procedure TfmSInv_det.acSetPrice2Execute(Sender: TObject);
begin
  inherited;
  dminv.sqlPublic.SQL.Clear;
  dminv.sqlPublic.SQL.Add('execute procedure SET_INV_PRICE2(:INVID)');
  dminv.sqlPublic.ParamByName('INVID').AsInteger:=dminv.taSListINVID.AsInteger;
  dminv.sqlPublic.Prepare;
  dminv.sqlPublic.ExecQuery;
  ReOpenDataSet(dminv.tainvitems);
end;


procedure TfmSInv_det.acInvCopyExecute(Sender: TObject);
begin
  inherited;
  if SaveDialog1.Execute then dmInv.SaveInv(SaveDialog1.FileName);
end;

procedure TfmSInv_det.LoadInv(const FileName: string);
var f:textfile;
    str:string;
    uid:integer;
    st:string;
    i:integer;
begin
 try
  with dminv do begin
    AssignFile(f,FileName);
    Reset(f);

    while not Eof(f) do
    begin
     readln(f,str);
     st:='';
     i:=1;
     while (str[i]<>' ')
     do begin
      st:=st+str[i];
      inc(i);
     end;
     uid:=StrTOInt(st);
     //����

     st:='';
     while (i<=Length(str))
     do begin
      st:=st+str[i];
      inc(i);
     end;
     FCurrPrice2:=StrTOFloat(st);
     OnInvoiceArticleScaned(UID);
     {
     if InvoiceArticles.FindKey([UID]) then acAdd.Execute
     else showmessage('������� '+ IntToStr(uid)+' ��� �� ������')
     }
    end;
   end;
 finally
  CloseFile(f);
 end;
end;


procedure TfmSInv_det.acLoadInvExecute(Sender: TObject);
begin
  inherited;
  pcStore.ActivePageIndex := 1;
  if OpenDialog1.Execute then LoadInv(OpenDialog1.FileName);
end;

procedure TfmSInv_det.acLoadInvUpdate(Sender: TObject);
begin
  inherited;
  acLoadInv.Enabled := not FreadOnly;
end;

procedure TfmSInv_det.deSDateExit(Sender: TObject);
begin
  if not FChangeSDate then eXit;
  ReOpenInvoiceArticles;
end;

procedure TfmSInv_det.deSDateEnter(Sender: TObject);
begin
  FChangeSDate := False;
end;

procedure TfmSInv_det.deSDateUpdateData(Sender: TObject; var Handled: Boolean);
begin
  FChangeSDate := True;
end;

procedure TfmSInv_det.acCheckExecute(Sender: TObject);
begin
 inherited;

 if dmInv.taSListISCLOSE.AsInteger=1
 then begin
  showmessage('����� ��������� ��������� ����� �������!');
  exit;
 end;

 with dminv, sqlPublic
 do begin
  SQl.Clear;
  Sql.Add('execute procedure UNCHECK_SITEM(:INVID)');
  ParamByName('INVID').AsInteger:=taSListINVID.AsInteger;
  Prepare;
  ExecQuery;
  Transaction.CommitRetaining;
 end;
 try
  fmNotInInv:=TfmNotInInv.Create(self);
  fmNotInInv.ShowModal;
 finally
  fmNotInInv.Free;
 end;
end;

procedure TfmSInv_det.acPrice2PriceExecute(Sender: TObject);
begin
  inherited;
  with dminv, sqlPublic
    do begin
    SQl.Clear;
    Sql.Add('execute procedure PRICE_TO_PRICE2(:INVID)');
    ParamByName('INVID').AsInteger:=taSListINVID.AsInteger;
    Prepare;
    ExecQuery;
    Transaction.CommitRetaining;
  end;
  ReOpenDataSet(dminv.taINVItems);
end;

procedure TfmSInv_det.edUpPriceEditButtons0Click(Sender: TObject; var Handled: Boolean);
var
  Bookmark : TBookmark; 
begin
  Handled := True;
  if (dmInv.taSListISCLOSE.AsInteger = 1) then raise EWarning.Create(rsInvClose);
  PostDataSet(dmInv.taSList);
  if (dmInv.taINVItemsPRICE.IsNull) then raise EWarning.Create(rsNotSelFieldUpPrice);  
  if MessageDialog(rsRefreshPriceWithUpPrice, mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  dmInv.taINVItems.DisableControls;
  dmInv.taINVItems.DisableScrollEvents;
  Bookmark := dmInv.taINVItems.GetBookmark;
  try
    dmInv.taINVItems.First;
    while not dmInv.taINVItems.Eof do
    begin
      dmInv.taINVItems.Edit;
      dmInv.taINVItemsPRICE2.AsFloat := dmInv.taINVItemsPRICE.AsFloat+dmInv.taINVItemsPRICE.AsFloat*(dmInv.taSListUPPRICE.AsFloat/100);
      dmInv.taINVItems.Post;
      dmInv.taINVItems.Next;
    end;
  finally
    dmInv.taINVItems.GotoBookmark(Bookmark);
    dmInv.taINVItems.EnableControls;
    dmInv.taINVItems.EnableScrollEvents;
  end;
end;

procedure TfmSInv_det.acOpenSellUIDExecute(Sender: TObject);
begin
  PostDataSet(dmInv.taSList);
  ReOpenInvoiceArticles;
  FChangeFrom := False;
end;

procedure TfmSInv_det.acOpenSellUIDUpdate(Sender: TObject);
begin
  acOpenSellUID.Visible := (dmInv.IType = 3);
end;

procedure TfmSInv_det.lcbxDepFromKeyValueChanged(Sender: TObject);
begin
  FChangeFrom := True;
end;

procedure TfmSInv_det.lcbxDepFromExit(Sender: TObject);
const
  Msg : string = '��������� ����������! ������� ��������� ����� ���������� �������?';
begin
  if not FChangeFrom then eXit;



  if dmInv.ITYPE = 2 then
  begin
    if dmMain.taPenalty.Active then
    begin
      dmMain.PenaltyMode := 2;
      PenaltyFilter.ItemIndex := 1;
      CloseDataSet(dmMain.taPenalty);
      OpenDataSet(dmMain.taPenalty);
      if not dmMain.taPenalty.IsEmpty then
      pcSTORE.ActivePageIndex := TabSheetPenalty.PageIndex;
    end;
  end;

  if dmInv.taSList.State = dsEdit then
    dmInv.taSList.Post;

  if dmInv.ITYPE in [2,3] then
  begin
    with dm.quTmp do
    begin
      SQL.Clear;
      SQL.Text := 'select Address from D_Comp where Compid = :SupID';
      ParamByName('SupID').AsInteger := dmInv.taCompCOMPID.AsInteger;
      ExecQuery;
      mmDescr.Text := Fields[0].AsString;
      Close;
      SQL.Clear;
    end;

    with dmInv.sqlPublic do
    begin
      SQL.Clear;
      SQL.Text := 'select max(ReuDate) ReuDate from CompOwner where ' +
                  'SelfCompID = :SelfCompID and CompID = :SupID';
      ParamByName('SelfCompID').AsInteger := dm.User.SelfCompId;
      ParamByName('SupID').AsInteger := dmInv.taCompCOMPID.AsInteger;
      ExecQuery;
      if Fields[0].IsNull then
        InformationMessage('���� ��� �� ������� ��� ����������� ������� �� ������ �����������!')
      else
        if (Fields[0].AsDate < Today) then
          InformationMessage('� ���������� ���������� ���������(�) ��� ('+ Fields[0].AsString + ')');
      Close;
    end;
  end;

  if dmInv.ITYPE <> 3 then eXit;
  if ((not acOpenSellUID.Visible) or (not acOpenSellUID.Enabled)) then eXit;
  if MessageDialog(Msg, mtConfirmation, [mbYes, mbNo], 0) = mrNo then
  begin
     CloseInvoiceArticles;
    eXit;
  end;
  Self.Refresh;
  acOpenSellUID.Execute;
  pcSTORE.ActivePageIndex := 1;


end;

procedure TfmSInv_det.acUIDInfoExecute(Sender: TObject);
begin
  ShowUIDInfo(InvoiceArticlesView['UID'] , '');
end;

procedure TfmSInv_det.acUIDInfoUpdate(Sender: TObject);
begin
  acUIDInfo.Enabled := InvoiceArticles.Active and (not InvoiceArticles.IsEmpty);
end;

procedure TfmSInv_det.acUIDHistoryExecute(Sender: TObject);
begin
  if InvoiceArticlesUID.IsNull then dmInv.FCurrUID := 0
  else dmInv.FCurrUID := InvoiceArticlesUID.AsInteger;
  CloseDataSet(dmInv.taItemHistory);
  ShowAndFreeForm(TfmItemHistory, Self, TForm(fmItemHistory), True, False);
  CloseDataSet(dmInv.taItemHistory);
end;

procedure TfmSInv_det.ReCalcGridTotalRecord;
var
  Bookmark : Pointer;
begin
  //pos:=grEl.DataSource.DataSet.RecNo;
  //if pos > 0 then  grEl.DataSource.DataSet.MoveBy(pos);
  Bookmark := grEl.DataSource.DataSet.GetBookmark;
  try
    grEl.SumList.RecalcAll;
  finally
    grEl.DataSource.DataSet.GotoBookmark(Bookmark);
  end;
end;

procedure TfmSInv_det.acEServParamsExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'MATID'; sTable: 'ESERV_PARAMS'; sWhere: '');
begin
  ShowDictForm(TfmEServParams, dmInv.dsrEServParams, Rec, TAction(Sender).Caption, dmDictionary, nil);
end;

procedure TfmSInv_det.acEServParamsUpdate(Sender: TObject);
begin
  inherited;
  acEServParams.Enabled := not FreadOnly;
end;

procedure TfmSInv_det.acEServMatExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'MATID'; sTable: 'ESERV_PARAMS'; sWhere: '');
begin
  ShowDictForm(TfmEServMat, dmInv.dsrEServMat, Rec, '���������', dmSelRecordReadOnly, nil);
end;

procedure TfmSInv_det.acEServMatUpdate(Sender: TObject);
begin
  inherited;
  acEServMat.Enabled := not FreadOnly;
end;

procedure TfmSInv_det.acAddWUpdate(Sender: TObject);
begin
  //������� ������� ���� ���������� ��� ���������
  acAddW.Enabled := not FreadOnly and ((dm.User.SelfCompId = 266) or (dm.User.SelfCompId = 67));
end;

procedure TfmSInv_det.acAddWExecute(Sender: TObject);
//var
//  w: double;
begin
  //������ ���
  if ShowEditor(TfmeInvWeight, TfmEditor(fmeInvWeight)) <> mrOk then exit;
  //ShowMessage('��� /' + FloatToStr(iw) + '/');
  //2 - ��������� ���������� �� ���
  PostDataSet(dmInv.taSList);
  ExecSQL('execute procedure Add_ALL_IN_INV('+dmInv.taSListINVID.AsString+',2,'
    +FloatToStr(iw)+ ')', dm.quTmp);
  ReOpenDataSet(dmInv.taINVItems);
end;

procedure TfmSInv_det.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  with Tfm_SInv_Det_SIList.Create(Self) do
  begin
    DataBase := dm.db;
    if ShowModal = mrOk then
    begin
      dmInv.taSList.Edit;
      dmInv.taSList.FieldByName('Prihod_ID').AsInteger :=
        DataSet_SIList.FieldByName('InvID').AsInteger;
      dmInv.taSList.Post;
      dmInv.taSList.Refresh;
    end;
    Free;
  end;
end;

procedure TfmSInv_det.DBEditEh1Change(Sender: TObject);
var
  i: Integer;
  s: string;
begin
  s := TDBEditEh(Sender).Text;
  i := Pos('00:00:00', s);
  Delete(s, i, 8);
  ed_Prihod.Text := s;
  inherited;
end;

procedure TfmSInv_det.DBInvoiceArticlesAfterOpen(DataSet: TDataSet);
begin
  inherited;
  //ShowMessage('ST: ' + DBInvoiceArticles.ParamByName('state').AsString + ' DD: ' + DBInvoiceArticles.ParamByName('document$date').AsString + ' SI: ' + DBInvoiceArticles.ParamByName('supplier$id').AsString);
end;

procedure TfmSInv_det.acSetPriceExecute(Sender: TObject);
begin
  DialogSetPrice := TDialogSetPrice.Create(Self);
  if DialogSetPrice.Execute then
  begin
    grEL.Refresh;
  end;
  DialogSetPrice.Free;
end;

procedure TfmSInv_det.acSetPriceUpdate(Sender: TObject);
begin
  inherited;
  acSetPrice.Enabled := not FreadOnly;
end;

procedure TfmSInv_det.PenaltyFilterChange(Sender: TObject);
var
  PenaltyMode: Integer;
begin
 if PenaltyFilter.Items.Count = 4 then PenaltyMode := TdxBarCombo(Sender).ItemIndex
 else PenaltyMode := TdxBarCombo(Sender).ItemIndex + 1;
 if dmMain.taPenalty.State = dsEdit then
 dmMain.taPenalty.Post;
 if PenaltyMode <> dmMain.PenaltyMode then
 begin
   CloseDataSet(dmMain.taPenalty);
   OpenDataSet(dmMain.taPenalty);
 end;  
end;

procedure TfmSInv_det.GridPenaltyExit(Sender: TObject);
begin
  if dmMain.taPenalty.State = dsEdit then
  dmMain.taPenalty.Post;
end;

procedure TfmSInv_det.pcSTOREChange(Sender: TObject);
begin
  if pcStore.ActivePageIndex = 1 then
  begin
    if not InvoiceArticles.Active then
    begin
      case dmInv.ITYPE of
        2, 12 : OpenInvoiceArticles;
        3 : if not dmInv.taSListSUPID.IsNull then OpenInvoiceArticles;
      end;
    end;
  end;
end;

procedure TfmSInv_det.InvoiceArticlesCalcFields(DataSet: TDataSet);
begin
  DataSet.FieldByName('RecNo').Value := DataSet.RecNo;
end;

procedure TfmSInv_det.InvoiceArticlesBeforeOpen(DataSet: TDataSet);
begin
  InvoiceArticles.Params.ParamByName('State').AsInteger := dmInv.FCurrState;
  InvoiceArticles.Params.ParamByName('Supplier$ID').AsInteger := dmInv.taSListSUPID.AsInteger;;
  InvoiceArticles.Params.ParamByName('Document$Date').AsDateTime := dmInv.taSListDOCDATE.AsDateTime;
end;

procedure TfmSInv_det.EditorSearchArticleUIDChange(Sender: TObject);
var
  ArticleUID: string;
  Column: TColumnEh;
begin
  InvoiceArticlesView.IndexFieldNames := 'SEARCH$UID';
  EditorSearchArticleClass.OnChange := nil;
  EditorSearchArticleClass.Text := '';
  EditorSearchArticleClass.OnChange := EditorSearchArticleClassChange;

  EditorSearchArticleSubClass.OnChange := nil;
  EditorSearchArticleSubClass.Text := '';
  EditorSearchArticleSubClass.OnChange := EditorSearchArticleSubClassChange;

  if gridUID.SortMarkedColumns.Count <> 0 then
  begin
    Column := gridUID.SortMarkedColumns[0];
    Column.Title.SortMarker := smNoneEh;
  end;

  if EditorSearchArticleUID.Text <> '' then
  begin
    ArticleUID := EditorSearchArticleUID.Text;
    InvoiceArticlesView.SetRange([ArticleUID], [ArticleUID + #255]);
  end
  else InvoiceArticlesView.CancelRange;
end;

procedure TfmSInv_det.EditorSearchArticleClassChange(Sender: TObject);
var
  ArticleClass: string;
  FieldNames: string;
begin
  EditorSearchArticleUID.OnChange := nil;
  EditorSearchArticleUID.Text := '';
  EditorSearchArticleUID.OnChange := EditorSearchArticleUIDChange;
  EditorSearchArticleSubClass.OnChange := nil;
  EditorSearchArticleSubClass.Text := '';
  EditorSearchArticleSubClass.OnChange := EditorSearchArticleSubClassChange;

  if EditorSearchArticleClass.Text <> '' then
  begin
    ArticleClass := EditorSearchArticleClass.Text;
    if GridUID.SortMarkedColumns.Count = 0 then FieldNames := 'ART'
    else FieldNames := 'ART;' + GridUID.SortMarkedColumns[0].FieldName;
    InvoiceArticlesView.IndexFieldNames := FieldNames;
    InvoiceArticlesView.SetRange([ArticleClass], [ArticleClass + #255]);
  end
  else
  begin
    InvoiceArticlesView.CancelRange;
    if GridUID.SortMarkedColumns.Count <> 0 then     InvoiceArticlesView.IndexFieldNames := GridUID.SortMarkedColumns[0].FieldName
    else InvoiceArticlesView.IndexFieldNames := 'SEARCH$UID';
    InvoiceArticlesView.First;
  end;
end;

procedure TfmSInv_det.EditorSearchArticleUIDKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Ord(Key) in [Ord('0')..Ord('9'), VK_BACK]) then Key := #0;
end;

procedure TfmSInv_det.EditorSearchArticleClassKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = ' ' then Key := #0;
end;

procedure TfmSInv_det.gridUIDDblClick(Sender: TObject);
var
  ArticleUID: Integer;
begin
  if InvoiceArticlesView.Active and (InvoiceArticlesView.RecordCount <> 0) then
  begin
    ArticleUID := InvoiceArticlesView.FieldByName('UID').AsInteger;
    if ArticleUID <> 0 then
    OnInvoiceArticleScaned(ArticleUID);
  end;
end;

procedure TfmSInv_det.CloseInvoiceArticles;
begin
  if InvoiceArticlesView.Active then
  InvoiceArticlesView.Active := False;
  if InvoiceArticles.Active then
  InvoiceArticles.Active := False;
end;

procedure TfmSInv_det.OpenInvoiceArticles;
var
  NotifyDialog: TNotifyDialog;
begin
 try
   NotifyDialog := CreateNotifyDialog('�������� ������ �������.');
  // NotifyDialog.Show;
   Screen.cursor := crHourGlass;
   NotifyDialog.Perform(WM_PAINT, 0, 0);
   CloseInvoiceArticles;
   OpenDataSet(InvoiceArticles);
   InvoiceArticlesView.CloneCursor(InvoiceArticles, True, False);
   InvoiceArticlesView.IndexFieldNames := 'SEARCH$UID';
   OpenDataSet(InvoiceArticlesView);
   InvoiceArticlesView.First;
   Screen.Cursor := crDefault;
 except
   on E: Exception do
   begin
    //NotifyDialog.Free;
    showmessage(e.message);
    Screen.Cursor := crDefault;
     //raise E;
   end;
 end;
end;

procedure TfmSInv_det.ReOpenInvoiceArticles;
begin
  CloseInvoiceArticles;
  OpenInvoiceArticles;
end;

procedure TfmSInv_det.OnInvoiceArticleScaned(UID: Integer);
begin
  pcSTORE.ActivePageIndex := 1;
  if not InvoiceArticles.Active then OpenInvoiceArticles;
  stbrStatus.Panels[0].Text := '������ �����: '+IntToStr(UID);
  if InvoiceArticles.FindKey([UID]) then acAdd.Execute
  else ShowUIDInfo(UID, '������� ��� �� ������');
end;

procedure TfmSInv_det.DeleteInvoiceArticle(ArticleID: Integer);
begin
  pcSTORE.ActivePageIndex := 1;
  pcSTOREChange(nil);
  if InvoiceArticles.FindKey([ArticleID]) then
  InvoiceArticles.Delete;
end;

procedure TfmSInv_det.InsertInvoiceArticle(ArticleID: Integer);
var
  i: Integer;
begin
  pcSTORE.ActivePageIndex := 1;
  pcSTOREChange(nil);

  if not InvoiceArticles.FindKey([ArticleID]) then
  begin
    DBInvoiceArticle.ParamByName('UID').AsInteger := ArticleID;
    DBInvoiceArticle.Active := True;
    if not DBInvoiceArticle.IsEmpty  then
    begin
      InvoiceArticles.Insert;
      for i := 1 to InvoiceArticles.Fields.Count - 1 do
      InvoiceArticles.Fields[i].Value := DBInvoiceArticle.Fields[i-1].Value;
      InvoiceArticles.Post;
    end;
    DBInvoiceArticle.Active := False;
    InvoiceArticlesView.FindKey([ArticleID]);
  end
  else InvoiceArticlesView.FindKey([ArticleID]);
end;

procedure TfmSInv_det.EditorSearchArticleSubClassKeyPress(Sender: TObject; var Key: Char);
begin
  if not (Ord(Key) in [Ord('0')..Ord('9'), VK_BACK]) then Key := #0;
end;

procedure TfmSInv_det.EditorSearchArticleSubClassChange(Sender: TObject);
var
  ArticleSubClass: string;
  Column: TColumnEh;

begin
  EditorSearchArticleUID.OnChange := nil;
  EditorSearchArticleUID.Text := '';
  EditorSearchArticleUID.OnChange := EditorSearchArticleUIDChange;

  EditorSearchArticleClass.OnChange := nil;
  EditorSearchArticleClass.Text := '';
  EditorSearchArticleClass.OnChange := EditorSearchArticleClassChange;

  if gridUID.SortMarkedColumns.Count <> 0 then
  begin
    Column := gridUID.SortMarkedColumns[0];
    Column.Title.SortMarker := smNoneEh;
  end;

  if EditorSearchArticleSubClass.Text <> '' then
  begin
    ArticleSubClass := EditorSearchArticleSubClass.Text;
    InvoiceArticlesView.IndexFieldNames := 'ART2';
    InvoiceArticlesView.SetRange([ArticleSubClass], [ArticleSubClass + #255]);
  end
  else
  begin
    InvoiceArticlesView.CancelRange;
    InvoiceArticlesView.IndexFieldNames := 'SEARCH$UID';
    InvoiceArticlesView.First;
  end;
end;

procedure TfmSInv_det.gridUIDTitleBtnClick(Sender: TObject; ACol: Integer; Column: TColumnEh);
var
  ArticleClass: string;
  FieldNames: string;
//  FieldName: string;
begin
  if Column.Title.SortMarker = smNoneEh then
  begin
    EditorSearchArticleUID.OnChange := nil;
    EditorSearchArticleUID.Text := '';
    EditorSearchArticleUID.OnChange := EditorSearchArticleUIDChange;
    EditorSearchArticleSubClass.OnChange := nil;
    EditorSearchArticleSubClass.Text := '';
    EditorSearchArticleSubClass.OnChange := EditorSearchArticleSubClassChange;

    if GridUID.SortMarkedColumns.Count <> 0 then
    GridUID.SortMarkedColumns[0].Title.SortMarker := smNoneEh;

    ArticleClass := EditorSearchArticleClass.Text;
    if ArticleClass = '' then
    begin
      FieldNames := Column.FieldName;
      InvoiceArticlesView.IndexFieldNames := FieldNames;
      InvoiceArticlesView.First;
      Column.Title.SortMarker := smDownEh;
    end
    else
    begin
      if Column.FieldName <> 'ART' then
      begin
        InvoiceArticlesView.CancelRange;
        FieldNames := 'ART;' + Column.FieldName;
        InvoiceArticlesView.IndexFieldNames := FieldNames;
        InvoiceArticlesView.SetRange([ArticleClass], [ArticleClass + #255]);
        InvoiceArticlesView.First;
        Column.Title.SortMarker := smDownEh;
      end;
    end;
  end;
end;

procedure TfmSInv_det.N7Click(Sender: TObject);
begin
  PostDataSet(dmInv.taSList);
  ExecSQL('execute procedure Add_ALL_IN_INV('+dmInv.taSListINVID.AsString+',11, 0)', dm.quTmp);
  ReOpenDataSet(dmInv.taINVItems);
end;

procedure TfmSInv_det.N8Click(Sender: TObject);
begin
  PostDataSet(dmInv.taSList);
  ExecSQL('execute procedure Add_ALL_IN_INV('+dmInv.taSListINVID.AsString+',12, 0)', dm.quTmp);
  ReOpenDataSet(dmInv.taINVItems);
end;

procedure TfmSInv_det.ActionLabelsExecute(Sender: TObject);
begin
  ReportLabels.ShowReport;
end;

procedure TfmSInv_det.DataSetLabelsBeforeOpen(DataSet: TDataSet);
begin
  DataSetLabels.ParamByName('invid').AsInteger := dmInv.FCurrINVID;
end;

initialization
  TPicture.RegisterClipboardFormat(JpegClipboardFormat, TJpegImage);

end.

