unit eQW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, DBCtrls, Mask, DBCtrlsEh, RxToolEdit,
  RXSpin, rxCurrEdit;

type
  TfmeQW = class(TForm)
    Bevel1: TBevel;
    BitBtn1: TBitBtn;
    Label1: TLabel;
    DBText1: TDBText;
    Label2: TLabel;
    Label3: TLabel;
    edQ: TRxSpinEdit;
    edW: TRxCalcEdit;
    BitBtn2: TBitBtn;
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmeQW: TfmeQW;

implementation

uses InvData;

{$R *.dfm}

procedure TfmeQW.BitBtn1Click(Sender: TObject);
begin
 dmInv.FCurrSIEL_W:=edW.Value;
 dmInv.FCurrSIEL_Q:=edQ.AsInteger;
end;

end.
