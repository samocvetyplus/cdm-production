unit Protocol;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus, ImgList, Grids, DBGrids, RXDBCtrl,
  RxToolEdit, StdCtrls, Mask, DBCtrls, ExtCtrls, db,
  ComCtrls, DBGridEh, DBCtrlsEh, TB2Item, ActnList,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmProtocol = class(TfmDocAncestor)
    lbTo: TLabel;
    DBDateEdit1: TDBDateEdit;
    procedure grGetCellParams(Sender: TObject; Field: TField; AFont: TFont;
      var Background: TColor; Highlight: Boolean);
    procedure N1Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  end;

var
  fmProtocol: TfmProtocol;

implementation

uses MainData, PrintData, dbUtil, DictData, DateUtils;

{$R *.dfm}



procedure TfmProtocol.grGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if dmMain.taProtocolID.IsNull then Background := clMoneyGreen;
  if Assigned(Field)and(Field.FieldName='RecNo') then Background:= clMoneyGreen;
  if(Field.FieldName='S')and(Field.AsFloat > 0)then Background := clRed;
  if(Field.FieldName='OUT1')then Background := clMoneyGreen;
  if(Field.FieldName='OUT_PS')then Background := clMoneyGreen;
end;

procedure TfmProtocol.N1Click(Sender: TObject);
begin
  PostDataSets([dmMain.taProtocol, dmMain.taPList]);
  dmPrint.PrintDocumentA(dkProtocol, VarArrayOf([VarArrayOf([dmMain.taPListID.AsInteger]),
                VarArrayOf([dmMain.taPListID.AsInteger, 1])]));
end;

procedure TfmProtocol.N3Click(Sender: TObject);
begin
  PostDataSets([dmMain.taProtocol, dmMain.taPList]);
  dmPrint.PrintDocumentA(dkOb, VarArrayOf([VarArrayOf([dmMain.taPListID.AsInteger])]));
end;

procedure TfmProtocol.N2Click(Sender: TObject);
begin
  PostDataSets([dmMain.taProtocol, dmMain.taPList]);
  with dmMain do
    dmPrint.PrintDocumentA(dkActE, VarArrayOf([VarArrayOf([
       taPListBD.AsDateTime, taPListDOCDATE.AsDateTime])]));
end;

procedure TfmProtocol.FormCreate(Sender: TObject);
begin
  FDocName := 'Протокол';
  inherited;
  InitBtn(Boolean(dmMain.taPListISCLOSE.AsInteger));
end;

procedure TfmProtocol.gridItemGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if dmMain.taProtocolID.IsNull then Background := clMoneyGreen else Background := clInfoBk;
  if Assigned(Column.Field)and(Column.Field.FieldName='RecNo') then Background:= clMoneyGreen;
  if(Column.Field.FieldName='S')and(Column.Field.AsFloat > 0)then Background := clRed;
  if(Column.Field.FieldName='OUT1')then Background := clMoneyGreen;
  if(Column.Field.FieldName='OUT_PS')then Background := clMoneyGreen;
end;

end.
