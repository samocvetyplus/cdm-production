unit dSz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ImgList, Grids, DBGrids, RXDBCtrl,
  ExtCtrls, ComCtrls, ActnList, DBGridEh, Buttons,
  DBGridEhGrouping, rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmSz = class(TfmDictAncestor)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmSz: TfmSz;

implementation

uses DictData;

{$R *.dfm}

procedure TfmSz.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dm.LoadArtSL(SZ_DICT);
end;

end.
