inherited fmAWMatOff_UID: TfmAWMatOff_UID
  Left = 264
  Top = 184
  Height = 359
  Caption = #1048#1079#1076#1077#1083#1080#1103' '#1074#1086#1079#1074#1088#1072#1097#1077#1085#1085#1099#1077' '#1086#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 306
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 42
    Width = 386
    Height = 264
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsrAWMatOff_UID
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        Title.EndEllipsis = True
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'PRICE0'
        Footers = <>
        Title.Caption = #1059#1095#1077#1090#1085#1072#1103' '#1094#1077#1085#1072' '#1085#1072' '#1084#1086#1084#1077#1085#1090' '#1087#1088#1086#1076#1072#1078#1080
      end
      item
        EditButtons = <>
        FieldName = 'COST0'
        Footer.FieldName = 'COST0'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1059#1095#1077#1090#1085#1072#1103' '#1089#1090#1086#1080#1084#1086#1089#1090#1100' '#1085#1072' '#1084#1086#1084#1077#1085#1090' '#1087#1088#1086#1076#1072#1078#1080
        Title.EndEllipsis = True
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'N'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1040#1082#1090' '#1074#1099#1087#1086#1083#1085#1077#1085#1085#1099#1093' '#1088#1072#1073#1086#1090'|'#1053#1086#1084#1077#1088' '
        Title.EndEllipsis = True
        Title.TitleButton = True
      end
      item
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1040#1082#1090' '#1074#1099#1087#1086#1083#1085#1077#1085#1085#1099#1093' '#1088#1072#1073#1086#1090'|'#1044#1072#1090#1072
        Title.EndEllipsis = True
        Title.TitleButton = True
        Width = 71
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object taAWMatOff_UID: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.UID, it.INVID, it.COST0, i.DOCDATE, i.DOCNO,'
      '       inf.W, it.PRICE0, it.N'
      'from AWMatOff_UID it, Inv i, SInfo inf'
      'where it.T=1 and'
      '      it.AWMATOFFID=:ID and'
      '      it.INVID=i.INVID and'
      '      it.UID=inf.UID ')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 176
    Top = 92
    poSQLINT64ToBCD = True
    object taAWMatOff_UIDID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taAWMatOff_UIDUID: TFIBIntegerField
      DisplayLabel = #8470' '#1080#1079#1076#1077#1083#1080#1103
      FieldName = 'UID'
    end
    object taAWMatOff_UIDINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taAWMatOff_UIDCOST0: TFIBFloatField
      FieldName = 'COST0'
    end
    object taAWMatOff_UIDDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object taAWMatOff_UIDDOCNO: TFIBIntegerField
      FieldName = 'DOCNO'
    end
    object taAWMatOff_UIDW: TFIBFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      DisplayFormat = '0.00'
    end
    object taAWMatOff_UIDPRICE0: TFIBFloatField
      DisplayLabel = #1059#1095#1077#1090#1085#1072#1103' '#1094#1077#1085#1072
      FieldName = 'PRICE0'
    end
    object taAWMatOff_UIDN: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072' '#1087#1086#1090#1077#1088#1100
      FieldName = 'N'
      DisplayFormat = '0.##'
    end
  end
  object dsrAWMatOff_UID: TDataSource
    DataSet = taAWMatOff_UID
    Left = 176
    Top = 140
  end
end
