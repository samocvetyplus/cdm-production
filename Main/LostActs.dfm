inherited fmLostActs: TfmLostActs
  Left = 175
  Top = 151
  Caption = #1040#1082#1090#1099' '#1087#1086' '#1087#1086#1090#1077#1088#1103#1085#1099#1084' '#1082#1072#1084#1085#1103#1084
  PixelsPerInch = 96
  TextHeight = 13
  inherited tb1: TSpeedBar
    inherited spitAdd: TSpeedItem
      Enabled = False
      Visible = False
    end
    inherited spitDel: TSpeedItem
      Enabled = False
      Visible = False
    end
    inherited spitEdit: TSpeedItem
      Left = 3
      OnClick = acEditDocExecute
    end
    inherited spitPrint: TSpeedItem
      Left = 67
      OnClick = acPrintDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    DataSource = dmInv.dsrLostActs
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    UseMultiTitle = True
    Visible = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO1'
        Footers = <>
        Width = 127
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
      end>
  end
  inherited ppDep: TPopupMenu
    Left = 24
    Top = 124
  end
  inherited acAEvent: TActionList
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
    inherited acPrintDoc: TAction
      OnExecute = acPrintDocExecute
      OnUpdate = acPrintDocUpdate
    end
  end
end
