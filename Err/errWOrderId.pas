unit errWOrderId;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList, ExtCtrls, ComCtrls,
  Grids, DBGrids, RXDBCtrl, DBGridEh, ActnList, DBGridEhGrouping, GridsEh,
  rxPlacemnt, rxSpeedbar;

type
  TfmErrWOrderId = class(TfmAncestor)
    SpeedItem1: TSpeedItem;
    DBGridEh1: TDBGridEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmErrWOrderId: TfmErrWOrderId;

implementation

uses MainData, dbUtil;

{$R *.dfm}

procedure TfmErrWOrderId.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  CloseDataSet(dmMain.taErrWOrder);
end;

procedure TfmErrWOrderId.FormCreate(Sender: TObject);
begin
  ReOpenDataSet(dmMain.taErrWOrder);
end;

procedure TfmErrWOrderId.SpeedItem1Click(Sender: TObject);
begin
  dmMain.taErrWOrder.First;

  ReOpenDataSet(dmMain.taErrWOrder);
end;

end.
