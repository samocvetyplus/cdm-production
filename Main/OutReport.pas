unit OutReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGridEh, Buttons, Period, dbutil, StdCtrls, Mask, DBCtrlsEh,
  RxDateUtil, DateUtils, DBLookupEh, ActnList, DBGridEhGrouping, GridsEh,
  rxPlacemnt, rxSpeedbar;

type
  TfmOutReport = class(TfmAncestor)
    DBGridEh1: TDBGridEh;
    SpeedItem1: TSpeedItem;
    SpeedButton1: TSpeedButton;
    laPeriod: TLabel;
    Label2: TLabel;
    cbComp: TDBLookupComboboxEh;
    SpeedItem2: TSpeedItem;
    CheckBox1: TCheckBox;
    procedure ShowPeriod;
    procedure SpeedItem1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure SpeedItem2Click(Sender: TObject);
  private
    { Private declarations }
    FBD,FED:TDateTime;
  public
    { Public declarations }                    
  end;

var
  fmOutReport: TfmOutReport;

implementation

uses InvData, DictData, PrintData, DB;

{$R *.dfm}

procedure TfmOutReport.SpeedItem1Click(Sender: TObject);
var all:integer;
begin
  inherited;
  try
  Screen.Cursor:=crHourGlass;
  if CheckBox1.Checked then all:=0
  else all:=1;
  with dminv.sqlPublic do
  begin
    SQL.Clear;
    SQL.Add('execute procedure REPORT_PRODUCTION(:BD,:ED,:COMPID,:ALL)');
    ParamByName('BD').AsTimeStamp:=DateTimeToTimeStamp(FBD);
    ParamByName('ED').AsTimeStamp:=DateTimeToTimeStamp(FED);
    ParamByName('ALL').AsInteger:=all;
    if cbComp.Text='' then ParamByName('COMPID').AsInteger:=-1
    else   ParamByName('COMPID').AsInteger:=dm.taCompCOMPID.asinteger;
    ExecQuery;
    Transaction.CommitRetaining;
  end;
  ReOpenDataSet(dmInv.taProdReport);
  finally
   Screen.Cursor:=crDefault;
  end;
end;

procedure TfmOutReport.FormCreate(Sender: TObject);
begin
  inherited;

{  with dminv.sqlPublic do
  begin
    SQL.Clear;
    SQL.Add('Delete from TMP_PROD_REPORT');
    ExecQuery;
    Transaction.CommitRetaining;
  END; }


  reOpenDataSets([dm.taRec, dm.taComp , dmInv.taProdREport]);
  if (dm.taReCPRODREP_ED.AsDateTime>0) and (dm.taReCPRODREP_ED.AsDateTime>0) then
  begin
   FBD:=dm.taReCPRODREP_BD.AsDateTime;
   FED:=dm.taReCPRODREP_ED.AsDateTime;
  end else
  begin
    with dminv.sqlPublic do
    begin
      SQL.Clear;
      SQL.Add('Delete from TMP_PROD_REPORT');
      ExecQuery;
      Transaction.CommitRetaining;
    END;
   FBD:=StartOfTheMonth(Today);
   FED:=Today;
   ReOpenDataSet(dmInv.taProdReport);
  end;
  cbComp.Text:=dm.taReCName1.AsString;
 { cbComp.OnChange := nil;
  cbComp.Items.Clear;
  cbComp.Items.Assign(dm.dComp);
  cbComp.ItemIndex := 0;}
//  cbComp.OnChange := cbCompChange;

end;

procedure TfmOutReport.SpeedButton1Click(Sender: TObject);
begin
  inherited;
  if ShowPeriodForm(FBD, FED) then
  begin
//  FOnChangePeriod(FBD, FED);
    ShowPeriod;
    Application.ProcessMessages;
    ReOpenDataSets([dmInv.taProdREport]);
    Application.ProcessMessages;
  end;
end;

procedure TfmOutReport.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CloseDataSets([dm.taComp,dmInv.taProdREport]);
end;

procedure TfmOutReport.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(FBD) + ' �� ' +DateToStr(FED);
end;

procedure TfmOutReport.FormShow(Sender: TObject);
begin
  inherited;
  ShowPeriod;
end;

procedure TfmOutReport.DBGridEh1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if (Column.FieldName<>'ART') and (Column.FieldName<>'SZ') and
  (Column.Field.AsFloat < 0.01) then Background:=clWhite;
end;

procedure TfmOutReport.SpeedItem2Click(Sender: TObject);
begin
  inherited;
   dmPrint.PrintDocumentA(dkProdReport, null );
end;

end.
