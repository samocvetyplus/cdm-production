inherited fmProtocol_PS: TfmProtocol_PS
  Left = 0
  Top = 191
  Caption = #1048#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1084#1077#1089#1090#1072#1084' '#1093#1088#1072#1085#1077#1085#1080#1103
  ClientHeight = 688
  ClientWidth = 1001
  OldCreateOrder = True
  Position = poDesigned
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 1009
  ExplicitHeight = 722
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter2: TSplitter [0]
    Left = 0
    Top = 429
    Width = 1001
    Height = 4
    Cursor = crVSplit
    Align = alTop
  end
  inherited stbrStatus: TStatusBar
    Top = 669
    Width = 1001
    Panels = <
      item
        Width = 100
      end
      item
        Width = 50
      end>
    ExplicitTop = 669
    ExplicitWidth = 1001
  end
  inherited tb1: TSpeedBar
    Width = 1001
    ExplicitWidth = 1001
    inherited spitExit: TSpeedItem
      Left = 755
    end
    object spitCreateProtocol: TSpeedItem
      Action = acCreateProtocol
      BtnCaption = #1057#1086#1079#1076#1072#1090#1100
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00D3D3D300757575007575750075757500AD7376002E2D2D00676767007575
        75007575750075757500757575007575750074747400AAAAAA00FF00FF00FF00
        FF00AFAFAF00FFFFFF00FFFFFF00FFFFFF00F1D3D200BE76780067625D00B6B6
        B600FDFDFD00FFFFFF00FFFFFF00FFFFFF00FFFFFF0074747400FF00FF00FF00
        FF00AAAAAA00D5D5D500EFEFEF00D4D4D400F1F1F100C2785700CD6600005D4D
        3D0068686800959595009393930097979700F7F7F70075757500FF00FF00FF00
        FF00AAAAAA00DADADA00CBCBCB00C1C1C100CCCCCC00C7CAC700D26C0D00C962
        0000685A4C0077777700D4D4D40091919100FFFFFF0075757500E3AA9600D090
        790088645B006C656300A8A8A800C7C7C700FDFDFD00EEEEEE00FBF5F100D377
        1D00C95F0000675847008E8E8E0085858500FEFEFE0075757500FFCCB700D197
        8100DC967D00D1957C00FBB89A00D7957E0084767100F1F1F100FFFFFF00EFEC
        DF00D57E2700CC600000BE7D6000373A3A00AAAAAA0073737300FFD0BC00F0B9
        9E00E5C0A500EED2BA00D29A8500F7BA9C00CC9F8C00CDCDCD00D7D7D700FFFF
        FF00BEA78500EFDEE600C55D0400C16265004E50510055555500FFD4C300D19A
        8600DE9B8100C3887300FCC5A500D09F8700C8A18900D3D3D300DFDFDF00DBDB
        DB00DDDDDD00DAC19800E6984C00CE6100007F696E002E2D2D00FFD8C700F1BF
        A300E5C0A500EACFBA00D6A48D00FDD7BA00CBA38C00EFEFEF00FFFFFF00FDFD
        FD00FFFFFF00FFFFFF00F5E9DF00E8954700C6640B00B8A4A400FFDED000D09D
        8800E3A48900C78D7800FAC6A5008E899600CDAB9300E5E5E500F7F7F700F0F0
        F000F4F4F400F6F6F600ECECEC00D5D4CE00DF8F2D005B535200FFE1D500EEC1
        A500E4C0A400E4CAB700D4B09C0085B4FF00CFAB9200C3C3C300CBCBCB00C8C8
        C800CACACA00CACACA00C7C7C700CBCBCB00F7F7F70075757500FFE5DC00EBBD
        9F00E5B69A00E9BA9E00F0CAAE00F1CCB000CFB09B00E6E6E600FAFAFA00F3F3
        F300F7F7F700FAFAFA00EEEEEE00FBFBFB00F2F2F20075757500FFEDE600E792
        6500FFA06A00FFA67400FFAD7D00FDAF8200D0AE9B00FCFCFC00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B7B7B00FFEDE700FAD4
        B400E3A17D00E0977200E88C5E00E87E4900DBBFAC00FBFBFB00CFCFCF00CFCF
        CF00CFCFCF00FFFFFF00B3B3B300BEBEBE0082828200EAEAEA00EEE0C900FFF0
        CE00FFFDD800FFF6CF00FFF7D100FFFAD500E4CBB000FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF00F1F1F1009F9F9F00F0F0F000FF00FF00FF00FF00FF00
        FF00ACABAC00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00FFFFFF00FFFFFF0094949400F4F4F400FF00FF00FF00FF00}
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acCreateProtocolExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      Action = acDelProtocol
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Spacing = 1
      Left = 67
      Top = 3
      Visible = True
      OnClick = acDelProtocolExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      Action = acRefreshProtocol
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00B5848400B5848400B5848400B5848400B5848400B5848400B5848400B584
        8400B5848400B5848400B5848400B5848400B5848400FF00FF00FF00FF00FF00
        FF00B5848400FFEFD600D6DEAD00DEDEAD00F7DEB500EFD6A500EFD69C00F7CE
        9C00F7CE9400F7CE9C00F7CE9C00F7D69C00B5848400FF00FF00FF00FF00FF00
        FF00B5848400FFEFDE00ADCE940042AD39008CBD6B0031A5290031A529007BB5
        5200D6C68C00EFCE9400EFCE9400F7D69C00B5848400FF00FF00FF00FF00FF00
        FF00B5847300FFF7E700CEDEAD0021A51800009C0000009C0000009C0000009C
        000042AD2900E7CE9400EFCE9400F7D69C00B5848400FF00FF00FF00FF00FF00
        FF00B5847300FFF7EF00CEDEB50021A51800009C000029A52100BDCE8C008CBD
        6B00089C080094BD6300EFCE9C00F7D69C00B5848400FF00FF00FF00FF00FF00
        FF00BD8C8400FFFFF700CEDEBD0010A51000009C0000089C08009CC67B00F7DE
        BD00BDC68C0084BD6B00F7D6A500F7D69C00B5848400FF00FF00FF00FF00FF00
        FF00BD8C8400FFFFFF00EFEFDE00B5DEA500ADD69C00ADCE9400ADCE9400F7DE
        C600F7DEBD00B5D69400F7D6AD00F7D6A500B5848400FF00FF00FF00FF00FF00
        FF00CE9C8400FFFFFF00BDE7B500FFF7EF00F7EFDE00B5D69C00ADCE9400ADCE
        8C00B5CE8C00EFDEB500F7DEB500F7DEAD00B5848400FF00FF00FF00FF00FF00
        FF00CE9C8400FFFFFF008CD68C00C6E7BD00FFF7EF009CCE8C00089C0800009C
        000010A51000F7DEC600F7DEBD00FFDEB500B5848400FF00FF00FF00FF00FF00
        FF00DEAD8400FFFFFF009CD69C00089C080094D68C00C6DEB50031AD2900009C
        000021A51800F7E7CE00F7E7C600F7DEB500B5848400FF00FF00FF00FF00FF00
        FF00DEAD8400FFFFFF00F7FFF70042B54200009C0000009C0000009C0000009C
        000021A51800FFEFDE00E7DEC600C6BDAD00B5848400FF00FF00FF00FF00FF00
        FF00E7B58C00FFFFFF00FFFFFF00E7F7E70084CE840039B5390039AD31008CCE
        840042AD3900AD847300BD847B00BD847B00B5848400FF00FF00FF00FF00FF00
        FF00E7B58C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFF7EF00E7F7E700FFFF
        FF00BDC6AD00A58C6B00EFB57300EFA54A00C6846B00FF00FF00FF00FF00FF00
        FF00EFBD9400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
        FF00E7D6D600BD847B00FFC67300CE947300FF00FF00FF00FF00FF00FF00FF00
        FF00EFBD9400FFF7F700FFF7F700FFF7F700FFF7F700FFF7F700FFF7F700FFF7
        F700E7D6CE00BD847B00CE9C8400FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00EFBD9400DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD
        8400DEAD8400BD847B00FF00FF00FF00FF00FF00FF00FF00FF00}
      Spacing = 1
      Left = 131
      Top = 3
      Visible = True
      OnClick = acRefreshProtocolExecute
      SectionName = 'Untitled (0)'
    end
  end
  object Panel2: TPanel [3]
    Left = 0
    Top = 42
    Width = 1001
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 8
      Top = 12
      Width = 135
      Height = 13
      Caption = #1055#1088#1086#1090#1086#1082#1086#1083' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
    end
    object Label2: TLabel
      Left = 8
      Top = 32
      Width = 112
      Height = 13
      Caption = #1044#1072#1090#1072' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
    end
    object Label3: TLabel
      Left = 232
      Top = 12
      Width = 70
      Height = 13
      Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
    end
    object txtUpd: TDBText
      Left = 308
      Top = 12
      Width = 157
      Height = 13
      DataField = 'UPD'
      DataSource = dmMain.dsrPList
    end
    object edDocNo: TDBEditEh
      Left = 148
      Top = 8
      Width = 73
      Height = 19
      DataField = 'DOCNO'
      DataSource = dmMain.dsrPList
      EditButtons = <>
      Enabled = False
      Flat = True
      TabOrder = 0
      Visible = True
    end
    object edDocDate: TDBDateTimeEditEh
      Left = 148
      Top = 28
      Width = 74
      Height = 19
      DataField = 'DOCDATE'
      DataSource = dmMain.dsrPList
      Enabled = False
      EditButton.Style = ebsGlyphEh
      EditButtons = <>
      Flat = True
      Kind = dtkDateEh
      TabOrder = 1
      Visible = True
    end
  end
  object plMain: TPanel [4]
    Left = 0
    Top = 93
    Width = 1001
    Height = 336
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object Splitter1: TSplitter
      Left = 500
      Top = 0
      Width = 4
      Height = 336
    end
    object plSemis: TPanel
      Left = 0
      Top = 0
      Width = 500
      Height = 336
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object gridSemis: TDBGridEh
        Left = 0
        Top = 184
        Width = 500
        Height = 152
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dmMain.dsrPIt_InSemis_d
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 2
        FrozenCols = 2
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
        PopupMenu = ppSemis
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        OnSumListAfterRecalcAll = gridSemisSumListAfterRecalcAll
        Columns = <
          item
            Color = clBtnFace
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            DropDownRows = 15
            DropDownWidth = -1
            EditButtons = <>
            FieldName = 'SEMISNAME1'
            Footers = <
              item
                Color = clInfoBk
                Value = #1042' 585 '#1087#1088#1086#1073#1077
                ValueType = fvtStaticText
              end
              item
                Value = #1041#1077#1079' '#1087#1077#1088#1077#1089#1095#1077#1090#1072
                ValueType = fvtStaticText
              end>
            Width = 119
          end
          item
            Color = clBtnFace
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            DropDownRows = 15
            DropDownSpecRow.CellsText = #1085#1077' '#1086#1087#1088#1077#1076#1077#1083#1077#1085#1072
            DropDownSpecRow.ShortCut = 16573
            DropDownSpecRow.Visible = True
            DropDownWidth = -1
            EditButtons = <>
            FieldName = 'FROMOPERNAME'
            Footers = <>
            Width = 115
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'INQ'
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'INQ'
                ValueType = fvtSum
              end>
            Title.Caption = #1042#1093#1086#1076#1103#1097#1080#1077'|'#1082#1086#1083'-'#1074#1086
            Width = 40
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'INW'
            Footer.FieldName = 'INW'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'INW'
                ValueType = fvtSum
              end>
            Title.Caption = #1042#1093#1086#1076#1103#1097#1080#1077'|'#1074#1077#1089
            Width = 32
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'GETQ'
            Footer.FieldName = 'GETQ'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'GETQ'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1042#1099#1076#1072#1085#1086'|'#1082#1086#1083'-'#1074#1086
            Width = 36
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'GETW'
            Footer.FieldName = 'GETW'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'GETW'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1042#1099#1076#1072#1085#1086'|'#1074#1077#1089
            Width = 28
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'REWORKQ'
            Footer.FieldName = 'REWORKQ'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'REWORKQ'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1044#1086#1088#1072#1073#1086#1090#1082#1072'|'#1082#1086#1083'-'#1074#1086
            Width = 36
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'REWORKW'
            Footer.FieldName = 'REWORKW'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'REWORKQ'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1044#1086#1088#1072#1073#1086#1090#1082#1072'|'#1074#1077#1089
            Width = 29
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'DONEQ'
            Footer.FieldName = 'DONEQ'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'DONEQ'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1053#1072#1088#1103#1076'|'#1082#1086#1083'-'#1074#1086
            Width = 38
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'DONEW'
            Footer.FieldName = 'DONEW'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'DONEW'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1053#1072#1088#1103#1076'|'#1074#1077#1089
            Width = 26
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'RETQ'
            Footer.FieldName = 'RETQ'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'RETQ'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1042#1086#1079#1074#1088#1072#1090'|'#1082#1086#1083'-'#1074#1086
            Width = 39
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'RETW'
            Footer.FieldName = 'RETW'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'RETW'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1042#1086#1079#1074#1088#1072#1090'|'#1074#1077#1089
            Width = 28
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'REJQ'
            Footer.FieldName = 'REJQ'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'REJQ'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1041#1088#1072#1082'|'#1082#1086#1083'-'#1074#1086
            Width = 40
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'REJW'
            Footer.FieldName = 'REJW'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'REJW'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1041#1088#1072#1082'|'#1074#1077#1089
            Width = 30
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'NKQ'
            Footer.FieldName = 'NKQ'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'NKQ'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1053#1077#1082#1086#1084#1087#1083#1077#1082#1090'|'#1082#1086#1083'-'#1074#1086
            Width = 41
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'NKW'
            Footer.FieldName = 'NKW'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'NKW'
                ValueType = fvtSum
              end>
            Title.Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074'|'#1053#1077#1082#1086#1084#1087#1083#1077#1082#1090'|'#1074#1077#1089
            Width = 33
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'IQ'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'Q'
                ValueType = fvtSum
              end>
            Title.Caption = #1059#1095#1077#1090#1085#1086#1077' '#1085#1072#1083#1080#1095#1080#1077'|'#1082#1086#1083'-'#1074#1086
            Width = 38
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'IW'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'W'
                ValueType = fvtSum
              end>
            Title.Caption = #1059#1095#1077#1090#1085#1086#1077' '#1085#1072#1083#1080#1095#1080#1077'|'#1074#1077#1089
            Width = 39
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'FQ'
            Footer.FieldName = 'Q'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'FQ'
                ValueType = fvtSum
              end>
            Title.Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077' '#1085#1072#1083#1080#1095#1080#1077'|'#1082#1086#1083'-'#1074#1086
            Width = 39
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'FW'
            Footer.DisplayFormat = '0.###'
            Footer.FieldName = 'W'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'FW'
                ValueType = fvtSum
              end>
            Title.Caption = #1060#1072#1082#1090#1080#1095#1077#1089#1082#1086#1077' '#1085#1072#1083#1080#1095#1080#1077'|'#1074#1077#1089
            Width = 32
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'RQ'
            Footer.FieldName = 'RQ'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'RQ'
                ValueType = fvtSum
              end>
            Title.Caption = #1054#1089#1090#1072#1090#1086#1082'|'#1082#1086#1083'-'#1074#1086
            Width = 36
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'RW'
            Footer.FieldName = 'RW'
            Footer.ValueType = fvtSum
            Footers = <
              item
                Color = clInfoBk
                ValueType = fvtStaticText
              end
              item
                FieldName = 'RW'
                ValueType = fvtSum
              end>
            Title.Caption = #1054#1089#1090#1072#1090#1086#1082'|'#1074#1077#1089
            Width = 41
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel5: TPanel
        Left = 0
        Top = 129
        Width = 500
        Height = 16
        Align = alTop
        BevelOuter = bvNone
        Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
        Color = clMoneyGreen
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object TBDock1: TTBDock
        Left = 0
        Top = 145
        Width = 500
        Height = 39
        object tlbrSemis: TTBToolbar
          Left = 0
          Top = 0
          Align = alClient
          AutoResize = False
          BorderStyle = bsNone
          DockMode = dmCannotFloat
          DockPos = 0
          Images = ilButtons
          Options = [tboImageAboveCaption]
          ParentShowHint = False
          ProcessShortCuts = True
          ShowHint = True
          TabOrder = 0
          object TBItem2: TTBItem
            Action = acAddSemis
          end
          object TBItem1: TTBItem
            Action = acDelSemis
          end
          object TBItem12: TTBItem
            Action = acCopySemis
          end
          object TBItem13: TTBItem
            Action = acZeroSemis
          end
        end
      end
      object gridPS: TDBGridEh
        Left = 0
        Top = 39
        Width = 500
        Height = 90
        Align = alTop
        AllowedOperations = []
        AutoFitColWidths = True
        BorderStyle = bsNone
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dmMain.dsrProtocol_d
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghHighlightFocus, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 3
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'PSNAME'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Footers = <>
            Title.Alignment = taCenter
            Title.Color = clMoneyGreen
            Title.Font.Charset = RUSSIAN_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 205
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'OPERNAME'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Footers = <>
            Title.Alignment = taCenter
            Title.Color = clMoneyGreen
            Title.Font.Charset = RUSSIAN_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'Tahoma'
            Title.Font.Style = [fsBold]
            Width = 165
          end
          item
            EditButtons = <>
            FieldName = 'LOSSES'
            Footers = <>
            Title.Caption = #1057#1098#1077#1084
            Title.Color = clMoneyGreen
            Title.Font.Charset = DEFAULT_CHARSET
            Title.Font.Color = clWindowText
            Title.Font.Height = -11
            Title.Font.Name = 'MS Sans Serif'
            Title.Font.Style = [fsBold]
            Width = 101
          end>
        object RowDetailData: TRowDetailPanelControlEh
          object DBGrid1: TDBGrid
            Left = -320
            Top = -120
            Width = 320
            Height = 120
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
          end
        end
      end
      object TBDock4: TTBDock
        Left = 0
        Top = 0
        Width = 500
        Height = 39
        object tlbrPS: TTBToolbar
          Left = 6
          Top = 0
          AutoResize = False
          BorderStyle = bsNone
          DockMode = dmCannotFloat
          DockPos = 6
          Images = ilButtons
          Options = [tboImageAboveCaption]
          TabOrder = 0
          object TBItem21: TTBItem
            Action = acOpenDetail
          end
          object TBItem22: TTBItem
            Action = acPrintProtocol
          end
          object TBItem23: TTBItem
            Action = acPrintResult
            Caption = #1056#1072#1089#1093#1086#1076
          end
        end
      end
    end
    object Panel6: TPanel
      Left = 504
      Top = 0
      Width = 497
      Height = 336
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 497
        Height = 16
        Align = alTop
        BevelOuter = bvNone
        Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
        Color = clMoneyGreen
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object gridAssort: TDBGridEh
        Left = 0
        Top = 55
        Width = 497
        Height = 281
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dmMain.dsrPArt
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        FrozenCols = 5
        Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
        PopupMenu = ppArt
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            Color = clBtnFace
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            DropDownRows = 15
            DropDownWidth = -1
            EditButtons = <>
            FieldName = 'OPERATIONNAME'
            Footers = <>
            Title.EndEllipsis = True
            Width = 145
          end
          item
            Color = clBtnFace
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART'
            Footers = <>
            Title.EndEllipsis = True
            Width = 65
          end
          item
            Color = clBtnFace
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART2'
            Footers = <>
            Title.EndEllipsis = True
            Width = 41
          end
          item
            Color = clBtnFace
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'SZNAME'
            Footers = <>
            Title.EndEllipsis = True
            Width = 38
          end
          item
            Color = clBtnFace
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'U'
            Footers = <>
            KeyList.Strings = (
              '1'
              '2')
            PickList.Strings = (
              #1064#1090'.'
              #1055#1072#1088#1072)
            Title.EndEllipsis = True
            Width = 31
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'REST_IN_Q'
            Footer.FieldName = 'REST_IN_Q'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.EndEllipsis = True
            Width = 48
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'GET_Q'
            Footer.FieldName = 'GET_Q'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.EndEllipsis = True
            Width = 43
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'DONE_Q'
            Footer.FieldName = 'DONE_Q'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.EndEllipsis = True
            Width = 41
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'REJ_Q'
            Footer.FieldName = 'REJ_Q'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.EndEllipsis = True
            Width = 45
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.ValueType = fvtSum
            Footers = <>
            ReadOnly = True
            Title.EndEllipsis = True
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'FQ'
            Footer.FieldName = 'FQ'
            Footer.ValueType = fvtSum
            Footers = <>
            Title.EndEllipsis = True
            Width = 74
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object TBDock2: TTBDock
        Left = 0
        Top = 16
        Width = 497
        Height = 39
        object tlbrArt: TTBToolbar
          Left = 0
          Top = 0
          AutoResize = False
          BorderStyle = bsNone
          DockMode = dmCannotFloat
          DockPos = 4
          Images = ilButtons
          Options = [tboImageAboveCaption]
          Stretch = True
          TabOrder = 0
          object TBItem4: TTBItem
            Action = acAddArt
          end
          object TBItem3: TTBItem
            Action = acDelArt
          end
          object TBItem15: TTBItem
            Action = acCopyArt
          end
          object TBItem14: TTBItem
            Action = acZeroArt
          end
        end
      end
    end
  end
  object plWOrder: TPanel [5]
    Left = 0
    Top = 433
    Width = 1001
    Height = 236
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 4
    object TBDock3: TTBDock
      Left = 0
      Top = 0
      Width = 1001
      Height = 39
      object tlbrWOrder: TTBToolbar
        Left = 0
        Top = 0
        Align = alClient
        AutoResize = False
        BorderStyle = bsNone
        FullSize = True
        Images = ilButtons
        Options = [tboImageAboveCaption]
        TabOrder = 0
        object TBItem19: TTBItem
          Action = acCreateWOrder
        end
        object TBItem20: TTBItem
          Action = acCloseWOrder
        end
        object TBSeparatorItem3: TTBSeparatorItem
        end
        object TBControlItem1: TTBControlItem
          Control = Label4
        end
        object TBControlItem2: TTBControlItem
          Control = txtWOrderNo
        end
        object TBControlItem3: TTBControlItem
          Control = Label5
        end
        object TBControlItem4: TTBControlItem
          Control = txtWOrderDate
        end
        object Label4: TLabel
          Left = 185
          Top = 11
          Width = 46
          Height = 13
          Caption = #1053#1072#1088#1103#1076' '#8470
        end
        object txtWOrderNo: TDBText
          Left = 231
          Top = 11
          Width = 65
          Height = 13
          DataField = 'DOCNO'
          DataSource = dmMain.dsrWOrderList
        end
        object Label5: TLabel
          Left = 296
          Top = 11
          Width = 17
          Height = 13
          Caption = ' '#1086#1090' '
        end
        object txtWOrderDate: TDBText
          Left = 313
          Top = 11
          Width = 65
          Height = 13
          DataField = 'DOCDATE'
          DataSource = dmMain.dsrWOrderList
        end
      end
    end
    object plExt: TPanel
      Left = 0
      Top = 39
      Width = 277
      Height = 197
      Align = alLeft
      BevelInner = bvLowered
      TabOrder = 1
      object plExtTitle: TPanel
        Left = 2
        Top = 21
        Width = 273
        Height = 23
        Align = alTop
        BevelOuter = bvNone
        Constraints.MinHeight = 20
        Constraints.MinWidth = 273
        TabOrder = 0
        object lbMat: TLabel
          Left = 3
          Top = 5
          Width = 23
          Height = 13
          Caption = #1052#1072#1090'.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label7: TLabel
          Left = 116
          Top = 4
          Width = 56
          Height = 13
          Caption = #1054#1087#1077#1088#1072#1094#1080#1103'  '
        end
        object cmbxMat: TDBComboBoxEh
          Left = 32
          Top = 2
          Width = 81
          Height = 19
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Width = -1
          EditButtons = <>
          Flat = True
          TabOrder = 0
          Visible = True
          OnChange = cmbxMatChange
        end
        object cmbxOper: TDBComboBoxEh
          Left = 170
          Top = 2
          Width = 102
          Height = 19
          DropDownBox.Rows = 20
          DropDownBox.Width = -1
          EditButtons = <>
          Flat = True
          TabOrder = 1
          Visible = True
          OnChange = cmbxOperChange
        end
      end
      object gridWhSemis: TDBGridEh
        Left = 2
        Top = 44
        Width = 273
        Height = 151
        Align = alClient
        AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
        Color = clWhite
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dmMain.dsrWhSemis
        Flat = True
        FooterColor = clBtnFace
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        FooterRowCount = 1
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit]
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        RowLines = 2
        RowSizingAllowed = True
        SumList.Active = True
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyDown = gridWhSemisKeyDown
        Columns = <
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'SEMISNAME'
            Footers = <>
            Width = 99
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'Q'
            Footer.FieldName = 'Q'
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 38
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'W'
            Footer.FieldName = 'W'
            Footer.ValueType = fvtSum
            Footers = <>
            Width = 60
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'OPERNAME'
            Footers = <>
            Width = 82
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'UQ'
            Footers = <>
            Title.Caption = #1045#1076'. '#1080#1079#1084'|'#1082#1086#1083'-'#1074#1072
            Width = 43
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'UW'
            Footers = <>
            Title.Caption = #1045#1076'. '#1080#1079#1084'|'#1074#1077#1089#1072
            Width = 39
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'SEMISID'
            Footers = <>
            Visible = False
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'OPERID'
            Footers = <>
            Visible = False
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'GOOD'
            Footers = <>
            Visible = False
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object tlbrT: TTBToolbar
        Left = 2
        Top = 2
        Width = 273
        Height = 19
        Align = alTop
        ProcessShortCuts = True
        TabOrder = 2
        object it0: TTBItem
          Action = acItType0
          GroupIndex = 1
        end
        object TBSeparatorItem4: TTBSeparatorItem
        end
        object it3: TTBItem
          Action = acItType3
          GroupIndex = 1
        end
        object TBControlItem5: TTBControlItem
          Control = chbxW
        end
        object chbxW: TDBCheckBoxEh
          Left = 110
          Top = 3
          Width = 155
          Height = 13
          Alignment = taLeftJustify
          Caption = '                                      '#1042#1077#1089#1099
          Flat = True
          TabOrder = 0
          ValueChecked = 'True'
          ValueUnchecked = 'False'
        end
      end
    end
    object tbdcSemis: TTBDock
      Left = 277
      Top = 39
      Width = 66
      Height = 197
      Position = dpLeft
      object tlbrWOrderControlPanel: TTBToolbar
        Left = 0
        Top = 0
        Align = alLeft
        DockMode = dmCannotFloat
        DockPos = 0
        Images = ilButtons
        Options = [tboImageAboveCaption]
        TabOrder = 0
        object TBItem16: TTBItem
          Action = acAddNewToWOrder
        end
        object TBItem17: TTBItem
          Action = acAddToWOrder
        end
        object TBItem18: TTBItem
          Action = acDelFromWOrder
        end
      end
    end
    object gridItem: TDBGridEh
      Left = 343
      Top = 39
      Width = 658
      Height = 197
      Align = alClient
      AllowedOperations = [alopUpdateEh]
      AllowedSelections = []
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dmMain.dsrWOrder
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      PopupMenu = ppInv
      RowDetailPanel.Color = clBtnFace
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      OnExit = gridItemExit
      OnGetCellParams = gridItemGetCellParams
      OnKeyDown = gridItemKeyDown
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Width = 22
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'DOCNO'
          Footers = <>
          Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1053#1086#1084#1077#1088
          Title.EndEllipsis = True
          Width = 40
        end
        item
          DisplayFormat = 'd.mm.yy'
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'DOCDATE'
          Footers = <>
          Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1044#1072#1090#1072
          Title.EndEllipsis = True
          Width = 36
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ADOCNO'
          Footers = <>
          Title.EndEllipsis = True
          Width = 48
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERNAME'
          Footers = <>
          Title.EndEllipsis = True
          Width = 73
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ITTYPE'
          Footers = <>
          Title.EndEllipsis = True
          Width = 60
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          DropDownRows = 20
          EditButtons = <>
          FieldName = 'SEMISNAME'
          Footers = <>
          Title.EndEllipsis = True
          Width = 82
        end
        item
          ButtonStyle = cbsEllipsis
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'Q'
          Footers = <>
          Title.EndEllipsis = True
          Width = 43
          OnEditButtonClick = gridItemColumns7EditButtonClick
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'W'
          Footers = <>
          Title.EndEllipsis = True
          Width = 39
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'USERNAME'
          Footers = <>
          Title.Caption = #1042#1099#1076#1072#1085#1086'|'#1042#1099#1076#1072#1083
          Title.EndEllipsis = True
          Width = 66
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'DEPNAME'
          Footers = <>
          Title.Caption = #1042#1099#1076#1072#1085#1086'|'#1052#1061'1'
          Title.EndEllipsis = True
          Width = 56
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'JOBNAME'
          Footers = <>
          Title.Caption = #1055#1086#1083#1091#1095#1077#1085#1086'|'#1055#1086#1083#1091#1095#1080#1083
          Title.EndEllipsis = True
          Width = 61
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'JOBDEPNAME'
          Footers = <>
          Title.Caption = #1055#1086#1083#1091#1095#1077#1085#1086'|'#1052#1061'2'
          Title.EndEllipsis = True
          Width = 49
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'FROMOPERNAME'
          Footers = <>
          Title.EndEllipsis = True
          Width = 67
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'INVID'
          Footers = <>
          Title.EndEllipsis = True
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WOITEMID'
          Footers = <>
          Title.EndEllipsis = True
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WORDERID'
          Footers = <>
          Title.EndEllipsis = True
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ITDATE'
          Footers = <>
          Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1042#1099#1076#1072#1085#1086
          Title.EndEllipsis = True
          Visible = False
          Width = 66
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'REJPSNAME'
          Footers = <>
          Title.EndEllipsis = True
          Visible = False
          Width = 122
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'REJREF'
          Footers = <>
          Title.EndEllipsis = True
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'FULLART'
          Footers = <>
          Title.EndEllipsis = True
          Width = 50
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'REF'
          Footers = <>
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          DropDownRows = 15
          DropDownWidth = -1
          EditButtons = <>
          FieldName = 'REJNAME'
          Footers = <>
          Title.Caption = #1041#1088#1072#1082'|'#1055#1088#1080#1095#1080#1085#1072
          Title.EndEllipsis = True
          Width = 60
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'REJPSNAME'
          Footers = <>
          Title.Caption = #1041#1088#1072#1082'|'#1042#1080#1085#1086#1074#1085#1080#1082
          Title.EndEllipsis = True
          Width = 53
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  inherited ilButtons: TImageList
    Left = 18
    Top = 316
    Bitmap = {
      494C010111001300040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000005000000001002000000000000050
      000000000000000000000000000000000000000000000000000000000000299C
      DE00299CDE00A57B7300A57B7300A57B7300A57B7300A57B7300A57B7300A57B
      7300A57B7300A57B7300A57B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000299CDE008CD6
      EF0084D6F700CEC6BD00FFEFDE00FFF7E700FFF7E700F7EFDE00F7EFDE00F7EF
      DE00F7EFDE00FFEFDE00A57B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00A5EFFF0094F7
      FF008CF7FF00CEC6BD00F7E7DE00F7E7D600F7DEC600F7DEC600F7DEC600F7DE
      BD00F7DEC600F7E7D600A57B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00A5E7FF0094EF
      FF0084EFFF00CEC6BD00FFEFDE00FFE7CE00FFDEBD00FFDEBD00FFDEBD00FFDE
      BD00F7DEC600F7E7D600A57B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00ADEFFF00A5F7
      FF0094F7FF00CEC6BD00FFEFE700FFE7D600FFDEC600FFDEC600FFDEC600F7DE
      BD00F7DEC600F7E7D600A57B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00B5EFFF00ADF7
      FF00A5F7FF00CEC6BD00FFEFEF00FFEFDE00FFE7D600FFE7CE00FFE7CE00FFE7
      CE00F7E7D600F7EFDE00A57B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00BDEFFF00BDF7
      FF00B5F7FF00CEC6BD00FFF7F700FFEFD600FFDEBD00FFDEBD00FFDEBD00FFDE
      B500FFE7CE00F7EFE700A57B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00C6EFFF00CEF7
      FF00BDF7FF00CEC6BD00FFF7F700FFF7F700FFF7E700FFEFE700FFEFE700FFEF
      E700FFF7E700EFE7DE00A57B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00CEEFFF00DEFF
      FF00CEFFFF00CEC6BD00FFFFF700FFFFFF00FFFFFF00FFFFFF00FFFFFF00F7E7
      E700D6BDB500C6ADA500A57B7300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00D6F7FF00EFFF
      FF00DEFFFF00CEC6BD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00DECE
      C600E7AD7300F7945A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00DEF7FF00FFFF
      FF00EFFFFF00CEC6BD00FFF7EF00FFF7F700FFF7F700FFF7F700FFF7F700E7C6
      BD00C6AD8C00299CDE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00DEF7FF00FFFF
      FF00F7FFFF00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEC6
      BD0084C6DE00299CDE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00DEF7FF00F7F7
      F700B5C6CE00ADC6CE00A5C6CE00A5C6CE00A5C6CE00A5C6CE00B5D6D600DEFF
      FF008CDEF700299CDE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000299CDE00E7FFFF00DECE
      C600BDA59C00CEC6BD00CEC6BD00CEC6BD00CEC6BD00CEBDB500BD9C9400E7EF
      E70094DEF700299CDE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000299CDE00B5D6
      E700949C9C00E7DED600FFFFFF00FFFFF700FFFFF700D6C6BD00849CA5008CCE
      E700299CDE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000299C
      DE00299CDE009C948C009C948C009C948C009C948C009C948C00299CDE00299C
      DE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000392D3900392D3900392D3900392D390000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000020A1C9002CAACF001082AC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B58C8C008C5A5A008C5A5A008C5A5A008C5A5A008C5A5A008C5A
      5A008C5A5A008C5A5A008C5A5A00000000000000000000000000000000000000
      0000392D39007A657300A494A000B5A9B500887D8800392D3900392D3900392D
      3900000000000000000000000000000000000000000000000000000000002BA3
      C90024A5CC000F84AE00149AC30024AED60033B1D500188BB4001787AF0043AB
      CC003DA8CB000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B58C8C00FFFFEF00FFF7E700FFF7DE00F7EFDE00F7EFDE00F7EF
      DE00FFEFDE00F7E7D6008C5A5A0000000000000000000000000000000000392D
      39007A6A7A00877784005C505C008D818D00CFC3CF00F7EBF700E5D9E500A99C
      A800392D3900392D3900392D39000000000000000000000000000000000032A5
      C90037B8DC0014AED90011A1CB001DC7F00048D7F80034A6CA005CC1DD0067C4
      DE003FA4C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B58C8C00FFF7E700F7E7CE00F7E7CE00F7DECE00F7DEC600F7DE
      C600F7E7CE00EFDECE008C5A5A00000000000000000000000000392D39007768
      77007C6C7C00907F8F0055495500554955007A6E7A00807480008D818D00A196
      A200DDD2DD0095899500C6BAC6007462730000000000208CB4002C98BD004EB5
      D50085DBEF0051C0DE0039C8EC001ED7FF003ADBFF005FD4F10075C6DF00B0E1
      EC0090CEE1001B8BB50000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      000000000000B58C8C00FFF7E700FFD6A500FFD6A500FFD6A500FFD6A500FFD6
      A500FFD6A500EFDECE008C5A5A000000000000000000392D3900D0C2D200CDBC
      CD009D8B9C0084778400A798A700847C84005549550055495500554955005549
      550081758100877C88005C505C00584E58000000000060BAD70048B0D20053BE
      E00095EDFF008DEFFF005AE5FF0027DAFF001CD8FF0052E2FF0079E8FF007DEB
      FF003FCBEE0031B1D9002CA4CE00000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00BDBDBD000000000000000000B58C8C008C5A5A008C5A
      5A008C5A5A00B58C8C00FFF7EF00FFE7CE00FFE7C600FFDEC600FFDEC600F7DE
      BD00F7E7D600EFDECE009C6B630000000000392D390096899700D7CCD900D7CC
      D700E6DAE900625663006357630082758100AB9FAD00B9AFBB00B1A5B1009387
      930055495500554955005D505D005A4E5A00000000001391C00022A6D7004EC3
      EA0081DFF70092EFFF0071D3E8005CB8CC0050B2C9002AC7EB0007D1FF0004CF
      FE0009D0FE0008C9F5000EB4E100000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000B58C8C00FFFFEF00FFF7
      E700FFF7DE00B58C8C00FFFFF700FFE7CE00FFE7CE00FFE7C600FFDEC600FFDE
      C600F7E7D600EFE7D6009C6B6B0000000000392D3900EFE2F200E5D9E700F4EA
      F700E6D9E900E5D6E600C6B9C8005F525F006E606E005C4A5700665A66008B7F
      8B00A99DA900C5B9C5008F838F003B2F3B00000000001C8EB80028ADDE003FBB
      E7006DD7F60091C7D3009796960097969600979696009796960045BCD80002D2
      FF0014D3FE001ED2FB000C9ECB00000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF00FFFFFF000000000000000000B58C8C00FFF7E700F7E7
      CE00F7E7CE00B58C8C00FFFFF700FFD6A500FFD6A500FFD6A500FFD6A500FFD6
      A500FFD6A500F7E7DE00A57B730000000000392D3900FCF0FC00FAF0FC00F2E6
      F400F0E3F200BA7A6D009C401100B371650084778400BAA8BA00978497006E5E
      6D005A4B57005F525E005E525E0000000000000000001E92BC0027ADDF0031B4
      E3005ACFF40097969600E9E3E200B2B3B300ACA2A200E1B7B5009796960059DC
      FA009CF0FF00B5EFFC003D9BBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000000000000000000B58C8C00FFF7E700FFD6
      A500FFD6A500B58C8C00FFFFFF00FFEFDE00FFE7D600FFE7D600FFE7D600FFEF
      D600FFF7E700EFE7DE00A57B730000000000392D3900FFF3FF00FCF0FC00FFF3
      FF00FCEFFE00C44D0F00E1C49400851E0C00CDBDCD00C1AFC100B6A3B5009B87
      9900392D3900382B3700000000000000000000000000148CB8001F9DCD0027AC
      DD0047C5EF0097969600E9E2E000B1B2B200ACA2A200DCB5B400979696007CE4
      FB00ACECF9008ACBE00048A5C4000000000000000000FF000000FF000000FF00
      0000FF000000FF00000000000000000000000000000000000000000000000000
      0000FFFFFF00FFFFFF00000000000000000000000000B58C8C00FFF7EF00FFE7
      CE00FFE7C600B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFDE
      DE00D6C6C600BDADAD00B584730000000000493D4900493D4900493D4900E2D6
      E100D9CFDA00C44D0F00E1C49400851E0C00D5C9D500B6A5B6007C6C7B00392D
      39000000000000000000000000000000000000000000000000000E80AA0020A2
      D30036BCEB0097969600E9E2E000B1B2B200ACA2A200DDB6B4009796960065E1
      FD0068CAE6000579A40000000000000000000000000000000000FF000000FF00
      0000FF0000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFF700FFE7
      CE00FFE7CE00B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B58C
      8C00B58C8C00B58C8C00B58C8C0000000000000000000000000000000000493D
      49002B1F2B00C44D0F00E1C49400851E0C0092849300392D3900000000000000
      0000000000000000000000000000000000000000000000000000000000001389
      B4000D8FBE0097969600ECE5E200B1B2B200ACA2A200E0B8B600979696001E9A
      C1002293BA00000000000000000000000000000000000000000000000000FF00
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFF700FFD6
      A500FFD6A500B58C8C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00B58C
      8C00EFB56B00C68C7B0000000000000000000000000000000000000000000000
      00000C4786000C4786000C4786000C4786000C47860000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000097969600E5E3E200AEAFAF00ABA1A100D8B6B600979696000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B0000FFFF007B7B7B0000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFEF
      DE00FFE7D600B58C8C00B58C8C00B58C8C00B58C8C00B58C8C00B58C8C00B58C
      8C00BD848400000000000000000000000000851E0C00851E0C00851E0C00851E
      0C000C4786006ED2FF002AC2FF000CB5FF000C478600851E0C00851E0C00851E
      0C00851E0C00851E0C00851E0C00851E0C000000000000000000000000000000
      00000000000097969600B2B2B200A0A0A00099969600ABA0A000979696000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF00000000000000000000FFFF0000FFFF0000FFFF0000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00EFDEDE00D6C6C600BDADAD00B58473000000
      000000000000000000000000000000000000E1C49400E1C49400E1C49400E1C4
      94000C478600C1E6FF006ED2FF002AC2FF000C478600E1C49400E1C49400E1C4
      9400E1C49400E1C49400E1C49400E1C494000000000000000000000000000000
      00000000000097969600E1E0E000C6C7C700A5A3A300B3A5A500979696000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF0000000000000000007B7B7B0000FFFF007B7B7B0000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B58C8C00B58C8C00B58C8C00B58C8C000000
      000000000000000000000000000000000000C44D0F00C44D0F00C44D0F00C44D
      0F000C478600FFF3FF00C0E6FF006DD2FF000C478600C44D0F00C44D0F00C44D
      0F00C44D0F00C44D0F00C44D0F00C44D0F000000000000000000000000000000
      00000000000097969600E5E4E400EAE9E900BCBCBC00A39E9E00979696000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B58C8C00EFB56B00C68C7B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000C4786000C4786000C4786000C4786000C47860000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000097969600979696009796960097969600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B58C8C00B58C8C00B58C
      8C00B58C8C00B58C8C00B58C8C00B58C8C00BD84840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A6696B00A6696B00A669
      6B00A6696B00A6696B00A6696B00A6696B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A6696B00F3D3A400F0CB
      9700EFC68A00EDC18000EBBB7600A6696B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A7756B00F6DDBA00707B
      CE000F2EF3006E75BF00EEC48400A6696B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BC826800F8E7CE000F30
      F700001EFF000F2FF300F0CC9600A6696B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D1926D00FBF2E2007486
      E7000F30F800727FD700F3D7AB00A6696B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FF000000FF000000FF
      000000FF000000FF000000FF0000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DA9D7500FEFAF300FBF4
      E700FAEEDC00F8E7CF00F6E1C000A6696B000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000000000FF
      000000FF00000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7AB7900FFFFFF00FEFB
      F800FCF7EE00A6696A00A6696A00AC6C5A000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000FF
      000000FF0000000000000000000000000000A4676900A4676900A4676900A467
      6900A4676900A4676900A4676900A467690000000000E7AB7900FFFFFF00FFFF
      FF00FEFCFB00A6696A00C67F4E00000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF000000FF0000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000000000
      000000000000000000000000000000000000A4676900E9C49D00D8A57B00D8A3
      7300D59D6600D1965A00EAB66C00A467690000000000E7AB7900D1926D00D192
      6D002A702700A6696A000000000000000000000000000000FF000000FF000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF000000FF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000A4676900BB7650007D180000821F
      0000811F0000811E0000DCA16200A4676900000000000000000000000000015A
      06002CC558001C96310000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000A7756B00F6E9DD008A2A08007F1A
      00007B160000AB5B3000FCDD9F00A46769000000000000000000005D030030BD
      570041E0750022B63E0008751100000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000000000000000000000000000BC826800FFFFFF00C99379007914
      000089290700E6BD9900FFEAB500A4676900000000002D671800067F14001A92
      2F0027BB4500149A26000C881600036307000000000000000000000000000000
      00000000000000000000000000000000FF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      000000000000000000000000000000000000D1926D00FFFFFF00FBF4F2009941
      1E00BF7D5900FAEDD400D4BCA000A46769000000000000000000000000000155
      030015A5280005700B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000
      000000000000000000000000000000000000DA9D7500FFFFFF00FFFFFF00E7D0
      C400F7EEE300A4676900A4676900A46A5A000000000000000000000000000360
      070009961300036C070000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      000000000000000000000000000000000000E7AB7900FFFFFF00FFFFFF00FFFF
      FF00FCFFFF00A4676900D1864900000000000000000000000000036E07000478
      0A00058C0D000260050000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E7AB7900D1926D00D1926D00D192
      6D00D1926D00A467690000000000000000000266060002660600017707000584
      0C00026606000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000069D
      CC0049B7E00060C1E80054BCE3003DB2DC004DB9E10000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5848400B584
      8400B5848400B5848400B5848400B5848400B5848400B5848400B5848400B584
      8400B5848400B5848400B5848400000000000000000000000000D3D3D3007575
      75007575750075757500AD7376002E2D2D006767670075757500757575007575
      7500757575007575750074747400AAAAAA00000000000000000008A1CE000EA5
      D1007FD2F100A0DFFE0097DAFB0093D9FA007CCDEF0073CAEE0048B7E0000000
      000000000000000000000000000000000000000000000000000000000000069D
      CC0049B7E00060C1E80054BCE3003DB2DC004DB9E10000000000000000000000
      0000000000000000000000000000000000000000000000000000B5848400FFEF
      D600D6DEAD00DEDEAD00F7DEB500EFD6A500EFD69C00F7CE9C00F7CE9400F7CE
      9C00F7CE9C00F7D69C00B5848400000000000000000000000000AFAFAF00FFFF
      FF00FFFFFF00FFFFFF00F1D3D200BE76780067625D00B6B6B600FDFDFD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0074747400000000000000000013A8D4001DAF
      D80083D8EE00A0E3FB0093DAF90082D0F20059BBE0008FD7F7009ADCFB0076CB
      F00000000000000000000000000000000000000000000000000008A1CE000EA5
      D1007FD2F100A0DFFE0097DAFB0093D9FA007CCDEF0073CAEE0048B7E0000000
      0000000000000000000000000000000000000000000000000000B5848400FFEF
      DE00ADCE940042AD39008CBD6B0031A5290031A529007BB55200D6C68C00EFCE
      9400EFCE9400F7D69C00B5848400000000000000000000000000AAAAAA00D5D5
      D500EFEFEF00D4D4D400F1F1F100C2785700CD6600005D4D3D00686868009595
      95009393930097979700F7F7F70075757500000000000000000020AFD8002EB7
      DE0094DFEF00B1EEFA00A2E7FD0064B2D100369BC0006FC7EB0095DAFB009ADC
      FB0034B1DA00000000000000000000000000000000000000000013A8D4001DAF
      D80083D8EE00A0E3FB0093DAF90082D0F20059BBE0008FD7F7009ADCFB0076CB
      F000000000000000000000000000000000000000000000000000B5847300FFF7
      E700CEDEAD0021A51800009C0000009C0000009C0000009C000042AD2900E7CE
      9400EFCE9400F7D69C00B5848400000000000000000000000000AAAAAA00DADA
      DA00CBCBCB00C1C1C100CCCCCC00C7CAC700D26C0D00C9620000685A4C007777
      7700D4D4D40091919100FFFFFF007575750000000000000000002DB5DD003FBE
      E400B1E5F100CFF8FD00B6F4FF0083C9DE00287FA10069BDE00094DBFC0095DA
      FA0033B0D900000000000000000000000000000000000000000020AFD8002EB7
      DE0094DFEF00B1EEFA00A2E7FD0064B2D100369BC0006FC7EB0095DAFB009ADC
      FB0034B1DA000000000000000000000000000000000000000000B5847300FFF7
      EF00CEDEB50021A51800009C000029A52100BDCE8C008CBD6B00089C080094BD
      6300EFCE9C00F7D69C00B584840000000000E3AA9600D090790088645B006C65
      6300A8A8A800C7C7C700FDFDFD00EEEEEE00FBF5F100D3771D00C95F00006758
      47008E8E8E0085858500FEFEFE0075757500000000000000000039BBE30048C1
      E800C5E8F400ECFAFD00CAF5FB00B3ECF50063A9BF008BCFE80099DFFE0094D8
      FA0033B0D90000000000000000000000000000000000000000002DB5DD003FBE
      E400B1E5F100CFF8FD00B6F4FF0083C9DE00287FA10069BDE00094DBFC0095DA
      FA0033B0D9000000000000000000000000000000000000000000BD8C8400FFFF
      F700CEDEBD0010A51000009C0000089C08009CC67B00F7DEBD00BDC68C0084BD
      6B00F7D6A500F7D69C00B584840000000000FFCCB700D1978100DC967D00D195
      7C00FBB89A00D7957E0084767100F1F1F100FFFFFF00EFECDF00D57E2700CC60
      0000BE7D6000373A3A00AAAAAA007373730000000000000000002EB3DB0044C1
      E2005EC0DB006CC8E00062C8E20060C7E2006ECFE70086D8EE009DE2F900A2E1
      FE0034B1DA00000000000000000000000000000000000000000039BBE30048C1
      E800C5E8F400ECFAFD00CAF5FB00B3ECF50063A9BF008BCFE80099DFFE0094D8
      FA0033B0D9000000000000000000000000000000000000000000BD8C8400FFFF
      FF00EFEFDE00B5DEA500ADD69C00ADCE9400ADCE9400F7DEC600F7DEBD00B5D6
      9400F7D6AD00F7D6A500B584840000000000FFD0BC00F0B99E00E5C0A500EED2
      BA00D29A8500F7BA9C00CC9F8C00CDCDCD00D7D7D700FFFFFF00BEA78500EFDE
      E600C55D0400C16265004E50510055555500000000000000000029B3D80075DC
      E700328BA5003A9AB30069E1F00044CCE70021B5D90014AAD3002CAFD60079D0
      ED0037B3DA0000000000000000000000000000000000000000002EB3DB0044C1
      E2005EC0DB006CC8E00062C8E20060C7E2006ECFE70086D8EE009DE2F900A2E1
      FE0034B1DA000000000000000000000000000000000000000000CE9C8400FFFF
      FF00BDE7B500FFF7EF00F7EFDE00B5D69C00ADCE9400ADCE8C00B5CE8C00EFDE
      B500F7DEB500F7DEAD00B584840000000000FFD4C300D19A8600DE9B8100C388
      7300FCC5A500D09F8700C8A18900D3D3D300DFDFDF00DBDBDB00DDDDDD00DAC1
      9800E6984C00CE6100007F696E002E2D2D0000000000000000000000000030B2
      D6004FBBD10074D7E30098FDFF0080F1FA0050CFE60012A1C8000397C30013A4
      CF0018A5D00000000000000000000000000000000000000000002BB1D80058CD
      E40040B8D70046BEDB0045C2DF0037B7D9004AC2E2006BCFEA009AE1F80086D4
      F40025AAD4000000000000000000000000000000000000000000CE9C8400FFFF
      FF008CD68C00C6E7BD00FFF7EF009CCE8C00089C0800009C000010A51000F7DE
      C600F7DEBD00FFDEB500B584840000000000FFD8C700F1BFA300E5C0A500EACF
      BA00D6A48D00FDD7BA00CBA38C00EFEFEF00FFFFFF00FDFDFD00FFFFFF00FFFF
      FF00F5E9DF00E8954700C6640B00B8A4A4000000000000000000000000000000
      0000000000000000000054CFE7004ACAE40045C1DD00129AC3000F98C20011A5
      D00011A2CF00000000000000000000000000000000000000000028B1D7005CCC
      DF000C9BC4003CB6D1008AF5FB0055D7EC0024B6DA00049DCA0025ABD20052C0
      E0001FA7D2000000000000000000000000000000000000000000DEAD8400FFFF
      FF009CD69C00089C080094D68C00C6DEB50031AD2900009C000021A51800F7E7
      CE00F7E7C600F7DEB500B584840000000000FFDED000D09D8800E3A48900C78D
      7800FAC6A5008E899600CDAB9300E5E5E500F7F7F700F0F0F000F4F4F400F6F6
      F600ECECEC00D5D4CE00DF8F2D005B5352000000000000000000000000000000
      00000000000000000000000000000000000000000000109CC5000894C0000000
      0000000000000000000000000000000000000000000000000000000000000C94
      C1000091BF002FAECF0087F0F8007AECF80047C2DC000B96C000119DC4001CA9
      D000000000000000000000000000000000000000000000000000DEAD8400FFFF
      FF00F7FFF70042B54200009C0000009C0000009C0000009C000021A51800FFEF
      DE00E7DEC600C6BDAD00B584840000000000FFE1D500EEC1A500E4C0A400E4CA
      B700D4B09C0085B4FF00CFAB9200C3C3C300CBCBCB00C8C8C800CACACA00CACA
      CA00C7C7C700CBCBCB00F7F7F700757575000000000000000000000000000000
      0000078CB8000587B300000000000000000000000000089AC6000B91BC000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000B99C600199BC300000000003ABFE00020A7CD000A95C0000D96C2000000
      0000000000000000000000000000000000000000000000000000E7B58C00FFFF
      FF00FFFFFF00E7F7E70084CE840039B5390039AD31008CCE840042AD3900AD84
      7300BD847B00BD847B00B584840000000000FFE5DC00EBBD9F00E5B69A00E9BA
      9E00F0CAAE00F1CCB000CFB09B00E6E6E600FAFAFA00F3F3F300F7F7F700FAFA
      FA00EEEEEE00FBFBFB00F2F2F200757575000000000000000000000000000000
      00002EB0DA001E9FCA00000000000000000000000000099CC8000B92BD000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004FC3E6002A9DC6000000000000000000000000000994BF000C8AB5000000
      0000000000000000000000000000000000000000000000000000E7B58C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00EFF7EF00E7F7E700FFFFFF00BDC6AD00A58C
      6B00EFB57300EFA54A00C6846B0000000000FFEDE600E7926500FFA06A00FFA6
      7400FFAD7D00FDAF8200D0AE9B00FCFCFC00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007B7B7B000000000000000000000000000000
      00004CB5D50068D0EA000000000000000000000000000999C5000A92BE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00003BA6C60077DEEE003DAAC80000000000148AB6000C9DC800000000000000
      0000000000000000000000000000000000000000000000000000EFBD9400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7D6D600BD84
      7B00FFC67300CE9473000000000000000000FFEDE700FAD4B400E3A17D00E097
      7200E88C5E00E87E4900DBBFAC00FBFBFB00CFCFCF00CFCFCF00CFCFCF00FFFF
      FF00B3B3B300BEBEBE0082828200EAEAEA000000000000000000000000000000
      0000000000005BC4D90078DFEB004FB9D4003EAED2001DA6D100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004AB5CD007BE0EC006BD1E6004ABCE0001898C300000000000000
      0000000000000000000000000000000000000000000000000000EFBD9400FFF7
      F700FFF7F700FFF7F700FFF7F700FFF7F700FFF7F700FFF7F700E7D6CE00BD84
      7B00CE9C8400000000000000000000000000EEE0C900FFF0CE00FFFDD800FFF6
      CF00FFF7D100FFFAD500E4CBB000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F1F1F1009F9F9F00F0F0F000000000000000000000000000000000000000
      0000000000000000000055BFD40055C0D6004FBAD80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFBD9400DEAD
      8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400BD84
      7B00000000000000000000000000000000000000000000000000ACABAC00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0094949400F4F4F40000000000000000000000000000000000000000000000
      00000000000000000000000000009A6666006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B5B5B5000000000000000000000000009C9C9C009C9C
      9C00A5A5A5000000000000000000000000000000000000000000000000000000
      00009A6666009A666600B9666600BB6868006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000008780E0008780E0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5B5B500B5B5B500DEDEDE00ADADAD00525252006B6B6B00ADADAD00D6D6
      D600E7DEDE009C9C9C00000000000000000000000000000000009A6666009A66
      6600C66A6B00D06A6B00D2686900C3686900693334009A6666009A6666009A66
      66009A6666009A6666009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000DA31B0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5B5B500B5B5
      B500FFFFFF00FFFFFF00E7DEDE00B5B5B5005A5A630031313100313131004A4A
      4A00848484009C9C9C00A5A5A5000000000000000000000000009A666600DE73
      7400D7707100D56F7000D56D6E00C76A6D0069333400FEA2A300FCAFB000FABC
      BD00F9C5C600F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000EAA1D0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A5A5A500B5B5B500F7F7F700FFFF
      FF00F7F7F700D6D6D600B5B5B500A5A5A500ADADAD009C9C9C007B7B7B005252
      520031313900313131008C8C8C000000000000000000000000009A666600E077
      7800DB757600DA747500DA727300CC6E71006933340039C5650025CF630029CC
      630019CB5B00F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000EA81C0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B5ADAD00EFEFEF00EFEFEF00CECE
      CE00BDB5B500C6C6C600D6CECE00B5B5B500ADADAD00ADA5A500ADADAD00ADAD
      AD00A5A5A500848484009C9C9C000000000000000000000000009A666600E57D
      7E00E07A7B00DF797A00DF777800D07275006933340042C4680030CD670033CB
      670024CB6000F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70010AA1F0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADA5AD00BDBDBD00B5B5B500C6C6
      C600D6D6D600DEDEDE00F7F7F700F7F7F700E7E7E700D6D6D600BDBDBD00ADAD
      AD00ADA5A500ADADAD00A5A5A5000000000000000000000000009A666600EA82
      8300E57F8000E37D7E00E6808100D3747600693334003DC2640029CB63002FCA
      640020CA5E00F9C5C6009A66660000000000000000000000000008780E000878
      0E0008780E0008780E0008780E0076F9A70019B02C0008780E0008780E000878
      0E0008780E0008780E0000000000000000000000000000000000000000000104
      A2000104A2000104A2000104A2000104A2000104A2000104A2000104A2000104
      A2000104A200000000000000000000000000A5A5A500C6BDC600D6D6D600DEDE
      DE00D6D6D600EFEFEF00DEDEDE00B5BDB500CECECE00D6DEDE00DEDEDE00DEDE
      DE00D6D6D600C6C6C600ADADAD000000000000000000000000009A666600F087
      8800E9818200EC969700FBDDDE00D8888A0069333400B8E1AC006BDC89005DD5
      800046D47300F9C5C6009A666600000000000000000008780E0076F9A70055E3
      830049DA720042D3680037C856002AB9430022B337001CB2300016AF27000FA8
      1D000EA91B000DA21B0008780E000000000000000000000000000104A2005983
      FF000026FF000030FF000030FB00002FF200002FE900002EE1000030D8000031
      D0000034CB000104A200000000000000000000000000B5ADAD00DEDEDE00DEDE
      DE00E7E7E700D6D6D600C6C6C600BDDEBD00CED6CE00D6BDB500BDB5B500BDBD
      BD00C6C6C600D6CECE00BDBDBD000000000000000000000000009A666600F58C
      8D00EE868700F0999A00FDDCDD00DA888A0069333400FFF5D800FFFFE000FFFF
      DE00ECFDD400F9C5C6009A666600000000000000000008780E0076F9A70076F9
      A70076F9A70076F9A70076F9A70076F9A7002CBB480076F9A70076F9A70076F9
      A70076F9A70076F9A70008780E000000000000000000000000000104A200ABC2
      FF006480FF006688FF006688FF006687FA006787F5006787F0005779E9004D70
      E4004D74E2000104A20000000000000000000000000000000000B5ADAD00CECE
      CE00C6BDBD00C6C6C600EFEFEF00FFF7F700F7F7F700F7EFE700E7E7E700DEDE
      DE00C6C6C600ADA5A500000000000000000000000000000000009A666600FA91
      9200F48E8F00F28B8C00F48C8D00DC7F800069333400FDF3D400FFFFDF00FFFF
      DD00FFFFE000F9C5C6009A66660000000000000000000000000008780E000878
      0E0008780E0008780E0008780E0076F9A7003CCB5D0008780E0008780E000878
      0E0008780E0008780E0000000000000000000000000000000000000000000104
      A2000104A2000104A2000104A2000104A2000104A2000104A2000104A2000104
      A2000104A200000000000000000000000000000000000000000000000000B5AD
      AD00E7E7E700D6D6D600B5B5B500CECECE00DEDEE700DEE7E700DEDEDE00D6D6
      D600C6BDBD0000000000000000000000000000000000000000009A666600FE97
      9800F9939400F8929300F9909200E085850069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70049D9720008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFEFEF00FFE7DE00EFD6CE00EFD6CE00EFDED600E7DEDE00DEDEDE00ADAD
      AD000000000000000000000000000000000000000000000000009A666600FF9B
      9C00FD979800FC969700FE979800E388890069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70055E2820008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      00000000000000000000000000000000000000000000000000009A666600FF9F
      A000FF9A9B00FF999A00FF9A9B00E78C8D0069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70063F0970008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      00000000000000000000000000000000000000000000000000009A6666009A66
      6600E98E8F00FE999A00FF9D9E00EB8F900069333400FBF0D200FDFCDC00FDFC
      DA00FDFCDC00F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70076F9A70008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009A666600B0717200D7868700DA888800693334009A6666009A6666009A66
      66009A6666009A6666009A666600000000000000000000000000000000000000
      000000000000000000000000000008780E0008780E0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEB5
      B500FFE7DE00FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009A6666009A6666006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEB5
      B500DEB5B500DEB5B500DEB5B500F7BDB500F7BDB50000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000500000000100010000000000800200000000000000000000
      000000000000000000000000FFFFFF00E001000000000000C001000000000000
      8001000000000000800100000000000080010000000000008001000000000000
      8001000000000000800100000000000080010000000000008003000000000000
      8003000000000000800300000000000080030000000000008003000000000000
      C007000000000000E00F000000000000F87FFE3FFE7FF801F00FE007FE1FF801
      E001E007FC07F801C0008003FC01F80180008001F800800100008001F8008001
      000080010000800100018001000080010003800100018001000FC00300328001
      E03FE007003E8003F07FF81F003E80070000F81F003E801F0000F81F001D801F
      0000F81F0023803FF07FFC3F003F807FFF80FFFFFFFFFFC3FF80FFFFFFFFFFC3
      FF80FFFFFFFFC000FF80FFFFFF3FC000FF80FCFFFC3FC000FF80FC3FF03FC000
      FF80FC0FC000C003008100030000C00300830000C000C00700E30003F03FC007
      00C1FC0FFC3FC0070080FC3FFF3FC00700E3FCFFFFFFC00F00E3FFFFFFFFC01F
      01C3FFFFFFFFC03F0307FFFFFFFFFFFFE07FFFFFC001C000C01FE07FC001C000
      C00FC01FC001C000C007C00FC001C000C007C007C0010000C007C007C0010000
      C007C007C0010000C007C007C0010000E007C007C0010000FC07C007C0010000
      FF9FE00FC0010000F39FF21FC0010000F39FF39FC0010000F39FF13FC0030000
      F83FF83FC0070001FC7FFFFFC00FC003FE7FFFFFFFFFFDC7F07FFE7FFFFFF003
      C001FC3FFFFFC001C001FC3FFFFF0001C001FC3FFFFF0001C001FC3FFFFF0001
      C001C003E0070001C0018001C0038001C0018001C003C003C001C003E007E007
      C001FC3FFFFFF00FC001FC3FFFFFF03FC001FC3FFFFFF03FC001FC3FFFFFF03F
      F001FE7FFFFFE03FFC7FFFFFFFFFE07F00000000000000000000000000000000
      000000000000}
  end
  inherited fmstr: TFormStorage
    Options = [fpState]
    StoredProps.Strings = (
      'plSemis.Width'
      'plWOrder.Height'
      'plMain.Height')
    Left = 20
    Top = 268
  end
  object taPIt_TQ: TpFIBDataSet
    SelectSQL.Strings = (
      'select sum(it.INQ) INQ,  '
      '          sum(it.GETQ) GETQ, '
      '          sum(it.REWORKQ) REWORKQ,'
      '          sum(it.DONEQ) DONEQ, '
      '          sum(it.RETQ) RETQ, '
      '          sum(it.REJQ) REJQ,'
      '          sum(it.NKQ) NKQ,'
      '          sum(it.IQ) IQ,'
      '          sum(it.FQ) FQ,'
      '          sum(it.RQ) RQ '
      'from AIt_InSemis it,  D_Semis s'
      'where it.PROTOCOLID = :ID and'
      '           it.SEMISID = s.SEMISID and'
      '           s.GOOD = 1')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taPIt_TQBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 68
    Top = 316
    object taPIt_TQINQ: TFIBBCDField
      FieldName = 'INQ'
      Size = 0
      RoundByScale = True
    end
    object taPIt_TQGETQ: TFIBBCDField
      FieldName = 'GETQ'
      Size = 0
      RoundByScale = True
    end
    object taPIt_TQREWORKQ: TFIBBCDField
      FieldName = 'REWORKQ'
      Size = 0
      RoundByScale = True
    end
    object taPIt_TQDONEQ: TFIBBCDField
      FieldName = 'DONEQ'
      Size = 0
      RoundByScale = True
    end
    object taPIt_TQRETQ: TFIBBCDField
      FieldName = 'RETQ'
      Size = 0
      RoundByScale = True
    end
    object taPIt_TQREJQ: TFIBBCDField
      FieldName = 'REJQ'
      Size = 0
      RoundByScale = True
    end
    object taPIt_TQNKQ: TFIBBCDField
      FieldName = 'NKQ'
      Size = 0
      RoundByScale = True
    end
    object taPIt_TQIQ: TFIBBCDField
      FieldName = 'IQ'
      Size = 0
      RoundByScale = True
    end
    object taPIt_TQFQ: TFIBBCDField
      FieldName = 'FQ'
      Size = 0
      RoundByScale = True
    end
    object taPIt_TQRQ: TFIBBCDField
      FieldName = 'RQ'
      Size = 0
      RoundByScale = True
    end
  end
  object taPIt_TW: TpFIBDataSet
    SelectSQL.Strings = (
      'select sum(it.INW*s.Q0) INW,  '
      '          sum(it.GETW*s.Q0) GETW, '
      '          sum(it.REWORKW*s.Q0) REWORKW,'
      '          sum(it.DONEW*s.Q0) DONEW, '
      '          sum(it.RETW*s.Q0) RETW, '
      '          sum(it.REJW*s.Q0) REJW,'
      '          sum(it.NKW*s.Q0) NKW,'
      '          sum(it.IW*s.Q0) IW,'
      '          sum(it.FW*s.Q0) FW,'
      '          sum(it.RW*s.Q0) RW '
      'from AIt_InSemis it,  D_Semis s'
      'where it.PROTOCOLID = :ID and'
      '           it.SEMISID = s.SEMISID and'
      '           s.MAT = '#39'1'#39
      '           ')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taPIt_TWBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 68
    Top = 360
    object taPIt_TWINW: TFloatField
      FieldName = 'INW'
    end
    object taPIt_TWGETW: TFloatField
      FieldName = 'GETW'
    end
    object taPIt_TWREWORKW: TFloatField
      FieldName = 'REWORKW'
    end
    object taPIt_TWDONEW: TFloatField
      FieldName = 'DONEW'
    end
    object taPIt_TWRETW: TFloatField
      FieldName = 'RETW'
    end
    object taPIt_TWREJW: TFloatField
      FieldName = 'REJW'
    end
    object taPIt_TWNKW: TFloatField
      FieldName = 'NKW'
    end
    object taPIt_TWIW: TFloatField
      FieldName = 'IW'
    end
    object taPIt_TWFW: TFloatField
      FieldName = 'FW'
    end
    object taPIt_TWRW: TFloatField
      FieldName = 'RW'
    end
  end
  object acEvent: TActionList
    Images = ilButtons
    Left = 124
    Top = 316
    object acAddSemis: TAction
      Category = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      OnExecute = acAddSemisExecute
      OnUpdate = acAddSemisUpdate
    end
    object acAddNewToWOrder: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = '  '#1050' '#1085#1086#1074#1086#1081' '
      ImageIndex = 11
      OnExecute = acAddNewToWOrderExecute
      OnUpdate = acAddNewToWOrderUpdate
    end
    object acAddToWOrder: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 9
      OnExecute = acAddToWOrderExecute
      OnUpdate = acAddToWOrderUpdate
    end
    object acDelSemis: TAction
      Category = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      OnExecute = acDelSemisExecute
      OnUpdate = acDelSemisUpdate
    end
    object acAddArt: TAction
      Category = #1044#1074#1080#1078#1077#1085#1080#1077' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
      OnExecute = acAddArtExecute
      OnUpdate = acAddArtUpdate
    end
    object acDelArt: TAction
      Category = #1044#1074#1080#1078#1077#1085#1080#1077' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
      OnExecute = acDelArtExecute
      OnUpdate = acDelArtUpdate
    end
    object acCreateProtocol: TAction
      Category = #1055#1088#1086#1090#1086#1082#1086#1083
      Caption = #1057#1086#1079#1076#1072#1090#1100
      ImageIndex = 7
      OnExecute = acCreateProtocolExecute
      OnUpdate = acCreateProtocolUpdate
    end
    object acCloseProtocol: TAction
      Category = #1055#1088#1086#1090#1086#1082#1086#1083
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 4
      Visible = False
      OnExecute = acCloseProtocolExecute
      OnUpdate = acCloseProtocolUpdate
    end
    object acDelProtocol: TAction
      Category = #1055#1088#1086#1090#1086#1082#1086#1083
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = acDelProtocolExecute
      OnUpdate = acDelProtocolUpdate
    end
    object acRefreshProtocol: TAction
      Category = #1055#1088#1086#1090#1086#1082#1086#1083
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      ImageIndex = 6
      OnExecute = acRefreshProtocolExecute
      OnUpdate = acRefreshProtocolUpdate
    end
    object acDetailSemis: TAction
      Category = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
      Caption = #1055#1086#1076#1088#1086#1073#1085#1086
      OnExecute = acDetailSemisExecute
    end
    object acDetailArt: TAction
      Category = #1044#1074#1080#1078#1077#1085#1080#1077' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = #1055#1086#1076#1088#1086#1073#1085#1086
      OnExecute = acDetailArtExecute
    end
    object acSemisInv: TAction
      Category = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086' '#1085#1072#1082#1083#1072#1076#1085#1099#1084
      OnExecute = acSemisInvExecute
    end
    object acZeroArt: TAction
      Category = #1044#1074#1080#1078#1077#1085#1080#1077' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = #1054#1073#1085#1091#1083#1080#1090#1100
      OnExecute = acZeroArtExecute
      OnUpdate = acZeroArtUpdate
    end
    object acZeroSemis: TAction
      Category = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
      Caption = #1054#1073#1085#1091#1083#1080#1090#1100
      OnExecute = acZeroSemisExecute
      OnUpdate = acZeroSemisUpdate
    end
    object acCopySemis: TAction
      Category = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 8
      OnExecute = acCopySemisExecute
      OnUpdate = acCopySemisUpdate
    end
    object acCopyArt: TAction
      Category = #1044#1074#1080#1078#1077#1085#1080#1077' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 8
      OnExecute = acCopyArtExecute
      OnUpdate = acCopyArtUpdate
    end
    object acPrintProtocol: TAction
      Category = #1055#1088#1086#1090#1086#1082#1086#1083
      Caption = #1054#1087#1080#1089#1100
      ImageIndex = 3
      OnExecute = acPrintProtocolExecute
      OnUpdate = acPrintProtocolUpdate
    end
    object acDelFromWOrder: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = ' '#1059#1076#1072#1083#1080#1090#1100' '
      ImageIndex = 10
      OnExecute = acDelFromWOrderExecute
      OnUpdate = acDelFromWOrderUpdate
    end
    object acCreateWOrder: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1085#1072#1088#1103#1076
      ImageIndex = 13
      OnExecute = acCreateWOrderExecute
      OnUpdate = acCreateWOrderUpdate
    end
    object acItType0: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = #1042#1099#1076#1072#1095#1072
      ShortCut = 32817
      OnExecute = acItType0Execute
      OnUpdate = acItType0Update
    end
    object acItType3: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = #1042#1086#1079#1074#1088#1072#1090
      ShortCut = 32818
      OnExecute = acItType3Execute
      OnUpdate = acItType3Update
    end
    object acPrintInv: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = #1055#1077#1095#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      ImageIndex = 3
      OnExecute = acPrintInvExecute
      OnUpdate = acPrintInvUpdate
    end
    object acPrintInvWithAssort: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = #1055#1077#1095#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1089' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1086#1084
      ImageIndex = 3
      OnExecute = acPrintInvWithAssortExecute
      OnUpdate = acPrintInvWithAssortUpdate
    end
    object acAssortiment: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      ImageIndex = 14
      ShortCut = 16449
      OnExecute = acAssortimentExecute
      OnUpdate = acAssortimentUpdate
    end
    object acCloseWOrder: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1085#1072#1088#1103#1076
      ImageIndex = 5
      OnExecute = acCloseWOrderExecute
      OnUpdate = acCloseWOrderUpdate
    end
    object acOpenDetail: TAction
      Category = #1055#1088#1086#1090#1086#1082#1086#1083
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1076#1072#1085#1085#1099#1077
      ImageIndex = 12
      ShortCut = 114
      OnExecute = acOpenDetailExecute
      OnUpdate = acOpenDetailUpdate
    end
    object acAddNew: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = 'acAddNew'
      OnExecute = acAddNewExecute
    end
    object acAddItem: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = 'acAddItem'
      OnExecute = acAddItemExecute
    end
    object acACopy: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      ImageIndex = 15
      OnExecute = acACopyExecute
      OnUpdate = acACopyUpdate
    end
    object acAPaste: TAction
      Category = #1053#1072#1088#1103#1076
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      ImageIndex = 16
      OnExecute = acAPasteExecute
      OnUpdate = acAPasteUpdate
    end
    object acPrintResult: TAction
      Category = #1055#1088#1086#1090#1086#1082#1086#1083
      Caption = #1054#1089#1090#1072#1090#1086#1082
      ImageIndex = 3
      OnExecute = acPrintResultExecute
    end
  end
  object ppSemis: TTBPopupMenu
    Images = ilButtons
    Left = 180
    Top = 316
    object TBItem7: TTBItem
      Action = acAddSemis
    end
    object TBItem6: TTBItem
      Action = acDelSemis
    end
    object TBItem11: TTBItem
      Action = acSemisInv
    end
    object TBSeparatorItem1: TTBSeparatorItem
    end
    object TBItem5: TTBItem
      Action = acDetailSemis
    end
  end
  object ppArt: TTBPopupMenu
    Images = ilButtons
    Left = 656
    Top = 344
    object TBItem10: TTBItem
      Action = acAddArt
    end
    object TBItem9: TTBItem
      Action = acDelArt
    end
    object TBSeparatorItem2: TTBSeparatorItem
    end
    object TBItem8: TTBItem
      Action = acDetailArt
    end
  end
  object ppInv: TPopupMenu
    Images = ilButtons
    Left = 428
    Top = 504
    object mnitAddToInv: TMenuItem
      Action = acAddToWOrder
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object mnitAddToNew: TMenuItem
      Action = acAddNewToWOrder
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082' '#1085#1086#1074#1086#1081' '#1085#1072#1082#1083'.'
    end
    object mnitDelItem: TMenuItem
      Action = acDelFromWOrder
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object mnitPrint: TMenuItem
      Tag = 1
      Action = acPrintInv
    end
    object N7: TMenuItem
      Action = acPrintInvWithAssort
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Action = acAssortiment
    end
    object N4: TMenuItem
      Action = acACopy
    end
    object N5: TMenuItem
      Action = acAPaste
    end
  end
end
