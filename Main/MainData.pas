unit MainData;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, FIBDataSet, pFIBDataSet, Menus, pFIBQuery, pFIBDatabase, Variants,
  dbTree, FR_DSet, FR_DBSet, ActnList, MsgDialog, FIBQuery, UtilLib, ScaleData;

const
  ErrorResId = 60000;
  SelfId = 1;

type
  TplKind = (plOper, plSemis, plUnit);
  TWorderMode = (wmWOrder, wmRej);
  TAffinajMode = (amS, amL);
  TDetailWhMode = (dwmI, dwmO, dwmDI, dwmDO, dwmPS, dwmRI, dwmRO);


  TdmMain = class(TDataModule)
    taWOrderList: TpFIBDataSet;
    dsrWOrderList: TDataSource;
    taWOrderListWORDERID: TIntegerField;
    taWOrderListDOCNO: TIntegerField;
    taWOrderListMOLID: TIntegerField;
    taWOrderListFIO: TFIBStringField;
    taWOrder: TpFIBDataSet;
    dsrWOrder: TDataSource;
    quTmp: TpFIBQuery;
    taWOrderWOITEMID: TIntegerField;
    taWOrderWORDERID: TIntegerField;
    taWOrderOPERID: TFIBStringField;
    taWOrderSEMIS: TFIBStringField;
    taWOrderITDATE: TDateTimeField;
    taWOrderITTYPE: TIntegerField;
    taWOrderRecNo: TIntegerField;
    taWOrderQ: TFloatField;
    taWOrderW: TFloatField;
    taWOrderDOCNO: TIntegerField;
    taWOrderDOCDATE: TDateTimeField;
    taWOrderINVID: TIntegerField;
    taWOrderListISCLOSE: TSmallintField;
    taWOrderListJOBID: TIntegerField;
    taWOrderListJOBFACE: TFIBStringField;
    taWOrderListDEPID: TIntegerField;
    taWOrderListDEPNAME: TFIBStringField;
    taWOrderUSERID: TIntegerField;
    taWOrderUserName: TStringField;
    quBuffer: TpFIBQuery;
    quWOrder_T: TpFIBDataSet;
    dsrWOrder_T: TDataSource;
    taPList: TpFIBDataSet;
    taProtocol: TpFIBDataSet;
    dsrPList: TDataSource;
    dsrProtocol: TDataSource;
    taPListID: TIntegerField;
    taPListDOCNO: TIntegerField;
    taPListDOCDATE: TDateTimeField;
    taPListUSERID: TIntegerField;
    taPListUSERNAME: TStringField;
    taPListRecNo: TIntegerField;
    taProtocolID: TIntegerField;
    taProtocolACTID: TIntegerField;
    taProtocolRecNo: TIntegerField;
    taProtocolF: TFloatField;
    taProtocolN: TFloatField;
    taProtocolE: TFloatField;
    taProtocolS: TFloatField;
    taProtocolI: TFloatField;
    taProtocolH: TFloatField;
    taProtocolWZ: TFloatField;
    taProtocolOUT: TFloatField;
    taPListBD: TDateTimeField;
    taWOrderE: TSmallintField;
    taZList: TpFIBDataSet;
    taZListID: TIntegerField;
    taZListDOCNO: TIntegerField;
    taZListDOCDATE: TDateTimeField;
    taZListUSERID: TIntegerField;
    dsrZList: TDataSource;
    taZListRecNo: TIntegerField;
    taZItem: TpFIBDataSet;
    dsrZItem: TDataSource;
    taZItemID: TIntegerField;
    taZItemOPER: TFIBStringField;
    taZItemZP: TFloatField;
    taZItemU: TFloatField;
    taZItemNR: TFloatField;
    taZItemDONE: TFloatField;
    taZItemE: TFloatField;
    taZItemS: TFloatField;
    taZItemZPID: TIntegerField;
    taZItemOPERNAME: TStringField;
    taProtocolOUT1: TFloatField;
    taZListBD: TDateTimeField;
    taZItemTZP: TFloatField;
    taZListUSERNAME: TStringField;
    taWOrderListDEFOPER: TFIBStringField;
    taWOrderListDEFOPERNAME: TFIBStringField;
    taMol: TpFIBDataSet;
    taMolIO: TpFIBDataSet;
    taWOrderOPERNAME: TStringField;
    quWOrder_TGET: TFloatField;
    quWOrder_TDONE: TFloatField;
    quWOrder_TRET: TFloatField;
    quWOrder_TREJ: TFloatField;
    quWOrder_TREWORK: TFloatField;
    quWOrder_TF: TFloatField;
    quWOrder_TSTONEQ: TFloatField;
    quWOrder_TSTONEW: TFloatField;
    taWOrderDEPID: TIntegerField;
    taWOrderJOBDEPID: TIntegerField;
    taWOrderDEPNAME: TStringField;
    taWOrderJOBDEPNAME: TStringField;
    taWOrderListJOBDEPID: TIntegerField;
    taPSMol: TpFIBDataSet;
    taPsDep: TpFIBDataSet;
    taPsDepDEPID: TIntegerField;
    taPsDepPARENT: TIntegerField;
    taPsDepCHILD: TIntegerField;
    taPsDepCOLOR: TIntegerField;
    taPsDepSNAME: TFIBStringField;
    taPsDepNAME: TFIBStringField;
    taPsDepSORTIND: TIntegerField;
    dsrPsDep: TDataSource;
    dsrPsMol: TDataSource;
    taWOrderSEMISNAME: TStringField;
    taPSMolMOLID: TIntegerField;
    taPSMolFIO: TFIBStringField;
    taPSMolDEPID: TIntegerField;
    taWOrderJOBNAME: TStringField;
    taWOrderJOBID: TIntegerField;
    taZItemPSID: TIntegerField;
    taZItemNAME: TFIBStringField;
    taProtocolPSID: TIntegerField;
    taProtocolOPERID: TFIBStringField;
    taProtocolPSNAME: TFIBStringField;
    taProtocolOPERNAME: TStringField;
    taPListISCLOSE: TSmallintField;
    taZItemPSNAME: TStringField;
    taZItemDOC: TSmallintField;
    taZItemNR2: TFloatField;
    taZItemNP: TFloatField;
    taWOrderAINV: TIntegerField;
    taWOrderREF: TIntegerField;
    taProtocolOUT_PS: TFloatField;
    taProtocolOUT_PS1: TFloatField;
    taWOrderSEMISNAME1: TFIBStringField;
    taStoneSemis: TpFIBDataSet;
    dsStoneSemis: TDataSource;
    quWOrder_TBRW: TFloatField;
    quWOrder_TBRQ: TFloatField;
    taErrWOrder: TpFIBDataSet;
    dsrErrWOrder: TDataSource;
    taErrWOrderINV_ID: TIntegerField;
    taErrWOrderINV_DOCNO: TIntegerField;
    taErrWOrderINV_DATE: TDateTimeField;
    taErrWOrderINV_WORDERID: TIntegerField;
    taErrWOrderINV_WORDERDOCNO: TIntegerField;
    taErrWOrderINV_WORDERDOCDATE: TDateTimeField;
    taErrWOrderIT_ID: TIntegerField;
    taErrWOrderIT_ITDATE: TDateTimeField;
    taErrWOrderIT_WORDERID: TIntegerField;
    taErrWOrderIT_WORDERDOCNO: TIntegerField;
    taErrWOrderIT_WORDERDOCDATE: TDateTimeField;
    taErrWOrderIT_SEMIS: TFIBStringField;
    taErrWOrderIT_W: TFloatField;
    taPsOper: TpFIBDataSet;
    dsrPsOper: TDataSource;
    taPsOperID: TIntegerField;
    taPsOperOPERID: TFIBStringField;
    taPsOperDEPID: TIntegerField;
    taPsOperOPERATION: TFIBStringField;
    dsrSIList: TDataSource;
    taSIList: TpFIBDataSet;
    taSIListINVID: TIntegerField;
    taSIListDOCNO: TIntegerField;
    taSIListDOCDATE: TDateTimeField;
    taSIListITYPE: TSmallintField;
    taSIListDEPID: TIntegerField;
    taSIListUSERID: TIntegerField;
    taSIListSUPID: TIntegerField;
    taSIListSUPNAME: TFIBStringField;
    taSIListFIO: TFIBStringField;
    taSIListISCLOSE: TSmallintField;
    taSIListDEPNAME: TFIBStringField;
    taSIListNDSID: TIntegerField;
    taSIListREF: TIntegerField;
    taSIListQ: TIntegerField;
    taSIEl: TpFIBDataSet;
    dsrSIEl: TDataSource;
    taTmp: TpFIBDataSet;
    taSIElID: TIntegerField;
    taSIElINVID: TIntegerField;
    taSIElSEMIS: TFIBStringField;
    taSIElQ: TFloatField;
    taSIElW: TFloatField;
    taSIElTPRICE: TFloatField;
    taSIElPRICE: TFloatField;
    taSIElNDS: TFloatField;
    taSIElSEMISNAME: TStringField;
    taSIElUQ: TStringField;
    taSIElNDSID: TIntegerField;
    taSIElUW: TStringField;
    taSIElRecNo: TIntegerField;
    taSIElT: TSmallintField;
    taWhSemis: TpFIBDataSet;
    dsrWhSemis: TDataSource;
    taWhSemisDEPID: TIntegerField;
    taWhSemisSEMISID: TFIBStringField;
    taWhSemisW: TFloatField;
    taWhSemisUQ: TSmallintField;
    taWhSemisUW: TSmallintField;
    taWhSemisSEMISNAME: TFIBStringField;
    taWhSemisQ: TFloatField;
    taSIElCOST: TFloatField;
    taSIListSSF: TFIBStringField;
    taSIListSSFDT: TDateTimeField;
    taSIListW: TFloatField;
    taSIListCOST: TFloatField;
    taErrWhArt: TpFIBDataSet;
    dsrErrWhArt: TDataSource;
    taErrWhArtPSID: TIntegerField;
    taErrWhArtOPERID: TFIBStringField;
    taErrWhArtART2ID: TIntegerField;
    taErrWhArtART: TFIBStringField;
    taErrWhArtART2: TFIBStringField;
    taErrWhArtSZID: TIntegerField;
    taErrWhArtRQ: TIntegerField;
    taErrWhArtFQ: TIntegerField;
    taErrWhArtU: TSmallintField;
    taErrWhArtWHID: TIntegerField;
    taErrWhArtNAME: TFIBStringField;
    taSIElOPERID: TFIBStringField;
    taSIElOPERNAME: TStringField;
    taASItem: TpFIBDataSet;
    taASItemRecNo: TIntegerField;
    taASItemID: TIntegerField;
    taASItemSELID: TIntegerField;
    taASItemARTID: TIntegerField;
    taASItemART: TFIBStringField;
    taASItemART2ID: TIntegerField;
    taASItemART2: TFIBStringField;
    taASItemOPERID: TFIBStringField;
    taASItemOPERNAME: TFIBStringField;
    taASItemUID: TIntegerField;
    taASItemW: TFloatField;
    taASItemSZID: TIntegerField;
    taASItemSZNAME: TFIBStringField;
    taASItemU: TSmallintField;
    taASItemP0: TSmallintField;
    taASItemINVID: TIntegerField;
    taASItemPRICE: TFloatField;
    taASItemTPRICE: TFloatField;
    taASItemUA: TSmallintField;
    dsrASItem: TDataSource;
    taASList: TpFIBDataSet;
    taASListINVID: TIntegerField;
    taASListDOCNO: TIntegerField;
    taASListDOCDATE: TDateTimeField;
    taASListITYPE: TSmallintField;
    taASListDEPID: TIntegerField;
    taASListUSERID: TIntegerField;
    taASListFIO: TFIBStringField;
    taASListISCLOSE: TSmallintField;
    taASListDEPNAME: TFIBStringField;
    taASListFROMDEPID: TIntegerField;
    taASListFROMDEPNAME: TFIBStringField;
    taASListNDSID: TIntegerField;
    taASListREF: TIntegerField;
    taASListQ: TIntegerField;
    dsrASList: TDataSource;
    taASListW: TFloatField;
    taASListCOST: TFloatField;
    taASItemCOST: TFloatField;
    taErrWhSemis: TpFIBDataSet;
    dsrErrWhSemis: TDataSource;
    taErrWhSemisDEPID: TIntegerField;
    taErrWhSemisDEPNAME: TFIBStringField;
    taErrWhSemisSEMISID: TFIBStringField;
    taErrWhSemisSEMISNAME: TFIBStringField;
    taErrWhSemisOPERID: TFIBStringField;
    taErrWhSemisOPERNAME: TFIBStringField;
    taErrWhSemisFQ: TIntegerField;
    taErrWhSemisFW: TFloatField;
    taErrWhSemisRQ: TIntegerField;
    taErrWhSemisRW: TFloatField;
    taErrWhSemisUQ: TSmallintField;
    taErrWhSemisUW: TSmallintField;
    taErrSInvProd: TpFIBDataSet;
    dsrErrSinvProd: TDataSource;
    taErrSInvProdINVID: TIntegerField;
    taErrSInvProdSELID: TIntegerField;
    taErrSInvProdITEMID: TIntegerField;
    taErrSInvProdARTID: TIntegerField;
    taErrSInvProdART: TIntegerField;
    taErrSInvProdART2ID: TFIBStringField;
    taErrSInvProdART2: TFIBStringField;
    taErrSInvProdUID: TIntegerField;
    taErrSInvProdUA: TSmallintField;
    taErrSInvProdOPERID: TFIBStringField;
    taWhArt: TpFIBDataSet;
    dsrWhArt: TDataSource;
    taWhArtID: TIntegerField;
    taWhArtPSID: TIntegerField;
    taWhArtOPERID: TFIBStringField;
    taWhArtARTID: TIntegerField;
    taWhArtART2ID: TIntegerField;
    taWhArtSZID: TIntegerField;
    taWhArtT: TSmallintField;
    taWhArtQ: TIntegerField;
    taWhArtART2: TFIBStringField;
    taWhArtART: TFIBStringField;
    taWhArtSZ: TFIBStringField;
    taWhArtOPERNAME: TFIBStringField;
    taWhArtU: TSmallintField;
    taWhArtPSNAME: TFIBStringField;
    taWhS: TpFIBDataSet;
    dsrWhS: TDataSource;
    taWhSID: TIntegerField;
    taWhSDEPID: TIntegerField;
    taWhSDEPNAME: TFIBStringField;
    taWhSSEMISID: TFIBStringField;
    taWhSSEMISNAME: TFIBStringField;
    taWhSMATID: TFIBStringField;
    taWhSMATNAME: TFIBStringField;
    taWhSOPERID: TFIBStringField;
    taWhSOPERNAME: TFIBStringField;
    taWhSQ: TFloatField;
    taWhSW: TFloatField;
    taWhSUQ: TSmallintField;
    taWhSUW: TSmallintField;
    taASItemUA_OLD: TSmallintField;
    taPIt_InSemis_d: TpFIBDataSet;
    dsrPIt_InSemis_d: TDataSource;
    taPListT: TSmallintField;
    taPListMATNAME: TStringField;
    taProtocol_d: TpFIBDataSet;
    taPListMATID: TFIBStringField;
    dsrProtocol_d: TDataSource;
    taProtocol_dID: TIntegerField;
    taProtocol_dACTID: TIntegerField;
    taProtocol_dPSID: TIntegerField;
    taProtocol_dOPERID: TFIBStringField;
    taProtocol_dF: TFloatField;
    taProtocol_dN: TFloatField;
    taProtocol_dE: TFloatField;
    taProtocol_dS: TFloatField;
    taProtocol_dI: TFloatField;
    taProtocol_dH: TFloatField;
    taProtocol_dPSNAME: TFIBStringField;
    taPIt_InSemis_dID: TIntegerField;
    taPIt_InSemis_dPROTOCOLID: TIntegerField;
    taPIt_InSemis_dPSID: TIntegerField;
    taPIt_InSemis_dPSNAME: TFIBStringField;
    taPIt_InSemis_dOPERID: TFIBStringField;
    taPIt_InSemis_dOPERNAME: TFIBStringField;
    taPIt_InSemis_dSEMISID: TFIBStringField;
    taPIt_InSemis_dSEMISNAME: TFIBStringField;
    taPIt_InSemis_dFQ: TIntegerField;
    taPIt_InSemis_dFW: TFloatField;
    taDIList: TpFIBDataSet;
    taDIEl: TpFIBDataSet;
    dsrDIList: TDataSource;
    dsrDIEl: TDataSource;
    taDIListINVID: TIntegerField;
    taDIListDOCNO: TIntegerField;
    taDIListDOCDATE: TDateTimeField;
    taDIListITYPE: TSmallintField;
    taDIListDEPID: TIntegerField;
    taDIListDEPNAME: TFIBStringField;
    taDIListUSERID: TIntegerField;
    taDIListNDSID: TIntegerField;
    taDIListFIO: TFIBStringField;
    taDIListISCLOSE: TSmallintField;
    taDIListREF: TIntegerField;
    taDIListFROMDEPID: TIntegerField;
    taDIListFROMDEPNAME: TFIBStringField;
    taDIListQ: TIntegerField;
    taDIListW: TFloatField;
    taDIListCOST: TFloatField;
    taDIElID: TIntegerField;
    taDIElINVID: TIntegerField;
    taDIElSEMIS: TFIBStringField;
    taDIElQ: TFloatField;
    taDIElW: TFloatField;
    taDIElTPRICE: TFloatField;
    taDIElPRICE: TFloatField;
    taDIElNDSID: TIntegerField;
    taDIElNDS: TFloatField;
    taDIElT: TSmallintField;
    taDIElCOST: TFloatField;
    taDIElOPERID: TFIBStringField;
    taDWHSemis: TpFIBDataSet;
    dsrDWHSemis: TDataSource;
    taHWhArt: TpFIBDataSet;
    dsrHWhArt: TDataSource;
    taDWHSemisID: TIntegerField;
    taDWHSemisSEMISID: TFIBStringField;
    taDWHSemisSEMISNAME: TFIBStringField;
    taDWHSemisMATID: TFIBStringField;
    taDWHSemisMATNAME: TFIBStringField;
    taDWHSemisOPERID: TFIBStringField;
    taDWHSemisOPERNAME: TFIBStringField;
    taDWHSemisQ_FROM: TFloatField;
    taDWHSemisW_FROM: TFloatField;
    taDWHSemisQ: TFloatField;
    taDWHSemisW: TFloatField;
    taDWHSemisUQ: TSmallintField;
    taDWHSemisUW: TSmallintField;
    taDIElSEMISNAME: TStringField;
    taDWHSemisDEPFROMID: TIntegerField;
    taDWHSemisDEPFROMNAME: TFIBStringField;
    taDWHSemisDEPID: TIntegerField;
    taDIElUQ: TSmallintField;
    taDIElUW: TSmallintField;
    taDIElOPERNAME: TFIBStringField;
    taWOrderART2ID: TIntegerField;
    taWOrderAPPLINV: TIntegerField;
    taWhSemisOPERID: TFIBStringField;
    taWhSemisOPERNAME: TFIBStringField;
    quWOrder_TNK: TFloatField;
    taABuffer: TpFIBDataSet;
    taABufferID: TIntegerField;
    taABufferUSERID: TIntegerField;
    taABufferART2ID: TIntegerField;
    taABufferSZID: TIntegerField;
    taABufferOPERID: TFIBStringField;
    taABufferU: TSmallintField;
    taABufferQ: TIntegerField;
    taABufferARTID: TIntegerField;
    taTmp_WhArt: TpFIBDataSet;
    dsrTmp_WhArt: TDataSource;
    taDIElREF: TIntegerField;
    taDIListJOBID: TIntegerField;
    taDIListJOBNAME: TFIBStringField;
    taPIt_InSemis_dRQ: TIntegerField;
    taPIt_InSemis_dRW: TFloatField;
    taASItemS_OPERID: TFIBStringField;
    taWOrderFROMOPERID: TFIBStringField;
    taWOrderFROMOPERNAME: TStringField;
    taWhSemisGOOD: TSmallintField;
    taPIt_InSemis_dSEMISNAME1: TStringField;
    taSBuffer: TpFIBDataSet;
    taSBufferID: TIntegerField;
    taSBufferUSERID: TIntegerField;
    taSBufferDEPID: TIntegerField;
    taSBufferSEMIS: TFIBStringField;
    taSBufferOPERID: TFIBStringField;
    taSBufferQ: TFloatField;
    taSBufferW: TFloatField;
    taSemisT1: TpFIBDataSet;
    taSemisT1SEMISID: TFIBStringField;
    taSemisT1SEMIS: TFIBStringField;
    dsrSemisT1: TDataSource;
    taAssortInv: TpFIBDataSet;
    taAssortEl: TpFIBDataSet;
    dsrAssortInv: TDataSource;
    dsrAssortEl: TDataSource;
    taAssortInvDOCNO: TFIBStringField;
    taAssortInvDOCDATE: TDateTimeField;
    taAssortElID: TIntegerField;
    taAssortElINVID: TIntegerField;
    taAssortElARTID: TIntegerField;
    taAssortElART2ID: TIntegerField;
    taAssortElSZID: TIntegerField;
    taAssortElART: TFIBStringField;
    taAssortElART2: TFIBStringField;
    taAssortElSZ: TFIBStringField;
    taAssortElQ: TIntegerField;
    taAssortElT: TSmallintField;
    taAssortElOPERID: TFIBStringField;
    taAssortElOPERATION: TFIBStringField;
    taAssortElOPERIDFROM: TFIBStringField;
    taAssortElOPERATIONFROM: TFIBStringField;
    taAssortElART2ID_OLD: TIntegerField;
    taAssortElART2_OLD: TFIBStringField;
    taAssortElU: TSmallintField;
    taAssortElU_OLD: TSmallintField;
    taAssortElRecNo: TIntegerField;
    taProtocol_dDONEW: TFloatField;
    taProtocol_dOPERNAME: TFIBStringField;
    taProtocol_dRW: TFloatField;
    taSOList: TpFIBDataSet;
    dsrSOList: TDataSource;
    dsrSOItem: TDataSource;
    taSOListINVID: TIntegerField;
    taSOListDOCNO: TIntegerField;
    taSOListDOCDATE: TDateTimeField;
    taSOListITYPE: TSmallintField;
    taSOListDEPID: TIntegerField;
    taSOListUSERID: TIntegerField;
    taSOListSUPID: TIntegerField;
    taSOListFIO: TFIBStringField;
    taSOListSUPNAME: TFIBStringField;
    taSOListISCLOSE: TSmallintField;
    taSOListDEPNAME: TFIBStringField;
    taSOListNDSID: TIntegerField;
    taSOListREF: TIntegerField;
    taSOListQ: TIntegerField;
    taSOListSSF: TFIBStringField;
    taSOListSSFDT: TDateTimeField;
    taSOListW: TFloatField;
    taSOListCOST: TFloatField;
    taSOListMEMO: TMemoField;
    taSIElFOR_Q_U: TSmallintField;
    taTmpWhSemis: TpFIBDataSet;
    dsrTmpWhSemis: TDataSource;
    taTmpWhSemisID: TIntegerField;
    taTmpWhSemisDEPID: TIntegerField;
    taTmpWhSemisDEPNAME: TFIBStringField;
    taTmpWhSemisSEMISID: TFIBStringField;
    taTmpWhSemisSEMISNAME: TFIBStringField;
    taTmpWhSemisMATID: TFIBStringField;
    taTmpWhSemisMATNAME: TFIBStringField;
    taTmpWhSemisOPERID: TFIBStringField;
    taTmpWhSemisOPERNAME: TFIBStringField;
    taTmpWhSemisQ: TFloatField;
    taTmpWhSemisW: TFloatField;
    taTmpWhSemisUQ: TSmallintField;
    taTmpWhSemisUW: TSmallintField;
    taProtocol_dU: TFloatField;
    taPIt_InSemis_dGOOD: TSmallintField;
    taProtocol_dF_999: TFloatField;
    taProtocol_dE_999: TFloatField;
    taProtocol_dS_999: TFloatField;
    taProtocol_dN_999: TFloatField;
    taPIt_InSemis_dGETW: TFloatField;
    taPIt_InSemis_dGETQ: TIntegerField;
    taPIt_InSemis_dDONEW: TFloatField;
    taPIt_InSemis_dDONEQ: TIntegerField;
    taPIt_InSemis_dRETW: TFloatField;
    taPIt_InSemis_dRETQ: TIntegerField;
    taPIt_InSemis_dREJW: TFloatField;
    taPIt_InSemis_dREJQ: TIntegerField;
    taPIt_InSemis_dREWORKW: TFloatField;
    taPIt_InSemis_dREWORKQ: TIntegerField;
    taPIt_InSemis_dNKW: TFloatField;
    taPIt_InSemis_dNKQ: TIntegerField;
    taProtocol_dINW: TFloatField;
    taProtocol_dINQ: TIntegerField;
    taProtocol_dRQ: TIntegerField;
    taPIt_InSemis_dIQ: TIntegerField;
    taPIt_InSemis_dIW: TFloatField;
    taPIt_InSemis_dINW: TFloatField;
    taPIt_InSemis_dINQ: TIntegerField;
    taProtSJInfo: TpFIBDataSet;
    dsrProtSJInfo: TDataSource;
    taProtSJInfoINVNO: TIntegerField;
    taProtSJInfoWORDERNO: TIntegerField;
    taProtSJInfoITDATE: TDateTimeField;
    taProtSJInfoSEMISNAME: TFIBStringField;
    taProtSJInfoW: TFloatField;
    taProtSJInfoQ: TFloatField;
    taProtSJInfoITTYPE: TSmallintField;
    taProtSJInfoWHNAME: TFIBStringField;
    taProtocol_dDONEQ: TIntegerField;
    taDIElFROMOPERNAME: TFIBStringField;
    taDIElFROMOPERID: TFIBStringField;
    taDWHSemisGOOD: TSmallintField;
    taDIElGOOD: TSmallintField;
    taProtWhItem: TpFIBDataSet;
    dsrProtWHItem: TDataSource;
    taProtWhItemDOCNO: TIntegerField;
    taProtWhItemDOCDATE: TDateTimeField;
    taProtWhItemDEPID: TIntegerField;
    taProtWhItemDEPNAME: TFIBStringField;
    taProtWhItemDEPFROMID: TIntegerField;
    taProtWhItemDEPFROMNAME: TFIBStringField;
    taProtWhItemSEMISID: TFIBStringField;
    taProtWhItemSEMISNAME: TFIBStringField;
    taProtWhItemOPERID: TFIBStringField;
    taProtWhItemOPERNAME: TFIBStringField;
    taProtWhItemUQ: TSmallintField;
    taProtWhItemUW: TSmallintField;
    taProtWhItemQ: TFloatField;
    taProtWhItemW: TFloatField;
    taProtWhItemT: TSmallintField;
    taProtWhItemT1: TSmallintField;
    taProtWhItemFROMOPERID: TFIBStringField;
    taProtWhItemFROMOPERNAME: TFIBStringField;
    taProtWhItemWORDERNO: TIntegerField;
    taProtWhItemWORDERDATE: TDateTimeField;
    taVList: TpFIBDataSet;
    dsrVList: TDataSource;
    taVListID: TIntegerField;
    taVListDOCNO: TIntegerField;
    taVListDOCDATE: TDateTimeField;
    taVListUSERID: TIntegerField;
    taVListBD: TDateTimeField;
    taVListISCLOSE: TSmallintField;
    taVListT: TSmallintField;
    taVListMATID: TFIBStringField;
    taVListUSERNAME: TFIBStringField;
    taVListDEPID: TIntegerField;
    taVListDEPNAME: TFIBStringField;
    taPListED: TDateField;
    taPSMOL1: TpFIBDataSet;
    dsrPSMOL1: TDataSource;
    taWOrderADOCNO: TFIBStringField;
    taSBufferGOOD: TSmallintField;
    taSBufferSEMISNAME: TFIBStringField;
    taZListED: TDateField;
    taProtocol_dQ_0: TFloatField;
    taProtocol_dQ_1: TFloatField;
    taZ_Assembly: TpFIBDataSet;
    dsrZ_Assembly: TDataSource;
    taZ_AssemblyDEPNAME: TFIBStringField;
    taZ_AssemblyOPERNAME: TFIBStringField;
    taZ_AssemblySEMISNAME: TFIBStringField;
    taZ_AssemblyQ: TIntegerField;
    taZ_AssemblyQC: TIntegerField;
    taZItemEP: TFloatField;
    taZItemEP1: TFloatField;
    taVArt: TpFIBDataSet;
    dsrVArt: TDataSource;
    taVArtID: TIntegerField;
    taVArtVITEMID: TIntegerField;
    taVArtDEPID: TIntegerField;
    taVArtARTID: TIntegerField;
    taVArtART2ID: TIntegerField;
    taVArtOPERID: TFIBStringField;
    taVArtU: TSmallintField;
    taVArtQ: TIntegerField;
    taVArtFQ: TIntegerField;
    taVArtOPERNAME: TFIBStringField;
    taVArtDEPNAME: TFIBStringField;
    taVArtART: TFIBStringField;
    taVArtART2: TFIBStringField;
    taVArtSZNAME: TFIBStringField;
    taVArtOUT_Q: TIntegerField;
    taVArtPROD_Q: TIntegerField;
    taVArtRET_PROD_Q: TIntegerField;
    taVArtMOVE_IN_Q: TIntegerField;
    taVArtMOVE_OUT_Q: TIntegerField;
    taVArtSZID: TIntegerField;
    taVArtREST_IN_Q: TIntegerField;
    taVArtIN7_Q: TIntegerField;
    taVArtIN9_Q: TIntegerField;
    taVArtRQ: TIntegerField;
    taVListA: TSmallintField;
    taBulkList: TpFIBDataSet;
    taBulk: TpFIBDataSet;
    dsrBulkList: TDataSource;
    dsrBulk: TDataSource;
    taBulkListINVID: TIntegerField;
    taBulkListDOCNO: TIntegerField;
    taBulkListDOCDATE: TDateTimeField;
    taBulkListITYPE: TSmallintField;
    taBulkListDEPID: TIntegerField;
    taBulkListUSERID: TIntegerField;
    taBulkListSUPID: TIntegerField;
    taBulkListFIO: TFIBStringField;
    taBulkListSUPNAME: TFIBStringField;
    taBulkListISCLOSE: TSmallintField;
    taBulkListDEPNAME: TFIBStringField;
    taBulkListNDSID: TIntegerField;
    taBulkListREF: TIntegerField;
    taBulkListQ: TIntegerField;
    taBulkListSSF: TFIBStringField;
    taBulkListSSFDT: TDateTimeField;
    taBulkListW: TFloatField;
    taBulkListCOST: TFloatField;
    taBulkID: TIntegerField;
    taBulkINVID: TIntegerField;
    taBulkSEMIS: TFIBStringField;
    taBulkQ: TFloatField;
    taBulkW: TFloatField;
    taBulkTPRICE: TFloatField;
    taBulkPRICE: TFloatField;
    taBulkT: TSmallintField;
    taBulkOPERID: TFIBStringField;
    taBulkQ0: TIntegerField;
    taBulkW0: TFloatField;
    taBulkSEMISNAME: TStringField;
    taBulkRW: TFloatField;
    taBulkCOST: TFloatField;
    taWOrderListJOBDEPNAME: TFIBStringField;
    taWOrderREJREF: TIntegerField;
    taWOrderREJPSID: TIntegerField;
    taWOrderREJPSNAME: TFIBStringField;
    taPArt: TpFIBDataSet;
    dsrPArt: TDataSource;
    taPArtID: TIntegerField;
    taPArtACTID: TIntegerField;
    taPArtARTID: TIntegerField;
    taPArtART2ID: TIntegerField;
    taPArtSZID: TIntegerField;
    taPArtOPERID: TFIBStringField;
    taPArtU: TSmallintField;
    taPArtREST_IN_Q: TIntegerField;
    taPArtGET_Q: TIntegerField;
    taPArtQ: TIntegerField;
    taPArtFQ: TIntegerField;
    taPListA: TSmallintField;
    taPArtART: TFIBStringField;
    taPArtART2: TFIBStringField;
    taPArtOPERNAME: TFIBStringField;
    taPArtDEPNAME: TFIBStringField;
    taPArtSZNAME: TFIBStringField;
    taPArtRQ: TIntegerField;
    taWOrderFULLART: TFIBStringField;
    taWOrderART: TFIBStringField;
    taWOrderART2: TFIBStringField;
    taProtocol_dQ_A: TIntegerField;
    taProtocol_dFQ_A: TIntegerField;
    taPArtDONE_Q: TIntegerField;
    taPArtREJ_Q: TIntegerField;
    taAssortElQ1: TIntegerField;
    taZItemRATE: TFloatField;
    taZItemZT: TSmallintField;
    taZpAssort: TpFIBDataSet;
    dsrZpAssort: TDataSource;
    taZpAssortARTID: TIntegerField;
    taZpAssortART: TFIBStringField;
    taZpAssortQ: TIntegerField;
    taZpAssortMW: TFloatField;
    taZpAssortK: TFloatField;
    taZpAssortARTK: TFloatField;
    taZpAssortKT: TFloatField;
    taZItemARTK: TFloatField;
    taAFList: TpFIBDataSet;
    dsrAFList: TDataSource;
    taAFListINVID: TIntegerField;
    taAFListDOCNO: TIntegerField;
    taAFListDOCDATE: TDateTimeField;
    taAFListITYPE: TSmallintField;
    taAFListUSERID: TIntegerField;
    taAFListSUPID: TIntegerField;
    taAFListISCLOSE: TSmallintField;
    taAFListDEPID: TIntegerField;
    taAFListDEPNAME: TFIBStringField;
    taAFListSUPNAME: TFIBStringField;
    taPIt_InSemis_dQ: TIntegerField;
    taPIt_InSemis_dW: TFloatField;
    taPArtOPERATIONNAME: TStringField;
    taPArtPSID: TIntegerField;
    taPIt_InSemis_dQ0: TFloatField;
    taAFListUSERNAME: TFIBStringField;
    taAffinaj: TpFIBDataSet;
    dsrAffinaj: TDataSource;
    taWhAffinaj: TpFIBDataSet;
    dsrWhAffinaj: TDataSource;
    taWhAffinajID: TIntegerField;
    taWhAffinajSEMISID: TFIBStringField;
    taWhAffinajSEMISNAME: TFIBStringField;
    taWhAffinajQ: TIntegerField;
    taWhAffinajW: TFloatField;
    taAffinajID: TIntegerField;
    taAffinajINVID: TIntegerField;
    taAffinajSEMISNAME: TFIBStringField;
    taAffinajQ: TIntegerField;
    taAffinajW: TFloatField;
    taAffinajT: TSmallintField;
    taAffinajSEMISAFFID: TIntegerField;
    taAffinajDEPID: TIntegerField;
    taAffinajDEPNAME: TFIBStringField;
    taAffinajSEMISAFFNAME: TStringField;
    taAFListCOMPID: TIntegerField;
    taAFListCOMPNAME: TFIBStringField;
    taPIt_InSemis_dFROMOPERID: TFIBStringField;
    taPIt_InSemis_dFROMOPERNAME: TStringField;
    taPListUPD: TSmallintField;
    taZItemZPDONE: TFloatField;
    taAffinajSEMISID: TFIBStringField;
    taWhAffinajT: TSmallintField;
    taWhAffinajUQ: TSmallintField;
    taWhAffinajUW: TSmallintField;
    taAffinajUQ: TSmallintField;
    taAffinajUW: TSmallintField;
    taAffinajQ0: TIntegerField;
    taAffinajW0: TFloatField;
    taWhAffinajDEPID: TIntegerField;
    taAffinajOPERID: TFIBStringField;
    taAffinajOPERNAME: TFIBStringField;
    taWhAffinajGOOD: TSmallintField;
    taWhAffinajOPERID: TFIBStringField;
    taWhAffinajOPERNAME: TFIBStringField;
    taWOrderUQ: TSmallintField;
    taWOrderUW: TSmallintField;
    taWOrderREJID: TIntegerField;
    taWOrderREJNAME: TStringField;
    taWhSemisDEPNAME: TFIBStringField;
    taWhSemisSORTINDDEPID: TIntegerField;
    taASItemARTU: TSmallintField;
    taASItemS_OPERNAME: TFIBStringField;
    taWhSemisSORTIND: TIntegerField;
    taProtocolMat: TpFIBDataSet;
    dsrProtocolMat: TDataSource;
    taProtocolMatMATNAME: TFIBStringField;
    taProtocolMatW: TFloatField;
    taProtocolMatU: TSmallintField;
    taWOrderListPROTOCOLID: TIntegerField;
    taWOrderPROTOCOLID: TIntegerField;
    taVListBUH: TSmallintField;
    taCheckProtocolData: TpFIBDataSet;
    dsrCheckProtocolData: TDataSource;
    taCheckProtocolDataID: TIntegerField;
    taCheckProtocolDataDEPID: TIntegerField;
    taCheckProtocolDataOPERID: TFIBStringField;
    taCheckProtocolDataWORDER_GET_W: TFloatField;
    taCheckProtocolDataWORDER_RET_W: TFloatField;
    taCheckProtocolDataWORDER_F: TFloatField;
    taCheckProtocolDataPROTOCOL_IN: TFloatField;
    taCheckProtocolDataPROTOCOL_DONE_W: TFloatField;
    taCheckProtocolDataPROTOCOL_OUT: TFloatField;
    taCheckProtocolDataR: TFloatField;
    taCheckProtocolDataOPERATION: TFIBStringField;
    taCheckProtocolDataNAME: TFIBStringField;
    taBulkListREFNO: TIntegerField;
    taProtocol_dMATNAME: TFIBStringField;
    taSChargeList: TpFIBDataSet;
    taSCharge: TpFIBDataSet;
    dsrSChargeList: TDataSource;
    dsrSCharge: TDataSource;
    taSChargeListINVID: TIntegerField;
    taSChargeListDOCNO: TIntegerField;
    taSChargeListDOCDATE: TDateTimeField;
    taSChargeListITYPE: TSmallintField;
    taSChargeListDEPID: TIntegerField;
    taSChargeListUSERID: TIntegerField;
    taSChargeListSUPID: TIntegerField;
    taSChargeListFIO: TFIBStringField;
    taSChargeListSUPNAME: TFIBStringField;
    taSChargeListISCLOSE: TSmallintField;
    taSChargeListDEPNAME: TFIBStringField;
    taSChargeListNDSID: TIntegerField;
    taSChargeListREF: TIntegerField;
    taSChargeListQ: TIntegerField;
    taSChargeListSSF: TFIBStringField;
    taSChargeListSSFDT: TDateTimeField;
    taSChargeListW: TFloatField;
    taSChargeListCOST: TFloatField;
    taSChargeID: TIntegerField;
    taSChargeINVID: TIntegerField;
    taSChargeSEMIS: TFIBStringField;
    taSChargeQ: TFloatField;
    taSChargeW: TFloatField;
    taSChargeTPRICE: TFloatField;
    taSChargePRICE: TFloatField;
    taSChargeNDSID: TIntegerField;
    taSChargeT: TSmallintField;
    taSChargeCOST: TFloatField;
    taSChargeOPERID: TFIBStringField;
    taSChargeFOR_Q_U: TSmallintField;
    taSChargeREF: TIntegerField;
    taSChargeSEMISNAME: TStringField;
    taSChargeOPERNAME: TStringField;
    taSChargeUQ: TSmallintField;
    taSChargeUW: TSmallintField;
    taSChargeWH: TpFIBDataSet;
    dsrSChargeWH: TDataSource;
    taSChargeWHDEPID: TIntegerField;
    taSChargeWHDEPNAME: TFIBStringField;
    taSChargeWHSEMISID: TFIBStringField;
    taSChargeWHQ: TFloatField;
    taSChargeWHW: TFloatField;
    taSChargeWHUQ: TSmallintField;
    taSChargeWHUW: TSmallintField;
    taSChargeWHSEMISNAME: TFIBStringField;
    taSChargeWHOPERID: TFIBStringField;
    taSChargeWHOPERNAME: TFIBStringField;
    taSChargeWHGOOD: TSmallintField;
    taSChargeWHSORTINDDEPID: TIntegerField;
    taSChargeWHSORTIND: TIntegerField;
    taSChargeNDS: TFloatField;
    taProtocol_dMATID: TFIBStringField;
    taProtocol_dTRANSID: TIntegerField;
    taSChargeDEPID: TIntegerField;
    taSChargeDEPNAME: TStringField;
    taSChargeListREFDOCNO: TIntegerField;
    taSChargeListREFDOCDATE: TDateTimeField;
    taCheckStone: TpFIBDataSet;
    dsrCheckStone: TDataSource;
    taCheckStoneSEMISID: TFIBStringField;
    taCheckStoneUQ: TSmallintField;
    taCheckStoneUW: TSmallintField;
    taCheckStoneSEMISNAME: TFIBStringField;
    taCheckStoneREST_INQ_1: TIntegerField;
    taCheckStoneREST_INW_1: TFloatField;
    taCheckStoneINQ_1: TIntegerField;
    taCheckStoneINW_1: TFloatField;
    taCheckStonePRODQ_1: TIntegerField;
    taCheckStonePRODW_1: TFloatField;
    taCheckStoneRETPRODQ_1: TIntegerField;
    taCheckStoneRETPRODW_1: TFloatField;
    taCheckStoneSCHARGEQ_1: TIntegerField;
    taCheckStoneSCHARGEW_1: TFloatField;
    taCheckStoneFQ_1: TIntegerField;
    taCheckStoneFW_1: TFloatField;
    taCheckStoneREST_INQ_2: TIntegerField;
    taCheckStoneREST_INW_2: TFloatField;
    taCheckStoneINQ_2: TIntegerField;
    taCheckStoneINW_2: TFloatField;
    taCheckStoneWORDERQ_2: TIntegerField;
    taCheckStoneWORDERW_2: TFloatField;
    taCheckStoneSCHARGEQ_2: TIntegerField;
    taCheckStoneSCHARGEW_2: TFloatField;
    taCheckStoneFQ_2: TIntegerField;
    taCheckStoneFW_2: TFloatField;
    taCheckStoneRQ_1: TIntegerField;
    taCheckStoneRW_1: TFloatField;
    taCheckStoneRQ_2: TIntegerField;
    taCheckStoneRW_2: TFloatField;
    taCheckStoneRQ: TIntegerField;
    taCheckStoneRW: TFloatField;
    taMatrixProd: TpFIBDataSet;
    dsrMatrixProd: TDataSource;
    taASItemSORTIND: TIntegerField;
    taB_UID: TpFIBDataSet;
    dsrB_UID: TDataSource;
    taHWhArtID: TIntegerField;
    taHWhArtUSERID: TIntegerField;
    taHWhArtITYPE: TSmallintField;
    taHWhArtARTID: TIntegerField;
    taHWhArtART2ID: TIntegerField;
    taHWhArtSZID: TIntegerField;
    taHWhArtU: TSmallintField;
    taHWhArtOPERID: TFIBStringField;
    taHWhArtDEPID: TIntegerField;
    taHWhArtDEPFROMID: TIntegerField;
    taHWhArtOLD_U: TSmallintField;
    taHWhArtOLD_OPERID: TFIBStringField;
    taHWhArtOLD_ART2ID: TIntegerField;
    taHWhArtART: TFIBStringField;
    taHWhArtART2: TFIBStringField;
    taHWhArtSZNAME: TFIBStringField;
    taHWhArtOPERNAME: TFIBStringField;
    taHWhArtDEPNAME: TFIBStringField;
    taHWhArtDEPFROMNAME: TFIBStringField;
    taHWhArtDOCID: TIntegerField;
    taHWhArtDOCNO: TIntegerField;
    taHWhArtDOCDATE: TDateTimeField;
    taHWhArtWINVID: TIntegerField;
    taHWhArtWINVNO: TIntegerField;
    taHWhArtWINVDATE: TDateTimeField;
    taHWhArtWINVSEMIS: TFIBStringField;
    taHWhArtWORDERID: TIntegerField;
    taHWhArtWORDERNO: TIntegerField;
    taHWhArtWORDERDATE: TDateTimeField;
    taHWhArtQ: TIntegerField;
    taTmp_WhArtART: TFIBStringField;
    taTmp_WhArtART2: TFIBStringField;
    taTmp_WhArtSZNAME: TFIBStringField;
    taTmp_WhArtU: TSmallintField;
    taTmp_WhArtOPERNAME: TFIBStringField;
    taTmp_WhArtDEPNAME: TFIBStringField;
    taTmp_WhArtITYPE: TSmallintField;
    taAFListR: TFloatField;
    taWhTails: TpFIBDataSet;
    dsrWhTails: TDataSource;
    taWhTailsID: TIntegerField;
    taWhTailsTAILSID: TIntegerField;
    taWhTailsW: TFloatField;
    taWhTailsUW: TSmallintField;
    taWhTailsTAILNAME: TFIBStringField;
    taProtocolMatTAILNAME: TFIBStringField;
    taTmp_WhArtDEPID: TIntegerField;
    taInsBuffer: TpFIBDataSet;
    taInsBufferUSERID: TFIBIntegerField;
    taInsBufferINSID: TFIBStringField;
    taInsBufferQ: TFIBIntegerField;
    taInsBufferW: TFIBFloatField;
    taWOrderListDOCDATE: TFIBDateTimeField;
    taTmp_WhArtQ: TFIBBCDField;
    taAFListREF: TFIBIntegerField;
    taAFListREFDOCNO: TFIBIntegerField;
    taAFListREFDOCDATE: TFIBDateTimeField;
    taASListSCOST: TFIBFloatField;
    taSIListCONTRACTID: TFIBIntegerField;
    taSIListCONTRACTNAME: TFIBStringField;
    taSOListCONTRACTID: TFIBIntegerField;
    taSOListCONTRACTNAME: TFIBStringField;
    taASListCONTRACTID: TFIBIntegerField;
    taASListCONTRACTNAME: TFIBStringField;
    taASListINVCOLORID: TFIBIntegerField;
    taASListCOLOR: TFIBIntegerField;
    taSIListUE: TFIBFloatField;
    taSIElUEPRICE: TFIBFloatField;
    taSIListSCHEMAID: TFIBIntegerField;
    taSOListSCHEMAID: TFIBIntegerField;
    taInvSubType: TpFIBDataSet;
    dsrInvSubType: TDataSource;
    taSOListINVSUBTYPEID: TFIBIntegerField;
    taSOListINVSUBTYPENAME: TFIBStringField;
    taSIListINVSUBTYPEID: TFIBIntegerField;
    taSIListINVSUBTYPENAME: TFIBStringField;
    taInsBufferCOMPID: TFIBIntegerField;
    taVListCREATED: TFIBDateTimeField;
    taProtocol_dRET_PERCENT: TFIBFloatField;
    taProtocol_dREJ_PERCENT: TFIBFloatField;
    taWhSQ0: TFIBFloatField;
    taWhSW0: TFIBFloatField;
    taWhAffinajDEPNAME: TFIBStringField;
    taAffinajCOVERDEPID: TFIBIntegerField;
    taAffinajCOVERDEPNAME: TFIBStringField;
    taWhTailsDEPNAME: TFIBStringField;
    taDIListINVCOLORID: TFIBIntegerField;
    taDIListCOLOR: TFIBIntegerField;
    taSIListINVCOLORID: TFIBIntegerField;
    taSIListCOLOR: TFIBIntegerField;
    taVListREFID: TFIBIntegerField;
    taVListREFDATE: TFIBDateTimeField;
    taVListREFNAME: TFIBStringField;
    taVListREFNO: TFIBIntegerField;
    taVListED: TFIBDateTimeField;
    taDIAssortEl: TpFIBDataSet;
    dsrDIAssort: TDataSource;
    taDIAssortElID: TFIBIntegerField;
    taDIAssortElINVID: TFIBIntegerField;
    taDIAssortElARTID: TFIBIntegerField;
    taDIAssortElART2ID: TFIBIntegerField;
    taDIAssortElSZ: TFIBIntegerField;
    taDIAssortElQ: TFIBIntegerField;
    taDIAssortElART: TFIBStringField;
    taDIAssortElART2: TFIBStringField;
    taDIAssortElT: TFIBSmallIntField;
    taDIAssortElOPERID: TFIBStringField;
    taDIAssortElOPERATION: TFIBStringField;
    taDIAssortElOPERIDFROM: TFIBStringField;
    taDIAssortElOLD_ART2ID: TFIBIntegerField;
    taDIAssortElU: TFIBSmallIntField;
    taDIAssortElOLD_U: TFIBSmallIntField;
    taDIAssortElCOMMENTID: TFIBSmallIntField;
    taDIAssortElSZNAME: TFIBStringField;
    taDIAssortElRecNo: TIntegerField;
    taDIAssortElQ1: TFIBBCDField;
    taDIAssortInv: TpFIBDataSet;
    dsrDIAssortInv: TDataSource;
    taDIAssortInvDOCNO: TFIBIntegerField;
    taDIAssortInvDOCDATE: TFIBDateTimeField;
    taDIAssortInvINVID: TFIBIntegerField;
    taASListJOBID: TFIBIntegerField;
    taASListJOBNAME: TFIBStringField;
    taASListShipedBy: TFIBIntegerField;
    dsrPenalty: TDataSource;
    taPenalty: TpFIBDataSet;
    taPenaltyINVID: TFIBIntegerField;
    taPenaltyDOCNO: TFIBIntegerField;
    taPenaltyDOCDATE: TFIBDateField;
    taPenaltyCOST: TFIBFloatField;
    taPenaltyPENALTY: TFIBFloatField;
    taProtocol_dLOSSES: TFIBBCDField;
    taSOListW3: TFIBFloatField;
    taSIListW3: TFIBFloatField;
    taProtocol_dN2: TFIBFloatField;
    taProtocol_dSHORTAGE: TFIBBCDField;
    taSIListSELFCOMPNAME: TFIBStringField;
    taSOListSELFCOMPNAME: TFIBStringField;
    taAssortInvAINV: TFIBIntegerField;
    taSIListLINESCOUNT: TFIBIntegerField;
    taASItemISPRINTED: TFIBSmallIntField;
    taSOEl: TpFIBDataSet;
    taSOElID: TFIBIntegerField;
    taSOElINVID: TFIBIntegerField;
    taSOElSEMIS: TFIBStringField;
    taSOElQ: TFIBFloatField;
    taSOElW: TFIBFloatField;
    taSOElTPRICE: TFIBFloatField;
    taSOElPRICE: TFIBFloatField;
    taSOElNDSID: TFIBIntegerField;
    taSOElNDS: TFIBFloatField;
    taSOElT: TFIBSmallIntField;
    taSOElCOST: TFIBFloatField;
    taSOElOPERID: TFIBStringField;
    taSOElFOR_Q_U: TFIBSmallIntField;
    taSOElREF: TFIBIntegerField;
    taSOElOPERNAME: TStringField;
    taSOElSEMISNAME: TStringField;
    taSOElUQ: TFIBStringField;
    taSOElUW: TFIBStringField;
    taSOElFORQUNAME: TFIBStringField;
    DataSetFilterContract: TpFIBDataSet;
    DataSourceFilterContract: TDataSource;
    DataSetFilterContractCONTRACTID: TFIBIntegerField;
    DataSetFilterContractCONTRACTNAME: TFIBStringField;
    DataSetFilterContractACCOUNTINGTYPE: TFIBIntegerField;
    taAFListW0: TFIBFloatField;
    taAFListW1: TFIBBCDField;
    taAFListW: TFIBFloatField;
    taAFListISFOREIGNGOLD: TFIBSmallIntField;
    taSIListCREATED: TFIBDateTimeField;
    taSIListREFERENCEID: TFIBIntegerField;
    taSOListCREATED: TFIBDateTimeField;
    taSOListREFERENCEID: TFIBIntegerField;
    taASListMC: TFIBIntegerField;
    taASListPRODUCERID: TFIBIntegerField;
    taASListPRODUCERNAME: TFIBStringField;
    procedure CommitRetaininig(DataSet: TDataSet);
    procedure taWOrderListNewRecord(DataSet: TDataSet);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure taWOrderListBeforeOpen(DataSet: TDataSet);
    procedure taWOrderBeforeOpen(DataSet: TDataSet);
    procedure taWOrderNewRecord(DataSet: TDataSet);
    procedure taWOrderBeforeInsert(DataSet: TDataSet);
    procedure DelConf(DataSet: TDataSet);
    procedure ITTYPESetText(Sender: TField; const Text: String);
    procedure taWOJobBeforeInsert(DataSet: TDataSet);
    procedure taWOrderAfterPost(DataSet: TDataSet);
    procedure taWOrderAfterDelete(DataSet: TDataSet);
    procedure taWOrderBeforeEdit(DataSet: TDataSet);
    procedure taCalcLossAfterOpen(DataSet: TDataSet);
    procedure quWOrder_TBeforeOpen(DataSet: TDataSet);
    procedure quWOrderList_TBeforeOpen(DataSet: TDataSet);
    procedure taPListNewRecord(DataSet: TDataSet);
    procedure CalcRecNo(DataSet: TDataSet);
    procedure taProtocolNewRecord(DataSet: TDataSet);
    procedure taActPGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taActPSetText(Sender: TField; const Text: String);
    procedure taProtocolBeforeOpen(DataSet: TDataSet);
    procedure taPListBeforeOpen(DataSet: TDataSet);
    procedure taZListNewRecord(DataSet: TDataSet);
    procedure taZListBeforeOpen(DataSet: TDataSet);
    procedure taProtocolCalcFields(DataSet: TDataSet);
    procedure taZItemCalcFields(DataSet: TDataSet);
    procedure taPSMolBeforeOpen(DataSet: TDataSet);
    procedure ITTYPEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taZItemBeforeOpen(DataSet: TDataSet);
    procedure taZItemNewRecord(DataSet: TDataSet);
    procedure taWOrderBeforeDelete(DataSet: TDataSet);
    procedure taPListBeforeDelete(DataSet: TDataSet);
    procedure taWOrderAfterOpen(DataSet: TDataSet);
    procedure taWOrderAfterScroll(DataSet: TDataSet);
    procedure taProtocolBeforeEdit(DataSet: TDataSet);
    procedure taPsOperBeforeOpen(DataSet: TDataSet);
    procedure taSIListBeforeOpen(DataSet: TDataSet);
    procedure taSIListNewRecord(DataSet: TDataSet);
    procedure taSIElCalcFields(DataSet: TDataSet);
    procedure taSIElNewRecord(DataSet: TDataSet);
    procedure taSIElBeforeOpen(DataSet: TDataSet);
    procedure UQGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure UWGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taWhSemisBeforeOpen(DataSet: TDataSet);
    procedure taWOrderQChange(Sender: TField);
    procedure taWOrderWChange(Sender: TField);
    procedure taSIElTPRICEChange(Sender: TField);
    procedure SICheckClose(DataSet: TDataSet);
    procedure taASListBeforeOpen(DataSet: TDataSet);
    procedure taASListNewRecord(DataSet: TDataSet);
    procedure taASItemBeforeOpen(DataSet: TDataSet);
    procedure taASItemNewRecord(DataSet: TDataSet);
    procedure UGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ASICheckClose(DataSet: TDataSet);
    procedure taASItemCalcFields(DataSet: TDataSet);
    procedure taWhArtBeforeOpen(DataSet: TDataSet);
    procedure taWhSBeforeOpen(DataSet: TDataSet);
    procedure taASItemPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure taProtocol_dBeforeOpen(DataSet: TDataSet);
    procedure taProtocol_dBeforeEdit(DataSet: TDataSet);
    procedure taPIt_InSemis_dBeforeOpen(DataSet: TDataSet);
    procedure taDIListBeforeOpen(DataSet: TDataSet);
    procedure taDIListNewRecord(DataSet: TDataSet);
    procedure DICheckClose(DataSet: TDataSet);
    procedure taDIElBeforeInsert(DataSet: TDataSet);
    procedure taDIElBeforeOpen(DataSet: TDataSet);
    procedure taDIElNewRecord(DataSet: TDataSet);
    procedure taHWhArtITYPEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taHWhArtBeforeOpen(DataSet: TDataSet);
    procedure taDWHSemisBeforeOpen(DataSet: TDataSet);
    procedure taDIElPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure taDWHSemisNewRecord(DataSet: TDataSet);
    procedure taABufferBeforeOpen(DataSet: TDataSet);
    procedure taPIt_InSemis_dCalcFields(DataSet: TDataSet);
    procedure taPIt_InSemis_dBeforeEdit(DataSet: TDataSet);
    procedure taASItemUA_OLDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taPIt_InSemis_dNewRecord(DataSet: TDataSet);
    procedure taSBufferBeforeOpen(DataSet: TDataSet);
    procedure taAssortInvBeforeOpen(DataSet: TDataSet);
    procedure taAssortElBeforeOpen(DataSet: TDataSet);
    procedure taSOListBeforeOpen(DataSet: TDataSet);
    procedure taSOListNewRecord(DataSet: TDataSet);
    procedure SOCheckClose(DataSet: TDataSet);
    procedure taProtSJInfoBeforeOpen(DataSet: TDataSet);
    procedure taDIElFROMOPERNAMEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taDIElOPERNAMEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taMolIOBeforeOpen(DataSet: TDataSet);
    procedure taHWhArtFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure taProtWhItemTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taProtWhItemT1GetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taProtWhItemBeforeOpen(DataSet: TDataSet);
    procedure taVListBeforeDelete(DataSet: TDataSet);
    procedure taVListBeforeOpen(DataSet: TDataSet);
    procedure taVListNewRecord(DataSet: TDataSet);
    procedure taPListCalcFields(DataSet: TDataSet);
    procedure taASListBeforeDelete(DataSet: TDataSet);
    procedure taWOrderAfterInsert(DataSet: TDataSet);
    procedure taDIListBeforeDelete(DataSet: TDataSet);
    procedure taPSMOL1BeforeOpen(DataSet: TDataSet);
    procedure taDIListDOCDATEChange(Sender: TField);
    procedure taSIListBeforeDelete(DataSet: TDataSet);
    procedure taSIListDOCDATEChange(Sender: TField);
    procedure taSOListBeforeDelete(DataSet: TDataSet);
    procedure taSOListDOCDATEChange(Sender: TField);
    procedure taZListCalcFields(DataSet: TDataSet);
    procedure taZListBeforeDelete(DataSet: TDataSet);
    procedure taWOrderListBeforeDelete(DataSet: TDataSet);
    procedure taZ_AssemblyBeforeOpen(DataSet: TDataSet);
    procedure taVArtBeforeOpen(DataSet: TDataSet);
    procedure taVArtNewRecord(DataSet: TDataSet);
    procedure taVArtCalcFields(DataSet: TDataSet);
    procedure USetText(Sender: TField; const Text: String);
    procedure taVArtBeforeDelete(DataSet: TDataSet);
    procedure taBulkListBeforeDelete(DataSet: TDataSet);
    procedure taBulkListBeforeOpen(DataSet: TDataSet);
    procedure taBulkListNewRecord(DataSet: TDataSet);
    procedure taBulkCalcFields(DataSet: TDataSet);
    procedure BulkCheckClose(DataSet: TDataSet);
    procedure taBulkBeforeOpen(DataSet: TDataSet);
    procedure taBulkNewRecord(DataSet: TDataSet);
    procedure taASListDOCDATEValidate(Sender: TField);
    procedure taPArtBeforeDelete(DataSet: TDataSet);
    procedure taPArtBeforeOpen(DataSet: TDataSet);
    procedure taPArtCalcFields(DataSet: TDataSet);
    procedure taPArtNewRecord(DataSet: TDataSet);
    procedure taASItemDeleteError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure taZpAssortCalcFields(DataSet: TDataSet);
    procedure taZpAssortBeforeOpen(DataSet: TDataSet);
    procedure taBulkBeforePost(DataSet: TDataSet);
    procedure taAFListBeforeOpen(DataSet: TDataSet);
    procedure taProtocol_dFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure taVArtARTValidate(Sender: TField);
    procedure taVArtART2Validate(Sender: TField);
    procedure taVArtSZNAMEValidate(Sender: TField);
    procedure taPArtARTValidate(Sender: TField);
    procedure taPArtART2Validate(Sender: TField);
    procedure taPArtSZNAMEValidate(Sender: TField);
    procedure taAFListNewRecord(DataSet: TDataSet);
    procedure taAFListBeforeDelete(DataSet: TDataSet);
    procedure taWhAffinajBeforeOpen(DataSet: TDataSet);
    procedure taAffinajBeforeOpen(DataSet: TDataSet);
    procedure taAffinajBeforeDelete(DataSet: TDataSet);
    procedure taAffinajNewRecord(DataSet: TDataSet);
    procedure taPListUPDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taAffinajTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taAFListDEPIDValidate(Sender: TField);
    procedure taAffinajPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure taWOrderListBeforeEdit(DataSet: TDataSet);
    procedure taPsOperNewRecord(DataSet: TDataSet);
    procedure taProtocolMatBeforeOpen(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure taSChargeListBeforeDelete(DataSet: TDataSet);
    procedure taSChargeListBeforeOpen(DataSet: TDataSet);
    procedure taSChargeListNewRecord(DataSet: TDataSet);
    procedure SChargeCheckClose(DataSet: TDataSet);
    procedure taSChargeBeforeOpen(DataSet: TDataSet);
    procedure taSChargeNewRecord(DataSet: TDataSet);
    procedure taSChargeWHBeforeOpen(DataSet: TDataSet);
    procedure taSChargeBeforeDelete(DataSet: TDataSet);
    procedure taCheckStoneBeforeOpen(DataSet: TDataSet);
    procedure taCheckStoneCalcFields(DataSet: TDataSet);
    procedure taProtocolMatUGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taTmp_WhArtBeforeOpen(DataSet: TDataSet);
    procedure taTmp_WhArtITYPEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taAFListCalcFields(DataSet: TDataSet);
    procedure taInsBufferBeforeOpen(DataSet: TDataSet);
    procedure taSIElFOR_Q_UChange(Sender: TField);
    procedure taSIElQChange(Sender: TField);
    procedure taSIElWChange(Sender: TField);
    procedure taSIElUEPRICEChange(Sender: TField);
    procedure taAffinajWQChange(Sender: TField);
    procedure taAffinajBeforePost(DataSet: TDataSet);
    procedure taAffinajAfterPost(DataSet: TDataSet);
    procedure taSIListDeleteError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure taSOListAfterPost(DataSet: TDataSet);
    procedure taInvSubTypeBeforeOpen(DataSet: TDataSet);
    procedure taASListCONTRACTIDChange(Sender: TField);
    procedure taWhSCalcFields(DataSet: TDataSet);
    procedure taSOListDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taDIAssortInvBeforeOpen(DataSet: TDataSet);
    procedure taDIElAfterOpen(DataSet: TDataSet);
    procedure taDIElAfterScroll(DataSet: TDataSet);
    procedure taPenaltyBeforeOpen(DataSet: TDataSet);
    procedure taPenaltyAfterPost(DataSet: TDataSet);
    procedure taWOrderBeforePost(DataSet: TDataSet);
    procedure taAssortInvAfterOpen(DataSet: TDataSet);
    procedure taWhSemisAfterOpen(DataSet: TDataSet);
    procedure taSOElQChange(Sender: TField);
    procedure taSOElWChange(Sender: TField);
    procedure taSOElTPRICEChange(Sender: TField);
    procedure taSOElNewRecord(DataSet: TDataSet);
    procedure taSOElCalcFields(DataSet: TDataSet);
    procedure taSOElBeforeOpen(DataSet: TDataSet);
    procedure taSOElFOR_Q_UChange(Sender: TField);
    procedure taSIListPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
  private
    FCurrInv: integer;
    FInvMode: integer;
    FSemisW: double;
    FSemisQ: integer;
    FInvQValue : integer;
    FInvWValue : double;
    FWhSemisQValue : integer;
    FWhSemisWValue : double;
    FFilterDepId: string;
    FFilterOperId: string;
    FFilterMatId: string;
    FFilterSemisId: string;
    FhSzId: integer;
    FhArt2Id: integer;
    FDOperId: string;
    FDWhSemisNewId: integer;
    FFromOperId: integer;
    FDOperName: string;
    FSemis: string;
    FhWhArtEd: TDateTime;
    FhWhArtBd: TDateTime;
    FDetailWhMode: TDetailWhMode;
    FWorderMode: TWorderMode;
    FAffinajMode : TAffinajMode;
    FWhAffinajMode: byte;
    FCurrPsId: integer;
    //FZpYear: word;
    FOperId_WOrderTotal: string;
    FPsId_WorderTotal: integer;
    FBD_WOrderTotal: TDateTime;
    FED_WOrderTotal: TDateTime;
    FPsName_WOrderTotal: string;
    FOperName_WOrderTotal: string;
    FSheetMatrixProd: integer;
    FFilterArt: string;
    FDocListYear: word;
    FFilterCompId: string;
    FUseUE: boolean;
    FFilterZpKArt: string;
    FPenaltyMode: Integer;
  public
    FQChange : boolean;
    FWChange : boolean;
    FSpoilageWeight: Double;
    FSpoilageQuantity: Integer;
    FSupname: string;    

             (* ������� ��������� ��� ������� ������� *)
    property CurrInv : integer read FCurrInv write FCurrInv;
    property CurrPsId : integer read FCurrPsId write FCurrPsId;
    property InvMode : integer  read FInvMode write FInvMode;
    property PenaltyMode: Integer read FPenaltyMode write FPenaltyMode;

    (* ��� ����������� �� ������ �������������� � ������� *)
    property Semis : string read FSemis write FSemis;
    property SemisQ : integer read FSemisQ write FSemisQ;
    property SemisW : double read FSemisW write FSemisW;
    property FromOperId : integer read FFromOperId write FFromOperId;

                            (* �������� *)
    property DetailWhMode : TDetailWhMode read FDetailWhMode write FDetailWhMode;

                           (* ���������� *)
    property FilterOperId : string read FFilterOperId write FFilterOperId;
    property FilterDepId : string read FFilterDepId write FFilterDepId;
    property FilterSemisId : string read FFilterSemisId write FFilterSemisId;
    property FilterMatId : string read FFilterMatId write FFilterMatId;
    property FilterArt : string read FFilterArt write FFilterArt;
    property FilterCompId : string read FFilterCompId write FFilterCompId;
    property hWhArtBd : TDateTime read FhWhArtBd write FhWhArtBd;
    property hWhArtEd : TDateTime read FhWhArtEd write FhWhArtEd;
    property FilterZpKArt : string read FFilterZpKArt write FFilterZpKArt;

                      (* ������� *)
    property hArt2Id : integer read FhArt2Id write FhArt2Id;
    property hSzId : integer read FhSzId write FhSzId;

                      (* ��������� ����������� *)
    property DOperId : string read FDOperId write FDOperId;
    property DOperName : string read FDOperName write FDOperName;
    property DWhSemisNewId : integer read FDWhSemisNewId write FDWhSemisNewId;

                      (* ����� ������ ������ *)
    property WorderMode : TWorderMode read FWorderMode write FWorderMode;
    property AffinajMode : TAffinajMode read FAffinajMode write FAffinajMode;
    // 0 - ����� ������, 2 - ����� ������ 
    property WhAffinajMode : byte read FWhAffinajMode write FWhAffinajMode;

                      (* ��� ��� �������� *)
    //property DocListYear : word read FZpYear write FZpYear;
    property DocListYear : word read FDocListYear write FDocListYear;

    (* ������������ �.�. ��� ����������� ���������� *)
    property UseUE : boolean read FUseUE write FUseUE;

             (* ����� �� ����� �������� � �������� �� ������ *)
    property PsId_WOrderTotal : integer read FPsId_WorderTotal write FPsId_WorderTotal;
    property PsName_WOrderTotal : string read FPsName_WOrderTotal write FPsName_WOrderTotal;
    property OperId_WOrderTotal : string read FOperId_WOrderTotal write FOperId_WOrderTotal;
    property OperName_WOrderTotal : string read FOperName_WOrderTotal write FOperName_WOrderTotal;
    property BD_WOrderTotal : TDateTime read FBD_WOrderTotal write FBD_WOrderTotal;
    property ED_WOrderTotal : TDateTime read FED_WOrderTotal write FED_WOrderTotal;

    property SheetMatrixProd : integer read FSheetMatrixProd write FSheetMatrixProd;

    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;

    function _ShowEditorQW(Q : integer; W : double; UseWeigth : boolean) : boolean;

    procedure ChangePeriod(BD, ED : TDateTime);
    procedure ChangeAPeriod(BD, ED : TDateTime);
    procedure ChangeWhPeriod(BD, ED : TDateTime);
    procedure ChangeZPeriod(BD, ED : TDateTime);
    procedure ChangeVPeriod(BD, ED : TDateTime);

    procedure ContractFilter(DataSet: TpFIBDataSet; FieldName: String; KeyValue: Variant);

    procedure FillList(List : TStrings);
    // �����. �������� ���� FnId �� ����. Table � ���� Fn ��� ��� �������� Value
    function GetIdByField(Table, Fn, FnId : string; Value : Variant) : Variant;

    function CreateV_for_Wh(Id : string) : boolean;
    function CreateV_for_Buh(Id : string) : boolean;
  end;

var
  dmMain: TdmMain;

implementation

uses DictData, dbUtil, WOrder, rxStrUtils, Splash,
     StdCtrls, DateUtils,
     ApplData, fmUtils, InvData, WOrderRej, eSQ, Editor,
  Protocol_PS, ProductionConsts, uSemisStorage, frmDialogSemis;

{$R *.DFM}

procedure TdmMain.CommitRetaininig(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).Transaction.CommitRetaining;
end;

constructor TdmMain.Create(AOwner: TComponent);
begin
  inherited;
  fmSplash.lbState.Caption := '�������� ��������� ������ ������';
  fmSplash.lbState.Update;
end;

destructor TdmMain.Destroy;
begin
  inherited;
end;

procedure TdmMain.ContractFilter(DataSet: TpFIBDataSet; FieldName: String; KeyValue: Variant);
var
  Filter: String;
  Template: String;
  Begining: String;

  procedure DeleteSubstring;
  var
    i: integer;
    oldPosition: Integer;
  begin
    OldPosition := Pos(Template, Filter);

    if OldPosition <> 0 then
    begin

      Delete(Filter, oldPosition, length(Template));

      for i := OldPosition to length(Filter) - 1 do
      begin
        if Filter[i] = ' ' then
        begin
          break;
        end;
      end;

      Delete(Filter, oldPosition, i - oldPosition);
    end;
  end;

begin

  DataSet.DisableControls;

  Template := FieldName + ' = ';  

  if DataSet.Filtered then
  begin
    Filter := DataSet.Filter;

    Filter := DelRSpace(Filter);

    if Filter <> '' then
    begin
      Begining := Copy(Filter, 0, length(FieldName));

      if FieldName <> Begining then
      begin
        Template := ' and ' + FieldName + ' = ';
      end;

      DeleteSubString;
    end;

  end else
  begin
    Filter := '';
  end;

  DataSet.Filtered := false;

  if KeyValue <> -1 then
  begin
    Filter := Filter + Template + VarToStr(KeyValue);
  end;

  DataSet.Filter := Filter;

  if Filter <> '' then
  begin
    try
      DataSet.Filtered := true;
    except
      On E:Exception do
      begin
        ShowMessage(E.Message);
      end; 
    end;
  end;

  DataSet.EnableControls;

end;

procedure TdmMain.taWOrderListNewRecord(DataSet: TDataSet);
begin
  taWOrderListWORDERID.AsInteger := dm.GetId(2);
  taWOrderListMOLID.AsInteger := dm.User.UserId;
  taWOrderListDOCNO.AsInteger := dm.GetId(7);
  taWOrderListDOCDATE.AsDateTime := Today;
  taWOrderListDEPID.AsInteger := dm.CurrDep;
  taWOrderListISCLOSE.AsInteger := 0;
end;

procedure TdmMain.PostBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure TdmMain.taWOrderListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
    if dm.ShowKindDoc = -1  then
    begin
      ByName['SHOWKINDDOC1'].AsInteger := -MAXINT;
      ByName['SHOWKINDDOC2'].AsInteger := MAXINT;
    end
    else begin
      ByName['SHOWKINDDOC1'].AsInteger := dm.ShowKindDoc;
      ByName['SHOWKINDDOC2'].AsInteger := dm.ShowKindDoc;
    end;
    if dm.CurrDep = -1 then
    begin
      ByName['Dep1'].AsInteger := -MAXINT;
      ByName['Dep2'].AsInteger := MAXINT;
    end
    else begin
      ByName['Dep1'].AsInteger := dm.CurrDep;
      ByName['Dep2'].AsInteger := dm.CurrDep;
    end;
  end;
end;

procedure TdmMain.taWOrderBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['WORDERID'].AsInteger := taWOrderListWORDERID.AsInteger;
end;

procedure TdmMain.taWOrderNewRecord(DataSet: TDataSet);
begin

  if dm.ReadOnly then
    raise Exception.Create('�� �� ������ ���� �� ��������� ������!'+#13+'���������� � ��������������');
  if taWOrderListJOBID.IsNull then raise Exception.Create('�������� �����������!');
  if taWOrderListDEFOPER.IsNull then raise Exception.Create('�������� �������� �� ���������!');
  if taWOrderListJOBDEPID.IsNull then raise Exception.Create('�������� ����� ��������');

  taWOrderWOITEMID.AsInteger := dm.GetId(3);
  taWOrderWORDERID.AsInteger := taWOrderListWORDERID.AsInteger;
  taWOrderOPERID.AsString := taWOrderListDEFOPER.AsString;

  taWOrderQ.AsFloat := 0;
  taWOrderW.AsFloat := 0;
  taWOrderITTYPE.AsInteger := 0;
  taWOrderFROMOPERID.AsVariant := Null;
  case InvMode of
    0 : begin
          taWOrderINVID.AsVariant := Null;
          taWOrderDOCDATE.AsDateTime := ToDay;
          taWorderDOCNO.AsInteger := dm.GetId(9);
    end;
    1 : begin
          if CurrInv = -1 then
          begin
            taWOrderDOCDATE.AsDateTime := ToDay;
            taWorderDOCNO.AsInteger := dm.GetId(9);
            taWOrderINVID.AsVariant := Null;
          end
          else begin
            taWOrderDOCNO.AsInteger := ExecSelectSQL('select DocNo from Inv where InvId = '+
              IntToStr(FCurrInv), quTmp);
            taWOrderDOCDATE.AsDateTime := ExecSelectSQL('select DocDate from Inv where InvId = ' +
              IntToStr(FCurrInv), quTmp);
            taWOrderINVID.AsInteger := CurrInv;
          end
    end;
  end;
  taWOrderE.AsInteger := 0;
  taWOrderITDATE.AsDateTime := taWOrderDOCDATE.AsDateTime;
  (* ����� �������� � �������� *)
  taWOrderDEPID.AsInteger := dm.User.wWhId; //taWOrderListDEPID.AsInteger;
  taWOrderJOBDEPID.AsInteger := taWOrderListJOBDEPID.AsInteger;
  (* ��� *)
  taWOrderUSERID.AsInteger := dm.User.UserId;
  taWOrderJOBID.AsInteger := taWOrderListJOBID.AsInteger;
  taWOrderUQ.AsInteger := -1;
  taWOrderUW.AsInteger := -1;
  taWOrderPROTOCOLID.AsVariant := Null;
end;

procedure TdmMain.taWOrderBeforeInsert(DataSet: TDataSet);
var
  s : string;
begin
  PostDataSets([taWOrderList]);
  if(taWOrderListISCLOSE.AsInteger = 1)then
    raise Exception.Create(Format(rsWOrderClose, [taWOrderListDOCNO.AsString]));

  if(taWOrderListDOCDATE.AsDateTime <= dm.PsBeginDate) then
  begin
    case dm.PsTypeBeginDate of
      0: s := '�� �������� ��������������';
      1: s := '�� ������������ ���������';
    end;
    raise Exception.Create(Format(rsPeriodClose, [dmMain.taWOrderJOBDEPNAME.AsString, DateToStr(dm.PsBeginDate), s]));
  end;
end;

function TdmMain.GetIdByField(Table, Fn, FnId: string; Value: Variant): Variant;
begin
  quTmp.Close;
  case VarType(Value) of
    varSmallint, varInteger, varSingle, varDouble,
    varCurrency, varBoolean, varVariant, varByte :
        quTmp.SQL.Text := 'select '+FnId+' from '+Table+' where '+Fn+'='+Value
  else quTmp.SQL.Text := 'select '+FnId+' from '+Table+' where '+Fn+'='#39+Value+#39;
  end;
  quTmp.ExecQuery;
  Result := quTmp.Fields[0].AsVariant;
  quTmp.Close;
end;

procedure TdmMain.DelConf(DataSet: TDataSet);
begin
  if dm.ReadOnly then Exception.Create('�� �� ����� ���� �� ��������� ������!'+#13+
    '���������� � ��������������');
  if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then SysUtils.Abort;
end;

procedure TdmMain.ITTYPESetText(Sender: TField; const Text: String);
begin
  if Text = '������' then Sender.AsInteger := 0;
  if Text = '�����' then Sender.AsInteger := 1;
  if Text = '�������' then Sender.AsInteger := 3;
  if Text = '����' then Sender.AsInteger := 5;
  if Text = '���������' then Sender.AsInteger := 6;
  if Text = '����������' then Sender.AsInteger := 7;
end;

procedure TdmMain.taWOJobBeforeInsert(DataSet: TDataSet);
begin
  PostDataSets([taWOrderList]);
  if taWOrderListISCLOSE.AsInteger = 1 then raise Exception.Create('����� ������');
end;

procedure TdmMain.taWOrderAfterPost(DataSet: TDataSet);
begin
  CommitRetaining(DataSet);
  if(Screen.ActiveForm.ClassType = TfmWOrder)then TfmWOrder(Screen.ActiveForm).SetCurrInv;
  if(Screen.ActiveForm.ClassType = TfmWOrderRej)then TfmWOrderRej(Screen.ActiveForm).SetCurrInv;
  if(Screen.ActiveForm.ClassType = TfmProtocol_PS) then TfmProtocol_PS(Screen.ActiveForm).SetCurrInv;
  FQChange := False;
  FWChange := False;
end;

procedure TdmMain.taWOrderAfterDelete(DataSet: TDataSet);
begin
  case FWorderMode of
    wmWOrder : begin
      if(Screen.ActiveForm.ClassType = TfmWOrder)then TfmWOrder(Screen.ActiveForm).SetCurrInv;
      if(Screen.ActiveForm.ClassType = TfmProtocol_PS) then TfmProtocol_PS(Screen.ActiveForm).SetCurrInv;
    end;
    wmRej : if(Screen.ActiveForm.ClassType = TfmWOrderRej) then TfmWOrderRej(Screen.ActiveForm).SetCurrInv;
  end;
  taWOrder.Transaction.CommitRetaining;
end;


procedure TdmMain.taCalcLossAfterOpen(DataSet: TDataSet);
begin
  taWOrderList.Refresh;
end;

procedure TdmMain.quWOrder_TBeforeOpen(DataSet: TDataSet);
begin
  dm.SetPeriodParams(TpFIBDataSet(DataSet).Params);
  with TpFIBDataSet(DataSet).Params do
    begin
      ByName['WORDERID'].AsInteger := taWOrderListWORDERID.AsInteger;
      if dm.User.wWhId in [250,251] then
        ByName['Material'].AsInteger := 3
      else
        ByName['Material'].AsInteger := 1;
    end;

    {showmessage(TpFIBDataSet(DataSet).ParamByName('WORDERID').AsString);
    showmessage(TpFIBDataSet(DataSet).ParamByName('BD').AsString);
    showmessage(TpFIBDataSet(DataSet).ParamByName('ED').AsString);}
end;

procedure TdmMain.quWOrderList_TBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
    if dm.CurrDep = -1 then
    begin
      ByName['DEPID1'].AsInteger := -MAXINT;
      ByName['DEPID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['DEPID1'].AsInteger := dm.CurrDep;
      ByName['DEPID2'].AsInteger := dm.CurrDep;
    end;
    if dm.ShowKindDoc = -1  then
    begin
      ByName['SHOWCLOSED1'].AsInteger := -MAXINT;
      ByName['SHOWCLOSED2'].AsInteger := MAXINT;
    end
    else begin
      ByName['SHOWCLOSED1'].AsInteger := dm.ShowKindDoc;
      ByName['SHOWCLOSED2'].AsInteger := dm.ShowKindDoc;
    end;
  end;
end;

procedure TdmMain.FillList(List: TStrings);
var nd: TNodeData;
    s: string[30];

  procedure LoadSL(SQLText: string);
  begin
    with quTmp, List do
      begin
        SQL.Text:=SQLText;
          ExecQuery;
          while NOT EOF do
            begin
              nd:=TNodeData.Create;
              s:=DelRSpace(Fields[1].AsString);
              nd.Name:=s;
              nd.Code:=DelRSpace(Fields[0].AsString);
              AddObject(s, nd);
              Next;
            end;
          Close;
        end;
  end;

begin
  List.Clear;
  LoadSL('SELECT OperId, Operation FROM D_Oper order by OperId');
end;

procedure TdmMain.taPListNewRecord(DataSet: TDataSet);
begin
  if FilterMatId = ROOT_MAT_CODE then raise Exception.Create('�� ������ ��������');
  taPListID.AsInteger := dm.GetId(10);
  taPListUSERID.Value := dm.User.UserId;
  taPListDOCDATE.Value := dm.EndDate;
  taPListBD.Value := dm.BeginDate;
  taPListDocNo.Value := dm.GetId(17);
  taPListISCLOSE.AsInteger := 0;
  {$ifdef OLD_PROTOCOL}
  taPListT.AsInteger := 0;
  {$else}
  taPListT.AsInteger := 1;
  taPListA.AsInteger := 1;
  {$endif}
  taPListMATID.AsString := FilterMatId;
  taPListUPD.AsInteger := 0;
end;

procedure TdmMain.CalcRecNo(DataSet: TDataSet);
begin
  DataSet.FieldByName('RecNo').Value := DataSet.RecNo;
end;

procedure TdmMain.taProtocolNewRecord(DataSet: TDataSet);
begin
  taProtocolID.AsInteger := dm.GetId(8);
  taProtocolACTID.AsInteger := taPListID.AsInteger;
end;

procedure TdmMain.taActPGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text := ''
  else case Sender.AsInteger of
    0 : Text := '�������� ������������';
    1 : Text := '� ���� ��������';
    else Text := '';
  end
end;

procedure TdmMain.taActPSetText(Sender: TField; const Text: String);
begin
  if Text = '�������� ������������' then Sender.AsInteger := 0
  else if Text = '� ���� ��������' then Sender.AsInteger := 1
  else Sender.Value := Null;
end;

procedure TdmMain.taProtocolBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['ACTID'].AsInteger := taPListID.AsInteger;
  end;
end;

procedure TdmMain.taPListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(StartOfAYear(DocListYear));
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(EndOfAYear(DocListYear));
    if FilterMatId = ROOT_MAT_CODE then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := FilterMatId;
      ByName['MATID2'].AsString := FilterMatId;
    end;
    dm.SetSelfCompParams(taPList.Params);
  end;
end;

procedure TdmMain.taZListNewRecord(DataSet: TDataSet);
begin
  taZListID.AsInteger := dm.GetId(70);
  taZListUSERID.Value := dm.User.UserId;
  taZListBD.Value := dm.zBeginDate;
  taZListDOCDATE.Value := dm.zEndDate;
  taZListDocNo.Value := dm.GetId(18);
end;

procedure TdmMain.taZListBeforeOpen(DataSet: TDataSet);
begin
  taZList.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(StartOfAYear(DocListYear));
  taZList.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(EndOfAYear(DocListYear));
  dm.SetSelfCompParams(taZList.Params);
end;

procedure TdmMain.taProtocolCalcFields(DataSet: TDataSet);
begin
  DataSet.FieldByName('RecNo').Value := DataSet.RecNo;
  if not taProtocolID.IsNull then taProtocolOUT1.AsFloat := taProtocolS.AsFloat -
     taProtocolWZ.AsFloat
  else taProtocolOUT1.AsFloat := taProtocolOUT.AsFloat;
  taProtocolOUT_PS1.AsFloat := taProtocolOUT_PS.AsFloat;
end;

procedure TdmMain.taZItemCalcFields(DataSet: TDataSet);
begin
  taZItemTZP.AsFloat := taZItemZP.AsFloat + (taZItemNR.AsFloat*taZItemZP.AsFloat)/100+
    taZItemNP.AsFloat - taZItemU.AsFloat + taZItemNR2.AsFloat;
  taZItemEP1.AsFloat := taZItemEP.AsFloat;
end;

procedure TdmMain.ChangePeriod(BD, ED: TDateTime);
begin
  if dm.BeginDate <> BD then dm.BeginDate := BD;
  if dm.EndDate <> ED then dm.EndDate := ED;
end;

procedure TdmMain.taPSMolBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    if(dm.WorkMode = 'aDIEl')then ByName['DEPID'].AsInteger := taDIListFROMDEPID.AsInteger
    else ByName['DEPID'].AsInteger := taWOrderListJOBDEPID.AsInteger;
end;

procedure TdmMain.ITTYPEGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           0: Text := '������';
           1: Text := '�����';
           3: Text := '�������';
           5: Text := '����';
           6: Text := '���������';
           7: Text := '����������';
         end;
end;

procedure TdmMain.taZItemBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ZPID'].AsInteger := taZListID.AsInteger;
end;

procedure TdmMain.taZItemNewRecord(DataSet: TDataSet);
begin
  taZItemID.AsInteger := dm.GetId(71);
  taZItemZPID.AsInteger := taZListID.AsInteger;
  taZItemZP.AsFloat := 0;
  taZItemU.AsFloat := 0;
  taZItemNP.AsFloat := 0;
  taZItemNR.AsFloat := 0;
  taZItemNR2.AsFloat := 0;
  taZItemDONE.AsFloat := 0;
  taZItemE.AsFloat := 0;
  taZItemS.AsFloat := 0;
  taZItemDOC.AsInteger := 1;
  taZItemARTK.AsFloat := 100;
  taZItemZPDONE.AsFloat := 0;
end;

procedure TdmMain.ChangeAPeriod(BD, ED: TDateTime);
begin
  if dm.aBeginDate <> BD  then dm.aBeginDate := BD;
  if dm.aEndDate <> ED then dm.aEndDate := ED;
end;

procedure TdmMain.ChangeWhPeriod(BD, ED: TDateTime);
begin
  if dm.whBeginDate <> BD  then dm.whBeginDate := BD;
  if dm.whEndDate <> ED then dm.whEndDate := ED;
end;

var
  BlockAInvDel : boolean;

procedure TdmMain.taWOrderBeforeDelete(DataSet: TDataSet);
var
  s : string;
  RefFlag : boolean;
  RefCount : integer;
  BookMark : Pointer;
  DepartmentID: Integer;
  EnableDelete: Boolean;
begin
  if(taWOrderListISCLOSE.AsInteger = 1)then raise Exception.Create(Format(rsWOrderClose, [taWOrderListDOCNO.AsString]));

  EnableDelete := False;

  if dm.IsAdm then EnableDelete := True
  else
  begin
    DepartmentID := taWOrderDEPID.AsInteger;
    if DepartmentID = dm.User.wWhId then EnableDelete := True;
  end;

  if not EnableDelete then
  raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '��� ������ � '+taWOrderDEPNAME.AsString]));

  if(taWOrderITDATE.AsDateTime <= dm.PsBeginDate) then
  begin
    case dm.PsTypeBeginDate of
      0: s := '�� �������� ��������������';
      1: s := '�� ������������ ���������';
    end;
    raise Exception.Create(Format(rsPeriodClose, [dmMain.taWOrderJOBDEPNAME.AsString, DateToStr(dm.PsBeginDate), s]));
  end;
  RefFlag := ExecSelectSQL('select count(*) from WOItem where Ref='+taWOrderWOITEMID.AsString, quTmp) > 0;
  BlockAInvDel := False;
  if RefFlag then
  begin
    s := '�� ��������� ������� ������ ��������� ������ ������� ������ (�����).'+#13+
         '�������� �������� �� ����� �������� ����������� �������?';
    case MessageDialog(s, mtConfirmation, [mbYes, mbNo], 0) of
      mrYes : begin
        // ��������
        taTmp.Close;
        taTmp.SelectSQL.Text := 'select WOITEMID from WOITEM where REF='+taWOrderWOITEMID.AsString;
        taTmp.Open;
        BookMark := taWorder.GetBookMark;
        while not taTmp.Eof do
        begin
          if taWorder.Locate('WOITEMID', taTmp.Fields[0].AsInteger , []) then
          begin
            BlockAInvDel := True;
            taWorder.Delete;
            BlockAInvDel := False;
          end
          else ExecSQL('delete from WOItem where WOItemId='+taTmp.Fields[0].AsString, quTmp);
          taTmp.Next;
        end;
        taTmp.Close;
        taTmp.Transaction.CommitRetaining;
        taWOrder.GotoBookmark(BookMark);
        if(not taWOrderAINV.IsNull)or(not taWOrderAPPLINV.IsNull)then
        begin
          // ������� ��������� ������������
          if not taWOrderAINV.IsNull then ExecSQL('delete from Inv where InvId='+taWOrderAINV.AsString, quTmp);
          // ������� ��������� ������
          if not taWOrderAPPLINV.IsNull then ExecSQL('delete from Inv where InvId='+taWOrderAPPLINV.AsString, quTmp);
        end;
      end;
      else SysUtils.Abort;
    end;
  end;
  if(not BlockAInvDel)then
  begin
    (* ��������� ��������� ������ ��� ���������� Inv.Ref = woItem.Id and Inv.WOrderId=woItem.wOrderId *)
    RefCount := ExecSelectSQL('select count(*) from Inv where Ref='+taWOrderWOITEMID.AsString+' and WOrderId='+
       dmMain.taWOrderWORDERID.AsString, dmMain.quTmp);
    if (RefCount <> 0)or(not taWOrderAINV.IsNull)or(not taWOrderAPPLINV.IsNull)then
    begin
      s := '�� ��������� �������� ��������� ������ ����������(�) ���������(��) ������������. '#13+'�������?';
      case MessageDialog(s, mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
         mrYes : begin
           // ������� ��������� ������������
           if not taWOrderAINV.IsNull then
             ExecSQL('delete from Inv where InvId='+taWOrderAINV.AsString, quTmp);
           if not taWOrderAPPLINV.IsNull then
             ExecSQL('delete from Inv where InvId='+taWOrderAPPLINV.AsString, quTmp);
           ExecSQL('delete from Inv where Ref='+taWOrderWOITEMID.AsString+' and WOrderId='+dmMain.taWOrderWORDERID.AsString, quTmp);
         end;
         mrCancel : begin
            SysUtils.Abort;
         end;
         mrNo : ; // ������ �� ������
      end;
    end;
  end;
end;

procedure TdmMain.taPListBeforeDelete(DataSet: TDataSet);
begin
  if dm.ReadOnly then Exception.Create('�� �� ����� ���� �� ��������� ������!'+#13+
      '���������� � ��������������');
  if MessageDialog('������� �������� ��������������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmMain.taWOrderAfterOpen(DataSet: TDataSet);
begin
  FQChange := False;
  FWChange := False;
  if (dm.WorkMode = 'wOrder')or(dm.WorkMode = 'wOrderRej') then
  begin
    if not (taWOrderAPPLINV.IsNull and taWOrderAINV.IsNull)
    then ReOpenDataSets([taAssortInv, taAssortEl])
    else CloseDataSets([taAssortInv, taAssortEl]);
  end
end;

procedure TdmMain.taWOrderAfterScroll(DataSet: TDataSet);
begin
  FQChange := False;
  FWChange := False;
  if(dm.WorkMode = 'wOrder')or(dm.WorkMode = 'wOrderRej') then
  begin
    //ShowMessage('������: '+ taWOrderAPPLINV.AsString+ ' ; ��������� ' + taWOrderAINV.AsString);
    if not (taWOrderAPPLINV.IsNull and taWOrderAINV.IsNull)
    then ReOpenDataSets([taAssortInv, taAssortEl])
    else CloseDataSets([taAssortInv, taAssortEl]);
  end
end;

procedure TdmMain.ChangeZPeriod(BD, ED: TDateTime);
begin
  if dm.zBeginDate <> BD  then dm.zBeginDate := BD;
  if dm.zEndDate <> ED then dm.zEndDate := ED;
end;

procedure TdmMain.taProtocolBeforeEdit(DataSet: TDataSet);
begin
  if dmMain.taProtocolID.IsNull then SysUtils.Abort;
end;

procedure TdmMain.taPsOperBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['DEPID'].AsInteger := taWOrderListJOBDEPID.AsInteger;
end;

procedure TdmMain.taSIListBeforeOpen(DataSet: TDataSet);
begin
  taSIList.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.whBeginDate);
  taSIList.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.whEndDate);
  dm.SetDepParams(TpFIBDataSet(DataSet).Params);
  taSIList.ParambyName('ITYPE_').AsInteger := 13;

  if FSupname=ROOT_KNAME_CODE then
  begin
    taSIList.ParamByName('SUPNAME1').AsString :=Low(Char);
    taSIList.ParamByName('SUPNAME2').AsString :=High(char);
  end
  else begin
    taSIList.ParamByName('SUPNAME1').AsString :=FSupname;
    taSIList.ParamByName('SUPNAME2').AsString :=FSupname;
  end;
end;



procedure TdmMain.taSIListNewRecord(DataSet: TDataSet);
begin
  taSIListINVID.AsInteger := dm.GetId(12);
  taSIListDOCNO.AsInteger := dm.GetId(31);
  taSIListDOCDATE.AsDateTime := Today;
  taSIListUSERID.AsInteger := dm.User.UserId;
  taSIListISCLOSE.AsInteger := 0;

  taSIListDEPID.AsInteger := dm.CurrDep;

  taSIListNDSID.AsInteger := -1;
  taSIListITYPE.AsInteger:= 13;
  taSIListFIO.AsString:=dm.User.FIO;
  taSIListCONTRACTID.AsInteger := dm.Rec.DefaultContractId;
  taSIListUE.AsFloat := 0;
end;

procedure TdmMain.taSIListPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  DataSet.Cancel;
end;

procedure TdmMain.taSIElCalcFields(DataSet: TDataSet);
var
  r : Variant;
begin
  if taSIElSEMIS.AsString <> '' then
  begin
    r := ExecSelectSQL('select UQ, UW from D_Semis where SemisId='#39+taSIElSEMIS.AsString+#39, quTmp);
    try taSIElUQ.AsString := r[0] except end;
    try taSIElUW.AsString := r[1]; except end;
  end;
  DataSet.FieldByName('RecNo').Value := DataSet.RecNo;
end;

procedure TdmMain.taSIElNewRecord(DataSet: TDataSet);
begin
  taSIElID.AsInteger := dm.GetId(42);
  taSIElINVID.AsInteger := taSIListINVID.AsInteger;
  taSIElQ.AsFloat := 0;
  taSIElW.AsFloat := 0;
  taSIElTPRICE.AsFloat := 0;
  taSIElPRICE.AsFloat := 0;
  taSIElCOST.AsFloat := 0;
  taSIElNDS.AsFloat := 0;
  taSIElNDSID.AsInteger := taSIListNDSID.AsInteger;
  taSIElFOR_Q_U.AsInteger := 0;
end;

procedure TdmMain.taSIElBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(Dataset).Params do
    ByName['INVID'].asInteger := taSIListINVID.AsInteger;
end;

procedure TdmMain.UQGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
          -1 : Text:='���';
           0 : Text:='��';
           1 : Text:='����';
         end;
end;

procedure TdmMain.UWGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
          -1 : Text:='���';
           3 : Text:='��';
           4 : Text:='�����';
         end;
end;

procedure TdmMain.taWhSemisAfterOpen(DataSet: TDataSet);
//var i : integer;
begin
  {for i := 0 to taWhSemis.Params.Count - 1 do
    begin
      ShowMessage(taWhSemis.Params[i].Name + '  ' + VarToStr(taWhSemis.Params[i].AsVariant));
    end;}
end;

procedure TdmMain.taWhSemisBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    if(dm.WorkMode = 'wOrder')then
    begin
      ByName['OPERID'].AsString := taWOrderListDEFOPER.AsString;
      if Screen.ActiveForm.InheritsFrom(TfmWOrder) then
        case TfmWOrder(Screen.ActiveForm).ItType of
          0, 6    : ByName['DEPID'].AsInteger := dm.User.wWhId;// taWOrderListDEPID.AsInteger;
          1,3,5,7 : ByName['DEPID'].AsInteger := taWOrderListJOBDEPID.AsInteger;
          else raise EInternal.Create(Format(rsInternalError, ['101']));
        end
      else if Screen.ActiveForm.InheritsFrom(TfmProtocol_PS) then
        case TfmProtocol_PS(Screen.ActiveForm).ItType of
          0 : ByName['DEPID'].AsInteger := dm.User.wWhId;
          3 : ByName['DEPID'].AsInteger := taWOrderListJOBDEPID.AsInteger;
          else raise EInternal.Create(Format(rsInternalError, ['101']));
        end
      else ByName['DEPID'].AsInteger := dm.User.wWhId;

      if Screen.ActiveForm.InheritsFrom(TfmWOrder) then
        if(TfmWOrder(Screen.ActiveForm).ItType in [0,1,2,3,4,5,6,7])
        then ByName['T'].AsShort := TfmWOrder(Screen.ActiveForm).ItType
        else ByName['T'].AsShort := 0
      else if Screen.ActiveForm.InheritsFrom(TfmProtocol_PS) then
        if(TfmProtocol_PS(Screen.ActiveForm).ItType in [0,1,2,3,4,5,6,7])
        then ByName['T'].AsShort := TfmProtocol_PS(Screen.ActiveForm).ItType
        else ByName['T'].AsShort := 0;
    end
    else if(dm.WorkMode = 'wOrderRej')then
    begin
       ByName['OPERID'].AsVariant := Null;
      //ByName('OPERID').AsString := dm.RejOperId;
      if Screen.ActiveForm.InheritsFrom(TfmWOrderRej) then
      begin
        case TfmWOrderRej(Screen.ActiveForm).ItType of
          0, 6    : ByName['DEPID'].AsInteger := dm.User.wWhId;// taWOrderListDEPID.AsInteger;
          1,3,5,7 : ByName['DEPID'].AsVariant := Null; //taWOrderListJOBDEPID.AsInteger;
          else raise EInternal.Create(Format(rsInternalError, ['101']));
        end;
        ByName['T'].AsShort := -TfmWOrderRej(Screen.ActiveForm).ItType-1;
      end
      else begin
        ByName['DEPID'].AsInteger := dm.User.wWhId;//taWOrderListDEPID.AsInteger;
        ByName['T'].AsShort := 0-1;
      end;
    end
    else raise EInternal.Create(Format(rsInternalError, ['102']));

    if(dm.AMatId = ROOT_MAT_CODE)then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := dm.AMatId;
      ByName['MATID2'].AsString := dm.AMatId;
    end;
    if(FilterOperId = ROOT_OPER_CODE)then ByName['FILTEROPERID'].AsString := ROOT_OPER_CODE
    else if(FilterOperId = sNoOperation)then ByName['FILTEROPERID'].AsVariant := Null
    else ByName['FILTEROPERID'].AsString := FilterOperId;
  end;
end;

procedure TdmMain.taWOrderQChange(Sender: TField);
var
  b : boolean;
begin
  if not taWhSemis.Active then eXit;
  if not(Sender.DataSet.State in [dsInsert]) then
    if not FQChange then
    begin
      FQChange := True;
      FWChange := False;
      if taWOrderFROMOPERID.IsNull then b := taWhSemis.Locate('SEMISID', taWOrderSEMIS.AsString, [])
      else b := taWhSemis.Locate('SEMISID;OPERID', VarArrayOf([taWOrderSEMIS.AsString, taWOrderFROMOPERID.AsString]), []);

      // ItType ����� ������ ������ �.�. ����� ����
      if b then
        case TfmWOrder(Screen.ActiveForm).ItType of // c���� ��������������
          0,6 : case taWOrderITTYPE.AsInteger of
            0,6 : begin
              FWhSemisQValue := taWhSemisQ.AsInteger; // �� ������
              FInvQValue := Sender.OldValue; // ���� �� ���������
              if not (taWHSemis.State in [dsEdit, dsInsert]) then taWHSemis.Edit;
              taWhSemisQ.AsInteger := FWhSemisQValue + (FInvQValue - Sender.NewValue);
            end;
            1,3,5 : begin
              FWhSemisQValue := taWhSemisQ.AsInteger; // �� ������
              FInvQValue := Sender.OldValue; // ���� �� ���������
              if not (taWHSemis.State in [dsEdit, dsInsert]) then taWHSemis.Edit;
              taWhSemisQ.AsInteger := FWhSemisQValue - (Sender.NewValue - FInvQValue);
            end;
          end;
          1,3,5 : case taWOrderITTYPE.AsInteger of
            0,6 : begin
              FWhSemisQValue := taWhSemisQ.AsInteger; // �� ������
              FInvQValue := Sender.OldValue; // ���� �� ���������
              if not (taWHSemis.State in [dsEdit, dsInsert]) then taWHSemis.Edit;
              taWhSemisQ.AsInteger := FWhSemisQValue - (FInvQValue - Sender.NewValue);
            end;
            1,3,5 : begin
              FWhSemisQValue := taWhSemisQ.AsInteger; // �� ������
              FInvQValue := Sender.OldValue; // ���� �� ���������
              if not (taWHSemis.State in [dsEdit, dsInsert]) then taWHSemis.Edit;
              taWhSemisQ.AsInteger := FWhSemisQValue + (Sender.NewValue - FInvQValue);
            end;
          end;
      end;
    end
    else
      if taWhSemis.Locate('SEMISID', taWOrderSEMIS.AsString, [])then
      begin
        if not(taWhSemis.State in [dsEdit, dsInsert]) then taWhSemis.Edit;
        case TfmWOrder(Screen.ActiveForm).ItType of // c���� ��������������
          0,6 : case taWOrderITTYPE.AsInteger of
            0,6   : taWHSemisQ.AsInteger := FWhSemisQValue + (FInvQValue - Sender.NewValue);
            1,3,5 : taWHSemisQ.AsInteger := FWhSemisQValue - (Sender.NewValue - FInvQValue);
          end;
          1,3,5 : case taWOrderITTYPE.AsInteger of
            0,6   : taWHSemisQ.AsInteger := FWhSemisQValue - (FInvQValue - Sender.NewValue);
            1,3,5 : taWHSemisQ.AsInteger := FWhSemisQValue + (Sender.NewValue - FInvQValue);
          end;
        end;
      end
end;

procedure TdmMain.taWOrderWChange(Sender: TField);
begin
  if not (taWhSemis.Active)then eXit;
  if not(Sender.DataSet.State in [dsInsert]) then
    if not FWChange then
    begin
      FWChange := True;
      FQChange := False;
      if taWhSemis.Locate('SEMISID', taWOrderSEMIS.AsString, [])then
        case TfmWOrder(Screen.ActiveForm).ItType of // c���� ��������������
          0,6 : case taWOrderITTYPE.AsInteger of
            0,6 : begin
                FWhSemisWValue := taWhSemisW.AsFloat; // �� ������
                FInvWValue := Sender.OldValue; // ���� �� ���������
                if not (taWHSemis.State in [dsEdit, dsInsert]) then taWHSemis.Edit;
                taWhSemisW.AsFloat := FWhSemisWValue + (FInvWValue - Sender.NewValue);
            end;
            1,3,5 : begin
               FWhSemisWValue := taWhSemisW.AsFloat; // �� ������
               FInvWValue := Sender.OldValue; // ���� �� ���������
               if not (taWHSemis.State in [dsEdit, dsInsert]) then taWHSemis.Edit;
               taWhSemisW.AsFloat := FWhSemisWValue - (FInvWValue - Sender.NewValue);
            end;
          end;
          1,3,5 : case taWOrderITTYPE.AsInteger of
            0,6 : begin
              FWhSemisWValue := taWhSemisW.AsFloat; // �� ������
              FInvWValue := Sender.OldValue; // ���� �� ���������
              if not (taWHSemis.State in [dsEdit, dsInsert]) then taWHSemis.Edit;
              taWhSemisW.AsFloat := FWhSemisWValue - (FInvWValue - Sender.NewValue);
            end;
            1,3,5 : begin
              FWhSemisWValue := taWhSemisW.AsFloat; // �� ������
              FInvWValue := Sender.OldValue; // ���� �� ���������
              if not (taWHSemis.State in [dsEdit, dsInsert]) then taWHSemis.Edit;
              taWhSemisW.AsFloat := FWhSemisWValue + (FInvWValue - Sender.NewValue);
            end;
          end;
        end;
    end
    else
      if taWhSemis.Locate('SEMISID', taWOrderSEMIS.AsString, [])then
      begin
        if not(taWhSemis.State in [dsEdit, dsInsert]) then taWhSemis.Edit;
        case TfmWOrder(Screen.ActiveForm).ItType of // c���� ��������������
          0,6 : case taWOrderITTYPE.AsInteger of
            0,6 : taWHSemisW.AsInteger := FWhSemisWValue + (FInvWValue - Sender.NewValue);
            1,3,5 : taWHSemisW.AsFloat := FWhSemisWValue - (FInvWValue - Sender.NewValue);
          end;
          1,3,5 : case taWOrderITTYPE.AsInteger of
            0,6 : taWHSemisW.AsInteger := FWhSemisWValue - (FInvWValue - Sender.NewValue);
            1,3,5 : taWHSemisW.AsFloat := FWhSemisWValue + (FInvWValue - Sender.NewValue);
          end;
        end;
      end;
end;

procedure TdmMain.taSIElTPRICEChange(Sender: TField);
var
  Cost: double;
begin
  if(Sender.DataSet.State in [dsInsert, dsEdit])then
  begin
    if dm.taNDS.Locate('NDSID', taSIListNDSID.AsInteger, []) then
    begin
      Cost := fif(taSIElFOR_Q_U.AsInteger=0, Sender.AsFloat*taSIElW.AsFloat, Sender.AsFloat*taSIElQ.AsInteger);
      taSIElCOST.AsFloat := Cost;
      taSIELNDS.AsFloat := dm.CalcNDS(Cost, dm.taNDSNDS.AsFloat, dm.taNDST.AsInteger);
      if(dm.taNDST.AsInteger=0)then taSIElCOST.AsFloat := Cost+taSIElNDS.AsFloat;
    end;
  end;
end;

procedure TdmMain.SICheckClose(DataSet: TDataSet);
begin
{  if taSIListISCLOSE.AsInteger=1 then
    raise Exception.Create('��������� �������');}
end;

procedure TdmMain.taASListBeforeOpen(DataSet: TDataSet);
begin
  taASList.ParambyName('ITYPE_').ASInteger := 4;
  taASList.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.whBeginDate);
  taASList.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.whEndDate);
  dm.SetDepParams(TpFIBDataSet(DataSet).Params);
end;

procedure TdmMain.taASListNewRecord(DataSet: TDataSet);
begin
  taASListINVID.AsInteger := dm.GetId(12);
  taASListDOCNO.AsInteger := dm.GetId(20);
  taASListDOCDATE.AsDateTime := dm.ServerDateTime;
  taASListUSERID.AsInteger := dm.User.UserId;
  taASListFIO.AsString:=dm.User.FIO;
  taASListShipedBy.AsInteger := dm.User.UserId;
  taASListNDSID.AsInteger := -1;
  taASListISCLOSE.AsInteger := 0;
  taASListITYPE.AsInteger := 4;
  taASListDEPID.AsInteger := dm.CurrDep;
  taASListFROMDEPID.AsInteger := dm.CurrFromDep;
  taASListCONTRACTID.AsInteger := dm.Rec.DefaultProdContractId;
  taASListINVCOLORID.AsVariant := Null;
end;

procedure TdmMain.taASItemBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := taASListINVID.AsInteger;
end;

procedure TdmMain.taASItemNewRecord(DataSet: TDataSet);
begin
  taASItemID.AsInteger := dm.GetId(29);
  taASItemINVID.AsInteger := taASListINVID.AsInteger;
  taASItemARTID.AsInteger := dmAppl.taWHApplARTID.AsInteger;
  taASItemART2ID.AsInteger := dmAppl.taWHApplART2ID.AsInteger;
  taASItemSZID.AsInteger := dmAppl.taWHApplSZID.AsInteger;
  taASItemOPERID.AsString := dmAppl.taWHApplOPERID.AsString;
  taASItemS_OPERID.AsString := dm.LastOperId;
  taASItemW.AsFloat := dmAppl.ADistrW;
  taASItemUID.AsInteger := dm.GetId(22);
  taASItemP0.AsInteger := 1;
  taASItemUA_OLD.AsInteger := dmAppl.taWHApplU.AsInteger;
  taASItemUA.AsInteger := dmAppl.ADistrNewU;
  {if(dmAppl.taWHApplU.AsInteger = dmAppl.ADistrNewU)then taASItemUA_OLD.AsInteger := 1
  else if(dmAppl.taWHApplU.AsInteger<dmAppl.ADistrNewU)then taASItemUA_OLD.AsInteger := dmAppl.ADistrNewU
  else raise Exception.Create('�� ���������� ��������');}
end;

procedure TdmMain.UGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  case Sender.AsInteger of
    1 : Text := '��.';
    2 : Text := '����';
    else Text := '�� ����������'
  end;
end;

procedure TdmMain.ASICheckClose(DataSet: TDataSet);
begin
  if(taASListISCLOSE.AsInteger = 1)then
    raise Exception.Create(Format(rsInvClose, [taASListDOCNO.AsString]));
end;

procedure TdmMain.taASItemCalcFields(DataSet: TDataSet);
begin
  if(taASItemARTU.AsInteger = 0)then taASItemCOST.AsFloat := taASItemPRICE.AsFloat
  else taASItemCOST.AsFloat := taASItemPRICE.AsFloat * taASItemW.AsFloat;
end;

procedure TdmMain.taWhArtBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    if FilterDepId = ROOT_PS_CODE then
    begin
      ByName['DEPID1'].AsInteger := -MAXINT;
      ByName['DEPID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['DEPID1'].AsInteger := StrToIntDef(FilterDepId, -MAXINT);
      ByName['DEPID2'].AsInteger := StrToIntDef(FilterDepId, MAXINT);
    end;

    if FilterOperId = ROOT_OPER_CODE then
    begin
      ByName['OPERID1'].AsString := MINSTR;
      ByName['OPERID2'].AsString := MAXSTR;
    end
    else begin
      ByName['OPERID1'].AsString := FilterOperId;
      ByName['OPERID2'].AsString := FilterOperId;
    end;
    ByName['ART'].AsString := FilterArt;

    //dm.SetSelfCompParams(TpFIBDataSet(DataSet).Params);
  end;
end;

procedure TdmMain.taWhSBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    if FilterDepId = ROOT_PS_CODE then
    begin
      ByName['DEPID1'].AsInteger := -MAXINT;
      ByName['DEPID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['DEPID1'].AsInteger := StrToIntDef(FilterDepId, -MAXINT);
      ByName['DEPID2'].AsInteger := StrToIntDef(FilterDepId, MAXINT);
    end;

    if FilterOperId = ROOT_OPER_CODE then
    begin
      ByName['OPERID1'].AsString := MINSTR;
      ByName['OPERID2'].AsString := MAXSTR;
    end
    else if(FilterOperId = sNoOperation) then
    begin
      ByName['OPERID1'].AsVariant := Null;
      ByName['OPERID2'].AsVariant := Null;
    end
    else begin
      ByName['OPERID1'].AsString := FilterOperId;
      ByName['OPERID2'].AsString := FilterOperId;
    end;

    if FilterSemisId = ROOT_SEMIS_CODE then
    begin
      ByName['SEMISID1'].AsString := MINSTR;
      ByName['SEMISID2'].AsString := MAXSTR;
    end
    else begin
      ByName['SEMISID1'].AsString := FilterSemisId;
      ByName['SEMISID2'].AsString := FilterSemisId;
    end;

    if FilterMatId = ROOT_MAT_CODE then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := FilterMatId;
      ByName['MATID2'].AsString := filterMatId;
    end;

  end;
end;

procedure TdmMain.taASItemPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  Action := daFail;
  DataSet.Cancel;
end;

procedure TdmMain.taProtocol_dBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ACTID'].AsInteger := taPListID.AsInteger;
end;

procedure TdmMain.taProtocol_dBeforeEdit(DataSet: TDataSet);
begin
  if dmMain.taProtocol_dID.IsNull then SysUtils.Abort;
  if(dmMain.taPListISCLOSE.AsInteger = 1) then EWarning.Create('�������� ������');
end;

procedure TdmMain.taPIt_InSemis_dBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['PROTOCOLID'].AsInteger := taProtocol_dID.AsInteger;
    ByName['PSID'].AsInteger := taProtocol_dPSID.AsInteger;
    ByName['OPERID'].AsString := taProtocol_dOPERID.AsString;
    if(FilterMatId = ROOT_MAT_CODE)then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := FilterMatId;
      ByName['MATID2'].AsString := FilterMatId;
    end;
  end;
end;

procedure TdmMain.taDIListBeforeOpen(DataSet: TDataSet);
begin
  taDIList.ParambyName('ITYPE_').ASInteger := 14;
  taDIList.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.whBeginDate);
  taDIList.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.whEndDate);
  dm.SetDepParams(TpFIBDataSet(DataSet).Params);
end;

procedure TdmMain.taDIListNewRecord(DataSet: TDataSet);
begin
  taDIListINVID.AsInteger := dm.GetId(12);
  taDIListDOCNO.AsInteger := dm.GetId3(1, dm.CurrDep, dm.CurrFromDep);
  taDIListDOCDATE.AsDateTime := Today;
  taDIListUSERID.AsInteger := dm.User.UserId;
  taDIListFIO.AsString:=dm.User.FIO;
  taDIListNDSID.AsInteger := -1;
  taDIListISCLOSE.AsInteger := 0;
  taDIListITYPE.AsInteger := 14;
  taDIListDEPID.AsInteger := dm.CurrDep;
  taDIListFROMDEPID.AsInteger := dm.CurrFromDep;
end;

procedure TdmMain.DICheckClose(DataSet: TDataSet);
begin
  if taDIListISCLOSE.AsInteger = 1 then raise Exception.Create('��������� �������');
end;

procedure TdmMain.taDIElBeforeInsert(DataSet: TDataSet);
begin
  if taSIListISCLOSE.AsInteger=1 then
    raise Exception.Create('��������� �������');
end;

procedure TdmMain.taDIElBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(Dataset).Params do
    ByName['INVID'].AsInteger := taDIListINVID.AsInteger;
end;

procedure TdmMain.taDIElNewRecord(DataSet: TDataSet);
begin
  taDIElID.AsInteger := dm.GetId(42);
  taDIElINVID.AsInteger := taDIListINVID.AsInteger;
  taDIElQ.AsInteger := SemisQ;
  taDIElW.AsFloat := SemisW;
  taDIElT.AsInteger := 1;
  taDIElTPRICE.AsFloat := 0;
  taDIElPRICE.AsFloat := 0;
  taDIElCOST.AsFloat := 0;
  taDIElNDS.AsFloat := 0;
  taDIElNDSID.AsInteger := taDIListNDSID.AsInteger;
  taDIElFROMOPERID.AsString := taDWHSemisOPERID.AsString;
  taDIElFROMOPERNAME.AsString := taDWHSemisOPERNAME.AsString;
end;

procedure TdmMain.taHWhArtITYPEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  case Sender.AsInteger of
    -1 : Text := 'C���.���������';
    -2 : Text := '�������� �����.';
     4 : Text := '��������';
     6 : Text := '�����. �����������';
     7 : Text := '������';
     8 : Text := '����� � ������';
     9 : Text := '�������� � ������';
    12 : Text := '������ �� ������������';
  end;
end;

procedure TdmMain.taHWhArtBeforeOpen(DataSet: TDataSet);
begin
  with taHWhArt.Params do
  begin
    ByName['USERID'].AsInteger := dm.User.UserId;
    ByName['ART2ID'].AsInteger := hArt2Id;
    if(hSzId = ROOT_SZ_CODE)then
    begin
      ByName['SZID1'].AsInteger := -MAXINT;
      ByName['SZID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['SZID1'].AsInteger := hSzId;
      ByName['SZID2'].AsInteger := hSzId;
    end;
  end;
end;

procedure TdmMain.taDWHSemisBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['DEPID1'].AsInteger := dm.CurrDep;
    ByName['DEPID2'].AsInteger := dm.CurrDep;
    ByName['DEPFROMID1'].AsInteger := dm.CurrFromDep;
    ByName['DEPFROMID2'].AsInteger := dm.CurrFromDep;

    if(FilterOperId = ROOT_OPER_CODE)then
    begin
      ByName['OPERID1'].AsString := MINSTR;
      ByName['OPERID2'].AsString := MAXSTR;
    end
    else if FilterOperId = sNoOperation then
    begin
      ByName['OPERID1'].AsVariant := Null;
      ByName['OPERID2'].AsVariant := Null;
    end
    else begin
      ByName['OPERID1'].AsString := FilterOperId;
      ByName['OPERID2'].AsString := FilterOperId;
    end;
    ByName['SEMISID1'].AsString := MINSTR;
    ByName['SEMISID2'].AsString := MAXSTR;

    if(FilterMatId = ROOT_MAT_CODE)then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := FilterMatId;
      ByName['MATID2'].AsString := filterMatId;
    end;
  end;
end;

procedure TdmMain.taDIElPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  CancelDataSet(DataSet);
  Action := daFail;
end;

procedure TdmMain.taDWHSemisNewRecord(DataSet: TDataSet);
begin
  taDWHSemisID.AsInteger := DWhSemisNewId;
end;

procedure TdmMain.taABufferBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['USERID'].AsInteger := dm.User.UserId;
  end;
end;

procedure TdmMain.taPIt_InSemis_dCalcFields(DataSet: TDataSet);
begin
  taPIt_InSemis_dQ.AsInteger := taPIt_InSemis_dIQ.AsInteger+taPIt_InSemis_dINQ.AsInteger;
  taPIt_InSemis_dW.AsFloat := taPIt_InSemis_dIW.AsFloat+taPIt_InSemis_dINW.AsFloat;
  taPIt_InSemis_dRQ.AsInteger := taPIt_InSemis_dIQ.AsInteger+taPIt_InSemis_dINQ.AsInteger - taPIt_InSemis_dFQ.AsInteger;
  taPIt_InSemis_dRW.AsFloat := taPIt_InSemis_dIW.AsFloat+taPIt_InSemis_dINW.AsFloat - taPIt_InSemis_dFW.AsFloat;
end;

procedure TdmMain.taPIt_InSemis_dBeforeEdit(DataSet: TDataSet);
begin
  if(dmMain.taPListISCLOSE.AsInteger = 1) then EWarning.Create('�������� ������');
end;

procedure TdmMain.taASItemUA_OLDGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if(taASItemUA.AsInteger = taASItemUA_OLD.AsInteger)then Text := taASItemUA.DisplayText
  else case taASItemUA_OLD.AsInteger of
    1 : Text := '��.';
    2 : Text := '����';
  end;
end;

procedure TdmMain.taPIt_InSemis_dNewRecord(DataSet: TDataSet);
begin
  taPIt_InSemis_dID.AsInteger := dm.GetId(68);
  taPIt_InSemis_dPROTOCOLID.AsInteger := taProtocol_dID.AsInteger;
  taPIt_InSemis_dPSID.AsInteger := taProtocol_dPSID.AsInteger;
  taPIt_InSemis_dPSNAME.AsString := ExecSelectSQL('select Name from D_Dep where '+
   ' DepId='+taProtocol_dPSID.AsString, quTmp);
  taPIt_InSemis_dOPERID.AsString := taProtocol_dOPERID.AsString;
  taPIt_InSemis_dOPERNAME.AsString := ExecSelectSQL('select Operation from D_Oper '+
    ' where OperId='#39+taProtocol_dOPERID.AsString+#39, quTmp);
  taPIt_InSemis_dINQ.AsInteger := 0;
  taPIt_InSemis_dINW.AsFloat := 0;
  taPIt_InSemis_dGETQ.AsInteger := 0;
  taPIt_InSemis_dGETW.AsFloat := 0;
  taPIt_InSemis_dREWORKQ.AsInteger := 0;
  taPIt_InSemis_dREWORKW.AsFloat := 0;
  taPIt_InSemis_dDONEQ.AsInteger := 0;
  taPIt_InSemis_dDONEW.AsFloat := 0;
  taPIt_InSemis_dRETQ.AsInteger := 0;
  taPIt_InSemis_dRETW.AsFloat := 0;
  taPIt_InSemis_dREJQ.AsInteger := 0;
  taPIt_InSemis_dREJW.AsFloat := 0;
  taPIt_InSemis_dNKQ.AsInteger := 0;
  taPIt_InSemis_dNKW.AsFloat := 0;
  taPIt_InSemis_dIQ.AsInteger := 0;
  taPIt_InSemis_dIW.AsFloat := 0;
  taPIt_InSemis_dFQ.AsInteger := 0;
  taPIt_InSemis_dFW.AsFloat := 0;
  taPIt_InSemis_dRW.AsFloat := 0;
  taPIt_InSemis_dRQ.AsFloat := 0;
  taPIt_InSemis_dIQ.AsFloat := 0;
  taPIt_InSemis_dIW.AsFloat := 0;
  taPIt_InSemis_dQ0.AsInteger := 1;
end;

procedure TdmMain.taSBufferBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['USERID'].AsInteger := dm.User.UserId;
  end;
end;

procedure TdmMain.taAssortInvAfterOpen(DataSet: TDataSet);
begin
  //ShowMessage('���������: ' + taAssortInvAINV.AsString)
end;

procedure TdmMain.taAssortInvBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['WOITEMID'].AsInteger := taWOrderWOITEMID.AsInteger;
end;

procedure TdmMain.taAssortElBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['WOITEMID'].AsInteger := taWOrderWOITEMID.AsInteger;
end;

procedure TdmMain.taSOListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.whBeginDate);
    ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.whEndDate);
    ParamByName('ITYPE_').AsInteger := 15;

  if FSupname=ROOT_KNAME_CODE then
  begin
    ParamByName('SUPNAME1').AsString :=Low(Char) ;
    ParamByName('SUPNAME2').AsString :=High(char) ;
  end
  else begin
    ParamByName('SUPNAME1').AsString :=FSupname;
    ParamByName('SUPNAME2').AsString :=FSupname;
  end;
    dm.SetDepParams(Params);
  end;
end;

procedure TdmMain.taSOListNewRecord(DataSet: TDataSet);
begin
  taSOListINVID.AsInteger := dm.GetId(12);
  taSOListDEPID.AsInteger := dm.CurrDep;
  //taSOListDOCDATE.OnChange := nil;
  //try
    taSOListDOCDATE.AsDateTime := dm.ServerDateTime;
  //finally
  //  taSOListDOCDATE.OnChange := taSOListDOCDATEChange;
  //end;
  taSOListDOCNO.AsInteger := dm.GetId(36);
  taSOListUSERID.AsInteger := dm.User.UserId;
  taSOListISCLOSE.AsInteger := 0;
  taSOListNDSID.AsInteger := -1;
  taSOListITYPE.AsInteger:= 15;
  taSOListFIO.AsString:=dm.User.FIO;
  taSOListCONTRACTID.AsInteger := dm.rec.DefaultContractId;
end;

procedure TdmMain.SOCheckClose(DataSet: TDataSet);
begin
  if(taSOListISCLOSE.AsInteger = 1) then raise Exception.Create('��������� �������');
end;


procedure TdmMain.taProtSJInfoBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['PSID'].AsInteger := taPIt_InSemis_dPSID.AsInteger;
    ByName['OPERID'].AsString := taPIt_InSemis_dOPERID.AsString;
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(taPListBD.AsDateTime);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(taPListDOCDATE.AsDateTime);
    ByName['SEMISID'].AsString := taPIt_InSemis_dSEMISID.AsString;
  end;
end;

procedure TdmMain.taDIElFROMOPERNAMEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if taDIElFROMOPERID.IsNull then Text := sNoOperation else Text := taDIElFROMOPERNAME.AsString;
end;

procedure TdmMain.taDIElOPERNAMEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if taDIElOPERID.IsNull then Text := sNoOperation else Text := taDIElOPERNAME.AsString;
end;

procedure TdmMain.taMolIOBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['MOLID'].AsInteger := dm.User.UserId;
  end;
end;

procedure TdmMain.taHWhArtFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (taHWhArtDOCDATE.AsDateTime >= hWhArtBd) and
            (taHWhArtDOCDATE.AsDateTime <= hWhArtEd);
end;

procedure TdmMain.taProtWhItemTGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  case Sender.AsInteger of
    1 : Text := '������';
    2 : Text := '������';
    3 : Text := '��';
    4 : Text := 'MX';
    5 : Text := '��� ��������';
    6 : Text := '����������� ������ (����)';
    7 : Text := '����������� ����';
    else Text := ''
  end;
end;

procedure TdmMain.taProtWhItemT1GetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  case taProtWhItemT.AsInteger of
    1 : case Sender.AsInteger of
          1 : Text := '����������� ����������';
          2 : Text := '������� ������� ���������';
          else Text := '';
        end;
    2 : case Sender.AsInteger of
          1 : Text := '������ ����������';
          2 : Text := '�������� ������� ���������';
          else Text := '';
        end;
    3 : case Sender.AsInteger of
          1 : Text := '����';
          2 : Text := '������';
          else Text := '';
        end;
    4 : case Sender.AsInteger of
          0 : Text := '������';
          1 : Text := '�����';
          3 : Text := '�������';
          5 : Text := '����';
          6 : Text := '���������';
          7 : Text := '����������';
          else Text := '';
        end;
    7 : case Sender.AsInteger of
          6 : Text := '������';
          7 : Text := '�������';
          else Text := '';
        end;
     else Text := ''; 
  end;
end;

procedure TdmMain.taProtWhItemBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['ACTID'].AsInteger := taVListID.AsInteger;
    ByName['DEPID'].AsInteger := taVListDEPID.AsInteger;
    ByName['SEMISID'].AsVariant := dmInv.taSemisReportSEMISID.AsVariant;
    ByName['OPERID'].AsVariant := dmInv.taSemisReportOPERID.AsVariant;
    ByName['UW'].AsShort := dmInv.taSemisReportUW.AsInteger;
    ByName['UQ'].AsShort := dmInv.taSemisReportUQ.AsInteger;
  end;
end;

procedure TdmMain.taVListBeforeDelete(DataSet: TDataSet);
begin
  if dm.ReadOnly then raise Exception.Create(rsReadOnly);
  if(taVListISCLOSE.AsInteger = 1)then raise EWarning.Create(Format(rsVSheetClose, [taVListDOCNO.AsString]));
  if not((dm.User.Profile = -1)or(dm.User.Profile = 3))then
    if(dmMain.taVListUSERID.AsInteger <> dm.User.UserId) then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '��� �������� ������������ ��������� �������� '+dmMain.taVListDEPNAME.AsString]));
  if MessageDialog('������� ������������ ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmMain.taVListBeforeOpen(DataSet: TDataSet);
begin
  taVList.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(StartOfAYear(DocListYear));
  taVList.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(EndOfAYear(DocListYear));
end;

procedure TdmMain.taVListNewRecord(DataSet: TDataSet);
begin
  taVListID.AsInteger := dm.GetId(10);
  taVListDOCNO.Value := dm.GetId(38);
  taVListDOCDATE.Value := EndOfTheDay(dm.vEndDate);
  taVListBD.Value := dm.vBeginDate;
  taVListISCLOSE.AsInteger := 0;
  taVListT.AsInteger := 2;
  taVListUSERID.Value := dm.User.UserId;
  taVListDEPID.AsInteger := dm.CurrDep;
  taVListA.AsInteger := 1;
  taVListBUH.AsInteger := 0;
  taVListCREATED.AsDateTime := Now;
end;

procedure TdmMain.ChangeVPeriod(BD, ED: TDateTime);
begin
  if dm.vBeginDate <> BD  then dm.vBeginDate := BD;
  if dm.vEndDate <> ED then dm.vEndDate := ED;
end;

procedure TdmMain.taPListCalcFields(DataSet: TDataSet);
begin
  taPListED.AsDateTime := taPListDOCDATE.AsDateTime;
end;

procedure TdmMain.taASListBeforeDelete(DataSet: TDataSet);
begin
  if dm.ReadOnly then raise Exception.Create(rsReadOnly);
  dm.DepBeginDate := dm.GetDepBeginDate(dmMain.taASListFROMDEPID.AsInteger);
  if(dm.DepBeginDate > taASListDOCDATE.AsDateTime)then
    raise Exception.Create(Format(rsPeriodClose, [taASListFROMDEPNAME.AsString, DateToStr(dm.DepBeginDate), '�� ������������ ���������']));
  if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmMain.taWOrderAfterInsert(DataSet: TDataSet);
var
  s : string;
begin
  PostDataSets([taWOrderList]);
  if(taWOrderListISCLOSE.AsInteger = 1)then
    raise Exception.Create(Format(rsWOrderClose, [taWOrderListDOCNO.AsString]));
  if(taWOrderDEPID.AsInteger <> dm.User.wWhId) then
    raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '��� ������ � '+taWOrderDEPNAME.AsString]));
  if taWOrderListISCLOSE.AsInteger = 1 then
    raise Exception.Create(Format(rsWOrderClose, [taWOrderListDOCNO.AsString]));
  if(taWOrderITDATE.AsDateTime < dm.PsBeginDate) then
  begin
    case dm.PsTypeBeginDate of
      0: s := '�� �������� ��������������';
      1: s := '�� ������������ ���������';
    end;
    raise Exception.Create(Format(rsPeriodClose, [dmMain.taWOrderJOBDEPNAME.AsString, DateToStr(dm.PsBeginDate), s]));
  end;
end;

procedure TdmMain.taDIListBeforeDelete(DataSet: TDataSet);
var
  FromDepBD, DepBD : TDateTime;
  t : byte;
  s : string;
begin
  (* �������� ������� *)
  FromDepBD := dm.GetDepBeginDate(taDIListFROMDEPID.AsInteger);
  DepBD := dm.GetDepBeginDate(taDIListDEPID.AsInteger);
  if(FromDepBd > DepBd)then
  begin
    dm.DepBeginDate := FromDepBd;
    t := 0;
  end
  else if(FromDepBd < DepBd)then
  begin
    dm.DepBeginDate := DepBd;
    t := 1;
  end
  else begin
    dm.DepBeginDate := DepBd;
    t := 2;
  end;
  if(dm.DepBeginDate > taDIListDOCDATE.AsDateTime)then
  begin
    case t of
      0: s := taDIListFROMDEPNAME.AsString;
      1: s := taDIListDEPNAME.AsString;
      2: s := taDIListDEPNAME.AsString+', '+taDIListFROMDEPNAME.AsString;
    end;
    raise Exception.Create(Format(rsPeriodClose, [s, DateToStr(dm.DepBeginDate), '�� ������������ ���������']));
  end;
  if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmMain.taPSMOL1BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    if(dm.WorkMode = 'aDIEl')then ByName['DEPID'].AsInteger := taDIListDEPID.AsInteger;
end;

procedure TdmMain.taDIListDOCDATEChange(Sender: TField);
var
  FromDepBD, DepBD : TDateTime;
  t : byte;
  s : string;
begin
  (* �������� ������� *)
  FromDepBD := dm.GetDepBeginDate(taDIListFROMDEPID.AsInteger);
  DepBD := dm.GetDepBeginDate(taDIListDEPID.AsInteger);
  if(FromDepBd > DepBd)then
  begin
    dm.DepBeginDate := FromDepBd;
    t := 0;
  end
  else if(FromDepBd < DepBd)then
  begin
    dm.DepBeginDate := DepBd;
    t := 1;
  end
  else begin
    dm.DepBeginDate := DepBd;
    t := 2;
  end;
  if(dm.DepBeginDate > taDIListDOCDATE.AsDateTime)then
  begin
    case t of
      0: s := taDIListFROMDEPNAME.AsString;
      1: s := taDIListDEPNAME.AsString;
      2: s := taDIListDEPNAME.AsString+', '+taDIListFROMDEPNAME.AsString;
    end;
    MessageDialog(Format(rsPeriodClose, [s, DateToStr(dm.DepBeginDate), '�� ������������ ���������']), mtError, [mbYes], 0);
    SysUtils.Abort;
  end;
end;

procedure TdmMain.taSIListBeforeDelete(DataSet: TDataSet);
begin
  if(dmMain.taSIListISCLOSE.AsInteger = 1) then
    EWarning.Create(Format(rsInvClose, [dmMain.taSIListDOCNO.AsString]));
  (* �������� ������� *)
  dm.DepBeginDate := dm.GetDepBeginDate(taSIListDEPID.AsInteger);
  if(dm.DepBeginDate > taSIListDOCDATE.AsDateTime)then
    raise Exception.Create(Format(rsPeriodClose, [taSIListDEPNAME.AsString, DateToStr(dm.DepBeginDate), '�� ������������ ���������']));
  if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;

end;

procedure TdmMain.taSIListDOCDATEChange(Sender: TField);
begin
  dm.DepBeginDate := dm.GetDepBeginDate(taSIListDEPID.AsInteger);
  if(dm.DepBeginDate > taSIListDOCDATE.AsDateTime)then
  begin
    MessageDialog(Format(rsPeriodClose, [taSIListDEPNAME.AsString, DateToStr(dm.DepBeginDate), '�� ������������ ���������']), mtError, [mbOk], 0);
    SysUtils.Abort;
  end;
end;

procedure TdmMain.taSOListBeforeDelete(DataSet: TDataSet);
begin
  if dm.ReadOnly then raise Exception.Create(rsReadOnly);
  if(taSOListISCLOSE.AsInteger = 1)then raise Exception.Create(Format(rsInvClose, [taSOListDOCNO.AsString]));
  (* �������� ������� *)
  dm.DepBeginDate := dm.GetDepBeginDate(taSOListDEPID.AsInteger);
  if(dm.DepBeginDate > taSOListDOCDATE.AsDateTime)then
    raise Exception.Create(Format(rsPeriodClose, [taSOListDEPNAME.AsString, DateToStr(dm.DepBeginDate), '�� ������������ ���������']));
  if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmMain.taSOListDOCDATEChange(Sender: TField);
begin
  dm.DepBeginDate := dm.GetDepBeginDate(taSOListDEPID.AsInteger);
  if(dm.DepBeginDate > taSOListDOCDATE.AsDateTime)then
  begin
    MessageDialog(Format(rsPeriodClose, [taSOListDEPNAME.AsString, DateToStr(dm.DepBeginDate), '�� ������������ ���������']), mtError, [mbOk], 0);
    SysUtils.Abort;
  end;
end;

procedure TdmMain.taZListCalcFields(DataSet: TDataSet);
begin
  taZListED.AsDateTime := taZListDOCDATE.AsDateTime;
end;

procedure TdmMain.taZListBeforeDelete(DataSet: TDataSet);
begin
  if dm.ReadOnly then Exception.Create('�� �� ������ ���� �� ��������� ������!'+#13+'���������� � ��������������');
  if MessageDialog('������� ��������� �� ��������?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then SysUtils.Abort;
end;

procedure TdmMain.taWOrderListBeforeDelete(DataSet: TDataSet);
begin
  if dm.ReadOnly then Exception.Create(rsReadOnly);
  (* ��������� ����� ������� *)
  if(dm.User.UserId <> taWOrderListMOLID.AsInteger)then
    raise EWarning.Create(Format(rsAccessDenied, [dm.User.FIO, '��� �������� ������ �� '+taWOrderListDEPNAME.AsString]));
  if MessageDialog('������� �����?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then SysUtils.Abort;
end;

procedure TdmMain.taZ_AssemblyBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ZPID'].AsInteger := taZListID.AsInteger;
end;

procedure TdmMain.taVArtBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['VITEMID'].AsInteger := dmInv.taSemisReportID.AsInteger;
end;

procedure TdmMain.taVArtNewRecord(DataSet: TDataSet);
begin
  taVArtID.AsInteger := dm.GetId(67);
  taVArtVITEMID.AsInteger := dmInv.taSemisReportID.AsInteger;
  taVArtDEPID.AsInteger := dmInv.taSemisReportDEPID.AsInteger;
  taVArtDEPNAME.AsString := dmInv.taSemisReportDEPNAME.AsString;
  taVArtOPERID.AsString := dmInv.taSemisReportOPERID.AsString;
  taVArtOPERNAME.AsString := dmInv.taSemisReportOPERATION.AsString;
  taVArtU.AsInteger := 1; //��.
  taVArtQ.AsInteger := 0;
  taVArtFQ.AsInteger := 0;
  taVArtREST_IN_Q.AsInteger := 0;
  taVArtIN7_Q.AsInteger := 0;
  taVArtIN9_Q.AsInteger := 0;
  taVArtMOVE_IN_Q.AsInteger := 0;
  taVArtMOVE_OUT_Q.AsInteger := 0;
  taVArtOUT_Q.AsInteger := 0;
  taVArtRET_PROD_Q.AsInteger := 0;
  taVArtPROD_Q.AsInteger := 0;
end;

procedure TdmMain.taVArtCalcFields(DataSet: TDataSet);
begin
  taVArtRQ.AsInteger := taVArtQ.AsInteger - taVArtFQ.AsInteger;
end;

procedure TdmMain.USetText(Sender: TField; const Text: String);
begin
  if(Text = '��.')then Sender.AsInteger := 1
  else if(Text = '����')then Sender.AsInteger := 2
  else Sender.Clear;
end;

procedure TdmMain.taVArtBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmMain.taBulkListBeforeDelete(DataSet: TDataSet);
begin
  if dm.ReadOnly then raise Exception.Create(rsReadOnly);
  if(taBulkListISCLOSE.AsInteger = 1)then raise Exception.Create(Format(rsInvClose, [taBulkListDOCNO.AsString]));
  (* �������� ������� *)
  dm.DepBeginDate := dm.GetDepBeginDate(taBulkListDEPID.AsInteger);
  if(dm.DepBeginDate > taBulkListDOCDATE.AsDateTime)then
    raise Exception.Create(Format(rsPeriodClose, [taBulkListDEPNAME.AsString, DateToStr(dm.DepBeginDate), '�� ������������ ���������']));
  if MessageDialog('������� ��� ����������� (������������) ����������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmMain.taBulkListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.whBeginDate);
    ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.whEndDate);
    ParamByName('ITYPE_').AsInteger := 16;
    dm.SetDepParams(Params);
  end;
end;

procedure TdmMain.taBulkListNewRecord(DataSet: TDataSet);
begin
  taBulkListINVID.AsInteger    := dm.GetId(12);
  taBulkListDOCDATE.AsDateTime := Today;
  taBulkListDOCNO.AsInteger    := dm.getId(37);
  taBulkListUSERID.AsInteger   := dm.User.UserId;
  taBulkListISCLOSE.AsInteger  := 0;
  taBulkListDEPID.AsInteger    := dm.CurrDep;
  taBulkListNDSID.AsInteger    := -1;
  taBulkListITYPE.AsInteger    := 16;
  taBulkListFIO.AsString       := dm.User.FIO;
end;

procedure TdmMain.taBulkCalcFields(DataSet: TDataSet);
begin
  taBulkRW.AsFloat := taBulkW.AsFloat - taBulkW0.AsFloat;
end;

procedure TdmMain.BulkCheckClose(DataSet: TDataSet);
begin
  if(taBulkListISCLOSE.AsInteger = 1) then
    raise Exception.Create(Format(rsInvClose, [taBulkListDOCNO.AsString]));
end;

procedure TdmMain.taBulkBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(Dataset).Params do
    ByName['INVID'].AsInteger := taBulkListINVID.AsInteger;
end;

procedure TdmMain.taBulkNewRecord(DataSet: TDataSet);
begin
  taBulkID.AsInteger := dm.GetId(42);
  taBulkINVID.AsInteger := taBulkListINVID.AsInteger;
  taBulkQ.AsFloat  := 0;
  taBulkW.AsFloat  := 0;
  taBulkW0.AsFloat := 0;
  taBulkQ0.AsFloat := 0;
  taBulkTPRICE.AsFloat := 0;
  taBulkPRICE.AsFloat := 0;
  taBulkCOST.AsFloat := 0;
end;

procedure TdmMain.taASListDOCDATEValidate(Sender: TField);
begin
  dm.DepBeginDate := dm.GetDepBeginDate(dmMain.taASListFROMDEPID.AsInteger);
  if(dm.DepBeginDate > VarToDateTime(Sender.NewValue))then
    raise Exception.Create(Format(rsPeriodClose, [taASListFROMDEPNAME.AsString, DateToStr(dm.DepBeginDate), '�� ������������ ���������']));
end;

procedure TdmMain.taPArtBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmMain.taPArtBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['DEPID'].AsInteger := taProtocol_dPSID.AsInteger;
    ByName['ACTID'].AsInteger := taPListID.AsInteger;
  end;
end;

procedure TdmMain.taPArtCalcFields(DataSet: TDataSet);
begin
  taPArtRQ.AsInteger := taPArtQ.AsInteger - taPArtFQ.AsInteger;
end;

procedure TdmMain.taPArtNewRecord(DataSet: TDataSet);
begin
  taPArtID.AsInteger := dm.GetId(69);
  taPArtACTID.AsInteger := taPListID.AsInteger;
  taPArtPSID.AsInteger := taProtocol_dPSID.AsInteger;
  taPArtDEPNAME.AsString := taProtocol_dPSNAME.AsString;
  taPArtOPERID.AsString := taProtocol_dOPERID.AsString;
  taPArtOPERNAME.AsString := taProtocol_dOPERNAME.AsString;
  taPArtOPERATIONNAME.AsString := taProtocol_dOPERNAME.AsString;
  taPArtU.AsInteger := 1; //��.
  taPArtREST_IN_Q.AsInteger := 0;
  taPArtGET_Q.AsInteger := 0;
  taPArtDONE_Q.AsInteger := 0;
  taPArtREJ_Q.AsInteger := 0;
  taPArtQ.AsInteger := 0;
  taPArtFQ.AsInteger := 0;
end;

procedure TdmMain.taASItemDeleteError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  Action := daFail;
end;

procedure TdmMain.taZpAssortCalcFields(DataSet: TDataSet);
begin
  taZpAssortKT.AsFloat := taZpAssortARTK.AsFloat + taZpAssortK.AsFloat;
end;

procedure TdmMain.taZpAssortBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ZITEMID'].AsInteger := taZItemID.AsInteger;
end;

procedure TdmMain.taBulkBeforePost(DataSet: TDataSet);
begin
  if(taBulkQ.AsInteger <> taBulkQ0.AsInteger)then
    raise EWarning.Create('���-�� �� ����������� ������ ���� ������ ���-�� ����� �����������');
end;

procedure TdmMain.taAFListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
  end;
end;

procedure TdmMain.taProtocol_dFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := (not taProtocol_dID.IsNull)and(not taProtocol_dOPERID.IsNull)and(taProtocol_dMATID.AsString='1');
end;

procedure TdmMain.taVArtARTValidate(Sender: TField);
var
  ArtId : Variant;
begin
  if not dm.CheckValidArt(Sender.AsString) then raise Exception.Create(rsInvalidArtFormat);
  ArtId := ExecSelectSQL('select D_ArtId from D_Art where Art='#39+Sender.AsString+#39, quTmp);
  if VarIsNull(ArtId)or(ArtId = 0)then taVArtARTID.AsInteger := dm.InsertArt(Sender.AsString)
  else taVArtARTID.AsInteger := ArtId;
end;

procedure TdmMain.taVArtART2Validate(Sender: TField);
var
  Art2Id : Variant;
begin
  if taVArtARTID.IsNull then raise Exception.Create(rsInputArt);
  if not dm.CheckValidArt2(Sender.AsString) then raise Exception.Create('������������ ������ ���2');
  Art2Id := ExecSelectSQL('select Art2Id from Art2 where ART2='#39+Sender.AsString+#39' and D_ARTID='+taVArtARTID.AsString, quTmp);
  if VarIsNull(Art2Id)or(Art2Id = 0) then raise Exception.Create('���2 '+Sender.AsString + ' ��� � ��.')
  else taVArtART2ID.AsInteger := Art2Id;
end;

procedure TdmMain.taVArtSZNAMEValidate(Sender: TField);
var
  SzId : Variant;
begin
  if Sender.AsString = '15,5' then SzId := 0
  else begin
    SzId := ExecSelectSQL('select ID from D_Sz where NAME='#39+Sender.AsString+#39, quTmp);
    if VarIsNull(SzId)or(SzId=0)then raise Exception.Create('������� ������ ������ ��� ������ ������� ��� � �����������');
  end;
  taVArtSZID.AsInteger := SzId;
end;

procedure TdmMain.taPArtARTValidate(Sender: TField);
var
  ArtId : Variant;
begin
  if not dm.CheckValidArt(Sender.AsString) then raise Exception.Create(rsInvalidArtFormat);
  ArtId := ExecSelectSQL('select D_ArtId from D_Art where Art='#39+Sender.AsString+#39, quTmp);
  if VarIsNull(ArtId)or(ArtId = 0)then taPArtARTID.AsInteger := dm.InsertArt(Sender.AsString)
  else taPArtARTID.AsInteger := ArtId;
end;

procedure TdmMain.taPArtART2Validate(Sender: TField);
var
  Art2Id : Variant;
begin
  if taPArtARTID.IsNull then raise Exception.Create(rsInputArt);
  if not dm.CheckValidArt2(Sender.AsString) then raise Exception.Create('������������ ������ ���2');
  Art2Id := ExecSelectSQL('select Art2Id from Art2 where Art2='#39+Sender.AsString+#39' and D_ArtId='+taPArtARTID.AsString, quTmp);
  if VarIsNull(Art2Id)or(Art2Id = 0) then raise Exception.Create('���2 '+Sender.AsString + ' ��� � ��.')
  else taPArtART2ID.AsInteger := Art2Id;
end;

procedure TdmMain.taPArtSZNAMEValidate(Sender: TField);
var
  SzId : Variant;
begin
  if Sender.AsString = '15,5' then SzId := 0
  else begin
    SzId := ExecSelectSQL('select ID from D_Sz where Name='#39+Sender.AsString+#39, quTmp);
    if VarIsNull(SzId)or(SzId=0)then raise Exception.Create('������� ������ ������ ��� ������ ������� ��� � �����������');
  end;
  taPArtSZID.AsInteger := SzId;
end;

procedure TdmMain.taAFListNewRecord(DataSet: TDataSet);
begin
  taAFListINVID.AsInteger := dm.GetId(12);
  taAFListDOCNO.AsInteger := dm.GetId(40);
  taAFListDOCDATE.AsDateTime := ToDay;
  taAFListCOMPID.AsInteger := dm.OrganizationId;
  taAFListCOMPNAME.AsString := dm.OrganizationName;
  case AffinajMode of
    amS : taAFListITYPE.AsInteger := 17;
    amL : taAFListITYPE.AsInteger := 18;
  end;
  taAFListUSERID.AsInteger := dm.User.UserId;
  taAFListUSERNAME.AsString := dm.User.FIO;
  taAFListISCLOSE.AsInteger := 0;
  taAFListREF.AsVariant := Null;
end;

procedure TdmMain.taAFListBeforeDelete(DataSet: TDataSet);
begin
  (* ��������� ����� �� ������������ ������� ��������� *)
  if(not dm.IsAdm)then
    if(dm.User.UserId <> taAFListUSERID.AsInteger)then
      raise EWarning.Create(Format(rsAccessDenied, [dm.User.FIO, '��� �������� ���������']));
  if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then SysUtils.Abort;
end;

procedure TdmMain.taWhAffinajBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['T'].AsInteger := WhAffinajMode;
    ByName['DEPID'].AsInteger := taAFListDEPID.AsInteger;
    if(dm.AMatId = ROOT_MAT_CODE)then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := dm.AMatId;
      ByName['MATID2'].AsString := dm.AMatId;
    end;
    if(FilterOperId = ROOT_OPER_CODE)then ByName['FILTEROPERID'].AsString := ROOT_OPER_CODE
    else if(FilterOperId = sNoOperation)then ByName['FILTEROPERID'].AsVariant := Null
    else ByName['FILTEROPERID'].AsString := FilterOperId;
  end;
end;

procedure TdmMain.taAffinajBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := taAFListINVID.AsInteger;
end;

procedure TdmMain.taAffinajBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ������� �� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmMain.taAffinajNewRecord(DataSet: TDataSet);
begin
  taAffinajID.AsInteger := dm.GetId(42);
  taAffinajINVID.AsInteger := taAFListINVID.AsInteger;
  taAffinajSEMISID.AsString := '18';
  taAffinajQ.AsInteger := SemisQ;
  taAffinajW.AsFloat := SemisW;
end;

procedure TdmMain.taPListUPDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  case Sender.AsInteger of
    0 : Text := '';
    1 : Text := '���������� �������� ����������';
  end;
end;

procedure TdmMain.taAffinajTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  case Sender.AsInteger of
    4 : Text := '������';
    5 : Text := '�����';
    6 : Text := '������';
    7 : Text := '�����';
  end;
end;

procedure TdmMain.taAFListDEPIDValidate(Sender: TField);
begin
{  if(dm.WorkMode = 'Affinaj')then
    if(dmMain.taAffinaj.Active and (not dmMain.taAffinaj.IsEmpty))then
    begin
       MessageDialogA('� ��������� ��� ���� �������. ��������� �������� �� ��������', mtWarning);
      SysUtils.Abort;
    end;}
end;

procedure TdmMain.taAffinajPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  Action := daFail;
  Dataset.Cancel;
end;

function TdmMain._ShowEditorQW(Q: integer; W: double; UseWeigth : boolean) : boolean;
   (* UseWeigth - ������������ ���� ��� ��������� ���� *)
begin
  dmMain.SemisQ := Q;
  if UseWeigth then dmMain.SemisW := dmScale.GetWeigth
  else dmMain.SemisW := W;
  Result := ShowEditor(TfmeSQ, TfmEditor(fmeSQ))= mrOk;
end;

procedure TdmMain.taWOrderListBeforeEdit(DataSet: TDataSet);
begin
  if(dm.User.UserId <> taWOrderListMOLID.AsInteger)then
    raise EWarning.Create(Format(rsAccessDenied, [dm.User.FIO, '��� ��������� ������ � '+taWOrderListDEPNAME.AsString]));
end;

procedure TdmMain.taPsOperNewRecord(DataSet: TDataSet);
begin
  taPsOperID.AsInteger := dm.GetId(52);
end;

procedure TdmMain.taProtocolMatBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['ACTID1'].AsInteger := taPListID.AsInteger;
    ByName['ACTID2'].AsInteger := taPListID.AsInteger;
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(taPListBD.AsDateTime);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(taPListDOCDATE.AsDateTime);
  end;
end;

function TdmMain.CreateV_for_Buh(Id: string): boolean;
begin
  Result := False;
  Screen.Cursor := crSQLWait;
  try
    try
      ExecSQL('execute procedure Create_Semis_Report('+Id+')', quTmp);
      Result := True;
    except
      on E : Exception do begin
        MessageDialogA('��� ������������ ��������� �������� ������. '+#10#13+E.Message, mtWarning);
        Result := False;
      end;
    end;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

function TdmMain.CreateV_for_Wh(Id: string): boolean;
begin
  Result := False;
  Screen.Cursor := crSQLWait;
  try
    try
      ExecSQL('execute procedure Create_Semis_Report('+Id+')', quTmp);
      ExecSQL('execute procedure Create_VArt('+Id+')', quTmp);
      Result := True;
    except
      on E : Exception do begin
        MessageDialogA('��� ������������ ��������� �������� ������. '+#10#13+E.Message, mtWarning);
        Result := False;
      end;
    end
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TdmMain.DataModuleCreate(Sender: TObject);
var
  d, m : word;
begin
  DecodeDate(ToDay, FDocListYear, m, d);
end;

procedure TdmMain.taSChargeListBeforeDelete(DataSet: TDataSet);
begin
  if dm.ReadOnly then raise Exception.Create(rsReadOnly);
  if(taSChargeListISCLOSE.AsInteger = 1)then raise Exception.Create(Format(rsInvClose, [taSChargeListDOCNO.AsString]));
  (* �������� ������� *)
  dm.DepBeginDate := dm.GetDepBeginDate(taSChargeListDEPID.AsInteger);
  if(dm.DepBeginDate > taSChargeListDOCDATE.AsDateTime)then
    raise Exception.Create(Format(rsPeriodClose, [taSChargeListDEPNAME.AsString, DateToStr(dm.DepBeginDate), '�� ������������ ���������']));
  if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmMain.taSChargeListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet), Params do
  begin
    ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
    ParamByName('ITYPE_').AsInteger := 19;
    dm.SetDepParams(Params);
  end;
end;

procedure TdmMain.taSChargeListNewRecord(DataSet: TDataSet);
begin
  taSChargeListINVID.AsInteger := dm.GetId(12);
  taSChargeListDOCDATE.AsDateTime := Today;
  taSChargeListDOCNO.AsInteger := dm.getId(78);
  taSChargeListUSERID.AsInteger := dm.User.UserId;
  taSChargeListISCLOSE.AsInteger := 0;
  taSChargeListDEPID.AsInteger := dm.CurrDep;
  taSChargeListNDSID.AsInteger := -1;
  taSChargeListITYPE.AsInteger:= 19;
  taSChargeListFIO.AsString:=dm.User.FIO;
end;

procedure TdmMain.SChargeCheckClose(DataSet: TDataSet);
begin
  if(taSChargeListISCLOSE.AsInteger = 1) then raise EWarning.Create(Format(rsInvClose, ['']));
end;

procedure TdmMain.taSChargeBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(Dataset).Params do
    ByName['INVID'].AsInteger := taSChargeListINVID.AsInteger;
end;

procedure TdmMain.taSChargeNewRecord(DataSet: TDataSet);
begin
  taSChargeID.AsInteger := dm.GetId(42);
  taSChargeINVID.AsInteger := taSChargeListINVID.AsInteger;
  taSChargeQ.AsFloat := 0;
  taSChargeW.AsFloat := 0;
  taSChargeTPRICE.AsFloat := 0;
  taSChargePRICE.AsFloat := 0;
  taSChargeCOST.AsFloat := 0;
  taSChargeNDS.AsFloat := 0;
  taSChargeNDSID.AsInteger := taSChargeListNDSID.AsInteger;
  taSChargeFOR_Q_U.AsInteger := 0;
  taSChargeUQ.AsInteger := -1;
  taSChargeUW.AsInteger := -1;
  taSChargeDEPID.AsVariant := Null;
end;

procedure TdmMain.taSChargeWHBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    //ByName('OPERID').AsString := dm.RejOperId;
    ByName['DEPID'].AsInteger := taSChargeListDEPID.AsInteger;
    ByName['T'].AsShort := 0-1;
    if(dm.AMatId = ROOT_MAT_CODE)then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := dm.AMatId;
      ByName['MATID2'].AsString := dm.AMatId;
    end;
    if(FilterOperId = ROOT_OPER_CODE)then ByName['FILTEROPERID'].AsString := ROOT_OPER_CODE
    else if(FilterOperId = sNoOperation)then ByName['FILTEROPERID'].AsVariant := Null
    else ByName['FILTEROPERID'].AsString := FilterOperId;
  end;
end;

procedure TdmMain.taSChargeBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ������� �� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmMain.taCheckStoneBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['USERID'].AsInteger := dm.User.UserId;
    if FilterMatId = ROOT_MAT_CODE then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := FilterMatId;
      ByName['MATID2'].AsString := FilterMatId;
    end;
  end;
end;

procedure TdmMain.taCheckStoneCalcFields(DataSet: TDataSet);
begin
  taCheckStoneRQ.AsInteger := taCheckStoneRQ_1.AsInteger-taCheckStoneRQ_2.AsInteger;
  taCheckStoneRW.AsFloat := taCheckStoneRW_1.AsFloat-taCheckStoneRW_2.AsFloat;
end;

procedure TdmMain.taProtocolMatUGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
          -1 : Text:='���';
           0 : Text:='��';
           1 : Text:='����';
           3 : Text:='��';
           4 : Text:='�����';
         end;
end;

procedure TdmMain.taTmp_WhArtBeforeOpen(DataSet: TDataSet);
begin
  with taTmp_WhArt.Params do
  begin
    ByName['USERID'].AsInteger := dm.User.UserId;
    ByName['ART2ID'].AsInteger := hArt2Id;
    if(hSzId = ROOT_SZ_CODE)then
    begin
      ByName['SZID1'].AsInteger := -MAXINT;
      ByName['SZID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['SZID1'].AsInteger := hSzId;
      ByName['SZID2'].AsInteger := hSzId;
    end;
  end;
end;

procedure TdmMain.taTmp_WhArtITYPEGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  case Sender.AsInteger of
    -1 : Text := '����. �� ��������� �����.';
    -2 : Text := '����. �� ����.���������';
  end;
end;

procedure TdmMain.taAFListCalcFields(DataSet: TDataSet);
begin
  taAFListR.AsFloat := taAFListW0.AsFloat - taAFListW.AsFloat;
end;

procedure TdmMain.taInsBufferBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet) do
    ParamByName('USERID').AsInteger :=  dm.User.UserId; 
end;

procedure TdmMain.taSIElFOR_Q_UChange(Sender: TField);
var
  Cost: double;
begin
  if dm.taNDS.Locate('NDSID', taSIListNDSID.AsInteger, []) then
  begin
    Cost := fif(Sender.AsInteger=0, taSIElTPRICE.AsFloat*taSIElW.AsFloat, taSIElTPRICE.AsFloat*taSIElQ.AsInteger);
    taSIElCOST.AsFloat := Cost;
    taSIELNDS.AsFloat := dm.CalcNDS(Cost, dm.taNDSNDS.AsFloat, dm.taNDST.AsInteger);
    if(dm.taNDST.AsInteger=0)then taSIElCOST.AsFloat := Cost+taSIElNDS.AsFloat;
  end;
end;

procedure TdmMain.taSIElQChange(Sender: TField);
var
  Cost: double;
begin
  if dm.taNDS.Locate('NDSID', taSIListNDSID.AsInteger, []) then
  begin
    Cost := fif(taSIElFOR_Q_U.AsInteger=0, taSIElTPRICE.AsFloat*taSIElW.AsFloat, taSIElTPRICE.AsFloat*Sender.AsInteger);
    taSIElCOST.AsFloat := Cost;
    taSIElNDS.AsFloat := dm.CalcNDS(Cost, dm.taNDSNDS.AsFloat, dm.taNDST.AsInteger);
    if(dm.taNDST.AsInteger=0)then taSIElCOST.AsFloat := Cost+taSIElNDS.AsFloat;
  end;
end;

procedure TdmMain.taSIElWChange(Sender: TField);
var
  Cost: double;
begin
  if dm.taNDS.Locate('NDSID', taSIListNDSID.AsInteger, []) then
  begin
    Cost := fif(taSIElFOR_Q_U.AsInteger=0, taSIElTPRICE.AsFloat*Sender.AsFloat, taSIElTPRICE.AsFloat*Sender.AsInteger);
    taSIElCOST.AsFloat := Cost;
    taSIElNDS.AsFloat := dm.CalcNDS(Cost, dm.taNDSNDS.AsFloat, dm.taNDST.AsInteger);
    if (dm.taNDST.AsInteger=0)then taSIElCOST.AsFloat := Cost+taSIElNDS.AsFloat;
  end;
end;

procedure TdmMain.taSIElUEPRICEChange(Sender: TField);
begin
  if UseUE then
    taSIElTPRICE.AsFloat := Sender.AsFloat*taSIListUE.AsFloat;
end;

procedure TdmMain.taAffinajWQChange(Sender: TField);
var
  bEqual : boolean;
begin
  if not (taWhAffinaj.Active)then eXit;
  if (Sender.DataSet.State in [dsInsert]) then eXit;
  bEqual := False;
  case AffinajMode of
    // ������� �����
    amS :
      // ����� �������� ������ �������� WhAffinajMode ������ ���������������
      // ���� �������� taAffinajT.AsInteger � ������������ ������
      case WhAffinajMode of // c���� ��������
        // ������ �� �������
        0 : bEqual := taAffinajT.AsInteger=4;
        // ����� � ��������
        2 : bEqual := taAffinajT.AsInteger=5;
        else raise EInternal.Create(Format(rsInternalError, ['400']));
      end;
    // ������� ����
    amL :
      case WhAffinajMode of // c���� ��������
        // ������ �� �������
        1 : bEqual := taAffinajT.AsInteger=6;
        // ����� � ��������
        2 : bEqual := taAffinajT.AsInteger=7;
        else raise EInternal.Create(Format(rsInternalError, ['401']));
      end;
    else raise EInternal.Create(Format(rsInternalError, ['402']));
  end; // case
  if not bEqual then eXit;
  if Sender.FieldName='W' then
  begin
    FWChange := True;
    FInvWValue := Sender.OldValue-Sender.NewValue;
  end
  else if Sender.FieldName='Q' then
  begin
    FQChange := True;
    FInvQValue := Sender.OldValue-Sender.NewValue;
  end;
end;

procedure TdmMain.taAffinajBeforePost(DataSet: TDataSet);

  procedure ChangeValue(Field : TField; Val : double);
  var
    bFind : boolean;
    Wh : double;
    Id : Variant;
  begin
    case AffinajMode of
      amS : begin
        bFind := False;
        case WhAffinajMode of
          0 : bFind := taWhAffinaj.Locate('SEMISID', taAffinajSEMISID.AsString, []);
          2 : bFind := taWhAffinaj.Locate('SEMISID;UQ;UW', VarArrayOf([taAffinajSEMISID.AsString, taAffinajUQ.AsInteger, taAffinajUW.AsInteger]), []);
          else raise EInternal.Create(Format(rsInternalError, ['400']));
        end;
        if bFind then Wh := Field.AsFloat
        else begin
          Wh := 0;
          taWhAffinaj.Insert;
          taWhAffinaj.Post;
        end;
        if not (taWhAffinaj.State in [dsEdit, dsInsert]) then taWhAffinaj.Edit;
        case WhAffinajMode of
          0 : Field.AsFloat := Wh+Val;
          2 : Field.AsFloat := Wh-Val;
          else raise EInternal.Create(Format(rsInternalError, ['400']));
        end;
      end;

      amL : begin
        bFind := False;
        case WhAffinajMode of
          1 : bFind := taWhAffinaj.Locate('SEMISID;OPERID;UQ;UW', VarArrayOf([taAffinajSEMISID.AsString, taAffinajOPERID.AsVariant, taAffinajUQ.AsInteger, taAffinajUW.AsInteger]), []);
          2 : bFind := taWhAffinaj.Locate('SEMISID;UQ;UW', VarArrayOf([taAffinajSEMISID.AsString, taAffinajUQ.AsInteger, taAffinajUW.AsInteger]), []);
          else raise EInternal.Create(Format(rsInternalError, ['401']));
        end;
        Wh := 0;
        if bFind then Wh := Field.AsFloat
        else begin
          Id := ExecSelectSQL('select Id from WhSemis where DEPID='+taAFListDEPID.AsString+
            ' and SemisId="'+taAffinajSEMISID.AsString+'" and OperId '+fif(taAffinajOPERID.IsNull, 'is null','="'+taAffinajOPERID.AsString+'"')+
            ' and UQ='+taAffinajUQ.AsString+' and UW='+taAffinajUW.AsString, quTmp);
          if VarIsNull(Id) then eXit;
          taWhAffinaj.Insert;
          taWhAffinajID.AsInteger := Id;
          taWhAffinajT.AsInteger := 1;
          taWhAffinaj.Post;
        end;
        if not (taWhAffinaj.State in [dsEdit, dsInsert]) then taWhAffinaj.Edit;
        case WhAffinajMode of
          1 : Field.AsFloat := Wh+Val;
          2 : Field.AsFloat := Wh-Val;
          else raise EInternal.Create(Format(rsInternalError, ['401']));
        end;
      end;
    end;

  end;

begin
  if not (FWChange or FQChange) then eXit;
  if FWChange then ChangeValue(taWhAffinajW, FInvWValue);
  if FQChange then ChangeValue(taWhAffinajQ, FInvQValue);
  FWChange := False;
  FQChange := False;
end;

procedure TdmMain.taAffinajAfterPost(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).Transaction.CommitRetaining;
  PostDataSet(taWhAffinaj);
end;

procedure TdmMain.taSIListDeleteError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if Pos('FK_SIEL', E.Message)<>0 then
  begin
    MessageDialog('������� ���������� ������� ������ ���������.', mtInformation, [mbOk], 0);
    Action := daAbort;
  end;
end;

procedure TdmMain.taSOElQChange(Sender: TField);
begin
with taSOEl do
    begin
      DisableControls;
      Edit;
      if dmMain.taSOElFOR_Q_U.AsInteger = 1 then
        dmMain.taSOELCOST.AsFloat := dmMain.taSOELQ.AsFloat * dmMain.taSOELTPRICE.AsFloat;
      Post;
      ApplyUpdates;
      EnableControls;
    end;
end;

procedure TdmMain.taSOElTPRICEChange(Sender: TField);
begin
  with taSOEl do
    begin
      DisableControls;
      Edit;
      if dmMain.taSOELFOR_Q_U.AsInteger = 0 then
        dmMain.taSOELCOST.AsFloat := dmMain.taSOELW.AsFloat * dmMain.taSOELTPRICE.AsFloat
      else
        dmMain.taSOELCOST.AsFloat := dmMain.taSOELQ.AsFloat * dmMain.taSOELTPRICE.AsFloat;
      Post;
      ApplyUpdates;
      EnableControls;
    end;
end;

procedure TdmMain.taSOElWChange(Sender: TField);
begin
  with taSOEL do
    begin
      DisableControls;
      Edit;
      if dmMain.taSOELFOR_Q_U.AsInteger = 0 then
        dmMain.taSOELCOST.AsFloat := dmMain.taSOELW.AsFloat * dmMain.taSOELTPRICE.AsFloat;
      Post;
      ApplyUpdates;
      EnableControls;
    end;
end;

procedure TdmMain.taSOListAfterPost(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).Transaction.CommitRetaining;
end;

procedure TdmMain.taInvSubTypeBeforeOpen(DataSet: TDataSet);
begin
  dm.SetSchemaParams(taInvSubType.Params);
  taInvSubType.ParamByName('ITYPE').AsInteger := dmInv.IType;
end;

procedure TdmMain.taSOElBeforeOpen(DataSet: TDataSet);
begin
  tpFIBDataSet(dataSet).ParamByName('InvID').AsInteger := taSolistINVID.AsInteger;
end;

procedure TdmMain.taSOElCalcFields(DataSet: TDataSet);
var
  r : Variant;
begin
if(taSOElSEMIS.AsString <> '')then
  begin
    r := ExecSelectSQL('select qu.USEMIS, wu.USEMIS from D_Semis s, D_Unit qu, D_Unit wu '+
       ' where s.SemisId="'+taSOElSEMIS.AsString+'" and s.UQ=qu.UnitId and s.UW=wu.UnitId', quTmp);
    try taSOElUQ.AsString := r[0] except end;
    try taSOElUW.AsString := r[1]; except end;
  end;
  if taSOElFOR_Q_U.AsInteger = 0 then
    taSOElFORQUNAME.AsString := '�� ���'
  else
    if taSOElFOR_Q_U.AsInteger = 1 then
      taSOElFORQUNAME.AsString := '�� ���-��';
end;

procedure TdmMain.taSOElFOR_Q_UChange(Sender: TField);
begin
  with taSOEl do
    begin
      DisableControls;
      Edit;
      if dmMain.taSOELFOR_Q_U.AsInteger = 0 then
        dmMain.taSOELCOST.AsFloat := dmMain.taSOELW.AsFloat * dmMain.taSOELTPRICE.AsFloat
      else
        dmMain.taSOELCOST.AsFloat := dmMain.taSOELQ.AsFloat * dmMain.taSOELTPRICE.AsFloat;
      Post;
      ApplyUpdates;
      EnableControls;
    end;
end;

procedure TdmMain.taSOElNewRecord(DataSet: TDataSet);
begin
//  taSOElID.AsInteger := dm.GetId(42);
//  taSOElINVID.AsInteger := taSOListINVID.AsInteger;
//  taSOElQ.AsFloat := 0;
//  taSOElW.AsFloat := 0;
//  taSOElTPRICE.AsFloat := 0;
//  taSOElPRICE.AsFloat := 0;
//  taSOElCOST.AsFloat := 0;
//  taSOElNDS.AsFloat := 0;
//  taSOElNDSID.AsInteger := taSOListNDSID.AsInteger;
//  taSOElFOR_Q_U.AsInteger := 0;
end;

procedure TdmMain.taASListCONTRACTIDChange(Sender: TField);
begin
  if taASItem.Active then
  begin
    if not taASItem.IsEmpty then raise Exception.Create('��������� �������� �������');
  end
  else

end;

procedure TdmMain.taWhSCalcFields(DataSet: TDataSet);
begin
  taWhSW0.AsFloat := taWhSW.AsFloat*taWhSQ0.AsFloat;  
end;

procedure TdmMain.taSOListDeleteError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if Pos('FK_SIEL', E.Message) <> 0 then
  begin
    MessageDialog('������� ���������� ������� ������ ���������.', mtInformation, [mbOk], 0);
    Action := daAbort;
  end;
end;

procedure TdmMain.taDIAssortInvBeforeOpen(DataSet: TDataSet);
begin
  taDIAssortInv.ParamByName('REF').AsInteger := taDIElREF.AsInteger;
end;

procedure TdmMain.taDIElAfterOpen(DataSet: TDataSet);
begin
  if dm.WorkMode = 'aDIEl' then
    ReOpenDataSet(taDIAssortInv);
end;

procedure TdmMain.taDIElAfterScroll(DataSet: TDataSet);
begin
  if dm.WorkMode = 'aDIEl' then
    ReOpenDataSet(taDIAssortInv);
end;

procedure TdmMain.taPenaltyBeforeOpen(DataSet: TDataSet);
begin
  taPenalty.ParamByName('MODE').AsInteger := PenaltyMode;
  taPenalty.ParamByName('SELF$COMPANY$ID').AsInteger := dm.User.SelfCompId;
  taPenalty.ParamByName('COMPANY$ID').AsInteger := dmInv.taSListSUPID.AsInteger;
  if dmInv.ITYPE = 3 then taPenalty.ParamByName('INVOICE$ID').AsInteger := dmInv.FCurrINVID
  else taPenalty.ParamByName('INVOICE$ID').AsInteger := 0;
end;

procedure TdmMain.taPenaltyAfterPost(DataSet: TDataSet);
begin
  taPenalty.Transaction.CommitRetaining;
end;

procedure TdmMain.taWOrderBeforePost(DataSet: TDataSet);
var
  SpoilageWeight: Double;
  SpoilageQuantity: Integer;
  DeltaSpoilageWeight: Double;
  DeltaSpoilageQuantity: Integer;
  Delta: TSpoilageDelta;
  Storage: TStorage;
begin
  if taWOrder.State = dsEdit then
  begin
    if (taWOrderITTYPE.AsInteger = 0) and (taWOrderREJID.AsInteger <> 0) then
    begin
      //Application.MessageBox('������ �����','5',0);
    end
    else
    if taWOrderITTYPE.AsInteger = 5 then
    begin
      SpoilageWeight := taWOrderW.AsFloat;
      SpoilageQuantity := taWOrderQ.AsInteger;
      DeltaSpoilageWeight := Round((SpoilageWeight - FSpoilageWeight) * 1000) / 1000;
      DeltaSpoilageQuantity := SpoilageQuantity - FSpoilageQuantity;

      Storage := TStorage.Create;
      Storage.ID := dm.User.wWhId;
      Storage.Element.Code := Trim(dmMain.taWOrderSEMIS.AsString);
      Storage.Element.Operation := dm.RejOperId;
      Storage.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
      Storage.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
      Storage.Connected := True;
      Storage.Element.Connected := True;

      Delta.StorageElementID := Storage.Element.ID;
      Delta.InvoiceElementID := dmmain.taWOrderWOITEMID.AsInteger;
      Delta.OperationID := ExecSelectSQL('select ID from d_oper where operid = ' + #39 + taWOrderListDEFOPER.AsString+ #39, dmMain.quTmp, False);
      Delta.Quantity := DeltaSpoilageQuantity;
      Delta.Weight := DeltaSpoilageWeight;
      if (Delta.Quantity <> 0) or (Delta.Weight <> 0) then
      TDialogSemis.Delta(Delta);
      Storage.Free;
    end;
  end;
end;

procedure TdmMain.taWOrderBeforeEdit(DataSet: TDataSet);
var
  s, s1 : string;
  DepartmentID: Integer;
  EnableEdit: Boolean;
begin
  if taWOrderListISCLOSE.AsInteger = 1 then
  raise Exception.Create(Format(rsWOrderClose, [taWOrderListDOCNO.AsString]));

  EnableEdit := False;
  if dm.IsAdm then EnableEdit := True
  else
  begin
    DepartmentID := taWOrderDEPID.AsInteger;
    if DepartmentID = dm.User.wWhId then EnableEdit := True;
  end;

  if not EnableEdit then
  raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '��� ������ � '+taWOrderDEPNAME.AsString]));


  if(taWOrderITDATE.AsDateTime <= dm.PsBeginDate) then
  begin
    case dm.PsTypeBeginDate of
      0: begin
        s := '� �������� ��������������';
        s1 := dmMain.taWOrderJOBDEPNAME.AsString;
      end;
      1: begin
        s := '� ������������ ���������';
        s1 := dmMain.taWOrderDEPNAME.AsString;
      end
    end;
    raise Exception.Create(Format(rsPeriodClose, [s1, DateToStr(dm.PsBeginDate), s]));
  end;
  if (taWOrderITTYPE.AsInteger in [0,1,6]) then
    if(dm.WorkMode = 'wOrder') then
      if taWhSemis.Active then taWhSemis.Locate('SEMISID', taWOrderSEMIS.AsString, []);
 // if not taWOrderREF.IsNull then raise Exception.Create('������������� �� ��������');

 FSpoilageWeight := taWOrderW.AsFloat;
 FSpoilageQuantity := taWOrderQ.AsInteger;
end;


end.
