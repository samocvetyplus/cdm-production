object fmOperHistory: TfmOperHistory
  Left = 312
  Top = 145
  BorderStyle = bsToolWindow
  ClientHeight = 229
  ClientWidth = 408
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object gridNorma: TDBGridEh
    Left = 0
    Top = 0
    Width = 408
    Height = 229
    Align = alClient
    AutoFitColWidths = True
    ColumnDefValues.Title.TitleButton = True
    DataSource = dsrNormaHistory
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FrozenCols = 1
    Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Visible = False
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RecNo'
        Footers = <>
        Width = 31
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OPERNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 154
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'N'
        Footers = <>
        Title.EndEllipsis = True
        Width = 43
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'N1'
        Footers = <>
        Title.EndEllipsis = True
        Width = 39
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'NQ'
        Footers = <>
        Title.EndEllipsis = True
        Width = 38
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'TAILNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 92
      end>
  end
  object gridZp: TDBGridEh
    Left = 0
    Top = 0
    Width = 408
    Height = 229
    Align = alClient
    AllowedOperations = []
    AutoFitColWidths = True
    ColumnDefValues.Title.TitleButton = True
    DataSource = dsrZpHistory
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FrozenCols = 1
    Options = [dgEditing, dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Visible = False
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RecNo'
        Footers = <>
        Width = 28
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OPERNAME'
        Footers = <>
        Width = 173
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RATE'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ZP'
        Footers = <>
        Width = 69
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'ZBONUS'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
      end>
  end
  object taNormaHistory: TpFIBDataSet
    Database = dm.db
    Transaction = dm.tr
    SelectSQL.Strings = (
      'select OPERNAME, N, N1, NQ, TAILNAME  '
      'from History_OperNorma'
      'where HDATE=:HDATE'
      'order by OPERNAME')
    OnCalcFields = taNormaHistoryCalcFields
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 64
    Top = 112
    poSQLINT64ToBCD = True
    object taNormaHistoryRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taNormaHistoryOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taNormaHistoryN: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072'|'#1054#1090' '#1074#1077#1089#1072
      FieldName = 'N'
    end
    object taNormaHistoryN1: TFIBFloatField
      DisplayLabel = #1053#1086#1088#1084#1072'|'#1054#1090' '#1095#1080#1089#1090#1086#1090#1099
      FieldName = 'N1'
    end
    object taNormaHistoryTAILNAME: TFIBStringField
      DisplayLabel = #1053#1086#1088#1084#1072'|'#1042#1080#1076' '#1086#1090#1093#1086#1076#1072
      FieldName = 'TAILNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taNormaHistoryNQ: TFIBStringField
      DisplayLabel = #1053#1086#1088#1084#1072'|'#1054#1090' '#1095#1077#1075#1086
      FieldName = 'NQ'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrNormaHistory: TDataSource
    DataSet = taNormaHistory
    Left = 64
    Top = 160
  end
  object dsrZpHistory: TDataSource
    DataSet = taZpHistory
    Left = 144
    Top = 160
  end
  object taZpHistory: TpFIBDataSet
    Database = dm.db
    Transaction = dm.tr
    SelectSQL.Strings = (
      'select OPERNAME, RATE, ZP, ZBONUS'
      'from History_ZpRate'
      'where HDATE=:HDATE'
      'order by OPERNAME')
    OnCalcFields = taZpHistoryCalcFields
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 144
    Top = 112
    poSQLINT64ToBCD = True
    object taZpHistoryRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taZpHistoryOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taZpHistoryRATE: TFIBFloatField
      DisplayLabel = #1057#1090#1072#1074#1082#1072
      FieldName = 'RATE'
    end
    object taZpHistoryZP: TFIBStringField
      DisplayLabel = #1054#1090' '#1095#1077#1075#1086
      FieldName = 'ZP'
      Size = 60
      EmptyStrToNull = True
    end
    object taZpHistoryZBONUS: TFIBSmallIntField
      DisplayLabel = #1056#1072#1089#1095#1077#1090' '#1087#1088#1077#1084#1080#1080
      FieldName = 'ZBONUS'
    end
  end
end
