inherited fmePArt: TfmePArt
  Left = 311
  Top = 200
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1072#1088#1090#1080#1082#1091#1083#1072
  ClientHeight = 151
  ClientWidth = 211
  KeyPreview = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 211
    Height = 151
    object lbQ: TLabel [0]
      Left = 8
      Top = 30
      Width = 34
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel [1]
      Left = 8
      Top = 51
      Width = 39
      Height = 13
      Caption = #1056#1072#1079#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel [2]
      Left = 8
      Top = 9
      Width = 41
      Height = 13
      Caption = #1040#1088#1090#1080#1082#1091#1083
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbOper: TLabel [3]
      Left = 8
      Top = 73
      Width = 50
      Height = 13
      Caption = #1054#1087#1077#1088#1072#1094#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbU: TLabel [4]
      Left = 9
      Top = 95
      Width = 42
      Height = 13
      Caption = #1045#1076'. '#1080#1079#1084'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    inherited btnOk: TBitBtn
      Left = 24
      Top = 116
      TabOrder = 5
    end
    inherited btnCancel: TBitBtn
      Left = 120
      Top = 116
      TabOrder = 6
    end
    object edQ: TDBNumberEditEh
      Left = 64
      Top = 25
      Width = 137
      Height = 19
      EditButton.Style = ebsUpDownEh
      EditButton.Visible = True
      EditButtons = <>
      Flat = True
      TabOrder = 1
      Value = 1
      Visible = True
    end
    object cmbxU: TDBComboBoxEh
      Left = 64
      Top = 88
      Width = 137
      Height = 19
      EditButtons = <>
      Flat = True
      Items.Strings = (
        #1064#1090'.'
        #1055#1072#1088#1072)
      KeyItems.Strings = (
        '1'
        '2')
      TabOrder = 4
      Visible = True
    end
    object lcbxSz: TDBLookupComboboxEh
      Left = 64
      Top = 46
      Width = 137
      Height = 19
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      DropDownBox.AutoDrop = True
      DropDownBox.Rows = 15
      EditButtons = <>
      Flat = True
      KeyField = 'ID'
      ListField = 'NAME'
      ListSource = dm.dsrSz
      TabOrder = 2
      Visible = True
    end
    object lcbxOper: TDBLookupComboboxEh
      Left = 64
      Top = 67
      Width = 137
      Height = 19
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      DropDownBox.AutoDrop = True
      DropDownBox.Rows = 15
      EditButtons = <>
      Flat = True
      KeyField = 'OPERID'
      ListField = 'OPERATION'
      ListSource = dm.dsrOper
      TabOrder = 3
      Visible = True
    end
    object edArt: TDBEditEh
      Left = 64
      Top = 4
      Width = 137
      Height = 19
      EditButtons = <>
      Flat = True
      TabOrder = 0
      Visible = True
    end
  end
end
