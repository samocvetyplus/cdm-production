inherited fmCompDoc: TfmCompDoc
  Left = 274
  Top = 190
  Width = 511
  Caption = #1044#1086#1082#1091#1084#1077#1085#1090#1099' '#1076#1083#1103' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 503
  end
  inherited tb1: TSpeedBar
    Width = 503
  end
  object gridDoc: TDBGridEh [2]
    Left = 0
    Top = 42
    Width = 503
    Height = 200
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsrDoc
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SortLocal = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Width = 95
      end
      item
        EditButtons = <>
        FieldName = 'SELFCOMPNAME'
        Footers = <>
        Width = 124
      end
      item
        EditButtons = <>
        FieldName = 'ITYPENAME'
        Footers = <>
        Width = 163
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object taDoc: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select cast(i.DOCNO as char(10)) DOCNO, i.DOCDATE, c.NAME SELFCO' +
        'MPNAME,'
      '       t.NAME ITYPENAME '
      'from Inv i, D_Comp c, D_InvType t'
      'where i.SupID=:SUPID and'
      '      i.SELFCOMPID=c.COMPID and       '
      '      i.ITYPE=t.ID'
      ''
      'union'
      ''
      'select i.NDOC DOCNO, i.OPERDATE DOCDATE, c.NAME SELFCOMPNAME,'
      '       case i.T '
      
        '         when 1 then cast('#39#1055#1088#1086#1076#1072#1078#1072' '#1087#1088#1086#1073#1080#1090' '#1082#1072#1089#1089#1086#1074#1099#1081' '#1095#1077#1082#39' as char(' +
        '80))'
      
        '         when 2 then cast('#39#1042#1086#1079#1074#1088#1072#1090' '#1087#1088#1086#1073#1080#1090' '#1082#1072#1089#1089#1086#1074#1099#1081' '#1095#1077#1082#39' as char(' +
        '80))'
      '       end '
      'from Cash i, D_Comp c'
      'where i.COMPID=:SUPID and'
      '      i.SELFCOMPID=c.COMPID'
      ''
      'union '
      ''
      
        'select cast('#39#39' as char(10)) DOCNO, i.PAYDATE DOCDATE, c.NAME SEL' +
        'FCOMPNAME,'
      '       cast('#39#1054#1087#1083#1072#1090#1072' '#1079#1072' '#1090#1086#1074#1072#1088' '#1087#1086' '#1073#1072#1085#1082#1091#39' as char(80))'
      'from CashBank i, D_Comp c'
      'where i.COMPID=:SUPID and'
      '      i.SELFCOMPID=c.COMPID'
      ''
      'order by 2       ')
    BeforeOpen = taDocBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 184
    Top = 100
    object taDocDOCNO: TFIBStringField
      DisplayLabel = #1053#1086#1084#1077#1088' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      FieldName = 'DOCNO'
      Size = 10
      EmptyStrToNull = True
    end
    object taDocDOCDATE: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object taDocSELFCOMPNAME: TFIBStringField
      DisplayLabel = #1042#1083#1072#1076#1077#1083#1077#1094
      FieldName = 'SELFCOMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taDocITYPENAME: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
      FieldName = 'ITYPENAME'
      Size = 80
      EmptyStrToNull = True
    end
  end
  object dsrDoc: TDataSource
    DataSet = taDoc
    Left = 184
    Top = 148
  end
end
