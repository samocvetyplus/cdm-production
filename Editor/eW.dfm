inherited fmeW: TfmeW
  Left = 285
  Top = 232
  Caption = #1056#1077#1076#1072#1082#1090#1086#1088' '#1074#1077#1089#1072
  ClientHeight = 119
  ClientWidth = 223
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  ExplicitWidth = 229
  ExplicitHeight = 151
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 223
    Height = 119
    ExplicitWidth = 223
    ExplicitHeight = 119
    object lbW: TLabel [0]
      Left = 8
      Top = 24
      Width = 64
      Height = 13
      Caption = #1042#1077#1089' '#1080#1079#1076#1077#1083#1080#1103
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel [1]
      Left = 8
      Top = 67
      Width = 42
      Height = 13
      Caption = #1045#1076'. '#1080#1079#1084'.'
    end
    object Label2: TLabel [2]
      Left = 8
      Top = 45
      Width = 53
      Height = 13
      Caption = #1054#1087#1077#1088#1072#1094#1080#1103':'
    end
    object lbComment: TLabel [3]
      Left = 8
      Top = 4
      Width = 191
      Height = 13
      Caption = 'F5 - '#1089#1095#1080#1090#1072#1090#1100' '#1074#1077#1089' '#1089' '#1074#1077#1089#1086#1074'; F9 - '#1087#1088#1080#1085#1103#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    inherited btnOk: TBitBtn
      Left = 28
      Top = 88
      TabOrder = 3
      ExplicitLeft = 28
      ExplicitTop = 88
    end
    inherited btnCancel: TBitBtn
      Left = 132
      Top = 88
      TabOrder = 4
      ExplicitLeft = 132
      ExplicitTop = 88
    end
    object cmbxOper: TDBComboBoxEh
      Left = 80
      Top = 42
      Width = 133
      Height = 19
      DropDownBox.Rows = 15
      DropDownBox.Width = -1
      EditButtons = <>
      Flat = True
      TabOrder = 1
      Visible = True
    end
    object cmbxU: TDBComboBoxEh
      Left = 80
      Top = 64
      Width = 133
      Height = 19
      EditButtons = <>
      Flat = True
      Items.Strings = (
        #1064#1090'.'
        #1055#1072#1088#1072'.')
      TabOrder = 2
      Visible = True
    end
    object edW: TDBNumberEditEh
      Left = 80
      Top = 20
      Width = 133
      Height = 19
      EditButtons = <>
      Flat = True
      TabOrder = 0
      Visible = True
    end
  end
  object acEvent: TActionList
    Left = 144
    Top = 40
    object acReadW: TAction
      ShortCut = 116
      OnExecute = acReadWExecute
    end
    object acOk: TAction
      Caption = 'acOk'
      ShortCut = 120
      OnExecute = acOkExecute
    end
  end
end
