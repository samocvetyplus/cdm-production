unit Data;

interface

uses
  SysUtils, Classes, DB, FIBDataSet, pFIBDataSet, xmldom, XMLIntf,
  msxmldom, XMLDoc, ADODB, FIBQuery, pFIBQuery, FR_Class, frOLEExl;

type
  // ��� ������������ - ���������, ���������
  TExportKind = (ekMaterial, ekProduct);

  TdmData = class(TDataModule)
    taActWorkList: TpFIBDataSet;
    dsrActWorkList: TDataSource;
    taActWork: TpFIBDataSet;
    dsrActWork: TDataSource;
    taActWorkListINVID: TFIBIntegerField;
    taActWorkListDOCNO: TFIBIntegerField;
    taActWorkListDOCDATE: TFIBDateTimeField;
    taActWorkListCOMPID: TFIBIntegerField;
    taActWorkListBD: TFIBDateTimeField;
    taActWorkListED: TFIBDateTimeField;
    taActWorkListUSERID: TFIBIntegerField;
    taActWorkListCOMPNAME: TFIBStringField;
    taActWorkListUSERNAME: TFIBStringField;
    taActWorkListISCLOSE: TFIBSmallIntField;
    taActWorkID: TFIBIntegerField;
    taActWorkINVID: TFIBIntegerField;
    taActWorkMATID: TFIBStringField;
    taActWorkN: TFIBFloatField;
    taActWorkGETW: TFIBFloatField;
    taActWorkRETW: TFIBFloatField;
    taActWorkNW: TFIBFloatField;
    taActWorkMATNAME: TStringField;
    taOutAnaliz: TpFIBDataSet;
    taOutAnalizINVID: TIntegerField;
    taOutAnalizDOCNO: TIntegerField;
    taOutAnalizDOCDATE: TDateTimeField;
    taOutAnalizCOMPNAME: TFIBStringField;
    taOutAnalizTYPENAME: TFIBStringField;
    taOutAnalizQ: TIntegerField;
    taOutAnalizW: TFloatField;
    taOutAnalizSUMM: TFloatField;
    taOutAnalizA: TSmallintField;
    dsrOutAnaliz: TDataSource;
    taActWorkListINVSUBTYPEID: TFIBIntegerField;
    taActWorkListINVSUBTYPENAME: TFIBStringField;
    taActWorkListREFINVID: TFIBIntegerField;
    taActWorkListREFDOCNO: TFIBIntegerField;
    taActWorkListREFDOCDATE: TFIBDateTimeField;
    taActWorkListREFNAME: TFIBStringField;
    taServItem: TpFIBDataSet;
    dsrServItem: TDataSource;
    taServItemID: TFIBIntegerField;
    taServItemINVID: TFIBIntegerField;
    taServItemSITEMID: TFIBIntegerField;
    taServItemSCOST: TFIBFloatField;
    taServItemUID: TFIBIntegerField;
    taServItemART2ID: TFIBIntegerField;
    taServItemSZID: TFIBIntegerField;
    taServItemARTID: TFIBIntegerField;
    taServItemART2: TFIBStringField;
    taServItemART: TFIBStringField;
    taServItemSZNAME: TFIBStringField;
    taActWorkADDBYUSER: TFIBSmallIntField;
    taServItemW: TFIBFloatField;
    taActWorkUW: TFIBSmallIntField;
    taActWorkCLIENTMAT: TFIBSmallIntField;
    taVerificationList: TpFIBDataSet;
    dsrVerificationList: TDataSource;
    taActVerification: TpFIBDataSet;
    dsrActVerification: TDataSource;
    taVerificationListINVID: TFIBIntegerField;
    taVerificationListDOCNO: TFIBIntegerField;
    taVerificationListDOCDATE: TFIBDateTimeField;
    taVerificationListUSERID: TFIBIntegerField;
    taVerificationListISCLOSE: TFIBSmallIntField;
    taVerificationListSUPID: TFIBIntegerField;
    taVerificationListSUPNAME: TFIBStringField;
    taVerificationListUSERNAME: TFIBStringField;
    taVerificationListBD: TFIBDateTimeField;
    taVerificationListED: TFIBDateTimeField;
    taActWorkCOST: TFIBFloatField;
    taRequestList: TpFIBDataSet;
    dsrRequestList: TDataSource;
    taRequestListINVID: TFIBIntegerField;
    taRequestListDOCNO: TFIBIntegerField;
    taRequestListDOCDATE: TFIBDateTimeField;
    taRequestListABD: TFIBDateTimeField;
    taRequestListAED: TFIBDateTimeField;
    taRequestListSUPID: TFIBIntegerField;
    taRequestListSUPNAME: TFIBStringField;
    taRequestListQ: TFIBIntegerField;
    taRequest: TpFIBDataSet;
    dsrRequest: TDataSource;
    taRequestID: TFIBIntegerField;
    taRequestINVID: TFIBIntegerField;
    taRequestARTID: TFIBIntegerField;
    taRequestART: TFIBStringField;
    taRequestART2ID: TFIBIntegerField;
    taRequestART2: TFIBStringField;
    taRequestSZID: TFIBIntegerField;
    taRequestSZNAME: TFIBStringField;
    taRequestOPERID: TFIBStringField;
    taRequestOPERNAME: TFIBStringField;
    taRequestQ: TFIBIntegerField;
    taRequestMW: TFIBFloatField;
    taRequestListMW: TFIBFloatField;
    taRequestListANALIZ: TFIBSmallIntField;
    taRequestListSUPCODE: TFIBStringField;
    taRequestListMEMO: TMemoField;
    taRequestListBOSSFIO: TFIBStringField;
    taRequestListBOSSPHONE: TFIBStringField;
    taRequestListOFIO: TFIBStringField;
    taRequestListOFIOPHONE: TFIBStringField;
    taRequestListPOSTADDRESS: TFIBStringField;
    taRequestListPHONE: TFIBStringField;
    taRequestListFAX: TFIBStringField;
    taRequestListEMAIL: TFIBStringField;
    taActWorkListNO_CONTRACT: TFIBStringField;
    taActWorkListBD_CONTRACT: TFIBDateTimeField;
    taActWorkListSELFBOSSFIO: TFIBStringField;
    taActWorkListCOMPBOSSFIO: TFIBStringField;
    taActWorkListSELFCOMPNAME: TFIBStringField;
    taRequestListINVCOLORID: TFIBIntegerField;
    taRequestListCOLOR: TFIBIntegerField;
    taActVerificationID: TFIBIntegerField;
    taActVerificationINVID: TFIBIntegerField;
    taActVerificationMATID: TFIBStringField;
    taActVerificationINVSUBTYPEID: TFIBIntegerField;
    taActVerificationMATNAME: TFIBStringField;
    taActVerificationINVSUBNAME: TFIBStringField;
    taActVerificationQ: TFIBIntegerField;
    taActVerificationW: TFIBFloatField;
    taActVerificationN: TFIBFloatField;
    taActWorkListCOST: TFIBFloatField;
    taZpTPS: TpFIBDataSet;
    dsrZpTPS: TDataSource;
    taZpTPSID: TFIBIntegerField;
    taZpTPSZPID: TFIBIntegerField;
    taZpTPSPSID: TFIBIntegerField;
    taZpTPSS: TFIBFloatField;
    taZpTPSE: TFIBFloatField;
    taZpTPSREJ: TFIBFloatField;
    taZpTPSPSNAME: TFIBStringField;
    taActWorkListREFISCLOSE: TFIBSmallIntField;
    taServItemCOMMENT: TFIBStringField;
    taZpTPSRET_PERCENT: TFIBFloatField;
    taZpTPSREJ_PERCENT: TFIBFloatField;
    taExportDoc: TpFIBDataSet;
    taExportDocItem: TpFIBDataSet;
    taMBList: TpFIBDataSet;
    dsrMBList: TDataSource;
    taMatBalance: TpFIBDataSet;
    dsrMatBalance: TDataSource;
    taMBListINVID: TFIBIntegerField;
    taMBListDOCNO: TFIBIntegerField;
    taMBListDOCDATE: TFIBDateTimeField;
    taMBListUSERID: TFIBIntegerField;
    taMBListUSERNAME: TFIBStringField;
    taMBListABD: TFIBDateTimeField;
    taMBListAED: TFIBDateTimeField;
    taMatBalanceID: TFIBIntegerField;
    taMatBalanceINVID: TFIBIntegerField;
    taMatBalanceMATID: TFIBStringField;
    taMatBalanceW: TFIBFloatField;
    taMatBalanceDESCR: TFIBStringField;
    taMatBalanceMATNAME: TFIBStringField;
    taMBListSELFCOMPID: TFIBIntegerField;
    taMBListISCLOSE: TFIBSmallIntField;
    taActWorkPRICE0: TFIBFloatField;
    taAWMOList: TpFIBDataSet;
    dsrAWMOList: TDataSource;
    taAWMO: TpFIBDataSet;
    dsrAWMO: TDataSource;
    taAWMOListINVID: TFIBIntegerField;
    taAWMOListDOCNO: TFIBIntegerField;
    taAWMOListDOCDATE: TFIBDateTimeField;
    taAWMOListUSERID: TFIBIntegerField;
    taAWMOListABD: TFIBDateTimeField;
    taAWMOListAED: TFIBDateTimeField;
    taAWMOListUSERNAME: TFIBStringField;
    taAWMOListSELFCOMPID: TFIBIntegerField;
    taAWMOListISCLOSE: TFIBSmallIntField;
    taAWMOListSUPID: TFIBIntegerField;
    taAWMOListSUPNAME: TFIBStringField;
    taAWMOListREF: TFIBIntegerField;
    taAWMOID: TFIBIntegerField;
    taAWMOINVID: TFIBIntegerField;
    taAWMOMATID: TFIBStringField;
    taAWMOIW: TFIBFloatField;
    taAWMOIC0: TFIBFloatField;
    taAWMOS: TFIBFloatField;
    taAWMOSC0: TFIBFloatField;
    taAWMOR: TFIBFloatField;
    taAWMORC0: TFIBFloatField;
    taAWMODW: TFIBFloatField;
    taAWMODC0: TFIBFloatField;
    taAWMOKC: TFIBFloatField;
    taAWMONW: TFIBFloatField;
    taAWMONC0: TFIBFloatField;
    taAWMOOW: TFIBFloatField;
    taAWMOOC0: TFIBFloatField;
    taAWMOMATNAME: TFIBStringField;
    taAWMOListQ: TFIBFloatField;
    taAWMOListW: TFIBFloatField;
    taAWMOListW2: TFIBFloatField;
    taAWMOListCOST: TFIBFloatField;
    taAWMOListAKCIZ: TFIBFloatField;
    taAWMOListUPPRICE: TFIBFloatField;
    taAWMONRW: TFIBFloatField;
    taAWMONRC0: TFIBFloatField;
    taAWMOListUE: TFIBFloatField;
    taAWMOListTRANS_PERCENT: TFIBFloatField;
    taRequestListSUPCOLOR: TFIBIntegerField;
    taTaskList: TpFIBDataSet;
    dsrTaskList: TDataSource;
    taTaskListID: TFIBIntegerField;
    taTaskListTTYPE: TFIBIntegerField;
    taTaskListN: TFIBIntegerField;
    taTaskListDAT: TFIBDateTimeField;
    taTaskListDEPID: TFIBIntegerField;
    taTaskListDEPNAME: TFIBStringField;
    taTaskListBRIEFDESCR: TFIBStringField;
    taTaskListDESCR: TFIBMemoField;
    taTaskListCREATEBYUSERID: TFIBIntegerField;
    taTaskListCREATEBYUSER: TFIBStringField;
    taTaskListK: TFIBFloatField;
    taTaskListRECNO: TIntegerField;
    taLookupTaskType: TpFIBDataSet;
    dsrLookupTaskType: TDataSource;
    taLookupTaskTypeID: TFIBIntegerField;
    taLookupTaskTypeNAME: TFIBStringField;
    taLookupTaskTypeCOLOR: TFIBIntegerField;
    taTaskListCOLOR: TFIBIntegerField;
    taTaskListTYPE: TStringField;
    taTaskOperations: TpFIBDataSet;
    dsTaskOperations: TDataSource;
    taTaskOperationsTASKID: TFIBIntegerField;
    taTaskOperationsDTASKOPERID: TFIBIntegerField;
    taTaskOperationsTASKOPERNAME: TFIBStringField;
    taTaskOperationsTASK_OPERID: TFIBIntegerField;
    taTaskOperationsSTATE: TFIBSmallIntField;
    taTaskOperationsDEPID: TFIBIntegerField;
    taTaskOperationsUSERID: TFIBIntegerField;
    taTaskOperationsGIVEDATE: TFIBDateTimeField;
    taTaskOperationsDONEDATE: TFIBDateTimeField;
    dsLookupDeps: TDataSource;
    taLookupDeps: TpFIBDataSet;
    taLookupDepsDEPID: TFIBIntegerField;
    taLookupDepsCOLOR: TFIBIntegerField;
    taLookupDepsSNAME: TFIBStringField;
    taLookupDepsNAME: TFIBStringField;
    taLookupDepsWH: TFIBSmallIntField;
    taLookupDepsWP: TFIBSmallIntField;
    taLookupDepsMO: TFIBSmallIntField;
    taLookupDepsDEPTYPES: TFIBIntegerField;
    taLookupDepsREF: TFIBIntegerField;
    taLookupDepsSELFCOMPID: TFIBIntegerField;
    taTaskOperationsUSERNAME: TFIBStringField;
    taTaskOperationsDEPNAME: TFIBStringField;
    dsTaskOperSigns: TDataSource;
    taTaskOperSigns: TpFIBDataSet;
    taDeps: TpFIBDataSet;
    taOperToGive: TpFIBDataSet;
    dsDeps: TDataSource;
    dsOperToGive: TDataSource;
    quAddOper: TpFIBQuery;
    taDepsDEPID: TFIBIntegerField;
    taDepsNAME: TFIBStringField;
    taTaskOperSignsID: TFIBIntegerField;
    taTaskOperSignsTASKOPERID: TFIBIntegerField;
    taTaskOperSignsMOLID: TFIBIntegerField;
    taTaskOperSignsDAT: TFIBDateTimeField;
    taLookupMol: TpFIBDataSet;
    taLookupMolMOLID: TFIBIntegerField;
    taLookupMolFNAME: TFIBStringField;
    taLookupMolLNAME: TFIBStringField;
    taLookupMolMNAME: TFIBStringField;
    taLookupMolFIO: TFIBStringField;
    taTaskOperSignsMOLNAME: TStringField;
    taTaskOperSignsCOMENT: TFIBStringField;
    taTaskImg: TpFIBDataSet;
    dsTaskImg: TDataSource;
    taTaskImgID: TFIBIntegerField;
    taTaskImgTASKID: TFIBIntegerField;
    taTaskImgNAME: TFIBStringField;
    taTaskImgIMG: TFIBBlobField;
    frOLEExcelExport1: TfrOLEExcelExport;
    taActWorkListNORM: TFIBFloatField;
    taActWorkCommon: TpFIBDataSet;
    FIBStringField1: TFIBStringField;
    FIBFloatField1: TFIBFloatField;
    FIBFloatField2: TFIBFloatField;
    FIBFloatField3: TFIBFloatField;
    FIBFloatField4: TFIBFloatField;
    StringField1: TStringField;
    FIBSmallIntField2: TFIBSmallIntField;
    FIBSmallIntField3: TFIBSmallIntField;
    FIBFloatField5: TFIBFloatField;
    FIBFloatField6: TFIBFloatField;
    taActWorkListCommon: TpFIBDataSet;
    FIBStringField2: TFIBStringField;
    FIBStringField6: TFIBStringField;
    FIBDateTimeField5: TFIBDateTimeField;
    FIBStringField7: TFIBStringField;
    FIBStringField8: TFIBStringField;
    FIBStringField9: TFIBStringField;
    FIBFloatField7: TFIBFloatField;
    taVerificationListTolling: TpFIBDataSet;
    dsVerificationListTolling: TDataSource;
    taVerificationListTollingINVID: TFIBIntegerField;
    taVerificationListTollingDOCNO: TFIBIntegerField;
    taVerificationListTollingDOCDATE: TFIBDateTimeField;
    taVerificationListTollingUSERID: TFIBIntegerField;
    taVerificationListTollingSUPID: TFIBIntegerField;
    taVerificationListTollingSELFCOMPID: TFIBIntegerField;
    taVerificationListTollingABD: TFIBDateTimeField;
    taVerificationListTollingAED: TFIBDateTimeField;
    taVerificationListTollingISCLOSE: TFIBSmallIntField;
    taVerificationListTollingITYPE: TFIBSmallIntField;
    taVerificationListTollingUSERNAME: TFIBStringField;
    taVerificationListTollingSUPNAME: TFIBStringField;
    taVerificationListTollingSELFCOMPNAME: TFIBStringField;
    procedure taActWorkListBeforeOpen(DataSet: TDataSet);
    procedure taActWorkListNewRecord(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taActWorkNewRecord(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure taServItemBeforeOpen(DataSet: TDataSet);
    procedure taActWorkBeforeOpen(DataSet: TDataSet);
    procedure taActWorkListBeforeDelete(DataSet: TDataSet);
    procedure taVerificationListBeforeDelete(DataSet: TDataSet);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure taRequestListBeforeDelete(DataSet: TDataSet);
    procedure taRequestARTValidate(Sender: TField);
    procedure taRequestBeforeDelete(DataSet: TDataSet);
    procedure taRequestNewRecord(DataSet: TDataSet);
    procedure taRequestSZNAMEValidate(Sender: TField);
    procedure taRequestBeforeOpen(DataSet: TDataSet);
    procedure taRequestBeforePost(DataSet: TDataSet);
    procedure taVerificationListNewRecord(DataSet: TDataSet);
    procedure taVerificationListBeforeOpen(DataSet: TDataSet);
    procedure taActVerificationBeforeOpen(DataSet: TDataSet);
    procedure taZpTPSBeforeOpen(DataSet: TDataSet);
    procedure taMBListNewRecord(DataSet: TDataSet);
    procedure taMBListBeforeOpen(DataSet: TDataSet);
    procedure taMatBalanceBeforeOpen(DataSet: TDataSet);
    procedure taAWMOListBeforeOpen(DataSet: TDataSet);
    procedure taAWMOListNewRecord(DataSet: TDataSet);
    procedure taAWMOBeforeOpen(DataSet: TDataSet);
    procedure ConfirmDelete(DataSet: TDataSet);
    procedure taTaskListCalcFields(DataSet: TDataSet);
    procedure taTaskListNewRecord(DataSet: TDataSet);
    procedure taTaskListBeforeOpen(DataSet: TDataSet);
    procedure taTaskOperationsBeforeOpen(DataSet: TDataSet);
    procedure taOperToGiveBeforeOpen(DataSet: TDataSet);
    procedure taTaskOperationsSTATEGetText(Sender: TField;
      var Text: String; DisplayText: Boolean);
    procedure taTaskOperSignsNewRecord(DataSet: TDataSet);
    procedure taTaskImgBeforeOpen(DataSet: TDataSet);
    procedure taTaskImgNewRecord(DataSet: TDataSet);
    procedure taActWorkCommonBeforeOpen(DataSet: TDataSet);
    procedure taActWorkListCommonBeforeOpen(DataSet: TDataSet);
    procedure taVerificationListTollingNewRecord(DataSet: TDataSet);
    procedure taVerificationListTollingBeforeOpen(DataSet: TDataSet);
    procedure taVerificationListTollingAfterPost(DataSet: TDataSet);
    procedure taVerificationListTollingAfterDelete(DataSet: TDataSet);
  public
    BeginDate, EndDate:TDateTime;
    procedure ExportDoc(FileName: string; Kind:TExportKind; DocId: integer);
  end;


var
  dmData: TdmData;

implementation

uses DictData, MainData, DateUtils, dbUtil, MsgDialog, Dialogs, Controls, Variants,
  ProductionConsts, TaskList;

{$R *.dfm}


procedure TdmData.taActWorkListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate); //DateTimeToTimeStamp(StartOfAYear(dmMain.DocListYear));
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate); //DateTimeToTimeStamp(EndOfAYear(dmMain.DocListYear));
  end;
end;

procedure TdmData.taActWorkListNewRecord(DataSet: TDataSet);
begin
  taActWorkListINVID.AsInteger := dm.GetId(12);
  taActWorkListDOCNO.AsInteger := dm.GetId(86);
  taActWorkListDOCDATE.AsDateTime := dm.ServerDateTime;
  taActWorkListISCLOSE.AsInteger := 0;
  taActWorkListUSERID.AsInteger := dm.User.UserId;
  if dm.User.SelfCompId = 1 then taActWorkListINVSUBTYPEID.AsInteger := 10
  else taActWorkListINVSUBTYPEID.AsInteger := 11;
end;

procedure TdmData.CommitRetaining(DataSet: TDataSet);
begin
  //ShowMessage(taRequestList.parambyname('bd').AsString);
//ShowMessage(taRequestList.parambyname('ed').AsString);
  dbUtil.CommitRetaining(DataSet);
end;

procedure TdmData.taActWorkNewRecord(DataSet: TDataSet);
begin
  taActWorkID.AsInteger := dm.GetId(85);
  taActWorkINVID.AsInteger := taActWorkListINVID.AsInteger;
  taActWorkN.AsFloat := 0;
  taActWorkGETW.AsFloat := 0;
  taActWorkRETW.AsFloat := 0;
  taActWorkNW.AsFloat := 0;
  taActWorkADDBYUSER.AsInteger := 1;
  taActWorkCLIENTMAT.AsInteger := 0;
  taActWorkUW.AsInteger := 3;
end;

procedure TdmData.DataModuleCreate(Sender: TObject);
begin
  BeginDate:=StartOfTheYear(Today);
  EndDate:=Today;
end;

procedure TdmData.taServItemBeforeOpen(DataSet: TDataSet);
begin
  taServItem.ParamByName('INVID').AsInteger := taActWorkListINVID.AsInteger;
end;

procedure TdmData.taActWorkBeforeOpen(DataSet: TDataSet);
begin
  taActWork.ParamByName('INVID').AsInteger := taActWorkListINVID.AsInteger;
end;

procedure TdmData.taActWorkListBeforeDelete(DataSet: TDataSet);
begin
  if dm.ReadOnly then Exception.Create(rsReadOnly);
  (* ��������� ����� ������� *)
  {if(dm.User.UserId <> taWOrderListMOLID.AsInteger)then
    raise EWarning.Create(Format(rsAccessDenied, [dm.User.FIO, '��� �������� ������ �� '+taWOrderListDEPNAME.AsString]));}
  if MessageDialog('������� ��� �����?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then SysUtils.Abort;
end;

procedure TdmData.taVerificationListBeforeDelete(DataSet: TDataSet);
begin
  if dm.ReadOnly then Exception.Create(rsReadOnly);
  (* ��������� ����� ������� *)
  {if(dm.User.UserId <> taWOrderListMOLID.AsInteger)then
    raise EWarning.Create(Format(rsAccessDenied, [dm.User.FIO, '��� �������� ������ �� '+taWOrderListDEPNAME.AsString]));}
  if MessageDialog('������� ��� ������?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then SysUtils.Abort;
end;

procedure TdmData.PostBeforeClose(DataSet: TDataSet);
begin
 if DataSet.State in [dsEdit, dsInsert] then DataSet.Post;
end;

procedure TdmData.taRequestListBeforeDelete(DataSet: TDataSet);
begin
  //if MessageDialog('������� ������ ����������?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then SysUtils.Abort;
end;

procedure TdmData.taRequestARTValidate(Sender: TField);
var
  ArtId, Art2Id, OperId, MW : Variant;
begin
  if not dm.CheckValidArt(Sender.AsString) then raise Exception.Create(rsInvalidArtFormat);
  ArtId := ExecSelectSQL('select D_ArtId from D_Art where Art='#39+Sender.AsString+#39, dm.quTmp);
  if VarIsNull(ArtId)or(ArtId = 0)then
    raise Exception.Create(Format(rsArtNotFound, [Sender.AsString, '� �����������']));
  taRequestARTID.AsInteger := ArtId;
  // �������� ������ �� ������ �������
  Art2Id := ExecSelectSQL('select Art2Id from Art2 where D_ArtId='+VarToStr(ArtId)+' and Art2="-"', dm.quTmp);
  if VarIsNull(Art2Id)or(Art2Id=0) then
    raise EInternal.Create(Format(rsInternalError, ['001']));
  taRequestART2ID.AsInteger := Art2Id;
  // �������� ��������
  OperId := ExecSelectSQL('select ApplOperId from D_Art where D_ArtId='+VarToStr(ArtId), dm.quTmp);
  taRequestOPERID.AsVariant := OperId;
  if VarIsNull(OperId)or(OperId='') then taRequestOPERNAME.AsString := sNoOperation
  else taRequestOPERNAME.AsString := ExecSelectSQL('select Operation from D_Oper where OperId="'+VarToStr(OperId)+'"', dm.quTmp);
  // �������� ������� ���
  MW := ExecSelectSQL('select MW from D_Art where D_ArtId='+VarToStr(ArtId), dm.quTmp);
  if VarIsNull(MW) then MW:=0;
  taRequestMW.AsFloat := MW;
end;

procedure TdmData.taRequestBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ������� �� ������?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then SysUtils.Abort;
end;

procedure TdmData.taRequestNewRecord(DataSet: TDataSet);
begin
  taRequestID.AsInteger := dm.GetId(89);
  taRequestINVID.AsInteger := taRequestListINVID.AsInteger;
  taRequestQ.AsInteger := 0;
end;

procedure TdmData.taRequestSZNAMEValidate(Sender: TField);
var
  SzId : Variant;
begin
  if Sender.AsString = '15,5' then SzId := 0
  else begin
    SzId := ExecSelectSQL('select ID from D_Sz where NAME='#39+Sender.AsString+#39, dm.quTmp);
    if VarIsNull(SzId)or(SzId=0)then raise Exception.Create(rsInvalidSzFormat+' ��� ������ ������� ��� � �����������');
  end;
  taRequestSZID.AsInteger := SzId;
end;

procedure TdmData.taRequestBeforeOpen(DataSet: TDataSet);
begin
  taRequest.ParamByName('INVID').AsInteger := taRequestListINVID.AsInteger;
end;

procedure TdmData.taRequestBeforePost(DataSet: TDataSet);
begin
  if(taRequestARTID.IsNull)then raise Exception.Create(rsInputArt);
  if(taRequestSZID.IsNull) then raise Exception.Create(rsInputSz);
end;

procedure TdmData.taVerificationListNewRecord(DataSet: TDataSet);
begin
  taVerificationListINVID.AsInteger := dm.GetId(12);
  taVerificationListDOCNO.AsInteger := dm.GetId(91);
  taVerificationListDOCDATE.AsDateTime := ServerDateTime(dm.quTmp);
  taVerificationListISCLOSE.AsInteger := 0;
  taVerificationListUSERID.AsInteger := dm.User.UserId;
end;

procedure TdmData.taVerificationListTollingAfterDelete(DataSet: TDataSet);
begin
  with  taVerificationListTolling.Transaction do
    if Active then
      CommitRetaining;
end;

procedure TdmData.taVerificationListTollingAfterPost(DataSet: TDataSet);
begin
  taVerificationListTolling.Transaction.CommitRetaining;
end;

procedure TdmData.taVerificationListTollingBeforeOpen(DataSet: TDataSet);
begin
  (DataSet as TpFIBDataSet).ParamByName('SelfCompID').AsInteger := dm.User.SelfCompId; 
end;

procedure TdmData.taVerificationListTollingNewRecord(DataSet: TDataSet);
begin
  taVerificationListTollingINVID.AsInteger := dm.GetId(12);
  taVerificationListTollingDOCDATE.AsDateTime := ServerDateTime(dm.quTmp);
  taVerificationListTollingSELFCOMPID.AsInteger := dm.User.SelfCompId;
  taVerificationListTollingUSERID.AsInteger := dm.User.UserId;
end;

procedure TdmData.taVerificationListBeforeOpen(DataSet: TDataSet);
begin
  taVerificationList.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(StartOfAYear(dmMain.DocListYear));
  taVerificationList.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(EndOfAYear(dmMain.DocListYear));
end;

procedure TdmData.taActVerificationBeforeOpen(DataSet: TDataSet);
begin
  taActVerification.ParamByName('INVID').AsInteger := taVerificationListINVID.AsInteger;
end;

procedure TdmData.taZpTPSBeforeOpen(DataSet: TDataSet);
begin
  taZpTPS.ParamByName('ZPID').AsInteger := dmMain.taZListID.AsInteger;
end;

procedure TdmData.ExportDoc(FileName: string; Kind: TExportKind; DocId: integer);
var
  Doc, Item, Info, Att, TablePart: IXMLNode;
  i: integer;
begin
  {
  // ��������� ������
  CloseDataSets([taExportDoc, taExportDocItem]);
  if not taExportDoc.Transaction.Active then taExportDoc.StartTransaction
  else taExportDoc.Transaction.CommitRetaining;
  taExportDoc.ParamByName('DOCID').AsInteger := DocId;
  taExportDoc.ParamByName('T').AsInteger := ord(Kind);
  taExportDocItem.ParamByName('DOCID').AsInteger := DocId;
  taExportDocItem.ParamByName('T').AsInteger := ord(Kind);
  OpenDataSets([taExportDoc, taExportDocItem]);
  // ������� xml-����
  XMLExport.Active := True;
  // ����� ����� ����������
  Info := XMLExport.DocumentElement.AddChild('����������');
  Att := XMLExport.CreateNode('��', ntAttribute); Att.NodeValue := dm.db.DatabaseName;
  Info.AttributeNodes.Add(Att);
  Att := XMLExport.CreateNode('����_��������', ntAttribute); Att.NodeValue := Now;
  Info.AttributeNodes.Add(Att);
  Att := XMLExport.CreateNode('������������', ntAttribute); Att.NodeValue := dm.User.FIO;
  Info.AttributeNodes.Add(Att);
  // ����� ��������� ���������
  Doc := XMLExport.DocumentElement.AddChild('��������');
  for i:=0 to Pred(taExportDoc.FieldCount) do with taExportDoc.Fields[i] do
    Doc.AddChild(FieldName).NodeValue := AsString;
  TablePart := Doc.AddChild('���������_�����');
  // ����� ������(�������) ���������
  while not taExportDocItem.Eof do
  begin
    Item := TablePart.AddChild('ITEM');
    for i:=0 to Pred(taExportDocItem.FieldCount) do with taExportDocItem.Fields[i] do
    if (DataType=ftCurrency) or (DataType=ftFloat) then
      Item.AddChild(FieldName).NodeValue := StringReplace(ASString,',','.',[])
      else Item.AddChild(FieldName).NodeValue := AsString;
    taExportDocItem.Next;
  end;
  CloseDataSets([taExportDoc, taExportDocItem]);
  // ��������� ��������� xml ����
  XMLExport.SaveToFile(FileName);
  XMLExport.Active := False;}
end;

procedure TdmData.taMBListNewRecord(DataSet: TDataSet);
begin
  taMBListINVID.AsInteger := dm.GetId(12);
  taMBListDOCNO.AsInteger := dm.GetId(96);
  taMBListDOCDATE.AsDateTime := dm.ServerDateTime;
  taMBListISCLOSE.AsInteger := 0;
  taMBListSELFCOMPID.AsInteger := dm.User.SelfCompId;
  taMBListUSERID.AsInteger := dm.User.UserId;
end;

procedure TdmData.taMBListBeforeOpen(DataSet: TDataSet);
begin
  taMBList.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(StartOfAYear(dmMain.DocListYear));
  taMBList.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(EndOfAYear(dmMain.DocListYear));
  dm.SetSelfCompParams(taMBList.Params);
end;

procedure TdmData.taMatBalanceBeforeOpen(DataSet: TDataSet);
begin
  taMatBalance.ParamByName('INVID').AsInteger := taMBListINVID.AsInteger;
end;

procedure TdmData.taAWMOListBeforeOpen(DataSet: TDataSet);
begin
  //taAWMOList.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
  //taAWMOList.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);  � ������ AWMatOffList
  dm.SetSelfCompParams(taAWMOList.Params);
end;

procedure TdmData.taAWMOListNewRecord(DataSet: TDataSet);
begin
  taAWMOListINVID.AsInteger := dm.GetId(12);
  //taAWMOListDOCNO.AsInteger := //dm.GetId(97);
  taAWMOListDOCDATE.AsDateTime := ToDay;
  taAWMOListISCLOSE.AsInteger := 0;
  taAWMOListUSERID.AsInteger := dm.User.UserId;
end;

procedure TdmData.taAWMOBeforeOpen(DataSet: TDataSet);
begin
  taAWMO.ParamByName('INVID').AsInteger := taAWMOListINVID.AsInteger;
end;

procedure TdmData.ConfirmDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� �������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort
end;

procedure TdmData.taTaskListCalcFields(DataSet: TDataSet);
begin
  taTaskListRECNO.AsInteger:=taTaskList.RecNo;
end;

procedure TdmData.taTaskListNewRecord(DataSet: TDataSet);
  var n:integer;
begin
  taTaskListDAT.AsDateTime:=  ServerDateTime(dm.quTmp);
  taTaskListCREATEBYUSERID.AsInteger:=dm.User.UserId;
  with dm, quTmp do begin
    if Open then quTmp.Close;
    SQL.Text:='select max(n) from tasks';
    ExecQuery;
    if Fields[0].IsNull then N:=1 else N:=Fields[0].AsInteger;
  end;
  taTaskListN.AsInteger:=N;
end;

procedure TdmData.taTaskListBeforeOpen(DataSet: TDataSet);
begin
  with taTaskList do begin
    ParamByName('BDATE').AsTimeStamp:=DateTimeToTimeStamp(dm.BeginDate);
    ParamByName('EDATE').AsTimeStamp:=DateTimeToTimeStamp(dm.EndDate);
   end;
end;

procedure TdmData.taTaskOperationsBeforeOpen(DataSet: TDataSet);
begin
  taTaskOperations.ParamByName('I_TASKID').AsInteger:=taTaskListID.AsInteger;
end;

procedure TdmData.taOperToGiveBeforeOpen(DataSet: TDataSet);
begin
  taOperToGive.ParamByName('TASKID').AsInteger:=taTaskListID.AsInteger;
end;

procedure TdmData.taTaskOperationsSTATEGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else
    case Sender.AsInteger of
      0: Text:='������';
      1: Text:='���������';
    end;
end;

procedure TdmData.taTaskOperSignsNewRecord(DataSet: TDataSet);
begin
  taTaskOperSignsTASKOPERID.AsInteger:=taTaskOperationsTASK_OPERID.AsInteger;
  taTaskOperSignsDAT.AsDateTime:=Now;
end;

procedure TdmData.taTaskImgBeforeOpen(DataSet: TDataSet);
begin
  taTaskImg.ParamByName('TASK_ID').AsInteger:=taTaskListID.AsInteger;
end;

procedure TdmData.taTaskImgNewRecord(DataSet: TDataSet);
begin
  taTaskImgTASKID.AsInteger:=taTaskListID.AsInteger;
end;

procedure TdmData.taActWorkCommonBeforeOpen(DataSet: TDataSet);
begin
  taActWorkCommon.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
  taActWorkCommon.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
  taActWorkCommon.ParamByName('Company$ID').AsInteger := StrToInt(dmMain.FilterCompId);
end;

procedure TdmData.taActWorkListCommonBeforeOpen(DataSet: TDataSet);
begin
 with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
    ByName['Company$ID'].AsInteger := StrToInt(dmMain.FilterCompId);
  end;
end;

end.
