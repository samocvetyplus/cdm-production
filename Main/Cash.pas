{*************************************}
{  ������ ����� ����� (���)           }
{  Copyrigth (C) 2004 basile for CDM  }
{  v0.10                              }
{*************************************}
unit Cash;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList,  ImgList,  ExtCtrls,
  ComCtrls, Grids, DBGridEh, TB2Dock, TB2Toolbar, TB2Item, PrnDbgeh,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmCash = class(TfmAncestor)
    gridCash: TDBGridEh;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem1: TTBItem;
    acPeriod: TAction;
    PrintDBGridEh1: TPrintDBGridEh;
    acPrint: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPeriodExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
  end;

var
  fmCash: TfmCash;

implementation

uses InvData, dbUtil, DictData, Period;

{$R *.dfm}

procedure TfmCash.FormCreate(Sender: TObject);
begin
  inherited;
  acPeriod.Caption := '������ � '+DateToStr(dm.BeginDate)+' �� '+DateToStr(dm.EndDate);
  OpenDataSet(dmInv.taCash);
end;

procedure TfmCash.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmInv.taCash);
  inherited;
end;

procedure TfmCash.acPeriodExecute(Sender: TObject);
var
  Bd, Ed : TDateTime;
begin
  Bd := dm.BeginDate;
  Ed := dm.EndDate;
  if ShowPeriodForm(Bd, Ed) then
  begin
    dm.BeginDate := Bd;
    dm.EndDate := Ed;
    acPeriod.Caption := '������ � '+DateToStr(dm.BeginDate)+' �� '+DateToStr(dm.EndDate);
    Application.ProcessMessages;
    ReOpenDataSet(dmInv.taCash);
    Application.ProcessMessages;
  end;
end;

procedure TfmCash.acPrintExecute(Sender: TObject);
begin
  inherited;
  PrintDBGridEh1.Preview;
end;

end.
