unit CheckProtocolData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGridEh, ActnList, DBGridEhGrouping, GridsEh, rxPlacemnt,
  rxSpeedbar;

type
  TfmCheckProtocolData = class(TfmAncestor)
    gridProtocolData: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCheckProtocolData: TfmCheckProtocolData;

implementation

uses MainData, dbUtil;

{$R *.dfm}

procedure TfmCheckProtocolData.FormCreate(Sender: TObject);
begin
  Screen.Cursor := crSQLWait;
  try
    ExecSQL('execute procedure RECALC_PROTOCOL_DONEW('+dmMain.taPListID.AsString+')', dmMain.quTmp);
    ExecSQL('execute procedure COMPARE_PROTOCOL_WORDER('+dmMain.taPListID.AsString+')', dmMain.quTmp);
  finally
    Screen.Cursor := crDefault;
  end;
  OpenDataSet(dmMain.taCheckProtocolData);
end;

procedure TfmCheckProtocolData.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmMain.taCheckProtocolData);
  inherited;
end;

end.
