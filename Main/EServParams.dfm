inherited fmEServParams: TfmEServParams
  Left = 291
  Top = 146
  Caption = #1055#1072#1088#1072#1084#1077#1090#1088#1099' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080
  ClientWidth = 481
  ExplicitWidth = 489
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 481
    ExplicitWidth = 481
  end
  inherited plDict: TPanel
    Width = 481
    ExplicitWidth = 481
    inherited plSelect: TPanel
      Width = 477
      ExplicitWidth = 477
      inherited btnOk: TSpeedButton
        Left = 299
        ExplicitLeft = 299
      end
      inherited btnCancel: TSpeedButton
        Left = 383
        ExplicitLeft = 383
      end
    end
    inherited gridDict: TDBGridEh
      Width = 477
      AllowedOperations = [alopUpdateEh]
      Columns = <
        item
          EditButtons = <>
          FieldName = 'MATNAME'
          Footers = <>
          Title.EndEllipsis = True
          Width = 168
        end
        item
          EditButtons = <>
          FieldName = 'U'
          Footers = <>
          KeyList.Strings = (
            '3'
            '4')
          PickList.Strings = (
            #1043#1088'.'
            #1050#1072#1088#1072#1090)
          Title.EndEllipsis = True
        end
        item
          EditButtons = <>
          FieldName = 'N'
          Footers = <>
          Title.EndEllipsis = True
        end
        item
          EditButtons = <>
          FieldName = 'SERVCOST'
          Footers = <>
          Title.EndEllipsis = True
          Width = 68
        end
        item
          EditButtons = <>
          FieldName = 'EXTRA'
          Footers = <>
          Title.EndEllipsis = True
        end>
    end
  end
  inherited tb1: TSpeedBar
    Width = 481
    ExplicitWidth = 481
  end
end
