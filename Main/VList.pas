{***********************************************}
{  ������ ������������ ����������               }
{  Copyrigth (C) 2003-2004 basile for CDM       }
{  v0.33                                        }
{  create      13.05.2003                       }
{  last update 05.10.2004                       }
{***********************************************}
unit VList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, Menus,  ImgList, DBGridEh, 
  StdCtrls, ExtCtrls, Grids, DBGrids, RXDBCtrl,
  ComCtrls, ActnList, DB, Buttons, TB2Item,
  Mask, DBCtrlsEh, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmVList = class(TfmListAncestor)
    ppPrint: TTBPopupMenu;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    acPrintSIVReport: TAction;
    acPrintVReport: TAction;
    Label1: TLabel;
    edYear: TDBNumberEditEh;
    acPrintAssort: TAction;
    TBItem7: TTBItem;
    TBSubmenuItem1: TTBSubmenuItem;
    TBItem8: TTBItem;
    TBItem9: TTBItem;
    TBItem10: TTBItem;
    SpeedItem1: TSpeedItem;
    acViewCorrections: TAction;
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintSIVReportExecute(Sender: TObject);
    procedure acPrintVReportExecute(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure edYearChange(Sender: TObject);
    procedure edYearKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acDelDocUpdate(Sender: TObject);
    procedure acPrintAssortExecute(Sender: TObject);
    procedure acViewCorrectionsExecute(Sender: TObject);
    procedure acViewCorrectionsUpdate(Sender: TObject);
  protected
    procedure DepClick(Sender : TObject);override;
    procedure ShowPeriod;override;
    procedure Access;
    procedure SetReadOnly(ReadOnly : boolean);override;
  private
    procedure SetPeriodDep(DepId : integer);
  end;

var
  fmVList: TfmVList;

implementation

uses MainData, DictData, dbUtil, DocAncestor, InvData, VSheet, DateUtils,
  PrintData, MsgDialog, fmUtils, Period, eVType, Editor, DbUtilsEh,
  ProductionConsts, InventoryCorrection;

{$R *.dfm}

procedure TfmVList.FormCreate(Sender: TObject);
var
  i, j : integer;
  Item : TMenuItem;
begin
  edYear.OnChange := nil;
  edYear.Value := dmMain.DocListYear;
  edYear.OnChange := edYearChange;
  inherited;
  Access;
  //ppPrint.Skin := dm.ppSkin;
end;

procedure TfmVList.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  SetPeriodDep(dm.CurrDep);
  ShowPeriod;
  Access;
  laDep.Caption := TMenuItem(Sender).Caption;
  FCurrDep := TMenuItem(Sender).Tag;
end;

procedure TfmVList.ShowPeriod;
begin
  inherited;
  if(dm.CurrDep = -1)then laPeriod.Caption := '<�� ���������>';
end;

procedure TfmVList.SetPeriodDep(DepId: integer);
var
  v : Variant;
begin
  // ���������� ������
  if(DepId <> -1)then
  begin
    v := ExecSelectSQL('select max(DocDate) from Act '+
      ' where T=2 and IsClose = 1 and DepId ='+IntToStr(dm.CurrDep), dmMain.quTmp);
    if not VarIsNull(v) then FBD := StartOfTheDay(v+1) else FBD := StrToDate('01.04.2003');
    if FBD > EndOfTheDay(ToDay) then FED := EndOfTheDay(EndOfTheMonth(FBD))
    else FED := EndOfTheDay(ToDay);
    ShowPeriod;
    FOnChangePeriod(FBD, FED);
  end;
end;

procedure TfmVList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(dmMain.taVListIsClose.AsInteger = 1)then Background := clBtnFace;
  if(AnsiUpperCase(Column.FieldName) = 'USERNAME')then 
    if(dmMain.taVListBUH.AsInteger = 1)then Background := clMoneyGreen
end;



procedure TfmVList.Access;
var
  u : Variant;
begin
  with stbrStatus.Panels[0] do
    if dm.IsAdm then Text := 'Adm'
    else if((dm.User.AccProd_d and 64) = 64)then Text := 'View'
    else Text := 'Full';
  if dm.IsAdm then eXit;
  if((dm.User.AccProd_d and 64) = 64)then // ��������
  begin
    spitAdd.Enabled := False;
    spitDel.Enabled := False;
    spitEdit.Enabled := False;
  end
  else begin
    u := ExecSelectSQL('select MOLID from D_PS where DEPID='+
      IntToStr(dm.User.DepID)+' and MOLID='+IntToStr(dm.User.UserId), dmMain.quTmp);
    SetReadOnly(VarIsNull(u)or(u=0));
  end;
end;

procedure TfmVList.SetReadOnly(ReadOnly: boolean);
begin
  inherited;
end;

procedure TfmVList.acPeriodExecute(Sender: TObject);
begin
  if ShowPeriodForm(FBD, FED) then
  begin
    FOnChangePeriod(FBD, FED);
    ShowPeriod;
  end;
end;

procedure TfmVList.acAddDocExecute(Sender: TObject);
var
  b : boolean;
begin
  if ShowEditor(TfmeVType, TfmEditor(fmeVType)) <> mrOk then Exit;

  with dmMain do
  try
    DataSet.Append;
    DataSet.FieldByName('BUH').AsInteger := Editor.vResult;
    DataSet.Post;
    DataSet.Refresh;
    DataSet.Transaction.CommitRetaining;
    case DataSet.FieldByName('BUH').AsInteger of
      0 : b := CreateV_for_Wh(taVListID.AsString);
      1 : b := CreateV_for_Buh(taVListID.AsString);
    end;
    if b then ShowDocForm(TfmVSheet, taVList, taVListID.AsInteger, quTmp);
  except
    on E : Exception do begin
      DataSet.Cancel;
      MessageDialogA(E.Message, mtWarning);
    end;
  end;
end;

procedure TfmVList.acEditDocExecute(Sender: TObject);
begin
  with dmMain do
  begin
    // ��������� ����� �� ��������
    { TODO : ��������� }
    if not dm.IsAdm and ((dm.User.AccProd_d and 64) <> 64)then
      if taVListDEPID.AsInteger <> dm.User.wWhId then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '��� ��������� ������������ ��������� �������� "'+taVListDEPNAME.AsString+'"']));
    ShowDocForm(TfmVSheet, taVList, taVListID.AsInteger, quTmp);
    RefreshDataSet(taVList);
  end;
end;

procedure TfmVList.acPrintSIVReportExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taVList]);
  dmInv.CurrSemis := ROOT_SEMIS_CODE;
  dmInv.CurrOper := ROOT_OPER_CODE;
  dmMain.FilterMatId := ROOT_MAT_CODE;
  dmPrint.PrintDocumentA(dkSemisReport, VarArrayOf([VarArrayOf([dmMain.taVListID.AsInteger]),
     VarArrayOf([dmMain.taVListID.AsInteger]),
     VarArrayOf([dmMain.taVListID.AsInteger])]));
end;

procedure TfmVList.acPrintVReportExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taVList]);
  dmInv.CurrSemis := ROOT_SEMIS_CODE;
  dmInv.CurrOper := ROOT_OPER_CODE;
  dmMain.FilterMatId := ROOT_MAT_CODE;
  dmMain.FilterOperId := ROOT_OPER_CODE;
    
  dmPrint.PrintDocumentA(dkVSheet, VarArrayOf([VarArrayOf([dmMain.taVListID.AsInteger]),
     VarArrayOf([dmMain.taVListID.AsInteger]),
     VarArrayOf([dmMain.taVListID.AsInteger]) ]));
end;

procedure TfmVList.acViewCorrectionsExecute(Sender: TObject);
var AReadOnly: boolean;
begin

  inherited;

  AReadOnly := true;

  //��������� �������������� ����������� � ���������������

  if (dm.User.Profile = 4) or (dm.IsAdm) then
  begin
    AReadOnly := false;
  end;

  TfmInventoryCorrection.Execute(AReadOnly);

end;

procedure TfmVList.acViewCorrectionsUpdate(Sender: TObject);
begin
  inherited;
  acViewCorrections.Enabled :=  not dmMain.taVList.IsEmpty;
end;

procedure TfmVList.acPrintDocExecute(Sender: TObject);
begin
  inherited;
  //
end;

procedure TfmVList.edYearChange(Sender: TObject);
begin
  dmMain.DocListYear := edYear.Value;
  ReOpenDataSet(DataSet);
end;

procedure TfmVList.edYearKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : begin
      dmMain.DocListYear := edYear.Value;
      ReOpenDataSet(DataSet);
    end;
  end;
end;

procedure TfmVList.acDelDocUpdate(Sender: TObject);
begin
  if dmMain.taVListISCLOSE.AsInteger = 1 then acDelDoc.Enabled := False
  else inherited;
end;

procedure TfmVList.acPrintAssortExecute(Sender: TObject);
begin
  PostDataSet(dmMain.taVList);
  dmPrint.PrintDocumentA(dkVSheetAssort, VarArrayOf([VarArrayOf([dmMain.taVListID.AsInteger])]));
end;

end.


