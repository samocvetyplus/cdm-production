inherited fmActWorkList: TfmActWorkList
  Left = 181
  Top = 243
  Caption = #1057#1087#1080#1089#1086#1082' '#1072#1082#1090#1086#1074' '#1074#1099#1087#1086#1083#1085#1077#1085#1085#1099#1093' '#1088#1072#1073#1086#1090
  ClientHeight = 402
  ClientWidth = 700
  ExplicitWidth = 708
  ExplicitHeight = 436
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 383
    Width = 700
    ExplicitTop = 383
    ExplicitWidth = 700
  end
  inherited tb2: TSpeedBar
    Width = 700
    ExplicitWidth = 700
    inherited laDep: TLabel
      Left = 588
      Top = 4
      Visible = False
      ExplicitLeft = 588
      ExplicitTop = 4
    end
    inherited laPeriod: TLabel
      Left = 348
      ExplicitLeft = 348
    end
    object lbYear: TLabel [2]
      Left = 540
      Top = 7
      Width = 73
      Height = 13
      Caption = #1042#1099#1073#1080#1088#1080#1090#1077' '#1075#1086#1076' '
      Transparent = True
      Visible = False
    end
    object lbComp: TLabel [3]
      Left = 8
      Top = 7
      Width = 67
      Height = 13
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      Transparent = True
    end
    object edYear: TDBNumberEditEh [4]
      Left = 613
      Top = 4
      Width = 60
      Height = 19
      EditButton.Style = ebsUpDownEh
      EditButton.Visible = True
      EditButtons = <>
      Flat = True
      MaxValue = 2050.000000000000000000
      MinValue = 2000.000000000000000000
      ShowHint = True
      TabOrder = 0
      Visible = False
      OnChange = edYearChange
      OnKeyDown = edYearKeyDown
    end
    object cbComp: TDBComboBoxEh [5]
      Left = 79
      Top = 4
      Width = 134
      Height = 19
      DropDownBox.Sizable = True
      DropDownBox.Width = -1
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 1
      Text = 'cbComp'
      Visible = True
      OnChange = cbCompChange
    end
    inherited spitDep: TSpeedItem
      Visible = False
    end
    inherited spitPeriod: TSpeedItem
      Left = 259
    end
  end
  inherited tb1: TSpeedBar
    Width = 700
    ExplicitWidth = 700
    inherited spitPrint: TSpeedItem [4]
      DropDownMenu = ppPrint
      OnClick = acPrintDocExecute
    end
    inherited spitEdit: TSpeedItem [5]
      OnClick = acEditDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    Width = 700
    Height = 312
    DataSource = dmData.dsrActWorkList
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    SumList.Active = True
    UseMultiTitle = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1040#1082#1090'|'#1053#1086#1084#1077#1088
        Title.EndEllipsis = True
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1040#1082#1090'|'#1044#1072#1090#1072
        Title.EndEllipsis = True
        Width = 72
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'BD'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076'|'#1053#1072#1095#1072#1083#1086
        Title.EndEllipsis = True
        Visible = False
        Width = 63
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ED'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076'|'#1050#1086#1085#1077#1094
        Title.EndEllipsis = True
        Visible = False
        Width = 72
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'COMPNAME'
        Footers = <>
        Title.Caption = #1047#1072#1082#1072#1079#1095#1080#1082
        Title.EndEllipsis = True
        Width = 124
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 96
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INVSUBTYPENAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 65
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REFDOCNO'
        Footers = <>
        Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090'|'#1053#1086#1084#1077#1088
        Title.EndEllipsis = True
        Width = 49
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REFDOCDATE'
        Footers = <>
        Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090'|'#1044#1072#1090#1072
        Title.EndEllipsis = True
        Width = 58
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REFNAME'
        Footers = <>
        Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090'|'#1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
        Title.EndEllipsis = True
        Width = 74
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INVID'
        Footers = <>
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ISCLOSE'
        Footers = <>
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'COST'
        Footer.FieldName = 'COST'
        Footer.ValueType = fvtSum
        Footers = <>
      end>
  end
  inherited ilButtons: TImageList
    Top = 192
  end
  inherited ppDep: TPopupMenu
    Top = 92
  end
  inherited acAEvent: TActionList
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
    inherited acPrintDoc: TAction
      Category = #1055#1077#1095#1072#1090#1100
      OnExecute = acPrintDocExecute
    end
    object acShowID: TAction
      ShortCut = 24644
      OnExecute = acShowIDExecute
    end
    object acPrintProdTolling: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1054#1090#1095#1077#1090' '#1086' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1077' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
      OnExecute = acPrintProdTollingExecute
      OnUpdate = acPrintProdTollingUpdate
    end
    object acPrintActWork: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1040#1082#1090' '#1087#1088#1080#1077#1084#1072'-'#1089#1076#1072#1095#1080' '#1088#1072#1073#1086#1090' ('#1091#1089#1083#1091#1075')'
      OnExecute = acPrintActWorkExecute
      OnUpdate = acPrintActWorkUpdate
    end
    object acPrintActWorkGenaral: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1057#1074#1086#1076#1085#1099#1081' '#1086#1090#1095#1077#1090
      OnExecute = acPrintActWorkGenaralExecute
    end
    object acReCalcAct: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1072#1082#1090' ('#1087#1086' '#1090#1077#1082#1091#1097#1080#1084' '#1094#1077#1085#1072#1084')'
      OnExecute = acReCalcActExecute
      OnUpdate = acReCalcActUpdate
    end
    object acPrintInvMX18: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' MX-18'
      OnExecute = acPrintInvMX18Execute
      OnUpdate = acPrintInvMX18Update
    end
    object acPrintActRet: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1040#1082#1090' '#1086' '#1074#1079#1072#1080#1084#1085#1099#1093' '#1088#1072#1089#1095#1077#1090#1072#1093
      OnExecute = acPrintActRetExecute
      OnUpdate = acPrintActRetUpdate
    end
    object acPrintAWRej: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1040#1082#1090' '#1073#1088#1072#1082#1072
      OnExecute = acPrintAWRejExecute
      OnUpdate = acPrintAWRejUpdate
    end
    object acPrintAWTorg12: TAction
      Category = #1055#1077#1095#1072#1090#1100
      Caption = #1058#1054#1056#1043'-12'
      OnExecute = acPrintAWTorg12Execute
      OnUpdate = acPrintAWTorg12Update
    end
    object acReCalcTW: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1089#1091#1084#1084#1099' '#1087#1086' '#1085#1072#1082#1083#1072#1076#1085#1099#1084' '#1079#1072' '#1074#1099#1073#1088#1072#1085#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
      OnExecute = acReCalcTWExecute
      OnUpdate = acReCalcTWUpdate
    end
    object acReCalcActGold: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1072#1082#1090' ('#1090#1086#1083#1100#1082#1086' '#1079#1086#1083#1086#1090#1086')'
      OnExecute = acReCalcActGoldExecute
      OnUpdate = acReCalcActGoldUpdate
    end
    object acReCalcActGoldAll: TAction
      Caption = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1074#1089#1077' '#1072#1082#1090#1099' ('#1090#1086#1083#1100#1082#1086' '#1079#1086#1083#1086#1090#1086')'
      OnExecute = acReCalcActGoldAllExecute
      OnUpdate = acReCalcActGoldAllUpdate
    end
    object acCalcMatByUIDAll: TAction
      Caption = #1056#1072#1089#1095#1105#1090' '#1074#1089#1077#1093' '#1072#1082#1090#1086#1074' '#1087#1077#1088#1080#1086#1076#1072
      OnExecute = acCalcMatByUIDAllExecute
    end
    object acCloseAllInThisPeriod: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1074#1089#1077' '#1079#1072' '#1087#1077#1088#1080#1086#1076
      OnExecute = acCloseAllInThisPeriodExecute
    end
  end
  inherited ppDoc: TTBPopupMenu
    Left = 212
    Top = 132
    object TBItem8: TTBItem
      Action = acReCalcAct
    end
    object TBItem13: TTBItem
      Action = acReCalcTW
    end
    object TBItem14: TTBItem
      Action = acReCalcActGold
    end
    object TBItem15: TTBItem
      Action = acReCalcActGoldAll
    end
    object TBSeparatorItem2: TTBSeparatorItem
    end
    object TBItem19: TTBItem
      Action = acCalcMatByUIDAll
    end
    object TBItem20: TTBItem
      Action = acCloseAllInThisPeriod
    end
  end
  object ppPrint: TTBPopupMenu
    Left = 264
    Top = 132
    object TBItem6: TTBItem
      Action = acPrintActWork
    end
    object TBItem5: TTBItem
      Action = acPrintProdTolling
    end
    object TBItem9: TTBItem
      Action = acPrintInvMX18
    end
    object TBItem10: TTBItem
      Action = acPrintActRet
    end
    object TBItem11: TTBItem
      Action = acPrintAWRej
    end
    object TBItem12: TTBItem
      Action = acPrintAWTorg12
    end
    object TBItem7: TTBItem
      Action = acPrintActWorkGenaral
    end
    object TBItem16: TTBItem
      Caption = #1057#1074#1086#1076#1085#1099#1081' '#1072#1082#1090' '#1087#1088#1080#1077#1084#1072'-'#1089#1076#1072#1095#1080' '#1088#1072#1073#1086#1090' ('#1091#1089#1083#1091#1075')'
      OnClick = TBItem16Click
    end
    object TBItem17: TTBItem
      Caption = #1057#1074#1086#1076#1085#1099#1081' '#1086#1090#1095#1077#1090' '#1086' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1077' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
      OnClick = TBItem17Click
    end
    object TBItem18: TTBItem
      Caption = #1057#1074#1086#1076#1085#1072#1103' '#1085#1072#1082#1083#1072#1076#1085#1072#1103' MX-18'
      OnClick = TBItem18Click
    end
  end
end
