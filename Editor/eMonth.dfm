inherited fmeMonth: TfmeMonth
  Left = 328
  Top = 217
  Caption = #1042#1099#1073#1086#1088' '#1084#1077#1089#1103#1094#1072
  ClientHeight = 74
  ClientWidth = 240
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 240
    Height = 74
    object lbMonth: TLabel [0]
      Left = 8
      Top = 12
      Width = 85
      Height = 13
      Caption = #1042#1099#1073#1080#1088#1080#1090#1077' '#1084#1077#1089#1103#1094
    end
    inherited btnOk: TBitBtn
      Left = 60
      Top = 40
    end
    inherited btnCancel: TBitBtn
      Left = 148
      Top = 40
    end
    object cmbxMonth: TDBComboBoxEh
      Left = 100
      Top = 9
      Width = 133
      Height = 19
      EditButtons = <>
      Flat = True
      Items.Strings = (
        #1103#1085#1074#1072#1088#1100
        #1092#1077#1074#1088#1072#1083#1100
        #1084#1072#1088#1090
        #1072#1087#1088#1077#1083#1100
        #1084#1072#1081
        #1080#1102#1085#1100
        #1080#1102#1083#1100
        #1072#1074#1075#1091#1089#1090
        #1089#1077#1085#1090#1103#1073#1088#1100
        #1086#1082#1090#1103#1073#1088#1100
        #1085#1086#1103#1073#1088#1100
        #1076#1077#1082#1072#1073#1088#1100)
      KeyItems.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12')
      TabOrder = 2
      Visible = True
    end
  end
end
