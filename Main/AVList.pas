unit AVList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, Menus,  ImgList, DBGridEh, 
  StdCtrls, ExtCtrls, Grids, DBGrids, RXDBCtrl,  
  ComCtrls, ActnList, fmutils, dbutil,  TB2Item,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmAVList = class(TfmListAncestor)
    procedure acAddDocExecute(Sender: TObject);
    procedure acDelDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintDocUpdate(Sender: TObject);
  end;

var
  fmAVList: TfmAVList;

implementation

uses InvData, eArtVedomost, ArtVedomost, PrintData, MsgDialog;

{$R *.dfm}

procedure TfmAVList.acAddDocExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmeArtVedomost,TForm(fmeArtVedomost));
end;

procedure TfmAVList.acDelDocExecute(Sender: TObject);
begin
  if MessageDialog('������� ���������?',mtWarning,[mbYes,mbNo],0) =mrYes
  then inherited;
end;

procedure TfmAVList.acPrintDocExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkJournalOrder,null);
end;

procedure TfmAVList.acEditDocExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmArtVedomost,TForm(fmArtVedomost));
end;

procedure TfmAVList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(dminv.taAVListISCLOSE.AsInteger = 1) then Background:=clBtnFace;
end;

procedure TfmAVList.acPrintDocUpdate(Sender: TObject);
begin
  acPrintDoc.Enabled := (DataSet.Active) and (not DataSet.IsEmpty);
end;

end.
