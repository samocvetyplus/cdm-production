inherited fmdInvColor: TfmdInvColor
  Left = 217
  Top = 138
  Width = 437
  Caption = #1062#1074#1077#1090#1086#1074#1072#1103' '#1089#1093#1077#1084#1072' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 429
  end
  inherited plDict: TPanel
    Width = 429
    inherited plSelect: TPanel
      Width = 425
      inherited btnOk: TSpeedButton
        Left = 247
      end
      inherited btnCancel: TSpeedButton
        Left = 331
      end
    end
    inherited gridDict: TDBGridEh
      Width = 425
      DataSource = dm.dsrInvColor
      OptionsEh = [dghFixed3D, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'INVTYPENAME'
          Footers = <>
          Title.Caption = #1058#1080#1087' '#1076#1086#1082#1091#1084#1077#1085#1090#1072
          Width = 126
        end
        item
          AlwaysShowEditButton = True
          ButtonStyle = cbsEllipsis
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          Footers = <>
          Title.Caption = #1062#1074#1077#1090
          Width = 87
          OnEditButtonClick = gridDictColumns1EditButtonClick
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NAME'
          Footers = <>
          Width = 148
        end>
    end
  end
  inherited tb1: TSpeedBar
    Width = 429
  end
  inherited ActionList2: TActionList
    Left = 88
  end
end
