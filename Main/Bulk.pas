{***********************************************}
{  ��� ����������� (������������) ����. ������  }
{  Copyrigth (C) 2003 basile for CDM            }
{  v0.10                                        }
{  create      28.06.2003                       }
{  last update 28.06.2003                       }
{***********************************************}

unit Bulk;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus,  ImgList, Grids, DBGridEh,
  DBCtrlsEh, StdCtrls, Mask, DBCtrls, ExtCtrls,  ComCtrls, TB2Item,
  ActnList, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmBulk = class(TfmDocAncestor)
    acEvent: TActionList;
    acPrintAct: TAction;
    TBItem1: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spitCloseClick(Sender: TObject);
    procedure acPrintActExecute(Sender: TObject);
  protected 
    FReadOnlyMode : boolean;
    procedure CloseDoc;override;
    procedure OpenDoc;override;
    procedure InitBtn(T : boolean);
  end;

var
  fmBulk: TfmBulk;

implementation

uses MainData, DictData, dbUtil, fmUtils, DateUtils, PrintData;

{$R *.dfm}

procedure TfmBulk.FormCreate(Sender: TObject);
begin
  FDocName := '��� ��������';
  dm.WorkMode := 'Bulk';
  inherited;
  with dm, dmMain do OpenDataSets([taSemis, taSIEl]);
  InitBtn(dmMain.taBulkListISCLOSE.AsInteger=1);                             
  (* �������� ������� *)
  dm.DepBeginDate := dm.GetDepBeginDate(dmMain.taBulkListDEPID.AsInteger);
  if(dm.DepBeginDate > dmMain.taBulkListDOCDATE.AsDateTime)then
  begin
    FReadOnlyMode := True;
    stbrStatus.Panels[0].Text := '�������� ������ ���������� � '+DateToStr(dm.DepBeginDate);
    lbNoDoc.Enabled := False;
    edNoDoc.Enabled := False;
    lbDateDoc.Enabled := False;
    dtedDateDoc.Enabled := False;
    gridItem.ReadOnly := True;
  end
  else FReadOnlyMode := False;
end;

procedure TfmBulk.CloseDoc;
begin
  ExecSQL('execute procedure CloseDoc("��� ��������", '+dmMain.taBulkListINVID.AsString+')', dmMain.quTmp);
end;

procedure TfmBulk.OpenDoc;
begin
  ExecSQL('execute procedure OpenDoc("��� ��������", '+dmMain.taBulkListINVID.AsString+')', dmMain.quTmp);
end;

procedure TfmBulk.InitBtn(T: boolean);
begin
  if not T then // �������� ������
  begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 6;
  end
  else begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 5;
  end
end;


procedure TfmBulk.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  CloseDataSets([dm.taSemis, dm.taOper]);
end;

procedure TfmBulk.spitCloseClick(Sender: TObject);
begin
  PostDataSet(dmMain.taBulk);
  if dmMain.taBulk.RecordCount=0 then raise EWarning.Create('������ ������� ���������!');
  if spitClose.BtnCaption = '������' then
    try
      PostDataSet(dmMain.taBulk);
      CloseDoc;
      spitClose.BtnCaption := '������';
      spitClose.Hint := '������� ��������';
      spitClose.ImageIndex := 5;
      RefreshDataSet(dmMain.taBulkList);
    except
      on E: Exception do { TODO : !!! }//if not TranslateIbMsg(E, dmMain.taBulk.Database, dmMain.taBulk.Transaction)
           //then
            ShowMessage(E.Message);
    end
  else
    try
      PostDataSet(dmMain.taBulkList);
      OpenDoc;
      spitClose.BtnCaption := '������';
      spitClose.Hint := '������� ��������';
      spitClose.ImageIndex := 6;

      RefreshDataSet(dmMain.taBulkList);
    except
      on E: Exception do { TODO : !!! }//if not TranslateIbMsg(E, dmMain.taBulk.Database, dmMain.taBulk.Transaction)
           //then
           ShowMessage(E.Message);
    end
end;

procedure TfmBulk.acPrintActExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkBulkAct, VarArrayOf([VarArrayOf([dmMain.taBulkListINVID.AsInteger])]));
end;

end.
