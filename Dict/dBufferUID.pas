unit dBufferUID;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList, ImgList, Grids,
  DBGridEh, Buttons, ExtCtrls, ComCtrls, TB2Dock, TB2Toolbar, pFIBQuery, SInvProd, db,
  TB2Item, Menus, pFIBDataSet, FIBQuery, DBGridEhGrouping, rxPlacemnt,
  rxSpeedbar, GridsEh;

type
  TfmdBufferUID = class(TfmDictAncestor)
    dkSheet: TTBDock;
    tlbrSheet: TTBToolbar;
    quDDL: TpFIBQuery;
    ppDict: TTBPopupMenu;
    acPrint: TAction;
    TBItem1: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormResize(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acPrintUpdate(Sender: TObject);
  private
    x : integer;
    FSelComponent : TButtonPanel;
  public
    procedure BeforeOpen(Sender : TDataSet);
    procedure CreateSheetButton;
    procedure CellGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure ButtonClick(Sender : TObject);
  end;

var
  fmdBufferUID: TfmdBufferUID;

implementation

uses MainData, DictData, dbUtil, MsgDialog, PrintData;

{$R *.dfm}

procedure TfmdBufferUID.BeforeOpen(Sender: TDataSet);
begin
  with TpFIBDataSet(dmMain.taB_UID).Params do
    if Assigned(FSelComponent) then ByName['SHEETNO'].AsInteger := (FSelComponent as TButtonPanel).Data.SheetNo
    else ByName['SHEETNO'].AsInteger := 1;
end;

procedure TfmdBufferUID.ButtonClick(Sender: TObject);
begin
  if (Sender is TButtonPanel) then
  begin
    if Assigned(FSelComponent) then FSelComponent.BevelOuter := bvNone;
    (Sender as TButtonPanel).BevelOuter := bvLowered;
    //�������
    CloseDataSet(dmMain.taB_UID);
    //dmMain.SheetMatrixProd := (Sender as TButtonPanel).Data.SheetNo;
    FSelComponent := (Sender as TButtonPanel);
    OpenDataSet(dmMain.taB_UID);
  end;
end;

procedure TfmdBufferUID.CellGetText(Sender: TField; var Text: String; DisplayText: Boolean);
var
  sl : TStringList;
begin
  sl := TStringList.Create;
  try
    sl.Delimiter := ';';
    sl.DelimitedText := Sender.AsString;
    Text := '���. '+sl.Values['ART']+#13+'UID='+sl.Values['UID'];
  finally
    sl.Free;
  end;
end;

procedure TfmdBufferUID.CreateSheetButton;
var
  SheetButton : TButtonPanel;
  Separator : TTBSeparatorItem;
  Item : TTBControlItem;
  i, c : integer;
  SheetNo : integer;
begin
  (* ������� ������ *)
  tlbrSheet.BeginUpdate;
  try
    i := 0;
    while i<=Pred(tlbrSheet.ComponentCount)do
    begin
      if tlbrSheet.Components[i].InheritsFrom(TButtonPanel) then
         TButtonPanel(tlbrSheet.Components[i]).Free
      else inc(i);
    end;
  finally
    tlbrSheet.EndUpdate;
  end;
  Item := nil;
  c := ExecSelectSQL('select count(*) from B_UID where UserId='+IntToStr(dm.User.UserId), dmMain.quTmp);
  OpenDataSet(dm.taRec);
  if(c=0)then SheetNo := 1
  else if(c mod (dm.taRecX_MATRIXPROD.AsInteger*dm.taRecY_MATRIXPROD.AsInteger) = 0)
  then SheetNo := Trunc(c / (dm.taRecX_MATRIXPROD.AsInteger*dm.taRecY_MATRIXPROD.AsInteger))
  else SheetNo := Trunc(c / (dm.taRecX_MATRIXPROD.AsInteger*dm.taRecY_MATRIXPROD.AsInteger))+1;
  i := 1;
  tlbrSheet.BeginUpdate;
  try
    for i:=0 to Pred(SheetNo) do
    begin
      if Assigned(Item) then
      Separator := TTBSeparatorItem.Create(Item);
      SheetButton := TButtonPanel.Create(tlbrSheet);
      SheetButton.Data := TButtonInfo.Create;
      with SheetButton.Data do
      begin
        SheetNo := i+1;
      end;
      SheetButton.Height := 20;
      SheetButton.Width := 45;
      SheetButton.OnMouseMove := nil;
      SheetButton.OnClick := ButtonClick;
      SheetButton.BevelOuter := bvNone;
      SheetButton.Caption := '���� '+IntToStr(i+1);
      SheetButton.Tag := i;
      SheetButton.Parent := tlbrSheet;
    end;
  finally
    tlbrSheet.EndUpdate;
  end;
end;

procedure TfmdBufferUID.FormCreate(Sender: TObject);
var
  NoProc, s : string;
  bDrop : boolean;
  i : integer;
  sf : TStringField;
begin
  //ppDict.Skin := dm.ppSkin;
  FSelComponent := nil;
  // ������� ������ ��� ���������� ������� �������.
  NoProc := IntToStr(Integer(@dm.tr.Handle));
  dmMain.taB_UID.BeforeOpen := BeforeOpen;
  ReOpenDataSet(dm.taRec);
  dmMain.taB_UID.SelectSQL.Text := '';
  try
    bDrop := ExecSelectSQL('select count(*) from RDB$PROCEDURES where RDB$PROCEDURE_NAME="TMP_B_UID_S_'+NoProc+'"', dmMain.quTmp)<>0;
    if bDrop then
      ExecSQL('drop procedure RDB$PROCEDURE_NAME="TMP_B_UID_S_'+NoProc+'"', quDDL);
  except
    MessageDialogA('���������� ������� ��������� TMP_B_UID_S_'+NoProc, mtWarning);
  end;

  s := 'create procedure Tmp_B_UID_S_'+NoProc+'(I_USERID integer, SheetNo integer  ) '+
   ' returns(';
  for i:=0 to Pred(dm.taRecY_MATRIXPROD.AsInteger)do
    s := s+'Col'+IntToStr(i)+' char(4000), ';
  s := copy(s, 0, length(s)-2)+') as '+
    'declare variable ARTID INTEGER; '+
    'declare variable ART CHAR(20); '+
    'declare variable ART2ID INTEGER; '+
    'declare variable ART2 CHAR(20); '+
    'declare variable UID INTEGER; '+
    'declare variable W DOUBLE PRECISION; '+
    'declare variable SZID INTEGER; '+
    'declare variable SZNAME CHAR(20); '+
    'declare variable SEMISNAME CHAR(20); '+
    'declare variable c integer; '+
    'declare variable i integer; '+
    'declare variable sheetuid integer; '+
    'declare variable sheetcount integer; '+
    'declare variable uidcount integer; '+
    'declare variable k integer; '+
  'begin ';
  s := s+'i=0; '+
         'c='+IntToStr(dm.taRecY_MATRIXPROD.AsInteger-1)+'; '+
         'sheetuid='+IntToStr(dm.taRecY_MATRIXPROD.AsInteger)+'*'+IntToStr(dm.taRecX_MATRIXPROD.AsInteger)+';'+
         'sheetcount = 1;'+
         'uidcount = 0; '+
         'k = 0;';
  s := s+' for '+
      '  select ARTID,ART,ART2ID,ART2,UID,W,SZID,SZNAME,SEMISNAME '+
      '  from B_UID_S(:I_UserId) '+
      '  into :ARTID,:ART,:ART2ID,:ART2,:UID,:W,:SZID,:SZNAME,:SEMISNAME '+
      'do begin '+
      '  if (uidcount = sheetuid) then '+
      '  begin '+
      '    sheetcount = sheetcount+1; '+
      '    uidcount=0; '+
      '  end '+
      '  if (sheetcount = sheetno) then '+
      '  begin ';
  for i:=0 to Pred(dm.taRecY_MATRIXPROD.AsInteger)do
    s:=s+' if(i='+IntToStr(i)+')then Col'+IntToStr(i)+'="ART="||stretrim(:ART)||";UID="||:UID||";ART2="||stretrim(:ART2)||";SZNAME="||stretrim(:SZNAME)||";GOOD="||stretrim(:SEMISNAME)||";ART2ID="||stretrim(:ART2ID)||";W="||stretrim(:W);';   // '(sheetno-1)*sheetuid+k+'+IntToStr(i)+';';
  s:= s+' if(c=i)then begin k=k+c+1;suspend;i=0; ';
  for i:=0 to Pred(dm.taRecY_MATRIXPROD.AsInteger)do
    s:=s+' Col'+IntToStr(i)+'=null; ';
  s:=s+' end '+
      '    else i=i+1; '+
      '    if (k>=sheetuid) then sheetcount=sheetcount+1; '+
      '  end '+
      '  else uidcount=uidcount+1; '+
      'end '+
      'if ((i<>(c+1))and(sheetcount=sheetno)) then suspend; '+
      'end ';
  ExecSQL(s, quDDL);
  s :='select ';
  for i:=0 to Pred(dm.taRecY_MATRIXPROD.AsInteger) do
    s := s+' Col'+IntToStr(i)+', ';
  s := copy(s, 0, length(s)-2)+' from Tmp_B_UID_S_'+NoProc+'('+IntToStr(dm.User.UserId)+', :SHEETNO)';
  dmMain.taB_UID.SelectSQL.Text := s;
  // �������� ���� � �������
  for i:=0 to Pred(dm.taRecY_MATRIXPROD.AsInteger) do
  begin
    sf := TStringField.Create(dmMain.taB_UID);
    sf.FieldKind := fkData;
    sf.Size := 4000;
    sf.DisplayWidth := 30;
    sf.FieldName := 'Col'+IntToStr(i);
    sf.Name := dmMain.taB_UID.Name+sf.FieldName;
    sf.DisplayLabel := IntToStr(i+1);
    sf.DataSet := dmMain.taB_UID;
    sf.OnGetText := CellGetText;
  end;
  // �������� ������� � ����
  x := dm.taRecX_MATRIXPROD.AsInteger;
  if(x=0)then gridDict.RowHeight := gridDict.Height
  else gridDict.RowHeight := Trunc(gridDict.Height / x);
  gridDict.Columns.BeginUpdate;
  with dmMain.taB_UID do
  try
    for i:=0 to Pred(Fields.Count) do
      with gridDict.Columns.Add do
      begin
        FieldName := Fields[i].FieldName;
        Alignment := taLeftJustify;
        Width := Trunc(gridDict.Width / 20);
        ReadOnly := True;
        Title.Caption := Field.DisplayLabel;
        ToolTips := True;
        Title.Alignment := taCenter;
        Tag := i;
      end;
  finally
    gridDict.Columns.EndUpdate;
  end;
  CreateSheetButton;
  CloseDataSet(dm.taRec);
  inherited;
end;

procedure TfmdBufferUID.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i : integer;
begin
  inherited;
  dmMain.taB_UID.BeforeOpen := nil;
  try
    ExecSQL('drop procedure Tmp_B_UID_S_'+IntToStr(Integer(@dm.tr.Handle)), quDDL);
  except
  end;
  // ������� ������
  tlbrSheet.BeginUpdate;
  try
    i := 0;
    while i<=Pred(tlbrSheet.ComponentCount)do
    begin
      if tlbrSheet.Components[i].InheritsFrom(TButtonPanel) then
         TButtonPanel(tlbrSheet.Components[i]).Free
      else inc(i);
    end;
  finally
    tlbrSheet.EndUpdate;
  end;
  // ������� ������� � �����
  gridDict.Columns.BeginUpdate;
  with gridDict, Columns do
  try
    i := 0;
    while i<=Pred(Count) do Columns[i].Free;
  finally
    EndUpdate;
  end;
  // ������� ����
  i:=0;
  while i<=Pred(dmMain.taB_UID.FieldCount) do dmMain.taB_UID.Fields[i].Free;
end;

procedure TfmdBufferUID.FormResize(Sender: TObject);
begin
  inherited;
  if(x=0)then gridDict.RowHeight := gridDict.Height
  else gridDict.RowHeight := Trunc(gridDict.Height / x);
end;

procedure TfmdBufferUID.acPrintExecute(Sender: TObject);
var
  i : integer;
begin
  with dmPrint, dmMain.taB_UID do
  begin
    TagSource := 1;
    for i:=0 to Pred(FieldCount) do Fields[i].OnGetText := nil;
    try
      PrintDocumentA(dkTag2, Null);
    finally
      for i:=0 to Pred(FieldCount) do Fields[i].OnGetText := CellGetText;
    end;
  end;
end;

procedure TfmdBufferUID.acPrintUpdate(Sender: TObject);
begin
  acPrint.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

end.


