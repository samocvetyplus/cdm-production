program Production;



uses
  Forms,
  SysUtils,
  cxGridStrs in 'cxGridStrs.pas',
  RxAppUtils,
  fmUtils,
  Main in 'Main.pas' {fmMain},
  dOper in 'Dict\dOper.pas' {fmOper},
  dSemis in 'Dict\dSemis.pas' {fmSemis},
  dMOL in 'Dict\dMOL.pas' {fmMOL},
  MainData in 'Main\MainData.pas' {dmMain: TDataModule},
  WOrderList in 'Main\WOrderList.pas' {fmWOrderList},
  Setting in 'Setting.pas' {fmSetting},
  dComp in 'Dict\dComp.pas' {fmComp},
  WOrder in 'Main\WOrder.pas' {fmWOrder},
  Splash in 'Splash.pas' {fmSplash},
  RProd in 'RProd.pas' {fmRProd},
  dTails in 'Dict\dTails.pas' {fmTails},
  PList in 'Main\PList.pas' {fmPList},
  Protocol in 'Main\Protocol.pas' {fmProtocol},
  ZList in 'Main\ZList.pas' {fmZList},
  Zp in 'Main\Zp.pas' {fmZp},
  ApplData in 'Main\ApplData.pas' {dmAppl: TDataModule},
  AList in 'Main\AList.pas' {fmAList},
  eJa in 'Editor\eJa.pas' {fmJaEditor},
  eJaSz in 'Editor\eJaSz.pas' {fmJaSzEditor},
  dPrill in 'Dict\dPrill.pas' {fmPrill},
  dMat in 'Dict\dMat.pas' {fmMat},
  dNDS in 'Dict\dNDS.pas' {fmNDS},
  dDiscount in 'Dict\dDiscount.pas' {fmDiscount},
  dIns in 'Dict\dIns.pas' {fmIns},
  dEdgT in 'Dict\dEdgT.pas' {fmEdgT},
  dEdgShape in 'Dict\dEdgShape.pas' {fmEdgShape},
  Appl in 'Main\Appl.pas' {fmAppl},
  dArt in 'Dict\dArt.pas' {fmArt},
  SInv_det in 'Main\SInv_det.pas' {fmSInv_det},
  SItem in 'Main\SItem.pas' {fmSItem},
  InvData in 'DM\InvData.pas' {dmInv: TDataModule},
  SList in 'Main\SList.pas' {fmSList},
  dSz in 'Dict\dSz.pas' {fmSz},
  ADistr in 'Main\ADistr.pas' {fmADistr},
  Store in 'Main\Store.pas' {fmSTORE},
  StoreItems in 'Main\StoreItems.pas' {fmStoreItems},
  INV_STORE in 'Main\INV_STORE.pas' {fmINV_STORE},
  StoreByItems in 'Main\StoreByItems.pas' {fmStorebyItems},
  ePArt in 'Editor\ePArt.pas' {fmePArt},
  ItemHistory in 'Main\ItemHistory.pas' {fmItemHistory},
  AIns in 'Main\AIns.pas' {fmAIns},
  eIns in 'Editor\eIns.pas' {fmeIns},
  eSemisProp in 'Editor\eSemisProp.pas' {fmeSemisProp},
  dColor in 'Dict\dColor.pas' {fmColor},
  dCleannes in 'Dict\dCleannes.pas' {fmCleannes},
  dChromaticity in 'Dict\dChromaticity.pas' {fmChromaticity},
  dIGr in 'Dict\dIGr.pas' {fmIGr},
  ePswd in 'Editor\ePswd.pas' {fmPswd},
  Store_SUM in 'Main\Store_SUM.pas' {fmStore_SUM},
  eQ in 'Editor\eQ.pas' {fmeQ},
  INVCopy in 'Main\INVCopy.pas' {fmInvCopy},
  Month in 'Main\Month.pas' {fmMonth},
  SInvProd in 'Main\SInvProd.pas' {fmSInvProd},
  dPrice in 'Dict\dPrice.pas' {fmPriceDict},
  NewPrice in 'Main\NewPrice.pas' {fmNewPrice},
  PriceACT in 'Main\PriceACT.pas' {fmPriceACT},
  ACTbyItem in 'Main\ACTbyItem.pas' {fmACTbyItem},
  dWhA2 in 'Dict\dWhA2.pas' {fmWhA2},
  eW in 'Editor\eW.pas' {fmeW},
  eSz in 'Editor\eSz.pas' {fmeSz},
  errWOrderId in 'Err\errWOrderId.pas' {fmErrWOrderId},
  dCode in 'Dict\dCode.pas' {fmCode},
  Pact_Items in 'Main\Pact_Items.pas' {fmPact_Items},
  ArtImage in 'Main\ArtImage.pas' {fmImage},
  SIEl in 'Main\SIEl.pas' {fmSIEl},
  Err_View in 'Main\Err_View.pas' {fmErrArt},
  dOperOut in 'Dict\dOperOut.pas' {fmOperOut},
  dOperIn in 'Dict\dOperIn.pas' {fmOperIn},
  eSQ in 'Editor\eSQ.pas' {fmeSQ},
  DIEl in 'Main\DIEl.pas' {fmDIEl},
  eQW in 'Editor\eQW.pas' {fmeQW},
  SIList in 'Main\SIList.pas' {fmSIList},
  Access in 'Main\Access.pas' {fmAccess},
  errWhArt in 'Err\errWhArt.pas' {fmErrWhArt},
  ASList in 'Main\ASList.pas' {fmASList},
  dOperTails in 'Dict\dOperTails.pas' {fmOperTails},
  errWhSemis in 'Err\errWhSemis.pas' {fmErrWhSemis},
  errSInvProd in 'Err\errSInvProd.pas' {fmErrSInvProd},
  WhArt in 'Main\WhArt.pas' {fmWhArt},
  WhSemis in 'Main\WhSemis.pas' {fmWHSemis},
  Protocol_d in 'Main\Protocol_d.pas' {fmProtocol_d},
  ProtItem in 'Main\ProtItem.pas' {fmProtItem},
  DIList in 'Main\DIList.pas' {fmDIList},
  eDSemis in 'Editor\eDSemis.pas' {fmDSemis},
  hWhArt in 'Main\hWhArt.pas' {fmHWhArt},
  ApplProd in 'Main\ApplProd.pas' {fmApplProd},
  dAList in 'Dict\dAList.pas' {fmdAList},
  eNewPriceU in 'Editor\eNewPriceU.pas' {fmNewPriceU},
  eMW in 'Editor\eMW.pas' {fmMW},
  dSemisT1 in 'Dict\dSemisT1.pas' {fmSemisT1},
  ExecAppl in 'Main\ExecAppl.pas' {fmExecAppl},
  RejAdd in 'Main\RejAdd.pas' {fmRejAdd},
  eUID in 'Editor\eUID.pas' {fmUID},
  dTransformPs in 'Dict\dTransformPs.pas' {fmTransformPs},
  dOperRej in 'Dict\dOperRej.pas' {fmOperRej},
  SOList in 'Main\SOList.pas' {fmSOList},
  SOEl in 'Main\SOEl.pas' {fmSOEl},
  TmpWhSemis in 'Main\TmpWhSemis.pas' {fmTmpWHSemis},
  eCasePS in 'Editor\eCasePS.pas' {fmCasePS},
  ProtSJInfo in 'Main\ProtSJInfo.pas' {fmProtSJInfo},
  Log in 'Log.pas' {fmLog},
  ApplWhArt in 'Main\ApplWhArt.pas' {fmApplWhArt},
  ProtWhItem in 'Main\ProtWhItem.pas' {fmProtWhItem},
  eInvCreateMode in 'Editor\eInvCreateMode.pas' {fmeInvCreateMode},
  VList in 'Main\VList.pas' {fmVList},
  VSheet in 'Main\VSheet.pas' {fmVSheet},
  PArt in 'Main\PArt.pas' {fmPArt},
  BulkList in 'Main\BulkList.pas' {fmBulkList},
  Bulk in 'Main\Bulk.pas' {fmBulk},
  ePriceItem in 'Editor\ePriceItem.pas' {fmPriceItem},
  dTech in 'Dict\dTech.pas' {fmTech},
  dDepT1 in 'Dict\dDepT1.pas' {fmDepT1},
  vart in 'Main\vart.pas' {fmVArt},
  dArt2Ins in 'Dict\dArt2Ins.pas' {fmA2Ins},
  eArtIns in 'Editor\eArtIns.pas' {fmArtIns},
  dTransMat in 'Dict\dTransMat.pas' {fmTransMat},
  eLostStones in 'Editor\eLostStones.pas' {fmLostStones},
  dZpK in 'Dict\dZpK.pas' {fmdZpK},
  ZpAssort in 'Main\ZpAssort.pas' {fmZpAssort},
  AFList in 'Main\AFList.pas' {fmAfList},
  DbLogin in 'DbLogin.pas' {fmDBLogin},
  Protocol_PS in 'Main\Protocol_PS.pas' {fmProtocol_PS},
  Affinaj in 'Main\Affinaj.pas' {fmAffinaj},
  dSemisAff in 'Dict\dSemisAff.pas' {fmdSemisAff},
  AVList in 'Main\AVList.pas' {fmAVList},
  dRej in 'Dict\dRej.pas' {fmdRej},
  eArtVedomost in 'Editor\eArtVedomost.pas' {fmEArtVedomost},
  ArtVedomost in 'Main\ArtVedomost.pas' {fmArtVedomost},
  LostActs in 'Main\LostActs.pas' {fmLostActs},
  LostStones_det in 'Main\LostStones_det.pas' {fmLostStones_det},
  WOrderRej in 'Main\WOrderRej.pas' {fmWOrderRej},
  eAffInvType in 'Editor\eAffInvType.pas' {fmeAffInvT},
  dSimpleDepart in 'Dict\dSimpleDepart.pas' {fmdSimpleDepart},
  Export1C in 'Main\Export1C.pas' {fmExport1C},
  ProtocolMat in 'Main\ProtocolMat.pas' {fmProtocolMat},
  OutReport in 'Main\OutReport.pas' {fmOutReport},
  eVType in 'Editor\eVType.pas' {fmeVType},
  InitStone in 'Main\InitStone.pas' {fmInitStone},
  eOperOrder in 'Editor\eOperOrder.pas' {fmOperOrder},
  CheckProtocolData in 'Main\CheckProtocolData.pas' {fmCheckProtocolData},
  WOrderTotal in 'Main\WOrderTotal.pas' {fmWOrderTotal},
  SChargeList in 'Main\SChargeList.pas' {fmSChargeList},
  SCharge in 'Main\SCharge.pas' {fmSCharge},
  dPaytype in 'Dict\dPaytype.pas' {fmPaytypeDict},
  CheckStone in 'Main\CheckStone.pas' {fmCheckStone},
  dBufferUID in 'Dict\dBufferUID.pas' {fmdBufferUID},
  SelectInv in 'Main\SelectInv.pas' {fmSelectInv},
  WhTails in 'Main\WhTails.pas' {fmWhTails},
  ExportMC in 'Main\ExportMC.pas' {fmExportMC},
  dSzDiamond in 'Dict\dSzDiamond.pas' {fmdSzDiamond},
  eArtSzDiamond in 'Editor\eArtSzDiamond.pas' {fmeArtSzDiamond},
  eQ1 in 'Editor\eQ1.pas' {fmeQ1},
  NeedStone in 'Main\NeedStone.pas' {fmNeedStone},
  eSelAppl in 'Editor\eSelAppl.pas' {fmeSelAppl},
  AnlzRej in 'Main\AnlzRej.pas' {fmAnlzRej},
  FRData in 'DM\FRData.pas' {dmFR: TDataModule},
  eCheckOpt in 'Editor\eCheckOpt.pas' {fmeCheckOpt},
  Cash in 'Main\Cash.pas' {fmCash},
  ImportMC in 'Main\ImportMC.pas' {fmImportMC},
  dClient in 'Dict\dClient.pas' {fmdClient},
  DictData in 'DM\DictData.pas' {dm: TDataModule},
  dContract in 'Dict\dContract.pas' {fmdContract},
  Inv_Store4ArtId in 'Main\Inv_Store4ArtId.pas' {fmInv_Store4ArtId},
  dInvColor in 'Dict\dInvColor.pas' {fmdInvColor},
  VerificationList in 'Main\VerificationList.pas' {fmVerificationList},
  Data in 'DM\Data.pas' {dmData: TDataModule},
  ActWork in 'Main\ActWork.pas' {fmActWork},
  CompLogin in 'CompLogin.pas' {fmCompLogin},
  InvCopyTolling in 'Main\InvCopyTolling.pas' {fmInvCopyTolling},
  ActWorkList in 'Main\ActWorkList.pas' {fmActWorkList},
  eActVerification in 'Editor\eActVerification.pas' {fmeActVerification},
  OperHistory in 'Main\OperHistory.pas' {fmOperHistory},
  RequestList in 'Main\RequestList.pas' {fmRequestList},
  PrintData2 in 'DM\PrintData2.pas' {dmPrint2: TDataModule},
  PrintData in 'DM\PrintData.pas' {dmPrint: TDataModule},
  Request in 'Main\Request.pas' {fmRequest},
  dArtRoot in 'Dict\dArtRoot.pas' {fmdArtRoot},
  eComp in 'Editor\eComp.pas' {fmeComp},
  ChangeAssortByAppl in 'Main\ChangeAssortByAppl.pas' {fmChangeAssortByAppl},
  ProductionConsts in 'ProductionConsts.pas',
  dPriceList in 'Dict\dPriceList.pas' {fmPriceList},
  VerificationAct in 'Main\VerificationAct.pas' {fmVerificationAct},
  GODEXIntf in 'EZ1100\GODEXIntf.pas',
  eStickerDirection in 'Editor\eStickerDirection.pas' {fmeStikerDirection},
  ScaleData in 'DM\ScaleData.pas' {dmScale: TDataModule},
  Rev_Det in 'Main\Rev_Det.pas' {fmRevDet},
  RList in 'Main\RList.pas' {fmRList},
  eCoverDepart in 'Editor\eCoverDepart.pas' {fmeCoverDepart},
  eNotInInv in 'Editor\eNotInInv.pas' {fmNotInInv},
  Protocol_PsCover in 'Main\Protocol_PsCover.pas' {fmProtocol_PsCover},
  DotMatrixReport in 'Main\DotMatrixReport.pas' {fmDotMatrixReport},
  qArtStone in 'Editor\qArtStone.pas' {fmqArtStone},
  Ancestor in 'Share\Ancestor.pas' {fmAncestor},
  Editor in 'Share\Editor.pas' {fmEditor},
  SettingAncestor in 'Share\SettingAncestor.pas' {fmAncestorSetting},
  DictAncestor in 'Share\DictAncestor.pas' {fmDictAncestor},
  DocAncestor in 'Share\DocAncestor.pas' {fmDocAncestor},
  ListAncestor in 'Share\ListAncestor.pas' {fmListAncestor},
  Period in 'Share\Period.pas' {fmPeriod},
  Sort in 'Share\Sort.pas' {fmSort},
  DbEditor in 'Share\DbEditor.pas' {fmDbEditor},
  ProductionHelp in 'ProductionHelp.pas',
  DocMovingSemis in 'Main\DocMovingSemis.pas' {fmDocMoving},
  CashBank in 'Main\CashBank.pas' {fmCashBank},
  eUIDInfo in 'Editor\eUIDInfo.pas' {fmeUIDInfo},
  MBList in 'Main\MBList.pas' {fmMBList},
  MatBalance in 'Main\MatBalance.pas' {fmMatBalance},
  eMonth in 'Editor\eMonth.pas' {fmeMonth},
  AWMatOffList in 'Main\AWMatOffList.pas' {fmAWMatOffList},
  AWMatOff in 'Main\AWMatOff.pas' {fmAWMatOff},
  eSelDate in 'Editor\eSelDate.pas' {fmSelDate},
  AWMatOff_UID in 'Main\AWMatOff_UID.pas' {fmAWMatOff_UID},
  EServParams in 'Main\EServParams.pas' {fmEServParams},
  EServMat in 'Main\EServMat.pas' {fmEServMat},
  eArt in 'Editor\eArt.pas' {fmeArt},
  eSelWh in 'Editor\eSelWh.pas' {fmeSelWh},
  CompDoc in 'Main\CompDoc.pas' {fmCompDoc},
  TaskList in 'Main\TaskList.pas' {fmTaskList},
  eTask in 'Editor\eTask.pas' {fmeTask},
  eGivTask in 'Editor\eGivTask.pas' {fmeGivTask},
  eTaskImg in 'Editor\eTaskImg.pas' {fmeTaskImage},
  Analiz in 'Main\Analiz.pas' {fmAnaliz},
  eInvWeight in 'Editor\eInvWeight.pas' {fmeInvWeight},
  SInv_Det_SIList in 'SInv_Det\SIList\SInv_Det_SIList.pas' {fm_SInv_Det_SIList},
  frmSetPrice in 'Main\frmSetPrice.pas' {DialogSetPrice},
  CalcExpress in 'CalcExpress.pas',
  uUtils in 'uUtils.pas',
  frmTaskArticleOut in 'Main\frmTaskArticleOut.pas' {DialogTaskArticleOut},
  frmAffiangePassport in 'frmAffiangePassport.pas' {DialogAffinage},
  dmAffiange in 'Source\dmAffiange.pas' {DataModuleAffiange: TDataModule},
  uDialogs in 'Source\uDialogs.pas',
  frmDialogSemisRej in 'frmDialogSemisRej.pas' {DialogSemisRej},
  frmDialogSemis in 'frmDialogSemis.pas' {DialogSemis},
  uSemisStorage in 'uSemisStorage.pas',
  frmControl in 'frmControl.pas' {ControlDialog},
  frmPassportConfigure in 'frmPassportConfigure.pas' {DialogPassportConfigure},
  frmAnalizeSell in 'frmAnalizeSell.pas' {DialogAnalizeSell},
  frmNormLosses in 'frmNormLosses.pas' {DialogNormLosses},
  frmDialogArticleStructure in 'frmDialogArticleStructure.pas' {DialogArticleStructure},
  uBuyInvoice in 'uBuyInvoice.pas',
  uScanCode in 'uScanCode.pas',
  frmAffiangePassport2 in 'frmAffiangePassport2.pas' {DialogAffinage2},
  dmReportInventory in 'dmReportInventory.pas' {ReportInventory: TDataModule},
  dmErrorHandler in 'dmErrorHandler.pas' {ErrorHandler: TDataModule},
  frmLink in 'frmLink.pas' {DialogLink},
  SuplierCase in 'SuplierCase.pas' {fmSuplierCase},
  PrintPersonalInfo in 'PrintPersonalInfo.pas' {PlaceOfStorageInventoryPrint},
  Unit5 in 'Unit5.pas' {Form5},
  AddEmptyInv in 'AddEmptyInv.pas' {fmAddEmptyInv},
  BalanceAndPayments in 'BalanceAndPayments.pas' {fmPaymentsAndBalance},
  VerificationListTolling in 'Main\VerificationListTolling.pas' {fmVerificationListTolling},
  VerificationActTolling in 'VerificationActTolling.pas' {fmVerificationActTolling},
  TechnologyGroups in 'TechnologyGroups.pas' {fmTechnologyGroups},
  TechnologyGroupCustomization in 'TechnologyGroupCustomization.pas' {fmTechnologyGroupCustomization},
  uDialogSetParams in 'uDialogSetParams.pas' {DialogSetParams},
  fmDialogTechnologyRecalc in 'fmDialogTechnologyRecalc.pas' {DialogTechnologyRecalc},
  FinMonitoring in 'FinMonitoring.pas' {fmFinMonitoring},
  fmAssayStickersPrint in 'fmAssayStickersPrint.pas' {fmPrintAssayStickers},
  fmDialogBuyReport in 'fmDialogBuyReport.pas' {DialogBuyReport},
  DialogTechnologyOperationReport in 'DialogTechnologyOperationReport.pas' {dlgTechnologyOperationReport},
  InventoryCorrection in 'InventoryCorrection.pas' {fmInventoryCorrection},
  CrudeStore in 'CrudeStore.pas' {fmCrudeStore},
  PatternsData in 'PatternsData.pas' {PatternsImport: TDataModule},
  eDate in 'Editor\eDate.pas' {fmEDate},
  dmEventAlerts in 'DM\dmEventAlerts.pas' {EventAlertsData: TDataModule},
  DialogImport in 'Import\DialogImport.pas' {fmDialogImport},
  ImportData in 'Import\ImportData.pas' {dmImportData: TDataModule};

{$R *.res}

var
  Hook: TApplicationHook;

procedure InitAppParam;
var
  i : integer;
begin
  AppDebug := False;
  for i := 1 to ParamCount do
    if (UpperCase(ParamStr(i))='DEBUG') then AppDebug := True;
end;

begin
  with Application do
  begin
    Initialize;
    RxAppUtils.OnGetDefaultIniName := @GetIniFileName;
    Title := '������������ ��������� �������';
    HelpFile := ExtractFileDir(ExeName)+'\Help\production.hlp';
    AppVersion := '4.0.8';
    InitAppParam;
    fmSplash:=TfmSplash.Create(NIL);
    dm := Tdm.Create(Application);
    if not dm.Logined then
    begin
      dm.Free;
      eXit;
    end;
    fmSplash.Show;
    ProcessMessages;
    dm.GlobalInitialization;
    ErrorHandler := TErrorHandler.Create(Application);
    dmMain := TdmMain.Create(Application);
    dmAppl := TdmAppl.Create(Application);
    dmInv := TdmInv.Create(Application);
    dmPrint := TdmPrint.Create(Application);
    dmPrint2 := TdmPrint2.Create(Application);
    dmFR := TdmFR.Create(Application);
    dmScale := TdmScale.Create(Application);
    dmData := TdmData.Create(Application);
//    EventAlertsData := TEventAlertsData.Create(Application);
    fmDotMatrixReport := TfmDotMatrixReport.Create(Application);
    ProcessMessages;
    fmLog := TfmLog.Create(Application);
    CreateForm(TfmMain, fmMain);
    if ParamCount > 0 then
    //if UpperCase(ParamStr(1)) = 'DEBUG' then
    //CreateForm(TControlDialog, ControlDialog);
    ProcessMessages;
    fmSplash.Free;
    fmSplash := nil;
    ProcessMessages;
    Hook := TApplicationHook.Create;
    Run;
    Hook.Free;
  end;
end.



