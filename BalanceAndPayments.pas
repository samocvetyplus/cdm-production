unit BalanceAndPayments;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxClasses,
  dxBar, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxContainer, cxCheckBox,
  cxGroupBox, cxSplitter, dxStatusBar, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, FIBDataSet,
  FIBDatabase, pFIBDatabase, pFIBDataSet, cxCheckListBox, cxDBCheckListBox,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, StdCtrls, Menus, ActnList,
  cxButtons, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxPScxPageControlProducer, dxPSCore, dxPScxCommon,
  dxPScxGrid6Lnk;

type
  TfmPaymentsAndBalance = class(TForm)
    GridBalanceView: TcxGridDBTableView;
    GridBalanceLevel1: TcxGridLevel;
    GridBalance: TcxGrid;
    StatusBar: TdxStatusBar;
    Splitter: TcxSplitter;
    GroupBox: TcxGroupBox;
    DataSetCompany: TpFIBDataSet;
    DataSetPayments: TpFIBDataSet;
    trPayments: TpFIBTransaction;
    dsCompany: TDataSource;
    dsPayments: TDataSource;
    DataSetCompanyID: TFIBIntegerField;
    DataSetCompanyNAME: TFIBStringField;
    PeriodBeginDateEdit: TcxDateEdit;
    PeriodEndDateEdit: TcxDateEdit;
    LabelPeriodStart: TLabel;
    LabelPeriodEnd: TLabel;
    ButtonCalcPayments: TcxButton;
    ActionList: TActionList;
    acRecalcPayments: TAction;
    acPrint: TAction;
    ButtonPrint: TcxButton;
    Printer: TdxComponentPrinter;
    GridBalancePrinterLink: TdxGridReportLink;
    GridBalanceViewContragent: TcxGridDBColumn;
    GridBalanceViewIncome: TcxGridDBColumn;
    GridBalanceViewSells: TcxGridDBColumn;
    GridBalanceViewRefunds: TcxGridDBColumn;
    GridBalanceViewPayments: TcxGridDBColumn;
    GridBalanceViewOutcome: TcxGridDBColumn;
    CheckListCompany: TcxCheckListBox;
    GridBalanceViewNumber: TcxGridDBColumn;
    DataSetPaymentsAll: TpFIBDataSet;
    procedure FormCreate(Sender: TObject);
    procedure acRecalcPaymentsExecute(Sender: TObject);
    procedure DataSetPaymentsBeforeOpen(DataSet: TDataSet);
    procedure DataSetPaymentsAllBeforeOpen(DataSet: TDataSet);
    procedure acPrintExecute(Sender: TObject);
  private
    { Private declarations }
    CheckedCount: Smallint;
    StringOfCompID: AnsiString;
    function ConditionsComplete: Integer;
  public
    class procedure Execute;
  end;

var
  fmPaymentsAndBalance: TfmPaymentsAndBalance;

implementation

uses DictData, uDialogs, DateUtils, uUtils, dbUtil, StrUtils;

{$R *.dfm}

procedure TfmPaymentsAndBalance.acPrintExecute(Sender: TObject);
var
  Link: TdxgridReportLink;
  i: integer;
begin
  Link := TdxGridReportLink(Printer.LinkByName('GridBalancePrinterLink'));
  Link.ReportTitle.Text := '������������� � ' + DateToStr(PeriodBeginDateEdit.Date) + ' �� ' + DateToStr(PeriodEndDateEdit.Date) + #13;
  for i := 0 to CheckListCompany.Items.Count - 1  do
    if CheckListCompany.Items[i].Checked then
        Link.ReportTitle.Text := Link.ReportTitle.Text + CheckListCompany.Items[i].Text + '  ';
  Link.ReportDocument.Caption := Link.ReportTitle.Text;
  Link.ReportDocument.CreationDate := Today;
  Link.Preview;
end;

procedure TfmPaymentsAndBalance.acRecalcPaymentsExecute(Sender: TObject);
var
  Bookmark: Pointer;
  DataSet: TpFIBDataSet;
  TempStr : AnsiString;
begin
  CheckedCount := ConditionsComplete;
  if  CheckedCount < 1 then SysUtils.Abort;
  BookMark := DataSetCompany.GetBookMark;
  with DataSetCompany do
    begin
      DisableControls;
      First;
      if CheckedCount > 1 then
        DataSet := DataSetPaymentsAll
      else
        if CheckedCount = 1 then
          DataSet := DataSetPayments;
      EnableControls;
    end;
  DataSetCompany.GotoBookmark(Bookmark);
  dsPayments.DataSet := DataSet;
  Screen.Cursor := crSQLWait;
  if DataSet.Active then DataSet.Close;

  if CheckedCount > 1 then
  begin
    DataSetPaymentsAll.SelectSQL.Strings[11] := 'self$company$id in (Companys)'
  end;

  DataSet.Open;
  Screen.Cursor := crDefault;
end;

function TfmPaymentsAndBalance.ConditionsComplete;
var
  i, CheckedItems: smallint;
begin
  CheckedItems := 0;
  StringOfCompID := '';
  for i := 0 to CheckListCompany.Items.Count - 1  do
    if CheckListCompany.Items[i].Checked then
      begin
        inc(CheckedItems);
        StringOfCompID := StringOfCompID + IntToStr(CheckListCompany.Items[i].tag) + ', '
      end;
  Delete(StringOfCompID, length(StringOfCompID)-1, 2);
  if CheckedItems = 0 then
    begin
      MessageDlg('�������� �����������!', mtError, [mbOk], 0);
      Result := CheckedItems;
      SysUtils.Abort;
    end;
  if PeriodBeginDateEdit.Date > PeriodEndDateEdit.Date then
    MessageDlg('������ ������� �� ����� ���� ����� �����!', mtError, [mbOk], 0);
  Result := CheckedItems;
end;

procedure TfmPaymentsAndBalance.DataSetPaymentsAllBeforeOpen(DataSet: TDataSet);
var TempStr: AnsiString;
begin
  TempStr := DataSetPaymentsAll.SelectSQL.Text;
  TempStr := AnsiReplaceStr(TempStr, 'Companys', StringOfCompID);
  DataSetPaymentsAll.SelectSQL.Text := TempStr;
  with DataSetPaymentsAll.Params do
    begin
      ByName['PeriodBegin'].AsDate := PeriodBeginDateEdit.Date;
      ByName['PeriodEnd'].AsDate := PeriodEndDateEdit.Date;
    end;
end;

procedure TfmPaymentsAndBalance.DataSetPaymentsBeforeOpen(DataSet: TDataSet);
begin
  with DataSetPayments.Params do
    begin
      ByName['SelfCompID'].AsInteger := StrToInt(StringOfCompID);
      ByName['PeriodBegin'].AsDate := PeriodBeginDateEdit.Date;
      ByName['PeriodEnd'].AsDate := PeriodEndDateEdit.Date;
    end;
end;

class procedure TfmPaymentsAndBalance.Execute;
begin
  fmPaymentsAndBalance := nil;
  try
    fmPaymentsAndBalance := TfmPaymentsAndBalance.Create(Application);
    fmPaymentsAndBalance.ShowModal;
  finally
    FreeAndNil(fmPaymentsAndBalance);
  end;
end;

procedure TfmPaymentsAndBalance.FormCreate(Sender: TObject);
begin
  StringOfCompID := 'Companys';
  DataSetCompany.Open;
  DataSetCompany.First;
  while not DataSetCompany.Eof do
    begin
      CheckListCompany.Items[DataSetCompany.RecNo - 1].Text := DataSetCompanyNAME.AsString;
      CheckListCompany.Items[DataSetCompany.RecNo - 1].Tag := DataSetCompanyID.AsInteger;
      DataSetCompany.Next;
    end;
  PeriodBeginDateEdit.Date := StartOfTheMonth(Today);
  PeriodEndDateEdit.Date := EndOfTheMonth(Today);
end;

end.
