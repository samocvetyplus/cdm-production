inherited fmeVType: TfmeVType
  Left = 255
  Top = 203
  Caption = #1058#1080#1087' '#1074#1077#1076#1086#1084#1086#1089#1090#1080
  ClientHeight = 154
  ClientWidth = 241
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 241
    Height = 154
    BevelInner = bvNone
    BevelOuter = bvNone
    object lbDep: TLabel [0]
      Left = 8
      Top = 80
      Width = 49
      Height = 13
      Caption = #1050#1083#1072#1076#1086#1074#1072#1103
    end
    object Label2: TLabel [1]
      Left = 8
      Top = 102
      Width = 47
      Height = 13
      Caption = #1055#1077#1088#1080#1086#1076' '#1089
    end
    object Label3: TLabel [2]
      Left = 140
      Top = 101
      Width = 12
      Height = 13
      Caption = #1087#1086
    end
    inherited btnOk: TBitBtn
      Left = 68
      Top = 124
      TabOrder = 4
    end
    inherited btnCancel: TBitBtn
      Left = 156
      Top = 124
      TabOrder = 5
    end
    object rgrType: TRadioGroup
      Left = 8
      Top = 8
      Width = 229
      Height = 65
      Caption = #1042#1099#1073#1080#1088#1080#1090#1077' '#1090#1080#1087' '#1089#1083#1080#1095#1080#1090#1077#1083#1100#1085#1086#1081' '#1074#1077#1076#1086#1084#1086#1089#1090#1080
      Items.Strings = (
        #1044#1083#1103' '#1082#1083#1072#1076#1086#1074#1097#1080#1082#1072
        #1044#1083#1103' '#1073#1091#1093#1075#1072#1083#1090#1077#1088#1072)
      TabOrder = 0
    end
    object cmbxDep: TDBComboBoxEh
      Left = 60
      Top = 76
      Width = 121
      Height = 19
      EditButtons = <>
      Flat = True
      TabOrder = 1
      Visible = True
      OnEnter = cmbxDepEnter
      OnExit = cmbxDepExit
      OnUpdateData = cmbxDepUpdateData
    end
    object deBD: TDBDateTimeEditEh
      Left = 60
      Top = 97
      Width = 79
      Height = 19
      EditButton.ShortCut = 16397
      EditButton.Style = ebsGlyphEh
      EditButtons = <>
      Flat = True
      Kind = dtkDateEh
      TabOrder = 2
      Visible = True
    end
    object deED: TDBDateTimeEditEh
      Left = 156
      Top = 96
      Width = 79
      Height = 19
      EditButton.ShortCut = 16397
      EditButton.Style = ebsGlyphEh
      EditButtons = <>
      Flat = True
      Kind = dtkDateEh
      TabOrder = 3
      Visible = True
    end
  end
end
