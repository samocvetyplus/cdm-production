inherited fmCheckStone: TfmCheckStone
  Left = 70
  Top = 165
  Width = 809
  Height = 513
  Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1088#1072#1089#1093#1086#1076#1086#1074#1072#1085#1080#1103' '#1076#1088#1072#1075'. '#1082#1072#1084#1085#1077#1081
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 460
    Width = 801
  end
  inherited tb1: TSpeedBar
    Width = 801
    object spitCreate: TSpeedItem
      Action = acCreate
      BtnCaption = #1057#1086#1079#1076#1072#1090#1100
      Caption = #1057#1086#1079#1076#1072#1090#1100
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acCreateExecute
      SectionName = 'Untitled (0)'
    end
  end
  object TBDock1: TTBDock [2]
    Left = 0
    Top = 42
    Width = 801
    Height = 23
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      Caption = 'TBToolbar1'
      TabOrder = 0
      object TBControlItem1: TTBControlItem
        Control = Label1
      end
      object TBControlItem2: TTBControlItem
        Control = cmbxMat
      end
      object Label1: TLabel
        Left = 0
        Top = 3
        Width = 53
        Height = 13
        Caption = #1052#1072#1090#1077#1088#1080#1072#1083' '
      end
      object cmbxMat: TDBComboBoxEh
        Left = 53
        Top = 0
        Width = 121
        Height = 19
        DropDownBox.Rows = 20
        EditButtons = <>
        Flat = True
        TabOrder = 0
        Text = 'cmbxMat'
        Visible = True
        OnChange = cmbxMatChange
      end
    end
  end
  object gridCheckStone: TDBGridEh [3]
    Left = 0
    Top = 65
    Width = 801
    Height = 395
    Align = alClient
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmMain.dsrCheckStone
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = gridCheckStoneGetCellParams
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SEMISNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 128
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'UQ'
        Footers = <>
        Title.Caption = #1045#1076'. '#1080#1079#1084'.|'#1082#1086#1083'-'#1074#1072
        Title.EndEllipsis = True
        Width = 39
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'UW'
        Footers = <>
        Title.Caption = #1045#1076'. '#1080#1079#1084'.|'#1074#1077#1089#1072
        Title.EndEllipsis = True
        Width = 30
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REST_INQ_1'
        Footer.FieldName = 'REST_INQ_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1042#1093#1086#1076#1103#1097#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 39
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REST_INW_1'
        Footer.FieldName = 'REST_INW_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1042#1093#1086#1076#1103#1097#1080#1077'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 36
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INQ_1'
        Footer.FieldName = 'INQ_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1055#1086#1089#1090#1091#1087#1083#1077#1085#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 42
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INW_1'
        Footer.FieldName = 'INW_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1055#1086#1089#1090#1091#1087#1083#1077#1085#1080#1077'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 38
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'PRODQ_1'
        Footer.FieldName = 'PRODQ_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1054#1090#1075#1088#1091#1079#1082#1072' '#1075#1086#1090'. '#1087#1088#1086#1076#1091#1082#1094#1080#1080'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 42
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'PRODW_1'
        Footer.FieldName = 'PRODW_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1054#1090#1075#1088#1091#1079#1082#1072' '#1075#1086#1090'. '#1087#1088#1086#1076#1091#1082#1094#1080#1080'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 44
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RETPRODQ_1'
        Footer.FieldName = 'RETPRODQ_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1042#1086#1079#1074#1088#1072#1090' '#1075#1086#1090'. '#1087#1088#1086#1076#1091#1082#1094#1080#1080'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 42
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RETPRODW_1'
        Footer.FieldName = 'RETPRODW_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1042#1086#1079#1074#1088#1072#1090' '#1075#1086#1090'. '#1087#1088#1086#1076#1091#1082#1094#1080#1080'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 40
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SCHARGEQ_1'
        Footer.FieldName = 'SCHARGEQ_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1057#1087#1080#1089#1072#1085#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 38
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SCHARGEW_1'
        Footer.FieldName = 'SCHARGEW_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1057#1087#1080#1089#1072#1085#1080#1077'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 39
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FQ_1'
        Footer.FieldName = 'FQ_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1082#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 38
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FW_1'
        Footer.FieldName = 'FW_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1054#1089#1090#1072#1090#1086#1082' '#1085#1072' '#1082#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 40
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RQ_1'
        Footer.FieldName = 'RQ_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 41
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RW_1'
        Footer.FieldName = 'RW_1'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 1|'#1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 36
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REST_INQ_2'
        Footer.FieldName = 'REST_INQ_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1042#1093#1086#1076#1103#1097#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 41
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REST_INW_2'
        Footer.FieldName = 'REST_INW_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1042#1093#1086#1076#1103#1097#1080#1077'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 39
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INQ_2'
        Footer.FieldName = 'INQ_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1055#1086#1089#1090#1091#1087#1083#1077#1085#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 37
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INW_2'
        Footer.FieldName = 'INW_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1055#1086#1089#1090#1091#1087#1083#1077#1085#1080#1077'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 40
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WORDERQ_2'
        Footer.FieldName = 'WORDERQ_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1053#1072#1088#1103#1076'|'#1082#1086#1083'-'#1074#1086' '
        Title.EndEllipsis = True
        Width = 44
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WORDERW_2'
        Footer.FieldName = 'WORDERW_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1053#1072#1088#1103#1076'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 41
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SCHARGEQ_2'
        Footer.FieldName = 'SCHARGEQ_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1057#1087#1080#1089#1072#1085#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 40
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SCHARGEW_2'
        Footer.FieldName = 'SCHARGEW_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1057#1087#1080#1089#1072#1085#1080#1077'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 43
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FQ_2'
        Footer.FieldName = 'FQ_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1060#1072#1082#1090'. '#1085#1072#1083#1080#1095#1080#1077' '#1087#1086' '#1087#1088#1086#1090#1086#1082#1086#1083#1091'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 48
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FW_2'
        Footer.FieldName = 'FW_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1060#1072#1082#1090'. '#1085#1072#1083#1080#1095#1080#1077' '#1087#1086' '#1087#1088#1086#1090#1086#1082#1086#1083#1091'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 38
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RQ_2'
        Footer.FieldName = 'RQ_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 43
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RW_2'
        Footer.FieldName = 'RW_2'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1040#1083#1075#1086#1088#1080#1090#1084' 2|'#1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 37
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RQ'
        Footer.FieldName = 'RQ'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1056#1072#1079#1085#1080#1094#1072'|'#1082#1086#1083'-'#1074#1086
        Width = 42
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RW'
        Footer.FieldName = 'RW'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1056#1072#1079#1085#1080#1094#1072'|'#1074#1077#1089
        Width = 42
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ilButtons: TImageList
    Left = 70
    Top = 156
  end
  inherited fmstr: TFormStorage
    Left = 112
    Top = 156
  end
  inherited ActionList2: TActionList
    Images = ilButtons
    Left = 68
    Top = 208
    object acCreate: TAction
      Caption = #1057#1086#1079#1076#1072#1090#1100
      OnExecute = acCreateExecute
    end
  end
end
