unit eSemisProp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DbEditor, StdCtrls, Buttons, ExtCtrls, DBCtrls, Mask, DBCtrlsEh,
  DBLookupEh, DBGridEh;

type
  TfmeSemisProp = class(TfmDbEditor)
    lbEdgSh: TLabel;
    lbEdgT: TLabel;
    lbChromaticity: TLabel;
    lbCleannes: TLabel;
    lbGr: TLabel;
    lbColor: TLabel;
    lbSz: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    edSz: TDBEditEh;
    edO1: TDBEditEh;
    lcbxEdgShape: TDBLookupComboboxEh;
    lcbxEdgT: TDBLookupComboboxEh;
    lcbxChromaticity: TDBLookupComboboxEh;
    lcbxCleannes: TDBLookupComboboxEh;
    lcbxIGr: TDBLookupComboboxEh;
    lcbxColor: TDBLookupComboboxEh;
    edPrice: TDBNumberEditEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmeSemisProp: TfmeSemisProp;

implementation

uses DictData, dbUtil;

{$R *.dfm}

procedure TfmeSemisProp.FormCreate(Sender: TObject);
begin
  with dm do
    OpenDataSets([taEdgShape, taEdgT, taChromaticity, taColor, taCleannes, taIGr]);
  inherited;
end;

procedure TfmeSemisProp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  with dm do
    CloseDataSets([taEdgShape, taEdgT, taChromaticity, taColor, taCleannes, taIGr]);
  inherited;
end;

end.
