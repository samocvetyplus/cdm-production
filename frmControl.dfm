object ControlDialog: TControlDialog
  Left = 216
  Top = 163
  BorderIcons = [biMaximize]
  BorderStyle = bsSingle
  BorderWidth = 8
  Caption = #1050#1086#1085#1090#1088#1086#1083#1100
  ClientHeight = 537
  ClientWidth = 764
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  Visible = True
  WindowState = wsMaximized
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 764
    Height = 537
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    object TabSheet: TTabSheet
      Caption = 'Log'
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet1: TTabSheet
      Caption = #1041#1088#1072#1082
      ImageIndex = 1
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 756
        Height = 509
        Align = alClient
        TabOrder = 0
        LookAndFeel.Kind = lfOffice11
        object GridView: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = DataSource
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.ScrollBars = ssVertical
          OptionsView.GroupRowStyle = grsOffice11
          object GridViewCLASS: TcxGridDBColumn
            Caption = #1043#1088#1091#1087#1087#1072
            DataBinding.FieldName = 'CLASS$TITLE'
            Visible = False
            GroupIndex = 0
          end
          object GridViewSTORAGETITLE: TcxGridDBColumn
            Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
            DataBinding.FieldName = 'STORAGE$TITLE'
            Visible = False
            GroupIndex = 1
            Width = 114
          end
          object GridViewELEMENT: TcxGridDBColumn
            Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
            DataBinding.FieldName = 'ELEMENT'
            Width = 145
          end
          object GridViewOPERATION: TcxGridDBColumn
            Caption = #1054#1087#1077#1088#1072#1094#1080#1103
            DataBinding.FieldName = 'OPERATION'
            Width = 147
          end
          object GridViewFROMOPERATION: TcxGridDBColumn
            Caption = #1041#1088#1072#1082
            DataBinding.FieldName = 'FROM$OPERATION'
            Width = 161
          end
          object GridViewWEIGHT: TcxGridDBColumn
            Caption = #1042#1077#1089
            DataBinding.FieldName = 'WEIGHT'
          end
          object GridViewQUANTITY: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'QUANTITY'
          end
          object GridViewDOCUMENT: TcxGridDBColumn
            Caption = #1044#1086#1082'.'
            DataBinding.FieldName = 'DOCUMENT'
          end
          object Grid: TcxGridDBColumn
            Caption = #1044#1072#1090#1072
            DataBinding.FieldName = 'DOCUMENT$DATE'
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = GridView
        end
      end
    end
  end
  object DataSource: TDataSource
    DataSet = DataSet
    Left = 72
    Top = 232
  end
  object DataSet: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from SPOILAGE$CONTROL'
      'order by'
      ' CLASS, '
      ' STORAGE$TITLE,'
      ' DOCUMENT,'
      ' DOCUMENT$DATE,'
      ' ELEMENT,'
      ' OPERATION,'
      ' FROM$OPERATION')
    OnCalcFields = DataSetCalcFields
    Transaction = dm.tr
    Database = dm.db
    Left = 72
    Top = 264
    object DataSetCLASS: TFIBIntegerField
      FieldName = 'CLASS'
    end
    object DataSetDOCUMENT: TFIBIntegerField
      FieldName = 'DOCUMENT'
    end
    object DataSetDOCUMENTDATE: TFIBDateField
      FieldName = 'DOCUMENT$DATE'
    end
    object DataSetID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetSTORAGETITLE: TFIBStringField
      FieldName = 'STORAGE$TITLE'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetELEMENT: TFIBStringField
      FieldName = 'ELEMENT'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetFROMOPERATION: TFIBStringField
      FieldName = 'FROM$OPERATION'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetWEIGHT: TFIBFloatField
      FieldName = 'WEIGHT'
      DisplayFormat = '########0.000'
    end
    object DataSetQUANTITY: TFIBIntegerField
      FieldName = 'QUANTITY'
    end
    object DataSetCLASSTITLE: TFIBStringField
      FieldKind = fkCalculated
      FieldName = 'CLASS$TITLE'
      Size = 64
      EmptyStrToNull = True
      Calculated = True
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    OnMessage = ApplicationEvents1Message
    Left = 72
    Top = 296
  end
end
