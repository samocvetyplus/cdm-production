unit dComp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DictAncestor, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls, Grids, DBGrids,
  RXDBCtrl, ComCtrls, DBCtrls, RxToolEdit, StdCtrls,
  Mask, ActnList, DBGridEh, Buttons, DBCtrlsEh, M207Ctrls, TB2Dock,
  TB2Toolbar, TB2Item, Menus, DB, DBGridEhGrouping, GridsEh;

type
  TfmComp = class(TfmDictAncestor)
    Splitter: TSplitter;
    plMain: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label23: TLabel;
    Label25: TLabel;
    pgctl: TPageControl;
    tbshProps: TTabSheet;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label30: TLabel;
    edCountry: TDBEditEh;
    edPostIndex: TDBEditEh;
    edAddress: TDBEditEh;
    edPhone: TDBEditEh;
    edFax: TDBEditEh;
    edEMail: TDBEditEh;
    cbSeller: TDBCheckBoxEh;
    cbBuyer: TDBCheckBoxEh;
    chbxLawFace1: TDBCheckBoxEh;
    chbxPhysFace: TDBCheckBoxEh;
    Label19: TLabel;
    edName: TDBEditEh;
    Label7: TLabel;
    edINN: TDBEditEh;
    Label18: TLabel;
    Label21: TLabel;
    edBoss: TDBEditEh;
    edBossPhone: TDBEditEh;
    edBuh: TDBEditEh;
    edBuhPhone: TDBEditEh;
    Label11: TLabel;
    edOKPO: TDBEditEh;
    Label12: TLabel;
    edOKONH: TDBEditEh;
    edBIK: TDBEditEh;
    edBank: TDBEditEh;
    edBill: TDBEditEh;
    edKBank: TDBEditEh;
    edKBill: TDBEditEh;
    edPaspSer: TDBEditEh;
    edPaspNum: TDBEditEh;
    edDistrPlace: TDBEditEh;
    deDistrDate: TDBDateTimeEditEh;
    Label5: TLabel;
    Label17: TLabel;
    edCertN: TDBEditEh;
    Label20: TLabel;
    Label22: TLabel;
    dtCertDate: TDBDateTimeEditEh;
    Label24: TLabel;
    edKPP: TDBEditEh;
    Label31: TLabel;
    Label32: TLabel;
    edNo_GovReg: TDBEditEh;
    Label33: TLabel;
    edD_GovReg: TDBDateTimeEditEh;
    Label35: TLabel;
    edNo_FisLPlace: TDBEditEh;
    Label36: TLabel;
    edD_FisLPlace: TDBDateTimeEditEh;
    Label37: TLabel;
    edOFIO: TDBEditEh;
    edOFIOPhone: TDBEditEh;
    TabSheet1: TTabSheet;
    mmComment: TDBMemo;
    TabSheet2: TTabSheet;
    gridCompOwner: TDBGridEh;
    TBDock1: TTBDock;
    tlbrCompOwner: TTBToolbar;
    acAddCompOwner: TAction;
    acDelCompOwner: TAction;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    edFactAddress: TDBEditEh;
    Label38: TLabel;
    Label39: TLabel;
    edPostAddress: TDBEditEh;
    SpeedItem4: TSpeedItem;
    ppPrint: TTBPopupMenu;
    TBItem3: TTBItem;
    acPrintCompList: TAction;
    TBDock2: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label40: TLabel;
    edFilterName: TDBEditEh;
    TBControlItem2: TTBControlItem;
    edDogNo: TDBEditEh;
    dtedDogBd: TDBDateTimeEditEh;
    dtedDogED: TDBDateTimeEditEh;
    dtedREUED: TDBDateTimeEditEh;
    Label26: TLabel;
    Label27: TLabel;
    Label29: TLabel;
    Label28: TLabel;
    ppComp: TTBPopupMenu;
    acDoc: TAction;
    TBItem4: TTBItem;
    TabSheet3: TTabSheet;
    mmDocBad_Memo: TDBMemo;
    Label34: TLabel;
    acCompId: TAction;
    TBItem5: TTBItem;
    dlgColor: TColorDialog;
    ReadOnlyBtn: TButton;
    TBItem6: TTBItem;
    acPrintClientCard: TAction;
    cbProducer: TDBCheckBoxEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure acAddCompOwnerExecute(Sender: TObject);
    procedure acAddCompOwnerUpdate(Sender: TObject);
    procedure acDelCompOwnerExecute(Sender: TObject);
    procedure acDelCompOwnerUpdate(Sender: TObject);
    procedure acPrintCompListExecute(Sender: TObject);
    procedure acPrintCompListUpdate(Sender: TObject);
    procedure gridDictGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure edFilterNameChange(Sender: TObject);
    procedure acDocExecute(Sender: TObject);
    procedure acCompIdExecute(Sender: TObject);
    procedure acCompIdUpdate(Sender: TObject);
    procedure gridDictEditButtonClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SetReadOnly(ReadOnly: boolean);
    procedure ReadOnlyBtnClick(Sender: TObject);
    procedure acPrintClientCardExecute(Sender: TObject);
  end;

var
  ReadOnly : boolean;

implementation

{$R *.DFM}

uses DictData, dbUtil, PrintData, Variants, fmUtils, CompDoc, MsgDialog;


procedure TfmComp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dm.LoadArtSL(PROD_DICT);
  dm.LoadArtSL(SELLER_DICT);
  dm.LoadArtSL(COMP_DICT);
  CloseDataSet(dm.taCompSelf);
end;

procedure TfmComp.FormCreate(Sender: TObject);
begin
  inherited;
  OpenDataSet(dm.taCompSelf);
  ReadOnly := true;
  SetReadOnly(ReadOnly);
end;

procedure TfmComp.SetReadOnly(ReadOnly: Boolean);
begin
  cbBuyer.ReadOnly := ReadOnly;
  cbSeller.ReadOnly := ReadOnly;
  cbProducer.ReadOnly := ReadOnly;
  chbxLawFace1.ReadOnly := ReadOnly;
  chbxPhysFace.ReadOnly := ReadOnly;
  edAddress.ReadOnly := ReadOnly;
  edBoss.ReadOnly := ReadOnly;
  edBossPhone.ReadOnly := ReadOnly;
  edBuh.ReadOnly := ReadOnly;
  edBuhPhone.ReadOnly := ReadOnly;
  edCountry.ReadOnly := ReadOnly;
  edEmail.ReadOnly := ReadOnly;
  edFactAddress.ReadOnly := ReadOnly;
  edFax.ReadOnly := ReadOnly;
  edINN.ReadOnly := ReadOnly;
  edKPP.ReadOnly := ReadOnly;
  edName.ReadOnly := ReadOnly;
  edOFIO.ReadOnly := ReadOnly;
  edOFIOPhone.ReadOnly := ReadOnly;
  edOKONH.ReadOnly := ReadOnly;
  edOKPO.ReadOnly := ReadOnly;
  edPhone.ReadOnly := ReadOnly;
  edPostAddress.ReadOnly := ReadOnly;
  edPostIndex.ReadOnly := ReadOnly;
  edKBank.ReadOnly := ReadOnly;
  ReadOnly := ReadOnly;
  GridDict.ReadOnly := ReadOnly;
  if ReadOnly then
    ReadOnlyBtn.Caption := '�������'
  else
    ReadOnlyBtn.Caption := '�������';
end;

procedure TfmComp.FormShow(Sender: TObject);
begin
  inherited;
  SetReadOnly(ReadOnly);
end;

procedure TfmComp.acAddCompOwnerExecute(Sender: TObject);
begin
  dm.taCompOwner.Append;
end;

procedure TfmComp.acAddCompOwnerUpdate(Sender: TObject);
begin
  acAddCompOwner.Enabled := dm.taCompOwner.Active;
end;

procedure TfmComp.acDelCompOwnerExecute(Sender: TObject);
begin
  dm.taCompOwner.Delete;
end;

procedure TfmComp.acDelCompOwnerUpdate(Sender: TObject);
begin
  acDelCompOwner.Enabled := dm.taCompOwner.Active and (not dm.taCompOwner.IsEmpty);
end;

procedure TfmComp.acPrintClientCardExecute(Sender: TObject);
begin
  with dmPrint do
  begin
    FDocID := 5;
    frReport.Dataset := dm.frComp;
    frReport.OnGetValue := nil;
    taDoc.Open;
    frReport.LoadFromDB(taDoc, FDocID);
    dm.DocPreview:=dm.GetDocPreview;
    if dm.DocPreview=pvDesign
      then
        frReport.DesignReport
      else
        frReport.ShowReport;
    taDoc.Close;
  end;
end;

procedure TfmComp.acPrintCompListExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkCompManager, VarArrayOf([VarArrayOf([dm.User.SelfCompId, dm.User.SelfCompId])]));
end;

procedure TfmComp.acPrintCompListUpdate(Sender: TObject);
begin
  acPrintCompList.Enabled := dm.taComp.Active;
end;

procedure TfmComp.gridDictGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if Column.FieldName = 'CODE' then
  begin
    if (dm.taCompDOCBAD_P.AsInteger <> 0) then Background := clAqua;
  end
  else
  if (Column.FieldName = 'COLOR')and(not dm.taCompCOLOR.IsNull) then
  begin
    Background := dm.taCompCOLOR.AsInteger;
  end;
  inherited;
end;





procedure TfmComp.ReadOnlyBtnClick(Sender: TObject);
begin

  inherited;
  
  ReadOnly := not ReadOnly;

  SetReadOnly(ReadOnly);
  
end;

procedure TfmComp.edFilterNameChange(Sender: TObject);
begin
  if(edFilterName.Text='')then dm.taComp.Filtered := False
  else begin
    dm.taComp.DisableScrollEvents;
    dm.taComp.DisableControls;
    try
      dm.taComp.Filtered := False;
      dm.taComp.Filter := 'upper(NAME) like upper('+ #39 + '%' + trim(edFilterName.Text) + '%' + #39 + ')'; //) '#39+edFilterName.Text+'*'+#39;
      dm.taComp.Filtered := True;
    finally
      dm.taComp.EnableScrollEvents;
      dm.taComp.EnableControls;
    end;
  end;
end;

procedure TfmComp.acDocExecute(Sender: TObject);
begin
  //
  ShowAndFreeForm(TfmCompDoc, TForm(fmCompDoc));
end;

procedure TfmComp.acCompIdExecute(Sender: TObject);
begin
  MessageDialog(dm.taCompCOMPID.AsString, mtInformation, [mbOk], 0)
end;

procedure TfmComp.acCompIdUpdate(Sender: TObject);
begin
  acCompId.Visible := AppDebug;
  if acCompId.Visible then
    acCompId.Enabled := dm.taComp.Active and (not dm.taComp.IsEmpty);
end;

procedure TfmComp.gridDictEditButtonClick(Sender: TObject);
begin
  inherited;
  if gridDict.SelectedField.FieldName='COLOR' then
    if dlgColor.Execute then begin
      if not (dm.taComp.State in [dsEdit, dsInsert]) then dm.taComp.Edit;
      dm.taCompCOLOR.AsInteger:=dlgColor.Color; 
    end;
end;

end.
