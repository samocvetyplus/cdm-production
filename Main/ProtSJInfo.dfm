inherited fmProtSJInfo: TfmProtSJInfo
  Left = 98
  Top = 173
  Width = 628
  Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1072
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 620
  end
  inherited tb1: TSpeedBar
    Width = 620
  end
  object gridHistSemis: TDBGridEh [2]
    Left = 0
    Top = 42
    Width = 620
    Height = 200
    Align = alClient
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmMain.dsrProtSJInfo
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WHNAME'
        Footers = <>
        Width = 80
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WORDERNO'
        Footers = <>
        Title.Caption = #8470#1085#1072#1088#1103#1076#1072
        Width = 53
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INVNO'
        Footers = <>
        Title.Caption = #8470' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
        Width = 73
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ITDATE'
        Footers = <>
        Width = 74
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SEMISNAME'
        Footers = <>
        Width = 101
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Width = 39
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footers = <>
        Width = 57
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ITTYPE'
        Footers = <>
        Width = 76
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
end
