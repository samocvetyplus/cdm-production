//������ ��� ������ ������������������ ����� �� ������ ��������

unit dmReportInventory;

interface

uses

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FR_Desgn, FR_Class, StdCtrls, Buttons, Mask, rxToolEdit, Grids,
  DBGrids, DB, FIBDataSet, pFIBDataSet, FIBDatabase, pFIBDatabase,
  DBGridEhGrouping, GridsEh, DBGridEh, FR_DSet, FR_DBSet;

type

  TReportInventory = class(TDataModule)
    taMat: TpFIBDataSet;
    taOper: TpFIBDataSet;
    taSubOper: TpFIBDataSet;
    dsrMat: TDataSource;
    dsrOper: TDataSource;
    dsrSubOper: TDataSource;
    frMat: TfrDBDataSet;
    frOper: TfrDBDataSet;
    frSubOper: TfrDBDataSet;
    Report: TfrReport;
    taOperOPERID: TFIBStringField;
    taOperOPERATION: TFIBStringField;
    taMatMATID: TFIBStringField;
    taMatNAME: TFIBStringField;
    taHeader: TpFIBDataSet;
    frHeader: TfrDBDataSet;
    dsrHeader: TDataSource;
    frSubreport: TfrDBDataSet;
    taSubReport: TpFIBDataSet;
    dsrSubreport: TDataSource;
    taOperS: TFIBFloatField;
    taOperE: TFIBFloatField;
    taOperN: TFIBFloatField;
    taOperA: TFIBSmallIntField;
    taTotalLosses: TpFIBDataSet;
    dsrTotalLosses: TDataSource;
    frTotalLosses: TfrDBDataSet;
    taTotalLossesTOTALSJEM: TFIBFloatField;
    taTotalLossesTOTALLOSSESNORM: TFIBFloatField;
    taTotalLossesSJEM: TFIBBCDField;
    taTotalLossesLOSSESNORM: TFIBBCDField;
    taTotalLossesRESULTLOSSES: TFIBFloatField;
    taTotalLossesSJEMNORM: TFIBBCDField;
    taSubOperNAME0: TFIBStringField;
    taSubOperNAME: TFIBStringField;
    taSubOperWEIGHT: TFIBBCDField;
    taSubOperQUANTITY: TFIBIntegerField;
    taTotalLossesPOLISHSJEMNORM: TFIBBCDField;
    taTotalLossesPOLISHSJEMNORMMONTH: TFIBBCDField;
    procedure taSubOperBeforeOpen(DataSet: TDataSet);
    procedure taOperBeforeOpen(DataSet: TDataSet);
    procedure taMatBeforeOpen(DataSet: TDataSet);
    procedure taHeaderBeforeOpen(DataSet: TDataSet);
    procedure ReportGetValue(const ParName: string; var ParValue: Variant);
    procedure taSubReportBeforeOpen(DataSet: TDataSet);
    procedure taTotalLossesBeforeOpen(DataSet: TDataSet);

  public
    BD, ED, WeightOut: String;
    APlaceOfStorageID: Integer;
    procedure Prepare;
  end;

var
  ReportInventory: TReportInventory;

implementation

uses DictData, frmAffiangePassport, PrintData, PrintPersonalInfo, DateUtils;

{$R *.dfm}


{ TReportInventory }

procedure TReportInventory.ReportGetValue(const ParName: string;
  var ParValue: Variant);
var
  SQL: AnsiString;
  BeginDate : TDate;
begin

  SQL := 'select first 1 OnlyDate(Period$End + 1) from Affinage$Passport where ID > 0 and Designation > 0 and State = 2 order by ID desc';

  BeginDate := StartOfTheMonth(StrToDateTime(ED));
  
  if ParName = 'BD' then ParValue := DateToStr(BeginDate);
  if ParName = 'ED' then ParValue := ED;
  if ParName = 'PD' then ParValue := String(dm.db.QueryValue(SQL, 0, []));
  if ParName = 'WeightOut' then ParValue := WeightOut;
    
end;

procedure TReportInventory.taHeaderBeforeOpen(DataSet: TDataSet);
begin
  taHeader.ParamByName('PlaceOfStorageID').AsInteger := APlaceOfStorageID;
end;

procedure TReportInventory.taMatBeforeOpen(DataSet: TDataSet);
begin
with taMat do
  begin
    ParamByName('BD').AsString := BD;
    ParamByName('ED').AsString := ED;
    ParamByName('PlaceOfStorageID').AsInteger := APlaceOfStorageID;
  end;
end;

procedure TReportInventory.taOperBeforeOpen(DataSet: TDataSet);
begin
with taOper do
  begin
    ParamByName('BD').AsString := BD;
    ParamByName('ED').AsString := ED;
    ParamByName('PlaceOfStorageID').AsInteger := APlaceOfStorageID;
  end;
end;

procedure TReportInventory.taSubOperBeforeOpen(DataSet: TDataSet);
begin
with taSubOper.Params do
  begin
    ByName['BD'].AsString := BD;
    ByName['ED'].AsString := ED;
    ByName['PlaceOfStorageID'].AsInteger := APlaceOfStorageID;
    ByName['MaterialID'].AsString := taMatMATID.AsString;
  end;
end;

procedure TReportInventory.taSubReportBeforeOpen(DataSet: TDataSet);
begin
with taSubReport do
  begin
    ParamByName('BD').AsString := BD;
    ParamByName('ED').AsString := ED;
    ParamByName('PlaceOfStorageID').AsInteger := APlaceOfStorageID;
  end;
end;

procedure TReportInventory.taTotalLossesBeforeOpen(DataSet: TDataSet);
begin
with taTotalLosses do
  begin
    ParamByName('PlaceOfStorageID').AsInteger := APlaceOfStorageID;
    ParamByName('ED').AsString := ED;
    ParamByName('MatID').AsString := taMatMATID.AsString;
  end;
end;

procedure TReportInventory.Prepare;
begin
  taMat.Open;
  taTotalLosses.Open;
  taHeader.Open;
  taSubReport.Open;
  dmPrint.FDocId := 111;
  dmPrint.taDoc.Open;
  Report.LoadFromDB(dmPrint.taDoc, dmPrint.FDocID);
  if dm.IsAdm then
    Report.DesignReport
  else
    begin
      Report.PrepareReport;
      Report.ShowPreparedReport;
    end;
end;

end.
