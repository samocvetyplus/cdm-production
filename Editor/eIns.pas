unit eIns;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DbEditor, StdCtrls, Buttons, ExtCtrls, Grids, DBGrids, RXDBCtrl,
  db, RxPlacemnt, DBGridEh, TB2Dock, TB2Toolbar, ActnList, ImgList, TB2Item,
  DBGridEhGrouping, GridsEh;

type
  TfmeIns = class(TfmDbEditor)
    gridIns: TDBGridEh;
    TBDock1: TTBDock;
    tlbrFunc: TTBToolbar;
    acEvent: TActionList;
    im16: TImageList;
    acCopy: TAction;
    acPaste: TAction;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure acCopyExecute(Sender: TObject);
    procedure acCopyUpdate(Sender: TObject);
    procedure acPasteExecute(Sender: TObject);
    procedure acPasteUpdate(Sender: TObject);
  private
    FCompOpen : boolean; 
  end;

var
  fmeIns: TfmeIns;

implementation

uses DictData, dbUtil, MainData, MsgDialog;

{$R *.dfm}

procedure TfmeIns.FormClose(Sender: TObject; var Action: TCloseAction);

  procedure HandlePost;
  begin
    DataSet.Cancel;
  end;

begin
  inherited;
  try
    if DataSet.State in [dsInsert, dsEdit] then DataSet.Post;
  except
    on E : Exception do HandlePost;
  end;
  DataSet.Close;
  CloseDataSet(dmMain.taInsBuffer);
  if not FCompOpen then CloseDataSet(dm.taComp); 
end;

procedure TfmeIns.FormCreate(Sender: TObject);
begin
  inherited;
  //tlbrFunc.Skin := dm.TBSkin;
  OpenDataSet(dmMain.taInsBuffer);
  if(EditorMode = emEdit)then DataSet.Cancel;
  FCompOpen := dm.taComp.Active;
  if not FCompOpen then OpenDataSet(dm.taComp); 
end;

procedure TfmeIns.acCopyExecute(Sender: TObject);
var
  bm : Pointer;
//  i : integer;
begin
  with gridIns.DataSource.DataSet do
  begin
    PostDataSet(DataSet);
    ExecSQL('delete from B_Ins where UserId='+IntToStr(dm.User.UserId), dm.quTmp);
    bm := GetBookmark;
    DisableControls;
    try
      First;
      while not Eof do
      begin 
        ExecSQL('insert into B_INS(ID, USERID, INSID, Q, W, COMPID) values('+IntToStr(dm.GetId(80))+','+
          IntToStr(dm.User.UserId)+','#39+FieldByName('INSID').AsString+#39','+
          FieldByName('QUANTITY').AsString+','+StringReplace(FieldByName('WEIGHT').AsString, ',', '.', [])+','+
          FieldByName('COMPID').AsString+')', dm.quTmp);
        Next;
      end;
    finally
      GotoBookmark(bm);
      EnableControls;
    end;
    ReOpenDataSet(dmMain.taInsBuffer);
  end
end;

procedure TfmeIns.acCopyUpdate(Sender: TObject);
begin
  with gridIns.DataSource.DataSet do
    acCopy.Enabled := Active and (not IsEmpty);
end;

procedure TfmeIns.acPasteExecute(Sender: TObject);
begin
  dmMain.taInsBuffer.First;
  while not dmMain.taInsBuffer.Eof do
  begin
    dm.taA2Ins.Insert;
    dm.taA2InsINSID.AsString := dmMain.taInsBufferINSID.AsString;
    dm.taA2InsQUANTITY.AsInteger := dmMain.taInsBufferQ.AsInteger;
    dm.taA2InsWEIGHT.AsFloat := dmMain.taInsBufferW.AsFloat;
    dm.taA2InsCOMPID.AsInteger := dmMain.taInsBufferCOMPID.AsInteger;
    try
      dm.taA2Ins.Post;
    except
      on E:Exception do
        begin
          dm.taA2Ins.Cancel;
          { TODO : ����������� ����� ������� MessageDialogA }
          //MessageDialogA('������ '+E.Message, mtError);
          MessageDialog('������ '+E.Message, mtError, [mbOk], 0);
        end;
    end;
    dmMain.taInsBuffer.Next;
  end;
end;

procedure TfmeIns.acPasteUpdate(Sender: TObject);
begin
  acPaste.Enabled := dmMain.taInsBuffer.Active and (not dmMain.taInsBuffer.IsEmpty);
end;

end.
