object DialogNormLosses: TDialogNormLosses
  Left = 293
  Top = 176
  Width = 338
  Height = 418
  Caption = #1053#1086#1088#1084#1099' '#1087#1086#1090#1077#1088#1100
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel: TBevel
    Left = 8
    Top = 344
    Width = 313
    Height = 2
    Shape = bsTopLine
  end
  object Grid: TcxGrid
    Left = 8
    Top = 16
    Width = 313
    Height = 321
    TabOrder = 0
    LookAndFeel.Kind = lfStandard
    object GridDBTableView: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = DataSource
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.GroupRowStyle = grsOffice11
      object GridDBTableViewMaterial: TcxGridDBColumn
        Caption = #1052#1072#1090#1077#1088#1080#1072#1083
        DataBinding.FieldName = 'Material'
        Options.Moving = False
        Options.Sorting = False
        SortIndex = 0
        SortOrder = soAscending
        Width = 140
      end
      object GridDBTableViewBEGINDATE: TcxGridDBColumn
        Caption = #1044#1072#1090#1072
        DataBinding.FieldName = 'BEGIN$DATE'
        Options.Filtering = False
        Options.IncSearch = False
        Options.Grouping = False
        Options.Moving = False
        Options.Sorting = False
        SortIndex = 1
        SortOrder = soDescending
        Width = 80
      end
      object GridDBTableViewNORM: TcxGridDBColumn
        Caption = #1053#1086#1088#1084#1072
        DataBinding.FieldName = 'NORM'
        Options.Filtering = False
        Options.IncSearch = False
        Options.Grouping = False
        Options.Moving = False
        Options.Sorting = False
      end
    end
    object GridLevel: TcxGridLevel
      GridView = GridDBTableView
    end
  end
  object ButtonClose: TcxButton
    Left = 248
    Top = 352
    Width = 75
    Height = 25
    Cancel = True
    Caption = #1047#1072#1082#1088#1099#1090#1100
    ModalResult = 2
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
  end
  object DataSource: TDataSource
    DataSet = DataSet
    Left = 24
    Top = 96
  end
  object DataSet: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE NORM$LOSSES'
      'SET '
      '    MATERIAL$ID = :MATERIAL$ID,'
      '    BEGIN$DATE = :BEGIN$DATE,'
      '    NORM = :NORM'
      'WHERE'
      '    COMPANY$ID = :OLD_COMPANY$ID'
      '    and MATERIAL$ID = :OLD_MATERIAL$ID'
      '    and BEGIN$DATE = :OLD_BEGIN$DATE'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    NORM$LOSSES'
      'WHERE'
      '        COMPANY$ID = :OLD_COMPANY$ID'
      '    and MATERIAL$ID = :OLD_MATERIAL$ID'
      '    and BEGIN$DATE = :OLD_BEGIN$DATE'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO NORM$LOSSES('
      '    COMPANY$ID,'
      '    MATERIAL$ID,'
      '    BEGIN$DATE,'
      '    NORM'
      ')'
      'VALUES('
      '    :COMPANY$ID,'
      '    :MATERIAL$ID,'
      '    :BEGIN$DATE,'
      '    :NORM'
      ')')
    RefreshSQL.Strings = (
      ''
      '    ')
    SelectSQL.Strings = (
      'select '
      '  company$id,'
      '  MATERIAL$ID,'
      '  BEGIN$DATE,'
      '  NORM       '
      'from norm$losses'
      'where company$id = :company$id'
      'order by material$id, begin$date')
    BeforeOpen = DataSetBeforeOpen
    OnNewRecord = DataSetNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 24
    Top = 128
    oRefreshAfterPost = False
    object DataSetCOMPANYID: TFIBIntegerField
      FieldName = 'COMPANY$ID'
      Required = True
    end
    object DataSetMATERIALID: TFIBIntegerField
      FieldName = 'MATERIAL$ID'
      Required = True
    end
    object DataSetBEGINDATE: TFIBDateField
      FieldName = 'BEGIN$DATE'
      Required = True
    end
    object DataSetNORM: TFIBFloatField
      FieldName = 'NORM'
      Required = True
    end
    object DataSetMaterial2: TStringField
      FieldKind = fkLookup
      FieldName = 'Material'
      LookupDataSet = DataSetMaterial
      LookupKeyFields = 'ID'
      LookupResultField = 'TITLE'
      KeyFields = 'MATERIAL$ID'
      Size = 64
      Lookup = True
    end
  end
  object DataSetMaterial: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select cast(stretrim(id) as integer) id, cast(stretrim(name) as ' +
        'varchar(64)) title'
      'from d_mat where isins = 0')
    Transaction = dm.tr
    Database = dm.db
    Left = 24
    Top = 160
    object DataSetMaterialID2: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetMaterialTITLE: TFIBStringField
      FieldName = 'TITLE'
      Size = 64
      EmptyStrToNull = False
    end
  end
end
