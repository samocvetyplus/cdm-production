unit TechnologyGroupCustomization;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxContainer, cxLabel,
  cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, Menus, StdCtrls, cxButtons,
  dxBar, ImgList, ActnList,  FIBDataSet, pFIBDataSet;

type
  TfmTechnologyGroupCustomization = class(TForm)
    ArticlesView: TcxGridDBTableView;
    ArticlesLevel: TcxGridLevel;
    Articles: TcxGrid;
    Operations: TcxGrid;
    OperationsView: TcxGridDBTableView;
    OperationsLevel: TcxGridLevel;
    GroupOperations: TcxGrid;
    GroupOperationsView: TcxGridDBTableView;
    GroupOperationsLevel: TcxGridLevel;
    GroupArticles: TcxGrid;
    GroupArticlesView: TcxGridDBTableView;
    GroupArticlesLevel: TcxGridLevel;
    BarManager: TdxBarManager;
    BarManagerBar1: TdxBar;
    dxBarButton1: TdxBarButton;
    dxBarDockControl1: TdxBarDockControl;
    dxBarLargeButton2: TdxBarLargeButton;
    BarManagerBar2: TdxBar;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    Images: TImageList;
    dxBarLargeButton1: TdxBarLargeButton;
    Actions: TActionList;
    acAddOperation: TAction;
    acDeleteOperation: TAction;
    acAddArticle: TAction;
    acDeleteArticle: TAction;
    ArticlesViewColumn2: TcxGridDBColumn;
    GroupArticlesViewColumn1: TcxGridDBColumn;
    OperationsViewColumn1: TcxGridDBColumn;
    GroupOperationsViewColumn1: TcxGridDBColumn;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    acAddArticleMask: TAction;
    acDeleteGroupArticles: TAction;
    acDeleteGroupOperations: TAction;
    procedure acAddOperationExecute(Sender: TObject);
    procedure acDeleteOperationExecute(Sender: TObject);
    procedure acAddArticleExecute(Sender: TObject);
    procedure acDeleteArticleExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDeleteGroupArticlesExecute(Sender: TObject);
    procedure acAddArticleMaskExecute(Sender: TObject);
    procedure acDeleteGroupOperationsExecute(Sender: TObject);
    procedure acDeleteOperationUpdate(Sender: TObject);
    procedure acDeleteGroupOperationsUpdate(Sender: TObject);
    procedure acDeleteArticleUpdate(Sender: TObject);
    procedure acDeleteGroupArticlesUpdate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    class procedure Execute;
    { Public declarations }
  end;

var
  fmTechnologyGroupCustomization: TfmTechnologyGroupCustomization;

implementation

{$R *.dfm}
uses TechnologyGroups, uUtils, StrUtils, uDialogs;

procedure TfmTechnologyGroupCustomization.acAddArticleExecute(Sender: TObject);
begin
with fmTechnologyGroups do
  begin
    if not VarIsNull(GroupArticles.Lookup('GroupID;Article$ID', VarArrayOf([Groups.FieldByName('ID').AsInteger,ArticlesD_ARTID.AsInteger]), 'Article$ID')) then
      begin
        DialogErrorOkMessage('������� ��� ����������� �����!');
        SysUtils.Abort;
      end;
    try
      GroupArticles.Insert;
      GroupArticles.Post;
    finally

    end;
    GroupArticles.Close;
    GroupArticles.Open;
  end;
end;

procedure TfmTechnologyGroupCustomization.acAddArticleMaskExecute(
  Sender: TObject);
var i: integer;
begin
if DialogQuestionYesNoMessage('�������� ��� �������� � �����?') = mrYes then
  with ArticlesView.DataController do
    begin
      BeginUpdate;
      for i := 0 to FilteredRecordCount - 1 do
        begin
          FocusedRecordIndex := FilteredRecordIndex[i];
          acAddArticle.Execute;
        end;
      EndUpdate;
    end;
end;

procedure TfmTechnologyGroupCustomization.acAddOperationExecute(
  Sender: TObject);
begin
with fmTechnologyGroups do
  begin
    if not VarIsNull(GroupOperations.Lookup('Operation$ID', OperationsID.AsInteger, 'Operation$ID')) then
      begin
        DialogErrorOkMessage('�������� ��� ����������� �����!');
        SysUtils.Abort;
      end;
    try
      GroupOperations.Insert;
      GroupOperations.Post;
    finally

    end;
    GroupOperations.Close;
    GroupOperations.Open;
  end;
end;

procedure TfmTechnologyGroupCustomization.acDeleteArticleExecute(
  Sender: TObject);
begin
  with fmTechnologyGroups.GroupArticles do
    try
      Delete;
    finally
      Transaction.CommitRetaining;
    end;
end;

procedure TfmTechnologyGroupCustomization.acDeleteArticleUpdate(
  Sender: TObject);
begin
  acDeleteArticle.Enabled := (fmTechnologyGroups.GroupArticles.RecordCount > 0) and
    (GroupArticlesView.Controller.SelectedRowCount <> 0);
end;

procedure TfmTechnologyGroupCustomization.acDeleteGroupArticlesExecute(
  Sender: TObject);
var i, ACount: integer;
begin
  with GroupArticlesView.dataController do
    begin
      ACount := RecordCount;
      if DialogQuestionYesNoMessage('������� ��� �������� �����?') = mrYes then
        begin
          BeginUpdate;
          for i := 0 to ACount - 1 do
            begin
              FocusedRecordIndex := 0;
              acDeleteArticle.Execute;
            end;
          EndUpdate;
        end;
    end;
end;


procedure TfmTechnologyGroupCustomization.acDeleteGroupArticlesUpdate(
  Sender: TObject);
begin
  acDeleteGroupArticles.Enabled := (fmTechnologyGroups.GroupArticles.RecordCount > 0) and
    (GroupArticlesView.Controller.SelectedRowCount <> 0);
end;

procedure TfmTechnologyGroupCustomization.acDeleteGroupOperationsExecute(
  Sender: TObject);
var i, ACount: integer;
begin
  with GroupOperationsView.dataController do
    begin
      ACount := RecordCount;
      if DialogQuestionYesNoMessage('������� ��� �������� �����?') = mrYes then
        begin
          BeginUpdate;
          for i := 0 to ACount - 1 do
            begin
              FocusedRecordIndex := 0;
              acDeleteOperation.Execute;
            end;
          EndUpdate;
        end;
    end;
end;

procedure TfmTechnologyGroupCustomization.acDeleteGroupOperationsUpdate(
  Sender: TObject);
begin
  acDeleteGroupOperations.Enabled := (fmTechnologyGroups.GroupOperations.RecordCount > 0) and
    (GroupOperationsView.Controller.SelectedRowCount <> 0);
end;

procedure TfmTechnologyGroupCustomization.acDeleteOperationExecute(
  Sender: TObject);
begin
with fmTechnologyGroups.GroupOperations do
    try
      Delete;
    finally
      Transaction.CommitRetaining;
    end;
end;

procedure TfmTechnologyGroupCustomization.acDeleteOperationUpdate(
  Sender: TObject);
begin
  acDeleteOperation.Enabled := (fmTechnologyGroups.GroupOperations.RecordCount > 0) and
    (GroupOperationsView.Controller.SelectedRowCount <> 0);
end;

class procedure TfmTechnologyGroupCustomization.Execute;
begin
  fmTechnologyGroupCustomization := nil;
  try
    fmTechnologyGroupCustomization := TfmTechnologyGroupCustomization.Create(Application);
    fmTechnologyGroupCustomization.ShowModal;
  finally
    FreeAndNil(fmTechnologyGroupCustomization);
  end;
end;

procedure TfmTechnologyGroupCustomization.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  with fmTechnologyGroups do
    begin
      GroupArticles.Filter := '';
      GroupArticles.Filtered := False;
      GroupOperations.CloseOpen(false);
    end;
end;

procedure TfmTechnologyGroupCustomization.FormCreate(Sender: TObject);
begin
with fmTechnologyGroups do
  begin
    Operations.CloseOpen(False);
    Articles.CloseOpen(False);
  end;
end;


end.
