unit Setting;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ExtCtrls, DBCtrls, StdCtrls, RxToolEdit, Mask,
  RxDBComb, db,  Menus, Buttons, RXDBCtrl, Grids, DBGrids,
  CheckLst, DBGridEh, TB2Dock, TB2Toolbar,
  TB2Item, ImgList, ActnList, M207DBCtrls, DBCtrlsEh, DBLookupEh, M207Ctrls,
  DBGridEhGrouping, rxPlacemnt, GridsEh, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxPC, dxLayoutControl, dxLayoutLookAndFeels,
  cxContainer, cxEdit, dxLayoutcxEditAdapters, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxDBData, FIBDataSet, pFIBDataSet, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, dxBar, cxCheckBox, cxDBEdit, cxSpinEdit,
  cxTextEdit, cxMaskEdit, cxButtonEdit, cxShellBrowserDialog;

const
  PatternPanelIndex = 3;

type
  TfmSetting = class(TForm)
    plSetup: TPanel;
    Splitter1: TSplitter;
    plOrg: TPanel;
    plDep: TPanel;
    Splitter2: TSplitter;
    plParams: TPanel;
    Label2: TLabel;
    Label8: TLabel;
    edSName: TDBEdit;
    dbclrDep: TM207DBColor;
    plRoot: TPanel;
    Label1: TLabel;
    fmstr: TFormStorage;
    lstvSetting: TListView;
    pmOrg: TPopupMenu;
    mnitAddDep: TMenuItem;
    mnitDelDep: TMenuItem;
    mnitEditDep: TMenuItem;
    Panel1: TPanel;
    Panel2: TPanel;
    plListOper: TPanel;
    btsDepTypes: TM207DBBits;
    plConst: TPanel;
    Label3: TLabel;
    DBGridEh1: TDBGridEh;
    DBNavigator1: TDBNavigator;
    Panel3: TPanel;
    TBDock1: TTBDock;
    tlbrTree: TTBToolbar;
    tv1: TTreeView;
    acEvent: TActionList;
    acAddDepart: TAction;
    acDelDepart: TAction;
    im16: TImageList;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acEditDepart: TAction;
    TBItem3: TTBItem;
    TBDock2: TTBDock;
    tlbrOper: TTBToolbar;
    acAddOper: TAction;
    acDelOper: TAction;
    TBItem4: TTBItem;
    TBItem5: TTBItem;
    gridOperList: TDBGridEh;
    grPs: TDBGridEh;
    TBDock: TTBDock;
    tlbrPS: TTBToolbar;
    acAddMOL: TAction;
    acDelMOL: TAction;
    TBItem6: TTBItem;
    TBItem7: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem8: TTBItem;
    acSort: TAction;
    Panel4: TPanel;
    chbxUseDotMatrix: TDBCheckBoxEh;
    Label4: TLabel;
    Label5: TLabel;
    edY_MatrixProd: TDBNumberEditEh;
    edX_MatrixProd: TDBNumberEditEh;
    chbxShowDialogStone: TDBCheckBoxEh;
    cmbxCassaNDS: TDBCheckBoxEh;
    lbNDS: TLabel;
    edCassaNDS: TDBNumberEditEh;
    Label6: TLabel;
    cmbxCOM: TDBComboBoxEh;
    Label7: TLabel;
    edDbPath: TDBEditEh;
    Label9: TLabel;
    edExtDbPath: TDBEditEh;
    lbCompName: TLabel;
    lbPortBarCodePrinter: TLabel;
    Label10: TLabel;
    cmbxPortBarCodePrinter: TDBComboBoxEh;
    lbModelNameBarCodePrinter: TLabel;
    M207Bevel1: TM207Bevel;
    chbxUseScale: TDBCheckBoxEh;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    cmbxPortScale: TDBComboBoxEh;
    M207Bevel2: TM207Bevel;
    chbxUseBarCodePrinter: TDBCheckBoxEh;
    chbxUseBarCodeScaner: TDBCheckBoxEh;
    Label14: TLabel;
    cmbxPortBarCodeScaner: TDBComboBoxEh;
    Label15: TLabel;
    M207Bevel3: TM207Bevel;
    lbBarCode_Step: TLabel;
    edBarCodePrinterStep: TDBEditEh;
    Button1: TButton;
    acFormCoverDepart: TAction;
    Label16: TLabel;
    cmbxChangeApplOperId: TDBLookupComboboxEh;
    cbScaleClass: TDBComboBoxEh;
    PanelPatternSettings: TPanel;
    LayoutLookAndFeel: TdxLayoutLookAndFeelList;
    dxLayoutCxLookAndFeel: TdxLayoutCxLookAndFeel;
    LayoutGroupRoot: TdxLayoutGroup;
    Layout: TdxLayoutControl;
    GroupPatterns: TdxLayoutGroup;
    GroupSettings: TdxLayoutGroup;
    GroupFilesAndFolders: TdxLayoutGroup;
    GroupOptions: TdxLayoutGroup;
    GroupAdditional: TdxLayoutGroup;
    EditPriceFilePath: TcxDBButtonEdit;
    ItemPriceFilePath: TdxLayoutItem;
    EditPatternExportFolder: TcxDBButtonEdit;
    ItemPatternExportFolder: TdxLayoutItem;
    EditDecimalDigits: TcxDBSpinEdit;
    ItemDecimalDigits: TdxLayoutItem;
    EditMaxSkipCellCount: TcxDBSpinEdit;
    ItemMaxSkipCellCount: TdxLayoutItem;
    CheckBoxIgnoreZeroPrice: TcxDBCheckBox;
    ItemIgnoreZeroPrice: TdxLayoutItem;
    CheckBoxIgnoreZeroAverageWeight: TcxDBCheckBox;
    ItemIgnoreZeroAverageWeight: TdxLayoutItem;
    CheckBoxIgnoreNotIsANumber: TcxDBCheckBox;
    ItemIgnoreNotIsANumber: TdxLayoutItem;
    GroupGrid: TdxLayoutGroup;
    GroupMenu: TdxLayoutGroup;
    BarManadger: TdxBarManager;
    Bar: TdxBar;
    DockControl: TdxBarDockControl;
    ItemMenu: TdxLayoutItem;
    View: TcxGridDBTableView;
    Level: TcxGridLevel;
    Grid: TcxGrid;
    ItemGrid: TdxLayoutItem;
    DataSetSettings: TpFIBDataSet;
    DataSetPatterns: TpFIBDataSet;
    DataSetPatternsID: TFIBIntegerField;
    DataSetPatternsNAME: TFIBStringField;
    DataSetPatternsPATTERNORDER: TFIBIntegerField;
    DataSetPatternsFIELDNAME: TFIBStringField;
    DataSetPatternsBITPARAMS: TFIBIntegerField;
    SourceSettings: TDataSource;
    SourcePatterns: TDataSource;
    DataSetSettingsDECIMALDIGITS: TFIBIntegerField;
    DataSetSettingsMAXSKIPCELLSCOUNT: TFIBSmallIntField;
    DataSetSettingsBITPARAMS: TFIBIntegerField;
    ViewNAME: TcxGridDBColumn;
    ViewPATTERNORDER: TcxGridDBColumn;
    ViewFIELDNAME: TcxGridDBColumn;
    ViewISTOLLING: TcxGridDBColumn;
    ButtonAdd: TdxBarButton;
    ButtonDelete: TdxBarButton;
    AddPattern: TAction;
    DeletePattern: TAction;
    OpenDialog: TOpenDialog;
    BrowserDialog: TcxShellBrowserDialog;
    DataSetSettingsIGNOREZEROPRICE: TFIBIntegerField;
    DataSetSettingsIGNOREZEROWEIGHT: TFIBIntegerField;
    DataSetSettingsIGNORENANSYMBOLS: TFIBIntegerField;
    DataSetSettingsFILENAME: TFIBStringField;
    DataSetSettingsOUTPUTDIRECTORY: TFIBStringField;
    DataSetPatternsCOMPANYID: TFIBIntegerField;
    DataSetPatternsISTOLLING: TFIBIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tv1Change(Sender: TObject; Node: TTreeNode);
    procedure tv1Changing(Sender: TObject; Node: TTreeNode; var AllowChange: Boolean);
    procedure tv1Deletion(Sender: TObject; Node: TTreeNode);
    procedure tv1DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure tv1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure tv1Edited(Sender: TObject; Node: TTreeNode; var S: String);
    procedure tv1Editing(Sender: TObject; Node: TTreeNode; var AllowEdit: Boolean);
    procedure tv1Exit(Sender: TObject);
    procedure tv1Expanding(Sender: TObject; Node: TTreeNode; var AllowExpansion: Boolean);
    procedure tv1GetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure tv1GetSelectedIndex(Sender: TObject; Node: TTreeNode);
    procedure tv1StartDrag(Sender: TObject; var DragObject: TDragObject);
    procedure lstvSettingClick(Sender: TObject);
    procedure lstvSettingChange(Sender: TObject; Item: TListItem; Change: TItemChange);
    procedure mnitAddDepClick(Sender: TObject);
    procedure mnitDelDepClick(Sender: TObject);
    procedure mnitEditDepClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acAddDepartExecute(Sender: TObject);
    procedure acDelDepartExecute(Sender: TObject);
    procedure acEditDepartExecute(Sender: TObject);
    procedure acAddOperExecute(Sender: TObject);
    procedure acDelOperExecute(Sender: TObject);
    procedure acAddMOLExecute(Sender: TObject);
    procedure acDelMOLExecute(Sender: TObject);
    procedure acDelMOLUpdate(Sender: TObject);
    procedure acDelOperUpdate(Sender: TObject);
    procedure gridOperListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure grPsGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acSortExecute(Sender: TObject);
    procedure lpcbxRootCloseUp(Sender: TObject; Accept: Boolean);
    procedure acFormCoverDepartExecute(Sender: TObject);
    procedure DataSetSettingsBeforePost(DataSet: TDataSet);
    procedure DataSetPatternsBeforePost(DataSet: TDataSet);
    procedure DataSetSettingsBeforeOpen(DataSet: TDataSet);
    procedure DataSetPatternsBeforeOpen(DataSet: TDataSet);
    procedure OpenDialogClose(Sender: TObject);
    procedure PropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
    procedure DataSetSettingsBeforeClose(DataSet: TDataSet);
    procedure DataSetPatternsBeforeClose(DataSet: TDataSet);
    procedure EditValueChanged(Sender: TObject);
    procedure DataSetSettingsAfterPost(DataSet: TDataSet);
    procedure DeletePatternExecute(Sender: TObject);
    procedure AddPatternExecute(Sender: TObject);
  private
    DraggingNode: TTreeNode;
    SelfId : integer;
    SelfName : string;
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
    procedure AddDepart;
    procedure DelDepart;
    procedure SetParams(DataSet: TpFIBDataSet);
  public
    constructor Create(AOwner : TComponent); override;
  end;

var
  fmSetting: TfmSetting;
  SettingTextNodes : array of string;

implementation

uses DictData, fmUtils, dbUtil, DBTree, MainData, PrintData, Variants,
  DictAncestor, MsgDialog, Sort, IniFiles, UtilLib, Editor, eCoverDepart,
  Main, uUtils, ShlObj;

{$R *.DFM}

{ TfmSetting }

procedure TfmSetting.AddDepart;
var n: TTreeNode;
    nd: TNodeData;
begin
  if tv1.Selected=NIL then tv1.Selected:=tv1.TopItem;
  with tv1 do
    begin
      tv1.Selected.HasChildren:=True;
      n:=Items.AddChild(tv1.Selected, '');
      n.Data:=NIL;
      Selected:=n;
      n.EditText;
    end;

   nd:=TNodeData.Create;
   n.Data:=nd;
   with nd do
     begin
       Code:=dm.GetID(4);
       Loaded:=False;
     end;
  dm.D_DepId := -1;
   with dm, taDep do
     begin
       Params[0].AsInteger:=nd.Code;
       Active:=True;
       Insert;

       taDepDepId.AsInteger:=nd.Code;
       if  TNodeData(n.Parent.Data).Code<>NULL then taDepParent.AsInteger:=TNodeData(n.Parent.Data).Code
       else taDepParent.Clear;
     end;

end;

procedure TfmSetting.AddPatternExecute(Sender: TObject);
begin

  DataSetPatterns.Append;

  DataSetPatternsID.AsInteger := DataSetPatterns.Database.Gen_Id('G$PATTERN$ITEMS$ID', 1);
  DataSetPatternsCOMPANYID.AsInteger := dm.User.SelfCompId;
  DataSetPatternsPATTERNORDER.AsInteger := DataSetPatterns.Database.QueryValue('select max(pattern$order) + 1 from pattern$items where company$id = :company$id', 0, [dm.User.SelfCompId]);
  DataSetPatternsNAME.AsString := '��� �������';

  DataSetPatterns.Post;

end;

procedure TfmSetting.PropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
var
  Edit: TcxDBButtonEdit;
  DataSet: TpFIBDataSet;
  FieldName: String;
begin

  if Sender is TcxDBButtonEdit then
  begin
    Edit := TcxDBButtonEdit(Sender);
  end else
  begin
    Exit;
  end;

  Edit.Tag := 1;
  Edit.DoEditing;
  
  DataSet := TpFIBDataSet(Edit.DataBinding.DataSource.DataSet);
  FieldName := Edit.DataBinding.DataField;

  if Edit.Name = 'EditPriceFilePath' then
  begin
    OpenDialog.Title := '�������� ���� ������';
    OpenDialog.InitialDir := 'Y:\_������\';
    OpenDialog.Filter := 'Microsoft Excel (xls, xlsx)|*.xlsx; *.xls';
    OpenDialog.Options := OpenDialog.Options + [ofFileMustExist];

    if OpenDialog.Execute() then
    begin
      Edit.Text := OpenDialog.FileName;
    end;
  end else
  if Edit.Name = 'EditPatternExportFolder' then
  begin
    if BrowserDialog.Execute then
    begin
      Edit.Text := BrowserDialog.Path;
    end;
  end;

end;

procedure TfmSetting.DataSetPatternsBeforeClose(DataSet: TDataSet);
begin
  if DataSet.State in [dsEdit, dsInsert] then
  begin
    DataSet.Post;
  end;
end;

procedure TfmSetting.DataSetPatternsBeforeOpen(DataSet: TDataSet);
begin
  SetParams(TpFIBDataSet(DataSet));
end;

procedure TfmSetting.DataSetPatternsBeforePost(DataSet: TDataSet);
var
  BitParams: Integer;
begin

  BitParams := 0;

  if DataSetPatternsISTOLLING.AsBoolean then
  begin
    BitParams := BitParams or 1;
  end;

  DataSetPatternsBITPARAMS.AsInteger := BitParams;

end;

procedure TfmSetting.SetParams(DataSet: TpFIBDataSet);
begin
  DataSet.ParamByName('Company$ID').AsInteger := dm.User.SelfCompId;
end;

procedure TfmSetting.DataSetSettingsAfterPost(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).Transaction.CommitRetaining;
end;

procedure TfmSetting.DataSetSettingsBeforeClose(DataSet: TDataSet);
begin
  if DataSet.State in [dsEdit, dsInsert] then
  begin
    DataSet.Post;
  end;  
end;

procedure TfmSetting.DataSetSettingsBeforeOpen(DataSet: TDataSet);
begin
  SetParams(TpFIBDataSet(DataSet));
end;

procedure TfmSetting.DataSetSettingsBeforePost(DataSet: TDataSet);
var
  BitParams: Integer;
begin

  BitParams := 0;

  if DataSetSettingsIGNOREZEROPRICE.AsInteger = 1 then
  begin
    BitParams := BitParams or 1;
  end;

  if DataSetSettingsIGNOREZEROWEIGHT.AsInteger = 1 then
  begin
    BitParams := BitParams or 2;
  end;

  if DataSetSettingsIGNORENANSYMBOLS.AsInteger = 1 then
  begin
    BitParams := BitParams or 4;
  end;

  DataSetSettingsBITPARAMS.AsInteger := BitParams;

end;

procedure TfmSetting.DelDepart;
var
  n: TTreeNode;
begin
  n:=tv1.Selected;
  if (n=tv1.TopItem) or (n=NIL) then exit;
  dm.taDep.Delete;
  n.Delete;
end;

procedure TfmSetting.DeletePatternExecute(Sender: TObject);
begin
  DataSetPatterns.Delete;
end;

procedure TfmSetting.EditValueChanged(Sender: TObject);
var
  ButtonEdit: TcxDBButtonEdit;
  DataSet: TDataSet;
  FieldName: String;
begin

  if (Sender is TcxDBButtonEdit) then
  begin
    ButtonEdit := TcxDBButtonEdit(Sender);
  end else
  begin
    Exit;
  end;

  DataSet := ButtonEdit.DataBinding.DataSource.DataSet;
  FieldName := ButtonEdit.DataBinding.DataField;

  if DataSet.Active then
  begin
    if DataSet.State in [dsEdit, dsInsert] then
    begin
      DataSet.Post;
    end;
    
    DataSet.Edit;

    DataSet.FieldByName(FieldName).AsVariant := ButtonEdit.EditValue;

    DataSet.Post;
  end;

end;

procedure TfmSetting.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then MinimizeApp
  else inherited;
end;

procedure TfmSetting.FormCreate(Sender: TObject);
var
  i : integer;
  Ini : TIniFile;
  sect : TStringList;
begin
  lbCompName.Caption := dm.User.SelfCompName;
  (* ��������� ������ �� SettingTextNodes *)
  lstvSetting.Items.BeginUpdate;
  try
    lstvSetting.Items.Clear;
    for i:=Low(SettingTextNodes) to High(SettingTextNodes) do
      lstvSetting.Items.Add.Caption := SettingTextNodes[i];
  finally
    lstvSetting.Items.EndUpdate;
  end;

  with plSetup do
    for i:=0 to Pred(ControlCount) do
      if Controls[i].InheritsFrom(TPanel) then
        if Controls[i].Tag = 0 then
        begin
          Controls[i].Visible := True;
          Controls[i].Align := alClient;
        end
        else Controls[i].Visible := False;

  with dm do
  begin
    if not tr.Active then tr.StartTransaction;
    OpenDataSets([taPs, taComp, taRec, taDep, taOper, taConst]);

    taMOL.Locate('MOLID', dm.User.UserId, []);
    InitTree('������������ �����������', tv1, NULL);
    SelfId := dm.User.SelfCompId;
    SelfName := dm.User.SelfCompName;
  end;

  lstvSetting.Selected := lstvSetting.TopItem;
  ActiveControl := lstvSetting;
  //tlbrTree.Skin := dm.TBSkin;
  //tlbrPS.Skin := dm.TBSkin;
  //tlbrOper.Skin := dm.TBSkin;
  (* ��������� ini-���� *)
  Ini := TIniFile.Create(GetIniFileName);
  sect := TStringList.Create;
  try
    Ini.ReadSectionValues('ReportSetting', sect);
    chbxUseDotMatrix.Checked := sect.Values['UseDotMatrix'] = '1';
    // ��������� ������
    Ini.ReadSectionValues('LocalSetting', sect);
    chbxShowDialogStone.Checked := sect.Values['UseStoneDialog'] = '1';
    lbModelNameBarCodePrinter.Caption := 'GODEX EZ1100';
    chbxUseBarCodePrinter.Checked := sect.Values['BarCodePrinterUse']='1';
    cmbxPortBarCodePrinter.ItemIndex := cmbxPortBarCodePrinter.Items.IndexOf(sect.Values['BarCodePrinterPort']);



    chbxUseScale.Checked := sect.Values['ScaleUse']='1';
    cmbxPortScale.ItemIndex := cmbxPortScale.Items.IndexOf(sect.Values['ScalePort']);
    if Sect.IndexOfName('ScaleClass') = -1 then cbScaleClass.ItemIndex := -1
    else cbScaleClass.ItemIndex := StrToInt(Sect.Values['ScaleClass']);

    chbxUseBarCodeScaner.Checked := sect.Values['BarCodeScanerUse']='1';
    cmbxPortBarCodeScaner.ItemIndex := cmbxPortBarCodeScaner.Items.IndexOf(sect.Values['BarCodeScanerPort']);
    edBarCodePrinterStep.Text := sect.Values['BarCodePrinterStep'];
    
    // �����
    Ini.ReadSectionValues('Cassa', sect);
    cmbxCassaNDS.Checked := sect.Values['UseNDS'] = '1';
    if sect.Values['NDS']='' then edCassaNDS.Value := 0
    else edCassaNDS.Value := ConvertStrToFloat(sect.Values['NDS']);
    if sect.Values['Port']='' then cmbxCOM.Text := 'COM1'
    else cmbxCOM.Text := sect.Values['Port'];
  finally
    Ini.Free;
    sect.Free;
  end;

  DataSetSettings.Open;
  DataSetPatterns.Open;
end;

procedure TfmSetting.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Ini : TIniFile;
begin
  tv1Exit(tv1);
  with dm do
  begin
    PostDataSets([taPs, taPsOper, taComp, taDep, taRec, taMOL, taConst]);
    //SelfName := taRecName.AsString;
    //SelfId := taRecCompId.AsInteger;
    CloseDataSets([taComp, taRec, taDep, taMOL, taConst]);
    tr.Commit;
  end;
  dm.InitDeps;
  dm.LoadArtSL(PS_DICT);
  dm.LoadArtSL(WHPROD_DICT);
  dm.LoadArtSL(PSWHPROD_DICT);
  // �������� Ini-����
  Ini := TIniFile.Create(GetIniFileName);
  try

    if chbxUseDotMatrix.Checked then Ini.WriteString('ReportSetting', 'UseDotMatrix', '1')
    else Ini.WriteString('ReportSetting', 'UseDotMatrix', '0');

    if chbxShowDialogStone.Checked then Ini.WriteString('LocalSetting', 'UseStoneDialog', '1')
    else Ini.WriteString('LocalSetting', 'UseStoneDialog', '0');

    if cmbxCassaNDS.Checked then Ini.WriteString('Cassa', 'UseNDS', '1')
    else Ini.WriteString('Cassa','UseNDS', '0');
    Ini.WriteString('Cassa', 'NDS', edCassaNDS.Value);

    if cmbxCOM.ItemIndex = -1 then Ini.WriteString('Cassa', 'Port',cmbxCOM.Items[0])
    else Ini.WriteString('Cassa', 'Port', cmbxCOM.Items[cmbxCOM.ItemIndex]);

    Ini.WriteBool('LocalSetting', 'BarCodePrinterUse', chbxUseBarCodePrinter.Checked);
    if(cmbxPortBarCodePrinter.ItemIndex = -1)then Ini.WriteString('LocalSetting','BarCodePrinterPort',cmbxPortBarCodePrinter.Items[0])
    else Ini.WriteString('LocalSetting', 'BarCodePrinterPort', cmbxPortBarCodePrinter.Items[cmbxPortBarCodePrinter.ItemIndex]);
    Ini.WriteString('LocalSetting', 'BarCodePrinterStep', edBarCodePrinterStep.Text);

    Ini.WriteBool('LocalSetting', 'ScaleUse', chbxUseScale.Checked);

    if(cmbxPortScale.ItemIndex = -1)then Ini.WriteString('LocalSetting','ScalePort',cmbxPortScale.Items[0])
    else Ini.WriteString('LocalSetting', 'ScalePort', cmbxPortScale.Items[cmbxPortScale.ItemIndex]);

    Ini.WriteString('LocalSetting', 'ScaleClass', IntToStr(cbScaleClass.ItemIndex));

    Ini.WriteBool('LocalSetting', 'BarCodeScanerUse', chbxUseBarCodeScaner.Checked);
    if(cmbxPortBarCodeScaner.ItemIndex = -1)then Ini.WriteString('LocalSetting', 'BarCodeScanerPort', cmbxPortBarCodeScaner.Items[0])
    else Ini.WriteString('LocalSetting', 'BarCodeScanerPort', cmbxPortBarCodeScaner.Items[cmbxPortBarCodeScaner.ItemIndex]);

  finally
    Ini.Free;
  end;
  dmPrint.InitReportSetting;
  dm.InitLocalSetting;
  if dm.LocalSetting.BarCodeScaner_Use then
  begin
    //fmMain.CommPortDriver1.Disconnect;
    //fmMain.CommPortDriver1.Connect;
  end;

  DataSetSettings.Active := false;
  DataSetPatterns.Active := false;

end;

procedure TfmSetting.tv1Change(Sender: TObject; Node: TTreeNode);
var
  nd: TNodeData;
begin
  try
    nd:=TNodeData(Node.Data);
    if nd<>NIL then
      with  dm do
      begin
        if TNodeData(Node.Data).Code<>NULL then
        begin
          D_DepId := TNodeData(Node.Data).Code;
          ReOpenDataSets([taDep]);
        end
      end;
  except
    on E: Exception do MessageDialog(e.Message, mtError, [mbOK], 0);
  end;
end;

procedure TfmSetting.tv1Changing(Sender: TObject; Node: TTreeNode;
  var AllowChange: Boolean);
begin
  dm.taDep.Active:=False;
end;

procedure TfmSetting.tv1Deletion(Sender: TObject; Node: TTreeNode);
begin
  if (Node<>NIL) and Assigned(Node.Data) then TNodeData(Node.Data).Free;
end;

procedure TfmSetting.tv1DragDrop(Sender, Source: TObject; X, Y: Integer);
var Node: TTreeNode;
begin
  Node:=tv1.GetNodeAt(X,Y);
  if Node=NIL then exit;
  if Node.Data=NIL then exit;

  DraggingNode.MoveTo(Node, naAddChild);

  with dm, taDep do
    begin
      if not (State = dsInsert)or(State = dsEdit) then Edit;
      if TNodeData(Node.Data).Code<>NULL then taDepParent.AsInteger := TNodeData(Node.Data).Code
      else taDepParent.Clear;
      Post;
    end;
end;

procedure TfmSetting.tv1DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
var Node: TTreeNode;
begin
  Node:=tv1.GetNodeAt(X,Y);
  Accept:=(Node<>NIL) AND
          (Node<>DraggingNode) AND
          (Node<>DraggingNode.Parent) AND
          (DraggingNode<>tv1.TopItem);

end;

procedure TfmSetting.tv1Edited(Sender: TObject; Node: TTreeNode;
  var S: String);
begin
  if Node.Data<>NIL then
     with dm, taDep do
       begin
         if not (State in [dsEdit, dsInsert]) then Edit;
         taDepName.AsString:=S;
       end;
end;

procedure TfmSetting.tv1Editing(Sender: TObject; Node: TTreeNode;
  var AllowEdit: Boolean);
begin
  AllowEdit:=Node<>tv1.TopItem;
end;

procedure TfmSetting.tv1Exit(Sender: TObject);
begin
  if tv1.IsEditing then tv1.Selected.EndEdit(False);
end;

procedure TfmSetting.tv1Expanding(Sender: TObject; Node: TTreeNode; var AllowExpansion: Boolean);
var
  nd : TNodeData;
  Params : array of Variant;
begin
  SetLength(Params, 2);
  nd := TNodeData(Node.Data);  
  if nd.Code=NULL then
  begin
    Params[0]:='-2';
    Params[1]:=1;
  end
  else begin
    Params[0]:=ND.Code;
    Params[1]:=0;
  end;
  ExpandTreeD(TTreeView(Node.TreeView), Node, NodeText1,
      dm.quDepT, ['DEPID', 'NAME', 'PARENT', 'CHILD'], Params)
  //ExpandTree(TTreeView(Node.TreeView), Node, NodeText1, dm.quDepT, [0,1,3]);
end;

procedure TfmSetting.tv1GetImageIndex(Sender: TObject; Node: TTreeNode);
begin
  GetNodeImages(Node);
end;

procedure TfmSetting.tv1GetSelectedIndex(Sender: TObject; Node: TTreeNode);
begin
  GetSelNodeImages(Node);
end;

procedure TfmSetting.tv1StartDrag(Sender: TObject;
  var DragObject: TDragObject);
begin
if tv1.Selected=tv1.TopItem then SysUtils.Abort;
  DraggingNode:=tv1.Selected;
end;


procedure TfmSetting.lstvSettingClick(Sender: TObject);

  function Find(s : string) : integer;
  var
    i : integer;
  begin
    Result := -1;
    for i := Low(SettingTextNodes) to High(SettingTextNodes) do
      if SettingTextNodes[i] = s then
      begin
        Result := i+1;
        break;
      end;
  end;
  
var
  Ind, i: integer;
begin
  if not Assigned(lstvSetting.Selected) then eXit;
  Ind := Find(lstvSetting.Selected.Caption);

  if ((Ind = PatternPanelIndex + 1) and (not dm.IsAdm)) then
  begin
    Exit;
  end;

  with plSetup do
    for i:=0 to Pred(ControlCount) do
      if Controls[i].InheritsFrom(TPanel) then
        if Controls[i].Tag = Ind then
        begin
          TCustomPanel(Controls[i]).Visible := True;
          TCustomPanel(Controls[i]).Align := alClient;
        end
        else TCustomPanel(Controls[i]).Visible := False;
end;

procedure TfmSetting.lstvSettingChange(Sender: TObject; Item: TListItem;
  Change: TItemChange);
begin
  lstvSettingClick(Sender);
end;

procedure TfmSetting.mnitAddDepClick(Sender: TObject);
begin
  AddDepart;
  ReOpenDataSets([dm.taPs, dm.taPsOper]);
end;

procedure TfmSetting.mnitDelDepClick(Sender: TObject);
begin
  DelDepart;
  ReOpenDataSets([dm.taPS, dm.taPsOper]);
end;

procedure TfmSetting.mnitEditDepClick(Sender: TObject);
begin
  with tv1 do
    if Selected<>NIL then Selected.EditText;
end;

procedure TfmSetting.OpenDialogClose(Sender: TObject);
begin
  if EditPriceFilePath.Tag = 1 then
  begin
    EditPriceFilePath.Tag := 0;
  end;

  if EditPatternExportFolder.Tag = 1 then
  begin
    EditPatternExportFolder.Tag := 0;
  end;
end;

procedure TfmSetting.FormActivate(Sender: TObject);
begin
  tv1.TopItem.Text:= SelfName;
end;

constructor TfmSetting.Create(AOwner: TComponent);
begin
  inherited;
end;

procedure TfmSetting.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_ESCAPE : if Shift = [] then Close;
  end;
end;

procedure TfmSetting.acAddDepartExecute(Sender: TObject);
begin
  AddDepart;
  ReOpenDataSets([dm.taPs, dm.taPsOper]);
end;

procedure TfmSetting.acDelDepartExecute(Sender: TObject);
begin
  DelDepart;
  ReOpenDataSets([dm.taPS, dm.taPsOper]);
end;

procedure TfmSetting.acEditDepartExecute(Sender: TObject);
begin
  with tv1 do
    if Selected<>NIL then Selected.EditText;
end;

procedure TfmSetting.acAddOperExecute(Sender: TObject);
begin
  dm.taPsOper.Insert;
end;

procedure TfmSetting.acDelOperExecute(Sender: TObject);
begin
  dm.taPsOper.Delete;
end;

procedure TfmSetting.acAddMOLExecute(Sender: TObject);
begin
  dm.taPS.Insert;
end;

procedure TfmSetting.acDelMOLExecute(Sender: TObject);
begin
  dm.taPS.Delete;
end;

procedure TfmSetting.acDelMOLUpdate(Sender: TObject);
begin
  (Sender as Taction).Enabled := not dm.taPS.IsEmpty;
end;

procedure TfmSetting.acDelOperUpdate(Sender: TObject);
begin
  (Sender as Taction).Enabled := not dm.taPsOper.IsEmpty;
end;

procedure TfmSetting.gridOperListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(Column.FieldName = 'RecNo')then Background := clMoneyGreen;
end;

procedure TfmSetting.grPsGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if(Column.FieldName = 'RecNo')then Background := clMoneyGreen;
end;

procedure TfmSetting.acSortExecute(Sender: TObject);
var
  i: integer;
  nd: TNodeData;
begin
  fmSort:=TfmSort.Create(Application);
  with fmSort do
  try
    lb1.Items.Clear;
    ReOpenDataSet(dm.taDepart);
    while not dm.taDepart.Eof do
    begin
      nd:=TNodeData.Create;
      nd.Code:=dm.taDepartDEPID.AsInteger;
      lb1.Items.AddObject(dm.taDepartNAME.AsString, nd);
      dm.taDepart.Next;
    end;
    dm.taDepart.Close;

    if(ShowModal <> mrOK)then eXit;
    for i:=0 to lb1.Items.Count-1 do
      ExecSQL('update D_Dep set SortInd='+IntToStr(i)+' where DepID='+IntToStr(TNodeData(lb1.Items.Objects[i]).Code), dm.quTmp);
  finally
    fmSort.Free;
  end;
end;

procedure TfmSetting.lpcbxRootCloseUp(Sender: TObject; Accept: Boolean);
begin
   tv1.TopItem.Text := dm.User.SelfCompName;
   Accept := True;
end;

procedure TfmSetting.acFormCoverDepartExecute(Sender: TObject);
begin
  if not(btsDepTypes.State[5] = cbChecked)then raise EWarning.Create('������������� �� �������� �������� �����');
  ShowEditor(TfmeCoverDepart, TfmEditor(fmeCoverDepart));
end;

initialization
  SetLength(SettingTextNodes, 4);
  SettingTextNodes[0] := '�����������';
  SettingTextNodes[1] := '���������';
  SettingTextNodes[2] := '���������';
  SettingTextNodes[PatternPanelIndex] := '�������';

  
finalization
  Finalize(SettingTextNodes);
  
end.
