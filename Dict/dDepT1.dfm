inherited fmDepT1: TfmDepT1
  Left = 139
  Top = 147
  Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082' '#1084#1077#1089#1090' '#1093#1088#1072#1085#1077#1085#1080#1081
  ClientHeight = 297
  ClientWidth = 383
  ExplicitWidth = 391
  ExplicitHeight = 331
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 278
    Width = 383
    ExplicitTop = 278
    ExplicitWidth = 383
  end
  inherited plDict: TPanel
    Width = 383
    Height = 236
    ExplicitWidth = 383
    ExplicitHeight = 236
    object Splitter1: TSplitter [0]
      Left = 177
      Top = 2
      Width = 4
      Height = 191
    end
    inherited plSelect: TPanel
      Top = 193
      Width = 379
      ExplicitTop = 193
      ExplicitWidth = 379
      inherited btnOk: TSpeedButton
        Left = 201
        Top = 8
        ExplicitLeft = 201
        ExplicitTop = 8
      end
      inherited btnCancel: TSpeedButton
        Left = 285
        Top = 7
        Width = 80
        ExplicitLeft = 285
        ExplicitTop = 7
        ExplicitWidth = 80
      end
    end
    inherited gridDict: TDBGridEh
      Width = 175
      Height = 191
      Align = alLeft
      AllowedOperations = []
      AutoFitColWidths = True
      DataSource = dm.dsrRej
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NAME'
          Footers = <>
          Title.Caption = #1055#1088#1080#1095#1080#1085#1072' '#1073#1088#1072#1082#1072
          Width = 146
        end>
    end
    object gridPS: TDBGridEh
      Left = 181
      Top = 2
      Width = 200
      Height = 191
      Align = alClient
      AllowedOperations = []
      AutoFitColWidths = True
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dm.dsrDepart
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      RowDetailPanel.Color = clBtnFace
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      OnGetCellParams = gridPSGetCellParams
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NAME'
          Footers = <>
          Title.Caption = #1042#1080#1085#1086#1074#1085#1080#1082' '#1073#1088#1072#1082#1072
          Width = 138
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  inherited tb1: TSpeedBar
    Width = 383
    ExplicitWidth = 383
  end
  inherited fmstr: TFormStorage
    Active = False
    Left = 36
    Top = 188
  end
end
