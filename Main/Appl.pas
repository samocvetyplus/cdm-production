{*******************************************************}
{  ����� ���������                                      }
{  Copyrigth (C) 2002-2003 basile, blsser for CDM       }
{  v1.35                                                }              
{  last update 19.01.2003                               }
{*******************************************************}
unit Appl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus, RxPlacemnt, ImgList, Grids, DBGrids, RXDBCtrl,
  RxToolEdit, StdCtrls, Mask, DBCtrls, ExtCtrls,
  ComCtrls, db, Buttons, ActnList, ArtImage, DBCtrlsEh,
  DBLookupEh, DBGridEh, FIBDataSet,  TB2Item, InvData,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, cxGridCustomTableView, DBClient, DBGridEhGrouping, GridsEh, rxSpeedbar,
  cxLookAndFeels, cxLookAndFeelPainters, FIBQuery, pFIBQuery, pFIBDataSet;

type

  TfmAppl = class(TfmDocAncestor)
    SpeedButton1: TSpeedButton;
    acEvent: TActionList;
    acDelRecord: TAction;
    acAddRecord: TAction;
    acPrintAppl: TAction;
    acShowWork: TAction;
    spitWhA2: TSplitter;
    plWhA2: TPanel;
    SpeedItem1: TSpeedItem;
    OpenDialog1: TOpenDialog;
    acImportAnaliz: TAction;
    Label2: TLabel;
    acAddSz: TAction;
    acImage: TAction;
    acClose: TAction;
    lbW: TLabel;
    lbQ: TLabel;
    txtQ: TDBText;
    txtW: TDBText;
    acLinkAnaliz: TAction;
    SpeedItem2: TSpeedItem;
    acShowID: TAction;
    plAssort: TPanel;
    Panel2: TPanel;
    gridPEl: TDBGridEh;
    splAssort: TSplitter;
    Splitter2: TSplitter;
    Panel4: TPanel;
    Panel5: TPanel;
    gridRejInv: TDBGridEh;
    Panel3: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Panel6: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    SpeedItem3: TSpeedItem;
    txtPDocNo: TDBText;
    txtPDocDate: TDBText;
    txtRejDocNo: TDBText;
    txtRejDocDate: TDBText;
    acChangeArt: TAction;
    acChangeSz: TAction;
    acShowAssort: TAction;
    taWhWorkOper: TpFIBDataSet;
    taWhWorkOperOPERID: TFIBStringField;
    taWhWorkOperU: TSmallintField;
    taWhWorkOperPSID: TIntegerField;
    taWhWorkOperQ: TIntegerField;
    ppRefresh: TTBPopupMenu;
    TBItem1: TTBItem;
    acRefreshWork: TAction;
    edArt: TDBEditEh;
    Panel1: TPanel;
    Label3: TLabel;
    txtPeriod: TLabel;
    ppDoc: TTBPopupMenu;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem4: TTBItem;
    TBItem6: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem5: TTBItem;
    ppAdd: TTBPopupMenu;
    TBItem8: TTBItem;
    TBItem9: TTBItem;
    TBItem11: TTBItem;
    TBItem12: TTBItem;
    TBItem10: TTBItem;
    TBItem7: TTBItem;
    acRefreshSell: TAction;
    Label1: TLabel;
    txtA1BD: TDBText;
    txtA1ED: TDBText;
    Label4: TLabel;
    TBItem13: TTBItem;
    acAddArtByMask: TAction;
    taArtMask: TpFIBDataSet;
    taArtMaskART2ID: TFIBIntegerField;
    taArtMaskD_ARTID: TFIBIntegerField;
    taArtMaskART: TFIBStringField;
    taArtMaskMW: TFIBFloatField;
    TBItem14: TTBItem;
    tbiPrintReport: TTBItem;
    ButtonArticleStructure: TSpeedItem;
    DataSetControl: TpFIBDataSet;
    DataSetControlARTICLETITLE: TFIBStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1SUBMODELTITLE: TcxGridDBColumn;
    cxGrid1DBTableView1SIZETITLE: TcxGridDBColumn;
    cxGrid1DBTableView1OPERATIONTITLE: TcxGridDBColumn;
    cxGrid1DBTableView1PLACEOFSTORAGETITLE: TcxGridDBColumn;
    cxGrid1DBTableView1QUANTITY: TcxGridDBColumn;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2MODELTITLE: TcxGridDBColumn;
    cxGrid1DBTableView2QUANTITY: TcxGridDBColumn;
    cxGrid1DBTableView2TOTALQUANTITY: TcxGridDBColumn;
    cxGrid1DBTableView2Difference: TcxGridDBColumn;
    Analize: TClientDataSet;
    SpeedItem4: TSpeedItem;
    AnalizeMODEL: TStringField;
    AnalizeMODELSIZE: TStringField;
    AnalizeDEPARTMENTID: TIntegerField;
    AnalizeSELLCOUNT: TIntegerField;
    AnalizeSTORAGECOUNT: TIntegerField;
    taSize: TpFIBDataSet;
    taSizeID: TFIBIntegerField;
    DTEStartEh: TDBDateTimeEditEh;
    DTEFinishEh: TDBDateTimeEditEh;
    DTEReadyEh: TDBDateTimeEditEh;
    lbFirstSemis: TLabel;
    lbLastSemis: TLabel;
    lbReadyProduct: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    taRespPersons: TpFIBDataSet;
    dsRespPersons: TDataSource;
    CBStartEh: TDBLookupComboboxEh;
    CBFinishEh: TDBLookupComboboxEh;
    CBReadyEh: TDBLookupComboboxEh;
    taRespPersonsNAME: TFIBStringField;
    taRespPersonsDEPID: TFIBIntegerField;
    SpeedItem5: TSpeedItem;
    SpeedItem6: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acShowWorkExecute(Sender: TObject);
    procedure acDelRecordExecute(Sender: TObject);
    procedure acAddRecordExecute(Sender: TObject);
    procedure acImportAnalizExecute(Sender: TObject);
    procedure acPrintApplExecute(Sender: TObject);
    procedure acAddSzExecute(Sender: TObject);
    procedure acImageExecute(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acLinkAnalizExecute(Sender: TObject);
    procedure acShowIDExecute(Sender: TObject);
    procedure SetReadOnly(readOnly: boolean);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure ButtonArticleStructureClick(Sender: TObject);
    procedure acChangeArtExecute(Sender: TObject);
    procedure acChangeSzExecute(Sender: TObject);
    procedure acShowAssortExecute(Sender: TObject);
    procedure taWhWorkOperBeforeOpen(DataSet: TDataSet);
    procedure acRefreshWorkExecute(Sender: TObject);
    procedure DBEditEh1Change(Sender: TObject);
    procedure acRefreshSellExecute(Sender: TObject);
    procedure acRefreshSellUpdate(Sender: TObject);
    procedure acAddArtByMaskExecute(Sender: TObject);
    procedure tbiPrintReportClick(Sender: TObject);
    procedure edArtKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edArtEditButtons0Click(Sender: TObject;
      var Handled: Boolean);
    procedure acImportAnalizUpdate(Sender: TObject);
    procedure DataSetControlBeforeOpen(DataSet: TDataSet);
    procedure SpeedItem4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CBStartEhKeyValueChanged(Sender: TObject);
    procedure CBFinishEhKeyValueChanged(Sender: TObject);
    procedure CBReadyEhKeyValueChanged(Sender: TObject);
    procedure DTEStartEhChange(Sender: TObject);
    procedure DTEFinishEhChange(Sender: TObject);
    procedure DTEReadyEhChange(Sender: TObject);
  private
    Import: boolean;
    FShowWh: boolean;
    FChange: boolean;
    FShowArtImage: boolean;
    FCOMPNAME:shortstring; // ��� ����������, ����������� �� �����
    FBDATEANALIZ:TDateTime; // ������ ������� �������, ����������� �� �����
    FEDATEANALIZ:TDateTime;
    FShowAssort: boolean; // ����� ������� �������, ����������� �� �����
    function IsClosed: Boolean;
    procedure SetShowArtImage(const Value: boolean);
    procedure SetShowWh(const Value: boolean);
    procedure SetShowAssort(const Value: boolean);
    procedure OnCalculateAppl(DataSet: TDataSet);
  public
    property ShowWh : boolean read FShowWh write SetShowWh;
    property ShowAssort : boolean read FShowAssort write SetShowAssort;
    property ShowArtImage : boolean read FShowArtImage write SetShowArtImage;
    procedure Import_Analiz(const FileName:string);
    procedure UpdateArt_Sum(const Chunk:TArtChunk);
  end;

implementation

uses ApplData, DictData, AList, dbUtil, DictAncestor, dWhA2,
  PrintData, Editor, eJaSz, eSz, eQ, fmUtils, dArt, MsgDialog,
  ProductionConsts, DateUtils, Period, eJa, frmDialogArticleStructure,
  uUtils, uDialogs, ShlObj;

{$R *.dfm}

procedure TfmAppl.FormCreate(Sender: TObject);
var
  v : Variant;

procedure ClearSortMarks;
var
  i: Integer;
begin
  for i := 0 to gridItem.Columns.Count - 1 do
  gridItem.Columns[i].Title.SortMarker := smNoneEh;
end;

begin
 // if dmAppl.taAppl.RecordCount then

  FDocName := ApplKind;
  FShowAssort := True;
  FShowWh := False;
  ShowAssort := False;
  dm.WorkMode := 'Appl';
  dmAppl.FilterApplArt := '%';
  gridItem.datasource := dmAppl.dsrAppl;
  FChange := false;
  SetReadOnly(dmAppl.taAListISCLOSE.AsInteger = 1);
  edNoDoc.DataSource := dmAppl.dsrAList;
  dtedDateDoc.DataSource := dmAppl.dsrAList;
  inherited;
  if(FDocName = '������')then
  begin
    InitBtn(Boolean(dmAppl.taAListISCLOSE.AsInteger));
    dmAppl.taAppl.Filtered := False;
  end
  else begin
    InitBtn(Boolean(dmAppl.taAListISPROCESS.AsInteger));
    dmAppl.taAppl.Filtered := True;
    gridItem.PopupMenu := nil;
  end;
  taRespPersons.Open;
  dmAppl.taApplResp.Open;
  dmAppl.taApplResp.Edit;
  

  {
  stbrStatus.Panels[0].Text := '������ �������: � '+
     dmAppl.taAList.FieldByName('ABD').AsString+' �� '+
     dmAppl.taAList.FieldByName('AED').AsString;
  }

  txtPeriod.Caption:='c '+DateToStr(dmAppl.taAListABD.AsDateTime)+' �� '+DateToStr(dmAppl.taAListAED.AsDateTime);
  // �������� ������ �� ��������� ������ � ����������
  v := ExecSelectSQL('select InvId from Inv where IType=18 and Ref='+dmAppl.taAListID.AsString, dmAppl.quTmp);
  if VarIsNull(v)then dmAppl.ApplRejInvId := 0 else dmAppl.ApplRejInvId := v;

  v := ExecSelectSQL('select InvId from Inv where IType=19 and Ref='+dmAppl.taAListID.AsString, dmAppl.quTmp);
  if VarIsNull(v) then dmAppl.ApplPInvId := 0 else dmAppl.ApplPInvId := v;

  {v := ExecSelectSQL('select Start$Semis$Resp$Person from Ja_Resp_Persons where invid = ' + dmAppl.taAListID.AsString, dmAppl.quTmp);
  if not VarISNull(v) then
    begin
    CBStartEh.DataField := 'Start$Semis$Resp$Person';
      ShowMessage(dmAppl.taApplResp.FieldByName('Start$Semis$Resp$Person').AsString);
      CBFinishEh.DataField := 'Finish$Semis$Resp$Person';
      ShowMessage(dmAppl.taApplResp.FieldByName('Finish$Semis$Resp$Person').AsString);
      CBReadyEh.DataField := 'Ready$Prod$Resp$Person';
      ShowMessage(dmAppl.taApplResp.FieldByName('Ready$Prod$Resp$Person').AsString);
      DTEStartEh.DataField := 'Start$Semis$Date';
      DTEFinishEh.DataField := 'Finish$Semis$Date';
      DTEReadyEh.DataField := 'Ready$Prod$Date';
      with CBStartEh, CbFinishEh, CBReadyEh, DTEStartEh, DTEFinishEh, DTEReadyEh do
        begin
          Datasource := dmAppl.dsrApplResp;
          ListSource := nil;
          ListField := '';
        end;
    end
  else
    begin
    taRespPersons.Active := true;
    CBStartEh.listField := 'Name';
      CBFinishEh.listField := 'Name';
      CBReadyEh.listField := 'Name';
      with CBStartEh, CbFinishEh, CBReadyEh, DTEStartEh, DTEFinishEh, DTEReadyEh do
        begin
          Datasource := nil;
          DataField := '';
          ListSource := dsRespPersons;
          DropDownBox.ListSource := dsRespPersons;
        end;
      {CBStartEh.listField := 'Name';
      CBFinishEh.listField := 'Name';
      CBReadyEh.listField := 'Name';}
    //end;
    //ppDoc.Skin := dm.ppSkin;
  //ppRefresh.Skin := dm.ppSkin;
  //ppPrint.Skin := dm.ppSkin;
  //ppAdd.Skin := dm.ppSkin;
//  ClearSortMarks;
end;

procedure TfmAppl.FormShow(Sender: TObject);
begin
  inherited;
 Import := false;
end;

procedure TfmAppl.acShowWorkExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_TAILS'; sWhere: '');
begin
  ShowWh := not ShowWh;
 { dm.AArt2Id := dmAppl.taApplART2ID.AsInteger;
  dm.ASzId := dmAppl.taApplSZ.AsInteger;
  ShowDictForm(TfmWhA2, dmAppl.dsrWhA2, Rec, '��������� ������������');}
end;

procedure TfmAppl.SetShowArtImage(const Value: boolean);
begin
  FShowArtImage := Value;
end;

procedure TfmAppl.SetShowWh(const Value: boolean);  //������ ���� ��� "� ������"
begin
  if FShowWh = Value then eXit;
  FShowWh := Value;
  if Value then
  begin
    plWhA2.Visible := True;
    spitWhA2.Visible := True;
    dm.AArt2Id := dmAppl.taApplART2ID.AsInteger;
    dm.ASzId := dmAppl.taApplSZ.AsInteger;
    ReOpenDataSet(dmAppl.taWhA2);
    ReOpenDataSet(dmAppl.taWhA2d);
  end
  else
    begin
      spitWhA2.Visible := False;
      plWhA2.Visible := False;
      CloseDataSet(dmAppl.taWhA2);
      CloseDataSet(dmAppl.taWhA2d);
    end;
end;

procedure TfmAppl.acDelRecordExecute(Sender: TObject);
begin
  dmAppl.taAppl.Delete;
end;

procedure TfmAppl.acAddRecordExecute(Sender: TObject);
begin
  dmAppl.taAppl.Insert;
end;

Procedure TfmAppl.UpdateArt_Sum(const Chunk:TArtChunk);
begin
  with dmInv do
  try
   sqlUpdateARTSum.ParamByName('APPLID').AsInteger:=dmAppl.taAListID.AsInteger;
   sqlUpdateARTSum.ParamByName('ART').AsString:=Chunk.ART;
   sqlUpdateARTSum.ParamByName('SZ').AsString:=Chunk.SZ;
   sqlUpdateARTSum.ParamByName('INW').AsFloat:=Chunk.INW;
   sqlUpdateARTSum.ParamByName('INQ').AsInteger:=Chunk.INQ;
   sqlUpdateARTSum.ParamByName('OUTW').AsFloat:=Chunk.OUTW;
   sqlUpdateARTSum.ParamByName('OUTQ').AsInteger:=Chunk.OUTQ;
   sqlUpdateARTSum.ParamByName('RETW').AsFloat:=Chunk.RETW;
   sqlUpdateARTSum.ParamByName('RETQ').AsInteger:=Chunk.RETQ;
   sqlUpdateARTSum.ParamByName('SALEW').AsFloat:=Chunk.SALEW;
   sqlUpdateARTSum.ParamByName('SALEQ').AsInteger:=Chunk.SALEQ;
   sqlUpdateARTSum.ParamByName('DEBTORW').AsFloat:=Chunk.DEBTORW;
   sqlUpdateARTSum.ParamByName('DEBTORQ').AsInteger:=Chunk.DEBTORQ;
   sqlUpdateARTSum.ParamByName('STOREW').AsFloat:=Chunk.STOREW;
   sqlUpdateARTSum.ParamByName('STOREQ').AsInteger:=Chunk.STOREQ;
   sqlUpdateARTSum.ExecQuery;
//   sqlUpdateARTSum.Transaction.CommitRetaining;
  except
    on E:Exception do ShowMessage(e.Message+#13+'�������='+Chunk.ART+#13+sqlUpdateARTSum.SQL.Text);
  end;
end;


Procedure TfmAppl.Import_Analiz(const FileName:string);
var f: Textfile;
    str: string;
    Chunk: TARTChunk;
begin

 try
   AssignFile(f,FIleName);
   Reset(f);
   readln(F,str);
   FCOMPNAME:=str;
   readln(F,str);
   readln(F,str);
   FBDATEANALIZ:=StrToDate(Str);
   readln(F,str);
   FEDATEANALIZ:=StrToDate(Str);
   txtPeriod.Caption:='c '+DateToStr(FBDATEANALIZ)+' �� '+DateToStr(FEDATEANALIZ);
   while not eof(f) do
   begin
    Readln(f,str);
    try
     Chunk:=dmInv.STR_TO_Chunk(str);

    except
      MessageDialog('������ ��� UPDATE_CHUNK'+ str, mtError, [mbOk], 0);
    end;
    try
      UpdateArt_Sum(Chunk);
      dmInv.sqlUpdateARTSum.Transaction.CommitRetaining;
    except
     MessageDialog('������ ��� UPDATE_ART_SUM'+ str,mtError,[mbOk],0);
     dmInv.sqlUpdateARTSum.Transaction.Rollback;
    end;
   end;

  finally
    CLoseFile(f);
  end;
  dmAppl.taAList.Edit; // ��������� ������ �������
  dmAppl.taAListABD.AsDateTime:=FBDATEANALIZ;
  dmAppl.taAListAED.AsDateTime:=FEDATEANALIZ;
  dmAppl.taAList.Post;
  dmAppl.taAList.Transaction.CommitRetaining;

end;

procedure TfmAppl.acImportAnalizExecute(Sender: TObject);
begin
  inherited;
  Screen.Cursor:=crSQLWait;
  dmINV.sqlCreateAnaliz.ParamByName('APPLID').AsInteger:=dmAppl.taAListID.AsInteger;
  dmINV.sqlCreateAnaliz.ExecQuery;
  dmINV.sqlCreateAnaliz.Transaction.CommitRetaining;
  ReOpenDataSet(dmAppl.taAppl);
  Screen.Cursor := crDefault;
  SpeedItem1.Enabled := false;
end;

procedure TfmAppl.acPrintApplExecute(Sender: TObject);
begin

if dmAppl.taApplResp.State in [dsEdit, dsInsert] then
  begin
    dmAppl.taApplRespINVID.AsInteger := dmAppl.taAListID.AsInteger;
    dmAppl.taApplResp.Post;
    dmAppl.taApplResp.Transaction.CommitRetaining;
  end;

  dmPrint.taProdApplResp.Open;
  PostDataSet(dmAppl.taAppl);
   dmPrint.PrintDocumentA(dkProdAppl,
   VarArrayOf([
     VarArrayOf([dmAppl.taAListID.AsInteger]),
     VarArrayOf([dmAppl.taAListID.AsInteger]),
     VarArrayOf([dmAppl.taAListID.AsInteger])
     ]));
   dmPrint.taProdApplResp.Close;
end;

procedure TfmAppl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if ((AList.FCreated or FChange) and (dmAppl.taApplResp.State in [dsEdit, dsInsert])) then
      with dmAppl.taApplResp do
        begin
          Post;
          Transaction.CommitRetaining;
        end;

  dmAppl.taAList.Close;
  dmAppl.taAList.Open;

  dmAppl.taApplResp.Close;
  taRespPersons.Close;

  inherited;
  if Assigned(dmAppl.FArtImage) then
  begin
    dmAppl.FArtImage.Close;
    dmAppl.FArtImage := nil;
  end;
  dmAppl.OnCalculateAppl := nil;
end;

procedure TfmAppl.acAddSzExecute(Sender: TObject);
var
  ArtId : integer;
  Art2Id : integer;
  Art : string;
  Mw : double;
  SizeExist: Variant;
begin
  PostDataSet(dmAppl.taAppl);
  if(dmAppl.taAppl.IsEmpty)or(dmAppl.taApplARTID.IsNull) then eXit;
  if ShowEditor(TfmJaSzEditor, TfmEditor(fmJaSzEditor))= mrOk then
  begin
    ArtId := dmAppl.taApplARTID.AsInteger;
    Art := dmAppl.taApplART.AsString;
    Art2Id := dmAppl.taApplART2ID.AsInteger;
    MW := dmAppl.taApplMW.AsFloat;
    dmAppl.taAppl.Insert;
    dmAppl.taApplART.AsString := Art;
    dmAppl.taApplARTID.AsInteger := ArtId;
    dmAppl.taApplART2ID.AsInteger := Art2Id;
    dmAppl.taApplSZ.AsInteger := Editor.vResult;
    dmAppl.taApplSZNAME.AsString := ExecSelectSQL('select Name from D_Sz where Id='+
      IntToStr(Editor.vResult), dmAppl.quTmp);
    dmAppl.taApplMW.AsFloat := MW;
    dmAppl.taAppl.Post;
    SizeExist := ExecSelectSQL('select Result from ARTICLE$STRUCTURE$DEFINED(' + IntToStr(ArtId) + ', Null)', dmAppl.quTmp);
    if SizeExist <> 0 then
    begin
      SizeExist := ExecSelectSQL('select Result from ARTICLE$STRUCTURE$DEFINED(' + IntToStr(ArtId) + ', ' + IntToStr(dmAppl.taApplSZ.AsInteger) + ')', dmAppl.quTmp);
      if SizeExist = 0 then
      begin
        InformationMessage('�� ��������� ������ �������� ��� ������� �������.');
        ButtonArticleStructure.Click;
      end;
    end;
  end;
end;

procedure TfmAppl.acImageExecute(Sender: TObject);
begin
  if Assigned(dmAppl.FArtImage) then
  begin
    dmAppl.FArtImage.Close;
    dmAppl.FArtImage := nil;
  end
  else begin
    dmAppl.FArtImage := TfmImage.Create(Self);
    dmAppl.FArtImage.Caption := '�������: '+dmAppl.taApplART.AsString;
    dmAppl.FArtImage.Show;
  end;
  acImage.Checked := Assigned(dmAppl.FArtImage);
end;

procedure TfmAppl.N8Click(Sender: TObject);
begin
  (Sender as TMenuItem).Checked := not (Sender as TMenuItem).Checked;
  ShowWh := (Sender as TMenuItem).Checked;
  if ShowWh then
  begin
    ReOpenDataSet(dmAppl.taWhA2);
    ReOpenDataSet(dmAppl.taWhA2d);
  end
end;

procedure TfmAppl.acCloseExecute(Sender: TObject);
var
  i, c: Integer;
  Msg: string;
begin

  PostDataSets([DataSet, MasterDataSet]);
  if(FDocName = '������')then
  begin
    if(spitClose.BtnCaption = '������')then
    begin

    Msg := '';
    DataSetControl.Active := True;
    if DataSetControl.RecordCount <> 0 then
    begin
      c := 32;
      DataSetControl.First;
      while not DataSetControl.Eof do
      begin
        if c = 0 then
        begin
          c := -1;
          break;
        end;
        if Msg = '' then Msg := DataSetControlARTICLETITLE.AsString
        else  Msg := Msg + ','#13#10 + DataSetControlARTICLETITLE.AsString;
        Dec(c);
        DataSetControl.Next;
      end;
      Msg := '�� ��������� ������ ��� ��������� ��������� :'#13#10 + Msg;
      if c = -1 then Msg := Msg + ','#13#10'...'
      else Msg := Msg + '.';
    end;
    DataSetControl.Active := True;
    SetReadOnly(True);
    if Msg = '' then inherited spitCloseClick(Sender)
    else DialogErrorOkMessage(Msg);

    end
    else
      begin
        inherited spitCloseClick(Sender);
        SetReadOnly(False);
      end;
    PostDatasets([Dataset, MasterDataset]);
    RefreshDatasets([Dataset, MasterDataset]);
  end
  else
    if(spitClose.BtnCaption = '������')then
      try
        MasterDataSet.Edit;
        MasterDataSet.FieldByName('ISPROCESS').AsInteger := 1;
        MasterDataSet.Post;
        SetReadOnly(true);
        if dmAppl.taApplResp.State in [dsEdit, dsInsert] then dmAppl.taApplResp.Post;
        spitClose.BtnCaption := '������';
        spitClose.Hint := '������� ��������';
        spitClose.ImageIndex := 5;
        RefreshDataSets([MasterDataSet, DataSet]);
      except
        on E: Exception do
        { TODO : !!! }
          //if not TranslateIbMsg(E, DataSet.Database, DataSet.Transaction)
             //then
             ShowMessage(E.Message);
      end
    else
      try
        MasterDataSet.Edit;
        MasterDataSet.FieldByName('ISPROCESS').AsInteger := 0;
        MasterDataSet.Post;
        spitClose.BtnCaption := '������';
        setReadOnly(False);
        dmAppl.taApplResp.Edit;
        spitClose.Hint := '������� ��������';
        spitClose.ImageIndex := 6;
        RefreshDataSets([MasterDataSet, DataSet]);
      except
        on E: Exception do
          { TODO : !!! }
          //if not TranslateIbMsg(E, DataSet.Database, DataSet.Transaction)
             //then
             ShowMessage(E.Message);
      end
end;

procedure TfmAppl.SetReadOnly(readOnly: boolean);
begin
  cbStartEh.Enabled := not readOnly;
  cbFinishEh.Enabled := not readOnly;
  cbReadyEh.Enabled := not readOnly;
  dteStartEh.Enabled := not readOnly;
  dteFinishEh.Enabled := not readOnly;
  dteReadyEh.Enabled := not readOnly;
end;

procedure TfmAppl.acLinkAnalizExecute(Sender: TObject);
begin
  inherited;
  If OpenDialog1.Execute then
  begin
   Import_Analiz(OpenDialog1.FileName);
   ReOpenDataSet(dmAppl.taAppl);
  end else MessageDialog('������ �� ��������!',mtWarning,[mbOk],0);
end;

procedure TfmAppl.acShowIDExecute(Sender: TObject);
begin
  gridItem.FieldColumns['FQ'].Visible := not gridItem.FieldColumns['FQ'].Visible;
  gridItem.FieldColumns['ARTID'].Visible := not gridItem.FieldColumns['ARTID'].Visible;
  gridItem.FieldColumns['SZ'].Visible := not gridItem.FieldColumns['SZ'].Visible;
  gridItem.FieldColumns['ID'].Visible := not gridItem.FieldColumns['ID'].Visible;
  gridItem.FieldColumns['ARTID'].Visible := not gridItem.FieldColumns['ARTID'].Visible;
end;

procedure TfmAppl.gridItemGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if (Column.Field <> nil) and (Column.Field.Tag <> 0) then Background := clBtnFace;
  if(Column.FieldName = 'R')or(Column.FieldName = 'MODEL') or
    (Column.FieldName = 'RecNo') then Background := clMoneygreen;
  if(Column.FieldName = 'MW')and(Column.Field.AsFloat=0) then Background:=clWhite;
  if(Column.FieldName = 'Q') then Background:=clReturn;
  if(Column.FieldName = 'INQ')or(Column.FieldName = 'INW') then Background:=clIn;
  if(Column.FieldName = 'DEBTORQ')or(Column.FieldName = 'DEBTORW') then Background:=clDebtor;
  if(Column.FieldName = 'SALEQ')or(Column.FieldName = 'SALEW') then Background:=clSell;
  if(Column.FieldName = 'OUTQ')or(Column.FieldName = 'OUTW') then Background:=clOut;
  if(Column.FieldName = 'RETQ')or(Column.FieldName = 'RETW') then Background:=clReturn;
  if((Column.FieldName = 'INQ')or(Column.FieldName = 'DEBTORQ')or(Column.FieldName = 'SALEQ')or(Column.FieldName = 'OUTQ')or(Column.FieldName = 'RETQ')or(Column.FieldName = 'STOREQ'))
     and(Column.Field.AsFloat = 0) then Background:=clWhite;
  if((Column.FieldName = 'INW')or(Column.FieldName = 'DEBTORW')or(Column.FieldName = 'SALEW')or(Column.FieldName = 'OUTW')or(Column.FieldName = 'RETW')or(Column.FieldName = 'STOREW'))
     and(Column.Field.AsFloat = 0) then Background:=clWhite;
  if(Column.FieldName = 'ART')and(dmAppl.taApplARTROOT.AsInteger=1)then Background:=clFuchsia;
end;

procedure TfmAppl.SetShowAssort(const Value: boolean);
begin
  if(FShowAssort = Value)then eXit;
  if Value then
  begin
    plAssort.Visible := True;
    splAssort.Visible := True;
    OpenDataSets([dmAppl.taApplPInv, dmAppl.taApplPEl, dmAppl.taApplRejInv,
      dmAppl.taApplRejEl]);
  end
  else begin
    plAssort.Visible := False;
    splAssort.Visible := False;
    CloseDataSets([dmAppl.taApplPInv, dmAppl.taApplPEl, dmAppl.taApplRejInv,
      dmAppl.taApplRejEl]);
  end;
  FShowAssort := Value;
end;

procedure TfmAppl.ButtonArticleStructureClick(Sender: TObject);
var
  Article: string;
  Size: string;
begin
  Article := Trim(dmAppl.taApplART.AsString);
  Size := Trim(dmAppl.taApplSZNAME.AsString);
  TDialogArticleStructure.Execute(Article, Size);
end;


procedure TfmAppl.CBFinishEhKeyValueChanged(Sender: TObject);
begin
  inherited;
  FChange := true;
end;

procedure TfmAppl.CBReadyEhKeyValueChanged(Sender: TObject);
begin
  inherited;
  FChange := true;
end;

procedure TfmAppl.CBStartEhKeyValueChanged(Sender: TObject);
begin
  inherited;
  FChange := true;
end;

procedure TfmAppl.acChangeArtExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ARTID'; sName: 'ART'; sTable: 'D_ART'; sWhere: '');
var
  Art2Id, NewArt2Id : Variant;
  NewArtId,  NewSzId, NewU : integer;
  NewOperId : string;
  q : integer;
begin
  with dmAppl do
  begin
    // �������� ���� �� ������ ������� � ������ � ��������
    Art2Id := ExecSelectSQL('select Art2Id from Art2 where D_ArtId='+taApplARTID.AsString+
      ' and Art2="-"', quTmp);
    if VarIsNull(Art2Id)or(Art2Id = 0) then raise EInternal.Create(Format(rsInternalError, ['001']));

    q := ExecSelectSQL('select count(*) '+
      'from WhAppl wh, D_Dep d '+
      'where wh.Art2Id='+IntToStr(Art2Id)+' and '+
      '      wh.Sz='+taApplSZ.AsString+' and '+
      '      wh.PsId=d.DepId and '+
      '      wh.Q<>0 and '+
      '      band(d.DepTypes, 2)= 2', quTmp);
    if(q = 0)then EWarning.Create('� �������� ��� ������� �������� � �������!');

    // ����� ��������
    OpenDataSet(taWhWorkOper);
    taWhWorkOper.Last;
    case taWhWorkOper.RecordCount of
      0 : raise EInternal.Create(Format(rsInternalError, ['301']));
      1 : begin
        NewOperId := taWhWorkOperOPERID.AsString;
        NewU := taWhWorkOperU.AsInteger;
        ADistrQ := taWhWorkOperQ.AsInteger;
      end
      else { TODO : DoIt }
        raise Exception.Create(rsNotImplementation);// �������� ��������
    end;

    // ����� ��������
    dArt.FindArtId := taApplARTID.AsInteger;
    dArt.FindArt := taApplART.AsString;
    if ShowDictForm(TfmArt, dm.dsrArt, Rec, '���������� ���������',  dmSelRecordEdit, nil)<>mrOk then eXit;
    NewArtId := vResult;
    NewArt2Id := ExecSelectSQL('select Art2Id from Art2 where D_ArtId='+IntToStr(NewArtId)+' and Art2="-"', quTmp);
    if VarIsNull(NewArt2Id)or(NewArt2Id=0) then raise EInternal.Create(Format(rsInternalError, ['001']));
    NewSzId := taApplSZ.AsInteger;
    if(ShowEditor(TfmeQ, TfmEditor(fmeQ)) <> mrOk)then eXit;
    (* ����� � ������ ������� WhApplArt2Id *)
    {if((ApplRejInvId = 0) or taApplRejInv.IsEmpty) then
    begin
      taApplRejInv.Append;
      taApplRejInv.Post;
      ReOpenDataSets([taApplRejInv]);
    end;
    taApplRejEl.Append;
    try
      taApplRejElQ.AsInteger := dmAppl.ADistrQ;
      taApplRejElARTID.AsInteger := dmAppl.taWHApplARTID.AsInteger;
      taApplRejElOPERID.AsString := dmAppl.taWHApplOPERID.AsString;
      taApplRejElSZID.AsInteger := dmAppl.taWHApplSZID. AsInteger;
      taApplRejElART2ID.AsInteger := dmAppl.taWHApplART2ID.AsInteger;
      taApplRejElCOMMENTID.AsInteger := 2;
      taApplRejEl.Post;
    except
      on E : Exception do HandlePost(E);
    end;
    (* �������� � ������ ������� *)
    if(dmAppl.taPInv.IsEmpty or (dmAppl.PInvid = 0)) then
    begin
        plPInv.Visible := True;
        dmAppl.taPInv.Append;
        dmAppl.taPInv.Post;
        ReOpenDataSets([dmAppl.taPInv]);
      end;
      dmAppl.taPEl.Append;
      try
        dmAppl.taPElQ.AsInteger := dmAppl.ADistrQ;
        dmAppl.taPElARTID.AsInteger := NewArtId;
        dmAppl.taPElOPERID.AsString := NewOperId;
        dmAppl.taPElSZID.AsInteger := NewSzId;
        dmAppl.taPElART2ID.AsInteger := NewArt2Id;
        dmAppl.taPElU.AsInteger := NewU;
        dmAppl.taPElCOMMENTID.AsInteger := 2;
        dmAppl.taPEl.Post;
      except
        on E : Exception do HandlePost(E);
      end;
      with dmAppl do
        if not taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([NewArt2Id, NewSzId, NewOperId]), [])
        then InsertWhRecord(taPEl, taPElOPERID, taPElOPERATION, 0, taPElARTID,
           taPElART2ID, taPElART2, taPElU)
        else taWHAppl.Refresh;
    end;}
  end;
end;

procedure TfmAppl.acChangeSzExecute(Sender: TObject);
{const
  Rec : TRecName = (sId: 'ARTID'; sName: 'ART'; sTable: 'D_ART'; sWhere: '');
var
  NewArtId, NewArt2Id, NewSzId, NewU : integer;
  NewOperId : string;}
begin
  {if ShowEditor(TfmeSz, TfmEditor(fmeSz))=mrOk then
  begin
    if(ResultSz = dmAppl.taWHApplSZID.AsInteger)  then
      raise Exception.Create('������ �� �������!');
    NewArtId := dmAppl.taWHApplARTID.AsInteger;
    NewArt2Id := dmAppl.taWHApplART2ID.AsInteger;
    NewSzId := ResultSz;
    NewOperId := dmAppl.taWHApplOPERID.AsString;
    NewU := dmAppl.taWHApplU.AsInteger;
    dmAppl.ADistrQ := dmAppl.taWHApplQ.AsInteger;
    if(ShowEditor(TfmeQ, TfmEditor(fmeQ)) = mrOk)then
    begin
      (* ����� � ������ ������� WhApplArt2Id *)
      if((dmAppl.RInvId = 0) or dmAppl.taRejInv.IsEmpty) then
      begin
        plRej.Visible := True;
        dmAppl.taRejInv.Append;
        dmAppl.taRejInv.Post;
        dmAppl.taRejInv.Transaction.CommitRetaining;
        ReOpenDataSets([dmAppl.taRejInv]);
      end;
      dmAppl.taRejEl.Append;
      try
        dmAppl.taRejElQ.AsInteger := dmAppl.ADistrQ;
        dmAppl.taRejElARTID.AsInteger := dmAppl.taWHApplARTID.AsInteger;
        dmAppl.taRejElOPERID.AsString := dmAppl.taWHApplOPERID.AsString;
        dmAppl.taRejElSZID.AsInteger := dmAppl.taWHApplSZID. AsInteger;
        dmAppl.taRejElART2ID.AsInteger := dmAppl.taWHApplART2ID.AsInteger;
        dmAppl.taRejElCOMMENTID.AsInteger := 1;
        dmAppl.taRejEl.Post;
      except
        on E : Exception do HandlePost(E);
      end;
      dmAppl.taWHAppl.Refresh;
      (* �������� � ������ ������� *)
      if(dmAppl.taPInv.IsEmpty or (dmAppl.PInvid = 0)) then
      begin
        plPInv.Visible := True;
        dmAppl.taPInv.Append;
        dmAppl.taPInv.Post;
        ReOpenDataSets([dmAppl.taPInv]);
      end;
      dmAppl.taPEl.Append;
      try
        dmAppl.taPElQ.AsInteger := dmAppl.ADistrQ;
        dmAppl.taPElARTID.AsInteger := NewArtId;
        dmAppl.taPElOPERID.AsString := NewOperId;
        dmAppl.taPElSZID.AsInteger := NewSzId;
        dmAppl.taPElART2ID.AsInteger := NewArt2Id;
        dmAppl.taPElU.AsInteger := NewU;
        dmAppl.taPElCOMMENTID.AsInteger := 1;
        dmAppl.taPEl.Post;
      except
        on E : Exception do HandlePost(E);
      end;
      with dmAppl do
        if not taWHAppl.Locate('ART2ID;SZID;OPERID', VarArrayOf([NewArt2Id, NewSzId, NewOperId]), [])
        then InsertWhRecord(taPEl, taPElOPERID, taPElOPERATION, 0, taPElARTID,
           taPElART2ID, taPElART2, taPElU)
        else taWHAppl.Refresh;
    end;
  end;}
end;

procedure TfmAppl.acShowAssortExecute(Sender: TObject);
begin
  ShowAssort := not ShowAssort;
  acShowAssort.Checked := ShowAssort;
end;

procedure TfmAppl.taWhWorkOperBeforeOpen(DataSet: TDataSet);
var
  Art2Id : integer;
begin
  Art2Id := ExecSelectSQL('select Art2Id from Art2 where D_ArtId='+dmAppl.taApplARTID.AsString+
      ' and Art2="-"', dmAppl.quTmp);
  if(Art2Id = 0) then raise EInternal.Create(Format(rsInternalError, ['300']));
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['ART2ID'].AsInteger := Art2Id;
    ByName['SZ'].AsInteger := dmAppl.taApplSZ.AsInteger;
  end;
end;

procedure TfmAppl.acRefreshWorkExecute(Sender: TObject);
begin
  raise Exception.Create(rsNotImplementation);
end;

procedure TfmAppl.DBEditEh1Change(Sender: TObject);
begin
  dmAppl.FilterApplArt := edArt.Text+'%';
  //dmAppl.taAppl.ParamByName('ART_').AsString:=
  ReOpenDataSet(dmAppl.taAppl);
end;

procedure TfmAppl.DTEFinishEhChange(Sender: TObject);
begin
  inherited;
  FChange := true;
end;

procedure TfmAppl.DTEReadyEhChange(Sender: TObject);
begin
  inherited;
  FChange := true;
end;

procedure TfmAppl.DTEStartEhChange(Sender: TObject);
begin
  inherited;
  FChange := true;
end;

procedure TfmAppl.acRefreshSellExecute(Sender: TObject);
var
  Bd, Ed : TDateTime;
begin
  PostDataSet(dmAppl.taAList);
  if dmAppl.taAListA1BD.IsNull then Bd := StartOfTheDay(IncMonth(Today, -6))
  else Bd := dmAppl.taAListA1BD.AsDateTime;
  if dmAppl.taAListA1ED.IsNull then Ed := EndOfTheDay(Today)
  else Ed := dmAppl.taAListA1ED.AsDateTime;
  if ShowPeriodForm(BD, ED) then
  begin
    Application.ProcessMessages;
    ExecSQL('execute procedure Appl_U_Sell('+dmAppl.taAListID.AsString+','+
      '"'+DateTimeToStr(Bd)+'","'+DateTimeToStr(Ed)+'")', dm.quTmp);
    dmAppl.taAList.Edit;
    dmAppl.taAListA1BD.AsDateTime := Bd;
    dmAppl.taAListA1ED.AsDateTime := Ed;
    dmAppl.taAList.Post;
    Application.ProcessMessages;
    ReOpenDataSets([DataSet]);
    Application.ProcessMessages;
  end;
end;

procedure TfmAppl.acRefreshSellUpdate(Sender: TObject);
begin
  acRefreshSell.Enabled := dmAppl.taAList.Active and dmAppl.taAppl.Active;
end;

procedure TfmAppl.acAddArtByMaskExecute(Sender: TObject);
var
  Msg, Art : string;
  r : Variant;
  i : integer;
  SaveEvent :  TDataSetNotifyEvent;
begin
  PostDataSet(dmAppl.taAppl);
  if not Import then
    if ShowEditor(TfmJaEditor, TfmEditor(fmJaEditor)) <> mrOk
      then eXit;
  CloseDataSet(taArtMask);
  if not Import then
    taArtMask.ParamByName('ART').AsString := Editor.vResult+'%'
  else
    taArtMask.ParamByName('ART').AsString := '%';

  OpenDataSet(taArtMask);
  if taArtMask.IsEmpty then
    begin
      CloseDataSet(taArtMask);
      if not Import then
        MessageDialog('��������� ����� �� ������������� �� ������ ��������', mtInformation, [mbOk], 0);
    end;

  if not Import then
    begin
      i := 0;
      taArtMask.Last;
      Msg := '� ������ ����� ��������� �������� �� ����� ' + Editor.vResult +
      '__'#13+Msg;
      if MessageDialog(Msg, mtConfirmation, [mbYes, mbNo], 0)=mrNo then
        begin
          CloseDataSet(taArtMask);
          eXit;
        end;
    end;
   i := 0;
  taArtMask.First;
  SaveEvent := dmAppl.taAppl.AfterPost;
  try
     dmAppl.taAppl.AfterPost := nil;
     while not taArtMask.Eof do
     begin
       r := ExecSelectSQL('select count(*) from JA where ApplId='+dmAppl.taAListID.AsString+' and Art2Id='+taArtMaskART2ID.AsString, dmAppl.quTmp);
       if ((r=0) and (length(trim(taArtMaskART.AsString)) < 8)) then
         begin

           //Art := '';
           Art := taArtMaskART.AsString;

           taSize.Close;
           case Art[1] of
             '1' : taSize.SelectSQL.Text := 'select id from D_SZ where id between -3 and 15 and id <> -1';
             '4' : taSize.SelectSQL.Text := 'select id, name from D_SZ where id between 1 and 13 and (id - (id/2)*2 = 1)';
             '5' : taSize.SelectSql.Text := 'select id from D_SZ where id between 96 and 114 and id <> 100';
           else taSize.SelectSql.Text := '';
         end;

         if taSize.SelectSQL.Text <> ''
           then
             begin
               taSize.Open;
               taSize.FieldDefs.Update;
               taSize.First;
               while not taSize.Eof do
                 begin
                   dmAppl.taAppl.Insert;
                   dmAppl.taApplART.AsString := taArtMaskART.AsString;
                   dmAppl.taApplARTID.AsInteger := taArtMaskD_ARTID.AsInteger;
                   dmAppl.taApplART2ID.AsInteger := taArtMaskART2ID.AsInteger;
                   dmAppl.taApplMW.AsFloat := taArtMaskMW.AsFloat;
                   dmAppl.taApplSZ.AsInteger := taSizeID.AsInteger;
                   dmAppl.taAppl.Post;
                   taSize.Next;
                 end;
             end
         else
           begin
             dmAppl.taAppl.Insert;
             dmAppl.taApplART.AsString := taArtMaskART.AsString;
             dmAppl.taApplARTID.AsInteger := taArtMaskD_ARTID.AsInteger;
             dmAppl.taApplART2ID.AsInteger := taArtMaskART2ID.AsInteger;
             dmAppl.taApplMW.AsFloat := taArtMaskMW.AsFloat;
             dmAppl.taApplSZ.AsInteger := -1;
             dmAppl.taAppl.Post;
           end;

         dmAppl.taAppl.Refresh;
         inc(i);
       end;
       taArtMask.Next;
     end;
     //ShowMessage('While �� ArtMask ������!');
  finally
    dmAppl.taAppl.AfterPost := SaveEvent;
    //ShowMessage('AfterPost ������!');
  end;
  ReOpenDataSet(dmAppl.taAppl);
  CloseDataSet(taArtMask);
  if Import then
    begin
      Msg := IntToStr(i) + ' �������(��) ������� ��������(�) � ������!';
      MessageDlg(Msg, mtInformation, [mbOk], 0);
    end;
end;

procedure TfmAppl.tbiPrintReportClick(Sender: TObject);
begin
 PostDataSet(dmAppl.taAppl);
 //ShowMessage('PrintReportClick: '+dmAppl.taAppl.FieldByName('applid').AsString);   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  dmPrint.PrintDocumentA(dkOrderReport, VarArrayOf([VarArrayOf([dmAppl.taAListID.AsInteger])]));
end;

procedure TfmAppl.edArtKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = VK_RETURN then
  begin
    Screen.Cursor := crHourGlass;
    dmAppl.FilterApplArt := edArt.Text+'%';
    try
      ReOpenDataSet(dmAppl.taAppl);
    finally
      Screen.Cursor := crDefault;
      //ShowMessage('Slave, edArtKeyDown:'+dmAppl.taAppl.FieldByName('ApplID').AsString);//!!!!!!!!!!!!!!!!!!!!!!
      //ShowMessage('Master, edArtKeyDown:'+dmAppl.taAList.FieldByName('ID').AsString);
    end;
  end;
end;

procedure TfmAppl.edArtEditButtons0Click(Sender: TObject; var Handled: Boolean);
begin
  Screen.Cursor := crHourGlass;
  edArt.Text := '';
  dmAppl.FilterApplArt := '%';
  try
    ReOpenDataSet(dmAppl.taAppl);
  finally
    Screen.Cursor := crDefault;
  end;
  Handled := True;
end;

function TfmAppl.IsClosed: Boolean;
begin
  Result := dmAppl.taAListISCLOSE.AsInteger = 1;
end;

procedure TfmAppl.acImportAnalizUpdate(Sender: TObject);
begin
  acImportAnaliz.Enabled := not IsClosed;
  edNoDoc.ReadOnly := IsClosed;
  dtedDateDoc.ReadOnly := IsClosed;
end;

procedure TfmAppl.DataSetControlBeforeOpen(DataSet: TDataSet);
begin
  DataSetControl.ParamByName('Application$ID').AsInteger := dmAppl.taAListID.AsInteger;
end;

procedure TfmAppl.SpeedItem4Click(Sender: TObject);
var
  FileName: string;

procedure Prepare;
var
  i: Integer;
  Column: TColumnEh;
  DepartmentCount: Integer;
  DepartmentID: Integer;
  DepartmentName: string;
  Field: TField;
  DataSet: TDataSet;


procedure CreateColumns;
begin
    DepartmentID := Analize.GetOptionalParam('Department$ID$' + IntToStr(i));
    DepartmentName := Analize.GetOptionalParam('Department$Name$' + IntToStr(i));

    Field := TFIBFloatField.Create(Self);
    Field.Tag := DepartmentID;
    Field.FieldName := 'sell$' + IntToStr(DepartmentID);
    Field.FieldKind := fkCalculated;
    Field.DataSet := DataSet;

    Column := gridItem.Columns.Add;
    Column.Tag := MaxInt;
    Column.Title.Caption := '��� "��������� ����"|' + DepartmentName + '|�������';
    Column.FieldName := Field.FieldName;
    Column.Footer.FieldName := Column.FieldName;
    Column.Footer.ValueType := fvtSum;


    Field := TFIBFloatField.Create(Self);
    Field.Tag := DepartmentID;
    Field.FieldName := 'storage$' +  IntToStr(DepartmentID);
    Field.FieldKind := fkCalculated;
    Field.DataSet := DataSet;


    Column := gridItem.Columns.Add;
    Column.Tag := MaxInt;
    Column.Title.Caption := '��� "��������� ����"|' + DepartmentName + '|�� ������';
    Column.FieldName := Field.FieldName;
    Column.Footer.FieldName := Column.FieldName;
    Column.Footer.ValueType := fvtSum;
end;

begin
  DataSet := dmAppl.taAppl;

  for i := GridItem.Columns.Count - 1 downto 0 do
  begin
    Column := GridItem.Columns[i];
    if Column.Tag = MaxInt then
    gridItem.Columns.Delete(i);
  end;

  for i := DataSet.Fields.Count - 1 downto 0 do
  begin
    Field := DataSet.Fields[i];
    if Field.Tag <> 0 then Field.Free;
  end;

  Field := TFIBFloatField.Create(Self);
  Field.Tag := MaxInt;
  Field.FieldName := 'sell$total';
  Field.FieldKind := fkCalculated;
  Field.DataSet := DataSet;

  Column := gridItem.Columns.Add;
  Column.Tag := MaxInt;
  Column.Title.Caption := '��� "��������� ����"|�����|�������';
  Column.FieldName := Field.FieldName;
  Column.Footer.FieldName := Column.FieldName;
  Column.Footer.ValueType := fvtSum;

  Field := TFIBFloatField.Create(Self);
  Field.Tag := MaxInt;
  Field.FieldName := 'storage$total';
  Field.FieldKind := fkCalculated;
  Field.DataSet := DataSet;

  Column := gridItem.Columns.Add;
  Column.Tag := MaxInt;
  Column.Title.Caption := '��� "��������� ����"|�����|�� ������';
  Column.FieldName := Field.FieldName;
  Column.Footer.FieldName := Column.FieldName;
  Column.Footer.ValueType := fvtSum;


  DepartmentCount := Analize.GetOptionalParam('Department$Count');
  for i := 2 to DepartmentCount do
  CreateColumns;
  i := 1;
  CreateColumns;

  Field := TFIBFloatField.Create(Self);
  Field.Tag := MaxInt;
  Field.FieldName := 'shortage$or$surplus';
  Field.FieldKind := fkCalculated;
  Field.DataSet := DataSet;

  Column := gridItem.Columns.Add;
  Column.Tag := MaxInt;
  Column.Title.Caption := '��������� + / ������� -';
  Column.FieldName := Field.FieldName;
end;

begin
  FileName := GetSpecialFolderLocation(CSIDL_APPDATA) + '\jew\analize.cds';
  if FileExists(FileName) then
  begin
    Screen.Cursor := crHourGlass;
    Application.HandleMessage;

    dmAppl.dsrAppl.DataSet.DisableControls;
    dmAppl.dsrAppl.DataSet.Active := False;
    dmAppl.OnCalculateAppl := nil;

    Analize.LoadFromFile(FileName);
    Analize.IndexFieldNames := 'Model;Model$Size;Department$ID';
    Analize.First;

    Prepare;

    dmAppl.OnCalculateAppl := OnCalculateAppl;
    dmAppl.dsrAppl.DataSet.Active := True;
    dmAppl.dsrAppl.DataSet.EnableControls;

    Screen.Cursor := crDefault;
    InformationMessage('������ ��������')
  end
  else ErrorOkMessage('������ �� ������');
end;




procedure TfmAppl.OnCalculateAppl(DataSet: TDataSet);
var
  i: Integer;
  Field: TField;
  FieldName: string;
  Model: string;
  ModelSize: string;
  DepartmentID: Integer;
  SellCount: Integer;
  StorageCount: Integer;
  TotalSellCount: Integer;
  TotalStorageCount: Integer;
begin
  if Analize.Active then
  begin
    for i := 0 to dmAppl.taAppl.Fields.Count - 1 do
      begin
        Field := dmAppl.taAppl.Fields[i];
        if Field.Tag <> 0 then Field.AsInteger := 0;
      end;
    Model := Trim(dmAppl.taApplART.AsString);
    ModelSize := Trim(dmAppl.taApplSZNAME.AsString);
    Analize.SetRange([Model, ModelSize],[Model, ModelSize]);
    Analize.First;
    TotalSellCount := 0;
    TotalStorageCount := 0;
    while not Analize.Eof do
    begin
      SellCount := AnalizeSELLCOUNT.AsInteger;
      StorageCount := AnalizeSTORAGECOUNT.AsInteger;

      TotalSellCount := TotalSellCount + SellCount;
      TotalStorageCount := TotalStorageCount + StorageCount;
      DepartmentID := AnalizeDEPARTMENTID.AsInteger;
      FieldName := 'sell$' + IntToStr(DepartmentID);
      Field := dmAppl.taAppl.FieldByName(FieldName);
      if Field <> nil then Field.AsInteger := SellCount;
      FieldName := 'storage$' +  IntToStr(DepartmentID);
      Field := dmAppl.taAppl.FieldByName(FieldName);
      if Field <> nil then Field.AsInteger := StorageCount;
      Analize.Next;
    end;
    FieldName := 'storage$total';
    Field := dmAppl.taAppl.FieldByName(FieldName);
    if Field <> nil then Field.AsInteger := TotalStorageCount;
    FieldName := 'sell$total';
    Field := dmAppl.taAppl.FieldByName(FieldName);
    if Field <> nil then Field.AsInteger := TotalSellCount;
    FieldName := 'shortage$or$surplus';
    Field := dmAppl.taAppl.FieldByName(FieldName);
    if Field <> nil then Field.AsInteger := TotalSellCount + dmAppl.taApplSELL0.AsInteger - dmAppl.taApplWQ.AsInteger - dmAppl.taApplPSTOREQ.AsInteger - TotalStorageCount;
  end;

end;

end.


