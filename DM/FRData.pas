unit FRData;

interface

uses
  SysUtils, Classes, ComDrv32, ExtCtrls, Controls;

const
   Password: String = '0000';

   FRErrorStr:array[1..34] of String=('������ � ���������� ������, ������� ����������',
                                  '�� ������� �����',
                                  '�������� ������ ������� ������� � ���������� ������',
                                  '��������� ����� ���� �������',
                                  '�������� ������ ���� �������',
                                  '������ ������ �������',
                                  '�������� ����',
                                  '�������� �����',
                                  '���� ������ ��������� ����, ������������������ � ���������� ������',
                                  '�������� �������� �������������. �������� �����������',
                                  '����������� ������� ��',
                                  '�� ������� �����',
                                  '�� ������������',
                                  '������������ ��������� ������',
                                  '������ ������ � ���������� ������',
                                  '������ ��������� �������',
                                  '�������� ������ �������� ����������',
                                  '�������� ������ �� �����',
                                  '�������� ������ ���������������',
                                  '������� �� ��������������',
                                  '�������� ���� ������� ��� ���������',
                                  '������ ������ ���������� ������',
                                  '������������ ��������',
                                  '������������ ���� ������� ����� ������� �����',
                                  '�������� ������ �������',
                                  '���� ��� ����� ���������� ��������� � ����� �����������',
                                  '�� ������������',
                                  '������ � ������������ ����������',
                                  '��� ����� �������',
                                  '�������� ����������� �����',
                                  '��� ���������� �������',
                                  '����� ������ ����',
                                  '������ �� �������',
                                  '�� �� �������� !!!');

type
  TFRPrinterStatus = record
    Err       : boolean;  // ������
    PrnCheck  : boolean; // ������� ������ ???
    PaperLost : boolean; // ����������� ������
    Ready     : boolean; // ������� �����
    PrnUse    : boolean; // ����� �����, ��������� � ��������� offline ��� ��������� ������
  end;

  TFRStatus  = record
    SellOpen : boolean; // ����� �������?
    SellOpenDateTime : TDateTime;
    Fiscal : boolean; // ������ ��������������
    FiscalMemoryNearFull : boolean; // ���������� ������ ������ � ����� (�������� ������ 20 �������)
    FiscalMemoryFull : boolean; // ���������� ������ ���������
  end;

  TFRDocumentKind = (skSell, skRetSell, skMoney, skRetMoney);
  // skSell - ������� $30
  // skRetSell - ������� ������� $31
  // skMoney - �������� ����� $32
  // skRetMoney - ������� ����� $33

type
  TdmFR = class(TDataModule)
    tm: TTimer;
    com: TCommPortDriver;
    procedure DataModuleCreate(Sender: TObject);
    procedure comReceiveData(Sender: TObject; DataPtr: Pointer; DataSize: Integer);
    procedure tmTimer(Sender: TObject);
   private
    CurPos: Integer;
    FFRStatus : TFRStatus;
    FFRPrinterStatus : TFRPrinterStatus;
    FFRDateTime : TDateTime;
   private
     function ParseDateTime(Offs : word) : TDateTime;
   public
    // ������� � ��������� ��� ������ � ��
    Status: Integer;
    SendNext, Answer, Err: Boolean;
    OutBuf: array[1..2000] of byte;
    Buf: array[0..200] of byte;
    function FRReady : boolean;
    function GetFRStatus(var FRStatus : TFRStatus; var FRPrinterStatus : TFRPrinterStatus) : boolean; // �������� ��������� �������� ��
    function GetFRDateTime(var FRDateTime :TDateTime) : boolean; // �������� ����� ������� ��
    function SetFRDateTime(FRDateTime : TDateTime) : boolean; // ���������� ����� ������� �� (����� ����� ���� � ������ ���� ����� �������)
    function GetLastFRError : string;
    // �����
    function OpenSell(UserId : integer; UserName : string;var NSell : integer) : boolean;
    function CloseSell : boolean; // Z-����� ����� �������� �����
    function FRXReport : boolean; // X-����� (���������� �����)
    // ���������� ��������
    function FRDocument(Kind : TFRDocumentKind;var DocNo : integer;var DocDate : TDateTime;
       BillNo,Cost,PayCost :string; UseNDS : boolean=False; NDSCost:string=''): boolean;

    procedure SendPacket(Code, Size:Integer; var A: array of byte);

    // ��������� � ������� Cash
    procedure SaveFRDocToDB(Kind : TFRDocumentKind;DocNo : integer;DocDate : TDateTime);
  end;

var
  dmFR: TdmFR;

implementation

{$R *.dfm}

uses Dialogs, RxStrUtils, Forms, UtilLib, DateUtils, MsgDialog, DictData,
  InvData, dbUtil, uUtils;

{ TdmFR }

function Num2ASCIICode(ch : integer; p : byte) : byte;
var
  s : string;
begin
  case p of
    1 : ch := ch mod 10;
    2 : ch := ch div 10 mod 10;
    3 : ch := ch div 100 mod 10;
    4 : ch := ch div 1000 mod 10;
  end;
  s := IntToStr(ch);
  Result := ord(s[1]);
end;

function TdmFR.CloseSell: boolean;
begin
  if not com.Connected then com.Connect;
  //��������� ����� Z-�����
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  OutBuf[8]:=$32; // $39
  SendPacket($35, 12, OutBuf);
  Result := Status=0;
  com.Disconnect;
end;

function TdmFR.FRDocument(Kind: TFRDocumentKind; var DocNo: integer; var DocDate : TDateTime;
  BillNo, Cost, PayCost : string; UseNDS : boolean = False; NDSCost: string =''): boolean;
var
  PropCount : word;

  procedure InitProp(Prop : byte; HOffs, VOffs: word;PropValue : string);
  var
    j : integer;
  begin
    inc(PropCount);
    // ��� ���������
    OutBuf[(PropCount-1)*52+13]:=Ord(Format('%.2d', [Prop])[1]);
    OutBuf[(PropCount-1)*52+14]:=Ord(Format('%.2d', [Prop])[2]);
    // ���� ��������� (�����)
    case Prop of
      11 : OutBuf[(PropCount-1)*52+16]:=$43;
      12 : OutBuf[(PropCount-1)*52+16]:=$63;
      else OutBuf[(PropCount-1)*52+16]:=Num2ASCIICode(0, 1);
    end;
    // �������� ��������� �� ����������� �� ������ ������
    OutBuf[(PropCount-1)*52+18]:=Ord(Format('%.2d', [HOffs])[1]);
    OutBuf[(PropCount-1)*52+19]:=Ord(Format('%.2d', [HOffs])[2]);
    // �������� ��������� �� ��������� �� ������� ������
    OutBuf[(PropCount-1)*52+21]:= Ord(Format('%.2d', [VOffs])[1]);
    OutBuf[(PropCount-1)*52+22]:= Ord(Format('%.2d', [VOffs])[2]);
    // �������� ���������
    for j:=1 to Length(PropValue) do OutBuf[(PropCount-1)*52+23+j]:=Ord(PropValue[j]);
  end;

begin
  if not com.Connected then com.Connect;
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  // �������� ����� �����
  Cost := StringReplace(Cost, ',','.',[rfReplaceAll, rfIgnoreCase]);
  PayCost := StringReplace(payCost,',','.',[rfReplaceAll, rfIgnoreCase]);
  NDSCost := StringReplace(NDSCost,',','.',[rfReplaceAll, rfIgnoreCase]);
  PropCount:=0;
  InitProp(01,10,00,'');      //
  InitProp(00,00,02,'');
  InitProp(10,20,02,'');
  InitProp(02,00,03,'');
  InitProp(03,12,03,'');
  InitProp(04,20,03,'');
  InitProp(06,00,04,'');
  InitProp(09,12,04,BillNo); // ����� �����
  InitProp(07,20,04,'');
  InitProp(05,07,05,Cost); // ����� ������
  if UseNDS then
  begin
    InitProp(99,00,06,StrToOem('� �.�. ��� '+NDSCost));
    InitProp(08,00,07,'');
    InitProp(11,00,08,PayCost); // �������� �����
    InitProp(12,00,09,'');
  end
  else begin
    InitProp(08,00,06,'');
    InitProp(11,00,07,PayCost); // �������� �����
    InitProp(12,00,08,'');
  end;
  case Kind of
    skSell : OutBuf[8]:=$30;
    skRetSell : OutBuf[8]:=$31;
    skMoney : OutBuf[8]:=$32;
    skRetMoney : OutBuf[8]:=$33;
    else raise Exception.Create('����������� ��� ����������� ���������');
  end;
  // ���-�� ������������ ����������
  OutBuf[10]:=Ord(Format('%.2d', [PropCount])[1]);
  OutBuf[11]:=Ord(Format('%.2d', [PropCount])[2]);
  OutBuf[13+PropCount*52]:=$30;
  SendPacket($33, 17+PropCount*52, OutBuf);
  Result := Status=0;
  if not Result then eXit;
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  SendPacket($4A, 10, OutBuf);
  Result := Status = 0;
  if not Result then eXit;
  DocNo:=StrToInt(chr(Buf[19])+chr(Buf[20])+chr(Buf[21])+chr(Buf[22])+ chr(Buf[23]));
  DocDate := ParseDateTime(25);
  if not com.Connected then com.Connect;
end;

function TdmFR.FRReady: boolean;
var
  frprinterstat : TFRPrinterStatus;
  frstat : TFRStatus;
begin
  Result := GetFRStatus(frstat, frprinterstat);
  if Result then Result := frprinterstat.Ready;
end;

function TdmFR.FRXReport: boolean;
begin
  if not com.Connected then com.Connect;
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  OutBuf[8]:=$30;
  SendPacket($35, 12, OutBuf);
  Result := Status = 0;
  com.Disconnect;
end;

function TdmFR.GetFRDateTime(var FRDateTime: TDateTime): boolean;
begin
  if not com.Connected then com.Connect;
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  SendPacket($48, 10, OutBuf);
  // �������� ����-�����
  Result := (Status = 0);
  if Result then FRDateTime := FFRDateTime;
end;

function TdmFR.GetFRStatus(var FRStatus: TFRStatus; var FRPrinterStatus: TFRPrinterStatus): boolean;
begin
  if not com.Connected then com.Connect;
  // ������ ��������
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  OutBuf[8] := $30; // �� ��������
  SendPacket($44, 12, OutBuf);
  if (Status > 0) then Result := False
  else begin
    Result := True;
    FRPrinterStatus := Self.FFRPrinterStatus;
    FRStatus := Self.FFRStatus;
  end;
end;

function TdmFR.GetLastFRError: string;
begin
  if Status > 0 then Result := FRErrorStr[Status]
  else Result := '';
end;

function TdmFR.OpenSell(UserId: integer; UserName: string;
  var NSell: integer): boolean;
var
  i : integer;
begin
  Result := False;
  if not com.Connected then com.Connect;

  if uUtils.QuestionYesNoMessage('���������������� ���� � �����.') then
  begin
    if not SetFRDateTime(Now)then eXit;
  end;
  //�������� ����� �����
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  OutBuf[8]:=$30;
  SendPacket($44, 12, OutBuf);
  if(Status > 0) then eXit;
  NSell:=StrToInt(chr(Buf[32])+chr(Buf[33])+chr(Buf[34])+chr(Buf[35])+chr(Buf[36])) + 1;
  //��������� �����
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  OutBuf[8]:=Num2ASCIICode(UserId, 2);
  OutBuf[9]:=Num2ASCIICode(UserId, 1);
  for i:=1 to Length(UserName) do
    if(i<40)then OutBuf[10+i]:=Ord(StrToOem(UserName)[i])
    else continue;
  SendPacket($31, 53, OutBuf);
  Result := Status = 0;
end;

procedure TdmFR.SendPacket(Code, Size: Integer; var A: array of byte);
var
  s, i: Integer;
  f : TFileStream;
begin
  Err:=False;
  while not Answer do
    Application.ProcessMessages;
  A[0]:=$02;
  A[1]:=Code;
  for i:=2 to 5 do A[i]:=Ord(Password[i-1]);
  s:=0;
  for i:=1 to Size-4 do
    if (s + A[i]) > 255 then s:=s + A[i] - 256
    else s:=s + A[i];
  A[Size-3]:=Ord(Dec2Hex(s,2)[1]);
  A[Size-2]:=Ord(Dec2Hex(s,2)[2]);
  A[Size-1]:=$03;
  {$IFDEF DEBUG_FR}
  // �������� ������ � ����
  f := TFileStream.Create('c:\sell.ttt', fmCreate);
  try
    for i:=0 to Pred(Size) do
      f.Write(A[i], 1);
  finally
    FreeAndNil(f);
  end;
  {$ENDIF}
  Answer:=False;
  tm.Enabled:=True;
  com.SendData(@A, Size);

  while not Answer and not Err do Application.ProcessMessages;
end;

function TdmFR.SetFRDateTime(FRDateTime: TDateTime): boolean;
var
  y,m,d,h,s,ms : word;
  ch : char;
begin
  if not com.Connected then com.Connect;
  // ������������� ����-�����
  FillChar(OutBuf, SizeOf(OutBuf), 0);
  //������������� ����-�����
  DecodeDate(FRDateTime,y,m,d);
  //����� ����
  OutBuf[8]:= Num2ASCIICode(d,2);
  OutBuf[9]:= Num2ASCIICode(d,1);
  OutBuf[10]:= Num2ASCIICode(m,2);
  OutBuf[11]:= Num2ASCIICode(m,1);
  OutBuf[12]:= Num2ASCIICode(y,4);
  OutBuf[13]:= Num2ASCIICode(y,3);
  OutBuf[14]:= Num2ASCIICode(y,2);
  OutBuf[15]:= Num2ASCIICode(y,1);
  //����� �����
  DecodeTime(FRDateTime,h,m,s,ms);
  OutBuf[17]:= Num2ASCIICode(h,2);
  OutBuf[18]:= Num2ASCIICode(h,1);
  OutBuf[19]:= Num2ASCIICode(m,2);
  OutBuf[20]:= Num2ASCIICode(m,1);
  SendPacket($47, 24, OutBuf);
  Result := Status = 0;
end;

procedure TdmFR.comReceiveData(Sender: TObject; DataPtr: Pointer; DataSize: Integer);
var P, p1: PChar;
    i: Integer;
    s : string;
    {*************************************************************************}
    { ��������� ����������: 1 ��� - ���������, 8 ��� - ������, 1 ��� - ��������, ��� ��������, �������� �������� - 9600,
        ���������� ���������� � ����� ASCII. ������ ����� ������������� ������� �� ������ ��������� 100 ����.
      ����������� ������ ������ ��������� ���������, ������� ��������� ���������.
      �������, ���������� ����������� � ������ ��, ���������� ASCII ������������ ��������� ��������� 02H (STX) � �������� 03H (ETX).
      ����� �������� �������� ���������� ������ �������� ����� (���).
      BCC ����������� ��������� ��� �������� ���� ������ �������, ��� ����� ����� STX � ������������ ����� 2-������� �����,
      ������������ �� ������� �������� ����. �������, ���������� ��, ����� ������, ��������������� ���
      ����������� ������������������� ������.
      ��������� ������ ������� ����� �� �������, ������ ��� �������� ���������.
      ��� �������� ���������� ������� ����� �� ����� ������������ ��������� �������� � ���������� ����� ������.
      ����, ������� �� ����� ���� ���������, �� ����� ���� �������� �� �������� ����� ��. FRErrorStr �
      ���� "��� ���������" ���������� ��� �������.
      ���� �� ��������� �������� ����������� �����, ����������� ���  ������� ���
      ��������� ������������ ��������� ������ � ���� "��� ���������" ���������� ���  "00".
      ���� �� ����� ������ ���������� ������ ��������, �� �������� ������ 05H �
      ������ �������� � ������� �������� ����,  �  ��������� � ����� �������� �������
      �� ����������. � ���� ������ ��������� ������ ������� ������ ����������� ������ 04H
      ��� ������ ������ ������ 01H. ���� ������ ������ ������ ������, �� ����������
      ���������� ���������, �������� ����� � ��������� � ����� �������� ������.
      ��������� ������� ����� ����� ���������� ������� ����� ����������.
      �� ����� ���������� ������� ����������� �������� ������ 100ms ��� 06H
      �������������� ���������� ��������. ���� ��������� ��������� ���� ��� ��� ������,
      ��� ��������� ������� ���������� ����������� � ��������� ������ �����
      ���������� ������� ������. ���������� ����� � ������� 1s ������ ��������������� ���
      ��������� ����������.
    ***************************************************************************}
begin
  tm.Enabled:=False;
  tm.Enabled:=True;
  P:=DataPtr;
  for i:=1 to DataSize do
  begin
    case Ord(P^) of
      2 : begin
        CurPos:=0;
        Buf[0]:=2;
      end;
      5 : begin
        if MessageDialog('������ ������! ���������� ?', mtError, [mbYes, mbNo], 0) = mrYes
        then com.SendString(#04)
        else begin
          com.SendString(#01);
          Err:=True;
        end;
      end;
      6 : begin
        tm.Enabled := False;
        sleep(10);
        tm.Enabled := True;
      end;
      else if (CurPos >= 0) and (Buf[CurPos] <> 3) then
       begin
         inc(CurPos);
         Buf[CurPos]:=Ord(P^);
       end;
    end;
    inc(P);
  end;
  if (CurPos > 0) and (Buf[CurPos] = 3) then
  begin
   tm.Enabled:=False;
   Answer:=True;
   Status:=Hex2Dec(chr(Buf[9])+chr(Buf[10])+chr(Buf[11])+chr(Buf[12]));
   if Status = 0 then
     case Buf[1] of
       $48 : begin
         s := chr(Buf[16])+chr(Buf[17])+'.'+chr(Buf[18])+chr(Buf[19])+'.'+chr(Buf[20])+chr(Buf[21])+chr(Buf[22])+chr(Buf[23]);
         s := s+' '+chr(Buf[25])+chr(Buf[26])+':'+chr(Buf[27])+chr(Buf[28]);
         FFRDateTime := StrToDateTime(s);
       end;               
       $44 : begin
         s := '$'+chr(Buf[13]) + chr(Buf[14]);
         i := Hex2Dec(s);
         FFRPrinterStatus.Err := ((i shr 3) and 1) = 0;
         FFRPrinterStatus.PrnCheck := ((i shr 4) and 1) = 1;
         FFRPrinterStatus.PaperLost := ((i shr 5) and 1) = 1;
         FFRPrinterStatus.Ready := ((i shr 6) and 1) = 1;
         FFRPrinterStatus.PrnUse := ((i shr 7) and 1) = 1;
         s := '$'+chr(Buf[3])+chr(Buf[4])+chr(Buf[5])+chr(Buf[6]);
         i := Hex2Dec(s);
         FFRStatus.SellOpen := ((i shr 0) and 1)=1;
         FFRStatus.Fiscal := ((i shr 4) and 1)=1;
         FFRStatus.FiscalMemoryNearFull := ((i shr 5) and 1)=1;
         FFRStatus.FiscalMemoryFull := ((i shr 7) and 1)=1;
         if FFRStatus.SellOpen then
         begin
           s := chr(Buf[38])+chr(Buf[39])+'.'+chr(Buf[40])+chr(Buf[41])+'.'+chr(Buf[42])+chr(Buf[43])+chr(Buf[44])+chr(Buf[45]);
           s := s+' '+chr(Buf[47])+chr(Buf[48])+':'+chr(Buf[49])+chr(Buf[50]);
           FFRStatus.SellOpenDateTime := StrToDateTime(s);
         end
         else FFRStatus.SellOpenDateTime := ToDay;
       end;
     end;     
   CurPos:=-1;
  end;
end;

procedure TdmFR.DataModuleCreate(Sender: TObject);
begin
  CurPos:=-1;
  Status := 0;
  Answer:=True;
  with dm.LocalSetting do
  begin
    if (Cassa_Port = 'COM1') then com.ComPort := pnCOM1
    else if (Cassa_Port = 'COM2') then com.ComPort := pnCOM2
    else if (Cassa_Port = 'COM3') then com.ComPort := pnCOM3
    else if (Cassa_port = 'COM4') then com.ComPort := pnCOM4
    else com.ComPort := pnCOM1;
  end;
end;

function TdmFR.ParseDateTime(Offs: word) : TDateTime;
var
  s : string;
begin
  s := chr(Buf[Offs+0])+chr(Buf[Offs+1])+'.'+
       chr(Buf[Offs+2])+chr(Buf[Offs+3])+'.'+
       chr(Buf[Offs+4])+chr(Buf[Offs+5])+chr(Buf[Offs+6])+chr(Buf[Offs+7])+' '+
       chr(Buf[Offs+9])+chr(Buf[Offs+10])+':'+
       chr(Buf[Offs+11])+chr(Buf[Offs+12]);
  Result := StrToDateTime(s);
end;

procedure TdmFR.SaveFRDocToDB(Kind: TFRDocumentKind; DocNo: integer; DocDate: TDateTime);
var
  t : string;
begin
  case Kind of
    skSell: t:='1';
    skRetSell: t:='2';
    skMoney: t:='3';
    skRetMoney: t:='4';
    else t:='-1'; 
  end;
  try
    ExecSQL('insert into Cash ( ID, COMPID, COST, NDS, OPERDATE, NDOC, T) values '+
       '(gen_id(gen_cash_id, 1), ' + IntToStr(dmInv.FCassaCompId) + ', ' +
        StringReplace(FloatToStr(dmInv.FCassaMoney), ',','.',[rfReplaceAll, rfIgnoreCase])+','+
        StringReplace(FloatToStr(dmInv.FCassaNDSMoney),',','.',[rfReplaceAll, rfIgnoreCase])+','#39+
        DateTimeToStr(DocDate)+#39','#39+IntToStr(DocNo)+#39','+t+')', dm.quTmp);
  except
    on E:Exception do MessageDialogA('�� ������� ��������� ������ � ������� � ��.'#13+
      '���������� ���������� � �������������'#13+E.Message, mtError);
  end; 
end;

procedure TdmFR.tmTimer(Sender: TObject);
begin
  tm.Enabled:=False;
  Answer:=True;
  Status:=34;
end;

end.
