{******************************************}
{  ������ ����� ����������� (������������) }
{  ����������� ������                      }
{  Copyrigth (C) 2003 basile for CDM       }
{  v0.16                                   }
{  create      28.06.2003                  }
{  last update 24.12.2003                  }
{******************************************}

unit BulkList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, Menus,  ImgList, DBGridEh, 
  StdCtrls, ExtCtrls, Grids, DBGrids, RXDBCtrl,
  ComCtrls, ActnList,  TB2Item, DBGridEhGrouping, rxPlacemnt,
  GridsEh, rxSpeedbar;

type
  TfmBulkList = class(TfmListAncestor)
    procedure FormCreate(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acPrintDocUpdate(Sender: TObject);
  protected
    procedure DepClick(Sender : TObject);override;
  end;

var
  fmBulkList: TfmBulkList;

implementation

uses MainData, DictData, dbUtil, Bulk, DocAncestor, PrintData, DB;

{$R *.dfm}

procedure TfmBulkList.FormCreate(Sender: TObject);
var
  i, j : integer;
  Item : TMenuItem;
begin
  inherited;
  // ���������� PopupMenu �� FDepInfo;
  j := 0;
  for i:=0 to Pred(dm.CountDep) do
    if (dm.DepInfo[i].DepTypes and $2)=$2 then
    begin
      Item := TMenuItem.Create(ppDep);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepClick;
      Item.RadioItem := True;
      if Item.Tag = dm.DepDef then DepClick(Item);
      inc(j);
      if j>30 then
      begin
        Item.Break := mbBreak;
        j:=0;
      end;
      ppDep.Items.Add(Item);
    end;
end;

procedure TfmBulkList.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  ShowPeriod;
  inherited;
end;

procedure TfmBulkList.acPrintDocExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkBulkAct, VarArrayOf([VarArrayOf([dmMain.taBulkListINVID.AsInteger])]));
end;

procedure TfmBulkList.acAddDocExecute(Sender: TObject);
begin
  with dmMain do
  begin
    PostDataSet(taBulkList);
    inherited;
    ShowDocForm(TfmBulk, taBulkList, taBulkListINVID.AsInteger, quTmp);
    RefreshDataSet(taBulkList);
  end;
end;

procedure TfmBulkList.acEditDocExecute(Sender: TObject);
begin
  with dmMain do
  begin
    ShowDocForm(TfmBulk, taBulkList, taBulkListINVID.AsInteger, quTmp);
    RefreshDataSet(taBulkList);
  end;
end;

procedure TfmBulkList.acPrintDocUpdate(Sender: TObject);
begin
  acPrintDoc.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

end.
