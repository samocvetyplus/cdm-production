{***********************************************}
{  ����������� �������������� ����� ���������   }
{  Copyrigth (C) 2002-2003 basile for CDM       }
{  v0.91                                        }
{  create 01.02.2003                            }
{  last update 28.05.2003                       }
{  last update 19.04.2004                       }
{***********************************************}
unit DIEl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList, ExtCtrls, ComCtrls,
  DBCtrls, RxToolEdit, RXDBCtrl, StdCtrls, Mask, DBGrids,
  Grids, DBGridEh, DButil, DB, TB2Item, TB2Dock,
  TB2Toolbar, ActnList, DBCtrlsEh, DBLookupEh, Menus, MsgDialog, Buttons,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmDIEl = class(TfmAncestor)
    paHeader: TPanel;
    lbDocDate: TLabel;
    lbDepTo: TLabel;
    txtDep: TDBText;
    lbDepFrom: TLabel;
    spitPrint: TSpeedItem;
    spitClose: TSpeedItem;
    txtDepTo: TDBText;
    TBDock1: TTBDock;
    TBToolbar2: TTBToolbar;
    TBItem6: TTBItem;
    TBItem5: TTBItem;
    acEvent: TActionList;
    acAddSemis: TAction;
    acDelSemis: TAction;
    plWhSemis: TPanel;
    gridWhSemis: TDBGridEh;
    TBDock2: TTBDock;
    tbWhSemis: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label3: TLabel;
    TBControlItem2: TTBControlItem;
    cmbxOper: TDBComboBoxEh;
    TBControlItem3: TTBControlItem;
    Label5: TLabel;
    TBControlItem4: TTBControlItem;
    cmbxMat: TDBComboBoxEh;
    spltGrids: TSplitter;
    lbJobId: TLabel;
    lcbxJobId: TDBLookupComboboxEh;
    lbUserId: TLabel;
    ppInv: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acCopyAssort: TAction;
    acPasteAssort: TAction;
    lcbxUser: TDBLookupComboboxEh;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lbDocNo: TLabel;
    TBSeparatorItem1: TTBSeparatorItem;
    lcbxInvColor: TDBLookupComboboxEh;
    Label4: TLabel;
    acLogic: TAction;
    SpeedButton1: TSpeedButton;
    plRigth: TPanel;
    gridEl: TDBGridEh;
    Splitter1: TSplitter;
    plAssort: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    txtADocNo: TDBText;
    Label2: TLabel;
    txtADocDate: TDBText;
    gridAssort: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddSemisExecute(Sender: TObject);
    procedure acDelSemisExecute(Sender: TObject);
    procedure spitCloseClick(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
    procedure cmbxOperChange(Sender: TObject);
    procedure cmbxMatChange(Sender: TObject);
    procedure gridElEditButtonClick(Sender: TObject);
    procedure gridWhSemisKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acCopyAssortExecute(Sender: TObject);
    procedure acPasteAssortExecute(Sender: TObject);
    procedure acCopyAssortUpdate(Sender: TObject);
    procedure acPasteAssortUpdate(Sender: TObject);
    procedure acDelSemisUpdate(Sender: TObject);
    procedure acAddSemisUpdate(Sender: TObject);
    procedure acLogicExecute(Sender: TObject);
  private
    FReadOnlyMode : boolean; 
    procedure CloseDoc;
    procedure OpenDoc;
    procedure InitBtn(T : boolean);
    procedure InsertDWHSemisRecord(DepId : integer; SemisId : string; OperId : Variant; UQ, UW : integer);
    procedure FilterInvColor(DataSet: TDataSet; var Accept: Boolean);
    procedure InvColorGetCellParams(Sender: TObject; EditMode: Boolean; Params: TColCellParamsEh);
  end;

var
  fmDIEl: TfmDIEl;

implementation

uses eSQ, PrintData, MainData, DictData, fmUtils, eDSemis, Editor, dbTree,
  ApplData, ADistr, ProductionConsts;

{$R *.dfm}

procedure TfmDIEl.FormCreate(Sender: TObject);
var
  FromDepBD, DepBD : TDateTime;
begin
  dm.WorkMode := 'aDIEl';
  gridWhSemis.Color := clWindow;
  gridEl.Color := clWindow;
  // ���������������� ������� ������ ������������
  cmbxOper.OnChange := nil;
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.ItemIndex := 0;
  cmbxOper.Items.Insert(1, sNoOperation);
  dmMain.FilterOperId := ROOT_OPER_CODE;
  cmbxOper.OnChange := cmbxOperChange;

  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMat);
  cmbxMat.ItemIndex := 0;
  dmMain.FilterMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := cmbxMatChange;
  // ��������
  lcbxInvColor.DropDownBox.Columns[0].OnGetCellParams := InvColorGetCellParams;
  // ������� ������ ������
  dm.taInvColor.OnFilterRecord := FilterInvColor;
  dm.taInvColor.Filtered := True;
  dmMain.taDIEl.AfterScroll := nil;
  with dmMain do
    OpenDataSets([taDWHSemis, dm.taInvColor, taPSMol, taPSMOL1, taDIEl]);
  dmMain.taDIEl.AfterScroll := dmMain.taDIElAfterScroll;
  InitBtn(dmMain.taDIListISCLOSE.AsInteger=1);

  (* �������� ������� *)
  FromDepBD := dm.GetDepBeginDate(dmMain.taDIListFROMDEPID.AsInteger);
  DepBD := dm.GetDepBeginDate(dmMain.taDIListDEPID.AsInteger);
  if(FromDepBd > DepBd)then dm.DepBeginDate := FromDepBd else dm.DepBeginDate := DepBd;
  if(dm.DepBeginDate > dmMain.taDIListDOCDATE.AsDateTime)then
  begin
    FReadOnlyMode := True;
    stbrStatus.Panels[0].Text := '�������� ������ ���������� �: '+DateToStr(dm.DepBeginDate);
    lbDocNo.Enabled := False;
    lbDocDate.Enabled := False;
    edSN.Enabled := False;
    deSDate.Enabled := False;
    lbDepFrom.Enabled := False;
    lbDepTo.Enabled := False;
    txtDep.Enabled := False;
    txtDepTo.Enabled := False;
    lbUserId.Enabled := False;
    lbJobId.Enabled := False;
    lcbxUser.Enabled := False;
    lcbxJobId.Enabled := False;
    acAddSemis.Enabled := False;
    acDelSemis.Enabled := False;
    acCopyAssort.Enabled := False;
    acPasteAssort.Enabled := False;
    gridWhSemis.ReadOnly := True;
    gridEl.ReadOnly := True;
  end
  else FReadOnlyMode := False;
end;

procedure TfmDIEl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if(lcbxUser.Text = '')then raise Exception.Create('��������� ���� <�����>');
  if(lcbxJobId.Text = '')then raise Exception.Create('��������� ���� <������>');
  inherited;
  if (dmMain.taDIList.State in [dsInsert, dsEdit]) then dmMain.taDIList.Post;
  with dmMain do
    CloseDataSets([taDIEl, taDWHSemis, taPSMOL1, taPSMol, dm.taInvColor]);
  dm.taInvColor.OnFilterRecord := nil;
  dm.taInvColor.Filtered := False;
end;

procedure TfmDIEl.spitCloseClick(Sender: TObject);
begin
  if (dmMain.taDIListFROMDEPID.AsInteger <> dm.User.wWhId) then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taDIListFROMDEPNAME.AsString]));
  PostDataSets([dmMain.taDIEl, dmMain.taDIList]);
  if dmMain.taDIEl.RecordCount=0 then
    raise EWarning.Create('��� ������� � ���������!');
  if spitClose.BtnCaption = '������' then
    try
      CloseDoc;
      spitClose.BtnCaption := '������';
      spitClose.Hint := '������� ��������';
      spitClose.ImageIndex := 5;
    except
      on E: Exception do
         { TODO : !!! }
      //if not TranslateIbMsg(E, dmMain.taSIEl.Database, dmMain.taSIEl.Transaction)
          // then
          ShowMessage(E.Message);
    end
  else
    try
      OpenDoc;
      spitClose.BtnCaption := '������';
      spitClose.Hint := '������� ��������';
      spitClose.ImageIndex := 6;
    except
      on E: Exception do //if not TranslateIbMsg(E, dmMain.taDIEl.Database, dmMain.taDIEl.Transaction)
           { TODO : !!! }
           //then
           ShowMessage(E.Message);
    end;
  RefreshDataSet(dmMain.taDILIst);
end;

procedure TfmDIEl.spitPrintClick(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkWHSemis_Transfer, null);
end;

procedure TfmDIEl.InitBtn(T: boolean);
begin
  if not T then // �������� ������
  begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 6;
  end
  else begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 5;
  end
end;

procedure TfmDIEl.CloseDoc;
begin
  ExecSQL('execute procedure CloseDoc("����������� ����������", '+IntToStr(
    dmMain.taDIListINVID.AsInteger)+')', dmMain.quTmp);
end;

procedure TfmDIEl.OpenDoc;
begin
  ExecSQL('execute procedure OpenDoc("����������� ����������", '+IntToStr(
    dmMain.taDIListINVID.AsInteger)+')', dmMain.quTmp);
end;

procedure TfmDIEl.acAddSemisExecute(Sender: TObject);
begin
  if (dmMain.taDIListFROMDEPID.AsInteger <> dm.User.wWhId) then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taDIListFROMDEPNAME.AsString]));
  PostDataSet(dmMain.taDIList);
  dmMain.Semis := dmMain.taDWHSemisSEMISID.AsString; 
  dmMain.SemisQ := dmMain.taDWHSemisQ_FROM.AsInteger;
  dmMain.SemisW := dmMain.taDWHSemisW_FROM.AsFloat;
  dmMain.DOperId := dmMain.taDWHSemisOPERID.AsString;
  eDSemis.Operation := dmMain.taDWHSemisOPERNAME.AsString;
  eDSemis.CanChangeOperation := True;
  eDSemis.Good := dmMain.taDWHSemisGOOD.AsInteger = 1;

  if ShowEditor(TfmDSemis, TfmEditor(fmDSemis))<>mrOk then eXit;

  with dmMain do
  begin
    if taDIEl.Locate('SEMIS;OPERNAME;FROMOPERID;UQ;UW', VarArrayOf([dmMain.taDWHSemisSEMISID.AsString,
      DOperName, taDWHSemisOPERID.AsVariant, taDWHSemisUQ.AsInteger,
      taDWHSemisUW.AsInteger]), []) then
    begin
      taDIEl.Edit;
      taDIElQ.AsInteger := taDIElQ.AsInteger + SemisQ;
      taDIElW.AsFloat   := taDIElW.AsFloat + SemisW;
    end
    else begin
      taDIEl.Append;
      taDIElSEMIS.AsString := taDWHSemisSEMISID.AsString;
      taDIElSEMISNAME.AsString := taDWHSemisSEMISNAME.AsString;
      if DOperId = sNoOperation then taDIElOPERID.AsVariant := Null
      else taDIElOPERID.AsString := DOperId;
      taDIElOPERNAME.AsString := DOperName;
      taDIElUQ.AsInteger := taDWHSemisUQ.AsInteger;
      taDIElUW.AsInteger := taDWHSemisUW.AsInteger;
    end;
    taDIEl.Post;
    RefreshDataSet(taDWhSemis);
  end
end;

procedure TfmDIEl.acDelSemisExecute(Sender: TObject);
var
  SaveSemisId : string;
  SaveUQ, SaveUW : integer;
  SaveOperId : Variant;
begin
  if (dmMain.taDIListFROMDEPID.AsInteger <> dm.User.wWhId) then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taDIListFROMDEPNAME.AsString]));
  with dmMain do
  begin
    if(taDIListISCLOSE.ASinteger = 1)then
       raise EWarning.Create(Format(rsInvClose, [dmMain.taDIListDOCNO.AsString]));
    PostDataSets([taDIList, taDIEl]);
    SemisQ := taDIElQ.AsInteger;
    SemisW := taDIElW.AsFloat;
    if taDIElFROMOPERID.IsNull then
    begin
      DOperId := '';
      eDSemis.Operation := sNoOperation;
    end
    else begin
      DOperId := taDIElFROMOPERID.AsString;
      eDSemis.Operation := taDIElFROMOPERNAME.AsString;
    end;
    eDSemis.Good := taDIElGOOD.AsInteger = 1;
    eDSemis.CanChangeOperation := False;

    if ShowEditor(TfmDSemis, TfmEditor(fmDSemis))<>mrOk then eXit;

    SaveUQ := taDIElUQ.AsInteger;
    SaveUW := taDIElUW.AsInteger;
    SaveOperId := taDIElFROMOPERID.AsVariant;
    SaveSemisId := taDIElSEMIS.AsString;
    if(SemisQ = taDIElQ.AsInteger)and
      (Abs(SemisW - taDIElW.AsFloat)<Eps) then taDIEl.Delete
    else begin
      taDIEl.Edit;
      taDIElQ.AsInteger := taDIElQ.AsInteger - SemisQ;
      taDIElW.AsFloat := taDIElW.AsFloat - SemisW;
      taDIEl.Post;
    end;

    if taDWHSemis.Locate('SEMISID;OPERID;UQ;UW',  VarArrayOf([SaveSemisId,
         SaveOperId, SaveUQ, SaveUW]), [])
    then RefreshDataSet(dmMain.taDWhSemis)
    else // ������ ����������
      InsertDWHSemisRecord(taDIListFROMDEPID.AsInteger, SaveSemisId, SaveOperId, SaveUQ, SaveUW);
  end;
end;

procedure TfmDIEl.cmbxOperChange(Sender: TObject);
begin
  with cmbxOper, Items do
    if Items[ItemIndex] = sNoOperation then dmMain.FilterOperId := sNoOperation
    else dmMain.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taDWHSemis);
end;

procedure TfmDIEl.cmbxMatChange(Sender: TObject);
begin
  dmMain.FilterMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taDWHSemis);
end;

procedure TfmDIEl.InsertDWHSemisRecord(DepId : integer; SemisId : string; OperId : Variant;
       UQ, UW : integer);
begin
  if(VarIsNull(OperId))then
    dmMain.DWhSemisNewId := ExecSelectSQL('select WhSemisId from Check_WhSemis('+IntToStr(DepId)+',"'+
      SemisId+'", null, '+IntToStr(UQ)+','+IntToStr(UW)+','+IntToStr(dm.User.SelfCompId)+')', dmMain.quTmp)
  else
    dmMain.DWhSemisNewId := ExecSelectSQL('select WhSemisId from Check_WhSemis('+IntToStr(DepId)+',"'+
      SemisId+'","'+OperId+'",'+IntToStr(UQ)+','+IntToStr(UW)+','+IntToStr(dm.User.SelfCompId)+')', dmMain.quTmp);
  with dmMain do
  begin
    taDWHSemis.Insert;
    taDWHSemis.Post;
    dm.tr.CommitRetaining;
    taDWHSemis.Refresh;
  end;
end;

procedure TfmDIEl.gridElEditButtonClick(Sender: TObject);
var
  r : Variant;
begin
  if(gridEl.SelectedField.FieldName <> 'Q')then eXit;
  if(dmMain.taDIListFROMDEPID.AsInteger <> dm.User.wWhId) then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taDIListFROMDEPNAME.AsString]));
  with dmMain, dmAppl do
  begin
    if(taDIListISCLOSE.AsInteger = 1)then
      raise EWarning.Create(Format(rsInvClose, [dmMain.taDIListDOCNO.AsString]));
    // ��������� ����� �������
    if((dm.User.AccProd and $20)<>$20)then
      raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� ������������!']));
    PostDataSets([taDIList, taDIEl]);
    dmAppl.ADistrType := adtDIEl;
    (* ������� �� ���������? *)
    r := ExecSelectSQL('select InvId from Inv where InvId='+IntToStr(taDIElREF.AsInteger), quTmp); 
    if  VarIsNull(r)or(r=0) then
    begin
      taDIEl.Edit;
      taDIElREF.AsVariant := Null;
      taDIEl.Post;
    end;
    if taDIElREF.IsNull then
    begin
      if trAppl.Active then trAppl.Commit;
      trAppl.StartTransaction;
      OpenDataSets([taAInv]);
      taAInv.Append;
      taDIEl.Edit;
      taDIElREF.AsInteger := taAInvINVID.AsInteger;
      PostDataSets([taDIEl, taAInv]);
    end;
    CloseDataSets([taAInv, taAEl]);
    dmAppl.ADistrKind := 0;
    ShowAndFreeForm(TfmADistr, TForm(fmADistr));
    dm.WorkMode := 'aDIEl';
    (* �������� ��������� ������� ��������� *)
    gridEl.SelectedField := dmMain.taDIElQ;
    ReOpenDataSet(taDIAssortInv);  
  end;
end;

procedure TfmDIEl.gridWhSemisKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_SPACE  : acAddSemis.Execute;
  end;
end;

procedure TfmDIEl.acCopyAssortExecute(Sender: TObject);
var
  InvId : integer;
begin
  with dmMain do
  begin
    InvId := ExecSelectSQL('select InvId from Inv where InvId='+IntToStr(taDIElREF.AsInteger), quTmp);
    ExecSQL('execute procedure COPY_B_AINV('+IntToStr(dm.User.UserId)+', '+IntToStr(InvId)+')', quTmp);
  end;
end;

procedure TfmDIEl.acPasteAssortExecute(Sender: TObject);
var
  DocNo : string;
  Q : integer;
  InvId : Variant;
begin
  // ��������� ����� �������
  if((dm.User.AccProd and $20)<>$20)then
     raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� ������������!']));
  with dmMain do
  begin
    PostDataSets([taDIList, taDIEl]);
    dmAppl.ADistrType := adtDIEl;
    if taDIElREF.IsNull then InvId := Null
    else InvId := ExecSelectSQL('select InvId from Inv where InvId='+taDIElREF.AsString, quTmp);
    if VarIsNull(InvId)or(InvId = 0) then
    begin
      if MessageDialog('������� ����� ��������� ������������?', mtConfirmation, [mbYes, mbNo], 0)<>mrYes then eXit;
      if dmAppl.trAppl.Active then dmAppl.trAppl.Commit;
      dmAppl.trAppl.StartTransaction;
      OpenDataSets([dmAppl.taAInv]);
      dmAppl.taAInv.Append;
      taDIEl.Edit;
      taDIElREF.AsInteger := dmAppl.taAInvINVID.AsInteger;
      PostDataSets([taDIEl, dmAppl.taAInv]);
    end
    else begin
      DocNo := ExecSelectSQL('select DOCNO from Inv where InvId='+taDIElREF.AsString, quTmp);
      if (DocNo = '0')then raise EInternal.Create(Format(rsInternalError, ['503']));
      if MessageDialog('�������� � ��������� �'+DocNo, mtConfirmation, [mbYes, mbNo], 0)<>mrYes then eXit;
    end;
    // ���������� � ���������
    ExecSQL('execute procedure Paste_A1('+IntToStr(dm.User.UserId)+', '+taDIELID.AsString+')', quTmp);
    // �������� ���-�� � ������ ��������� ������
    Q := ExecSelectSQL('select sum(Q) from AEl where InvId='+taDIElREF.AsString, quTmp);
    taDIEl.Edit;
    taDIElQ.AsInteger := Q;
    taDIEl.Post;
    ReOpenDataSet(dmMain.taDIAssortInv);
    //taDIEl.Refresh;
  end;
end;

procedure TfmDIEl.acCopyAssortUpdate(Sender: TObject);
var
  b : Variant;
begin
  if FReadOnlyMode then acCopyAssort.Enabled := False
  else begin
    b := ExecSelectSQL('select InvId from Inv where InvId='+IntToStr(dmMain.taDIElREF.AsInteger), dmMain.quTmp);
    acCopyAssort.Enabled := (b <> 0) and not VarIsNull(b);
  end;
end;

procedure TfmDIEl.acPasteAssortUpdate(Sender: TObject);
begin
  if FReadOnlyMode then acPasteAssort.Enabled := False
  else
    acPasteAssort.Enabled := ExecSelectSQL('select count(*) from B_AInv where UserId='+
      IntToStr(dm.User.UserId), dmMain.quTmp)<>0;
end;

procedure TfmDIEl.acDelSemisUpdate(Sender: TObject);
begin
  acDelSemis.Enabled := dmMain.taDIEl.Active and (not dmMain.taDIEl.IsEmpty);
end;

procedure TfmDIEl.acAddSemisUpdate(Sender: TObject);
begin
  acAddSemis.Enabled := dmMain.taDWHSemis.Active and (not dmMain.taDWHSemis.IsEmpty);
end;

procedure TfmDIEl.FilterInvColor(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := dm.taInvColorINVTYPEID.AsInteger = 14;
end;

procedure TfmDIEl.InvColorGetCellParams(Sender: TObject; EditMode: Boolean;
  Params: TColCellParamsEh);
begin
  Params.Background := dm.taInvColorCOLOR.AsInteger;
end;

procedure TfmDIEl.acLogicExecute(Sender: TObject);
begin
  edSN.Enabled := dmMain.taDIListISCLOSE.AsInteger = 0;
  deSDate.Enabled := dmMain.taDIListISCLOSE.AsInteger = 0;
  lcbxUser.Enabled := dmMain.taDIListISCLOSE.AsInteger = 0;
  lcbxJobId.Enabled := dmMain.taDIListISCLOSE.AsInteger = 0;
  lcbxInvColor.Enabled := dmMain.taDIListISCLOSE.AsInteger = 0;
end;

end.

