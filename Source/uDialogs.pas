unit uDialogs;

interface

uses Forms, Dialogs, Controls, Consts, StdCtrls;

function DialogQuestionYesNoMessage(Msg: string): TModalResult;
procedure DialogErrorOkMessage(Msg: string);

implementation

function DialogQuestionYesNoMessage(Msg: string): TModalResult;
var
  Form: TForm;
  Button: TButton;
begin
  if (Msg <> '') and (Msg[Length(Msg)] <> '.') then Msg := Msg + '.';
  Form := CreateMessageDialog(Msg, mtConfirmation, [mbYes, mbNo]);
  Form.Caption := '������';
  Button := TButton(Form.FindChildControl('Yes'));
  Button.Caption := '��';
  Button := TButton(Form.FindChildControl('No'));
  Button.Caption := '���';
  Form.BorderStyle := bsToolWindow;
  Form.Position := poScreenCenter;
  Result := Form.ShowModal;
end;

procedure DialogErrorOkMessage(Msg: string);
var
  Form: TForm;
begin
  if (Msg <> '') and (Msg[Length(Msg)] <> '.') then Msg := Msg + '.';
  Form := CreateMessageDialog(Msg, mtError, [mbOK]);
  Form.Caption := '������';
  Form.BorderStyle := bsToolWindow;
  Form.Position := poScreenCenter;
  Form.ShowModal;
end;


end.
