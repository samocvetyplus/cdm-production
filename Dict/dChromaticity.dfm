inherited fmChromaticity: TfmChromaticity
  Caption = ''
  PixelsPerInch = 96
  TextHeight = 13
  inherited plDict: TPanel
    inherited gridDict: TDBGridEh
      DataSource = dm.dsrChromaticity
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Width = 28
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ID'
          Footers = <>
          Width = 65
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NAME'
          Footers = <>
          Width = 159
        end>
    end
  end
  inherited ilButtons: TImageList
    Left = 162
    Top = 168
  end
  inherited fmstr: TFormStorage
    Left = 108
    Top = 164
  end
end
