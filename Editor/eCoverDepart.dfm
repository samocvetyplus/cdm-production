inherited fmeCoverDepart: TfmeCoverDepart
  Left = 275
  Top = 178
  Caption = #1057#1086#1089#1090#1072#1074' '#1091#1095#1072#1089#1090#1082#1072' '#1089#1098#1077#1084#1072
  ClientHeight = 245
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Height = 245
    object Label1: TLabel [0]
      Left = 2
      Top = 2
      Width = 367
      Height = 13
      Align = alTop
      Caption = '  '#1054#1090#1084#1077#1090#1100#1090#1077' '#1084#1077#1089#1090#1072' '#1093#1088#1072#1085#1077#1085#1080#1103' '#1082#1086#1090#1086#1088#1099#1077' '#1074#1093#1086#1076#1103#1090' '#1074' '#1091#1095#1072#1089#1090#1086#1082' '#1089#1098#1077#1084#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    inherited btnOk: TBitBtn
      Left = 196
      Top = 208
    end
    inherited btnCancel: TBitBtn
      Left = 284
      Top = 208
    end
    object chlistPS: TCheckListBox
      Left = 2
      Top = 15
      Width = 367
      Height = 190
      OnClickCheck = chlistPSClickCheck
      Align = alTop
      Columns = 2
      ItemHeight = 13
      TabOrder = 2
    end
  end
  object taPS: TpFIBDataSet
    Database = dm.db
    Transaction = dm.tr
    SelectSQL.Strings = (
      'select d.DEPID, d.NAME, c.ID COVERID'
      
        'from D_Dep d left join D_PSCOVER c on (d.DEPID=c.DEPID and c.COV' +
        'ERDEPID = :DEPID)'
      'where bit(d.DEPTYPES, 0)=1 and'
      '      bit(d.DEPTYPES, 4)=0'
      'order by SORTIND')
    BeforeOpen = taPSBeforeOpen
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 16
    Top = 24
    poSQLINT64ToBCD = True
    object taPSDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taPSNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taPSCOVERID: TFIBIntegerField
      FieldName = 'COVERID'
    end
  end
end
