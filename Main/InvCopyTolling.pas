unit InvCopyTolling;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh, DBLookupEh,
  DB, FIBDataSet, pFIBDataSet, DBGridEh;

type
  TfmInvCopyTolling = class(TfmEditor)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edDocNo: TDBNumberEditEh;
    edDocDate: TDBDateTimeEditEh;
    lcbxDep: TDBLookupComboboxEh;
    taDep: TpFIBDataSet;
    taDepDEPID: TFIBIntegerField;
    taDepNAME: TFIBStringField;
    dsrDep: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

function ShowInvCopyTolling(MasterIType : integer):boolean;

implementation

uses DictData, dbUtil;

{$R *.dfm}

function ShowInvCopyTolling(MasterIType : integer):boolean;
var
  fmInvCopyTolling: TfmInvCopyTolling;
  DocNo : Variant;
begin
  fmInvCopyTolling := TfmInvCopyTolling.Create(nil);
  try
    case MasterIType of
      13 : begin
        DocNo := ExecSelectSQL('select max(DocNo)+1 from Inv where EYear(DocDate)=EYear("TODAY") and IType='+IntToStr(MasterIType)+' and '+
          'SelfCompId='+IntToStr(dm.User.TollingCompId), dm.quTmp);
        if VarIsNull(DocNo) then DocNo := 1;
      end;
      else raise Exception.Create('ToDo');
    end;
    fmInvCopyTolling.edDocNo.Text := DocNo;
    fmInvCopyTolling.edDocDate.Value := dm.ServerDateTime;
    Result := fmInvCopyTolling.ShowModal = mrOk;
  finally
    FreeAndNil(fmInvCopyTolling);
  end;
end;

procedure TfmInvCopyTolling.FormCreate(Sender: TObject);
begin
  inherited;
  OpenDataSet(taDep);
end;

procedure TfmInvCopyTolling.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(taDep);
  inherited;
end;

end.
