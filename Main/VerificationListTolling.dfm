inherited fmVerificationListTolling: TfmVerificationListTolling
  Caption = 'fmVerificationListTolling'
  ClientHeight = 422
  ClientWidth = 636
  ExplicitWidth = 644
  ExplicitHeight = 456
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 403
    Width = 636
    ExplicitTop = 403
    ExplicitWidth = 636
  end
  inherited tb2: TSpeedBar
    Width = 636
    ExplicitWidth = 636
    inherited laDep: TLabel
      Left = 536
      Top = 6
      Visible = False
      ExplicitLeft = 536
      ExplicitTop = 6
    end
    inherited laPeriod: TLabel
      Visible = False
    end
    object laComp: TLabel [2]
      Left = 16
      Top = 8
      Width = 67
      Height = 13
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      Transparent = True
    end
    object cbComp: TDBComboBoxEh [3]
      Left = 96
      Top = 4
      Width = 161
      Height = 21
      EditButtons = <>
      ShowHint = True
      TabOrder = 0
      Text = 'cbComp'
      Visible = True
      OnChange = cbCompChange
    end
    inherited spitPeriod: TSpeedItem [5]
      Visible = False
    end
    inherited spitDep: TSpeedItem [6]
      Enabled = False
      Visible = False
    end
  end
  inherited tb1: TSpeedBar
    Width = 636
    ExplicitWidth = 636
    inherited spitAdd: TSpeedItem [1]
    end
    inherited spitDel: TSpeedItem [2]
    end
    inherited spitExit: TSpeedItem [3]
      Left = 563
    end
    inherited spitPrint: TSpeedItem [4]
    end
    inherited spitEdit: TSpeedItem [5]
      OnClick = acEditDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    Width = 636
    Height = 332
    DataSource = dmData.dsVerificationListTolling
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
      end
      item
        Alignment = taCenter
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
      end
      item
        Alignment = taCenter
        EditButtons = <>
        FieldName = 'ABD'
        Footers = <>
        Width = 100
      end
      item
        Alignment = taCenter
        EditButtons = <>
        FieldName = 'AED'
        Footers = <>
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'SUPNAME'
        Footers = <>
        Width = 140
      end
      item
        Alignment = taCenter
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
        Width = 115
      end>
  end
  inherited acAEvent: TActionList
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
  end
end
