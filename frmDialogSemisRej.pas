unit frmDialogSemisRej;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxLookAndFeelPainters, StdCtrls,
  cxButtons, ExtCtrls, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxPC, FIBDataSet, DBClient,
  cxGridBandedTableView, cxGridDBBandedTableView, dxBar, pFIBDataSet,
  dxDockControl, dxDockPanel, cxListBox, cxDBEdit, cxLabel, cxContainer,
  cxTextEdit, ActnList, FIBQuery, pFIBQuery, pFIBStoredProc, Provider,
  uSemisStorage, cxLookAndFeels, Menus;

type

  TDialogSemisRej = class(TForm)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    GridSourceLevel1: TcxGridLevel;
    GridSource: TcxGrid;
    PanelBottom: TPanel;
    ButtonOK: TcxButton;
    DataSourceSource: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    StyleReadOnly: TcxStyle;
    DataSetDBSource: TpFIBDataSet;
    DataSetDBSourceOPERATION: TFIBStringField;
    DataSetOperation: TpFIBDataSet;
    DataSetOperationID: TFIBIntegerField;
    DataSetOperationOPERATION: TFIBStringField;
    GridSourceView: TcxGridDBTableView;
    GridSourceViewColumn1: TcxGridDBColumn;
    GridSourceViewColumn2: TcxGridDBColumn;
    GridSourceViewColumn3: TcxGridDBColumn;
    GridTarget: TcxGrid;
    GridTargetView: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    ButtonCancel: TcxButton;
    PanelCenter: TPanel;
    EditorQuantity: TcxTextEdit;
    EditorWeight: TcxTextEdit;
    LabelQuantity: TcxLabel;
    LabelWeight: TcxLabel;
    DataSetTarget: TClientDataSet;
    DataSourceTarget: TDataSource;
    DataSetTargetOperation: TStringField;
    DataSetTargetQuantity: TIntegerField;
    DataSetTargetWeight: TFloatField;
    DataSetTargetOperationID: TIntegerField;
    GridTargetViewOperation: TcxGridDBColumn;
    GridTargetViewQuantity: TcxGridDBColumn;
    GridTargetViewWeight: TcxGridDBColumn;
    ButtonDelete: TcxButton;
    ButtonInsert: TcxButton;
    Bevel1: TBevel;
    ButtonSourceTitle1: TcxButton;
    ButtonTargetTitle1: TcxButton;
    ButtonSourceTitle2: TcxButton;
    ButtonTargetTitle2: TcxButton;
    ActionList1: TActionList;
    ActionInsert: TAction;
    ActionDelete: TAction;
    ActionOK: TAction;
    DataSetTargetSummary: TClientDataSet;
    DataSetTargetSummaryQuantity: TIntegerField;
    DataSetTargetSummaryWeight: TFloatField;
    DataSetTargetUnitQuantity: TIntegerField;
    Query: TpFIBQuery;
    StoredProc: TpFIBStoredProc;
    DataSetDBSourceOPERATIONID: TFIBIntegerField;
    DataSetDBSourceWEIGHT: TFIBFloatField;
    DataSetDBSourceQUANTITY: TFIBIntegerField;
    DataSetSourceProvider: TDataSetProvider;
    DataSetSource: TClientDataSet;
    DataSetSourceOPERATIONID: TIntegerField;
    DataSetSourceOPERATION: TStringField;
    DataSetSourceQUANTITY: TIntegerField;
    DataSetSourceWEIGHT: TFloatField;
    DataSetDBTarget: TpFIBDataSet;
    DataSetTargetProvider: TDataSetProvider;
    DataSetSourceSummary: TClientDataSet;
    DataSetDBTargetOPERATIONID: TFIBIntegerField;
    DataSetDBTargetQUANTITY: TFIBIntegerField;
    DataSetDBTargetWEIGHT: TFIBFloatField;
    DataSetDBTargetUNITQUANTITY: TFIBIntegerField;
    DataSetDBTargetUNITWEIGHT: TFIBIntegerField;
    DataSetTargetUnitWeight: TIntegerField;
    procedure DataSetDBSourceBeforeOpen(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
    procedure ActionInsertUpdate(Sender: TObject);
    procedure ActionInsertExecute(Sender: TObject);
    procedure ActionDeleteUpdate(Sender: TObject);
    procedure GridSourceViewDblClick(Sender: TObject);
    procedure ActionDeleteExecute(Sender: TObject);
    procedure GridTargetViewDblClick(Sender: TObject);
    procedure ActionOKExecute(Sender: TObject);
    procedure ActionOKUpdate(Sender: TObject);
    procedure DataSetTargetNewRecord(DataSet: TDataSet);
    procedure DataSetDBTargetBeforeOpen(DataSet: TDataSet);
    procedure GridSourceEnter(Sender: TObject);
    procedure GridTargetEnter(Sender: TObject);
  private
    FSource: TStorage;
    FTarget: Tstorage;
    FMode: TDialogSemisMode;
    FInvoiceElementID: Integer;
    FAll: Boolean;
  public
    function Execute(ASource: TStorage; ATarget: TStorage; Mode: TDialogSemisMode; InvoiceElementID: Integer = 0): Boolean;
    class procedure Delta(SpoilageDelta: TSpoilageDelta);
    property Source: TStorage read FSource;
    property Target: TStorage read FTarget;
    property All: Boolean read FAll write FAll;
  end;

var
  DialogSemisRej: TDialogSemisRej;

implementation

uses DictData, math, StrUtils, MainData;

{$R *.dfm}

{ TDialogSemis }

function TDialogSemisRej.Execute(ASource: TStorage; ATarget: TStorage; Mode: TDialogSemisMode; InvoiceElementID: Integer = 0): Boolean;
var
  DeltaQ: Integer;
  DeltaW: Double;


procedure PrepareTargetSummary;
var
  Quantity: Integer;
  Weight: Double;
begin
  DataSetTarget.First;
  while not DataSetTarget.Eof do
  begin
    Quantity := DataSetTargetQuantity.AsInteger;
    Weight := DataSetTargetWeight.AsFloat;
    if not DataSetTargetSummary.IsEmpty then
    begin
      DataSetTargetSummary.Edit;
      DataSetTargetSummaryQuantity.AsInteger := DataSetTargetSummaryQuantity.AsInteger + Quantity;
      DataSetTargetSummaryWeight.AsFloat := Round((DataSetTargetSummaryWeight.AsFloat + Weight) * 1000) / 1000;
      DataSetTargetSummary.Post;
    end
    else
    begin
      DataSetTargetSummary.Append;
      DataSetTargetSummaryQuantity.AsInteger := Quantity;
      DataSetTargetSummaryWeight.AsFloat := Weight;
      DataSetTargetSummary.Post;
    end;
    DataSetTarget.Next;
  end;
end;

procedure PrepareSummary;
begin
  PrepareTargetSummary;
  DataSetSourceSummary.Data := DataSetSource.Data;
end;


begin
  FSource := ASource;
  if Source <> nil then Source.Connected := True;
  FTarget := ATarget;
  if Target <> nil then Target.Connected := True;

  FMode := Mode;
  FInvoiceElementID := InvoiceElementID;

  if Source.Connected and Target.Connected then
  begin
    Source.Element.Connected := True;

    Target.Element.Connected := True;
    if Source.Element.Connected and Target.Element.Connected then
    begin
      ButtonSourceTitle1.Caption := Source.Title;
      ButtonSourceTitle2.Caption := Source.Title;
      ButtonTargetTitle1.Caption := Target.Title;
      ButtonTargetTitle2.Caption := Target.Title;

      DataSetSource.Active := False;

      DataSetTarget.Active := False;
      DataSetTargetSummary.Active := False;
      DataSetSource.Active := False;
      DataSetSourceSummary.Active := False;
      if Mode = smOut then
      begin
        ActiveControl := GridSource;
        DataSetTarget.ProviderName := '';
        DataSetSource.Active := True;
        DataSetTarget.CreateDataSet;
        Caption := '���������� �������������� �� �������� �� ������� �����';
      end
      else
      if Mode = smIn then
      begin
        ActiveControl := GridTarget;        
        DataSetSource.ProviderName := '';
        DataSetSource.CreateDataSet;
        DataSetTarget.Active := True;
        Caption := '�������� �������������� � �������� ����� � ��������';
      end;
      if not All then
      begin
        Result := ShowModal = mrOK;
      end
      else
      begin
        if Mode = SmIn then
        begin
          DataSetTarget.First;
          while not DataSetTarget.Eof do
          begin
            GridTargetViewDblClick(nil);
            ButtonDelete.Click;
            DataSetTarget.First;
          end;
        end
        else Result := False;
      end;
      if Result then
      begin
         //DataSetSourceSummary.CreateDataSet;
         DataSetTargetSummary.CreateDataSet;
         PrepareSummary;
      end;
    end
    else Result := False;
  end
  else Result := False;
end;

procedure TDialogSemisRej.DataSetDBSourceBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('storage$element$id').AsInteger := Source.Element.ID;
end;

procedure TDialogSemisRej.FormDestroy(Sender: TObject);
begin
  DataSetSource.Active := False;
end;

procedure TDialogSemisRej.ActionInsertExecute(Sender: TObject);
var
  Weight: Extended;
  DeltaWeight: Double;
  Quantity: Integer;
  OperationID: Integer;
begin
  Weight := StrToFloat(EditorWeight.Text);
  DeltaWeight := Round(Weight * 1000) / 1000;

  Quantity := StrToInt(EditorQuantity.Text);
  OperationID := DataSetSourceOPERATIONID.AsInteger;


  if DataSetTarget.Locate('Operation$ID', OperationID, []) then
  begin
    DataSetTarget.Edit;
    DataSetTargetOperationID.AsInteger := OperationID;
    DataSetTargetWeight.AsFloat := Round((DataSetTargetWeight.AsFloat + DeltaWeight) * 1000) / 1000;
    DataSetTargetQuantity.AsInteger := DataSetTargetQuantity.AsInteger + Quantity;
    DataSetTarget.Post;
    DataSetSource.Edit;
    DataSetSourceWEIGHT.AsFloat :=  Round((DataSetSourceWEIGHT.AsFloat - DeltaWeight) * 1000) / 1000;
    DataSetSourceQUANTITY.AsInteger := DataSetSourceQUANTITY.AsInteger - Quantity;
    DataSetSource.Post;
  end
  else
  begin
    DataSetTarget.Append;
    DataSetTargetOperationID.AsInteger := OperationID;
    DataSetTargetWeight.AsFloat := DeltaWeight;
    DataSetTargetQuantity.AsInteger := Quantity;
    DataSetTarget.Post;
    DataSetSource.Edit;
    DataSetSourceWEIGHT.AsFloat :=  Round((DataSetSourceWEIGHT.AsFloat - DeltaWeight)*1000) / 1000;
    DataSetSourceQUANTITY.AsInteger := DataSetSourceQUANTITY.AsInteger - Quantity;
    DataSetSource.Post;
  end;

  EditorQuantity.Text := DataSetSourceQUANTITY.AsString;
  EditorWeight.Text := DataSetSourceWEIGHT.AsString;

end;


procedure TDialogSemisRej.ActionInsertUpdate(Sender: TObject);
var
  Enabled: Boolean;
  Weight: Extended;
  Quantity: Integer;
begin
  Enabled := True;

  if Enabled then
  Enabled := DataSetSource.Active and (DataSetSource.RecordCount <> 0);

  if Enabled then
  Enabled := TryStrToInt(EditorQuantity.Text, Quantity);
  if Enabled then
  Enabled := (Quantity >= 0);

  if Enabled then
  Enabled := TryStrToFloat(EditorWeight.Text, Weight);

  if Enabled then
    Enabled := (Weight >= 0);

  if Enabled then
  begin
    Enabled := not ((Quantity = 0) and (Weight = 0))
  end;
  ActionInsert.Enabled := Enabled;
end;


procedure TDialogSemisRej.ActionDeleteUpdate(Sender: TObject);
var
  Enabled: Boolean;
  Weight: Extended;
  DeltaWeight: Double;
  TargetWeight: Double;
  Quantity: Integer;
begin
  Enabled := True;

  if Enabled then
  Enabled := DataSetTarget.Active and (DataSetTarget.RecordCount <> 0);

  if Enabled then
  Enabled := TryStrToInt(EditorQuantity.Text, Quantity);
  if Enabled then
  Enabled := (Quantity >= 0);

  if Enabled then
  Enabled := TryStrToFloat(EditorWeight.Text, Weight);
  if Enabled then
  Enabled := (Weight >= 0);

  if Enabled then
  begin
    Enabled := not ((Quantity = 0) and (Weight = 0))
  end;

  ActionDelete.Enabled := Enabled;
end;
procedure TDialogSemisRej.GridSourceViewDblClick(Sender: TObject);
begin
  if DataSetSource.Active and (DataSetSource.RecordCount <> 0) then
  begin
    EditorQuantity.Text := DataSetSourceQUANTITY.AsString;
    EditorWeight.Text := DataSetSourceWEIGHT.AsString;
  end
  else
  begin
    EditorQuantity.Text := '0';
    EditorWeight.Text := '0';
  end;
end;

procedure TDialogSemisRej.ActionDeleteExecute(Sender: TObject);
var
  Weight: Double;
  Quantity: Integer;
  OperationID: Integer;
  DeltaWeight: Double;
begin
  Weight := StrToFloat(EditorWeight.Text);
  DeltaWeight := Round(Weight * 1000) / 1000;

  Quantity := StrToInt(EditorQuantity.Text);

  OperationID := DataSetTargetOperationID.AsInteger;

  DataSetTarget.Edit;
  DataSetTargetWeight.AsFloat := Round((DataSetTargetWeight.AsFloat - DeltaWeight) * 1000) / 1000;
  DataSetTargetQuantity.AsInteger := DataSetTargetQuantity.AsInteger - Quantity;
  DataSetTarget.Post;

  if (DataSetTargetWeight.AsFloat = 0) and (DataSetTargetQuantity.AsInteger = 0) then
  begin
    EditorQuantity.Text := '0';
    EditorWeight.Text := '0';
    DataSetTarget.Delete;
  end
  else
  begin
    EditorQuantity.Text := DataSetSourceQUANTITY.AsString;
    EditorWeight.Text := DataSetSourceWEIGHT.AsString;
  end;

  DataSetSource.Locate('Operation$ID', OperationID, []);
  DataSetSource.Edit;
  DataSetSourceWEIGHT.AsFloat :=  Round((DataSetSourceWEIGHT.AsFloat + DeltaWeight)*1000) / 1000;
  DataSetSourceQUANTITY.AsInteger := DataSetSourceQUANTITY.AsInteger + Quantity;
  DataSetSource.Post;
end;

procedure TDialogSemisRej.GridTargetViewDblClick(Sender: TObject);
begin
 if DataSetTarget.Active and (DataSetTarget.RecordCount <> 0) then
  begin
    EditorQuantity.Text := DataSetTargetQUANTITY.AsString;
    EditorWeight.Text := DataSetTargetWEIGHT.AsString;
  end
  else
  begin
    EditorQuantity.Text := '0';
    EditorWeight.Text := '0';
  end;
end;

procedure TDialogSemisRej.ActionOKExecute(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TDialogSemisRej.ActionOKUpdate(Sender: TObject);
begin
  ActionOK.Enabled := ( (FMode = smOut) and not DataSetTarget.IsEmpty) or ( (FMode = smIn) and not DataSetSource.IsEmpty);
end;

procedure TDialogSemisRej.DataSetTargetNewRecord(DataSet: TDataSet);
begin
  DataSetTargetUnitQuantity.AsInteger := Source.Element.UnitQuantity;
  DataSetTargetUnitWeight.AsInteger := Source.Element.UnitWeight;
end;

procedure TDialogSemisRej.DataSetDBTargetBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('in$storage$element$id').AsInteger := Target.Element.ID;
  TpFIBDataSet(DataSet).ParamByName('invoice$element$id').AsInteger := FInvoiceElementID;
end;

class procedure TDialogSemisRej.Delta(SpoilageDelta: TSpoilageDelta);
var
  StoredProc: TpFIBStoredProc;
begin
  StoredProc := TpFIBStoredProc.Create(nil);
  StoredProc.Database := dm.db;
  StoredProc.Transaction := dm.tr;
  StoredProc.StoredProcName := 'SPOILAGE$DELTA';
  StoredProc.ParamByName('STORAGE$ELEMENT$ID').AsInteger := SpoilageDelta.StorageElementID;
  StoredProc.ParamByName('OPERATION$ID').AsInteger := SpoilageDelta.OperationID;
  StoredProc.ParamByName('WEIGHT').AsFloat := SpoilageDelta.Weight;
  StoredProc.ParamByName('QUANTITY').AsInteger := SpoilageDelta.Quantity;
  StoredProc.ParamByName('INVOICE$ELEMENT$ID').AsInteger := SpoilageDelta.InvoiceElementID;
  StoredProc.ExecProc;
  StoredProc.Transaction.CommitRetaining;
  StoredProc.Free;
end;


procedure TDialogSemisRej.GridSourceEnter(Sender: TObject);
begin
  GridTargetView.Styles.Background := StyleReadOnly;
  GridTargetView.Styles.Content := StyleReadOnly;
  GridTargetView.Styles.Group := StyleReadOnly;
  GridSourceView.Styles.Background := nil;
  GridSourceView.Styles.Content := nil;
  GridSourceView.Styles.Group := nil;
  ButtonInsert.Action := ActionInsert;  
end;

procedure TDialogSemisRej.GridTargetEnter(Sender: TObject);
begin
  GridTargetView.Styles.Background := nil;
  GridTargetView.Styles.Content := nil;
  GridTargetView.Styles.Group := nil;
  GridSourceView.Styles.Background := StyleReadOnly;
  GridSourceView.Styles.Content := StyleReadOnly;
  GridSourceView.Styles.Group := StyleReadOnly;
  ButtonInsert.Action := ActionDelete;
end;

end.
