unit ActWorkList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor,  TB2Item, ActnList, Menus, 
  ImgList, Grids, DBGridEh, StdCtrls, ExtCtrls, ComCtrls, Mask,
  DBCtrlsEh, DBLookupEh, Db, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmActWorkList = class(TfmListAncestor)
    lbYear: TLabel;
    edYear: TDBNumberEditEh;
    lbComp: TLabel;
    cbComp: TDBComboBoxEh;
    acShowID: TAction;
    ppPrint: TTBPopupMenu;
    acPrintProdTolling: TAction;
    acPrintActWork: TAction;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    acPrintActWorkGenaral: TAction;
    TBItem7: TTBItem;
    acReCalcAct: TAction;
    TBItem8: TTBItem;
    acPrintInvMX18: TAction;
    TBItem9: TTBItem;
    acPrintActRet: TAction;
    TBItem10: TTBItem;
    acPrintAWRej: TAction;
    TBItem11: TTBItem;
    TBItem12: TTBItem;
    acPrintAWTorg12: TAction;
    acReCalcTW: TAction;
    TBItem13: TTBItem;
    TBItem14: TTBItem;
    acReCalcActGold: TAction;
    TBItem15: TTBItem;
    acReCalcActGoldAll: TAction;
    TBItem16: TTBItem;
    TBItem17: TTBItem;
    TBItem18: TTBItem;
    acCalcMatByUIDAll: TAction;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem19: TTBItem;
    TBItem20: TTBItem;
    acCloseAllInThisPeriod: TAction;
    procedure edYearChange(Sender: TObject);
    procedure edYearKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure cbCompChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acShowIDExecute(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintDocExecute(Sender: TObject);
    procedure acPrintProdTollingExecute(Sender: TObject);
    procedure acPrintActWorkExecute(Sender: TObject);
    procedure acPrintActWorkUpdate(Sender: TObject);
    procedure acPrintProdTollingUpdate(Sender: TObject);
    procedure acPrintActWorkGenaralExecute(Sender: TObject);
    procedure acReCalcActUpdate(Sender: TObject);
    procedure acReCalcActExecute(Sender: TObject);
    procedure acPrintInvMX18Execute(Sender: TObject);
    procedure acPrintInvMX18Update(Sender: TObject);
    procedure acPrintActRetExecute(Sender: TObject);
    procedure acPrintActRetUpdate(Sender: TObject);
    procedure acPrintAWRejExecute(Sender: TObject);
    procedure acPrintAWRejUpdate(Sender: TObject);
    procedure acPrintAWTorg12Execute(Sender: TObject);
    procedure acPrintAWTorg12Update(Sender: TObject);
    procedure acReCalcTWExecute(Sender: TObject);
    procedure acReCalcTWUpdate(Sender: TObject);
    procedure acReCalcActGoldExecute(Sender: TObject);
    procedure acReCalcActGoldUpdate(Sender: TObject);
    procedure acReCalcActGoldAllUpdate(Sender: TObject);
    procedure acReCalcActGoldAllExecute(Sender: TObject);
    procedure TBItem16Click(Sender: TObject);
    procedure TBItem17Click(Sender: TObject);
    procedure TBItem18Click(Sender: TObject);
    procedure acCalcMatByUIDAllExecute(Sender: TObject);
    procedure acCloseAllInThisPeriodExecute(Sender: TObject);
  private
    procedure FilterActWorkList(DataSet: TDataSet; var Accept: boolean);
  end;

var
  fmActWorkList: TfmActWorkList;

implementation

uses Data, MainData, dbUtil, DictData, dbTree, ActWork, fmUtils, DocAncestor, MsgDialog,
  InvData, PrintData, ProductionConsts, PrintData2;

{$R *.dfm}

procedure TfmActWorkList.edYearChange(Sender: TObject);
begin
  dmMain.DocListYear := edYear.Value;
  ReOpenDataSet(DataSet);
end;

procedure TfmActWorkList.edYearKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : begin
      dmMain.DocListYear := edYear.Value;
      ReOpenDataSet(DataSet);
    end;
  end;
end;

procedure TfmActWorkList.FormCreate(Sender: TObject);
var
  nd: TNodeData;
begin
  gridDocList.DataSource := dmData.dsrActWorkList;
  dmInv.IType := 21;
  dm.SchemaId := 1;
  (* ���������� ��� *)
  edYear.OnChange := nil;
  edYear.Value := dmMain.DocListYear;
  edYear.OnChange := edYearChange;
  (* ��������� ������ �� ����������� *)
  cbComp.OnChange := nil;
  cbComp.Items.Clear;
  nd := TNodeData.Create;
  nd.Code := -1;
  cbComp.Items.AddObject('*���', nd);
  OpenDataSet(dm.taCompLogin);
  while not dm.taCompLogin.Eof do
  begin
    if (dm.User.SelfCompId <> dm.taCompLoginCOMPID.AsInteger) then
    begin
      nd := TNodeData.Create;
      nd.Code := dm.taCompLoginCOMPID.AsInteger;
      cbComp.Items.AddObject(dm.taCompLoginNAME.AsString, nd);
    end;
    dm.taCompLogin.Next;
  end;
  CloseDataSet(dm.taCompLogin);
  cbComp.ItemIndex := 0;
  cbComp.OnChange := cbCompChange;
  dmMain.FilterCompId := IntToStr(ROOT_COMP_CODE);
  DataSet.OnFilterRecord := FilterActWorkList;
  DataSet.Filtered := True;
  //ppPrint.Skin := dm.TBSkin;
  inherited;
  gridDocList.FieldColumns['INVID'].Visible := False;
  gridDocList.FieldColumns['ISCLOSE'].Visible := False;
end;

procedure TfmActWorkList.cbCompChange(Sender: TObject);
begin
  if(cbComp.ItemIndex<=0)then dmMain.FilterCompId := IntToStr(ROOT_COMP_CODE)
  else with cbComp do
    dmMain.FilterCompId := IntToStr(TNodeData(Items.Objects[ItemIndex]).Code);
  DataSet.Filtered := False;
  DataSet.Filtered := True;
  gridDocList.SumList.RecalcAll;
  //ShowMessage('CompID' +IntToStr(TNodeData(cbComp.Items.Objects[cbComp.ItemIndex]).Code));
end;

procedure TfmActWorkList.FilterActWorkList(DataSet: TDataSet; var Accept: boolean);
begin
  Accept := (dmMain.FilterCompId = IntToStr(ROOT_COMP_CODE)) or (dmMain.FilterCompId = DataSet.FieldByName('COMPID').AsString);
end;

procedure TfmActWorkList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  DataSet.OnFilterRecord := nil;
  DataSet.Filtered := False;
end;

procedure TfmActWorkList.acAddDocExecute(Sender: TObject);
begin
  if (cbComp.ItemIndex<=0) then
  begin
    MessageDialogA(rsNotDefineCompId, mtWarning);
    eXit;
  end;
  with dmData do
  begin
    DataSet.Append;
    DataSet.FieldByName('COMPID').AsInteger := TNodeData(cbComp.Items.Objects[cbComp.ItemIndex]).Code;
    PostDataSet(taActWorkList);
    ShowDocForm(TfmActWork, dmData.taActWorkList, dmData.taActWorkListINVID.AsInteger, dm.quTmp);
    PostDataSet(taActWorkList);
    RefreshDataSet(taActWorkList);
  end;
end;

procedure TfmActWorkList.acCalcMatByUIDAllExecute(Sender: TObject);
var Bookmark: Pointer;
begin
  inherited;
  Bookmark := dmData.taActWork.GetBookmark;
  with dmData.taActWorkList do
    begin
      if State in [dsEdit, dsInsert] then
        begin
          Post;
          Transaction.CommitRetaining;
        end;
      First;
      while not EOF do
        begin
          ExecSQL('execute procedure Calc_ServMatByUID('+dmData.taActWorkListINVID.AsString+')', dm.quTmp);
          Next;
        end;
      ReOpenDataSet(dmData.dsrServItem.DataSet);
    end;
  dmData.taActWorkList.GotoBookmark(Bookmark);
end;

procedure TfmActWorkList.acCloseAllInThisPeriodExecute(Sender: TObject);
var
  Bookmark: Pointer;
begin
  inherited;
  Screen.Cursor := crSQLWait;
  //i := 0;
  Bookmark := dmData.taActWorkList.GetBookmark;
  with dmData.taActWorkList do
    begin
      if State in [dsEdit, dsInsert] then
        begin
          Post;
          Transaction.CommitRetaining;
        end;
      Filter := 'ISCLOSE = 0';
      Filtered := true;
      if IsEmpty then
        begin
          ShowMessage('� ��������� ������� ��� �������� �����!');
          Filter := '';
          Filtered := false;
          eXit;
        end;
      First;
      while not EOF do
        begin
          ExecSQL('execute procedure CloseDoc2('#39+'��� ����������� �����'+#39','+dmData.taActWorkListINVID.AsString+ ')', dm.quTmp);
          Next;
          //inc(i);
        end;
      //ShowMessage('����������:' + inttostr(i));
      Close;
      Filter := '';
      Filtered := false;
      Open;
    end;
  dmData.taActWorkList.GotoBookmark(Bookmark);
  Screen.Cursor := crDefault;
end;

procedure TfmActWorkList.acEditDocExecute(Sender: TObject);
begin
  with dmData do
  begin
    taActWorkList.Edit;
    ShowDocForm(TfmActWork, dmData.taActWorkList, dmData.taActWorkListINVID.AsInteger, dm.quTmp);
    taActWorkList.Transaction.CommitRetaining;
    RefreshDataSet(taActWorkList);
  end;
end;

procedure TfmActWorkList.acShowIDExecute(Sender: TObject);
begin
  gridDocList.FieldColumns['INVID'].Visible := not gridDocList.FieldColumns['INVID'].Visible;
  gridDocList.FieldColumns['ISCLOSE'].Visible := not gridDocList.FieldColumns['ISCLOSE'].Visible;
end;

procedure TfmActWorkList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if DataSet.FieldByName('ISCLOSE').AsInteger = 1 then Background := clMenuBar
  else Background := clWindow;
end;

procedure TfmActWorkList.acPrintDocExecute(Sender: TObject);
begin
  //
end;

procedure TfmActWorkList.acPrintProdTollingExecute(Sender: TObject);
begin
  PostDataSet(dmData.taActWorkList);
  dmPrint.PrintDocumentA(dkProdTolling, VarArrayOf([VarArrayOf([
       dmData.taActWorkListINVID.AsInteger])]));
  CloseDataSet(dmData.taActWork);
end;

procedure TfmActWorkList.acPrintActWorkExecute(Sender: TObject);
begin
  PostDataSet(dmData.taActWorkList);
  dmPrint.PrintDocumentA(dkActWork, VarArrayOf([VarArrayOf([
       dmData.taActWorkListINVID.AsInteger])]));
  CloseDataSet(dmData.taActWork);
end;

procedure TfmActWorkList.acPrintActWorkUpdate(Sender: TObject);
begin
  acPrintActWork.Enabled := dmData.taActWorkList.Active and (not dmData.taActWorkList.IsEmpty) and
    (dmData.taActWorkListINVSUBTYPEID.AsInteger=10);
end;

procedure TfmActWorkList.acPrintProdTollingUpdate(Sender: TObject);
begin
  acPrintProdTolling.Enabled := dmData.taActWorkList.Active and (not dmData.taActWorkList.IsEmpty) and
    (dmData.taActWorkListINVSUBTYPEID.AsInteger = 10);
end;

procedure TfmActWorkList.acPrintActWorkGenaralExecute(Sender: TObject);
var
  SupId1 : integer;
  SupId2 : integer;
  SupName : string;
begin
  PostDataSet(dmData.taActWorkList);
  SupName := cbComp.Text;
  if cbComp.ItemIndex > 0 then
  begin
    SupId1 := TNodeData(cbComp.Items.Objects[cbComp.ItemIndex]).Code;
    SupId2 := TNodeData(cbComp.Items.Objects[cbComp.ItemIndex]).Code;
  end
  else begin
    SupId1 := -MAXINT;
    SupId2 :=  MAXINT;
  end;
  dmPrint.PrintDocumentA(dkActWorkGeneral, VarArrayOf([
     VarArrayOf([dm.BeginDate, dm.EndDate, SupId1, SupId2]),
     VarArrayOf([dm.BeginDate, dm.EndDate, dm.User.SelfCompId]),
     VarArrayOf([dm.BeginDate, dm.EndDate, dm.User.SelfCompName, SupName])
     ]));
end;

procedure TfmActWorkList.acReCalcActUpdate(Sender: TObject);
begin
  acReCalcAct.Visible := (dm.User.Profile = -1);
  if acReCalcAct.Visible then
    acReCalcAct.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmActWorkList.acReCalcActExecute(Sender: TObject);
begin
  ExecSQL('execute procedure RECALC_ACTWORK('+dmData.taActWorkListINVID.AsString+')', dm.quTmp);
end;

procedure TfmActWorkList.acPrintInvMX18Execute(Sender: TObject);
begin
  PostDataSet(dmData.taActWorkList);
  dmPrint.PrintDocumentA(dkInvMX18, VarArrayOf([
    VarArrayOf([dmData.taActWorkListINVID.AsInteger]),
    VarArrayOf([dmData.taActWorkListINVID.AsInteger])
    ]));
end;

procedure TfmActWorkList.acPrintInvMX18Update(Sender: TObject);
begin
  acPrintInvMX18.Enabled := DataSet.Active and (not DataSet.IsEmpty)and
    (dmData.taActWorkListINVSUBTYPEID.AsInteger=10);
end;

procedure TfmActWorkList.acPrintActRetExecute(Sender: TObject);
begin
  PostDataSet(DataSet);
  dmPrint.PrintDocumentA(dkActWorkRetServ, VarArrayOf([
    VarArrayOf([dmData.taActWorkListINVID.AsInteger])
    ]));
end;

procedure TfmActWorkList.acPrintActRetUpdate(Sender: TObject);
begin
  acPrintActRet.Enabled := (dmData.taActWorkList.Active) and (not dmData.taActWorkList.IsEmpty) and
    (dmData.taActWorkListINVSUBTYPEID.AsInteger = 11);
end;

procedure TfmActWorkList.acPrintAWRejExecute(Sender: TObject);
begin
  PostDataSet(DataSet);
  dmPrint.PrintDocumentA(dkActWorkRej, VarArrayOf([
    VarArrayOf([dmData.taActWorkListINVID.AsInteger])
    ]));
end;

procedure TfmActWorkList.acPrintAWRejUpdate(Sender: TObject);
begin
  acPrintAWRej.Enabled := DataSet.Active and (not DataSet.IsEmpty) and
    (dmData.taActWorkListINVSUBTYPEID.AsInteger = 11);
end;

procedure TfmActWorkList.acPrintAWTorg12Execute(Sender: TObject);
begin
  PostDataSet(DataSet);
  dmPrint.PrintDocumentA(dkAWTorg12, VarArrayOf([
    VarArrayOf([dmData.taActWorkListINVID.AsInteger]),
    VarArrayOf([dmData.taActWorkListINVID.AsInteger])
    ]));
end;

procedure TfmActWorkList.acPrintAWTorg12Update(Sender: TObject);
begin
  acPrintAWTorg12.Enabled := DataSet.Active and (not DataSet.IsEmpty) and
    (dmData.taActWorkListINVSUBTYPEID.AsInteger = 11);
end;

procedure TfmActWorkList.acReCalcTWExecute(Sender: TObject);
var
  b : Pointer;
  Ref : string;
begin
  b := dmData.taActWorkList.GetBookmark;
  dmData.taActWorkList.DisableControls;
  dmData.taActWorkList.DisableScrollEvents;
  try
    dmData.taActWorkList.First;
    while not dmData.taActWorkList.Eof do
    begin
      if not dmData.taActWorkListREFINVID.IsNull then
      begin
        Ref := dmData.taActWorkListREFINVID.AsString;
        ExecSQL('update Inv set W=(select W from inv_sum('+Ref+')) where InvId='+Ref, dm.quTmp);
      end;
      dmData.taActWorkList.Next;
    end
  finally
    dmData.taActWorkList.GotoBookmark(b);
    dmData.taActWorkList.EnableScrollEvents;
    dmData.taActWorkList.EnableControls;
  end;
end;

procedure TfmActWorkList.acReCalcTWUpdate(Sender: TObject);
begin
  acReCalcTW.Visible := (dm.User.Profile = -1);
  if acReCalcTW.Visible then
    acReCalcTW.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmActWorkList.acReCalcActGoldExecute(Sender: TObject);
begin
  ExecSQL('execute procedure RECALC_ACTWORKGOLD('+dmData.taActWorkListINVID.AsString+')', dm.quTmp);
  dmData.taActWorkList.Refresh;
end;

procedure TfmActWorkList.acReCalcActGoldUpdate(Sender: TObject);
begin
  acReCalcActGold.Visible := (dm.User.Profile = -1);
  if acReCalcActGold.Visible then
    acReCalcActGold.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmActWorkList.acReCalcActGoldAllUpdate(Sender: TObject);
begin
  acReCalcActGoldAll.Visible := (dm.User.Profile = -1);
end;

procedure TfmActWorkList.acReCalcActGoldAllExecute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  Bookmark := dmData.taActWorkList.GetBookmark;
  dmData.taActWorkList.First;
  while not dmData.taActWorkList.Eof do
  begin
    acReCalcActGold.Execute;
    dmData.taActWorkList.Next;
  end;
  dmData.taActWorkList.GotoBookmark(Bookmark);
end;

procedure TfmActWorkList.TBItem16Click(Sender: TObject);
begin
if dmMain.FilterCompId <> '-1' then
  begin
    PostDataSet(dmData.taActWorkList);
    dmPrint.PrintDocumentA(dkActWorkCommon, VarArrayOf([VarArrayOf([
       dmData.taActWorkListINVID.AsInteger])]));
    CloseDataSet(dmData.taActWorkCommon);
    CloseDataSet(dmData.taActWorkListCommon);
  end
else MessageDialog('�� ������� �����������!', mtWarning, [mbOk], 0);
end;

procedure TfmActWorkList.TBItem17Click(Sender: TObject);
begin
if dmMain.FilterCompId <> '-1' then
  begin
    PostDataSet(dmData.taActWorkList);
    dmPrint.PrintDocumentA(dkProdTollingCommon, VarArrayOf([VarArrayOf([
       dmData.taActWorkListINVID.AsInteger])]));
    CloseDataSet(dmData.taActWorkCommon);
    CloseDataSet(dmData.taActWorkListCommon);
  end
else MessageDialog('�� ������� �����������!', mtWarning, [mbOk], 0);
end;

procedure TfmActWorkList.TBItem18Click(Sender: TObject);
begin
  if dmMain.FilterCompId = '-1' then
    begin
      MessageDialog('�� ������� �����������!', mtWarning, [mbOK], 0);
      Exit;
    end;
  PostDataSet(dmData.taActWorkList);
  dmPrint.PrintDocumentA(dkInvMX18Common, VarArrayOf([
    VarArrayOf([dmData.taActWorkListINVID.AsInteger]),
    VarArrayOf([dmData.taActWorkListINVID.AsInteger])
    ]));
  if dmPrint2.taMX18ItemCommon.Active then dmPrint2.taMX18ItemCommon.Close;
end;

end.
