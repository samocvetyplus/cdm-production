unit frmAffiangePassportSet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridLevel, cxGrid, ImgList,
  ExtCtrls, dxsbar, cxPC, dxStatusBar, ActnList, dxBar, FIBDataSet,
  pFIBDataSet, FIBDatabase, pFIBDatabase;

type
  TDialogAffinagePassportSet = class(TForm)
    StatusBar: TdxStatusBar;
    Pages: TcxPageControl;
    SheetPassports: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    ImageList: TImageList;
    GridPassportsLevel1: TcxGridLevel;
    GridPassports: TcxGrid;
    GridPassportsDBBandedTableView1: TcxGridDBBandedTableView;
    BarManager: TdxBarManager;
    BarButtonAppend: TdxBarButton;
    dxBarButton2: TdxBarButton;
    Actions: TActionList;
    ActionAppend: TAction;
    ActionDelete: TAction;
    ActionOpen: TAction;
    ActionClose: TAction;
    dxBarButton1: TdxBarButton;
    ActionView: TAction;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    GridPassportsDBBandedTableView1DESIGNATION: TcxGridDBBandedColumn;
    GridPassportsDBBandedTableView1PERIODBEGIN: TcxGridDBBandedColumn;
    GridPassportsDBBandedTableView1PERIODEND: TcxGridDBBandedColumn;
    GridPassportsDBBandedTableView1CREATED: TcxGridDBBandedColumn;
    GridPassportsDBBandedTableView1CREATEDBY: TcxGridDBBandedColumn;
    GridPassportsDBBandedTableView1STATE: TcxGridDBBandedColumn;
    GridPassportsDBBandedTableView1STATEBY: TcxGridDBBandedColumn;
    GridPassportsDBBandedTableView1STATEDATE: TcxGridDBBandedColumn;
    BarDockControl: TdxBarDockControl;
    dxBarButton5: TdxBarButton;
    DataSetPassportSet: TpFIBDataSet;
    DataSetPassportSetID: TFIBIntegerField;
    DataSetPassportSetDESIGNATION: TFIBIntegerField;
    DataSetPassportSetPERIODBEGIN: TFIBDateTimeField;
    DataSetPassportSetPERIODEND: TFIBDateTimeField;
    DataSetPassportSetCREATED: TFIBDateTimeField;
    DataSetPassportSetCREATEDBY: TFIBIntegerField;
    DataSetPassportSetSTATE: TFIBIntegerField;
    DataSetPassportSetSTATEBY: TFIBIntegerField;
    DataSetPassportSetSTATEDATE: TFIBDateTimeField;
    DataSourcePassportSet: TDataSource;
    Transaction: TpFIBTransaction;
    SheetPassport: TcxTabSheet;
    SheetInvoices: TcxTabSheet;
    SheetInvoice: TcxTabSheet;
    procedure ActionAppendUpdate(Sender: TObject);
    procedure ActionDeleteUpdate(Sender: TObject);
    procedure ActionOpenUpdate(Sender: TObject);
    procedure ActionCloseUpdate(Sender: TObject);
    procedure ActionViewUpdate(Sender: TObject);
    procedure ActionViewExecute(Sender: TObject);
    procedure ActionAppendExecute(Sender: TObject);
    procedure ActionDeleteExecute(Sender: TObject);
    procedure ActionOpenExecute(Sender: TObject);
    procedure ActionCloseExecute(Sender: TObject);
  private

  public
    { Public declarations }
  end;

var
  DialogAffinagePassportSet: TDialogAffinagePassportSet;

implementation

uses dmAffiange, frmAffiangePassport, DictData;

{$R *.dfm}

procedure TDialogAffinagePassportSet.ActionAppendUpdate(Sender: TObject);
begin
  ActionAppend.Enabled := True;
end;

procedure TDialogAffinagePassportSet.ActionDeleteUpdate(Sender: TObject);
begin
  ActionDelete.Enabled := True;
end;

procedure TDialogAffinagePassportSet.ActionOpenUpdate(Sender: TObject);
begin
  ActionOpen.Enabled := True;
end;

procedure TDialogAffinagePassportSet.ActionCloseUpdate(Sender: TObject);
begin
  ActionClose.Enabled := True;
end;

procedure TDialogAffinagePassportSet.ActionViewUpdate(Sender: TObject);
begin
  ActionView.Enabled := True;
end;

procedure TDialogAffinagePassportSet.ActionViewExecute(Sender: TObject);
begin
//
end;

procedure TDialogAffinagePassportSet.ActionAppendExecute(Sender: TObject);
begin
//
end;

procedure TDialogAffinagePassportSet.ActionDeleteExecute(Sender: TObject);
begin
//
end;

procedure TDialogAffinagePassportSet.ActionOpenExecute(Sender: TObject);
begin
//
end;

procedure TDialogAffinagePassportSet.ActionCloseExecute(Sender: TObject);
begin
//
end;

end.
