{***********************************************}
{  ������ �������������� + �����������          }
{  Copyrigth (C) 2002-2003 basile for CDM       }
{  v0.15                                        }
{  created 02.04.2003                           }
{  last updated 21.06.2004                      }
{***********************************************}
unit SOEl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList, ExtCtrls, ComCtrls,
  DBLookupEh, DBCtrlsEh, DBCtrls, RxToolEdit, RXDBCtrl, StdCtrls, Mask,
  Grids, DBGridEh, db, ActnList, DBGridEhGrouping, GridsEh, rxPlacemnt,
  rxSpeedbar, FIBDataSet, pFIBDataSet, Buttons, Menus, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxGridCustomView, cxClasses,
  cxGridLevel, cxGrid, cxSpinEdit, cxButtonEdit, cxDropDownEdit, cxCurrencyEdit, DBClient;

type
  TfmSOEl = class(TfmAncestor)
    paHeader: TPanel;
    lbDocDate: TLabel;
    lbDocNo: TLabel;
    lbSup: TLabel;
    txtDep: TDBText;
    lbDep: TLabel;
    spitClose: TSpeedItem;
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;
    edDesrc: TDBRichEdit;
    lbDescr: TLabel;
    edSN: TDBEditEh;
    deSDate: TDBDateTimeEditEh;
    lcbxDepFrom: TDBLookupComboboxEh;
    Label1: TLabel;
    lcbxContract: TDBLookupComboboxEh;
    Label2: TLabel;
    lcbxSubType: TDBLookupComboboxEh;
    acAdd: TAction;
    acDel: TAction;
    acClose: TAction;
    edSuplier: TDBEdit;
    btnSuplierCase: TSpeedButton;
    spitPrint: TSpeedItem;
    ppPrint: TPopupMenu;
    PrintInvSOEl: TMenuItem;
    PrintActSOEl: TMenuItem;
    cxGridDocListLevel: TcxGridLevel;
    cxGridDocList: TcxGrid;
    cxGridDocListItemsView: TcxGridDBBandedTableView;
    cxGridDocListSEMIS: TcxGridDBBandedColumn;
    cxGridDocListQ: TcxGridDBBandedColumn;
    cxGridDocListIW: TcxGridDBBandedColumn;
    cxGridDocListFOR_Q_U: TcxGridDBBandedColumn;
    cxGridDocListPRICE: TcxGridDBBandedColumn;
    cxGridDocListNDS: TcxGridDBBandedColumn;
    cxGridDocListCOST: TcxGridDBBandedColumn;
    cxGridDocListUQ: TcxGridDBBandedColumn;
    cxGridDocListUW: TcxGridDBBandedColumn;
    cxGridDocListOPERATION: TcxGridDBBandedColumn;
    N121: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure lcbxContractEnter(Sender: TObject);
    procedure lcbxContractExit(Sender: TObject);
    procedure lcbxContractUpdateData(Sender: TObject;
      var Handled: Boolean);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acCloseUpdate(Sender: TObject);
    procedure PrintActSOElClick(Sender: TObject);
    procedure PrintInvSOElClick(Sender: TObject);
    procedure btnSuplierCaseClick(Sender: TObject);
    procedure cxGridDocListQPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure N121Click(Sender: TObject);
  public
    taForQULookUp: TClientDataSet;
    FReadOnlyMode : boolean;
    FChangeContract : boolean; // ���� ��������� ���� �������
    procedure CloseDoc;
    procedure OpenDoc;
    procedure InitBtn(T : boolean);
  private
    FGridEditing: boolean;
    procedure SetEditOptions(AReadOnly: boolean);
  end;

var
  fmSOEl: TfmSOEl;

implementation

uses MainData, DictData, dbUtil, fmUtils, ApplData, ADistr, InvData,
  ProductionConsts, PrintData, SuplierCase;

{$R *.dfm}

procedure TfmSOEl.FormCreate(Sender: TObject);
var
  Field: TField;
begin
  dm.WorkMode := 'aSOEl';
  dmInv.IType := 15;
  dm.SchemaId := dmMain.taSOListSCHEMAID.AsInteger;

  inherited;

  taForQULookUp := TClientDataSet.Create(Self);

  Field := TIntegerField.Create(Self);
  Field.FieldName := 'ID';
  Field.FieldKind := fkData;
  Field.DataSet := taForQULookUp;

  Field := TStringField.Create(Self);
  Field.FieldName := 'NAME';
  Field.FieldKind := fkData;
  Field.DataSet := taForQULookUp;

  taForQULookUp.CreateDataSet;

  taForQULookUp.Append;
  taForQULookUp.FieldByName('ID').AsInteger := 0;
  taForQULookUp.FieldByName('NAME').AsString := '�� ���';

  taForQULookUp.Append;
  taForQULookUp.FieldByName('ID').AsInteger := 1;
  taForQULookUp.FieldByName('NAME').AsString := '�� ���-��';

  taForQULookUp.Post;

  with dmMain.taSOElFORQUNAME do
    begin
      LookupDataSet := taForQULookUp;
      LookupkeyFields := 'ID';
      LookupResultField := 'NAME';
    end;

  cxGridDocListFOR_Q_U.DataBinding.FieldName := 'FORQUNAME';
  cxGridDocListFOR_Q_U.DataBinding.ValueType := 'String';

//  OpenDataSets([dm.taComp, dm.taSemis, dm.taContract, dm.taOper, dmMain.taInvSubType, dmMain.taSOEl]);

//  if not dm.taComp.Active then dm.taComp.Open;

  dm.taSemis.Open;
  dm.taContract.Open;
  dm.taOper.Open;
  dmMain.taInvSubType.Open;
  dmMain.taSOEl.Open;

  InitBtn(dmMain.taSOListISCLOSE.AsInteger = 1);

  dm.DepBeginDate := dm.GetDepBeginDate(dmMain.taSOListDEPID.AsInteger);

  if(dm.DepBeginDate > dmMain.taSOListDOCDATE.AsDateTime) or (dmMain.taSOListISCLOSE.AsInteger = 1) then
  begin

    FReadOnlyMode := True

  end else
  begin

    if dmMain.taSOListDEPID.AsInteger = dm.User.wWhId then
    begin

      FReadOnlyMode := False;

    end else
    begin

      FReadOnlyMode := True;

      stbrStatus.Panels[0].Text := '��� �������������� ���������� ����� � ' + dmMain.taSOListDEPNAME.AsString;

    end;

  end;

  SetEditOptions(FReadOnlyMode);

end;

procedure TfmSOEL.SetEditOptions(AReadOnly: Boolean);
begin
  with cxGridDocListItemsView do
    begin
      OptionsBehavior.AlwaysShowEditor := not AReadOnly;
      OptionsData.Editing := not AReadOnly;
      OptionsData.Appending := not AReadOnly;
      OptionsData.Deleting := not AReadOnly;
      OptionsData.Inserting := not AReadOnly;
    end;
    lbDocNo.Enabled := not AReadOnly;
    edSN.Enabled := not AReadOnly;
    lbDocDate.Enabled := not AReadOnly;
    deSDate.Enabled := not AReadOnly;
    lbDep.Enabled := not AReadOnly;
    txtDep.Enabled := not AReadOnly;
    lbSup.Enabled := not AReadOnly;
    lcbxDepFrom.Enabled := not AReadOnly;
    lcbxContract.Enabled := not AReadOnly;
    lcbxSubType.Enabled := not AReadOnly;
    lbDescr.Enabled := not AReadOnly;
    edDesrc.Enabled := not AReadOnly;
    edSuplier.ReadOnly := AReadOnly;
    btnSuplierCase.Enabled := not AReadOnly;
end;

procedure TfmSOEl.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if (dmMain.taSOList.State in [dsInsert, dsEdit]) then dmMain.taSOList.Post;
  CloseDataSets([dmMain.taSOEl, dmMain.taInvSubType]);
  with dm do
    CloseDataSets([taNDS, taSemis, {taComp,} taOper]);
  FreeAndNil(taForQULookUp);
end;

procedure TfmSOEl.InitBtn(T: boolean);
begin
  if not T then // �������� ������
  begin
    acClose.Caption := '������';
    acClose.Hint := '������� ��������';
    acClose.ImageIndex := 6;
  end
  else begin
    acClose.Caption := '������';
    acClose.Hint := '������� ��������';
    acClose.ImageIndex := 5;
  end
end;

procedure TfmSOEl.CloseDoc;
begin
  ExecSQL('execute procedure CloseDoc("������ ����������", '+IntToStr(
    dmMain.taSOListINVID.AsInteger)+')', dmMain.quTmp);
end;




procedure TfmSOEl.cxGridDocListQPropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
begin
  inherited;
  if(TcxGridDBBandedColumn(cxGridDocListItemsView.Controller.FocusedColumn).DataBinding.FieldName <> 'Q')then
   eXit;
  PostDataSets([dmMain.taSOList, dmMain.taSOEl]);
  //if((dm.User.AccProd and $20)<>$20)then raise Exception.Create('� ��� ��� ���� ������� � ������������!');
  if(dmMain.taSOListISCLOSE.AsInteger = 1)then raise EWarning.Create(Format(rsInvClose, [dmMain.taSOListDOCNO.AsString]));
  with dmMain, dmAppl do
  begin
    dmAppl.ADistrType := adtSOEl;
    (* ������� �� ���������? *)
    if ExecSelectSQL('select InvId from Inv where InvId='+IntToStr(taSOElREF.AsInteger), quTmp) = 0
    then begin
      dmMain.taSOEl.Edit;
      taSOElREF.AsVariant := Null;
      dmMain.taSOEl.Post;
    end;
    if taSOElREF.IsNull then
    begin
      if trAppl.Active then trAppl.Commit;
      trAppl.StartTransaction;
      OpenDataSets([taRejInv]);
      taRejInv.Append;
      dmMain.taSOEl.Edit;
      taSOElREF.AsInteger := taRejInvINVID.AsInteger;
      PostDataSets([taRejInv, dmMain.taSOEl]);
    end;
    CloseDataSets([taRejInv, taRejEl]);
    dmAppl.ADistrKind := 0;
    ShowAndFreeForm(TfmADistr, TForm(fmADistr));
    dm.WorkMode := 'aSOEl';
end;

end;


procedure TfmSOEl.OpenDoc;
begin
  ExecSQL('execute procedure OpenDoc("������ ����������", '+IntToStr(
    dmMain.taSOListINVID.AsInteger)+')', dmMain.quTmp);
end;

procedure TfmSOEl.PrintActSOElClick(Sender: TObject);
begin
  inherited;
  dmPrint.FDocID:=2010;
  dmInv.FCurrINVID := dmMain.taSOElINVID.AsInteger;

  with dmPrint do
    begin
      if not taSumOrNotSOEl.Active then taSumOrNotSOEl.Open;
      taInvHeader.Close;
      taInvHeader.Open;
      frReport.Dataset:=frSumOrNotSOEl;
      taDoc.Open;
      frReport.LoadFromDB(taDoc, FDocID);
      dm.DocPreview:=dm.GetDocPreview;
      if dm.DocPreview=pvDesign
        then
          frReport.DesignReport
        else
          frReport.ShowReport;
       taDoc.Close;
       taSumOrNotSOEl.Close;
    end;
end;

procedure TfmSOEl.PrintInvSOElClick(Sender: TObject);
begin
  inherited;
 dmPrint.FDocID:=2011;
 dmInv.FCurrINVID := dmMain.taSOElINVID.AsInteger;

 with dmPrint do
  begin
    if not taSumOrNotSOEl.Active then taSumOrNotSOEl.Open;
    taInvoiceHeader.Close;
    taInvoiceHeader.ParamByName('InvID').AsInteger := dmInv.FCurrINVID;
    taInvoiceHeader.Open;
    frReport.Dataset := frSumOrNotSOEl;
    taDoc.Open;
    frReport.LoadFromDB(taDoc, FDocID);
    dm.DocPreview:=dm.GetDocPreview;
    if dm.DocPreview = pvDesign
      then
        frReport.DesignReport
      else
        frReport.ShowReport;
    taDoc.Close;
    taSumOrNotSOEl.Close;
  end;
end;



procedure TfmSOEl.lcbxContractEnter(Sender: TObject);
begin
  FChangeContract := False;
end;

procedure TfmSOEl.lcbxContractExit(Sender: TObject);
begin
  if not FChangeContract then eXit;
  dm.SchemaId := dm.taContractT0.AsInteger;
  ReOpenDataSet(dmMain.taInvSubType);
end;

procedure TfmSOEl.lcbxContractUpdateData(Sender: TObject; var Handled: Boolean);
begin
  if dm.taContractT0.AsInteger <> dmMain.taSOListSCHEMAID.AsInteger then
  begin
    FChangeContract := True;
    dm.SchemaId := dm.taContractT0.AsInteger;
    ReOpenDataSet(dmMain.taInvSubType);
  end;
end;

procedure TfmSOEl.N121Click(Sender: TObject);
begin
  inherited;

  dmPrint.FDocID:=116;
  dmInv.FCurrINVID := dmMain.taSOElINVID.AsInteger;

  with dmPrint do
    begin
      if not taSumOrNotSOEl.Active then taSumOrNotSOEl.Open;
      taInvHeader.Close;
      taInvHeader.Open;
      taInvToPrint.CloseOpen(true);
      frReport.Dataset:=frSumOrNotSOEl;
      frReport.OnGetValue := dmPrint.Inv15Topr12GetValue;
      taDoc.Open;
      frReport.LoadFromDB(taDoc, FDocID);

      dm.DocPreview:=dm.GetDocPreview;
      if dm.DocPreview=pvDesign
        then
          frReport.DesignReport
        else
          frReport.ShowReport;

       taDoc.Close;
       taSumOrNotSOEl.Close;
    end;


end;

procedure TfmSOEl.acAddExecute(Sender: TObject);
begin
  inherited;
  with dmMain.quTmp do
    begin
      SQL.Clear;
      SQL.Text := 'insert into SIEl(ID, INVID, SEMIS, Q,  W, TPRICE, PRICE, NDSID, NDS, T, COST, FOR_Q_U) ' +
                'values (:ID, :INVID, 9, 0, 0, 0, 0, :NDSID, 0, 2, 0, 0)';
      ParamByName('ID').AsInteger := dm.GetId(42);
      ParamByName('InvID').AsInteger := dmMain.taSOListINVID.AsInteger;
      ParamByName('NDSID').AsInteger := dmMain.taSOListNDSID.AsInteger;
      ExecQuery;
      Transaction.CommitRetaining;
    end;
  dmMain.taSOEl.Close;
  dmMain.taSOEl.Open;
  ActiveControl := cxGridDocList;
end;

procedure TfmSOEl.acAddUpdate(Sender: TObject);
begin
 acAdd.Enabled := dmMain.taSOEl.Active;
end;

procedure TfmSOEl.acDelExecute(Sender: TObject);
begin
 dmMain.taSOEl.Delete;
end;

procedure TfmSOEl.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := (dmMain.taSOEl.Active) and (not dmMain.taSOEl.IsEmpty);
end;

procedure TfmSOEl.btnSuplierCaseClick(Sender: TObject);
begin
  inherited;
if ShowAndFreeForm(TfmSuplierCase, TForm(fmSuplierCase)) = mrOK
  then
    begin
      dmMain.taSOEl.Close;
      dmMain.taSOEl.Open;
    end;
end;

procedure TfmSOEl.acCloseExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taSOList, dmMain.taSOEl]);
  if dmMain.taSOEl.IsEmpty then raise EWarning.Create(rsInvEmpty);

  if acClose.Caption = '������' then
    begin
      if trim(lcbxSubType.Text) = '' then
      begin
        ShowMessage('��������! �� �������� "��� �������"!');
        eXit;
      end;
      try
        CloseDoc;
        acClose.Caption := '������';
        acClose.Hint := '������� ��������';
        acClose.ImageIndex := 5;
      except
        on E: Exception do ShowMessage(E.Message);
      end;
    end
  else
    try
      OpenDoc;
      acClose.Caption := '������';
      acClose.Hint := '������� ��������';
      acClose.ImageIndex := 6;
    except
      on E: Exception do ShowMessage(E.Message);
    end;
    
  RefreshDataSet(dmMain.taSOList);
  FReadOnlyMode := not FReadOnlyMode;
  SetEditOptions(FReadOnlyMode);
end;

procedure TfmSOEl.acCloseUpdate(Sender: TObject);
begin
  acClose.Enabled := dmMain.taSOList.Active and dmMain.taSOEl.Active and (not dmMain.taSOEl.IsEmpty);
end;

end.
