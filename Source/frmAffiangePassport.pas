unit frmAffiangePassport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxButtonEdit,
  cxDBExtLookupComboBox, cxCalendar, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGrid6Lnk, Provider,
  DBClient, FR_DSet, FR_DBSet, FIBDatabase, pFIBDatabase, FR_Class,
  ImgList, FIBQuery, pFIBQuery, pFIBStoredProc, FIBDataSet, cxHint,
  pFIBDataSet, ActnList, dxBar, dxBarExtItems, cxClasses, ExtCtrls,
  cxSplitter, cxVGrid, cxDBVGrid, cxInplaceContainer, cxGridDBTableView,
  cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxControls,
  cxGridCustomView, cxGrid, cxPC, dxStatusBar;


type

  { TStoredProcedure }

  TStoredProcedure = class(TObject)
  private
    FErrorDescription: string;
    FProcedure: TpFIBStoredProc;
    function GetName: string;
    function GetValue(AName: string): Variant;
    procedure SetName(const Value: string);
    procedure SetValue(AName: string; const Value: Variant);
    function GetError: Boolean;
    function GetSuccess: Boolean;
  public
    constructor Create(AProcedure: TpFIBStoredProc = nil);
    procedure Execute;
    procedure Bind(AProcedure: TpFIBStoredProc);
    property Name: string read GetName write SetName;
    property Success: Boolean read GetSuccess;
    property Error: Boolean read GetError;
    property ErrorDescription: string read FErrorDescription;
    property Value[AName: string]: Variant read GetValue write SetValue; default;
  end;

  { TDialogAffinage }

  TDialogAffinage = class(TForm)
    StatusBar: TdxStatusBar;
    PagesPassport: TcxPageControl;
    SheetPassports: TcxTabSheet;
    GridPassportsLevel1: TcxGridLevel;
    GridPassports: TcxGrid;
    GridPassportsView: TcxGridDBBandedTableView;
    BarManager: TdxBarManager;
    BarButtonAppend: TdxBarButton;
    dxBarButton2: TdxBarButton;
    Actions: TActionList;
    ActionPassportAppend: TAction;
    ActionPassportDelete: TAction;
    ActionPassportOpen: TAction;
    dxBarButton1: TdxBarButton;
    ActionPassportView: TAction;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    DockControlPassports: TdxBarDockControl;
    dxBarButton5: TdxBarButton;
    DataSourcePassports: TDataSource;
    SheetPassport: TcxTabSheet;
    dxBarButton6: TdxBarButton;
    ActionPassportExit: TAction;
    dxBarStatic1: TdxBarStatic;
    DateComboPassportPeriodBegin: TdxBarDateCombo;
    dxBarStatic2: TdxBarStatic;
    DateComboPassportPeriodEnd: TdxBarDateCombo;
    dxBarStatic3: TdxBarStatic;
    DataSetPassports: TpFIBDataSet;
    GridPassportsViewDESIGNATION: TcxGridDBBandedColumn;
    GridPassportsViewPERIODBEGIN: TcxGridDBBandedColumn;
    GridPassportsViewPERIODEND: TcxGridDBBandedColumn;
    GridPassportsViewCREATEDDATE: TcxGridDBBandedColumn;
    GridPassportsViewCREATEDBY: TcxGridDBBandedColumn;
    GridPassportsViewSTATE: TcxGridDBBandedColumn;
    GridPassportsViewSTATEBY: TcxGridDBBandedColumn;
    GridPassportsViewSTATEDATE: TcxGridDBBandedColumn;
    HintStyleController: TcxHintStyleController;
    GridPassportLevel: TcxGridLevel;
    GridPassportItems: TcxGrid;
    GridPassportView: TcxGridDBBandedTableView;
    DataSetPassportItems: TpFIBDataSet;
    GridPassport: TcxDBVerticalGrid;
    GridPassportDBEditorRow1: TcxDBEditorRow;
    GridPassportCategoryRow1: TcxCategoryRow;
    GridPassportDBEditorRow2: TcxDBEditorRow;
    GridPassportDBEditorRow3: TcxDBEditorRow;
    GridPassportCategoryRow2: TcxCategoryRow;
    GridPassportDBEditorRow4: TcxDBEditorRow;
    GridPassportDBEditorRow5: TcxDBEditorRow;
    GridPassportCategoryRow3: TcxCategoryRow;
    GridPassportDBEditorRow6: TcxDBEditorRow;
    GridPassportDBEditorRow7: TcxDBEditorRow;
    GridPassportDBEditorRow8: TcxDBEditorRow;
    DataSourcePassportItems: TDataSource;
    DataSetPassportsID: TFIBIntegerField;
    DataSetPassportsDESIGNATION: TFIBIntegerField;
    DataSetPassportsPERIODBEGIN: TFIBDateTimeField;
    DataSetPassportsPERIODEND: TFIBDateTimeField;
    DataSetPassportsCREATEDDATE: TFIBDateTimeField;
    DataSetPassportsCREATEDBY: TFIBIntegerField;
    DataSetPassportsSTATE: TFIBIntegerField;
    DataSetPassportsSTATEBY: TFIBIntegerField;
    DataSetPassportsSTATEDATE: TFIBDateTimeField;
    DataSetPassportsCREATEDBYTITLE: TFIBStringField;
    DataSetPassportsSTATEBYTITLE: TFIBStringField;
    DataSetPassportsSTATETITLE: TFIBStringField;
    dxBarButton7: TdxBarButton;
    DockControlPassport: TdxBarDockControl;
    ButtonPassportOpen: TdxBarButton;
    ActionPassportRefresh: TAction;
    dxBarButton9: TdxBarButton;
    GridPassportViewWEIGHT: TcxGridDBBandedColumn;
    GridPassportViewWEIGHTIN: TcxGridDBBandedColumn;
    GridPassportViewWEIGHTREMAINDERCURRENT: TcxGridDBBandedColumn;
    GridPassportViewWEIGHTREMAINDEROUT: TcxGridDBBandedColumn;
    GridPassportViewWEIGHTREMAINDERIN: TcxGridDBBandedColumn;
    Splitter: TcxSplitter;
    TmpProcedure: TpFIBStoredProc;
    EnabledLargeImages: TImageList;
    PagesGroup: TcxPageControl;
    SheetGroupPassport: TcxTabSheet;
    SheetGroupInvoice: TcxTabSheet;
    ButtonPassportClose: TdxBarButton;
    dxBarButton11: TdxBarButton;
    ActionPassportClose: TAction;
    PagesInvoice: TcxPageControl;
    SheetInvoices: TcxTabSheet;
    SheetInvoice: TcxTabSheet;
    GridInvoicesLevel1: TcxGridLevel;
    GridInvoices: TcxGrid;
    GridInvoicesDBBandedTableView1: TcxGridDBBandedTableView;
    GridInvoiceItemsLevel1: TcxGridLevel;
    GridInvoiceItems: TcxGrid;
    DockControlInvoices: TdxBarDockControl;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    DateComboInvoiceBegin: TdxBarDateCombo;
    DateComboInvoiceEnd: TdxBarDateCombo;
    DataSetInvoices: TpFIBDataSet;
    DataSetInvoiceItems: TpFIBDataSet;
    DataSourceInvoices: TDataSource;
    DataSourceInvoiceItems: TDataSource;
    ActionInvoiceAppendOut: TAction;
    ActionInvoiceDelete: TAction;
    ActionInvoiceView: TAction;
    ActionInvoiceOpen: TAction;
    ActionInvoiceClose: TAction;
    ActionInvoiceExit: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    DockControlInvoice: TdxBarDockControl;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    GridInvoicesDBBandedTableView1DESIGNATION: TcxGridDBBandedColumn;
    GridInvoicesDBBandedTableView1CLASSTITLE: TcxGridDBBandedColumn;
    GridInvoicesDBBandedTableView1DATE: TcxGridDBBandedColumn;
    GridInvoicesDBBandedTableView1CREATEDDATE: TcxGridDBBandedColumn;
    GridInvoicesDBBandedTableView1CREATEDBYTITLE: TcxGridDBBandedColumn;
    GridInvoicesDBBandedTableView1STATETITLE: TcxGridDBBandedColumn;
    GridInvoicesDBBandedTableView1STATEBYTITLE: TcxGridDBBandedColumn;
    GridInvoicesDBBandedTableView1STATEDATE: TcxGridDBBandedColumn;
    ActionInvoiceAppendIn: TAction;
    DisabledLargeImages: TImageList;
    DataSetInvoicesID: TFIBIntegerField;
    DataSetInvoicesDESIGNATION: TFIBIntegerField;
    DataSetInvoicesCLASS: TFIBIntegerField;
    DataSetInvoicesCLASSTITLE: TFIBStringField;
    DataSetInvoicesDATE: TFIBDateTimeField;
    DataSetInvoicesCREATEDDATE: TFIBDateTimeField;
    DataSetInvoicesCREATEDBY: TFIBIntegerField;
    DataSetInvoicesCREATEDBYTITLE: TFIBStringField;
    DataSetInvoicesSTATE: TFIBIntegerField;
    DataSetInvoicesSTATETITLE: TFIBStringField;
    DataSetInvoicesSTATEBY: TFIBIntegerField;
    DataSetInvoicesSTATEBYTITLE: TFIBStringField;
    DataSetInvoicesSTATEDATE: TFIBDateTimeField;
    GridInvoiceItemsView: TcxGridDBBandedTableView;
    GridInvoiceItemsViewOPERATIONTITLE: TcxGridDBBandedColumn;
    GridInvoiceItemsViewPLACEOFSTORAGETITLE: TcxGridDBBandedColumn;
    GridInvoiceItemsViewMATERIALWEIGHT: TcxGridDBBandedColumn;
    GridInvoiceItemsViewMATERAILQUANTITY: TcxGridDBBandedColumn;
    ActionInvoiceAppend: TAction;
    DataSetOperations: TpFIBDataSet;
    DataSetPassportItemsID: TFIBIntegerField;
    DataSetPassportItemsPASSPORTID: TFIBIntegerField;
    DataSetPassportItemsOPERATIONID: TFIBIntegerField;
    DataSetPassportItemsOPERATIONTITLE: TFIBStringField;
    DataSetPassportItemsPLACEOFSTORAGEID: TFIBIntegerField;
    DataSetPassportItemsPLACEOFSTORAGETITLE: TFIBStringField;
    DataSetPassportItemsWEIGHT: TFIBBCDField;
    DataSetPassportItemsWEIGHTOUT: TFIBBCDField;
    DataSetPassportItemsWEIGHTIN: TFIBBCDField;
    DataSetPassportItemsWEIGHTREMAINDERCURRENT: TFIBBCDField;
    DataSetPassportItemsWEIGHTREMAINDEROUT: TFIBBCDField;
    DataSetPassportItemsWEIGHTREMAINDERIN: TFIBBCDField;
    DataSetInvoiceItemsID: TFIBIntegerField;
    DataSetInvoiceItemsINVOICEID: TFIBIntegerField;
    DataSetInvoiceItemsOPERATIONID: TFIBIntegerField;
    DataSetInvoiceItemsPLACEOFSTORAGEID: TFIBIntegerField;
    DataSetInvoiceItemsPLACEOFSTORAGETITLE: TFIBStringField;
    DataSetInvoiceItemsWEIGHT: TFIBBCDField;
    DataSetInvoiceItemsMATERIALID: TFIBIntegerField;
    DataSetInvoiceItemsMATERIALTITLE: TFIBStringField;
    DataSetInvoiceItemsMATERIALWEIGHT: TFIBBCDField;
    DataSetInvoiceItemsMATERAILQUANTITY: TFIBIntegerField;
    DataSetInvoiceItemsOPERATIONTITLE: TStringField;
    DataSetPlaceOfStorage: TpFIBDataSet;
    DataSetPlaceOfStorageID: TFIBIntegerField;
    DataSetPlaceOfStorageTITLE: TFIBStringField;
    DataSetOperationsID: TFIBIntegerField;
    DataSetOperationsTITLE: TFIBStringField;
    DataSetMaterial: TpFIBDataSet;
    DataSetMaterialID: TFIBIntegerField;
    DataSetMaterialTITLE: TFIBStringField;
    GridInvoice: TcxDBVerticalGrid;
    cxSplitter1: TcxSplitter;
    GridInvoiceDESIGNATION1: TcxDBEditorRow;
    GridInvoiceCLASSTITLE1: TcxDBEditorRow;
    GridInvoiceDATE1: TcxDBEditorRow;
    GridInvoiceCategoryRow1: TcxCategoryRow;
    GridInvoiceDBEditorRow1: TcxDBEditorRow;
    GridInvoiceDBEditorRow2: TcxDBEditorRow;
    GridInvoiceCategoryRow2: TcxCategoryRow;
    GridInvoiceDBEditorRow3: TcxDBEditorRow;
    GridInvoiceDBEditorRow4: TcxDBEditorRow;
    GridInvoiceDBEditorRow5: TcxDBEditorRow;
    ActionInvoiceItemAdd: TAction;
    ActionInvoiceItemDelete: TAction;
    dxBarButton8: TdxBarButton;
    dxBarButton10: TdxBarButton;
    Panel1: TPanel;
    DockControlInvoiceItems: TdxBarDockControl;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    GridPassportViewWEIGHTREMAINDER: TcxGridDBBandedColumn;
    ActionInvoicePrint: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    ReportInvoiceOut: TfrReport;
    DataSetInvoicesDEPARTMENTID: TFIBIntegerField;
    DataSetInvoicesTARGETCOMPANYID: TFIBIntegerField;
    DataSetCompanies: TpFIBDataSet;
    Transaction: TpFIBTransaction;
    DataSetCompaniesID: TFIBIntegerField;
    DataSetCompaniesTITLE: TFIBStringField;
    DataSetDepartments: TpFIBDataSet;
    DataSetDepartmentsID: TFIBIntegerField;
    DataSetDepartmentsTITLE: TFIBStringField;
    DataSetInvoicesDepartmentTITLE: TStringField;
    DataSetInvoicesCompanyTITLE: TStringField;
    GridInvoiceCategoryRow3: TcxCategoryRow;
    GridInvoiceDBEditorRow6: TcxDBEditorRow;
    GridInvoiceDBEditorRow7: TcxDBEditorRow;
    DataSetMeasures: TpFIBDataSet;
    DataSetMeasuresID: TFIBIntegerField;
    DataSetMeasuresTITLE: TFIBStringField;
    DataSetMeasuresDESIGNATION: TFIBStringField;
    DataSetInvoiceItemsMATERIALMEASURETITLE: TStringField;
    GridInvoiceItemsViewMATERIALMEASUREWEIGHTTITLE: TcxGridDBBandedColumn;
    DataSetInvoiceItemsMATERIALMEASUREWEIGHTID: TFIBIntegerField;
    DataSetInvoiceItemsMATERIALMEASUREQUANTITYID: TFIBIntegerField;
    DataSetInvoiceItemsMATERIALMEASUREQUANTITYTITLE: TStringField;
    GridInvoiceItemsViewMATERIALMEASUREQUANTITYTITLE: TcxGridDBBandedColumn;
    DataSetInvoicePrintHeader: TpFIBDataSet;
    DataSetInvoicePrintItem: TpFIBDataSet;
    DataSetInvoicePrintHeaderHEADERDESIGNATION: TFIBIntegerField;
    DataSetInvoicePrintHeaderHEADERDATE: TFIBDateTimeField;
    DataSetInvoicePrintHeaderHEADERSOURCECOMPANY: TFIBStringField;
    DataSetInvoicePrintHeaderHEADERTARGETCOMPANY: TFIBStringField;
    DataSetInvoicePrintHeaderHEADERDEPARTMENTTITLE: TFIBStringField;
    DataSetInvoicePrintItemITEMOPERATIONTITLE: TFIBStringField;
    DataSetInvoicePrintItemITEMPLACEOFSTORAGETITLE: TFIBStringField;
    DataSetInvoicePrintItemITEMWEIGHT: TFIBBCDField;
    DataSetInvoicePrintItemITEMMATERIALTITLE: TFIBStringField;
    DataSetInvoicePrintItemITEMMEASUREWEIGHTTITLE: TFIBStringField;
    DataSetInvoicePrintItemITEMMEASUREQUANTITYTITLE: TFIBStringField;
    DataSetInvoicePrintItemITEMMATERIALWEIGHT: TFIBBCDField;
    DataSetInvoicePrintItemITEMMATERIALQUANTITY: TFIBIntegerField;
    dxBarLargeButton19: TdxBarLargeButton;
    ActionInvoiceItemApply: TAction;
    ActionInvoiceItemCancel: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    DataSourcePlaceOfStorageCustom: TDataSource;
    GridPlaceOfStorageView: TcxGridDBTableView;
    GridPlaceOfStorageLevel1: TcxGridLevel;
    GridPlaceOfStorage: TcxGrid;
    GridPlaceOfStorageViewTITLE: TcxGridDBColumn;
    DataSetPlaceOfStorageCustom: TpFIBDataSet;
    DataSourcePlaceOfStorage: TDataSource;
    frDBDataSetInvoicePrintHeader: TfrDBDataSet;
    frDBDataSetInvoicePrintItem: TfrDBDataSet;
    DataSetInvoicePrintItemITEMINDEXINGROUP: TFIBIntegerField;
    ReportInvoiceIn: TfrReport;
    dxBarCombo1: TdxBarCombo;
    GridInvoiceItemsViewMATERIALTITLE: TcxGridDBBandedColumn;
    TmpTransaction: TpFIBTransaction;
    DataSetInvoicePrintHeaderHEADERREASON: TFIBStringField;
    GridPassportCategoryRow4: TcxCategoryRow;
    GridPassportDBEditorRow9: TcxDBEditorRow;
    GridPassportAffinagePeriodEndEditor: TcxDBEditorRow;
    DataSetPassportsAFFINAGEPERIODBEGIN: TFIBDateTimeField;
    DataSetPassportsAFFINAGEPERIODEND: TFIBDateTimeField;
    Groups: TClientDataSet;
    GroupsTitle: TStringField;
    GroupsItems: TDataSetField;
    GroupItems: TClientDataSet;
    GroupItemsOperationID: TIntegerField;
    GroupItemsPlaceOfStorageId: TIntegerField;
    ButtonGroup: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    ActionPassportDoGrouping: TAction;
    DataSourceGroups: TDataSource;
    DataSourceGroupItems: TDataSource;
    GroupsID: TAutoIncField;
    GroupItemsOperationTitle: TStringField;
    GroupItemsPlaceOfStorageTitle: TStringField;
    GroupItemVariants: TClientDataSet;
    GroupItemVariantsOperationID: TIntegerField;
    GroupItemVariantsPlaceOfStorageId: TIntegerField;
    GroupItemsAll: TClientDataSet;
    GroupItemsAllOperationID: TIntegerField;
    GroupItemsAllPlaceOfStorageID: TIntegerField;
    DataSourceGroupItemVariants: TDataSource;
    GroupItemVariantsOperationTitle: TStringField;
    GroupItemVariantsPlaceOfStorageTitle: TStringField;
    PassportItems: TClientDataSet;
    ProviderPassportItems: TDataSetProvider;
    PassportItemsID: TIntegerField;
    PassportItemsPASSPORTID: TIntegerField;
    PassportItemsOPERATIONID: TIntegerField;
    PassportItemsOPERATIONTITLE: TStringField;
    PassportItemsPLACEOFSTORAGEID: TIntegerField;
    PassportItemsPLACEOFSTORAGETITLE: TStringField;
    PassportItemsWEIGHT: TBCDField;
    GroupItemsAllGroupID: TIntegerField;
    GridPassportItemsLevel2: TcxGridLevel;
    GridPassportViewColumn1: TcxGridDBBandedColumn;
    GridPassportItemsView: TcxGridDBTableView;
    PassportGroups: TClientDataSet;
    PassportGroupsTitle: TStringField;
    DataSourcePassportGroups: TDataSource;
    DataSetPassportItemsGROUPID: TFIBIntegerField;
    PassportItemsGROUPID: TIntegerField;
    GridPassportItemsViewOPERATIONTITLE: TcxGridDBColumn;
    GridPassportItemsViewPLACEOFSTORAGETITLE: TcxGridDBColumn;
    GridPassportItemsViewWEIGHT: TcxGridDBColumn;
    PassportGroupsID: TIntegerField;
    DataSetPassportItemsWEIGHTREMAINDER: TFIBBCDField;
    PassportGroupsWEIGHT: TBCDField;
    PassportGroupsWEIGHTOUT: TBCDField;
    PassportGroupsWEIGHTIN: TBCDField;
    PassportGroupsWEIGHTREMAINDERCURRENT: TBCDField;
    PassportGroupsWEIGHTREMAINDEROUT: TBCDField;
    PassportGroupsWEIGHTREMAINDERIN: TBCDField;
    PassportGroupsWEIGHTREMAINDER: TBCDField;
    cxStyleRepository: TcxStyleRepository;
    StyleReadOnly: TcxStyle;
    StyleVirtual: TcxStyle;
    GridPassportViewColumn2: TcxGridDBBandedColumn;
    GridPassportItemsViewColumn1: TcxGridDBColumn;
    PassportItemsWEIGHTIN: TBCDField;
    GroupsWEIGHTRemainderOUT: TBCDField;
    GroupsWEIGHTRemainderIN: TBCDField;
    ActionExit: TAction;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarLargeButton25: TdxBarLargeButton;
    TmpQuery: TpFIBQuery;
    Printer: TdxComponentPrinter;
    GridPassportItemsPrinterLink: TdxGridReportLink;
    dxBarLargeButton27: TdxBarLargeButton;
    ActionPassportPrint: TAction;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    dxPSEngineController1: TdxPSEngineController;
    COperations: TClientDataSet;
    CPlaceOfStorage: TClientDataSet;
    ProviderOperations: TDataSetProvider;
    ProviderPlaceOfStorage: TDataSetProvider;
    COperationsID: TIntegerField;
    COperationsTITLE: TStringField;
    CPlaceOfStorageID: TIntegerField;
    CPlaceOfStorageTITLE: TStringField;
    GroupsOperationID: TIntegerField;
    GroupsOperationTitle: TStringField;
    GroupsPlaceOfStorageId: TIntegerField;
    GroupsPlaceOfStorageTitle: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DataSetPassportItemsBeforeOpen(DataSet: TDataSet);
    procedure ActionPassportAppendUpdate(Sender: TObject);
    procedure ActionPassportDeleteUpdate(Sender: TObject);
    procedure ActionPassportViewUpdate(Sender: TObject);
    procedure ActionPassportRefreshUpdate(Sender: TObject);
    procedure ActionPassportOpenUpdate(Sender: TObject);
    procedure ActionPassportExitUpdate(Sender: TObject);
    procedure ActionPassportAppendExecute(Sender: TObject);
    procedure ActionPassportDeleteExecute(Sender: TObject);
    procedure ActionPassportOpenExecute(Sender: TObject);
    procedure ActionPassportRefreshExecute(Sender: TObject);
    procedure ActionPassportExitExecute(Sender: TObject);
    procedure ActionPassportViewExecute(Sender: TObject);
    procedure ActionPassportCloseUpdate(Sender: TObject);
    procedure ActionPassportCloseExecute(Sender: TObject);
    procedure DataSetInvoicesBeforeOpen(DataSet: TDataSet);
    procedure ActionInvoiceAppendUpdate(Sender: TObject);
    procedure ActionInvoiceDeleteUpdate(Sender: TObject);
    procedure ActionInvoiceViewUpdate(Sender: TObject);
    procedure ActionInvoiceOpenUpdate(Sender: TObject);
    procedure ActionInvoiceCloseUpdate(Sender: TObject);
    procedure ActionInvoiceExitUpdate(Sender: TObject);
    procedure ActionInvoiceAppendExecute(Sender: TObject);
    procedure ActionInvoiceDeleteExecute(Sender: TObject);
    procedure ActionInvoiceViewExecute(Sender: TObject);
    procedure ActionInvoiceOpenExecute(Sender: TObject);
    procedure ActionInvoiceCloseExecute(Sender: TObject);
    procedure ActionInvoiceExitExecute(Sender: TObject);
    procedure DateComboInvoiceBeginChange(Sender: TObject);
    procedure DataSetInvoiceItemsBeforeOpen(DataSet: TDataSet);
    procedure DataSetInvoiceItemsBeforePost(DataSet: TDataSet);
    procedure OnDataSetValidate(DataSet: TDataSet);
    procedure ActionInvoiceItemAddUpdate(Sender: TObject);
    procedure ActionInvoiceItemDeleteUpdate(Sender: TObject);
    procedure ActionInvoiceItemAddExecute(Sender: TObject);
    procedure ActionInvoiceItemDeleteExecute(Sender: TObject);
    procedure DataSetPassportItemsCalcFields(DataSet: TDataSet);
    procedure GridPassportsViewDblClick(Sender: TObject);
    procedure GridInvoicesDBBandedTableView1DblClick(Sender: TObject);
    procedure DataSetInvoicesAfterInsert(DataSet: TDataSet);
    procedure DataSetInvoicesAfterDelete(DataSet: TDataSet);
    procedure DataSetCommitRetaining(DataSet: TDataSet);
    procedure DataSetInvoiceItemsNewRecord(DataSet: TDataSet);
    procedure DataSetInvoicePrintHeaderBeforeOpen(DataSet: TDataSet);
    procedure DataSetInvoicePrintItemBeforeOpen(DataSet: TDataSet);
    procedure ActionInvoicePrintExecute(Sender: TObject);
    procedure ActionInvoicePrintUpdate(Sender: TObject);
    procedure ActionInvoiceItemApplyUpdate(Sender: TObject);
    procedure ActionInvoiceItemCancelUpdate(Sender: TObject);
    procedure ActionInvoiceItemApplyExecute(Sender: TObject);
    procedure ActionInvoiceItemCancelExecute(Sender: TObject);
    procedure DataSetInvoiceItemsBeforeInsert(DataSet: TDataSet);
    procedure DataSetInvoiceItemsAfterCancel(DataSet: TDataSet);
    procedure GridInvoiceItemsViewPLACEOFSTORAGETITLEPropertiesInitPopup(
      Sender: TObject);
    procedure DataSetPlaceOfStorageCustomBeforeOpen(DataSet: TDataSet);
    procedure DataSourceInvoiceItemsDataChange(Sender: TObject;
      Field: TField);
    procedure DataSetMaterialBeforeOpen(DataSet: TDataSet);
    procedure DataSetPassportsAFFINAGEPERIODENDChange(Sender: TField);
    procedure DataSetPassportsBeforeEdit(DataSet: TDataSet);
    procedure GroupItemsBeforePost(DataSet: TDataSet);
    procedure GroupItemsAfterDelete(DataSet: TDataSet);
    procedure PassportItemsBeforeOpen(DataSet: TDataSet);
    procedure ActionPassportDoGroupingExecute(Sender: TObject);
    procedure PassportGroupsBeforeEdit(DataSet: TDataSet);
    procedure PassportGroupsBeforePost(DataSet: TDataSet);
    procedure GridPassportViewColumn1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure GridPassportViewWEIGHTREMAINDEROUTPropertiesButtonClick(
      Sender: TObject; AButtonIndex: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionExitExecute(Sender: TObject);
    procedure GroupItemsBeforeInsert(DataSet: TDataSet);
    procedure GroupItemsBeforeEdit(DataSet: TDataSet);
    procedure GroupItemsAfterCancel(DataSet: TDataSet);
    procedure GroupItemsAfterPost(DataSet: TDataSet);
    procedure ActionPassportPrintExecute(Sender: TObject);
    procedure GroupsBeforePost(DataSet: TDataSet);
  private
    procedure SetReadOnly(const Value: Boolean);
  private
    FReadOnly: Boolean;
    PeriodBegin: TDateTime;
    PeriodEnd: TDateTime;
    StoredProcedure: TStoredProcedure;
    AffinagePeriodEnd: TDateTime;
    GroupWeightRemainderOut: Double;
    GroupEditing: Boolean;
    GroupsData: OleVariant;
    StoredOperationID: Variant;
    StoredPlaceOfStorageID: Variant;
    procedure LoadGroups(PassportID: Integer; DataSet: TClientDataSet; Refresh: Boolean = False);
    procedure SaveGroups(PassportID: Integer; DataSet: TClientDataSet);
    procedure RefreshPeriod;
    procedure CreateView(Refresh: Boolean = False);
  public
    Flag: Integer;
    CachedOperationID: Integer;
    procedure CalculateGroupVariants;
    class procedure Execute(ReadOnly: Boolean);
    property ReadOnly: Boolean read FReadOnly write SetReadOnly;
  end;

var
  DialogAffinage: TDialogAffinage;

implementation

uses uDialogs, DictData, DateUtils, uUtils, frmPassportConfigure, dbUtil;

{$R *.dfm}

function DataSetValidateRequired(DataSet: TDataSet): Boolean;
var
  i: Integer;
  Field: TField;
begin
  Result := True;
  if Assigned(DataSet) and DataSet.Active then
  begin
    for i := 0 to DataSet.Fields.Count - 1 do
    begin
      Field := DataSet.Fields[i];
      if Field.Tag = 1 then
      begin
        if Field.IsNull then
        begin
          Result := False;
          DialogErrorOkMessage('�� ���������� �������� ��� '#13#10 + '"' + Field.DisplayLabel + '"');
        end;
      end;
    end;
  end;
end;


{ TStoredProcedure }

constructor TStoredProcedure.Create(AProcedure: TpFIBStoredProc);
begin
  FProcedure := AProcedure;
  if Assigned(FProcedure) then
  begin
    FErrorDescription := '';
  end
end;

function TStoredProcedure.GetName: string;
begin
  if Assigned(FProcedure) then Result := FProcedure.StoredProcName
  else Result := '';
end;

procedure TStoredProcedure.SetName(const Value: string);
begin
  if Assigned(FProcedure) then
  begin
    try
      FProcedure.StoredProcName := Trim(Value);
    except
      FProcedure.StoredProcName := '';
    end;
    FErrorDescription := '';
  end;
end;

function TStoredProcedure.GetValue(AName: string): Variant;
var
  Parameter: TFIBXSQLVAR;
begin
  if Assigned(FProcedure) then
  begin
    Parameter := FProcedure.ParamByName(AName);
    if Assigned(Parameter) then Result := Parameter.Value
    else Result := Unassigned;
  end
  else Result := Unassigned;
end;

procedure TStoredProcedure.SetValue(AName: string; const Value: Variant);
var
  Parameter: TFIBXSQLVAR;
begin
  if Assigned(FProcedure) then
  begin
    Parameter := FProcedure.ParamByName(AName);
    if Assigned(Parameter) then Parameter.Value := Value;
  end;
end;

procedure TStoredProcedure.Execute;
begin
  FErrorDescription := '';
  if Assigned(FProcedure) then
  begin
    try
     FProcedure.Transaction.StartTransaction;
     FProcedure.ExecProc;
     FProcedure.Transaction.Commit;
    except
      on E: Exception do
      begin
        FProcedure.Transaction.Rollback;
        FErrorDescription := E.Message;
      end;
    end
  end
end;

procedure TStoredProcedure.Bind(AProcedure: TpFIBStoredProc);
begin
  FProcedure := AProcedure;
end;

function TStoredProcedure.GetSuccess: Boolean;
begin
  Result := FErrorDescription = '';
end;

function TStoredProcedure.GetError: Boolean;
begin
  Result := FErrorDescription <> '';
end;

{ TDialogAffinage }

class procedure TDialogAffinage.Execute(ReadOnly: Boolean);
begin
  DialogAffinage := nil;
  try
    DialogAffinage := TDialogAffinage.Create(Application);
    DialogAffinage.ReadOnly := ReadOnly;
    DialogAffinage.ShowModal;
    FreeAndNil(DialogAffinage);
  except

  end;
  if Assigned(DialogAffinage) then
  FreeAndNil(DialogAffinage);
end;

procedure TDialogAffinage.FormCreate(Sender: TObject);
var
  Rect: TRect;
begin
  Transaction.StartTransaction;
  StoredProcedure := TStoredProcedure.Create;
  StoredProcedure.Bind(TmpProcedure);
  RefreshPeriod;
  DateComboInvoiceBegin.Date := DateOf(StartOfTheMonth(Now));
  DateComboInvoiceEnd.Date := DateOf(EndOfTheMonth(Now));
  DataSetPassports.Active := True;
  DataSetInvoices.Active := True;
  GrayScaleImageList(EnabledLargeImages, DisabledLargeImages);
  CachedOperationID := 0;
  if not dm.db.DefaultTransaction.Active then
  dm.db.DefaultTransaction.StartTransaction;
  COperations.Active := True;
  COperations.ProviderName := '';
  CPlaceOfStorage.Active := True;
  CPlaceOfStorage.ProviderName := '';
  SystemParametersInfo(SPI_GETWORKAREA, 0, @Rect, 0);
  BoundsRect := Rect;
end;

procedure TDialogAffinage.FormDestroy(Sender: TObject);
begin
  DataSetPlaceOfStorageCustom.Active := False;
  DataSetInvoiceItems.Active := False;
  DataSetInvoices.Active := False;
  DataSetPassports.Active := False;
  StoredProcedure.Free;
  Transaction.Commit;
end;

procedure TDialogAffinage.SetReadOnly(const Value: Boolean);
begin
  FReadOnly := Value;
end;

procedure TDialogAffinage.RefreshPeriod;
begin
  StoredProcedure.Name := 'AFFINAGE$PASSPORT$PERIOD';
  StoredProcedure.Execute;
  PeriodBegin := StoredProcedure['Period$Begin'];
  PeriodEnd := StoredProcedure['Period$End'];
  DateComboPassportPeriodBegin.Date := PeriodBegin;
  DateComboPassportPeriodEnd.MinDate := PeriodBegin;
  DateComboPassportPeriodEnd.Date := PeriodEnd;
end;

procedure TDialogAffinage.ActionPassportAppendUpdate(Sender: TObject);
begin
  ActionPassportAppend.Enabled := not ReadOnly and DataSetPassports.Active;
end;

procedure TDialogAffinage.ActionPassportDeleteUpdate(Sender: TObject);
begin
  ActionPassportDelete.Enabled := not FReadOnly and DataSetPassports.Active and not DataSetPassports.IsEmpty;
end;

procedure TDialogAffinage.ActionPassportViewUpdate(Sender: TObject);
begin
  ActionPassportView.Enabled := DataSetPassports.Active and not DataSetPassports.IsEmpty;
end;

procedure TDialogAffinage.ActionPassportOpenUpdate(Sender: TObject);
begin
  ActionPassportOpen.Enabled := not ReadOnly and DataSetPassports.Active and not DataSetPassports.IsEmpty and (DataSetPassportsSTATE.AsInteger = 2);
end;

procedure TDialogAffinage.ActionPassportCloseUpdate(Sender: TObject);
begin
  ActionPassportClose.Enabled := not ReadOnly and DataSetPassports.Active and not DataSetPassports.IsEmpty and (DataSetPassportsSTATE.AsInteger = 1);
end;

procedure TDialogAffinage.ActionPassportRefreshUpdate(Sender: TObject);
var
  AReadOnly: Boolean;
begin
  AReadOnly := ReadOnly;
  if not AReadOnly then AReadOnly := not DataSetPassports.Active;
  if not AReadOnly then AReadOnly := DataSetPassports.IsEmpty;
  if not AReadOnly then AReadOnly := DataSetPassportsSTATE.AsInteger = 2;

  ActionPassportRefresh.Enabled := not AReadOnly;

  GridPassportAffinagePeriodEndEditor.Properties.EditProperties.ReadOnly := AReadOnly;
  GridPassportView.OptionsData.Editing := not AReadOnly;
  if AReadOnly then AReadOnly := not GroupEditing;
  ActionPassportDoGrouping.Enabled := not AReadOnly;
end;

procedure TDialogAffinage.ActionPassportExitUpdate(Sender: TObject);
begin
  ActionPassportExit.Enabled := True;
  DateComboPassportPeriodEnd.Enabled := not FReadOnly;
  GridPassportItemsView.OptionsData.Editing := not ReadOnly and DataSetPassports.Active and not DataSetPassports.IsEmpty and (DataSetPassportsSTATE.AsInteger = 1);
end;

procedure TDialogAffinage.ActionPassportAppendExecute(Sender: TObject);
var
  PassportID: Integer;
begin
  StoredProcedure.Name := 'SP_GEN_AFFINAGE$PASSPORT';
  StoredProcedure.Execute;
  PassportID := StoredProcedure['ID'];
  DataSetPassports.Append;
  DataSetPassportsID.AsInteger := PassportID;
  DataSetPassportsPeriodBegin.AsDateTime := Trunc(DateComboPassportPeriodBegin.Date);
  DataSetPassportsPeriodEnd.AsDateTime := Trunc(DateComboPassportPeriodEnd.Date);
  try
    DataSetPassports.Post;
    DataSetPassports.Transaction.CommitRetaining;
    RefreshPeriod;
  except
    on E: Exception do
    begin
      DialogErrorOkMessage(E.Message);
    end;
  end;
end;

procedure TDialogAffinage.ActionPassportDeleteExecute(Sender: TObject);
begin
  if DialogQuestionYesNoMessage('������� �������.') = mrYes then
  try
    DataSetPassports.Delete;
    DataSetPassports.Transaction.CommitRetaining;
  except
    on E: Exception do
    begin
      DialogErrorOkMessage(E.Message);
    end;  
  end;
  RefreshPeriod;
end;

procedure TDialogAffinage.ActionPassportViewExecute(Sender: TObject);
var
  PassportID: Integer;
  SQL: string;
begin
  PassportID := DataSetPassportsID.AsInteger;
  SQL := 'select first 1 id from affinage$passport where id > ' + IntTostr(PassportID);
  GroupEditing := VarIsNull(ExecSelectSQL(SQL, TmpQuery));
  CreateView;
  SheetPassports.TabVisible := True;
  PagesPassport.ActivePage := SheetPassport;
  SheetPassports.TabVisible := False;
end;

procedure TDialogAffinage.ActionPassportOpenExecute(Sender: TObject);
begin
  StoredProcedure.Name := 'AFFINAGE$PASSPORT$OPEN';
  StoredProcedure['ID'] := DataSetPassportsID.AsInteger;
  StoredProcedure.Execute;
  if StoredProcedure.Success then
  begin
    DataSetPassports.Refresh;
  end
  else
  begin
    DialogErrorOkMessage(StoredProcedure.ErrorDescription);
  end;
end;

procedure TDialogAffinage.ActionPassportCloseExecute(Sender: TObject);
begin
  StoredProcedure.Name := 'AFFINAGE$PASSPORT$CLOSE';
  StoredProcedure['ID'] := DataSetPassportsID.AsInteger;
  StoredProcedure.Execute;
  if StoredProcedure.Success then
  begin
    //ActionPassportRefresh.Execute;
    DataSetPassports.Refresh;
  end
  else
  begin
    DialogErrorOkMessage(StoredProcedure.ErrorDescription);
  end;
end;

procedure TDialogAffinage.ActionPassportRefreshExecute(Sender: TObject);
begin
  StoredProcedure.Name := 'AFFINAGE$PASSPORT$ITEM_C';
  StoredProcedure['PASSPORT$ID'] := DataSetPassportsID.AsInteger;
  StoredProcedure.Execute;
  CreateView(True);
end;

procedure TDialogAffinage.ActionPassportExitExecute(Sender: TObject);
begin
  Groups.Active := False;
  PassportGroups.Active := False;
  PassportItems.Active := False;
  SheetPassports.TabVisible := True;
  PagesPassport.ActivePage := SheetPassports;
  SheetPassport.TabVisible := False;
end;

procedure TDialogAffinage.DataSetPassportItemsBeforeOpen(DataSet: TDataSet);
begin
  DataSetPassportItems.ParamByName('PassportID').AsInteger := DataSetPassportsID.AsInteger;
end;

procedure TDialogAffinage.DataSetInvoicesBeforeOpen(DataSet: TDataSet);
begin
  DataSetInvoices.ParamByName('Period$Begin').AsDateTime := DateComboInvoiceBegin.Date;
  DataSetInvoices.ParamByName('Period$End').AsDateTime := DateComboInvoiceEnd.Date;
end;

procedure TDialogAffinage.ActionInvoiceAppendUpdate(Sender: TObject);
begin
  ActionInvoiceAppend.Enabled := not ReadOnly and DataSetInvoices.Active;
  ActionInvoiceAppendIn.Enabled := not ReadOnly and DataSetInvoices.Active;
  ActionInvoiceAppendOut.Enabled := not ReadOnly and DataSetInvoices.Active;
end;

procedure TDialogAffinage.ActionInvoiceDeleteUpdate(Sender: TObject);
begin
  ActionInvoiceDelete.Enabled := DataSetInvoices.Active and not DataSetInvoices.IsEmpty and not ReadOnly;
end;

procedure TDialogAffinage.ActionInvoiceViewUpdate(Sender: TObject);
begin
  ActionInvoiceView.Enabled := DataSetInvoices.Active and not DataSetInvoices.IsEmpty;
end;

procedure TDialogAffinage.ActionInvoiceOpenUpdate(Sender: TObject);
begin
  ActionInvoiceOpen.Enabled := DataSetInvoices.Active and not DataSetInvoices.IsEmpty and (DataSetInvoicesSTATE.AsInteger = 2) and not ReadOnly;
end;

procedure TDialogAffinage.ActionInvoiceCloseUpdate(Sender: TObject);
begin
  ActionInvoiceClose.Enabled := DataSetInvoices.Active and not DataSetInvoices.IsEmpty and (DataSetInvoicesSTATE.AsInteger = 1) and not ReadOnly;
end;

procedure TDialogAffinage.ActionInvoiceExitUpdate(Sender: TObject);
begin
  ActionInvoiceExit.Enabled := True;
  //GridInvoiceItemsViewMATERIALTITLE.Editing := DataSetInvoicesCLASS.AsInteger = 1;
  GridInvoiceItemsView.Bands[3].Visible := DataSetInvoicesCLASS.AsInteger = 1;
  if DataSetInvoicesCLASS.AsInteger = 1 then GridInvoiceItemsViewMATERIALTITLE.Options.ShowEditButtons := isebAlways
  else GridInvoiceItemsViewMATERIALTITLE.Options.ShowEditButtons := isebNever;
  GridInvoice.OptionsData.Editing := not ReadOnly and DataSetInvoices.Active and not DataSetInvoices.IsEmpty and (DataSetInvoicesState.AsInteger = 1);
  GridInvoiceItemsView.OptionsData.Editing := not ReadOnly and DataSetInvoices.Active and not DataSetInvoices.IsEmpty and (DataSetInvoicesState.AsInteger = 1);
end;

procedure TDialogAffinage.ActionInvoiceAppendExecute(Sender: TObject);
var
  InvoiceID: Integer;
begin
  StoredProcedure.Name := 'SP_GEN_AFFINAGE$INVOICE';
  StoredProcedure.Execute;
  InvoiceID := StoredProcedure['ID'];
  DataSetInvoices.Append;
  DataSetInvoicesID.AsInteger := InvoiceID;
  DataSetInvoicesCLASS.AsInteger := TAction(Sender).Tag;
  try
    DataSetInvoices.Post;
    DataSetInvoices.Transaction.CommitRetaining;
  except
    on E: Exception do
    begin
      DialogErrorOkMessage(E.Message);
    end;
  end;
end;

procedure TDialogAffinage.ActionInvoiceDeleteExecute(Sender: TObject);
begin
  try
    DataSetInvoices.Delete;
    DataSetInvoices.Transaction.CommitRetaining;
  except
    on E: Exception do
    begin
      DialogErrorOkMessage(E.Message);
    end;
  end;
end;

procedure TDialogAffinage.ActionInvoiceViewExecute(Sender: TObject);
begin
  DataSetMaterial.Active := False;
  DataSetMaterial.Active := True;  
  DataSetInvoiceItems.Active := True;
  SheetInvoice.TabVisible := True;
  PagesInvoice.ActivePage := SheetInvoice;
  SheetInvoices.TabVisible := False;
end;

procedure TDialogAffinage.ActionInvoiceOpenExecute(Sender: TObject);
begin
  StoredProcedure.Name := 'AFFINAGE$INVOICE$OPEN';
  StoredProcedure['ID'] := DataSetInvoicesID.AsInteger;
  StoredProcedure.Execute;
  if StoredProcedure.Success then
  begin
    DataSetInvoices.Refresh;
  end
  else
  begin
    DialogErrorOkMessage(StoredProcedure.ErrorDescription);
  end;
end;

procedure TDialogAffinage.ActionInvoiceCloseExecute(Sender: TObject);
begin
  if DataSetInvoiceItems.Active then
  begin
    if DataSetInvoiceItems.State <> dsBrowse then
    begin
      DataSetInvoiceItems.Post;
      Transaction.CommitRetaining;
    end;
  end;
  if DataSetInvoices.Active then
  begin
    if DataSetInvoices.State <> dsBrowse then
    DataSetInvoices.Post;
    Transaction.CommitRetaining;
  end;
  StoredProcedure.Name := 'AFFINAGE$INVOICE$CLOSE';
  StoredProcedure['ID'] := DataSetInvoicesID.AsInteger;
  StoredProcedure.Execute;
  if StoredProcedure.Success then
  begin
    DataSetInvoices.Refresh;
  end
  else
  begin
    DialogErrorOkMessage(StoredProcedure.ErrorDescription);
  end;
end;

procedure TDialogAffinage.ActionInvoiceExitExecute(Sender: TObject);
begin
  if DataSetInvoices.Active then
  begin
    if DataSetInvoices.State <> dsBrowse then
    DataSetInvoices.Post;
  end;

  if DataSetInvoiceItems.Active then
  begin
    if DataSetInvoiceItems.State <> dsBrowse then
    DataSetInvoiceItems.Post;
    DataSetInvoiceItems.Active := False;
  end;

  SheetInvoices.TabVisible := True;
  PagesInvoice.ActivePage := SheetInvoices;
  SheetInvoice.TabVisible := False;
end;

procedure TDialogAffinage.DateComboInvoiceBeginChange(Sender: TObject);
begin
  DataSetInvoices.Active := False;
  DataSetInvoices.Active := True;
end;

procedure TDialogAffinage.DataSetInvoiceItemsBeforeOpen(DataSet: TDataSet);
begin
  DataSetInvoiceItems.ParamByName('Invoice$ID').AsInteger := DataSetInvoicesID.AsInteger;
end;

procedure TDialogAffinage.DataSetInvoiceItemsBeforePost(DataSet: TDataSet);
var
  InvoiceItemID: Integer;
begin
  if not DataSetValidateRequired(DataSet) then Abort;

  if (DataSet.State = dsInsert) and (DataSetInvoiceItemsID.AsInteger = 0) then
  begin
    StoredProcedure.Name := 'SP_GEN_AFFINAGE$INVOICE$ITEM';
    StoredProcedure.Execute;
    InvoiceItemID := StoredProcedure['ID'];
    DataSetInvoiceItems['ID'] := InvoiceItemID;
    DataSetInvoiceItems['Invoice$ID'] := DataSetInvoicesID.AsInteger;
  end;
end;

procedure TDialogAffinage.OnDataSetValidate(DataSet: TDataSet);
begin
  if not DataSetValidateRequired(DataSet) then Abort;
end;

procedure TDialogAffinage.ActionInvoiceItemAddUpdate(Sender: TObject);
begin
  ActionInvoiceItemAdd.Enabled := DataSetInvoiceItems.Active and DataSetInvoices.Active and (DataSetInvoicesSTATE.AsInteger = 1) and not ReadOnly;
end;

procedure TDialogAffinage.ActionInvoiceItemDeleteUpdate(Sender: TObject);
begin
  ActionInvoiceItemDelete.Enabled := DataSetInvoiceItems.Active and not DataSetInvoiceItems.IsEmpty and DataSetInvoices.Active and (DataSetInvoicesSTATE.AsInteger = 1) and not ReadOnly;
end;

procedure TDialogAffinage.ActionInvoiceItemAddExecute(Sender: TObject);
begin
  DataSetInvoiceItems.Append;
end;

procedure TDialogAffinage.ActionInvoiceItemDeleteExecute(Sender: TObject);
begin
  if DialogQuestionYesNoMessage('������� ������ ���������.') = mrYes then
  DataSetInvoiceItems.Delete;
end;

procedure TDialogAffinage.DataSetPassportItemsCalcFields(DataSet: TDataSet);
begin
  DataSet['WEIGHT$REMAINDER'] := DataSetPassportItemsWEIGHT.AsFloat + DataSetPassportItemsWEIGHTREMAINDERIN.AsFloat;
end;

procedure TDialogAffinage.GridPassportsViewDblClick(Sender: TObject);
begin
  if ActionPassportView.Enabled then ActionPassportView.Execute;
end;

procedure TDialogAffinage.GridInvoicesDBBandedTableView1DblClick(Sender: TObject);
begin
  if ActionInvoiceView.Enabled then ActionInvoiceView.Execute;
end;

procedure TDialogAffinage.DataSetInvoicesAfterInsert(DataSet: TDataSet);
begin
  Transaction.CommitRetaining;
end;

procedure TDialogAffinage.DataSetInvoicesAfterDelete(DataSet: TDataSet);
begin
  Transaction.CommitRetaining;
  DataSetPassportsID.RefreshLookupList
end;

procedure TDialogAffinage.DataSetInvoiceItemsNewRecord(DataSet: TDataSet);
begin
  DataSetInvoiceItemsMATERIALMEASUREWEIGHTID.AsInteger := 163;
  DataSetInvoiceItemsMATERIALMEASUREQUANTITYID.AsInteger := 796;
  if DataSetInvoicesCLASS.AsInteger = 2 then
  begin
    DataSetInvoiceItemsMATERIALID.AsInteger := -1;
  end
  else
  if DataSetInvoicesCLASS.AsInteger = 1 then
  begin
    DataSetInvoiceItemsOPERATIONID.AsInteger := -1;
    DataSetInvoiceItemsPLACEOFSTORAGEID.AsInteger := -1;
  end;
end;

procedure TDialogAffinage.DataSetInvoicePrintHeaderBeforeOpen(
  DataSet: TDataSet);
begin
  DataSetInvoicePrintHeader.ParamByName('ID').AsInteger := DataSetInvoicesID.AsInteger;
end;

procedure TDialogAffinage.DataSetInvoicePrintItemBeforeOpen(
  DataSet: TDataSet);
begin
  DataSetInvoicePrintItem.ParamByName('ID').AsInteger := DataSetInvoicesID.AsInteger;
end;

procedure TDialogAffinage.ActionInvoicePrintExecute(Sender: TObject);
begin
  try
    DataSetInvoicePrintHeader.Active := True;
    DataSetInvoicePrintItem.Active := True;
    if DataSetInvoicesCLASS.AsInteger = 1 then
    begin
      ReportInvoiceOut.PrepareReport;
      ReportInvoiceOut.ShowPreparedReport;
    end
    else
    begin
      ReportInvoiceIn.PrepareReport;
      ReportInvoiceIn.ShowPreparedReport;
    end
  finally
    DataSetInvoicePrintHeader.Active := False;
    DataSetInvoicePrintItem.Active := False;
  end;
end;

procedure TDialogAffinage.ActionInvoicePrintUpdate(Sender: TObject);
begin
  ActionInvoicePrint.Enabled := DataSetInvoices.Active and not DataSetInvoices.IsEmpty;
end;

procedure TDialogAffinage.ActionInvoiceItemApplyUpdate(Sender: TObject);
begin
  ActionInvoiceItemApply.Enabled := DataSetInvoiceItems.Active and (DataSetInvoiceItems.State <> dsBrowse);
end;

procedure TDialogAffinage.ActionInvoiceItemCancelUpdate(Sender: TObject);
begin
  ActionInvoiceItemCancel.Enabled := DataSetInvoiceItems.Active and (DataSetInvoiceItems.State <> dsBrowse);
end;

procedure TDialogAffinage.ActionInvoiceItemApplyExecute(Sender: TObject);
begin
  DataSetInvoiceItems.Post;
end;

procedure TDialogAffinage.ActionInvoiceItemCancelExecute(Sender: TObject);
begin
  DataSetInvoiceItems.Cancel;
end;

procedure TDialogAffinage.DataSetInvoiceItemsBeforeInsert(
  DataSet: TDataSet);
begin
  GridInvoiceItemsView.OptionsView.NewItemRow := True;
end;

procedure TDialogAffinage.DataSetInvoiceItemsAfterCancel(
  DataSet: TDataSet);
begin

  GridInvoiceItemsView.OptionsView.NewItemRow := False;end;

procedure TDialogAffinage.DataSetCommitRetaining(DataSet: TDataSet);
begin
  Transaction.CommitRetaining;
  GridInvoiceItemsView.OptionsView.NewItemRow := False;
end;

procedure TDialogAffinage.GridInvoiceItemsViewPLACEOFSTORAGETITLEPropertiesInitPopup(Sender: TObject);
var
  OperationID: Integer;
begin
  GridPlaceOfStorageView.DataController.DataSource := DataSourcePlaceOfStorageCustom;

  if DataSetPlaceOfStorageCustom.Active then
  begin
    OperationID := DataSetInvoiceItemsOPERATIONID.AsInteger;
    if OperationID <> CachedOperationID then
    begin
      DataSetPlaceOfStorageCustom.Active := False;
      DataSetPlaceOfStorageCustom.Active := True;
      CachedOperationID := OperationID;
    end;
  end
  else DataSetPlaceOfStorageCustom.Active := True;
end;

procedure TDialogAffinage.DataSetPlaceOfStorageCustomBeforeOpen(
  DataSet: TDataSet);
begin
  if DataSetInvoiceItemsOPERATIONID.IsNull then DataSetPlaceOfStorageCustom.ParamByName('OPERATION$ID').AsVariant := Null
  else DataSetPlaceOfStorageCustom.ParamByName('OPERATION$ID').AsVariant := DataSetInvoiceItemsOPERATIONID.AsInteger;

end;

procedure TDialogAffinage.DataSourceInvoiceItemsDataChange(Sender: TObject; Field: TField);
begin
  if Field = nil then
  GridPlaceOfStorageView.DataController.DataSource := DataSourcePlaceOfStorage;  
end;

procedure TDialogAffinage.DataSetMaterialBeforeOpen(DataSet: TDataSet);
begin
  DataSetMaterial.ParamByName('CLASS').AsInteger := DataSetInvoicesCLASS.AsInteger;
end;

procedure TDialogAffinage.DataSetPassportsAFFINAGEPERIODENDChange(Sender: TField);
begin
  if DataSetPassports.State = dsEdit then
  begin
    if Sender.AsDateTime <> AffinagePeriodEnd then
    begin
      AffinagePeriodEnd := Sender.AsDateTime;
      DataSetPassports.Post;
      DataSetPassports.Transaction.CommitRetaining;
      ActionPassportRefresh.Execute;
    end;
  end;
end;

procedure TDialogAffinage.CalculateGroupVariants;
var
  GroupItemsClone: TClientDataSet;
  GroupsClone: TClientDataSet;
  OperationID: Integer;
  PlaceOfStorageID: Integer;
  PassportItemsClone: TClientDataSet;
  Found: Boolean;
begin
  DialogPassportConfigure.GridVariantsView.DataController.BeginFullUpdate;

  GroupsClone := TClientDataSet.Create(Self);
  GroupsClone.Data := Groups.Data;

  GroupItemsClone := TClientDataSet.Create(Self);
  GroupItemsClone.DataSetField := TDataSetField(GroupsClone.FieldByName('Items'));


  PassportItemsClone := TClientDataSet.Create(Self);
  PassportItemsClone.CloneCursor(PassportItems, True, False);

  GroupItemsAll.CreateDataSet;

  if GroupItemVariants.Active then GroupItemVariants.EmptyDataSet
  else GroupItemVariants.CreateDataSet;

  GroupsClone.First;
  while not GroupsClone.Eof do
  begin
    GroupItemsClone.First;
    while not GroupItemsClone.Eof do
    begin
      GroupItemsAll.Append;
      GroupItemsAllOperationID.AsVariant := GroupItemsClone.FieldByName('Operation$ID').AsVariant;
      GroupItemsAllPlaceOfStorageID.AsVariant := GroupItemsClone.FieldByName('Place$Of$Storage$ID').AsVariant;
      GroupItemsAll.Post;
      GroupItemsClone.Next;
    end;
    GroupsClone.Next;
  end;

  PassportItemsClone.First;
  while not PassportItemsClone.Eof do
  begin
    OperationID := PassportItemsClone.FieldByName('Operation$ID').AsInteger;
    PlaceOfStorageID := PassportItemsClone.FieldByName('Place$Of$Storage$ID').AsInteger;

    Found := False;

    if not Found then
    Found := GroupItemsAll.FindKey([OperationID, -1]);
    //Found := GroupItemsAll.FindKey([OperationID, Null]);
    if not Found then
    Found := GroupItemsAll.FindKey([-1, PlaceOfStorageID]);
    //Found := GroupItemsAll.FindKey([Null, PlaceOfStorageID]);
    if not Found then
    Found := GroupItemsAll.FindKey([OperationID, PlaceOfStorageID]);

    if not Found then
    begin
      GroupItemVariants.Append;
      GroupItemVariantsOperationID.AsInteger := OperationID;
      GroupItemVariantsOperationTitle.AsString := PassportItemsClone.FieldByName('Operation$Title').AsString;
      GroupItemVariantsPlaceOfStorageTitle.AsString := PassportItemsClone.FieldByName('Place$Of$Storage$Title').AsString;
      GroupItemVariantsPlaceOfStorageID.AsInteger := PlaceOfStorageID;
      GroupItemVariants.Post;
    end;
    PassportItemsClone.Next;
  end;
  GroupItemsAll.Active := False;
  PassportItemsClone.Free;
  GroupItemsClone.Free;
  GroupsClone.Free;
  DialogPassportConfigure.GridVariantsView.DataController.EndFullUpdate;
  DialogPassportConfigure.GridVariantsView.DataController.GotoFirst;
end;


procedure TDialogAffinage.DataSetPassportsBeforeEdit(DataSet: TDataSet);
begin
  AffinagePeriodEnd := DataSetPassportsAFFINAGEPERIODEND.AsDateTime;
end;

procedure TDialogAffinage.GroupItemsBeforePost(DataSet: TDataSet);
var
  GroupItemsClone: TClientDataSet;
  GroupsClone: TClientDataSet;
  OperationID: Variant;
  PlaceOfStorageID: Variant;
  AOperationID: Variant;
  APlaceOfStorageID: Variant;
  NeedAbort: Boolean;
begin
  if Flag = 0 then
  begin
    NeedAbort := False;

    OperationID := GroupItemsOperationID.AsVariant;
    PlaceOfStorageID := GroupItemsPlaceOfStorageId.AsVariant;

    GroupsClone := TClientDataSet.Create(Self);
    GroupsClone.Data := GroupsData;
    GroupItemsClone := TClientDataSet.Create(Self);
    GroupItemsClone.DataSetField := TDataSetField(GroupsClone.FieldByName('Items'));

    GroupItemsAll.CreateDataSet;

    GroupsClone.First;
    while not GroupsClone.Eof do
    begin
      GroupItemsClone.First;
      while not GroupItemsClone.Eof do
      begin
        AOperationID := GroupItemsClone.FieldByName('Operation$ID').AsVariant;
        APlaceOfStorageID := GroupItemsClone.FieldByName('Place$Of$Storage$ID').AsVariant;
        if GroupItems.State = dsEdit then
        begin
          if (AOperationID <> StoredOperationID) or (APlaceOfStorageID <> StoredPlaceOfStorageID) then
          begin
            GroupItemsAll.Append;
            GroupItemsAllOperationID.AsVariant := AOperationID;
            GroupItemsAllPlaceOfStorageID.AsVariant := APlaceOfStorageID;
            GroupItemsAll.Post;
          end;
        end
        else
        begin
          GroupItemsAll.Append;
          GroupItemsAllOperationID.AsVariant := AOperationID;
          GroupItemsAllPlaceOfStorageID.AsVariant := APlaceOfStorageID;
          GroupItemsAll.Post;
        end;
        GroupItemsClone.Next;
      end;
      GroupsClone.Next;
    end;

    if GroupItemsAll.FindKey([OperationID, PlaceOfStorageID]) then
    begin
      DialogErrorOkMessage('������� ��� ����������.');
      NeedAbort := True;
    end;

    if not NeedAbort and GroupItemsAll.FindKey([-1, PlaceOfStorageID]) then
    //if not NeedAbort and GroupItemsAll.FindKey([Null, PlaceOfStorageID]) then
    begin
      DialogErrorOkMessage('������� ��� ���������� � ����� ������� ������.');
      NeedAbort := True;
    end;
    if not NeedAbort and GroupItemsAll.FindKey([OperationID, -1]) then
    //if not NeedAbort and GroupItemsAll.FindKey([OperationID, Null]) then
    begin
      DialogErrorOkMessage('������� ��� ���������� � ����� ������� ������.');
      NeedAbort := True;
    end;
    if not NeedAbort then
    begin
      if (OperationID = -1) or (PlaceOfStorageID = -1) then
      //if VarIsNull(OperationID) or VarIsNull(PlaceOfStorageID) then
      begin
        if PlaceOfStorageID = -1 then
        //if VarIsNull(PlaceOfStorageID) then
        begin
          GroupItemsAll.IndexFieldNames := 'Operation$ID';
          GroupItemsAll.SetRange([OperationID], [OperationID]);
          if GroupItemsAll.RecordCount <> 0 then
          begin
            DialogErrorOkMessage('���������� ����� ����� ������.');
            NeedAbort := True;
          end;
          GroupItemsAll.CancelRange;
          GroupItemsAll.IndexFieldNames := 'Operation$ID;Place$Of$Storage$Id';
        end
        else
        if (OperationID = -1) then
        //if VarIsNull(OperationID) then
        begin
          GroupItemsAll.IndexFieldNames := 'Place$Of$Storage$Id';
          GroupItemsAll.SetRange([PlaceOfStorageID], [PlaceOfStorageID]);
          if GroupItemsAll.RecordCount <> 0 then
          begin
            DialogErrorOkMessage('���������� ����� ����� ������.');
            NeedAbort := True;
          end;
          GroupItemsAll.CancelRange;
          GroupItemsAll.IndexFieldNames := 'Operation$ID;Place$Of$Storage$Id';
        end;
      end;
    end;

    GroupItemsAll.Active := False;
    GroupItemsClone.Free;
    GroupsClone.Free;

    if NeedAbort then Abort;
  end;
end;

procedure TDialogAffinage.GroupItemsAfterDelete(DataSet: TDataSet);
begin
  CalculateGroupVariants;
end;

procedure TDialogAffinage.PassportItemsBeforeOpen(DataSet: TDataSet);
begin
  PassportItems.Params.ParamByName('PassportID').AsInteger := DataSetPassportsID.AsInteger;
end;

procedure TDialogAffinage.CreateView(Refresh: Boolean = False);
var
  GroupItemsClone: TClientDataSet;
  GroupsClone: TClientDataSet;
  PassportGroupsClone: TClientDataSet;
  PassportItemsClone: TClientDataSet;

  OperationID: Integer;
  PlaceOfStorageID: Integer;
  GhostGroupID: Integer;
  OperationTitle: string;
  PlaceOfStorageTitle: string;
  GroupID: Integer;
  GroupTitle: string;
  Found: Boolean;
  Weight: Double;
  WeightIn: Double;
  IndexFieldNames: string;
  WeightRemainderOut:Double;
  WeightRemainderIn:Double;
  WeightRemainder:Double;
begin
  if not Groups.Active then LoadGroups(DataSetPassportsID.AsInteger, Groups)
  else if Refresh then LoadGroups(DataSetPassportsID.AsInteger, Groups, True);
  //GroupItems.LogChanges := False;

  GridPassportView.DataController.BeginFullUpdate;

  if PassportGroups.Active then PassportGroups.EmptyDataSet
  else PassportGroups.CreateDataSet;

  if Refresh then
  begin
    if PassportItems.Active then
    PassportItems.Active := False;
    Passportitems.ProviderName := 'ProviderPassportItems';
    Passportitems.Active := True;
    Passportitems.ProviderName := '';
  end
  else
  begin
    if not PassportItems.Active then
    begin
      Passportitems.ProviderName := 'ProviderPassportItems';
      Passportitems.Active := True;
      Passportitems.ProviderName := '';
    end;
  end;

  GroupsClone := TClientDataSet.Create(Self);
  GroupsClone.Data := Groups.Data;

  GroupItemsClone := TClientDataSet.Create(Self);
  GroupItemsClone.DataSetField := TDataSetField(GroupsClone.FieldByName('Items'));

  PassportGroupsClone := TClientDataSet.Create(Self);
  PassportGroupsClone.CloneCursor(PassportGroups, True, False);
  PassportGroupsClone.IndexFieldNames := 'ID';

  IndexFieldNames := PassportItems.IndexFieldNames;
  PassportItems.IndexFieldNames := '';
  PassportItems.LogChanges := False;

  PassportItemsClone := TClientDataSet.Create(Self);
  PassportItemsClone.CloneCursor(PassportItems, True, False);

  GroupItemsAll.CreateDataSet;

  GroupsClone.First;
  while not GroupsClone.Eof do
  begin
    GroupItemsClone.First;
    while not GroupItemsClone.Eof do
    begin
      GroupItemsAll.Append;
      GroupItemsAllOperationID.AsVariant := GroupItemsClone.FieldByName('Operation$ID').AsVariant;
      GroupItemsAllPlaceOfStorageID.AsVariant := GroupItemsClone.FieldByName('Place$Of$Storage$ID').AsVariant;
      GroupItemsAllGroupID.AsInteger := GroupsClone.FieldByName('ID').AsInteger;
      GroupItemsAll.Post;
      GroupItemsClone.Next;
    end;
    GroupsClone.Next;
  end;

  GroupsClone.First;
  while not GroupsClone.Eof do
  begin
    PassportGroupsClone.Append;
    PassportGroupsClone['ID'] := GroupsClone.FieldByName('ID').AsInteger;
    PassportGroupsClone['Title'] := GroupsClone.FieldByName('Title').AsString;


    WeightRemainderIn := GroupsClone.FieldByName('Weight$Remainder$In').AsFloat;
    WeightRemainderOut := GroupsClone.FieldByName('Weight$Remainder$Out').AsFloat;
    WeightRemainder := WeightRemainderIn;

    PassportGroupsClone['Weight'] := 0;
    PassportGroupsClone['Weight$In'] := 0;
    PassportGroupsClone['Weight$Remainder$Current'] := WeightRemainder;
    PassportGroupsClone['Weight$Remainder$Out'] := WeightRemainderOut;
    PassportGroupsClone['Weight$Remainder$In'] := WeightRemainderIn;
    PassportGroupsClone['Weight$Remainder'] := WeightRemainder;

    PassportGroupsClone.Post;
    GroupsClone.Next;
  end;

  GhostGroupID := -1;

  PassportItemsClone.First;

  while not PassportItemsClone.Eof do
  begin
    OperationID := PassportItemsClone.FieldByName('Operation$ID').AsInteger;
    PlaceOfStorageID := PassportItemsClone.FieldByName('Place$Of$Storage$ID').AsInteger;
    Weight := PassportItemsClone.FieldByName('Weight').AsFloat;
    WeightIn := PassportItemsClone.FieldByName('Weight$In').AsFloat;

    Found := False;

    if not Found then
    Found := GroupItemsAll.FindKey([OperationID, Null]);
    if not Found then
    Found := GroupItemsAll.FindKey([Null, PlaceOfStorageID]);
    if not Found then
    Found := GroupItemsAll.FindKey([OperationID, PlaceOfStorageID]);

    if Found then
    begin
      GroupID := GroupItemsAllGroupID.AsInteger;
      PassportItemsClone.Edit;
      PassportItemsClone['Group$ID']:= GroupID;
      PassportItemsClone.Post;
      PassportGroupsClone.FindKey([GroupID]);
      PassportGroupsClone.Edit;
      PassportGroupsClone['Weight'] := PassportGroupsClone['Weight'] + Weight;
      PassportGroupsClone['Weight$In'] := PassportGroupsClone['Weight$In'] + WeightIn;
      PassportGroupsClone['Weight$Remainder$Current'] := PassportGroupsClone['Weight$Remainder$Current'] + Weight - WeightIn;
      PassportGroupsClone['Weight$Remainder'] := PassportGroupsClone['Weight$Remainder'] + Weight;
      PassportGroupsClone.Post;
    end
    else
    begin
      OperationTitle := PassportItemsClone.FieldByName('Operation$Title').AsString;
      PlaceOfStorageTitle := PassportItemsClone.FieldByName('Place$Of$Storage$Title').AsString;
      GroupTitle := OperationTitle + ', ' + PlaceOfStorageTitle;

      PassportItemsClone.Edit;
      PassportItemsClone['Group$ID'] := GhostGroupID;
      PassportItemsClone.Post;

      PassportGroupsClone.Append;
      PassportGroupsClone['ID'] := GhostGroupID;
      PassportGroupsClone['Title'] := GroupTitle;
      PassportGroupsClone['Weight'] := Weight;
      PassportGroupsClone['Weight$In'] := WeightIn;
      PassportGroupsClone['Weight$Remainder$Current'] := Weight - WeightIn;
      PassportGroupsClone['Weight$Remainder$Out'] := 0;
      PassportGroupsClone['Weight$Remainder$In'] := 0;
      PassportGroupsClone['Weight$Remainder'] := Weight;
      PassportGroupsClone.Post;
      Dec(GhostGroupID);
    end;
    PassportItemsClone.Next;
  end;

  GroupItemsAll.Active := False;
  GroupItemsClone.Free;
  GroupsClone.Free;
  PassportGroupsClone.Free;
  PassportItemsClone.Free;

  PassportItems.IndexFieldNames := IndexFieldNames;
  GridPassportView.DataController.EndFullUpdate;
  GridPassportView.DataController.GotoFirst;
end;


procedure TDialogAffinage.ActionPassportDoGroupingExecute(Sender: TObject);
begin
  DialogPassportConfigure := TDialogPassportConfigure.Create(Self);
  CalculateGroupVariants;
  if DialogPassportConfigure.ShowModal= mrOK then
  begin
    SaveGroups(DataSetPassportsID.AsInteger, Groups);
    CreateView;
  end;
  DialogPassportConfigure.Free;
end;

procedure TDialogAffinage.PassportGroupsBeforeEdit(DataSet: TDataSet);
begin
  GroupWeightRemainderOut := PassportGroupsWEIGHTREMAINDEROUT.AsFloat;
end;

procedure TDialogAffinage.PassportGroupsBeforePost(DataSet: TDataSet);
var
  GroupID: Integer;
  GroupTitle: string;
  OperationID: Integer;
  PlaceOfStorageID: Integer;
  PassportItemsClone: TClientDataSet;
begin
  if PassportGroups.State = dsEdit then
  begin
    GroupID := PassportGroupsID.AsInteger;
    if GroupID < 0 then
    begin
      PassportItemsClone := TClientDataSet.Create(Self);
      PassportItemsClone.CloneCursor(PassportItems, True, False);
      PassportItemsClone.IndexFieldNames := 'Group$ID';
      if PassportItemsClone.FindKey([GroupID]) then
      begin
        GroupTitle := PassportGroupsTitle.AsString;
        Groups.Append;
        GroupsTitle.AsString := GroupTitle;
        GroupsWEIGHTRemainderOUT.AsFloat := PassportGroupsWEIGHTREMAINDEROUT.AsFloat;
        Groups.Post;
        GroupID := GroupsID.AsInteger;

        OperationID := PassportItemsClone.FieldByName('Operation$ID').AsInteger;
        PlaceOfStorageID := PassportItemsClone.FieldByName('Place$Of$Storage$ID').AsInteger;

        Flag := 1;
        GroupItems.Append;
        GroupItemsOperationID.AsInteger := OperationID;
        GroupItemsPlaceOfStorageId.AsInteger := PlaceOfStorageID;
        GroupItems.Post;

        PassportItemsClone.Edit;
        PassportItemsClone['Group$ID'] := GroupID;
        PassportItemsClone.Post;

        PassportGroupsID.AsInteger := GroupID;

        Flag := 0;
      end;
      PassportItemsClone.Free;
    end
    else
    begin
      Groups.IndexFieldNames := 'ID';
      Groups.FindKey([GroupID]);
      Groups.Edit;
      GroupsWEIGHTRemainderOUT.AsFloat := PassportGroupsWEIGHTREMAINDEROUT.AsFloat;
      Groups.Post;
      Groups.IndexFieldNames := 'Title';
    end;
    SaveGroups(DataSetPassportsID.AsInteger, Groups);
  end;
end;

procedure TDialogAffinage.GridPassportViewColumn1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  Value: Variant;
begin
  if not AItem.Options.Editing then
  begin
    Value := ARecord.Values[0];
    if not (VarIsEmpty(Value) or VarIsNull(Value)) then
    begin
      if Value < 0 then AStyle := StyleVirtual
      else AStyle := StyleReadOnly;
    end;
  end;
end;

procedure TDialogAffinage.LoadGroups(PassportID: Integer; DataSet: TClientDataSet; Refresh: Boolean = False);
var
  Stream: TStream;
  Query: TpFIBQuery;
  Result: Variant;
  SQL: string;
  PriorPassportID: Integer;
  Prior: TClientDataSet;
  Current: TClientDataSet;
  PassportState: Integer;
  GroupID: Integer;
begin
  if Refresh then
  begin
    SQL := 'select first 1 id, state from affinage$passport where id < ' + IntTostr(PassportID) + ' order by id desc';
    Result := ExecSelectSQL(SQL, TmpQuery);
    if not varIsNull(Result[0]) then
    begin
      PassportState := Result[1];
      if PassportState = 2 then
      begin
        PriorPassportID := Result[0];
        Prior := TClientDataSet.Create(Self);
        Current := TClientDataSet.Create(Self);
        LoadGroups(PassportID, Current);
        Current.LogChanges := False;
        LoadGroups(PriorPassportID, Prior);
        Prior.IndexDefs.Add('ID', 'ID', []);
        Prior.IndexFieldNames := 'ID';
        Current.First;
        while not Current.Eof do
        begin
          GroupID := Current.FieldByName('ID').AsInteger;
          if Prior.FindKey([GroupID]) then
          begin
            Current.Edit;
            Current.FieldByName('Weight$Remainder$In').AsFloat := Prior.FieldByName('Weight$Remainder$Out').AsFloat;
            Current.Post;
          end;
          Current.Next;
        end;
        if DataSet.Active then
        DataSet.Active := False;
        DataSet.FieldDefs.Clear;
        DataSet.Data := Current.Data;
        DataSet.LogChanges := False;
        SaveGroups(PassportID, DataSet);
        Current.Free;
        Prior.Free;
      end;
    end;
  end
  else
  begin

    if DataSet.Active then
    DataSet.Active := False;
    DataSet.FieldDefs.Clear;

    Query := TpFIBQuery.Create(Self);
    Query.Database := DataSetPassports.Database;
    Query.SQL.Text := 'select groups from affinage$passport passport where id = :id';
    Query.ParamByName('ID').AsInteger := PassportID;
    Query.ExecQuery;

    if Query.Fields[0].IsNull then
    begin
      SQL := 'select first 1 id, state from affinage$passport where id < ' + IntTostr(PassportID) + ' order by id desc';
      Result := ExecSelectSQL(SQL, TmpQuery);
      if VarIsNull(Result[0]) then
      begin
        DataSet.CreateDataSet;
        DataSet.LogChanges := False;
      end
      else
      begin
        PriorPassportID := Result[0];
        PassportState := Result[1];
        Prior := TClientDataSet.Create(Self);
        LoadGroups(PriorPassportID, Prior);
        Prior.LogChanges := False;
        Prior.First;
        while not Prior.Eof do
        begin
          Prior.Edit;
          if PassportState = 2 then Prior['Weight$Remainder$In'] := Prior['Weight$Remainder$Out']
          else Prior['Weight$Remainder$In'] := 0;
          Prior['Weight$Remainder$Out'] := 0;
          Prior.Post;
          Prior.Next;
        end;
        DataSet.Data := Prior.Data;
        DataSet.LogChanges := False;
        SaveGroups(PassportID, DataSet);
        Prior.Free;
      end;
    end
    else
    begin
      Stream := TMemoryStream.Create;
      Query.Fields[0].SaveToStream(Stream);
      Stream.Position := 0;
      DataSet.LoadFromStream(Stream);
      DataSet.LogChanges := False;
      Stream.Free;
    end;
    Query.Transaction.CommitRetaining;
    Query.Free;
  end;  
end;

procedure TDialogAffinage.SaveGroups(PassportID: Integer; DataSet: TClientDataSet);
var
  Stream: TStream;
  Query: TpFIBQuery;
begin
  if DataSet.Active then
  begin
    Stream := TMemoryStream.Create;
    DataSet.SaveToStream(Stream, dfBinary);
    Stream.Position := 0;
    Query := TpFIBQuery.Create(Self);
    Query.Database := DataSetPassports.Database;
    Query.SQL.Text := 'update affinage$passport set groups = :groups where id = :id';
    Query.ParamByName('ID').AsInteger := PassportID;
    Query.ParamByName('Groups').LoadFromStream(Stream);
    Query.ExecQuery;
    Query.Transaction.CommitRetaining;
    Query.Free;
    Stream.Free;
  end;
end;

procedure TDialogAffinage.GridPassportViewWEIGHTREMAINDEROUTPropertiesButtonClick(Sender: TObject; AButtonIndex: Integer);
begin
  PassportGroups.Edit;
  PassportGroupsWEIGHTREMAINDEROUT.AsFloat := PassportGroupsWEIGHTREMAINDERCURRENT.AsFloat;
  PassportGroups.Post;
end;

procedure TDialogAffinage.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if PagesPassport.ActivePage = SheetPassport then
  ActionPassportExit.Execute;
end;

procedure TDialogAffinage.ActionExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TDialogAffinage.GroupItemsBeforeInsert(DataSet: TDataSet);
begin
  GroupsData := Groups.Data;
end;

procedure TDialogAffinage.GroupItemsBeforeEdit(DataSet: TDataSet);
begin
  StoredOperationID := GroupItemsOperationID.AsVariant;
  StoredPlaceOfStorageID := GroupItemsPlaceOfStorageID.AsVariant;
  GroupsData := Groups.Data;
end;

procedure TDialogAffinage.GroupItemsAfterCancel(DataSet: TDataSet);
begin
  GroupsData := Unassigned;
  if Groups.State in [dsEdit, dsInsert] then Groups.Cancel;  
end;

procedure TDialogAffinage.GroupItemsAfterPost(DataSet: TDataSet);
begin
  if Groups.State in [dsEdit, dsInsert] then Groups.Post;
  GroupsData := Unassigned;
  if Flag = 0 then
  CalculateGroupVariants;
end;

procedure TDialogAffinage.ActionPassportPrintExecute(Sender: TObject);
var
  i: Integer;
  List: TList;
  Link: TdxGridReportLink;
  Expanded: Boolean;
begin
  List := TList.Create;
  for i := 0 to GridPassportView.ViewData.RowCount - 1 do
  begin
    Expanded :=  TcxGridMasterDataRow(GridPassportView.ViewData.Rows[i]).Expanded;
    if Expanded then List.Add(Pointer(Integer(1)))
    else List.Add(nil);
  end;

  Link := TdxGridReportLink(Printer.LinkByName('GridPassportItemsPrinterLink'));
  Link.ReportTitle.Text := '������� �������� � '  + DataSetPassportsDESIGNATION.AsString;
  Link.ReportDocument.Caption := Link.ReportTitle.Text;
  Link.ReportDocument.CreationDate := DataSetPassportsCREATEDDATE.AsDateTime;
  Link.ReportDocument.Creator := DataSetPassportsCREATEDBYTITLE.AsString;
  Link.Preview;

  GridPassportView.BeginUpdate;
  for i := 0 to GridPassportView.ViewData.RowCount - 1 do
  begin
    Expanded := List[i] <> nil;
    TcxGridMasterDataRow(GridPassportView.ViewData.Rows[i]).Expanded := Expanded;
  end;
  GridPassportView.EndUpdate;
  List.Free;
end;

procedure TDialogAffinage.GroupsBeforePost(DataSet: TDataSet);
var
  OperationID: Integer;
  PlaceOfStorageID: Integer;
  OperationTitle: string;
  PlaceOfStorageTitle: string;
  Title: string;
begin

  OperationID := GroupsOperationID.AsInteger;

  if OperationID = 0 then
  begin
    DialogErrorOkMessage('�� ������� ��������.');
    Abort;
  end;

  if OperationID = -1 then
  begin
    DialogErrorOkMessage('�������� �� ����� ���� "�� ����������".');
    Abort;
  end;

  PlaceOfStorageID := GroupsPlaceOfStorageId.AsInteger;

  if PlaceOfStorageID = 0 then
  begin
    DialogErrorOkMessage('�� ������� ����� ��������.');
    Abort;
  end;

  if PlaceOfStorageID = -1 then
  begin
    DialogErrorOkMessage('����� �������� �� ����� ���� "���".');
    Abort;
  end;

  OperationTitle := GroupsOperationTitle.AsString;
  PlaceOfStorageTitle := GroupsPlaceOfStorageTitle.AsString;
  Title := OperationTitle + ', ' + PlaceOfStorageTitle;
  GroupsTitle.AsString := Title;
end;

end.


