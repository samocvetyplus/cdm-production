inherited fmTransMat: TfmTransMat
  Left = 217
  Top = 193
  Caption = #1052#1072#1090#1077#1088#1080#1072#1083#1099' '#1087#1088#1077#1086#1073#1088#1072#1079#1091#1077#1084#1099#1077' '#1074' '#1084#1077#1090#1072#1083#1083', '#1074' '#1093#1086#1076#1077' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1072
  ClientWidth = 575
  ExplicitWidth = 583
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 575
    ExplicitWidth = 452
  end
  inherited plDict: TPanel
    Width = 575
    ExplicitWidth = 452
    inherited plSelect: TPanel
      Width = 571
      ExplicitWidth = 448
      inherited btnOk: TSpeedButton
        Left = 393
        ExplicitLeft = 270
      end
      inherited btnCancel: TSpeedButton
        Left = 477
        ExplicitLeft = 354
      end
    end
    inherited gridDict: TDBGridEh
      Width = 571
      DataSource = dm.dsrTransMat
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'MATNAME'
          Footers = <>
          Width = 186
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'N'
          Footers = <>
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NT'
          Footers = <>
          KeyList.Strings = (
            '0'
            '1')
          PickList.Strings = (
            #1086#1090' '#1074#1077#1089#1072
            #1086#1090' '#1082#1086#1083'-'#1074#1072)
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'K'
          Footers = <>
          Title.EndEllipsis = True
        end
        item
          EditButtons = <>
          FieldName = 'BASEMAT'
          Footers = <>
          KeyList.Strings = (
            '1'
            '3')
          PickList.Strings = (
            #1079#1086#1083#1086#1090#1086
            #1089#1077#1088#1077#1073#1088#1086)
          Title.Caption = #1052#1077#1090#1072#1083#1083
        end>
    end
  end
  inherited tb1: TSpeedBar
    Width = 575
    ExplicitWidth = 452
  end
end
