inherited fmAppl: TfmAppl
  Left = 229
  Top = 0
  Caption = #1047#1072#1082#1072#1079
  ClientHeight = 692
  ClientWidth = 1031
  Position = poDesigned
  ExplicitWidth = 1039
  ExplicitHeight = 726
  PixelsPerInch = 96
  TextHeight = 13
  object spitWhA2: TSplitter [0]
    Left = 0
    Top = 561
    Width = 1031
    Height = 4
    Cursor = crVSplit
    Align = alBottom
    Visible = False
    ExplicitTop = 485
    ExplicitWidth = 988
  end
  object splAssort: TSplitter [1]
    Left = 0
    Top = 445
    Width = 1031
    Height = 4
    Cursor = crVSplit
    Align = alBottom
    ExplicitTop = 369
    ExplicitWidth = 988
  end
  inherited stbrStatus: TStatusBar
    Top = 673
    Width = 1031
    Panels = <
      item
        Width = 200
      end>
    ExplicitTop = 673
    ExplicitWidth = 1031
  end
  inherited tb1: TSpeedBar
    Width = 1031
    Height = 41
    TabOrder = 2
    ExplicitWidth = 1031
    ExplicitHeight = 41
    inherited spitAdd: TSpeedItem [1]
      DropDownMenu = ppAdd
      Hint = ''
      OnClick = acAddRecordExecute
    end
    inherited spitDel: TSpeedItem [2]
      Action = acDelRecord
      Hint = ''
      OnClick = acDelRecordExecute
    end
    inherited spitPrint: TSpeedItem [3]
      Action = acPrintAppl
      Hint = ''
      OnClick = acPrintApplExecute
    end
    inherited spitClose: TSpeedItem [4]
      Action = acClose
      Hint = ''
      OnClick = acCloseExecute
    end
    inherited spitExit: TSpeedItem [5]
      Left = 515
    end
    object SpeedItem1: TSpeedItem
      Action = acImportAnaliz
      BtnCaption = #1048#1084#1087#1086#1088#1090
      Caption = #1048#1084#1087#1086#1088#1090
      ImageIndex = 8
      Spacing = 1
      Left = 259
      Top = 3
      Visible = True
      OnClick = acImportAnalizExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      Action = acLinkAnaliz
      BtnCaption = #1055#1088#1080#1089#1086#1077#1076#1077#1085#1080#1090#1100
      Caption = 'SpeedItem2'
      ImageIndex = 9
      Spacing = 1
      Left = 451
      Top = 3
      OnClick = acLinkAnalizExecute
      SectionName = 'Untitled (0)'
    end
    object SpeedItem3: TSpeedItem
      BtnCaption = #1054#1073#1085#1086#1074#1080#1090#1100
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100
      DropDownMenu = ppRefresh
      Hint = #1054#1073#1085#1086#1074#1080#1090#1100'|'
      ImageIndex = 3
      Spacing = 1
      Left = 323
      Top = 3
      Visible = True
      SectionName = 'Untitled (0)'
    end
    object ButtonArticleStructure: TSpeedItem
      BtnCaption = #1057#1086#1089#1090#1072#1074
      Caption = #1057#1086#1089#1090#1072#1074
      Hint = #1057#1086#1089#1090#1072#1074'|'#1057#1086#1089#1090#1072#1074' '#1072#1088#1090#1080#1082#1091#1083#1072
      ImageIndex = 10
      Spacing = 1
      Left = 387
      Top = 3
      Visible = True
      OnClick = ButtonArticleStructureClick
      SectionName = 'Untitled (0)'
    end
    object SpeedItem4: TSpeedItem
      BtnCaption = #1040#1085#1072#1083#1080#1079
      Caption = #1040#1085#1072#1083#1080#1079
      Hint = #1040#1085#1072#1083#1080#1079'|'
      ImageIndex = 11
      Spacing = 1
      Left = 451
      Top = 3
      Visible = True
      OnClick = SpeedItem4Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem5: TSpeedItem
      Caption = 'SpeedItem5'
      Hint = 'SpeedItem5|'
      Spacing = 1
      Top = 3
      SectionName = 'Untitled (0)'
    end
    object SpeedItem6: TSpeedItem
      Caption = 'SpeedItem6'
      Hint = 'SpeedItem6|'
      Spacing = 1
      Top = 3
      SectionName = 'Untitled (0)'
    end
  end
  inherited plMain: TPanel
    Top = 41
    Width = 1031
    Height = 96
    TabOrder = 3
    ExplicitTop = 41
    ExplicitWidth = 1031
    ExplicitHeight = 96
    inherited lbDateDoc: TLabel
      Left = 6
      Top = 28
      ExplicitLeft = 6
      ExplicitTop = 28
    end
    inherited lbDep: TLabel
      Left = 716
      Top = 7
      Visible = False
      ExplicitLeft = 716
      ExplicitTop = 7
    end
    inherited txtDep: TDBText
      Left = 752
      Top = 7
      Width = 77
      DataField = 'DEPID'
      Visible = False
      ExplicitLeft = 752
      ExplicitTop = 7
      ExplicitWidth = 77
    end
    object SpeedButton1: TSpeedButton [4]
      Left = 1016
      Top = 4
      Width = 133
      Height = 22
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1087#1086#1083#1077' '#1074' '#1088#1072#1073#1086#1090#1077
      Flat = True
      Visible = False
    end
    object Label2: TLabel [5]
      Left = 8
      Top = 50
      Width = 103
      Height = 13
      Caption = #1060#1080#1083#1100#1090#1088' '#1087#1086' '#1072#1088#1090#1080#1082#1091#1083#1091
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Transparent = True
    end
    object lbW: TLabel [6]
      Left = 8
      Top = 77
      Width = 22
      Height = 13
      Caption = #1042#1077#1089':'
    end
    object lbQ: TLabel [7]
      Left = 135
      Top = 77
      Width = 37
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086':'
    end
    object txtQ: TDBText [8]
      Left = 183
      Top = 77
      Width = 77
      Height = 14
      DataField = 'Q'
      DataSource = dmAppl.dsrAList
    end
    object txtW: TDBText [9]
      Left = 44
      Top = 76
      Width = 73
      Height = 15
      DataField = 'W'
      DataSource = dmAppl.dsrAList
    end
    object Label1: TLabel [10]
      Left = 716
      Top = 35
      Width = 294
      Height = 26
      Caption = 
        #1056#1072#1089#1095#1077#1090' '#1087#1088#1086#1076#1072#1078' ('#1079#1072' '#1074#1099#1095#1077#1090#1086#1084' '#1074#1086#1079#1074#1088#1072#1090#1072') '#1089#1086' '#1089#1082#1083#1072#1076#1072' '#1075#1086#1090#1086#1074#1086#1081' '#13#10#1087#1088#1086#1076#1091#1082#1094#1080 +
        #1080' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1089#1103' '#1079#1072' 6 '#1084#1077#1089#1103#1094#1077#1074
    end
    object txtA1BD: TDBText [11]
      Left = 836
      Top = 78
      Width = 39
      Height = 13
      AutoSize = True
      DataField = 'A1BD'
      DataSource = dmAppl.dsrAList
    end
    object txtA1ED: TDBText [12]
      Left = 904
      Top = 78
      Width = 39
      Height = 13
      AutoSize = True
      DataField = 'A1ED'
      DataSource = dmAppl.dsrAList
    end
    object Label4: TLabel [13]
      Left = 716
      Top = 77
      Width = 113
      Height = 13
      Caption = #1056#1072#1089#1095#1077#1090' '#1087#1088#1086#1080#1079#1074#1077#1076#1077#1085' '#1079#1072
    end
    object lbFirstSemis: TLabel [14]
      Left = 343
      Top = 6
      Width = 72
      Height = 26
      Caption = #1053#1072#1095#1072#1083#1100#1085#1099#1081#13#10#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
    end
    object lbLastSemis: TLabel [15]
      Left = 474
      Top = 6
      Width = 72
      Height = 26
      Caption = #1060#1080#1085#1080#1096#1085#1099#1081#13#10#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
    end
    object lbReadyProduct: TLabel [16]
      Left = 583
      Top = 13
      Width = 86
      Height = 13
      Caption = #1043#1086#1090#1086#1074#1086#1077' '#1080#1079#1076#1077#1083#1080#1077
    end
    object Label5: TLabel [17]
      Left = 250
      Top = 41
      Width = 79
      Height = 13
      Caption = #1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1081
    end
    object Label6: TLabel [18]
      Left = 250
      Top = 69
      Width = 25
      Height = 13
      Caption = #1057#1088#1086#1082
    end
    inherited edNoDoc: TDBEditEh
      Left = 120
      Width = 78
      DataField = 'DOCNO'
      ExplicitLeft = 120
      ExplicitWidth = 78
    end
    inherited dtedDateDoc: TDBDateTimeEditEh
      Left = 117
      Top = 25
      DataField = 'DOCDATE'
      ExplicitLeft = 117
      ExplicitTop = 25
    end
    object edArt: TDBEditEh
      Left = 120
      Top = 50
      Width = 91
      Height = 19
      Hint = 
        '_ - '#1086#1076#1080#1085' '#1089#1080#1084#1074#1086#1083#13#10'% - '#1083#1102#1073#1072#1103' '#1087#1086#1089#1083#1077#1076#1086#1074#1072#1090#1077#1083#1100#1085#1086#1089#1090#1100' '#1089#1080#1084#1074#1086#1083#1086#1074' ('#1074' '#1090#1086#1084' '#1095#1080 +
        #1089#1083#1077' '#1080' '#1085#1091#1083#1077#1074#1072#1103')'
      EditButtons = <
        item
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF0001079F000313A9000418AE000419AE000313
            A9000108A000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF0001049D00041CB1000730C0000734C4000735C5000735C5000734
            C3000731C100041FB30001069E00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF000109A100052BC3000735C7000733C2000732C2000732C2000732C2000732
            C2000733C3000735C400062DBE00020CA400FF00FF00FF00FF00FF00FF000104
            9B00052BCA000636D8000431CD000027C400032EC1000732C2000732C2000430
            C1000027BF00042FC1000735C400072EBE0001069E00FF00FF00FF00FF00031A
            BA000537E7000331DD00123DD8006480E0001840CB00002CC100022DC0000F38
            C4006580D9001B43C700052FC1000735C500051FB300FF00FF009E01DA000430
            E4000436F100002AE4005070E900FFFFFF00B7C4F1000D36CA00042DC300A2B2
            E800FFFFFF006984DA000026BE000733C3000731C1000108A0009E06E0000336
            FA000335F8000232EE000A35E8008CA2F200FFFFFF00B4C2F100A9B8ED00FFFF
            FF00A7B7E900133AC400052FC1000732C2000734C4000313AA00A009E5001747
            FE00093AFC000435F8000131F000002BE80091A5F400FFFFFF00FFFFFF00ABBA
            EF00062FC500022DC0000732C2000732C2000736C5000419AE00A10BE6004168
            FE001C49FC000335FB000031F9000531F200A4B5F700FFFFFF00FFFFFF00B9C6
            F2000D36D000002CC6000732C2000732C2000736C5000418AD00A007E2005B7C
            FC00486CFD000133FB00113CFB00A1B4FE00FFFFFF00A4B6F80092A7F500FFFF
            FF00B6C4F2001A41D300042FC8000732C4000734C3000212A9009D01DA004A6A
            F3008FA6FF001F46FB004C6FFC00FFFFFF00A7B8FE000733F600002AED008CA2
            F600FFFFFF00627FE7000028D0000734CC000730C30000069F00FF00FF001A2F
            CB0099AFFF008BA2FE00214DFB004D71FC000E3DFB000030FB000031F7000636
            F1004C6EF100103CE3000432DB000636D700041CB500FF00FF00FF00FF000004
            A000415EEC00B8C7FF009CAFFD003A5CFC000A3AFB000335FB000335FB000133
            F900052FF2000635EB000537E900052CCD0000049C00FF00FF00FF00FF00FF00
            FF000309A5004260EC00A9BBFF00BDCAFF008EA5FE006483FD005073FC004A6E
            FD003961FD001444F900042CD7000109A200FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF000004A0001E32CD005876F600859EFE008BA3FF007994FE005376
            FC00234AF000051EC50001049C00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF006102C400670ED2006B15DA006913D900630B
            D1006203C400FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          ShortCut = 16397
          Style = ebsGlyphEh
          Width = 18
          OnClick = edArtEditButtons0Click
        end>
      Flat = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      OnKeyDown = edArtKeyDown
    end
    object DTEStartEh: TDBDateTimeEditEh
      Left = 335
      Top = 63
      Width = 97
      Height = 19
      DataField = 'START$SEMIS$DATE'
      DataSource = dmAppl.dsrApplResp
      EditButton.Style = ebsGlyphEh
      EditButtons = <>
      Flat = True
      Kind = dtkDateEh
      TabOrder = 3
      Visible = True
      OnChange = DTEStartEhChange
    end
    object DTEFinishEh: TDBDateTimeEditEh
      Left = 457
      Top = 63
      Width = 97
      Height = 19
      DataField = 'FINISH$SEMIS$DATE'
      DataSource = dmAppl.dsrApplResp
      EditButton.Style = ebsGlyphEh
      EditButtons = <>
      Flat = True
      Kind = dtkDateEh
      TabOrder = 4
      Visible = True
      OnChange = DTEFinishEhChange
    end
    object DTEReadyEh: TDBDateTimeEditEh
      Left = 580
      Top = 63
      Width = 97
      Height = 19
      DataField = 'READY$SEMIS$DATE'
      DataSource = dmAppl.dsrApplResp
      EditButton.Style = ebsGlyphEh
      EditButtons = <>
      Flat = True
      Kind = dtkDateEh
      TabOrder = 6
      Visible = True
      OnChange = DTEReadyEhChange
    end
    object CBStartEh: TDBLookupComboboxEh
      Left = 335
      Top = 36
      Width = 97
      Height = 19
      Color = clHighlightText
      DataField = 'START$SEMIS$RESP$PERSON'
      DataSource = dmAppl.dsrApplResp
      DropDownBox.Align = daCenter
      DropDownBox.Rows = 15
      DropDownBox.SpecRow.CellsText = '<'#1085#1077' '#1079#1072#1076#1072#1085'>'
      DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
      DropDownBox.SpecRow.Font.Color = cl3DDkShadow
      DropDownBox.SpecRow.Font.Height = -11
      DropDownBox.SpecRow.Font.Name = 'Tahoma'
      DropDownBox.SpecRow.Font.Style = []
      DropDownBox.SpecRow.Value = -1
      DropDownBox.SpecRow.Visible = True
      EditButtons = <>
      Flat = True
      KeyField = 'DEPID'
      ListField = 'NAME'
      ListSource = dsRespPersons
      TabOrder = 5
      Visible = True
      OnKeyValueChanged = CBStartEhKeyValueChanged
    end
    object CBFinishEh: TDBLookupComboboxEh
      Left = 457
      Top = 36
      Width = 97
      Height = 19
      DataField = 'FINISH$SEMIS$RESP$PERSON'
      DataSource = dmAppl.dsrApplResp
      DropDownBox.Align = daCenter
      DropDownBox.Rows = 15
      DropDownBox.SpecRow.CellsText = '<'#1085#1077' '#1079#1072#1076#1072#1085'>'
      DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
      DropDownBox.SpecRow.Font.Color = cl3DDkShadow
      DropDownBox.SpecRow.Font.Height = -11
      DropDownBox.SpecRow.Font.Name = 'Tahoma'
      DropDownBox.SpecRow.Font.Style = []
      DropDownBox.SpecRow.Value = -1
      DropDownBox.SpecRow.Visible = True
      EditButtons = <>
      Flat = True
      KeyField = 'DEPID'
      ListField = 'NAME'
      ListSource = dsRespPersons
      TabOrder = 7
      Visible = True
      OnKeyValueChanged = CBFinishEhKeyValueChanged
    end
    object CBReadyEh: TDBLookupComboboxEh
      Left = 580
      Top = 36
      Width = 97
      Height = 19
      DataField = 'READY$PROD$RESP$PERSON'
      DataSource = dmAppl.dsrApplResp
      DropDownBox.Align = daCenter
      DropDownBox.Rows = 15
      DropDownBox.SpecRow.CellsText = '<'#1085#1077' '#1079#1072#1076#1072#1085'>'
      DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
      DropDownBox.SpecRow.Font.Color = cl3DDkShadow
      DropDownBox.SpecRow.Font.Height = -11
      DropDownBox.SpecRow.Font.Name = 'Tahoma'
      DropDownBox.SpecRow.Font.Style = []
      DropDownBox.SpecRow.Value = -1
      DropDownBox.SpecRow.Visible = True
      EditButtons = <>
      Flat = True
      KeyField = 'DEPID'
      ListField = 'NAME'
      ListSource = dsRespPersons
      TabOrder = 8
      Visible = True
      OnKeyValueChanged = CBReadyEhKeyValueChanged
    end
  end
  inherited gridItem: TDBGridEh
    Top = 153
    Width = 1031
    Height = 292
    Color = clWhite
    FooterRowCount = 1
    OptionsEh = [dghFixed3D, dghFooter3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    PopupMenu = ppDoc
    SumList.Active = True
    TabOrder = 0
    UseMultiTitle = True
    OnGetCellParams = gridItemGetCellParams
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RecNo'
        Footer.FieldName = 'ID'
        Footer.ValueType = fvtCount
        Footers = <>
        ReadOnly = True
        Width = 29
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ID'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
        Width = 49
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ARTID'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ART2ID'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SZ'
        Footers = <>
        Title.EndEllipsis = True
        Visible = False
        Width = 43
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ART'
        Footers = <>
        Title.EndEllipsis = True
        Width = 46
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SZNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 48
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'MW'
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Width = 49
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WQ'
        Footer.FieldName = 'WQ'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Width = 39
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'NQ_APPL'
        Footer.FieldName = 'NQ_APPL'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1041#1088#1072#1082'/'#1053#1077#1082#1086#1084#1087#1083#1077#1082#1090
        Title.EndEllipsis = True
        Width = 59
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'AQ'
        Footer.FieldName = 'AQ'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Width = 48
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footer.FieldName = 'Q'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.EndEllipsis = True
        Width = 53
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FQ'
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Visible = False
      end
      item
        Checkboxes = True
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'R'
        Footer.FieldName = 'R'
        Footer.ValueType = fvtSum
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.EndEllipsis = True
        Width = 27
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REQUESTQ'
        Footer.FieldName = 'REQUESTQ'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.EndEllipsis = True
        Width = 62
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REQUESTQ2'
        Footer.FieldName = 'REQUESTQ2'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1058#1088#1077#1073'. '#1087#1086' '#1079#1072#1103#1074#1082#1072#1084
        Width = 58
      end
      item
        Checkboxes = True
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'MODEL'
        Footer.FieldName = 'MODEL'
        Footer.ValueType = fvtSum
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Title.EndEllipsis = True
        Width = 47
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INQ'
        Footer.FieldName = 'INQ'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1093#1086#1076#1103#1097#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Visible = False
        Width = 40
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'INW'
        Footer.FieldName = 'INW'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1093#1086#1076#1103#1097#1080#1077'|'#1074#1077#1089
        Title.EndEllipsis = True
        Visible = False
        Width = 36
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEBTORQ'
        Footer.FieldName = 'DEBTORQ'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1082#1072'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Visible = False
        Width = 43
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEBTORW'
        Footer.FieldName = 'DEBTORW'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1086#1089#1090#1072#1074#1082#1072'|'#1074#1077#1089
        Title.EndEllipsis = True
        Visible = False
        Width = 41
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SALEQ'
        Footer.FieldName = 'SALEQ'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1080'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Visible = False
        Width = 46
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SALEW'
        Footer.FieldName = 'SALEW'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1088#1086#1076#1072#1078#1080'|'#1074#1077#1089
        Title.EndEllipsis = True
        Visible = False
        Width = 43
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RETQ'
        Footer.FieldName = 'RETQ'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Visible = False
        Width = 43
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RETW'
        Footer.FieldName = 'RETW'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042#1086#1079#1074#1088#1072#1090'|'#1074#1077#1089
        Title.EndEllipsis = True
        Visible = False
        Width = 45
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OUTQ'
        Footer.FieldName = 'OUTQ'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Visible = False
        Width = 41
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OUTW'
        Footer.FieldName = 'OUTQ'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077'|'#1074#1077#1089
        Title.EndEllipsis = True
        Visible = False
        Width = 42
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'STOREQ'
        Footer.FieldName = 'STOREQ'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042' '#1084#1072#1075#1072#1079#1080#1085#1077'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Visible = False
        Width = 44
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'STOREW'
        Footer.FieldName = 'STOREW'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1042' '#1084#1072#1075#1072#1079#1080#1085#1077'|'#1074#1077#1089
        Title.EndEllipsis = True
        Visible = False
        Width = 36
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'PSTOREQ'
        Footer.FieldName = 'PSTOREQ'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1053#1072' '#1089#1082#1083#1072#1076#1077'|'#1050#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 51
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'PSTOREW'
        Footer.FieldName = 'PSTOREW'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1053#1072' '#1089#1082#1083#1072#1076#1077'|'#1042#1077#1089
        Title.EndEllipsis = True
        Width = 40
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'COMMENT'
        Footers = <>
        Title.EndEllipsis = True
        Width = 43
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'KOEF'
        Footers = <>
        ReadOnly = True
        Title.Caption = #1050#1086#1101#1092'.'
        Width = 57
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SELL0'
        Footer.FieldName = 'SELL0'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1086#1076#1072#1085#1086' '#1089#1086' '#1089#1082#1083#1072#1076#1072'|0'
        Title.EndEllipsis = True
        Width = 48
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SELL1'
        Footer.FieldName = 'SELL1'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Title.Caption = #1055#1088#1086#1076#1072#1085#1086' '#1089#1086' '#1089#1082#1083#1072#1076#1072'|'#1056#1077#1072#1083#1080#1079#1072#1094#1080#1103
        Title.EndEllipsis = True
        Width = 51
      end>
  end
  object plWhA2: TPanel [6]
    Left = 0
    Top = 565
    Width = 1031
    Height = 108
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 4
    Visible = False
    object cxGrid1: TcxGrid
      Left = 0
      Top = 0
      Width = 1031
      Height = 108
      Align = alClient
      TabOrder = 0
      LookAndFeel.Kind = lfStandard
      object cxGrid1DBTableView1: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmAppl.dsrWh2d
        DataController.DetailKeyFieldNames = 'MODEL$TITLE'
        DataController.KeyFieldNames = 'MODEL$TITLE'
        DataController.MasterKeyFieldNames = 'MODEL$TITLE'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.ColumnSorting = False
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object cxGrid1DBTableView1SUBMODELTITLE: TcxGridDBColumn
          Caption = #1040#1088#1090'.2'
          DataBinding.FieldName = 'SUB$MODEL$TITLE'
          Visible = False
          Width = 87
        end
        object cxGrid1DBTableView1SIZETITLE: TcxGridDBColumn
          Caption = #1056#1072#1079#1084#1077#1088
          DataBinding.FieldName = 'SIZE$TITLE'
          Width = 60
        end
        object cxGrid1DBTableView1OPERATIONTITLE: TcxGridDBColumn
          Caption = #1054#1087#1077#1088#1072#1094#1080#1103
          DataBinding.FieldName = 'OPERATION$TITLE'
          Width = 163
        end
        object cxGrid1DBTableView1PLACEOFSTORAGETITLE: TcxGridDBColumn
          Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
          DataBinding.FieldName = 'PLACE$OF$STORAGE$TITLE'
          Width = 228
        end
        object cxGrid1DBTableView1QUANTITY: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086
          DataBinding.FieldName = 'QUANTITY'
        end
      end
      object cxGrid1DBTableView2: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = dmAppl.dsrWh2m
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView2MODELTITLE: TcxGridDBColumn
          Caption = #1040#1088#1090#1080#1082#1091#1083
          DataBinding.FieldName = 'MODEL$TITLE'
        end
        object cxGrid1DBTableView2QUANTITY: TcxGridDBColumn
          Caption = #1050#1086#1083'-'#1074#1086
          DataBinding.FieldName = 'QUANTITY'
        end
        object cxGrid1DBTableView2TOTALQUANTITY: TcxGridDBColumn
          Caption = #1047#1072#1103#1074#1080#1090#1100
          DataBinding.FieldName = 'TOTAL$QUANTITY'
        end
        object cxGrid1DBTableView2Difference: TcxGridDBColumn
          Caption = #1056#1072#1079#1085#1080#1094#1072
          DataBinding.FieldName = 'Difference'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView2
        object cxGrid1Level2: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object plAssort: TPanel [7]
    Left = 0
    Top = 449
    Width = 1031
    Height = 112
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 5
    object Splitter2: TSplitter
      Left = 449
      Top = 0
      Width = 4
      Height = 112
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 449
      Height = 112
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object gridPEl: TDBGridEh
        Left = 0
        Top = 31
        Width = 449
        Height = 81
        Align = alClient
        AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
        Color = clWhite
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dmAppl.dsrApplPEl
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART'
            Footers = <>
            Width = 45
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART2'
            Footers = <>
            Width = 40
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'SZ'
            Footers = <>
            Width = 46
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'Q'
            Footers = <>
            Width = 39
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'OPERATION'
            Footers = <>
            Width = 74
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'U'
            Footers = <>
            Width = 54
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'COMMENTID'
            Footers = <>
            Width = 94
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'COMMENT'
            Footers = <>
            Width = 100
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 449
        Height = 31
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label13: TLabel
          Left = 4
          Top = 15
          Width = 37
          Height = 13
          Caption = #1053#1086#1084#1077#1088':'
        end
        object Label14: TLabel
          Left = 120
          Top = 15
          Width = 29
          Height = 13
          Caption = #1044#1072#1090#1072':'
        end
        object Label16: TLabel
          Left = 6
          Top = 2
          Width = 122
          Height = 13
          Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1085#1099#1077' '#1072#1088#1090#1080#1082#1091#1083#1099
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object txtPDocNo: TDBText
          Left = 44
          Top = 16
          Width = 73
          Height = 14
          DataField = 'DOCNO'
          DataSource = dmAppl.dsrApplPInv
        end
        object txtPDocDate: TDBText
          Left = 151
          Top = 15
          Width = 65
          Height = 14
          DataField = 'DOCDATE'
          DataSource = dmAppl.dsrApplPInv
        end
      end
    end
    object Panel4: TPanel
      Left = 453
      Top = 0
      Width = 578
      Height = 112
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 578
        Height = 31
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label15: TLabel
          Left = 4
          Top = 1
          Width = 89
          Height = 13
          Caption = #1057#1085#1103#1090#1099#1077' '#1072#1088#1090#1080#1082#1091#1083#1099
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 578
          Height = 31
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label17: TLabel
            Left = 4
            Top = 16
            Width = 34
            Height = 13
            Caption = #1053#1086#1084#1077#1088
          end
          object Label18: TLabel
            Left = 120
            Top = 15
            Width = 26
            Height = 13
            Caption = #1044#1072#1090#1072
          end
          object Label19: TLabel
            Left = 6
            Top = 2
            Width = 89
            Height = 13
            Caption = #1057#1085#1103#1090#1099#1077' '#1072#1088#1090#1080#1082#1091#1083#1099
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object txtRejDocNo: TDBText
            Left = 44
            Top = 16
            Width = 73
            Height = 14
            DataField = 'DOCNO'
            DataSource = dmAppl.dsrApplPInv
          end
          object txtRejDocDate: TDBText
            Left = 151
            Top = 15
            Width = 65
            Height = 14
            DataField = 'DOCDATE'
            DataSource = dmAppl.dsrRejInv
          end
        end
      end
      object gridRejInv: TDBGridEh
        Left = 0
        Top = 31
        Width = 578
        Height = 81
        Align = alClient
        AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
        Color = clWhite
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dmAppl.dsrApplRejEl
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART'
            Footers = <>
            Width = 63
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'ART2'
            Footers = <>
            Width = 36
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'SZ'
            Footers = <>
            Width = 45
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'Q'
            Footers = <>
            Width = 47
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'OPERATION'
            Footers = <>
            Width = 65
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'U'
            Footers = <>
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'COMMENTID'
            Footers = <>
          end
          item
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'COMMENT'
            Footers = <>
            Width = 100
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object Panel1: TPanel [8]
    Left = 0
    Top = 137
    Width = 1031
    Height = 16
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 6
    Visible = False
    object Label3: TLabel
      Left = 6
      Top = 1
      Width = 83
      Height = 13
      Caption = #1055#1077#1088#1080#1086#1076' '#1072#1085#1072#1083#1080#1079#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object txtPeriod: TLabel
      Left = 94
      Top = 1
      Width = 51
      Height = 13
      Caption = 'txtPeriod'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Visible = False
    end
  end
  inherited ilButtons: TImageList
    Left = 134
    Top = 196
    Bitmap = {
      494C01010C000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000080808000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C000C0C0
      C000C0C0C000C0C0C00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002222
      2200222222002222220022222200222222002222220022222200222222002222
      2200222222002222220022222200222222000000000080808000C0C0C000C0C0
      C00080808000C0C0C000C0C0C000C0C0C000C0C0C000C0C0C00080808000C0C0
      C00080808000C0C0C000C0C0C000C0C0C0000000000000000000000000000000
      00000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C0C0C0000000000000000000DC8B0000DC8B0000DC8B0000DC8B
      0000DC8B0000DC8B0000DC8B0000000000000000000000008D0000008D000000
      8D0000008D0000008D0000008D0000008D000000000000000000444444000033
      6600003366000033660022222200003366000033660000336600003366002222
      220022222200003366000033660022222200000000008080800000FF000000FF
      00008080800000FF000000FF000000FF000000FF000000FF00008080800000FF
      00008080800000FF000000FF0000C0C0C0000000000000000000000000000000
      00000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C0C0C0000000000000000000DC8B0000FFEFB700FFEFB700FFEF
      B700FFEFB700FFEFB700DC8B0000000000000000000000008D002E78F300378C
      F700378DF8002F7AF4002F7AF40000008D000000000000336600444444000033
      6600777777000033660022222200003366000033660000336600003366002222
      2200222222009999990099999900222222000000000080808000FFFFFF00FFFF
      FF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080808000FFFF
      FF0080808000FFFFFF00FFFFFF00C0C0C0000000000000000000000000000000
      00000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C0C0C0000000000000000000DC8B0000FFDC9100FFDC9100FFDC
      9100FFDC9100FFDC9100DC8B0000000000000000000000008D002563EF002B70
      F2002B71F2002665EF002665EF0000008D000033660000336600444444000033
      6600777777000033660044444400003366000033660000336600003366002222
      2200222222009999990099999900222222000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000000000000000000000
      00000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C0C0C0000000000000000000DC8B0000FFC86C00FFC86C00FFC8
      6C00FFC86C00FFC86C00DC8B0000000000000000000000008D001B49EA001F53
      EC001F53EC001B4BEA001B4BEA0000008D000033660000336600444444000033
      6600777777000033660022222200003366000033660000336600003366002222
      220022222200003366000033660022222200000000008080800000FF000000FF
      00008080800000FF000000FF000000FF000000FF000000FF00008080800000FF
      00008080800000FF000000FF0000C0C0C0000000000000000000000000000000
      00000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C0C0C0000000000000000000DC8B0000DC8B0000DC8B0000DC8B
      0000DC8B0000DC8B0000DC8B0000000000000000000000008D0000008D000000
      8D0000008D0000008D0000008D0000008D000033660000336600444444000033
      6600555555000033660022222200003366000033660000336600003366002222
      2200222222009999990099999900222222000000000080808000FFFFFF00FFFF
      FF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080808000FFFF
      FF0080808000FFFFFF00FFFFFF00C0C0C0000000000000000000000000000000
      00000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C0C0C00000000000000000000000000000000000000000006060
      6000000000000000000000000000000000000000000000000000000000000000
      0000606060000000000000000000FA0AFB000033660000336600444444000033
      6600777777000033660044444400003366000033660000336600003366002222
      2200222222009999990099999900222222000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000000000FFFFFF000000
      00000000000080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C0C0C00000000000000000000000000000000000000000006060
      6000606060006060600060606000606060006060600060606000606060006060
      6000606060000000000000000000000000000033660000336600444444000033
      6600003366000033660022222200003366000033660000336600003366002222
      220022222200003366000033660022222200000000008080800000FF000000FF
      00008080800000FF000000FF000000FF000000FF000000FF00008080800000FF
      00008080800000FF000000FF0000C0C0C000000000000000000080808000FFFF
      FF008080800080808000FFFFFF00FFFFFF0080808000FFFFFF00FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000606060006060600000000000000000000000
      0000000000000000000000000000000000000033660000336600444444000033
      6600003366000033660022222200003366000033660000336600003366002222
      2200222222000033660000336600222222000000000080808000FFFFFF00FFFF
      FF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080808000FFFF
      FF0080808000FFFFFF00FFFFFF00C0C0C0000000000000000000000000008080
      800000FFFF0080808000FFFFFF0080808000FFFFFF00FFFFFF00FFFFFF00C0C0
      C000FFFFFF008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000606060006060600000000000000000000000
      0000000000000000000000000000000000000033660000336600444444000033
      6600003366000033660022222200003366000033660000336600003366002222
      2200222222000033660000336600003366000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000000000FFFFFF00FFFF
      FF0080808000FFFFFF0000FFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C0C0
      C000808080000000000000000000000000000000000000000000000000000000
      0000005BCA00005BCA00005BCA00005BCA00005BCA00005BCA00005BCA00005B
      CA000000000000000000000000000000000000336600003366005555550000FF
      FF0099669900FF990000C0C0C000555555003366FF003366FF00555555009966
      990000BB0000555555000033660000000000000000008080800000FF000000FF
      00008080800000FF000000FF000000FF000000FF000000FF00008080800000FF
      00008080800000FF000000FF0000C0C0C0000000000000000000000000008080
      80008080800000FFFF00FFFFFF00FFFFFF008080800080808000808080008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000005BCA005ED6FF005ED6FF005ED6FF005ED6FF005ED6FF005ED6FF00005B
      CA0000000000000000000000000000000000003366003366FF0000BB00005555
      5500FF990000555555005555550000FFFF0000BB000099003300FF9900003366
      FF000000DD000033660000000000000000000000000080808000FFFFFF00FFFF
      FF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080808000FFFF
      FF0080808000FFFFFF00FFFFFF00C0C0C0000000000000000000000000008080
      800000FFFF008080800000FFFF0080808000FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000005BCA004AC9FF004AC9FF004AC9FF004AC9FF004AC9FF004AC9FF00005B
      CA00000000000000000000000000000000000033660000336600003366000033
      6600003366000033660000336600003366000033660000336600003366000033
      6600003366000000000000000000000000000000000080808000FFFFFF00FFFF
      FF0080808000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0080808000FFFF
      FF0080808000FFFFFF00FFFFFF00C0C0C000000000000000000080808000FFFF
      FF000000000080808000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000005BCA0037BCFF0037BCFF0037BCFF0037BCFF0037BCFF0037BCFF00005B
      CA00000000000000000000000000000000000000000000336600003366000033
      6600003366000033660000336600003366000033660000336600003366000033
      6600003366000033660000000000000000000000000080808000808080008080
      8000808080008080800080808000808080008080800080808000808080008080
      8000808080008080800080808000808080000000000000000000FFFFFF000000
      00000000000080808000FFFFFF00000000000000000080808000FFFFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000005BCA00005BCA00005BCA00005BCA00005BCA00005BCA00005BCA00005B
      CA00000000000000000000000000000000000000000000000000003366000033
      6600003366000033660000336600003366000033660000336600003366000033
      6600003366000033660000336600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000080808000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000033
      6600777777007777770000336600003366000033660000336600003366007777
      7700777777000033660000336600003366000000000000000000000000000000
      00000000000000000000B5B5B5000000000000000000000000009C9C9C009C9C
      9C00A5A5A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000069D
      CC0049B7E00060C1E80054BCE3003DB2DC004DB9E10000000000000000000000
      0000000000000000000000000000000000007B7B7B004A637B00BD9494000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5B5B500B5B5B500DEDEDE00ADADAD00525252006B6B6B00ADADAD00D6D6
      D600E7DEDE009C9C9C000000000000000000000000000000000000000000069D
      CC0049B7E00060C1E80054BCE3003DB2DC004DB9E10000000000000000000000
      000000000000000000000000000000000000000000000000000008A1CE000EA5
      D1007FD2F100A0DFFE0097DAFB0093D9FA007CCDEF0073CAEE0048B7E0000000
      0000000000000000000000000000000000006B9CC600188CEF004A7BA500CE94
      9400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5B5B500B5B5
      B500FFFFFF00FFFFFF00E7DEDE00B5B5B5005A5A630031313100313131004A4A
      4A00848484009C9C9C00A5A5A50000000000000000000000000008A1CE000EA5
      D1007FD2F100A0DFFE0097DAFB0093D9FA007CCDEF0073CAEE0048B7E0000000
      000000000000000000000000000000000000000000000000000013A8D4001DAF
      D80083D8EE00A0E3FB0093DAF90082D0F20059BBE0008FD7F7009ADCFB0076CB
      F000000000000000000000000000000000004AB5FF0052B5FF00218CEF004A7B
      A500C69494000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A5A5A500B5B5B500F7F7F700FFFF
      FF00F7F7F700D6D6D600B5B5B500A5A5A500ADADAD009C9C9C007B7B7B005252
      520031313900313131008C8C8C0000000000000000000000000013A8D4001DAF
      D80083D8EE00A0E3FB0093DAF90082D0F20059BBE0008FD7F7009ADCFB0076CB
      F00000000000000000000000000000000000000000000000000020AFD8002EB7
      DE0094DFEF00B1EEFA00A2E7FD0064B2D100369BC0006FC7EB0095DAFB009ADC
      FB0034B1DA000000000000000000000000000000000052B5FF0052B5FF001884
      E7004A7BA500CE94940000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B5ADAD00EFEFEF00EFEFEF00CECE
      CE00BDB5B500C6C6C600D6CECE00B5B5B500ADADAD00ADA5A500ADADAD00ADAD
      AD00A5A5A500848484009C9C9C0000000000000000000000000020AFD8002EB7
      DE0094DFEF00B1EEFA00A2E7FD0064B2D100369BC0006FC7EB0095DAFB009ADC
      FB0034B1DA0000000000000000000000000000000000000000002DB5DD003FBE
      E400B1E5F100CFF8FD00B6F4FF0083C9DE00287FA10069BDE00094DBFC0095DA
      FA0033B0D900000000000000000000000000000000000000000052B5FF004AB5
      FF00188CE7004A7BA500BD949400000000000000000000000000000000000000
      000000000000000000000000000000000000ADA5AD00BDBDBD00B5B5B500C6C6
      C600D6D6D600DEDEDE00F7F7F700F7F7F700E7E7E700D6D6D600BDBDBD00ADAD
      AD00ADA5A500ADADAD00A5A5A5000000000000000000000000002DB5DD003FBE
      E400B1E5F100CFF8FD00B6F4FF0083C9DE00287FA10069BDE00094DBFC0095DA
      FA0033B0D900000000000000000000000000000000000000000039BBE30048C1
      E800C5E8F400ECFAFD00CAF5FB00B3ECF50063A9BF008BCFE80099DFFE0094D8
      FA0033B0D90000000000000000000000000000000000000000000000000052BD
      FF004AB5FF002184DE005A6B730000000000AD7B7300C6A59C00D6B5A500D6A5
      9C0000000000000000000000000000000000A5A5A500C6BDC600D6D6D600DEDE
      DE00D6D6D600EFEFEF00DEDEDE00B5BDB500CECECE00D6DEDE00DEDEDE00DEDE
      DE00D6D6D600C6C6C600ADADAD0000000000000000000000000039BBE30048C1
      E800C5E8F400ECFAFD00CAF5FB00B3ECF50063A9BF008BCFE80099DFFE0094D8
      FA0033B0D90000000000000000000000000000000000000000002EB3DB0044C1
      E2005EC0DB006CC8E00062C8E20060C7E2006ECFE70086D8EE009DE2F900A2E1
      FE0034B1DA000000000000000000000000000000000000000000000000000000
      000052BDFF00B5D6EF00A5948C00B59C8C00F7E7CE00FFFFDE00FFFFDE00FFFF
      DE00EFDEC600CEADA500000000000000000000000000B5ADAD00DEDEDE00DEDE
      DE00E7E7E700D6D6D600C6C6C600BDDEBD00CED6CE00D6BDB500BDB5B500BDBD
      BD00C6C6C600D6CECE00BDBDBD000000000000000000000000002EB3DB0044C1
      E2005EC0DB006CC8E00062C8E20060C7E2006ECFE70086D8EE009DE2F900A2E1
      FE0034B1DA00000000000000000000000000000000000000000029B3D80075DC
      E700328BA5003A9AB30069E1F00044CCE70021B5D90014AAD3002CAFD60079D0
      ED0037B3DA000000000000000000000000000000000000000000000000000000
      000000000000CEB5B500DEBDA500FFF7C600FFFFD600FFFFDE00FFFFDE00FFFF
      DE00FFFFEF00F7F7EF00B58C8C00000000000000000000000000B5ADAD00CECE
      CE00C6BDBD00C6C6C600EFEFEF00FFF7F700F7F7F700F7EFE700E7E7E700DEDE
      DE00C6C6C600ADA5A500000000000000000000000000000000002BB1D80058CD
      E40040B8D70046BEDB0045C2DF0037B7D9004AC2E2006BCFEA009AE1F80086D4
      F40025AAD40000000000000000000000000000000000000000000000000030B2
      D6004FBBD10074D7E30098FDFF0080F1FA0050CFE60012A1C8000397C30013A4
      CF0018A5D0000000000000000000000000000000000000000000000000000000
      000000000000C6948C00F7E7B500FFDEAD00FFF7D600FFFFDE00FFFFE700FFFF
      F700FFFFFF00FFFFFF00DED6BD0000000000000000000000000000000000B5AD
      AD00E7E7E700D6D6D600B5B5B500CECECE00DEDEE700DEE7E700DEDEDE00D6D6
      D600C6BDBD00000000000000000000000000000000000000000028B1D7005CCC
      DF000C9BC4003CB6D1008AF5FB0055D7EC0024B6DA00049DCA0025ABD20052C0
      E0001FA7D2000000000000000000000000000000000000000000000000000000
      0000000000000000000054CFE7004ACAE40045C1DD00129AC3000F98C20011A5
      D00011A2CF000000000000000000000000000000000000000000000000000000
      000000000000DEBDAD00FFE7AD00F7CE9400FFF7CE00FFFFDE00FFFFE700FFFF
      FF00FFFFFF00FFFFEF00F7EFD600C69C94000000000000000000000000000000
      0000FFEFEF00FFE7DE00EFD6CE00EFD6CE00EFDED600E7DEDE00DEDEDE00ADAD
      AD00000000000000000000000000000000000000000000000000000000000C94
      C1000091BF002FAECF0087F0F8007AECF80047C2DC000B96C000119DC4001CA9
      D000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000109CC5000894C0000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E7C6AD00FFE7AD00EFBD8400FFE7B500FFFFDE00FFFFDE00FFFF
      EF00FFFFEF00FFFFE700FFFFD600C6AD9C000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000B99C600199BC300000000003ABFE00020A7CD000A95C0000D96C2000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000078CB8000587B300000000000000000000000000089AC6000B91BC000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DEC6AD00FFEFB500EFBD8400F7CE9C00FFEFC600FFFFDE00FFFF
      DE00FFFFDE00FFFFDE00F7F7D600CEA59C000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004FC3E6002A9DC6000000000000000000000000000994BF000C8AB5000000
      0000000000000000000000000000000000000000000000000000000000000000
      00002EB0DA001E9FCA00000000000000000000000000099CC8000B92BD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CEA59C00FFF7C600FFEFC600F7D6A500F7D69C00FFE7BD00FFF7
      CE00FFFFD600FFFFDE00E7DEBD00000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00003BA6C60077DEEE003DAAC80000000000148AB6000C9DC800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004CB5D50068D0EA000000000000000000000000000999C5000A92BE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DEC6AD00FFFFFF00FFF7EF00F7CE9C00F7BD8C00F7CE
      9C00FFE7B500FFF7CE00BD9C8C0000000000000000000000000000000000DEB5
      B500FFE7DE00FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004AB5CD007BE0EC006BD1E6004ABCE0001898C300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005BC4D90078DFEB004FB9D4003EAED2001DA6D100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D6BDBD00F7F7DE00FFF7C600FFE7B500FFEF
      B500F7DEB500D6AD9C000000000000000000000000000000000000000000DEB5
      B500DEB5B500DEB5B500DEB5B500F7BDB500F7BDB50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000055BFD40055C0D6004FBAD80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000CEAD9400CEAD9C00DEBDAD00DEBD
      AD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009A6666006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5848400B584
      8400B5848400B5848400B5848400B5848400B5848400B5848400B5848400B584
      8400B5848400B5848400B5848400000000000000000000000000000000000000
      00009A6666009A666600B9666600BB6868006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000008780E0008780E0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5848400FFEF
      D600D6DEAD00DEDEAD00F7DEB500EFD6A500EFD69C00F7CE9C00F7CE9400F7CE
      9C00F7CE9C00F7D69C00B58484000000000000000000000000009A6666009A66
      6600C66A6B00D06A6B00D2686900C3686900693334009A6666009A6666009A66
      66009A6666009A6666009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000DA31B0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5848400FFEF
      DE00ADCE940042AD39008CBD6B0031A5290031A529007BB55200D6C68C00EFCE
      9400EFCE9400F7D69C00B58484000000000000000000000000009A666600DE73
      7400D7707100D56F7000D56D6E00C76A6D0069333400FEA2A300FCAFB000FABC
      BD00F9C5C600F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000EAA1D0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5847300FFF7
      E700CEDEAD0021A51800009C0000009C0000009C0000009C000042AD2900E7CE
      9400EFCE9400F7D69C00B58484000000000000000000000000009A666600E077
      7800DB757600DA747500DA727300CC6E71006933340039C5650025CF630029CC
      630019CB5B00F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A7000EA81C0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5847300FFF7
      EF00CEDEB50021A51800009C000029A52100BDCE8C008CBD6B00089C080094BD
      6300EFCE9C00F7D69C00B58484000000000000000000000000009A666600E57D
      7E00E07A7B00DF797A00DF777800D07275006933340042C4680030CD670033CB
      670024CB6000F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70010AA1F0008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BD8C8400FFFF
      F700CEDEBD0010A51000009C0000089C08009CC67B00F7DEBD00BDC68C0084BD
      6B00F7D6A500F7D69C00B58484000000000000000000000000009A666600EA82
      8300E57F8000E37D7E00E6808100D3747600693334003DC2640029CB63002FCA
      640020CA5E00F9C5C6009A66660000000000000000000000000008780E000878
      0E0008780E0008780E0008780E0076F9A70019B02C0008780E0008780E000878
      0E0008780E0008780E0000000000000000000000000000000000000000000104
      A2000104A2000104A2000104A2000104A2000104A2000104A2000104A2000104
      A2000104A2000000000000000000000000000000000000000000BD8C8400FFFF
      FF00EFEFDE00B5DEA500ADD69C00ADCE9400ADCE9400F7DEC600F7DEBD00B5D6
      9400F7D6AD00F7D6A500B58484000000000000000000000000009A666600F087
      8800E9818200EC969700FBDDDE00D8888A0069333400B8E1AC006BDC89005DD5
      800046D47300F9C5C6009A666600000000000000000008780E0076F9A70055E3
      830049DA720042D3680037C856002AB9430022B337001CB2300016AF27000FA8
      1D000EA91B000DA21B0008780E000000000000000000000000000104A2005983
      FF000026FF000030FF000030FB00002FF200002FE900002EE1000030D8000031
      D0000034CB000104A20000000000000000000000000000000000CE9C8400FFFF
      FF00BDE7B500FFF7EF00F7EFDE00B5D69C00ADCE9400ADCE8C00B5CE8C00EFDE
      B500F7DEB500F7DEAD00B58484000000000000000000000000009A666600F58C
      8D00EE868700F0999A00FDDCDD00DA888A0069333400FFF5D800FFFFE000FFFF
      DE00ECFDD400F9C5C6009A666600000000000000000008780E0076F9A70076F9
      A70076F9A70076F9A70076F9A70076F9A7002CBB480076F9A70076F9A70076F9
      A70076F9A70076F9A70008780E000000000000000000000000000104A200ABC2
      FF006480FF006688FF006688FF006687FA006787F5006787F0005779E9004D70
      E4004D74E2000104A20000000000000000000000000000000000CE9C8400FFFF
      FF008CD68C00C6E7BD00FFF7EF009CCE8C00089C0800009C000010A51000F7DE
      C600F7DEBD00FFDEB500B58484000000000000000000000000009A666600FA91
      9200F48E8F00F28B8C00F48C8D00DC7F800069333400FDF3D400FFFFDF00FFFF
      DD00FFFFE000F9C5C6009A66660000000000000000000000000008780E000878
      0E0008780E0008780E0008780E0076F9A7003CCB5D0008780E0008780E000878
      0E0008780E0008780E0000000000000000000000000000000000000000000104
      A2000104A2000104A2000104A2000104A2000104A2000104A2000104A2000104
      A2000104A2000000000000000000000000000000000000000000DEAD8400FFFF
      FF009CD69C00089C080094D68C00C6DEB50031AD2900009C000021A51800F7E7
      CE00F7E7C600F7DEB500B58484000000000000000000000000009A666600FE97
      9800F9939400F8929300F9909200E085850069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70049D9720008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEAD8400FFFF
      FF00F7FFF70042B54200009C0000009C0000009C0000009C000021A51800FFEF
      DE00E7DEC600C6BDAD00B58484000000000000000000000000009A666600FF9B
      9C00FD979800FC969700FE979800E388890069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70055E2820008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7B58C00FFFF
      FF00FFFFFF00E7F7E70084CE840039B5390039AD31008CCE840042AD3900AD84
      7300BD847B00BD847B00B58484000000000000000000000000009A666600FF9F
      A000FF9A9B00FF999A00FF9A9B00E78C8D0069333400FDF3D400FFFFDF00FFFF
      DD00FFFFDF00F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70063F0970008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7B58C00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00EFF7EF00E7F7E700FFFFFF00BDC6AD00A58C
      6B00EFB57300EFA54A00C6846B000000000000000000000000009A6666009A66
      6600E98E8F00FE999A00FF9D9E00EB8F900069333400FBF0D200FDFCDC00FDFC
      DA00FDFCDC00F9C5C6009A666600000000000000000000000000000000000000
      0000000000000000000008780E0076F9A70076F9A70008780E00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFBD9400FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7D6D600BD84
      7B00FFC67300CE94730000000000000000000000000000000000000000000000
      00009A666600B0717200D7868700DA888800693334009A6666009A6666009A66
      66009A6666009A6666009A666600000000000000000000000000000000000000
      000000000000000000000000000008780E0008780E0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFBD9400FFF7
      F700FFF7F700FFF7F700FFF7F700FFF7F700FFF7F700FFF7F700E7D6CE00BD84
      7B00CE9C84000000000000000000000000000000000000000000000000000000
      000000000000000000009A6666009A6666006933340000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFBD9400DEAD
      8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400DEAD8400BD84
      7B0000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000008000F801FFFFE0008000F8010180C000
      8000F801018080008000F801018000008000F801018000008000F80101800000
      8000F801EFF600008000D801E00700008000C001FE7F00008000E003FE7F0000
      8000C007F00F00018000E00FF00F00038000E07FF00F00078000C9FFF00F8003
      8000D99FF00FC001FFFFF9FFFFFFE000FDC7FFFFE07F1FFFF003E07FC01F0FFF
      C001C01FC00F07FF0001C00FC00783FF0001C007C007C1FF0001C007C007E10F
      0001C007C007F0038001C007C007F801C003C007E007F801E007C007FC07F800
      F00FE00FFF9FF800F03FF21FF39FF800F03FF39FF39FF801F03FF13FF39FFC01
      E03FF83FF83FFE03E07FFFFFFC7FFF0FFE7FFFFFFFFFC001F07FFE7FFFFFC001
      C001FC3FFFFFC001C001FC3FFFFFC001C001FC3FFFFFC001C001FC3FFFFFC001
      C001C003E007C001C0018001C003C001C0018001C003C001C001C003E007C001
      C001FC3FFFFFC001C001FC3FFFFFC001C001FC3FFFFFC001C001FC3FFFFFC003
      F001FE7FFFFFC007FC7FFFFFFFFFC00F00000000000000000000000000000000
      000000000000}
  end
  inherited fmstr: TFormStorage
    StoredProps.Strings = (
      'plWhA2.Height')
    Left = 196
    Top = 248
  end
  inherited ActionList2: TActionList
    Left = 196
    Top = 196
  end
  inherited ppPrint: TTBPopupMenu
    Left = 340
    Top = 236
    object TBItem14: TTBItem
      Action = acPrintAppl
    end
    object tbiPrintReport: TTBItem
      Caption = #1055#1077#1095#1072#1090#1072#1090#1100' '#1072#1088#1090#1080#1082#1091#1083#1099' '#1089' '#1084#1072#1089#1082#1072#1084#1080
      OnClick = tbiPrintReportClick
    end
  end
  object acEvent: TActionList
    Images = ilButtons
    Left = 76
    Top = 192
    object acAddRecord: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
      ImageIndex = 1
      OnExecute = acAddRecordExecute
    end
    object acAddSz: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1088#1072#1079#1084#1077#1088
      OnExecute = acAddSzExecute
    end
    object acAddArtByMask: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083#1099' '#1087#1086' '#1084#1072#1089#1082#1077
      OnExecute = acAddArtByMaskExecute
    end
    object acDelRecord: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
      ImageIndex = 2
      OnExecute = acDelRecordExecute
    end
    object acClose: TAction
      Caption = #1047#1072#1082#1088#1099#1090#1100
      ImageIndex = 5
      OnExecute = acCloseExecute
    end
    object acImportAnaliz: TAction
      Caption = #1048#1084#1087#1086#1088#1090
      ImageIndex = 8
      OnExecute = acImportAnalizExecute
      OnUpdate = acImportAnalizUpdate
    end
    object acLinkAnaliz: TAction
      Caption = #1055#1088#1080#1089#1086#1077#1076#1077#1085#1080#1090#1100
      ImageIndex = 9
      OnExecute = acLinkAnalizExecute
    end
    object acRefreshWork: TAction
      Caption = #1050#1086#1083#1086#1085#1082#1091' "'#1042' '#1088#1072#1073#1086#1090#1077'"'
      ImageIndex = 3
      OnExecute = acRefreshWorkExecute
    end
    object acImage: TAction
      Caption = #1048#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
      OnExecute = acImageExecute
    end
    object acShowWork: TAction
      Caption = #1042' '#1088#1072#1073#1086#1090#1077
      OnExecute = acShowWorkExecute
    end
    object acPrintAppl: TAction
      Caption = #1055#1077#1095#1072#1090#1072#1090#1100' '#1079#1072#1103#1074#1082#1091
      ImageIndex = 4
      OnExecute = acPrintApplExecute
    end
    object acShowID: TAction
      ShortCut = 8260
      OnExecute = acShowIDExecute
    end
    object acChangeArt: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1072#1088#1090#1080#1082#1091#1083
      Enabled = False
      OnExecute = acChangeArtExecute
    end
    object acChangeSz: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1088#1072#1079#1084#1077#1088
      Enabled = False
      OnExecute = acChangeSzExecute
    end
    object acShowAssort: TAction
      Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      OnExecute = acShowAssortExecute
    end
    object acRefreshSell: TAction
      Caption = #1050#1086#1083#1086#1085#1082#1091' "'#1055#1088#1086#1076#1072#1078#1080'"'
      OnExecute = acRefreshSellExecute
      OnUpdate = acRefreshSellUpdate
    end
  end
  object OpenDialog1: TOpenDialog
    Filter = #1040#1085#1072#1083#1080#1079'|*.nlz|'#1042#1089#1077'|*.*'
    Left = 272
    Top = 184
  end
  object taWhWorkOper: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct wh.PSID, wh.OPERID, wh.U, wh.Q'
      'from WhAppl wh, D_Dep d'
      'where wh.Art2Id=:ART2ID and'
      '          wh.SZ=:SZ and          '
      '          wh.PSID=d.DEPID and'
      '          wh.Q<>0  and'
      '          band(d.DEPTYPES, 2)=2')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taWhWorkOperBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 76
    Top = 240
    object taWhWorkOperOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'WHAPPL.OPERID'
      Required = True
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWhWorkOperU: TSmallintField
      FieldName = 'U'
      Origin = 'WHAPPL.U'
      Required = True
    end
    object taWhWorkOperPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'WHAPPL.PSID'
      Required = True
    end
    object taWhWorkOperQ: TIntegerField
      FieldName = 'Q'
      Origin = 'WHAPPL.Q'
      Required = True
    end
  end
  object ppRefresh: TTBPopupMenu
    Left = 392
    Top = 236
    object TBItem1: TTBItem
      Action = acRefreshWork
    end
    object TBItem7: TTBItem
      Action = acRefreshSell
    end
  end
  object ppDoc: TTBPopupMenu
    Images = ilButtons
    Left = 340
    Top = 184
    object TBItem3: TTBItem
      Action = acAddRecord
    end
    object TBItem6: TTBItem
      Action = acAddSz
    end
    object TBItem2: TTBItem
      Action = acDelRecord
      Caption = #1059#1076#1072#1083#1080#1090#1100
    end
    object TBItem12: TTBItem
      Action = acChangeArt
    end
    object TBItem11: TTBItem
      Action = acChangeSz
    end
    object TBSeparatorItem1: TTBSeparatorItem
    end
    object TBItem4: TTBItem
      Action = acPrintAppl
    end
    object TBSeparatorItem2: TTBSeparatorItem
    end
    object TBItem5: TTBItem
      Action = acImage
    end
    object TBItem10: TTBItem
      Action = acShowWork
    end
  end
  object ppAdd: TTBPopupMenu
    Left = 396
    Top = 184
    object TBItem9: TTBItem
      Action = acAddRecord
    end
    object TBItem8: TTBItem
      Action = acAddSz
    end
    object TBItem13: TTBItem
      Action = acAddArtByMask
    end
  end
  object taArtMask: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  a2.Art2Id, '
      '  a.D_ArtId, '
      '  a.Art, '
      '  a.MW'
      'from '
      '  Art2 a2 '
      '  left outer join D_Art a on (a.D_ARTID = a2.D_ARTID)'
      'where '
      '  a.Art like :ART and'
      '  a2.Art2='#39'-'#39
      '      ')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 160
    Top = 248
    poSQLINT64ToBCD = True
    object taArtMaskART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object taArtMaskD_ARTID: TFIBIntegerField
      FieldName = 'D_ARTID'
    end
    object taArtMaskART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taArtMaskMW: TFIBFloatField
      DefaultExpression = '0'
      FieldName = 'MW'
    end
  end
  object DataSetControl: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from APPLICATION$CONTROL(:APPLICATION$ID)')
    BeforeOpen = DataSetControlBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 152
    Top = 296
    object DataSetControlARTICLETITLE: TFIBStringField
      FieldName = 'ARTICLE$TITLE'
      Size = 32
      EmptyStrToNull = True
    end
  end
  object Analize: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 192
    Top = 296
    object AnalizeMODEL: TStringField
      FieldName = 'MODEL'
      Origin = 'ANALIZE$CDM$SELL.MODEL'
      Size = 32
    end
    object AnalizeMODELSIZE: TStringField
      FieldName = 'MODEL$SIZE'
      Origin = 'ANALIZE$CDM$SELL.MODEL$SIZE'
      Size = 8
    end
    object AnalizeDEPARTMENTID: TIntegerField
      FieldName = 'DEPARTMENT$ID'
      Origin = 'ANALIZE$CDM$SELL.DEPARTMENT$ID'
    end
    object AnalizeSELLCOUNT: TIntegerField
      FieldName = 'SELL$COUNT'
      Origin = 'ANALIZE$CDM$SELL.SELL$COUNT'
    end
    object AnalizeSTORAGECOUNT: TIntegerField
      FieldName = 'STORAGE$COUNT'
    end
  end
  object taSize: TpFIBDataSet
    SelectSQL.Strings = (
      '')
    Transaction = dm.tr
    Database = dm.db
    Left = 352
    Top = 288
    object taSizeID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object taRespPersons: TpFIBDataSet
    UpdateSQL.Strings = (
      '')
    InsertSQL.Strings = (
      '')
    SelectSQL.Strings = (
      'SELECT '
      '  DEPID, '
      '  NAME '
      'FROM '
      '  D_DEP '
      'WHERE '
      '  band(DEPTYPES, 1) = 1 and '
      '  band(DEPTYPES, 16) = 0 and'
      '  Name is not null'
      'ORDER BY '
      '  Name')
    Transaction = dm.tr
    Database = dm.db
    Left = 504
    Top = 272
    object taRespPersonsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taRespPersonsDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
  end
  object dsRespPersons: TDataSource
    DataSet = taRespPersons
    Left = 504
    Top = 304
  end
end
