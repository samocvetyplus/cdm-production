{*************************************}
{  ����� ��� ������ � ������.         }
{  Copyrigth (C) 2003 basile for CDM  }
{  v0.20                              }
{  created 29.10.2003                 }
{  last update 11.05.2004             }
{*************************************}
unit WOrderRej;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DocAncestor, ImgList, Grids, DBGrids, RXDBCtrl,
  RxToolEdit, StdCtrls, Mask, DBCtrls, ExtCtrls, 
  ComCtrls, Menus, db, Buttons, RXSplit, RxCurrEdit, RxCalc, 
  TB2Item, TB2Dock, TB2Toolbar, DBGridEh, DBCtrlsEh, ActnList,
  PrnDbgeh, DBLookupEh, M207Ctrls, uSemisStorage, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;

type
  TTransformInfo = record
    wOrderId : Variant;
    wOrderNo : string;
    JobId : integer;
    EndSemis : string;
    EndSemisName : string;
    FromOperId : string;
    FromOperName : string;
    RetFromOperId : string;
  end;

  TfmWOrderRej = class(TfmDocAncestor)
    lbOper: TLabel;
    lbExec: TLabel;
    ppInv: TPopupMenu;
    mnitPrint: TMenuItem;
    N3: TMenuItem;
    mnitAddToInv: TMenuItem;
    mnitAddToNew: TMenuItem;
    spitCalcLoss: TSpeedItem;
    mnitDelItem: TMenuItem;
    mnitSep1: TMenuItem;
    mnitCopy: TMenuItem;
    mnitIns: TMenuItem;
    mnitInsOrd: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N4: TMenuItem;
    SpeedItem1: TSpeedItem;
    Label1: TLabel;
    lbCurrInv: TLabel;
    Label6: TLabel;
    txtADocNo: TDBText;
    lbAInv: TLabel;
    lbAInvFrom: TLabel;
    txtADocDt: TDBText;
    plExt: TPanel;
    plExtTiitle: TPanel;
    lbMat: TLabel;
    tlbrT: TTBToolbar;
    it0: TTBItem;
    it3: TTBItem;
    it5: TTBItem;
    it1: TTBItem;
    gridSemis: TDBGridEh;
    cmbxMat: TDBComboBoxEh;
    tbdcSemis: TTBDock;
    TBToolbar2: TTBToolbar;
    TBItem5: TTBItem;
    acEvent: TActionList;
    acAddSemis: TAction;
    acDelSemis: TAction;
    it7: TTBItem;
    ppA: TPopupMenu;
    mnitCopyA: TMenuItem;
    mnitPasteA: TMenuItem;
    acACopy: TAction;
    acAPaste: TAction;
    N5: TMenuItem;
    acSCopy: TAction;
    acSPaste0: TAction;
    acSPaste1: TAction;
    acPrint: TAction;
    acArt: TAction;
    TBItem1: TTBItem;
    acTransform0: TAction;
    acAddSemisNew: TAction;
    printItem: TPrintDBGridEh;
    lbWhSemisName: TLabel;
    it6: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    Action1: TAction;
    chbxW: TDBCheckBoxEh;
    acExcel: TAction;
    Excel1: TMenuItem;
    acMultiSelect: TAction;
    lbDate: TLabel;
    acShowID: TAction;
    ac0: TAction;
    ac6: TAction;
    ac1: TAction;
    ac3: TAction;
    ac5: TAction;
    ac7: TAction;
    txtDefOper: TDBText;
    txtExec: TDBText;
    txtJobPS: TDBText;
    lcbxJobPS: TDBLookupComboboxEh;
    lcbxExec: TDBLookupComboboxEh;
    lcbxOper: TDBLookupComboboxEh;
    gridAEl: TDBGridEh;
    acPrintPreview: TAction;
    acPrintInvWithAssort: TAction;
    N7: TMenuItem;
    N8: TMenuItem;
    Label2: TLabel;
    lbCurrPS: TLabel;
    M207Bevel1: TM207Bevel;
    acActiveWH: TAction;
    acTransformDel: TAction;
    acTransformAssort: TAction;
    TBItem3: TTBItem;
    TBItem2: TTBItem;
    TBItem4: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnitAddToInvClick(Sender: TObject);
    procedure spitCalcLossClick(Sender: TObject);
    procedure spitSumClick(Sender: TObject);
    procedure lcbxOperEnter(Sender: TObject);
    procedure it5Click(Sender: TObject);
    procedure acAddSemisExecute(Sender: TObject);
    procedure gridSemisKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cmbxMatChange(Sender: TObject);
    procedure acDelSemisExecute(Sender: TObject);
    procedure acACopyExecute(Sender: TObject);
    procedure acAPasteExecute(Sender: TObject);
    procedure acSCopyExecute(Sender: TObject);
    procedure acPasteExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acArtExecute(Sender: TObject);
    procedure acTransform0Execute(Sender: TObject);
    procedure acAddSemisNewExecute(Sender: TObject);
    procedure gridItemColumns7EditButtonClick(Sender: TObject; var Handled: Boolean);
    procedure gridItemExit(Sender: TObject);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh; AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acExcelExecute(Sender: TObject);
    procedure acMultiSelectExecute(Sender: TObject);
    procedure acSPaste0Update(Sender: TObject);
    procedure acSPaste1Update(Sender: TObject);
    procedure acShowIDExecute(Sender: TObject);
    procedure ac0Execute(Sender: TObject);
    procedure ac6Execute(Sender: TObject);
    procedure ac1Execute(Sender: TObject);
    procedure ac3Execute(Sender: TObject);
    procedure ac5Execute(Sender: TObject);
    procedure ac7Execute(Sender: TObject);
    procedure acArtUpdate(Sender: TObject);
    procedure acAPasteUpdate(Sender: TObject);
    procedure acACopyUpdate(Sender: TObject);
    procedure lcbxExecEnter(Sender: TObject);
    procedure lcbxJobPSCloseUp(Sender: TObject; Accept: Boolean);
    procedure lcbxJobPSKeyValueChanged(Sender: TObject);
    procedure gridAElGetCellParams(Sender: TObject; Column: TColumnEh; AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure FormResize(Sender: TObject);
    procedure acPrintInvWithAssortExecute(Sender: TObject);
    procedure acPrintPreviewExecute(Sender: TObject);
    procedure acActiveWHExecute(Sender: TObject);
    procedure acDelSemisUpdate(Sender: TObject);
    procedure acTransform0Update(Sender: TObject);
    procedure acAddSemisUpdate(Sender: TObject);
    procedure acTransformDelExecute(Sender: TObject);
    procedure acTransformAssortExecute(Sender: TObject);
    procedure acTransformAssortUpdate(Sender: TObject);
  private
    FUseLog : boolean;
    FExtended : boolean;
    FItType : byte;
    FAddToNewInv : boolean;
    procedure SetExtended(const Value: boolean);
    procedure SetItType(const Value: byte); // ����� ������� ���������, ������������ ������ ��� �����������
  protected
    FTransformInfo : TTransformInfo;
    function FillTransformInfo : boolean;
    procedure Access;
    procedure SetReadOnly(ReadOnly : boolean);
    procedure SetOPeriod;
    procedure SetMode;
    function IsActionMayBe : boolean;
    function TryDelete: Boolean;
    function TryInsert: Boolean;
    function TryUpdate: Boolean;
  public
    procedure SetCurrInv;
    property Extended : boolean read FExtended write SetExtended;
    property ItType : byte read FItType write SetItType; // 0 - ������, 1 - �����
  end;

implementation

uses MainData, dbUtil, DictData, PrintData, fmUtils, Variants, ApplData,
     ADistr, dbTree, DictAncestor, dSemis, eSQ, Editor, DateUtils, dSemisT1,
     Log, M207Export, eInvCreateMode, DbEditor, MsgDialog,
     dOper, DbUtilsEh, dDepT1, dSimpleDepart, ProductionConsts,
     frmDialogSemisRej, frmDialogSemis;

{$R *.DFM}

const
  t_1 : set of byte = [0, 6];
  t_2 : set of byte = [1, 3, 5, 7];

{ TfmWOrderRej }

procedure TfmWOrderRej.FormCreate(Sender: TObject);
begin

  edNoDoc.DataSource := dmMain.dsrWOrderList;
  dtedDateDoc.DataSource := dmMain.dsrWOrderList;
  gridItem.DataSource := dmMain.dsrWOrder;


  gridItem.FieldColumns['INVID'].Visible    := False;
  gridItem.FieldColumns['WOITEMID'].Visible := False;
  gridItem.FieldColumns['WORDERID'].Visible := False;
  gridItem.FieldColumns['ITDATE'].Visible   := False;
  gridItem.FieldColumns['REJREF'].Visible   := False;

  lbWhSemisName.Caption := '��������: '+dm.User.wWhName;
  FDocName := '����� �� �����';
  dm.WorkMode := 'wOrderRej';
  gridSemis.RowLines := 2;
  // ��������� �������� ��� ������ ��������������
  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMat);
  cmbxMat.ItemIndex := 0;
  dm.AMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := cmbxMatChange;

  // ���������� �����������
  dmMain.FilterOperId := ROOT_OPER_CODE;

  // ��������� ����� ������
  FItType := 255;
  FExtended := False;//True;
  Extended := True;  //False;

  FAddToNewInv := False;
  // ����� �������
  Access;
  
  inherited;

  OpenDataSets([dm.taSemis, dm.taOper, dmMain.taPsDep, dmMain.taPsMol, dmMain.taWOrder,
    dm.taDepart, dmMain.quWOrder_T, dmMain.taPsOper]);
  if not dmMain.taWOrderListJOBDEPID.IsNull and
         dmMain.taWOrderListJOBID.IsNull and
         (not dmMain.taPSMol.IsEmpty) then
  begin
    dmMain.taWOrderList.Edit;
    dmMain.taWOrderListJOBID.AsInteger := dmMain.taPSMolMOLID.AsInteger;
    dmMain.taWOrderList.Post;
    dmMain.taWOrderList.Transaction.CommitRetaining;
  end;

  InitBtn(Boolean(dmMain.taWOrderListISCLOSE.AsInteger));
  SetCurrInv;
  SetOPeriod;
  SetMode;
  Self.Resize;
end;

procedure TfmWOrderRej.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PostDataSets([dmMain.taWOrder]);
  CloseDataSets([dmMain.taWOrder, dmMain.taPsOper, dmMain.taPsDep, dm.taSemis,
    dm.taDepart, dmMain.quWOrder_T, dmMain.taPsMol, dm.taOper]);
  dm.AMatId := ROOT_MAT_CODE;
  inherited;
end;

procedure TfmWOrderRej.SetCurrInv;
var
  Id : Variant;
  //PsName : Variant;
  CurrDoc, CurrPsName : string;
begin
  with dmMain do
  begin
    Id := ExecSelectSQL('select InvId from GetCurrInv('+taWOrderListWORDERID.AsString+', '+IntToStr(dm.User.wWhId)+')', quTmp);

    if VarIsNull(Id)or(Id = 0)then
    begin
      dmMain.CurrInv := -1;
      dmMain.CurrPsId := -1;
      CurrDoc := '�����������';
      CurrPsName := '�����������';
    end
    else begin
      dmMain.CurrInv := Id;
      dmMain.CurrPsId := ExecSelectSQL('select i.DepId from Inv i where i.InvId='+IntToStr(Id), quTmp);
      CurrDoc := ExecSelectSQL('select i.DocNo from Inv i where i.InvId='+IntToStr(Id), quTmp);
      CurrPsName := ExecSelectSQL('select d.Name from Inv i, D_Dep d where i.DepId=d.DepId and i.InvId='+IntToStr(Id), quTmp);
    end;
  end;
  lbCurrInv.Caption := CurrDoc;
  lbCurrPs.Caption := CurrPsName;
end;

procedure TfmWOrderRej.mnitAddToInvClick(Sender: TObject);
begin
  dmMain.CurrInv := dmMain.taWOrderINVID.AsInteger;
  dmMain.InvMode := 1;
  dmMain.taWOrder.Append;
  gridItem.SelectedField := dmMain.taWorderSEMISNAME;
end;

procedure TfmWOrderRej.spitCalcLossClick(Sender: TObject);
begin
  PostDataSets([dmMain.taWOrder]);
  dm.tr.CommitRetaining;
end;

procedure TfmWOrderRej.spitSumClick(Sender: TObject);
begin
  ReOpenDataSets([dmMain.quWOrder_T]);
end;

procedure TfmWOrderRej.lcbxOperEnter(Sender: TObject);
begin
  if dmMain.taWOrderListJOBDEPID.IsNull then
  begin
    ActiveControl := lcbxJobPS;
    raise Exception.Create(Format(rsSelectPS, ['']));
  end
  else begin
    ReOpenDataSet(dmMain.taPsOper);
    if not (dmMain.taWOrderList.State in [dsInsert, dsEdit]) then dmMain.taWOrderList.Edit;
    dmMain.taWOrderListDEFOPER.AsString := dmMain.taPsOperOPERID.AsString;
  end;
end;

procedure TfmWOrderRej.SetExtended(const Value: boolean);
begin
  if Value = FExtended then eXit;
  FExtended := Value;
  if Value then
  begin
    tlbrT.Visible := True;
    tbdcSemis.Visible := True;
    plExt.Visible := True;
    ItType := 0;
  end
  else begin
    plExt.Visible := False;
    tbdcSemis.Visible := False;
    tlbrT.Visible := False;
  end
end;

procedure TfmWOrderRej.it5Click(Sender: TObject);
begin
  ItType := 5;
end;

procedure TfmWOrderRej.SetItType(const Value: byte);
begin
  if(FItType = Value)then eXit;
  FItType := Value;
  ReOpenDataSet(dmMain.taWhSemis);
  case FItType of
    0 : it0.Checked := True;
    1 : it1.Checked := True;
    3 : it3.Checked := True;
    5 : it5.Checked := True;
    6 : it6.Checked := True;
    7 : it7.Checked := True;
  end;
  case FItType of
    1,3,5 : gridSemis.FieldColumns['DEPNAME'].Visible := True
    else gridSemis.FieldColumns['DEPNAME'].Visible := False;
  end
end;

procedure TfmWOrderRej.acAddSemisExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'DEPID'; sName: 'NAME'; sTable: 'D_DEP'; sWhere: '');
var
  t : Variant;
  PsId, MolId : integer;

  SourceStorage: TStorage;
  TargetStorage: TStorage;
  SpoilageDelta: TSpoilageDelta;

procedure Go;
begin
(* ���� ��������� ��������� �� ��������� �� ���� � ����� ������ �� ������
       ����� ��������� ����� ���������, ����� ��������� � ������ *)
  with dmMain do
  begin
    if(not FAddToNewInv)and(PsId = CurrPsId)then
    begin
      if(CurrInv = -1)then t := ItType
      else t := ExecSelectSQL('select max(ItType) from woItem where InvId='+IntToStr(CurrInv), quTmp);
      if VarIsNull(t)then t := ItType;
      case t of
        0,6 : if(ItType in t_1)then dmMain.InvMode := 1 else dmMain.InvMode := 0;
        1,3,5,7 : if(ItType in t_2)then dmMain.InvMode := 1 else dmMain.InvMode := 0;
        else raise EInternal.Create(Format(rsInternalError, ['101']));
      end;
    end
    else dmMain.InvMode := 0;
    taWOrder.Append;
    taWOrderSEMIS.AsString := taWhSemisSEMISID.AsString;
    taWOrderOPERID.AsString := dm.ReAssemblyId;
    taWOrderFROMOPERID.AsString := dm.RejOperId;
    taWOrderITTYPE.AsInteger := ItType;
    taWOrderJOBDEPID.AsInteger := PsId;
    taWOrderJOBID.AsInteger := MolId;
    taWOrderQ.AsInteger := SemisQ;
    taWOrderW.AsFloat := SemisW;
    taWOrder.Post;
    taWhSemis.Refresh;
  end;
  FAddToNewInv := False;
end;

begin
  if not TryInsert then Exit;
  
  with dmMain do
  begin
    (* �������� �� ��� ������������� *)
    if(ItType in t_1)or((ItType in t_2) and taWhSemisDEPID.IsNull)then
    begin
      if ShowDictForm(TfmdSimpleDepart, dm.dsrDepart, Rec, '����� ����� ��������', dmSelRecordReadOnly, nil)<>mrOk then SysUtils.Abort;
      PsId := DictAncestor.vResult;
    end
    else PsId := taWhSemisDEPID.AsInteger;
    MolId := ExecSelectSQL('select min(MolId) from D_PS where DepId='+IntToStr(PsId), quTmp);
    (* ���-�� � ��� *)
    if ItType = 0 then
    begin

    if DialogSemisRej <> nil then FreeAndNil(DialogSemisRej);
      try
        DialogSemisRej := TDialogSemisRej.Create(Self);
        SourceStorage := TStorage.Create;
        SourceStorage.ID := dmMain.taWhSemisDEPID.AsInteger;
        SourceStorage.Element.Code := Trim(dmMain.taWhSemisSEMISID.AsString);
        SourceStorage.Element.Operation := Trim(dmMain.taWhSemisOPERID.AsString);
        SourceStorage.Element.UnitQuantity := dmMain.taWhSemisUQ.AsInteger;
        SourceStorage.Element.UnitWeight := dmMain.taWhSemisUW.AsInteger;
        TargetStorage := TStorage.Create;
        TargetStorage.ID := PsId;
        TargetStorage.Element.Code := Trim(dmMain.taWhSemisSEMISID.AsString);
        TargetStorage.Element.Operation := dm.ReAssemblyId;
        TargetStorage.Element.UnitQuantity := dmMain.taWhSemisUQ.AsInteger;
        TargetStorage.Element.UnitWeight := dmMain.taWhSemisUW.AsInteger;

        if DialogSemisRej.Execute(SourceStorage, TargetStorage, smOut) then
        begin
          dmMain.SemisQ := DialogSemisRej.DataSetTargetSummaryQuantity.AsInteger;
          dmMain.SemisW := DialogSemisRej.DataSetTargetSummaryWeight.AsFloat;
          Go;

          DialogSemisRej.DataSetTarget.First;
          while not DialogSemisRej.DataSetTarget.Eof do
          begin
            SpoilageDelta.StorageElementID := TargetStorage.Element.ID;
            SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
            SpoilageDelta.OperationID := DialogSemisRej.DataSetTargetOperationID.AsInteger;
            SpoilageDelta.Quantity := DialogSemisRej.DataSetTargetQuantity.AsInteger;
            SpoilageDelta.Weight := DialogSemisRej.DataSetTargetWeight.AsFloat;
            TDialogSemisRej.Delta(SpoilageDelta);

            SpoilageDelta.StorageElementID := SourceStorage.Element.ID;
            SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
            SpoilageDelta.OperationID := DialogSemisRej.DataSetTargetOperationID.AsInteger;
            SpoilageDelta.Quantity := - DialogSemisRej.DataSetTargetQuantity.AsInteger;
            SpoilageDelta.Weight := - DialogSemisRej.DataSetTargetWeight.AsFloat;
            TDialogSemisRej.Delta(SpoilageDelta);
            DialogSemisRej.DataSetTarget.Next;
         end;
         DialogSemisRej.DataSetTargetSummary.Next;
       end;
     finally
       if SourceStorage <> nil then FreeAndNil(SourceStorage);
       if TargetStorage <> nil then FreeAndNil(TargetStorage);
       if DialogSemisRej <> nil then FreeAndNil(DialogSemisRej);
     end;

     end
     else
     begin
       if not _ShowEditorQW(taWhSemisQ.AsInteger, taWhSemisW.AsFloat, chbxW.Checked) then eXit;
       Go;
     end;
  end;
  FAddToNewInv := False;
end;

procedure TfmWOrderRej.gridSemisKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_SPACE : if(Shift = [])then acAddSemis.Execute;
  end;
end;

procedure TfmWOrderRej.cmbxMatChange(Sender: TObject);
begin
  dm.AMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSets([dmMain.taWhSemis]);
end;

procedure TfmWOrderRej.acDelSemisExecute(Sender: TObject);
var
  isLocateWh, isTransform : boolean;
  DepId, SemisId, Ref, OperId : Variant;


  Source, Target: TStorage;
  SpoilageDelta: TSpoilageDelta;
  ResultQuantity: Integer;
  ResultWeight: Double;
  TotalResultQuantity: Integer;
  TotalResultWeight: Double;

procedure Go;
begin
  with dmMain do
  begin
    (* ������� ������� �������� *)
    if taWOrderITTYPE.AsInteger <> ItType then ItType := taWOrderITTYPE.AsInteger;
    SemisId := taWOrderSEMIS.AsString;
    case ItType of
      0, 6    : begin
        OperId := taWOrderFROMOPERID.AsVariant;
        DepId := taWOrderDEPID.AsInteger;
      end;
      1,3,5,7 : begin
        OperId := taWOrderFROMOPERID.AsVariant;
        DepId := taWOrderJOBDEPID.AsInteger;
      end;
      else raise EInternal.Create(Format(rsInternalError, ['101']));
    end;
    isLocateWh := taWhSemis.Locate('DEPID;SEMISID;OPERID;UQ;UW', VarArrayOf([DepId, SemisId, OperId, taWOrderUQ.AsInteger, taWOrderUW.AsInteger]), []);

    if(dmMain.SemisQ = dmMain.taWOrderQ.AsInteger) and
      (Abs(dmMain.SemisW - dmMain.taWOrderW.AsFloat)<Eps)
    then taWOrder.Delete
    else begin
      taWOrder.Edit;
      taWOrderQ.AsInteger := taWOrderQ.AsInteger - dmMain.SemisQ;
      taWOrderW.AsFloat := taWOrderW.AsFloat - dmMain.SemisW;
      taWOrder.Post;
    end;
    if not isLocateWh then
    begin
      taWhSemis.Append;
      taWhSemisDEPID.AsInteger := DepId;
      taWhSemisOPERID.AsVariant := OperId;
      taWhSemisSEMISID.AsString := SemisId;
      taWhSemisQ.AsInteger := 0;
      taWhSemisW.AsInteger := 0;
      taWhSemis.Post;
    end;
    taWhSemis.Refresh;
  end;
end;

begin
  if not TryDelete then Exit;
  (* ����� �� �������� ������������� *)
  if(dmMain.taWOrderDEPID.AsInteger <> dm.User.wWhId) then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taWOrderDEPNAME.AsString]));

  (* ���������� ��� ��� -> ������������� ��� ������� �������� *)
  case dmMain.taWOrderITTYPE.AsInteger of
    0 : isTransform := not dmMain.taWOrderREJREF.IsNull;
    3 : begin
          Ref := ExecSelectSQL('select WOItemId from WOItem where RejRef = '+dmMain.taWOrderWOITEMID.AsString, dmMain.quTmp);
          isTransform := not(VarIsNull(Ref)or(Ref = 0));
    end;
    else isTransform := False;
  end;
  (* ���� ��� ������������� �������� �������������� �������� *)
  if isTransform then
  begin
    acTransformDel.Execute;
    eXit;
  end;
  //if not _ShowEditorQW(taWOrderQ.AsInteger, taWOrderW.AsFloat, chbxW.Checked) then eXit;
  //Go;
  with dmMain do
  begin
    if taWOrderITTYPE.AsInteger = 0 then
    begin

    Source := TStorage.Create;
    Source.ID := dm.User.wWhId;
    Source.Connected := True;
    Source.Element.Code := dmMain.taWOrderSEMIS.AsString;
    Source.Element.Operation := dm.RejOperId;
    Source.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
    Source.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
    Source.Element.Connected := True;

    Target := TStorage.Create;
    Target.ID := dmMain.taWOrderJOBDEPID.AsInteger;
    Target.Element.Code := dmMain.taWOrderSEMIS.AsString;
    Target.Element.Operation := dmMain.taWOrderOPERID.AsString;
    Target.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
    Target.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
    Target.Element.Connected := True;

    if DialogSemisRej <> nil then FreeAndNil(DialogSemisRej);
    DialogSemisRej := TDialogSemisRej.Create(Self);
    if DialogSemisRej.Execute(Source, Target, smIn, dmMain.taWOrderWOITEMID.AsInteger) then
    begin
      TotalResultQuantity := 0;
      TotalResultWeight := 0;
      DialogSemisRej.DataSetSource.First;
      while not DialogSemisRej.DataSetSource.Eof do
      begin
         ResultQuantity := DialogSemisRej.DataSetSourceQuantity.AsInteger;
         ResultWeight := DialogSemisRej.DataSetSourceWeight.AsFloat;
         TotalResultQuantity := TotalResultQuantity + ResultQuantity;
         TotalResultWeight := TotalResultWeight + ResultWeight;
         DialogSemisRej.DataSetSource.Next;
      end;
      dmMain.SemisQ := TotalResultQuantity;
      dmMain.SemisW := TotalResultWeight;

      DialogSemisRej.DataSetSource.First;
      while not DialogSemisRej.DataSetSource.Eof do
      begin
         ResultQuantity := DialogSemisRej.DataSetSourceQuantity.AsInteger;
         ResultWeight := DialogSemisRej.DataSetSourceWeight.AsFloat;

         SpoilageDelta.StorageElementID := Source.Element.ID;
         SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
         SpoilageDelta.OperationID := DialogSemisRej.DataSetSourceOperationID.AsInteger;
         SpoilageDelta.Quantity := ResultQuantity;
         SpoilageDelta.Weight := ResultWeight;

         TDialogSemisRej.Delta(SpoilageDelta);

         SpoilageDelta.StorageElementID := Target.Element.ID;
         SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
         SpoilageDelta.OperationID := DialogSemisRej.DataSetSourceOperationID.AsInteger;
         SpoilageDelta.Quantity := -ResultQuantity;
         SpoilageDelta.Weight := -ResultWeight;
         TDialogSemisRej.Delta(SpoilageDelta);

         DialogSemisRej.DataSetSource.Next;
      end;
      Go;      
    end;
    Source.Free;
    Target.Free;
    FreeAndNil(DialogSemisRej);

    end
    else
    begin
      if not _ShowEditorQW(taWOrderQ.AsInteger, taWOrderW.AsFloat, chbxW.Checked) then eXit;
      Go;
    end;
  end;
end;

procedure TfmWOrderRej.acACopyExecute(Sender: TObject);
var
  Bookmark : Pointer;
  i : integer;
begin
  with dmMain do
  begin
    if not taAssortEl.Active then eXit;
    ExecSQL('delete from B_AInv where UserId='+IntToStr(dm.User.UserId), dmMain.quTmp);
    if dgMultiSelect in gridAEl.Options then
      for i:=0 to Pred(gridAEl.SelectedRows.Count) do
      begin
        taAssortEl.GotoBookmark(Pointer(gridAEl.SelectedRows.Items[i]));
        ExecSQL('insert into B_AInv(ID, USERID, ART2ID, SZID, '+
           'OPERID, U, Q) values('+IntToStr(dm.GetId(33))+','+IntToStr(dm.User.UserId)+','+
           taAssortElART2.AsString+','+taAssortElSZID.AsString+',"'+
           taAssortElOPERID.AsString+'",'+taAssortElU.AsString+','+taAssortElQ.AsString+')', dmMain.quTmp);
    end
    else begin
      BookMark := taAssortEl.GetBookmark;
      try
        taAssortEl.First;
        while not taAssortEl.Eof do
        begin
          ExecSQL('insert into B_AInv(ID, USERID, ARTID, ART2ID, SZID, '+
             'OPERID, U, Q) values('+IntToStr(dm.GetId(33))+','+IntToStr(dm.User.UserId)+','+
             taAssortElARTID.AsString+','+taAssortElART2ID.AsString+','+
             taAssortElSZID.AsString+',"'+taAssortElOPERID.AsString+'",'+
             taAssortElU.AsString+','+taAssortElQ.AsString+')', dmMain.quTmp);
          taAssortEl.Next;
        end;
        taAssortEl.Transaction.CommitRetaining;
      finally
        taAssortEl.GotoBookmark(BookMark);
      end;
    end
  end
end;

procedure TfmWOrderRej.acAPasteExecute(Sender: TObject);
var
  DocNo : string;
  Q : integer;
begin
  if (dmMain.taWOrderDEPID.AsInteger <> dm.User.wWhId) then
    raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taWOrderDEPNAME.AsString]));
  with dmMain do
  begin
    if taWOrder.IsEmpty then eXit;
    if (dmMain.taWOrderAINV.IsNull)or(dmMain.taWOrderAINV.AsInteger = 0) then
    begin
      if MessageDialog('������� ����� ���������?', mtConfirmation, [mbOk, mbCancel], 0)<>mrOk then eXit;
      dmAppl.AInvId := -1;
      OpenDataSet(dmAppl.taAInv);
      dmAppl.taAInv.Append;
      taWOrder.Edit;
      taWOrderAINV.AsInteger := dmAppl.taAInvINVID.AsInteger;
      dmAppl.taAInv.Post;
      taWOrder.Post;
      CloseDataSet(dmAppl.taAInv);
    end
    else begin
      DocNo := ExecSelectSQL('select DOCNO from Inv where InvId='+taWOrderAINV.AsString, quTmp);
      if (DocNo = '0')then raise EInternal.Create(Format(rsInternalError, ['113']));
      if MessageDialog('�������� � ��������� �'+DocNo, mtConfirmation, [mbOk, mbCancel], 0)<>mrOk then eXit;
    end;
    // ���������� � ���������
    ExecSQL('execute procedure Paste_A('+IntToStr(dm.User.UserId)+', '+taWOrderWOITEMID.AsString+')', quTmp);
    dm.tr.CommitRetaining;
    ReOpenDataSets([taAssortInv, taAssortEl]);
    // �������� ���-�� � ������ ��������� ������
    Q := 0;
    while not taAssortEl.Eof do
    begin
      Q := Q + taAssortElQ.AsInteger;
      taAssortEl.Next;
    end;
    taWOrder.Edit;
    taWOrderQ.AsInteger := Q;
    taWOrder.Post;
  end;
end;

procedure TfmWOrderRej.acSCopyExecute(Sender: TObject);
var
  i : integer;
begin
  if(not dmMain.tawOrder.Active) then eXit;
  ExecSQL('delete from B_WInv where UserId='+IntToStr(dm.User.UserId), dmMain.quTmp);
  with dmMain do
    if(gridItem.SelectedRows.Count > 1)  then
      for i:=0 to Pred(gridItem.SelectedRows.Count) do
      begin
        taWOrder.GotoBookmark(Pointer(gridItem.SelectedRows.Items[i]));
        ExecSQL('insert into B_WInv(ID, USERID, DEPID, SEMIS, OPERID, Q, W) '+
           ' values('+IntToStr(dm.GetId(35))+','+IntToStr(dm.User.UserId)+','+
             taWOrderDEPID.AsString+',"'+taWOrderSEMIS.AsString+'", "'+
             taWOrderOPERID.AsString+'",'+taWOrderQ.AsString+', "'+
             StringReplace(taWOrderW.AsString, ',', '.', [])+'")', dmMain.quTmp);
      end
    else begin
      ExecSQL('insert into B_WInv(ID, USERID, DEPID, SEMIS, OPERID, Q, W) '+
           ' values('+IntToStr(dm.GetId(35))+','+IntToStr(dm.User.UserId)+','+
           taWOrderDEPID.AsString+', "'+taWOrderSEMIS.AsString+'", "'+
           taWOrderOPERID.AsString+'",'+taWOrderQ.AsString+',"'+
           StringReplace(taWOrderW.AsString, ',', '.', [])+'")', dmMain.quTmp);
      taWOrder.Transaction.CommitRetaining;
    end;
end;

procedure TfmWOrderRej.acPasteExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'OPERID'; sName: 'NAME'; sTable: 'D_OPER'; sWhere: '');
var
  ItType : integer;
  bNew : boolean;
  WhSemisName : string;
begin
  if not dmMain.taWOrder.Active then eXit;
  {if (dmMain.taWOrderDEPID.AsInteger <> dm.User.wWhId) then
    raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taWOrderDEPNAME.AsString]));}
  bNew := False;
  with dmMain do
  begin
    if(CurrInv <> -1)then
    begin
      ItType := ExecSelectSQL('select max(ItType) from woItem where InvId='+IntToStr(dmMain.CurrInv), dmMain.quTmp);
      if(ItType in [0, 6])then
        with (Sender as TComponent) do
          if(Tag = 1)then
            if MessageDialog('������� ����� ���������?', mtConfirmation, [mbOk, mbCancel], 0)<>mrOk then eXit
            else bNew := True
          else
            case MessageDialog('�������� � ������� ���������?', mtConfirmation, [mbNo, mbOk, mbCancel], 0) of
              mrCancel : eXit;
              mrOk : bNew := False;
              mrNo : bNew := True;
            end
      else if(ItType in [1, 3, 5, 7])then
      begin
        if(Tag = 0) then
          if MessageDialog('������� ����� ���������?', mtConfirmation, [mbOk, mbCancel], 0)<>mrOk then eXit
          else bNew := True
        else
          case MessageDialog('�������� � ������� ���������?', mtConfirmation, [mbNo, mbOk, mbCancel], 0) of
            mrCancel : eXit;
            mrOk : bNew := False;
            mrNo : bNew := True;
          end
      end
      else raise EInternal.Create(Format(rsInternalError, ['101']));
    end;
    taSBuffer.Open;
    while not taSBuffer.Eof do
    begin
      if bNew then InvMode := 0 else InvMode := 1;
      taWorder.Append;
      taWOrderITTYPE.AsInteger := (Sender as TComponent).Tag;
      taWOrderSEMIS.AsString := taSBufferSEMIS.AsString;
      taWOrderQ.AsFloat := taSBufferQ.AsFloat;
      taWOrderW.AsFloat := taSBufferW.AsFloat;

      case (Sender as TComponent).Tag of
        0 : if(taSBufferGOOD.AsInteger = 0) then // ������������ �� �������� �������
              taWOrderFROMOPERID.AsVariant := Null
            else
              if ShowDictform(TfmOper, dm.dsrOper, Rec, '�������� � ����� �������� �������� ������������ '+taSBufferSEMISNAME.AsString, dmSelRecordReadOnly)<> mrOk then
              begin
                taWOrder.Cancel;
                taSBuffer.Delete;
                continue;
              end
              else taWOrderFROMOPERID.AsVariant := DictAncestor.vResult;

        1 : if(taSBufferGOOD.AsInteger = 0) then // ������������ �� �������� �������
              taWOrderFROMOPERID.AsVariant := Null
            else
              if ShowDictform(TfmOper, dm.dsrOper, Rec, '�������� � ����� �������� ����������� ������������ '+taSBufferSEMISNAME.AsString, dmSelRecordReadOnly) <> mrOk then
              begin
                taWOrder.Cancel;
                taSBuffer.Delete;
                continue;
              end
              else taWOrderFROMOPERID.AsVariant := DictAncestor.vResult;
      end;
      taWOrder.Post;
      taSBuffer.Delete;
    end;
    taSBuffer.Close;
    dm.tr.CommitRetaining;
  end;
  // �������� ��������� ������
  WhSemisName := dmMain.taWhSemisSEMISNAME.AsString;
  ReOpenDataSet(dmMain.taWhSemis);
  // ����������� ����� 
  dmMain.taWhSemis.Locate('SEMISNAME', WhSemisName, []);
end;

procedure TfmWOrderRej.acArtExecute(Sender: TObject);
var
//  Position : integer;
  Id : Variant;
begin
  // ��������� ����� �������
  if (dm.User.AccProd and $20)<>$20 then raise Exception.Create('� ��� ��� ���� ������� � ������������!');
  if (dmMain.taWOrderDEPID.AsInteger <> dm.User.wWhId) then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taWOrderDEPNAME.AsString]));
  // ������ �� �����
  if(dmMain.taWOrderListISCLOSE.AsInteger =1)then raise Exception.Create(Format(rsWorderClose, [dmMain.taWOrderListDOCNO.AsString]));
  dmAppl.IsWorkWithStone := False;
  with dmMain do
  begin
    PostDataSet(taWOrder);
    if(dmMain.taWOrderOPERID.AsString = dm.ModifyAssortOperId)then
    begin
      dmAppl.ADistrType := adtMdfArt;
      dmAppl.ADistrKind := 0;
    end
    else if(dmMain.taWOrderOPERID.AsString = dm.ReAssemblyId)  then
    begin
      if taWOrderITTYPE.AsInteger in [0,6] then
      begin
        dmAppl.ADistrType := adtRej;
        dmAppl.ADistrKind := 0;
      end
      else begin
        raise Exception.Create(rsNotImplementation);
        eXit;
      end;
    end
    else begin
      dmAppl.ADistrType := adtRej;
      dmAppl.ADistrKind := 0;
    end;
    (* ������� �� ���������? *)
    Id := ExecSelectSQL('select InvId from Inv where InvId='+IntToStr(taWOrderAINV.AsInteger), dmMain.quTmp);
    if VarIsNull(Id)or(Id=0)then
    begin
      taWOrder.Edit;
      taWOrderAINV.AsVariant := Null;
      taWOrder.Post;
      taWOrder.Transaction.CommitRetaining;
    end;

    if taWOrderAINV.IsNull then
    begin
       if dmAppl.trAppl.Active then dmAppl.trAppl.Commit;
       dmAppl.trAppl.StartTransaction;
       OpenDataSet(dmAppl.taRejInv);
       dmAppl.taRejInv.Append;
       dmMain.taWOrder.Edit;
       dmMain.taWOrderAINV.AsInteger := dmAppl.taRejInvINVID.AsInteger;
       PostDataSets([dmAppl.taRejInv, dmMain.taWOrder]);
    end;
    CloseDataSets([dmAppl.taRejInv, dmAppl.taRejEl]);
    ShowAndFreeForm(TfmADistr, TForm(fmADistr));
    dm.WorkMode := 'wOrderRej';
    if not (taWOrderAPPLINV.IsNull and taWOrderAINV.IsNull) then ReOpenDataSets([taAssortInv, taAssortEl])
    else CloseDataSets([taAssortInv, taAssortEl]);
    RefreshDataSet(taWOrder);
    gridItem.SelectedField := taWOrderQ;
  end;
end;

procedure TfmWOrderRej.acTransform0Execute(Sender: TObject);
const
  Rec : TRecName = (sId: 'SEMISID'; sName: 'NAME'; sTable: 'D_SEMIS'; sWhere: '');
  RecDep : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_REJ'; sWhere: '');
var
  EndSemis : string;
  Id, RejId, RejRef : integer;
  RejPsId : Variant;

  SourceStorage: TStorage;
  TargetStorage: TStorage;
  SpoilageDelta: TSpoilageDelta;

procedure Go0;
begin
  with dmMain do
  begin
    // ��������� ������ �� � ����������� �������� �������������
    taSemisT1.Open;
    taSemisT1.Last;
    case taSemisT1.RecordCount of
      0 : raise EWarning.Create(Format(rsInvalidTransform1, [taWhSemisSEMISNAME.AsString]));
      1 : EndSemis := dmMain.taSemisT1SEMISID.AsString;
      else
        if ShowDictForm(TfmSemisT1, dmMain.dsrSemisT1, Rec, '������������� -> ������', dmSelRecordReadOnly)=mrOk
        then EndSemis := DictAncestor.vResult else SysUtils.Abort;
    end;
  end;
end;

procedure Go1;
begin
  with dmMain do
  begin
    InvMode := 0;
    taWOrder.Append;
    Id := taWOrderWOITEMID.AsInteger;
    taWOrderITTYPE.AsInteger := 0;
    taWOrderSEMIS.AsString := taWhSemisSEMISID.AsString;
    taWOrderOPERID.AsString := dm.TransformOperId;
    taWOrderFROMOPERID.AsString := dm.RejOperId;
    taWOrderDEPNAME.AsString := dm.User.wWhName;
    taWOrderJOBDEPNAME.AsString := taWOrderListJOBDEPNAME.AsString;
    taWOrderQ.AsInteger := SemisQ;
    taWOrderW.AsFloat := SemisW;
    taWOrderREJID.AsInteger := RejId;
    taWOrderREJNAME.AsString := dm.taRejNAME.AsString;
    taWOrderREJPSID.AsVariant := RejPsId;
    taWOrder.Post;
    taWhSemis.Refresh;

    if(taWhSemisGOOD.AsInteger = 1)and(MessageDialog('����� ����������� c ������������?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
       taWOrder.Locate('WOITEMID', Id, []);
       acArt.Execute;
    end;
  end;
end;

procedure Go2;
begin
  with dmMain do
  begin
    (* ������ ������� ������� �� �������� �� ���������� *)
    dmMain.InvMode := 0;
    dmMain.taWOrder.Append;
    RejRef := taWOrderWOITEMID.AsInteger;
    taWOrderITTYPE.AsInteger := 3;
    taWOrderSEMIS.AsString := EndSemis;
    taWOrderOPERID.AsString := dm.TransformOperId;
    taWOrderFROMOPERID.AsString := dm.TransformOperId;
    taWOrderDEPNAME.AsString := dm.User.wWhName;
    taWOrderJOBDEPNAME.AsString := taWOrderListJOBDEPNAME.AsString;
    taWOrderQ.AsInteger := 0;
    taWOrderW.AsFloat := SemisW;
    taWOrder.Post;
    taWhSemis.Refresh;
    if not taWOrder.Locate('WOITEMID', Id, []) then raise EInternal.Create('!');
    taWOrder.Edit;
    taWOrderREJREF.AsInteger := RejRef;
    taWOrder.Post;
  end;
end;

begin
  if not TryInsert then Exit;
  //if not dmMain._ShowEditorQW(dmMain.taWhSemisQ.AsInteger, dmMain.taWhSemisW.AsFloat, chbxW.Checked) then eXit;
                (* ������ �� �������� ������������� *)
  (* �������� ������� � ��������� ����� *)
  //if ShowDictForm(TfmDepT1, dm.dsrRej, RecDep, '����� ������� � ��������� �����', dmSelRecordReadOnly, nil)<>mrOk then SysUtils.Abort;
  //RejId := DictAncestor.vResult;
  //RejPsId := dDepT1.RejPsId;

  Go0;

  if DialogSemis <> nil then FreeAndNil(DialogSemis);
  try
    DialogSemis := TDialogSemis.Create(Self);
    SourceStorage := TStorage.Create;
    SourceStorage.ID := dmMain.taWhSemisDEPID.AsInteger;
    SourceStorage.Element.Code := Trim(dmMain.taWhSemisSEMISID.AsString);
    SourceStorage.Element.Operation := Trim(dm.RejOperId);
    SourceStorage.Element.UnitQuantity := dmMain.taWhSemisUQ.AsInteger;
    SourceStorage.Element.UnitWeight := dmMain.taWhSemisUW.AsInteger;

    TargetStorage := TStorage.Create;
    TargetStorage.ID := dmMain.taWOrderListJOBDEPID.AsInteger;
    TargetStorage.Element.Code := Trim(dmMain.taWhSemisSEMISID.AsString);
    TargetStorage.Element.Operation := Trim(dm.TransformOperId);
    TargetStorage.Element.UnitQuantity := dmMain.taWhSemisUQ.AsInteger;
    TargetStorage.Element.UnitWeight := dmMain.taWhSemisUW.AsInteger;

    if DialogSemis.Execute(SourceStorage, TargetStorage, smOut) then
    begin
      DialogSemis.DataSetTargetSummary.First;
      while not DialogSemis.DataSetTargetSummary.Eof do
      begin
        RejId := DialogSemis.DataSetTargetSummaryReasonID.AsInteger;
        RejPsId := DialogSemis.DataSetTargetSummaryOriginatorID.AsInteger;
        dmMain.SemisQ := DialogSemis.DataSetTargetSummaryQuantity.AsInteger;
        dmMain.SemisW := DialogSemis.DataSetTargetSummaryWeight.AsFloat;

        Go1;

        DialogSemis.DataSetTarget.SetRange([RejPsId, RejId], [RejPsId, RejId]);
        DialogSemis.DataSetTarget.First;

        while not DialogSemis.DataSetTarget.Eof do
        begin
          SpoilageDelta.StorageElementID := TargetStorage.Element.ID;
          SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
          SpoilageDelta.OperationID := DialogSemis.DataSetTargetOperationID.AsInteger;
          SpoilageDelta.Quantity := DialogSemis.DataSetTargetQuantity.AsInteger;
          SpoilageDelta.Weight := DialogSemis.DataSetTargetWeight.AsFloat;
          TDialogSemis.Delta(SpoilageDelta);

          SpoilageDelta.StorageElementID := SourceStorage.Element.ID;
          SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
          SpoilageDelta.OperationID := DialogSemis.DataSetTargetOperationID.AsInteger;
          SpoilageDelta.Quantity := - DialogSemis.DataSetTargetQuantity.AsInteger;
          SpoilageDelta.Weight := - DialogSemis.DataSetTargetWeight.AsFloat;
          TDialogSemis.Delta(SpoilageDelta);
          DialogSemis.DataSetTarget.Next;
        end;
        Go2;
        DialogSemis.DataSetTargetSummary.Next;
      end;
    end;
  finally
    if SourceStorage <> nil then FreeAndNil(SourceStorage);
    if TargetStorage <> nil then FreeAndNil(TargetStorage);
    if DialogSemis <> nil then FreeAndNil(DialogSemis);
  end;
end;


function TfmWOrderRej.FillTransformInfo : boolean;
const
  Rec : TRecName = (sId: 'SEMISID'; sName: 'NAME'; sTable: 'D_SEMIS'; sWhere: '');
var
  EndSemisGood : byte;
begin
  Result := False;
  if(dm.User.Transform_DepId = -2)then
    if FUseLog then
    begin
      fmLog.Add(Format(rsSelectPS, ['�������������']));
      eXit;
    end
    else raise EWarning.Create(Format(rsSelectPS, ['�������������']));
  if(dm.User.Transform_MolId = -2)then
    if FUseLog then
    begin
      fmLog.Add(Format(rsInternalError, ['111']));
      eXit;
    end
    else raise EInternal.Create(Format(rsInternalError, ['111']));
  // ��������� ��������� �� ������������ �� ��������
  if(dm.User.wWhId = -1)then
  begin
    if FUseLog then
    begin
      fmLog.Add(Format(rsIsNotMOL, [dm.User.FIO]));
      eXit;
    end
    else raise EInternal.Create(Format(rsInternalError, ['111']));
  end;
  FillChar(FTransformInfo, SizeOf(TTransformInfo), #0);
  with FTransformInfo do
  begin
    JobId := dm.User.Transform_MolId;
    // � ����� �������� ����������� ������ � �������
    if(dmMain.taWhSemisGOOD.AsInteger = 0) then // ������������ �� �������� �������
    begin
      if(dmMain.taWhSemisOPERID.AsString = dm.RejOperId)or
        (dmMain.taWhSemisOPERID.AsString = dm.NkOperId)or
        (dmMain.taWhSemisOPERID.AsString = dm.TransformOperId)
      then begin
        FromOperId := '"'+dmMain.taWhSemisOPERID.AsString+'"';
        FromOperName := dmMain.taWhSemisOPERNAME.AsString;
      end
      else begin
        FromOperId := 'Null';
        FromOperName := sNoOperation;
      end;
    end
    else begin
      if dmMain.taWhSemisOPERID.IsNull then FromOperId := 'Null'
      else FromOperId := '"'+dmMain.taWhSemisOPERID.AsString+'"';
      FromOperName := dmMain.taWhSemisOPERID.AsVariant;
    end;

    // ����� �����
    wOrderId := ExecSelectSQL('select max(wOrderId) from wOrder where DefOper="'+
            dm.TransformOperId+'" and JobDepId='+IntToStr(dm.User.Transform_DepId)+' and IsClose=0', dmMain.quTmp);
    // ��������� ���� �� ������������ � ������� ����� ������������� ���������
    dmMain.taSemisT1.Open;
    dmMain.taSemisT1.Last;
    case dmMain.taSemisT1.RecordCount of
       0 : if FUseLog then
           begin
             fmLog.Add(Format(rsInvalidTransform1, [dmMain.taWhSemisSEMISNAME.AsString]));
             eXit;
           end
           else raise EWarning.Create(Format(rsInvalidTransform1, [dmMain.taWhSemisSEMISNAME.AsString]));
       1 : EndSemis := dmMain.taSemisT1SEMISID.AsString;
       else if ShowDictForm(TfmSemisT1, dmMain.dsrSemisT1, Rec, '������������� -> ������',
               dmSelRecordReadOnly)=mrOk then EndSemis := DictAncestor.vResult
         else begin
           if FUseLog then fmLog.Add('�� ������ �������� ������������. ������������� ��������.');
           eXit;
         end;
    end;
    EndSemisName := ExecSelectSQL('select Semis from D_Semis where SemisId="'+EndSemis+'"', dmMain.quTmp);
    if FUseLog then fmLog.Add('������ �������� ������������: '+EndSemisName);
    EndSemisGood := ExecSelectSQL('select Good from D_Semis where SemisId="'+EndSemis+'"', dmMain.quTmp);
    // � ����� �������� ����������� �������
    if(EndSemisGood = 0)then RetFromOperId := '"'+dm.TransformOperId+'"'
    else RetFromOperId := FromOperId;

    if(VarIsNull(wOrderId)or(wOrderId=0))then
    begin
      if FUseLog then fmLog.Add('������� ����� �����...');
      with dmMain do
      begin // ������� �����
        wOrderId := dm.GetId(2);
        ExecSQL('insert into wOrder(WORDERID, DOCNO, DOCDATE, DEPID, MOLID, '+
           ' JOBDEPID, JOBFACE, DEFOPER, ISCLOSE) values('+
           IntToStr(wOrderId)+','+IntToStr(dm.GetId(7))+',"'+DateToStr(ToDay)+'",'+
           IntToStr(dm.User.wWhId)+','+IntToStr(dm.User.UserId)+','+
           IntToStr(dm.User.Transform_DepId)+','+IntToStr(dm.User.Transform_MolId)+',"'+
           dm.TransformOperId+'", 0)', quTmp);
      end;
      wOrderNo := ExecSelectSQL('select DocNo from wOrder where wOrderId='+IntToStr(wOrderId), dmMain.quTmp);
      if FUseLog then fmLog.Add('������ ����� �'+wOrderNo);
    end
    else begin
      wOrderNo := ExecSelectSQL('select DocNo from wOrder where wOrderId='+IntToStr(wOrderId), dmMain.quTmp);
      if FUseLog then fmLog.Add('������ ����� �'+wOrderNo);
    end;
  end;
  Result := True;
end;

procedure TfmWOrderRej.acAddSemisNewExecute(Sender: TObject);
begin
  if not TryInsert then Exit;
  FAddToNewInv := True;
  acAddSemis.Execute;
end;

procedure TfmWOrderRej.gridItemColumns7EditButtonClick(Sender: TObject; var Handled: Boolean);
begin
  acArt.Execute;
end;

procedure TfmWOrderRej.gridItemExit(Sender: TObject);
begin
  PostDataSet(DataSet);
end;

procedure TfmWOrderRej.gridItemGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
var
  i : integer;
begin
  case dmMain.taWOrderITTYPE.AsInteger of
    0,6 : if (dmMain.taWOrderE.AsInteger = -1) then BackGround := clRed
          else BackGround := clInfoBk;
    1,3,4,5,7 : if (dmMain.taWOrderE.AsInteger = -1) then BackGround := clGreen
                else Background := cl3DLight;
  end;
  if(not dmMain.taWOrderREF.IsNull) then Background := clMoneygreen;

  if(AnsiUpperCase(Column.FieldName)='RECNO')then
    if gridItem.SelectedRows.Find(Column.Field.DataSet.Bookmark, i) then Background:= clCream else Background:= clMoneygreen
  else if(Column.FieldName='ITDATE')and(Column.Field.AsDateTime>dm.EndDate) then BackGround := clRed
  else if(Column.FieldName='ITTYPE')then
    case dmMain.taWOrderITTYPE.AsInteger of
      0,6 : BackGround := clInfoBk;
      1,3,4,5,7 : Background := cl3DLight;
    end;
end;

procedure TfmWOrderRej.Access;
begin
  with stbrStatus.Panels[0] do
    if dm.IsAdm then Text := 'Adm'
    else if((dm.User.AccProd_d and 1) = 1)then Text := 'View'
    else Text := 'Full';
  if dm.IsAdm then eXit;
  if (dm.User.AccProd_d and 1)=1 then SetReadOnly(True)
  else begin // ��������� ����, ������������ ��� ��������� �� ��������
    if(dm.User.wWhId = -1)then
    begin
      ShowMessage('�� �� ���������� �� �� ����� �� ��������.'+#13#10+'�������� ����� ���������');
      SetReadOnly(True);
    end
    else SetReadOnly(False);
  end;
end;

procedure TfmWOrderRej.SetReadOnly(ReadOnly: boolean);
begin
  acAddSemisNew.Enabled := not ReadOnly;
  acAddSemis.Enabled := not ReadOnly;
  acDelSemis.Enabled := not ReadOnly;
  acSCopy.Enabled := not ReadOnly;
  acSPaste0.Enabled := not ReadOnly;
  acTransform0.Visible := not ReadOnly;
  acSPaste1.Enabled := not ReadOnly;
  acACopy.Enabled := not ReadOnly;
  acAPaste.Visible := not ReadOnly;
  gridItem.ReadOnly := ReadOnly;
  gridAEl.ReadOnly := ReadOnly;
  spitClose.Enabled := not ReadOnly;
  edNoDoc.Enabled := not ReadOnly;
  dtedDateDoc.Enabled := not ReadOnly;
  lcbxJobPS.Enabled := not ReadOnly;
  lcbxExec.Enabled := not ReadOnly;
  lcbxOper.Enabled := not ReadOnly;
  plExt.Visible := not ReadOnly;
  tbdcSemis.Visible := not ReadOnly;
  //spitStyle.Visible := not ReadOnly;
  tlbrT.Visible := not ReadOnly;
end;


procedure TfmWOrderRej.acExcelExecute(Sender: TObject);
var
  f : TFileName;
  c : string;
begin
  if dmMain.taWOrder.IsEmpty then eXit;
  c := '����� � '+dmMain.taWOrderDOCNO.AsString+' '+dmMain.taWOrderListJOBFACE.AsString;
  f := ExtractFileDir(ParamStr(0))+'\'+c+'.xls';
  GridToXLS(gridItem, f, c, True, True);
end;

procedure TfmWOrderRej.acMultiSelectExecute(Sender: TObject);
begin
  if(gridItem.AllowedSelections = [])then
    gridItem.AllowedSelections := gridItem.AllowedSelections + [gstRecordBookmarks]
  else begin
    gridItem.Selection.Clear;
    gridItem.AllowedSelections := [];
  end;
end;

procedure TfmWOrderRej.SetOPeriod;
var
  dt : Variant;
begin
  dm.PsBeginDate := StrToDate('01.01.2050');
  if(not dmMain.taWOrderListJOBDEPID.IsNull) then
  begin
    // ���������� ���� ��������� ��� ����� ��������
    dt := ExecSelectSQL('select max(a.DocDate) from Act a, AItem it '+
                        'where a.Id=it.ActId and '+
                        '      a.T = 1 and '+
                        '      a.IsClose=1 and '+
                        '      it.PsId = '+dmMain.taWOrderListJOBDEPID.AsString, dmMain.quTmp);
    if VarIsNull(dt)then dm.PsBeginDate := StrToDate(BeginDateByDefault) else dm.PsBeginDate := VarToDateTime(dt);
  end;

  if(dm.User.wWhId <> -1) then
  begin
    dt := ExecSelectSQL('select max(a.DocDate) from Act a, AItem it '+
                        'where a.Id=it.ActId and '+
                        '      a.T = 2 and '+
                        '      a.IsClose=1 and '+
                        '      a.DepId = '+IntToStr(dm.User.wWhId), dmMain.quTmp);
    if not VarIsNull(dt)then
    begin
      if dm.PsBeginDate < VarToDateTime(dt) then
      begin
        dm.PsBeginDate := VarToDateTime(dt);
        dm.PsTypeBeginDate := 1;
      end
      else dm.PsTypeBeginDate := 0;
    end
    else dm.PsTypeBeginDate := 0;
  end;
  lbDate.Caption := '������: '+DateToStr(dm.PsBeginDate);
end;

procedure TfmWOrderRej.acSPaste0Update(Sender: TObject);
begin
  (Sender as TAction).Enabled := ExecSelectSQL('select count(*) from B_WInv where UserId='+IntToStr(dm.User.UserId), dmMain.quTmp)<>0;;
end;

procedure TfmWOrderRej.acSPaste1Update(Sender: TObject);
begin
  acSPaste1.Enabled := acSPaste0.Enabled;
end;

procedure TfmWOrderRej.acShowIDExecute(Sender: TObject);
begin
  gridItem.FieldColumns['INVID'].Visible    := not gridItem.FieldColumns['INVID'].Visible;
  gridItem.FieldColumns['WOITEMID'].Visible := not gridItem.FieldColumns['WOITEMID'].Visible;
  gridItem.FieldColumns['WORDERID'].Visible := not gridItem.FieldColumns['WORDERID'].Visible;
  gridItem.FieldColumns['ITDATE'].Visible :=  not gridItem.FieldColumns['WORDERID'].Visible;
  gridItem.FieldColumns['REJREF'].Visible := not gridItem.FieldColumns['REJREF'].Visible;
  gridItem.FieldColumns['REJ'].Visible := not gridItem.FieldColumns['REJ'].Visible; 

  gridSemis.FieldColumns['GOOD'].Visible := not gridSemis.FieldColumns['GOOD'].Visible;
  gridSemis.FieldColumns['SEMISID'].Visible := not gridSemis.FieldColumns['SEMISID'].Visible;
  gridSemis.FieldColumns['OPERID'].Visible := not gridSemis.FieldColumns['OPERID'].Visible;
end;

procedure TfmWOrderRej.SetMode;
begin
  case dmMain.WorderMode of
    wmRej : begin
      ac0.Visible := True;
      ac1.Visible := True;
      ac3.Visible := True;
      ac5.Visible := True;
      ac6.Visible := False;
      ac7.Visible := False;
      acDelSemis.Visible := True;
      lcbxJobPS.Visible := False;
      lcbxExec.Visible := False;
      lcbxOper.Visible := False;
      cmbxMat.Enabled := True;
      //gridItem.ReadOnly := True;
      acTransform0.Visible := True;
    end;
    wmWorder : begin
    end;
  end;

end;

procedure TfmWOrderRej.ac0Execute(Sender: TObject);
begin
  ItType := 0;
end;

procedure TfmWOrderRej.ac6Execute(Sender: TObject);
begin
  ItType := 6;
end;

procedure TfmWOrderRej.ac1Execute(Sender: TObject);
begin
  ItType := 1;
end;

procedure TfmWOrderRej.ac3Execute(Sender: TObject);
begin
  ItType := 3;
end;

procedure TfmWOrderRej.ac5Execute(Sender: TObject);
begin
  ItType := 5;
end;

procedure TfmWOrderRej.ac7Execute(Sender: TObject);
begin
  ItType := 7;
end;

procedure TfmWOrderRej.acArtUpdate(Sender: TObject);
begin
  acArt.Enabled := not DataSet.IsEmpty;
end;

function TfmWOrderRej.IsActionMayBe: boolean;
begin
  Result := not(dmMain.taWOrderITDATE.AsDateTime <= dm.PsBeginDate);
end;

procedure TfmWOrderRej.acAPasteUpdate(Sender: TObject);
begin
  acAPaste.Enabled := (ExecSelectSQL('select count(*) from B_AInv where UserId='+
    IntToStr(dm.User.UserId), dmMain.quTmp)<>0) and
       IsActionMayBe;
end;

procedure TfmWOrderRej.acACopyUpdate(Sender: TObject);
begin
  acACopy.Enabled := not dmMain.taAssortEl.IsEmpty;
end;

procedure TfmWOrderRej.lcbxExecEnter(Sender: TObject);
begin
  if dmMain.taWOrderListJOBDEPID.IsNull then
  begin
    ActiveControl := lcbxJobPS;
    raise Exception.Create(Format(rsSelectPS, ['']));
  end
  else begin
    ReOpenDataSet(dmMain.taPSMol);
    if not (dmMain.taWOrderList.State in [dsInsert, dsEdit]) then dmMain.taWOrderList.Edit;
    dmMain.taWOrderListJOBID.AsInteger := dmMain.taPSMolMOLID.AsInteger;
  end;
  SetOPeriod;
end;

procedure TfmWOrderRej.lcbxJobPSCloseUp(Sender: TObject; Accept: Boolean);
begin
  ActiveControl := lcbxExec;
  ActiveControl := lcbxOper;
  ActiveControl := lcbxJobPS;
end;

procedure TfmWOrderRej.lcbxJobPSKeyValueChanged(Sender: TObject);
begin
  ActiveControl := lcbxExec;
  ActiveControl := lcbxOper;
  ActiveControl := lcbxJobPS;
end;

procedure TfmWOrderRej.gridAElGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if Column.FieldName = 'RecNo' then Background := clMoneyGreen;
end;

procedure TfmWOrderRej.FormResize(Sender: TObject);
begin
  spitExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmWOrderRej.acPrintExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taWOrder,  dmMain.taWOrderList]);
  if(dmMain.taWOrderITTYPE.AsInteger in [0,6])then
    dmPrint.PrintDocumentA(dkInv16In, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]))
  else
    dmPrint.PrintDocumentA(dkInv16Out, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]));
end;

procedure TfmWOrderRej.acPrintInvWithAssortExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taWOrder, dmMain.taWOrderList]);
  if(dmMain.taWOrderITTYPE.AsInteger in [0,6])then
    dmPrint.PrintDocumentA(dkInv16InAssort, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]))
  else
    dmPrint.PrintDocumentA(dkInv16OutAssort, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]));
end;

procedure TfmWOrderRej.acPrintPreviewExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taWOrder, dmMain.taWOrderList]);
  if(dmMain.taWOrderITTYPE.AsInteger in [0,6])then
    dmPrint.PrintDocumentA(dkInv16In_Preview, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]))
  else
    dmPrint.PrintDocumentA(dkInv16Out_Preview, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]));
end;

procedure TfmWOrderRej.acActiveWHExecute(Sender: TObject);
begin
  ActiveControl := gridSemis;
end;

procedure TfmWOrderRej.acDelSemisUpdate(Sender: TObject);
begin
  acDelSemis.Enabled := not dmMain.taWOrder.IsEmpty;
end;

procedure TfmWOrderRej.acTransform0Update(Sender: TObject);
begin
  acTransform0.Enabled := dmMain.taWhSemis.Active and (not dmMain.taWhSemis.IsEmpty) and
       (ItType = 0);
end;

procedure TfmWOrderRej.acAddSemisUpdate(Sender: TObject);
begin
  acAddSemis.Enabled := not dmMain.taWhSemis.IsEmpty;
end;

procedure TfmWOrderRej.acTransformDelExecute(Sender: TObject);
var
  isLocateWh : boolean;
  DepId, SemisId, OperId : Variant;
  Id, Ref : Variant;


  Source, Target: TStorage;
  SpoilageDelta: TSpoilageDelta;
  ResultQuantity: Integer;
  ResultWeight: Double;
  TotalResultQuantity: Integer;
  TotalResultWeight: Double;

begin
  (*
    �������� ���������� � ��������� �������: 1.������, 2. �������
    ������ ��� � ������ ����� ���� �������� �����������
  *)
  with dmMain do
  begin
    //if not _ShowEditorQW(taWOrderQ.AsInteger, taWOrderW.AsFloat, chbxW.Checked) then eXit;

    case taWOrderITTYPE.AsInteger of
      0 : begin
        Id  := taWOrderWOITEMID.AsInteger;
        if taWOrderREJREF.IsNull then raise EInternal.Create(Format(rsInternalError, ['142']));
        Ref := taWOrderREJREF.AsInteger;
        if not taWOrder.Locate('WOITEMID', taWOrderREJREF.AsInteger, []) then raise EInternal.Create(Format(rsInternalError, ['143']));
      end;
      3 : begin
        Ref := ExecSelectSQL('select WOItemId from WOItem where RejRef = '+taWOrderWOITEMID.AsString, quTmp);
        if VarIsNull(Ref)or(Ref = 0)then raise EInternal.Create(Format(rsInternalError, ['144']));
        if not taWOrder.Locate('WOITEMID', Ref, [])then raise EInternal.Create(Format(rsInternalError, ['146']));
        Id := taWOrderWOITEMID.AsInteger;
      end;
    end;

    // ������� �� ������
    taWOrder.Locate('WOITEMID', Id, []);
    //{
    Source := TStorage.Create;
    Source.ID := dm.User.wWhId;
    Source.Connected := True;
    Source.Element.Code := dmMain.taWOrderSEMIS.AsString;
    Source.Element.Operation := dm.RejOperId;
    Source.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
    Source.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
    Source.Element.Connected := True;

    Target := TStorage.Create;
    Target.ID := dmMain.taWOrderJOBDEPID.AsInteger;
    Target.Element.Code := dmMain.taWOrderSEMIS.AsString;
    Target.Element.Operation := dmMain.taWOrderOPERID.AsString;
    Target.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
    Target.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
    Target.Element.Connected := True;

    if DialogSemis <> nil then FreeAndNil(DialogSemis);
    DialogSemis := TDialogSemis.Create(Self);
    DialogSemis.All := True;
    if DialogSemis.Execute(Source, Target, smIn, dmMain.taWOrderWOITEMID.AsInteger) then
    begin
       DialogSemis.DataSetSource.First;
       TotalResultQuantity := 0;
       TotalResultWeight := 0;
       while not DialogSemis.DataSetSource.Eof do
       begin
         ResultQuantity := DialogSemis.DataSetSourceQuantity.AsInteger;
         ResultWeight := DialogSemis.DataSetSourceWeight.AsFloat;
         TotalResultQuantity := TotalResultQuantity + ResultQuantity;
         TotalResultWeight := TotalResultWeight + ResultWeight;

         SpoilageDelta.StorageElementID := Source.Element.ID;
         SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
         SpoilageDelta.OperationID := DialogSemis.DataSetSourceOperationID.AsInteger;
         SpoilageDelta.Quantity := ResultQuantity;
         SpoilageDelta.Weight := ResultWeight;

         TDialogSemis.Delta(SpoilageDelta);

         SpoilageDelta.StorageElementID := Target.Element.ID;
         SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
         SpoilageDelta.OperationID := DialogSemis.DataSetSourceOperationID.AsInteger;
         SpoilageDelta.Quantity := -ResultQuantity;
         SpoilageDelta.Weight := -ResultWeight;
         TDialogSemis.Delta(SpoilageDelta);

          DialogSemis.DataSetSource.Next;
       end;
       dmMain.SemisQ := TotalResultQuantity;
       dmMain.SemisW := TotalResultWeight;
    end;
    Source.Free;
    Target.Free;
    FreeAndNil(DialogSemis);

    //}
    if taWOrderFROMOPERID.IsNull then isLocateWh := taWhSemis.Locate('SEMISID', taWOrderSEMIS.AsString, [])
    else isLocateWh  := taWhSemis.Locate('SEMISID;OPERID', VarArrayOf([taWOrderSEMIS.AsString, taWOrderFROMOPERID.AsString]), []);    if not isLocateWh then
    begin
      SemisId := taWOrderSEMIS.AsString;
      OperId := taWOrderFROMOPERID.AsString;
      case ItType of
         0, 6    : DepId := taWOrderDEPID.AsInteger;
         1,3,5,7 : DepId := taWOrderJOBDEPID.AsInteger;
         else raise EInternal.Create(Format(rsInternalError, ['101']));
       end;
    end;
    dmMain.SemisQ := dmMain.taWOrderQ.AsInteger;
    dmMain.SemisW := dmMain.taWOrderW.AsFloat;
    taWOrder.Delete;
    // ������� �� �������
    taWOrder.Locate('WOITEMID', Ref, []);
    taWOrder.Delete;
    // �������� ������� ������
    if not isLocateWh  then
    begin
      taWhSemis.Append;
      taWhSemisDEPID.AsInteger := DepId;
      taWhSemisOPERID.AsString := OperId;
      taWhSemisSEMISID.AsString := SemisId;
      taWhSemisQ.AsInteger := 0;
      taWhSemisW.AsInteger := 0;
      taWhSemis.Post;
    end;
    taWhSemis.Refresh;
  end;
end;



procedure TfmWOrderRej.acTransformAssortExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'SEMISID'; sName: 'NAME'; sTable: 'D_SEMIS'; sWhere: '');
  RecDep : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_REJ'; sWhere: '');
var
  Id, RejId, RejRef : integer;
  RejPsId : Variant;
begin
  if not dmMain._ShowEditorQW(dmMain.taWhSemisQ.AsInteger, dmMain.taWhSemisW.AsFloat, chbxW.Checked) then eXit;
                (* ������ �� �������� ������������� *)
  (* �������� ������� � ��������� ����� *)
  if ShowDictForm(TfmDepT1, dm.dsrRej, RecDep, '����� ������� � ��������� �����', dmSelRecordReadOnly, nil)<>mrOk then SysUtils.Abort;
  RejId := DictAncestor.vResult;
  RejPsId := dDepT1.RejPsId;
  with dmMain do
  begin                             
    InvMode := 0;     
    taWOrder.Append;
    Id := taWOrderWOITEMID.AsInteger;
    taWOrderITTYPE.AsInteger := 0;
    taWOrderSEMIS.AsString := taWhSemisSEMISID.AsString;
    taWOrderOPERID.AsString := dm.ModifyAssortOperId;
    taWOrderFROMOPERID.AsVariant := dmMain.taWhSemisOPERID.AsVariant;
    taWOrderDEPNAME.AsString := dm.User.wWhName;
    taWOrderJOBDEPNAME.AsString := taWOrderListJOBDEPNAME.AsString;
    taWOrderQ.AsInteger := SemisQ;
    taWOrderW.AsFloat := SemisW;
    taWOrderREJID.AsInteger := RejId;
    taWOrderREJNAME.AsString := dm.taRejNAME.AsString;
    taWOrderREJPSID.AsVariant := RejPsId;
    taWOrder.Post;
    taWhSemis.Refresh;

    taWOrder.Locate('WOITEMID', Id, []);
    acArt.Execute;
                   (* ������ ������� ������ �� �������� dm.ModifyAssortOperId *)
    dmMain.InvMode := 0;
    dmMain.taWOrder.Append;
    RejRef := taWOrderWOITEMID.AsInteger;
    taWOrderITTYPE.AsInteger := 3;
    taWOrderSEMIS.AsString := taWhSemisSEMISID.AsString;
    taWOrderOPERID.AsString := dm.ModifyAssortOperId;
    taWOrderFROMOPERID.AsVariant := dmMain.taWhSemisOPERID.AsVariant;
    taWOrderDEPNAME.AsString := dm.User.wWhName;
    taWOrderJOBDEPNAME.AsString := taWOrderListJOBDEPNAME.AsString;
    taWOrderQ.AsInteger := SemisQ;
    taWOrderW.AsFloat := SemisW;
    taWOrder.Post;
    taWhSemis.Refresh;
    if not taWOrder.Locate('WOITEMID', Id, []) then raise EInternal.Create('!');
    taWOrder.Edit;
    taWOrderREJREF.AsInteger := RejRef;
    taWOrder.Post;
  end;
end;

procedure TfmWOrderRej.acTransformAssortUpdate(Sender: TObject);
begin
  acTransformAssort.Enabled := dmMain.taWhSemis.Active and (not dmMain.taWhSemis.IsEmpty) and
       (dmMain.taWhSemisGOOD.AsInteger = 1) and (ItType = 0);
end;

function TfmWOrderRej.TryDelete: Boolean;
var
  s: string;
  DepartmentID: Integer;
begin
  Result := True;
  if Result and (dmMain.taWOrderListISCLOSE.AsInteger = 1) then
  begin
    Application.MessageBox(PChar(Format(rsWOrderClose, [dmMain.taWOrderListDOCNO.AsString])),'������', MB_OK);
    Result := False;
  end;
  if Result and (dmMain.taWOrderITDATE.AsDateTime <= dm.PsBeginDate) then
  begin
    Result := False;
    case dm.PsTypeBeginDate of
      0: s := '�� �������� ��������������';
      1: s := '�� ������������ ���������';
    end;
    Application.MessageBox(PChar(Format(rsPeriodClose, [dmMain.taWOrderJOBDEPNAME.AsString, DateToStr(dm.PsBeginDate), s])),'������', MB_OK);
  end;
  if Result and not dm.IsAdm then
  begin
    DepartmentID := dmMain.taWOrderDEPID.AsInteger;
    if DepartmentID <> dm.User.wWhId then
    begin
      Result := False;
      Application.MessageBox(PChar(Format(rsAccessDenied, [dm.User.FIO, '��� ������ � '+dmmain.taWOrderDEPNAME.AsString])),'������', MB_OK);
    end;
  end;
end;

function TfmWOrderRej.TryInsert: Boolean;
var
  s: string;
  DepartmentID: Integer;
begin
  Result := True;
  if Result and (dmMain.taWOrderListISCLOSE.AsInteger = 1) then
  begin
    Application.MessageBox(PChar(Format(rsWOrderClose, [dmMain.taWOrderListDOCNO.AsString])),'������', MB_OK);
    Result := False;
  end;
  if Result and (dmMain.taWOrderListDOCDATE.AsDateTime <= dm.PsBeginDate) then
  begin
    Result := False;
    case dm.PsTypeBeginDate of
      0: s := '�� �������� ��������������';
      1: s := '�� ������������ ���������';
    end;
    Application.MessageBox(PChar(Format(rsPeriodClose, [dmMain.taWOrderJOBDEPNAME.AsString, DateToStr(dm.PsBeginDate), s])),'������', MB_OK);
  end;
end;

function TfmWOrderRej.TryUpdate: Boolean;
var
  s: string;
  DepartmentID: Integer;
begin
  Result := True;
  if Result and (dmMain.taWOrderListISCLOSE.AsInteger = 1) then
  begin
    Application.MessageBox(PChar(Format(rsWOrderClose, [dmMain.taWOrderListDOCNO.AsString])),'������', MB_OK);
    Result := False;
  end;

  if Result and (dmMain.taWOrderITDATE.AsDateTime <= dm.PsBeginDate) then
  begin
    Result := False;
    case dm.PsTypeBeginDate of
      0: s := '�� �������� ��������������';
      1: s := '�� ������������ ���������';
    end;
    Application.MessageBox(PChar(Format(rsPeriodClose, [dmMain.taWOrderJOBDEPNAME.AsString, DateToStr(dm.PsBeginDate), s])),'������', MB_OK);
  end;

  if Result and not dm.IsAdm then
  begin
    DepartmentID := dmMain.taWOrderDEPID.AsInteger;
    if DepartmentID <> dm.User.wWhId then
    begin
      Result := False;
      Application.MessageBox(PChar(Format(rsAccessDenied, [dm.User.FIO, '��� ������ � '+dmmain.taWOrderDEPNAME.AsString])),'������', MB_OK);
    end;
  end;
end;


end.
