unit eCasePS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, CheckLst, Buttons, ExtCtrls, TB2Item,
  ActnList, TB2Dock, TB2Toolbar;

type
  TfmCasePS = class(TfmEditor)
    chlbxPS: TCheckListBox;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    acEvent: TActionList;
    acCheckAll: TAction;
    acUnCheckAll: TAction;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure acCheckAllExecute(Sender: TObject);
    procedure acUnCheckAllExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCasePS: TfmCasePS;

implementation

uses MainData, dbUtil, dbTree, DictData;

{$R *.dfm}

procedure TfmCasePS.FormCreate(Sender: TObject);
var
  nd : TNodeData;
begin
  chlbxPS.Items.BeginUpdate;
  try
    chlbxPS.Items.Clear;
    OpenDataSet(dmMain.taPsDep);
    while not dmMain.taPsDep.Eof do
    begin
      nd := TNodeData.Create;
      nd.Code := dmMain.taPsDepDEPID.AsInteger;
      chlbxPS.Items.AddObject(dmMain.taPsDepNAME.AsString, nd);
      dmMain.taPsDep.Next;
    end;
  finally
    chlbxPS.Items.EndUpdate;
  end;
end;

procedure TfmCasePS.acCheckAllExecute(Sender: TObject);
var
  i : integer;
begin
  for i:=0 to Pred(chlbxPS.Items.Count) do chlbxPS.Checked[i] := True;
end;

procedure TfmCasePS.acUnCheckAllExecute(Sender: TObject);
var
  i : integer;
begin
  for i:=0 to Pred(chlbxPS.Items.Count) do chlbxPS.Checked[i] := False;
end;

procedure TfmCasePS.FormClose(Sender: TObject; var Action: TCloseAction);
var
  b : boolean;
  i : integer;
begin
  if(ModalResult = mrOk)then
  begin
    b := True;
    // ��������� ���� ��������� ����� ��������
    for i:=0 to Pred(chlbxPS.Items.Count) do
      if chlbxPS.Checked[i] then
      begin
        b := False;
        break;
      end;
    if b then raise Exception.Create('�� ������� �� ������ ����� ��������');
    // ������� ��� �� ������� TMP_D_J_PS
    ExecSQL('delete from TMP_D_J_PS where USERID='+IntToStr(dm.User.UserId), dm.quTmp);
    for i:=0 to Pred(chlbxPS.Items.Count) do
      if chlbxPS.Checked[i] then
        ExecSQL('insert into TMP_D_J_PS(ID, USERID, DEPID) '+
          ' values(gen_id(gen_tmp_d_j_ps_id, 1), '+IntToStr(dm.User.UserId)+','+
           IntToStr(TNodeData(chlbxPS.Items.Objects[i]).Code)+')', dm.quTmp);

  end;
  inherited;
end;

end.
