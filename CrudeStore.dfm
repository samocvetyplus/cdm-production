object fmCrudeStore: TfmCrudeStore
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1053#1072#1083#1080#1095#1080#1077' '#1084#1077#1090#1072#1083#1083#1072' '#1074' '
  ClientHeight = 569
  ClientWidth = 984
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxGrid
    Left = 0
    Top = 0
    Width = 984
    Height = 569
    Align = alClient
    PopupMenu = GridPopupMenu
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    object View: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsCrudeStore
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          Column = ViewTOTALWEIGHT
        end
        item
          Format = '0.000'
          Kind = skSum
          Position = spFooter
          Column = ViewTOTALWEIGHTPURE
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '0.000'
          Kind = skSum
          Column = ViewTOTALWEIGHT
        end
        item
          Format = '0.000'
          Kind = skSum
          Column = ViewTOTALWEIGHTPURE
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103'>'
      OptionsView.ExpandButtonsForEmptyDetails = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfAlwaysVisible
      OptionsView.BandHeaderLineCount = 2
      Styles.Group = cxStyle3
      Styles.Header = cxStyle2
      Styles.BandHeader = cxStyle1
      Bands = <
        item
          Caption = #1044#1074#1080#1078#1077#1085#1080#1103
          Width = 448
        end
        item
          Caption = #1054#1090#1082#1088#1099#1090#1099#1077' '#1085#1072#1082#1083#1072#1076#1085#1099#1077
          Width = 498
        end>
      object ViewMOVEMENTTYPENAME: TcxGridDBBandedColumn
        DataBinding.FieldName = 'MOVEMENT$TYPE$NAME'
        Visible = False
        GroupIndex = 0
        SortIndex = 0
        SortOrder = soAscending
        Width = 139
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
        IsCaptionAssigned = True
      end
      object ViewMOVEMENTSUBTYPENAME: TcxGridDBBandedColumn
        Caption = #1042#1080#1076' '#1076#1074#1080#1078#1077#1085#1080#1103
        DataBinding.FieldName = 'MOVEMENT$SUB$TYPE$NAME'
        HeaderAlignmentHorz = taCenter
        Width = 182
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewTOTALWEIGHT: TcxGridDBBandedColumn
        Caption = #1042#1077#1089
        DataBinding.FieldName = 'TOTAL$WEIGHT'
        HeaderAlignmentHorz = taCenter
        Width = 121
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object ViewTOTALWEIGHTPURE: TcxGridDBBandedColumn
        Caption = #1042#1077#1089
        DataBinding.FieldName = 'TOTAL$WEIGHT$PURE'
        HeaderAlignmentHorz = taCenter
        Width = 128
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object ViewOPENEDINVOICECOUNT: TcxGridDBBandedColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'OPENED$INVOICE$COUNT'
        HeaderAlignmentHorz = taCenter
        Width = 77
        Position.BandIndex = 1
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object ViewOPENEDINVOICENUMBERS: TcxGridDBBandedColumn
        Caption = #1053#1086#1084#1077#1088#1072
        DataBinding.FieldName = 'OPENED$INVOICE$NUMBERS'
        HeaderAlignmentHorz = taCenter
        Width = 131
        Position.BandIndex = 1
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object ViewOPENEDINVOICEDATES: TcxGridDBBandedColumn
        Caption = #1044#1072#1090#1099
        DataBinding.FieldName = 'OPENED$INVOICE$DATES'
        HeaderAlignmentHorz = taCenter
        Width = 290
        Position.BandIndex = 1
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
    end
    object Level: TcxGridLevel
      GridView = View
    end
  end
  object taCrudeStore: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  movement$type$name,'
      '  movement$sub$type$name,'
      '  total$weight,'
      '  opened$invoice$count,'
      '  opened$invoice$numbers,'
      '  opened$invoice$dates'
      'from Material$Residue(:Material$ID, :Current$Date)')
    BeforeOpen = taCrudeStoreBeforeOpen
    OnCalcFields = taCrudeStoreCalcFields
    Transaction = dm.tr
    Database = dm.db
    Left = 1168
    Top = 488
    object taCrudeStoreMOVEMENTTYPENAME: TFIBStringField
      FieldName = 'MOVEMENT$TYPE$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object taCrudeStoreMOVEMENTSUBTYPENAME: TFIBStringField
      FieldName = 'MOVEMENT$SUB$TYPE$NAME'
      Size = 128
      EmptyStrToNull = True
    end
    object taCrudeStoreTOTALWEIGHT: TFIBBCDField
      FieldName = 'TOTAL$WEIGHT'
      DisplayFormat = '0.000'
      Size = 3
      RoundByScale = True
    end
    object taCrudeStoreOPENEDINVOICECOUNT: TFIBSmallIntField
      FieldName = 'OPENED$INVOICE$COUNT'
    end
    object taCrudeStoreOPENEDINVOICENUMBERS: TFIBStringField
      FieldName = 'OPENED$INVOICE$NUMBERS'
      Size = 128
      EmptyStrToNull = True
    end
    object taCrudeStoreOPENEDINVOICEDATES: TFIBStringField
      FieldName = 'OPENED$INVOICE$DATES'
      Size = 128
      EmptyStrToNull = True
    end
    object taCrudeStoreTOTALWEIGHTPURE: TFIBBCDField
      FieldKind = fkCalculated
      FieldName = 'TOTAL$WEIGHT$PURE'
      DisplayFormat = '0.000'
      RoundByScale = True
      Calculated = True
    end
  end
  object dsCrudeStore: TDataSource
    DataSet = taCrudeStore
    Left = 1168
    Top = 520
  end
  object GridPopupMenu: TPopupMenu
    Left = 768
    Top = 264
    object N1: TMenuItem
      Caption = #1055#1077#1095#1072#1090#1100
      ShortCut = 112
      OnClick = N1Click
    end
  end
  object dxPrinter: TdxComponentPrinter
    CurrentLink = dxPrinterLink
    PreviewOptions.Caption = #1055#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1099#1081' '#1087#1088#1086#1089#1084#1086#1090#1088
    Version = 0
    Left = 768
    Top = 304
    object dxPrinterLink: TdxGridReportLink
      Active = True
      Component = Grid
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42584.558776504630000000
      ShrinkToPageWidth = True
      OptionsExpanding.ExpandGroupRows = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.BandHeaders = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      BuiltInReportLink = True
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -15
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Arial Black'
      Font.Style = [fsBold]
      TextColor = clBlack
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svFont, svTextColor]
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Arial Black'
      Font.Style = []
      TextColor = clHotLight
    end
  end
end
