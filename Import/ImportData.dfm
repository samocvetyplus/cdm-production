object dmImportData: TdmImportData
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 121
  Width = 196
  object Producers: TpFIBDataSet
    SelectSQL.Strings = (
      '')
    Transaction = dm.tr
    Database = dm.db
    Left = 120
    Top = 8
    object ProducersCOMPANYID: TFIBIntegerField
      FieldName = 'COMPANY$ID'
    end
    object ProducersKEYWORD: TFIBStringField
      FieldName = 'KEYWORD'
      Size = 32
      EmptyStrToNull = True
    end
    object ProducersHEADERPARAMS: TFIBStringField
      FieldName = 'HEADER$PARAMS'
      Size = 64
      EmptyStrToNull = True
    end
    object ProducersITEMPARAMS: TFIBStringField
      FieldName = 'ITEM$PARAMS'
      Size = 64
      EmptyStrToNull = True
    end
    object ProducersHEADERDELIMITER: TFIBStringField
      FieldName = 'HEADER$DELIMITER'
      Size = 2
      EmptyStrToNull = True
    end
    object ProducersITEMDELIMITER: TFIBStringField
      FieldName = 'ITEM$DELIMITER'
      Size = 2
      EmptyStrToNull = True
    end
  end
  object Invoice: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'select'
      '  InvID'
      'from'
      
        '  Import$Foreign$Invoice(:DepID, :FromDepID, :UserID, :ProducerI' +
        'D)')
    BeforeExecute = InvoiceBeforeExecute
    Left = 24
    Top = 8
  end
  object InvoiceItems: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    Left = 24
    Top = 64
  end
end
