unit RequestList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor,  TB2Item, ActnList, Menus,
  ImgList, Grids, DBGridEh,  StdCtrls, ExtCtrls, ComCtrls,
  TB2Dock, TB2Toolbar, TB2ExtItems, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmRequestList = class(TfmListAncestor)
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBSubmenuItem1: TTBSubmenuItem;
    TBItem5: TTBItem;
    acCheckAll: TAction;
    acUncheckAll: TAction;
    acInverse: TAction;
    TBItem6: TTBItem;
    TBItem7: TTBItem;
    TBEditItem1: TTBEditItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem8: TTBItem;
    acPrintRequestSummary: TAction;
    procedure acAddDocUpdate(Sender: TObject);
    procedure acDelDocUpdate(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acCheckAllExecute(Sender: TObject);
    procedure acUncheckAllExecute(Sender: TObject);
    procedure acInverseExecute(Sender: TObject);
    procedure TBEditItem1AcceptText(Sender: TObject; var NewText: String;
      var Accept: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure acPrintRequestSummaryExecute(Sender: TObject);
  end;

var
  fmRequestList: TfmRequestList;

implementation

uses Data, Request, DocAncestor, DictData, dbUtil, MsgDialog, InvData,
  ProductionConsts, PrintData;

{$R *.dfm}

procedure TfmRequestList.acAddDocUpdate(Sender: TObject);
begin
  acAddDoc.Enabled := False;
end;

procedure TfmRequestList.acDelDocUpdate(Sender: TObject);
begin
  acDelDoc.Enabled := dm.IsAdm;
end;

procedure TfmRequestList.acEditDocExecute(Sender: TObject);
begin
  inherited;
  with dmData do
  begin    
    taRequestList.Edit;
    ShowDocForm(TfmRequest, taRequestList, taRequestListINVID.AsInteger, dm.quTmp);
    taRequestList.Transaction.CommitRetaining;
    RefreshDataSet(taRequestList);
  end;
end;

procedure TfmRequestList.acCheckAllExecute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  PostDataSet(DataSet);
  Bookmark := DataSet.GetBookmark;
  DataSet.DisableScrollEvents;
  DataSet.DisableControls;
  try
    DataSet.First;
    while not DataSet.Eof do
    begin
      DataSet.Edit;
      DataSet.FieldByName('ANALIZ').AsInteger := 1;
      DataSet.Post;
      DataSet.Next;
    end;
  finally
    DataSet.GotoBookmark(Bookmark);
    DataSet.EnableScrollEvents;
    DataSet.EnableControls;
  end;
end;

procedure TfmRequestList.acUncheckAllExecute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  PostDataSet(DataSet);
  Bookmark := DataSet.GetBookmark;
  DataSet.DisableScrollEvents;
  DataSet.DisableControls;
  try
    DataSet.First;
    while not DataSet.Eof do
    begin
      DataSet.Edit;
      DataSet.FieldByName('ANALIZ').AsInteger := 0;
      DataSet.Post;
      DataSet.Next;
    end;
  finally
    DataSet.GotoBookmark(Bookmark);
    DataSet.EnableScrollEvents;
    DataSet.EnableControls;
  end;
end;

procedure TfmRequestList.acInverseExecute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  PostDataSet(DataSet);
  Bookmark := DataSet.GetBookmark;
  DataSet.DisableScrollEvents;
  DataSet.DisableControls;
  try
    DataSet.First;
    while not DataSet.Eof do
    begin
      DataSet.Edit;
      if DataSet.FieldByName('ANALIZ').AsInteger = 1
      then DataSet.FieldByName('ANALIZ').AsInteger := 0
      else DataSet.FieldByName('ANALIZ').AsInteger := 1;
      DataSet.Post;
      DataSet.Next;
    end;
  finally
    DataSet.GotoBookmark(Bookmark);
    DataSet.EnableScrollEvents;
    DataSet.EnableControls;
  end;
end;

procedure TfmRequestList.acPrintRequestSummaryExecute(Sender: TObject);
begin
  inherited;
  Screen.Cursor := crSQLWait;
  with dmPrint do
    begin
      try
        taPrintRequestSummary.Open;
      finally
        Screen.Cursor := crDefault;
      end;
      FDocID := 104;
      taDoc.Open;
      frReport.DataSet := frPrintRequestSummary;
      frReport.LoadFromDB(taDoc, FDocID);
      dm.DocPreview := dm.GetDocPreview;
      if dm.DocPreview = pvDesign then
        frReport.DesignReport
      else
        frReport.ShowReport;
      taDoc.Close;
      taPrintRequestSummary.Close;
    end;

end;

procedure TfmRequestList.TBEditItem1AcceptText(Sender: TObject;
  var NewText: String; var Accept: Boolean);
var
  Bookmark : Pointer;
  Date : TDateTime;
begin
  Accept := False;
  try
    Date := StrToDate(NewText);
  except
    on E:EConvertError do
    begin
      MessageDialogA(Format(rsInvalidDate, [NewText]), mtError);
      eXit;
    end;
  end;
  PostDataSet(DataSet);
  Bookmark := DataSet.GetBookmark;
  DataSet.DisableScrollEvents;
  DataSet.DisableControls;
  try
    DataSet.First;
    while not DataSet.Eof do
    begin
      DataSet.Edit;
      if not DataSet.FieldByName('AED').IsNull and
        (DataSet.FieldByName('AED').AsDateTime <= Date)
      then DataSet.FieldByName('ANALIZ').AsInteger := 1
      else DataSet.FieldByName('ANALIZ').AsInteger := 0;
      DataSet.Post;
      DataSet.Next;
    end;
  finally
    DataSet.GotoBookmark(Bookmark);
    DataSet.EnableScrollEvents;
    DataSet.EnableControls;
  end;
  Accept := True;
end;

procedure TfmRequestList.FormCreate(Sender: TObject);
begin
  dmInv.ITYpe := 23;
  gridDocList.DataSource := dmData.dsrRequestList;     //mfk
  inherited;
  Self.Resize;
end;

procedure TfmRequestList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(Column.FieldName = 'DOCNO')then
    if not dmData.taRequestListCOLOR.IsNull then Background := dmData.taRequestListCOLOR.AsInteger;
  if(Column.FieldName = 'SUPCODE')then
    if not dmData.taRequestListSUPCOLOR.IsNull then Background := dmData.taRequestListSUPCOLOR.AsInteger;
  inherited;
end;


procedure TfmRequestList.FormShow(Sender: TObject);
begin
  ReSize;
end;

end.
