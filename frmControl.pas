unit frmControl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, dxBar, FIBDataSet, pFIBDataSet,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  ExtCtrls, FIBQuery, pFIBQuery, AppEvnts, cxLookAndFeels,
  cxLookAndFeelPainters;

type
  TControlDialog = class(TForm)
    PageControl: TPageControl;
    TabSheet: TTabSheet;
    TabSheet1: TTabSheet;
    DataSource: TDataSource;
    GridView: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    DataSet: TpFIBDataSet;
    DataSetCLASS: TFIBIntegerField;
    DataSetDOCUMENT: TFIBIntegerField;
    DataSetDOCUMENTDATE: TFIBDateField;
    DataSetID: TFIBIntegerField;
    DataSetSTORAGETITLE: TFIBStringField;
    DataSetELEMENT: TFIBStringField;
    DataSetOPERATION: TFIBStringField;
    DataSetFROMOPERATION: TFIBStringField;
    DataSetWEIGHT: TFIBFloatField;
    DataSetQUANTITY: TFIBIntegerField;
    GridViewCLASS: TcxGridDBColumn;
    GridViewDOCUMENT: TcxGridDBColumn;
    Grid: TcxGridDBColumn;
    GridViewSTORAGETITLE: TcxGridDBColumn;
    GridViewELEMENT: TcxGridDBColumn;
    GridViewOPERATION: TcxGridDBColumn;
    GridViewFROMOPERATION: TcxGridDBColumn;
    GridViewWEIGHT: TcxGridDBColumn;
    GridViewQUANTITY: TcxGridDBColumn;
    ApplicationEvents1: TApplicationEvents;
    DataSetCLASSTITLE: TFIBStringField;
    procedure BarButtonRefreshClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure DataSetCalcFields(DataSet: TDataSet);
    procedure ApplicationEvents1Message(var Msg: tagMSG;
      var Handled: Boolean);
  private
    Value: Integer;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWindowHandle(const Params: TCreateParams); override;
    { Private declarations }


  public
    { Public declarations }

  end;

var
  ControlDialog: TControlDialog;

  procedure ControlLog(Message: string);


implementation

uses DictData, iBase, rxStrUtils;

{$R *.dfm}

function ExecSelectSQL(Statement : string; q : TpFIBQuery;const fCommitRetaining: boolean) : Variant;
var
  b : boolean;
  i : integer;
begin
  Screen.Cursor := crDefault;
  with q do
  try
    if Open then Close;
    b := Transaction.Active;
    if b then
    begin
      if fCommitRetaining then Transaction.CommitRetaining
    end
    else Transaction.StartTransaction;
    if(q.Database.SQLDialect = 3)then Statement := StringReplace(Statement, #34, #39, [rfReplaceAll]);
    SQL.Text := Statement;
    try
      ExecQuery;
    except
      on E: Exception do
      begin
        Application.MessageBox(PChar(E.Message + #13#10 + 'SQL = ' + SQL.Text), 'Exception', 0);
      end;
    end;
    if FieldCount = 1 then
    begin
      if Fields[0].IsNull then Result := null
      else
        case Fields[0].Data.sqltype and not(1) of
          SQL_TEXT : Result := DelRSpace(Fields[0].AsString)
          else Result := Fields[0].Value;
        end;
    end
    else begin
      Result := VarArrayCreate([0,Pred(FieldCount)], varVariant);
      for i:=0 to pred(FieldCount) do Result[i] := Fields[i].Value;
    end;
    Close;
    if b then
    begin
      if fCommitRetaining then Transaction.CommitRetaining
    end
    else Transaction.Commit;
  finally
    Screen.Cursor := crDefault;
  end
end;

procedure ControlLog(Message: string);
begin
  if ControlDialog <> nil then
  begin
  end;
end;

{ TControlDialog }

procedure TControlDialog.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.ExStyle := WS_EX_APPWINDOW or WS_EX_TOOLWINDOW;
end;


procedure TControlDialog.BarButtonRefreshClick(Sender: TObject);
var
  ID: Integer;
begin
  if DataSet.Active then ID := DataSet.FieldByName('ID').AsInteger
  else ID := 0;
  DataSet.Active := False;
  DataSet.Active := True;
  GridView.DataController.Groups.FullExpand;
  if ID <> 0 then
  begin
    if not DataSet.Locate('ID', ID, []) then DataSet.First;
  end;
end;

procedure TControlDialog.FormCreate(Sender: TObject);
var
  Result : Variant;
begin
  DataSet.Active := True;
  GridView.DataController.Groups.FullExpand;
  //Result := ExecSelectSQL('select max(SPOILAGE$ID) from spoilage',dm.quTmp, False);
  //if not VarIsNull(Result) then Value := Result
  //else Value := 0;
end;

procedure TControlDialog.CreateWindowHandle(const Params: TCreateParams);
var
  AParams: TCreateParams;
begin
  AParams := Params;
  AParams.WndParent := GetDesktopWindow;
  inherited CreateWindowHandle(AParams);
end;

procedure TControlDialog.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
var
  Result : Variant;
  AValue: Integer;

begin
  //Result := ExecSelectSQL('select max(SPOILAGE$ID) from spoilage',dm.quTmp, False);
  //if not VarIsNull(Result) then AValue := Result
  //else AValue := 0;
  //if AValue <> Value then
  //begin
  //  Value := AValue;
    BarButtonRefreshClick(nil);
  //end;
end;

procedure TControlDialog.DataSetCalcFields(DataSet: TDataSet);
begin
  if DataSetCLASS.AsInteger = 1 then DataSet['Class$Title'] := '1. ��������'
  else DataSet['Class$Title'] := '2. ����� ��������';
end;

procedure TControlDialog.ApplicationEvents1Message(var Msg: tagMSG; var Handled: Boolean);
var
  Message: TMessage;
begin
  if Msg.hwnd = Handle then
  begin
    Message.Msg := Msg.message;
    Message.WParam := Msg.wParam;
    Message.LParam := Msg.lParam;
    WndProc(Message);
    Handled := True;
  end
  else Handled := False;
end;

end.
