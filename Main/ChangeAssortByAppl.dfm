inherited fmChangeAssortByAppl: TfmChangeAssortByAppl
  Left = 112
  Top = 137
  Width = 706
  Height = 573
  Caption = #1048#1079#1084#1077#1085#1080#1090#1100' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090' '#1087#1086' '#1079#1072#1103#1074#1082#1077
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 520
    Width = 698
  end
  inherited plDict: TPanel
    Width = 698
    Height = 478
    inherited plSelect: TPanel
      Top = 435
      Width = 694
      inherited btnOk: TSpeedButton
        Left = 516
      end
      inherited btnCancel: TSpeedButton
        Left = 600
      end
      object chbxGet: TCheckBox
        Left = 308
        Top = 5
        Width = 201
        Height = 17
        Caption = #1040#1074#1090#1086#1084#1072#1090#1080#1095#1077#1089#1082#1080' '#1087#1088#1086#1080#1079#1074#1077#1089#1090#1080' '#1074#1099#1076#1072#1095#1091
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object chbxAQ: TCheckBox
        Left = 308
        Top = 23
        Width = 201
        Height = 17
        Caption = #1047#1072#1087#1086#1083#1085#1080#1090#1100' '#1087#1086#1083#1077' "'#1055#1088#1080#1085#1103#1090#1086' '#1074' '#1088#1072#1073#1086#1090#1091'"'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
    inherited gridDict: TDBGridEh
      Top = 25
      Width = 694
      Height = 410
      AllowedOperations = [alopUpdateEh]
      AllowedSelections = [gstRecordBookmarks, gstRectangle, gstColumns]
      FooterRowCount = 1
      OptionsEh = [dghFixed3D, dghFrozen3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      SumList.Active = True
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'AART'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085#1085#1099#1081' '#1079#1072#1082#1072#1079'|'#1040#1088#1090#1080#1082#1091#1083
          Title.EndEllipsis = True
          Title.TitleButton = False
          Width = 66
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ASZNAME'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085#1085#1099#1081' '#1079#1072#1082#1072#1079'|'#1056#1072#1079#1084#1077#1088
          Title.EndEllipsis = True
          Title.TitleButton = False
          Width = 69
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'AOPERNAME'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085#1085#1099#1081' '#1079#1072#1082#1072#1079'|'#1054#1087#1077#1088#1072#1094#1080#1103
          Title.EndEllipsis = True
          Title.TitleButton = False
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'AQ'
          Footer.FieldName = 'AQ'
          Footer.ValueType = fvtSum
          Footers = <>
          ReadOnly = True
          Title.Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085#1085#1099#1081' '#1079#1072#1082#1072#1079'|'#1047#1072#1103#1074#1083#1077#1085#1086
          Title.EndEllipsis = True
          Title.TitleButton = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'AFQ'
          Footer.FieldName = 'AFQ'
          Footer.ValueType = fvtSum
          Footers = <>
          Title.Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085#1085#1099#1081' '#1079#1072#1082#1072#1079'|'#1055#1088#1080#1085#1103#1090#1086' '#1074' '#1088#1072#1073#1086#1090#1091
          Title.EndEllipsis = True
          Title.TitleButton = False
          Width = 57
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WHART'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1042' '#1082#1083#1072#1076#1086#1074#1086#1081'|'#1040#1088#1090#1080#1082#1091#1083
          Title.EndEllipsis = True
          Title.TitleButton = False
          Width = 56
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WHSZNAME'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1042' '#1082#1083#1072#1076#1086#1074#1086#1081'|'#1056#1072#1079#1084#1077#1088
          Title.EndEllipsis = True
          Title.TitleButton = False
          Width = 56
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WHOPERNAME'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1042' '#1082#1083#1072#1076#1086#1074#1086#1081'|'#1054#1087#1077#1088#1072#1094#1080#1103
          Title.EndEllipsis = True
          Title.TitleButton = False
          Width = 68
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WHQ'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1042' '#1082#1083#1072#1076#1086#1074#1086#1081'|'#1050#1086#1083'-'#1074#1086
          Title.EndEllipsis = True
          Title.TitleButton = False
          Width = 44
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WHUNITNAME'
          Footers = <>
          ReadOnly = True
          Title.Caption = #1042' '#1082#1083#1072#1076#1086#1074#1086#1081'|'#1045#1076'. '#1080#1079#1084'.'
          Title.EndEllipsis = True
          Title.TitleButton = False
          Width = 50
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'Q'
          Footer.FieldName = 'Q'
          Footer.ValueType = fvtSum
          Footers = <>
          Title.EndEllipsis = True
          Title.TitleButton = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'JAID'
          Footers = <>
          ReadOnly = True
          Title.TitleButton = False
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'AARTID'
          Footers = <>
          ReadOnly = True
          Title.TitleButton = False
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'AART2ID'
          Footers = <>
          ReadOnly = True
          Title.TitleButton = False
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ASZID'
          Footers = <>
          ReadOnly = True
          Title.TitleButton = False
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'AOPERID'
          Footers = <>
          ReadOnly = True
          Title.TitleButton = False
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'AUNITID'
          Footers = <>
          ReadOnly = True
          Title.TitleButton = False
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WHARTID'
          Footers = <>
          ReadOnly = True
          Title.TitleButton = False
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WHART2ID'
          Footers = <>
          ReadOnly = True
          Title.TitleButton = False
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WHSZID'
          Footers = <>
          ReadOnly = True
          Title.TitleButton = False
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WHOPERID'
          Footers = <>
          ReadOnly = True
          Title.TitleButton = False
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WHUNITID'
          Footers = <>
          ReadOnly = True
          Title.TitleButton = False
          Visible = False
        end>
    end
    object TBDock1: TTBDock
      Left = 2
      Top = 2
      Width = 694
      Height = 23
      object TBToolbar1: TTBToolbar
        Left = 0
        Top = 0
        Caption = 'TBToolbar1'
        TabOrder = 0
        object TBControlItem1: TTBControlItem
          Control = Label1
        end
        object TBControlItem2: TTBControlItem
          Control = cmbxOper
        end
        object Label1: TLabel
          Left = 0
          Top = 3
          Width = 94
          Height = 13
          Caption = #1060#1080#1083#1100#1090#1088' '#1086#1087#1077#1088#1072#1094#1080#1080' '
        end
        object cmbxOper: TDBComboBoxEh
          Left = 94
          Top = 0
          Width = 137
          Height = 19
          TabStop = False
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Width = -1
          EditButtons = <>
          Flat = True
          TabOrder = 0
          Visible = True
          OnChange = cmbxOperChange
        end
      end
    end
  end
  inherited tb1: TSpeedBar
    Width = 698
  end
  inherited fmstr: TFormStorage
    Top = 204
  end
  inherited ActionList2: TActionList
    Left = 92
    Top = 140
  end
  inherited acEvent: TActionList
    object acShowID: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
      ShortCut = 8260
      OnExecute = acShowIDExecute
    end
    object acChangeArt: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
      OnExecute = acChangeArtExecute
    end
  end
end
