unit eMonth;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh;

type
  TfmeMonth = class(TfmEditor)
    lbMonth: TLabel;
    cmbxMonth: TDBComboBoxEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;


function ShowMonth(DefaultMonth : smallint = -1):boolean;

implementation

{$R *.dfm}

uses fmUtils;

function ShowMonth(DefaultMonth : smallint = -1):boolean;
var
  fmeMonth: TfmeMonth;
begin
  fmeMonth := TfmeMonth.Create(Application);
  try
    if(DefaultMonth <> -1)then fmeMonth.cmbxMonth.ItemIndex := DefaultMonth;
    Result := fmeMonth.ShowModal=mrOk;
  finally
    FreeAndNil(fmeMonth);
  end;
end;

procedure TfmeMonth.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk : begin
      if(cmbxMonth.ItemIndex = -1)then
      begin
        ActiveControl := cmbxMonth;
        raise EWarning.Create('�������� �����');
      end;
      VResult := cmbxMonth.ItemIndex+1;
    end;
  end;
end;

end.
