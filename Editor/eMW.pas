unit eMW;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, DBCtrlsEh, StdCtrls, Mask, Buttons, ExtCtrls;

type
  TfmMW = class(TfmEditor)
    lbMW: TLabel;
    lbU: TLabel;
    edMW: TDBNumberEditEh;
    cmbxU: TDBComboBoxEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FArtName : string;
    FArtId : integer;
  end;

procedure ShowMWForm(ArtName : string; ArtId : integer);

implementation

{$R *.dfm}

uses dbUtil, MainData; 

procedure ShowMWForm(ArtName : string; ArtId : integer);
var
  fmMW: TfmMW;
begin
  fmMW := TfmMW.Create(nil);
  try
    fmMW.FArtName := ArtName;
    fmMW.FArtId := ArtId;
    fmMW.Caption := '������� '+ArtName;
    fmMW.cmbxU.ItemIndex := 1;
    fmMW.ShowModal;
  finally
    FreeAndNil(fmMW);
  end;
end;

procedure TfmMW.FormClose(Sender: TObject; var Action: TCloseAction);
var
  smw : string;
begin
  smw := StringReplace(edMW.Text, ',', '.', [rfReplaceAll]);
  ExecSQL('update D_Art set MW='+smw+', UnitId='+IntToStr(cmbxU.ItemIndex)+
    ' where D_ArtId='+IntToStr(FArtId), dmMain.quTmp);
  vResult := edMW.Value;
end;

end.
