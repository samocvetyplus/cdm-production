unit DBTree;

interface

uses ComCtrls, Forms, Graphics, Controls, CheckOut, IBSQL;

Type TNodeText=function(q: TIBSQL): string;

Type TNodeData=class(TObject)
       public
         Name: string; // ��� ��� �����������
         Code: variant;
         Loaded: boolean;
         Value: integer;
         SaveValue: integer;
         constructor Create;
       end;

procedure InitTree(RootText: string; tv: TTreeView; RootCode: variant);overload;
procedure InitTree(tv : TTreeView; q : TIBSQL; FieldInd : array of byte);overload;// ���� ��� �����.
procedure ExpandTree(tv: TTreeView; Node: TTreeNode; NodeText: TNodeText; q: TIBSQL;
                      FieldInd: array of byte);




Procedure ReExpand(Node: TTreeNode);
Procedure GetNodeImages(Node: TTreeNode);
Procedure GetSelNodeImages(Node: TTreeNode);

Function NodeText(q: TIBSQL): string;
Function NodeText1(q: TIBSQL): string;
Function GetParent(Node: TTreeNode; Level: integer): TTreeNode;

implementation

uses rxStrUtils, Variants;

constructor TNodeData.Create;
begin
  inherited Create;
  Value:=0;
end;

procedure InitTree(tv : TTreeView; q : TIBSQL; FieldInd : array of byte);overload;
var
  TreeNode : TTreeNode;
  NodeData : TNodeData;
begin
  // FieldInd[0] - ��������� �� Id
  // FieldInd[1] - ��������� �� Name
  // FieldInd[2] - ��������� �� ���� Child
  with q do
  begin
    ExecQuery;
    while not Eof do
    begin
      TreeNode := tv.Items.AddChild(nil, NodeText(q));
      TreeNode.StateIndex:=1;
      TreeNode.HasChildren := q.Fields[FieldInd[2]].AsInteger>0;
      NodeData := TNodeData.Create;
      TreeNode.Data := NodeData;
      NodeData.Loaded:=False;
      NodeData.Code := DelRSpace(q.Fields[FieldInd[0]].AsString);
      NodeData.Name := DelRSpace(q.Fields[FieldInd[1]].AsString);
      Next;
    end;
  end;
end;

procedure ExpandTree(tv: TTreeView; Node: TTreeNode; NodeText: TNodeText; q: TIBSQL; FieldInd: array of byte);
var
  pN: TTreeNode;
  ND: TNodeData;
begin
  try
  Screen.Cursor:=crHourGlass;
  ND:=TNodeData(Node.Data);
  if NOT ND.Loaded then
  begin
    ND.Loaded:=True;
    with q do
    begin
      if ND.Code=NULL then
      begin
        Params[0].AsString:='-1';
        Params[1].AsInteger:=1;
      end
      else begin
        Params[0].AsString:=ND.Code;
        Params[1].AsInteger:=0;
      end;
      ExecQuery;
      Node.HasChildren:=NOT Fields[0].IsNull;
      while NOT EOF do
      begin
        pN:=tv.Items.AddChild(Node,NodeText(q)) ;
        pN.StateIndex:=1;
        pN.HasChildren:=q.Fields[FieldInd[2]].AsInteger>0;
        ND:=TNodeData.Create;
        pN.Data:=ND;
        ND.Loaded:=False;
        ND.Code:=DelRSpace(q.Fields[FieldInd[0]].AsString);
        ND.Name:=DelRSpace(q.Fields[FieldInd[1]].AsString);
        Next;
      end;
      Close;
    end;
  end;
  finally
    Screen.Cursor:=crDefault;
  end;
end;

procedure InitTree(RootText: string; tv: TTreeView; RootCode: variant);
var
  ND: TNodeData;
begin
  ND:=TNodeData.Create;
  ND.Code := RootCode;
  ND.Loaded := False;
  with tv.Items.AddChild(NIL, RootText) do
  begin
    Data:=ND;
    HasChildren:=True;
    StateIndex:=0;
  end;
  tv.TopItem.Expand(False);
end;

procedure ReExpand(Node: TTreeNode);
begin
  with Node do
    begin
      TNodeData(Data).Loaded:=False;
      Collapse(False);
      DeleteChildren;
      HasChildren:=True;
      Expand(False);
    end;
end;

Procedure GetNodeImages(Node: TTreeNode);
begin
  with Node do
    if Node.Parent=NIL then ImageIndex:=0
    else if HasChildren AND NOT Expanded then ImageIndex:=1
         else if HasChildren AND Expanded then ImageIndex:=2
              else ImageIndex:=3;
end;


Procedure GetSelNodeImages(Node: TTreeNode);
begin
  with Node do
    if Node.Parent=NIL then SelectedIndex:=0
    else if HasChildren AND NOT Expanded then SelectedIndex:=1
         else if HasChildren AND Expanded then SelectedIndex:=2
              else SelectedIndex:=3;
end;


Function NodeText(q: TIBSQL): string;
begin
  Result:=DelRSpace(q.Fields[0].AsString)+' - '+DelRSpace(q.Fields[1].AsString);
end;


Function NodeText1(q: TIBSQL): string;
begin
  Result:=DelRSpace(q.Fields[1].AsString);
end;


Procedure SaveNodesValue(tv: TTreeView);
var i: integer;
begin
  with tv do
    for i:=0 to Items.Count-1 do
      if Items[i].Data<>NIL then
      with TNodeData(Items[i].Data) do
        SaveValue:=Value;
end;


Procedure RestoreNodesValue(tv: TTreeView);
var i: integer;
begin
  with tv do
    for i:=0 to Items.Count-1 do
      if Items[i].Data<>NIL then
        with TNodeData(Items[i].Data) do
          Value:=SaveValue;
end;

{Procedure IterateOutLine(co: TCheckOutLine; ParentItem: integer; p: TIterateProcedure);
var i: integer;
begin
  p(TNodeData(co.Items[ParentItem].Data));
  i:=co.Items[ParentItem].GetFirstChild;
  while i<>-1 do
    begin
      IterateOutLine(co, i, p);
      i:=co.Items[ParentItem].GetNextChild(i);
    end;
end;}

Function  GetParent(Node: TTreeNode; Level: integer): TTreeNode;
var nd: TTreeNode;
begin
  nd:=Node;
  while (nd.Level<>Level) AND (nd.Parent<>NIL) do
    nd:= nd.Parent;
  Result:=nd;
end;

end.


