inherited fmSChargeList: TfmSChargeList
  Left = 84
  Top = 165
  Caption = #1040#1082#1090#1099' '#1089#1087#1080#1089#1072#1085#1080#1103' '#1076#1088#1072#1075'. '#1082#1072#1084#1085#1077#1081
  ClientHeight = 417
  ClientWidth = 673
  ExplicitWidth = 681
  ExplicitHeight = 451
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 398
    Width = 673
    ExplicitTop = 398
    ExplicitWidth = 673
  end
  inherited tb2: TSpeedBar
    Width = 673
    ExplicitWidth = 673
  end
  inherited tb1: TSpeedBar
    Width = 673
    ExplicitWidth = 673
    inherited spitPrint: TSpeedItem [4]
    end
    inherited spitEdit: TSpeedItem [5]
      OnClick = spitEditClick
    end
  end
  inherited gridDocList: TDBGridEh
    Width = 673
    Height = 327
    DataSource = dmMain.dsrSChargeList
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    SumList.Active = True
    UseMultiTitle = True
    Visible = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1040#1082#1090'|'#1085#1086#1084#1077#1088
        Title.EndEllipsis = True
        Width = 47
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1040#1082#1090'|'#1076#1072#1090#1072
        Title.EndEllipsis = True
        Width = 56
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 133
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Title.EndEllipsis = True
        Width = 133
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1082#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 44
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1074#1077#1089
        Title.EndEllipsis = True
        Width = 58
      end
      item
        Color = 16776176
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REFDOCNO'
        Footers = <>
        Title.Caption = #1055#1088#1086#1090#1086#1082#1086#1083'|'#1085#1086#1084#1077#1088
        Title.EndEllipsis = True
        Width = 38
      end
      item
        Color = 16776176
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'REFDOCDATE'
        Footers = <>
        Title.Caption = #1055#1088#1086#1090#1086#1082#1086#1083'|'#1076#1072#1090#1072
        Title.EndEllipsis = True
        Width = 56
      end>
  end
  inherited fmstr: TFormStorage
    Left = 128
  end
  inherited ActionList2: TActionList
    Top = 124
  end
  inherited ppDep: TPopupMenu
    Left = 24
    Top = 124
  end
  inherited acAEvent: TActionList
    Left = 128
    Top = 172
  end
  inherited ppDoc: TTBPopupMenu
    Left = 192
    Top = 124
  end
end
