object fmPeriod: TfmPeriod
  Left = 385
  Top = 281
  ActiveControl = deBD
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088' '#1086#1090#1095#1077#1090#1085#1086#1075#1086' '#1087#1077#1088#1080#1086#1076#1072
  ClientHeight = 58
  ClientWidth = 279
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 82
    Height = 13
    Caption = #1053#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 8
    Top = 32
    Width = 76
    Height = 13
    Caption = #1050#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object BitBtn1: TBitBtn
    Left = 196
    Top = 4
    Width = 75
    Height = 25
    TabOrder = 0
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 196
    Top = 29
    Width = 75
    Height = 25
    TabOrder = 1
    Kind = bkCancel
  end
  object deBD: TDBDateTimeEditEh
    Left = 104
    Top = 5
    Width = 81
    Height = 19
    EditButton.ShortCut = 16397
    EditButton.Style = ebsGlyphEh
    EditButtons = <>
    Flat = True
    Kind = dtkDateEh
    TabOrder = 2
    Visible = True
  end
  object deED: TDBDateTimeEditEh
    Left = 104
    Top = 28
    Width = 81
    Height = 19
    EditButton.ShortCut = 16397
    EditButton.Style = ebsGlyphEh
    EditButtons = <>
    Flat = True
    Kind = dtkDateEh
    TabOrder = 3
    Visible = True
  end
end
