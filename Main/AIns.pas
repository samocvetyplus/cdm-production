{***********************************************}
{  �������� � �������                           }
{  Copyrigth (C) 2002-2003 basile for CDM       }
{  v1.13                                        }
{  create      10.11.2002                       }
{  last update 22.08.2003                       }
{***********************************************}
unit AIns;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls, ComCtrls,
  Grids, DBGrids, RXDBCtrl, db, DBGridEh, ActnList, DBGridEhGrouping,
  GridsEh;

type
  TfmAIns = class(TfmAncestor)
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;
    spitRet: TSpeedItem;
    spitRej: TSpeedItem;
    gridAIns: TDBGridEh;
    acEvent: TActionList;
    acGet: TAction;
    acDel: TAction;
    acRet: TAction;
    acRej: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acGetExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acRetExecute(Sender: TObject);
    procedure acRejExecute(Sender: TObject);
  end;

var
  fmAIns: TfmAIns;

implementation

uses ApplData, dbUtil, DictData, WOrder, MainData;

{$R *.dfm}

procedure TfmAIns.FormCreate(Sender: TObject);
begin
  Caption := '��������� �'+dmAppl.taAInvDOCNO.AsString+' �� '+DateToStr(dmAppl.taAInvDOCDATE.AsDateTime)+'. '+
    '������� '+dmAppl.taAElART.AsString+' '+dmAppl.taAElART2.AsString+' - �������� ������';
  ReOpenDataSets([dm.taSIns, dm.taEdgT, dm.taEdgShape, dmAppl.taAIns]);
  dmAppl.AInsItType := 0;
end;

procedure TfmAIns.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dmAppl.taAIns, dm.taSIns, dm.taEdgT, dm.taEdgShape]);
end;

procedure TfmAIns.acGetExecute(Sender: TObject);
begin
  dmAppl.AInsItType := 0;
  dmAppl.taAIns.Insert;
end;

procedure TfmAIns.acDelExecute(Sender: TObject);
begin
  dmAppl.taAIns.Delete;
end;

procedure TfmAIns.acRetExecute(Sender: TObject);
begin
  dmAppl.AInsItType := 3;
  dmAppl.taAIns.Insert;
end;

procedure TfmAIns.acRejExecute(Sender: TObject);
begin
  dmAppl.AInsItType := 5;
  dmAppl.taAIns.Insert;
end;

end.
