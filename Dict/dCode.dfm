inherited fmCode: TfmCode
  Left = 72
  Top = 141
  Width = 640
  Height = 450
  Caption = #1050#1086#1076#1080#1092#1080#1082#1072#1094#1080#1103' '#1072#1088#1090#1080#1082#1091#1083#1072
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 397
    Width = 632
  end
  inherited tb1: TSpeedBar
    Width = 632
    inherited spitExit: TSpeedItem
      Left = 491
    end
  end
  object Panel1: TPanel [2]
    Left = 189
    Top = 42
    Width = 443
    Height = 355
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 443
      Height = 27
      Align = alTop
      BevelInner = bvLowered
      TabOrder = 0
      object Label1: TLabel
        Left = 6
        Top = 6
        Width = 60
        Height = 13
        Caption = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object cmbxDict: TDBComboBoxEh
        Left = 72
        Top = 4
        Width = 205
        Height = 19
        EditButtons = <>
        Flat = True
        TabOrder = 0
        Text = 'cmbxDict'
        Visible = True
      end
    end
    object TBDock1: TTBDock
      Left = 0
      Top = 27
      Width = 443
      Height = 23
      object TBToolbar1: TTBToolbar
        Left = 0
        Top = 0
        Caption = 'TBToolbar1'
        TabOrder = 0
        object TBItem1: TTBItem
          Action = acAddKey
        end
        object TBItem2: TTBItem
          Action = acDelKey
        end
      end
    end
    object gridKey: TDBGridEh
      Left = 0
      Top = 50
      Width = 443
      Height = 305
      Align = alClient
      AllowedOperations = [alopUpdateEh]
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dm.dsrCode
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      RowDetailPanel.Color = clBtnFace
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'K'
          Footers = <>
          Width = 46
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'VALNAME'
          Footers = <>
          Width = 58
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'PCOEF'
          Footers = <>
          Title.Caption = #1062#1077#1085#1086#1074#1086#1081' '#1082#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
          Width = 89
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object plTree: TPanel [3]
    Left = 0
    Top = 42
    Width = 189
    Height = 355
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 3
    object TBDock2: TTBDock
      Left = 0
      Top = 0
      Width = 189
      Height = 23
      object TBToolbar2: TTBToolbar
        Left = 0
        Top = 0
        Caption = 'TBToolbar2'
        TabOrder = 0
        object TBItem3: TTBItem
          Action = acN
          Caption = #1044#1086#1073#1072#1074#1080#1090#1100
        end
        object TBItem5: TTBItem
          Action = acEditN
        end
        object TBItem4: TTBItem
          Action = acDelN
          Caption = #1059#1076#1072#1083#1080#1090#1100
        end
      end
    end
    object trvArtCode: TTreeView
      Left = 0
      Top = 23
      Width = 189
      Height = 332
      Align = alClient
      HideSelection = False
      Indent = 19
      PopupMenu = ppCode
      ReadOnly = True
      TabOrder = 1
      OnChange = trvArtCodeChange
      OnExpanding = trvArtCodeExpanding
    end
  end
  inherited ilButtons: TImageList
    Left = 506
    Top = 60
  end
  inherited fmstr: TFormStorage
    Active = False
    Left = 464
    Top = 200
  end
  object ppCode: TPopupMenu
    Left = 240
    Top = 160
    object N1: TMenuItem
      Action = acN
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N2: TMenuItem
      Caption = #1053#1086#1074#1086#1077' '#1079#1085#1072#1095#1077#1085#1080#1077
    end
  end
  object acEvent: TActionList
    Left = 196
    Top = 160
    object acN: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1088#1103#1076#1082#1086#1074#1091#1102' '#1094#1080#1092#1088#1091
      OnExecute = acNExecute
      OnUpdate = acNUpdate
    end
    object acDelN: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1088#1103#1076#1082#1086#1074#1091#1102' '#1094#1080#1092#1088#1091
      OnExecute = acDelNExecute
      OnUpdate = acDelNUpdate
    end
    object acAddKey: TAction
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      OnExecute = acAddKeyExecute
    end
    object acDelKey: TAction
      Caption = #1059#1076#1072#1083#1080#1090#1100
      OnExecute = acDelKeyExecute
    end
    object acEditN: TAction
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100
      OnExecute = acEditNExecute
      OnUpdate = acEditNUpdate
    end
  end
end
