unit dOperOut;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList,  ImgList,  Buttons,
  Grids, RXDBCtrl, ExtCtrls, ComCtrls, DBGridEh, DBGridEhGrouping,
  rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmOperOut = class(TfmDictAncestor)
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

implementation

uses DictData, dbUtil;

{$R *.dfm}

procedure TfmOperOut.FormCreate(Sender: TObject);
begin
  OpenDataSet(dm.taSemis);
  inherited;
end;

procedure TfmOperOut.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dm.taSemis);
  inherited;
end;

end.
