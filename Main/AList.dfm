inherited fmAList: TfmAList
  Left = 143
  Top = 201
  Caption = #1057#1087#1080#1089#1086#1082' '#1079#1072#1082#1072#1079#1086#1074
  ClientHeight = 646
  ClientWidth = 735
  Color = clWindow
  ExplicitWidth = 743
  ExplicitHeight = 680
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 627
    Width = 735
    ExplicitTop = 627
    ExplicitWidth = 735
  end
  inherited tb2: TSpeedBar
    Width = 735
    TabOrder = 2
    ExplicitWidth = 735
    inherited laDep: TLabel
      Left = 496
      Top = 6
      Visible = False
      ExplicitLeft = 496
      ExplicitTop = 6
    end
    inherited laPeriod: TLabel
      Left = 92
      Top = 10
      ExplicitLeft = 92
      ExplicitTop = 10
    end
    object lbOper: TLabel [2]
      Left = 272
      Top = 10
      Width = 50
      Height = 13
      Caption = #1054#1087#1077#1088#1072#1094#1080#1103
      Transparent = True
    end
    object cmbxOper: TDBComboBoxEh [3]
      Left = 328
      Top = 6
      Width = 133
      Height = 19
      DropDownBox.Rows = 20
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnChange = cmbxOperChange
    end
    inherited spitDep: TSpeedItem
      Visible = False
    end
    inherited spitPeriod: TSpeedItem
      Left = 3
    end
  end
  inherited tb1: TSpeedBar
    Width = 735
    IniStorage = nil
    TabOrder = 3
    ExplicitWidth = 735
    inherited spitAdd: TSpeedItem [1]
    end
    inherited spitDel: TSpeedItem [2]
    end
    inherited spitExit: TSpeedItem [3]
      Left = 259
    end
    inherited spitPrint: TSpeedItem [4]
      Action = nil
      Hint = #1055#1077#1095#1072#1090#1100' '#1089#1087#1080#1089#1082#1072' '#1079#1072#1082#1072#1079#1086#1074
      OnClick = TBItem6Click
    end
    inherited spitEdit: TSpeedItem [5]
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000104A2000104A2000104A2000104A2000104A2000104A2000104
        A2000104A2000104A2000104A200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000104A2005983FF000026FF000030FF000030FB00002FF200002FE900002E
        E1000030D8000031D0000034CB000104A200FF00FF00FF00FF00FF00FF00FF00
        FF000104A200ABC2FF006480FF006688FF006688FF006687FA006787F5006787
        F0005779E9004D70E4004D74E2000104A200FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000104A2000104A2000104A2000104A2000104A2000104A2000104
        A2000104A2000104A2000104A200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      OnClick = acEditDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    Width = 735
    Height = 556
    AllowedSelections = [gstRecordBookmarks]
    DataSource = dmAppl.dsrAList
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    SumList.Active = True
    TabOrder = 0
    UseMultiTitle = True
    Visible = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1047#1072#1103#1074#1082#1072'|'#1053#1086#1084#1077#1088
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1047#1072#1103#1074#1082#1072'|'#1044#1072#1090#1072' '
      end
      item
        EditButtons = <>
        FieldName = 'DEPARTMENT'
        Footers = <>
        Title.Caption = #1057#1082#1083#1072#1076
        Width = 152
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OPERATION'
        Footers = <>
        Width = 133
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
        Width = 112
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footer.FieldName = 'Q'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1050#1086#1083'-'#1074#1086
        Width = 55
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1042#1077#1089
        Width = 46
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ABD'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076' '#1072#1085#1072#1083#1080#1079#1072'|'#1053#1072#1095#1072#1083#1086
        Visible = False
        Width = 62
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'AED'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076' '#1072#1085#1072#1083#1080#1079#1072'|'#1050#1086#1085#1077#1094
        Visible = False
        Width = 53
      end
      item
        EditButtons = <>
        FieldName = 'STARTN'
        Footers = <>
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'STARTD'
        Footers = <>
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'FINISHN'
        Footers = <>
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'FINISHD'
        Footers = <>
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'READYN'
        Footers = <>
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'READYD'
        Footers = <>
        Width = 100
      end>
  end
  inherited ActionList2: TActionList
    Left = 72
    Top = 224
  end
  inherited ppDep: TPopupMenu
    Left = 236
    Top = 128
  end
  inherited acAEvent: TActionList
    Left = 56
    Top = 100
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
    object acWork: TAction
      Caption = '% '#1042#1099#1087#1086#1083#1085#1077#1085#1080#1103
      OnExecute = acWorkExecute
    end
    object acRejAdd: TAction
      Caption = #1057#1085#1103#1090#1080#1077'+'#1044#1086#1073#1072#1074#1083#1077#1085#1080#1077
      OnExecute = acRejAddExecute
    end
    object acWorkCrr: TAction
      Caption = '% '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1103' '#1079#1072#1103#1074#1082#1080
      OnExecute = acWorkCrrExecute
    end
    object acMultiSelect: TAction
      Caption = 'acMultiSelect'
      ShortCut = 116
      OnExecute = acMultiSelectExecute
    end
  end
  inherited ppDoc: TTBPopupMenu
    Left = 180
    Top = 124
    object TBSeparatorItem2: TTBSeparatorItem [3]
    end
    object TBItem6: TTBItem [4]
      Caption = #1055#1077#1095#1072#1090#1100' '#1089#1087#1080#1089#1082#1072' '#1079#1072#1103#1074#1086#1082
      ImageIndex = 4
      OnClick = TBItem6Click
    end
    object TBSeparatorItem3: TTBSeparatorItem [5]
    end
    object TBItem5: TTBItem [6]
      Action = acWorkCrr
    end
  end
  object PrintGridDocList: TPrintDBGridEh
    DBGridEh = gridDocList
    Options = [pghFitGridToPageWidth, pghOptimalColWidths]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -16
    PageFooter.Font.Name = 'Times New Roman'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -21
    PageHeader.Font.Name = 'Times New Roman'
    PageHeader.Font.Style = []
    Units = MM
    Left = 328
    Top = 192
  end
end
