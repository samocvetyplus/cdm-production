unit frmSetPrice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, StdCtrls, DB, DBClient, ComCtrls, ImgList, IniFiles,
  ExtCtrls, Buttons, ToolWin, ActnList, Menus, Math, DBCtrlsEh, ToolCtrlsEh,
  DBGridEhGrouping, GridsEh;


const

   DefaultRoundValue = 0.01;
   DefaultRoundMiddle = 0.005;

   IDMaterialGold       = 1;  // ������
   IDMaterialDiamond    = 10; // ���������
   IDMaterialEmerald    = 14; // �������
   IDMaterialSaphire    = 17; // ������
   IDMaterialRuby       = 19; // �����
   IDMaterialColorStone = 13; // ������� �����

type
  TDialogSetPrice = class(TForm)
    Grid: TDBGridEh;
    AutoSource: TDataSource;
    Auto: TClientDataSet;
    AutoModel: TStringField;
    AutoSubModel: TStringField;
    AutoTargetCost: TCurrencyField;
    AutoSourceCost: TCurrencyField;
    CalcGroup: TGroupBox;
    AutoCheck: TBooleanField;
    EnabledImageList: TImageList;
    GroupBox2: TGroupBox;
    Bevel1: TBevel;
    GridManual: TDBGridEh;
    Manual: TClientDataSet;
    ManualFormula: TStringField;
    ManualSource: TDataSource;
    ActionList: TActionList;
    ActionAppend: TAction;
    ActionDelete: TAction;
    ActionEdit: TAction;
    ActionPost: TAction;
    ToolBarSet: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ActionCancel: TAction;
    ActionUp: TAction;
    ActionDown: TAction;
    ActionSet: TAction;
    ToolBar1: TToolBar;
    Bevel2: TBevel;
    ToolBar2: TToolBar;
    ToolButton8: TToolButton;
    ToolBar3: TToolBar;
    ToolButton10: TToolButton;
    ActionCalculate: TAction;
    ManualOrder: TIntegerField;
    ToolBar4: TToolBar;
    ButtonOk: TToolButton;
    ButtonCancel: TToolButton;
    Models: TClientDataSet;
    ModelsModel: TStringField;
    ModelsSource: TDataSource;
    ManualModel: TStringField;
    AutoBaseCost: TCurrencyField;
    OkAction: TAction;
    DisabledImageList: TImageList;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ActionOpen: TAction;
    ActionSave: TAction;
    ToolButton13: TToolButton;
    OpenDialog: TOpenDialog;
    SaveDialog: TSaveDialog;
    ActionEmpty: TAction;
    ToolButton14: TToolButton;
    CheckImageList: TImageList;
    RoundEditor: TEdit;
    Costs: TClientDataSet;
    CostsID: TIntegerField;
    CostsTitle: TStringField;
    CostsCost: TCurrencyField;
    GridCost: TDBGridEh;
    CostsSource: TDataSource;
    UpperBorder: TLabel;
    RoundMiddleEditor: TEdit;
    UpDown: TUpDown;
    RoundGroup: TGroupBox;
    LowerBorder: TLabel;
    Panel1: TPanel;
    Splitter: TSplitter;
    InsertionGrid: TDBGridEh;
    AutoSubModelID: TIntegerField;
    AutoWeight: TFloatField;
    Panel2: TPanel;
    procedure GridTitleClick(Column: TColumnEh);
    procedure OnCalculate(Sender: TObject);
    procedure GridGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure OnSet(Sender: TObject);
    procedure ActionAppendUpdate(Sender: TObject);
    procedure ActionDeleteUpdate(Sender: TObject);
    procedure ActionEditUpdate(Sender: TObject);
    procedure ActionPostUpdate(Sender: TObject);
    procedure ActionCancelUpdate(Sender: TObject);
    procedure ActionUpUpdate(Sender: TObject);
    procedure ActionDownUpdate(Sender: TObject);
    procedure ActionSetUpdate(Sender: TObject);
    procedure ActionCalculateUpdate(Sender: TObject);
    procedure ActionAppendExecute(Sender: TObject);
    procedure ActionDeleteExecute(Sender: TObject);
    procedure ActionEditExecute(Sender: TObject);
    procedure ActionPostExecute(Sender: TObject);
    procedure ActionCancelExecute(Sender: TObject);
    procedure ActionUpExecute(Sender: TObject);
    procedure ActionDownExecute(Sender: TObject);
    procedure ActionSetExecute(Sender: TObject);
    procedure ActionCalculateExecute(Sender: TObject);
    procedure ManualNewRecord(DataSet: TDataSet);
    procedure ButtonCancelClick(Sender: TObject);
    procedure ManualBeforePost(DataSet: TDataSet);
    procedure AutoBeforePost(DataSet: TDataSet);
    procedure OkActionUpdate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure OkActionExecute(Sender: TObject);
    procedure AutoBeforeEdit(DataSet: TDataSet);
    procedure ActionOpenUpdate(Sender: TObject);
    procedure ActionSaveUpdate(Sender: TObject);
    procedure ActionOpenExecute(Sender: TObject);
    procedure ActionSaveExecute(Sender: TObject);
    procedure ActionEmptyUpdate(Sender: TObject);
    procedure ActionEmptyExecute(Sender: TObject);
    procedure GridExit(Sender: TObject);
    procedure GridManualExit(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure UpDownChangingEx(Sender: TObject; var AllowChange: Boolean;
      NewValue: Smallint; Direction: TUpDownDirection);
    procedure AutoSourceDataChange(Sender: TObject; Field: TField);
    procedure AutoWeightGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
  private
    { Private declarations }
    CalcMode: Boolean;
    FCosts: TClientDataSet;
    OldCheck: Boolean;
    CheckCount: Integer;
    procedure SetCost;
    procedure CalculateCost;
    function GetCost(ID: Integer): Currency;
  public
    { Public declarations }
    function Execute: Boolean;
  end;

var
  DialogSetPrice: TDialogSetPrice;

implementation

{$R *.dfm}

uses InvData, CalcExpress, StrUtils, ShlObj, uUtils, fmUtils;

var
  Calculator: TCalcExpress;

function TryStrToFloat(const S: string; var C: Extended): Boolean;
begin
  Result := True;
  try
    C := StrToFloat(S);
  except
    Result := False;
  end;
end;

function SmartRound(Value: Currency): Currency;
var
  RoundValue: Extended;
  RoundMiddleValue: Extended;
begin
  RoundValue := StrToFloat(DialogSetPrice.RoundEditor.Text);
  RoundMiddleValue := StrToFloat(DialogSetPrice.RoundMiddleEditor.Text);
  RoundMiddleValue := 1 - (RoundMiddleValue / RoundValue);
  Result := Trunc((Value / RoundValue) + RoundMiddleValue) * RoundValue;
end;

procedure TDialogSetPrice.GridTitleClick(Column: TColumnEh);
var
  BookMark: TBookmark;
  Check: Boolean;
begin
  Check := not (Column.Title.ImageIndex = 1);
  BookMark := Auto.GetBookmark;
  Auto.DisableControls;
  Auto.First;
  while not Auto.Eof do
  begin
    Auto.Edit;
    AutoCheck.AsBoolean := Check;
    Auto.Post;
    Auto.Next;
  end;
  Auto.GotoBookmark(BookMark);
  Auto.EnableControls;
end;

function TDialogSetPrice.Execute: Boolean;
var
  Sourse: TDataSet;
  Model: string;
  SubModel: string;
  CostGold: string;

procedure LoadDefaults;
var
  FileName: string;
  RoundValue: Currency;
  RoundMiddle: Currency;


  function OleVariantToFloat(Value: OleVariant): Extended;
  begin
    if Value = Unassigned then Result := 0
    else Result := Value;
  end;

procedure AddMaterial(ID: Integer; Title: string);
begin
  Costs.Append;
  CostsID.AsInteger := ID;
  CostsTitle.AsString := Title;
  Costs.Post;
end;

begin
  FileName := ExtractFilePath(GetIniFileName) + 'costs.xml';
  if FileExists(FileName) then
  begin
    Costs.LoadFromFile(FileName);
    Costs.LogChanges := False;
  end
  else
  begin
    Costs.CreateDataSet;
    Costs.LogChanges := False;
    AddMaterial(10, '����������');
    AddMaterial(13, '������� �����');
    AddMaterial(14, '��������');
    AddMaterial(17, '�������');
    AddMaterial(19, '������');
  end;

  if Costs.Active then
  begin
    RoundValue := Costs.GetOptionalParam('Round');
    if RoundValue = 0 then RoundValue := DefaultRoundValue;
    RoundMiddle := Costs.GetOptionalParam('RoundMiddle');
    if RoundMiddle = 0 then RoundMiddle := DefaultRoundMiddle;
    UpDown.Position := Trunc(Log10(RoundValue));
    RoundEditor.Text := CurrToStr(RoundValue);
    RoundMiddleEditor.Text := CurrToStr(RoundMiddle);
  end;
  FCosts.CloneCursor(Costs, True, False);
  FCosts.IndexDefs.Add('ID', 'ID' ,[]);
  FCosts.IndexName := 'ID';

end;

procedure SaveDefaults;
var
  FileName: string;
  RoundValue: Extended;
  RoundMiddleValue: Extended;
begin
  if Costs.Active then
  begin
    if TryStrToFloat(RoundEditor.Text, RoundValue) then
    Costs.SetOptionalParam('Round', RoundValue);
    if TryStrToFloat(RoundMiddleEditor.Text, RoundMiddleValue) then
    begin
      if (RoundMiddleValue >= 0) and (RoundMiddleValue <= RoundValue) then
      Costs.SetOptionalParam('RoundMiddle', RoundMiddleValue);
    end;
  end;
  FileName := ExtractFilePath(GetIniFileName) + 'Costs.xml';
  Costs.SaveToFile(FileName);
end;

begin
  //if dmInv.taSListISCLOSE.AsInteger <> 0 then
  //begin Application.MessageBox('��������� �������.', '������', MB_OK); Exit; end;

  LoadDefaults;

  dminv.taConst.Active := False;
  dminv.taConst.Active := True;
  if dminv.taConst.Locate('NAME','���� �� �����',[loCaseInsensitive]) then
  begin
    CostGold := Trim(dminv.taConstVAL.AsString);
    if CostGold = '' then
    CostGold := '0';
    if FCosts.Active then
    begin
      if not FCosts.FindKey([IDMaterialGold]) then
      begin
        FCosts.Append;
        FCosts['ID'] := 1;
        FCosts['Title'] := '������';
      end
      else FCosts.Edit;
      FCosts['Cost'] := StrToCurr(CostGold);
      FCosts.Post;
    end;
  end;
  Costs.First;

  dminv.taConst.Active := False;


  Auto.IndexFieldNames := 'Model; SubModel';
  Manual.IndexDefs.Add('Model_Idx', 'Model', [ixDescending]);
  Manual.IndexName := 'Model_Idx';
  //Manual.IndexFieldNames := 'Order';
  Manual.LogChanges := False;
  Models.IndexFieldNames := 'Model';
  Calculator := TCalcExpress.Create(Self);
  CalculateCost;

  Models.First;
  while not Models.Eof do
  begin
    GridManual.Columns[0].PickList.Add(ModelsModel.AsString);
    Models.Next;
  end;

  Result := ShowModal = mrOK;
  Calculator.Free;
  SaveDefaults;
  if Result then
  begin
    begin
      //ShowMessage('Kol-vo ������� ' + inttostr(dmInv.taINVItems.RecordCount));
      Sourse := dmInv.taINVItems;
      //BookMark := Sourse.GetBookmark;
      //Sourse.DisableControls;
      Sourse.First;
      while not Sourse.Eof do
      begin
        SubModel := Trim(Sourse.FieldByName('ART2').AsString);
        if SubModel <> '' then
        begin
          Model := (Sourse.FieldByName('ART').AsString);
          if Auto.FindKey([Model, SubModel]) then
          begin
            if AutoCheck.AsBoolean then
            begin
             Sourse.Edit;
             Sourse['PRICE2'] := Auto['TargetCost'];
             Sourse.Post;
            end;
          end;
        end;
        Sourse.Next;
      end;
      // Sourse.GotoBookmark(BookMark);
      //  Sourse.EnableControls;
      Sourse.First;
    end;
  end;
end;

procedure TDialogSetPrice.OnCalculate(Sender: TObject);
begin
  if Auto.State = dsEdit then Auto.Post;
  if Manual.State <> dsBrowse then Manual.Post;
  CalculateCost;
end;

procedure TDialogSetPrice.CalculateCost;
var
  Sourse: TDataSet;
  Insertions: TDataSet;
  BookMark: TBookmark;
  Model: string;
  SubModel: string;
  BaseCost: Currency;
  SourceCost: Currency;
  TargetCost: Currency;
  SubModelID: Integer;
  Weight: Double;
  WeightGold: Double;  
  Insertion: Integer;
  Flag: Boolean;
begin
  CheckCount := 0;

//  if not TryStrToFloat(SRound, Tmp) then
//  begin Application.MessageBox('������������ �������� ����������', '������', MB_OK); Exit; end;

  Auto.BeforePost := nil;
  Auto.EmptyDataSet;
  Models.EmptyDataSet;
  Auto.DisableControls;
  Sourse := dmInv.taINVItems;  //������ �� ������� � ���������
  BookMark := Sourse.GetBookmark;
  Sourse.DisableControls;
  Sourse.First;
  Insertions := dminv.taArtIns;  // ������ �� ������� �� ���������
  Insertions.DisableControls;

while not Sourse.Eof  do    //��� ������� �������
  begin

    Model := Trim(Sourse.FieldByName('ART').AsString);
    SubModel := Trim(Sourse.FieldByName('ART2').AsString);

    if not Models.FindKey([Model]) then   //���� �� �����, �� ���������
    begin
      Models.Append;
      ModelsModel.AsString := Model;
      Models.Post;
    end;

    if SubModel = '' then SubModel := '-';   //��������� ������ ������ �� ������ ��������

    if SubModel <> '-' then   //���� ���� ������ ������� (�������)
    begin
                //Source = taInvItems
      SourceCost := Sourse.FieldByName('PRICE2').AsCurrency;  //��������� ���������� �� ������� �� ���������
      BaseCost := Sourse.FieldByName('PRICE').AsCurrency;
      SubModelID := Sourse.FieldByName('ART2ID').AsInteger;
      WeightGold := Sourse.FieldByName('W').AsFloat;
      if not Auto.FindKey([Model, SubModel]) then
      begin
        dmInv.CurrArt2Id := SubModelID;
        Insertions.Active := False;
        Insertions.Active := True;
        TargetCost := WeightGold * GetCost(IDMaterialGold);
        Insertions.First;
        while not Insertions.Eof do
        begin
          Weight := Insertions.FieldByName('WEIGHT').AsFloat;
          if Weight < 0.1 then Weight :=  Round(Weight * 100) / 100;
          Insertion := Insertions.FieldByName('MAT').AsInteger;
          TargetCost := TargetCost + (Weight * GetCost(Insertion));
          Insertions.Next;
        end;
        Auto.Append;
        AutoCheck.AsBoolean := False;
        AutoModel.AsString := Model;
        AutoSubModelID.AsInteger := SubModelID;
        AutoSubModel.AsString := SubModel;
        AutoSourceCost.AsCurrency := SourceCost;
        AutoTargetCost.AsCurrency := SmartRound(TargetCost);
        AutoBaseCost.AsCurrency := BaseCost;
        AutoWeight.AsFloat :=  WeightGold;
        Auto.Post;
      end;
    end
    else
    begin
      Model := (Sourse.FieldByName('ART').AsString);
      if not Auto.FindKey([Model, SubModel]) then
      begin
        SourceCost := Sourse.FieldByName('PRICE2').AsCurrency;
        BaseCost := Sourse.FieldByName('PRICE').AsCurrency;
        TargetCost := SourceCost;
        Auto.Append;
        AutoCheck.AsBoolean := False;
        AutoModel.AsString := Model;
        AutoSubModel.AsString := SubModel;
        AutoSubModelID.AsInteger := SubModelID;
        AutoSourceCost.AsCurrency := SourceCost;
        AutoTargetCost.AsCurrency := TargetCost;
        AutoBaseCost.AsCurrency := BaseCost;
        Auto.Post;
      end;
    end;
    Sourse.Next;
  end;
  Insertions.Active := False;
  Auto.First;
  Auto.EnableControls;
  Auto.BeforePost := AutoBeforePost;
  Sourse.GotoBookmark(BookMark);
  Sourse.EnableControls;
  CalcMode := not (SubModel = '-');
  InsertionGrid.Visible := CalcMode;
  Splitter.Visible := CalcMode;
  Insertions.EnableControls;
end;

procedure TDialogSetPrice.GridGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
var
  SourceCost: Currency;
  TargetCost: Currency;

begin
  if not (gdSelected in State) then
  begin
   if Column.FieldName = 'TargetCost' then
   begin
     SourceCost := Auto.FieldByName('SourceCost').AsCurrency;
     TargetCost := Auto.FieldByName('TargetCost').AsCurrency;
     if SourceCost > TargetCost then Background := clRed;
     if SourceCost < TargetCost then Background := clLime;
   end;
  end;
end;


procedure TDialogSetPrice.OnSet(Sender: TObject);
begin
  if Auto.State = dsEdit then Auto.Post;
  if Manual.State <> dsBrowse then Manual.Post;
  SetCost;
end;


procedure TDialogSetPrice.SetCost;
var
  AutoBookMark: TBookmark;
  ManualBookMark: TBookmark;
  Model: string;
  SubModel: string;
  Pattern: string;
  TargetCost: Currency;
  Formula: string;
  x: string;
  y: string;
  ADecimalSeparator: Char;


function Compare: Boolean;
var
  WildString: Char;
  WildChar: Char;
  MatchCase: Boolean;

function Matching(SearchString, Mask : string) : boolean;
var
   s, m   : string;
   ss     : string[1];
   c      : char;
begin
   s  := SearchString;
   m  := Mask;

   if not MatchCase
      then begin
             s := Uppercase(s);
             m := Uppercase(m);
   end;

   // Compare the two strings one character at a time until one or the
   // other is exhausted. For each character that matches, we truncate
   // both strings at the left.
   while (length(s) > 0) and (length(m) > 0) do begin

      // Take the first character from the mask.
      ss := copy(m,1,1);
      c := ss[1];

         // If the next mask character is wildchar, count the two characters
         // as matching; lop them off both strings.
      if c = WildChar then begin
                  delete(s,1,1);
                  delete(m,1,1);
                end

         // If the next character is a WildString, lop it off the
         // mask string, since it matches whatever follows.
         // Then keep calling this routine recursively
         // to see if what's left of the search string matches what
         // remains of the mask. If it doesn't, truncate the search
         // string at the left (the WildString character matches
         // those bytes, too) and repeat until either there's a match
         // or the search string is exhausted, which means the
         // WildString character has eaten the entire remaining
         // search string.
      else
         if c = WildString then begin
                   delete(m,1,1);
                   while (not Matching(s,m))
                               and (length(s) > 0) do
                       delete(s,1,1);
         end

         // Any other character must be an exact match. If not,
         // clear the search string to stop the loop, as a match
         // failure has been detected. This will be sealed below
         // because the search string is null but the mask string
         // is not.
         else if copy(s,1,1) = copy(m,1,1)
                 then begin
                        delete(s,1,1);
                        delete(m,1,1);
                 end
                 else s := '';
   end;

   // If the loop is ended, the strings have matched if they
   // are now both reduced to null or if the match string
   // is reduced to a single WildString character.
   result := ((length(s) = 0) and (length(m) = 0))
                or (m = WildString);
end;

begin
   WildString := '*';
   WildChar   := '?';
   MatchCase  := True;
   Result := Matching(Model, Pattern);
end;

begin
  AutoBookMark := Auto.GetBookmark;
  Auto.DisableControls;
  ManualBookMark := Manual.GetBookmark;
  Manual.DisableControls;
  Auto.First;

  while not Auto.Eof do   //���� �� �������� ���������
  begin
    Model := AutoModel.AsString;   //���
    SubModel := AutoSubModel.AsString;     //���2
    x := FloatToStr(AutoSourceCost.AsCurrency);
    y := FloatToStr(AutoTargetCost.AsCurrency);
    Manual.First;
    while not Manual.Eof do
    begin
      Pattern := ManualModel.AsString;
      if Compare then            //������� �������� ��� ������
      begin
        Auto.Edit;
        Formula := Trim(ManualFormula.AsString);
        if Formula = '' then TargetCost := 0
        else
        begin
          if Formula <> '?' then
          begin
            try
              Formula := AnsiReplaceStr(AnsiLowerCase(Formula), 'x', x);
              Formula := AnsiReplaceStr(AnsiLowerCase(Formula), 'y', y);
              Formula := AnsiReplaceStr(Formula, ',', '.');
              ADecimalSeparator := DecimalSeparator;
              DecimalSeparator := '.';
              try
                Calculator.Formula := Formula;
                TargetCost := Calculator.calc([]);
              finally
                DecimalSeparator := ADecimalSeparator;
              end;
            except
              TargetCost := -1;
              Manual.Edit;
              ManualFormula.AsString := '?';
              Manual.Post;
              break;
            end;
          end
          else TargetCost := -1;
        end;
        if TargetCost = 0 then
        begin
          AutoTargetCost.AsVariant := Null;
          AutoCheck.AsBoolean := False
        end
        else
        begin
          if TargetCost <> -1 then
          begin
            if SubModel = '-' then AutoTargetCost.AsCurrency := TargetCost
            else AutoTargetCost.AsCurrency := SmartRound(TargetCost);
            AutoCheck.AsBoolean := True;
          end;
        end;
        Auto.Post;
        Break;
      end
      else Manual.Next;
    end;
    Auto.Next;
  end;
  Manual.GotoBookmark(ManualBookMark);
  Manual.EnableControls;
  Auto.GotoBookmark(AutoBookMark);
  Auto.EnableControls;
end;

procedure TDialogSetPrice.ActionAppendUpdate(Sender: TObject);
begin
  ActionAppend.Enabled := Manual.Active and (Manual.State = dsBrowse);
end;

procedure TDialogSetPrice.ActionDeleteUpdate(Sender: TObject);
begin
  ActionDelete.Enabled := Manual.Active and (Manual.State = dsBrowse) and (Manual.RecordCount <> 0);
end;

procedure TDialogSetPrice.ActionEditUpdate(Sender: TObject);
begin
  ActionEdit.Enabled := Manual.Active and (Manual.State = dsBrowse) and (Manual.RecordCount <> 0);
end;

procedure TDialogSetPrice.ActionPostUpdate(Sender: TObject);
begin
  ActionPost.Enabled :=  Manual.Active and (Manual.State <> dsBrowse);
end;

procedure TDialogSetPrice.ActionCancelUpdate(Sender: TObject);
begin
  ActionCancel.Enabled := Manual.Active and (Manual.State <> dsBrowse);
end;

procedure TDialogSetPrice.ActionUpUpdate(Sender: TObject);
begin
  ActionUp.Enabled := false; //Manual.Active and (Manual.State = dsBrowse) and (Manual.RecordCount <> 0) and (Manual.RecNo <> 1);
end;

procedure TDialogSetPrice.ActionDownUpdate(Sender: TObject);
begin
  ActionDown.Enabled := false; //Manual.Active and (Manual.State = dsBrowse) and (Manual.RecordCount <> 0) and (Manual.RecNo <> Manual.RecordCount);
end;

procedure TDialogSetPrice.ActionSetUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := Auto.Active and (Auto.RecordCount <> 0);
  Enabled := Enabled and Manual.Active and (Manual.RecordCount <> 0);
  ActionSet.Enabled := Enabled;
end;

procedure TDialogSetPrice.ActionCalculateUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := Auto.Active and (Auto.RecordCount <> 0);
  ActionCalculate.Enabled :=  Enabled and CalcMode;
end;

procedure TDialogSetPrice.ActionAppendExecute(Sender: TObject);
begin
  Manual.Append;
end;

procedure TDialogSetPrice.ActionDeleteExecute(Sender: TObject);
begin
  Manual.Delete;
end;

procedure TDialogSetPrice.ActionEditExecute(Sender: TObject);
begin
  Manual.Edit;
end;

procedure TDialogSetPrice.ActionPostExecute(Sender: TObject);
begin
  Manual.Post;
end;

procedure TDialogSetPrice.ActionCancelExecute(Sender: TObject);
begin
  Manual.Cancel;
end;

procedure TDialogSetPrice.ActionUpExecute(Sender: TObject);
var
  Order: Integer;
begin
  if QuestionYesNoMessage('�������� �����') then
    begin
      Manual.DisableControls;
      Order := ManualOrder.AsInteger;
      Manual.Edit;
      ManualOrder.AsInteger := - Order;
      Manual.Post;
      Manual.FindKey([Order-1]);
      Manual.Edit;
      ManualOrder.AsInteger := Order;
      Manual.Post;
      Manual.FindKey([-Order]);
      Manual.Edit;
      ManualOrder.AsInteger := Order - 1;
      Manual.Post;
      Manual.FindKey([Order - 1]);
      Manual.EnableControls;
    end;
end;

procedure TDialogSetPrice.ActionDownExecute(Sender: TObject);
var
  Order: Integer;
begin
if QuestionYesNoMessage('�������� ����') then
  begin
    Manual.DisableControls;
    Order := ManualOrder.AsInteger;
    Manual.Edit;
    ManualOrder.AsInteger := - Order;
    Manual.Post;
    Manual.FindKey([Order+1]);
    Manual.Edit;
    ManualOrder.AsInteger := Order;
    Manual.Post;
    Manual.FindKey([-Order]);
    Manual.Edit;
    ManualOrder.AsInteger := Order + 1;
    Manual.Post;
    Manual.FindKey([Order - 1]);
    Manual.EnableControls;
  end;
end;

procedure TDialogSetPrice.ActionSetExecute(Sender: TObject);
begin
  SetCost;
end;

procedure TDialogSetPrice.ActionCalculateExecute(Sender: TObject);
begin
  CalculateCost;
end;

procedure TDialogSetPrice.ManualNewRecord(DataSet: TDataSet);
begin
  ManualOrder.AsInteger := Manual.RecordCount + 1;
end;

procedure TDialogSetPrice.ButtonCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TDialogSetPrice.ManualBeforePost(DataSet: TDataSet);
var
  Model: string;
  Formula: string;
begin
  Model := Trim(ManualModel.AsString);
  Formula :=  Trim(ManualFormula.AsString);
  if (Formula = '') then
  begin
     Formula := '0';
     ManualFormula.AsString := Formula;
  end;
  if (Model <> '') then
  begin
    if (Model[Length(Model)] <> '*') then
    begin
      Model := Model + '*';
      ManualModel.AsString := Model;
    end
  end
  else ManualModel.AsString := '*';
end;

procedure TDialogSetPrice.AutoBeforePost(DataSet: TDataSet);
var
  NewCheck: Boolean;
begin
  NewCheck := AutoCheck.AsBoolean;
  if NewCheck <> OldCheck then
  begin
    if NewCheck then Inc(CheckCount)
    else Dec(CheckCount);
  end;

  if CheckCount = 0 then Grid.Columns[0].Title.ImageIndex := 0 else
  if CheckCount = Auto.RecordCount then Grid.Columns[0].Title.ImageIndex := 1 else
  Grid.Columns[0].Title.ImageIndex := 2;
end;

procedure TDialogSetPrice.OkActionUpdate(Sender: TObject);
begin
  //ShowMessage
  OkAction.Enabled := Auto.Active and (CheckCount <> 0); //���������� �������� ������� � Grid <> 0
end;

procedure TDialogSetPrice.FormCreate(Sender: TObject);
begin
  GrayScaleImageList(EnabledImageList, DisabledImageList);
  FCosts := TClientDataSet.Create(Self);
end;

procedure TDialogSetPrice.OkActionExecute(Sender: TObject);
//var GoldPrice : Currency;
begin
  if QuestionYesNoMessage('�� �������, ��� ������ �������� ��������� ����') then
  begin
    ModalResult := mrOk;
//    ���������� ��������� ���� �� ������
//    GoldPrice := Costs.Lookup('ID', 1, 'Cost');
//    if not dmInv.taConst.Active then  dmInv.taConst.Open;
//    dmInv.taConst.Locate('Const_ID', 2, []);
//    if dmInv.taConstVAL.AsCurrency <> GoldPrice then
//      begin
//        dmInv.taConst.Edit;
//        dmInv.taConstVAL.AsCurrency := GoldPrice;
//        dmInv.taConst.Post;
//        dmInv.taConst.Close;
//      end;
  end;
end;

procedure TDialogSetPrice.AutoBeforeEdit(DataSet: TDataSet);
begin
  OldCheck := AutoCheck.AsBoolean;
end;

procedure TDialogSetPrice.ActionOpenUpdate(Sender: TObject);
begin
  ActionOpen.Enabled := True;
end;

procedure TDialogSetPrice.ActionSaveUpdate(Sender: TObject);
begin
  ActionSave.Enabled := Manual.Active and (Manual.RecordCount <> 0);
end;

function GetDataDirectory: string;
var
  ItemIDList: PItemIDList;
  DataDirectory: array[0..MAX_PATH] of Char;
begin
  SHGetSpecialFolderLocation(Application.Handle, CSIDL_APPDATA, ItemIDList);
  SHGetPathFromIDList(ItemIDList, @DataDirectory);
  Result := StrPas(@DataDirectory) + '\������������\';
  if not DirectoryExists(Result) then CreateDir(Result);
  Result := Result + '������� ���\';
  if not DirectoryExists(Result) then CreateDir(Result);
end;


procedure TDialogSetPrice.ActionOpenExecute(Sender: TObject);
begin
  OpenDialog.InitialDir := GetDataDirectory;
  if OpenDialog.Execute then
  begin
    Manual.LoadFromFile(OpenDialog.FileName);
    Manual.LogChanges := False;    
  end;
end;

procedure TDialogSetPrice.ActionSaveExecute(Sender: TObject);
begin
  with Manual do
    begin
      DisableControls;
      First;
      while not Eof do
        begin
          Edit;
          ManualOrder.AsInteger := Manual.RecNo;
          Post;
          Next;
        end;
      First;
      EnableControls;
    end;
  SaveDialog.InitialDir := GetDataDirectory;
  if SaveDialog.Execute then
  begin
    Manual.SaveToFile(SaveDialog.FileName, dfXML);
  end;
end;

procedure TDialogSetPrice.ActionEmptyUpdate(Sender: TObject);
begin
  ActionEmpty.Enabled := Manual.Active and (Manual.RecordCount <> 0);
end;

procedure TDialogSetPrice.ActionEmptyExecute(Sender: TObject);
begin
  if QuestionYesNoMessage('�������� ������� ��������� ���') then
  begin
    Manual.EmptyDataSet;
  end;
end;

procedure TDialogSetPrice.GridExit(Sender: TObject);
begin
  if Auto.State <> dsBrowse then Auto.Post;
end;

procedure TDialogSetPrice.GridManualExit(Sender: TObject);
begin
  if Manual.State <> dsBrowse then Manual.Post;
end;

function TDialogSetPrice.GetCost(ID: Integer): Currency;
begin
  if FCosts.Active then
  begin
    if FCosts.FindKey([ID]) then Result := FCosts.FieldByName('Cost').AsCurrency
    else Result := 0;
  end
  else Result := 0;    
end;

procedure TDialogSetPrice.FormDestroy(Sender: TObject);
begin
  FCosts.Free;
end;



procedure TDialogSetPrice.UpDownChangingEx(Sender: TObject;  var AllowChange: Boolean; NewValue: Smallint; Direction: TUpDownDirection);
begin
  if (NewValue >= UpDown.Min) and (NewValue <= UpDown.Max) then
  begin
    AllowChange := True;
    RoundEditor.Text := FloatToStr(Power(10, NewValue));
    RoundMiddleEditor.Text := FloatToStr(Power(10, NewValue) / 2);
  end
  else AllowChange := False;
end;

procedure TDialogSetPrice.AutoSourceDataChange(Sender: TObject; Field: TField);
var
  Insertions: TDataSet;
  SubModelID: Integer;
begin
  if Field = nil then
  begin
    SubModelID := AutoSubModelID.AsInteger;
    dmInv.CurrArt2Id := SubModelID;
    Insertions := dminv.taArtIns;
    Insertions.Active := False;
    Insertions.Active := True;
  end;
end;

procedure TDialogSetPrice.AutoWeightGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text := '-'
  else Text := Sender.AsString;
end;

end.


