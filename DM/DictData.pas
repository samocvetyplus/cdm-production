unit DictData;

interface                          

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ImgList, Db, pFIBQuery, AppEvnts, RxAppUtils, pFIBDataSet,
  DateUtils, ComDrv32, ADODB, xmldom, XMLIntf, msxmldom, XMLDoc,
  pFIBDatabase, FIBQuery, FIBDataSet, FIBDatabase, ExtCtrls,
  dxmdaset, MD5, FR_DSet, FR_DBSet, SIBEABase, SIBFIBEA, dbClient,
  Provider;

const
  Eps = 0.0001;

  PROD_DICT    = 1;
  MAT_DICT     = 2;
  MATGOOD_DICT = 256;
  SEMIS_DICT   = 4;
  INS_DICT     = 8;
  SELLER_DICT  = 16;
  COMP_DICT    = 32; // not used
  GOOD_DICT    = 64;
  PRILL_DICT   = 128;
  OPER_DICT    = 512;
  PS_DICT      = 1024;
  SZ_DICT      = 2048;
  OPERNOGOOD_DICT = 4096;
  WHPROD_DICT     = 8192;
  PSWHPROD_DICT   = 16384;
  MATINS_DICT     = 32768;
  ALL_DICT     = $FFFFFFF;


  ROOT_PROD_CODE    = -1;
  ROOT_MAT_CODE     = '*���';
  ROOT_MATGOOD_CODE = '*���';
  ROOT_SEMIS_CODE   = '*���';
  ROOT_INS_CODE     = '*���';
  ROOT_GOOD_CODE    = '*���';
  ROOT_GOODS_CODE   = -1;
  ROOT_SELLER_CODE  = -1;
  ROOT_COMP_CODE    = -1;
  ROOT_PRILL_CODE   = -1;
  ROOT_ART_CODE     = '*���';
  ROOT_ART2_CODE    = -1;
  ROOT_OPER_CODE    = '*���';
  ROOT_OPERNOGOOD_CODE = '*���';
  ROOT_PS_CODE      = '*���';
  ROOT_SZ_CODE      = $FFFF;
  ROOT_KNAME_CODE   = '*���';
  ROOT_MATINS_CODE  = '*���';

  MINSTR = Low(Char);
  MAXSTR = High(Char);

  GOLD_MATERIAL_ID : AnsiString = '1';
  SILVER_MATERIAL_ID : AnsiString = '3';

  BeginDateByDefault = '01.07.2002';

  sNoOperation = '�� ����������';
  sExpTolligPrefix = '������� � ';

  { color }
  clCloseDoc = clBtnFace;
  clOpenDoc  = clWindow;

  wtFromDep : set of byte = [0, 6];
  wtToDep   : set of byte = [1, 3, 5, 7];

type
  TDocPreview = (pvDesign, pvPreview, pvPrint);
  TBarCode_Model = (bcmEZ1100);
  TScale_Model = (scmE410M);

  TUserInfo = record
    UserId    : integer;
    FIO       : string;
    FName     : string;
    LName     : string;
    MName     : string;
    Alias     : string;
    Pswd      : string;
    DepId     : integer;
    PrwKind   : integer;
    BeginDate : TDateTime;
    EndDate   : TDateTime;
    SheetNo   : string;
    ShowKindDoc : integer;
    AccSubSys : integer;
    AccProd   : integer; // ������ � �������� ������������
    AccProd_d : integer; // �������� / �������������� ��� ������������

    AccDict   : integer;
    AccWh     : integer;
    AccAppl   : integer;
    AccMat    : integer;
    wWhId     : integer; // �������� �� ������� ��������� �������
    wWhName   : string;
    wWhMatID    : integer;
    Transform_DepId : integer; // ��� �������������
    Transform_MolId : integer; // ��� �������������
    Profile : integer;  // -1 �������������
                        //  0 ������������
                        //  1 ��������� ������������
                        //  2 ��������� ������ ������� ���������
                        //  3 ��������
                        //  4 ���������
    LogId : integer;  // ������ �� ������ � ����������� � ���������
    SelfCompId : integer; // ������ �� ����������� ��� ������� �������� ���������
    SelfCompName : string; // ������������ �����������
    UseTolling : boolean; // ������������ �������
    TollingCompId : integer; // ������ �� ����������� ��� ������� ����������� ��������� ��������
    TollingCompName : string;
    TollingType : integer; // 0 - ��������, 1 - ���������
  end;

  PDepInfo = ^TDepInfo;
  TDepInfo = record
    DepId   : integer;
    SortInd : integer;
    Name    : string;
    SName   : string;
    Color   : TColor;
    Ps      : boolean; // ����� ��������
    Wh      : boolean; // ����� ������� ���������
    PWh     : boolean; // �������� �� ������������
    Wp      : boolean; // ������� �����
    DepTypes : integer;
    Access: Boolean;
    Enabled: Boolean;
       // (DepTypes and $1)=$1 - ����� ��������
       // (DepTypes and $2)=$2 - �������� �� ������������
       // (DepTypes and $4)=$4 - ����� ������� ���������
       // (DepTypes and $8)=$8 - ����� ��������������
       // (DepTypes and $10)=$10(0) - ������������� �������������� (��������)
  end;

  TLocalSetting = record
    UseStoneDialog : boolean;
    Cassa_UseNDS : boolean;
    Cassa_NDS : double;
    Cassa_Port : string; // COM1, COM2
    BarCode_Use : boolean;
    BarCode_Model : TBarCode_Model;
    BarCode_Port : byte; // 0=LPT1, 1=COM1, 2=COM2, 3=COM3, 4=COM4, 5=LPT2. default=0
    BarCode_Count : integer; // ���-�� ������������� �������� �� ��������
    BarCode_Step : string; // �������� ��� ����������� ����� ������ � �����
    Scale_Use : boolean;
    Scale_Model : TScale_Model;
    Scale_Port : byte; // 0=COM1, 1=COM2, 2=COM3, 3=COM4. default=0
    ScaleClass: Integer;
    BarCodeScaner_Use : boolean;
    BarCodeScaner_Port : byte; // 0=COM1, 1=COM2, 2=COM3, 3=COM4. default=0
  end;

  TNotif = record
    Notif_IP : string;
    Notif_Port : string;
    Notif_Use : boolean;
  end;

  TRec = record
    DefaultContractId : integer;
    DefaultProdContractId : integer;
    DefaultInsOwner : Variant;
    DefaultInsOwnerName : string;
    NormaOrderDate : Variant;
    ZpRateOrderDate : Variant;
    Sticker : TStringList;
    Sticker_OTK : TBitmap;
    ChangeApplOperId: Variant;
    ExeUrl : string;
  end;

  EInternal = class(Exception);

  Tdm = class(TDataModule)
    ilButtons: TImageList;
    db: TpFIBDatabase;
    tr: TpFIBTransaction;
    taOper: TpFIBDataSet;
    dsrOper: TDataSource;
    taOperRecNo: TIntegerField;
    taSemis: TpFIBDataSet;
    dsrSemis: TDataSource;
    dsrMOL: TDataSource;
    taSemisSEMIS: TFIBStringField;
    taSemisSNAME: TFIBStringField;
    taSemisRecNo: TIntegerField;
    quTmp: TpFIBQuery;
    quGetID: TpFIBQuery;
    taDep: TpFIBDataSet;
    dsrDep: TDataSource;
    taComp: TpFIBDataSet;
    dsrComp: TDataSource;
    taCompCODE: TFIBStringField;
    taCompSNAME: TFIBStringField;
    taCompPOSTINDEX: TFIBStringField;
    taCompCOUNTRY: TFIBStringField;
    taCompCITY: TFIBStringField;
    taCompADDRESS: TFIBStringField;
    taCompPHONE: TFIBStringField;
    taCompFAX: TFIBStringField;
    taCompEMAIL: TFIBStringField;
    taCompWWW: TFIBStringField;
    taCompBOSSFIO: TFIBStringField;
    taCompBOSSPHONE: TFIBStringField;
    taCompBUHFIO: TFIBStringField;
    taCompBUHPHONE: TFIBStringField;
    taCompOFIO: TFIBStringField;
    taCompOFIOPHONE: TFIBStringField;
    taCompINN: TFIBStringField;
    taCompBIK: TFIBStringField;
    taCompOKPO: TFIBStringField;
    taCompOKONH: TFIBStringField;
    taCompBANK: TFIBStringField;
    taCompBILL: TFIBStringField;
    taCompKBANK: TFIBStringField;
    taCompKBILL: TFIBStringField;
    taCompADRBILL: TFIBStringField;
    taCompPASPSER: TFIBStringField;
    taCompPASPNUM: TFIBStringField;
    taCompDISTRDATE: TDateTimeField;
    taCompDISTRPLACE: TFIBStringField;
    taCompNDOG: TFIBStringField;
    taCompDOGBD: TDateTimeField;
    taCompDOGED: TDateTimeField;
    taCompCERTN: TFIBStringField;
    taCompCERTDATE: TDateTimeField;
    taCompREUED: TDateTimeField;
    taCompSELLER: TSmallintField;
    taCompBUYER: TSmallintField;
    taCompLAWFACE: TSmallintField;
    taCompPHISFACE: TSmallintField;
    taCompRecNo: TIntegerField;
    taCompCOMPID: TIntegerField;
    taRec: TpFIBDataSet;
    dsrRec: TDataSource;
    taRecRECID: TIntegerField;
    taRecCOMPID: TIntegerField;
    taRecNAME: TFIBStringField;
    taDepDEPID: TIntegerField;
    taDepPARENT: TIntegerField;
    taDepCHILD: TIntegerField;
    taDepCOLOR: TIntegerField;
    quDepT: TpFIBQuery;
    taDepNAME: TFIBStringField;
    taDepSNAME: TFIBStringField;
    taCompNAME: TFIBStringField;
    taSemisSEMISID: TFIBStringField;
    quDep: TpFIBDataSet;
    quDepDEPID: TIntegerField;
    quDepNAME: TFIBStringField;
    quDepSNAME: TFIBStringField;
    quDepCOLOR: TIntegerField;
    aplev: TApplicationEvents;
    taSemisUQ: TSmallintField;
    taUnit: TpFIBDataSet;
    taUnitU: TFIBStringField;
    dsrUnit: TDataSource;
    taUnitUNITID: TFIBStringField;
    taSemisUW: TSmallintField;
    taMOL: TpFIBDataSet;
    taMOLMOLID: TIntegerField;
    taMOLFNAME: TFIBStringField;
    taMOLLNAME: TFIBStringField;
    taMOLMNAME: TFIBStringField;
    taMOLPSWD: TFIBStringField;
    taMOLSHEETNO: TSmallintField;
    taMOLRecNo: TIntegerField;
    taMOLDEPID: TIntegerField;
    quRecTmp: TpFIBQuery;
    trRec: TpFIBTransaction;
    taMOLDepName: TStringField;
    taArt: TpFIBDataSet;
    dsrArt: TDataSource;
    taArtRecNo: TIntegerField;
    taNDS: TpFIBDataSet;
    dsrNDS: TDataSource;
    taNDSNDSID: TIntegerField;
    taNDSNDS: TFloatField;
    taNDSNAME: TFIBStringField;
    taMOLDOCPREVIEW: TSmallintField;
    taMOLSHOWKINDDOC: TSmallintField;
    taTails: TpFIBDataSet;
    taTailsID: TIntegerField;
    taTailsNAME: TFIBStringField;
    dsrTails: TDataSource;
    taTailsRecNo: TIntegerField;
    taSemisQ0: TFloatField;
    taPrill: TpFIBDataSet;
    dsrPrill: TDataSource;
    taPrillID: TIntegerField;
    taPrillPRILL: TFIBStringField;
    taPrillQ0: TFloatField;
    taPrillPURE: TFloatField;
    taPrillSORTIND: TIntegerField;
    taPrillRecNo: TIntegerField;
    taMat: TpFIBDataSet;
    taCompSORTIND: TIntegerField;
    dsrMat: TDataSource;
    taMatNAME: TFIBStringField;
    taMatSNAME: TFIBStringField;
    taMatSORTIND: TIntegerField;
    taMatRecNo: TIntegerField;
    taNDSRecNo: TIntegerField;
    taNDSSORTIND: TSmallintField;
    taNDSLONGNAME: TFIBStringField;
    taDiscount: TpFIBDataSet;
    taDiscountD_DISCOUNTID: TIntegerField;
    taDiscountNAME: TFIBStringField;
    taDiscountSNAME: TFIBStringField;
    taDiscountDISCOUNT: TFloatField;
    taDiscountSORTIND: TIntegerField;
    taDiscountRecNo: TIntegerField;
    dsrDiscount: TDataSource;
    taEdgShape: TpFIBDataSet;
    taEdgT: TpFIBDataSet;
    dsrEdgShape: TDataSource;
    dsrEdgT: TDataSource;
    taEdgShapeNAME: TFIBStringField;
    taEdgShapeSORTIND: TIntegerField;
    taEdgTNAME: TFIBStringField;
    taEdgTSORTIND: TIntegerField;
    taIns: TpFIBDataSet;
    dsrIns: TDataSource;
    taInsD_INSID: TFIBStringField;
    taInsNAME: TFIBStringField;
    taInsSNAME: TFIBStringField;
    taInsSORTIND: TSmallintField;
    taInsUNITID: TSmallintField;
    taInsEDGSHID: TFIBStringField;
    taInsEDGTID: TFIBStringField;
    taInsGR: TSmallintField;
    taEdgShapeRecNo: TIntegerField;
    taEdgTRecNo: TIntegerField;
    taInsRecNo: TIntegerField;
    taSemisMATNAME: TFIBStringField;
    taMOLSORTIND: TIntegerField;
    taArtARTID: TIntegerField;
    taArtCOMPID: TIntegerField;
    taArtSEMIS: TFIBStringField;
    taArtINSID: TFIBStringField;
    taArtART: TFIBStringField;
    taArtMEMO: TMemoField;
    taArtPICT: TBlobField;
    taArtFULLART: TFIBStringField;
    taDepart: TpFIBDataSet;
    quUser: TpFIBDataSet;
    taArtSEMISNAME: TStringField;
    taArtINSNAME: TStringField;
    taArtPRILLID: TIntegerField;
    taMatID: TFIBStringField;
    taArtMATNAME: TStringField;
    taArtPRILLNAME: TStringField;
    taEdgShapeID: TFIBStringField;
    taEdgTID: TFIBStringField;
    taSemisMAT: TFIBStringField;
    taArtMATID: TFIBStringField;
    taSemisGOOD: TSmallintField;
    taSemisPRILL: TIntegerField;
    taSemisPRILLNAME: TStringField;
    taArtUNITID: TSmallintField;
    taGood: TpFIBDataSet;
    taIOMol: TpFIBDataSet;
    taPS: TpFIBDataSet;
    dsrPs: TDataSource;
    taPSID: TIntegerField;
    taPSNAME: TFIBStringField;
    taPSMOLID: TIntegerField;
    taPSDEPID: TIntegerField;
    taPSMOLNAME: TStringField;
    taPSRecNo: TIntegerField;
    taPSISWORK: TSmallintField;
    taSz: TpFIBDataSet;
    dsrSz: TDataSource;
    taSzID: TIntegerField;
    taSzNAME: TFIBStringField;
    taSzSORTIND: TIntegerField;
    taSzRecNo: TIntegerField;
    taMatGOOD: TSmallintField;
    taMOLACCDICT: TIntegerField;
    taMOLACCSUBSYS: TIntegerField;
    taArt2: TpFIBDataSet;
    dsrArt2: TDataSource;
    taArt2ART2ID: TIntegerField;
    taArt2D_ARTID: TIntegerField;
    taArt2ART2: TFIBStringField;
    taA2Ins: TpFIBDataSet;
    dsrA2Ins: TDataSource;
    taA2InsID: TIntegerField;
    taA2InsART2ID: TIntegerField;
    taA2InsINSID: TFIBStringField;
    taA2InsQUANTITY: TSmallintField;
    taA2InsWEIGHT: TFloatField;
    taA2InsMAIN: TSmallintField;
    taA2InsEDGTYPEID: TFIBStringField;
    taA2InsEDGSHAPEID: TFIBStringField;
    taA2InsINSNAME: TStringField;
    taA2InsEDGTNAME: TStringField;
    taA2InsEDGSHAPENAME: TStringField;
    taSIns: TpFIBDataSet;
    dsrSIns: TDataSource;
    taSInsSEMISID: TFIBStringField;
    taSInsSEMIS: TFIBStringField;
    taColor: TpFIBDataSet;
    dsrColor: TDataSource;
    dsrCleannes: TDataSource;
    taCleannes: TpFIBDataSet;
    taChromaticity: TpFIBDataSet;
    dsrChromaticity: TDataSource;
    dsrIGr: TDataSource;
    taIGr: TpFIBDataSet;
    taColorID: TFIBStringField;
    taColorNAME: TFIBStringField;
    taColorSORTIND: TIntegerField;
    taColorRecNo: TIntegerField;
    taCleannesID: TFIBStringField;
    taCleannesNAME: TFIBStringField;
    taCleannesSORTIND: TIntegerField;
    taCleannesRecNo: TIntegerField;
    taChromaticityID: TFIBStringField;
    taChromaticityNAME: TFIBStringField;
    taChromaticitySORTIND: TIntegerField;
    taChromaticityRecNo: TIntegerField;
    taIGrID: TFIBStringField;
    taIGrNAME: TFIBStringField;
    taIGrSORTIND: TIntegerField;
    taIGrRecNo: TIntegerField;
    taMOLACCPROD: TIntegerField;
    taMOLACCAPPL: TIntegerField;
    taMOLACCWH: TIntegerField;
    taSemisEDGSHAPEID: TFIBStringField;
    taSemisEDGTID: TFIBStringField;
    taSemisCLEANNES: TFIBStringField;
    taSemisCHROMATICITY: TFIBStringField;
    taSemisIGR: TFIBStringField;
    taSemisCOLOR: TFIBStringField;
    taA2InsCOLOR: TFIBStringField;
    taA2InsCHROMATICITY: TFIBStringField;
    taA2InsCLEANNES: TFIBStringField;
    taA2InsIGR: TFIBStringField;
    taMatISINS: TSmallintField;
    taSemisSZ: TFIBStringField;
    taSemisO1: TFIBStringField;
    taA2InsC_COLOR: TStringField;
    taA2InsC_EDGTYPEID: TStringField;
    taA2InsC_EDGSHAPEID: TStringField;
    taA2InsC_CHROMATICITY: TStringField;
    taA2InsC_CLEANNES: TStringField;
    taA2InsC_IGR: TStringField;
    taArtK: TFloatField;
    taSemisPRICE: TFloatField;
    taDict: TpFIBDataSet;
    taDictID: TIntegerField;
    taDictDICTID: TIntegerField;
    taDictDICTNAME: TFIBStringField;
    taDictIDNAME: TFIBStringField;
    taDictNAME: TFIBStringField;
    taDictDISPLAYNAME: TFIBStringField;
    taCodeN: TpFIBDataSet;
    taCodeNN: TSmallintField;
    taCode: TpFIBDataSet;
    dsrCode: TDataSource;
    taCodeVALNAME: TStringField;
    taDictCLDATASET: TFIBStringField;
    taCodeNDICT: TIntegerField;
    taCodeVAL: TFIBStringField;
    taCodeID: TIntegerField;
    taCodeN2: TSmallintField;
    taCodeDICT: TIntegerField;
    taPsOper: TpFIBDataSet;
    dsrPsOper: TDataSource;
    taPsOperID: TIntegerField;
    taPsOperOPERID: TFIBStringField;
    taPsOperDEPID: TIntegerField;
    taPsOperOPERNAME: TStringField;
    taPsOperRecNo: TIntegerField;
    taPsOperDEPNAME: TStringField;
    taArtCode: TpFIBDataSet;
    taArtCodeID: TIntegerField;
    taArtCodeN: TSmallintField;
    taArtCodeVAL: TFIBStringField;
    quInsArt: TpFIBQuery;
    taArtCodeDICT: TIntegerField;
    taArtCodeK: TFIBStringField;
    taCodeK: TFIBStringField;
    taOperIn: TpFIBDataSet;
    taOperOut: TpFIBDataSet;
    taOperInID: TIntegerField;
    taOperInOPERID: TFIBStringField;
    taOperInSEMISID: TFIBStringField;
    taOperInOPERATION: TFIBStringField;
    taOperInSEMISNAME: TStringField;
    taOperOutID: TIntegerField;
    taOperOutOPERID: TFIBStringField;
    taOperOutSEMISID: TFIBStringField;
    taOperOutOPERATION: TFIBStringField;
    taOperOutSEMISNAME: TStringField;
    dsrOperIn: TDataSource;
    dsrOperOut: TDataSource;
    taOperInMATID: TFIBStringField;
    taOperInMATNAME: TStringField;
    taOperInRecNo: TIntegerField;
    taOperOutRecNo: TIntegerField;
    taMOLACCMAT: TIntegerField;
    taDepDEPTYPES: TIntegerField;
    taOperTail: TpFIBDataSet;
    dsrOperTail: TDataSource;
    taOperTailID: TIntegerField;
    taOperTailOPERID: TFIBStringField;
    taOperTailSEMISID: TFIBStringField;
    taOperTailOPERATION: TFIBStringField;
    taOperTailRecNo: TIntegerField;
    taOperTailSEMISNAME: TStringField;
    taArtMW: TFloatField;
    dsrConst: TDataSource;
    taConst: TpFIBDataSet;
    taConstNAME: TFIBStringField;
    taConstVAL: TFloatField;
    taConstCONST_ID: TIntegerField;
    taMOLTRANSFORM_DEPID: TIntegerField;
    taMOLTRANSFORM_MOLID: TIntegerField;
    taMOLTRANSFORMDEPNAME: TFIBStringField;
    taOperRej: TpFIBDataSet;
    dsrOperRej: TDataSource;
    taOperRejID: TIntegerField;
    taOperRejOPERID: TFIBStringField;
    taOperRejSEMISID: TFIBStringField;
    taOperRejOPERATION: TFIBStringField;
    taOperRejSEMISNAME: TStringField;
    taOperRejRecNo: TIntegerField;
    taMOLACCPROD_D: TIntegerField;
    taCompADATE: TDateTimeField;
    taCodePCOEF: TFloatField;
    taMatALG_CALCPRICE: TSmallintField;
    taTech: TpFIBDataSet;
    dsrTech: TDataSource;
    taTechID: TIntegerField;
    taTechNAME: TFIBStringField;
    dsrDepart: TDataSource;
    taDepartDEPID: TIntegerField;
    taDepartCOLOR: TIntegerField;
    taDepartSNAME: TFIBStringField;
    taDepartNAME: TFIBStringField;
    taDepartSORTIND: TIntegerField;
    taDepartPS: TSmallintField;
    taDepartWH: TSmallintField;
    taDepartPWH: TSmallintField;
    taDepartDEPTYPES: TIntegerField;
    taA2InsRecNo: TIntegerField;
    taTechRecNo: TIntegerField;
    taTransMat: TpFIBDataSet;
    dsrTransMat: TDataSource;
    taTransMatID: TIntegerField;
    taTransMatOPERID: TFIBStringField;
    taTransMatMATID: TFIBStringField;
    taTransMatBASEMAT: TFIBStringField;
    taTransMatMATNAME: TStringField;
    taMatPRICE_K: TFloatField;
    dsrZPK: TDataSource;
    taZPK: TpFIBDataSet;
    taZPKARTID: TIntegerField;
    taZPKOPERID: TFIBStringField;
    taZPKART: TFIBStringField;
    taZPKID: TIntegerField;
    taZPKK: TFloatField;
    taZPKARTK: TFloatField;
    taZPKKT: TIntegerField;
    taTechSORTIND: TIntegerField;
    taSemisUSEINART: TIntegerField;
    taSemisTYPES: TIntegerField;
    taTailsT: TIntegerField;
    taTailsS: TIntegerField;
    taTailsSORTIND: TIntegerField;
    taMOLPROFILE: TSmallintField;
    taOperOPERID: TFIBStringField;
    taOperOPERATION: TFIBStringField;
    taOperSOPER: TFIBStringField;
    taOperRATE: TFloatField;
    taOperU: TFIBStringField;
    taOperS: TFloatField;
    taOperTAILS: TIntegerField;
    taOperN: TFloatField;
    taOperNQ: TSmallintField;
    taOperZT: TSmallintField;
    taOperGR: TIntegerField;
    taOperSORTIND: TIntegerField;
    taOperLAST: TSmallintField;
    taOperOPERTYPES: TIntegerField;
    taOperN1: TFloatField;
    taOperA: TSmallintField;
    taOperUQ: TSmallintField;
    taZPKMW: TFloatField;
    taArtSEMIS_PROD: TFIBStringField;
    taArtSEMISPRODNAME: TStringField;
    taSemisAff: TpFIBDataSet;
    dsrSemisAff: TDataSource;
    taSemisAffID: TIntegerField;
    taSemisAffNAME: TFIBStringField;
    taSemisAffSORTIND: TIntegerField;
    taSemisAFFINAJ: TIntegerField;
    taRej: TpFIBDataSet;
    dsrRej: TDataSource;
    taRejID: TIntegerField;
    taRejNAME: TFIBStringField;
    taRejTYPES: TIntegerField;
    taRejPSID: TIntegerField;
    taRejPSIDMUSTHAVEVALUE: TIntegerField;
    taRejSORTIND: TIntegerField;
    taRejPSNAME: TStringField;
    taAIns: TpFIBDataSet;
    dsrAIns: TDataSource;
    taAInsID: TIntegerField;
    taAInsINSID: TFIBStringField;
    taAInsARTID: TIntegerField;
    taAInsQUANTITY: TSmallintField;
    taAInsWEIGHT: TFloatField;
    taAInsMAIN: TSmallintField;
    taAInsINSNAME: TStringField;
    taAInsEDGTYPEID: TStringField;
    taAInsEDGSHAPEID: TStringField;
    taAInsCLEANNES: TStringField;
    taAInsCOLOR: TStringField;
    taAInsCHROMATICITY: TStringField;
    taAInsIGR: TStringField;
    taOperTailMATID: TFIBStringField;
    taOperTailMATNAME: TStringField;
    taOperOutMATID: TFIBStringField;
    taOperOutMATNAME: TStringField;
    taOperRejMATID: TFIBStringField;
    taOperRejMATNAME: TStringField;
    taSemisBUH0: TFloatField;
    taSemisBUHU: TSmallintField;
    taSemisBUHPRICE: TFloatField;
    taOperTAILSNAME: TStringField;
    taRecPRODREP_BD: TDateTimeField;
    taRecPRODREP_ED: TDateTimeField;
    taRecNAME1: TFIBStringField;
    taRecSTONEBD: TDateTimeField;
    taInitStone: TpFIBDataSet;
    dsrInitStone: TDataSource;
    taInitStoneID: TIntegerField;
    taInitStoneSEMISID: TFIBStringField;
    taInitStoneQ: TIntegerField;
    taInitStoneW: TFloatField;
    taInitStoneSEMISNAME: TStringField;
    taArtHVENZA: TFloatField;
    taArtNDETAIL: TFloatField;
    taSemisISUSE: TIntegerField;
    taTransMatN: TFloatField;
    taTransMatNT: TSmallintField;
    taInitStoneADATE: TDateTimeField;
    taPaytype: TpFIBDataSet;
    dsrPaytype: TDataSource;
    taPaytypePAYTYPEID: TIntegerField;
    taPaytypeNAME: TFIBStringField;
    taPaytypeEXTRA: TFloatField;
    taInitStoneUQ: TFIBStringField;
    taInitStoneUW: TSmallintField;
    taRecX_MATRIXPROD: TSmallintField;
    taRecY_MATRIXPROD: TSmallintField;
    quGetId2: TpFIBQuery;
    taArtSAMID: TFIBStringField;
    taNDSID1C: TFIBStringField;
    taSzDiamond: TpFIBDataSet;
    dsrSzDiamond: TDataSource;
    taSzDiamondSZ: TFIBStringField;
    taArtDIAMOND: TFIBStringField;
    taClient: TpFIBDataSet;
    dsrClient: TDataSource;
    taClientCLIENTID: TFIBIntegerField;
    taClientFNAME: TFIBStringField;
    taClientMNAME: TFIBStringField;
    taClientLNAME: TFIBStringField;
    taClientADDRESS: TFIBStringField;
    taRecNDSID: TFIBIntegerField;
    taCompKPP: TFIBStringField;
    taRecNOTIF_IP: TFIBStringField;
    taRecNOTIF_PORT: TFIBStringField;
    taRecNOTIF_USE: TFIBSmallIntField;
    taContract: TpFIBDataSet;
    dsrContract: TDataSource;
    taContractID: TFIBIntegerField;
    taContractNAME: TFIBStringField;
    taContractSNAME: TFIBStringField;
    taContractSORTIND: TFIBIntegerField;
    taContractT0: TFIBSmallIntField;
    taRecDEFAULTCONTRACTID: TFIBIntegerField;
    taRecDEFAULTPRODCONTRACTID: TFIBIntegerField;
    trDateTime: TpFIBTransaction;
    quDateTime: TpFIBQuery;
    taInvColor: TpFIBDataSet;
    dsrInvColor: TDataSource;
    taInvColorID: TFIBIntegerField;
    taInvColorINVTYPEID: TFIBIntegerField;
    taInvColorCOLOR: TFIBIntegerField;
    taInvColorNAME: TFIBStringField;
    taInvColorSORTIND: TFIBIntegerField;
    taInvType: TpFIBDataSet;
    taInvTypeID: TFIBIntegerField;
    taInvTypeNAME: TFIBStringField;
    taInvTypeT: TFIBIntegerField;
    taInvTypeSNAME: TFIBStringField;
    taInvTypeGROUP_1C: TFIBStringField;
    dsrInvType: TDataSource;
    taInvColorINVTYPENAME: TStringField;
    taNDST: TFIBSmallIntField;
    taRecDBPATH: TFIBStringField;
    taTransMatK: TFIBFloatField;
    taRecEXTDBPATH: TFIBStringField;
    taA2InsCOMPID: TFIBIntegerField;
    taA2InsCOMPNAME: TFIBStringField;
    taA2InsCOMPNAME2: TFIBStringField;
    tmDemo: TTimer;
    taOperHIST_N: TFIBFloatField;
    taOperHIST_N1: TFIBFloatField;
    taOperHIST_TAILS: TFIBIntegerField;
    taOperHIST_NQ: TFIBSmallIntField;
    taOperHIST_RATE: TFIBFloatField;
    taOperHIST_ZT: TFIBSmallIntField;
    taOperHIST_TAILSNAME: TStringField;
    taCompNO_GOVREG: TFIBStringField;
    taCompD_GOVREG: TFIBDateTimeField;
    taCompNO_FISLPLACE: TFIBStringField;
    taCompD_FISLPLACE: TFIBDateTimeField;
    taCompOwner: TpFIBDataSet;
    dsrCompOwner: TDataSource;
    taCompCOMMENT: TMemoField;
    taCompOwnerID: TFIBIntegerField;
    taCompOwnerSELFCOMPID: TFIBIntegerField;
    taCompOwnerCOMPID: TFIBIntegerField;
    taCompSelf: TpFIBDataSet;
    dsrCompSelf: TDataSource;
    taCompSelfCOMPID: TFIBIntegerField;
    taCompSelfNAME: TFIBStringField;
    taCompOwnerSELFCOMPNAME: TStringField;
    taCompOwnerNO_CONTRACT: TFIBStringField;
    taCompOwnerBD_CONTRACT: TFIBDateTimeField;
    taCompOwnerED_CONTRACT: TFIBDateTimeField;
    taCompFACTADDRESS: TFIBStringField;
    taCompPOSTADDRESS: TFIBStringField;
    taCompOwnerREUDATE: TFIBDateTimeField;
    taArtAPPLOPERID: TFIBStringField;
    taArtAPPLOPERNAME: TStringField;
    taArtRoot: TpFIBDataSet;
    dsrArtRoot: TDataSource;
    taArtRootID: TFIBIntegerField;
    taArtRootARTID: TFIBIntegerField;
    taArtRootMASK: TFIBStringField;
    quLog: TpFIBQuery;
    trLog: TpFIBTransaction;
    taMatN: TFIBFloatField;
    taMatBUH0: TFIBFloatField;
    taRecSTICKER: TMemoField;
    taArtSTICKER_COMMENT: TFIBStringField;
    taRecSTICKER_OTK: TFIBBlobField;
    taSemisREJ: TFIBIntegerField;
    quGetId3: TpFIBQuery;
    taPaytypeSORTIND: TFIBIntegerField;
    taPaytypeAPPLT: TFIBSmallIntField;
    taRecCHANGEAPPLOPERID: TFIBStringField;
    taSemisFINISHTRANSFORM: TFIBIntegerField;
    taCompLogin: TpFIBDataSet;
    taCompLoginCOMPID: TFIBIntegerField;
    taCompLoginNAME: TFIBStringField;
    quGetId4: TpFIBQuery;
    taRecEXEPATH: TFIBStringField;
    taWhatsNew: TpFIBDataSet;
    taPaytypeRecNo: TIntegerField;
    taSzDiamondRecNo: TIntegerField;
    taClientRecNo: TIntegerField;
    taSemisAffRecNo: TIntegerField;
    taContractRecNo: TIntegerField;
    taArtPICTWIDTH: TFIBIntegerField;
    taArtPICTHEIGHT: TFIBIntegerField;
    taOperZBONUS: TFIBSmallIntField;
    taOperHIST_ZBONUS: TFIBSmallIntField;
    taCompDOCBAD_MEMO: TFIBStringField;
    taCompOwnerINFINITE_CONTRACT: TFIBSmallIntField;
    taUpdComponent: TpFIBDataSet;
    taUpdComponentNAME: TFIBStringField;
    taUpdComponentURL: TFIBStringField;
    taUpdComponentVERSION: TFIBStringField;
    taUpdComponentCLIENTLOCATION: TFIBStringField;
    taUpdComponentNEEDREG: TFIBIntegerField;
    taCompCOLOR: TFIBIntegerField;
    taMOLFIO: TFIBStringField;
    taArtstoneid: TFIBStringField;
    taArtSTONE: TFIBStringField;
    taArtSTONENAME: TStringField;
    taInsBasic: TpFIBDataSet;
    taInsAdditional: TpFIBDataSet;
    taInsBasicID: TFIBStringField;
    taInsBasicTITLE: TFIBStringField;
    taInsBasicISBASIC: TFIBSmallIntField;
    taInsAdditionalID: TFIBStringField;
    taInsAdditionalTITLE: TFIBStringField;
    taOperID: TFIBIntegerField;
    taUserDepartments: TpFIBDataSet;
    taUserDepartmentsDEPID: TFIBIntegerField;
    taUserDepartmentsDEPNAME: TFIBStringField;
    taOperNS: TFIBFloatField;
    taOperOPERATIONFLAG: TFIBIntegerField;
    dsOperationFlag: TdxMemData;
    dsOperationFlagID: TIntegerField;
    dsOperationFlagName: TStringField;
    dsrOperationFlag: TDataSource;
    taOperFlagName: TStringField;
    taDepartAccess: TFIBIntegerField;
    quUserMOLID: TFIBIntegerField;
    quUserFNAME: TFIBStringField;
    quUserLNAME: TFIBStringField;
    quUserMNAME: TFIBStringField;
    quUserFIO: TFIBStringField;
    quUserPSWD: TFIBStringField;
    quUserSHEETNO: TFIBSmallIntField;
    quUserDEPID: TFIBIntegerField;
    quUserBD: TFIBDateTimeField;
    quUserED: TFIBDateTimeField;
    quUserDOCPREVIEW: TFIBSmallIntField;
    quUserISWORK: TFIBSmallIntField;
    quUserSHOWKINDDOC: TFIBSmallIntField;
    quUserSORTIND: TFIBIntegerField;
    quUserIO: TFIBSmallIntField;
    quUserALIAS: TFIBStringField;
    quUserADM: TFIBSmallIntField;
    quUserACCDICT: TFIBIntegerField;
    quUserACCSUBSYS: TFIBIntegerField;
    quUserACCPROD: TFIBIntegerField;
    quUserACCWH: TFIBIntegerField;
    quUserACCAPPL: TFIBIntegerField;
    quUserACCMAT: TFIBIntegerField;
    quUserTRANSFORM_DEPID: TFIBIntegerField;
    quUserTRANSFORM_MOLID: TFIBIntegerField;
    quUserACCPROD_D: TFIBIntegerField;
    quUserPROFILE: TFIBSmallIntField;
    frComp: TfrDBDataSet;
    taOperMATERIALID: TFIBStringField;
    taOperMATERIALNAME: TStringField;
    taCompOwnerCONTRACTMATERIAL: TFIBSmallIntField;
    taCompOwnerMaterialName: TStringField;
    taArtProducerName: TStringField;
    taProducer: TpFIBDataSet;
    taProducerCOMPID: TFIBIntegerField;
    taProducerNAME: TFIBStringField;
    taCompPRODUCER: TFIBSmallIntField;
    taCompDOCBAD_P: TFIBIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure taMOLNewRecord(DataSet: TDataSet);
    procedure CalcRecNo(DataSet: TDataSet);
    procedure taCompNewRecord(DataSet: TDataSet);
    procedure taDepNewRecord(DataSet: TDataSet);
    procedure taRecNewRecord(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taDepBeforeOpen(DataSet: TDataSet);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure DelConf(DataSet: TDataSet);
    procedure taSemisUQGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taSemisUWGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taSemisUWSetText(Sender: TField; const Text: String);
    procedure taSemisUQSetText(Sender: TField; const Text: String);
    procedure quSeqBeforeOpen(DataSet: TDataSet);
    procedure aplevRestore(Sender: TObject);
    procedure taOperUGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taOperUSetText(Sender: TField; const Text: String);
    procedure taCxOperBeforeOpen(DataSet: TDataSet);
    procedure taCxOperOPERATIONSLSetText(Sender: TField; const Text: String);
    procedure SEMISGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure SEMISSetText(Sender: TField; const Text: String);
    procedure aplevException(Sender: TObject; E: Exception);
    procedure taOperCLASSOPGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taOperCLASSOPSetText(Sender: TField; const Text: String);
    procedure taRProdMSetText(Sender: TField; const Text: String);
    procedure taRProdMGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taOperZTSetText(Sender: TField; const Text: String);
    procedure taOperZTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taNDSNewRecord(DataSet: TDataSet);
    procedure taDiscountNewRecord(DataSet: TDataSet);
    procedure quUserBeforeOpen(DataSet: TDataSet);
    procedure taArtBeforeOpen(DataSet: TDataSet);
    procedure taArtUNITIDGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taArtUNITIDSetText(Sender: TField; const Text: String);
    procedure taArtNewRecord(DataSet: TDataSet);
    procedure taArtAfterScroll(DataSet: TDataSet);
    procedure taPSBeforeOpen(DataSet: TDataSet);
    procedure taPSNewRecord(DataSet: TDataSet);
    procedure taDepAfterOpen(DataSet: TDataSet);
    procedure taPSBeforeInsert(DataSet: TDataSet);
    procedure taArt2NewRecord(DataSet: TDataSet);
    procedure taArt2BeforeOpen(DataSet: TDataSet);
    procedure taA2InsBeforeOpen(DataSet: TDataSet);
    procedure taA2InsNewRecord(DataSet: TDataSet);
    procedure UpdArt2(DataSet: TDataSet);
    procedure taEdgShapeBeforePost(DataSet: TDataSet);
    procedure taEdgTBeforePost(DataSet: TDataSet);
    procedure taInsBeforePost(DataSet: TDataSet);
    procedure taColorBeforePost(DataSet: TDataSet);
    procedure taCleannesBeforePost(DataSet: TDataSet);
    procedure taChromaticityBeforePost(DataSet: TDataSet);
    procedure taIGrBeforePost(DataSet: TDataSet);
    procedure taMatNewRecord(DataSet: TDataSet);
    procedure taSemisBeforeOpen(DataSet: TDataSet);
    procedure taA2InsCalcFields(DataSet: TDataSet);
    procedure taCodeNewRecord(DataSet: TDataSet);
    procedure taPsOperBeforeInsert(DataSet: TDataSet);
    procedure taPsOperBeforeOpen(DataSet: TDataSet);
    procedure taPsOperNewRecord(DataSet: TDataSet);
    procedure taOperInBeforeOpen(DataSet: TDataSet);
    procedure taOperOutBeforeOpen(DataSet: TDataSet);
    procedure taOperInNewRecord(DataSet: TDataSet);
    procedure taOperOutNewRecord(DataSet: TDataSet);
    procedure taOperTailBeforeOpen(DataSet: TDataSet);
    procedure taOperTailNewRecord(DataSet: TDataSet);
    procedure E410MReceiveData(Sender: TObject; DataPtr: Pointer; DataSize: Integer);
    procedure taOperNewRecord(DataSet: TDataSet);
    procedure taOperRejBeforeOpen(DataSet: TDataSet);
    procedure taOperRejNewRecord(DataSet: TDataSet);
    procedure taOperAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taOperASetText(Sender: TField; const Text: String);
    procedure taOperUQGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taOperUQSetText(Sender: TField; const Text: String);
    procedure taSemisNewRecord(DataSet: TDataSet);
    procedure taMatALG_CALCPRICESetText(Sender: TField; const Text: String);
    procedure taTransMatBeforeOpen(DataSet: TDataSet);
    procedure taTransMatNewRecord(DataSet: TDataSet);
    procedure taZPKBeforeOpen(DataSet: TDataSet);
    procedure taZPKCalcFields(DataSet: TDataSet);
    procedure taTailsNewRecord(DataSet: TDataSet);
    procedure taOperBeforeDelete(DataSet: TDataSet);
    procedure taMOLBeforeDelete(DataSet: TDataSet);
    procedure taArtBeforeDelete(DataSet: TDataSet);
    procedure taArtBeforePost(DataSet: TDataSet);
    procedure taSemisAffNewRecord(DataSet: TDataSet);
    procedure taRejNewRecord(DataSet: TDataSet);
    procedure taAInsBeforeOpen(DataSet: TDataSet);
    procedure taAInsNewRecord(DataSet: TDataSet);
    procedure taArtAfterOpen(DataSet: TDataSet);
    procedure taArtAfterClose(DataSet: TDataSet);
    procedure taAInsCalcFields(DataSet: TDataSet);
    procedure taRejAfterOpen(DataSet: TDataSet);
    procedure taPrillNewRecord(DataSet: TDataSet);
    procedure taConstNewRecord(DataSet: TDataSet);
    procedure taTechNewRecord(DataSet: TDataSet);
    procedure taEdgShapeNewRecord(DataSet: TDataSet);
    procedure taEdgTNewRecord(DataSet: TDataSet);
    procedure taSzNewRecord(DataSet: TDataSet);
    procedure taInitStoneNewRecord(DataSet: TDataSet);
    procedure taSemisPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure taInitStoneBeforeOpen(DataSet: TDataSet);
    procedure taPaytypeNewRecord(DataSet: TDataSet);
    procedure taArtPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
    procedure DataModuleDestroy(Sender: TObject);
    procedure taClientBeforeDelete(DataSet: TDataSet);
    procedure taContractNewRecord(DataSet: TDataSet);
    procedure taContractDeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taContractBeforeDelete(DataSet: TDataSet);
    procedure taInvColorNewRecord(DataSet: TDataSet);
    procedure taTransMatKGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taTransMatKSetText(Sender: TField; const Text: String);
    procedure quDepBeforeOpen(DataSet: TDataSet);
    procedure quDepTBeforeExecute(Sender: TObject);
    procedure taDepartBeforeOpen(DataSet: TDataSet);
    procedure tmDemoTimer(Sender: TObject);
    procedure taCompOwnerNewRecord(DataSet: TDataSet);
    procedure taArtRootNewRecord(DataSet: TDataSet);
    procedure taArtRootBeforeOpen(DataSet: TDataSet);
    procedure aplevActivate(Sender: TObject);
    procedure aplevDeactivate(Sender: TObject);
    procedure taSzPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taSzDiamondPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure DeleteError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taInvColorBeforePost(DataSet: TDataSet);
    procedure taClientNewRecord(DataSet: TDataSet);
    procedure taClientBeforePost(DataSet: TDataSet);
    procedure taSemisAffBeforePost(DataSet: TDataSet);
    procedure taColorPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taCleannesPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taChromaticityPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taIGrPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taEdgShapePostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taEdgTPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taTechPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taTailsPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taTailsBeforePost(DataSet: TDataSet);
    procedure taInsPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taContractPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taTechBeforeDelete(DataSet: TDataSet);
    procedure taSemisBeforePost(DataSet: TDataSet);
    procedure taMatPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taOperPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taOperBeforePost(DataSet: TDataSet);
    procedure taMatBeforePost(DataSet: TDataSet);
    procedure taContractBeforePost(DataSet: TDataSet);
    procedure taPrillBeforePost(DataSet: TDataSet);
    procedure taNDSBeforePost(DataSet: TDataSet);
    procedure taSzBeforePost(DataSet: TDataSet);
    procedure taSzDiamondBeforePost(DataSet: TDataSet);
    procedure taPaytypeBeforePost(DataSet: TDataSet);
    procedure taPaytypePostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taTechBeforePost(DataSet: TDataSet);
    procedure taRejBeforePost(DataSet: TDataSet);
    procedure taRejPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taCompBeforeOpen(DataSet: TDataSet);
  private
    FChangeProp : boolean;
    FConfirmDel : boolean;
    FLogined : boolean;
    FUserId : integer;
    FBeginDate, FEndDate : TDateTime;
    FDepDef : integer;
    FCompId: integer;
    FArtId: integer;
    FOld_CompId: integer;
    FArt2Id: integer;
    FOld_InsId: shortstring;
    FArt2: shortstring;
    FGoodId: shortstring;
    FMatId: shortstring;
    FMatGoodId: shortstring;
    FArt: shortstring;
    FOld_GoodId: shortstring;
    FOld_MatId: shortstring;
    FInsId: shortstring;
    FdIns: TStringList;
    FdSeller: TStringList;
    FdProd: TStringList;
    FdMat: TStringList;
    FdMatGood: TStringList;
    FdComp: TStringList;
    FdSemis: TStringList;
    FDepInfo : array of TDepInfo;
    FOperInfo   : TStrings;
    FSemisInfo  : TStrings;
    FCurrDep    : integer;   // ������� �����������
    FInvMode    : integer; //0 - ���� ��������� ����� ���������, 1 - ���. ��-�� CurrInv
                           // 2 - ���. �������� ���� WOitemInvId
    FCurrInv    : integer;
    //FShowClosed : integer; // ������� ����������� ����������
                           // 0 - ��������, 1 - ��������� -1 - ���
    FDistrDep : integer;
    FUserInfo: TUserInfo;
    FdGood: TStringList;
    FOld_PrillId: integer;
    FPrillId: integer;
    FdPrill: TStringList;
    FReadOnly: boolean;
    FwhEndDate: TDateTime;
    FwhBeginDate: TDateTime;
    FIsAdm: boolean;
    FaBeginDate: TDateTime;
    FaEndDate: TDateTime;
    FdOper: TStringList;
    FdOperNoGood : TStringList;
    FANewArt2id: integer;
    FWorkMode: string;
    FzEndDate: TDateTime;
    FzBeginDate: TDateTime;
    FdPS: TStringList;
    FASzid: integer;
    FdSz: TStringList;
    FArtLength: byte;
    FRejOper: string;
    FNkOper: string;
    FLastOperId: string;
    FDbVersion: string;
    FTransformOperId: string;
    //FO1Id: shortstring;
    //FAStoneSzid: string;
    FdWhProd: TStringList;
    FTransformOperName: string;
    FvEndDate: TDateTime;
    FvBeginDate: TDateTime;
    FPsBeginDate: TDateTime;
    FPsTypeBeginDate: byte;
    FDepBeginDate: TDateTime;
    FdPsWhProd: TStringList;
    FDbPath: string;
    FdKName: TStringList;
    FAOperId: string;
    FOrganizationId : integer;
    FOrganizationName: string;
    FShowArtIns: boolean;
    FReAssemblyId: string;
    FInitStone_ADate: TDateTime;
    FdMatIns: TStringList;
    FLocalSetting: TLocalSetting;
    FArtSzDiamond: string;
    FDictMode: string;
    FModifyAssortOperId: string;
    FNotif: TNotif;
    FRec: TRec;
    FSchemaId: integer;

//    function GetColorById(Index: integer): TColor;
//    function GetColorByName(const Name: string): TColor;
    function GetDepCount: integer;
    function GetDepIdByName(const Name: string): integer;
    function GetDepNameById(Index: integer): string;
    function GetDepInfo(Index: integer): TDepInfo;
    function LoginFunction(UsersTable: TpFIBDataSet; const Password: String): Boolean;
    function GetShowKindDoc: integer;
    procedure SetBeginDate(const Value: TDateTime);
    procedure SetEndDate(const Value: TDateTime);
    procedure SetShowKindDoc(const Value: integer);
    function GetDepDef: integer;

    procedure SetDocPreview(const Value: TDocPreview);
    function getRec(Query : string) : Variant;
    procedure setRec(Query : string);
    procedure SetDepDef(const Value: integer);
    procedure SetWhEndDate(const Value: TDateTime);
    function GetDepInfoById(Index: integer): TDepInfo;
    procedure SetBarCodePrinter_Count(const Value: integer);
    function GetBarCodePrinter_Count: integer;
    function GetBarCodePrinter_Step: string;
    procedure SetBarCodePrinter_Step(const Value: string);
  public
    D_DepId : integer;
    SeqId : integer;
    function GetDocPreview: TDocPreview;
    property Logined : boolean read FLogined write FLogined;
    property ConfirmDel : boolean read FConfirmDel write FConfirmDel;
    property WorkMode : string read FWorkMode write FWorkMode;
    property DictMode : string read FDictMode write FDictMode;

                    (* ��������� *)
    property OrganizationId : integer read FOrganizationId write FOrganizationId;
    property OrganizationName : string read FOrganizationName write FOrganizationName;
    property LocalSetting : TLocalSetting read FLocalSetting write FLocalSetting;
    property BarCodePrinter_Count : integer read GetBarCodePrinter_Count write SetBarCodePrinter_Count;
    property BarCodePrinter_Step : string read GetBarCodePrinter_Step write SetBarCodePrinter_Step; 
    property Rec: TRec read FRec;
    procedure UpdateNormaOrderDate(D : Variant);
    procedure UpdateZpRateOrderDate(D: Variant);

    (* ���������� � ������������ � ��� ���������� *)
      (* ������������ ������� ����� � ��������� *)
    property User : TUserInfo read FUserInfo; //read only
    property ReadOnly : boolean read FReadOnly;
    property IsAdm : boolean read FIsAdm;
              (* �������� ������ � ���������� ������������ *)
    property BeginDate: TDateTime read FBeginDate write SetBeginDate;
    property EndDate: TDateTime read FEndDate write SetEndDate;
              (* �������� ������ � ���������� ����� ������� ��������� *)
    property whBeginDate : TDateTime read FwhBeginDate write FwhBeginDate;
    property whEndDate   : TDateTime read FwhEndDate   write SetWhEndDate;
              (* �������� ������ � ���������� ����� ��������� *)
    property aBeginDate : TDateTime read FaBeginDate write FaBeginDate;
    property aEndDate : TDateTime read FaEndDate write FaEndDate;
              (* �������� ������ � ���������� �������� *)
    property zBeginDate : TDateTime read FzBeginDate write FzBeginDate;
    property zEndDate   : TDateTime read FzEndDate write FzEndDate;
              (* �������� ������ � ������������ ���������� *)
    property vBeginDate : TDateTime read FvBeginDate write FvBeginDate;
    property vEndDate   : TDateTime read FvEndDate write FvEndDate;
              (* ��������� ���� ��� ����� �������� *)
    property PsBeginDate : TDateTime read FPsBeginDate write FPsBeginDate;
    property PsTypeBeginDate : byte read FPsTypeBeginDate write FPsTypeBeginDate; // 0 - ��������
                                                                                  // 1 - ������������ ���������
    property DepBeginDate : TDateTime read FDepBeginDate write FDepBeginDate;

    property DepDef : integer read GetDepDef write SetDepDef;
    property DocPreview : TDocPreview read GetDocPreview write SetDocPreview;
    property ShowKindDoc : integer read GetShowKindDoc write SetShowKindDoc;

    (* �������� ����� *)
//    property ColorByName[const Name: string]: TColor read GetColorByName;
//    property ColorById[Index : integer]: TColor read GetColorById;
    (* ���������� � �������������� *)
    property DepIdByName[const Name : string]: integer read GetDepIdByName;
    property DepNameById[Index : integer]: string read GetDepNameById;
    property DepInfo[Index : integer]: TDepInfo read GetDepInfo;
    property DepInfoById[Index : integer]: TDepInfo read GetDepInfoById;
    property CountDep : integer read GetDepCount;
    property CurrDep : integer read  FCurrDep write FCurrDep;
    property CurrFromDep : integer read FDistrDep write FDistrDep;

    (* ������������ ����������� *)
    property dProd    : TStringList read FdProd   write FdProd;
    property dMat     : TStringList read FdMat    write FdMat;
    // ��������� ������������ � ����������� ��������� 
    //property dMatGood : TStringList read FdMatGood write FdMatGood;
    property dMatGood : TStringList read FdMatGood write FdMatGood;
    property dMatIns  : TStringList read FdMatIns write FdMatIns;
    property dSemis   : TStringList read FdSemis  write FdSemis; //
    property dGood    : TStringList read FdGood   write FdGood; // ������������� �������
                        // �������� ������� (������, ������ � ��.)
    property dIns     : TStringList read FdIns    write FdIns;
    property dSeller  : TStringList read FdSeller write FdSeller;
    property dComp    : TStringList read FdComp   write FdComp;
    property dPrill   : TStringList read FdPrill  write FdPrill;
    property dOper    : TStringList read FdOper   write FdOper;
    property dOperNoGood : TStringList read FdOperNoGood write FdOperNoGood;
      // �������� �� ������������  ???
    property dPS     : TStringList read FdPS    write FdPS;
    property dWHProd : TStringList read FdWhProd write FdWHProd;
    property dPsWhProd : TStringList read FdPsWhProd write FdPsWhProd;
      // �������
    property dSz     : TStringList read FdSz    write FdSz;
     // �������� �������������
    property dKName  : TStringList read FdKName write FdKName;

          (* ������� ��������� ������� ��������, ������������� *)
    property ACompId  : integer     read FCompId  write FCompId;
    property AMatId   : shortstring read FMatId   write FMatId;
    property AMatGoodId   : shortstring read FMatGoodId   write FMatGoodId;
    property ASemisId : shortstring read FGoodId  write FGoodId;
    property AInsId   : shortstring read FInsId   write FInsId;
    property APrillId : integer     read FPrillId write FPrillId;
    property Old_ACompId  : integer     read FOld_CompId  write FOld_CompId;
    property Old_AMatId   : shortstring read FOld_MatId   write FOld_MatId;
    property Old_ASemisId : shortstring read FOld_GoodId  write FOld_GoodId;
    property Old_AInsId   : shortstring read FOld_InsId   write FOld_InsId;
    property Old_APrillId : integer     read FOld_PrillId write FOld_PrillId;
    property AArtId   : integer     read FArtId  write FArtId;
    property AArt     : shortstring read FArt    write FArt;
    property AArt2Id  : integer     read FArt2Id write FArt2Id;
    property AArt2    : shortstring read FArt2   write FArt2;
    property ANewArt2Id : integer read FANewArt2id write FANewArt2Id;
    property ASzId      : integer read FASzid write FASzId; // ������� ��������
    property AOperId    : string read FAOperId write FAOperId;
    // ������������ � ����� eAtrSzDiamond
    property ArtSzDiamond : string read FArtSzDiamond write FArtSzDiamond;
                       (* ���������� *)
    property ShowArtIns : boolean read FShowArtIns write FShowArtIns;

    (* ���������������� �������� *)
    property RejOperId : string read FRejOper write FRejOper;
    property NkOperId : string read FNkOper write FNkOper;
    property TransformOperId : string read FTransformOperId write FTransformOperId;
    property TransformOperName : string read FTransformOperName write FTransformOperName;
    property ReAssemblyId : string read FReAssemblyId write FReAssemblyId;
    property LastOperId : string read FLastOperId write FLastOperId;
    property ModifyAssortOperId : string read FModifyAssortOperId write FModifyAssortOperId;

    (* ��� ������� ����� ��� ��������� *)
    property SchemaId : integer read FSchemaId write FSchemaId;

    (* ������ ���� ������ *)
    property DbVersion : string read FDbVersion;
    property DbPath : string read FDbPath;

                 (* ������� �� ����. ������ *)
    property InitStone_ADate : TDateTime read FInitStone_ADate write FInitStone_ADate;

    (* ���������� � ������� ��������� *)
    property NotifInfo : TNotif read FNotif; 


            (* ����������� �������� *)
    property ArtLength : byte read FArtLength;
    function CheckValidArt(Art : string) : boolean;
    function CheckValidArt2(Art2 : string) : boolean;
    function GetArtVal(N : integer; Key : string): Variant;
    function GetArtField(N : integer): Variant;
    function InsertArt(Art : string): integer;

    (* ��������� ������������� *)
    procedure InitSetting;
    procedure InitDeps;
    procedure InitUser;
    procedure InitDicts;
    procedure InitArtParams;
    procedure InitArtCode;
    procedure InitPeriod; overload;
    procedure InitPeriod(Bd, Ed : TDateTime); overload;
    procedure InitLocalSetting;
    procedure GlobalInitialization;
    procedure SaveCompLoginInfo(CompId : integer; UseTolling:boolean; TollingType, TollingCompId: integer);

    (* ��������� ���������� ������ ������ *)
    procedure SetDepParams(Params : TFIBXSQLDA);
    procedure SetArtParams(Params : TFIBXSQLDA);
    procedure SetPeriodParams(Params : TFIBXSQLDA);
    procedure SetAPeriodParams(Params : TFIBXSQLDA);
    procedure SetWhPeriodParams(Params : TFIBXSQLDA);
    procedure SetSelfCompParams(Params : TFIBXSQLDA);
    procedure SetSchemaParams(Params : TFIBXSQLDA);

    function CalcNDS(Cost, NDS : double; T:byte):double;

                (* ������ ��������� ������� *)
    function GetDepBeginDate(DepId : integer) : TDateTime;
    function GetStoreBeginDate : TDateTime; // store
    function GetPsBeginDate(PsId : integer) : TDateTime;

    (* ����������� ������� *)
    function GetId(TableID: integer): integer;
    function GetId2(TableId, DocId : integer) : integer;
    function GetId3(TableId, DepId, DepFromId : integer) : integer;
    function GetId4(TableId, SelfCompId: integer) : integer;
    function ServerTime : TTime;
    function ServerDate : TDateTime;
    function ServerDateTime : TDateTime;
    procedure BlockApp;

    (* ��������������� ������� *)
    procedure LoadArtSL(Dict: integer); // �������� ����������(�)��

    function QueryData(const SQL: AnsiString): OleVariant;
    function ExecuteSQL(const ASQL: AnsiString; AParams: Variant): Boolean;

    constructor Create(AOwner : TComponent); override;
    destructor Destroy; override;
  end;

(*
   ���� ������
     - D_MOL 1 ���������� ����������� ������������ ���
     - WOrder 2 ������� � ������� �������
     - WOItem 3 ������� ������
     - D_Dep 4 ������� �������������
     - D_Comp 5 ������� ����������� � ���������� ���
     - D_Rec 6 ������� �������� �� ���������
     - null 7 ��������� ����������� ������ ��� ������(���������) ����� ��-15
     - D_Seq 8 ������� ������������ ��������
     - null 9 ���. ����. ������ ��� ���������
     - D_CxOper 10 - ������� � ��������� ������� ��������
     - null 11 ��������� ����������� ������ ��������� �����. �����������
     - Inv 12 ������� ���������
     - ItInv 13 ������� �������
     - D_Art 14 ������� ���������
     - Doc   15 ������� ����������
*)

var
  dm: Tdm;
  AppDebug: boolean;
  AppVersion : string;

implementation

uses DbLogin, rxStrUtils, dbUtil, fmUtils, MainData, Setting, Variants,
     bsUtils, StdCtrls, dbTree, dArt, Jpeg, InvData, SInv_det,
     dMOL, dCode, Main, MsgDialog, IniFiles, UtilLib, CompLogin,
     Log_Function, ProductionConsts, QStrings, eSelWh, FileCtrl,
  uUtils;

{$R *.DFM}

procedure Tdm.DataModuleCreate(Sender: TObject);
var
//  Id : integer;
  ip, host{, exeurl, ClientLocation, ClientLocationDir} : string;
  UseCompLogin : boolean;
  Ini : TIniFile;
  sect : TStringList;
//  r : Variant;
begin
  Logined:=False;
  if not LoginDB(db, 'D_MOL', 'FName', LoginFunction, 3, False, UseCompLogin, False) then
  begin
    Self.OnDestroy := nil;
    eXit;
  end;

  db.Connected := True;

  // ������� �����������
  if UseCompLogin then
    if ShowAndFreeForm(TfmCompLogin, TForm(fmCompLogin))<>mrOk then
    begin
      db.Close;
      Logined := False;
      eXit;
    end;
  (* ��������� ini-���� *)
  Ini := TIniFile.Create(GetIniFileName);
  sect := TStringList.Create;
  try
    Ini.ReadSectionValues('CompLogin', sect);
    FUserInfo.SelfCompId := StrToIntDef(sect.Values['CompId'], -1);
    if(FUserInfo.SelfCompId = -1)then
    begin
      db.Close;
      Logined := False;
      eXit;
    end
    else FUserInfo.SelfCompName := ExecSelectSQL('select Name from D_Comp where COMPID='+IntToStr(FUserInfo.SelfCompId), quTmp);
    FUserInfo.UseTolling := sect.Values['UseTolling']='1';
    FUserInfo.TollingCompId := StrToIntDef(sect.Values['TollingCompId'], -1);
    FUserInfo.TollingCompName := sect.Values['TollingCompName'];
    FUserInfo.TollingType := StrToIntDef(sect.Values['TollingType'], -1);
    Application.Title := '������ '+FUserInfo.SelfCompName;
  finally
    Ini.Free;
    sect.Free;
  end;
  // ������ � ������������

  InitUser;

  // �����������-���
  try
    if not tr.Active then tr.StartTransaction else tr.CommitRetaining;
    FUserInfo.LogId := GetId(81);
    tr.CommitRetaining;
    // �������� ��� � ip adress �����
    GetLocalIP(ip, host);

    ExecSQL('insert into LOG$USER(ID, USERID, UHOSTNAME, UIPADDR, CONNECTDATE, '+
       'DISCONNECTDATE, DBID, SELFCOMPID, TOLLINGCOMPID, HOST, DISCRIPT, USERNAME)'+
     'values('+IntToStr(User.LogId)+','+IntToStr(FUserId)+',"'+host+'","'+ip+'","'+DateTimeToStr(Now)+'", '+
       'null,'+IntToStr(db.AttachmentID)+','+IntToStr(FUserInfo.SelfCompId)+','+
      fif(FUserInfo.UseTolling, IntToStr(FUserInfo.TollingCompId),'null')+','+
      '"'+ip+':'+host+'", "Connected","'+FUserInfo.FIO+'")', quTmp);
  except
    on E:Exception do
    begin
      MessageDialogA(E.Message, mtError);
      Application.Terminate;
    end;
  end;
  SetIsMakeLog(quLog);
  FReadOnly := ExecSelectSQL('select IO from D_Mol where MolId='+IntToStr(FUserId), quTmp)=0;
  FIsAdm := ExecSelectSQL('select Adm from D_Mol where MolId='+IntToStr(FUserId), quTmp)=1;
  Logined:=True;
end;

function Tdm.LoginFunction(UsersTable: TpFIBDataSet; const Password: String): Boolean;
var
  APassword: string;
  MD5Digest: TMD5Digest;
begin
  if ParamCount <> 0 then
  begin
    APassword := ParamStr(1) + '+';
    if (APassword) = Password then
    begin
      with UsersTable do
      begin
        FUserId := FieldByName('MOLID').AsInteger;
        Result:=True;
        Exit;
      end;
    end;
  end;

  Result:=False;

  {
  // !!! no password
  with UsersTable do
  begin
    FUserId := FieldByName('MOLID').AsInteger;
    Result:=True;
    Exit;
  end;
  }
  MD5Digest :=  MD5String(Password);
  APassword := MD5DigestToBase64(MD5Digest);

  if APassword = DelESpace(UsersTable.FieldByName('PSWD').AsString) then
  with UsersTable do
  begin
    FUserId := FieldByName('MOLID').AsInteger;
    Result:=True;
  end;
end;

function Tdm.GetId(TableID: integer): integer;
begin
  with quGetID do
  begin
    if not Transaction.Active then Transaction.StartTransaction;
    Params[0].AsInteger:=TableID;
    ExecQuery;
    Result:=Fields[0].AsInteger;
    Close;
  end;
end;

function Tdm.QueryData(const SQL: AnsiString): OleVariant;
var
  DataSet: TClientDataSet;
  SourceDataSet: TpFIBDataSet;
  Provider: TDataSetProvider;
begin

  SourceDataSet := TpFIBDataSet.Create(Self);
  SourceDataSet.Database := db;
  SourceDataSet.Transaction := Tr;
  SourceDataSet.SelectSQL.Text := SQL;

  Provider := TDataSetProvider.Create(Self);
  Provider.Name := 'SourceProvider';
  Provider.DataSet := SourceDataSet;

  DataSet := TClientDataSet.Create(Self);

  DataSet.ProviderName := Provider.Name;

  try
    try
      DataSet.Active := true;
      Result := DataSet.Data;
    except
      on E:Exception do
        ShowMessage(E.Message);
    end;
  finally
    SourceDataSet.Active := false;
    DataSet.ProviderName := '';
    Provider.Free;
    SourceDataSet.Free;
    DataSet.Free;
  end;

end;

function Tdm.ExecuteSQL(const ASQL: AnsiString; AParams: Variant): Boolean;
var
  i :Integer;
  LowBound, HighBound: Integer;
begin

  Result := false;

  LowBound := VarArrayLowBound(AParams, 1);
  HighBound := VarArrayHighBound(AParams, 1);

  try

    with quTmp do
    begin

      Close;

      SQL.Text := ASQL;

      for i := LowBound to HighBound do
      begin
        Params[i].AsVariant := AParams[i];
      end;

      ExecQuery;

      Transaction.CommitRetaining;

    end;

  except

    On E: Exception do
    begin

      ShowMessage(E.Message);

      Exit;

    end;

  end;

  Result := true;

end;

procedure Tdm.taMOLNewRecord(DataSet: TDataSet);
begin
  taMOLMOLID.AsInteger := GetId(1);
  taMOLACCDICT.AsInteger := 0; // ������ ��������
  taMOLACCSUBSYS.AsInteger := 0; // ������ ��������
  taMOLACCPROD.AsInteger := 0;
  taMOLACCAPPL.AsInteger := 0;
  taMOLACCWH.AsInteger := 0;
  taMOLACCPROD_D.AsInteger := 0;
  taMOLPROFILE.AsInteger := 0;
  taMOLDOCPREVIEW.AsInteger := 1;
end;

procedure Tdm.CalcRecNo(DataSet: TDataSet);
begin
  DataSet.FieldByName('RecNo').AsInteger := DataSet.RecNo;
end;

procedure Tdm.taCompBeforeOpen(DataSet: TDataSet);
begin
//  ShowMessage('dm.taComp before open');
end;

procedure Tdm.taCompNewRecord(DataSet: TDataSet);
begin
  taCompCompId.AsInteger := GetID(5);
end;

procedure Tdm.taDepNewRecord(DataSet: TDataSet);
begin
  taDepDEPID.AsInteger := GetID(4);
end;

procedure Tdm.taRecNewRecord(DataSet: TDataSet);
begin
  taRecRecId.AsInteger := GetID(6);
end;

procedure Tdm.CommitRetaining(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure Tdm.taDepBeforeOpen(DataSet: TDataSet);
begin
  taDep.Params[0].AsInteger := D_DepId;
  SetSelfCompParams(taDep.Params);
end;

procedure Tdm.PostBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure Tdm.DelConf(DataSet: TDataSet);
begin
  if MessageDialog('������� �������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure Tdm.taSemisUQGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
          -1 : Text:='���';
           0 : Text:='��';
           1 : Text:='����';
         end;
end;

procedure Tdm.taSemisUQSetText(Sender: TField; const Text: String);
begin
  if Text='���' then Sender.AsInteger := -1;
  if Text='��' then Sender.AsInteger := 0;
  if Text='����' then Sender.AsInteger := 1;
end;

procedure Tdm.taSemisUWGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
          -1 : Text:='���';
           3 : Text:='��';
           4 : Text:='�����';
         end;
end;

procedure Tdm.taSemisUWSetText(Sender: TField; const Text: String);
begin
  if Text='���' then Sender.AsInteger := -1;
  if Text='��' then Sender.AsInteger:=3;
  if Text='�����' then Sender.AsInteger:=4;
end;

procedure Tdm.quSeqBeforeOpen(DataSet: TDataSet);
begin
  taDep.Params[0].AsInteger := SeqId;
end;

procedure Tdm.aplevRestore(Sender: TObject);
begin
  RestoreApp;
end;

procedure Tdm.taOperUGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text := ''
  else case Sender.AsInteger of
    0 : Text := '��.';
    1 : Text := '��.';
    2 : Text := '���';
    else Text := '';
  end;
end;

procedure Tdm.taOperUSetText(Sender: TField; const Text: String);
begin
  if(Text = '��.')then Sender.AsInteger := 0;
  if(Text = '��.')then Sender.AsInteger := 1;
  if(Text = '���')then Sender.AsInteger := 2;
end;

procedure Tdm.taCxOperBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['I_OperM'].AsString := taOperOPERID.AsString;
end;

procedure Tdm.taCxOperOPERATIONSLSetText(Sender: TField;
  const Text: String);
begin
  Sender.AsString := Text;
end;

procedure Tdm.SEMISGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  quTmp.Close;
  quTmp.SQL.Text := 'select Semis from D_Semis where SemisId='#39+Sender.AsString+#39;
  quTmp.ExecQuery;
  if quTmp.Fields[0].IsNull then Text := '' else Text := quTmp.Fields[0].AsString;
  quTmp.Close;
end;

procedure Tdm.SEMISSetText(Sender: TField; const Text: String);
var
   s : string;
begin
  s := dmMain.GetIdByField('D_Semis', 'Semis', 'SemisId', Text);
  if s <> '' then // ����� ��� �����-��
    Sender.AsString := s
  else //������� �� PickList'a
    Sender.AsString := Text;
end;

procedure Tdm.aplevException(Sender: TObject; E: Exception);
var
  i : integer;
  s : string;
begin
  if (E.ClassName = 'EWarning') then MessageDialogA(E.Message, mtWarning)
  else if (E.ClassName = 'EInternal') then
  begin
    i := StrToIntDef(Q_GetWordN(3, E.Message, [' ', '.']), -1);
    s := '';
    if i<>-1 then s:= InternalError[i+30000];
    if s='' then MessageDialog(E.Message+#13+'�������� ����������', mtError, [mbOk], 0)
    else MessageDialog(E.Message+#13+'���c����:'+s, mtError, [mbOk], 0);
  end
  else Application.ShowException(E);
end;

function Tdm.GetShowKindDoc: integer;
begin
  //MessageDialog('UserID: ' + IntToStr(FUserId), mtInformation, [mbOk], 0);
  Result := ExecSelectSQL('select ShowKindDoc from D_MOL where MOLID='+IntToStr(FUserId), quTmp);
end;

procedure Tdm.SetBeginDate(const Value: TDateTime);
begin
  FBeginDate := Value;
end;

procedure Tdm.SetEndDate(const Value: TDateTime);
begin
  if FBeginDate > Value then
    raise Exception.Create('����� ������� �� ����� ���� ������ ������ �������');
  FEndDate := Value;
end;

procedure Tdm.SetShowKindDoc(const Value: integer);
begin
  setRec('update D_MOL set ShowKindDoc='+IntToStr(Value)+' '+
                'where MolId='+IntToStr(FUserId));
end;

function Tdm.GetDepDef: integer;
var
  V : Variant;
begin
  V := getRec('select DEPID from D_MOL where MOLID='+IntToStr(User.UserId));
  if V = Null then Result := -1
  else Result := V;
end;

procedure Tdm.taOperCLASSOPGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           1 : Text:='������� ���������';
           2 : Text:='������';                           
           3 : Text:='�������������� �����-��';
         end;
end;

procedure Tdm.taOperCLASSOPSetText(Sender: TField; const Text: String);
begin
  if Text='������� ���������' then Sender.AsInteger := 1;
  if Text='������' then Sender.AsInteger := 2;
  if Text='�������������� �����-��' then Sender.AsInteger := 3;
end;

function Tdm.GetDocPreview: TDocPreview;
var
  v : Variant;
begin
  v := getRec('select DOCPREVIEW from D_MOL where MOLID='+IntToStr(User.UserId));
  if VarIsNull(v) then Result := pvPreview else Result := v;
end;

procedure Tdm.SetDocPreview(const Value: TDocPreview);
begin
  setRec('update D_MOL set DOCPREVIEW='+IntToStr(Integer(Value))+' '+
         'where MOLID='+IntToStr(User.UserId));
end;

function Tdm.GetRec(Query : string) : Variant;
begin
  with quRecTmp do
  begin
    Close;
    Transaction.StartTransaction;
    SQL.Text := Query;
    ExecQuery;
    Result := Fields[0].AsVariant;
    Transaction.Commit;
    Close;
  end;
end;

procedure Tdm.setRec(Query: string);
begin
  with quRecTmp do
  begin
    Close;
    Transaction.StartTransaction;
    SQL.Text := Query;
    ExecQuery;
    Close;
    Transaction.Commit;
  end;
end;

procedure Tdm.SetDepDef(const Value: integer);
begin
  setRec('update D_MOL set DEPID='+IntToStr(Value)+' '+
         'where MOLID='+IntToStr(User.UserId));
  FDepDef := Value;
end;

function GetDefaultIniName : string;
begin
  Result := ChangeFileExt(Application.ExeName, '.INI');
end;

procedure Tdm.taRProdMSetText(Sender: TField; const Text: String);
begin
  if(Text=csMonth[1])then Sender.AsInteger := 1;
  if(Text=csMonth[2])then Sender.AsInteger := 2;
  if(Text=csMonth[3])then Sender.AsInteger := 3;
  if(Text=csMonth[4])then Sender.AsInteger := 4;
  if(Text=csMonth[5])then Sender.AsInteger := 5;
  if(Text=csMonth[6])then Sender.AsInteger := 6;
  if(Text=csMonth[7])then Sender.AsInteger := 7;
  if(Text=csMonth[8])then Sender.AsInteger := 8;
  if(Text=csMonth[9])then Sender.AsInteger := 9;
  if(Text=csMonth[10])then Sender.AsInteger := 10;
  if(Text=csMonth[11])then Sender.AsInteger := 11;
  if(Text=csMonth[12])then Sender.AsInteger := 12;
end;

procedure Tdm.taRProdMGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text := ''
  else case Sender.AsInteger of
    1,2,3,4,5,6,7,8,9,10,11,12 : Text := csMonth[Sender.AsInteger];
    else Text := '';
  end;
end;

procedure Tdm.taOperZTSetText(Sender: TField; const Text: String);
begin
  if Text='�� ����' then Sender.AsInteger := 0;
  if Text='�� ���-��' then Sender.AsInteger := 1;
  if Text='�� ������' then Sender.AsInteger := 2;
end;

procedure Tdm.taOperZTGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text := ''
  else case Sender.AsInteger of
     0 : Text := '�� ����';
     1 : Text := '�� ���-��';
     2 : Text := '�� ������';
  end;
end;


procedure Tdm.taNDSNewRecord(DataSet: TDataSet);
begin
  taNDSNDSID.AsInteger := GetId(59);
  taNDSNDS.AsFloat := 0;
  taNDST.AsInteger := 0;
end;

procedure Tdm.taDiscountNewRecord(DataSet: TDataSet);
begin
  taDiscountD_DISCOUNTID.AsInteger := GetId(60);
  taDiscountDISCOUNT.AsFloat := 0;
end;

procedure Tdm.SetPeriodParams(Params: TFIBXSQLDA);
begin
  with Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
  end
end;
{
function Tdm.GetColorById(Index: integer): TColor;
begin

end;

function Tdm.GetColorByName(const Name: string): TColor;
begin

end;
}
function Tdm.GetDepCount: integer;
begin
  Result := Length(FDepInfo);
end;

function Tdm.GetDepIdByName(const Name: string): integer;
begin

end;

function Tdm.GetDepInfo(Index: integer): TDepInfo;
begin
  Result := FDepInfo[Index];
end;

function Tdm.GetDepNameById(Index: integer): string;
var
  i : integer;
begin
  Result := '';
  for i:=Low(FDepInfo) to High(FDepInfo) do
    if FDepInfo[i].DepId = Index then
    begin
      Result := FDepInfo[i].Name;
      break;
    end;
end;

procedure Tdm.InitArtParams;
begin
  ACompId  := ROOT_COMP_CODE;
  AMatId   := ROOT_MAT_CODE;
  AMatGoodId := ROOT_MAT_CODE;
  ASemisId := ROOT_SEMIS_CODE;
  AInsId   := ROOT_INS_CODE;
  APrillId := ROOT_PRILL_CODE;
  AArt     := '';
  Old_ACompId := ROOT_COMP_CODE;
  Old_AMatId := ROOT_MAT_CODE;
  Old_ASemisId := ROOT_SEMIS_CODE;
  Old_AInsId := ROOT_INS_CODE;
  Old_APrillId := ROOT_PRILL_CODE;
end;

procedure Tdm.InitDeps;
var
  b : boolean;
  i : integer;
begin
  Finalize(FDepInfo);
  b := tr.Active;
  if not b then tr.StartTransaction;
  OpenDataSet(taDepart);
  i := 0;
  while not taDepart.Eof do
  begin
    SetLength(FDepInfo, Length(FDepInfo)+1);
    FDepInfo[i].DepId    := taDepart.FieldByName('DEPID').AsInteger;
    FDepInfo[i].SortInd  := taDepart.FieldByName('SORTIND').AsInteger;
    FDepInfo[i].Name     := taDepart.FieldByName('NAME').AsString;
    FDepInfo[i].SName    := taDepart.FieldByName('SNAME').AsString;
    FDepInfo[i].Color    := taDepart.FieldByName('COLOR').AsInteger;
    FDepInfo[i].Ps       := taDepart.FieldByName('PS').AsInteger=1;
    FDepInfo[i].Wh       := taDepart.FieldByName('WH').AsInteger=1;
    FDepInfo[i].PWh      := taDepart.FieldByName('PWH').AsInteger=1;
    FDepInfo[i].DepTypes := taDepart.FieldByName('DEPTYPES').AsInteger;
    FDepInfo[i].Access := taDepart.FieldByName('Access').AsInteger=1;
    inc(i);
    dm.taDepart.Next;
  end;
  CloseDataSet(taDepart);
  if b then tr.CommitRetaining else tr.Commit;
end;

procedure Tdm.InitDicts;
begin
  LoadArtSL(ALL_DICT);
end;

procedure Tdm.InitUser;
var
  b : boolean;
begin
  b := tr.Active;
  if not b then tr.StartTransaction;
  OpenDataSet(quUser);
  with FUserInfo do
  begin
    UserId    := quUser.FieldByName('MOLID').AsInteger;
    FIO       := quUser.FieldByName('FIO').AsString;
    FName     := quUser.FieldByName('FNAME').AsString;
    LName     := quUser.FieldByName('LNAME').AsString;
    MName     := quUser.FieldByName('MNAME').AsString;
    Alias     := quUser.FieldByName('ALIAS').AsString;
    Pswd      := quUser.FieldByName('PSWD').AsString;
    if quUser.FieldByName('DEPID').IsNull then DepId := -1
    else DepId := quUser.FieldByName('DEPID').AsInteger;
    SheetNo   := quUser.FieldByName('SHEETNO').AsString;
    //IO        := quUser.FieldByName('IO').AsInteger = 1;
    BeginDate := quUser.FieldByName('BD').AsDateTime;
    EndDate   := quUser.FieldByName('ED').AsDateTime;
    PrwKind   := quUser.FieldByName('DOCPREVIEW').AsInteger;
    //IsWork    := quUser.FieldByName('ISWORK').AsInteger = 1;
    ShowKindDoc := quUser.FieldByName('SHOWKINDDOC').AsInteger;
    FIsAdm    := quUser.FieldByName('ADM').AsInteger = 1;
    AccSubSys := quUser.FieldByName('ACCSUBSYS').AsInteger;
    AccDict   := quUser.FieldByName('ACCDICT').AsInteger;
    AccWh     := quUser.FieldByName('ACCWH').AsInteger;
    AccProd   := quUser.FieldByName('ACCPROD').AsInteger;
    AccProd_d := quUser.FieldByName('ACCPROD_D').AsInteger;
    AccAppl   := quUser.FieldByName('ACCAPPL').AsInteger;
    AccMat    := quUser.FieldByName('ACCMAT').AsInteger;
    //DEBUG
    wWhId := ShowSelWh(FUserId, FIO);

    if (wWhId = -1 ) then
    begin

      wWhID := FUserInfo.DepId;

      wWhName := '';

      wWhMatID := -1;

    end else
    begin

      wWhMatID := ExecSelectSQL('select MO from D_DEP where DEPID = ' + IntToStr(wWhId), quTmp);

      wWhName := ExecSelectSQL('select NAME from D_DEP where DEPID = ' + IntToStr(wWhId), quTmp);

    end;

    if quUser.FieldByName('TRANSFORM_DEPID').IsNull then Transform_DepId := -2
    else Transform_DepId := quUser.FieldByName('TRANSFORM_DEPID').AsInteger;
    if quUser.FieldByName('TRANSFORM_MOLID').IsNull then Transform_MolId := -2
    else Transform_MolId := quUser.FieldByName('TRANSFORM_MOLID').AsInteger;

    Profile := quUser.FieldByName('PROFILE').AsInteger;
  end;
  CloseDataSets([quUser]);
  if b then tr.CommitRetaining else tr.Commit;
end;

procedure Tdm.SetArtParams(Params: TFIBXSQLDA);
begin
  with Params do
  begin

    if AMatId=ROOT_MAT_CODE then
      try
        ByName['MATID1'].AsString := MINSTR;
        ByName['MATID2'].AsString := MAXSTR;
      except
      end
    else
      try
        ByName['MATID1'].AsString := AMatId;
        ByName['MATID2'].AsString := AMatId;
      except
      end;

    if ASemisId=ROOT_SEMIS_CODE then
      try
        ByName['SEMISID1'].AsString := MINSTR;
        ByName['SEMISID2'].AsString := MAXSTR;
      except
      end
    else
      try
        ByName['SEMISID1'].AsString := ASemisId;
        ByName['SEMISID2'].AsString := ASemisId;
      except
      end;

    if AInsId=ROOT_INS_CODE then
      try
        ByName['INSID1'].AsString := MINSTR;
        ByName['INSID2'].AsString := MAXSTR;
      except
      end
    else
      try
        ByName['INSID1'].AsString := AInsId;
        ByName['INSID2'].AsString := AInsId;
      except
      end;
    if (DictMode = 'dArt')then
      try
        ByName['ART1'].AsString := AArt;
        ByName['ART2'].AsString := AArt;
      except
      end
    else begin
      if AArt = '' then
        try
          ByName['ART1'].AsString := '%';//MINSTR;
          ByName['ART2'].AsString := '%';//MAXSTR;
        except
        end
      else
        try
          ByName['ART1'].AsString := AArt+'%';//}AArt;
          ByName['ART2'].AsString := AArt+'%';//}AArt + MAXSTR;
        except
        end;
    end;
    if APrillId = -1 then
      try
        ByName['PRILLID1'].AsInteger := -MAXINT;
        ByName['PRILLID2'].AsInteger := MAXINT;
      except
      end
    else
      try
        ByName['PRILLID1'].AsInteger := APrillId;
        ByName['PRILLID2'].AsInteger := APrillId;
      except
      end
  end
end;

procedure Tdm.SetDepParams(Params: TFIBXSQLDA);

procedure SetParam(Name: string; Value: Integer);
var
  Param: TFIBXSQLVAR;
begin
  Param := Params.FindParam(Name);
  if Param <> nil then Param.AsInteger := Value;
end;

begin
    if CurrDep = -1 then
      try
        SetParam('DEPID1', -MAXINT);
        SetParam('DEPID2', MAXINT);
      except
      end
    else
      try
        SetParam('DEPID1', CurrDep);
        SetParam('DEPID2', CurrDep);
      except
      end;
   if CurrFromDep = -1 then
     try
       SetParam('FROMDEPID1', -MAXINT);
       SetParam('FROMDEPID2', MAXINT);
     except
     end
   else
     try
       SetParam('FROMDEPID1', CurrFromDep);
       SetParam('FROMDEPID2', CurrFromDep);
     except
     end;
end;

constructor Tdm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FCurrDep := -1;
  FOperInfo := TStringList.Create;
  FSemisInfo := TStringList.Create;
  FdProd   := TStringList.Create;
  FdMat    := TStringlist.Create;
  FdSemis  := TStringList.Create;
  FdIns    := TStringlist.Create;
  FdSeller := TStringlist.Create;
  FdComp   := TStringList.Create;
  FdGood   := TStringList.Create;
  FdPrill  := TStringList.Create;
  FdMatGood := TStringList.Create;
  FdOper    := TStringList.Create;
  FdOperNoGood := TStringList.Create;
  FdPS := TStringList.Create;
  FdSz  := TStringList.Create;
  FdWhProd := TStringList.Create;
  FdPSWhProd := TStringList.Create;
  FdKName := TStringList.Create;
  FdMatIns := TStringList.Create;
end;

destructor Tdm.Destroy;
begin
  FOperInfo.Free;
  FSemisInfo.Free;
  FdProd.Free;
  FdMat.Free;
  FdSemis.Free;
  FdIns.Free;
  FdSeller.Free;
  FdComp.Free;
  FdGood.Free;
  FdOperNoGood.Free;
  FdPrill.Free;
  FdMatGood.Free;
  FdOper.Free;
  FdPS.Free;
  FdSz.Free;
  FdWhProd.Free;
  FdPSWHProd.Free;
  FdKName.Free;
  FdMatIns.Free;
  inherited Destroy;
end;

procedure Tdm.LoadArtSL(Dict: integer);
var
  nd: TNodeData;
  s: string;
  MaterialID: AnsiString;
  QueryText: AnsiString;

  procedure LoadSL(sl: TStringList; SQLText: string; AllCode: variant);
  begin
    with quTmp, sl do
    begin
      sl.Clear;
      nd:=TNodeData.Create;
      nd.Name:='*���';
      nd.Code:=AllCode;
      AddObject('*���', nd);

      SQL.Text:=SQLText;
      ExecQuery;
      while not EOF do
      begin
        nd:=TNodeData.Create;
        s:=DelRSpace(Fields[1].AsString);
        nd.Name:=s;
        nd.Code:=DelRSpace(Fields[0].AsString);
        AddObject(s, nd);
        Next;
      end;
      Close;
    end;
  end;

begin

  if tr.Active then tr.CommitRetaining else tr.StartTransaction;
  if((Dict and PROD_DICT)<>0) then LoadSL(FdProd, 'SELECT COMPID, SNAME FROM D_COMP WHERE PRODUCER=1 ORDER BY SORTIND', ROOT_PROD_CODE);

  if((Dict and MAT_DICT)<>0)then
  begin

//    if not VarIsNull(User.wWhId) then
//    begin
//
//      MaterialID := VarToStr(db.QueryValue('SELECT MO FROM D_DEP WHERE DEPID = ' + IntToStr(User.wWhId), 0, []));
//
//      if MaterialID = SILVER_MATERIAL_ID then
//      begin
//
//        MaterialID := GOLD_MATERIAL_ID;
//
//      end;
//
//      if MaterialID = GOLD_MATERIAL_ID then
//      begin
//
//        MaterialID := SILVER_MATERIAL_ID;
//
//      end;
//
//    end;
//
//    if MaterialID = '' then
//    begin
//
//      MaterialID := '?';
//
//    end;

    QueryText :=  'SELECT ID, NAME FROM D_MAT ORDER BY SORTIND';

    LoadSL(FdMat, QueryText, ROOT_MAT_CODE);

  end;

  if((Dict and MATGOOD_DICT)<>0)then LoadSL(FdMatGood, 'SELECT ID, NAME FROM D_MAT WHERE GOOD=1 ORDER BY SORTIND', ROOT_MAT_CODE);
  if((Dict and SEMIS_DICT)<>0)then LoadSL(FdSemis, 'SELECT SEMISID, SEMIS FROM D_SEMIS ORDER BY SORTIND', ROOT_SEMIS_CODE);
  if((Dict and GOOD_DICT)<>0)then LoadSL(FdGood, 'SELECT SEMISID, SEMIS FROM D_SEMIS WHERE GOOD=1 ORDER BY SORTIND', ROOT_SEMIS_CODE);
  if((Dict and INS_DICT)<>0)then LoadSL(FdIns, 'SELECT D_INSID, NAME FROM D_INS ORDER BY SORTIND', ROOT_INS_CODE);
  if((Dict and SELLER_DICT)<>0)then LoadSL(FdSeller, 'SELECT COMPID, SNAME FROM D_COMP WHERE SELLER=1 ORDER BY SORTIND', ROOT_SELLER_CODE);
  if((Dict and PRILL_DICT)<>0)then LoadSL(FdPrill, 'SELECT ID, PRILL FROM D_PRILL ORDER BY SORTIND', ROOT_PRILL_CODE);
  if((Dict and OPER_DICT)<>0)then LoadSL(FdOper, 'SELECT OPERID, OPERATION FROM D_OPER /*WHERE N > 0 OR NS > 0 OR RATE > 0*/ ORDER BY SORTIND', ROOT_OPER_CODE);
  if((Dict and OPERNOGOOD_DICT)<>0)then LoadSL(FdOperNoGood, 'SELECT OPERID, OPERATION FROM D_OPER WHERE A>=-2 AND A<0 ORDER BY SORTIND', ROOT_OPERNOGOOD_CODE);
  if((Dict and PS_DICT)<>0)then LoadSL(FdPS, 'SELECT DEPID, NAME FROM D_DEP WHERE band(DEPTYPES, 1)=1 and band(DEPTYPES, 16)=0 ORDER BY SORTIND', ROOT_PS_CODE);
  if((Dict and WHPROD_DICT)<>0)then LoadSL(FdWhProd, 'SELECT DEPID, NAME FROM D_DEP WHERE band(DEPTYPES, 2)=2 ORDER BY SORTIND', ROOT_PS_CODE);
  if((Dict and SZ_DICT)<>0)then LoadSL(FdSz, 'SELECT ID, NAME FROM D_SZ ORDER BY SORTIND', ROOT_SZ_CODE);
  if((Dict and PSWHPROD_DICT)<>0)then LoadSL(FdPSWhProd, 'SELECT DEPID, NAME FROM D_DEP WHERE band(DEPTYPES, 2)=2 or band(DEPTYPES, 1)=1 ORDER BY SORTIND', ROOT_PS_CODE);
  if((Dict and COMP_DICT)<>0)then LoadSL(FdComp, 'SELECT COMPID, NAME FROM D_COMP ORDER BY SORTIND', ROOT_COMP_CODE);
  if((Dict and MATINS_DICT)<>0)then LoadSL(FdMatIns, 'SELECT ID, NAME FROM D_MAT WHERE ISINS=1 ORDER BY SORTIND', ROOT_MATINS_CODE);
end;

procedure Tdm.quUserBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['MOLID'].AsInteger := FUserId;
end;

procedure Tdm.taArtBeforeOpen(DataSet: TDataSet);
begin
  SetArtParams(TpFIBDataSet(DataSet).Params);
end;

procedure Tdm.taArtUNITIDGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
           0: Text:='��.';
           1: Text:='��.';
         end;
end;

procedure Tdm.taArtUNITIDSetText(Sender: TField; const Text: String);
begin
  if Text='��.' then Sender.AsInteger:=0
  else  Sender.AsInteger:=1;
end;

procedure Tdm.taArtNewRecord(DataSet: TDataSet);
begin
  taArtARTID.AsInteger := GetId(14);
  taArtUNITID.AsInteger := 1;
  taArtINSID.AsString := '-';
  taArtK.AsFloat := 0;
  taArtMW.AsFloat := 0;
end;

procedure Tdm.taArtAfterScroll(DataSet: TDataSet);
begin
  if (Screen.ActiveForm.ClassType = TfmSInv_det)and(fmSInv_det.FEnableScroll) then
  begin
    ReOpenDataSets([dmINV.taA2,dmInv.taOutStore]);
    TfmSInv_det(Screen.ActiveForm).imArt.Picture.Assign(LoadDbPicture(dm.taArtPICT, TJPEGImage));
  end;
  if ShowArtIns then ReOpenDataSet(taAIns);
end;

procedure Tdm.taPSBeforeOpen(DataSet: TDataSet);
begin
  taPs.Params[0].AsInteger := D_DepId;
end;

procedure Tdm.taPSNewRecord(DataSet: TDataSet);
begin
  taPSID.AsInteger := GetId(51);
  taPSDEPID.AsInteger := D_DepId;
  taPSNAME.AsString := taDepNAME.AsString;
  taPSISWORK.AsInteger := 1;
end;

procedure Tdm.taDepAfterOpen(DataSet: TDataSet);
begin
  ReOpenDataSets([taPs, taPsOper]);
end;

procedure Tdm.taPSBeforeInsert(DataSet: TDataSet);
begin
  PostDataSet(taDep);
  D_DepId := taDepDEPID.AsInteger;
end;

procedure Tdm.GlobalInitialization;
var
  v : Variant;


procedure InitUserDepartments;
var
  i: Integer;
  UserDepartment: Integer;
//  DepartmentInfo: TDepInfo;
begin
  taUserDepartments.ParamByName('USERID').AsInteger := User.UserID;
  OpenDataSet(taUserDepartments);
  for i := 0 to CountDep - 1 do
  FDepInfo[i].Enabled := False;
  while not taUserDepartments.Eof do
  begin
    UserDepartment := taUserDepartments.FieldByName('DepID').AsInteger;
    for i := 0 to CountDep - 1 do
    begin
      if FDepInfo[i].DepId = UserDepartment then
      begin
        FDepInfo[i].Enabled := True;
        break;
      end;
    end;
    taUserDepartments.Next;
  end;
  CloseDataSet(taUserDepartments);
end;

begin
  FInvMode := -1;
  FCurrInv := -1;

  InitPeriod;

  FwhBeginDate := StartOfTheMonth(ToDay);
  FwhEndDate := EndOfTheMonth(EndOfTheDay(Today));

  FaBeginDate := StartOfTheMonth(ToDay);
  FaEndDate := EndOfTheMonth(EndOfTheDay(Today));

  FzBeginDate := FBeginDate;
  FzEndDate := FEndDate;

  whBeginDate:= StartOfTheMonth(ToDay);
  whEndDate := EndOfTheMonth(EndOfTheDay(Today));

  InitSetting;

  (* ���������� ������ '������������' � ������ ��������� *)
  {SetLength(SettingTextNodes, Length(SettingTextNodes)+1);
  SettingTextNodes[Length(SettingTextNodes)-1] := User.FIO;}
  InitDeps;
  InitDicts;
  InitArtParams;
  InitArtCode;
  (* ���������������� �������� *)
  RejOperId := ExecSelectSQL('select OPERID from D_Oper where A=-1', quTmp);
  NkOperId :=  ExecSelectSQL('select OPERID from D_Oper where A=-2', quTmp);
  v := ExecSelectSQL('select OPERID from D_Oper where Last=1', quTmp);
  if not VarIsNull(v) then LastOperId := VarToStr(v)
  else begin
    MessageDialog('� ����������� �������� �� ���������� ��������� ��������', mtWarning, [mbOk], 0);
    LastOperId := '';
  end;

  TransformOperId := ExecSelectSQL('select OPERID from D_Oper where A = -3', quTmp);
  TransformOperName := ExecSelectSQL('select OPERATION from D_Oper where A = -3', quTmp);
  ReAssemblyId := ExecSelectSQL('select OPERID from D_Oper where A = -4', quTmp);
  ModifyAssortOperId := ExecSelectSQL('select OPERID from D_Oper where A = -5', quTmp);

  (* ���� � �� *)

  FDbPath := ExecSelectSQL('select DBPATH from D_Rec', quTmp);
  InitLocalSetting;
  InitUserDepartments;
end;

procedure Tdm.SetWhEndDate(const Value: TDateTime);
begin

  if FwhBeginDate > Value then
  begin
    raise Exception.Create('����� ������� �� ����� ���� ������ ������ ������ �������');
  end;

  FwhEndDate := Value;

end;

procedure Tdm.SetAPeriodParams(Params: TFIBXSQLDA);
begin
  with Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(aBeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(aEndDate);
  end;
end;

procedure Tdm.SetWhPeriodParams(Params: TFIBXSQLDA);
begin
  with Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(whBeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(whEndDate);
  end;
end;

procedure Tdm.taArt2NewRecord(DataSet: TDataSet);
begin
  taArt2ART2ID.AsInteger := GetId(65);
  taArt2D_ARTID.AsInteger := AArtId;
  ANewArt2Id := taArt2ART2ID.AsInteger;
end;

procedure Tdm.taArt2BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ARTID'].AsInteger := AArtId;
end;

procedure Tdm.taA2InsBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ART2ID'].AsInteger := AArt2Id;
end;

procedure Tdm.taA2InsNewRecord(DataSet: TDataSet);
begin
  taA2InsID.AsInteger := GetId(66);
  taA2InsQUANTITY.AsInteger := 0;
  taA2InsWEIGHT.AsInteger := 0;
  taA2InsART2ID.AsInteger := AArt2Id;
  if VarIsNull(Rec.DefaultInsOwner) then
    raise Exception.Create('�� ��������� ���� �������� ������������ ����� �� ���������'); 
  taA2InsCOMPID.AsInteger := Rec.DefaultInsOwner;
  taA2InsCOMPNAME.AsString := Rec.DefaultInsOwnerName;
end;

procedure Tdm.InitPeriod(Bd, Ed : TDateTime);
begin
  FBeginDate := Bd;
  FEndDate := Ed;
end;

procedure Tdm.InitPeriod;
//var
//  bd, ed : Variant;
begin
{  bd := ExecSelectSQL('select max(DocDate) from Act where IsClose=1 and T in (1,2)', quTmp);
  if VarIsNull(bd) then FBeginDate := StrToDateTime(BeginDateByDefault) else FBeginDate := StartOfTheDay(bd)+1;
  if FBeginDate > EndOfTheDay(ToDay) then FEndDate := EndOfTheDay(EndOfTheMonth(FBeginDate))
  else FEndDate := EndOfTheDay(ToDay);}
  FBeginDate := StartOfTheMonth(ToDay);
  FEndDate := EndOfTheDay(EndOfTheMonth(FBeginDate));
end;

procedure Tdm.UpdArt2(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
  if FChangeProp then
  begin
    beep;
  end;
  FChangeProp := False;
end;

procedure Tdm.taMatNewRecord(DataSet: TDataSet);
begin
  taMatID.AsString := IntToStr(GetId(50));
  taMatGOOD.AsInteger := 0;
  taMatALG_CALCPRICE.AsInteger := 0;
  taMatISINS.AsInteger := 0;
  taMatGOOD.AsInteger := 0;
  taMatPRICE_K.AsFloat:= 0;
  taMatN.AsFloat := 0;
  taMatBUH0.AsFloat := 1;
end;

procedure Tdm.taMatPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if(pos('UNQ1_D_MAT', E.Message)<>0)then
  raise EWarning.Create(Format(rsNotUniqueName, ['���������.']));
end;

procedure Tdm.taSemisBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    if AMatId = ROOT_MAT_CODE then
      try
        ByName['MAT1'].AsString := Low(Char);
        ByName['MAT2'].AsString := High(Char);
      except
      end
    else
      try
        ByName['MAT1'].AsString := AMatId;
        ByName['MAT2'].AsString := AMatId
      except
      end;
end;

procedure Tdm.taSemisBeforePost(DataSet: TDataSet);
begin
  if Trim(taSemisSemis.AsString) = '' then
  raise EWarning.Create(rsNotSelFieldName);

  if taSemis.State = dsInsert then
  begin
    if Trim(taSemisSEMISID.AsString) = '' then
    taSemisSEMISID.AsString := IntToStr(GetId(39));
  end;
end;

procedure Tdm.taA2InsCalcFields(DataSet: TDataSet);
//var
//  r : Variant;
begin
  taA2InsRecNo.AsInteger := DataSet.RecNo;
  if taA2InsINSID.AsString <> '' then
  begin
    with quTmp do
    begin
      if Open then Close;
      SQL.Text := 'select EDGSHAPEID, EDGTID, CLEANNES, CHROMATICITY, IGR, COLOR '+
        'from D_Semis where SemisId='#39+taA2InsINSID.AsString+#39;
      ExecQuery;
      taA2InsC_EDGSHAPEID.AsString := Fields[0].AsString;
      taA2InsC_EDGTYPEID.AsString := Fields[1].AsString;
      taA2InsC_CLEANNES.AsString := Fields[2].AsString;
      taA2InsC_CHROMATICITY.AsString := Fields[3].AsString;
      taA2InsC_IGR.AsString := Fields[4].AsString;
      taA2InsC_COLOR.AsString := Fields[5].AsString;
      Transaction.CommitRetaining
    end;
  end;
end;

procedure Tdm.taCodeNewRecord(DataSet: TDataSet);
begin
  taCodeID.AsInteger := GetId(30);
  taCodeDICT.AsInteger := fmCode.DictId;
  taCodeN2.AsInteger := fmCode.N;
  taCodePCOEF.AsFloat := 0;
end;

procedure Tdm.taPsOperBeforeInsert(DataSet: TDataSet);
begin
  PostDataSet(taDep);
  D_DepId := taDepDEPID.AsInteger;
end;

procedure Tdm.taPsOperBeforeOpen(DataSet: TDataSet);
begin
  taPsOper.Params[0].AsInteger := D_DepId;
end;

procedure Tdm.taPsOperNewRecord(DataSet: TDataSet);
begin
  taPsOperID.AsInteger := GetId(52);
  taPSOPERDEPID.AsInteger := D_DepId;
  taPSOPERDEPNAME.AsString := taDepNAME.AsString;
end;

procedure Tdm.InitArtCode;
begin
  FArtLength := ExecSelectSQL('select count(distinct N) from D_Code', quTmp);
end;

function Tdm.CheckValidArt(Art: string): boolean;
begin
  Result := False;
  //ShowMessage('ArtLength: ' + inttostr(ArtLength) + '; Length(Art): ' + IntTostr(Length(Art)) + '; Art: ' + Art + '.');
  if ArtLength<>Length(Art) then
  begin
    if Length(Art) = 8 then
    begin
      if Art[8] <> '+' then Exit;
    end
    else Exit;
  end;
  Result := True;
end;

function Tdm.CheckValidArt2(Art2: string): boolean;
var
  a2 : integer;
begin
  Result := False;
  if(Art2 = '-')then Result := True
  else begin
    a2 := StrToIntDef(Art2, -1);
    if(a2 <> -1)then Result := True;
  end;
end;


function Tdm.GetArtVal(N : integer; Key : string): Variant;
begin
  OpenDataSet(taArtCode);
  if taArtCode.Locate('N;K', VarArrayOf([N, Key]), [])
  then Result := taArtCodeVAL.Value else Result := null;
end;

function Tdm.InsertArt(Art: string) : integer;
var
//  ArtId : integer;
  i : integer;
  Val : Variant;
  ArtField : string;
begin
  raise EWarning.Create('�������� '+Art+' ��� � ����������.'); 
  Result := -1;
  quInsArt.Close;
  if quInsArt.Transaction.Active then quInsArt.Transaction.CommitRetaining
  else quInsArt.Transaction.StartTransaction;
  for i:=1 to ArtLength do
  begin
    Val := GetArtVal(i, Art[i]);
    if VarIsNull(Val) then continue;
    ArtField := GetArtField(i);
    try
      quInsArt.ParamByName(ArtField).AsVariant := Val;
    except
    end;
  end;
  Result := GetId(14);
  quInsArt.ParamByName('D_ARTID').AsInteger := Result;
  { TODO : ������������� }
  if Art[3]='7' then quInsArt.ParamByName('UNITID').AsShort := 0
  else quInsArt.ParamByName('UNITID').AsShort := 1;
  quInsArt.ParamByName('ART').AsString := Art;
  try
    quInsArt.ExecQuery;
  except
    ShowMessage('���������� �������� ����� �������!'+#13#10+quInsArt.SQL.Text);
  end;
  quInsArt.Transaction.CommitRetaining;
end;

function Tdm.GetArtField(N: integer): Variant;
begin
  OpenDataSet(taArtCode);
  if taArtCode.Locate('N', N, []) then
  begin
    Result := ExecSelectSQL('select ArtId from D_Dict where Id='+taArtCodeDICT.AsString, quTmp);
    if VarIsNull(Result)or(Result='') then Result := null;
  end
  else Result := null;
end;

procedure Tdm.taOperInBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['OPERID'].AsString := taOperOPERID.AsString;
end;

procedure Tdm.taOperOutBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['OPERID'].AsString := taOperOPERID.AsString;
end;

procedure Tdm.taOperTailBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['OPERID'].AsString := taOperOPERID.AsString;
end;

procedure Tdm.taOperInNewRecord(DataSet: TDataSet);
begin
  taOperInID.AsInteger := GetId(54);
  taOperInOPERID.AsString := taOperOPERID.AsString;
  taOperInOPERATION.AsString := taOperOPERATION.AsString;
end;

procedure Tdm.taOperOutNewRecord(DataSet: TDataSet);
begin
  taOperOutID.AsInteger := GetId(55);
  taOperOutOPERID.AsString := taOperOPERID.AsString;
  taOperOutOPERATION.AsString := taOperOPERATION.AsString;
end;

procedure Tdm.taOperPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if(pos('PK_D_OPER', E.Message)<>0)then
  raise EWarning.Create(Format(rsNotUniqueId, ['��������']));

  if(pos('UNQ1_D_OPER', E.Message)<>0)then
  raise EWarning.Create(Format(rsNotUniqueName, ['��������']));
end;

procedure Tdm.taOperTailNewRecord(DataSet: TDataSet);
begin
  taOperTailID.AsInteger := GetId(56);
  taOperTailOPERID.AsString := taOperOPERID.AsString;
  taOperTailOPERATION.AsString := taOperOPERATION.AsString;
end;

procedure Tdm.E410MReceiveData(Sender: TObject; DataPtr: Pointer;
  DataSize: Integer);
var
  s : string;
//  Addr : Pointer;
begin
  OnGetDefaultIniName := GetDefaultIniName;
  beep;
//  Addr := DataPtr;
  s:=PCHAR(DAtaPtr^);
  //fmMain.Label1.Caption:=s;
end;

procedure Tdm.taOperNewRecord(DataSet: TDataSet);
begin
  taOperLAST.AsInteger := 0;
  taOperUQ.AsInteger := 1;
  taOperA.AsInteger := 0;
  taOperN.AsFloat := 0;
  taOperN1.AsFloat := 0;
  taOperNQ.AsInteger := 0;
  taOperRATE.AsFloat := 0;
  taOperZT.AsInteger := 0;
end;

procedure Tdm.taOperRejBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['OPERID'].AsString := taOperOPERID.AsString;
end;

procedure Tdm.taOperRejNewRecord(DataSet: TDataSet);
begin
  taOperRejID.AsInteger := GetId(57);
  taOperRejOPERID.AsString := taOperOPERID.AsString;
  taOperRejOPERATION.AsString := taOperOPERATION.AsString;
end;

procedure Tdm.taOperAGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text := ''
  else case Sender.AsInteger of
     0 : Text := '������� ��������';
    -1 : Text := '����';
    -2 : Text := '����������';
    -3 : Text := '�������������';
    else Text := '';
  end;
end;

procedure Tdm.taOperASetText(Sender: TField; const Text: String);
begin
  if(Text = '������� ��������')then Sender.AsInteger := 0
  else if(Text = '����')then Sender.AsInteger := -1
  else if(Text = '����������')then Sender.AsInteger := -2
  else if(Text = '�������������')then Sender.AsInteger := -3
  else Sender.AsInteger := 0;
end;

procedure Tdm.taOperUQGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text := ''
  else case Sender.AsInteger of
    1 : Text := '��.';
    2 : Text := '����';
  end;
end;

procedure Tdm.taOperUQSetText(Sender: TField; const Text: String);
begin
  if(Text = '��.')then Sender.AsInteger := 1
  else if(Text = '����')then Sender.AsInteger := 2;
end;

procedure Tdm.taSemisNewRecord(DataSet: TDataSet);
begin
  //taSemisSEMISID.AsString := IntToStr(GetId(39));
  taSemisSEMIS.AsString := ' ';
  taSemisMAT.AsString := '1';  // ������
  taSemisPRILL.AsInteger := 1; // 585
  taSemisUQ.AsInteger := 0;
  taSemisUW.AsInteger := 3;
  taSemisQ0.AsInteger := 1;
  taSemisGOOD.AsInteger := 0;
  taSemisPRICE.AsFloat := 0;
  taSemisBUH0.AsInteger := 1;
  taSemisBUHU.AsInteger := 0;
  taSemisBUHPRICE.AsFloat := 0;
  taSemisUSEINART.AsInteger := 0;
  taSemisAFFINAJ.AsInteger := 0;
  taSemisISUSE.AsInteger := 1;
end;

function Tdm.GetDepBeginDate(DepId: integer): TDateTime;
var
  dt : Variant;
begin
  // ��� ������������
  dt := ExecSelectSQL('select max(a.DocDate) from Act a '+
                      'where a.T = 2 and '+
                      '      a.IsClose=1 and '+
                      '      a.DepId = '+IntToStr(DepId), dmMain.quTmp);
  dm.PsTypeBeginDate := 1;
  if not VarIsNull(dt)then Result := VarToDateTime(dt)
  else Result := StrToDateTime(BeginDateByDefault);
end;

function Tdm.GetPsBeginDate(PsId: integer): TDateTime;
begin

end;

procedure Tdm.BlockApp;
var
  UseCompLogin : boolean;
begin
  if not LoginDB(db, 'D_MOL', 'FName', LoginFunction, MAXINT, False, UseCompLogin, True)then Application.Terminate;
end;

procedure Tdm.taMatALG_CALCPRICESetText(Sender: TField; const Text: String);
begin
  if(Text = '��� ��������� (�������)')then Sender.AsInteger := 1
  else if(Text = '��� ������� �������������')then Sender.AsInteger := 2
  else Sender.AsVariant := Null;
end;

procedure Tdm.taMatBeforePost(DataSet: TDataSet);
begin
  if Trim(taMatNAME.AsString) = '' then
  raise EWarning.Create(rsNotSelFieldName);
end;

procedure Tdm.taTransMatBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['OPERID'].AsString := taOperOPERID.AsString;
end;

procedure Tdm.taTransMatNewRecord(DataSet: TDataSet);
begin
  taTransMatID.AsInteger := GetId(53);
  taTransMatBASEMAT.AsString := '1';
  taTransMatOPERID.AsString := taOperOPERID.AsString;
  taTransMatN.AsFloat := 0;
  taTransMatNT.AsInteger := 1; //�� ���-��
  taTransMatK.AsInteger := 1;
end;

procedure Tdm.taZPKBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['OPERID'].AsString := AOperId;
end;

procedure Tdm.taZPKCalcFields(DataSet: TDataSet);
begin
  taZPKKT.AsFloat := taZPKARTK.AsFloat + taZPKK.AsFloat;
end;

procedure Tdm.taTailsNewRecord(DataSet: TDataSet);
begin
  taTailsID.AsInteger := GetId(48);
  taTailsT.AsInteger := 0;
  taTailsS.AsInteger := 0;
end;

procedure Tdm.taOperBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ��������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure Tdm.taOperBeforePost(DataSet: TDataSet);
begin
  if Trim(taOperOPERID.AsString) = '' then
  raise EWarning.Create(rsNotSelFieldId);
  if Trim(taOperOPERATION.AsString) = '' then
  raise EWarning.Create(rsNotSelFieldName);
end;

procedure Tdm.taMOLBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� �����������-������������� ����?', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
end;

procedure Tdm.taArtBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� �������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
  try
    ExecSQL('delete from Art2 where D_ArtId='+dm.taArtARTID.AsString, dm.quTmp);
  except
    MessageDialogA('������� ������� '+dm.taArtART.AsString+' ����������', mtInformation);
    SysUtils.Abort;
  end;
end;

procedure Tdm.taArtBeforePost(DataSet: TDataSet);
var
  Art: string;
  ArtID: Variant;
begin
  if taArt.State = dsInsert then
  begin

  Art := Trim(taArt.FieldByName('ART').AsString);
  ArtID := ExecSelectSQL('select D_ARTID from  d_art where art = ' + #39 + Art + #39, quTmp);
  if not VarIsNull(ArtID) then
  begin
    Application.MessageBox('������� � ����� ������������ ��� ����������.', '������', MB_OK);
    Abort;
  end;

  end;

  if taArtSEMIS_PROD.IsNull then raise EWarning.Create(Format(rsFieldNull, ['������������ � ������������']));
  if taArtSEMIS.IsNull then raise EWarning.Create(Format(rsFieldNull, ['�����������']));
  if taArtINSID.IsNull then raise EWarning.Create(Format(rsFieldNull, ['�������� �������']));
  if taArtUNITID.IsNull then raise EWarning.Create(Format(rsFieldNull, ['������� ���������']));
end;

procedure Tdm.InitSetting;
var
  b : boolean;
//  r : Variant;
  s : TMemoryStream;
begin
  b := tr.Active;
  if not b then tr.StartTransaction;
  ReOpenDataSet(taRec);
  FOrganizationId := FUserInfo.SelfCompId; //taRecCOMPID.AsInteger;
  FOrganizationName := FUserInfo.SelfCompName;   //taRecNAME.AsString;
  FNotif.Notif_Ip := taRecNOTIF_IP.AsString;
  FNotif.Notif_Port := taRecNOTIF_PORT.AsString;
  FNotif.Notif_Use := taRecNOTIF_USE.AsInteger = 1;
  if taRecDEFAULTCONTRACTID.IsNull then
  begin
    MessageDialog('�� ��������� ���� ��� ������� �� ��������� ��� ����������', mtWarning, [mbOk], 0);
    FRec.DefaultContractId := -1;
  end
  else FRec.DefaultContractId := taRecDEFAULTCONTRACTID.AsInteger;

  if taRecDEFAULTPRODCONTRACTID.IsNull then
  begin
    MessageDialog('�� ��������� ���� ��� �������� �� ��������� ��� ������� ���������', mtWarning, [mbOk], 0);
    FRec.DefaultProdContractId := -1;
  end
  else FRec.DefaultProdContractId := taRecDEFAULTPRODCONTRACTID.AsInteger;

    // ���������� � �����
  if Assigned(FRec.Sticker) then FRec.Sticker.Free;
  FRec.Sticker := TStringList.Create;
  s := TMemoryStream.Create;
  try
    taRecSTICKER.SaveToStream(s);
    s.Position := 0;
    FRec.Sticker.LoadFromStream(s);
  finally
    s.Free;
  end;

  // �������� ���
  if Assigned(FRec.Sticker_OTK) then FRec.Sticker_OTK.Free;
  FRec.Sticker_OTK := TBitmap.Create;
  s := TMemoryStream.Create;
  try
    taRecSTICKER_OTK.SaveToStream(s);
    s.Position := 0;
    FRec.Sticker_OTK.LoadFromStream(s);
  finally
    s.Free;
  end;
  FRec.ChangeApplOperId := taRecCHANGEAPPLOPERID.AsVariant;
  FRec.ExeUrl := taRecEXEPATH.AsString; 
  CloseDataSet(taRec);

  FRec.DefaultInsOwner := ExecSelectSQL('select InsOwner from D_Comp where CompId='+IntToStr(FUserInfo.SelfCompId), quTmp);
  if VarIsNull(FRec.DefaultInsOwner) then
    FRec.DefaultInsOwner := FUserInfo.SelfCompId;
  FRec.DefaultInsOwnerName := ExecSelectSQL('select Name from D_Comp where CompId='+VarToStr(FRec.DefaultInsOwner), quTmp);
  try
    FRec.NormaOrderDate := ExecSelectSQL('select NORMAORDERDATE from D_COMP where COMPID='+IntToStr(FUserInfo.SelfCompId), quTmp);
  except
    FRec.NormaOrderDate := Null;
  end;
  try
    FRec.ZpRateOrderDate := ExecSelectSQL('select ZPRATEORDERDATE from D_COMP where COMPID='+IntToStr(FUserInfo.SelfCompId), quTmp);
  except
    FRec.ZpRateOrderDate := Null;
  end;
  if b then tr.CommitRetaining else tr.Commit;
end;

procedure Tdm.taSemisAffNewRecord(DataSet: TDataSet);
begin
  taSemisAffID.AsInteger := dm.GetId(41);
end;

procedure Tdm.taRejNewRecord(DataSet: TDataSet);
begin
  taRejID.AsInteger := dm.GetId(43);
  taRejTYPES.AsInteger := 0;
  taRejPSIDMUSTHAVEVALUE.AsInteger := 0;
end;

procedure Tdm.taAInsBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ARTID'].AsInteger := taArtARTID.AsInteger;
end;

procedure Tdm.taAInsNewRecord(DataSet: TDataSet);
begin
  taAInsID.AsInteger := GetId(44);
  taAInsARTID.AsInteger := taArtARTID.AsInteger;
  taAInsQUANTITY.AsInteger := 0;
  taAInsWEIGHT.AsFloat := 0;
  taAInsMAIN.AsInteger := 0;
end;

procedure Tdm.taArtAfterOpen(DataSet: TDataSet);
begin
  if ShowArtIns then ReOpenDataSet(taAIns);
end;

procedure Tdm.taArtAfterClose(DataSet: TDataSet);
begin
  if ShowArtIns then CloseDataSet(taAIns);
end;

procedure Tdm.taAInsCalcFields(DataSet: TDataSet);
begin
  if(taAInsINSID.AsString <> '')or(taAInsINSID.IsNull) then
  begin
    with quTmp do
    begin
      if Open then Close;
      SQL.Text := 'select EDGSHAPEID, EDGTID, CLEANNES, CHROMATICITY, IGR, COLOR '+
        'from D_Semis where SemisId='#39+taAInsINSID.AsString+#39;
      ExecQuery;
      taAInsEDGSHAPEID.AsString := Fields[0].AsString;
      taAInsEDGTYPEID.AsString := Fields[1].AsString;
      taAInsCLEANNES.AsString := Fields[2].AsString;
      taAInsCHROMATICITY.AsString := Fields[3].AsString;
      taAInsIGR.AsString := Fields[4].AsString;
      taAInsCOLOR.AsString := Fields[5].AsString;
      Transaction.CommitRetaining
    end;
  end;
end;

procedure Tdm.taRejAfterOpen(DataSet: TDataSet);
begin
  if(WorkMode = 'DepT1')and(taDepart.Active)then
    if(not taRejPSID.IsNull)then taDepart.Locate('DEPID', taRejPSID.AsInteger, [])
    else taDepart.Locate('DEPID', -1, []);
end;



procedure Tdm.taPrillNewRecord(DataSet: TDataSet);
begin
  taPrillID.AsInteger := GetId(49);
end;

procedure Tdm.taConstNewRecord(DataSet: TDataSet);
begin
  taConstCONST_ID.AsInteger := GetId(58);
end;

procedure Tdm.taTechBeforeDelete(DataSet: TDataSet);
var
  c: Integer;
begin
  c := ExecSelectSQL('select count(*) from ZPK_TECH where techid = ' + intToStr(taTechID.AsInteger) + ' and K <> 0', quTmp, False);
  if c = 0 then
  begin
    if MessageDialog('������� ����������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
  end
  else
  begin
    if MessageDialog('�� ������ ���������� ������������'#13#10'����������� ��� ��������� ��������.'#13#10'������� ����������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
  end;
end;


procedure Tdm.taTechNewRecord(DataSet: TDataSet);
begin
  taTechID.AsInteger := GetId(61);
end;

procedure Tdm.taEdgShapeNewRecord(DataSet: TDataSet);
begin
  taEdgShapeID.AsInteger := GetId(62);
end;

procedure Tdm.taEdgTNewRecord(DataSet: TDataSet);
begin
  taEdgTID.AsInteger := GetId(63);
end;

procedure Tdm.taSzNewRecord(DataSet: TDataSet);
begin
  taSzID.AsInteger := GetId(64);
end;

procedure Tdm.taInitStoneNewRecord(DataSet: TDataSet);
begin
  taInitStoneID.AsInteger := GetId(77);
  taInitStoneADATE.AsDateTime := InitStone_ADate;
  taInitStoneQ.AsInteger := 0;
  taInitStoneW.AsFloat := 0;
  taInitStoneUQ.AsInteger := 0;
  taInitStoneUW.AsInteger := 3;
end;


function Tdm.GetStoreBeginDate: TDateTime;
var
  dt : Variant;
begin
  // ��� ������������
  dt := ExecSelectSQL('select max(a.DocDate) from Act a '+
                      'where a.T = 4 and '+
                      '      a.IsClose=1  ', dmMain.quTmp);
  if not VarIsNull(dt)then Result := VarToDateTime(dt)
  else Result := StrToDateTime(BeginDateByDefault);
end;

procedure Tdm.taSemisPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('PK_D_SEMIS', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueId, ['�������������']));
  if(pos('UNQ1_D_SEMIS', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['�������������']));
end;

procedure Tdm.taInitStoneBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ADATE'].AsTimeStamp := DateTimeToTimeStamp(InitStone_ADate);
end;


procedure Tdm.taPaytypeNewRecord(DataSet: TDataSet);
begin
  taPaytypePAYTYPEID.AsInteger := GetId(79);
  taPaytypeAPPLT.AsInteger := 0;
end;


function Tdm.GetId2(TableId, DocId: integer): integer;
begin
  with quGetID2 do
  begin
    Params[0].AsInteger:=TableID;
    Params[1].AsInteger:=DocId;
    ExecQuery;
    Result:=Fields[0].AsInteger;
    Close;
  end;
end;

function Tdm.GetId3(TableId, DepId, DepFromId : integer) : integer;
begin
  with quGetId3 do
  begin
    ParamByName('TABLEID').AsInteger := TableId;
    ParamByName('DEPID').AsInteger := DepId;
    ParamByName('DEPFROMID').AsInteger := DepFromId;
    ExecQuery;
    Result := Fields[0].AsInteger;
    Close;
  end;
end;

procedure Tdm.InitLocalSetting;
var
  Ini : TIniFile;
  sect : TStringList;
//  s : string;
begin
  Ini := TIniFile.Create(GetIniFileName);
  sect := TStringList.Create;
  try
    // ������ LocalSetting
    Ini.ReadSectionValues('LocalSetting', sect);
    FLocalSetting.UseStoneDialog := sect.Values['UseStoneDialog'] = '1';
    FLocalSetting.BarCode_Model := bcmEZ1100;
    FLocalSetting.BarCode_Use := sect.Values['BarCodePrinterUse']='1';
    if(sect.Values['BarCodePrinterPort']='LPT1')then FLocalSetting.BarCode_Port := 0
    else if(sect.Values['BarCodePrinterPort']='COM1')then FLocalSetting.BarCode_Port := 1
    else if(sect.Values['BarCodePrinterPort']='COM2')then FLocalSetting.BarCode_Port := 2
    else if(sect.Values['BarCodePrinterPort']='COM3')then FLocalSetting.BarCode_Port := 3
    else if(sect.Values['BarCodePrinterPort']='COM3')then FLocalSetting.BarCode_Port := 4
    else if(sect.Values['BarCodePrinterPort']='LPT2')then FLocalSetting.BarCode_Port := 5
    else FLocalSetting.BarCode_Port := 0; // LPT1

    FLocalSetting.BarCode_Count := StrToIntDef(sect.Values['BarCodePrinterCount'], 0);
    FLocalSetting.BarCode_Step := sect.Values['BarCodePrinterStep'];

    FLocalSetting.Scale_Use := sect.Values['ScaleUse']= '1';
    FLocalSetting.Scale_Model := scmE410M;

    if sect.IndexOfName('ScaleClass') = -1 then FLocalSetting.ScaleClass := -1
    else FLocalSetting.ScaleClass := StrToInt(sect.Values['ScaleClass']);

    if sect.Values['ScalePort']='COM1' then FLocalSetting.Scale_Port := 0
    else if sect.Values['ScalePort']='COM2' then FLocalSetting.Scale_Port := 1
    else if sect.Values['ScalePort']='COM3' then FLocalSetting.Scale_Port := 2
    else if sect.Values['ScalePort']='COM4' then FLocalSetting.Scale_Port := 3
    else FLocalSetting.Scale_Port := 0;

    FLocalSetting.BarCodeScaner_Use := sect.Values['BarCodeScanerUse'] = '1';
    if sect.Values['BarCodeScanerPort']='COM1' then FLocalSetting.BarCodeScaner_Port := 0
    else if sect.Values['BarCodeScanerPort']='COM2' then FLocalSetting.BarCodeScaner_Port := 1
    else if sect.Values['BarCodeScanerPort']='COM3' then FLocalSetting.BarCodeScaner_Port := 2
    else if sect.Values['BarCodeScanerPort']='COM4' then FLocalSetting.BarCodeScaner_Port := 3
    else FLocalSetting.BarCodeScaner_Port := 0;

    
    // ������ Cassa
    Ini.ReadSectionValues('Cassa',sect);
    FLocalSetting.Cassa_UseNDS := sect.Values['UseNDS'] = '1';
    if sect.Values['NDS']='' then FLocalSetting.Cassa_NDS := 0
    else FLocalSetting.Cassa_NDS := ConvertStrToFloat(sect.Values['NDS']);
    if sect.Values['Port']='' then FLocalSetting.Cassa_Port := 'COM1'
    else FLocalSetting.Cassa_Port := sect.Values['Port'];
  finally
    Ini.Free;
    sect.Free;
  end;
end;

procedure Tdm.taArtPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('D_ART_IDX1', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['��������']));
end;

procedure Tdm.DataModuleDestroy(Sender: TObject);
begin
  try
    ExecSQL('update LOG$USER set DISCONNECTDATE='#39+DateTimeToStr(Now)+#39' where Id='+IntToStr(FUserInfo.LogId), quTmp);
  except
  end;
end;

procedure Tdm.taClientBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ���������� ����������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;


procedure Tdm.taContractNewRecord(DataSet: TDataSet);
begin
  taContractID.AsInteger := GetId(83);
  taContractNAME.AsString := '����� �������';
  taContractT0.AsInteger := 0;
end;

procedure Tdm.taContractDeleteError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if Pos('FK_INV_CONTRACTID', E.Message)<>0 then
  begin
    Action := daAbort;
    MessageDialogA('��� �������� "'+taContractNAME.AsString+'" ������������ � ���������.'#13+'�������� ����������.', mtError);
  end;
end;

procedure Tdm.taContractBeforeDelete(DataSet: TDataSet);
begin
   if MessageDialog('������� ��� ��������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;


function Tdm.ServerDate: TDateTime;
begin
  Result := ExecSelectSQL('select cast("NOW" as date) from rdb$database', quDateTime);
end;

function Tdm.ServerTime: TTime;
begin
  Result := ExecSelectSQL('select cast("NOW" as time) from rdb$database', quDateTime);
end;

function Tdm.ServerDateTime: TDateTime;
begin
  Result := ExecSelectSQL('select cast("NOW" as timestamp) from rdb$database', quDateTime);
end;

procedure Tdm.taInvColorNewRecord(DataSet: TDataSet);
begin
  taInvColorID.AsInteger := GetId(84);
end;

function Tdm.CalcNDS(Cost, NDS: double; T: byte): double;
begin
  case T of
    0 : Result := Cost*NDS/100;
    1 : Result := Cost*NDS/(100+NDS);
  end;
end;

procedure Tdm.taTransMatKGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  case Sender.AsInteger of
    -1 : Text := '-';
     1 : Text := '+';
    else Text := Sender.AsString;
  end;
end;

procedure Tdm.taTransMatKSetText(Sender: TField; const Text: String);
begin
  if Text='-' then Sender.AsInteger := -1
  else if Text='+' then Sender.AsInteger := 1
  else Sender.AsInteger := 1;
end;


procedure Tdm.SaveCompLoginInfo(CompId: integer; UseTolling: boolean;
  TollingType, TollingCompId: integer);
begin
  FUserInfo.SelfCompId := CompId;
  FUserInfo.UseTolling := UseTolling;
  FUserInfo.TollingCompId := TollingCompId;
  FUserInfo.TollingType := TollingType;
end;

procedure Tdm.quDepBeforeOpen(DataSet: TDataSet);
begin
  SetSelfCompParams(quDep.Params);
end;

procedure Tdm.quDepTBeforeExecute(Sender: TObject);
begin
  SetSelfCompParams(quDepT.Params);
end;

procedure Tdm.taDepartBeforeOpen(DataSet: TDataSet);
begin
  SetSelfCompParams(taDepart.Params);
  taDepart.ParamByName('UserID').AsInteger := User.UserId;
end;

procedure Tdm.SetSchemaParams(Params: TFIBXSQLDA);
begin
  with Params do
    if SchemaId = -1 then
    begin
      ByName['SCHEMAID1'].AsInteger := -MAXINT;
      ByName['SCHEMAID2'].AsInteger := MAXINT;
    end
    else begin
      ByName['SCHEMAID1'].AsInteger := SchemaId;
      ByName['SCHEMAID2'].AsInteger := SchemaId;
    end;    
end;

procedure Tdm.SetSelfCompParams(Params: TFIBXSQLDA);
begin
  with Params do
    ByName['SELFCOMPID'].AsInteger := FUserInfo.SelfCompId;
end;

procedure Tdm.tmDemoTimer(Sender: TObject);
begin
  {$ifdef TRIAL}
  ShowMessage('���� ������!');
  {$endif}
end;

procedure Tdm.UpdateNormaOrderDate(D: Variant);
begin
  FRec.NormaOrderDate := D;
end;

procedure Tdm.UpdateZpRateOrderDate(D: Variant);
begin
  FRec.ZpRateOrderDate := D;
end;

function Tdm.GetDepInfoById(Index: integer): TDepInfo;
var
  i : integer;
begin
  ZeroMemory(@Result, SizeOf(Result));

  for i:=0 to CountDep-1 do
    if FDepInfo[i].DepId = Index then
    begin
      Result := FDepInfo[i];
      eXit;
    end;
end;

procedure Tdm.taCompOwnerNewRecord(DataSet: TDataSet);
begin
  taCompOwnerID.AsInteger := GetId(87);
  taCompOwnerCOMPID.AsInteger := taCompCOMPID.AsInteger;
  taCompOwnerINFINITE_CONTRACT.AsInteger := 1;
end;

procedure Tdm.taArtRootNewRecord(DataSet: TDataSet);
begin
  taArtRootID.AsInteger := GetId(90);
  taArtRootARTID.AsInteger := taArtARTID.AsInteger;
  taArtRootMASK.AsString := '';
end;

procedure Tdm.taArtRootBeforeOpen(DataSet: TDataSet);
begin
  taArtRoot.ParamByName('ARTID').AsInteger := taArtARTID.AsInteger;
end;

procedure Tdm.SetBarCodePrinter_Count(const Value: integer);
begin
  FLocalSetting.BarCode_Count := Value;
end;

function Tdm.GetBarCodePrinter_Count: integer;
begin
  Result := FLocalSetting.BarCode_Count;
end;

function Tdm.GetBarCodePrinter_Step: string;
begin
  Result := FLocalSetting.BarCode_Step;
end;

procedure Tdm.SetBarCodePrinter_Step(const Value: string);
begin
  FLocalSetting.BarCode_Step := Value;
end;

function Tdm.GetId4(TableId, SelfCompId: integer): integer;
begin
  with quGetID4 do
  begin
    if not Transaction.Active then Transaction.StartTransaction;
    ParamByName('TABLEID').AsInteger := TableId;
    ParamByName('SELFCOMPID').AsInteger := SelfCompId;
    ExecQuery;
    Result:=Fields[0].AsInteger;
    Close;
  end;
end;

procedure Tdm.aplevActivate(Sender: TObject);
begin
//  if LocalSetting.BarCodeScaner_Use then fmMain.CommPortDriver1.Connect;
end;

procedure Tdm.aplevDeactivate(Sender: TObject);
begin
//  if LocalSetting.BarCodeScaner_Use then fmMain.CommPortDriver1.Disconnect;
end;

procedure Tdm.taSzPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('UNQ_D_SZ', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['�������']));
end;



procedure Tdm.taSzDiamondPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('PK_D_SZDIAMOND', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['������� ����������']));
end;

procedure Tdm.DeleteError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('FOREIGN KEY', E.Message) <> 0)then raise EWarning.Create(rsCantDeleteRecord);
end;

procedure Tdm.taInvColorBeforePost(DataSet: TDataSet);
begin
  if taInvColorNAME.IsNull then raise EWarning.Create(rsNotSelFieldName);
  if taInvColorCOLOR.IsNull then raise EWarning.Create(rsNotSelFieldColor);
  if taInvColorINVTYPEID.IsNull then raise EWarning.Create(rsNotSelFieldDocType);
end;

procedure Tdm.taClientNewRecord(DataSet: TDataSet);
begin
  taClientCLIENTID.Asinteger := GetId(95);
end;

procedure Tdm.taClientBeforePost(DataSet: TDataSet);
begin
  if taClientFNAME.IsNull then raise EWarning.Create(rsNotSelFieldFName);
  if taClientLNAME.IsNull then raise EWarning.Create(rsNotSelFieldLName);
  if taClientMNAME.IsNull then raise EWarning.Create(rsNotSelFieldMName)
end;

procedure Tdm.taSemisAffBeforePost(DataSet: TDataSet);
begin
  if taSemisAffNAME.IsNull then raise EWarning.Create(rsNotSelFieldName);
end;


procedure Tdm.taTechPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('D_TECH_IDX1', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['���������� ������������']));
end;

procedure Tdm.taTailsPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('D_TAILS_IDX1', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['���� ������']));
end;

procedure Tdm.taTailsBeforePost(DataSet: TDataSet);
begin
  if Trim(taTailsNAME.AsString) = '' then raise EWarning.Create(rsNotSelFieldName);
  if Trim(taTailsS.AsString) = '' then raise EWarning.Create(rsNotSelFieldKind);
end;

procedure Tdm.taInsPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if(pos('PK_D_INS', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueId, ['�������']));
  if(pos('UNQ1_D_INS', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['�������']));
end;

procedure Tdm.taContractPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('D_CONTRACT_IDX1', E.Message)<>0) then
    raise EWarning.Create(Format(rsNotUniqueName, ['��������']));
end;


procedure Tdm.taInsBeforePost(DataSet: TDataSet);
begin
  if(taInsD_INSID.OldValue <> taInsD_INSID.NewValue)then FChangeProp := True
  else FChangeProp := False;
  if Trim(taInsD_INSID.AsString) = '' then raise EWarning.Create(rsNotSelFieldId);
  if Trim(taInsNAME.AsString) = '' then raise EWarning.Create(rsNotSelFieldName);
end;

// --------------------------------------
procedure Tdm.taEdgShapeBeforePost(DataSet: TDataSet);
begin
  if(taEdgShapeID.OldValue <> taEdgShapeID.NewValue)then FChangeProp := True
  else FChangeProp := False;
  if Trim(taEdgShapeNAME.AsString) = '' then raise EWarning.Create(rsNotSelFieldName);
  if Trim(taEdgShapeID.AsString) = '' then raise EWarning.Create(rsNotSelFieldId);
end;

procedure Tdm.taEdgTBeforePost(DataSet: TDataSet);
begin
  if(taEdgTID.OldValue <> taEdgTID.NewValue)then FChangeProp := True
  else FChangeProp := False;
  if Trim(taEdgTNAME.AsString) = '' then raise EWarning.Create(rsNotSelFieldName);
  if Trim(taEdgTID.AsString) = '' then raise EWarning.Create(rsNotSelFieldId);
end;


procedure Tdm.taColorBeforePost(DataSet: TDataSet);
begin
  if(taColorID.OldValue <> taColorID.NewValue)then FChangeProp := True
  else FChangeProp := False;
  if Trim(taColorNAME.AsString) = '' then raise EWarning.Create(rsNotSelFieldName);
  if Trim(taColorID.AsString) = '' then raise EWarning.Create(rsNotSelFieldId);
end;

procedure Tdm.taCleannesBeforePost(DataSet: TDataSet);
begin
  if(taCleannesID.OldValue <> taCleannesID.NewValue)then FChangeProp := True
  else FChangeProp := False;
  if Trim(taCleannesNAME.AsString) = '' then raise EWarning.Create(rsNotSelFieldName);
  if Trim(taCleannesID.AsString) = '' then raise EWarning.Create(rsNotSelFieldId);
end;

procedure Tdm.taChromaticityBeforePost(DataSet: TDataSet);
begin
  if(taChromaticityID.OldValue <> taChromaticityID.NewValue)then FChangeProp := True
  else FChangeProp := False;
  if Trim(taChromaticityNAME.AsString) = '' then raise EWarning.Create(rsNotSelFieldName);
  if Trim(taChromaticityID.AsString) = '' then raise EWarning.Create(rsNotSelFieldId);
end;

procedure Tdm.taIGrBeforePost(DataSet: TDataSet);
begin
  if(taIGrID.OldValue <> taIGrID.NewValue)then FChangeProp := True
  else FChangeProp := False;
  if Trim(taIGrNAME.AsString) = '' then raise EWarning.Create(rsNotSelFieldName);
  if Trim(taIGrID.AsString) = '' then raise EWarning.Create(rsNotSelFieldId);
end;
// --------------------------------------

procedure Tdm.taColorPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('PK_D_COLOR', E.Message) <> 0)then
    raise EWarning.Create(Format(rsNotUniqueId, ['�����']));
  if(pos('UNQ1_D_COLOR', E.Message) <> 0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['�����']));
end;

procedure Tdm.taCleannesPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
  if(pos('INTEG_158', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueId, ['�������']));
  if(pos('UNQ1_D_CLEANNES', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['�������']));
end;

procedure Tdm.taChromaticityPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('INTEG_160', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueId, ['���������']));
  if(pos('UNQ1_D_CHROMATICITY', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['���������']));
end;

procedure Tdm.taIGrPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('PK_D_IGR', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueId, ['������']));
  if(pos('UNQ1_D_IGR', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['������']));
end;

procedure Tdm.taEdgShapePostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('PK_D_EDGSHAPE', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueId, ['����� �������']));
  if(pos('UNQ1_D_EDGSHAPE', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['����� �������']));
end;

procedure Tdm.taEdgTPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('PK_D_EDGT', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueId, ['���� �������']));
  if(pos('UNQ1_D_EDGT', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueName, ['���� �������']));
end;

procedure Tdm.taContractBeforePost(DataSet: TDataSet);
begin
  if Trim(taContractNAME.AsString) = '' then
    raise EWarning.Create(rsNotSelFieldName);

end;

procedure Tdm.taPrillBeforePost(DataSet: TDataSet);
begin
  if Trim(taPrillPRILL.AsString) = '' then
  raise EWarning.Create('�� ��������� ���� �������� �����');
end;

procedure Tdm.taNDSBeforePost(DataSet: TDataSet);
begin
  if Trim(taNDSNAME.AsString) = '' then
  raise EWarning.Create(rsNotSelFieldName);
end;

procedure Tdm.taSzBeforePost(DataSet: TDataSet);
begin
  if Trim(taSzNAME.AsString) = '' then
  raise EWarning.Create(rsNotSelFieldName);
end;

procedure Tdm.taSzDiamondBeforePost(DataSet: TDataSet);
begin
  if Trim(taSzDiamondSZ.AsString) = '' then
  raise EWarning.Create(rsNotSelFieldName);
end;

procedure Tdm.taPaytypeBeforePost(DataSet: TDataSet);
begin
  if Trim(taPaytypeNAME.AsString) = '' then
  raise EWarning.Create(rsNotSelFieldName);
end;

procedure Tdm.taPaytypePostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('UNQ1_D_PAYTYPE', E.Message)<>0)then
    raise EWarning.Create(Format(rsNotUniqueId, ['���� ������']));
end;

procedure Tdm.taTechBeforePost(DataSet: TDataSet);
begin
  if Trim(taTechNAME.AsString) = '' then
  raise EWarning.Create(rsNotSelFieldName);
end;


// --------------------------------------

procedure Tdm.taRejBeforePost(DataSet: TDataSet);
begin
  if Trim(taRejNAME.AsString) = '' then
  raise EWarning.Create(rsNotSelFieldName);
end;

procedure Tdm.taRejPostError(DataSet: TDataSet; E: EDatabaseError; var Action: TDataAction);
begin
  if(pos('UNQ1_D_REJ', E.Message)<>0)then
  raise EWarning.Create(Format(rsNotUniqueName, ['������� �����']));
end;


initialization
  OnGetDefaultIniName := GetDefaultIniName;

end.


