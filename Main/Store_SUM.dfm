inherited fmStore_SUM: TfmStore_SUM
  Left = 304
  Top = 260
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = #1048#1090#1086#1075#1086' '#1087#1086' '#1089#1082#1083#1072#1076#1091
  ClientHeight = 313
  ClientWidth = 475
  OldCreateOrder = True
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 294
    Width = 475
  end
  inherited tb1: TSpeedBar
    Width = 475
    Locked = True
    object Label1: TLabel [0]
      Left = 7
      Top = 5
      Width = 31
      Height = 13
      Caption = #1057#1082#1083#1072#1076
      Transparent = True
    end
    object Label2: TLabel [1]
      Left = 6
      Top = 23
      Width = 39
      Height = 13
      Caption = 'Label2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    inherited spitExit: TSpeedItem
      Left = 403
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 46
    Width = 233
    Height = 123
    Caption = #1053#1072' '#1089#1082#1083#1072#1076#1077
    Color = 16711119
    ParentColor = False
    TabOrder = 2
    object Label3: TLabel
      Left = 8
      Top = 26
      Width = 83
      Height = 13
      Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 46
      Width = 98
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086' '#1080#1079#1076#1077#1083#1080#1081':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 67
      Width = 103
      Height = 13
      Caption = #1057#1091#1084#1084#1072#1088#1085#1099#1081' '#1074#1077#1089': '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 8
      Top = 88
      Width = 69
      Height = 13
      Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label7: TLabel
      Left = 200
      Top = 24
      Width = 25
      Height = 13
      Caption = #1072#1088#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label8: TLabel
      Left = 200
      Top = 45
      Width = 20
      Height = 13
      Caption = #1096#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label9: TLabel
      Left = 200
      Top = 67
      Width = 11
      Height = 13
      Caption = #1075'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label10: TLabel
      Left = 200
      Top = 89
      Width = 12
      Height = 13
      Caption = #1088'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbAssort1: TLabel
      Left = 115
      Top = 26
      Width = 80
      Height = 15
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'lbAssort1'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lbQ1: TLabel
      Left = 115
      Top = 46
      Width = 80
      Height = 15
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Label11'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lbW1: TLabel
      Left = 115
      Top = 67
      Width = 80
      Height = 15
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Label11'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lbSUM1: TLabel
      Left = 115
      Top = 88
      Width = 80
      Height = 15
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Label11'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 0
    Top = 172
    Width = 233
    Height = 123
    Caption = #1055#1088#1086#1076#1072#1085#1086
    Color = 13106428
    ParentColor = False
    TabOrder = 3
    object Label11: TLabel
      Left = 8
      Top = 26
      Width = 83
      Height = 13
      Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 8
      Top = 46
      Width = 98
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086' '#1080#1079#1076#1077#1083#1080#1081':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 8
      Top = 67
      Width = 103
      Height = 13
      Caption = #1057#1091#1084#1084#1072#1088#1085#1099#1081' '#1074#1077#1089': '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label14: TLabel
      Left = 8
      Top = 88
      Width = 69
      Height = 13
      Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 200
      Top = 24
      Width = 25
      Height = 13
      Caption = #1072#1088#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label16: TLabel
      Left = 200
      Top = 45
      Width = 20
      Height = 13
      Caption = #1096#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label17: TLabel
      Left = 200
      Top = 67
      Width = 11
      Height = 13
      Caption = #1075'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label18: TLabel
      Left = 200
      Top = 89
      Width = 12
      Height = 13
      Caption = #1088'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbAssort2: TLabel
      Left = 140
      Top = 27
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'lbAssort1'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbQ2: TLabel
      Left = 147
      Top = 47
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Label11'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbW2: TLabel
      Left = 147
      Top = 68
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Label11'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbSUM2: TLabel
      Left = 147
      Top = 89
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Label11'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GroupBox3: TGroupBox [4]
    Left = 240
    Top = 46
    Width = 233
    Height = 123
    Caption = #1042#1086#1079#1074#1088#1072#1090#1099
    Color = 14218734
    ParentColor = False
    TabOrder = 4
    object Label19: TLabel
      Left = 8
      Top = 26
      Width = 83
      Height = 13
      Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label20: TLabel
      Left = 8
      Top = 46
      Width = 98
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086' '#1080#1079#1076#1077#1083#1080#1081':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label21: TLabel
      Left = 8
      Top = 67
      Width = 103
      Height = 13
      Caption = #1057#1091#1084#1084#1072#1088#1085#1099#1081' '#1074#1077#1089': '
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label22: TLabel
      Left = 8
      Top = 88
      Width = 69
      Height = 13
      Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label23: TLabel
      Left = 200
      Top = 24
      Width = 25
      Height = 13
      Caption = #1072#1088#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label24: TLabel
      Left = 200
      Top = 45
      Width = 20
      Height = 13
      Caption = #1096#1090'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label25: TLabel
      Left = 200
      Top = 67
      Width = 11
      Height = 13
      Caption = #1075'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label26: TLabel
      Left = 200
      Top = 89
      Width = 12
      Height = 13
      Caption = #1088'.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbAssort3: TLabel
      Left = 113
      Top = 26
      Width = 80
      Height = 15
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'lbAssort1'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lbQ3: TLabel
      Left = 115
      Top = 46
      Width = 80
      Height = 15
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Label11'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lbW3: TLabel
      Left = 115
      Top = 67
      Width = 80
      Height = 15
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Label11'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
    object lbSum3: TLabel
      Left = 115
      Top = 88
      Width = 80
      Height = 15
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Label11'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Transparent = True
    end
  end
  inherited ilButtons: TImageList
    Left = 214
    Top = 4
  end
  inherited fmstr: TFormStorage
    Left = 520
    Top = 56
  end
end
