unit dMat;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, RxPlacemnt, ImgList, Grids, DBGrids, RXDBCtrl,
  ExtCtrls, ComCtrls, DB, ActnList, Buttons,
  DBGridEh, DBGridEhGrouping, rxSpeedbar, GridsEh;

type
  TfmMat = class(TfmDictAncestor)
    SpeedItem4: TSpeedItem;
    ActionNormLosses: TAction;
    function grGetCellCheckBox(Sender: TObject; Field: TField;
      var StateCheckBox: Integer): Boolean;
    procedure spitExitClick(Sender: TObject);
    procedure ActionNormLossesUpdate(Sender: TObject);
    procedure ActionNormLossesExecute(Sender: TObject);
  end;

var
  fmMat: TfmMat;

implementation

uses DictData,   frmNormLosses;

{$R *.dfm}

function TfmMat.grGetCellCheckBox(Sender: TObject; Field: TField;
  var StateCheckBox: Integer): Boolean;
begin
 Result := (Field.FieldName='GOOD')or
           (Field.FieldName='ISINS');
end;

procedure TfmMat.spitExitClick(Sender: TObject);
begin
  inherited;
  dm.LoadArtSL(MAT_DICT);
  dm.LoadArtSL(MATGOOD_DICT);
end;

procedure TfmMat.ActionNormLossesUpdate(Sender: TObject);
begin
  ActionNormLosses.Enabled := not (dm.taMat.IsEmpty or (dm.taMat.State <> dsBrowse));
end;

procedure TfmMat.ActionNormLossesExecute(Sender: TObject);
var
  MaterialID: Integer;
  CurrentValue: Double;
begin
  MaterialID := StrToInt(Trim(dm.taMatID.AsString));
  TDialogNormLosses.Execute(MaterialID);
end;

end.
