unit dSimpleDepart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList,  ImgList,  Grids,
  DBGridEh, Buttons, ExtCtrls, ComCtrls, Db, DBGridEhGrouping, rxPlacemnt,
  rxSpeedbar, GridsEh;

type
  TfmdSimpleDepart = class(TfmDictAncestor)
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FSaveActiveDataSet : boolean;
    FSaveOnFilterRecord : TFilterRecordEvent;
    procedure FilterDepart(DataSet : TDataSet; var Accept : boolean);
  end;

var
  fmdSimpleDepart: TfmdSimpleDepart;

implementation

uses DictData, dbUtil, fmUtils, ProductionConsts;

{$R *.dfm}

procedure TfmdSimpleDepart.FormCreate(Sender: TObject);
begin
  FSaveActiveDataSet := DataSet.Active;
  FSaveOnFilterRecord := DataSet.OnFilterRecord;
  CloseDataSet(DataSet);
  DataSet.Filtered := True;
  DataSet.OnFilterRecord := FilterDepart;
  inherited;
end;

procedure TfmdSimpleDepart.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk : if VarIsNull(DataSet.FieldByName(FParams.sId).Value)then raise EWarning.Create(Format(rsSelectPS, ['']));
  end;
  inherited;
  DataSet.Filtered := False;
  DataSet.OnFilterRecord := FSaveOnFilterRecord;
  DataSet.Active := FSaveActiveDataSet;
end;

procedure TfmdSimpleDepart.FilterDepart(DataSet: TDataSet; var Accept: boolean);
var
  Id : Variant;
begin
  Accept := (dm.taDepartDEPTYPES.AsInteger and $1 = $1) and
            (dm.taDepartDEPTYPES.AsInteger and $10 <> $10);
  if Accept then
  begin
    Id := ExecSelectSQL('select Id from D_PsOper where OperId="'+dm.ReAssemblyId+'" and DepId='+dm.taDepartDEPID.AsString, dm.quTmp);
    if VarIsNull(Id) then Id := 0;
    Accept := Id<>0;
  end;
end;

end.
