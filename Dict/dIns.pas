unit dIns;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ImgList, Grids, DBGrids, RXDBCtrl,
  ExtCtrls, ComCtrls, ActnList, DBGridEh,
  Buttons, DBGridEhGrouping, rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmIns = class(TfmDictAncestor)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

implementation

uses DictData;

{$R *.dfm}

procedure TfmIns.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  dm.LoadArtSL(INS_DICT);
end;

end.
