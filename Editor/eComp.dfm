inherited fmeComp: TfmeComp
  Left = 207
  Top = 55
  Caption = #1056#1077#1082#1074#1080#1079#1080#1090#1099' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
  ClientHeight = 613
  ClientWidth = 464
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 464
    Height = 613
    ExplicitWidth = 464
    ExplicitHeight = 613
    inherited btnOk: TBitBtn
      Left = 280
      Top = 580
      ExplicitLeft = 280
      ExplicitTop = 580
    end
    inherited btnCancel: TBitBtn
      Left = 372
      Top = 580
      ExplicitLeft = 372
      ExplicitTop = 580
    end
    object Panel2: TPanel
      Left = 2
      Top = 2
      Width = 460
      Height = 293
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Label1: TLabel
        Left = 4
        Top = 64
        Width = 63
        Height = 13
        Caption = #1070#1088#1080#1076'. '#1072#1076#1088#1077#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label2: TLabel
        Left = 4
        Top = 123
        Width = 45
        Height = 13
        Caption = #1058#1077#1083#1077#1092#1086#1085
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 176
        Top = 123
        Width = 29
        Height = 13
        Caption = #1060#1072#1082#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 296
        Top = 123
        Width = 29
        Height = 13
        Caption = 'E-Mail'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label23: TLabel
        Left = 212
        Top = 44
        Width = 89
        Height = 13
        Caption = #1055#1086#1095#1090#1086#1074#1099#1081' '#1080#1085#1076#1077#1082#1089
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label25: TLabel
        Left = 4
        Top = 44
        Width = 36
        Height = 13
        Caption = #1057#1090#1088#1072#1085#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label19: TLabel
        Left = 4
        Top = 23
        Width = 76
        Height = 13
        Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
      end
      object Label7: TLabel
        Left = 5
        Top = 143
        Width = 24
        Height = 13
        Caption = #1048#1053#1053
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label18: TLabel
        Left = 160
        Top = 177
        Width = 148
        Height = 13
        Caption = #1060#1048#1054'/'#1090#1077#1083#1077#1092#1086#1085' '#1088#1091#1082#1086#1074#1086#1076#1080#1090#1077#1083#1103
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label21: TLabel
        Left = 148
        Top = 209
        Width = 183
        Height = 13
        Caption = #1060#1048#1054'/'#1090#1077#1083#1077#1092#1086#1085' '#1075#1083#1072#1074#1085#1086#1075#1086' '#1073#1091#1093#1075#1072#1083#1090#1077#1088#1072
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 176
        Top = 143
        Width = 31
        Height = 13
        Caption = #1054#1050#1055#1054
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 296
        Top = 143
        Width = 38
        Height = 13
        Caption = #1054#1050#1042#1069#1044
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label24: TLabel
        Left = 5
        Top = 162
        Width = 23
        Height = 13
        Caption = #1050#1055#1055
      end
      object Label37: TLabel
        Left = 168
        Top = 243
        Width = 136
        Height = 13
        Caption = #1060#1048#1054'/'#1090#1077#1083#1077#1092#1086#1085' '#1084#1077#1085#1077#1076#1078#1077#1088#1072
      end
      object Label38: TLabel
        Left = 4
        Top = 84
        Width = 64
        Height = 13
        Caption = #1060#1072#1082#1090'. '#1072#1076#1088#1077#1089
      end
      object Label39: TLabel
        Left = 4
        Top = 104
        Width = 60
        Height = 13
        Caption = #1055#1086#1095#1090'. '#1072#1076#1088#1077#1089
      end
      object edCountry: TDBEditEh
        Left = 84
        Top = 40
        Width = 121
        Height = 19
        DataField = 'COUNTRY'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 0
        Visible = True
      end
      object edPostIndex: TDBEditEh
        Left = 308
        Top = 40
        Width = 121
        Height = 19
        DataField = 'POSTINDEX'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 1
        Visible = True
      end
      object edAddress: TDBEditEh
        Left = 84
        Top = 60
        Width = 345
        Height = 19
        DataField = 'ADDRESS'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 2
        Visible = True
      end
      object edPhone: TDBEditEh
        Left = 84
        Top = 119
        Width = 89
        Height = 19
        DataField = 'PHONE'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 3
        Visible = True
      end
      object edFax: TDBEditEh
        Left = 212
        Top = 119
        Width = 77
        Height = 19
        DataField = 'FAX'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 4
        Visible = True
      end
      object edEMail: TDBEditEh
        Left = 328
        Top = 119
        Width = 101
        Height = 19
        DataField = 'EMAIL'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 5
        Visible = True
      end
      object cbSeller: TDBCheckBoxEh
        Left = 8
        Top = 276
        Width = 81
        Height = 17
        Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
        DataField = 'SELLER'
        DataSource = dm.dsrComp
        Flat = True
        TabOrder = 6
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object cbBuyer: TDBCheckBoxEh
        Left = 92
        Top = 276
        Width = 85
        Height = 17
        Caption = #1055#1086#1082#1091#1087#1072#1090#1077#1083#1100
        DataField = 'BUYER'
        DataSource = dm.dsrComp
        Flat = True
        TabOrder = 7
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object chbxLawFace1: TDBCheckBoxEh
        Left = 124
        Top = 1
        Width = 113
        Height = 17
        Caption = #1070#1088#1080#1076#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
        DataField = 'LAWFACE'
        DataSource = dm.dsrComp
        Flat = True
        TabOrder = 8
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object chbxPhysFace: TDBCheckBoxEh
        Left = 4
        Top = 1
        Width = 113
        Height = 17
        Caption = #1060#1080#1079#1080#1095#1077#1089#1082#1086#1077' '#1083#1080#1094#1086
        DataField = 'PHISFACE'
        DataSource = dm.dsrComp
        Flat = True
        TabOrder = 9
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object edName: TDBEditEh
        Left = 84
        Top = 20
        Width = 345
        Height = 19
        DataField = 'NAME'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 10
        Visible = True
      end
      object edINN: TDBEditEh
        Left = 84
        Top = 139
        Width = 89
        Height = 19
        DataField = 'INN'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 11
        Visible = True
      end
      object edBoss: TDBEditEh
        Left = 4
        Top = 190
        Width = 336
        Height = 19
        DataField = 'BOSSFIO'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 12
        Visible = True
      end
      object edBossPhone: TDBEditEh
        Left = 340
        Top = 190
        Width = 90
        Height = 19
        DataField = 'BOSSPHONE'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 13
        Visible = True
      end
      object edBuh: TDBEditEh
        Left = 4
        Top = 223
        Width = 335
        Height = 19
        DataField = 'BUHFIO'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 14
        Visible = True
      end
      object edBuhPhone: TDBEditEh
        Left = 340
        Top = 223
        Width = 89
        Height = 19
        DataField = 'BUHPHONE'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 15
        Visible = True
      end
      object edOKPO: TDBEditEh
        Left = 212
        Top = 139
        Width = 77
        Height = 19
        DataField = 'OKPO'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 16
        Visible = True
      end
      object edOKONH: TDBEditEh
        Left = 336
        Top = 139
        Width = 93
        Height = 19
        DataField = 'OKONH'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 17
        Visible = True
      end
      object edKPP: TDBEditEh
        Left = 84
        Top = 159
        Width = 89
        Height = 19
        DataField = 'KPP'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 18
        Visible = True
      end
      object edOFIO: TDBEditEh
        Left = 4
        Top = 255
        Width = 335
        Height = 19
        DataField = 'OFIO'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 19
        Visible = True
      end
      object edOFIOPhone: TDBEditEh
        Left = 340
        Top = 255
        Width = 89
        Height = 19
        DataField = 'OFIOPHONE'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 20
        Visible = True
      end
      object edFactAddress: TDBEditEh
        Left = 84
        Top = 79
        Width = 345
        Height = 19
        DataField = 'FACTADDRESS'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 21
        Visible = True
      end
      object edPostAddress: TDBEditEh
        Left = 84
        Top = 98
        Width = 345
        Height = 19
        DataField = 'POSTADDRESS'
        DataSource = dm.dsrComp
        EditButtons = <>
        Flat = True
        TabOrder = 22
        Visible = True
      end
    end
    object pgctl: TPageControl
      Left = 2
      Top = 295
      Width = 460
      Height = 278
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 3
      object tbshProps: TTabSheet
        Caption = #1056#1077#1082#1074#1080#1079#1080#1090#1099
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label10: TLabel
          Left = 4
          Top = 8
          Width = 22
          Height = 13
          Caption = #1041#1048#1050
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label13: TLabel
          Left = 4
          Top = 28
          Width = 25
          Height = 13
          Caption = #1041#1072#1085#1082
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label14: TLabel
          Left = 4
          Top = 48
          Width = 23
          Height = 13
          Caption = #1057#1095#1077#1090
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label15: TLabel
          Left = 4
          Top = 88
          Width = 46
          Height = 13
          Caption = #1050#1086#1088'.'#1073#1072#1085#1082
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label16: TLabel
          Left = 4
          Top = 68
          Width = 44
          Height = 13
          Caption = #1050#1086#1088'.'#1089#1095#1077#1090
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label6: TLabel
          Left = 4
          Top = 112
          Width = 43
          Height = 13
          Caption = #1055#1072#1089#1087#1086#1088#1090
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 4
          Top = 132
          Width = 56
          Height = 13
          Caption = #1050#1077#1084' '#1074#1099#1076#1072#1085
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 4
          Top = 152
          Width = 66
          Height = 13
          Caption = #1044#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label30: TLabel
          Left = 4
          Top = 172
          Width = 104
          Height = 13
          Caption = #1057#1074#1080#1076#1077#1090#1077#1083#1100#1089#1090#1074#1086' '#1048#1053#1053
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 100
          Top = 111
          Width = 30
          Height = 13
          Caption = #1089#1077#1088#1080#1103
        end
        object Label17: TLabel
          Left = 216
          Top = 110
          Width = 32
          Height = 13
          Caption = #1085#1086#1084#1077#1088
        end
        object Label20: TLabel
          Left = 128
          Top = 172
          Width = 32
          Height = 13
          Caption = #1085#1086#1084#1077#1088
        end
        object Label22: TLabel
          Left = 242
          Top = 172
          Width = 63
          Height = 13
          Caption = #1076#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
        end
        object Label31: TLabel
          Left = 4
          Top = 188
          Width = 115
          Height = 26
          Caption = #1057#1074#1080#1076#1077#1090#1077#1083#1100#1089#1090#1074#1086' '#1086' '#13#10#1074#1085#1077#1089#1077#1085#1080#1077' '#1074' '#1075#1086#1089'.'#1088#1077#1077#1089#1090#1088
        end
        object Label32: TLabel
          Left = 128
          Top = 196
          Width = 32
          Height = 13
          Caption = #1085#1086#1084#1077#1088
        end
        object Label33: TLabel
          Left = 242
          Top = 196
          Width = 63
          Height = 13
          Caption = #1076#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
        end
        object Label34: TLabel
          Left = 4
          Top = 220
          Width = 170
          Height = 26
          Caption = #1057#1074#1080#1076#1077#1090#1077#1083#1100#1089#1090#1074#1086' '#1086' '#1087#1086#1089#1090#1072#1085#1086#1074#1082#1077' '#1063#1055' '#13#10#1085#1072' '#1091#1095#1077#1090' '#1087#1086' '#1084#1077#1089#1090#1091' '#1078#1080#1090#1077#1083#1100#1089#1090#1074#1072
        end
        object Label35: TLabel
          Left = 184
          Top = 224
          Width = 32
          Height = 13
          Caption = #1085#1086#1084#1077#1088
        end
        object Label36: TLabel
          Left = 298
          Top = 224
          Width = 63
          Height = 13
          Caption = #1076#1072#1090#1072' '#1074#1099#1076#1072#1095#1080
        end
        object edBIK: TDBEditEh
          Left = 60
          Top = 4
          Width = 89
          Height = 19
          DataField = 'BIK'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 0
          Visible = True
        end
        object edBank: TDBEditEh
          Left = 60
          Top = 24
          Width = 297
          Height = 19
          DataField = 'BANK'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 1
          Visible = True
        end
        object edBill: TDBEditEh
          Left = 60
          Top = 44
          Width = 297
          Height = 19
          DataField = 'BILL'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 2
          Visible = True
        end
        object edKBank: TDBEditEh
          Left = 60
          Top = 84
          Width = 297
          Height = 19
          DataField = 'KBANK'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 3
          Visible = True
        end
        object edKBill: TDBEditEh
          Left = 60
          Top = 64
          Width = 297
          Height = 19
          DataField = 'KBILL'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 4
          Visible = True
        end
        object edPaspSer: TDBEditEh
          Left = 144
          Top = 108
          Width = 61
          Height = 19
          DataField = 'PASPSER'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 5
          Visible = True
        end
        object edPaspNum: TDBEditEh
          Left = 252
          Top = 108
          Width = 104
          Height = 19
          DataField = 'PASPNUM'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 6
          Visible = True
        end
        object edDistrPlace: TDBEditEh
          Left = 100
          Top = 128
          Width = 257
          Height = 19
          DataField = 'DISTRPLACE'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 7
          Visible = True
        end
        object deDistrDate: TDBDateTimeEditEh
          Left = 100
          Top = 148
          Width = 79
          Height = 19
          DataField = 'DISTRDATE'
          DataSource = dm.dsrComp
          EditButton.ShortCut = 16397
          EditButton.Style = ebsGlyphEh
          EditButtons = <>
          Flat = True
          Kind = dtkDateEh
          TabOrder = 8
          Visible = True
        end
        object edCertN: TDBEditEh
          Left = 164
          Top = 168
          Width = 75
          Height = 19
          DataField = 'CERTN'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 9
          Visible = True
        end
        object dtCertDate: TDBDateTimeEditEh
          Left = 308
          Top = 168
          Width = 79
          Height = 19
          DataField = 'CERTDATE'
          DataSource = dm.dsrComp
          EditButton.ShortCut = 16397
          EditButton.Style = ebsGlyphEh
          EditButtons = <>
          Flat = True
          Kind = dtkDateEh
          TabOrder = 10
          Visible = True
        end
        object edNo_GovReg: TDBEditEh
          Left = 164
          Top = 192
          Width = 75
          Height = 19
          DataField = 'NO_GOVREG'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 11
          Visible = True
        end
        object edD_GovReg: TDBDateTimeEditEh
          Left = 308
          Top = 192
          Width = 79
          Height = 19
          DataField = 'D_GOVREG'
          DataSource = dm.dsrComp
          EditButton.ShortCut = 16397
          EditButton.Style = ebsGlyphEh
          EditButtons = <>
          Flat = True
          Kind = dtkDateEh
          TabOrder = 12
          Visible = True
        end
        object edNo_FisLPlace: TDBEditEh
          Left = 220
          Top = 220
          Width = 75
          Height = 19
          DataField = 'NO_FISLPLACE'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 13
          Visible = True
        end
        object edD_FisLPlace: TDBDateTimeEditEh
          Left = 364
          Top = 220
          Width = 79
          Height = 19
          DataField = 'D_FISLPLACE'
          DataSource = dm.dsrComp
          EditButton.ShortCut = 16397
          EditButton.Style = ebsGlyphEh
          EditButtons = <>
          Flat = True
          Kind = dtkDateEh
          TabOrder = 14
          Visible = True
        end
      end
      object TabSheet2: TTabSheet
        Caption = #1044#1086#1075#1086#1074#1086#1088#1072
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object gridCompOwner: TDBGridEh
          Left = 0
          Top = 26
          Width = 452
          Height = 224
          Align = alClient
          AllowedOperations = [alopUpdateEh, alopAppendEh]
          ColumnDefValues.Title.TitleButton = True
          DataGrouping.GroupLevels = <>
          DataSource = dm.dsrCompOwner
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
          RowDetailPanel.Color = clBtnFace
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          UseMultiTitle = True
          Columns = <
            item
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'SELFCOMPNAME'
              Footers = <>
              Width = 151
            end
            item
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'NO_CONTRACT'
              Footers = <>
              Title.Caption = #1044#1086#1075#1086#1074#1086#1088'|'#8470
              Title.EndEllipsis = True
              Width = 61
            end
            item
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'BD_CONTRACT'
              Footers = <>
              Title.Caption = #1044#1086#1075#1086#1074#1086#1088'|'#1053#1072#1095#1072#1083#1086
              Title.EndEllipsis = True
              Width = 81
            end
            item
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'ED_CONTRACT'
              Footers = <>
              Title.Caption = #1044#1086#1075#1086#1074#1086#1088'|'#1054#1082#1086#1085#1095#1072#1085#1080#1077
              Title.EndEllipsis = True
              Width = 71
            end
            item
              DropDownBox.ColumnDefValues.Title.TitleButton = True
              EditButtons = <>
              FieldName = 'REUDATE'
              Footers = <>
              Title.Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1077' '#1056#1069#1059
              Title.EndEllipsis = True
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
        object TBDock1: TTBDock
          Left = 0
          Top = 0
          Width = 452
          Height = 26
          object tlbrCompOwner: TTBToolbar
            Left = 0
            Top = 0
            BorderStyle = bsNone
            DockMode = dmCannotFloat
            Images = ilButtons
            TabOrder = 0
            object TBItem2: TTBItem
              Action = acAddCompOwner
              DisplayMode = nbdmImageAndText
            end
            object TBItem1: TTBItem
              Action = acDelCompOwner
              DisplayMode = nbdmImageAndText
            end
          end
        end
      end
      object tbshDOG: TTabSheet
        Caption = #1044#1086#1075#1086#1074#1086#1088
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label26: TLabel
          Left = 4
          Top = 8
          Width = 84
          Height = 13
          Caption = #1053#1086#1084#1077#1088' '#1076#1086#1075#1086#1074#1086#1088#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label27: TLabel
          Left = 4
          Top = 28
          Width = 114
          Height = 13
          Caption = #1044#1072#1090#1072' '#1085#1072#1095#1072#1083#1072' '#1076#1086#1075#1086#1074#1086#1088#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label28: TLabel
          Left = 4
          Top = 48
          Width = 132
          Height = 13
          Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103' '#1076#1086#1075#1086#1074#1086#1088#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label29: TLabel
          Left = 4
          Top = 68
          Width = 107
          Height = 13
          Caption = #1044#1072#1090#1072' '#1086#1082#1086#1085#1095#1072#1085#1080#1103' '#1056#1069#1059
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object edDogNo: TDBEditEh
          Left = 144
          Top = 4
          Width = 94
          Height = 19
          DataField = 'NDOG'
          DataSource = dm.dsrComp
          EditButtons = <>
          Flat = True
          TabOrder = 0
          Visible = True
        end
        object dtedDogBd: TDBDateTimeEditEh
          Left = 144
          Top = 24
          Width = 94
          Height = 19
          DataField = 'DOGBD'
          DataSource = dm.dsrComp
          EditButton.ShortCut = 16397
          EditButton.Style = ebsGlyphEh
          EditButtons = <>
          Flat = True
          Kind = dtkDateEh
          TabOrder = 1
          Visible = True
        end
        object dtedDogED: TDBDateTimeEditEh
          Left = 144
          Top = 44
          Width = 94
          Height = 19
          DataField = 'DOGED'
          DataSource = dm.dsrComp
          EditButton.ShortCut = 16397
          EditButton.Style = ebsGlyphEh
          EditButtons = <>
          Flat = True
          Kind = dtkDateEh
          TabOrder = 2
          Visible = True
        end
        object dtedREUED: TDBDateTimeEditEh
          Left = 144
          Top = 64
          Width = 94
          Height = 19
          DataField = 'REUED'
          DataSource = dm.dsrComp
          EditButton.ShortCut = 16397
          EditButton.Style = ebsGlyphEh
          EditButtons = <>
          Flat = True
          Kind = dtkDateEh
          TabOrder = 3
          Visible = True
        end
      end
      object TabSheet1: TTabSheet
        Caption = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
        ImageIndex = 2
        object mmComment: TDBMemo
          Left = 0
          Top = 0
          Width = 452
          Height = 250
          Align = alClient
          DataField = 'COMMENT'
          DataSource = dm.dsrComp
          TabOrder = 0
        end
      end
    end
  end
  object acEvent: TActionList
    Images = ilButtons
    Left = 68
    Top = 428
    object acAdd: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
    end
    object acDel: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
    end
    object acSort: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
      Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 3
      ShortCut = 32851
    end
    object acExit: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
      Caption = #1042#1099#1093#1086#1076
      ImageIndex = 0
    end
    object acAddCompOwner: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 1
    end
    object acDelCompOwner: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 2
    end
    object acPrintCompList: TAction
      Category = #1057#1087#1088#1072#1074#1086#1095#1085#1080#1082
      Caption = #1055#1077#1088#1077#1095#1077#1085#1100' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081
    end
  end
  object ilButtons: TImageList
    Left = 118
    Top = 440
    Bitmap = {
      494C010105000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000002000000001002000000000000020
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B5B5B5000000000000000000000000009C9C9C009C9C
      9C00A5A5A5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B5B5B500B5B5B500DEDEDE00ADADAD00525252006B6B6B00ADADAD00D6D6
      D600E7DEDE009C9C9C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5B5B500B5B5
      B500FFFFFF00FFFFFF00E7DEDE00B5B5B5005A5A630031313100313131004A4A
      4A00848484009C9C9C00A5A5A500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A5A5A500B5B5B500F7F7F700FFFF
      FF00F7F7F700D6D6D600B5B5B500A5A5A500ADADAD009C9C9C007B7B7B005252
      520031313900313131008C8C8C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000B5ADAD00EFEFEF00EFEFEF00CECE
      CE00BDB5B500C6C6C600D6CECE00B5B5B500ADADAD00ADA5A500ADADAD00ADAD
      AD00A5A5A500848484009C9C9C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ADA5AD00BDBDBD00B5B5B500C6C6
      C600D6D6D600DEDEDE00F7F7F700F7F7F700E7E7E700D6D6D600BDBDBD00ADAD
      AD00ADA5A500ADADAD00A5A5A500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A5A5A500C6BDC600D6D6D600DEDE
      DE00D6D6D600EFEFEF00DEDEDE00B5BDB500CECECE00D6DEDE00DEDEDE00DEDE
      DE00D6D6D600C6C6C600ADADAD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B5ADAD00DEDEDE00DEDE
      DE00E7E7E700D6D6D600C6C6C600BDDEBD00CED6CE00D6BDB500BDB5B500BDBD
      BD00C6C6C600D6CECE00BDBDBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5ADAD00CECE
      CE00C6BDBD00C6C6C600EFEFEF00FFF7F700F7F7F700F7EFE700E7E7E700DEDE
      DE00C6C6C600ADA5A50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B5AD
      AD00E7E7E700D6D6D600B5B5B500CECECE00DEDEE700DEE7E700DEDEDE00D6D6
      D600C6BDBD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFEFEF00FFE7DE00EFD6CE00EFD6CE00EFDED600E7DEDE00DEDEDE00ADAD
      AD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEB5B500FFE7DE00FFDECE00FFD6C600FFCEB500FFC6AD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEB5
      B500FFE7DE00FFE7DE00FFDECE00FFD6C600FFCEB500FFBDAD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEB5
      B500DEB5B500DEB5B500DEB5B500F7BDB500F7BDB50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C6363006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000073000000730000006B000000630000006300000063000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000073000000730000006B000000630000006300000063000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009C6363009C636300BD636300BD6B6B006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      B5000000B5000008AD003939B5005A5AB5005252AD002929940000006B000000
      6B0000006B000000000000000000000000000000000000000000000000000000
      B5000000B5000008AD003939B5005A5AB5005252AD002929940000006B000000
      6B0000006B0000000000000000000000000000000000000000001863AD001863
      AD001863AD000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C6363009C63
      6300C66B6B00D66B6B00D66B6B00C66B6B006B3131009C6363009C6363009C63
      63009C6363009C6363009C6363000000000000000000000000000010CE000810
      C6006B73DE00CED6F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6DE005A5A
      9C0000006B0000007B00000000000000000000000000000000000010CE000810
      C6006B73DE00CED6F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6DE005A5A
      9C0000006B0000007B00000000000000000000000000000000001863AD00319C
      FF001863AD000000000000000000000000001084210010842100108421001084
      21001084210010842100108421001084210000000000000000009C636300DE73
      7300D6737300D66B7300D66B6B00C66B6B006B313100FFA5A500FFADB500FFBD
      BD00FFC6C600FFC6C6009C63630000000000000000000018DE000818D600ADB5
      EF00FFFFFF00FFFFFF00BDBDEF008C8CD6008C8CD600CECEEF00FFFFFF00FFFF
      FF008C8CBD0000006B000000730000000000000000000018DE000818D600ADB5
      EF00FFFFFF00FFFFFF00BDBDEF008C8CD6008C8CD600CECEEF00FFFFFF00FFFF
      FF008C8CBD0000006B00000073000000000000000000000000001863AD00319C
      FF001863AD000000000000000000000000001084210010842100108421001084
      21001084210010842100108421001084210000000000000000009C636300E773
      7B00DE737300DE737300DE737300CE6B73006B31310039C6630021CE630029CE
      630018CE5A00FFC6C6009C63630000000000000000000018DE00949CEF00FFFF
      FF00E7EFFF004A52CE000000AD0000009C0000009400000094005A5ABD00F7F7
      FF00FFFFFF006363A5000000730000000000000000000018DE00949CEF00FFFF
      FF00E7EFFF004A52CE000000AD001018BD001818B500000094005A5ABD00F7F7
      FF00FFFFFF006363A500000073000000000000000000000000001863AD00319C
      FF001863AD000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300E77B
      7B00E77B7B00DE7B7B00DE737B00D67373006B31310042C66B0031CE630031CE
      630021CE6300FFC6C6009C636300000000000018F7002142E700FFFFFF00F7FF
      FF00394ADE000000C6001018BD00B5B5E700A5A5E7000808A50000008C005252
      BD00FFFFFF00DEDEEF0008087B0000007B000018F7002142E700FFFFFF00F7FF
      FF00394ADE000000C6001018BD001018B5000808A50008089C0000008C005252
      BD00FFFFFF00DEDEEF0008087B0000007B0000000000000000001863AD00319C
      FF001863AD000000000000000000000000001084210010842100108421001084
      21001084210010842100000000000000000000000000000000009C636300EF84
      8400E77B8400E77B7B00E7848400D67373006B31310039C6630029CE630029CE
      630021CE5A00FFC6C6009C636300000000000018F7007384F700FFFFFF0094A5
      F7000008DE000008D6001018CE00F7F7FF00DEE7F7000008AD0000009C000000
      9400ADADDE00FFFFFF005252AD0000007B000018F7007384F700FFFFFF0094A5
      F7000008DE000008D6001018CE001818AD000808A5000008A50000009C000000
      9400ADADDE00FFFFFF005252AD0000007B0000000000000000001863AD00319C
      FF001863AD000000000000000000000000001084210010842100108421001084
      21001084210010842100000000000000000000000000000000009C636300F784
      8C00EF848400EF949400FFDEDE00DE8C8C006B313100BDE7AD006BDE8C005AD6
      840042D67300FFC6C6009C636300000000000021FF00ADBDFF00FFFFFF004263
      F7001031EF00A5ADF700BDBDF700FFFFFF00F7F7FF00B5BDEF00949CDE000808
      A5005A5ABD00FFFFFF008484D60000007B000021FF00ADBDFF00FFFFFF004263
      F7001031EF00FFFFFF00FFFFFF00FFFFFF00F7F7FF00F7F7FF00F7F7FF000808
      A5005A5ABD00FFFFFF008484D60000007B0000000000000000001863AD00399C
      FF001863AD000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300F78C
      8C00EF848400F79C9C00FFDEDE00DE8C8C006B313100FFF7DE00FFFFE700FFFF
      DE00EFFFD600FFC6C6009C636300000000000839FF00C6CEFF00FFFFFF00315A
      FF00214AFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFEFFF001018
      B5004A4AC600FFFFFF009494DE000000A5000839FF00C6CEFF00FFFFFF00315A
      FF00214AFF00C6CEFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFEFFF001018
      B5004A4AC600FFFFFF009494DE000000A50000000000000000001863AD0042A5
      FF001863AD000000000000000000000000001084210010842100108421001084
      21000000000000000000000000000000000000000000000000009C636300FF94
      9400F78C8C00F78C8C00F78C8C00DE7B84006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFE700FFC6C6009C636300000000003963FF00C6D6FF00FFFFFF005A7B
      FF000029FF00315AFF005A6BF700F7F7FF00E7E7FF004A5ADE003142D6000000
      B500737BD600FFFFFF007B7BD6000000AD003963FF00C6D6FF00FFFFFF005A7B
      FF000029FF001039FF001029EF000821E7000018DE000810D6000000B5000000
      B500737BD600FFFFFF007B7BD6000000AD0000000000000000001863AD0052B5
      FF001863AD000000000000000000000000001084210010842100108421001084
      21000000000000000000000000000000000000000000000000009C636300FF94
      9C00FF949400FF949400FF949400E78484006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C636300000000001039FF00B5C6FF00FFFFFF00C6D6
      FF000029FF000021FF001039FF00F7F7FF00E7E7FF000018DE000000CE000818
      CE00DEDEF700FFFFFF003139CE000000AD001039FF00B5C6FF00FFFFFF00C6D6
      FF000029FF000021FF000829FF001031EF000018E7000010DE000810D6000818
      CE00DEDEF700FFFFFF003139CE000000AD0000000000000000001863AD0063C6
      FF001863AD000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C636300FF9C
      9C00FF949C00FF949400FF949C00E78C8C006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C63630000000000000000008CA5FF00FFFFFF00FFFF
      FF008CA5FF000021FF000029FF00426BFF004263FF000018EF000018DE00A5AD
      F700FFFFFF00B5BDEF000000BD0000000000000000008CA5FF00FFFFFF00FFFF
      FF008CA5FF000021FF000029FF000021FF001839FF000018EF000018DE00A5AD
      F700FFFFFF00B5BDEF000000BD00000000001863AD001863AD001863AD007BCE
      FF001863AD001863AD001863AD00000000001084210010842100000000000000
      00000000000000000000000000000000000000000000000000009C636300FF9C
      A500FF9C9C00FF9C9C00FF9C9C00E78C8C006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C6363000000000000000000738CFF00DEE7FF00FFFF
      FF00FFFFFF00B5C6FF00395AFF001039FF001039FF004A63FF00C6CEFF00FFFF
      FF00EFEFFF003142D6000000BD000000000000000000738CFF00DEE7FF00FFFF
      FF00FFFFFF00B5C6FF00395AFF001039FF001039FF004A63FF00C6CEFF00FFFF
      FF00EFEFFF003142D6000000BD0000000000000000001863AD0094E7FF008CDE
      FF007BD6FF001863AD0000000000000000001084210010842100000000000000
      00000000000000000000000000000000000000000000000000009C6363009C63
      6300EF8C8C00FF9C9C00FF9C9C00EF8C94006B313100FFF7D600FFFFDE00FFFF
      DE00FFFFDE00FFC6C6009C6363000000000000000000000000008CA5FF00E7EF
      FF00FFFFFF00FFFFFF00FFFFFF00E7EFFF00E7EFFF00FFFFFF00FFFFFF00DEE7
      FF003952E7000008CE00000000000000000000000000000000008CA5FF00E7EF
      FF00FFFFFF00FFFFFF00FFFFFF00E7EFFF00E7EFFF00FFFFFF00FFFFFF00DEE7
      FF003952E7000008CE00000000000000000000000000000000001863AD009CE7
      FF001863AD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009C636300B5737300D6848400DE8C8C006B3131009C6363009C6363009C63
      63009C6363009C6363009C636300000000000000000000000000000000008CA5
      FF00BDC6FF00EFEFFF00FFFFFF00FFFFFF00FFFFFF00E7EFFF008C9CF7001039
      E7000008CE000000000000000000000000000000000000000000000000008CA5
      FF00BDC6FF00EFEFFF00FFFFFF00FFFFFF00FFFFFF00E7EFFF008C9CF7001039
      E7000008CE000000000000000000000000000000000000000000000000001863
      AD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C6363009C6363006B31310000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008CA5FF008CA5FF0094A5FF007B94FF004A6BFF001842EF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008CA5FF008CA5FF0094A5FF007B94FF004A6BFF001842EF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000200000000100010000000000000100000000000000000000
      000000000000000000000000FFFFFF00FDC7000000000000F003000000000000
      C001000000000000000100000000000000010000000000000001000000000000
      00010000000000008001000000000000C003000000000000E007000000000000
      F00F000000000000F03F000000000000F03F000000000000F03F000000000000
      E03F000000000000E07F000000000000FE7FF81FF81FFFFFF07FE007E007C7FF
      C001C003C003C700C00180018001C700C00180018001C7FFC00100000000C703
      C00100000000C703C00100000000C7FFC00100000000C70FC00100000000C70F
      C00100000000C7FFC00180018001013FC00180018001833FC001C003C003C7FF
      F001E007E007EFFFFC7FF81FF81FFFFF00000000000000000000000000000000
      000000000000}
  end
end
