unit VerificationListTolling;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor,  TB2Item, ActnList, Menus,
  ImgList, Grids, DBGridEh, StdCtrls, ExtCtrls, ComCtrls, Mask,
  DBCtrlsEh, DBLookupEh, Db, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmVerificationListTolling = class(TfmListAncestor)
    cbComp: TDBComboBoxEh;
    laComp: TLabel;
    procedure cbCompChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure FilterListTolling(DataSet: TDataSet; var Accept: boolean);
  end;

var
  fmVerificationListTolling: TfmVerificationListTolling;

implementation

{$R *.dfm}
uses Data, MainData, dbUtil, DictData, dbTree, fmUtils, DocAncestor,
  FIBDatabase, pFIBDataSet, MsgDialog, DateUtils, PrintData, PrintData2,
  VerificationActTolling;

procedure TfmVerificationListTolling.acAddDocExecute(Sender: TObject);
var SupID: integer;
begin

   with dmData.taVerificationListTolling do
     begin

       Insert;

       if cbComp.ItemIndex > 0 then
         FieldByName('SupID').AsInteger := TNodeData(cbComp.Items.Objects[cbComp.ItemIndex]).Code
       else
         FieldByName('SupID').AsInteger := 0;
       Post;

       Close;

       Open;
       
     end;
end;

procedure TfmVerificationListTolling.acEditDocExecute(Sender: TObject);
begin
  if dmData.taVerificationListTolling.State in [dsEdit, dsInsert] then dmData.taVerificationListTolling.Post;
  TfmVerificationActTolling.Execute;
end;

procedure TfmVerificationListTolling.cbCompChange(Sender: TObject);
begin
  if(cbComp.ItemIndex <= 0)then dmMain.FilterCompId := IntToStr(ROOT_COMP_CODE)
  else with cbComp do
    dmMain.FilterCompId := IntToStr(TNodeData(Items.Objects[ItemIndex]).Code);
  DataSet.Filtered := False;
  DataSet.Filtered := True;
  inherited;
end;

procedure TfmVerificationListTolling.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if dmData.taVerificationListTolling.State in [dsEdit, dsInsert] then dmData.taVerificationListTolling.Post;
end;

procedure TfmVerificationListTolling.FormCreate(Sender: TObject);
begin
  cbComp.OnChange := nil;
  cbComp.Items.Clear;
  cbComp.Items.Assign(dm.dComp);
  cbComp.ItemIndex := 0;
  cbComp.OnChange := cbCompChange;
  dmMain.FilterCompId := IntToStr(ROOT_COMP_CODE);
  DataSet.OnFilterRecord := FilterListTolling;
  DataSet.Filtered := True;
  inherited;
end;




procedure TfmVerificationListTolling.FilterListTolling(DataSet: TDataSet; var Accept: boolean);
begin
  Accept := (dmMain.FilterCompId = IntToStr(ROOT_COMP_CODE)) or (dmMain.FilterCompId = DataSet.FieldByName('SUPID').AsString);
end;

end.
