unit SList;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ListAncestor, Menus, RxPlacemnt, ImgList, RxSpeedBar, StdCtrls, ExtCtrls,
  Grids, DBGrids, RXDBCtrl,  ComCtrls, db, fmUtils,
  Period, DBGridEh , DateUtils, ActnList, Mask, DBCtrlsEh,
  DBLookupEh, TB2Item, Provider, DBClient, TB2Dock, TB2Toolbar,
  Buttons, RXCtrls, DBF, StrUtils, DBGridEhGrouping, GridsEh, FIBDataSet,
  pFIBDataSet, QExport3Dialog, ShlObj;

type
  TfmSList = class(TfmListAncestor)
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    spitRePrice: TSpeedItem;
    lbComp: TLabel;
    cbComp: TDBComboBoxEh;
    acRetPrice: TAction;
    acRePrice: TAction;
    acImportInv: TAction;
    acExportInv: TAction;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    TBItem7: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    SpeedItem1: TSpeedItem;
    acUnselect: TAction;
    acStone: TAction;
    acMultiSelect: TAction;
    TBItem10: TTBItem;
    acSemis: TAction;
    TBItem11: TTBItem;
    acExportMC: TAction;
    TBItem12: TTBItem;
    ppAdd: TTBPopupMenu;
    TBItem13: TTBItem;
    TBItem14: TTBItem;
    acAddMC: TAction;
    ppCassa: TTBPopupMenu;
    acCassa: TActionList;
    acFRStatus: TAction;
    acFRZReport: TAction;
    acFROpenSell: TAction;
    acPrintFRSellCheck: TAction;
    acPrintFRRetSellCheck: TAction;
    TBItem15: TTBItem;
    TBItem16: TTBItem;
    TBItem17: TTBItem;
    TBSeparatorItem5: TTBSeparatorItem;
    TBItem18: TTBItem;
    TBItem19: TTBItem;
    btnCassa: TRxSpeedButton;
    ppPrint: TTBPopupMenu;
    TBItem20: TTBItem;
    TBItem21: TTBItem;
    acPrintChit: TAction;
    acPrintInvoice: TAction;
    acPrintFacture: TAction;
    TBItem22: TTBItem;
    TBSeparatorItem4: TTBSeparatorItem;
    TBItem23: TTBItem;
    acCalcActWorkW: TAction;
    tbsRetProd: TTBSubmenuItem;
    acLogic: TAction;
    SpeedButton1: TSpeedButton;
    TBItem8: TTBItem;
    acCreateRetBuyerInv: TAction;
    tbsSellInv: TTBSubmenuItem;
    TBItem9: TTBItem;
    acPrintInvUID: TAction;
    acEServParams: TAction;
    TBItem24: TTBItem;
    TBSubmenuItem1: TTBSubmenuItem;
    acPrintActWork: TAction;
    acPrintProdTolling: TAction;
    TBItem26: TTBItem;
    TBSubmenuItem2: TTBSubmenuItem;
    acPrintProdTolling1: TAction;
    acPrintProdTolling2: TAction;
    TBItem25: TTBItem;
    TBItem27: TTBItem;
    MX18Print: TTBItem;
    TBItem28: TTBItem;
    TBItem29: TTBItem;
    acAddEmptyClosed: TAction;
    TBItem30: TTBItem;
    acInvToActWork: TAction;
    DataSetExport: TpFIBDataSet;
    ExportDialog: TQExport3Dialog;
    DataSetExportARTIKUL: TFIBStringField;
    DataSetExportUNIKAL: TFIBIntegerField;
    DataSetExportSH_KOD: TFIBStringField;
    DataSetExportTIP: TFIBStringField;
    DataSetExportKOL: TFIBIntegerField;
    DataSetExportVES: TFIBFloatField;
    DataSetExportRAZMER: TFIBStringField;
    DataSetExportPROBA: TFIBStringField;
    DataSetExportCENA: TFIBFloatField;
    DataSetExportSUMMA: TFIBFloatField;
    DataSetExportNDS: TFIBFloatField;
    DataSetExportVSTAVKI: TFIBStringField;
    DataSetExportVSEGO: TFIBFloatField;
    TBItem31: TTBItem;
    acBuyerExport: TAction;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem32: TTBItem;
    acPrintFinMonitoringReport: TAction;
    SpeedbarSection3: TSpeedbarSection;
    TBSeparatorItem6: TTBSeparatorItem;
    TBItem33: TTBItem;
    acFixInvoiceTotalSumm: TAction;
    procedure FormCreate(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acDelDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure acRePriceExecute(Sender: TObject);
    procedure acImportInvExecute(Sender: TObject);
    procedure acExportInvExecute(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acUnselectExecute(Sender: TObject);
    procedure acStoneExecute(Sender: TObject);
    procedure acStoneUpdate(Sender: TObject);
    procedure acMultiSelectExecute(Sender: TObject);
    procedure acSemisExecute(Sender: TObject);
    procedure acSemisUpdate(Sender: TObject);
    procedure acExportMCExecute(Sender: TObject);
    procedure acExportMCUpdate(Sender: TObject);
    procedure acAddMCExecute(Sender: TObject);
    procedure acFRStatusExecute(Sender: TObject);
    procedure acFRZReportExecute(Sender: TObject);
    procedure acFROpenSellExecute(Sender: TObject);
    procedure acPrintFRSellCheckExecute(Sender: TObject);
    procedure acPrintFRRetSellCheckExecute(Sender: TObject);
    procedure acPrintChitUpdate(Sender: TObject);
    procedure acPrintFactureUpdate(Sender: TObject);
    procedure acPrintInvoiceUpdate(Sender: TObject);
    procedure acPrintInvoiceExecute(Sender: TObject);
    procedure acPrintFactureExecute(Sender: TObject);
    procedure acPrintChitExecute(Sender: TObject);
    procedure acCalcActWorkWExecute(Sender: TObject);
    procedure acCalcActWorkWUpdate(Sender: TObject);
    procedure acLogicExecute(Sender: TObject);
    procedure acLogicUpdate(Sender: TObject);
    procedure acCreateRetBuyerInvExecute(Sender: TObject);
    procedure acCreateRetBuyerInvUpdate(Sender: TObject);
    procedure acPrintInvUIDExecute(Sender: TObject);
    procedure acPrintInvUIDUpdate(Sender: TObject);
    procedure acEServParamsExecute(Sender: TObject);
    procedure acEServParamsUpdate(Sender: TObject);
    procedure acPrintActWorkExecute(Sender: TObject);
    procedure acPrintActWorkUpdate(Sender: TObject);
    procedure acPrintProdTolling1Update(Sender: TObject);
    procedure acPrintProdTolling2Update(Sender: TObject);
    procedure acPrintProdTolling1Execute(Sender: TObject);
    procedure acPrintProdTolling2Execute(Sender: TObject);
    procedure cbCompKeyPress(Sender: TObject; var Key: Char);
    procedure MX18PrintClick(Sender: TObject);
    procedure TBItem28Click(Sender: TObject);
    procedure TBItem29Click(Sender: TObject);
    procedure acInvToActWorkExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acAddEmptyClosedExecute(Sender: TObject);
    procedure DataSetExportBeforeOpen(DataSet: TDataSet);
    procedure acBuyerExportExecute(Sender: TObject);
    procedure acFixInvoiceTotalSummExecute(Sender: TObject);
    procedure acFixInvoiceTotalSummUpdate(Sender: TObject);
  protected
    procedure DepClick(Sender : TObject);override;
    procedure acCreateRetInvExecute(Sender: TObject);
    procedure acCreateRetInvUpdate(Sender: TObject);
    procedure acCreateSellInvExecute(Sender: TObject);
    procedure acCreateSellInvUpdate(Sender: TObject);
  public
    DefMX18: Boolean;
    procedure ExportINV(const FileName:string);
    procedure ExportINVDBF(const FileName:string);
    procedure ImportINV(const FileName:string);
    procedure GetItemsFromSTR(var str:string; var UID:integer; var SZ:string; var W:double; var ART,ART2:string; var Price:double);
    procedure AddInv(var F: textfile; const ITYPE_:integer);
    
  end;

var
  fmSList: TfmSList;


implementation

uses MainData, DocAncestor, dbUtil, DictData, ApplData, SInv_det,
  InvData, PrintData,Variants, INVCopy, SInvProd, SIEl, MsgDialog, ExportMC,
  FRData, Editor, eCheckOpt, ProductionConsts, UtilLib, EServParams, DictAncestor,
  eInvWeight, GraphUtil, uUtils,  PrintData2, AddEmptyInv, QExport3;

{$R *.DFM}

{ TfmSList }                                         

procedure TfmSList.GetItemsFromSTR(var str:string; var UID:integer; var SZ: string; var W:double; var ART,ART2:string; var Price:double);
var i:integer;
    st:string;
    u:shortstring;
begin
 str:=StringReplace(str,'.',',',[rfReplaceAll]);
 i:=1;
 while str[i]=' ' do inc(i);
 //UID
 st:='';
 while Str[i]<>' ' do
 begin
  st:=st+str[i];
  inc(i);
 end;
 UID:=StrToInt(St);

  while str[i]=' ' do inc(i);
 //ART
 st:='';
 while Str[i]<>' ' do
 begin
  st:=st+str[i];
  inc(i);
 end;
 ART:=St;

  while str[i]=' ' do inc(i);
 //ART2
 st:='';
 while Str[i]<>' ' do
 begin
  st:=st+str[i];
  inc(i);
 end;
 ART2:=St;
// if ART2<>'-' then ART2:='-';
  while str[i]=' ' do inc(i);
 //SZ
 st:='';
 while Str[i]<>' ' do
 begin
  st:=st+str[i];
  inc(i);
 end;
 Sz:=St;

  while str[i]=' ' do inc(i);
 //W
 st:='';
 while Str[i]<>' ' do
 begin
  st:=st+str[i];
  inc(i);
 end;
 W:=StrToFloat(st);


 while str[i]=' ' do inc(i);
 //U
 st:='';
 while Str[i]<>' ' do
 begin
  st:=st+str[i];
  inc(i);
 end;
 u:=st;

 while str[i]=' ' do inc(i);
 //Price
 st:='';
 while Str[i]<>' ' do
 begin
  st:=st+str[i];
  inc(i);
 end;
 Price:=StrToFloat(st);

 if (u='��.') or (u='��,') then  ART2:='-';
end;

procedure TfmSList.DataSetExportBeforeOpen(DataSet: TDataSet);
begin
  inherited;
  DataSetExport.ParamByName('InvID').AsInteger := dmInv.taSListINVID.AsInteger;
end;

procedure TfmSList.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  inherited DepClick(Sender);
end;

procedure TfmSList.ImportINV(const FileName:string);
var f:textfile;
    str:string;
    COde:integer; // ��� ��������
begin
 try
  screen.Cursor:=crHourGlass;
  AssignFile(f,FIleName);
  Reset(f);
  with dmINV do
   begin
    // ��� �������� (1- ���=>��, 2- ��=>���, 3-���=>���    )
    Readln(F,str);
    Code:=StrToInt(str);
    Case Code of
     1:begin
        AddInv(F,3);// ������� �� ���
        Readln(F,str);
        AddInv(F,2); // �������� ��
       end;
     2:begin
        AddInv(F,3);// ������� �� ���
        Readln(F,str);
        AddInv(F,2); // �������� ����������
      end;
     3:begin
        AddInv(F,3);// ������� �� ���
       end;
     else showmessage('����������� ��������');
    end;
   end;
   CLoseFile(f);
   MessageDialog('������ ������� ��������������!',mtInformation,[mbOk],0);
  finally
   screen.Cursor:=crDefault;
  end;
end;


procedure TfmSList.MX18PrintClick(Sender: TObject);
begin
  inherited;
  dmInv.FCurrINVID := dmInv.taSListINVID.AsInteger;
  dmPrint.taSItemsPrint.ParamByName('ASINVID').AsInteger := dmInv.FCurrInvID;
  dmPrint2.taInvUID.CloseOpen(false);
  dmPrint.taINVHeader.CloseOpen(false);
  dmPrint.taSItemsPrint.CloseOpen(false);
  //dmInv.taINVItems.CloseOpen(false);
  with dmPrint do
    begin
      FDocId := 99;
      taDoc.Open;
      frReport.OnGetValue := dmPrint.InvGetValue;
      frReport.LoadFromDB(taDoc, FDocId);
      frReport.PrepareReport;
      if dm.IsAdm then
        frReport.DesignReport
      else
        frReport.ShowPreparedReport;
    end;
end;

procedure TfmSList.TBItem28Click(Sender: TObject);
begin
  inherited;

  with dmPrint do
    begin
      dmInv.FCurrInvID := dmInv.taSlistInvID.AsInteger;
      taINVHeader.CloseOpen(false);
      taSItemsPrint.ParamByName('ASINVID').AsInteger := dmInv.FCurrINVID;
      taSItemsPrint.CloseOpen(false);
      FDocId := 7;
      taDoc.Open;
      frReport.OnGetValue := dmPrint.InvGetValue;
      frReport.LoadFromDB(taDoc, FDocId);
      frReport.PrepareReport;
      if dm.IsAdm then
        frReport.DesignReport
      else
        begin
          frReport.ShowPreparedReport;
        end;
    end;
end;

procedure TfmSList.TBItem29Click(Sender: TObject);
begin
  inherited;
  with dmINV do
  begin
  taSlist.Append;
    //PostDataSets([taSList]);
    //ShowAndFreeForm();
    //PostDataSet(taSList);
  end;
end;

procedure TfmSList.ExportINVDBF(const FileName: string);
//var
//  f:textfile;
//  str, Ins_char, INN, INN2 : string;
//  r : Variant;
//  b: boolean;
begin

   {
  PostDataSet(dmInv.taSList);
  CloseDataSet(dmInv.taStoneItems);
  // ��������� ��������
  r := ExecSelectSQL('select INN from D_Comp where CompId='+IntToStr(dm.User.SelfCompId), dm.quTmp);
  if VarIsNull(r) then
  begin
    MessageDialog('� ����������� ����������� �� ����� ��� ��� '+dm.User.SelfCompName, mtWarning, [mbOk], 0);
    INN := '';
  end
  else INN := VarToStr(r);

  if dmInv.taSListDOCNO.IsNull then MessageDialog(rsNotSelFieldDocNo, mtWarning, [mbOk], 0);
  if dmInv.taSListDOCDATE.IsNull then MessageDialog(rsNotSelFieldDocDate, mtWarning, [mbOk], 0);
  if dmInv.taSListSUPID.IsNull then MessageDialog(rsNotSelFieldSup, mtWarning, [mbOk], 0)
  else begin
    r := ExecSelectSQL('select INN from D_Comp where CompId='+dmInv.taSListSUPID.AsString, dm.quTmp);
    if VarIsNull(r) then
    begin
      MessageDialog('� ����������� ����������� �� ����� ��� ��� '+dmInv.taSListSUPNAME.AsString, mtWarning, [mbOk], 0);
      INN2 := '';
    end
    else INN2 := VarToStr(r);
  end;

  // ???
  dmInv.IsPrint := False;

//  AssignFile(F, FileName);
//  ReWrite(F);
  with dmInv do
  try

    dbf.TableName := FileName;
    dbf.AddFieldDefs('UID', bfNumber, 10, 0);
    dbf.AddFieldDefs('ART', bfString, 16, 0);
    dbf.AddFieldDefs('ART2', bfString, 32, 0);
    dbf.AddFieldDefs('SIZE', bfString, 4, 0);
    dbf.AddFieldDefs('WEIGHT', bfNumber, 10, 2);
    dbf.AddFieldDefs('UNIT', bfString, 4, 0);
    dbf.AddFieldDefs('PRICE', bfNumber, 10, 2);
    dbf.AddFieldDefs('MAININS', bfString, 16, 0);
    dbf.AddFieldDefs('GOOD', bfString, 16, 0);
    dbf.AddFieldDefs('PROBE', bfNumber, 3, 0);
    dbf.AddFieldDefs('INS', bfString, 255, 0);

    dbf.CreateTable;
    dbf.CodePage := ANSI;
    dbf.Close;

    dbf.Open;

    
    //WriteLn(F, taSListDOCNO.AsString); // ����� ���������
    //WriteLn(F, DateToStr(taSListDOCDate.AsDateTime)); // ���� ���������
    //WriteLn(F, dm.User.SelfCompName); // �� ����
    //WriteLn(F, INN);  // ��� �� ����
    //WriteLn(F, dmInv.taSListSUPNAME.AsString); // ����
    //WriteLn(F, INN2); //��� ����
    OpenDataSet(dmInv.taInvByItems);
    taInvByItems.Last;
    //WriteLn(F, IntToStr(taInvByItems.RecordCount)); //���-�� �������
    taInvByItems.First;
    while not taINVbyItems.Eof do
    begin
      Ins_Char:='';
      // �������
      OpenDataSet(taStoneItems);
      taStoneItems.First;
      while not taStoneItems.Eof do
      begin
        Ins_char:=Ins_char+';'+
                  taStoneItemsMATNAME.AsString+' '+
                  taStoneItemsQUANTITY.AsString+' '+
                  FloatToStr(taStoneItemsWEIGHT.AsFloat)+' '+
                  fif(taStoneItemsEDGSHAPEID.IsNull, '-', taStoneItemsEDGSHAPEID.AsString)+' '+
                  fif(taStoneItemsEDGTYPEID.IsNull, '-', taStoneItemsEDGTYPEID.AsString)+' '+
                  fif(taStoneItemsCLEANNES.IsNull, '-', taStoneItemsCLEANNES.AsString)+' '+
                  fif(taStoneItemsCHROMATICITY.IsNull, '-', taStoneItemsCHROMATICITY.AsString)+' '+
                  fif(taStoneItemsIGR.IsNull, '-', taStoneItemsIGR.AsString)+' '+
                  fif(taStoneItemsCOLORNAME.IsNull, '-', taStoneItemsCOLORNAME.AsString);
        taStoneItems.Next;
      end;
      CloseDataSet(taStoneItems);
      b := taInvByItemsART2.AsString = '-';
      //

      //str:= taInvByItemsUID.AsString+' '+
      //      taInvByItemsART.AsString+' '+
      //      fif(b, fif(StringReplace(taInvByItemsCOMMENTART.AsString, ' ', '_', [rfReplaceAll])='','-',StringReplace(taInvByItemsCOMMENTART.AsString, ' ', '_', [rfReplaceAll])),taInvByItemsART2.AsString)+' '+
      //      taInvByItemsSz.AsString+' '+
      //      FormatFloat('0.00', taInvByItemsW.AsFloat)+' '+
      //      taInvByItemsU.AsString+' '+
      //      FloatToStr(taInvByItemsPRICE2.AsFloat)+' '+
      //      taInvByItemsINS.AsString+' '+
      //     taInvByItemsSAMID.AsString+' '+
      //      taInvByItemsPRILL.AsString+' '+
      //      Ins_char;
      //
      //WriteLn(F, str);


     dbf.Append;
     dbf.SetFieldData(1, taInvByItemsUID.AsString);
     dbf.SetFieldData(2, taInvByItemsART.AsString);
     dbf.SetFieldData(3, fif(b, fif(StringReplace(taInvByItemsCOMMENTART.AsString, ' ', '_', [rfReplaceAll])='','-',StringReplace(taInvByItemsCOMMENTART.AsString, ' ', '_', [rfReplaceAll])),taInvByItemsART2.AsString));
     dbf.SetFieldData(4, taInvByItemsSz.AsString);
     dbf.SetFieldData(5, FormatFloat('0.00', taInvByItemsW.AsFloat));
     dbf.SetFieldData(6, taInvByItemsU.AsString);
     dbf.SetFieldData(7, FloatToStr(taInvByItemsPRICE2.AsFloat));
     dbf.SetFieldData(8, taInvByItemsINS.AsString);
     dbf.SetFieldData(9, taInvByItemsSAMID.AsString);
     dbf.SetFieldData(10, taInvByItemsPRILL.AsString);
     dbf.SetFieldData(11, Ins_char);
      taInvByItems.Next;
    end;
    MessageDialogA('������ ������� ���������������', mtInformation);
  finally
     dbf.Close;
    //CloseFile(f);
    //CloseDataSets([taInvByItems, taStoneItems]);
  end; }
end;


procedure TfmSList.ExportINV(const FileName:string);
var
  f:textfile;
  str, Ins_char, INN, INN2 : string;
  r : Variant;
  b: boolean;
begin
  PostDataSet(dmInv.taSList);
  CloseDataSet(dmInv.taStoneItems);
  // ��������� ��������
  r := ExecSelectSQL('select INN from D_Comp where CompId='+IntToStr(dm.User.SelfCompId), dm.quTmp);
  if VarIsNull(r) then
  begin
    MessageDialog('� ����������� ����������� �� ����� ��� ��� '+dm.User.SelfCompName, mtWarning, [mbOk], 0);
    INN := '';
  end
  else INN := VarToStr(r);

  if dmInv.taSListDOCNO.IsNull then MessageDialog(rsNotSelFieldDocNo, mtWarning, [mbOk], 0);
  if dmInv.taSListDOCDATE.IsNull then MessageDialog(rsNotSelFieldDocDate, mtWarning, [mbOk], 0);
  if dmInv.taSListSUPID.IsNull then MessageDialog(rsNotSelFieldSup, mtWarning, [mbOk], 0)
  else begin
    r := ExecSelectSQL('select INN from D_Comp where CompId = '+dmInv.taSListSUPID.AsString, dm.quTmp);
    if VarIsNull(r) then
    begin
      MessageDialog('� ����������� ����������� �� ����� ��� ��� '+dmInv.taSListSUPNAME.AsString, mtWarning, [mbOk], 0);
      INN2 := '';
    end
    else INN2 := VarToStr(r);
  end;

  // ???
  dmInv.IsPrint := False;

  AssignFile(F, FileName);
  ReWrite(F);
  with dmInv do
  try
    WriteLn(F, taSListDOCNO.AsString); //����� ���������
    WriteLn(F, DateToStr(taSListDOCDate.AsDateTime)); // ���� ���������
    WriteLn(F, dm.User.SelfCompName); // �� ����
    WriteLn(F, INN);  //��� �� ����
    WriteLn(F, dmInv.taSListSUPNAME.AsString); // ����
    WriteLn(F, INN2); //��� ����
    OpenDataSet(dmInv.taInvByItems);
    taInvByItems.Last;
    WriteLn(F, IntToStr(taInvByItems.RecordCount)); //���-�� �������
    taInvByItems.First;
    while not taINVbyItems.Eof do
    begin
      Ins_Char:='';
      // �������
      OpenDataSet(taStoneItems);
      taStoneItems.First;
      while not taStoneItems.Eof do
      begin
        Ins_char:=Ins_char+';'+
                  AnsiReplaceStr(taStoneItemsMATNAME.AsString, ' ', '.')+' '+
                  IntToStr(taStoneItemsQUANTITY.AsInteger)+' '+
                  FloatToStr(taStoneItemsWEIGHT.AsFloat)+' '+
                  fif(taStoneItemsEDGSHAPEID.IsNull, '-', taStoneItemsEDGSHAPEID.AsString)+' '+
                  fif(taStoneItemsEDGTYPEID.IsNull, '-', taStoneItemsEDGTYPEID.AsString)+' '+
                  fif(taStoneItemsCLEANNES.IsNull, '-', taStoneItemsCLEANNES.AsString)+' '+
                  fif(taStoneItemsCHROMATICITY.IsNull, '-', taStoneItemsCHROMATICITY.AsString)+' '+
                  fif(taStoneItemsIGR.IsNull, '-', taStoneItemsIGR.AsString)+' '+
                  fif(taStoneItemsCOLORNAME.IsNull, '-', taStoneItemsCOLORNAME.AsString);
        taStoneItems.Next;
      end;
      CloseDataSet(taStoneItems);
      b := taInvByItemsART2.AsString = '-';
      str:= taInvByItemsUID.AsString+' '+
            taInvByItemsART.AsString+' '+
            fif(b, fif(StringReplace(taInvByItemsCOMMENTART.AsString, ' ', '_', [rfReplaceAll])='','-',StringReplace(taInvByItemsCOMMENTART.AsString, ' ', '_', [rfReplaceAll])),taInvByItemsART2.AsString)+' '+
            taInvByItemsSz.AsString+' '+
            FormatFloat('0.00', taInvByItemsW.AsFloat)+' '+
            taInvByItemsU.AsString+' '+
            FloatToStr(taInvByItemsPRICE2.AsFloat)+' '+
            AnsiReplaceStr(taInvByItemsINS.AsString, ' ', '.')+' '+
            taInvByItemsSAMID.AsString+' '+
            taInvByItemsPRILL.AsString+' '+
            Ins_char;
      WriteLn(F, str);
      taInvByItems.Next;
    end;
    MessageDialogA('������ ������� ��������������!', mtInformation);
  finally
    CloseFile(f);
    CloseDataSets([taInvByItems, taStoneItems]);
  end;
end;



procedure TfmSList.FormCreate(Sender: TObject);
var
  i, j : integer;
  Item : TMenuItem;
  FindDep : boolean;
  it : TTBItem;
  ac : TAction;
begin
  gridDocList.DataSource := dmInv.dsrSList;
  FBD := dm.BeginDate;
  FED := dm.EndDate;
  //ppCassa.Skin := dm.ppSkin;
  dm.WorkMode := 'SList';
  // ������ �� �����������
  cbComp.OnChange := nil;
  cbComp.Items.Clear;
  cbComp.Items.Assign(dm.dComp);
  cbComp.ItemIndex := 0;
  //cbComp.OnChange := cbCompChange;
 // DefMX18 := False;

  case dmInv.IType of
    2 : begin
      spitPrint.DropDownMenu := ppPrint;
      spitAdd.DropDownMenu := ppAdd;
    end;
    3,12 : begin
      spitPrint.DropDownMenu := nil;
      spitAdd.DropDownMenu := nil;
    end;
    else raise EInternal.Create(Format(rsInternalError, ['220']));
  end;
  FCurrDep := 23;
  //���������� PopupMenu �� FDepInfo;
  j := 0;
  for i:=0 to Pred(dm.CountDep) do
    if dm.DepInfo[i].Wh then
    begin
      if (FCurrDep = dm.DepInfo[i].DepId)then FindDep := True;
      Item := TMenuItem.Create(ppDep);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepClick;
      Item.RadioItem := True;
      if Item.Tag = dm.DepDef then DepClick(Item);
      inc(j);
      if j>30 then
      begin
        Item.Break := mbBreak;
        j:=0;
      end;
      ppDep.Items.Add(Item);
    end;
  if FindDep then
  begin
    dm.CurrDep := FCurrDep;
    laDep.Caption := dm.DepInfoById[FCurrDep].Name;
  end
  else begin
    dm.CurrDep := -1;
    laDep.Caption := mnitAllWh.Caption;
  end;

  // ���������� ���� ��� �������� �� ������������ 
  ReOpenDataSet(dm.taCompLogin);
  while not dm.taCompLogin.Eof do
  begin
    ac := TAction.Create(acAEvent);
    ac.OnUpdate := acCreateRetInvUpdate;
    ac.OnExecute := acCreateRetInvExecute;
    ac.Caption := dm.taCompLoginNAME.AsString;
    ac.Tag := dm.taCompLoginCOMPID.AsInteger;
    it := TTBItem.Create(ppDoc);
    it.Action := ac;
    it.Caption := dm.taCompLoginNAME.AsString;
    it.Tag := dm.taCompLoginCOMPID.AsInteger;
    tbsRetProd.Add(it);

    ac := TAction.Create(acAEvent);
    ac.OnUpdate := acCreateSellInvUpdate;
    ac.OnExecute := acCreateSellInvExecute;
    ac.Caption := dm.taCompLoginNAME.AsString;
    ac.Tag := dm.taCompLoginCOMPID.AsInteger;
    it := TTBItem.Create(ppDoc);
    it.Action := ac;
    it.Caption := dm.taCompLoginNAME.AsString;
    it.Tag := dm.taCompLoginCOMPID.AsInteger;
    tbsSellInv.Add(it);
    
    dm.taCompLogin.Next;
  end;

  inherited;

  case dmInv.ITYPE of
   2: begin // �������
        Caption:='������ ��������� ������';
        cbComp.Visible := True;
        acInvToActWork.Enabled := false;
        btnCassa.Visible := True;
        gridDocList.FieldColumns['SUPNAME'].Visible:=true;
        gridDocList.FieldColumns['FROMDEPNAME'].Visible:=false;
        gridDocList.FieldColumns['SUMM2'].Visible:=true;
        gridDocList.FieldColumns['RESTSUM'].Visible:=false;
        gridDocList.FieldColumns['CONTRACTNAME'].Visible := True;
        gridDocList.FieldColumns['DEPNAME'].Title.Caption:='���������';
        gridDocList.FieldColumns['SUPNAME'].Title.Caption:='����������';
      end;
   3: begin
        Caption:='������ ��������� �������� �� �����������';
        cbComp.Visible := True;
        acInvToActWork.Enabled := dm.User.SelfCompId <> 1;
        btnCassa.Visible := False;
        gridDocList.FieldColumns['ANALIZ'].Visible:=false;
        gridDocList.FieldColumns['SENDED'].Visible:=false;
        gridDocList.FieldColumns['PAYED'].Visible:=false;
        gridDocList.FieldColumns['TRANS_PRICE'].Visible:=false;
        gridDocList.FieldColumns['W2'].Visible:=false;

        gridDocList.FieldColumns['SUPNAME'].Visible:=true;
        gridDocList.FieldColumns['FROMDEPNAME'].Visible:=false;
        gridDocList.FieldColumns['SUMM2'].Visible:=true;
        gridDocList.FieldColumns['RESTSUM'].Visible:=false;
        gridDocList.FieldColumns['CONTRACTNAME'].Visible := True;
        gridDocList.FieldColumns['DEPNAME'].Title.Caption:='����������';
        gridDocList.FieldColumns['SUPNAME'].Title.Caption:='���������';
     end;
   4: begin
        Caption:='������ ��������� ��������';
        cbComp.Visible := False;
        acInvToActWork.Enabled := false;
        btnCassa.Visible := False;
        gridDocList.FieldColumns['SUPNAME'].Visible:=False;
        gridDocList.FieldColumns['FROMDEPNAME'].Visible:=True;
        gridDocList.FieldColumns['SUMM2'].Visible:=False;
        gridDocList.FieldColumns['RESTSUM'].Visible:=True;
        gridDocList.FieldColumns['CONTRACTNAME'].Visible := True;
        gridDocList.FieldColumns['FROMDEPNAME'].Title.Caption:='���������';
        gridDocList.FieldColumns['DEPNAME'].Title.Caption:='����������';
      end;
   12: begin
       cbComp.Visible:=false;
       acInvToActWork.Enabled := false;
       btnCassa.Visible := False;
       Caption:='������� �� ������������';
       gridDocList.FieldColumns['ANALIZ'].Visible:=false;
       gridDocList.FieldColumns['SENDED'].Visible:=false;
       gridDocList.FieldColumns['PAYED'].Visible:=false;
       gridDocList.FieldColumns['TRANS_PRICE'].Visible:=false;
       gridDocList.FieldColumns['SUPNAME'].Visible:=false;
       gridDocList.FieldColumns['FROMDEPNAME'].Visible:=true;
       gridDocList.FieldColumns['SUMM2'].Visible:=false;
       gridDocList.FieldColumns['RESTSUM'].Visible:=false;
       gridDocList.FieldColumns['CONTRACTNAME'].Visible := True;
       gridDocList.FieldColumns['FROMDEPNAME'].Title.Caption:='����������';
       gridDocList.FieldColumns['DEPNAME'].Title.Caption:='���������';
      end;
  end;
  lbComp.Visible:=cbComp.Visible;
  dmInv.FSupname:=ROOT_KNAME_CODE;
end;

procedure TfmSList.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  {case Key of
    VK_F6:
         begin
           if GridDocList.ReadOnly then
             begin
               GridDocList.readOnly := false;
               dmInv.taSlist.Edit;
               ShowMessage('�������������� ���������!');
               dmInv.sqlPublic.SQL.Clear;
               dmInv.sqlPublic.SQL.Add('update inv set w = :weight, q = :quant where invid = :invid');
             end
           else
             if dmInv.taSList.State = dsEdit then
               begin
                 try
                   dmInv.sqlPublic.ParamByName('Weight').AsFloat := dmInv.taSlistW.AsFloat;
                   dmInv.sqlPublic.ParamByName('Quant').AsFloat := dmInv.taSlistQ.AsInteger;
                   dmInv.sqlPublic.ParamByName('Invid').AsFloat := dmInv.taSlistINVID.AsInteger;
                   dmInv.sqlPublic.Prepare;
                   dmInv.sqlPublic.ExecQuery;
                   dmInv.sqlPublic.Transaction.CommitRetaining;
                 except
                   on E : exception do
                     ShowMessage('������ ��� ���������� ������: ' + E.Message);
                 end;
                 dmInv.taSList.Close;
                 dmInv.taSList.Open;
                 GridDocList.ReadOnly := true;
                 ShowMessage('������ ���������!');
               end;
         end;
  end;}
end;

procedure TfmSList.AddInv(var F: textfile; const ITYPE_:integer);
var
    str:string;
    i:integer;
//    COde:integer; // ��� ��������
    docno:integer;
    docdate:TDateTime;
    CUST:string;
    SUP:string;
//    SUM_W:double;// ��������� ���
    SUM_Q:integer;// �������� ����������
//    ART2ID:integer;
    ART:string;
    ART2:String;
    UID:integer;
    W:double;
    SZ:string;
    Price:double;
    INVID:integer;
begin
  try
  with dmINV do
   begin
    Readln(F,str);
    Docno:=StrToInt(str);

    Readln(F,str);
    Docdate:=StrToDate(str);

    if ITYPE_=3 then Readln(F,CUST)
    else Readln(F,SUP);

    Readln(F,str); //������

    if ITYPE_=3 then Readln(F,SUP)
    else Readln(F,CUST);

    Readln(F,str); //������
    Readln(F,str);
    SUM_Q:=StrToInt(str);
{   if taSList.Locate('DOCNO;DOCDATE',VarArrayOf([Docno,DocDate]),[loPartialKey])
    then showmessage('����� ��������� ��� ����!')
    else begin}
    INVID:=dm.GetId(12);
    sqlAddINV.ParamByName('INVID').asinteger:=INVID;
    sqlAddINV.ParamByName('DOCNO').asinteger:=DOCNO;
    sqlAddINV.ParamByName('ITYPE').asinteger:=ITYPE_;
    { TODO : ��� ���� �� �������� ���������?? }
    sqlAddINV.ParamByName('DOCDATE').AsDateTime:=docdate;
    sqlAddINV.ParamByName('DEPNAME').AsString:=SUP;
    sqlAddINV.ParamByName('SUPNAME').AsString:=CUST;
    sqlAddINV.ParamByName('USERID').AsInteger:=dm.User.UserId;

    sqlAddINV.ExecQuery;
    sqlAddINV.Transaction.CommitRetaining;

//    OpenDataSets([taINVItems,  dm.taArt]);
    OpenDataSets([dm.taArt]);
     for i:=1 to SUM_Q do
      begin
       Readln(F,str);
       GetItemsFromSTR(str,UID,SZ,W,ART,ART2,PRICE);
       dmInv.taFindArts.ParamByName('ART').AsString:=ART;
       dmInv.taFindArts.ParamByName('ART2').AsString:=ART2;
       ReOpenDataSet(dmInv.taFindArts);
       if dmInv.taFindArtsARTID.IsNull then
       begin
        ShowMessage('��� �������� '+ART);
        exit;
       end;
       if dmInv.taFindArtsART2ID.IsNull then
       begin
        ShowMessage('��� ��������2 '+ART2);
        exit;
       end;

       sqlAddSel.ParamByName('INVID').AsInteger:=INVID;
       sqlAddSel.ParamByName('ARTID').AsInteger:=dmINV.taFindArtsARTID.AsInteger;
       sqlAddSel.ParamByName('ART2ID').AsInteger:=dmINV.taFindArtsART2ID.AsInteger;
       sqlAddSel.ParamByName('UID').AsInteger:=UID;
       sqlAddSel.ParamByName('W').AsFloat:=W;
       sqlAddSel.ParamByName('SZ').AsSTring:=SZ;
       sqlAddSel.ParamByName('PRICE').AsFloat:=PRICE;
       sqlAddSel.ExecQuery;
       sqlAddSel.Transaction.CommitRetaining;
    end;
   end;
  except
   raise EWarning.Create('������ ��� �������� ������');
   dmInv.sqlAddSel.Transaction.Rollback;
  end;
  ReOpenDataSets([dmINV.taSList]);
  CloseDataSet(dmInv.taFindArts);
end;



procedure TfmSList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if dmInv.taSListINVSUBTYPEID.AsInteger = 9 then
     begin
       Background := clAqua;
       AFont.Color := clWindowText;
     end
  else
  if(Column.FieldName = 'DOCNO')then
  begin
    if dmINV.taSListISMC.AsInteger = 1 then
    begin
      Background:=$00ECEEF0;
      AFont.Color := clWindowText;
    end
    else
    begin
      if dmInv.taSListCOLOR.IsNull then
      begin
        Background := clMenuBar;
        AFont.Color := clBtnText;
      end
      else
      begin
        Background := dmInv.taSListCOLOR.AsInteger;
        AFont.Color := clWindowText;
      end;
    end;
  end
  else

    if (gdFocused in State) then
    begin
      if dmINV.taSListIsClose.AsInteger=1 then
      begin
        Background := clHighLight;
        AFont.Color := clHighlightText;
      end
      else
      begin
        Background :=  clWindow;
        AFont.Color := clWindowText;
      end;
    end
    else
    begin
      if dmINV.taSListIsClose.AsInteger = 1 then
      begin
        Background := clMenuBar;
        AFont.Color := clBtnText;
      end
      else
      begin
        Background := clWindow;
        AFont.Color := clWindowText;
      end;
    end;

end;

procedure TfmSList.cbCompKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key = #13 then
    begin
      if cbComp.ItemIndex>0
        then dmInv.FSupname:=cbComp.Value
      else dmInv.FSupname:=ROOT_KNAME_CODE;
      PostDataSet(dmInv.taSList);
      ReOpenDataSet(dmInv.taSList);
    end;
end;

procedure TfmSList.acAddDocExecute(Sender: TObject);
begin
  inherited;
  with dmINV do
  begin
    if ITYPE = 3 then
      InformationMessage('��������! ����� ��������� �������� ' + #13#10 +
                         '��������� � ���, ��� �� ������������ ������� ��� ' + #13#10 +
                         '������ ��� ���������� �����!');
    PostDataSets([taSList]);
    if SType = 0 then ShowAndFreeForm(TfmSInv_det, TForm(fmSInv_det))
    else ShowAndFreeForm(TfmSInvProd, TForm(fmSInvProd));
    if QSEL=0 then taSList.Delete;
    PostDataSet(taSList);
  end;
end;

procedure TfmSList.acDelDocExecute(Sender: TObject);
begin
  if ((dmInv.taSListISCLOSE.ASinteger = 1) and (dmInv.taSListInvSubTypeID.AsInteger <> 9)) then
    begin
      ShowMessage('������ ������� �������� ���������!');
      SysUtils.Abort;
    end;
  dmInv.taSList.Delete;
end;

procedure TfmSList.acEditDocExecute(Sender: TObject);
var Comment: String[80];
begin
  with dmINV do
  begin
    if taSListInvSubTypeID.AsInteger = 9 then
      begin
        TfmAddEmptyInv.Execute(1);
        eXit;
      end
    else
      begin
        taSList.Edit;
        if (dmInv.SType = 0) then ShowAndFreeForm(TfmSInv_det, TForm(fmSInv_det))
        else ShowAndFreeForm(TfmSInvProd, TForm(fmSInvProd));
        PostDataSet(taSList);
        RefreshDataSet(taSList);
      end;
  end;
end;

procedure TfmSList.acPrintDocExecute(Sender: TObject);
begin
  if (dmInv.IType = 2) then eXit;
  dmInv.IsPrint:=true;
  dmInv.FcurrINVID:=dminv.taSListINVID.asinteger;
  OpenDataSet(dmInv.taINVItems);
  case dmInv.ITYPE of
    //2:   dmPrint.PrintDocumentA(dkSell, VarArrayOf([VarArrayOf([dmInv.FcurrINVID]),VarArrayOf([dmInv.FcurrINVID])]));
    3:   dmPrint.PrintDocumentA(dkSellRet, VarArrayOf([VarArrayOf([dmInv.FcurrINVID]),VarArrayOf([dmInv.FcurrINVID])]));
    4:   dmPrint.PrintDocumentA(dkIn, VarArrayOf([VarArrayOf([dmInv.FcurrINVID]),VarArrayOf([dmInv.FcurrINVID])]));
    12:  dmPrint.PrintDocumentA(dkRet, VarArrayOf([VarArrayOf([dmInv.FcurrINVID]),VarArrayOf([dmInv.FcurrINVID])]));
    else raise EInternal.Create(Format(rsInternalError, ['220']));
  end;
  CloseDataSet(dminv.taINVItems);
end;

procedure TfmSList.acRePriceExecute(Sender: TObject);
begin
 inherited;

 dmInv.sqlRepriceINV.ParamByName('UP').AsInteger:=0;
 dmInv.sqlRepriceINV.ParamByName('INVID').AsInteger:=dmInv.taSListINVID.AsInteger;
 dmInv.sqlRepriceINV.ExecQuery;
 dmInv.sqlRepriceINV.Transaction.CommitRetaining;

end;

procedure TfmSList.acImportInvExecute(Sender: TObject);
begin
    inherited;
 if OpenDialog1.Execute then
 begin
  ImportINV(OpenDialog1.FileName);
 end;
end;

procedure TfmSList.acInvToActWorkExecute(Sender: TObject);
begin
  inherited;
  ExecSQL('execute procedure Inv_To_ActWork(' + dmInv.taSListINVID.AsString + ')', dmMain.quTmp);
  dmMain.quTmp.Transaction.CommitRetaining;
end;

procedure TfmSList.acExportInvExecute(Sender: TObject);
begin
  inherited;
  if SaveDialog1.Execute then
  if SaveDialog1.FilterIndex = 1 then ExportINV(SaveDialog1.FileName) else
//  if SaveDialog1.FilterIndex = 2 then ExportINVDBF(SaveDialog1.FileName);
end;

procedure TfmSList.acCreateSellInvExecute(Sender: TObject);
var
  c : TCopyInvInfo;
begin
  c.DefaultComp := -1; 
  c.SelfCompId := (Sender as TAction).Tag;
  c.CanChangeComp := True;
  c.DefaultDep := -1;
  c.CanChangeDep := True;
  c.DepComp := False;
  c.BetweenOrg := dm.User.SelfCompId <> (Sender as TAction).Tag;
  c.CopyInvId := dmInv.taSListINVID.AsInteger;
  c.CopyTypeFrom := 3;
  c.CopyType := 2;
  c.FromDep := -1;
  c.NDSId := -1;
  ShowInvCopy(c);
end;

procedure TfmSList.acCreateRetInvExecute(Sender: TObject);
var
  c : TCopyInvInfo;
begin
  c.DefaultComp := (Sender as TAction).Tag;
  c.CanChangeComp := False;
  c.DefaultDep := -1;
  c.CanChangeDep := True;
  c.DepComp := True;
  c.BetweenOrg := dm.User.SelfCompId <> (Sender as TAction).Tag;
  c.CopyInvId := dmInv.taSListINVID.AsInteger;
  c.CopyTypeFrom := 3;
  c.CopyType := 12;
  c.FromDep := dmInv.taSListDEPID.AsInteger;
  ShowInvCopy(c);
end;

procedure TfmSList.acPeriodExecute(Sender: TObject);
begin
  if ShowPeriodForm(FBD, FED) then
  begin
    dmMain.ChangePeriod(FBD, FED);
    ShowPeriod;
    Application.ProcessMessages;
    ReOpenDataSets([gridDocList.DataSource.DataSet]);
    Application.ProcessMessages;
  end;
end;

procedure TfmSList.acUnselectExecute(Sender: TObject);
begin
  inherited;
   with dminv.sqlPublic do
   begin
     SQL.Clear;
     SQL.Add('update INV set ANALIZ=0 where (ITYPE=2) or (ITYPE=3)');
     ExecQuery;
     Transaction.CommitRetaining;
   end;
   ReOpenDataSet(dminv.taSList);
end;

procedure TfmSList.acStoneExecute(Sender: TObject);
var
  SaveStatement, s : string;
  SaveStatementSum: string;
  i : integer;
begin
  with dmPrint, dmInv do
  try
    SaveStatement := taStoneInInv.SelectSQL.Text;
    SaveStatementSum := taStoneInInvSum.SelectSQL.Text;
    if(gridDocList.SelectedRows.Count > 1)  then
    begin
      taSList.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[0]));
      s := taSListINVID.AsString;
      for i:=1 to Pred(gridDocList.SelectedRows.Count) do
      begin
        taSList.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[i]));
        s := s+', '+taSListINVID.AsString;
      end;
      taStoneInInv.SelectSQL.Text := StringReplace(taStoneInInv.SelectSQL.Text, '__INSERT_TEXT_THIS__', s, [rfReplaceAll, rfIgnoreCase]);
      taStoneInInvSum.SelectSQL.Text := StringReplace(taStoneInInvSum.SelectSQL.Text, '__INSERT_TEXT_THIS__', s, [rfReplaceAll, rfIgnoreCase]);
    end
    else
    begin
      taStoneInInv.SelectSQL.Text := StringReplace(taStoneInInv.SelectSQL.Text, '__INSERT_TEXT_THIS__', taSListINVID.AsString, [rfReplaceAll, rfIgnoreCase]);
      taStoneInInvSum.SelectSQL.Text := StringReplace(taStoneInInvSum.SelectSQL.Text, '__INSERT_TEXT_THIS__', taSListINVID.AsString, [rfReplaceAll, rfIgnoreCase]);
    end;
    PrintDocumentA(dkStoneInInv, Null);
  finally
    taStoneInInv.SelectSQL.Text := SaveStatement;
    taStoneInInvSum.SelectSQL.Text := SaveStatementSum;    
  end
end;

procedure TfmSList.acStoneUpdate(Sender: TObject);
begin
  acStone.Enabled :=  DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmSList.acMultiSelectExecute(Sender: TObject);
begin
  if dgMultiSelect in gridDocList.Options then
  begin
    gridDocList.Selection.Clear;
    gridDocList.Options := gridDocList.Options - [dgMultiSelect];
  end
  else gridDocList.Options := gridDocList.Options+[dgMultiSelect];
end;

procedure TfmSList.acSemisExecute(Sender: TObject);
var
  SaveStatement, s : string;
  i : integer;
begin
  with dmPrint, dmInv do
  try
    SaveStatement := taSemisInInv.SelectSQL.Text;
    if(gridDocList.SelectedRows.Count > 1)  then
    begin
      taSList.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[0]));
      s := taSListINVID.AsString;
      for i:=1 to Pred(gridDocList.SelectedRows.Count) do
      begin
        taSList.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[i]));
        s := s+', '+taSListINVID.AsString;
      end;
      taSemisInInv.SelectSQL.Text := StringReplace(taSemisInInv.SelectSQL.Text, '__INSERT_TEXT_THIS__', s, [rfReplaceAll, rfIgnoreCase]);
    end
    else taSemisInInv.SelectSQL.Text := StringReplace(taSemisInInv.SelectSQL.Text, '__INSERT_TEXT_THIS__', taSListINVID.AsString, [rfReplaceAll, rfIgnoreCase]);
    PrintDocumentA(dkSemisInInv, Null);
  finally
    taSemisInInv.SelectSQL.Text := SaveStatement;
  end
end;

procedure TfmSList.acSemisUpdate(Sender: TObject);
begin
  acSemis.Enabled :=  DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmSList.acExportMCExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmExportMC, TForm(fmExportMC));
end;

procedure TfmSList.acExportMCUpdate(Sender: TObject);
begin
  with gridDocList.DataSource.DataSet do
    acExportMC.Enabled := Active and (not IsEmpty);
end;


procedure TfmSList.acAddEmptyClosedExecute(Sender: TObject);
begin
  inherited;
  TfmAddEmptyInv.Execute(0);
end;

procedure TfmSList.acAddMCExecute(Sender: TObject);
begin
  if(FCurrDep = -1)then
  begin
    MessageDialogA('���������� ������� �����', mtWarning, 0);
    Abort;
  end;
  with dmINV do
  begin
    DataSet.Append;
    taSListISMC.AsInteger := 1;
    DataSet.Post;
    DataSet.Transaction.CommitRetaining;
    ShowAndFreeForm(TfmSInv_det, TForm(fmSInv_det));
    PostDataSet(taSList);
    RefreshDataSet(taSList);
  end;
end;

procedure TfmSList.acBuyerExportExecute(Sender: TObject);
begin
  inherited;
  try
    QExportLocale.LoadDll('QERussian.dll'); //����������� �������
    DataSetExport.Open;
    ExportDialog.FileName := GetSpecialFolderLocation(CSIDL_Personal) + '\' + dmInv.taSListDOCNO.AsString;
    ExportDialog.Execute;
    QExportLocale.UnloadDll;
  finally
    DataSetExport.Close;
  end;
end;


procedure TfmSList.acFRStatusExecute(Sender: TObject);
var
  frprinterstat : TFRPrinterStatus;
  frstat : TFRStatus;
  s : string;
  b : boolean;
begin
  b := dmFR.GetFRStatus(frstat, frprinterstat);
  if not b then
  begin
    s := '�� ������� �������� ������ �� ����������� ������������'#13+
         '��������� '#13+
         '  1. ��������� �� ���������� ����������� � ���������� '#13+
         '  2. ������� �� ���������� ����������� � ������� �������';
  end
  else begin
    s := '��������� ����������� ������������:'#13;
    if(frprinterstat.Err) then s:=s+'  ������: ��'#13 else s:=s+'  ������: ���'#13;
    if(frprinterstat.PaperLost) then s:=s+'  ����������� ������: ��'#13 else s:=s+'  ����������� ������: ���'#13;
    if(frprinterstat.Ready)then s:=s+'  ������� �����: ��'#13 else s:=s+'  ������� �����: ���'#13;
    if(frprinterstat.PrnUse)then s:=s+'  ������� �����, ��������� � ��������� offline: ��'#13 else s:=s+'  ������� ����� ��� ��������: ���'#13;
    if(frstat.SellOpen)then s:=s+'  ����� �������: �� ('+DateTimeToStr(frstat.SellOpenDateTime)+')'#13 else s:=s+'  ����� �������: ���'#13;
    if(frstat.Fiscal)then s:=s+'  ������� ��������������: ��'#13 else s:=s+'  ������� ��������������: ���'#13;
    if(frstat.FiscalMemoryNearFull)then s:=s+'  ���������� ������ ������ � �����: ��'#13 else s:=s+'  ���������� ������ ������ � �����: ���'#13;
    if(frstat.FiscalMemoryFull)then s:=s+'  ���������� ������ ���������: ��'#13 else s:=s+'  ���������� ������ ���������: ���'#13;
  end;
  MessageDialogA(s, mtInformation);
end;

procedure TfmSList.acFRZReportExecute(Sender: TObject);
begin
  if not dmFR.CloseSell then
    MessageDialogA('������. '+dmFR.GetLastFRError, mtInformation)
end;

procedure TfmSList.acFixInvoiceTotalSummExecute(Sender: TObject);
begin

  inherited;

  with dmInv.sqlPublic do
  begin
    SQL.Text := 'execute procedure fix$invoice$total$sum(:invoice$id)';
    ParamByName('Invoice$ID').AsInteger := dmInv.taSListINVID.AsInteger;
    ExecQuery;
    Transaction.CommitRetaining;
    Close;
  end;

  dmInv.taSList.Refresh;

  gridDocList.Refresh;

end;

procedure TfmSList.acFixInvoiceTotalSummUpdate(Sender: TObject);
var
  InvoiceValid, UserValid: Boolean;
begin
  InvoiceValid := (dmInv.taSListITYPE.AsInteger in [2,3]) and (dmInv.taSListISCLOSE.AsInteger = 0);

  UserValid := (dm.User.Profile = -1) or (dm.User.Profile = 2);

  acFixInvoiceTotalSumm.Enabled := UserValid and InvoiceValid;
end;

procedure TfmSList.acFROpenSellExecute(Sender: TObject);
var
  n : integer;
begin
  if not dmFR.OpenSell(dm.User.UserId, dm.User.FIO, n)then
    MessageDialogA('������. '+dmFR.GetLastFRError, mtInformation)
  else MessageDialogA('������� ����� #'+IntToStr(n), mtInformation);
end;

procedure TfmSList.acPrintFRSellCheckExecute(Sender: TObject);
var
  n : integer;
  nDate : TDateTime;
begin
  dmInv.FCassaMoney := 0.00;
  dmInv.FCassaGiveMoney := 0.00;
  dmInv.FCassaUseNDS := dm.LocalSetting.Cassa_UseNDS;
  if ShowEditor(TfmeCheckOpt, TfmEditor(fmeCheckOpt))<>mrOk then eXit;
  if not dmFR.FRDocument(skSell, n, nDate, '0000', Format('%.2f',[dmInv.FCassaMoney]),
    Format('%.2f',[dmInv.FCassaGiveMoney]), dmInv.FCassaUseNDS, Format('%.2f',[dmInv.FCassaNDSMoney]))
  then MessageDialogA('������. '+dmFR.GetLastFRError, mtInformation)
  else begin
    dmFR.SaveFRDocToDB(skSell, n, nDate);
    MessageDialogA('������� ��� #'+IntToStr(n), mtInformation);
  end
end;

procedure TfmSList.acPrintFRRetSellCheckExecute(Sender: TObject);
var
  n : integer;
  nDate : TDateTime;
begin
  dmInv.FCassaMoney := 0.00;
  dmInv.FCassaGiveMoney := 0.00;
  dmInv.FCassaUseNDS := False;
  if ShowEditor(TfmeCheckOpt, TfmEditor(fmeCheckOpt))<>mrOk then eXit;
  if not dmFR.FRDocument(skRetSell, n, nDate, '0000', Format('%.2f',[dmInv.FCassaMoney]),
    Format('%.2f',[dmInv.FCassaGiveMoney]), dmInv.FCassaUseNDS, Format('%.2f',[dmInv.FCassaNDSMoney]))
  then MessageDialogA('������. '+dmFR.GetLastFRError, mtInformation)
  else begin
    dmFR.SaveFRDocToDB(skRetSell, n, nDate);
    MessageDialogA('�������. ���. #'+IntToStr(n), mtInformation);
  end;
end;

procedure TfmSList.acCreateSellInvUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := dmInv.taSList.Active and (not dmInv.taSList.IsEmpty) and
    (dmInv.taSListISCLOSE.AsInteger = 1); 
end;

procedure TfmSList.acCreateRetInvUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := dmInv.taSList.Active and (not dmInv.taSList.IsEmpty) and
     (dmInv.taSListISCLOSE.AsInteger = 1);
end;

procedure TfmSList.acPrintChitUpdate(Sender: TObject);
begin
  acPrintChit.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmSList.acPrintFactureUpdate(Sender: TObject);
begin
  acPrintFacture.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmSList.acPrintInvoiceUpdate(Sender: TObject);
begin
  acPrintInvoice.Enabled := DataSet.Active and (not DataSet.IsEmpty);
  MX18Print.Enabled := acPrintInvoice.Enabled;
end;

procedure TfmSList.acPrintInvoiceExecute(Sender: TObject);
begin
  dmInv.IsPrint:=true;
  dmInv.FCurrInvId := dmInv.taSListINVID.asinteger;
  if dmInv.taSListINVSUBTYPEID.AsInteger <> 9 then
    begin
      dmInv.IsPrint:=true;
      dmInv.FCurrInvId := dmInv.taSListINVID.asinteger;
      OpenDataSet(dmInv.taInvItems);
      try
        case dmInv.IType of
          2 : dmPrint.PrintDocumentA(dkSell_Invoice, VarArrayOf([VarArrayOf([dmInv.FcurrINVID]),VarArrayOf([dmInv.FcurrINVID])]));
        else raise EInternal.Create(Format(rsInternalError, ['220']));
        end;
      finally
        CloseDataSet(dmInv.taInvItems);
      end;
    end
  else
    with dmPrint do
      begin
        dmInv.taEmptyInv.Open;
        taInvHeader.Open;
        taInvToPrint.Open;
        FDocId := 105;
        taDoc.Open;
        frReport.OnGetValue := dmPrint.EmptyInvGetValue;
        frReport.LoadFromDB(taDoc, FDocId);
        if dm.IsAdm then
          frReport.DesignReport
        else
          frReport.ShowPreparedReport;
      end;
end;

procedure TfmSList.acPrintFactureExecute(Sender: TObject);
begin
  dmInv.IsPrint:=true;
  dmInv.FCurrInvId := dmInv.taSListINVID.asinteger;
  OpenDataSet(dmInv.taInvItems);
  try
    case dmInv.IType of
       2 : dmPrint.PrintDocumentA(dkSell_Facture, VarArrayOf([VarArrayOf([dmInv.FcurrINVID]),VarArrayOf([dmInv.FcurrINVID])]));
      else raise EInternal.Create(Format(rsInternalError, ['220']));
    end;
  finally
    CloseDataSet(dmInv.taInvItems);
  end;
end;

procedure TfmSList.acPrintChitExecute(Sender: TObject);
begin
  dmInv.IsPrint := True;
  dmInv.FCurrInvId := dmInv.taSListINVID.asinteger;
  OpenDataSet(dmInv.taInvItems);
  try
    case dmInv.IType of
       2 : dmPrint.PrintDocumentA(dkSell_Chit, VarArrayOf([VarArrayOf([dmInv.FcurrINVID]),VarArrayOf([dmInv.FcurrINVID])]));
      else raise EInternal.Create(Format(rsInternalError, ['220']));
    end;
  finally
    CloseDataSet(dmInv.taInvItems);
  end;
end;

procedure TfmSList.acCalcActWorkWExecute(Sender: TObject);
var
  b : Pointer;
  r : Variant;
begin
  b := dmInv.taSList.GetBookmark;
  dmInv.taSList.DisableControls;
  dmInv.taSList.DisableScrollEvents;
  try
    dmInv.taSList.First;
    while not dmInv.taSList.Eof do
    begin
      // ����� ��� ����������� �����
      r := ExecSelectSQL('select InvId from Inv where IType=21 and Ref='+dmInv.taSListINVID.AsString, dm.quTmp);
      if (not VarIsNull(r)) and (r<>0) then
      begin
        ExecSQL('update Inv set W2=(select sum(it.W) from ServItem si, SItem it where si.InvId='+VarToStr(r)+
          ' and si.SItemId=it.SItemId) where InvId='+dmInv.taSListINVID.AsString, dm.quTmp);
        dmInv.taSList.Refresh;
      end;
      dmInv.taSList.Next;
    end;
  finally
    dmInv.taSList.GotoBookmark(b);
    dmInv.taSList.EnableScrollEvents;
    dmInv.taSList.EnableControls;
  end;
end;

procedure TfmSList.acCalcActWorkWUpdate(Sender: TObject);
begin
  acCalcActWorkW.Visible := (dm.User.Profile = -1)or(dm.User.Profile = 3);
end;

procedure TfmSList.acLogicExecute(Sender: TObject);
begin
  //
end;

procedure TfmSList.acLogicUpdate(Sender: TObject);
begin
  tbsRetProd.Visible := dmInv.ITYPE = 3;
  tbsSellInv.Visible := dmInv.ITYPE = 3;
end;

procedure TfmSList.acCreateRetBuyerInvExecute(Sender: TObject);
var
  c : TCopyInvInfo;
begin
  c.DefaultComp := dmInv.taSListSUPID.AsInteger;
  c.CanChangeComp := False;
  c.DefaultDep := dmInv.taSListDEPID.AsInteger ;
  c.CanChangeDep := False;
  c.DepComp := False;
  c.BetweenOrg := False;
  c.CopyInvId := dmInv.taSListINVID.AsInteger;
  c.CopyTypeFrom := dmInv.ITYPE;
  c.CopyType := 3;
  c.FromDep := -1;
  if dmInv.taSListNDSID.IsNull then c.NDSId := -1
  else c.NDSId := dmInv.taSListNDSID.AsInteger;
  ShowInvCopy(c, True);
end;

procedure TfmSList.acCreateRetBuyerInvUpdate(Sender: TObject);
begin
  acCreateRetBuyerInv.Visible := (dmInv.ITYPE = 2);
  if acCreateRetBuyerInv.Visible then
    acCreateRetBuyerInv.Enabled := (dmInv.taSList.Active)and
      (not dmInv.taSList.IsEmpty)and(dmInv.taSListISCLOSE.AsInteger = 1);
end;

procedure TfmSList.acPrintInvUIDExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkInvUID, VarArrayOf([VarArrayOf([dmInv.taSListINVID.AsInteger]), VarArrayOf([dmInv.taSListINVID.AsInteger])]) );
end;

procedure TfmSList.acPrintInvUIDUpdate(Sender: TObject);
begin
  acPrintInvUID.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmSList.acEServParamsExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'ID'; sName: 'MATID'; sTable: 'ESERV_PARAMS'; sWhere: '');
begin
  ShowDictForm(TfmEServParams, dmInv.dsrEServParams, Rec, TAction(Sender).Caption, dmDictionary, nil);
end;

procedure TfmSList.acEServParamsUpdate(Sender: TObject);
begin
  acEServParams.Visible := dmInv.ITYPE in [2,3];
end;

procedure TfmSList.acPrintActWorkExecute(Sender: TObject);
begin
  PostDataSet(DataSet);
  dmPrint.PrintDocumentA(dkActWork_External, VarArrayOf([
      VarArrayOf([dmInv.taSListINVID.AsInteger, dm.User.SelfCompId]),
      VarArrayOf([dmInv.taSListINVID.AsInteger, dm.User.SelfCompId])
      ]));
end;

procedure TfmSList.acPrintActWorkUpdate(Sender: TObject);
begin
  acPrintActWork.Enabled := DataSet.Active and (not DataSet.IsEmpty) and
       (DataSet.FieldByName('SCHEMAID').AsInteger=2);
  TBItem28.Enabled := acPrintActWork.Enabled;
end;

procedure TfmSList.acPrintProdTolling1Execute(Sender: TObject);
begin
  dmInv.taSListCalcPrihodMode := 1;
  PostDataSet(DataSet);
  dmPrint.PrintDocumentA(dkProdTolling_External, VarArrayOf([
     VarArrayOf([dmInv.taSListINVID.AsInteger, dm.User.SelfCompId]),
     VarArrayOf([dmInv.taSListINVID.AsInteger, dmInv.taSListSUPID.AsInteger])
    ]));

end;

procedure TfmSList.acPrintProdTolling2Execute(Sender: TObject);
begin
  dmInv.taSListCalcPrihodMode := 2;
  PostDataSet(DataSet);
  dmPrint.PrintDocumentA(dkProdTolling_External,
    VarArrayOf([VarArrayOf([dmInv.taSListINVID.AsInteger, dm.User.SelfCompId]),
                VarArrayOf([dmInv.taSListINVID.AsInteger, dmInv.taSListSUPID.AsInteger])]));


end;

procedure TfmSList.acPrintProdTolling1Update(Sender: TObject);
begin
  acPrintProdTolling1.Enabled := DataSet.Active and (not DataSet.IsEmpty) and
       (DataSet.FieldByName('SCHEMAID').AsInteger=2);
end;

procedure TfmSList.acPrintProdTolling2Update(Sender: TObject);
begin
  acPrintProdTolling2.Enabled := DataSet.Active and (not DataSet.IsEmpty) and
       (DataSet.FieldByName('SCHEMAID').AsInteger = 2);
end;

end.
