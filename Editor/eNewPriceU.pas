unit eNewPriceU;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RxCurrEdit, ExtCtrls, Buttons, StdCtrls, Mask, RXCtrls,
  DateUtils, dbUtil, rxToolEdit;

type
  TfmNewPriceU = class(TForm)
    laDep: TRxLabel;
    RxLabel2: TRxLabel;
    RxLabel3: TRxLabel;
    EdDate: TDateEdit;
    SpeedButton1: TSpeedButton;
    Bevel1: TBevel;
    ceK: TRxCalcEdit;
    RxLabel1: TRxLabel;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmNewPriceU: TfmNewPriceU;

implementation

uses dPrice, InvData, DictData;

{$R *.dfm}

procedure TfmNewPriceU.FormCreate(Sender: TObject);
begin
 EdDate.Date:=Today;
 laDep.Caption:=fmPriceDict.laDep.Caption;
end;

procedure TfmNewPriceU.SpeedButton1Click(Sender: TObject);
begin
 If ceK.Value>0 then
 begin
  dmINV.sqlRepriceForU.ParamByName('DEPID').AsInteger:=dmINV.FCurrDep;
  dmINV.sqlRepriceForU.ParamByName('K').AsFloat:=ceK.Value;
  dmINV.sqlRepriceForU.ParamByName('NEW_DATE').AsDateTime:=EdDate.Date;
  dmINV.sqlRepriceForU.ParamByName('USERID').AsInteger:=dm.User.UserId;
  dmINV.sqlRepriceForU.ExecQuery;
  dmINV.sqlRepriceForU.Transaction.CommitRetaining;
  Close;
//  ReOpenDataSets([dmINV.taPriceDict]);
 end else ShowMessage('����������� ��������� ��� ������ ����>0');
end;

end.
