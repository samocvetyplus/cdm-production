unit InventoryCorrection;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, DB,
  cxDBData, cxGridLevel, cxClasses, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, dxBar, ImgList, ActnList,
  FIBDatabase, pFIBDatabase, FIBDataSet, pFIBDataSet, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport,
  cxDrawTextUtils, dxPSPrVwStd, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPSCore, dxPScxCommon, dxPScxGrid6Lnk;

type
  TfmInventoryCorrection = class(TForm)
    BarManager: TdxBarManager;
    Bar: TdxBar;
    ButtonAdd: TdxBarLargeButton;
    ButtonDelete: TdxBarLargeButton;
    ButtonExit: TdxBarLargeButton;
    View: TcxGridDBTableView;
    Level: TcxGridLevel;
    Grid: TcxGrid;
    Images: TImageList;
    Actions: TActionList;
    AddCorrection: TAction;
    DeleteCorrection: TAction;
    ExitForm: TAction;
    CorrectionDataSet: TpFIBDataSet;
    DataSource: TDataSource;
    Transaction: TpFIBTransaction;
    CorrectionDataSetID: TFIBIntegerField;
    CorrectionDataSetINVENTORYID: TFIBIntegerField;
    CorrectionDataSetCORRECTIONDATE: TFIBDateTimeField;
    CorrectionDataSetMATERIALID: TFIBIntegerField;
    CorrectionDataSetCOMMENT: TFIBStringField;
    CorrectionDataSetCORRECTIONWEIGHT: TFIBBCDField;
    CorrectionDataSetCORRECTEDBY: TFIBIntegerField;
    CorrectionDataSetPRILLID: TFIBIntegerField;
    CorrectionDataSetPUREWEIGHT: TFIBBCDField;
    CorrectionDataSetSTATE: TFIBSmallIntField;
    CorrectionDataSetMaterial: TStringField;
    CorrectionDataSetPrill: TStringField;
    CorrectionDataSetUser: TStringField;
    ViewCORRECTIONDATE: TcxGridDBColumn;
    ViewCOMMENT: TcxGridDBColumn;
    ViewCORRECTIONWEIGHT: TcxGridDBColumn;
    ViewPUREWEIGHT: TcxGridDBColumn;
    ViewMaterial: TcxGridDBColumn;
    ViewPrill: TcxGridDBColumn;
    ViewUser: TcxGridDBColumn;
    dxPrinter: TdxComponentPrinter;
    dxPrinterLink: TdxGridReportLink;
    dxBarLargeButton1: TdxBarLargeButton;
    acPrint: TAction;
    procedure CorrectionDataSetNewRecord(DataSet: TDataSet);
    procedure CorrectionDataSetAfterPost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure CorrectionDataSetBeforeOpen(DataSet: TDataSet);
    class procedure Execute(AReadOnly: boolean);
    procedure AddCorrectionExecute(Sender: TObject);
    procedure DeleteCorrectionExecute(Sender: TObject);
    procedure ExitFormExecute(Sender: TObject);
    procedure AddCorrectionUpdate(Sender: TObject);
    procedure DeleteCorrectionUpdate(Sender: TObject);
    procedure CorrectionDataSetBeforeClose(DataSet: TDataSet);
    procedure acPrintExecute(Sender: TObject);
  private
    FReadOnly : boolean;
  public
    property CorrectionReadOnly : boolean read FReadOnly write FReadOnly;
  end;

var
  fmInventoryCorrection: TfmInventoryCorrection;

implementation

{$R *.dfm}

uses DictData, MainData;

procedure TfmInventoryCorrection.acPrintExecute(Sender: TObject);
var
  Link: TdxGridReportLink;
begin
  Link := TdxGridReportLink(dxPrinter.LinkByName('dxPrinterLink'));

  Link.ReportTitle.Text := '�������� � ��������������';

  Link.ReportDocument.Caption := Link.ReportTitle.Text;

  Link.ReportDocument.CreationDate := Date;

  Link.ReportDocument.Creator := dm.User.FIO;

  Link.Preview;
end;

procedure TfmInventoryCorrection.AddCorrectionExecute(Sender: TObject);
begin

  if CorrectionDataSet.State in [dsEdit, dsInsert] then
  begin

    CorrectionDataSet.Post;

  end;

  CorrectionDataSet.Append;

  CorrectionDataSet.Post;

end;

procedure TfmInventoryCorrection.AddCorrectionUpdate(Sender: TObject);
begin

  AddCorrection.Enabled := CorrectionDataSet.Active and
                           (not FReadOnly) and
                           (dmMain.taVListISCLOSE.AsInteger = 1);

end;

procedure TfmInventoryCorrection.CorrectionDataSetAfterPost(DataSet: TDataSet);
begin

  CorrectionDataSet.Transaction.CommitRetaining;

end;

procedure TfmInventoryCorrection.CorrectionDataSetBeforeClose(
  DataSet: TDataSet);
begin

  if DataSet.State in [dsEdit, dsInsert] then
  begin

    DataSet.Post;

  end;

end;

procedure TfmInventoryCorrection.CorrectionDataSetBeforeOpen(DataSet: TDataSet);
begin

  CorrectionDataSet.ParamByName('Begin$Date').AsDateTime := dmMain.taVList.ParamByName('BD').AsDateTime;

  CorrectionDataSet.ParamByName('End$Date').AsDateTime := dmMain.taVList.ParamByName('ED').AsDateTime;

end;

procedure TfmInventoryCorrection.CorrectionDataSetNewRecord(DataSet: TDataSet);
begin

  DataSet.FieldByName('ID').AsInteger := dm.db.Gen_Id('G$INVENTORY$CORRECTION_ID', 1);

  DataSet.FieldByName('Inventory$ID').AsInteger := dmMain.taVListID.AsInteger;

end;

procedure TfmInventoryCorrection.DeleteCorrectionExecute(Sender: TObject);
begin

  CorrectionDataSet.Edit;

  CorrectionDataSetSTATE.AsInteger := 0;

  CorrectionDataSet.Post;

  CorrectionDataSet.Delete;

end;

procedure TfmInventoryCorrection.DeleteCorrectionUpdate(Sender: TObject);
begin

  DeleteCorrection.Enabled := CorrectionDataSet.Active and not (CorrectionDataSet.IsEmpty or FReadOnly);

end;


procedure TfmInventoryCorrection.FormCreate(Sender: TObject);
begin

  CorrectionDataSet.Active := true;

end;

class procedure TfmInventoryCorrection.Execute(AReadOnly: boolean);
begin

  fmInventoryCorrection := nil;

  try

    fmInventoryCorrection := TfmInventoryCorrection.Create(Application);

    fmInventoryCorrection.CorrectionReadOnly := AReadOnly;

    fmInventoryCorrection.ShowModal;

  finally

    FreeAndNil(fmInventoryCorrection);

  end;

end;

procedure TfmInventoryCorrection.ExitFormExecute(Sender: TObject);
begin

  if CorrectionDataSet.State in [dsEdit, dsInsert] then
  begin

    CorrectionDataSet.Post;

  end;

  CorrectionDataSet.Close;

  if Transaction.Active then
  begin

    Transaction.Commit;

  end;

  Close;

end;

end.
