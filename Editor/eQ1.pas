unit eQ1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh;

type
  TfmeQ1 = class(TfmEditor)
    Label1: TLabel;
    edQ: TDBNumberEditEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmeQ1: TfmeQ1;

implementation

{$R *.dfm}

procedure TfmeQ1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk : begin
      vResult := edQ.Value;
    end
  end;
  inherited;
end;

end.
