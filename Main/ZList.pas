{***********************************************}
{  ������ ������� �� ��������                   }
{  Copyrigth (C) 2001-2003 basile for CDM       }
{  v1.02                                        }
{***********************************************}

unit ZList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, Menus,  ImgList, StdCtrls,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, ComCtrls, db, DBGridEh, ActnList,
  TB2Item, TB2Dock, TB2Toolbar, Mask, DBCtrlsEh,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmZList = class(TfmListAncestor)
    Label1: TLabel;
    edYear: TDBNumberEditEh;
    procedure FormCreate(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure gridDocListOrderBy(Sender: TObject; OrderBy: String);
    procedure edYearKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure edYearChange(Sender: TObject);
  private
    procedure Access;
  end;

var
  fmZList: TfmZList;

implementation

uses MainData, DictData, Zp, dbUtil, fmUtils, DocAncestor, DbUtilsEh, pFIbDataSet;

{$R *.dfm}

procedure TfmZList.FormCreate(Sender: TObject);
var
  i, j : integer;
  Item : TMenuItem;
begin
  Access;
  dm.CurrDep := -1;
  inherited;
  edYear.OnChange := nil;
  edYear.Value := dmMain.DocListYear;
  edYear.OnChange := edYearChange;
end;

procedure TfmZList.acAddDocExecute(Sender: TObject);
begin
  DataSet.Append;
  DataSet.Post;
  DataSet.Transaction.CommitRetaining;
  Application.ProcessMessages;
  Screen.Cursor := crSQLWait;
  with dmMain do
  try
    ExecSQL('execute procedure G_Norma("'+DateToStr(dm.zBeginDate)+'", "'+DateTimeToStr(dm.zEndDate)+'")', quTmp);
    Application.ProcessMessages;
    ExecSQL('execute procedure Create_Zp('+taZListID.AsString+')', quTmp);
    Application.ProcessMessages;
    ExecSQL('execute procedure Build_Zp_Assembly('+taZListID.AsString+')', quTmp);
  finally
    Screen.Cursor := crDefault;
  end;
  ShowDocForm(TfmZp, dmMain.taZList, dmMain.taZListID.AsInteger, dmMain.quTmp);
  RefreshDataSet(dmMain.taZList);
end;

procedure TfmZList.acEditDocExecute(Sender: TObject);
begin
  with dmMain do
    ShowDocForm(TfmZp, taZList, taZListID.AsInteger, dmMain.quTmp);
end;

procedure TfmZList.gridDocListOrderBy(Sender: TObject; OrderBy: String);
begin
  OrderBy := StringReplace(OrderBy, 'ED', 'DOCDATE', [rfReplaceAll, rfIgnoreCase]);
  with (DataSet as TpFIbDataSet) do
    SelectSQL.Text := copy(SelectSQL.Text, 1, Pos('ORDER BY', SelectSQL.Text)-1)+OrderBy;
end;

procedure TfmZList.edYearKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : begin
      dmMain.DocListYear := edYear.Value;
      ReOpenDataSet(DataSet);
    end;
  end;
end;

procedure TfmZList.edYearChange(Sender: TObject);
begin
  dmMain.DocListYear := edYear.Value;
  ReOpenDataSet(DataSet);
end;

procedure TfmZList.Access;
begin
  with stbrStatus.Panels[0] do
    if dm.IsAdm then Text := 'Adm'
    else if((dm.User.AccProd_d and 4) = 4)then
    begin
     Text := 'View';
     FReadOnly := True;
    end
    else Text := 'Full';
  if dm.IsAdm then eXit;
end;

end.
