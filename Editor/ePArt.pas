{**************************************************}
{  �������� ���������� ������ �������� � ������    }
{  Copyrigth (C) 2002-2003 basile for CDM          }
{  created 05.10.2002                              }
{  last updated 08.09.2003                         }
{  v1.02                                           }
{**************************************************}
unit ePArt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DbEditor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, RxDBComb,
  DBCtrlsEh, DBLookupEh, DBGridEh;

type
  TfmePArt = class(TfmDbEditor)
    lbQ: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    lbOper: TLabel;
    lbU: TLabel;
    edQ: TDBNumberEditEh;
    cmbxU: TDBComboBoxEh;
    lcbxSz: TDBLookupComboboxEh;
    lcbxOper: TDBLookupComboboxEh;
    edArt: TDBEditEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FDictOperOpen : boolean;
  end;

var
  fmePArt: TfmePArt;

implementation

uses ApplData, DictData, dbUtil, ADistr, ProductionConsts;

{$R *.dfm}

procedure TfmePArt.FormCreate(Sender: TObject);
begin
  dm.InitArtParams;
  FDictOperOpen := dm.taOper.Active;
  ReOpenDataSets([dm.taSz, dm.taOper]);
  ActiveControl := edArt;
  cmbxU.ItemIndex := 0;
  if dmAppl.FilterOperId <> ROOT_OPER_CODE then lcbxOper.Value := dmAppl.FilterOperId;  
end;

procedure TfmePArt.FormClose(Sender: TObject; var Action: TCloseAction);

  procedure HandleClose(E: Exception);
  begin
    ShowMessage(E.Message);
    ModalResult := 0;
    ActiveControl := FindNextControl(ActiveControl, True, True, False);
    SysUtils.Abort;
  end;

var
  ArtId, Art2Id, Q, SzId, U, OperId : Variant;
begin
  case ModalResult of
    mrOk : begin
        if(ActiveControl = btnOk)then ModalResult := mrOk
        else if(ActiveControl = btnCancel)then ModalResult := mrCancel
        else begin
          ActiveControl := FindNextControl(ActiveControl, True, True, False);
          beep;
          SysUtils.Abort;
        end;
        // ��������� �������
        if not dm.CheckValidArt(edArt.Text)then raise Exception.Create(rsInvalidArtFormat);
        ArtId := ExecSelectSQL('select D_ArtId from D_Art where Art='#39+edArt.Text+#39, dmAppl.quTmp);
        if VarIsNull(ArtId)or(ArtId = 0)then raise Exception.Create(Format(rsArtNotFound, [edArt.Text, ' � �����������!']));
        //ArtId dm.InsertArt(Sender.AsString)

        if(StrToIntDef(edQ.Text, 0) = 0)then
        begin
          ActiveControl := edQ;
          SysUtils.Abort;
        end
        else Q := StrToInt(edQ.Text);
        if(0 >= Q)then
        begin
          ActiveControl := edQ;
          SysUtils.Abort;
        end;
        if VarIsNull(lcbxSz.KeyValue)then
        begin
          ActiveControl := lcbxSz;
          SysUtils.Abort;
        end
        else SzId := lcbxSz.KeyValue;

        if VarIsNull(lcbxOper.KeyValue) then
        begin
          ActiveControl := lcbxOper;
          SysUtils.Abort;
        end
        else OperId := lcbxOper.KeyValue;

        U := cmbxU.KeyItems[cmbxU.ItemIndex];

        // �������� ����� �������
        Art2Id := ExecSelectSQL('select Art2Id from Art2 where '+
                       'Art2="-" and D_ArtId='+VarToStr(ArtId), dmAppl.quTmp);
        if VarIsNull(Art2Id) then
          raise EInternal.Create(Format(rsInternalError, ['001']));
        if DataSet.Locate('ART2ID;SZID;OPERID;U', VarArrayOf([Art2Id, SzId, OperId, U]), [])then
        begin
          DataSet.Edit;
          DataSet.FieldByName('Q').AsInteger := DataSet.FieldByName('Q').AsInteger + Q;
          DataSet.Post;
        end
        else begin
          DataSet.Insert;
          DataSet.FieldByName('U').AsInteger := U;
          DataSet.FieldByName('OPERID').AsString := VarToStr(OperId);
          DataSet.FieldByName('SZID').AsInteger := SzId;
          DataSet.FieldByName('ARTID').AsInteger := ArtId;
          DataSet.FieldByName('ART2ID').AsInteger := Art2Id;
          DataSet.FieldByName('Q').AsInteger := Q;
          case dmAppl.ItType of
            0 : DataSet.FieldByName('COMMENTID').AsInteger := 3;
            1 : DataSet.FieldByName('COMMENTID').AsInteger := 5;
          end;
          DataSet.Post;
        end;
    end;
  end;
  try
    inherited;
  except
    on E:Exception do HandleClose(E);
  end;
  if not FDictOperOpen then CloseDataSet(dm.taOper);
  CloseDataSets([dm.taSz]);
end;

end.

