inherited fmDiscount: TfmDiscount
  Caption = ''
  PixelsPerInch = 96
  TextHeight = 13
  inherited plDict: TPanel
    inherited gridDict: TDBGridEh
      DataSource = dm.dsrDiscount
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Width = 33
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NAME'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1089#1082#1080#1076#1082#1080'|'#1055#1086#1083#1085#1086#1077
          Width = 94
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SNAME'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1089#1082#1080#1076#1082#1080'|'#1050#1088#1072#1090#1082#1086#1077
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'DISCOUNT'
          Footers = <>
        end>
    end
  end
  inherited fmstr: TFormStorage
    Left = 104
    Top = 164
  end
end
