{*************************************************}
{  �������� ���� � ���������� �������������       }
{  ��� ���������� �����������                     }
{  Copyrigth (C) 2003 basile for CDM              }
{  ������������ � ������ ����������� �����������, }
{  ������ � �������� ����, �����                  }
{  v0.15                                          }
{*************************************************}

unit eDSemis;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DbEditor, StdCtrls, Buttons, ExtCtrls, RXSpin, DBCtrlsEh, Mask,
  Editor;

type
  TfmDSemis = class(TfmEditor)
    lbQ: TLabel;
    lbW: TLabel;
    lbOper: TLabel;
    cmbxOper: TDBComboBoxEh;
    edW: TDBNumberEditEh;
    edQ: TDBNumberEditEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  end;

var
  fmDSemis: TfmDSemis;
  Operation : string;
  CanChangeOperation: boolean;
  Good : boolean;

function ShowDSemisEditor(AOperation : string; AGood : boolean) : word;

implementation

uses DictData, MainData, dbTree, UtilLib, ProductionConsts;

{$R *.dfm}

function ShowDSemisEditor(AOperation : string; AGood : boolean) : word;
begin
  Operation := AOperation;
  Good := AGood;
  fmDSemis := TfmDSemis.Create(Application);
  try
    Result := fmDSemis.ShowModal;
  finally
    FreeAndNil(fmDSemis);
  end;
end;

procedure TfmDSemis.FormCreate(Sender: TObject);
var
  i : integer;
begin
  inherited;
  edQ.Value := dmMain.SemisQ;
  edW.Value := dmMain.SemisW;
  cmbxOper.Enabled := CanChangeOperation;
  cmbxOper.Clear;
  if Good then
  begin
    cmbxOper.Items.Assign(dm.dOper);
    cmbxOper.Items.Delete(0);
    ActiveControl := edW;
  end
  else begin
    cmbxOper.Items.Assign(dm.dOperNoGood);
    cmbxOper.Items.Delete(0);
    cmbxOper.Items.Insert(0, sNoOperation);
    if(dmMain.SemisQ <> 0) then ActiveControl := edQ else ActiveControl := edW;
  end;

  i := cmbxOper.Items.IndexOf(Operation);
  if(i = -1)then
    if(dm.WorkMode = 'aDIEl')then raise EInternal.Create(Format(rsInternalError, ['501']))
    else cmbxOper.ItemIndex := -1
  else cmbxOper.ItemIndex := i;
end;

procedure TfmDSemis.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOK : begin
      if(ActiveControl = edQ)then
      begin
        ActiveControl := edW;
        SysUtils.Abort;
      end
      else if (ActiveControl = edW)then
      begin
        ActiveControl := cmbxOper;
        SysUtils.Abort;
      end;

      with cmbxOper, Items do
      begin
        if(IndexOf(cmbxOper.Text)=-1)then raise EWarning.Create('�������� ��������');
        if(Items[ItemIndex] = sNoOperation)then
        begin
          dmMain.DOperId := sNoOperation;
          dmMain.DOperName := sNoOperation;
        end
        else begin
          dmMain.DOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
          dmMain.DOperName := cmbxOper.Text;
        end;
        dmMain.SemisQ := Trunc(ConvertStrToFloat(edQ.Text));
        dmMain.SemisW := ConvertStrToFloat(edW.Text);
      end;
    end;
  end;
  inherited;
end;

procedure TfmDSemis.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_F5 : begin
      cmbxOper.Clear;
      cmbxOper.Items.Assign(dm.dOper);
      cmbxOper.Items.Delete(0);
      cmbxOper.Items.Insert(0, sNoOperation);
      cmbxOper.ItemIndex := cmbxOper.Items.IndexOf(Operation);
    end;
  end;
end;

initialization
  CanChangeOperation := True;

end.
