unit PatternsData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, FIBDatabase, pFIBDatabase, FIBDataSet, pFIBDataSet, DB, DBClient,
  XLSReadWriteII4, StdCtrls, Grids, DBGrids, Provider, ExtCtrls, ComCtrls;

const
  SheetGold = 0;
  SheetSilver = 1;
  MaxEmptyRowsCount = 3;

type

  TSimpleCell = record
    Row: integer;
    Col: integer;
  end;

  TPaymentType = record
    Name: String;
    Exist: Boolean;
    FirstCell: TSimpleCell;
    FieldName: String;    
  end;

  TPatternsImport = class(TDataModule)
    OD: TOpenDialog;
    XLS: TXLSReadWriteII4;
    ds_main: TpFIBDataSet;
    ds_technology: TpFIBDataSet;
    ds_technologyARTICLEID: TFIBIntegerField;
    ds_technologyART: TFIBStringField;
    ds_technologyPRICEPOSITION: TFIBSmallIntField;
    ds_technologyMIDDLEWEIGHT: TFIBFloatField;
    CDS: TClientDataSet;
    Patterns: TpFIBDataSet;
    PatternsID: TFIBIntegerField;
    PatternsNAME: TFIBStringField;
    PatternsPATTERNORDER: TFIBIntegerField;
    PatternsBITPARAMS: TFIBIntegerField;
    PatternsCOMPANYID: TFIBIntegerField;
    PatternsFIELDNAME: TFIBStringField;
    TR: TpFIBTransaction;
    DB: TpFIBDatabase;
    Settings: TpFIBDataSet;
    SettingsCOMPANYID: TFIBIntegerField;
    SettingsFILENAME: TFIBStringField;
    SettingsDECIMALDIGITS: TFIBIntegerField;
    SettingsOUTPUTDIRECTORY: TFIBStringField;
    SettingsBITPARAMS: TFIBIntegerField;
    SettingsMAXSKIPCELLSCOUNT: TFIBSmallIntField;
    procedure ds_technologyBeforeOpen(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  private
    CurrentSheet: SmallInt;
    LastColumn, LastRow: Integer;
    CellOrderStarts: TSimpleCell;
    CellPriceColumnStarts: TSimpleCell;
    TypesCount: Integer;
    CompanyID: Integer;
    PatternArray: array of TPaymentType;
    procedure xlsParsing(FileName: string);
    procedure GetCellWhichPriceOrderStarts;
    procedure LoadSettings;
    procedure PrepareData;
    procedure CreatePatterns(Source: TClientDataSet);
    procedure LoadPriceList;
    function PriceRound(Value: Double) : Double;
  public
    class procedure Import(MaterialID: Integer = 1);
  end;

var
  PatternsImport: TPatternsImport;

implementation

uses UtilLib, uUtils;

{$R *.dfm}

class procedure TPatternsImport.Import(MaterialID: Integer = 1);
begin
  PatternsImport := nil;
  Try

    Try
      PatternsImport := TPatternsImport.Create(Application);
    Except

    End;

    case MaterialID of
      1: PatternsImport.CurrentSheet := SheetGold;
      3: PatternsImport.CurrentSheet := SheetSilver;
    end;

    PatternsImport.LoadPriceList;

    PatternsImport.CreatePatterns(PatternsImport.CDS);
  Finally
    FreeAndNil(PatternsImport);
  End;
end;

function TPatternsImport.PriceRound(Value: Double) : Double;
var
  FracValue: Double;
begin

  FracValue := Frac(Value);

  if FracValue >= 0.5 then
  begin
    Result := Trunc(Value) + 1;
  end else
  begin
    Result := Trunc(Value);
  end;

end;

procedure TPatternsImport.xlsParsing(FileName: string);

procedure AddPricePosition(Number: Integer; FieldsList: Array of String; PriceList: array of Currency);
var
  i: integer;
begin
  with cds do
    begin

      Filter := 'PricePosition = ' + intToStr(number);
      Filtered := true;

      while not Eof do
        begin

          Edit;

          for i := 0 to TypesCount - 1 do
          begin
            cds.FieldByName(FieldsList[i]).AsCurrency := PriceRound(PriceList[i]);
          end;

          Post;

          Next;

        end;

      Filtered := false;
      Filter := '';
    end;
end;

var
  i, j, n: integer;
  Position: Integer;
  Value: Extended;
  Prices: array of Currency;
  FieldNames: array of String;
begin

  n := 0;

  XLS.Filename := FileName;
  XLS.Read;

  cds.DisableControls;

  LastColumn := XLS.Sheet[CurrentSheet].LastCol;
  LastRow := XLS.Sheet[CurrentSheet].LastRow;

  GetCellWhichPriceOrderStarts;

  SetLength(Prices, TypesCount);
  SetLength(FieldNames, TypesCount);

  for i := CellOrderStarts.Row to LastRow  do
  begin

    if tryStrToInt(XLS.Sheet[CurrentSheet].AsString[CellOrderStarts.Col, i], Position) then
    begin

      for j := 0 to TypesCount - 1 do
      begin
        if tryStrToFloat(xls.Sheet[CurrentSheet].AsString[PatternArray[j].FirstCell.Col, i], Value) then
        begin
          Prices[j] := Value;
          FieldNames[j] := PatternArray[j].FieldName;
        end;
      end;

      if (Position > 0) and (Prices[0] > 0) then
      begin
        AddPricePosition(Position, FieldNames, Prices);
        if n > 0 then n := 0;
      end else
      begin
        inc(n);
      end;

    end else
    begin
      inc(n);
    end;

    if n = MaxEmptyRowsCount then // �� ��������� ������ ��� ����� ��� ����
    begin
      break;
    end;

  end;

  cds.EnableControls;

end;

procedure TPatternsImport.LoadSettings;
var
  i: Integer;
  PatternsCount: Integer;
  Pattern : TPaymentType;
begin

  Settings.Active := true;

  Patterns.Active := true;

  Patterns.FetchAll;

  PatternsCount := Patterns.RecordCount;

  if PatternsCount > 0 then
  begin
    SetLength(PatternArray, PatternsCount);
  end else
  begin
    raise Exception.Create('���������� ��������� ������ �������� �� ����');
  end;

  i := 0;

  while not Patterns.Eof do
  begin
    Pattern.Name := PatternsNAME.AsString;
    Pattern.Exist := false;
    Pattern.FieldName := PatternsFIELDNAME.AsString;

    PatternArray[i] := Pattern;

    Patterns.Next;

    inc(i);
  end;

  TypesCount := PatternsCount;

end;


procedure TPatternsImport.PrepareData;
var
  i: Integer;
begin

  ds_technology.open;

  with CDS, FieldDefs do
  begin
    Add('ArticleID', ftInteger);
    Add('Article', ftString, 32);
    Add('PricePosition', ftInteger);
    Add('MiddleWeight', ftFloat);

    for i := low(PatternArray) to high(PatternArray) do
    begin
      Add(PatternArray[i].FieldName, ftCurrency);
    end;

    IndexFieldNames := 'PricePosition; ArticleID;';

    CreateDataSet;
  end;

  ds_technology.First;

  while not ds_technology.EoF do
  begin
    cds.Append;

    for i := 0 to ds_technology.FieldCount - 1 do
    begin
      cds.Fields[i].AsVariant := ds_technology.Fields[i].AsVariant;
    end;

    cds.Post;

    ds_technology.Next;
  end;

  ds_technology.Close;

end;

procedure TPatternsImport.DataModuleCreate(Sender: TObject);
begin

  CompanyID := DB.QueryValue('Select SelfCompID from GetSelfCompID(current_connection, 1)', 0);

  if CompanyID <= 0 then
  begin
    raise Exception.Create('�� ������� ���������� ������� �����������!');
  end;

  LoadSettings;

  PrepareData;

end;

procedure TPatternsImport.DataModuleDestroy(Sender: TObject);
begin
  ds_main.Active := false;
  ds_technology.Active := false;
  Patterns.Active := false;
  Settings.Active := false;
  cds.Active := false;
  tr.Commit;
  db.Connected := false;
end;

procedure TPatternsImport.ds_technologyBeforeOpen(DataSet: TDataSet);
begin

  if CurrentSheet = SheetGold then
  begin
    ds_technology.ParamByName('Material$ID').AsString := '1';
  end else
  if CurrentSheet = SheetSilver then
  begin
    ds_technology.ParamByName('Material$ID').AsString := '3';
  end else
  begin
    SysUtils.Abort;
  end;

end;

procedure TPatternsImport.CreatePatterns(Source: TClientDataSet);
var
  i: integer;
  FieldName: String;
  FileName: String;
  Price: String;
  PriceField: TCurrencyField;
  Target: TClientDataSet;
begin

  if (TypesCount > 0) and (Source.RecordCount > 0) then
  begin
    Target := TClientDataSet.Create(nil);

    with Target, FieldDefs do
    begin
      Add('Order', ftInteger);
      Add('Model', ftString, 32);
      Add('Formula', ftString, 32);

      IndexFieldNames := 'Order;';

      CreateDataSet;
    end;
  end else
  begin
    Exit;
  end;

 Source.DisableControls;

  for i := 0 to TypesCount - 1 do
  begin
    if PatternArray[i].FieldName <> '' then
    begin

      FieldName := trim(PatternArray[i].FieldName);

      FileName := trim(SettingsOUTPUTDIRECTORY.AsString) + '\' + trim(PatternArray[i].Name) + '.xml';

      if Source.FieldByName(FieldName).DataType = ftCurrency then
      begin

        PriceField := TCurrencyField(Source.FieldByName(FieldName));

        Source.First;

        Target.Active := true;
        Target.LogChanges := false;

        while not Source.Eof do
        begin

          if PriceField.AsFloat > 0 then
          begin
            Price := trim(PriceField.AsString);
          end else
          begin
            Price := '0';
          end;

          Target.Append;
          Target['Order'] := Source.RecNo;
          Target['Model'] := Source['Article'];
          Target['Formula'] := Price;
          Target.Post;

          Source.Next;

        end;

        Target.SaveToFile(FileName, dfXML);

        Target.EmptyDataSet;

        Target.Close;

      end;
    end;
  end;

  FreeAndNil(Target);

  Source.First;

  Source.EnableControls;

end;

procedure TPatternsImport.GetCellWhichPriceOrderStarts;
var
  i, j: Integer;
  TempString: String;
  Cell: TSimpleCell;
  Row: Integer;
  Value: Integer;
  DefColumn, DefRow: Integer;
begin

  for defRow := 0 to LastRow do
  begin

    for defColumn := 0 to LastColumn do
    begin

      tempString := xls.Sheet[CurrentSheet].AsString[defColumn, defRow];

      if tempString = PatternArray[0].Name  then
      begin

        Row := defRow + 1;

        while not tryStrToInt(xls.Sheet[CurrentSheet].AsString[defColumn, Row], value) do
        begin
          inc(Row);
        end;

        if xls.Sheet[CurrentSheet].AsInteger[LastColumn, Row] = 1 then
        begin
          if xls.Sheet[CurrentSheet].AsInteger[LastColumn, Row + 1] = 2 then
          begin

            CellOrderStarts.Row := Row;
            CellOrderStarts.Col := LastColumn;

            for j := defColumn to LastColumn do
            begin

              tempString := trim(xls.Sheet[CurrentSheet].AsString[j, defRow]);

              for i:= low(PatternArray) to High(PatternArray) do
              begin

                if PatternArray[i].Name = tempString then
                begin
                  PatternArray[i].Exist := true;
                  Cell.Row := Row;
                  Cell.Col := j;
                  PatternArray[i].FirstCell := Cell;
                  break;
                end;

              end;

            end;

          end;
        end;

        break;

      end;

    end;

  end;

end;

procedure TPatternsImport.LoadPriceList;
var
  FileName: String;
begin

  FileName := trim(SettingsFILENAME.AsString);

  if not FileExists(FileName) then
  begin

    OD.Execute();

    FileName := OD.FileName;

  end;

  xlsParsing(FileName);

end;


end.
