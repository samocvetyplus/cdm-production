inherited fmExecAppl: TfmExecAppl
  Left = 191
  Top = 219
  Width = 529
  Caption = #1054#1090#1095#1077#1090' '#1086' '#1074#1099#1087#1086#1083#1085#1077#1085#1080#1080' '#1079#1072#1074#1082#1080
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 521
  end
  inherited tb1: TSpeedBar
    Width = 521
    inherited spitExit: TSpeedItem
      Left = 427
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Hint = #1055#1077#1095#1072#1090#1100'|'
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 42
    Width = 521
    Height = 200
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object gridExecAppl: TDBGridEh
      Left = 0
      Top = 0
      Width = 521
      Height = 200
      Align = alClient
      ColumnDefValues.Title.TitleButton = True
      DataGrouping.GroupLevels = <>
      DataSource = dmAppl.dsrExecAppl
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'MS Sans Serif'
      FooterFont.Style = []
      OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
      RowDetailPanel.Color = clBtnFace
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      UseMultiTitle = True
      OnGetCellParams = gridExecApplGetCellParams
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'ART'
          Footers = <>
          Width = 61
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SZNAME'
          Footers = <>
          Width = 44
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  inherited ilButtons: TImageList
    Left = 178
    Top = 108
  end
  object quDDL: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    ParamCheck = False
    Left = 40
    Top = 116
  end
  object PrintExecAppl: TPrintDBGridEh
    DBGridEh = gridExecAppl
    Options = [pghFitGridToPageWidth]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 352
    Top = 100
  end
end
