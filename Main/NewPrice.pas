unit NewPrice;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, RXCtrls, StdCtrls, Mask, RxToolEdit, RxCurrEdit, //bsUtils,
  dbutil, DateUtils;

type
  TfmNewPrice = class(TForm)
    cePrice: TCurrencyEdit;
    RxLabel1: TRxLabel;
    SpeedButton1: TSpeedButton;
    Bevel1: TBevel;
    RxLabel2: TRxLabel;
    laDep: TRxLabel;
    RxLabel3: TRxLabel;
    EdDate: TDateEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmNewPrice: TfmNewPrice;

implementation

uses dPrice, InvData, DictData;

{$R *.dfm}

procedure TfmNewPrice.SpeedButton1Click(Sender: TObject);
begin
 If cePrice.Value>0 then
 begin
  dmINV.sqlACTPrice.ParamByName('DEPID').AsInteger:=dmINV.FCurrDep;
  dmINV.sqlACTPrice.ParamByName('NEW_PRICE').AsFloat:=cePrice.Value;
  dmINV.sqlACTPrice.ParamByName('NEW_DATE').AsDateTime:=EdDate.Date;
  dmINV.sqlACTPrice.ParamByName('USERID').AsInteger:=dm.User.UserId;
  dmINV.sqlACTPrice.ExecQuery;
  dmINV.sqlACTPrice.Transaction.CommitRetaining;
  Close;
//  ReOpenDataSets([dmINV.taPriceDict]);
 end else ShowMessage('���� ������ ���� ������ 0');
end;

procedure TfmNewPrice.FormCreate(Sender: TObject);
begin
 EdDate.Date:=Today;
 laDep.Caption:=fmPriceDict.laDep.Caption;
end;

end.
