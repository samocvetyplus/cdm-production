{**************************}
{  ������ ����������       }
{  Copyrigth (C) 2005      }
{  ��������� �������       }
{  v0.10                   }
{**************************}
unit MatBalance;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus, TB2Item, ActnList,  ImgList, Grids,
  DBGridEh, DBCtrlsEh, StdCtrls, Mask, DBCtrls, ExtCtrls, 
  ComCtrls, DB, FIBDataSet, pFIBDataSet, cxPC, cxControls,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters;

type
  TfmMatBalance = class(TfmDocAncestor)
    Label1: TLabel;
    cmbxMat: TDBComboBoxEh;
    spitRefresh: TSpeedItem;
    acRefresh: TAction;
    cxPageControl1: TcxPageControl;
    cxTabSheet: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    gridMov: TDBGridEh;
    taMov: TpFIBDataSet;
    dsrMov: TDataSource;
    taMovID: TFIBIntegerField;
    taMovINVID: TFIBIntegerField;
    taMovMATID: TFIBStringField;
    taMovW: TFIBFloatField;
    taMovDESCR: TFIBStringField;
    taMovMATNAME: TFIBStringField;
    taMovZNAK: TFIBSmallIntField;
    taMovZNAKNAME: TFIBStringField;
    TBItem2: TTBItem;
    acPrintMov: TAction;
    procedure acRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cmbxMatChange(Sender: TObject);
    procedure taMovCalcFields(DataSet: TDataSet);
    procedure taMovBeforeOpen(DataSet: TDataSet);
    procedure gridMovGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintMovExecute(Sender: TObject);
  end;

var
  fmMatBalance: TfmMatBalance;

implementation

uses Data, DictData, MainData, dbUtil, PrintData;

{$R *.dfm}

procedure TfmMatBalance.acRefreshExecute(Sender: TObject);
begin
  ExecSQL('execute procedure Create_MatBalance('+dmData.taMBListINVID.AsString+')', dm.quTmp);
  ReOpenDataSets([DataSet, taMov]);
end;

procedure TfmMatBalance.FormCreate(Sender: TObject);
begin
  edNoDoc.DataSource := dmData.dsrMBList;
  dtedDateDoc.DataSource := dmData.dsrMBList;
  gridItem.DataSource := dmData.dsrMatBalance;
  gridItem.Parent := cxTabSheet;
  FDocName := '������ �� ����������';
  // ������ �� ����������
  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMat);
  cmbxMat.ItemIndex := 0;
  dmMain.FilterMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := cmbxMatChange;
  inherited;
  InitBtn(Boolean(dmData.taMBListISCLOSE.AsInteger));
  OpenDataSet(taMov);
end;

procedure TfmMatBalance.cmbxMatChange(Sender: TObject);
begin
  if cmbxMat.ItemIndex = 0 then
  begin
    DataSet.Filtered := False;
    DataSet.Filter := '';
    taMov.Filtered := False;
    taMov.Filter := '';
  end
  else begin
    DataSet.DisableScrollEvents;
    DataSet.DisableControls;
    try
      DataSet.Filtered := False;
      DataSet.Filter := 'MATNAME='#39+cmbxMat.Text+#39;
      DataSet.Filtered := True;
    finally
      DataSet.EnableScrollEvents;
      DataSet.EnableControls;
    end;

    taMov.DisableControls;
    try
      taMov.Filtered := False;
      taMov.Filter := 'MATNAME='#39+cmbxMat.Text+#39;
      taMov.Filtered := True;
    finally
      taMov.EnableControls;
    end;
  end;
end;

procedure TfmMatBalance.taMovCalcFields(DataSet: TDataSet);
begin
  inherited;
  case taMovZNAK.AsInteger of
    -1: taMovZNAKNAME.AsString := '������';
     1: taMovZNAKNAME.AsString := '������';
  end;
end;

procedure TfmMatBalance.taMovBeforeOpen(DataSet: TDataSet);
begin
  taMov.ParamByName('INVID').AsInteger := dmData.taMBListINVID.AsInteger;
end;

procedure TfmMatBalance.gridMovGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  case taMovZNAK.AsInteger of
    -1 : Background := clGray;
     1 : Background := clWhite;
  end;
end;

procedure TfmMatBalance.acPrintMovExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkMatBalance_Move, VarArrayOf([VarArrayOf([dmData.taMBListINVID.AsInteger])]));
end;

end.
