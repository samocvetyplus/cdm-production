unit fmDialogTechnologyRecalc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxEdit, cxVGrid, cxDBVGrid,
  cxInplaceContainer, Menus, StdCtrls, cxButtons, DB, FIBDataSet, pFIBDataSet,
  cxCurrencyEdit, cxCalendar;

type
  TDialogTechnologyRecalc = class(TForm)
    Grid: TcxDBVerticalGrid;
    GridCategoryRow1: TcxCategoryRow;
    GridDBEditorRow1: TcxDBEditorRow;
    GridDBEditorRow2: TcxDBEditorRow;
    GridCategoryRow2: TcxCategoryRow;
    GridDBEditorRow3: TcxDBEditorRow;
    GridDBEditorRow4: TcxDBEditorRow;
    btnConfirm: TcxButton;
    btnCancel: TcxButton;
    GridCategoryRow3: TcxCategoryRow;
    GridDBEditorRow5: TcxDBEditorRow;
    GridDBEditorRow6: TcxDBEditorRow;
    procedure btnCancelClick(Sender: TObject);
    procedure btnConfirmClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public
    class function Execute : TModalResult;
  end;

var
  DialogTechnologyRecalc: TDialogTechnologyRecalc;

implementation

{$R *.dfm}
uses DictData, uDialogs, TechnologyGroups;

procedure TDialogTechnologyRecalc.btnCancelClick(Sender: TObject);
begin
  if fmTechnologyGroups.Consts.State = dsEdit then fmTechnologyGroups.Consts.Cancel;
end;

procedure TDialogTechnologyRecalc.btnConfirmClick(Sender: TObject);
begin
with fmTechnologyGroups do
  if Consts.State = dsEdit then
  begin
    Consts.Post;
    Consts.Transaction.CommitRetaining;
  end
  else
    if DialogQuestionYesNoMessage('����������� �� ������� ���������?') = mrNo then
      begin
        btnCancel.Click;
      end;
end;

class function TDialogTechnologyRecalc.Execute : TModalResult;
begin
  DialogTechnologyRecalc := nil;
  try
    DialogTechnologyRecalc := TDialogTechnologyRecalc.Create(Application);
    Result := DialogTechnologyRecalc.ShowModal;
  finally
    FreeAndNil(DialogTechnologyRecalc);
  end;
end;

procedure TDialogTechnologyRecalc.FormCreate(Sender: TObject);
begin
  fmTechnologyGroups.Consts.Active := true;
end;

end.
