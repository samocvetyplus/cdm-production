unit ArtImage;

interface                              

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, M207DbCtrls, RxPlacemnt, db, StdActns, ActnList, StdCtrls, Mask,
  Clipbrd, TB2Item, Menus, ImgList, rxToolEdit;

const
  JpegClipboardFormat = $1200;

type
  TfmImage = class(TForm)
    im: TM207DBImage;
    FormPlacement: TFormPlacement;
    fedArtImage: TFilenameEdit;
    acEvent: TActionList;
    EditPaste: TEditPaste;
    EditCopy: TEditCopy;
    ppImage: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acAddImage: TAction;
    TBItem3: TTBItem;
    imImage: TImageList;
    acClearImage: TAction;
    TBItem4: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    acSizeImage: TAction;
    TBItem5: TTBItem;
    procedure EditPasteExecute(Sender: TObject);
    procedure acAddImageExecute(Sender: TObject);
    procedure acClearImageExecute(Sender: TObject);
    procedure EditCopyExecute(Sender: TObject);
    procedure acSizeImageExecute(Sender: TObject);
  private
    FH, FW : TField;
  end;

  function ShowDbImage(DataSource : TDataSource; Field, H,W : TField;  FormCaption : string) : TfmImage;

implementation

{$R *.dfm}

uses DbUtil, jpeg;

function ShowDbImage(DataSource : TDataSource; Field, H,W : TField; FormCaption : string) : TfmImage;
begin
  Result := TfmImage.Create(nil);
  with Result do
  begin
    Caption := FormCaption;
    im.DataSource := DataSource;
    im.DataField := Field.FieldName;
    FH := H;
    FW := W; 
    Show;
  end;
end;

procedure TfmImage.EditPasteExecute(Sender: TObject);
begin
  if Clipboard.HasFormat(CF_BITMAP) then
  begin
    im.Picture.Bitmap.Assign(Clipboard);
    im.Refresh;
    SaveDbPicture(TBlobField(im.Field), im.Picture.Graphic, TJPEGImage);
  end;
end;

procedure TfmImage.EditCopyExecute(Sender: TObject);
var
  Format: word;
  data: cardinal;
  palette: hPalette;
  b : boolean;
begin
  if im.Picture.Graphic <> nil then Clipboard.Assign(im.Picture);
  b := im.DataSource.DataSet.State in [dsEdit, dsInsert];
  if not b then im.DataSource.DataSet.Edit;
  FH.AsInteger := im.Picture.Height;
  FW.AsInteger := im.Picture.Width;
  if not b then im.DataSource.DataSet.Post;
end;

procedure TfmImage.acAddImageExecute(Sender: TObject);
var
  b : boolean;
begin
  if fedArtImage.Dialog.Execute then
  begin
    fedArtImage.InitialDir := ExtractFileDir(fedArtImage.Dialog.FileName);
    im.Picture.LoadFromFile(fedArtImage.Dialog.FileName);
    im.Refresh;
    SaveDbPicture(TBlobField(im.Field), im.Picture.Graphic, TJPEGImage);
    b := im.DataSource.DataSet.State in [dsEdit, dsInsert];
    if not b then im.DataSource.DataSet.Edit;
    FH.AsInteger := im.Picture.Height;
    FW.AsInteger := im.Picture.Width;
    if not b then im.DataSource.DataSet.Post;
  end;
end;

procedure TfmImage.acClearImageExecute(Sender: TObject);
var
  b : boolean;
begin
  b := im.DataSource.DataSet.State in [dsEdit, dsInsert];
  if not b then im.DataSource.DataSet.Edit;
  im.Field.AsVariant := null;
  FH.AsInteger := 0;
  FW.AsInteger := 0;
  if not b then im.DataSource.DataSet.Post;
  im.Repaint;
end;

procedure TfmImage.acSizeImageExecute(Sender: TObject);
begin
  ShowMessage('������:'+IntToStr(im.Picture.Width)+' ������:'+IntToStr(im.Picture.Height));
end;

initialization
  TPicture.RegisterClipboardFormat(JpegClipboardFormat, TJpegImage);

end.
