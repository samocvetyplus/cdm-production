inherited fmSOList: TfmSOList
  Left = 199
  Top = 200
  Caption = #1057#1087#1080#1089#1086#1082' '#1085#1072#1082#1083#1072#1076#1085#1099#1093' '#1088#1072#1089#1093#1086#1076#1072' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
  ClientHeight = 370
  ClientWidth = 871
  ExplicitWidth = 879
  ExplicitHeight = 404
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 351
    Width = 871
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
    ExplicitTop = 351
    ExplicitWidth = 871
  end
  inherited tb2: TSpeedBar
    Width = 871
    TabOrder = 2
    ExplicitWidth = 871
    object lbComp: TLabel [2]
      Left = 552
      Top = 8
      Width = 59
      Height = 13
      Caption = #1055#1086#1083#1091#1095#1072#1090#1077#1083#1100
      Transparent = True
    end
    object cbComp: TDBComboBoxEh [3]
      Left = 616
      Top = 4
      Width = 121
      Height = 19
      DropDownBox.Rows = 15
      DropDownBox.Sizable = True
      DropDownBox.Width = -1
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 0
      Text = 'cbComp'
      Visible = True
      OnCloseUp = cbCompCloseUp
    end
  end
  inherited tb1: TSpeedBar
    Width = 871
    TabOrder = 3
    OnClick = spitExitClick
    ExplicitWidth = 871
    inherited spitEdit: TSpeedItem
      OnClick = acEditDocExecute
    end
    inherited spitPrint: TSpeedItem
      AllowAllUp = True
      Hint = #1055#1077#1095#1072#1090#1100
      Left = 203
      Visible = False
      OnClick = acPrintDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    Width = 871
    Height = 280
    DataSource = dmMain.dsrSOList
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    SumList.Active = True
    TabOrder = 0
    UseMultiTitle = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1053#1086#1084#1077#1088
        Width = 54
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1044#1072#1090#1072
        Width = 53
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Title.Caption = #1054#1090#1087#1088#1072#1074#1080#1090#1077#1083#1100
        Width = 99
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SUPNAME'
        Footers = <>
        Width = 100
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'CONTRACTNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 87
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Width = 100
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footer.FieldName = 'Q'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1050#1086#1083'-'#1074#1086
        Width = 56
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1042#1077#1089
        Width = 52
      end
      item
        BiDiMode = bdLeftToRight
        DisplayFormat = '#.##'
        EditButtons = <>
        FieldName = 'W3'
        Footer.FieldName = 'W3'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1063#1080#1089#1090#1099#1081' '#1074#1077#1089
        Width = 79
      end>
  end
  inherited ilButtons: TImageList
    Left = 418
    Top = 196
  end
  inherited fmstr: TFormStorage
    Left = 160
    Top = 200
  end
  inherited ppDep: TPopupMenu
    Left = 236
    Top = 188
  end
  inherited acAEvent: TActionList
    Left = 668
    Top = 220
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
    inherited acPrintDoc: TAction
      OnExecute = acPrintDocExecute
    end
    object acPrintAct: TAction
      Caption = #1040#1082#1090' '#1088#1072#1089#1093#1086#1076#1072
      OnExecute = acPrintActExecute
      OnUpdate = acPrintActUpdate
    end
    object acPrintInv15: TAction
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1085#1072' '#1086#1090#1087#1091#1089#1082' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074' '#1085#1072' '#1089#1090#1086#1088#1086#1085#1091
      OnExecute = acPrintInv15Execute
      OnUpdate = acPrintInv15Update
    end
    object acExport: TAction
      Caption = #1055#1086#1089#1090#1091#1087#1083#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074' '#1074
    end
    object acUnionInvSOEl: TAction
      Caption = #1054#1073#1098#1077#1076#1080#1085#1080#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1099#1077
      OnExecute = acUnionInvSOElExecute
    end
  end
  inherited ppDoc: TTBPopupMenu
    Left = 612
    Top = 180
    object tbsCreateSI: TTBSubmenuItem [3]
      Caption = #1055#1086#1089#1090#1091#1087#1083#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074' '#1074
    end
    object TBItem5: TTBItem
      Caption = #1055#1077#1095#1072#1090#1100' '#1089#1087#1080#1089#1082#1072' '#1085#1072#1082#1083#1072#1076#1085#1099#1093
      ImageIndex = 4
      OnClick = btnPrintClick
    end
    object TBItem7: TTBItem
      Caption = #1058#1086#1074#1072#1088
      OnClick = N1Click
    end
    object TBSeparatorItem2: TTBSeparatorItem
    end
    object TBItem6: TTBItem
      Action = acUnionInvSOEl
    end
  end
  object sdExport: TSaveDialog
    Filter = #1048#1084#1087#1086#1088#1090'/'#1069#1082#1089#1087#1086#1088#1090'(*.exp)|*.exp'
    Left = 732
    Top = 228
  end
  object PrintSOListGrid: TPrintDBGridEh
    DBGridEh = gridDocList
    Options = [pghFitGridToPageWidth]
    Page.BottomMargin = 1.000000000000000000
    Page.LeftMargin = 1.000000000000000000
    Page.RightMargin = 1.000000000000000000
    Page.TopMargin = 1.000000000000000000
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'Tahoma'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'Tahoma'
    PageHeader.Font.Style = []
    Units = MM
    Left = 328
    Top = 192
  end
end
