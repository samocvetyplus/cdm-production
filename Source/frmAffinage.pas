unit frmAffinage;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxStyles, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridBandedTableView,
  cxGridDBBandedTableView, cxClasses, cxGridLevel, cxGrid, ImgList,
  ExtCtrls, dxsbar, cxPC, dxStatusBar, ActnList, dxBar, FIBDataSet,
  pFIBDataSet, FIBDatabase, pFIBDatabase, cxLabel, cxContainer, cxGroupBox,
  cxTextEdit, cxDBEdit, dxBarExtItems;

type
  TDialogAffinage = class(TForm)
    StatusBar: TdxStatusBar;
    Pages: TcxPageControl;
    SheetPassports: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    ImageList: TImageList;
    GridPassportsLevel1: TcxGridLevel;
    GridPassports: TcxGrid;
    GridPassportsDBBandedTableView1: TcxGridDBBandedTableView;
    BarManager: TdxBarManager;
    BarButtonAppend: TdxBarButton;
    dxBarButton2: TdxBarButton;
    Actions: TActionList;
    ActionAppend: TAction;
    ActionDelete: TAction;
    ActionOpenClose: TAction;
    dxBarButton1: TdxBarButton;
    ActionView: TAction;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    BarDockControl: TdxBarDockControl;
    dxBarButton5: TdxBarButton;
    DataSourcePassportSet: TDataSource;
    SheetPassport: TcxTabSheet;
    dxBarButton6: TdxBarButton;
    ActionPeriod: TAction;
    dxBarStatic1: TdxBarStatic;
    dxBarDateCombo1: TdxBarDateCombo;
    dxBarStatic2: TdxBarStatic;
    dxBarDateCombo2: TdxBarDateCombo;
    dxBarStatic3: TdxBarStatic;
    procedure ActionAppendUpdate(Sender: TObject);
    procedure ActionDeleteUpdate(Sender: TObject);
    procedure ActionOpenCloseUpdate(Sender: TObject);
    procedure ActionCloseUpdate(Sender: TObject);
    procedure ActionViewUpdate(Sender: TObject);
    procedure ActionViewExecute(Sender: TObject);
    procedure ActionAppendExecute(Sender: TObject);
    procedure ActionDeleteExecute(Sender: TObject);
    procedure ActionOpenCloseExecute(Sender: TObject);
    procedure ActionCloseExecute(Sender: TObject);
  private

  public
    { Public declarations }
  end;

var
  DialogAffinage: TDialogAffinage;

implementation

uses dmAffiange, frmAffiangePassport, DictData;

{$R *.dfm}

procedure TDialogAffinage.ActionAppendUpdate(Sender: TObject);
begin
  ActionAppend.Enabled := True;
end;

procedure TDialogAffinage.ActionDeleteUpdate(Sender: TObject);
begin
  ActionDelete.Enabled := True;
end;

procedure TDialogAffinage.ActionOpenCloseUpdate(Sender: TObject);
begin
  ActionOpen.Enabled := True;
end;

procedure TDialogAffinage.ActionCloseUpdate(Sender: TObject);
begin
  ActionClose.Enabled := True;
end;

procedure TDialogAffinage.ActionViewUpdate(Sender: TObject);
begin
  ActionView.Enabled := True;
end;

procedure TDialogAffinage.ActionViewExecute(Sender: TObject);
begin
//
end;

procedure TDialogAffinage.ActionAppendExecute(Sender: TObject);
begin
//
end;

procedure TDialogAffinage.ActionDeleteExecute(Sender: TObject);
begin
//
end;

procedure TDialogAffinage.ActionOpenCloseExecute(Sender: TObject);
begin
//
end;

procedure TDialogAffinage.ActionCloseExecute(Sender: TObject);
begin
//
end;

end.
