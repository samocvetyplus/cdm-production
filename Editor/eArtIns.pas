unit eArtIns;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, DBUtil, DB, TB2Dock, TB2Toolbar,
  ActnList, TB2Item, DBGridEhGrouping, GridsEh;

type
  TfmArtIns = class(TForm)
    InsGrid: TDBGridEh;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    itDocHist: TTBItem;
    acEvent: TActionList;
    acDocMovingSemis: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure acDocMovingSemisExecute(Sender: TObject);
    procedure acDocMovingSemisUpdate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  end;

var
  fmArtIns: TfmArtIns;

implementation

uses InvData, dPrice, SInv_det, DocMovingSemis;

{$R *.dfm}

procedure TfmArtIns.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action:=caFree;
  if dminv.taArtIns.State in [dsEdit] then dminv.taArtIns.Post;
  CloseDataSet(dminv.taArtIns);
  fmArtIns:=nil;
end;

procedure TfmArtIns.FormCreate(Sender: TObject);
begin // ���� E �������
  if (Screen.ActiveForm=fmPriceDict)or((Screen.ActiveForm=fmSinv_det) and(dminv.taSListITYPE.Asinteger<>12)) then
  begin
    InsGrid.FieldColumns['E'].Visible:=false;
  end
  else   InsGrid.FieldColumns['E'].Visible:=true;
  ReOpenDataSet(dminv.taArtIns);
end;

procedure TfmArtIns.acDocMovingSemisExecute(Sender: TObject);
begin
  ShowDocMovingSemis(dmInv.taArtInsSEMISID.AsString);  
end;

procedure TfmArtIns.acDocMovingSemisUpdate(Sender: TObject);
begin
  acDocMovingSemis.Enabled := dmInv.taArtIns.Active and (not dmInv.taArtIns.IsEmpty);
end;

procedure TfmArtIns.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_ESCAPE : Close;
  end;
end;

end.
