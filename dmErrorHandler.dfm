object ErrorHandler: TErrorHandler
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 226
  Width = 167
  object FIBErrorHandler: TpFibErrorHandler
    OnFIBErrorEvent = OnError
    Options = [oeException, oeForeignKey, oeLostConnect, oeCheck, oeUniqueViolation]
    Left = 40
    Top = 24
  end
  object Transaction: TpFIBTransaction
    DefaultDatabase = dm.db
    TimeoutAction = TARollback
    Left = 40
    Top = 80
  end
  object DBErrors: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE ERRORS'
      'SET '
      '    ERROR$DATE = :ERROR$DATE,'
      '    ERROR$USER = :ERROR$USER,'
      '    ERROR$SOURCE = :ERROR$SOURCE,'
      '    ERROR$SQL$MESSAGE = :ERROR$SQL$MESSAGE,'
      '    ERROR$IB$MESSAGE = :ERROR$IB$MESSAGE,'
      '    ERROR$COMPONENT = :ERROR$COMPONENT,'
      '    ERROR$SQL = :ERROR$SQL'
      'WHERE'
      '    ERROR$ID = :OLD_ERROR$ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    ERRORS'
      'WHERE'
      '        ERROR$ID = :OLD_ERROR$ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO ERRORS('
      '    ERROR$ID,'
      '    ERROR$DATE,'
      '    ERROR$USER,'
      '    ERROR$SOURCE,'
      '    ERROR$SQL$MESSAGE,'
      '    ERROR$IB$MESSAGE,'
      '    ERROR$COMPONENT,'
      '    ERROR$SQL'
      ')'
      'VALUES('
      '    :ERROR$ID,'
      '    :ERROR$DATE,'
      '    :ERROR$USER,'
      '    :ERROR$SOURCE,'
      '    :ERROR$SQL$MESSAGE,'
      '    :ERROR$IB$MESSAGE,'
      '    :ERROR$COMPONENT,'
      '    :ERROR$SQL'
      ')')
    RefreshSQL.Strings = (
      'select * from'
      'errors where (ERRORS.ERROR$ID = :OLD_ERROR$ID)'
      '    ')
    SelectSQL.Strings = (
      'select * from'
      'errors where error$id = 0')
    Transaction = Transaction
    Database = dm.db
    Left = 40
    Top = 136
    oRefreshAfterPost = False
    oStartTransaction = False
    object DBErrorsERRORID: TFIBIntegerField
      FieldName = 'ERROR$ID'
    end
    object DBErrorsERRORDATE: TFIBDateTimeField
      FieldName = 'ERROR$DATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object DBErrorsERRORUSER: TFIBStringField
      FieldName = 'ERROR$USER'
      Size = 64
      EmptyStrToNull = True
    end
    object DBErrorsERRORSOURCE: TFIBStringField
      FieldName = 'ERROR$SOURCE'
      Size = 32
      EmptyStrToNull = True
    end
    object DBErrorsERRORSQLMESSAGE: TFIBStringField
      FieldName = 'ERROR$SQL$MESSAGE'
      Size = 1024
      EmptyStrToNull = True
    end
    object DBErrorsERRORIBMESSAGE: TFIBStringField
      FieldName = 'ERROR$IB$MESSAGE'
      Size = 1024
      EmptyStrToNull = True
    end
    object DBErrorsERRORCOMPONENT: TFIBStringField
      FieldName = 'ERROR$COMPONENT'
      Size = 256
      EmptyStrToNull = True
    end
    object DBErrorsERRORSQL: TFIBMemoField
      FieldName = 'ERROR$SQL'
      BlobType = ftMemo
      Size = 8
    end
  end
end
