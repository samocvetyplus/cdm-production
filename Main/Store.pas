unit Store;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,ListAncestor, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls, ComCtrls,  DB,
  Menus, StdCtrls, Grids, DBGrids, RXDBCtrl, dbUtil, fmUtils,
  DBGridEh, ActnList, PrnDbgeh, DBGridEhGrouping, GridsEh;

type
  TfmStore = class(TfmAncestor)
    tb2: TSpeedBar;
    laDep: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    spitDep: TSpeedItem;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    SpeedItem3: TSpeedItem;
    ppDep: TPopupMenu;
    mnitAllWh: TMenuItem;
    mnitSep: TMenuItem;
    SpeedItem4: TSpeedItem;
    szCheck: TCheckBox;
    SpeedItem5: TSpeedItem;
    grStore: TDBGridEh;
    SpeedItem6: TSpeedItem;
    acPrint: TAction;
    PrintDBGridEh1: TPrintDBGridEh;
    SpeedItem7: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure grStoreGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure SpeedItem4Click(Sender: TObject);
    procedure szCheckClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem2Click(Sender: TObject);
    procedure SpeedItem1Click(Sender: TObject);
    procedure SpeedItem3Click(Sender: TObject);
    procedure SpeedItem5Click(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
  private
    { Private declarations }
     procedure DepClick(Sender: TObject);
  protected

  public
    { Public declarations }

  end;

var
  fmStore: TfmStore;

implementation

uses DictData, InvData, StoreItems, MainData, SList;

{$R *.dfm}

procedure TfmStore.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  laDep.Caption := TMenuItem(Sender).Caption;
  dmInv.FCurrDep := TMenuItem(Sender).Tag;
  ReOpenDataSets([dmINV.taStoreByArt]);
end;


procedure TfmStore.FormCreate(Sender: TObject);
var
  i, j : integer;
  Item : TMenuItem;
begin
   dminv.sqlPublic.SQL.Clear;
   dminv.sqlPublic.Sql.add('execute procedure CALC_ADREST');
   dminv.sqlPublic.Prepare;
   dminv.sqlPublic.ExecQuery;

   dm.CurrDep := dm.DepDef;
   dmInv.FCurrDep:= ROOT_COMP_CODE;
 // ��������!!
  dmINV.WSZ:=true;
  mnitAllWh.OnClick := DepClick;
  laDep.Caption := mnitAllWh.Caption;
  inherited;
  //���������� PopupMenu �� FDepInfo;
  j := 0;
  for i:=0 to Pred(dm.CountDep) do
  begin
  if dm.DepInfo[i].Wh then
  begin
    Item := TMenuItem.Create(ppDep);
    Item.Caption := dm.DepInfo[i].SName;
    Item.Tag := dm.DepInfo[i].DepId;
    Item.OnClick := DepClick;
    Item.RadioItem := True;
    if Item.Tag = dm.DepDef then DepClick(Item);
    inc(j);
    if j>30 then
    begin
      Item.Break := mbBreak;
      j:=0;
    end;
    ppDep.Items.Add(Item);
  end;
  end;
  OpenDataSets([dmINV.taStorebyArt]);
//  ReopenDataSets([taINVItems,taArt, taA2]);

end;

procedure TfmStore.grStoreGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  inherited;
  if (FIELD.FieldName= 'RQ')or(FIELD.FieldName='RW')or(FIELD.FieldName='SZ')
    then Background:=clInfoBk
    else Background:=clBtnFace;
end;

procedure TfmStore.SpeedItem4Click(Sender: TObject);
begin
  inherited;
  if dmInv.taStorebyArtRQ.ASInteger>0 then
  ShowAndFreeForm(TfmStoreItems, Self, TForm(fmStoreItems), True, False);
end;

procedure TfmStore.szCheckClick(Sender: TObject);
begin
  inherited;
   if  szCheck.Checked then   grStore.FieldColumns['SZ'].Visible:=true
   else grStore.FieldColumns['SZ'].Visible:=false;
   dmINV.WSZ:=szCheck.Checked;
   ReOpenDataSets([dmInv.taStorebyART]);
end;

procedure TfmStore.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
   CloseDataSets([dmINV.taStorebyART]);
end;

procedure TfmStore.SpeedItem2Click(Sender: TObject);
begin
  inherited;
  dmINV.ITYPE:=2;
  ShowListForm(TfmSList, dmInv.taSList, dm.CurrDep, dm.BeginDate, dm.EndDate,
    dmMain.ChangePeriod, dmMain.quTmp);
   ReOpenDataSets([dmINV.taStorebyART]);
end;

procedure TfmStore.SpeedItem1Click(Sender: TObject);
begin
  inherited;
  dmINV.ITYPE:=4;
  ShowListForm(TfmSList, dmInv.taSList, dm.CurrDep, dm.BeginDate, dm.EndDate,
    dmMain.ChangePeriod, dmMain.quTmp);
   ReOpenDataSets([dmINV.taStoreByArt]);
end;

procedure TfmStore.SpeedItem3Click(Sender: TObject);
begin
  inherited;
   dmINV.ITYPE:=3;
   ShowListForm(TfmSList, dmInv.taSList, dm.CurrDep, dm.BeginDate, dm.EndDate,
    dmMain.ChangePeriod, dmMain.quTmp);
    ReOpenDataSets([dmINV.taStoreByArt]);
end;

procedure TfmStore.SpeedItem5Click(Sender: TObject);
begin
  inherited;
   dmINV.ITYPE:=5;
   ShowListForm(TfmSList, dmInv.taSList, dm.CurrDep, dm.BeginDate, dm.EndDate,
    dmMain.ChangePeriod, dmMain.quTmp);
    ReOpenDataSets([dmINV.taStoreByArt]);
end;

procedure TfmStore.acPrintExecute(Sender: TObject);
begin
  inherited;
  PrintDBGridEh1.Preview;
//  PrintDBGridEh1.Print;
end;

end.
