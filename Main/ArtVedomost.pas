unit ArtVedomost;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGridEh, ActnList, DB, DBUtil, DBGridEhGrouping, GridsEh,
  rxPlacemnt, rxSpeedbar;

type
  TfmArtVedomost = class(TfmAncestor)
    DBGridEh1: TDBGridEh;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    ActionList1: TActionList;
    acOpen: TAction;
    acPrint: TAction;
    procedure acOpenExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGridEh1GetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmArtVedomost: TfmArtVedomost;

implementation

uses InvData, MainData, PrintData;

{$R *.dfm}

procedure TfmArtVedomost.acOpenExecute(Sender: TObject);
begin
  inherited;
//  ShowMessage(inttostr(DBGridEh1.SelectedRows.Count));
  if not (dmInv.taAVList.State in [dsEdit,dsInsert]) then dmInv.taAVList.Edit;
  if acOpen.Caption = '������' then
  begin
    try
      dmInv.taAVListISCLOSE.AsInteger := 1;
    except
    end;
    acOpen.Caption    := '������';
    acOpen.Hint       := '������� ��������';
    acOpen.ImageIndex := 2;
  end
  else begin
    try
      dmInv.taAVListISCLOSE.AsInteger:=0;
    except
    end;
    acOpen.Caption    := '������';
    acOpen.Hint       := '������� ��������';
    acOpen.ImageIndex := 1;
  end;
  PostDataSet(dmInv.taAVList);
end;

procedure TfmArtVedomost.FormCreate(Sender: TObject);
begin
  inherited;

  if dmInv.taAVListISCLOSE.Asinteger=1 then
  begin
    acOpen.Caption    := '������';
    acOpen.Hint       := '������� ��������';
    acOpen.ImageIndex := 2;
  end else begin
    acOpen.Caption    := '������';
    acOpen.Hint       := '������� ��������';
    acOpen.ImageIndex := 1;
  end;
  OpenDataSet(dmInv.taArtVedomost);
end;

procedure TfmArtVedomost.DBGridEh1GetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if (Column.Field.AsFloat=0)and(Column.FieldName<>'ART') then Background:=clwhite;
end;

procedure TfmArtVedomost.acPrintExecute(Sender: TObject);
begin
  inherited;
   dmPrint.PrintDocumentA(dkJournalOrder,null);
end;

procedure TfmArtVedomost.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CloseDataSet(dmInv.taArtVedomost);
end;

end.
