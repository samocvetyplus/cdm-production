unit DictAncestor;

interface  

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Ancestor, ImgList, ComCtrls, RxSpeedBar, ExtCtrls, Grids,
  DBGrids, RXDBCtrl, db,
  {$ifdef FIBP} FIBQuery, FIBDataSet, pFIBDataSet, {$endif}
  {$ifdef IBX} IBSQL, IbCustomDataSet, {$endif}
  StdCtrls, Buttons, ActnList, DBGridEh, DBGridEhGrouping, GridsEh,
  rxPlacemnt;

type
  TRecName=record
    sID    : string; // ��� ����-��������������
    sName  : string; // ��� ����, ������������������ ��� ������ ������������ ��� ����������
    sTable : string; // ��� �������
    sWhere : string; // ����� ������� ������������� ����������� ��� ������ �� ������� ��� ����������
  end;

  TDictMode = (dmDictionary, dmSelRecordEdit, dmSelRecordReadOnly);

  TDictAncestorClass = class of TfmDictAncestor;

  TfmDictAncestor = class(TfmAncestor)
    plDict: TPanel;
    plSelect: TPanel;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acSort: TAction;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    btnOk: TSpeedButton;
    btnCancel: TSpeedButton;
    SpeedItem3: TSpeedItem;
    acExit: TAction;
    gridDict: TDBGridEh;
    acHelp: TAction;
    procedure spitAddClick(Sender: TObject);
    procedure spitDelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure grGetCellParams(Sender: TObject; Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure grKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure grDblClick(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acSortExecute(Sender: TObject);
    procedure btnOkClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure gridDictDblClick(Sender: TObject);
    procedure gridDictGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure gridDictKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acExitExecute(Sender: TObject);
    procedure Action1Execute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acHelpUpdate(Sender: TObject);
  private
    FOldHint : TNotifyEvent;
    {$ifdef IBX}
    FQuery : TIBSQL;
    FDataSet: TIBCustomDAtaSet;
    {$else}
    FQuery : TFIBQuery;
    FDataSet: TpFIBDataSet;
    {$endif}
    procedure Hint(Sender : TObject);
  protected
    FParams : TRecName;
    FMode : TDictMode;
    FHelpContext : THelpContext;
  public
    constructor Create(AOwner:TComponent; dtr:TDataSource; const aSr: TRecName;
       Mode : TDictMode; OnFilterRecord : TFilterRecordEvent; HelpContext: THelpContext);overload;
    constructor CreateParented(ParentWindow : HWND; dtr:TDataSource; const aSr: TRecName;
      Mode : TDictMode; OnFilterRecord : TFilterRecordEvent; HelpContext: THelpContext);overload;
    {$ifdef IBX} property DataSet : TIbCustomDataSet read FDataSet write FDataSet; {$endif}
    {$ifdef FIBP} property DataSet : TpFIBDataSet read FDataSet write FDataSet; {$endif}
  end;


  (* sFName - ��������� ����� ����������� *)
  function ShowDictform(FC : TDictAncestorClass; dtr : TDataSource;
     const aSr : TRecName; const sFName:string='';
     const Mode : TDictMode = dmDictionary; OnFilterRecord : TFilterRecordEvent = nil;
     HelpContext: THelpContext=0) : word;

 (* Example
    procedure ...
    const
      Rec : TRecName = (sId: 'ID'; sName: 'PRILL'; sTable: 'D_PRILL'; sWhere: '');
    begin
      ShowDictForm(TfmPrill, TfmDictAncestor(fmPrill), dm.dsrPrill, Rec);
    end;

 *)
var
  vResult : Variant;

implementation

{$R *.DFM}

uses dbUtil, Sort, Variants;

type
   TNodeData = class(TObject)
   public
     Name: string; // ��� ��� �����������
     Code: variant;
     Loaded: boolean;
     Value: integer;
     SaveValue: integer;
     constructor Create;
   end;

var
  Dictionary : TfmDictAncestor;

function ShowDictForm(FC : TDictAncestorClass; dtr : TDataSource;
   const aSr : TRecName; const sFName:string='';
   const Mode : TDictMode = dmDictionary; OnFilterRecord : TFilterRecordEvent = nil;
   HelpContext: THelpContext=0)  : word;
begin
  try
    Application.ProcessMessages;
    Dictionary := NIL;
    try
      Dictionary := FC.Create(Application, dtr, aSr, Mode, OnFilterRecord, HelpContext);
      Dictionary.Caption := sFName; //������������ ��� ������������ �����
      Result := Dictionary.ShowModal;
    finally
      FreeAndNil(Dictionary);
    end
  finally
  end;
end;

constructor TfmDictAncestor.Create(AOwner : TComponent; dtr:TDataSource;
  const aSr : TRecName; Mode : TDictMode; OnFilterRecord : TFilterRecordEvent;
  HelpContext : THelpContext);
begin
  {$ifdef IBX} FDataSet := TIBCustomDataSet(dtr.DataSet); {$endif}
  {$ifdef FIBP} FDataSet := TpFIBDataSet(dtr.DataSet); {$endif}
  FDataSet.OnFilterRecord := OnFilterRecord;
  if Assigned(OnFilterRecord) then FDataSet.Filtered := True;
  FParams := aSr;
  FMode := Mode;
  FHelpContext := HelpContext;
  inherited Create(AOwner);
end;

constructor TfmDictAncestor.CreateParented(ParentWindow : HWND; dtr:TDataSource; const aSr: TRecName;
  Mode : TDictMode;OnFilterRecord : TFilterRecordEvent; HelpContext: THelpContext);
begin
  {$ifdef IBX}  FDataSet := TIBCustomDataSet(dtr.DataSet); {$endif}
  {$ifdef FIBP} FDataSet := TpFIBDataSet(dtr.DataSet); {$endif}
  FDataSet.OnFilterRecord := OnFilterRecord;
  if Assigned(OnFilterRecord) then FDataSet.Filtered := True;
  FParams := aSr;
  FMode := Mode;
  FHelpContext := HelpContext;
  inherited CreateParented(ParentWindow);
end;


procedure TfmDictAncestor.spitAddClick(Sender: TObject);
begin
  if Assigned(DataSet) then
  begin
    PostDataSet(DataSet);
    DataSet.Append;
  end;
end;

procedure TfmDictAncestor.spitDelClick(Sender: TObject);
begin
  if Assigned(DataSet) then
  begin
    PostDataSet(DataSet);
    DataSet.Delete;
  end;
end;

procedure TfmDictAncestor.FormCreate(Sender: TObject);
begin
  inherited;
  if Assigned(DataSet) then
  begin
    ReOpenDataSets([DataSet]);
    if not DataSet.Transaction.Active then DataSet.Transaction.StartTransaction;
  end;
  {$ifdef IBX} FQuery := TIBSQL.Create(Application);{$endif}
  {$ifdef FIBP}FQuery := TFIBQuery.Create(Application);{$endif}
  //gr.FixedCols := 1;
  //InitDbGrid(gr);
  case FMode of
    dmDictionary : begin
      plSelect.Visible := False;
    end;
    dmSelRecordEdit : begin
       spitExit.Visible := False;
       stbrStatus.Visible := False;
       BorderStyle := bsToolWindow;
    end;
    dmSelRecordReadOnly : begin
      gridDict.ReadOnly := True;
      tb1.Visible := False;
      ActiveControl := gridDict;
    end;
  end
end;

procedure TfmDictAncestor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if Assigned(DataSet) then
  begin
    if (DataSet.State = dsEdit)or(DataSet.State = dsInsert) then DataSet.Post;
    if FMode in [dmSelRecordEdit, dmSelRecordReadOnly] then
      if Assigned(DataSet.FindField(FParams.sId)) then
      {$ifdef IBX}
      vResult := DataSet.FieldByName(FParams.sId).Value
      {$endif}
      {$ifdef FIBP}
      vResult := DataSet.FieldByName(FParams.sId).Value
      {$endif}
      else vResult := null
    else vResult := null;
    CloseDataSets([DataSet]);
    DataSet.Filtered := False;
    FQuery.Free;
    if DataSet.Transaction.Active then DataSet.Transaction.CommitRetaining;
  end;
  Action := caFree;
end;

procedure TfmDictAncestor.grGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if AnsiUpperCase(Field.FieldName)='RECNO' then Background:= clMoneyGreen;
end;

procedure TfmDictAncestor.FormActivate(Sender: TObject);
begin
  FOldHint := Application.OnHint;
  Application.OnHint := Hint;
end;

procedure TfmDictAncestor.FormDeactivate(Sender: TObject);
begin
  Application.OnHint := FOldHint;
end;

procedure TfmDictAncestor.Hint(Sender: TObject);
begin
  stbrStatus.SimpleText := Application.Hint;
end;

procedure TfmDictAncestor.grKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_INSERT : if Shift = [] then spitAddClick(nil);
    VK_DELETE : begin
      if (Shift = [ssCtrl])and(not(DataSet.State in[dsEdit, dsInsert])) then spitDelClick(nil);
    end
  end;
end;


{TNodeData}

constructor TNodeData.Create;
begin
  inherited Create;
  Value := 0;
end;

procedure TfmDictAncestor.grDblClick(Sender: TObject);
begin
  if FMode in [dmSelRecordEdit, dmSelRecordReadOnly] then
  begin
    ModalResult := mrOk;
  end;
end;

procedure TfmDictAncestor.acAddExecute(Sender: TObject);
begin
  DataSet.Insert;
end;

procedure TfmDictAncestor.acDelExecute(Sender: TObject);
begin
   DataSet.Delete;
end;

procedure TfmDictAncestor.acSortExecute(Sender: TObject);
var i: integer;
    nd: TNodeData;
begin
  PostDataSet(DataSet);
  fmSort:=TfmSort.Create(Application);
  with fmSort do
  try
    lb1.Items.Clear;
    with FQuery do
    begin
      Database:=DataSet.Database;
      Transaction:=DataSet.Transaction;
      SQL.Text:='SELECT '+FParams.sID+', '+FParams.sName+' FROM '+FParams.sTable+FParams.sWhere+' ORDER BY SortInd';
      ExecQuery;
      while NOT EOF do
      begin
        nd:=TNodeData.Create;
        nd.Code:=Fields[0].AsString;
        lb1.Items.AddObject(Fields[1].AsString, nd);
        Next;
      end;
      Close;
    end;
    if ShowModal=mrOK then
    begin
      FQuery.SQL.Text:='update '+FParams.sTable+' set SortInd=?SortInd where '+FParams.sID+'=:'+FParams.sID;
      for i:=0 to lb1.Items.Count-1 do
        with FQuery  do
        begin
          Params[0].AsInteger:=i;
          Params[1].AsString:=TNodeData(lb1.Items.Objects[i]).Code;
          ExecQuery;
        end;
        FQuery.Transaction.CommitRetaining;
        ReOpenDataSets([DataSet]);
    end
  finally
    fmSort.Free;
  end;
end;

procedure TfmDictAncestor.btnOkClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TfmDictAncestor.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfmDictAncestor.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : if(FMode = dmSelRecordReadOnly)then ModalResult := mrOk
      else if(FMode = dmSelRecordEdit)and(Shift = [ssCtrl])then ModalResult := mrOk;
  end;
end;

procedure TfmDictAncestor.gridDictDblClick(Sender: TObject);
begin
  if FMode in [dmSelRecordEdit, dmSelRecordReadOnly] then
  begin
    ModalResult := mrOk;
  end;
end;

procedure TfmDictAncestor.gridDictGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if AnsiUpperCase(Column.FieldName)='RECNO' then Background:= clMoneyGreen;
end;

procedure TfmDictAncestor.gridDictKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_INSERT : if Shift = [] then acAdd.Execute;
    VK_DELETE : begin
      if (Shift = [ssCtrl])and(not(DataSet.State in[dsEdit, dsInsert])) then acDel.Execute;
    end
  end;
end;

procedure TfmDictAncestor.acExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfmDictAncestor.Action1Execute(Sender: TObject);
begin
  inherited;
  //
end;

procedure TfmDictAncestor.acHelpExecute(Sender: TObject);
begin
  Application.HelpContext(FHelpContext);
end;

procedure TfmDictAncestor.acHelpUpdate(Sender: TObject);
begin
  acHelp.Visible := FHelpContext<>0;
end;

end.



