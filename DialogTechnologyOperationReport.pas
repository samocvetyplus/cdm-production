unit DialogTechnologyOperationReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, FIBDataSet, pFIBDataSet, DBClient, FIBDatabase, pFIBDatabase,
  cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxStyles,
  cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridLevel, cxGrid, ActnList, StdCtrls, Menus, cxButtons, ImgList, Provider,
  FR_DSet, FR_DBSet, dxBar;

const
  sqlOperations: string = 'select ID, Operation from d_oper';

  sqlMultipleMasteroperations: string = 'Select group$id, group$name, operation, quota from technology$master$operations(:Operations)';

  sqlAdditionalLosses: string = 'select operation$name, sjem$norm, losses$norm, wage$rate from technology$additional$oper$S(:master$operations, :detail$operations) order by operation$name';

type
  TdlgTechnologyOperationReport = class(TForm)
    DataSetOperations: TClientDataSet;
    trRead: TpFIBTransaction;
    GridSourceLevel: TcxGridLevel;
    GridSource: TcxGrid;
    GridSourceView: TcxGridDBTableView;
    GridMasterView: TcxGridDBTableView;
    GridMasterLevel: TcxGridLevel;
    GridMaster: TcxGrid;
    GridDetail: TcxGrid;
    GridDetailView: TcxGridDBTableView;
    GridDetailLevel: TcxGridLevel;
    ActionList: TActionList;
    DataSetMaster: TClientDataSet;
    DataSetDetail: TClientDataSet;
    Images32: TImageList;
    btnAddMaster: TcxButton;
    btnAddDetail: TcxButton;
    GridDetailViewColumn: TcxGridDBColumn;
    GridMasterViewColumn: TcxGridDBColumn;
    GridSourceViewColumn: TcxGridDBColumn;
    SourceDetail: TDataSource;
    SourceMaster: TDataSource;
    SourceOperations: TDataSource;
    btnReturnDetail: TcxButton;
    btnReturnMaster: TcxButton;
    acAddMaster: TAction;
    acReturnMaster: TAction;
    acAddDetail: TAction;
    acReturnDetail: TAction;
    acMultipleMaster: TAction;
    DataSetPrint: TClientDataSet;
    frPrint: TfrDBDataSet;
    acAdditionalLosses: TAction;
    BarManager: TdxBarManager;
    Bar: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    procedure FormActivate(Sender: TObject);
    procedure acAddMasterExecute(Sender: TObject);
    procedure acReturnMasterExecute(Sender: TObject);
    procedure acAddDetailExecute(Sender: TObject);
    procedure acReturnDetailExecute(Sender: TObject);
    procedure acReturnDetailUpdate(Sender: TObject);
    procedure acAddDetailUpdate(Sender: TObject);
    procedure acReturnMasterUpdate(Sender: TObject);
    procedure acAddMasterUpdate(Sender: TObject);
    procedure acMultipleMasterExecute(Sender: TObject);
    procedure acAdditionalLossesExecute(Sender: TObject);
    procedure dxBarLargeButton3Click(Sender: TObject);
    procedure acMultipleMasterUpdate(Sender: TObject);
    procedure acAdditionalLossesUpdate(Sender: TObject);
  private
    function GetData(SQL:string; Params: array of Variant): OleVariant;
    function GetOperationsString(DataSet: TDataSet; FieldName: String): String;
    procedure OperationDataSetInit(DataSet: TClientDataSet);
    procedure MoveOperation(Source: TClientDataSet; Target: TClientDataSet; Direction: SmallInt);
  public
    class procedure Execute;
  end;

var
  dlgTechnologyOperationReport: TdlgTechnologyOperationReport;

implementation

uses DictData, uDialogs, TechnologyGroups, PrintData;

{$R *.dfm}

procedure TdlgTechnologyOperationReport.acAddDetailExecute(Sender: TObject);
var
  Direction: SmallInt;
begin
  Direction := -1;

  if Sender is TAction then
  begin
    Direction := TAction(Sender).Tag;
  end;

  MoveOperation(DataSetOperations, DataSetDetail, Direction);
end;

procedure TdlgTechnologyOperationReport.acAddDetailUpdate(Sender: TObject);
begin
  acAdddetail.Enabled := not DataSetOperations.IsEmpty;
end;

procedure TdlgTechnologyOperationReport.acAdditionalLossesExecute(
  Sender: TObject);
var
  MasterOperations: String;
  DetailOperations: String;
begin

  if DataSetMaster.IsEmpty then
  begin
    DialogErrorOkMessage('����������� ������� ��������!');
    Exit;
  end;

  if DataSetDetail.IsEmpty then
  begin
    DialogErrorOkMessage('����������� �������������� ��������!');
    Exit;
  end;

  MasterOperations := GetOperationsString(DataSetMaster, 'Operation$ID');

  DetailOperations := GetOperationsString(DataSetDetail, 'Operation$ID');

  DataSetPrint.Data := GetData(sqlAdditionalLosses, [MasterOperations, DetailOperations]);

  try
    fmTechnologyGroups.printDoc(119, nil, frPrint);
  except
    On E:Exception do
    begin
      DialogErrorOKMessage('������ ��� �������� �������� �����: ' + E.Message);
    end;
  end;

end;

procedure TdlgTechnologyOperationReport.acAdditionalLossesUpdate(
  Sender: TObject);
var Enabled: Boolean;
begin

  Enabled := not (DataSetMaster.IsEmpty);
  Enabled := Enabled and (not DataSetDetail.IsEmpty);

  acAdditionalLosses.Enabled := Enabled;
end;

procedure tdlgTechnologyoperationReport.OperationDataSetInit(DataSet: TClientDataSet);
begin
  DataSet.FieldDefs.Add('Operation$ID', ftInteger);
  DataSet.FieldDefs.Add('Operation$Name', ftString, 60);
  DataSet.FieldDefs.Add('Operation$Order', ftInteger);

  DataSet.CreateDataSet;

  DataSet.IndexFieldNames := 'Operation$Order';
end;

function TdlgTechnologyOperationReport.GetOperationsString(DataSet: TDataSet; FieldName: String): String;
var
  Operation: String;
  Operations: String;
begin
  DataSet.DisableControls;
  DataSet.First;

  if DataSet.FindField(FieldName) <> nil then
  begin
    Operations := trim(DataSet.FieldByName(FieldName).AsString);
    DataSet.Next;

    while not DataSet.EoF do
    begin
      Operation := trim(DataSet.FieldByName(FieldName).AsString);
      Operations := Operations + ',' + Operation;
      DataSet.Next;
    end;

  end;

  Result := Operations;

  DataSet.First;
  DataSet.EnableControls;
end;

procedure TdlgTechnologyOperationReport.FormActivate(Sender: TObject);
begin

  DataSetOperations.Data := GetData(sqlOperations, []);
  DataSetOperations.IndexFieldNames := 'Operation';

  OperationDataSetInit(DataSetMaster);
  OperationDataSetInit(DataSetDetail);

  GridSourceView.DataController.DataSource := SourceOperations;
  GridMasterView.DataController.DataSource := SourceMaster;
  GridDetailView.DataController.DataSource := SourceDetail;

  GridMasterViewColumn.DataBinding.FieldName := 'Operation$Name';
  GridMasterViewColumn.Caption := '�������� ��������';

  GridDetailViewColumn.DataBinding.FieldName := 'Operation$Name';
  GridDetailViewColumn.Caption := '�������������� ��������';

  GridSourceViewColumn.DataBinding.FieldName := 'Operation';
  GridSourceViewColumn.Caption := '��� ��������';
end;

procedure TdlgTechnologyOperationReport.MoveOperation(Source: TClientDataSet; Target: TClientDataSet; Direction: Smallint);
var
  i: Integer;
  FieldsCount: Integer;
begin
  Screen.Cursor := crHourGlass;

  case Direction of
    1: FieldsCount := Target.FieldCount - 1;
    2: FieldsCount := Target.FieldCount;
  else DialogErrorOkMessage('����������� ����������� ������������!');
  end;

  try
    try
      Target.Append;

      for i := 0 to FieldsCount - 1 do
      begin
        Target.Fields[i].AsVariant := Source.Fields[i].AsVariant;
      end;

      if Direction = 1 then
      begin
        Target.Fields[FieldsCount].AsInteger := Target.RecordCount + 1;
      end;
      
    except
      On E:Exception do
        begin
          Target.Cancel;
          DialogErrorOkMessage('������ ��� ������� ����������� ��������: ' + E.Message);
        end;
    end;
  finally
    Target.Post;
    Source.Delete;
    Screen.Cursor := crDefault;
  end;

end;

function TdlgTechnologyOperationReport.GetData(SQL: string; Params: array of Variant): OleVariant;
var
  cdsProvider: TDataSetProvider;
  DataSet: TClientDataSet;
  dbDataSet: TpFIBDataSet;
  i: integer;
begin
  if SQL <> '' then
  begin
    dbDataSet := TpFibDataSet.Create(Self);
    dbDataSet.Transaction := trRead;
    dbDataSet.Database := trRead.DefaultDatabase;
    dbDataSet.SelectSQL.Text := SQL;

    cdsProvider := TDataSetProvider.Create(Self);
    cdsProvider.DataSet := dbDataSet;
    cdsProvider.Name := 'DataProvider';

    DataSet := TClientDataSet.Create(Self);
    DataSet.ProviderName := cdsProvider.Name;

    if Length(Params) <> 0 then
    begin
      for i := Low(Params) to High(Params) do
      begin
        dbDataSet.Params[i].AsVariant := Params[i];
      end;
    end;

    try
      DataSet.Active := true;
    except
      On E:Exception do
      begin
        DialogErrorOkMessage('������ ��� �������� ������: ' + E.Message);
      end;
    end;

    Result := DataSet.Data;

    DataSet.Active := false;
    DataSet.Free;

    cdsProvider.Free;

    dbDataSet.Free;
  end;
end;

procedure TdlgTechnologyOperationReport.acAddMasterExecute(Sender: TObject);
var
  Direction: SmallInt;
begin
  Direction := -1;

  if Sender is TAction then
  begin
    Direction := TAction(Sender).Tag;
  end;

  MoveOperation(DataSetOperations, DataSetMaster, Direction);
end;

procedure TdlgTechnologyOperationReport.acAddMasterUpdate(Sender: TObject);
begin
  acAddMaster.Enabled := not DataSetOperations.IsEmpty;
end;

procedure TdlgTechnologyOperationReport.acMultipleMasterExecute(
  Sender: TObject);
var
  MasterOperations: String;
  OperationID: String;
begin
  if not DataSetMaster.IsEmpty then
  begin
    if DataSetMaster.RecordCount > 1 then
    begin
      Screen.Cursor := crHourGlass;

      MasterOperations := GetOperationsString(DataSetMaster, 'Operation$ID');

      DataSetPrint.Data := GetData(sqlMultipleMasterOperations, [MasterOperations]);

      try
        fmTechnologyGroups.PrintDoc(118, nil, frPrint);
      except
        On E:Exception do
        begin
          DialogErrorOkMessage('������ ��� �������� �������� �����: ' + E.Message);
        end;
      end;

    end else
    begin
      DialogErrorOkMessage('� ������ ���� ��������!');
    end;
  end else
  begin
    DialogErrorOkMessage('������ �������� �������� ����!');
  end;
end;

procedure TdlgTechnologyOperationReport.acMultipleMasterUpdate(Sender: TObject);
var
  Enabled: Boolean;
begin
  Enabled := not DataSetMaster.IsEmpty;

  acMultipleMaster.Enabled := Enabled;
end;

procedure TdlgTechnologyOperationReport.acReturnDetailExecute(Sender: TObject);
var
  Direction: SmallInt;
begin
  Direction := -1;

  if Sender is TAction then
  begin
    Direction := TAction(Sender).Tag;
  end;

  MoveOperation(DataSetDetail, DataSetOperations, Direction);
end;

procedure TdlgTechnologyOperationReport.acReturnDetailUpdate(Sender: TObject);
begin
  acReturnDetail.Enabled := not DataSetDetail.IsEmpty;
end;

procedure TdlgTechnologyOperationReport.acReturnMasterExecute(Sender: TObject);
var
  Direction: SmallInt;
begin
  Direction := -1;

  if Sender is TAction then
  begin
    Direction := TAction(Sender).Tag;
  end;

  MoveOperation(DataSetMaster, DataSetOperations, Direction);
end;

procedure TdlgTechnologyOperationReport.acReturnMasterUpdate(Sender: TObject);
begin
  acReturnMaster.Enabled := not DataSetMaster.IsEmpty;
end;

procedure TdlgTechnologyOperationReport.dxBarLargeButton3Click(Sender: TObject);
begin
  Close;
end;

class procedure TdlgTechnologyOperationReport.Execute;
begin
  dlgTechnologyOperationReport := nil;

  try
    dlgTechnologyOperationReport := TdlgtechnologyOperationReport.Create(Application);
    dlgTechnologyOperationReport.ShowModal;
  finally
    FreeAndNil(dlgTechnologyOperationreport);
  end;

end;

end.
