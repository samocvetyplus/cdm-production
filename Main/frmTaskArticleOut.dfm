object DialogTaskArticleOut: TDialogTaskArticleOut
  Left = 155
  Top = 46
  BorderStyle = bsDialog
  Caption = #1055#1088#1080#1077#1084' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090#1072' '#1087#1086' '#1079#1072#1082#1072#1079#1091
  ClientHeight = 663
  ClientWidth = 799
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TcxPageControl
    Left = 0
    Top = 0
    Width = 799
    Height = 624
    ActivePage = TabSheet2
    Align = alClient
    LookAndFeel.Kind = lfOffice11
    Style = 5
    TabOrder = 0
    OnChange = PageControlChange
    ClientRectBottom = 624
    ClientRectRight = 799
    ClientRectTop = 23
    object TabSheet1: TcxTabSheet
      Caption = #1047#1072#1082#1072#1079#1099
      ImageIndex = 0
      object GridDemand: TcxGrid
        Left = 0
        Top = 0
        Width = 799
        Height = 601
        Align = alClient
        TabOrder = 0
        object GridDemandDBTableView: TcxGridDBTableView
          OnDblClick = ActionNextExecute
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = DataSourceDemand
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object GridDemandDBTableViewDOCNO: TcxGridDBColumn
            Caption = #8470
            DataBinding.FieldName = 'DOCNO'
            Options.Editing = False
            Width = 37
          end
          object GridDemandDBTableViewOPERATION: TcxGridDBColumn
            Caption = #1054#1087#1077#1088#1072#1094#1080#1103
            DataBinding.FieldName = 'OPERATION'
            Options.Editing = False
            Width = 133
          end
          object GridDemandDBTableViewDOCDATE: TcxGridDBColumn
            Caption = #1044#1072#1090#1072
            DataBinding.FieldName = 'DOCDATE'
            Options.Editing = False
            Width = 92
          end
          object GridDemandDBTableViewW: TcxGridDBColumn
            Caption = #1042#1077#1089
            DataBinding.FieldName = 'W'
            Options.Editing = False
          end
          object GridDemandDBTableViewQ: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'Q'
            Options.Editing = False
          end
          object GridDemandDBTableViewFIO: TcxGridDBColumn
            Caption = #1060#1072#1084#1080#1083#1080#1103' '#1048'.'#1054'.'
            DataBinding.FieldName = 'FIO'
            Options.Editing = False
            Width = 325
          end
        end
        object GridDemandLevel: TcxGridLevel
          GridView = GridDemandDBTableView
        end
      end
    end
    object TabSheet2: TcxTabSheet
      Caption = #1040#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      ImageIndex = 1
      object GridDemandAndTask: TcxGrid
        Left = 0
        Top = 0
        Width = 799
        Height = 601
        Align = alClient
        TabOrder = 0
        object GridDemandAndTaskMaster: TcxGridDBBandedTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = SourceArticlesMaster
          DataController.KeyFieldNames = 'JAID'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#####'
              Kind = skSum
            end
            item
              Format = '0'
              Kind = skSum
              Column = GridDemandAndTaskMasterAQ
            end
            item
              Format = '0'
              Kind = skSum
              Column = GridDemandAndTaskMasterAFQ
            end
            item
              Format = '0'
              Kind = skSum
              Column = GridDemandAndTaskMasterQ
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.IncSearch = True
          OptionsBehavior.IncSearchItem = GridDemandAndTaskMasterAART
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.ScrollBars = ssVertical
          OptionsView.ExpandButtonsForEmptyDetails = False
          OptionsView.Footer = True
          OptionsView.FooterAutoHeight = True
          OptionsView.GroupByBox = False
          OptionsView.GroupRowStyle = grsOffice11
          OptionsView.Indicator = True
          Bands = <
            item
              Caption = #1047#1072#1082#1072#1079
              Width = 248
            end
            item
              Position.BandIndex = 0
              Position.ColIndex = 0
              Width = 124
            end
            item
              Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
              Position.BandIndex = 0
              Position.ColIndex = 1
              Width = 104
            end
            item
              Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
              Width = 126
            end>
          object GridDemandAndTaskMasterMaxQ: TcxGridDBBandedColumn
            Caption = #1044#1086#1089#1090#1091#1087#1085#1086
            DataBinding.FieldName = 'MaxQ'
            Options.Editing = False
            Options.Filtering = False
            Options.IncSearch = False
            Options.Grouping = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Content = cxStyle5
            Width = 73
            Position.BandIndex = 3
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object GridDemandAndTaskMasterAART: TcxGridDBBandedColumn
            Caption = #1040#1088#1090#1080#1082#1091#1083
            DataBinding.FieldName = 'AART'
            Options.Editing = False
            Options.Moving = False
            Styles.Content = cxStyle5
            Width = 68
            Position.BandIndex = 1
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object GridDemandAndTaskMasterASZNAME: TcxGridDBBandedColumn
            Caption = #1056#1072#1079#1084#1077#1088
            DataBinding.FieldName = 'ASZNAME'
            Options.Editing = False
            Options.Filtering = False
            Options.IncSearch = False
            Options.Grouping = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Content = cxStyle5
            Width = 50
            Position.BandIndex = 1
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object GridDemandAndTaskMasterAQ: TcxGridDBBandedColumn
            Caption = #1047#1072#1103#1074#1083#1077#1085#1086
            DataBinding.FieldName = 'AQ'
            Options.Editing = False
            Options.Filtering = False
            Options.IncSearch = False
            Options.Grouping = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Content = cxStyle5
            Width = 163
            Position.BandIndex = 2
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object GridDemandAndTaskMasterAFQ: TcxGridDBBandedColumn
            Caption = #1055#1088#1080#1085#1103#1090#1086
            DataBinding.FieldName = 'AFQ'
            Options.Editing = False
            Options.Filtering = False
            Options.IncSearch = False
            Options.Grouping = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Content = cxStyle5
            Width = 144
            Position.BandIndex = 2
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object GridDemandAndTaskMasterQ: TcxGridDBBandedColumn
            Caption = #1055#1088#1080#1085#1103#1090#1100
            DataBinding.FieldName = 'Q'
            Options.Filtering = False
            Options.IncSearch = False
            Options.Grouping = False
            Options.Moving = False
            Styles.OnGetContentStyle = GridDemandAndTaskMasterQStylesGetContentStyle
            Width = 55
            Position.BandIndex = 3
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object GridDemandAndTaskMasterColumn1: TcxGridDBBandedColumn
            Caption = #1044#1086#1089#1090#1091#1087#1085#1086
            DataBinding.FieldName = 'MoreQ'
            Visible = False
            Options.Editing = False
            Options.Filtering = False
            Options.IncSearch = False
            Options.Grouping = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Content = cxStyle5
            Width = 71
            Position.BandIndex = 3
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
        end
        object GridDemandAndTaskDetail: TcxGridDBTableView
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = SourceArticlesDetail
          DataController.DetailKeyFieldNames = 'JAID'
          DataController.MasterKeyFieldNames = 'JAID'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.ScrollBars = ssNone
          OptionsView.GroupByBox = False
          object GridDemandAndTaskDetailWHART: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1091#1083
            DataBinding.FieldName = 'WHART'
            Options.Editing = False
            Options.Filtering = False
            Options.IncSearch = False
            Options.Grouping = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Content = cxStyle1
            Width = 85
          end
          object GridDemandAndTaskDetailWHSZNAME: TcxGridDBColumn
            Caption = #1056#1072#1079#1084#1077#1088
            DataBinding.FieldName = 'WHSZNAME'
            Options.Editing = False
            Options.Filtering = False
            Options.IncSearch = False
            Options.Grouping = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Content = cxStyle1
            Width = 70
          end
          object GridDemandAndTaskDetailWHOPERNAME: TcxGridDBColumn
            Caption = #1054#1087#1077#1088#1072#1094#1080#1103
            DataBinding.FieldName = 'WHOPERNAME'
            Options.Editing = False
            Options.Filtering = False
            Options.IncSearch = False
            Options.Grouping = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Content = cxStyle1
            Width = 213
          end
          object GridDemandAndTaskDetailWHUNITNAME: TcxGridDBColumn
            Caption = #1045#1076'.'#1080#1079#1084'.'
            DataBinding.FieldName = 'WHUNITNAME'
            Options.Editing = False
            Options.Filtering = False
            Options.IncSearch = False
            Options.Grouping = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Content = cxStyle1
          end
          object GridDemandAndTaskDetailWHQ: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'WHQ'
            FooterAlignmentHorz = taRightJustify
            Options.Editing = False
            Options.Filtering = False
            Options.IncSearch = False
            Options.Grouping = False
            Options.Moving = False
            Options.Sorting = False
            Styles.Content = cxStyle1
          end
        end
        object GridDemandAndTaskLevel: TcxGridLevel
          GridView = GridDemandAndTaskMaster
          object GridDemandAndTaskLevel1: TcxGridLevel
            GridView = GridDemandAndTaskDetail
          end
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 624
    Width = 799
    Height = 39
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object PanelPrior: TPanel
      Left = 478
      Top = 0
      Width = 81
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object ButtonPrior: TButton
        Left = 0
        Top = 8
        Width = 75
        Height = 25
        Action = ActionPrior
        TabOrder = 0
      end
    end
    object PanelNext: TPanel
      Left = 559
      Top = 0
      Width = 80
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object ButtonNext: TButton
        Left = 0
        Top = 8
        Width = 75
        Height = 25
        Action = ActionNext
        TabOrder = 0
      end
    end
    object PanelOK: TPanel
      Left = 639
      Top = 0
      Width = 80
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 2
      object ButtonOK: TButton
        Left = 0
        Top = 8
        Width = 75
        Height = 25
        Action = ActionOK
        ModalResult = 1
        TabOrder = 0
      end
    end
    object PanelCancel: TPanel
      Left = 719
      Top = 0
      Width = 80
      Height = 39
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 3
      object ButtonCancel: TButton
        Left = 0
        Top = 8
        Width = 75
        Height = 25
        Action = ActionCancel
        Cancel = True
        ModalResult = 2
        TabOrder = 0
      end
    end
    object cbOnlyValidEntries: TcxCheckBox
      Left = 8
      Top = 8
      Width = 313
      Height = 21
      Caption = #1055#1086#1082#1072#1079#1072#1090#1100' '#1090#1086#1083#1100#1082#1086' '#1076#1086#1089#1090#1091#1087#1085#1099#1081' '#1076#1083#1103' '#1087#1088#1080#1077#1084#1072' '#1072#1089#1089#1086#1088#1090#1080#1084#1077#1085#1090
      State = cbsChecked
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = True
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 4
    end
  end
  object DataSourceDemand: TDataSource
    DataSet = DataSetDemand
    Left = 128
    Top = 24
  end
  object Transaction: TpFIBTransaction
    DefaultDatabase = dm.db
    TimeoutAction = TARollback
    Left = 272
    Top = 128
  end
  object DataSetDemand: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select i.INVID ID, i.DOCNO, i.DOCDATE, i.W, i.Q, o.OPERATION, m.' +
        'FIO,'
      '          i.ISPROCESS'
      'from Inv i left join D_Oper o on (i.OperId=o.OperId),'
      '        D_Mol m  '
      'where i.ITYPE=1 and'
      '           i.ISCLOSE=1 and'
      '           i.ISPROCESS=0 and'
      '           i.USERID=m.MOLID'
      'order by i.DOCDATE')
    AllowedUpdateKinds = [ukModify]
    Transaction = Transaction
    Database = dm.db
    Left = 48
    Top = 128
    oStartTransaction = False
    object DataSetDemandID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetDemandDOCNO: TFIBIntegerField
      FieldName = 'DOCNO'
    end
    object DataSetDemandDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object DataSetDemandW: TFIBFloatField
      FieldName = 'W'
      DisplayFormat = '########.##'
    end
    object DataSetDemandQ: TFIBFloatField
      FieldName = 'Q'
    end
    object DataSetDemandOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = False
    end
    object DataSetDemandFIO: TFIBStringField
      FieldName = 'FIO'
      Size = 60
      EmptyStrToNull = False
    end
    object DataSetDemandISPROCESS: TFIBSmallIntField
      DefaultExpression = '0'
      FieldName = 'ISPROCESS'
    end
  end
  object ActionList: TActionList
    Left = 160
    Top = 24
    object ActionNext: TAction
      Caption = #1044#1072#1083#1077#1077
      OnExecute = ActionNextExecute
      OnUpdate = ActionNextUpdate
    end
    object ActionPrior: TAction
      Caption = #1053#1072#1079#1072#1076
      OnExecute = ActionPriorExecute
      OnUpdate = ActionPriorUpdate
    end
    object ActionOK: TAction
      Caption = 'OK'
      OnUpdate = ActionOKUpdate
    end
    object ActionCancel: TAction
      Caption = #1054#1090#1084#1077#1085#1072
      OnUpdate = ActionCancelUpdate
    end
  end
  object DataSourceArticles: TDataSource
    Left = 224
    Top = 24
  end
  object cxLookAndFeelController: TcxLookAndFeelController
    Kind = lfOffice11
    Left = 192
    Top = 24
  end
  object DataSetInvoiceRemoved: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into Inv(INVID, DOCNO, DOCDATE, DEPID, FROMDEPID,'
      '    USERID, Q, JOBID, ITYPE, REF, WORDERID)'
      'values (:INVID, :DOCNO, :DOCDATE, :DEPID, :FROMDEPID,'
      '    :USERID, :Q, :JOBID, :ITYPE, :REF, :WORDERID)')
    SelectSQL.Strings = (
      
        'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID, USERID, Q,      ' +
        '   JOBID, ITYPE, REF, WORDERID'
      'from AInv_S(:ID)')
    BeforeOpen = DataSetInvoiceRemovedBeforeOpen
    OnNewRecord = DataSetInvoiceRemovedNewRecord
    Transaction = Transaction
    Database = dm.db
    Left = 112
    Top = 128
    oStartTransaction = False
    object DataSetInvoiceRemovedINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object DataSetInvoiceRemovedDOCNO: TFIBIntegerField
      FieldName = 'DOCNO'
    end
    object DataSetInvoiceRemovedDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object DataSetInvoiceRemovedDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object DataSetInvoiceRemovedFROMDEPID: TFIBIntegerField
      FieldName = 'FROMDEPID'
    end
    object DataSetInvoiceRemovedUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object DataSetInvoiceRemovedQ: TFIBFloatField
      FieldName = 'Q'
    end
    object DataSetInvoiceRemovedJOBID: TFIBIntegerField
      FieldName = 'JOBID'
    end
    object DataSetInvoiceRemovedITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object DataSetInvoiceRemovedREF: TFIBIntegerField
      FieldName = 'REF'
    end
    object DataSetInvoiceRemovedWORDERID: TFIBIntegerField
      FieldName = 'WORDERID'
    end
  end
  object DataSetInvoiceAdded: TpFIBDataSet
    InsertSQL.Strings = (
      'insert into Inv(INVID, DOCNO, DOCDATE, DEPID, FROMDEPID,'
      '    USERID, Q, JOBID, REF, ITYPE, WORDERID)'
      'values (:INVID, :DOCNO, :DOCDATE, :DEPID, :FROMDEPID,'
      '    :USERID, :Q, :JOBID, :REF, :ITYPE, :WORDERID)')
    SelectSQL.Strings = (
      
        'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID, USERID, Q,     J' +
        'OBID, REF, ITYPE, WORDERID'
      'from AInv_S(:ID)')
    BeforeOpen = DataSetInvoiceAddedBeforeOpen
    OnNewRecord = DataSetInvoiceAddedNewRecord
    Transaction = Transaction
    Database = dm.db
    Left = 80
    Top = 128
    oStartTransaction = False
    object DataSetInvoiceAddedINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object DataSetInvoiceAddedDOCNO: TFIBIntegerField
      FieldName = 'DOCNO'
    end
    object DataSetInvoiceAddedDOCDATE: TFIBDateTimeField
      FieldName = 'DOCDATE'
    end
    object DataSetInvoiceAddedDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object DataSetInvoiceAddedFROMDEPID: TFIBIntegerField
      FieldName = 'FROMDEPID'
    end
    object DataSetInvoiceAddedUSERID: TFIBIntegerField
      FieldName = 'USERID'
    end
    object DataSetInvoiceAddedQ: TFIBFloatField
      FieldName = 'Q'
    end
    object DataSetInvoiceAddedJOBID: TFIBIntegerField
      FieldName = 'JOBID'
    end
    object DataSetInvoiceAddedREF: TFIBIntegerField
      FieldName = 'REF'
    end
    object DataSetInvoiceAddedITYPE: TFIBSmallIntField
      FieldName = 'ITYPE'
    end
    object DataSetInvoiceAddedWORDERID: TFIBIntegerField
      FieldName = 'WORDERID'
    end
  end
  object QueryTmp: TpFIBQuery
    Transaction = TransactionTmp
    Database = dm.db
    Left = 240
    Top = 128
  end
  object DataSetInvoiceAddedItems: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AEl set '
      '  Q=:Q,'
      '  COMMENT=:COMMENT'
      'where ID=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into AEl(ID, INVID, ARTID, ART2ID, SZ, Q, T, OPERID, U, C' +
        'OMMENTID, COMMENT)'
      
        'values(:ID, :INVID, :ARTID, :ART2ID, :SZID, :Q, :T, :OPERID, :U,' +
        ' :COMMENTID, :COMMENT)')
    SelectSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      '   OPERID, OPERATION, U, COMMENTID, COMMENT, Q1'
      'from AEl_S(:INVID)')
    BeforeOpen = DataSetInvoiceAddedItemsBeforeOpen
    BeforePost = DataSetInvoiceAddedItemsBeforePost
    OnNewRecord = DataSetInvoiceAddedItemsNewRecord
    Transaction = Transaction
    Database = dm.db
    Left = 144
    Top = 128
    oStartTransaction = False
    object DataSetInvoiceAddedItemsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetInvoiceAddedItemsINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object DataSetInvoiceAddedItemsARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object DataSetInvoiceAddedItemsART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object DataSetInvoiceAddedItemsSZID: TFIBIntegerField
      FieldName = 'SZID'
    end
    object DataSetInvoiceAddedItemsART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = False
    end
    object DataSetInvoiceAddedItemsART2: TFIBStringField
      FieldName = 'ART2'
      Size = 80
      EmptyStrToNull = False
    end
    object DataSetInvoiceAddedItemsSZ: TFIBStringField
      FieldName = 'SZ'
      EmptyStrToNull = False
    end
    object DataSetInvoiceAddedItemsQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object DataSetInvoiceAddedItemsT: TFIBSmallIntField
      FieldName = 'T'
    end
    object DataSetInvoiceAddedItemsOPERID: TFIBStringField
      FieldName = 'OPERID'
      Size = 10
      EmptyStrToNull = False
    end
    object DataSetInvoiceAddedItemsOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = False
    end
    object DataSetInvoiceAddedItemsU: TFIBSmallIntField
      FieldName = 'U'
    end
    object DataSetInvoiceAddedItemsCOMMENTID: TFIBSmallIntField
      FieldName = 'COMMENTID'
    end
    object DataSetInvoiceAddedItemsCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 80
      EmptyStrToNull = False
    end
    object DataSetInvoiceAddedItemsQ1: TFIBIntegerField
      FieldName = 'Q1'
    end
  end
  object DataSetInvoiceRemovedItems: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AEl set '
      '  Q=:Q,'
      '  COMMENT=:COMMENT'
      'where ID=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into AEl(ID, INVID, ARTID, ART2ID, SZ, Q, T, OPERID, U, C' +
        'OMMENTID, COMMENT)'
      
        'values(:ID, :INVID, :ARTID, :ART2ID, :SZID, :Q, :T, :OPERID, :U,' +
        ' :COMMENTID, :COMMENT)')
    SelectSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T, '
      '  OPERID, OPERATION, U, COMMENTID, COMMENT, Q1'
      'from AEl_S(:INVID)')
    BeforeOpen = DataSetInvoiceRemovedItemsBeforeOpen
    BeforePost = DataSetInvoiceRemovedItemsBeforePost
    OnNewRecord = DataSetInvoiceRemovedItemsNewRecord
    Transaction = Transaction
    Database = dm.db
    Left = 176
    Top = 128
    oStartTransaction = False
    object DataSetInvoiceRemovedItemsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetInvoiceRemovedItemsINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object DataSetInvoiceRemovedItemsARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object DataSetInvoiceRemovedItemsART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object DataSetInvoiceRemovedItemsSZID: TFIBIntegerField
      FieldName = 'SZID'
    end
    object DataSetInvoiceRemovedItemsART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = False
    end
    object DataSetInvoiceRemovedItemsART2: TFIBStringField
      FieldName = 'ART2'
      Size = 80
      EmptyStrToNull = False
    end
    object DataSetInvoiceRemovedItemsSZ: TFIBStringField
      FieldName = 'SZ'
      EmptyStrToNull = False
    end
    object DataSetInvoiceRemovedItemsQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object DataSetInvoiceRemovedItemsT: TFIBSmallIntField
      FieldName = 'T'
    end
    object DataSetInvoiceRemovedItemsOPERID: TFIBStringField
      FieldName = 'OPERID'
      Size = 10
      EmptyStrToNull = False
    end
    object DataSetInvoiceRemovedItemsOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = False
    end
    object DataSetInvoiceRemovedItemsU: TFIBSmallIntField
      FieldName = 'U'
    end
    object DataSetInvoiceRemovedItemsCOMMENTID: TFIBSmallIntField
      FieldName = 'COMMENTID'
    end
    object DataSetInvoiceRemovedItemsCOMMENT: TFIBStringField
      FieldName = 'COMMENT'
      Size = 80
      EmptyStrToNull = False
    end
    object DataSetInvoiceRemovedItemsQ1: TFIBIntegerField
      FieldName = 'Q1'
    end
  end
  object DataSetSubStorageItems: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AEl set  '
      '  Q=:Q'
      'where Id=:OLD_Id')
    InsertSQL.Strings = (
      'insert into AEl(ID, INVID, ARTID, ART2ID, SZ, Q, T, OPERID, '
      '  OPERIDFROM, OLD_ART2ID, U, OLD_U)'
      'values(:ID, :INVID, :ARTID, :ART2ID, :SZID, :Q, :T, :OPERID, '
      '  :OPERIDFROM, :ART2ID_OLD, :U, :U_OLD)')
    SelectSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      '  OPERID, OPERATION, OPERIDFROM, OPERATIONFROM,'
      '  ART2ID_OLD, ART2_OLD, U, U_OLD, Q1, PATTERN'
      'from AEl_S(:INVID)')
    BeforeOpen = DataSetSubStorageItemsBeforeOpen
    BeforePost = DataSetSubStorageItemsBeforePost
    OnNewRecord = DataSetSubStorageItemsNewRecord
    Transaction = Transaction
    Database = dm.db
    Left = 208
    Top = 128
    oStartTransaction = False
    object DataSetSubStorageItemsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetSubStorageItemsINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object DataSetSubStorageItemsARTID: TFIBIntegerField
      FieldName = 'ARTID'
    end
    object DataSetSubStorageItemsART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
    object DataSetSubStorageItemsSZID: TFIBIntegerField
      FieldName = 'SZID'
    end
    object DataSetSubStorageItemsART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = False
    end
    object DataSetSubStorageItemsART2: TFIBStringField
      FieldName = 'ART2'
      Size = 80
      EmptyStrToNull = False
    end
    object DataSetSubStorageItemsSZ: TFIBStringField
      FieldName = 'SZ'
      EmptyStrToNull = False
    end
    object DataSetSubStorageItemsQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object DataSetSubStorageItemsT: TFIBSmallIntField
      FieldName = 'T'
    end
    object DataSetSubStorageItemsOPERID: TFIBStringField
      FieldName = 'OPERID'
      Size = 10
      EmptyStrToNull = False
    end
    object DataSetSubStorageItemsOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = False
    end
    object DataSetSubStorageItemsOPERIDFROM: TFIBStringField
      FieldName = 'OPERIDFROM'
      Size = 10
      EmptyStrToNull = False
    end
    object DataSetSubStorageItemsOPERATIONFROM: TFIBStringField
      FieldName = 'OPERATIONFROM'
      Size = 60
      EmptyStrToNull = False
    end
    object DataSetSubStorageItemsART2ID_OLD: TFIBIntegerField
      FieldName = 'ART2ID_OLD'
    end
    object DataSetSubStorageItemsART2_OLD: TFIBStringField
      FieldName = 'ART2_OLD'
      EmptyStrToNull = False
    end
    object DataSetSubStorageItemsU: TFIBSmallIntField
      FieldName = 'U'
    end
    object DataSetSubStorageItemsU_OLD: TFIBSmallIntField
      FieldName = 'U_OLD'
    end
    object DataSetSubStorageItemsQ1: TFIBIntegerField
      FieldName = 'Q1'
    end
    object DataSetSubStorageItemsPATTERN: TFIBStringField
      FieldName = 'PATTERN'
      Size = 4000
      EmptyStrToNull = False
    end
  end
  object TransactionTmp: TpFIBTransaction
    DefaultDatabase = dm.db
    TimeoutAction = TARollback
    Left = 304
    Top = 128
  end
  object StyleRepository: TcxStyleRepository
    Left = 256
    Top = 24
    object cxStyle1: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnFace
    end
    object cxStyle2: TcxStyle
      AssignedValues = [svColor]
      Color = clRed
    end
    object cxStyle3: TcxStyle
      AssignedValues = [svColor]
      Color = clYellow
    end
    object cxStyle4: TcxStyle
      AssignedValues = [svColor]
      Color = clLime
    end
    object cxStyle5: TcxStyle
      AssignedValues = [svColor]
      Color = clMoneyGreen
    end
  end
  object DBDataArticlesMaster: TpFIBDataSet
    SelectSQL.Strings = (
      'select x.*,(0) MaxQ'
      'from CHANGE$ASSORTMENT$MASTER(:DemandID) x')
    Transaction = Transaction
    Database = dm.db
    Left = 312
    Top = 264
    oRefreshAfterPost = False
    oFetchAll = True
    object DBDataArticlesMasterJAID: TFIBIntegerField
      FieldName = 'JAID'
    end
    object DBDataArticlesMasterAARTID: TFIBIntegerField
      FieldName = 'AARTID'
    end
    object DBDataArticlesMasterAART: TFIBStringField
      FieldName = 'AART'
      EmptyStrToNull = False
    end
    object DBDataArticlesMasterAART2ID: TFIBIntegerField
      FieldName = 'AART2ID'
    end
    object DBDataArticlesMasterASZID: TFIBIntegerField
      FieldName = 'ASZID'
    end
    object DBDataArticlesMasterASZNAME: TFIBStringField
      FieldName = 'ASZNAME'
      EmptyStrToNull = False
    end
    object DBDataArticlesMasterAQ: TFIBIntegerField
      FieldName = 'AQ'
    end
    object DBDataArticlesMasterAFQ: TFIBIntegerField
      FieldName = 'AFQ'
    end
    object DBDataArticlesMasterAOPERID: TFIBStringField
      FieldName = 'AOPERID'
      Size = 10
      EmptyStrToNull = False
    end
    object DBDataArticlesMasterAOPERNAME: TFIBStringField
      FieldName = 'AOPERNAME'
      Size = 60
      EmptyStrToNull = False
    end
    object DBDataArticlesMasterAUNITID: TFIBIntegerField
      FieldName = 'AUNITID'
    end
    object DBDataArticlesMasterAUNITNAME: TFIBStringField
      FieldName = 'AUNITNAME'
      Size = 10
      EmptyStrToNull = False
    end
    object DBDataArticlesMasterQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object DBDataArticlesMasterMAXQ: TFIBIntegerField
      FieldName = 'MAXQ'
    end
  end
  object DBDataArticlesDetail: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from CHANGE$ASSORTMENT$Detail(:DemandID, :SourceStorageID)')
    Transaction = Transaction
    Database = dm.db
    Left = 312
    Top = 232
    oRefreshAfterPost = False
    oFetchAll = True
    object DBDataArticlesDetailJAID: TFIBIntegerField
      FieldName = 'JAID'
    end
    object DBDataArticlesDetailWHARTID: TFIBIntegerField
      FieldName = 'WHARTID'
    end
    object DBDataArticlesDetailWHART: TFIBStringField
      FieldName = 'WHART'
      EmptyStrToNull = False
    end
    object DBDataArticlesDetailWHART2ID: TFIBIntegerField
      FieldName = 'WHART2ID'
    end
    object DBDataArticlesDetailWHSZID: TFIBIntegerField
      FieldName = 'WHSZID'
    end
    object DBDataArticlesDetailWHSZNAME: TFIBStringField
      FieldName = 'WHSZNAME'
      EmptyStrToNull = False
    end
    object DBDataArticlesDetailWHOPERID: TFIBStringField
      FieldName = 'WHOPERID'
      Size = 10
      EmptyStrToNull = False
    end
    object DBDataArticlesDetailWHOPERNAME: TFIBStringField
      FieldName = 'WHOPERNAME'
      Size = 60
      EmptyStrToNull = False
    end
    object DBDataArticlesDetailWHUNITID: TFIBIntegerField
      FieldName = 'WHUNITID'
    end
    object DBDataArticlesDetailWHUNITNAME: TFIBStringField
      FieldName = 'WHUNITNAME'
      Size = 10
      EmptyStrToNull = False
    end
    object DBDataArticlesDetailWHQ: TFIBIntegerField
      FieldName = 'WHQ'
    end
    object DBDataArticlesDetailQ: TFIBIntegerField
      FieldName = 'Q'
    end
  end
  object DataArticlesMaster: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'DEMANDID'
        ParamType = ptInputOutput
      end>
    BeforeOpen = DataArticlesMasterBeforeOpen
    BeforeEdit = DataArticlesMasterBeforeEdit
    BeforePost = DataArticlesMasterBeforePost
    AfterPost = DataArticlesMasterAfterPost
    OnCalcFields = DataArticlesMasterCalcFields
    Left = 408
    Top = 264
    object DataArticlesMasterJAID: TIntegerField
      FieldName = 'JAID'
    end
    object DataArticlesMasterAARTID: TIntegerField
      FieldName = 'AARTID'
    end
    object DataArticlesMasterAART: TStringField
      FieldName = 'AART'
    end
    object DataArticlesMasterAART2ID: TIntegerField
      FieldName = 'AART2ID'
    end
    object DataArticlesMasterASZID: TIntegerField
      FieldName = 'ASZID'
    end
    object DataArticlesMasterASZNAME: TStringField
      FieldName = 'ASZNAME'
    end
    object DataArticlesMasterAQ: TIntegerField
      FieldName = 'AQ'
    end
    object DataArticlesMasterAFQ: TIntegerField
      FieldName = 'AFQ'
    end
    object DataArticlesMasterAOPERID: TStringField
      FieldName = 'AOPERID'
      Size = 10
    end
    object DataArticlesMasterAOPERNAME: TStringField
      FieldName = 'AOPERNAME'
      Size = 60
    end
    object DataArticlesMasterAUNITID: TIntegerField
      FieldName = 'AUNITID'
    end
    object DataArticlesMasterAUNITNAME: TStringField
      FieldName = 'AUNITNAME'
      Size = 10
    end
    object DataArticlesMasterQ: TIntegerField
      FieldName = 'Q'
    end
    object DataArticlesMasterMaxQ: TIntegerField
      FieldName = 'MaxQ'
    end
    object DataArticlesMasterMoreQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'MoreQ'
      Calculated = True
    end
  end
  object SourceArticlesMaster: TDataSource
    DataSet = DataArticlesMaster
    Left = 344
    Top = 264
  end
  object ProviderArticlesMaster: TDataSetProvider
    DataSet = DBDataArticlesMaster
    Constraints = True
    Left = 376
    Top = 264
  end
  object SourceArticlesDetail: TDataSource
    DataSet = DataArticlesDetail
    Left = 344
    Top = 232
  end
  object DataArticlesDetail: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'JAID'
        DataType = ftInteger
      end
      item
        Name = 'WHARTID'
        DataType = ftInteger
      end
      item
        Name = 'WHART'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'WHART2ID'
        DataType = ftInteger
      end
      item
        Name = 'WHSZID'
        DataType = ftInteger
      end
      item
        Name = 'WHSZNAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'WHOPERID'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'WHOPERNAME'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'WHUNITID'
        DataType = ftInteger
      end
      item
        Name = 'WHUNITNAME'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'WHQ'
        DataType = ftInteger
      end
      item
        Name = 'Q'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'DEMANDID'
        ParamType = ptInputOutput
      end
      item
        DataType = ftInteger
        Name = 'SOURCESTORAGEID'
        ParamType = ptInputOutput
      end>
    StoreDefs = True
    BeforeOpen = DataArticlesDetailBeforeOpen
    Left = 408
    Top = 232
    object DataArticlesDetailJAID: TIntegerField
      FieldName = 'JAID'
    end
    object DataArticlesDetailWHARTID: TIntegerField
      FieldName = 'WHARTID'
    end
    object DataArticlesDetailWHART: TStringField
      FieldName = 'WHART'
    end
    object DataArticlesDetailWHART2ID: TIntegerField
      FieldName = 'WHART2ID'
    end
    object DataArticlesDetailWHSZID: TIntegerField
      FieldName = 'WHSZID'
    end
    object DataArticlesDetailWHSZNAME: TStringField
      FieldName = 'WHSZNAME'
    end
    object DataArticlesDetailWHOPERID: TStringField
      FieldName = 'WHOPERID'
      Size = 10
    end
    object DataArticlesDetailWHOPERNAME: TStringField
      FieldName = 'WHOPERNAME'
      Size = 60
    end
    object DataArticlesDetailWHUNITID: TIntegerField
      FieldName = 'WHUNITID'
    end
    object DataArticlesDetailWHUNITNAME: TStringField
      FieldName = 'WHUNITNAME'
      Size = 10
    end
    object DataArticlesDetailWHQ: TIntegerField
      FieldName = 'WHQ'
    end
    object DataArticlesDetailQ: TIntegerField
      FieldName = 'Q'
    end
  end
  object ProviderArticlesDetail: TDataSetProvider
    DataSet = DBDataArticlesDetail
    Constraints = True
    Left = 376
    Top = 232
  end
  object Detail: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'JAID'
        DataType = ftInteger
      end
      item
        Name = 'WHARTID'
        DataType = ftInteger
      end
      item
        Name = 'WHART'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'WHART2ID'
        DataType = ftInteger
      end
      item
        Name = 'WHSZID'
        DataType = ftInteger
      end
      item
        Name = 'WHSZNAME'
        DataType = ftString
        Size = 20
      end
      item
        Name = 'WHOPERID'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'WHOPERNAME'
        DataType = ftString
        Size = 60
      end
      item
        Name = 'WHUNITID'
        DataType = ftInteger
      end
      item
        Name = 'WHUNITNAME'
        DataType = ftString
        Size = 10
      end
      item
        Name = 'WHQ'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 440
    Top = 232
    object DetailJAID: TIntegerField
      FieldName = 'JAID'
    end
    object DetailWHARTID: TIntegerField
      FieldName = 'WHARTID'
    end
    object DetailWHART: TStringField
      FieldName = 'WHART'
    end
    object DetailWHART2ID: TIntegerField
      FieldName = 'WHART2ID'
    end
    object DetailWHSZID: TIntegerField
      FieldName = 'WHSZID'
    end
    object DetailWHSZNAME: TStringField
      FieldName = 'WHSZNAME'
    end
    object DetailWHOPERID: TStringField
      FieldName = 'WHOPERID'
      Size = 10
    end
    object DetailWHOPERNAME: TStringField
      FieldName = 'WHOPERNAME'
      Size = 60
    end
    object DetailWHUNITID: TIntegerField
      FieldName = 'WHUNITID'
    end
    object DetailWHUNITNAME: TStringField
      FieldName = 'WHUNITNAME'
      Size = 10
    end
    object DetailWHQ: TIntegerField
      FieldName = 'WHQ'
    end
    object DetailQ: TIntegerField
      FieldName = 'Q'
    end
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 440
    Top = 264
    object MasterJAID: TIntegerField
      FieldName = 'JAID'
    end
    object MasterAARTID: TIntegerField
      FieldName = 'AARTID'
    end
    object MasterAART: TStringField
      FieldName = 'AART'
    end
    object MasterAART2ID: TIntegerField
      FieldName = 'AART2ID'
    end
    object MasterASZID: TIntegerField
      FieldName = 'ASZID'
    end
    object MasterASZNAME: TStringField
      FieldName = 'ASZNAME'
    end
    object MasterAQ: TIntegerField
      FieldName = 'AQ'
    end
    object MasterAFQ: TIntegerField
      FieldName = 'AFQ'
    end
    object MasterAOPERID: TStringField
      FieldName = 'AOPERID'
      Size = 10
    end
    object MasterAOPERNAME: TStringField
      FieldName = 'AOPERNAME'
      Size = 60
    end
    object MasterAUNITID: TIntegerField
      FieldName = 'AUNITID'
    end
    object MasterAUNITNAME: TStringField
      FieldName = 'AUNITNAME'
      Size = 10
    end
    object MasterQ: TIntegerField
      FieldName = 'Q'
    end
    object MasterMaxQ: TIntegerField
      FieldName = 'MaxQ'
    end
  end
end
