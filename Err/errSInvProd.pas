unit errSInvProd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList, ExtCtrls, ComCtrls,
  Grids, DBGridEh, ActnList, DBGridEhGrouping, GridsEh, rxPlacemnt,
  rxSpeedbar;

type
  TfmErrSInvProd = class(TfmAncestor)
    gridSinvProd: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmErrSInvProd: TfmErrSInvProd;

implementation

uses MainData, dbUtil;

{$R *.dfm}

procedure TfmErrSInvProd.FormCreate(Sender: TObject);
begin
  inherited;
  with dmMain, taErrSInvProd do
  begin
    if Transaction.Active then Transaction.Commit;
    Transaction.StartTransaction;
    OpenDataSet(taErrSInvProd);
  end;
end;

procedure TfmErrSInvProd.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  with dmMain, taErrSInvProd do
  begin
    CloseDataSet(taErrSInvProd);
    if Transaction.Active then Transaction.Commit;
  end;
  inherited;
end;

end.
