unit errWhSemis;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList, ExtCtrls, ComCtrls,
  Grids, DBGridEh, ActnList, DBGridEhGrouping, GridsEh, rxPlacemnt,
  rxSpeedbar;

type
  TfmErrWhSemis = class(TfmAncestor)
    gridWhSemis: TDBGridEh;
    spitDoIt: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spitDoItClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmErrWhSemis: TfmErrWhSemis;

implementation

uses MainData, dbUtil, DictData;

{$R *.dfm}

procedure TfmErrWhSemis.FormCreate(Sender: TObject);
begin
  inherited;
  Application.ProcessMessages;
  if dmMain.taErrWhSemis.Transaction.Active then dmMain.taErrWhSemis.Transaction.Commit;
  dmMain.taErrWhSemis.Transaction.StartTransaction;
  ExecSQL('execute procedure Build_WhSemis("01.04.2003", "01.01.2050")', dmMain.quTmp);
  dmMain.taErrWhSemis.ParamByName('T').AsInteger := 0;
  OpenDataSet(dmMain.taErrWhSemis);
end;

procedure TfmErrWhSemis.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CloseDataSet(dmMain.taErrWhSemis);
  if dmMain.taErrWhSemis.Transaction.Active then dmMain.taErrWhSemis.Transaction.Commit;
end;

procedure TfmErrWhSemis.spitDoItClick(Sender: TObject);
begin
  CloseDataSet(dmMain.taErrWhSemis);
  //dmMain.taErrWhSemis.Transaction.Commit;
  dmMain.taErrWhSemis.ParamByName('T').AsInteger := 1;
  //dmMain.taErrWhSemis.Transaction.StartTransaction;
  OpenDataSet(dmMain.taErrWhSemis);
  dm.tr.CommitRetaining;
end;

end.
