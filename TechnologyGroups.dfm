object fmTechnologyGroups: TfmTechnologyGroups
  Left = 0
  Top = 156
  Caption = #1058#1077#1093#1085#1086#1083#1086#1075#1080#1095#1077#1089#1082#1080#1077' '#1082#1072#1088#1090#1099
  ClientHeight = 752
  ClientWidth = 1255
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesigned
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar: TdxStatusBar
    Left = 0
    Top = 732
    Width = 1255
    Height = 20
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Bevel = dxpbRaised
        Width = 465
      end
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Bevel = dxpbRaised
        BiDiMode = bdLeftToRight
        MinWidth = 100
        ParentBiDiMode = False
        Width = 100
      end>
    PaintStyle = stpsOffice11
    LookAndFeel.Kind = lfOffice11
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object GridGroups: TcxGrid
    Left = 0
    Top = 56
    Width = 470
    Height = 676
    Align = alClient
    PopupMenu = gridGroupsPopupMenu
    TabOrder = 1
    LookAndFeel.Kind = lfOffice11
    object GroupsView: TcxGridDBTableView
      OnDblClick = GroupsViewDblClick
      NavigatorButtons.ConfirmDelete = False
      FilterBox.Visible = fvNever
      DataController.DataSource = SourceGroups
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      FilterRow.ApplyChanges = fracImmediately
      OptionsBehavior.CellHints = True
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103'>'
      OptionsView.GroupByBox = False
      object GroupsViewColumn1: TcxGridDBColumn
        AlternateCaption = #1059#1085#1080#1082#1072#1083#1100#1085#1099#1081' '#1085#1086#1084#1077#1088' '#1082#1072#1088#1090#1099
        Caption = #8470' '#1082#1072#1088#1090#1099' '
        DataBinding.FieldName = 'ID'
        Options.Editing = False
        Width = 55
      end
      object GroupsViewColumn2: TcxGridDBColumn
        AlternateCaption = #1048#1084#1103' '#1082#1072#1088#1090#1099
        Caption = #1053#1072#1079#1074#1072#1085#1080#1077
        DataBinding.FieldName = 'NAME'
        Width = 289
      end
      object GroupsViewColumn13: TcxGridDBColumn
        AlternateCaption = #1055#1086#1079#1080#1094#1080#1103' '#1082#1072#1088#1090#1099' '#1074' '#1074#1099#1089#1090#1072#1074#1086#1095#1085#1086#1084' '#1087#1088#1072#1081#1089'-'#1083#1080#1089#1090#1077
        Caption = #1055#1088#1072#1081#1089' '#8470
        DataBinding.FieldName = 'PRICE$POSITION'
        SortIndex = 0
        SortOrder = soAscending
        Width = 74
      end
      object GroupsViewColumn14: TcxGridDBColumn
        Caption = #1052#1072#1090#1077#1088#1080#1072#1083
        DataBinding.FieldName = 'GROUP$MATERIAL$ID'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAME'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = dm.dsrMat
      end
      object GroupsViewColumn3: TcxGridDBColumn
        AlternateCaption = #1057#1088#1077#1076#1085#1080#1081' '#1074#1077#1089' '#1080#1079#1076#1077#1083#1080#1103' '#1090#1077#1093'.'#1082#1072#1088#1090#1099
        Caption = #1057#1088'. '#1074#1077#1089
        DataBinding.FieldName = 'AVERAGE$WEIGHT'
        Options.Editing = False
        Width = 48
      end
      object GroupsViewColumn4: TcxGridDBColumn
        AlternateCaption = 
          '% '#1080#1079#1076#1077#1083#1080#1081' '#1090#1077#1093'. '#1082#1072#1088#1090#1099' '#1086#1090' '#1086#1073#1097#1077#1075#1086' '#1082#1086#1083'-'#1074#1072' '#1075#1086#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080' '#1079#1072' '#1087#1077#1088#1080#1086 +
          #1076
        Caption = #1050#1086#1083'-'#1074#1086', %'
        DataBinding.FieldName = 'QUANTITY$PART'
        Options.Editing = False
        Width = 59
      end
      object GroupsViewColumn5: TcxGridDBColumn
        AlternateCaption = '% '#1080#1079#1076#1077#1083#1080#1081' '#1090#1077#1093'. '#1082#1072#1088#1090#1099' '#1086#1090' '#1086#1073#1097#1077#1075#1086' '#1074#1077#1089#1072' '#1075#1086#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080' '#1079#1072' '#1087#1077#1088#1080#1086#1076
        Caption = #1042#1077#1089', %'
        DataBinding.FieldName = 'WEIGHT$PART'
        Options.Editing = False
        Width = 46
      end
      object GroupsViewColumn6: TcxGridDBColumn
        AlternateCaption = 
          #1057#1091#1084#1084#1072#1088#1085#1099#1081' '#1087#1086' '#1086#1087#1077#1088#1072#1094#1080#1103#1084' '#1090#1077#1093'. '#1082#1072#1088#1090#1099' '#1087#1088#1086#1094#1077#1085#1090' '#1074#1086#1079#1074#1088#1072#1090#1072' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086 +
          #1074' '#1074#1080#1076#1072' "'#1086#1090#1093#1086#1076#1099'" '#1086#1090' '#1089#1076#1072#1085#1085#1086#1075#1086' '#1087#1086' '#1085#1072#1088#1103#1076#1091
        Caption = #1054#1090#1093#1086#1076#1099', %'
        DataBinding.FieldName = 'WASTE$PERCENT'
        Options.Editing = False
        Width = 64
      end
      object GroupsViewColumn7: TcxGridDBColumn
        AlternateCaption = #1042#1099#1087#1083#1072#1090#1072' '#1079'/'#1087' '#1074' '#1088#1091#1073#1083#1103#1093' '#1085#1072' 1 '#1075#1088#1072#1084#1084' '#1075#1086#1090#1086#1074#1086#1075#1086' '#1087#1088#1086#1076#1091#1082#1090#1072
        Caption = #1056#1072#1073#1086#1090#1099', '#1088#1091#1073'.'
        DataBinding.FieldName = 'WORKS$COST'
        Options.Editing = False
        Width = 69
      end
      object GroupsViewColumn8: TcxGridDBColumn
        AlternateCaption = #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1089#1074#1077#1088#1093#1085#1086#1088#1084#1072#1090#1080#1074#1085#1099#1093' '#1087#1086#1090#1077#1088#1100' '#1074' '#1088#1091#1073#1083#1103#1093
        Caption = #1055#1086#1090#1077#1088#1080', '#1088#1091#1073'.'
        DataBinding.FieldName = 'OVERNORM$LOSSES$COST'
        Options.Editing = False
        Width = 67
      end
      object GroupsViewColumn9: TcxGridDBColumn
        AlternateCaption = 
          #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1074#1089#1077#1093' '#1089#1090#1086#1088#1086#1085#1085#1080#1093' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074', '#1091#1095#1072#1074#1089#1090#1074#1091#1102#1097#1080#1093' '#1074' '#1089#1086#1079#1076#1072#1085#1080#1080 +
          ' '#1080#1079#1076#1077#1083#1080#1103
        Caption = #1055#1060', '#1088#1091#1073'.'
        DataBinding.FieldName = 'FM$COST'
        Options.Editing = False
        Width = 50
      end
      object GroupsViewColumn10: TcxGridDBColumn
        AlternateCaption = 
          #1055#1088#1086#1094#1077#1085#1090', '#1082#1086#1090#1086#1088#1099#1081' '#1079#1072#1085#1080#1084#1072#1102#1090' '#1089#1090#1086#1088#1086#1085#1085#1080#1077' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1099' '#1086#1090' '#1074#1077#1089#1072' '#1080#1079#1076#1077#1083#1080 +
          #1103
        Caption = #1055#1060', %'
        DataBinding.FieldName = 'SUMMARY$WEIGHT$PERCENT'
        Options.Editing = False
        Width = 51
      end
      object GroupsViewColumn12: TcxGridDBColumn
        AlternateCaption = 
          #1057#1090#1086#1080#1084#1086#1089#1090#1100' '#1089#1090#1086#1088#1086#1085#1085#1080#1093' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074' - '#1089#1090#1086#1080#1084#1086#1089#1090#1100' "'#1074#1099#1090#1077#1089#1085#1077#1085#1085#1086#1075#1086'" '#1080#1084 +
          #1080' '#1079#1086#1083#1086#1090#1072' '#1080#1079' '#1080#1079#1076#1077#1083#1080#1103
        Caption = #1055#1060' - %'
        DataBinding.FieldName = 'FM$CLEAR$COST'
        Options.Editing = False
        Width = 50
      end
      object GroupsViewColumn11: TcxGridDBColumn
        AlternateCaption = #1048#1090#1086#1075#1086#1074#1072#1103' '#1089#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1100' '#1075#1088#1072#1084#1084#1072' '#1080#1079#1076#1077#1083#1080#1103' '#1073#1077#1079' '#1091#1095#1105#1090#1072' '#1089#1090#1086#1080#1084#1086#1089#1090#1080' '#1079#1086#1083#1086#1090#1072
        Caption = #1048#1090#1086#1075#1086', '#1088#1091#1073'.'
        DataBinding.FieldName = 'SELF$COST'
        Options.Editing = False
        Width = 62
      end
    end
    object ArticlesView: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = SourceGroupArticles
      DataController.DetailKeyFieldNames = 'GROUPID'
      DataController.KeyFieldNames = 'GROUPID'
      DataController.MasterKeyFieldNames = 'ID'
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsCustomize.ColumnFiltering = False
      OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103'>'
      OptionsView.GroupByBox = False
      object ArticlesViewName: TcxGridDBColumn
        Caption = #1040#1088#1090#1080#1082#1091#1083
        DataBinding.FieldName = 'ARTICLE$NAME'
        Options.Editing = False
        Width = 215
      end
    end
    object GroupsLevel: TcxGridLevel
      GridView = GroupsView
      object ArticlesLevel: TcxGridLevel
        GridView = ArticlesView
      end
    end
  end
  object GridOperations: TcxGrid
    Left = 478
    Top = 56
    Width = 777
    Height = 676
    Align = alRight
    TabOrder = 6
    LookAndFeel.Kind = lfOffice11
    object GridOperationsDBBandedTableView1: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103'>'
      OptionsView.GroupByBox = False
      Bands = <
        item
        end>
    end
    object OperationsView: TcxGridDBBandedTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataModeController.GridMode = True
      DataController.DataSource = SourceGroupOperations
      DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.OnDetailExpanding = OperationsViewDataControllerDetailExpanding
      Filtering.ColumnMRUItemsList = False
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.ImmediateEditor = False
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsData.Deleting = False
      OptionsData.Inserting = False
      OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103'>'
      OptionsView.GroupByBox = False
      Bands = <
        item
          Width = 694
        end>
      object OperationsViewColumn1: TcxGridDBBandedColumn
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103
        DataBinding.FieldName = 'OPERATION$ID'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'OPERATION'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = SourceOperations
        Options.Filtering = False
        Options.Grouping = False
        Width = 154
        Position.BandIndex = 0
        Position.ColIndex = 0
        Position.RowIndex = 0
      end
      object OperationsViewColumn2: TcxGridDBBandedColumn
        Caption = #1055#1086#1088#1103#1076#1086#1082
        DataBinding.FieldName = 'OPERATION$ORDER'
        Width = 68
        Position.BandIndex = 0
        Position.ColIndex = 1
        Position.RowIndex = 0
      end
      object OperationsViewColumn3: TcxGridDBBandedColumn
        Caption = #1044#1086#1083#1103
        DataBinding.FieldName = 'QUOTA'
        Width = 57
        Position.BandIndex = 0
        Position.ColIndex = 2
        Position.RowIndex = 0
      end
      object OperationsViewColumn4: TcxGridDBBandedColumn
        Caption = #1053#1086#1088#1084#1072' '#1089#1098#1105#1084#1072
        DataBinding.FieldName = 'N'
        Options.Editing = False
        Options.Filtering = False
        Width = 85
        Position.BandIndex = 0
        Position.ColIndex = 3
        Position.RowIndex = 0
      end
      object OperationsViewColumn5: TcxGridDBBandedColumn
        Caption = #1053#1086#1088#1084#1072' '#1087#1086#1090#1077#1088#1100
        DataBinding.FieldName = 'NS'
        Options.Editing = False
        Options.Filtering = False
        Width = 78
        Position.BandIndex = 0
        Position.ColIndex = 4
        Position.RowIndex = 0
      end
      object OperationsViewColumn6: TcxGridDBBandedColumn
        Caption = #1057#1090#1072#1074#1082#1072' '#1079'/'#1087
        DataBinding.FieldName = 'RATE'
        Options.Editing = False
        Width = 102
        Position.BandIndex = 0
        Position.ColIndex = 5
        Position.RowIndex = 0
      end
      object OperationsViewColumn7: TcxGridDBBandedColumn
        Caption = #1048#1089#1082#1083#1102#1095#1077#1085#1085#1099#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
        DataBinding.FieldName = 'EXCLUDED$OPERATIONS'
        Width = 144
        Position.BandIndex = 0
        Position.ColIndex = 6
        Position.RowIndex = 0
      end
    end
    object SemisView: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataModeController.GridMode = True
      DataController.DataSource = SourceGroupSemis
      DataController.DetailKeyFieldNames = 'GROUP$OPERATIONS$ID'
      DataController.KeyFieldNames = 'GROUP$OPERATIONS$ID'
      DataController.MasterKeyFieldNames = 'ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      DataController.Summary.Options = [soNullIgnore]
      NewItemRow.InfoText = #1044#1086#1073#1072#1074#1080#1090#1100' '#1084#1072#1090#1077#1088#1080#1072#1083
      NewItemRow.Visible = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103'>'
      OptionsView.GroupByBox = False
      object SemisViewColumn1: TcxGridDBColumn
        Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
        DataBinding.FieldName = 'SEMIS$ID'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'SEMIS'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = SourceSemis
        Width = 156
      end
      object SemisViewColumn2: TcxGridDBColumn
        Caption = #1057#1090#1086#1080#1084#1086#1089#1090#1100
        DataBinding.FieldName = 'COST'
        Width = 96
      end
      object SemisViewColumn3: TcxGridDBColumn
        Caption = '% '#1086#1090' '#1074#1077#1089#1072
        DataBinding.FieldName = 'WEIGHT$PERCENT'
        Width = 82
      end
      object SemisViewColumn4: TcxGridDBColumn
        Caption = #1050#1086#1083'-'#1074#1086
        DataBinding.FieldName = 'QUANTITY'
        Width = 64
      end
      object SemisViewColumn5: TcxGridDBColumn
        Caption = #1045#1076'. '#1080#1079#1084#1077#1088#1077#1085#1080#1103
        DataBinding.FieldName = 'UNIT$ID'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.AutoSelect = False
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'UNITID'
        Properties.ListColumns = <
          item
            FieldName = 'U'
          end>
        Properties.ListOptions.ShowHeader = False
        Properties.ListSource = UnitSource
        Properties.OnEditValueChanged = SemisViewColumn5PropertiesEditValueChanged
        Width = 85
      end
    end
    object OperationsLevel: TcxGridLevel
      GridView = OperationsView
      object SemisLevel: TcxGridLevel
        GridView = SemisView
      end
    end
  end
  object Splitter: TcxSplitter
    Left = 470
    Top = 56
    Width = 8
    Height = 676
    HotZoneClassName = 'TcxMediaPlayer9Style'
    AlignSplitter = salRight
    AutoPosition = False
    PositionAfterOpen = 715
    MinSize = 100
    Control = GridOperations
  end
  object BarManager: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = SmallImages
    ImageOptions.LargeImages = ImageList1
    LookAndFeel.Kind = lfOffice11
    PopupMenuLinks = <>
    StoreInIniFile = True
    Style = bmsOffice11
    UseSystemFont = True
    Left = 8
    Top = 112
    DockControlHeights = (
      0
      0
      56
      0)
    object BarGroups: TdxBar
      AllowClose = False
      AllowQuickCustomizing = False
      Caption = #1052#1077#1085#1102
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsTop
      FloatLeft = 957
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'cxlbInfo'
        end
        item
          Visible = True
          ItemName = 'cxBarEditItem2'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 63
          Visible = True
          ItemName = 'GoldPriceEdit'
        end
        item
          Visible = True
          ItemName = 'cxBarEditItem1'
        end
        item
          UserDefine = [udWidth]
          UserWidth = 57
          Visible = True
          ItemName = 'SilverPriceEdit'
        end
        item
          Visible = True
          ItemName = 'dxBarButton13'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = acAdd
      Category = 0
      LargeImageIndex = 0
      AutoGrayScale = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = acDel
      Category = 0
      LargeImageIndex = 1
      AutoGrayScale = False
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = acCustomize
      Category = 0
      LargeImageIndex = 16
      AutoGrayScale = False
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = acExit
      Align = iaRight
      Category = 0
      LargeImageIndex = 6
      AutoGrayScale = False
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Caption = #1055#1077#1095#1072#1090#1100
      Category = 0
      Hint = #1055#1077#1095#1072#1090#1100
      Visible = ivAlways
      ButtonStyle = bsDropDown
      DropDownMenu = BarPopupMenu
      LargeImageIndex = 11
      AutoGrayScale = False
    end
    object dxBarButton1: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton3: TdxBarButton
      Action = acPrintTechnologyCard
      Category = 0
    end
    object dxBarButton4: TdxBarButton
      Action = acPrintCardsArticles
      Category = 0
    end
    object dxBarButton5: TdxBarButton
      Action = acPrintCardsWithOperation
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton7: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton8: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1057#1090#1086#1088#1086#1085#1085#1080#1081' '#1084#1072#1090#1077#1088#1080#1072#1083
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarButton10'
        end>
    end
    object dxBarButton9: TdxBarButton
      Action = acPrintForeignMaterialReport
      Category = 0
    end
    object dxBarButton10: TdxBarButton
      Action = acPrintForeignMaterialSemisReport
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton11: TdxBarButton
      Action = acPrintGroups
      Category = 0
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = acCalcCreationCosts
      Category = 0
      LargeImageIndex = 7
      AutoGrayScale = False
    end
    object cxLabel: TcxBarEditItem
      Category = 0
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxTextEditProperties'
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton12: TdxBarButton
      Action = acPrintSemisExpense
      Category = 0
    end
    object cxlbInfo: TcxBarEditItem
      Category = 0
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLabelProperties'
    end
    object GoldPriceEdit: TcxBarEditItem
      Category = 0
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxCurrencyEditProperties'
      InternalEditValue = 0c
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1062#1077#1085#1072' '#1079#1086#1083#1086#1090#1072': '
      Category = 0
      Hint = #1062#1077#1085#1072' '#1079#1086#1083#1086#1090#1072': '
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLabelProperties'
    end
    object dxBarButton13: TdxBarButton
      Action = acChangePrice
      Category = 0
      ImageIndex = 4
    end
    object dxBarButton14: TdxBarButton
      Action = acPrintOperationFrequency
      Category = 0
    end
    object dxBarButton15: TdxBarButton
      Action = acPrintPricereport
      Category = 0
    end
    object dxBarEdit1: TdxBarEdit
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      Width = 100
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = #1062#1077#1085#1072' '#1089#1077#1088#1077#1073#1088#1072':'
      Category = 0
      Hint = #1062#1077#1085#1072' '#1089#1077#1088#1077#1073#1088#1072':'
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxLabelProperties'
    end
    object SilverPriceEdit: TcxBarEditItem
      Category = 0
      Visible = ivAlways
      Width = 100
      PropertiesClassName = 'TcxCurrencyEditProperties'
      InternalEditValue = 0c
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton16: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton17: TdxBarButton
      Action = acPrintMissingArticles
      Category = 0
    end
    object dxBarButton18: TdxBarButton
      Caption = #1043#1083#1072#1074#1085#1099#1077' / '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
      Category = 0
      Hint = #1043#1083#1072#1074#1085#1099#1077' / '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
      Visible = ivAlways
    end
    object dxBarSubItem5: TdxBarSubItem
      Caption = #1075#1083#1072#1074#1085#1099#1077'/'#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton19: TdxBarButton
      Caption = #1058#1077#1093'. '#1082#1072#1088#1090#1099' '#1089' '#1085#1077#1089#1082#1086#1083#1100#1082#1080#1084#1080' '#1075#1083#1072#1074#1085#1099#1084#1080
      Category = 0
      Hint = #1058#1077#1093'. '#1082#1072#1088#1090#1099' '#1089' '#1085#1077#1089#1082#1086#1083#1100#1082#1080#1084#1080' '#1075#1083#1072#1074#1085#1099#1084#1080
      Visible = ivAlways
    end
    object dxBarButton20: TdxBarButton
      Caption = #1054#1090#1095#1077#1090' '#1087#1086' '#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1086#1084#1091' '#1089#1098#1077#1084#1091
      Category = 0
      Visible = ivAlways
      OnClick = acCalcAdditionalLossesExecute
    end
    object dxBarButton21: TdxBarButton
      Action = acCalcAdditionalLosses
      Category = 0
    end
  end
  object ImageList1: TImageList
    Height = 32
    Width = 32
    Left = 8
    Top = 144
    Bitmap = {
      494C010114001800040020002000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000080000000C000000001002000000000000080
      0100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FAEEDBFFF2D4
      A3FFFCFCFAFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FBF2E4FFFBF2
      E2FFFBF2E2FFFBF2E2FFFBF2E2FFFBF2E2FFFBF2E2FFFBF2E2FFFBF2E2FFFBF2
      E2FFFBF2E2FFFBF3E6FF00000000000000000000000000000000F7F7F8FFE4E4
      F3FFE2E2F2FFE2E2F2FFE2E2F2FFE2E2F2FFE2E2F2FFE2E2F2FFE2E2F2FFE2E2
      F2FFE2E2F2FFE2E2F2FFF3F3F9FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFEFEFFEDDFC9FFE0981CFFE397
      16FFE6AF50FFEFEAE1FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F3F3F3FF74A984FF5D926DFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFDE90
      08FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE90
      08FFDE9008FFDE9008FF00000000000000000000000000000000B0B0BEFF0611
      9DFF06119DFF06119DFF06119DFF06119DFF06119DFF06119DFF06119DFF0611
      9DFF06119DFF06119DFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EDDFC9FFE2A030FFF0AD3CFFFABF
      5CFFE59D22FFE6AF50FFFCFCFAFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000090BA9DFF128637FF278545FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9E1
      9DFFFFF2BDFFFFF2BDFFFFF2BDFFFFF2BDFFFFF2BDFFFFF2BDFFFFF2BDFFFFF2
      BDFFF9E19DFFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF2D76F3FF3282F6FF388EF8FF3B96FAFF3C96FAFF3890F8FF3384F6FF2E79
      F4FF2869F1FF1840CEFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FEFEFEFFEDDFC9FFE0981CFFF0AD3CFFFFC76AFFFFC7
      6AFFFABF5CFFE39716FFF2D4A3FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFCFFB1CEBAFF1F9044FF399D58FF309650FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9DC
      93FFFFECB0FFFFECB0FFFFECB0FFFFECB0FFFFECB0FFFFECB0FFFFECB0FFFFEC
      B0FFF9DC93FFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF2B6FF2FF2F79F4FF3384F6FF3689F7FF3689F7FF3485F6FF307BF4FF2C72
      F2FF2663EFFF163CCDFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EDDFC9FFE2A030FFF0AD3CFFFDC465FFFFC76AFFFFC7
      6AFFF0AD3CFFE0981CFFFAEEDBFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C7DCCDFF279148FF3B9D5AFF80CA98FF309650FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9D2
      81FFFFE19CFFFFE19CFFFFE19CFFFFE19CFFFFE19CFFFFE19CFFFFE19CFFFFE1
      9CFFF9D281FFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF2664EFFF296DF1FF2D75F3FF2F79F4FF2F79F4FF2E76F3FF2A6EF2FF2766
      F0FF225AEDFF1437CCFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FEFEFEFFEDDFC9FFE0981CFFF0AD3CFFFFC76AFFFFC76AFFFDC465FFF0AD
      3CFFE2A030FFEDDFC9FF00000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DCDF
      DDFF229045FF4BA667FFA5DCB6FF94D4A8FF309650FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9CA
      70FFFFD687FFFFD687FFFFD687FFFFD687FFFFD687FFFFD687FFFFD687FFFFD6
      87FFF9CA70FFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF225AEDFF2562EFFF2868F0FF296BF1FF296CF1FF2869F1FF2563EFFF225C
      EEFF1E51EBFF1131CBFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EDDFC9FFE2A030FFF0AD3CFFFDC465FFFFC76AFFFFC76AFFF0AD3CFFE098
      1CFFEDDFC9FFFEFEFEFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E3E6E3FF4E9B
      66FF44A161FFA2D7B2FFB1F8CCFF98D4ABFF29914AFF309650FF309650FF3096
      50FF309650FF309650FF309650FF309650FF309650FF309650FF309650FF2887
      46FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9BF
      5CFFFFC96EFFFFC96EFFFFC96EFFFFC96EFFFFC96EFFFFC96EFFFFC96EFFFFC9
      6EFFF9BF5CFFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF1C4CEAFF1F53ECFF2158EDFF225BEEFF225BEEFF2159EDFF1F54ECFF1C4E
      EBFF1844E9FF0E29C9FF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FEFEFEFFEDDF
      C9FFE0981CFFF0AD3CFFFFC76AFFFFC76AFFFDC465FFF0AD3CFFE2A030FFEDDF
      C9FF000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F5F6F5FF4AA366FF3C9C
      5AFFACDABBFF7EF3ABFF96F5BBFFD8F4E1FF9BD4ADFF99D4ACFF96D4A9FF94D4
      A8FF90D4A6FF8ED4A5FF8BD4A2FF89D4A1FF86D49FFF84D49DFF6CC589FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9BD
      58FFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC7
      6AFFF9BD58FFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF1741E8FF1946E9FF1B4BEAFF1D4EEBFF1D4EEBFF1B4CEAFF1A47E9FF1842
      E9FF143BE7FF0E27C9FF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EDDFC9FFE2A0
      30FFF0AD3CFFFDC465FFFFC76AFFFFC76AFFF0AD3CFFE0981CFFEDDFC9FFFEFE
      FEFF000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F5F6F5FF80B791FF349854FFA3D0
      B1FFA5F6C5FF4EEE8CFF9CF6BFFFF2FFF6FFEEFFF3FFEBFFF1FFE6FFEEFFE3FF
      ECFFDEFFE9FFDBFFE6FFD7FFE4FFD3FFE1FFCFFFDEFFCCFFDCFFA6E9BCFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFF9BD
      58FFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC76AFFFFC7
      6AFFF9BD58FFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF163EE8FF143BE7FF153CE7FF153DE8FF153DE8FF153CE7FF143BE7FF153D
      E8FF1D4EEBFF1943CEFF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFEFEFFEDDFC9FFE0981CFFF0AD
      3CFFFFC76AFFFFC76AFFFDC465FFF0AD3CFFE2A030FFEDDFC9FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FCFCFCFF7CBC90FF2E954EFF6DB583FFC6F9
      DAFF50EF8DFF4EEE8CFF8DF4B5FFCDFADEFFCAFADCFFC7FADAFFC2FAD7FFBFFA
      D5FFBBFAD2FFB8FAD0FFB4FACEFFB1FACBFFB8FBD0FFD0FEDFFFADE9C0FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFEAA4
      2CFFEDA935FFEDA935FFEDA935FFEDA935FFEDA935FFEDA935FFEDA935FFEDA9
      35FFEAA42CFFDE9008FF00000000000000000000000000000000AAAAB9FF0611
      9DFF0F29BCFF0C21BAFF0A1DBAFF091CBAFF091CBAFF0A1DBAFF0B20BAFF0F27
      BCFF1538BFFF122EB0FF06119DFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EDDFC9FFE2A030FFF0AD3CFFFDC4
      65FFFFC76AFFFFC76AFFF0AD3CFFE0981CFFEDDFC9FFFEFEFEFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B5CDBCFF279248FF5EAD77FFDAFBE7FF72F3
      A3FF56EF91FF53EF90FF58EF92FF5DF096FF59EF94FF57EF92FF54EF90FF51EE
      8EFF4EEE8CFF4BED8AFF48ED89FF46EC87FF6AF19EFFCDFDDEFFB3E9C4FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DE9008FFDE90
      08FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE9008FFDE90
      08FFDE9008FFDE9008FF00000000000000000000000000000000E7E7E9FF0611
      9DFF06119DFF06119DFF06119DFF06119DFF06119DFF06119DFF06119DFF0611
      9DFF06119DFF06119DFF06119DFF000000000000000000000000000000000000
      00000000000000000000ECECEDFFBEC5CFFFFCFCFCFF00000000000000000000
      00000000000000000000FEFEFEFFEDDFC9FFE0981CFFF0AD3CFFFFC76AFFFFC7
      6AFFFDC465FFF0AD3CFFE2A030FFEDDFC9FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000AAC1B1FF248F46FF5DAD76FFEEFDF4FF78F4A7FF60F1
      98FF5DF196FF5BF094FF57F092FF55F091FF52EF8FFF50EF8DFF4CEE8BFF4AEE
      89FF47ED87FF44ED86FF41EC84FF3FEC82FF66F09CFFD4FDE2FFBAE9C9FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000ECECECFF606060FF606060FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000606060FF606060FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C2C9D2FF2270D1FF9CBBE1FFFCFCFCFF000000000000
      00000000000000000000EDDFC9FFE2A030FFF0AD3CFFFDC465FFFFC76AFFFFC7
      6AFFF0AD3CFFE0981CFFEDDFC9FFFEFEFEFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DCE3DEFF289249FF4CA468FFF5FEF8FFA3F8C3FF68F29DFF66F2
      9CFF63F19AFF60F198FF5DF196FF5BF094FF58F092FF55EF91FF52EF8FFF50EE
      8DFF4DEE8BFF4AEE8AFF47ED87FF45ED86FF6CF1A0FFDBFDE7FFBFE9CCFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000ECECECFF606060FF606060FFB4B4B4FFB4B4B4FFB4B4
      B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4
      B4FFB4B4B4FFB4B4B4FFB4B4B4FFB4B4B4FF606060FF606060FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000AEB7C3FF1274D4FF126ED2FF92A7C1FF000000000000
      0000FEFEFEFFEDDFC9FFE0981CFFF0AD3CFFFFC76AFFFFC76AFFFDC465FFF0AD
      3CFFE2A030FFEDDFC9FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D7E1DAFF389A57FF3A9B58FFF9FDFBFFADF9C9FF75F4A5FF70F4A2FF6DF4
      A1FF6AF39EFF68F39DFF64F29BFF62F299FF5FF197FF5DF196FF59F094FF57F0
      92FF54EF90FF52EF8EFF4EEE8CFF4CEE8BFF74F2A5FFE3FDECFFC5E9D1FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F2F2F2FF606060FF606060FF606060FF606060FF6060
      60FF606060FF606060FF606060FF606060FF606060FF606060FF606060FF6060
      60FF606060FF606060FF606060FF606060FF606060FF606060FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000AAB4C0FF1C84DCFF34A9ECFF1B72D2FFB9C3D1FF0000
      0000E8DCCBFF85666BFFDBA148FFFDC668FFFFC76AFFFFC76AFFF0AD3CFFE098
      1CFFEDDFC9FFFEFEFEFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004BA368FF389A57FFDEEEE3FFD2FCE2FF7FF6ACFF79F5A8FF76F5A6FF73F4
      A4FF70F4A2FF6EF3A1FF6AF39FFF68F29DFF65F29BFF62F199FF5FF197FF5DF1
      96FF5AF094FF57F092FF54EF90FF52EF8FFF7AF2A8FFE9FDF1FFCBE9D5FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BFBFBFFF606060FFDADADAFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000080ADE4FF2C97E2FF56D8FFFF3AB1EFFF2373D0FFC1BC
      B1FF493D82FF1433CAFF3C3B9AFFCFA97EFFFDC669FFF0AD3CFFE2A030FFEDDF
      C9FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000269148FF4FA56AFFF2F9F5FFC8FBDBFF84F7AFFF80F7ADFF7DF6ABFF7BF6
      A9FF77F5A7FF75F5A6FF71F4A3FF6FF4A2FF6CF3A0FF6AF39EFF67F29CFF64F2
      9BFF61F199FF5FF197FF5BF095FF59F093FF80F3ADFFF0FDF5FFD2E9D9FF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BFBFBFFF606060FFDADADAFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000072A4E1FF34A0E6FF5CDCFFFF55D6FEFF408EC1FF2D2E
      7EFF1537D4FF235AF8FF1E4ADFFF3C3B9AFFDBA149FFE0981CFFEDDFC9FFFEFE
      FEFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C5DACCFF238F45FF82BF96FFF6FDF8FFADFACAFF86F7B1FF83F7AEFF80F6
      ADFF7DF6ABFF7BF6A9FF77F5A7FF75F5A6FF72F4A3FF70F4A2FF6DF3A0FF6AF3
      9EFF67F29CFF64F29BFF61F198FF5FF197FF85F4B0FFF1FDF6FFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000BFBFBFFF606060FFDADADAFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F8F8F8FFDCE0
      E5FFC0C7D0FFA0BADAFF2B77D2FF47B7EDFF64DFFDFF65BFD3FF333386FF0F2C
      CEFF1D4DF2FF235AF9FF2762FCFF1637CBFF795E72FFEDDFC9FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000C9DBCEFF238F44FF7CBD90FFF2FEF6FFADFACAFF8AF8B3FF87F8
      B2FF84F7AFFF82F7AEFF7FF6ACFF7CF6ABFF79F5A8FF77F5A7FF73F5A5FF71F4
      A3FF6EF4A1FF6CF39FFF68F39DFF66F29CFF8AF5B4FFF2FEF6FFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E3E7ECFF0565D0FF0565
      D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565
      D0FF0565D0FF0565D0FF0565D0FFF1F2F3FF0000000000000000000000000000
      000000000000000000000000000000000000F4F7FAFF84A0C4FF4E7EBAFF327B
      D4FF247AD4FF2888D9FF43ACE7FF65D9F9FF6FC5D3FF3F4788FF0C24CAFF153B
      E8FF1C4BF2FF2258F8FF1B44DBFF2E2D95FFE5DACEFFFEFEFEFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009FC0A9FF218E44FFA9D3B6FFEAFEF2FF98FABDFF8DF9
      B5FF8AF8B3FF88F8B2FF85F7B0FF82F7AEFF7FF6ACFF7CF6AAFF79F5A8FF77F5
      A7FF74F4A5FF71F4A3FF6EF3A1FF6CF3A0FF8FF6B6FFF2FEF6FFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF3A9F
      E4FF3A9FE4FF3A9FE4FF3A9FE4FF3A9FE4FF3A9FE4FF3A9FE4FF3A9FE4FF3A9F
      E4FF3A9FE4FF3A9FE4FF0565D0FFD5D9E0FF0000000000000000000000000000
      000000000000000000000000000000000000EFF4FBFF377ED5FF1C79D3FF6BD1
      F0FF8CF8FFFF86F5FFFF7EF0FFFF7EE1E8FF353282FF0C23C8FF1235E6FF143A
      E8FF1A48EEFF163AD5FF172490FF6C8EC0FFE7E8EBFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A8CAB3FF2D944DFFABD5B9FFDDFDE9FF9EFA
      C0FF91F9B8FF8FF9B7FF8CF9B4FF89F8B3FF86F8B1FF84F7AFFF81F7ADFF7EF6
      ACFF7BF6AAFF79F5A8FF75F5A6FF73F5A4FF94F6BAFFF3FEF7FFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF6DE0
      FFFF6DE0FFFF6DE0FFFF6DE0FFFF6DE0FFFF6DE0FFFF6DE0FFFF6DE0FFFF6DE0
      FFFF6DE0FFFF6DE0FFFF0565D0FFD5D9E0FF0000000000000000000000000000
      000000000000000000000000000000000000FEFEFEFFD7DBE1FF4D8DDAFF217D
      D4FF74DCF4FF8BF8FFFF84F3FFFF81E7EFFF2E3B98FF0C23C8FF1235E6FF1439
      E8FF1130D0FF1C2696FF429FD4FF167CD7FF4B83C9FFCED9E7FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FCFCFCFF76B98BFF2F954FFFD2E9D9FFD1FD
      E1FF97FABCFF95FABAFFB6FBCFFFD8FDE6FFD7FCE5FFD6FCE5FFD6FCE4FFD5FC
      E4FFD4FCE3FFD3FCE2FFD2FCE2FFD1FBE1FFDCFCE8FFFBFEFCFFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF62D9
      FFFF62D9FFFF62D9FFFF62D9FFFF62D9FFFF62D9FFFF62D9FFFF62D9FFFF62D9
      FFFF62D9FFFF62D9FFFF0565D0FFD5D9E0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F8F8F9FF9CB3
      D0FF2474D1FF46A8E3FF84F1FCFF84F3FFFF78D6E7FF3B539FFF0C23C8FF0C24
      C9FF263798FF56B5DBFF51D5FFFF4BD1FFFF2598E6FF0E6CD1FF93B0D5FFECEE
      F0FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F5F6F5FF79B68CFF339752FFCAE4
      D1FFC4FDD9FF9EFBC0FFCCFDDEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4E9DBFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF5AD3
      FFFF5AD3FFFF5AD3FFFF5AD3FFFF5AD3FFFF5AD3FFFF5AD3FFFF5AD3FFFF5AD3
      FFFF5AD3FFFF5AD3FFFF0565D0FFD5D9E0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000B1C0D3FF2573D1FF69D0F1FF8AF7FFFF82F2FFFF77D5E7FF2D3B98FF1D23
      92FF60BBDBFF60DCFDFF57D9FFFF51D5FFFF44C8FBFF2496E5FF0D63CCFF669C
      DFFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F5F6F5FF49A265FF389A
      57FFDDF2E4FFB9FDD1FFCFFDE0FFE9F4EDFFAAD4B7FFAAD4B7FFAAD4B7FFAAD4
      B7FFAAD4B7FFAAD4B7FFAAD4B7FFAAD4B7FFAAD4B7FFAAD4B7FF8EC59FFF3096
      50FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF4FCB
      FFFF4FCBFFFF4FCBFFFF4FCBFFFF4FCBFFFF4FCBFFFF4FCBFFFF4FCBFFFF4FCB
      FFFF4FCBFFFF4FCBFFFF0565D0FFD5D9E0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D0D5DCFF2976D2FF89F0FAFF90FBFFFF89F6FFFF83F2FFFF81E2E8FF7EDC
      E1FF6BE3FEFF48BAEFFF228BDDFF1C7AD5FF2B76D2FF4084D7FF6B8DB8FFAABE
      D6FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E3E6E3FF4C9C
      65FF3E9D5CFFD6EDDDFFDEFEEAFFAAD4B7FF1B833BFF309650FF309650FF3096
      50FF309650FF309650FF309650FF309650FF309650FF309650FF309650FF2887
      46FF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF46C6
      FFFF46C6FFFF46C6FFFF46C6FFFF46C6FFFF46C6FFFF46C6FFFF46C6FFFF46C6
      FFFF46C6FFFF46C6FFFF0565D0FFD5D9E0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A2BBDBFF2682D5FF96FFFFFF95FDFFFF8FFAFFFF88F6FFFF80F1FFFF7AED
      FFFF6ADFFBFF1D80D8FF799DCBFFB5BDC8FFD5D9E0FFEAECEFFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DCDF
      DDFF239046FF45A061FFE5F3EAFFAAD4B7FF2B8245FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF3BBE
      FFFF3BBEFFFF3BBEFFFF3BBEFFFF3BBEFFFF3BBEFFFF3BBEFFFF3BBEFFFF3BBE
      FFFF3BBEFFFF3BBEFFFF0565D0FFD5D9E0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004C8CD9FF5ABCE9FF8DF4FBFF72D7F2FF308DDAFF1672D1FF70DBF6FF81F1
      FFFF6CDEF9FF1B7CD6FFDCE4EEFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C7DCCDFF289149FF4CA467FF98CAA8FF309650FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AABED6FF0565D0FF33B9
      FFFF33B9FFFF33B9FFFF33B9FFFF33B9FFFF33B9FFFF33B9FFFF33B9FFFF33B9
      FFFF33B9FFFF33B9FFFF0565D0FFD5D9E0FF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F8F9
      FAFF166ED0FF4FB1E6FF2D8BD9FF1B71D0FF83A5CFFF6790C4FF2686D8FF73DF
      F7FF6BD9F6FF1574D3FFF8F8F9FF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFCFFB1CEBAFF269047FF3E9D5CFF309650FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000AEC1D8FF0565D0FF0565
      D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565D0FF0565
      D0FF0565D0FF0565D0FF0565D0FFD7DBE1FF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B5C6
      DBFF0B62CCFF397FD5FF9CB3D0FFF1F2F3FF00000000000000006B93C6FF2581
      D7FF5FC8EFFF1E76D2FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000090BA9DFF128637FF278545FF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F7F8FBFFE2ECF9FFE2EC
      F9FFE2ECF9FFE2ECF9FFE2ECF9FFE2ECF9FFE2ECF9FFE2ECF9FFE2ECF9FFE2EC
      F9FFE2ECF9FFE2ECF9FFE8F0FAFFFBFBFCFF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D9E1
      ECFFA7BFDDFFE3E6EAFF00000000000000000000000000000000E7E9EDFF528F
      DBFF1877D3FF2F7BD4FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F3F3F3FF74A984FF5D926DFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F8F9
      FAFF4688D8FF4688D8FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F2F7FCFFEEF4FBFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BBBBE0FFB1B1DCFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F5F9F7FFA3DDB9FFBBE6CBFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EEF4FBFFE3EDF9FFE8F0FAFFFDFDFEFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F8F8F8FFAEAEC6FF3939A7FF020698FF020291FF4142ADFFAEAE
      C6FFF8F8F8FF000000000000000000000000000000000000000000000000EAEC
      EBFF8AD4A6FF21B159FF08A947FF05A847FF39B669FFAECAB9FFF8F8F8FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DBDBEB00B1B1D400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E7E7EB00A1A1CB00F1F1F50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000ECF2FBFF2C77D2FF4487D7FF2C77D3FF90B8E7FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E3E3E6FF7272B7FF12169CFF091CBBFF0A1EBFFF06089BFF0E12ADFF181A
      A2FF7272B7FFE3E3E6FF00000000000000000000000000000000C0DACAFF4EB1
      74FF16B656FF3EDE83FF1DBE5FFF19C065FF17BE63FF15AD52FF72C190FFE3E6
      E4FF000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FAFAFA00C1C1D6001D208600111887006E6EA000EEEE
      EE00000000000000000000000000000000000000000000000000000000000000
      000000000000DCDCDF002D2F8D00121D910037379100D0D0DB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E6EFF9FF337BD4FF8BB7E7FFDDF7FEFFC6E4F7FF4588D8FF8DB6E7FFFDFD
      FEFF000000000000000000000000000000000000000000000000E3E3E8FF8E8E
      BEFF10149BFF091AB3FF1132DFFF1235E6FF0C23C8FF06089BFF121AB8FF1218
      B7FF0D10AAFF13159FFF9595C2FFE3E3E8FFC8DBCFFF69C88EFF15B255FF3BD5
      7DFF5DFCA5FF5FFFA8FF20C162FF1EC56CFF2DD783FF29D27DFF12B75AFF11AB
      4FFF8EC5A3FFE3E8E5FF00000000000000000000000000000000000000000000
      00000000000000000000B0B0CB0022228400455DC2006184E5001E248E006E6E
      A000000000000000000000000000000000000000000000000000000000000000
      0000DCDCDF0047478F00253BAE004B78EF002135A90037389100EAEAEE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F8F9F800B1D7BD00D0DFD500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FBFC
      FEFF4989D9FF639BDEFFB2ECFDFF85E8FFFF95ECFFFFBEE3F7FF337BD3FF8DB6
      E7FF0000000000000000000000000000000000000000000000004040A9FF0F1B
      A7FF1539D7FF1842EAFF1339E8FF1235E6FF0C23C8FF060A9CFF131CBBFF131A
      B9FF1218B7FF1015B2FF1013A4FF39529DFF25B05CFF33C371FF68EFA8FF6EFD
      B2FF61FFAAFF5FFFA8FF20C162FF21C66FFF30D885FF2ED783FF2BD480FF20C8
      70FF11B053FF40B96FFF00000000000000000000000000000000000000000000
      0000FAFAFA00B0B0CB00161B85005976D3006B92FA004D74F4006589EA00222E
      99006E6EA000EEEEEE000000000000000000000000000000000000000000DCDC
      DF002E2F8D002A3EAE004F7DFA003A65F4005281F8002C45B80037389100D0D0
      DB00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000212D
      93002D7D570025924800229246009CB7A4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FDFDFEFF89B3
      E6FF5794DCFFACE2FAFF6FE1FFFF72E3FFFF76E5FFFF83E9FFFFBFE3F7FF4688
      D8FF90B8E7FFFDFDFEFF0000000000000000000000000000000006109FFF1E4E
      E9FF2053F6FF1B4AF0FF163DEAFF1236E6FF0C23C8FF070B9DFF1420BFFF141E
      BDFF131CBAFF131AB9FF1218B7FF094187FF33C16FFF93FFCDFF83FFC1FF76FF
      B8FF66FFADFF5FFFA8FF20C162FF26C872FF36DA89FF34D987FF30D885FF2ED7
      84FF25CE77FF07AB4AFF00000000000000000000000000000000000000000000
      0000B0B0CB00222286005F7AD3007BA1F9002A4EEB001437E600486EF200688C
      EA001F258E006E6EA00000000000000000000000000000000000DCDCDF004747
      8F002D41AE005883F3002349EB001337E6003761F2005684F8002538A9003738
      9100F1F1F5000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004469E5003384
      7B002692490079D596006CCC8B0022924600B9C8BD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A5C5ECFF2F79
      D3FFADDCF7FF78E0FFFF6CDFFFFF6FE1FFFF72E3FFFF76E5FFFF95ECFFFFC6E4
      F7FF2C76D3FFE8F0FAFF000000000000000000000000000000000610A0FF2051
      EAFF2157F8FF1D4DF2FF1741ECFF1337E7FF0C23C8FF070C9EFF1523C1FF1421
      BFFF141EBDFF131CBBFF121AB9FF094288FF34C170FF97FFD0FF88FFC5FF7BFF
      BBFF6AFFB0FF60FFA9FF20C162FF29C974FF3CDC8DFF39DB8BFF35DA88FF32D9
      86FF27CE79FF07AB4AFF00000000000000000000000000000000000000000000
      00001D1D82004151B00089AEFD00395DEE001235E6001235E6001336E6003E63
      EF006C8FEA00242F99006E6EA000EEEEEE0000000000DCDCDF002F2F8D003244
      AE005E89FA00254AEB001235E6001235E600163AE7004874F6005781EF001520
      9100A8A8CF000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001133D3000E715B002792
      490081DEA0006FF4A0008BF8B2007DDA9B002292460087A29000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFEFEFFC2D7F2FF2673D1FFA0CE
      F2FF6ADAFFFF64DAFFFF68DCFFFF6BDEFFFF6FE1FFFF72E3FFFF85E8FFFFDDF7
      FEFF4487D7FFE3EDF9FF000000000000000000000000000000000611A0FF2153
      ECFF245BFAFF1F52F5FF1945EEFF153BE9FF0C23C8FF070D9FFF1627C5FF1625
      C3FF1522C1FF1420BFFF141EBDFF0A448AFF35C171FF9CFFD3FF8EFFC9FF80FF
      C0FF70FFB4FF64FFABFF20C162FF2ECA77FF41DE92FF3FDD90FF3BDC8DFF39DB
      8BFF2DD07DFF08AB4BFF00000000000000000000000000000000000000000000
      00009C9CB400202388006983D7006D91F7001538E6001235E6001235E6001336
      E6004C71F2006F91EA0020268E006E6EA000DCDCDF0047478F003547AE00678E
      F300264CEB001235E6001235E6001235E6003960F0006491FD002B40AE002E2F
      8D00EAEAED000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001335D30010666A0027934A007FD8
      9B006BF39F003DEB810046ED87008BF8B2006CCC8B0012823C00264382000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D3E2F5FF2774D2FF94C4EEFF7ADA
      FEFF5ED6FFFF61D8FFFF65DAFFFF68DCFFFF6CDFFFFF6FE1FFFFB1ECFDFF8FBA
      E9FF2A76D2FFEEF4FBFF000000000000000000000000000000000611A0FF2154
      ECFF255FFCFF2055F7FF1B49F0FF153BE6FF0612ABFF040898FF1627C4FF1627
      C5FF1525C3FF1523C1FF1421BFFF0A458BFF35C171FF9EFFD4FF92FFCCFF85FF
      C3FF75FFB7FF54EC99FF0DAF4DFF17B65CFF43DC90FF44DF93FF41DE91FF3EDD
      8FFF31D27FFF09AB4BFF00000000000000000000000000000000000000000000
      000000000000B9B9C600202388005E74C8007093F7002245E9001235E6001235
      E6001336E6004165EF007394EA002731990013157F00394AAE006C94FA00294D
      EB001235E6001235E6001437E6003A60EF006B94F5003043AE0047478F00DCDC
      DF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000566CB8002487600028934A0091E4AC006EF3
      A00038EB7E0038EB7E0038EB7E003BEB80008BF8B2007DDA9B0015894600194A
      9800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F8FAFDFF2E78D3FF7DB0E7FF70D7FFFF57D1
      FFFF5BD3FFFF5DD5FFFF61D8FFFF64DAFFFF78E0FFFFADE3FAFF6099DDFF337C
      D4FFECF2FBFF00000000000000000000000000000000000000000611A0FF2154
      ECFF2762FDFF2052EFFF0F2AC4FF040C9DFF0A1CB4FF0B1FBBFF050C9FFF0D18
      AEFF1525C2FF1627C5FF1525C3FF0A478DFF35C171FF9FFFD5FF97FECFFF77F1
      B3FF2FC56FFF15B254FF37CF78FF2AC76CFF0DAE4EFF27C46EFF41DA8EFF44DF
      93FF36D483FF0AAC4CFF00000000000000000000000000000000000000000000
      000000000000000000009C9CB400212488006F88D7007395F7001639E6001235
      E6001235E6001336E6005075F2007696EA00546BC600779AF3002A4FEB001235
      E6001235E6001235E6003459ED006F99FB003346AE002E2F8D00DCDCDF000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000008F91A50014604D0028934A0093DCAA0074F3A4003DEB
      810038EB7E0038EB7E0038EB7E0038EB7E0046ED87008BF8B2006CCC8B000E81
      44000D35AB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FAFBFEFF6B9FDFFF5999DFFF82D4FBFF51CDFFFF54CF
      FFFF57D1FFFF5BD3FFFF5ED6FFFF6ADAFFFFAFDFF8FF5593DCFF4889D8FFE6EF
      F9FF0000000000000000000000000000000000000000000000000611A0FF2154
      ECFF1E4BE2FF0B1CAEFF0713A5FF1435D1FF1A46EDFF1740EBFF0F2BD4FF0715
      B0FF070D9FFF111FB8FF1627C5FF0B498EFF35C171FF9FFFD5FF57D692FF1DB4
      5BFF35C573FF66EAA5FF75FFB7FF68FCADFF3EDE84FF14B656FF16B559FF36D1
      80FF3AD586FF0BAC4DFF00000000000000000000000000000000000000000000
      0000000000000000000000000000B9B9C600212488006378C8007697F7002346
      E9001235E6001235E6001336E6004468EF006A8EF7002D50EB001235E6001235
      E6001437E600375BED00799EF400384AAE0047478F00DCDCDF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EAECEA0045A0620028934A009FE4B6007BF4A90043ED85003DEB
      820039EB7E0038EB7E0038EB7E0038EB7E0038EB7E003BEB80008BF8B2007DDA
      9C000E8144002253990000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FCFDFEFF6099DDFF4387D8FF87D7FDFF4CC8FFFF4DCAFFFF50CC
      FFFF54CFFFFF57D1FFFF7ADAFEFFA3D1F3FF2E78D3FF89B3E6FFFBFCFEFF0000
      0000000000000000000000000000000000000000000000000000040998FF0C1D
      AEFF0712A1FF1637CCFF245CF8FF2156F7FF1C4CF2FF1944EEFF143AE8FF1235
      E6FF0C25CCFF050FA7FF0912A6FF084487FF1AB158FF25B862FF2EBD6AFF6FE3
      A8FF92FFCCFF87FFC4FF7AFFBBFF6FFFB3FF63FFAAFF5CFCA5FF35D579FF11B2
      52FF15B459FF07A847FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9CB40021248800758DD700799A
      F700163AE6001235E6001235E6001336E6001A3DE7001235E6001235E6001235
      E600395DED007FA5FB003B4CAE002F308D00DCDCDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EAECEA005AA5720029934A00A1DCB40082F5AD0057F092004DEE8C0047ED
      880052EE8E0089F6B20062F199003AEB7F0038EB7E0038EB7E0046ED87008EF8
      B4006ECC8D00188A46002B468200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FEFEFEFFA0C2EBFF307AD3FF85C9F5FF46C4FFFF46C5FFFF4AC8FFFF4DCA
      FFFF51CDFFFF70D7FFFF96C5EEFF2573D2FFA5C5ECFFFDFDFEFF000000000000
      000000000000000000000000000000000000000000000000000000008DFF050E
      9DFF2154ECFF2763FDFF2661FCFF2259F8FF1D4FF3FF1A47EFFF153DEAFF1236
      E6FF1235E6FF1030DEFF191FA2FF6F5941FF709721FF4BC170FF91F7C8FF9EFF
      D4FF96FFCEFF8BFFC7FF7EFFBDFF73FFB6FF66FFACFF5FFFA8FF5DFDA6FF4FEF
      96FF0DAE4DFF00A23FFF00000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B9B9C60022248800687C
      C8007C9DF7002549EA001336E7001235E6001235E6001235E6001336E600395C
      ED0089AAF4003F4FAE0047478F00DCDCDF000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EAEC
      EA0045A0620029934B00B2E4C20093F7B90066F39C0060F1980059F0930066F1
      9C00BEFBD400B2EAC300C1F8D30084F4AE003AEB7F0038EB7E0038EB7E003BEB
      80009BFABD0086DAA10013823D0087A290000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FEFEFEFFF5F5F5FFD1D1D1FFABABABFF9A9A9AFFB3B3B3FFDFDF
      DFFFC6D5E8FF2975D0FF7FC7F6FF44C1FFFF40C1FFFF42C3FFFF46C5FFFF4CC8
      FFFF82D5FBFF7DB0E7FF2673D1FFC2D7F2FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5B5D0FF4B4B
      A3FF0B19A7FF1B44DAFF2660FBFF245CFAFF1F52F5FF1C4AF1FF1740ECFF1338
      E8FF2035C6FF59497CFFC67F17FFE0910BFFE08E07FFC68F0BFF93BF65FF8FE9
      AFFF9AFFD1FF90FFCAFF83FFC1FF78FFB9FF6BFFB0FF5EFCA6FF40E186FF18B7
      58FF4BB072FFB5D4C1FF00000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008E8EA9001518
      810099B4EF00587BF200143AE8001338E7001237E7001235E6001B3EE7007598
      F7005E72C60014157F00DCDCDF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EAECEA005AA5
      720029934B00B2DCBF00A2F9C3007AF5A90070F4A2006AF39F0075F4A600BDFB
      D300AEE3BF002E9950006BBE8500C6F8D70078F3A7003DEB810038EB7E0038EB
      7E0051EE8E00A3FAC20075CC910024924700A7C3AF00FCFCFC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFCFFBABABAFF7D7D7DFF8B8B8BFFB5B5B5FFC2C2C2FFADADADFF8F8F
      8FFF5C6879FF6987A4FF48BDFAFF39BCFFFF3CBFFFFF40C1FFFF46C4FFFF87D7
      FDFF5999DFFF2E78D3FFD2E2F5FFFEFEFEFF0000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EAEA
      ECFF8787C9FF1A1F9FFF112CBFFF2051ECFF2055F6FF1D4DF2FF173FE8FF363E
      A7FFA16B32FFD78E15FFF4B448FFEBA52FFFEB971EFFF49E2EFFD8900DFFB39D
      25FF85D389FF8BF9C5FF86FFC4FF7BFFBCFF5FF2A3FF2CCB6FFF1EB058FF87D3
      A4FFEAECEBFF0000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DCDCDF0031328D00505C
      AE009CBBFB00385EEF00163DEA00153CE900143AE9001439E8001539E7004C6E
      F00088A4EA002D3799006E6EA000EEEEEE000000000000000000000000000000
      00000000000000000000000000000000000000000000E7EBE8002D944E002993
      4B00C3E5CF00B3FBCD0089F8B30083F7AF007CF6AA0081F6AD00C9FBDB00B8E3
      C6001C8B46001D5F8A000770520054AE7300CFF9DD009DF7BF0042EC840039EB
      7E0038EB7E0044EC8500A7FAC5008DDAA7001A8D3E0080BE9300FCFCFC000000
      000000000000000000000000000000000000000000000000000000000000FCFC
      FCFF888888FF8F8F8FFFE3E2E2FFF1F0F0FFEAE7E7FFE6E3E3FFE6E3E3FFEAE7
      E7FFD8D8D8FF989898FF5A7B8BFF3EAAE2FF39BCFFFF44C1FFFF85C9F5FF4388
      D9FF6A9FE0FFF7FAFDFF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000EAEAEFFF6767A7FF1A1D9DFF1531C5FF394DBBFF8E6346FFD78D
      15FFF0AE3EFFFDC364FFFFC76AFFEDA935FFED9922FFFFA744FFFDA540FFF09B
      27FFD78F0BFFA2A330FF81DC8FFF49D484FF1EAF57FF67B284FFEAEFECFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DCDCDF0047478F00545FAE00AEC7
      F400476EF2001943ED001741EC00163FEB00163EEA00153CE900143BE900163B
      E8005E80F3008BA7EA00252A8E006E6EA0000000000000000000000000000000
      00000000000000000000000000000000000000000000A1CFAF0028924A00B5D9
      C000E0FEEB00A0FBC20093FAB9008DF9B60090F8B700CBFBDD00C0E3CC002D96
      4E00286997001840E4001341C5000771530070BE8800D4F9E00090F5B60046ED
      87003AEB7F0038EB7E0053EE8F00ABFAC70084D29D00229246009CC7A900FCFC
      FC00000000000000000000000000000000000000000000000000FDFDFDFFB0B0
      B0FF939393FFE5E5E5FFF0EEEEFFECEAEAFFE9E6E6FFE6E3E3FFE2DFDFFFDFDB
      DBFFDFDBDBFFE1DEDEFFA0A1A1FF5A7B8BFF48BDFAFF7FC7F6FF2F7AD4FF5F98
      DDFFFAFBFEFF0000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F5F5F5FFBCBCD8FF584D93FFAC6F23FFE9A733FFF7C1
      62FFFFC96FFFFFC76AFFFFC76AFFEDA935FFED9922FFFFA744FFFFA744FFFFA7
      44FFF7A135FFE9951AFFAF9312FF59B462FFBCDDC8FFF5F5F5FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DCDCDF0031328D005862AE00B5D0FD005178
      F5001A47EF001946EE001A45EE003C63F1006083F4002D54EE00163EEB00153D
      EA00173DE9005073F1008EAAEA00303899006E6EA000EEEEEE00000000000000
      00000000000000000000000000000000000000000000F1F5F200399A570064B0
      7C00F3F9F500DAFEE700A2FCC300A2FBC300DEFCEA00C8E3D000208D46002E6E
      9800000000000000000000000000375FDE00177F550058AF7600DAF9E400A7F8
      C6004BED8A003FEC820039EB7F0045EC8600AFFACA00ACE9BF002694490080BE
      9300FCFCFC000000000000000000000000000000000000000000E2E2E2FF8080
      80FFF0EFEFFFF3F2F2FFF0EEEEFFE5E3E3FFE9E6E6FFE6E3E3FFE2DFDFFFDFDB
      DBFFDBD7D7FFD8D4D4FFE0DEDEFF989898FF6987A4FF2B75D1FFA1C2EBFFFDFD
      FEFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A12CFFFFDB90FFFFD6
      86FFFFCF7AFFFFCA70FFFFC76AFFEDA935FFED9922FFFFA744FFFFA744FFFFA7
      44FFFFA744FFFFA744FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DCDCDF0047478F005C65AE00C3D9F500547DF600204F
      F2001B4AF1001B49F0003F68F200A4C1FC00A8C0EF0097B5FA00254EEE001740
      EC00163FEB00183FEA006386F40092ACEA00262B8E006E6EA000000000000000
      0000000000000000000000000000000000000000000000000000D0DCD400399A
      57007FBE9300F3F9F500D9FEE700DEFEEA00B9DCC4002F965000307199001F4C
      E9000000000000000000000000000000000089A9C700238A560075BE8C00DEF9
      E7009AF6BD004FEE8D0042EC84003CEB800055EE9100B3FACD0099DBAE002694
      49009CC7A900FCFCFC00000000000000000000000000000000009C9C9CFFB4B4
      B4FFF7F6F6FFF4F2F2FFDDDBDBFF959595FFC4C2C2FFE6E3E3FFE2DFDFFFDFDB
      DBFFDCD7D7FFD8D4D4FFDBD7D7FFD8D7D7FF5C6879FFC6D6E8FFFEFEFEFF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A12DFFFFDF97FFFFDA
      8FFFFFD483FFFFCF79FFFFC96DFFEDA935FFED9923FFFFA744FFFFA744FFFFA7
      44FFFFA744FFFFA744FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DCDCDF0032338D005F68AE00CEE5FE006F95F9001F51F5001E50
      F4001F4FF300446EF500B0C8F3005761AE00171981007888C8009FBDFB003960
      F1001842ED001742EC001942EC005478F20096AFEA00323A99006E6EA000EEEE
      EE0000000000000000000000000000000000000000000000000000000000EAEE
      EB00399A570064B07C00EFF7F100AFD6BB0015873B003F7F9B00000000000000
      000000000000000000000000000000000000000000006E81B900248C56004BA6
      6A00D9F4E200B1F8CC0054EF900048ED880041EC830049ED8900B8FAD000B5E9
      C50027944A0080BE9300FDFDFD000000000000000000000000007F7F7FFFD8D8
      D8FFF7F6F6FFE0DFDFFF808080FFACACACFF7A7A7AFFB3B1B1FFE3DFDFFFDFDC
      DCFFDCD7D7FFD8D4D4FFD5D0D0FFE7E4E4FF909090FFDFDFDFFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A22EFFFFE4A0FFFFE0
      98FFFFD98DFFFFD483FFFFCE77FFEDAA38FFED9D29FFFFAC4CFFFFA744FFFFA7
      44FFFFA744FFFFA744FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DCDCDF0047478F005F68AE00CEE1F500739AFB00285CF7002054F6001F53
      F5004773F600B4CFFC005A64AE0031328D008E8EA900242688008FA2D700A3C0
      FB002852F0001944EE001843ED001A43EC00688BF50099B1EA00282C8E006E6E
      A000000000000000000000000000000000000000000000000000000000000000
      0000B3BFBA0011723F004FA66B00238F46004583A3002658ED00000000000000
      00000000000000000000000000000000000000000000000000007590B300238B
      55005FB07900DEF4E500A3F7C30058F093004BEE8A0045ED860060F09800C4FB
      D800A1DBB30028944A00BFDEC9000000000000000000000000007C7C7CFFE0E0
      E0FFE3E2E2FF8C8C8CFFBABABAFFFDFDFDFFDDDDDDFF7A7A7AFFBFBCBCFFDFDC
      DCFFDCD8D8FFD9D4D4FFD5D0D0FFDCD7D7FFADADADFFB3B3B3FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A22FFFFFE7A7FFFFE3
      9FFFFFDE95FFFFD98CFFF8C467FFE49B1DFFE49516FFF8A944FFFFAB4AFFFFA7
      44FFFFA744FFFFA744FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000036368F00434A9D00DEF2FF00A0C0FD00235BFA00235AF9002359F8004A78
      F900BED3F4005E67AE0047478F00DCDCDF0000000000B9B9C600252788007D8C
      C800A7C3FB003F68F3001A47EF001946EE00224CEF0088A8F900A0B7E8002125
      8A00ADADD1000000000000000000000000000000000000000000000000000000
      0000000000003D4891004AA56F006FA2B9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000007384
      B90034936A0044A06600E3F4E800BBF9D3005DF0960053EF90009BF6BE00E3FE
      ED00A6DBB70028944B00BFDEC9000000000000000000000000007D7D7DFFCBCB
      CBFF818181FFBABABAFF000000000000000000000000ECECECFF7A7A7AFFAFAD
      ADFFDCD8D8FFD9D4D4FFD5D0D0FFDAD5D5FFC2C2C2FF9A9A9AFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A331FFFFEBAEFFFFE7
      A8FFF9D586FFECB044FFE19614FFECA833FFECA732FFE19411FFEC9B25FFF9A6
      40FFFFA744FFFFA744FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008080B800272A8A00B9C9E700CEE5FE005887FC00245DFA004B7BFB00BAD5
      FD005F68AE0032338D00DCDCDF000000000000000000000000009C9CB4002527
      880095A6D700B1CCFC003964F2001B48F0006589F600BAD5FD008698D3001B1D
      8300CDCDE3000000000000000000000000000000000000000000000000000000
      00000000000000000000B2C2DE00CCE2FB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000839AC00038966A0062B07B00E6F4EA00B1F8CD00A1F7C200EEFEF400C9E9
      D30027924B0080BE9300FDFDFD000000000000000000000000007C7C7CFF7777
      77FFBABABAFFFDFDFDFF00000000000000000000000000000000DDDDDDFF7A7A
      7AFFBAB7B7FFD9D4D4FFD5D0D0FFDAD5D5FFB5B5B5FFABABABFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE4A431FFFEEBAFFFF2C8
      6FFFE29C1FFFE6A32BFFF8C466FFFEC96EFFFEC567FFF8BB55FFE69D20FFE292
      0FFFF29E2EFFFEA642FFE49820FFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFC008E8EBA002C308D00B4C4E400CCE3FE007AA2FD00BFD4F4005F68
      AE0047478F00DCDCDF000000000000000000000000000000000000000000B9B9
      C60025278800828FC800BBD5FD0096B4FA00CBE2FC008D9DD30023248600B0B0
      CB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000007E8CC3003B996A0048A26600E9F4ED00FFFFFF00B7DBC2002891
      4B009CC7A900FCFCFC0000000000000000000000000000000000B7B7B7FFBFBF
      BFFF000000000000000000000000000000000000000000000000FDFDFDFFACAC
      ACFF8F8D8DFFD3CECEFFD5D0D0FFE7E3E3FF8B8B8BFFD1D1D1FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D2CBC0FFE19C23FFE5A42EFFE3A0
      27FFF5CE79FFFEDF98FFFFD98CFFFFD380FFFFCA70FFFFC76AFFFEC668FFF5B7
      4DFFE39817FFE59313FFE1961AFFD2CBC0FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F5F5F5006464A90032379100CBDEF300D8EEFF005F68AE003233
      8D00DCDCDF000000000000000000000000000000000000000000000000000000
      00009C9CB400262788009DACD700D8EDFF0092A2D3001F228500B0B0CB00FAFA
      FA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000008BA0C00044A06B0063B07B00BFDFC90029914B007DBB
      9000FCFCFC000000000000000000000000000000000000000000FCFCFCFFFEFE
      FEFF000000000000000000000000000000000000000000000000BABABAFF7D7D
      7DFFCBC8C8FFD9D5D5FFD9D5D5FFDDDCDCFF7D7D7DFFF5F5F5FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D4CDC3FFDF9619FFE7AA38FFFAE1
      9CFFFFEAACFFFFE5A3FFFFDE96FFFFD88BFFFFD07BFFFFC96FFFFFC76AFFFFC7
      6AFFFABE5BFFE59C1DFFDF9619FFD4CDC3FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F5F5F5008080B200313590004C54A30047478F00DCDC
      DF00000000000000000000000000000000000000000000000000000000000000
      000000000000B9B9C60026288800626BB00024248600B0B0CB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000005F69AB000B6145000B8132009CC7A900FCFC
      FC00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFDFDFFBABABAFF878686FFCFCB
      CBFFDCD8D8FFDAD6D6FFDBD8D8FF8F8F8FFFB9B9B9FFFEFEFEFF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FCFCFCFFE2D4BCFFE3A336FFE4A3
      2FFFF6D485FFFDE6A6FFFFE4A1FFFFDE97FFFFD789FFFFD17CFFFDC76AFFF6B7
      4EFFE49B1EFFE3A336FFE2D4BCFFFCFCFCFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F5F5F5006464A9002F2F8B00DCDCDF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000A0A0B70028288800B0B0CB00FAFAFA00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AFB3C800E4EFE700FEFEFE000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BABABAFF7E7E7EFFD1CFCFFFE0DD
      DDFFDFDBDBFFE3E1E1FF929292FF888888FFFCFCFCFF00000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E8E5E0FFD9BD
      8EFFE09B1FFFEAB248FFFBE09BFFFFE3A0FFFFDD92FFFBD07CFFEAA630FFE097
      19FFD9BD8EFFE8E5E0FF00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FEFEFEFFBFBFBFFF777777FFC6C5C5FFD7D6D6FFD1CF
      CFFFB3B2B2FF808080FFB0B0B0FFFCFCFCFF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F0E8DCFFDCB26BFFE19B20FFEEBB58FFEEB953FFE19A1FFFDCB26BFFF0E8
      DCFF000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FCFCFCFFB7B7B7FF7C7C7CFF7D7D7DFF7E7E7EFF8080
      80FF9C9C9CFFE2E2E2FFFDFDFDFF000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F9F8F8FFD4C6AEFFE3A53AFFE3A53AFFD4C6AEFFF9F8F8FF0000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F4F4F400698FA000698FA000307C9C00307C
      9C00307C9C00307C9C00307C9C00307C9C00307C9C00307C9C00307C9C00307C
      9C00307C9C00307C9C00307C9C004C849B00F4F4F40000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F1008282820083838300838383008383830083838300838383008383
      8300838383008383830083838300838383008383830083838300838383008383
      8300838383008383830083838300838383008383830083838300838383008282
      8200000000000000000000000000000000000000000000000000000000000000
      0000E7E7E7008282820083838300838383008383830083838300838383008383
      8300838383008383830083838300838383008383830083838300838383008383
      8300838383008383830083838300838383008383830083838300838383008282
      8200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF00307C9C00CEF8FB00D0FCFF00D0FC
      FF00D0FCFF00D0FCFF00D0FCFF00D0FCFF00D0FCFF00D0FCFF00D0FCFF00D0FC
      FF00D0FCFF00D0FCFF00CEF8FB00307C9C00DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006AC1EB00609ED400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000066CAEB006393CB00B8E4F50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10084848400FDFDFD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FCFCFC008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80084848400FDFDFD00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00DCDEDC00238D380097BA9B00FAFAFA00FFFFFF00FCFCFC008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF00307C9C00D0FCFF00A6E6FF00A2E4
      FF009DE1FF0099DFFF0094DCFF0090DAFF008AD8FF0086D6FF0084D5FF0084D5
      FF0084D5FF0084D5FF00D0FCFF00307C9C00DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005BCFFA003AA0D6000B1B86001118870033578D001582
      3A00000000000000000000000000000000000000000000000000000000000000
      00000000000068AEB80009248D00121D9100142F91004CACDB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00F8F8F800F6F6F600F6F6F600F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F4F4F400F4F4F400F4F4F400F4F4F400F3F3
      F300F3F3F300F3F3F300F3F3F300F3F3F300F2F2F200F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00F8F8F800F6F6F600F6F6F600F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F4F4F400F4F4F400F4F4F400F4F4F400F3F3
      F300F3F3F300D1D6D30023934500128E3900629D7200F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF00307C9C00D0FCFF0090E1FF008BDE
      FF0084DBFF007ED8FF0077D5FF0072D2FF006BCFFF0066CCFF005FC9FF005CC7
      FF005BC7FF005BC7FF00D0FCFF00307C9C00DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004092CB00101E8400455DC2006184E50019228C000A3C
      4D00000000000000000000000000000000000000000000000000000000000000
      000013783800253A8200253BAE004B78EF002035A900142F910000389F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00F1F1F100CCCFCC00208B36008BAE9000E7E7
      E700ECECEC00EBEBEB00EBEBEB00EAEAEA00EAEAEA00E9E9E900E9E9E900E9E9
      E900E8E8E800E8E8E800E7E7E700E7E7E700E6E6E600F2F2F200FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00F1F1F100EDEDED00EDEDED00ECECEC00ECEC
      EC00ECECEC00CACDCA00C9CFCB00C8CECA00C8CECA00C8CDC900C8CDC900C8CD
      C900C8CDC900A9BAAE001D873600D1FFE1003EA860003B9C5300D4E0D7008383
      8300000000000000000000000000000000000000000000000000FEFEFE00C2C2
      C200696A6A0074787800747878006F717100307C9C00D0FCFF0097E5FF0092E2
      FF008BDFFF0085DCFF007FD9FF0079D6FF0072D2FF006DD0FF0066CCFF0060CA
      FF005BC7FF005BC7FF00D0FCFF00307C9C006F7171007478780074787800696A
      6A00C2C2C200FEFEFE0000000000000000000000000000000000000000000000
      0000FAFAFA00002A9000111A85005976D3006B92FA004D74F4006589EA00222E
      9900556D8F0058E08C0000000000000000000000000000000000000000009CDA
      B400071B6C002A3EAE004F7DFA003A65F4005281F8002C45B800000E7E00D0D0
      DB00000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00F2F2F200CED3CF0022924400128E3900609B
      7000EDEDED00ECECEC00ECECEC00ECECEC00EBEBEB00EBEBEB00EAEAEA00EAEA
      EA00E9E9E900E9E9E900E8E8E800E8E8E800E7E7E700F3F3F300FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00F2F2F200EFEFEF00EEEEEE00EEEEEE00EDED
      ED00EDEDED001E873000238C3A00238A3A00238A3900238A3900228A3900228A
      3900228A39001E86340006701600D0FFE400B9FCCF0049AA670024913E006F77
      7100000000000000000000000000000000000000000000000000F0F0F0007476
      7600F9F9F900F0F0F000777A7A00B2B1B100307C9C00D0FCFF00A0EAFF009BE6
      FF0094E3FF008FE0FF0088DDFF0082DAFF007BD7FF0076D4FF006FD1FF006ACE
      FF0063CBFF005EC8FF00D0FCFF00307C9C00A29F9F0075787800EFEFEF00F9F9
      F90074757500F0F0F00000000000000000000000000000000000000000000000
      0000B0B0CB00030A7C005F7AD3007BA1F9002A4EEB001437E600486EF200688C
      EA001D258C0029687300000000000000000000000000000000004FCF83003446
      82002D41AE005883F3002349EB001337E6003761F2005684F8002336A8003738
      9100F1F1F500000000000000000000000000DCDEDC00DBE0DC00DBE0DC00DBE0
      DC00C7CEC90072777300DBE0DC00D0D5D200AEBFB3001E883700D1FFE1003EA8
      60003A9A5200C5D1C800ECECEC00ECECEC00EBEBEB00EBEBEB00EBEBEB00EAEA
      EA00EAEAEA00E9E9E900E9E9E900E8E8E800E8E8E800F3F3F300FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000CCD0CD0085858500FFFFFF00F2F2F200EFEFEF00EFEFEF00EEEEEE00EEEE
      EE00EDEDED000B843100E0F7E800E5FFF300E3FFEF00DFFFED00DDFFEB00DAFF
      EA00D8FFE800D4FFE700D4FFE5009EF8BF0031E97A00ADFFCB0086D29F000682
      2D008CCC9E000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F3F2F20074787800DBD9D900447F9700CEF7FC00A7EDFF00A2EA
      FF009BE7FF0096E4FF008FE1FF0089DEFF0083DBFF007DD8FF0076D4FF0071D2
      FF006ACEFF0064CCFF00CBF5FC00447F9700C2BEBE0074787800ECEBEB00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      00001D1D82004151B00089AEFD00395DEE001235E6001235E6001336E6003E63
      EF006C8FEA00242F99001865680034DB76000000000030CB7000142C7B003244
      AE005E89FA00254AEB001235E6001235E600163AE7004874F6005781EF001520
      9100A8A8CF0000000000000000000000000021893300268F3D00268D3C00268D
      3C00479A5900137B2A00268D3C00248C3B001F87350007711700D0FFE400B9FC
      CF0049AA6700218E3C00CAD2CC00EDEDED00EDEDED00ECECEC00ECECEC00EBEB
      EB00EBEBEB00EBEBEB00EAEAEA00EAEAEA00E9E9E900F4F4F400FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      000076AE820085858500FFFFFF00F3F3F300F0F0F000F0F0F000EFEFEF00EFEF
      EF00EEEEEE0009822F00DDEFE2004FEF8E0050EE8D004CEC8A004CED8B004BED
      8A004CED8A004BED8A004CED8B0047EC870036EA7D0039EA7E0090FCB8009DDA
      AF000D86330081BB9000FCFCFC00000000000000000000000000EDEDED007478
      7800FDFDFD00EBE9E90074787800E2E1E1007578780074787800747878007478
      7800747878007478780074787800747878007478780074787800747878007478
      780074787800747878007478780074777700C9C4C40074787800D8D4D400FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      00009C9CB4000A1281006983D7006D91F7001538E6001235E6001235E6001336
      E6004C71F2006F91EA00172588001865680030CB700010416B003547AE00678E
      F300264CEB001235E6001235E6001235E6003960F0006491FD002B40AE002E2F
      8D00EAEAED000000000000000000000000000B843100E0F7E800E5FFF300E3FF
      EF00DFFFED00DDFFEB00DAFFEA00D8FFE800D4FFE700D4FFE5009EF8BF0031E9
      7A00ADFFCB0086D29F000C88330082C29500EDEDED00EDEDED00ECECEC00ECEC
      EC00EBEBEB00EBEBEB00EBEBEB00EAEAEA00EAEAEA00F4F4F400FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000DCF1E50085858500FFFFFF00F4F4F400F1F1F100F1F1F100F0F0F000F0F0
      F000EFEFEF0009823000E4EFE7004DF08C004FEE8C0042EC85003AEB7F0035EA
      7C0035EA7C0035EA7C0035EA7C0036EA7D0037EB7D0037EA7D002CE9760076F7
      A700CBFFDD0029974B0038A25500E4E6E4000000000000000000EDEDED007478
      7800FDFDFD00EDEBEB0074787800F4F3F300DCDBDB00DBDADA00DAD8D800D8D7
      D700D6D5D500D5D3D300D4D2D200D3D0D000D1CFCF00CFCDCD00CECBCB00CDCA
      CA00CBC8C800CAC7C700C8C5C500C7C4C400D7D2D200787B7B00D9D4D400FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      0000000000005B739500171E84005E74C8007093F7002245E9001235E6001235
      E6001336E6004165EF007394EA002731990009137900394AAE006C94FA00294D
      EB001235E6001235E6001437E6003A60EF006B94F5003043AE00232C7C00A5B3
      B7000000000000000000000000000000000009822F00DDEFE2004FEF8E0050EE
      8D004CEC8A004CED8B004BED8A004CED8A004BED8A004CED8B0047EC870036EA
      7D0039EA7E0090FCB8009DDAAF000D86330078B38800EBEBEB00EEEEEE00EDED
      ED00EDEDED00ECECEC00ECECEC00EBEBEB00EBEBEB00F5F5F500FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      00007AE4A30085858500FFFFFF00F5F5F500F2F2F200F2F2F200F1F1F100F1F1
      F100F0F0F0000A823000E7EFE90063F39B0063F2990058EF92004FEF8D0045EC
      86003CEA810037EA7C0037EA7D0037EB7D0038EB7E0038EB7E0037EB7D0033E9
      7A0058F19200CAFBDA005BB57800319244000000000000000000EDEDED007478
      7800FDFDFD00EEECEC0075787800747878007478780074787800747878007478
      7800747878007478780074787800747878007478780074787800747878007478
      78007478780074787800747878007478780074787800777A7A00DAD6D600FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      000000000000000000001C247A000F1380006F88D7007395F7001639E6001235
      E6001235E6001336E6005075F2007696EA00546BC600779AF3002A4FEB001235
      E6001235E6001235E6003459ED006F99FB003346AE00060A7D000F1F8E000000
      00000000000000000000000000000000000009823000E4EFE7004DF08C004FEE
      8C0042EC85003AEB7F0035EA7C0035EA7C0035EA7C0035EA7C0036EA7D0037EB
      7D0037EA7D002CE9760076F7A700CBFFDD0029974B00349E5200D4D6D400EEEE
      EE00EDEDED00EDEDED00ECECEC00ECECEC00EBEBEB00F5F5F500FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      000073E4A00085858500FFFFFF00F5F5F500F3F3F300F2F2F200F2F2F200F2F2
      F200F1F1F1000A823100E9EFEA007EF8AB007BF6AA0070F4A30068F39D005DF1
      960055F0900049ED880041ED830039EA7D0036EA7D0037EB7D0037EB7D0033E9
      7B006FF2A100D4FDE2005BB57700319244000000000000000000EDEDED007478
      7800FDFDFD00F0EEEE00EFEEEE00F6F5F500F6F5F500F6F5F500F6F5F500F6F5
      F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5
      F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5F500F6F5F500F0EEEE00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      00000000000000000000000000004868C500141A88006378C8007697F7002346
      E9001235E6001235E6001336E6004468EF006A8EF7002D50EB001235E6001235
      E6001437E600375BED00799EF400384AAE001C288E00557CDD00000000000000
      0000000000000000000000000000000000000A823000E7EFE90063F39B0063F2
      990058EF92004FEF8D0045EC86003CEA810037EA7C0037EA7D0037EB7D0038EB
      7E0038EB7E0037EB7D0033E97A0058F19200CAFBDA005BB578002D8F4100E5EC
      E700EEEEEE00EEEEEE00EEEEEE00EDEDED00EDEDED00F5F5F500FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      000082E6A80085858500FFFFFF00F6F6F600F4F4F400F4F4F400F3F3F300F3F3
      F300F2F2F2000A823100E7EEE90091FCB9008CF8B50082F6AD0079F6A9006DF3
      A00066F19B005AF0940051EE8E0047EE870040EB830036EA7E0027E9730098F9
      BD00E3FFEC002A974C0038A25500E4E6E4000000000000000000EDEDED007478
      7800FDFDFD00F1EFEF00F0EEEE00EEECEC00EDEBEB00ECE9E900EBE8E800E8E4
      E300DAD0C400CEBEA900C4AD8D00C3AB8800C2AA8700C2AA8B00C9B8A300D2C7
      BB00DCD8D700DDD8D800DCD7D700DAD6D600D9D5D500D7D3D300DDD9D900FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000F2AAA000D148600758DD700799A
      F700163AE6001235E6001235E6001336E6001A3DE7001235E6001235E6001235
      E600395DED007FA5FB003B4CAE00070E8900102EC90000000000000000000000
      0000000000000000000000000000000000000A823100E9EFEA007EF8AB007BF6
      AA0070F4A30068F39D005DF1960055F0900049ED880041ED830039EA7D0036EA
      7D0037EB7D0037EB7D0033E97B006FF2A100D4FDE2005BB577002E8F4100E6EC
      E800EFEFEF00EFEFEF00EEEEEE00EEEEEE00EDEDED00F6F6F600FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      000092EAB40085858500FFFFFF00F7F7F700F5F5F500F4F4F400F4F4F400F3F3
      F300F3F3F3000A823100E4EEE700B5FFD000B1FDCC00A7FBC6009EFAC10096F9
      BA008EF8B60084F5AE007DF5AB006FF1A20056EF910055EF9100BAFED400AFDA
      BB000D86340081BB9000FCFCFC00000000000000000000000000EDEDED007478
      7800FDFDFD00F2F1F100F1EFEF00F0EEEE00EEECEC00EDEBEB00D9CCBC00B189
      4F00B0854400AD803D00AD803D00AD803D00AD803D00AD803D00AD803D00AD80
      3D00AE844800D2C8BE00DCD8D800DCD7D700DAD6D600D9D5D500DEDADA00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001435BB000E148700687C
      C8007C9DF7002549EA001336E7001235E6001235E6001235E6001336E600395C
      ED0089AAF4003F4FAE00050F8800102EC9000000000000000000000000000000
      0000000000000000000000000000000000000A823100E7EEE90091FCB9008CF8
      B50082F6AD0079F6A9006DF3A00066F19B005AF0940051EE8E0047EE870040EB
      830036EA7E0027E9730098F9BD00E3FFEC002A974C00359F5200D7D9D700F1F1
      F100F0F0F000F0F0F000EFEFEF00EFEFEF00EEEEEE00F6F6F600FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      00009EEBBA0085858500FFFFFF00F7F7F700F6F6F600F5F5F500F5F5F500F5F5
      F500F4F4F4000E843400F1F7F300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00CAFADD0070F4A300DEFFEC00A2D3B1000682
      2D008CCC9E000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F3F2F200F2F1F100F1EFEF00ECE8E400CAB49600AE823F00DAB9
      7700FFEAAE00FFECB100FFEEB300FFEEB400FFEDB300FFECB100FFEAAD00FFE8
      A800E0BF7A00AF834000C0A88B00DAD4D100DCD8D800DAD6D600DFDCDC00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000102AA1000E12
      810099B4EF00587BF200143AE8001338E7001237E7001235E6001B3EE7007598
      F7005E72C60006097E00102EC900000000000000000000000000000000000000
      0000000000000000000000000000000000000A823100E4EEE700B5FFD000B1FD
      CC00A7FBC6009EFAC10096F9BA008EF8B60084F5AE007DF5AB006FF1A20056EF
      910055EF9100BAFED400AFDABB000D8634007AB58A00EFEFEF00F2F2F200F2F2
      F200F1F1F100F1F1F100F0F0F000F0F0F000EFEFEF00F7F7F700FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000B6EECB0085858500FFFFFF00F8F8F800F6F6F600F6F6F600F5F5F500F5F5
      F500F5F5F5001F883100248D3B00248C3900248C3900248B3900248B3900248B
      3900238B39001F86340007701300FFFFFF00EEFDF30058AA720024913C006F77
      7100000000000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F5F4F400F3F2F200F2F1F100C8B19200AF833F00ECCB8600FFE4
      A200FFE7A700FFE8AA00FFEAAC00FFEAAD00FFE9AB00FFE8AA00FFE6A600FFE4
      A200FFE19C00ECC88000AF823F00BEA68600DDD9D900DCD8D800E0DDDD00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001C48D7000B148B00505C
      AE009CBBFB00385EEF00163DEA00153CE900143AE9001439E8001539E7004C6E
      F00088A4EA002D379900081795001131D7000000000000000000000000000000
      0000000000000000000000000000000000000E843400F1F7F300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CAFADD0070F4
      A300DEFFEC00A2D3B1000C88330086C69900F4F4F400F4F4F400F3F3F300F3F3
      F300F2F2F200F2F2F200F1F1F100F1F1F100F0F0F000F7F7F700FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00F9F9F900F8F8F800F7F7F700F7F7F700F6F6
      F600F6F6F600D3D6D300D2D7D300D2D7D300D1D6D300D1D6D300D0D6D200D0D6
      D200D0D5D200B0C2B5001F883400FFFFFF0051A76C003D9D5300D4E0D7008383
      8300000000000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F6F5F500F5F4F400E3D9CD00B0823D00EDC67B00FFDD9400FFDF
      9800FFE29D00FFE3A000FFE4A200FFE5A200FFE4A100FFE3A000FFE19C00FFDF
      9800FFDC9200FFD98C00EDC27500AE813C00CDBDAB00DDD9D900E2DEDE00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001E4EDA0009198D00545FAE00AEC7
      F400476EF2001943ED001741EC00163FEB00163EEA00153CE900143BE900163B
      E8005E80F3008BA7EA001B218D00081795000000000000000000000000000000
      00000000000000000000000000000000000021893300268F3C00268D3B00268D
      3B00479A5800137B2900268D3B00258D3A002087340007701300FFFFFF00EEFD
      F30058AA7200228F3B00D0D8D200F5F5F500F5F5F500F4F4F400F4F4F400F3F3
      F300F3F3F300F2F2F200F2F2F200F2F2F200F1F1F100F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      000076AE820085858500FFFFFF00FAFAFA00F8F8F800F8F8F800F7F7F700F7F7
      F700F6F6F600F6F6F600F5F5F500F5F5F500F5F5F500F4F4F400F4F4F400F3F3
      F300F3F3F300D0D5D20023934500178E3D00629C7000F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F8F7F700F3F1EF00AF854700E7B96800FFD58400FFD88B00FFDA
      9000FFDD9400FFDE9600FFDF9800FFE09900FFDF9800FFDE9600FFDC9300FFDA
      8F00FFD78A00FFD58400FFD17C00E0AF5D00AD803F00DCD7D500E3DFDF00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      00000000000000000000000000004873DD00141D8C005862AE00B5D0FD005178
      F5001A47EF001946EE001A45EE003C63F1006083F4002D54EE00163EEB00153D
      EA00173DE9005073F1008EAAEA0030389900192998003556DC00000000000000
      000000000000000000000000000000000000DCDEDC00DBE0DC00DBE0DC00DBE0
      DC00C7CEC90072777300DBE0DC00D8DDDA00B5C7BB0020893500FFFFFF0051A7
      6C003D9D5300CDD9D100F7F7F700F6F6F600F6F6F600F5F5F500F5F5F500F5F5
      F500F4F4F400F4F4F400F3F3F300F3F3F300F2F2F200F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000CCD0CD0085858500FFFFFF00FBFBFB00F9F9F900F9F9F900F8F8F800F8F8
      F800F8F8F800F7F7F700F7F7F700F6F6F600F6F6F600F5F5F500F5F5F500F5F5
      F500F4F4F400D2D5D200218C36008FB29400EDEDED00F8F8F800FFFFFF008383
      8300000000000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00F9F8F800E6DCCF00AD803D00FFCB7300FFCF7900FFD27F00FFD4
      8300FFD68800FFD88A00FFD88B00FFD98C00FFD88B00FFD88900FFD68600FFD4
      8300FFD17E00FFCE7800FFCA7100FFC86C00AD803D00D2C6B900E5E1E100FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      000000000000000000007381B600252982005C65AE00C3D9F500547DF600204F
      F2001B4AF1001B49F0003F68F200A4C1FC00A8C0EF0097B5FA00254EEE001740
      EC00163FEB00183FEA006386F40092ACEA0020268C002E378B00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FBFBFB00D7DCD90024944600178E3D0065A0
      7300F8F8F800F8F8F800F7F7F700F7F7F700F6F6F600F6F6F600F5F5F500F5F5
      F500F5F5F500F4F4F400F4F4F400F3F3F300F3F3F300F9F9F900FFFFFF008383
      8300000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FBFBFB00FAFAFA00FAFAFA00F9F9F900F9F9
      F900F8F8F800F8F8F800F7F7F700F7F7F700F6F6F600F6F6F600F5F5F500F5F5
      F500F5F5F500F4F4F400F4F4F400F3F3F300F3F3F300F9F9F900FFFFFF008383
      8300000000000000000000000000000000000000000000000000EDEDED007478
      7800FDFDFD00FBFBFB00E0D3C200AD803D00FFC76B00FFC96F00FFCC7400FFCE
      7800FFD07C00FFD27F00FFD28000FFD38100FFD38000FFD27F00FFD07B00FFCE
      7800FFCB7300FFC96E00FFC76B00FFC76A00AD803D00D1C2B000F0EEEE00FDFD
      FD0074787800EDEDED0000000000000000000000000000000000000000000000
      000000000000202680000D0F7A005F68AE00CEE5FE006F95F9001F51F5001E50
      F4001F4FF300446EF500B0C8F3005761AE0010127E007888C8009FBDFB003960
      F1001842ED001742EC001942EC005478F20096AFEA00323A9900101371002329
      8700000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FCFCFC00D8DBD800228D380094B79800F5F5
      F500F9F9F900F9F9F900F8F8F800F8F8F800F8F8F800F7F7F700F0F0F000F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500EFEFEF00F4F4F4008686
      8600000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FCFCFC00FBFBFB00FBFBFB00FAFAFA00FAFA
      FA00F9F9F900F9F9F900F8F8F800F8F8F800F8F8F800F7F7F700F0F0F000F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500EFEFEF00F4F4F4008686
      8600000000000000000000000000000000000000000000000000F2F2F2007375
      7500EEEEEE00CBCBCB00BCAF9E00B0833E00E3AC5400E3AC5400E3AC5500E3AD
      5600E3AE5900E3AF5A00E3B05C00E3B05C00E3B05C00E3B05B00E3AE5800E3AD
      5600E3AC5500E3AC5400E3AC5400E3AC5400AD803D00BCAF9E00CBCBCB00EEEE
      EE0073757500F2F2F20000000000000000000000000000000000000000000000
      0000AFC5B800393F82005F68AE00CEE1F500739AFB00285CF7002054F6001F53
      F5004773F600B4CFFC005A64AE000F2F77001F83610013247D008FA2D700A3C0
      FB002852F0001944EE001843ED001A43EC00688BF50099B1EA00262B8C005862
      8D00000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FDFDFD00FCFCFC00FBFBFB00FBFBFB00FBFB
      FB00FAFAFA00FAFAFA00F9F9F900F9F9F900F8F8F800F8F8F800848484008383
      8300848484008484840084848400848484008484840084848400848484008686
      8600000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FDFDFD00FCFCFC00FBFBFB00FBFBFB00FBFB
      FB00FAFAFA00FAFAFA00F9F9F900F9F9F900F8F8F800F8F8F800848484008383
      8300848484008484840084848400848484008484840084848400848484008282
      8200000000000000000000000000000000000000000000000000FCFCFC00C6C6
      C6007A7B7B00747878007478780080776700A17D4800AD803D00AD803D00AD80
      3D00AD803D00AD803D00AD803D00AD803D00AD803D00AD803D00AD803D00AD80
      3D00AD803D00AD803D00AD803D00A67D41009C7A490078787500747878007A7B
      7B00C6C6C600FCFCFC0000000000000000000000000000000000000000000000
      000036368F00434A9D00DEF2FF00A0C0FD00235BFA00235AF9002359F8004A78
      F900BED3F4005E67AE0010416B0031CB70000000000029AA6800182680007D8C
      C800A7C3FB003F68F3001A47EF001946EE00224CEF0088A8F900A0B7E8002125
      8A00ADADD1000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FEFEFE00FDFDFD00FDFDFD00FCFCFC00FCFC
      FC00FBFBFB00FBFBFB00FAFAFA00FAFAFA00F9F9F900F9F9F90084848400C9C9
      C900E1E1E100E1E1E100E1E1E100E1E1E100DDDDDD00B1B1B10087878700D6D6
      D600000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FEFEFE00FDFDFD00FDFDFD00FCFCFC00FCFC
      FC00FBFBFB00FBFBFB00FAFAFA00FAFAFA00F9F9F900F9F9F90084848400C9C9
      C900E1E1E100E1E1E100E1E1E100E1E1E100DDDDDD00B1B1B10080808000B9B9
      B900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FDFDFD00F1F1F100EFEF
      EF00EEEEEE00EDEDED00EBEBEB00EAEAEA00E9E9E900E7E7E700E6E6E600E6E6
      E600E6E6E600E6E6E600FDFDFD00787C7C00DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008080B800272A8A00B9C9E700CEE5FE005887FC00245DFA004B7BFB00BAD5
      FD005F68AE001B317E003CCC7700000000000000000000000000409379002427
      870095A6D700B1CCFC003964F2001B48F0006589F600BAD5FD008698D3001B1D
      8300CDCDE3000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FEFEFE00FEFEFE00FDFDFD00FDFDFD00FCFC
      FC00FCFCFC00FBFBFB00FBFBFB00FBFBFB00FAFAFA00FAFAFA0084848400C9C9
      C900E1E1E100E1E1E100E1E1E100E1E1E100B1B1B10087878700D6D6D600FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FEFEFE00FEFEFE00FDFDFD00FDFDFD00FCFC
      FC00FCFCFC00FBFBFB00FBFBFB00FBFBFB00FAFAFA00FAFAFA0084848400C9C9
      C900E1E1E100E1E1E100E1E1E100E1E1E100B1B1B10084848400B9B9B900FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FFFFFF00F3F3F300F1F1
      F100F0F0F000EFEFEF00EDEDED00ECECEC00EAEAEA00E9E9E900E7E7E700E6E6
      E600E6E6E600E6E6E600FFFFFF0074787800DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FCFCFC008E8EBA002C308D00B4C4E400CCE3FE007AA2FD00BFD4F4005F68
      AE0043478C0073D39B000000000000000000000000000000000000000000ADB8
      BE00111D7800828FC800BBD5FD0096B4FA00CBE2FC008D9DD30023248600B0B0
      CB00000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFE00FEFE
      FE00FDFDFD00FDFDFD00FCFCFC00FCFCFC00FBFBFB00FBFBFB0084848400C9C9
      C900E1E1E100E1E1E100DDDDDD00B1B1B10087878700D6D6D600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFEFE00FEFE
      FE00FDFDFD00FDFDFD00FCFCFC00FCFCFC00FBFBFB00FBFBFB0084848400C9C9
      C900E1E1E100E1E1E100DDDDDD00B1B1B10080808000B9B9B900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FFFFFF00F5F5F500F3F3
      F300F2F2F200F0F0F000EFEFEF00EEEEEE00EDEDED00EBEBEB00EAEAEA00E8E8
      E800E7E7E700E6E6E600FFFFFF0074787800DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F5F5F5006464A90032379100CBDEF300D8EEFF005F68AE000C20
      6D00D2DBD8000000000000000000000000000000000000000000000000000000
      000012564000202383009DACD700D8EDFF0092A2D3001F228500B0B0CB00FAFA
      FA00000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFE
      FE00FEFEFE00FDFDFD00FDFDFD00FCFCFC00FCFCFC00FBFBFB0084848400C9C9
      C900E1E1E100E1E1E100B1B1B10087878700D6D6D600FEFEFE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FEFE
      FE00FEFEFE00FDFDFD00FDFDFD00FCFCFC00FCFCFC00FBFBFB0084848400C9C9
      C900E1E1E100E1E1E100B1B1B10084848400B9B9B900FDFDFD00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FFFFFF00F7F7F700F5F5
      F500F4F4F400F2F2F200F1F1F100F0F0F000EEEEEE00EDEDED00EBEBEB00EAEA
      EA00E9E9E900E7E7E700FFFFFF0074787800DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F5F5F5008080B20030348F004C54A300373E81001979
      3B00000000000000000000000000000000000000000000000000000000000000
      000000000000899B9A001B1D7D00626BB00024248600B0B0CB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FEFEFE00FEFEFE00FEFEFE00FDFDFD00FDFDFD0084848400C9C9
      C900DDDDDD00B1B1B10087878700D6D6D6000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FEFEFE00FEFEFE00FEFEFE00FDFDFD00FDFDFD0084848400C9C9
      C900DDDDDD00B1B1B10080808000B9B9B9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FFFFFF00F9F9F900F7F7
      F700F6F6F600F5F5F500F3F3F300F2F2F200F1F1F100EFEFEF00EEEEEE00ECEC
      EC00EBEBEB00E9E9E900FFFFFF0074787800DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F5F5F5005C5CA1001B1B77008DA296000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005D5D740025258500B0B0CB00FAFAFA00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FEFEFE00FEFEFE00FEFEFE00FDFDFD0084848400C9C9
      C900B1B1B10087878700D6D6D600FEFEFE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FEFEFE00FEFEFE00FEFEFE00FDFDFD0084848400C9C9
      C900B1B1B10084848400B9B9B900FDFDFD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF0074787800FFFFFF00FAFAFA00F9F9
      F900F8F8F800F7F7F700F5F5F500F4F4F400F2F2F200F1F1F100EFEFEF00EEEE
      EE00EDEDED00EBEBEB00FFFFFF0074787800DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484009D9D
      9D0087878700D6D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80085858500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00848484009D9D
      9D0080808000B9B9B90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DFDFDF00777A7A00FEFEFE00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FEFEFE00777A7A00DFDFDF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F1F1F10084848400F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500EDEDED00828282008484
      8400D6D6D600FEFEFE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E8E8E80084848400F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500EDEDED00828282008181
      8100B9B9B900FDFDFD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E5E5E5006A6A6A0075787800747878007478
      7800747878007478780074787800747878007478780074787800747878007478
      780074787800747878007478780071747400E5E5E50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F9F9F9008F8F8F0094949400949494009494940094949400949494009494
      940094949400949494009494940094949400949494009494940085858500E1E1
      E100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F4F4F4008B8B8B0091919100919191009191910091919100919191009191
      910091919100919191009191910091919100919191009191910085858500CBCB
      CB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F2F7F200F3F6F300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F3F3F300F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1
      F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1
      F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1F100F1F1
      F100FAFAFA000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E0E0F4009295
      D9004549BD00171BAC00070BA6000514BB000514BB000609A4001518A8004346
      BA009192D600DFE0F30000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000096B496002981290082A08200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FEFEFE00F2F5FA00E2E8F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8
      F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8
      F500E2E8F500E2E8F500E2E8F500E2E8F500E2E8F500EAEEF700FEFEFE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000868686009B9B9B00A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0
      A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0
      A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A000A0A0A0009696
      9600C7C7C7000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D1D2EF00575CC7000A13B200071D
      CB000328E4000131FA000033FF000033FF000033FF000033FF000030F9000226
      E3000419C600060CAA005456C000D0D0ED000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C0CDC0004295
      430013981E001CAD2A0080A08000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F0F3F9001E5FBA000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B400286FBF00C5D1EB000000
      0000000000000000000000000000000000000000000000000000000000000000
      000094949400E1E1E100F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4
      F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4
      F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400F4F4F400CECE
      CE00C7C7C7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000777CD3000D18B6000C29D7000938FA000638
      FF000336FF000134FF000033FF000033FF000033FF000033FF000033FF000033
      FF000033FF000031F900041FD300070EAC007376CD0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7F7F7008CB48C00238F2A0020AF
      32002AC5400020B5320081A18100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004D84C900A9EDF600A9FBFD009FF9FE0092F6FE0090F5FE008DF4FE008CF4
      FE008BF4FE008BF4FE008BF4FE008BF4FE008BF4FE008BF4FE008AF4FE008AF4
      FE008AF4FE008AF4FE0089F4FE008EF6FE00A5FCFE00A4FBFD003770C000FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E4E4E400F3F3F300F2F2F200F1F1F100F0F0F000EFEFEF00EEEE
      EE00EEEEEE00D6D6D600B1B1B100B0B0B000DADADA00EAEAEA00E9E9E900E8E8
      E800E7E7E700E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600EAEAEA00D1D1
      D100C7C7C7000000000000000000000000000000000000000000000000000000
      000000000000F0F0FA003C44C300172AC8001948FA001042FF000D3FFF000B3D
      FF00083AFF000538FF000235FF000033FF000033FF000033FF000033FF000033
      FF000033FF000033FF000033FF000131FA000617BE00373AB800EFEFF9000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F9F9F900BDD1BD002C8E30001FA52F0035D0500032CC
      4B0030C9480028BB3B0082A18200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD009DF8FE005FE6FF003EDDFF0035DAFF002AD7FF0021D5
      FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4
      FF001ED4FF001ED4FF001ED4FF0023D6FF007EF1FF00A5FCFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E4E4E400F3F3F300F3F3F300F2F2F200F1F1F100F0F0F000EFEF
      EF00EEEEEE00C9C9C90013131300131313007F7F7F00EAEAEA00E9E9E900E9E9
      E900E8E8E800E7E7E700E6E6E600E6E6E600E6E6E600E6E6E600EAEAEA00D1D1
      D100C7C7C7000000000000000000000000000000000000000000000000000000
      0000F0F0FA003E47C600263FD3002051FF001849FF001547FF001244FF001042
      FF000D3FFF000A3CFF00073AFF000537FF000235FF000033FF000033FF000033
      FF000033FF000033FF000033FF000033FF000033FF00061DCB00383CBA00EFF0
      F900000000000000000000000000000000000000000000000000000000000000
      000000000000F8F9F8006594650023952D0032C54C003BD458003AD3550037D0
      550036CF51002BBE4200407E400082A1820096B19600B3C7B300E8EBE8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD0093F5FE004BE0FF0034DAFF0029D7FF001BD3FF0010D0
      FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0003CCFF0055E5FF00A7FCFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E5E5E500F5F5F500F4F4F400F3F3F300F2F2F200F1F1F100F0F0
      F000F0F0F000EFEFEF00575757000101010024242400C0C0C000EAEAEA00EAEA
      EA00E9E9E900E8E8E800E7E7E700E6E6E600E6E6E600E6E6E600EAEAEA00D1D1
      D100C7C7C7000000000000000000000000000000000000000000000000000000
      00004149CA002942D6002858FF002051FF001D4EFF001A4CFF001749FF001546
      FF001244FF000F41FF000C3EFF00093CFF000739FF000437FF000134FF000033
      FF000033FF000033FF000033FF000033FF000033FF000033FF00071ECC00383E
      BD0000000000000000000000000000000000000000000000000000000000F6F6
      F6009BB09B00268C2C0030BF480042DB630042DB630042DB630041DA620040D9
      60003DD65C003CD559002DC046002BBF420021AF3400199F26000E8B16001E8A
      220072AF7300B4C4B400F6F6F600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD0097F5FE0055E3FF003FDDFF0034DAFF0027D6FF001CD3
      FF000ED0FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0055E5FF00A7FBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E5E5E500F5F5F500F5F5F500F4F4F400F3F3F300F2F2F200F1F1
      F100F0F0F000F0F0F000B5B5B5001C1C1C000303030079797900EBEBEB00EBEB
      EB00E9E9E900E9E9E900E8E8E800E7E7E700E6E6E600E6E6E600EAEAEA00D1D1
      D100C7C7C7000000000000000000000000000000000000000000000000007C84
      DD002E43D1003868FF002758FF002555FF002253FF001F50FF001C4DFF001A4B
      FF001748FF001446FF001143FF000F41FF000C3EFF00093BFF000639FF000336
      FF000134FF000033FF000033FF000033FF000033FF000033FF000033FF000A1C
      C400777BD3000000000000000000000000000000000000000000F5F8F5005D95
      5D0026A435003BCF590046DF680047E06A0047E06B0049E26D0048E16C0046DF
      670043DC64003ED75E003BD4590037D0530032CC4B002DC7450027C23C0020B8
      320016A3210015901B0053915300BDCEBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD009BF6FE0062E6FF004EE1FF0042DDFF0035DAFF0029D6
      FF001CD3FF0011D0FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0056E5FF00A8FBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E6E6E600F6F6F600F6F6F600F5F5F500F4F4F400F3F3F300F2F2
      F200F1F1F100F1F1F100ECECEC006E6E6E00000000000D0D0D00D9D9D900ECEC
      EC00EBEBEB00EAEAEA00E9E9E900E8E8E800E7E7E700E7E7E700EAEAEA00D1D1
      D100C7C7C7000000000000000000000000000000000000000000D3D6F4001E2E
      C9005480FB00416DFF004872FF00456FFF003462FF002455FF002152FF001F50
      FF001C4DFF00194AFF001648FF001445FF001143FF000E40FF000B3EFF00093B
      FF000638FF000336FF000134FF000033FF000033FF000033FF000033FF000131
      FA000E19B800D1D3F00000000000000000000000000000000000F3F6F300518F
      510027AC3B0041D763004CE572004EE7750050E9780050E979004FE876004DE6
      730049E26D0045DE680041DC62003CD75B0037D1530032CC4B002BC5410026BF
      390020B930001BB62A0013A91D0007850A00729F7200ECEEEC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD009FF7FE006DE9FF005AE4FF004EE1FF0041DDFF0036DA
      FF0029D7FF001DD3FF0010D0FF0006CDFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0057E5FF00A9FBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E6E6E600F7F7F700F7F7F700F5F5F500F5F5F500F4F4F400F3F3
      F300F2F2F200F1F1F100F0F0F000CECECE0006060600000000008A8A8A00ECEC
      EC00EBEBEB00EBEBEB00EAEAEA00E9E9E900E8E8E800E7E7E700EBEBEB00D1D1
      D100C7C7C7000000000000000000000000000000000000000000626CDA005870
      E4007596FF006F90FF006B8DFF00668AFF006286FF00ABBEFF00FFFFFF00FFFF
      FF007492FF001E4FFF001B4DFF00194AFF001647FF001345FF001042FF006887
      FF00FFFFFF00FFFFFF00829BFF000235FF000134FF000033FF000033FF000033
      FF000826DB005A61CC000000000000000000000000000000000000000000F3F3
      F30088BB8800228D2A0043D5650055F0810056EF810056EF820055EE7F0050E9
      7B004DE6740042D56200138A1D00138D1C00148F1F001B9C280020AE310026BF
      3A0023BD34001DB72B0016AF210011AB190009870C00579E5700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00A4F8FE007AEDFF0068E8FF005CE4FF004FE1FF0043DD
      FF0036DAFF002BD7FF001ED4FF0012D0FF0007CEFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0057E5FF00ABFBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E7E7E700F8F8F800F8F8F800F7F7F700F6F6F600F5F5F500F4F4
      F400F3F3F300F3F3F300F2F2F200F1F1F10069696900010101002E2E2E00CDCD
      CD00EDEDED00ECECEC00EBEBEB00EAEAEA00E9E9E900E9E9E900ECECEC00D1D1
      D100C7C7C70000000000000000000000000000000000E2E4F8002536D0008BAA
      FC007999FF007596FF007293FF006E90FF00698CFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007593FF002051FF001E4FFF001B4CFF00184AFF006D8CFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00073AFF000538FF000235FF000033FF000033
      FF000132FB00111DBE00E1E2F600000000000000000000000000000000000000
      000000000000EAEDEA00538A54002DA43D0055EA80005EF88E005BF4880056EF
      820051EB790040D4620083A0830000000000EFF3EF00CBD8CB0093AE93005C94
      5C003193350015951D0013A71E0012AB1B000CA61200038907008AAF8A000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00A7F9FE0085F0FF0074EBFF0068E8FF005BE4FF0050E1
      FF0043DEFF0037DBFF002AD7FF001FD4FF0011D0FF0007CEFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0058E5FF00ACFBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E7E7E700F9F9F900F8F8F800F7F7F700F7F7F700F6F6F600F5F5
      F500F4F4F400F3F3F300F2F2F200F2F2F200BABABA001C1C1C00060606008B8B
      8B00EDEDED00EDEDED00ECECEC00EBEBEB00EAEAEA00E9E9E900ECECEC00D1D1
      D100C7C7C700000000000000000000000000000000009CA4EB005167E2008EAC
      FF00809FFF007C9CFF007899FF007495FF007092FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF007695FF002353FF002051FF007290FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF000C3FFF000A3CFF000739FF000437FF000235
      FF000033FF000C26D500979CE200000000000000000000000000000000000000
      00000000000000000000F6F6F600A6C1A6002A95340045D0680060FB91005AF3
      850052EB7B0042D4640081A18100000000000000000000000000000000000000
      0000DBDFDB00A8C2A80028892A0007880D000CA6130007A00A002F892F00E0E4
      E000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00ACFAFE0092F3FF0082EFFF0076EBFF0069E8FF005EE4
      FF0051E1FF0045DEFF0038DBFF002CD7FF001FD4FF0014D1FF0008CEFF0002CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0059E5FF00ADFBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E8E8E800FAFAFA00FAFAFA00F9F9F900F8F8F800F7F7F700F6F6
      F600F5F5F500F4F4F400F4F4F400F3F3F300EEEEEE007C7C7C00000000001A1A
      1A00E1E1E100EEEEEE00EDEDED00ECECEC00EBEBEB00EAEAEA00EDEDED00D1D1
      D100C7C7C700000000000000000000000000000000005866DF007D97F2008CAA
      FF0086A5FF0083A2FF007F9EFF007B9BFF007798FF00A8BDFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF007896FF007795FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF006C8CFF001243FF000F41FF000C3EFF00093CFF000639
      FF000436FF00082FEB004F58CF00000000000000000000000000000000000000
      000000000000000000000000000000000000EDEEED0079A779002CA13D0047D7
      6B0053EE7C0041D4620081A18100000000000000000000000000000000000000
      00000000000000000000FEFEFE00AFC4AF00228322000493070004890400B4BD
      B400000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00AFFBFE009DF6FF008EF2FF0082EFFF0075EBFF006AE8
      FF005DE5FF0051E2FF0044DEFF0039DBFF002BD7FF0020D4FF0013D1FF0009CE
      FF0001CCFF0001CCFF0001CCFF0002CCFF0059E5FF00AEFBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E8E8E800FBFBFB00FAFAFA00F9F9F900F9F9F900F8F8F800F7F7
      F700E2E2E2008F8F8F007A7A7A00797979007979790065656500000000000000
      000092929200EFEFEF00EDEDED00EDEDED00ECECEC00EBEBEB00EEEEEE00D1D1
      D100C7C7C700000000000000000000000000000000003043DA00A1BDFC008EAB
      FF008CAAFF0089A7FF0085A4FF0082A1FF007E9DFF007A9AFF00AABFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF007190FF00194BFF001648FF001445FF001143FF000E40FF000B3E
      FF00093BFF000737FB002531C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000ADBEAD003992
      3C0033BC4D003FD4600080A08000000000000000000000000000000000000000
      000000000000000000000000000000000000CAD5CA00338B330000850000ACB6
      AC00000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B4FCFE00A9F9FF009CF6FF0090F2FF0083EFFF0078EB
      FF006BE8FF005FE5FF0052E2FF0046DEFF0039DBFF002ED8FF0021D4FF0015D1
      FF0009CEFF0002CCFF0001CCFF0002CCFF005AE5FF00B0FBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E9E9E900FCFCFC00FBFBFB00FAFAFA00FAFAFA00F9F9F900F8F8
      F800CECECE002929290000000000000000000000000000000000000000000000
      000050505000F0F0F000EFEFEF00EEEEEE00EDEDED00ECECEC00EFEFEF00D1D1
      D100C7C7C700000000000000000000000000000000002D42DE00A9C4FF0091AE
      FF0090ACFF008EABFF008CA9FF0088A6FF0084A3FF0081A0FF007D9DFF00ACC0
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF007695FF002152FF001E4FFF001B4DFF00194AFF001648FF001345FF001042
      FF000E40FF000B3DFF001927C600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000082A882002388270082A38200000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EAEDEA001A7B1A00B3BC
      B300000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B8FCFE00B4FCFF00A8F9FF009CF6FF008FF2FF0084EF
      FF0077ECFF006CE9FF005EE5FF0053E2FF0046DFFF003ADBFF002DD8FF0022D5
      FF0015D1FF000ACFFF0001CCFF0002CCFF005BE5FF00B0FBFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000096969600E9E9E900FDFDFD00FCFCFC00FBFBFB00FBFBFB00FAFAFA00F9F9
      F900DCDCDC006E6E6E0052525200525252005252520051515100515151005050
      500086868600F1F1F100EFEFEF00EFEFEF00EEEEEE00EDEDED00EFEFEF00D2D2
      D200C7C7C700000000000000000000000000000000004960E700ABC6FF0095B0
      FF0093AFFF0091ADFF008FABFF008DAAFF008BA8FF0087A5FF0083A2FF00809F
      FF00AEC2FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007B99
      FF002959FF002656FF002354FF002151FF001E4FFF001B4CFF00184AFF001547
      FF001344FF001042FF001730D700000000000000000000000000000000000000
      0000FEFEFE000000000000000000000000000000000000000000000000000000
      0000FEFEFE00CED3CE00D1DED1000000000000000000FAFAFA00000000000000
      0000000000000000000000000000000000000000000000000000CBD7CB00E8EB
      E800000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B9FDFE00BCFEFF00B5FCFF00AAF9FF009EF6FF0093F3
      FF0086F0FF007AEDFF006DE9FF0062E6FF0054E3FF0049DFFF003CDCFF0030D9
      FF0023D5FF0018D2FF000CCFFF0006CCFF0060E7FE00B2FCFD000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      000082829400C3C3DA00D4D4EB00D3D3EA00D2D2EA00D2D2E900D1D1E800D0D0
      E800CFCFE700CFCFE600CECEE600CECEE500CDCDE400CCCCE400CBCBE300CACA
      E200CACAE100C9C9E100C8C8E000C8C8DF00C7C7DE00C6C6DE00C8C8E000AFAF
      C600C1C1C700000000000000000000000000000000004B62EA00B1CAFF0098B2
      FF0096B1FF0094AFFF0092AEFF0090ACFF008EABFF008CA9FF008AA7FF0086A4
      FF00B1C4FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF007D9B
      FF002E5EFF002B5BFF002859FF002656FF002353FF002051FF001D4EFF001A4C
      FF001849FF001547FF002C44DA00000000000000000000000000000000000000
      0000B9C2B9003F923F00F7F7F700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084A584003E933E00BDCB
      BD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400AAEBF600B9FCFD00BBFDFE00BAFCFE00B5FBFE00ACF9FE00A5F7
      FE009BF4FE0094F2FE008CF0FE0084EEFE007BEBFE0074EAFE006BE7FE0063E5
      FE005AE2FE0053E0FE0049DFFE0048DFFE009BF4FE00AEF5FA000053B400FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      00001E1E8E0027279C002A2AA0002A2AA0002A2A9F002A2A9F002A2A9F002929
      9F0029299F0029299F0029299E0029299E0029299E0029299E0028289E002828
      9E0028289E0028289E0028289D0028289D0028289D0027279D0028289D002323
      98009A9ACB00000000000000000000000000000000002A42E600B3CCFF009CB6
      FF0099B3FF0097B2FF0095B0FF0093AFFF0091ADFF0090ACFF008EABFF00B7C9
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF007F9DFF003060FF002D5DFF002B5BFF002858FF002556FF002253FF001F50
      FF001D4EFF001A4BFF001F30D100000000000000000000000000000000000000
      0000ACB6AC0008810C007DAB7D00FBFBFB000000000000000000000000000000
      0000000000000000000000000000000000000000000080A180000F9816001A88
      1D0088B28800F4F4F40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004A83C80075AFDB00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FC
      FD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FC
      FD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD0076B2DC003A75C2000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000611A0002154EC002865FF002865FF002865FF003C73FF006D95FD00537F
      FB002A5EF8005980F8003360F4001B4BF1004F71F2004163EF004967EE002D4E
      EA001537E6004661EB004661EB001235E6001235E6001235E6001235E6000C23
      C8008D8DCC00000000000000000000000000000000003950EA00ACC5FE00A4BD
      FF009CB5FF009AB4FF0098B2FF0096B1FF0094B0FF0093AEFF00BACCFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00809EFF003262FF00305FFF002D5DFF002A5AFF002758FF002455
      FF002253FF003563FC002D3FD600000000000000000000000000000000000000
      0000B0B9B00017A22200169C220049994A00DADEDA0000000000000000000000
      0000000000000000000000000000000000000000000081A1810018AC25001BB6
      28000E95150021862200BBD0BB00F7F7F7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FEFEFE00316AAF000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4002A5EA100FCFCFC000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000611A0002154EC002865FF002865FF002865FF004278FF00A7BFFF00D6E1
      FE004574FA00C0CFFD0091A9F900426AF400DCE3FC007B93F400B1BFF800627A
      EF003C5AEA00D7DDFA00B0BBF6001235E6001235E6001235E6001235E6000C23
      C8008D8DCC00000000000000000000000000000000006275F0008CA5F900AEC6
      FF009FB7FF009DB6FF009BB4FF0099B3FF0098B2FF00BDCEFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B6C9FF00B5C7FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00829FFF003564FF003261FF002F5FFF002C5CFF002A5A
      FF002757FF00385FF3005968E100000000000000000000000000000000000000
      0000C9CFC90024922A002CC6420022AF34002C8A2E007DA47D00F8F9F8000000
      0000000000000000000000000000000000000000000081A181001DAF2B001EB7
      2E0019B3260013A71D001587180064946400F1F3F10000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD008E756A00F3DED700FDE9E200E9D2C8008E756A00FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000611A0002154EC002865FF002865FF002865FF002865FF003971FE00C9D8
      FE006C91FB007899FB00ECF0FE00CDD8FC00DFE5FC004568F100B1BFF9006981
      F100BDC7F800CDD4F900B0BBF6001235E6001235E6001235E6001235E6000C23
      C8008D8DCC0000000000000000000000000000000000A3AFF700637DF500B7CF
      FF00A3BBFF00A0B8FF009EB7FF009CB5FF009AB4FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00B9CBFF008EAAFF008CA9FF00B6C9FF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF003A69FF003766FF003464FF003161FF002F5E
      FF003060FF003E5CE9009EA8EF00000000000000000000000000000000000000
      0000FCFCFC006A9E6A0026B63A0035CF500037D1530028B43D001B8A21005BA5
      5D00ACC5AC00CCD1CC00E7E9E700F9F9F9000000000082A1820022B5320024BD
      36001FB82F001BB4290015B020000E9E150019811A0097AD9700F6F6F6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FEFEFE008E756A00F4E1D800FEEBE400F2DED5008E756A00FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000611A0002154EC002865FF002865FF002865FF002865FF003A71FF00C9D8
      FF006D93FC003B6CF900D3DDFD008EA7F900BFCCFA002650EF00B2C0F900AAB8
      F700D1D8FA008597F200B0BBF6001235E6001235E6001235E6001235E6000C23
      C8008D8DCC0000000000000000000000000000000000E5E8FD003A56F300B2C9
      FE00AFC6FF00A3BBFF00A1B9FF009FB8FF009DB6FF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00BCCDFF0093AEFF0091ADFF008FABFF008DAAFF00B7C9FF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF003F6DFF003C6BFF003968FF003666FF003363
      FF004E7BFD002B43E100E3E6FA00000000000000000000000000000000000000
      000000000000E9EAE9001E8521002FBF47003ED85D003ED85F003DD55C0035C5
      4F0029AD3D00329F3C003E9742004A954A00557E55002B702B0024B7380028C1
      3C0022BB35001FB82E0018B1260015AF1F000DA1140008880B005F985F00DFE7
      DF00000000000000000000000000000000000000000000000000000000000000
      000000000000FEFEFE008E756A00EFDDD500FEEDE700FDEAE4008E756A00DEDC
      DB00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000611A0002154EC002865FF002865FF003065E9003265E5003B72FA00C9D8
      FF006D94FD00235BF900C3D1FD00E4EAFE007793F7001A48F000B2C1F900FBFC
      FE005B75EF006179EF00B3BDF1001E3CD2001D3BD4001436E2001235E6000C23
      C8008D8DCC000000000000000000000000000000000000000000667BF8007891
      FA00BAD1FF00AAC0FF00A4BBFF00A2BAFF00A0B9FF00CFDCFF00FFFFFF00FFFF
      FF00BFD0FF0097B2FF0096B0FF0094AFFF0092AEFF0090ACFF008EABFF00B8CA
      FF00FFFFFF00FFFFFF00A3BAFF004472FF004170FF003E6DFF003B6AFF00406F
      FF004A6DF1006B7BEB0000000000000000000000000000000000000000000000
      00000000000000000000D3DFD300428943002EB9460044DC660048E16C0047E1
      6B0048E26B0046E1690044DE660040DB61003CD85A0038D3540031CA4A002DC6
      440027C03B0022BB34001DB62A0018B1240012AB1B000DA71400069709000179
      010092B29200F9FAF90000000000000000000000000000000000000000000000
      000000000000FEFEFE008E756A00EFDDD500FEEDE700FDEAE4008E756A000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008E756A008E756A008E756A008E756A008E756A0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000009119C002051E8002865FF003C64CB00586482005A647E00506DB60095B2
      FA005683FD00245CFA007395FA00B3C4FB003F68F4001B49F1007F98F5009BAD
      F7001B41E9004764EC009EA3B900555B7B00545B7D003247B3001235E6000B20
      C3008F8FCB000000000000000000000000000000000000000000D8DEFD003D5B
      F900B5CBFF00B9CFFF00A8BFFF00A5BCFF00A4BBFF00A2BAFF00A0B8FF009EB7
      FF009CB5FF009AB4FF0098B3FF0097B1FF0095B0FF0093AFFF0091ADFF008FAC
      FF008EAAFF007094FF004F7CFF004977FF004674FF004372FF004472FF006592
      FE002F49E800D6DBFA0000000000000000000000000000000000000000000000
      0000000000000000000000000000E5E6E50064A865001F92290042D5640050EA
      78004DE775004BE4710047E16A0043DC65003ED75D003AD3570033CC4E0030C9
      480029C23E0025BE38001EB72F001AB3270014AD1D000FA8170009A40D000393
      050019741900EEF5EE0000000000000000000000000000000000000000000000
      000000000000FEFEFE00C2AEA400E4D1C900FEEFE900FEEDE700D0B8AD00B7A8
      A100FEFEFE000000000000000000000000000000000000000000FEFEFE00D4D0
      CF00BA9D8F00F6DCD100FEE4DA00FDE2D700B99E9100E2E0DF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000045469F001028BB001A43D9004B598B00AAA7A700CBC7C700575E77002648
      C5001941D800183ED600173BD4001538D3001435D1001232D000112ECE00102C
      CC000E28CB000D26C9006F6F7400C0BDBD00AFACAC00575C7D000B21C500040B
      A100B9B9D8000000000000000000000000000000000000000000000000008E9F
      FD005875FD00BFD4FF00BAD0FF00AAC0FF00A6BDFF00A5BCFF00A3BAFF00A1B9
      FF009FB8FF009DB6FF009BB5FF009AB3FF0098B2FF0096B1FF0094B0FF0092AE
      FF0091ADFF008EABFF0089A7FF007397FF005E87FF005882FF006E98FF00415F
      F0008896F2000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E9ECE9006C986C003F99
      450034B1480038C355003DCF5D003FD35F003ED45D0039CF570038D2540033CC
      4C002DC6430027C03B0021BA31001CB52B0015AF21000FA31600108613005B8E
      5B00E9ECE9000000000000000000000000000000000000000000000000000000
      00000000000000000000D1C8C400C9B2A700FDF0EA00FEF0EA00F8E8E200C8AF
      A300B1A29A00D6D4D300F1F1F100FEFEFE00F6F5F500E4E2E200B6ADA900B498
      8900F4DCD300FEE7DE00FEE6DD00EAD0C500B3A09700FBFBFB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BFBFCC005E5EAC0042429D0056567800B7B4B400DDD9D9005E5E6D004747
      940042429D0042429D0042429D0042429D0042429D0042429D0042429D004242
      9D0042429D0042429D0079787800CBC8C800BEBBBB006767760044449F007070
      B200F0F0F1000000000000000000000000000000000000000000000000000000
      00005D76FE006B87FF00C0D5FF00BBD0FF00ACC2FF00A8BEFF00A6BDFF00A4BB
      FF00A2BAFF00A0B9FF009FB7FF009DB6FF009BB4FF0099B3FF0097B2FF0095B0
      FF0093AFFF0092AEFF0090ACFF008EABFF008EABFF00A0BEFF005B77F500556B
      F100000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7EA
      E700BCC3BC008FB38F0060A86300479E4B0035983B002088260034C84D0035CE
      4F002DC6440029C23E0022BB34001FBA2D00109A180019841B00B2CAB200F6F6
      F600000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EAE9E800B59A8F00F5E9E300FEF1EC00FEF1EC00F9EB
      E500D8C5BB00C0A69C00AC978D0097877F00B59D9000BBA19500D0B8AD00F0DB
      D200FEE9E100FEE8E000F9E3DA00BD9F9200DFDBD80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000081818100BBB9B900E3DFDF007E7E7E00DADA
      DA00000000000000000000000000000000000000000000000000000000000000
      000000000000000000007A797900D1CECE00C2C0C00086868600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F2F4FF005D77FF006B87FF00C1D5FF00BED3FF00B2C7FF00A9BFFF00A7BD
      FF00A5BCFF00A4BBFF00A2BAFF00A0B8FF009EB7FF009CB5FF009AB4FF0098B3
      FF0096B1FF0095B0FF0093AFFF009AB6FF00A7C4FF005D7AF800576EF400F2F3
      FE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000083A1830031C54A0036CF
      50002FC847002BC540001CAB2A001B8C1F007CAA7C00F0F1F000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D4D0CF00B89E9000F3E7E200FEF3EE00FEF2
      ED00FEF1EC00FDEFEA00FCEEE800FCEDE700FCECE600FDECE600FEECE500FEEC
      E500FEEBE400FDE9E200CCB2A500B6A7A000FEFEFE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000081818100BFBDBD00E8E5E5007E7E7E00DADA
      DA00000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7A7A00D5D3D300C6C4C40086868600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F2F4FF005D77FF005A78FF00B9CEFF00C0D5FF00BCD2FF00B3C8
      FF00AAC1FF00A6BDFF00A4BCFF00A3BAFF00A1B9FF009FB8FF009DB6FF009BB5
      FF009BB5FF00A0BBFF00ABC6FF00A5C0FF00516EF9005970F700F2F3FE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000081A1810031C54A0036CF
      51002EC946001BA42A00368E3800B5C4B5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FEFEFE00C9B8B000B9A09300F8EEEA00FDF2
      EE00FEF3EE00FEF2ED00FEF1EC00FFF1EC00FEF0EA00FEEFE900FEEEE800FEED
      E600FBE9E200D3BBAF00B9A29600E9E7E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000081818100C2C2C200ECEAEA007E7E7E00DADA
      DA00000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7A7A00D9D8D800CAC9C90086868600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008FA1FF004160FF007D97FF00B8CEFF00C0D5
      FF00BED4FF00B9CFFF00B5CCFF00B0C7FF00AEC5FF00ADC4FF00AFC7FF00B0C9
      FF00B3CCFF00AAC4FF007390FE003D5CFB008D9FFC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000081A1810030C3480029BA
      3F00258A29008AAD8A0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FBFBFB00D4CECB00B49A8E00D0BD
      B400EBDDD700F6EAE500FDF2ED00FDF1ED00FDF1EC00F9ECE600EEDDD600DFCA
      C100B99E9200B9ABA400F9F8F800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008686860098989800B1B0B00084848400E3E3
      E300000000000000000000000000000000000000000000000000000000000000
      000000000000000000007B7B7B00A7A7A7009A99990094949400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D9DFFF00768CFF004160FF006B87
      FF0096AEFF00B7CCFF00BED3FF00BCD2FF00BBD1FF00BAD1FF00B0C8FF008FA9
      FF006784FF00405FFF006981FF00D9DFFE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000007F9F7F00118D1C004194
      4300D1D5D100FEFEFE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E2E0DF00D9D1
      CE00C5B1A800BBA39700B89E9100B89E9100B89D9000B5998B00C3ADA300D4C6
      C000D7D5D400F7F7F70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000F4F4F400C2C2C200BCBCBC00E3E3E3000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E5E5E500BFBFBF00C6C6C600EFEFEF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E6EAFF00A7B5
      FF006981FF004462FF004060FF005977FF005977FF003858FF004462FF006981
      FF00A7B5FF00E6EAFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C9D7C900C5DDC500FEFE
      FE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EAEAEE009E9ED7009595D300C4C4
      E600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000070AA7000016C010002760300017602000175020001750100006B000070AA
      7000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DCDCE0002E2FAB002733C100303FD1001319
      B1005757BA00E1E1EA0000000000000000000000000000000000000000000000
      0000FDFDFD00E7ECF600CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9
      EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9
      EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00CFD9EE00DAE1F100FDFDFD000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BFD9
      BF00027003000AA00F0009A20E0008A10B00069F0900059E070003990500006F
      0100AFCFAF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000DCDCE0004747A700313FC300607DF7005A75FF00475C
      F300191FB4005757BA0000000000000000000000000000000000000000000000
      0000E4E9F4001156B6000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B400236CBD009FB3DE000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000080B5
      8100088A0D000EA715000CA512000BA4100009A20E0008A10B00069F09000383
      04007FB37F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DCDCE0002F30AB003A4BC3006C90F800234BD2000130B9001D44
      D1004154F3001116B100C4C4E600000000000000000000000000000000000000
      00003272C100A9EDF600A9FBFD009FF9FE0092F6FE0090F5FE008DF4FE008CF4
      FE008BF4FE008BF4FE008BF4FE008BF4FE008BF4FE008BF4FE008AF4FE008AF4
      FE008AF4FE008AF4FE0089F4FE008EF6FE00A5FCFE00A3FBFD002765BB00FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000081B7
      82000B8D110011AA1A0010A917000EA715000CA512000BA4100009A20E000484
      06007FB37F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DCDCE0004747A7003E51C30080A6F600284CDC00022BC400002FB9000130
      B8004D64FF002733D1009595D300000000000000000000000000000000000000
      00000053B400B9FCFD009DF8FE005FE6FF003EDDFF0035DAFF002AD7FF0021D5
      FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4FF001ED4
      FF001ED4FF001ED4FF001ED4FF0023D6FF007EF1FF00A5FCFD000053B400FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000082B9
      84000E91160015AE1F0013AC1D0011AA1A0010A917000EA715000CA512000686
      08007FB37F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DCDC
      E0002F30AB003E51C3007FA8FB002D4CE500001FD0000026C6000230BC001D47
      CE004E65F7001E27C1009E9ED700000000000000000000000000000000000000
      00000053B400B9FCFD0093F5FE004BE0FF0034DAFF0029D7FF001BD3FF0010D0
      FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0003CCFF0055E5FF00A7FCFD000053B400FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000083BB
      860011951A0019B2250017B0220015AE1F0013AC1D0011AA1A0010A917000888
      0C007FB37F000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DCDCE0004747
      A7003E51C30082AAF7002D46EC00031ADE00001ED3000024C900224AD2005979
      F6002632C3002D2EAB00EAEAEE00000000000000000000000000000000000000
      00000053B400B9FCFD0097F5FE0055E3FF003FDDFF0034DAFF0027D6FF001CD3
      FF000ED0FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0055E5FF00A7FBFD000053B400FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000084BD
      880014991F001DB62B001BB4280019B2250017B0220015AE1F0013AC1D000A8C
      0F0080B581000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DCDCE0002F30AB003E51
      C3007FA6FD002D42F3000010E7000015E000031FD600294DDD006C8DF6002F3D
      C3004747A700DCDCE00000000000000000000000000000000000000000000000
      00000053B400B9FCFD009BF6FE0062E6FF004EE1FF0042DDFF0035DAFF0029D6
      FF001CD3FF0011D0FF0005CDFF0001CCFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0056E5FF00A8FBFD000053B400FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000085BF
      8A00179D250021BA31001FB82E001DB62B001BB4280019B2250017B022000D8F
      140081B682000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EBE7E300CFC0AA00CFC0
      AA00CFC0AA00D7CAB800F0EDEA0000000000DCDCE0004747A7003E51C30082A9
      F7002D3EF800030DF100000EE9000013E2002D4CE5007BA3F9003647C3002E2F
      AB00DCDCE0000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD009FF7FE006DE9FF005AE4FF004EE1FF0041DDFF0036DA
      FF0029D7FF001DD3FF0010D0FF0006CDFF0001CCFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0057E5FF00A9FBFD000053B400FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000086C2
      8C001BA22A0025BE380023BC350021BA31001FB82E001DB62B001BB428001093
      180081B884000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F3F3
      F800BFD0D1007D9EBB0095C5D1009BC6D1009FC4D1009EC3D1009EC3D1004995
      6F00C7E1DD007AD8AB005DD599005DD599005DD599005DD5990077D8AA00BDE1
      D60047956E0086BAD10087BBD10087BCD10095C5D10095C5D1007D9EBB00BFD0
      D100C0D1D100FCFCFD0000000000000000000000000000000000000000000000
      00000000000000000000E0DDD800C3A88300B87D2600B4710B00BA780F00BB78
      0F00BA770C00B7740900B36F0B00BD873900614B79004550B20083ABFE002F3E
      FC000004F8000007F3000310EC002D46ED0082AAF6003E50C3004747A700DCDC
      E000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00A4F8FE007AEDFF0068E8FF005CE4FF004FE1FF0043DD
      FF0036DAFF002BD7FF001ED4FF0012D0FF0007CEFF0001CCFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0057E5FF00ABFBFD000053B400FDFD
      FD0000000000000000000000000000000000000000000000000000000000B8E2
      BF008DCF99008CCD97008BCC96008BCA95008AC9930089C8920089C691004DA8
      58001EA730002AC33E0027C03B0025BE380023BC350021BA31001FB82E001397
      1D0045994A0082B9850082B8840081B7830081B7820080B6810080B581007FB4
      8000CFE2CF000000000000000000000000000000000000000000000000003542
      8F0020289100303CA1002E3BA1002D3AA1002B39A1002A38A1002837A1002735
      A1002534A1002433A1002232A1002131A1001F2FA1001E2EA1001C2DA1001B2C
      A100192BA100182AA1001629A1001528A1001427A1001225A1001124A1000F23
      A100030B7F00C0D1D10000000000000000000000000000000000000000000000
      0000FCFCFC00D5C8B500B9802E00BC7D1900D2992F00DBA33600DEA63500DEA5
      3200DCA02B00D89B2400D0901A00C7851200AB690B009D7643007D92DB001921
      FD000004F9000007F5002D42F3007FA7FC003E51C3002F30AB00DCDCE0000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00A7F9FE0085F0FF0074EBFF0068E8FF005BE4FF0050E1
      FF0043DEFF0037DBFF002AD7FF001FD4FF0011D0FF0007CEFF0001CCFF0001CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0058E5FF00ACFBFD000053B400FDFD
      FD0000000000000000000000000000000000000000000000000082CF920025AE
      400031C14F002FBE4C002DBB49002BB845002AB6420027B33F0026B13C0024AE
      390029BA3F002EC745002CC542002AC33E0027C03B0025BE380023BC35001CAB
      2A001499200013971D0011951B00109318000E9116000D8F14000B8D12000A8C
      10000473060070AC71000000000000000000000000000000000000000000252C
      91006390FD006390FD006390FD006390FD006390FD006390FD006390FD006390
      FD006390FD006390FD006390FD006390FD006390FD006390FD006390FD006390
      FD006390FD006390FD006390FD006390FD006390FD006390FD006390FD006390
      FD001225A100BFD0D1000000000000000000000000000000000000000000FCFC
      FC00D4B18000B6771700D7A33F00E8B85200E7B64E00E6B44900E4B04300E3B1
      4800E1AB3D00DFA53200DCA12C00DB9E2700D6971E00C3801000A87224007F84
      B7001923FB002F41F90082AAF7003E51C3004747A700DCDCE000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00ACFAFE0092F3FF0082EFFF0076EBFF0069E8FF005EE4
      FF0051E1FF0045DEFF0038DBFF002CD7FF001FD4FF0014D1FF0008CEFF0002CC
      FF0001CCFF0001CCFF0001CCFF0002CCFF0059E5FF00ADFBFD000053B400FDFD
      FD0000000000000000000000000000000000000000000000000027B4460049E1
      6E0049E26E0046DF6A0044DD660041DA62003FD85E003CD55B003AD3570038D1
      530035CE500033CC4C0030C949002EC745002CC542002AC33E0027C03B0025BE
      380023BC350021BA31001FB82E001DB62B001BB4280019B2250017B0220015AE
      1F0012A81B00047106000000000000000000000000000000000000000000252C
      910093B5FC001C4AF1001A48EF001A48EF001945EE001843ED001740EC00163E
      EA00153BE9001439E8001337E7001235E6001235E6001235E6001235E6001235
      E6001235E6001235E6001235E6001235E6001235E6001235E6002047EC004579
      FD001729A100BFD0D1000000000000000000000000000000000000000000CDBD
      A700B6781800DBAA4A00EDC26000EBBF5C00EBBF6000F0D08B00F6E4BD00F7E9
      C900F6E5C300F2DBAC00E7BC6700DFA63600DCA02A00DA9C2400C27F1000A872
      24007D92DA0083ABFE003E51C3002F30AB00DCDCE00000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00AFFBFE009DF6FF008EF2FF0082EFFF0075EBFF006AE8
      FF005DE5FF0051E2FF0044DEFF0039DBFF002BD7FF0020D4FF0013D1FF0009CE
      FF0001CCFF0001CCFF0001CCFF0002CCFF0059E5FF00AEFBFD000053B400FDFD
      FD0000000000000000000000000000000000000000000000000032C5560051EA
      79004EE775004BE4720049E26E0046DF6A0044DD660041DA62003FD85E003CD5
      5B003AD3570038D1530035CE500033CC4C0030C949002EC745002CC542002AC3
      3E0027C03B0025BE380023BC350021BA31001FB82E001DB62B001BB4280019B2
      250017B0220009800E000000000000000000000000000000000000000000252C
      9100A3C2FD001E50F4001D4DF3001D4DF3001C4BF1001B49F0001A46EF001944
      EE001841EC00163FEB00153CEA00143AE9001337E7001236E6001235E6001235
      E6001235E6001235E6001235E6001235E6001235E6001235E600254BEC005686
      FD001D2EA100BFD0D10000000000000000000000000000000000DCD3C800B77C
      2600E2B65900F1CB6E00F0C96D00F3D59100FCF6E300FFFCEA00FFFEE700FFFF
      E600FFFFE700FFFFEA00FEFDF000F7EACC00E5B65800DEA53300DBA02A00C584
      16009D7643004550B2004747A700DCDCE0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B4FCFE00A9F9FF009CF6FF0090F2FF0083EFFF0078EB
      FF006BE8FF005FE5FF0052E2FF0046DEFF0039DBFF002ED8FF0021D4FF0015D1
      FF0009CEFF0002CCFF0001CCFF0002CCFF005AE5FF00B0FBFD000053B400FDFD
      FD0000000000000000000000000000000000000000000000000036CB5C0056EF
      810053EC7D0051EA79004EE775004BE4720049E26E0046DF6A0044DD660041DA
      62003FD85E003CD55B003AD3570038D1530035CE500033CC4C0030C949002EC7
      45002CC542002AC33E0027C03B0025BE380023BC350021BA31001FB82E001DB6
      2B001BB428000B8412000000000000000000000000000000000000000000252C
      9100B0CDFD002055F6001F52F5001F52F5001E50F4001D4DF2001C4BF1001B49
      F0001A46EF001844ED001741EC00163FEB00153CEA00143AE8001337E7001236
      E6001235E6001235E6001235E6001235E6001235E6001235E600284EEC006390
      FD002232A100BFD0D10000000000000000000000000000000000CFA87000C68E
      3000F5D17900F3CF7600F6DC9D00FDF5DC00FFF6CF00FFF8D200FFFBDE00FFFE
      E500FFFFE700FFFFE700FFFFE700FFFFEC00F8EDD300E6B95C00DFA73500DBA0
      2E00AC6B0F00614B7900DCDCE000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B8FCFE00B4FCFF00A8F9FF009CF6FF008FF2FF0084EF
      FF0077ECFF006CE9FF005EE5FF0053E2FF0046DFFF003ADBFF002DD8FF0022D5
      FF0015D1FF000ACFFF0001CCFF0002CCFF005BE5FF00B0FBFD000053B400FDFD
      FD000000000000000000000000000000000000000000000000003AD163005BF4
      8A0059F2850056EF810053EC7D0051EA79004EE775004BE4720049E26E0046DF
      6A0044DD660041DA62003FD85E003CD55B003AD3570038D1530035CE500033CC
      4C0030C949002EC745002CC542002AC33E0027C03B0025BE380023BC350021BA
      31001FB82E000E8816000000000000000000000000000000000000000000252C
      9100BDD7FE00235AF9002258F8002258F8002155F7002053F6001F50F4001D4E
      F3001C4BF2001B49F1001A47EF001944EE001842ED001740EC00163DEA00143B
      E9001338E7001236E6001235E6001235E6001235E6001235E6002D51EC00749D
      FD002836A100BFD0D100000000000000000000000000F6F5F300B6761700E8C0
      6900F7D68100F7DA8F00FEF7E000FFF1C000FFF8DE00FFF6D200FFF8D200FFFB
      DB00FFFEE500FFFFE600FFFFE700FFFFE700FFFFEC00F8ECCF00E4B04600E2AC
      3C00CC902400BD88390000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400B9FCFD00B9FDFE00BCFEFF00B5FCFF00AAF9FF009EF6FF0093F3
      FF0086F0FF007AEDFF006DE9FF0062E6FF0054E3FF0049DFFF003CDCFF0030D9
      FF0023D5FF0018D2FF000CCFFF0006CCFF0060E7FE00B2FCFD000053B400FDFD
      FD000000000000000000000000000000000000000000000000003ED86A0061FA
      92005EF78E005BF48A0059F2850056EF810053EC7D0051EA79004EE775004BE4
      720049E26E0046DF6A0044DD660041DA62003FD85E003CD55B003AD3570038D1
      530035CE500033CC4C0030C949002EC745002CC542002AC33E0027C03B0025BE
      380023BC3500108C1B000000000000000000000000000000000000000000252C
      9100BFD9FE00255FFC00245DFA00245DFA00235AF9002258F8002155F7001F53
      F5001F50F4001D4EF3001C4CF2001B49F0001A47EF001944EE001842ED00163F
      EB00163DEA00143BE8001338E8001236E6001235E6001235E6003154EC0081A8
      FD002D3AA100BFD0D100000000000000000000000000ECDDC900C0852600F5D5
      8200F9D98700FBEBC100FFF0C100FFF4D100FFFBEC00FFF3C300FFF5C900FFF8
      D200FFFBDE00FFFEE500FFFFE700FFFFE700FFFFE700FEFDF100ECC67600E4B2
      4600D8A13500B4731000F0EDEA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000053B400AAEBF600B9FCFD00BBFDFE00BAFCFE00B5FBFE00ACF9FE00A5F7
      FE009BF4FE0094F2FE008CF0FE0084EEFE007BEBFE0074EAFE006BE7FE0063E5
      FE005AE2FE0053E0FE0049DFFE0047DFFE009BF4FE00ADF5FA000053B400FDFD
      FD0000000000000000000000000000000000000000000000000038D5650062FC
      950064FD950061FA92005EF78E005BF48A0059F2850056EF810053EC7D0051EA
      79004EE775004BE4720049E26E0046DF6A0044DD660041DA62003FD85E003CD5
      5B003AD3570038D1530035CE500033CC4C0030C949002EC745002CC542002AC3
      3E0026BD39000E861A000000000000000000000000000000000000000000252C
      9100C7E0FF005587FE005485FD005485FD005384FC005282FC005180FB00517E
      FA00507CF8004F7BF7004E78F7004D77F6004B74F5004972F400476EF300456C
      F2004369F1004167F0003F63EF003D61EE003B5EED00395CEC005274F10095B8
      FD00333FA100BFD0D100000000000000000000000000E0C6A300D5A54C00FBDF
      8F00FBE09700FDF3D500FFECB100FFFBEF00FFF9E500FFEEB500FFF2BF00FFF4
      C700FFF8D200FFFBDB00FFFEE500FFFFE600FFFFE700FFFFEA00F6E2B700E8B9
      5100E3B24800BB7B1500D7CAB800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000286CBE006DAAD900B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FC
      FD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FC
      FD00B9FCFD00B9FCFD00B9FCFD00B9FCFD00B9FCFD0073B0DB001D60B9000000
      00000000000000000000000000000000000000000000000000008FE9A9003DDB
      6C004EE97E004DE77C004AE4780048E1750046DE710044DB6E0042D86A003FD5
      660048DF700051EA79004EE775004BE4720049E26E0046DF6A0044DD660038CC
      57002DBC48002BB9450029B6420027B43F0025B13C0023AF390022AC360020AA
      33001491230078BB7F000000000000000000000000000000000000000000252C
      9100C7E0FF00919FD0008595D0008495D0008495D0008495D0008494D0008494
      D0008494CF008494CF008493CF008393CF008292CF008090CF007D8ECF007A8C
      CF007789CF007588CE007285CE007083CE006D81CE006A7FCE006B7FCE00C7E0
      FF00252C9100BFD0D100000000000000000000000000D9BB9000DFB55F00FDE1
      9300FDE5A300FEF2CD00FFEDB700FFFDF700FFF9E800FFEDB300FFEFB700FFF2
      BF00FFF5C900FFF8D200FFFBDE00FFFEE500FFFFE700FFFFE700F9EBCC00EBBF
      5E00E9BB5400BE801B00CFC0AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FDFDFD00094DA0000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4000053B4000053B4000053
      B4000053B4000053B4000053B4000053B4000053B4000E499400F9F9F9000000
      000000000000000000000000000000000000000000000000000000000000D9F8
      E2009BEBB2009AE9B00099E7AF0098E5AD0097E4AC0096E2AA0096E0A90060D0
      7D0042D86A0056EF810053EC7D0051EA79004EE775004BE4720049E26E0033C4
      520057BB69008DCF99008CCE98008CCC96008BCB95008AC9940089C8920089C7
      9100B4DBB9000000000000000000000000000000000000000000000000003E52
      900036439000252C9100252C9100252C9100252C9100252C9100252C9100252C
      9100252C9100252C9100252C9100252C9100252C9100252C9100252C9100252C
      9100252C9100252C9100252C9100252C9100252C9100252C9100252C9100252C
      910035438F00CADED400000000000000000000000000D9B98D00E2B96500FEE4
      9700FEE7A700FEF2CC00FFEDB900FFFEFC00FFFDF700FFF0C200FFECAF00FFEE
      B500FFF2BF00FFF4C700FFF8D200FFFBDB00FFFEE500FFFFE600FAEFD200EEC7
      6C00ECC15F00BF821F00CFC0AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FBFBFB00896F6300F3DED700FDE9E200E9D2C800896F6300FDFD
      FD0000000000000000000000000000000000000000000000000000000000FEFE
      FE00CBC8C700CAADA000FCDED200FCDDD100CAAB9E00CBC8C700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000097E2
      AB0046DE71005BF48A0059F2850056EF810053EC7D0051EA79004EE7750037C9
      590090D59E000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DDC29C00D9AC5500FEE5
      9900FEE7A500FFF4D300FFEBB000FFFCF300FFFFFF00FFF9E700FFEBAC00FFEC
      AF00FFEFB700FFF2BF00FFF5C900FFF8D200FFFBDE00FFFEE700FAECCA00F0C9
      6C00EDC36400BE811E00CFC0AA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD00896F6300F4E1D800FEEBE400F2DED500896F6300FDFD
      FD00000000000000000000000000000000000000000000000000000000000000
      0000C0BCBA00D0B2A400FEE1D500FDDFD300CBAC9E00CCC9C800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000098E6
      AE004BE4790061FA92005EF78E005BF48A0059F2850056EF810053EC7D003CCF
      600091D8A1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E8D5BB00C6903400FDE3
      9600FFE69B00FFF4D600FFEBB000FFF6DB00FFFFFF00FFFFFF00FFFAEB00FFF1
      C500FFEFB800FFF0BC00FFF4C800FFF6D400FFF8D200FFFCEA00F8E0A500F3CE
      7400E8BD6100B7771500EDE7DF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD00896F6300EFDDD500FEEDE700FDEAE400896F6300C2BE
      BD0000000000000000000000000000000000000000000000000000000000FEFE
      FE00BEA79C00E1C4B800FEE3D800FDE1D600C5A89A00D2CFCE00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009AE9
      B1004EE97E0065FE980064FD950061FA92005EF78E005BF48A0059F2850040D5
      670093DBA4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F6EFE500B97C1C00F2D3
      8300FFE69A00FFEAAD00FFF5D600FFEBB000FFFCF600FFFFFF00FFFFFF00FFFD
      F900FFFAEC00FFFAEC00FFFBEF00FFF7D900FFF6CF00FEF8E700F7D78600F5D2
      7B00DFB25600B97E2900FCFCFC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FDFDFD0096786A00E4D1C900FEEFE900FEEDE700CCB3A800B7A8
      A100FEFEFE000000000000000000000000000000000000000000FEFEFE00D4D0
      CF00B2948500F6DBD000FEE4DA00FDE1D600A2827300E2E0DF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009CED
      B40050ED810066FF990066FF990065FE980064FD950061FA92005EF78E0044DB
      6E0095DEA7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C28F4800D6A8
      5100FFE69A00FFE69A00FFF2CB00FFF2CA00FFEBAD00FFF4D600FFFBEE00FFFC
      F600FFFCF200FFF9E800FFF1C300FFF0BD00FEF7DF00FBE5AA00F9D98500F6D6
      8100C0862700C3A8830000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000A7918600C8B0A400FDF0EA00FEF0EA00F8E8E200C7AE
      A200B1A29A00D6D4D300F1F1F100FEFEFE00F6F5F500E4E2E200B5ACA800AF93
      8400F4DCD300FEE7DE00FEE6DD00EAD0C50096786A00DEDCDC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009EF0
      B80051F0850066FF990066FF990066FF990066FF990065FE980064FD950049E1
      750096E2AA000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CBBFAE00B77A
      1F00F5D78900FFE69A00FFE7A100FFF2CB00FFF5D600FFEAAE00FFEAAB00FFEC
      B300FFECB100FFEAAA00FFF0C100FFF8E200FDE9B200FBDF9000FADC8B00E2B8
      6000BA802E00E0DDD80000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C1B7B300A8877900F5E9E300FEF1EC00FEF1EC00F9EB
      E500D6C3B900BAA09500A893890097877F00B29A8D00B59B8F00CDB5AA00F0DB
      D200FEE9E100FEE8E000F9E3DA00BD9E9100A28F840000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009EF2
      B90053F3870066FF990066FF990066FF990066FF990066FF990066FF99004DE7
      7C0098E5AD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FCFCFC00CCAD
      8300BF852700F5D68700FFE69A00FFE69A00FFEAAD00FFF5D600FFF4D300FFF3
      CD00FFF3CF00FFF5D900FEF1CB00FEE7A400FDE29400FCE19200E5BD6800B87A
      1D00D5C8B5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009B827800B79D8F00F3E7E200FEF3EE00FEF2
      ED00FEF1EC00FDEFEA00FCEEE800FCEDE600FCECE600FDECE600FEECE500FEEC
      E500FEEBE400FDE9E200CCB1A400977A6D00DEDAD70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009EF2
      B90053F3870066FF990066FF990066FF990066FF990066FF990066FF99004EEA
      7F009AE9B0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000F3F2
      F100C2904900BF852700F5D78900FFE69A00FFE69A00FFE69B00FFE9A600FFEA
      AB00FFEAAA00FFE8A300FEE59900FEE59800FEE49700EBC77400B87A1E00D4B1
      8000FCFCFC000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D3CFCE00A5877800B89F9200F8EEEA00FDF2
      EE00FEF3EE00FEF2ED00FEF1EC00FFF1EC00FEF0EA00FEEFE900FEEEE800FEED
      E600FBE9E200D3BBAF00A9897900A89892000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C2F7
      D30044E9790064FD970066FF990066FF990066FF990066FF990063FD970040E2
      7200D9F8E2000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F3F2F100CCAD8300B77A1F00D6A85100F2D38300FDE39600FFE69A00FFE6
      9A00FFE69A00FFE69A00FCE19400EDCB7B00CA943A00B77D2700CDBDA700FCFC
      FC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C6B5AD00A1887C00B3978A00D0BC
      B300EBDDD700F6EAE500FDF2ED00FDF1ED00FDF1EC00F9ECE600EEDDD600DFCA
      C100B89B8E00A3897C00B59E9200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000093F0B10041E8760049EC7D0049EC7D0049EC7D0049EC7D0041E7760092EF
      AF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FCFCFC00CBBFAE00C28F4800B97C1C00C6903400D9AC5500E3BA
      6600E0B76200D6A85100C38A2E00B7791B00CDA36900DCD3C800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A2897E00AD96
      8C00B89D9100B79D8F00B89E9100B89E9100B89D9000B5998B00B89C8F00B99D
      90009F877C00B3A69F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F6EFE500E8D5BB00DDC29C00D9B9
      8D00D9BB9000E0C6A300ECDDC900F6F5F3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D8D0
      CD00C4B0A700BBA39700B89E9100B89E9100B89D9000B5998B00C2ACA200D3C5
      BF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000C00000000100010000000000000C00000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFC7FFFFFFFFFFFFFFFFC003C001FFFFFF03FFFC7FFFFFFFFFFF
      C003C001FFFFFF01FFFC7FFFFFFFFFFFC003C001FFFFFC01FFF07FFFFFFFFFFF
      C003C001FFFFFC01FFF07FFFFFFFFFFFC003C001FFFFF003FFE07FFFFFFFFFFF
      C003C001FFFFF003FFC0000FFFFFFFFFC003C001FFFFC00FFF80000FFFFFFFFF
      C003C001FFFFC00FFF00000FFFFFFFFFC003C001FFFF003FFE00000FFFFFFFFF
      C003C001FFFF003FFE00000FFFFFFFFFC003C001FC7C00FFFC00000FFFFFFFFF
      FC7FFF3FFC3C00FFF800000FFFFFFFFFFC00003FFC3003FFF000000FFFFFFFFF
      FC00003FFC1003FFF000000FFFFFFFFFFFFE3FFFFC000FFFF000000FFFFFFFFF
      FFFE3FFFFC000FFFF000000FFFFFFFFFFFFE3FFFC0003FFFF800000FFFFFFFFF
      FF8000FF00003FFFFC00000FFFFFFFFFFF8000FF00007FFFFE00000FFFFFFFFF
      FF8000FF00003FFFFE00000FFFFFFFFFFF8000FFC0000FFFFF00000FFFFFFFFF
      FF8000FFF0000FFFFF80000FFFFFFFFFFF8000FFF0000FFFFFC0000FFFFFFFFF
      FF8000FFF0003FFFFFE07FFFFFFFFFFFFF8000FFF001FFFFFFF07FFFFFFFFFFF
      FF8000FFE001FFFFFFF07FFFFFFFFFFFFF8000FFE0C3FFFFFFFC7FFFFFFFFFFF
      FF8000FFE3C3FFFFFFFC7FFFFFFFFFFFFFFFFFFFFFE3FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFF3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFF3FF8FFFFFFFFFFFFFFFFFFFFFFFC3FF807E01F
      FF3FFC7FFFFFFFFFFFFFF83FF003C00FFC0FF83FFFFFFFFFFFFFF00FC0000003
      FC0FF01FFFF1FFFFFFFFE00FC0000003F003E00FFFE0FFFFFFFFC003C0000003
      F003C007FFC07FFFFFFFC003C0000003F0008007FF803FFFFFFF0003C0000003
      F0000007FF001FFFFFFF0003C0000003F800000FFE000FFFFFFE0007C0000003
      FC00001FFC0007FFFFFC000FC0000003FE00003FF80003FFFFF8001FC0000003
      FF00007FF00001FFFFF0003FC0000003FF8000FFE00000FFF80000FFC0000003
      FFC001FFC000003FF00000FFE0000007FF8000FF8000001FE00003FFF800001F
      FF0000FF8000000FC00007FFFC00003FFE00003F800E0007C0000FFFFF0000FF
      FC00003FC00F0003C0001FFFFF0000FFF800000FE03F8001C0003FFFFF0000FF
      F000000FF03FC001C0003FFFFF0000FFF0008007F8FFE001C3803FFFFF0000FF
      F001C007FCFFF001C3C03FFFFF0000FFF003E00FFFFFF803CFC03FFFFF0000FF
      F807F00FFFFFFC07CFC03FFFFF0000FFFC0FF83FFFFFFE0FFF003FFFFF0000FF
      FE1FFC3FFFFFFF1FFF007FFFFFC003FFFFFFFFFFFFFFFFFFFC00FFFFFFF00FFF
      FFFFFFFFFFFFFFFFFC01FFFFFFF81FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFE00007FFFFFFFFFF000000FF000000FFE00007F
      FF3FFC7FF000000FF000000FFE00007FFC0FF83FF000000FF000000FFE00007F
      FC0FF01FF000000FF000000FC0000003F003E00FF000000FF000000FC0000003
      F003C0070000000FF0000007C0000003F00080070000000FF0000001C0000003
      F00000070000000FF0000000C0000003F800000F0000000FF0000000C0000003
      FC00001F0000000FF0000000C0000003FE00003F0000000FF0000000C0000003
      FF00007F0000000FF0000001C0000003FF8000FF0000000FF0000007C0000003
      FFC001FF0000000FF000000FC0000003FF8000FF0000000FF000000FC0000003
      FF0000FF0000000FF000000FC0000003FE00003F0000000FF000000FC0000003
      FC00003FF000000FF000000FC0000003F800000FF000000FF000000FC0000003
      F000000FF000000FF000000FC0000003F0008007F000000FF000000FFE00007F
      F001C007F000000FF000000FFE00007FF003E00FF000003FF000003FFE00007F
      F807F00FF000003FF000003FFE00007FFC0FF83FF00000FFF00000FFFE00007F
      FE1FFC3FF00000FFF00000FFFE00007FFFFFFFFFF00003FFF00003FFFE00007F
      FFFFFFFFF00003FFF00003FFFE00007FFFFFFFFFF0000FFFF0000FFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FFFF
      FFFFFFFFF0000007FFC003FFFFF1FFFFF000001FF0000007FF0000FFFFC1FFFF
      F000001FF0000007FE00007FFF01FFFFF000000FF0000007F800001FFC01FFFF
      F000000FF0000007F000000FF8001FFFF000000FF0000007F000000FE00001FF
      F000000FF0000007E0000007C00000FFF000000FF0000007C0000003C000003F
      F000000FF0000007C0000003E000003FF000000FF000000780000001F801001F
      F000000FF000000780000001FC01F00FF000000FF000000780000001FF01FC0F
      F000000FF000000780000001FFC1FF0FF000000FF000000780000001FFF1FF8F
      F000000FF000000780000001F7F1BFCFF000000FF000000780000001F1FF8FFF
      F000000FF000000780000001F0FF83FFF000001FF000000780000001F07F80FF
      F000001FF000000780000001F01F807FF80FFFFFF000000780000001F000801F
      F80FFFFFF000000780000001F800000FF80FFFFFF0000007C0000003FC000003
      F81FF07FF0000007C0000003FE000003F807C03FF0000007E0000007FF800007
      FC00003FF0000007F000000FFFE0000FFC00007FFE0FFC3FF000000FFFFF803F
      FE00007FFE0FFC3FF800001FFFFF80FFFE0000FFFE0FFC3FFE00007FFFFF83FF
      FF0001FFFE0FFC3FFF0000FFFFFF83FFFFC003FFFE1FFC3FFFC003FFFFFF8FFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFF0FFFFFFFFFFFF00FFFFFFFFFFFFFFFFE03F000001F
      FFE007FFFFFFFFFFFFFFFC03F000001FFFE007FFFFFFFFFFFFFFF801F000000F
      FFE007FFFFFFFFFFFFFFF001F000000FFFE007FFFFFFFFFFFFFFE001F000000F
      FFE007FFFFFFFFFFFFFFC001F000000FFFE007FFFFFFFFFFFFFF8003F000000F
      FFE007FFFFFFFFFFFF810007F000000FFFE007FFE0000003FC00000FF000000F
      E0000007E0000003F000001FF000000FC0000003E0000003E000003FF000000F
      C0000003E0000003E000007FF000000FC0000003E0000003C00000FFF000000F
      C0000003E0000003C00001FFF000000FC0000003E0000003800003FFF000000F
      C0000003E0000003800001FFF000000FC0000003E0000003800001FFF000001F
      C0000003E0000003800001FFF000001FE0000007E0000003800001FFF80FE03F
      FFE007FFFFFFFFFF800001FFF80FF03FFFE007FFFFFFFFFF800001FFF80FE03F
      FFE007FFFFFFFFFF800001FFF807C03FFFE007FFFFFFFFFFC00003FFFC00003F
      FFE007FFFFFFFFFFC00003FFFC00007FFFE007FFFFFFFFFFC00007FFFE00007F
      FFE007FFFFFFFFFFE00007FFFE0000FFFFE007FFFFFFFFFFF0000FFFFF0001FF
      FFF00FFFFFFFFFFFF8003FFFFFC003FFFFFFFFFFFFFFFFFFFF00FFFFFFE00FFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
  end
  object Actions: TActionList
    Left = 8
    Top = 80
    object acAdd: TAction
      Category = 'Basic'
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1082#1072#1088#1090#1091
      Hint = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1085#1086#1074#1086#1081' '#13#10#1090#1077#1093'. '#1082#1072#1088#1090#1099
      OnExecute = acAddExecute
    end
    object acDel: TAction
      Category = 'Basic'
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1082#1072#1088#1090#1091
      Hint = #1041#1077#1079#1074#1086#1079#1074#1088#1072#1090#1085#1086#1077' '#1091#1076#1072#1083#1077#1085#1080#1077' '#1090#1077#1093'. '#1082#1072#1088#1090#1099#13#10#1074#1084#1077#1089#1090#1077' '#1089' '#1089#1086#1076#1077#1088#1078#1080#1084#1099#1084
      OnExecute = acDelExecute
      OnUpdate = acDelUpdate
    end
    object acCustomize: TAction
      Category = 'Basic'
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1082#1072#1088#1090#1091
      Hint = 
        #1042#1085#1077#1089#1090#1080' '#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1074' '#1082#1072#1088#1090#1091':'#13#10#1076#1086#1073#1072#1074#1080#1090#1100' ('#1091#1076#1072#1083#1080#1090#1100') '#1072#1088#1090#1080#1082#1091#1083#1099' ('#1086#1087#1077#1088#1072#1094#1080#1080 +
        ')'
      OnExecute = acCustomizeExecute
      OnUpdate = acCustomizeUpdate
    end
    object acExit: TAction
      Category = 'Basic'
      Caption = #1042#1099#1093#1086#1076
      Hint = #1042#1099#1093#1086#1076
      OnExecute = acExitExecute
    end
    object acOperationsCopy: TAction
      Category = 'Basic'
      Caption = 'acOperationsCopy'
      Hint = #1057#1082#1086#1087#1080#1088#1086#1074#1072#1090#1100' '#1090#1077#1093'. '#1082#1072#1088#1090#1091#13#10#1074#1084#1077#1089#1090#1077' '#1089' '#1086#1087#1077#1088#1072#1094#1080#1103#1084#1080
      OnExecute = acOperationsCopyExecute
    end
    object acOperationsPaste: TAction
      Category = 'Basic'
      Caption = 'acOperationsPaste'
      Hint = #1042#1089#1090#1072#1074#1082#1072' '#1089#1082#1086#1087#1080#1088#1086#1074#1072#1085#1085#1086#1081' '#1086#1087#1077#1088#1072#1094#1080#1080
      OnExecute = acOperationsPasteExecute
    end
    object acPrintTechnologyCard: TAction
      Category = 'Print'
      Caption = #1058#1077#1093'. '#1082#1072#1088#1090#1072
      Hint = #1055#1077#1095#1072#1090#1100' '#1090#1077#1093'.'#1082#1072#1088#1090#1099
      OnExecute = acPrintTechnologyCardExecute
    end
    object acPrintCardsArticles: TAction
      Category = 'Print'
      Caption = #1040#1088#1090#1080#1082#1091#1083#1099' '#1074' '#1082#1072#1088#1090#1072#1093
      Hint = #1055#1077#1095#1072#1090#1100' '#1072#1088#1090#1080#1082#1091#1083#1086#1074', '#1089#1086#1076#1077#1088#1078#1072#1097#1080#1093#1089#1103' '#13#10#1074' '#1090#1077#1093'. '#1082#1072#1088#1090#1072#1093
      OnExecute = acPrintCardsArticlesExecute
    end
    object acPrintCardsWithOperation: TAction
      Category = 'Print'
      Caption = #1050#1072#1088#1090#1099', '#1089#1086#1076#1077#1088#1078#1072#1097#1080#1077' '#1086#1087#1077#1088#1072#1094#1080#1102
      Hint = #1055#1077#1095#1072#1090#1100' '#1090#1077#1093'. '#1082#1072#1088#1090', '#1089#1086#1076#1077#1088#1078#1072#1097#1080#1093' '#13#10#1086#1087#1088#1077#1076#1077#1083#1105#1085#1085#1091#1102' '#1086#1087#1077#1088#1072#1094#1080#1102
      OnExecute = acPrintCardsWithOperationExecute
    end
    object acPrintForeignMaterialReport: TAction
      Category = 'Print'
      Caption = #1055#1086#1083#1085#1099#1081' '#1086#1090#1095#1105#1090
      Hint = #1054#1090#1095#1105#1090' '#1086#1073' '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1080' '#1089#1090#1086#1088#1086#1085#1085#1080#1093' '#13#10#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074' '#1074' '#1090#1077#1093'.'#1082#1072#1088#1090#1072#1093
      OnExecute = acPrintForeignMaterialReportExecute
    end
    object acPrintForeignMaterialSemisReport: TAction
      Category = 'Print'
      Caption = #1055#1086' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1091
      Hint = #1054#1090#1095#1105#1090' '#1086#1073' '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1080' '#1082#1086#1085#1082#1088#1077#1090#1085#1086#1075#1086' '#13#10#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1072' '#1074' '#1090#1077#1093'. '#1082#1072#1088#1090#1072#1093
      OnExecute = acPrintForeignMaterialSemisReportExecute
    end
    object acCalcCreationCosts: TAction
      Category = 'Calculations'
      Caption = #1055#1077#1088#1077#1088#1072#1089#1095#1105#1090
      Hint = #1047#1072#1087#1091#1089#1082' '#1087#1088#1086#1094#1077#1076#1091#1088#1099' '#1087#1086#1083#1085#1086#1075#1086' '#13#10#1087#1077#1088#1077#1089#1095#1105#1090#1072' '#1089#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1080' '#1080#1079#1076#1077#1083#1080#1081
      OnExecute = acCalcCreationCostsExecute
      OnUpdate = acCalcCreationCostsUpdate
    end
    object acPrintGroups: TAction
      Category = 'Print'
      Caption = #1054#1090#1095#1105#1090' '#1086' '#1089#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1080
      Hint = #1055#1077#1095#1072#1090#1100' '#1089#1077#1073#1077#1089#1090#1086#1080#1084#1086#1089#1090#1080' '#1090#1077#1093'. '#1082#1072#1088#1090
      OnExecute = acPrintGroupsExecute
    end
    object acPrintSemisExpense: TAction
      Category = 'Print'
      Caption = #1047#1072#1090#1088#1072#1090#1099' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
      Hint = #1054#1090#1095#1105#1090' '#1086' '#1088#1072#1089#1093#1086#1076#1077' '#1089#1090#1086#1088#1086#1085#1085#1080#1093' '#13#10#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074' '#1089#1086#1075#1083#1072#1089#1085#1086' '#1080#1093' '#1079#1072#1082#1091#1087#1082#1072#1084
      OnExecute = acPrintSemisExpenseExecute
    end
    object acChangePrice: TAction
      Category = 'Calculations'
      Hint = #1055#1077#1088#1077#1089#1095#1080#1090#1072#1090#1100' '#1086#1090#1085#1086#1089#1080#1090#1077#1083#1100#1085#1086' '#13#10#1094#1077#1085#1099' '#1085#1072' '#1079#1086#1083#1086#1090#1086
      OnExecute = acChangePriceExecute
      OnUpdate = acChangePriceUpdate
    end
    object acPrintOperationFrequency: TAction
      Category = 'Print'
      Caption = #1063#1072#1089#1090#1086#1090#1072' '#1080#1089#1087#1086#1083#1100#1079#1086#1074#1072#1085#1080#1103' '#1086#1087#1077#1088#1072#1094#1080#1080
      OnExecute = acPrintOperationFrequencyExecute
    end
    object acPrintPricereport: TAction
      Category = 'Print'
      Caption = #1055#1088#1072#1081#1089'-'#1083#1080#1089#1090
      OnExecute = acPrintPricereportExecute
    end
    object acPrintMissingArticles: TAction
      Category = 'Print'
      Caption = #1053#1077#1076#1086#1089#1090#1072#1102#1097#1080#1077' '#1072#1088#1090#1080#1082#1091#1083#1099
      OnExecute = acPrintMissingArticlesExecute
    end
    object acCalcAdditionalLosses: TAction
      Category = 'Calculations'
      Caption = #1054#1089#1085#1086#1074#1085#1099#1077'/'#1076#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
      OnExecute = acCalcAdditionalLossesExecute
    end
  end
  object Groups: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Technology$Groups '
      'set '
      '  Group$Name = :Name,'
      '  Price$Position = :Price$Position,'
      '  Group$Material$id = :Group$Material$id'
      'where Group$ID = :OLD_ID')
    DeleteSQL.Strings = (
      'delete from Technology$Groups where Group$ID = :ID')
    InsertSQL.Strings = (
      
        'insert into Technology$Groups(Group$ID, Group$Name, Price$Positi' +
        'on, group$Material$ID)'
      'values(:ID, :Name, :Price$Position, :group$material$id)')
    SelectSQL.Strings = (
      'select '
      '  ID, '
      '  Name,'
      '  Average$Weight,'
      '  Works$Cost,'
      '  Losses$Percent,'
      '  Waste$Percent,'
      '  Weight$Part,'
      '  Quantity$Part,'
      '  FM$Cost,'
      '  Overnorm$Losses$Cost,'
      '  Self$Cost,'
      '  Summary$Weight$Percent,'
      '  FM$Clear$Cost,'
      '  Price$Position,'
      '  Group$Material$ID'
      'from '
      '  Technology$Groups_S '
      'order by '
      '  Name')
    AfterPost = AfterPost
    OnNewRecord = GroupsNewRecord
    Transaction = Transaction
    Database = dm.db
    Left = 8
    Top = 632
    object GroupsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object GroupsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 80
      EmptyStrToNull = True
    end
    object GroupsAVERAGEWEIGHT: TFIBBCDField
      FieldName = 'AVERAGE$WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object GroupsWORKSCOST: TFIBBCDField
      FieldName = 'WORKS$COST'
      Size = 2
      RoundByScale = True
    end
    object GroupsLOSSESPERCENT: TFIBBCDField
      FieldName = 'LOSSES$PERCENT'
      Size = 3
      RoundByScale = True
    end
    object GroupsWASTEPERCENT: TFIBBCDField
      FieldName = 'WASTE$PERCENT'
      Size = 3
      RoundByScale = True
    end
    object GroupsWEIGHTPART: TFIBBCDField
      FieldName = 'WEIGHT$PART'
      Size = 3
      RoundByScale = True
    end
    object GroupsQUANTITYPART: TFIBBCDField
      FieldName = 'QUANTITY$PART'
      Size = 3
      RoundByScale = True
    end
    object GroupsFMCOST: TFIBBCDField
      FieldName = 'FM$COST'
      Size = 2
      RoundByScale = True
    end
    object GroupsOVERNORMLOSSESCOST: TFIBBCDField
      FieldName = 'OVERNORM$LOSSES$COST'
      Size = 2
      RoundByScale = True
    end
    object GroupsSELFCOST: TFIBBCDField
      FieldName = 'SELF$COST'
      Size = 2
      RoundByScale = True
    end
    object GroupsSUMMARYWEIGHTPERCENT: TFIBBCDField
      FieldName = 'SUMMARY$WEIGHT$PERCENT'
      Size = 4
      RoundByScale = True
    end
    object GroupsFMCLEARCOST: TFIBBCDField
      FieldName = 'FM$CLEAR$COST'
      Size = 2
      RoundByScale = True
    end
    object GroupsPRICEPOSITION: TFIBSmallIntField
      FieldName = 'PRICE$POSITION'
    end
    object GroupsGROUPMATERIALID: TFIBStringField
      FieldName = 'GROUP$MATERIAL$ID'
      LookupDataSet = dm.taMat
      LookupKeyFields = 'ID'
      LookupResultField = 'NAME'
      KeyFields = 'GROUP$MATERIAL$ID'
      Size = 10
      EmptyStrToNull = True
    end
  end
  object GroupArticles: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE TECHNOLOGY$GROUP$ARTICLES'
      'SET '
      '    GROUP$ID = :GROUP$ID,'
      '    ARTICLE$ID = :ARTICLE$ID'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    TECHNOLOGY$GROUP$ARTICLES'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO TECHNOLOGY$GROUP$ARTICLES('
      '    ID,'
      '    GROUP$ID,'
      '    ARTICLE$ID,'
      '    ARTICLE$SUMMARY$WEIGHT'
      ')'
      'VALUES('
      '    :ID,'
      '    :GROUPID,'
      '    :ARTICLE$ID,'
      '    0 '
      ')')
    RefreshSQL.Strings = (
      'Select '
      '  ID, '
      '  Group$ID,'
      '  Article$ID'
      'from'
      '  Technology$Group$Articles'
      ' WHERE '
      '        TECHNOLOGY$GROUP$ARTICLES.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'Select '
      '  ga.ID, '
      '  ga.Group$ID GroupID,'
      '  ga.Article$ID,'
      '  a.Art Article$Name,'
      '  g.Group$Name'
      'from'
      '  Technology$Group$Articles ga'
      '  left outer join D_Art a on (ga.Article$ID = a.D_ArtID)'
      
        '  left outer join Technology$Groups g on (ga.Group$ID = g.Group$' +
        'ID)'
      'order by '
      '  ga.Group$ID'
      '')
    FilterOptions = [foCaseInsensitive]
    AfterPost = AfterPost
    OnNewRecord = GroupArticlesNewRecord
    Transaction = Transaction
    Database = dm.db
    Left = 8
    Top = 600
    dcForceOpen = True
    object GroupArticlesID: TFIBIntegerField
      FieldName = 'ID'
    end
    object GroupArticlesARTICLEID: TFIBIntegerField
      FieldName = 'ARTICLE$ID'
    end
    object GroupArticlesARTICLENAME: TFIBStringField
      FieldName = 'ARTICLE$NAME'
      EmptyStrToNull = True
    end
    object GroupArticlesGROUPID2: TFIBIntegerField
      FieldName = 'GROUPID'
    end
    object GroupArticlesGROUPNAME: TFIBStringField
      FieldName = 'GROUP$NAME'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object Operations: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  ID,'
      '  Operation'
      'from '
      '  D_Oper')
    Transaction = Transaction
    Database = dm.db
    Left = 8
    Top = 504
    object OperationsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object OperationsOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object Articles: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  D_ArtID,'
      '  Art'
      'from '
      '  D_Art'
      'where '
      '  Group$ID is null')
    Transaction = Transaction
    Database = dm.db
    Left = 8
    Top = 472
    object ArticlesD_ARTID: TFIBIntegerField
      FieldName = 'D_ARTID'
    end
    object ArticlesART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
  end
  object SourceArticles: TDataSource
    DataSet = Articles
    Left = 40
    Top = 472
  end
  object SourceOperations: TDataSource
    DataSet = Operations
    Left = 40
    Top = 504
  end
  object SourceGroupOperations: TDataSource
    DataSet = GroupOperations
    Left = 40
    Top = 568
  end
  object SourceGroupArticles: TDataSource
    DataSet = GroupArticles
    Left = 40
    Top = 600
  end
  object SourceGroups: TDataSource
    DataSet = Groups
    Left = 40
    Top = 632
  end
  object Transaction: TpFIBTransaction
    DefaultDatabase = dm.db
    TimeoutAction = TARollback
    Left = 8
    Top = 664
  end
  object gridGroupsPopupMenu: TPopupMenu
    Images = SmallImages
    Left = 8
    Top = 176
    object N1: TMenuItem
      Action = acOperationsCopy
      Caption = #1050#1086#1087#1080#1088#1086#1074#1072#1090#1100
      ImageIndex = 2
      ShortCut = 16451
    end
    object N2: TMenuItem
      Action = acOperationsPaste
      Caption = #1042#1089#1090#1072#1074#1080#1090#1100
      ImageIndex = 3
      ShortCut = 16470
    end
  end
  object SmallImages: TImageList
    Left = 40
    Top = 144
    Bitmap = {
      494C010110001300040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000005000000001002000000000000050
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000601FC4000070C50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000601F
      C400006CC5003CC9FD00155DBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000601FC4000068
      C5002DBEFD00155DBD009F13DC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EA0B
      EA00943494007F3F7F00868484007F3F7F00A92AA9006021C0000063C5001DB3
      FD00155DBD009F13DC0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BF20BF008684
      8400B6B5B500EBE7E700E5E1E100D6D2D200F7F4F400868484001998DC00155D
      BD009F13DC000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DF10DF0086848400ECEB
      EB00DABC9500B8711600C2863200B8762300CDB19300F7F4F400868484008F18
      D100000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008A3A8A00F7F4F400DEBF
      9A00B8680000FDE8A300FFF8B900F8DE9500B8680000C8AC8D00F7F4F400A92A
      A900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000086848400FDFCFC00BC72
      1200FFFFDE00FFECA600FFF1AD00FFEDA600F8D07B00B5721E00F7F4F4008684
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000086848400FDFCFC00BE6B
      0000FFFFDE00FFEAAF00FFECB300FFEAB000FFDF9200B26A0F00F7F4F4008684
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000086848400FDFCFC00B76A
      0700FFFFDE00FFEDC200FFEDC400FFEDC200F8D29300B5721E00F7F4F4008684
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000086848400F7F4F400DEBF
      9A00C5832D00FFFFDE00FFFFDE00FFFFDE00C5832D00CAB59E00F7F4F4009434
      9400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C91AC90088888800F7F4
      F400DABC9500C2863800CC995900BE813400CDB19300F7F4F40086848400EA0B
      EA00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000943494008786
      8600F7F4F400F7F4F400F7F4F400F7F4F400F7F4F40086848400BF20BF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C91A
      C9008684840086848400868484008684840086848400D415D400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F11CF700000000000000000000000000000000000000000000000000F01D
      F500FD03FE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F518F600F90CFA000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A300
      BE0031319100C000C90000000000000000000000000000000000C000C9003131
      9100C000C9000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D806DA001B8F410012803600EE01EE0000000000000000000000
      00000000000000000000000000000000000000000000000000009E00BB003131
      91003D63F00031319100C000C9000000000000000000C000C900313191003058
      F00031319100C000C90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CE08D00020924400CEF7DC00CEF7DC0012803600EE01EE00000000000000
      0000000000000000000000000000000000000000000000000000313191006688
      EE001235E6001D41E80031319100C000C900C000C900313191002F55ED001235
      E6003F68F30031319100F21BF600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CE08
      D00027954A00CEF7DC0038EB7E0038EB7E00CEF7DC0012803600EE01EE000000
      0000000000000000000000000000000000000000000000000000EE00EF003131
      91005D7DEB001336E6001E42E80031319100313191003559ED001235E6003C62
      F00031319100C000C90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CE08D0002B95
      4C00CEF7DC0045ED870053EE8F0043EC850038EB7E00CEF7DC0013803600EE01
      EE0000000000000000000000000000000000000000000000000000000000EE00
      EF00313191006281EB001336E6001F42E8003154EC001235E6004165EF003131
      9100C000C9000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CE08D00030955000CEF7
      DC0068F39D007CF4AA0093D8AA009DE9B7004AED890038EB7E00CEF7DC001380
      3700E803E9000000000000000000000000000000000000000000000000000000
      0000EE00EF00313191006585F1001337E7001235E600375AEC0031319100C000
      C900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F022F20005802C00CEF7DC008BF8
      B40096F8BB009CD5AF00096F2D000E7A3200ACEAC10054EE90003BEB8000CEF7
      DC0016873B00E504E60000000000000000000000000000000000000000000000
      0000C000C900313191004D72F200163EEB00153BE9002347EA0031319100C000
      C900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FD04FD000F7A3300D0ECD900BFFC
      D600A4D4B4000A6F2D0000000000FA00FA000E7A3200B5EAC7005DEF96003FEC
      8200CEF7DC001A8A3E00E504E60000000000000000000000000000000000C000
      C900313191005E85F6001A48F0004E74F3007393F3001A42EC00254CEC003131
      9100C000C9000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FA00FA000F7A330084BE
      9700096F2C00000000000000000000000000FA00FA000D793100B2E4C30066F1
      9C0046ED8700CEF7DC001B8A3F00F713F8000000000000000000C000C9003131
      9100769CFA001F52F5005780F700313191003131910083A0F0001C47EE002850
      EE0031319100C000C90000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FA00FA000C6F
      2E000000000000000000000000000000000000000000FA00FA000C783100BAE4
      C80081F4AD00CEF7DC001C8A4000F713F800000000000000000031319100C1D8
      F8003066FA005D88FA0031319100C000C900EE00EF00313191008BA7F000224E
      F0007295F70031319100EF21F600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FE00FE000C78
      3100C2E0CC001F8A4100E504E600000000000000000000000000E000E5003131
      9100BAD4FB0031319100C000C9000000000000000000EE00EF0031319100B1C9
      F300313191009E00BB0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FE00
      FE001F844200EE0DEF000000000000000000000000000000000000000000DA00
      E00031319100C000C90000000000000000000000000000000000EE00EF003131
      91009E00BB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000093939300939393009393930093939300939393009393
      9300939393009393930093939300000000000000000000000000000000000000
      0000000000000000000093939300939393009393930093939300939393009393
      9300939393009393930093939300000000000000000000000000000000000000
      0000000000002D7E4C003C955B003B955B003B955A003B955A0018843C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000093939300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0093939300000000000000000000000000000000000000
      0000000000000000000093939300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0093939300000000000000000000000000000000000000
      00000000000018723800E3FDED00E3FDED00E3FDED00E3FDED00187238000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000096909600FFFFFF00EAEAEA00E9E9E900E9E9E900E8E8
      E800E7E7E700FFFFFF0093939300000000000000000000000000000000000000
      0000000000000000000093939300FFFFFF00EAEAEA00E9E9E900E9E9E900E8E8
      E800E7E7E700FFFFFF0093939300000000000000000000000000000000000000
      00000000000018723900E3FDED0038EB7E0038EB7E00E3FDED00187238000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000096909600FFFFFF00EEEEEE00EDEDED00ECECEC00EBEB
      EB00EBEBEB00FFFFFF009393930000000000000000003E6AB700275FB000275F
      B000275FB0003C6FB80093939300FFFFFF00EEEEEE00EDEDED00ECECEC00EBEB
      EB00EBEBEB00FFFFFF0093939300000000000000000000000000000000000000
      00000000000019723900E3FDED0038EB7E0038EB7E00E3FDED00187238000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000096909600FFFFFF00F1F1F100F1F1F100F0F0F000EFEF
      EF00EEEEEE00FFFFFF00939393000000000000000000275FB000C5FFFF00C5FF
      FF00C5FFFF00C5FFFF0093939300FFFFFF00F1F1F100F1F1F100F0F0F000EFEF
      EF00EEEEEE00FFFFFF0093939300000000000000000018843C001A723A001972
      3A0019723A000E843400E3FDED0038EB7E0038EB7E00E3FDED001C813D001872
      3900187238001872380018843C000000000000000000A481CE001F258C001F25
      8C001F258C001F258C001F258C001F258C001F258C001F258C001F258C001F25
      8C001F258C001F258C00A375CF00000000000000000093939300939393009393
      9300939393009393930096909600FFFFFF00F5F5F500F4F4F400F3F3F300F3F3
      F300F2F2F200FFFFFF00939393000000000000000000275FB000C5FFFF0056BA
      FF0051B8FF004DB5FF0093939300FFFFFF00F5F5F500F4F4F400F3F3F300F3F3
      F300F2F2F200FFFFFF0093939300000000000000000040955E00E3FDED00E3FD
      ED00E3FDED00E3FDED005EEF960038EB7E0038EB7E00E3FDED00E3FDED00E3FD
      ED00E3FDED00E3FDED003C955C0000000000000000001F258C0085A9FC0085A9
      FC0085A9FC0085A9FC0085A9FC0085A9FC0085A9FC0085A9FC0085A9FC0085A9
      FC0085A9FC0085A9FC001F258C00000000000000000096909600FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0096909600FFFFFF00F8F8F800F8F8F800F7F7F700F6F6
      F600F6F6F600F8F8F800939393000000000000000000275FB000C5FFFF0064C3
      FF005FC0FF005ABDFF0096909600FFFFFF00F8F8F800F8F8F800F7F7F700F6F6
      F600F6F6F600FFFFFF0093939300000000000000000040955E00E3FDED0038EB
      7E0038EB7E0038EB7E0038EB7E0038EB7E0038EB7E0038EB7E0038EB7E0038EB
      7E0038EB7E00E3FDED003D955C0000000000000000001F258C0085A9FC001F51
      F4001C4CF2001A47EF001842ED00163DEA001339E8001236E6001235E6001235
      E6001235E60085A9FC001F258C00000000000000000096909600FFFFFF00EBEB
      EB00EBEBEB00EAEAEA0096909600FFFFFF00FCFCFC00FBFBFB00FBFBFB009393
      93009393930093939300939393000000000000000000275FB000C5FFFF0072CB
      FF006DC8FF0068C5FF0096909600FFFFFF00FCFCFC00FBFBFB00FBFBFB009393
      9300939393009393930093939300000000000000000040955E00E3FDED0038EB
      7E0038EB7E0038EB7E0038EB7E0038EB7E0038EB7E0038EB7E0038EB7E0038EB
      7E0038EB7E00E3FDED003D955D0000000000000000001F258C0085A9FC00245B
      FA002157F7001F52F5001D4DF2001B48F0001843ED00163EEB001439E8001236
      E6001235E60085A9FC001F258C00000000000000000096909600FFFFFF00EFEF
      EF00EEEEEE00EDEDED0096909600FFFFFF00FFFFFF00FEFEFE00FEFEFE009393
      9300E7E7E70093939300FF01FF000000000000000000275FB000C5FFFF007FD3
      FF007BD0FF0076CDFF0096909600FFFFFF00FFFFFF00FEFEFE00FEFEFE009393
      9300CECECE0093939300FF01FF00000000000000000040955E00E3FDED00E3FD
      ED00E3FDED00E3FDED00E3FDED0038EB7E0038EB7E00E3FDED00E3FDED00E3FD
      ED00E3FDED00E3FDED003E955D0000000000000000001F258C0085A9FC006C8E
      E7006C8CE6006B8AE5006A88E4006885E3006582E200617DE0005C78DF005873
      DE00536EDD0085A9FC001F258C00000000000000000096909600FFFFFF00F2F2
      F200F2F2F200F1F1F10096909600FFFFFF00FFFFFF00FFFFFF00FFFFFF009393
      930093939300FF01FF00000000000000000000000000275FB000C5FFFF008DDB
      FF0089D8FF0084D5FF0096909600FFFFFF00FFFFFF00FFFFFF00FFFFFF00A3A3
      A30093939300FF01FF000000000000000000000000002D7E4C001A723A001A72
      3A001A723A001E813F00E3FDED0038EB7E0038EB7E00E3FDED000F8434001972
      3A0019723A001972390018843C000000000000000000A77FD0001F258C001F25
      8C001F258C001F258C001F258C001F258C001F258C001F258C001F258C001F25
      8C001F258C001F258C00A481CE00000000000000000096909600FFFFFF00F6F6
      F600F5F5F500F5F5F50093939300939393009393930093939300939393009393
      9300FF01FF0000000000000000000000000000000000275FB000C5FFFF009BE3
      FF0096E0FF0092DEFF00919BA000939393009393930093939300939393009393
      9300FF01FF000000000000000000000000000000000000000000000000000000
      0000000000001A723A00E3FDED003EEC820039EB7E00E3FDED001A723A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000096909600FFFFFF00FAFA
      FA00F9F9F900F8F8F800F8F8F800F8F8F800FFFFFF0093939300000000000000
      00000000000000000000000000000000000000000000275FB000C5FFFF00A9EB
      FF00A4E8FF009FE6FF009BE3FF0096E0FF0091DDFF00C5FFFF00436FB9000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001A723A00E3FDED005CF195004FEF8D00B2F7CD001A723A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000096909600FFFFFF00FDFD
      FD00FDFDFD00FCFCFC0093939300939393009393930093939300000000000000
      00000000000000000000000000000000000000000000275FB000C5FFFF009393
      93009393930093939300939393009393930093939300C5FFFF00275FB0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000001A723A00E3FDED00B9FAD200B3F8CE00DDFCE9001A723A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000096909600FFFFFF00FFFF
      FF00FFFFFF00FEFEFE0093939300E7E7E7009393930000000000000000000000
      000000000000000000000000000000000000000000003F6BB7004172B9009393
      9300D9D9D900D9D9D900D9D9D900D9D9D900939393002C63B200275FB0000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000018843C0040955E0040955E0040955E0040955E002D7E4C000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000093939300FFFFFF00EAEA
      EA00EAEAEA00EAEAEA0096909600939393000000000000000000000000000000
      00000000000000000000000000000000000000000000F804FA00EA02EF009393
      930093939300E0E0E000E0E0E0009393930093939300EA02EF00F804FA000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000093939300939393009393
      9300939393009393930093939300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D509D5009393930093939300D509D5000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000500000000100010000000000800200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFFFFFFFFF3FFFFFFFFFFFF
      FFE1FFFFFFFFFFFFFFC1FFFFFFFFFFFFE003FFFFFFFFFFFFC007FFFFFFFFFFFF
      800FFFFFFFFFFFFF800FFFFFFFFFFFFF800FFFFFFFFFFFFF800FFFFFFFFFFFFF
      800FFFFFFFFFFFFF800FFFFFFFFFFFFF800FFFFFFFFFFFFFC01FFFFFFFFFFFFF
      E03FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBFFF3E7FFFFFFFFF1FFF1C7FFFFFFFF
      E0FFF88FFFFFFFFFC47FFC1FE007E007CE3FFE3FE007E007FF1FFC1FE007E007
      FF8FF88FFFFFFFFFFFC7F1C7FFFFFFFFFFE7F3E7FFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7E7FFFFFFFF
      FCFFE3C7FFFFFFFFF87FC183FFFFFFFFF03FC001FBFFF3E7E01FC003F1FFF1C7
      C00FE007E0FFF88F8007F00FC47FFC1F0003F00FCE3FFE3F0201E007FF1FFC1F
      8700C003FF8FF88FCF80C001FFC7F1C7FFC1C183FFE7F3E7FFE3E3C7FFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC01FC01F81FFFFFFC01FC01
      F81FFFFFFC01FC01F81FFFFFFC018001F81FFFFFFC0180018001800180018001
      8001800180018001800180018001800180018001800180018001800180038003
      8001800180078007F81FFFFF803F801FF81FFFFF803F801FF81FFFFF807F801F
      F81FFFFF80FF801FFFFFFFFF81FFF0FF}
  end
  object CloneGroupOperations: TpFIBQuery
    Transaction = Transaction
    Database = dm.db
    SQL.Strings = (
      
        'execute procedure Clone$Groups$Operations(:SourceGroupID, :DestG' +
        'roupID)')
    Left = 160
    Top = 664
  end
  object GroupOperations: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE TECHNOLOGY$GROUP$OPERATIONS'
      'SET'
      '    OPERATION$ID = :OPERATION$ID,'
      '    OPERATION$ORDER = :OPERATION$ORDER,'
      '    QUOTA = :QUOTA,'
      '    EXCLUDED$OPERATIONS = :EXCLUDED$OPERATIONS'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    TECHNOLOGY$GROUP$OPERATIONS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO TECHNOLOGY$GROUP$OPERATIONS('
      '    ID,'
      '    GROUP$ID,'
      '    OPERATION$ID,'
      '    QUOTA,'
      '    EXCLUDED$OPERATIONS'
      ')'
      'VALUES('
      '    :ID,'
      '    :GROUP$ID,'
      '    :OPERATION$ID,'
      '    :QUOTA,'
      '    :EXCLUDED$OPERATIONS'
      ')')
    RefreshSQL.Strings = (
      'select'
      '  go.ID,'
      '  go.Group$ID,'
      '  go.Operation$ID,'
      '  go.Operation$Order,'
      '  go.Quota,'
      '  go.Excluded$Operations,'
      '  o.N,'
      '  o.NS,'
      '  o.Rate'
      'from'
      '  technology$group$operations go'
      '  left outer join D_Oper o on (o.ID = go.Operation$ID)'
      'where( '
      '  Group$ID = ?ID'
      '     ) and (     GO.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '  go.ID,'
      '  go.Group$ID,'
      '  go.Operation$ID,'
      '  go.Operation$Order,'
      '  go.Quota,'
      '  go.Excluded$Operations,'
      '  o.Operation,'
      '  o.N,'
      '  o.NS,'
      '  o.Rate'
      'from'
      '  technology$group$operations go'
      '  left outer join D_Oper o on (o.ID = go.Operation$ID)'
      'where'
      '  Group$ID = ?ID'
      'order by '
      '  Operation$Order')
    AfterPost = AfterPost
    OnNewRecord = GroupOperationsNewRecord
    Transaction = Transaction
    Database = dm.db
    DataSource = SourceGroups
    Left = 8
    Top = 568
    dcForceOpen = True
    object GroupOperationsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object GroupOperationsGROUPID: TFIBIntegerField
      FieldName = 'GROUP$ID'
    end
    object GroupOperationsOPERATIONID: TFIBIntegerField
      FieldName = 'OPERATION$ID'
    end
    object GroupOperationsOPERATIONORDER: TFIBIntegerField
      FieldName = 'OPERATION$ORDER'
    end
    object GroupOperationsOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = True
    end
    object GroupOperationsN: TFIBFloatField
      FieldName = 'N'
    end
    object GroupOperationsNS: TFIBFloatField
      FieldName = 'NS'
    end
    object GroupOperationsRATE: TFIBFloatField
      FieldName = 'RATE'
    end
    object GroupOperationsEXCLUDEDOPERATIONS: TFIBStringField
      FieldName = 'EXCLUDED$OPERATIONS'
      Size = 64
      EmptyStrToNull = True
    end
    object GroupOperationsQUOTA: TFIBFloatField
      FieldName = 'QUOTA'
    end
  end
  object frGroupOperations: TfrDBDataSet
    DataSet = GroupOperations
    Left = 72
    Top = 568
  end
  object Semis: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  ID,'
      '  SEMIS'
      'from'
      '  D_Semis'
      'where'
      '  bit(types, 3) = 1'
      'order by '
      '  Sortind')
    Transaction = Transaction
    Database = dm.db
    Left = 8
    Top = 440
    object SemisID: TFIBIntegerField
      FieldName = 'ID'
    end
    object SemisSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object SourceSemis: TDataSource
    DataSet = Semis
    Left = 40
    Top = 440
  end
  object BarPopupMenu: TdxBarPopupMenu
    BarManager = BarManager
    ItemLinks = <
      item
        Visible = True
        ItemName = 'dxBarButton3'
      end
      item
        Visible = True
        ItemName = 'dxBarButton4'
      end
      item
        Visible = True
        ItemName = 'dxBarButton17'
      end
      item
        Visible = True
        ItemName = 'dxBarButton5'
      end
      item
        Visible = True
        ItemName = 'dxBarSubItem1'
      end
      item
        Visible = True
        ItemName = 'dxBarButton12'
      end
      item
        Visible = True
        ItemName = 'dxBarButton11'
      end
      item
        Visible = True
        ItemName = 'dxBarButton14'
      end
      item
        Visible = True
        ItemName = 'dxBarButton15'
      end
      item
        Visible = True
        ItemName = 'dxBarButton21'
      end>
    UseOwnFont = False
    Left = 40
    Top = 112
  end
  object frGroupArticles: TfrDBDataSet
    DataSet = GroupArticles
    Left = 72
    Top = 600
  end
  object GroupSemis: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE TECHNOLOGY$GROUP$SEMIS'
      'SET '
      '    GROUP$OPERATIONS$ID = :GROUP$OPERATIONS$ID,'
      '    COST = :COST,'
      '    UNIT$ID = :UNIT$ID,'
      '    WEIGHT$PERCENT = :WEIGHT$PERCENT,'
      '    QUANTITY = :QUANTITY,'
      '    SEMIS$ID = :SEMIS$ID'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    TECHNOLOGY$GROUP$SEMIS'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO TECHNOLOGY$GROUP$SEMIS('
      '    ID,'
      '    GROUP$OPERATIONS$ID,'
      '    COST,'
      '    UNIT$ID,'
      '    WEIGHT$PERCENT,'
      '    QUANTITY,'
      '    SEMIS$ID'
      ')'
      'VALUES('
      '    :ID,'
      '    :GROUP$OPERATIONS$ID,'
      '    :COST,'
      '    :UNIT$ID,'
      '    :WEIGHT$PERCENT,'
      '    :QUANTITY,'
      '    :SEMIS$ID'
      ')')
    RefreshSQL.Strings = (
      'select'
      '  tgs.id,'
      '  tgs.group$operations$id,'
      '  tgs.cost,'
      '  tgs.unit$id,'
      '  tgs.weight$percent,'
      '  tgs.quantity,'
      '  tgs.semis$id'
      'from'
      '  technology$group$semis tgs'
      'where(  '
      '  tgs.group$operations$id = ?ID'
      '     ) and (     TGS.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'select'
      '  tgs.id,'
      '  tgs.group$operations$id,'
      '  tgs.cost,'
      '  tgs.unit$id,'
      '  tgs.weight$percent,'
      '  tgs.quantity,'
      '  tgs.semis$id'
      'from'
      '  technology$group$semis tgs'
      'where'
      '  tgs.group$operations$id = ?ID'
      '')
    AfterPost = GroupSemisAfterPost
    OnNewRecord = GroupSemisNewRecord
    Transaction = Transaction
    Database = dm.db
    DataSource = SourceGroupOperations
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 8
    Top = 536
    dcForceOpen = True
    oRefreshAfterDelete = True
    object GroupSemisID: TFIBIntegerField
      FieldName = 'ID'
    end
    object GroupSemisGROUPOPERATIONSID: TFIBIntegerField
      FieldName = 'GROUP$OPERATIONS$ID'
    end
    object GroupSemisCOST: TFIBBCDField
      FieldName = 'COST'
      DisplayFormat = '#,##0.00'
      EditFormat = '0.00'
      Size = 2
      RoundByScale = True
    end
    object GroupSemisUNITID: TFIBStringField
      FieldName = 'UNIT$ID'
      Size = 10
      EmptyStrToNull = True
    end
    object GroupSemisSEMISID: TFIBIntegerField
      FieldName = 'SEMIS$ID'
    end
    object GroupSemisQUANTITY: TFIBBCDField
      FieldName = 'QUANTITY'
      Size = 3
      RoundByScale = True
    end
    object GroupSemisWEIGHTPERCENT: TFIBBCDField
      FieldName = 'WEIGHT$PERCENT'
      Size = 4
      RoundByScale = True
    end
  end
  object SourceGroupSemis: TDataSource
    DataSet = GroupSemis
    Left = 40
    Top = 536
  end
  object Print: TpFIBDataSet
    Transaction = Transaction
    Database = dm.db
    Left = 8
    Top = 408
  end
  object frPrint: TfrDBDataSet
    DataSet = Print
    Left = 72
    Top = 408
  end
  object frGroups: TfrDBDataSet
    DataSet = Groups
    Left = 72
    Top = 632
  end
  object Printer: TdxComponentPrinter
    CurrentLink = PrinterLink
    Version = 0
    Left = 8
    Top = 208
    object PrinterLink: TdxGridReportLink
      Active = True
      Component = GridGroups
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 10000
      PrinterPage.Margins.Left = 10000
      PrinterPage.Margins.Right = 10000
      PrinterPage.Margins.Top = 10000
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42604.621637777770000000
      ShrinkToPageWidth = True
      OptionsView.Footers = False
      OptionsView.Caption = False
      OptionsView.ExpandButtons = False
      OptionsView.FilterBar = False
      BuiltInReportLink = True
    end
  end
  object Tmp: TpFIBQuery
    Transaction = Transaction
    Database = dm.db
    Left = 160
    Top = 632
  end
  object DataSetUnit: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select * from d_unit where unitid in (-1, 0, 3, 4) order by unit' +
        'id')
    Transaction = Transaction
    Database = dm.db
    Left = 8
    Top = 376
  end
  object UnitSource: TDataSource
    DataSet = DataSetUnit
    Left = 40
    Top = 376
  end
  object Consts: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE TECHNOLOGY$CONSTS'
      'SET '
      '    GOLD$COST = :GOLD$COST,'
      '    SILVER$COST = :SILVER$COST,'
      '    LOSSES$PERCENT = :LOSSES$PERCENT,'
      '    SILVER$LOSSES$PERCENT = :SILVER$LOSSES$PERCENT,'
      '    PERIOD$BEGIN = :STARTD,'
      '    PERIOD$END = :ENDD'
      'WHERE'
      '  id = 1'
      '    ')
    DeleteSQL.Strings = (
      ''
      '        ')
    RefreshSQL.Strings = (
      'select '
      '  gold$cost, silver$cost, losses$percent, silver$losses$percent,'
      '  onlydate(period$begin) STARTD, onlydate(period$end) ENDD'
      'from'
      '  technology$consts'
      'where '
      '  id = 1')
    SelectSQL.Strings = (
      'select '
      '  gold$cost, silver$cost, losses$percent, silver$losses$percent,'
      '  onlydate(period$begin) StartD, onlydate(period$end) EndD'
      'from'
      '  technology$consts'
      'where '
      '  id = 1')
    AfterOpen = ConstsAfterOpen
    AfterPost = AfterPost
    Transaction = dm.tr
    Database = dm.db
    Left = 8
    Top = 344
    object ConstsGOLDCOST: TFIBBCDField
      FieldName = 'GOLD$COST'
      DisplayFormat = '#,##0.00'
      EditFormat = '0.00'
      Size = 2
      RoundByScale = True
    end
    object ConstsLOSSESPERCENT: TFIBFloatField
      FieldName = 'LOSSES$PERCENT'
      DisplayFormat = '#,##0.0'
      EditFormat = '0.0'
    end
    object ConstsSTARTD: TFIBDateTimeField
      FieldName = 'STARTD'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object ConstsENDD: TFIBDateTimeField
      FieldName = 'ENDD'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object ConstsSILVERCOST: TFIBBCDField
      FieldName = 'SILVER$COST'
      Size = 2
      RoundByScale = True
    end
    object ConstsSILVERLOSSESPERCENT: TFIBFloatField
      FieldName = 'SILVER$LOSSES$PERCENT'
    end
  end
  object SourceConsts: TDataSource
    DataSet = Consts
    Left = 40
    Top = 344
  end
  object cxHintStyleController: TcxHintStyleController
    Global = False
    HintStyle.CaptionFont.Charset = DEFAULT_CHARSET
    HintStyle.CaptionFont.Color = clWindowText
    HintStyle.CaptionFont.Height = -11
    HintStyle.CaptionFont.Name = 'Tahoma'
    HintStyle.CaptionFont.Style = []
    HintStyle.Font.Charset = DEFAULT_CHARSET
    HintStyle.Font.Color = clWindowText
    HintStyle.Font.Height = -11
    HintStyle.Font.Name = 'Verdana'
    HintStyle.Font.Style = []
    HintStyle.IconSize = cxisSmall
    HintStyle.IconType = cxhiInformation
    Left = 40
    Top = 208
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 72
    Top = 208
    PixelsPerInch = 96
    object cxStyle1: TcxStyle
    end
  end
end
