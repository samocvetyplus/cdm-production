{*****************************************}
{  ������ ��������� ��������              }
{  Copyrigth (C) 2003-2004 basile for CDM }
{  created 01.10.2003                     }
{  last update 20.04.2004                 }
{  v0.17                                  }
{*****************************************}

unit AfList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  ActnList, Menus, TB2Item, TB2Dock, TB2Toolbar, Grids, DBGridEh, Buttons,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmAfList = class(TfmAncestor)
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;
    acEvent: TActionList;
    acAddS: TAction;
    acDel: TAction;
    ppAdd: TTBPopupMenu;
    acAddL: TAction;
    Panel1: TPanel;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem1: TTBItem;
    acPeriod: TAction;
    gridList: TDBGridEh;
    acEdit: TAction;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    SpeedItem1: TSpeedItem;
    acAdd: TAction;
    acRefreshW: TAction;
    ppDoc: TTBPopupMenu;
    TBItem4: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    TBItem7: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    TBItem8: TTBItem;
    acWhTails: TAction;
    spitPrint: TSpeedItem;
    acPrintS: TAction;
    acPrintL: TAction;
    ppPrint: TTBPopupMenu;
    TBItem9: TTBItem;
    TBItem10: TTBItem;
    TBItem11: TTBItem;
    acPrintOrder: TAction;
    acClose: TAction;
    SpeedItem2: TSpeedItem;
    acMiltiSelect: TAction;
    TBItem12: TTBItem;
    spbtnWhTails: TSpeedButton;
    TBItem13: TTBItem;
    acPrintPassport: TAction;
    procedure FormCreate(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure gridListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acAddSExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acAddLExecute(Sender: TObject);
    procedure acEditExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acRefreshWExecute(Sender: TObject);
    procedure acRefreshWUpdate(Sender: TObject);
    procedure acWhTailsExecute(Sender: TObject);
    procedure acPrintSExecute(Sender: TObject);
    procedure acPrintSUpdate(Sender: TObject);
    procedure acPrintLUpdate(Sender: TObject);
    procedure acPrintLExecute(Sender: TObject);
    procedure acPrintOrderExecute(Sender: TObject);
    procedure acPrintOrderUpdate(Sender: TObject);
    procedure acMiltiSelectExecute(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acEditUpdate(Sender: TObject);
    procedure acCloseUpdate(Sender: TObject);
    procedure acPrintPassportExecute(Sender: TObject);
    procedure acPrintPassportUpdate(Sender: TObject);
  private
  end;

var
  fmAfList: TfmAfList;

implementation

uses DictData, MainData, Period, dbUtil, Affinaj, DocAncestor, eAffInvType,
     Editor, WhTails, fmUtils, DB, PrintData, FIBDataSet, MsgDialog,
  ProductionConsts;

{$R *.dfm}

procedure TfmAfList.FormCreate(Sender: TObject);
begin
  //ppDoc.Skin := dm.ppSkin;
  //ppPrint.Skin := dm.ppSkin;
  acPeriod.Caption := '������ � '+DateToStr(dm.BeginDate)+' �� '+DateToStr(dm.EndDate);
  // ��������� ������
  if dm.tr.Active then dm.tr.Commit;
  dm.tr.StartTransaction;
  dmMain.taAFList.Open;
end;

procedure TfmAfList.acPeriodExecute(Sender: TObject);
var
  Bd, Ed : TDateTime;
begin
  Bd := dm.BeginDate;
  Ed := dm.EndDate;
  if ShowPeriodForm(Bd, Ed) then
  begin
    dm.BeginDate := Bd;
    dm.EndDate := Ed;
    acPeriod.Caption := '������ � '+DateToStr(dm.BeginDate)+' �� '+DateToStr(dm.EndDate);
    Application.ProcessMessages;
    ReOpenDataSet(dmMain.taAFList);
    Application.ProcessMessages;
  end;
end;

procedure TfmAfList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PostDataSet(dmMain.taAFList);
  CloseDataSet(dmMain.taAFList);
  dm.tr.Commit;
  Action := caFree;
end;

procedure TfmAfList.gridListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(dmMain.taAFListISCLOSE.AsInteger = 1)then Background := clBtnFace
  else if(AnsiUpperCase(Column.FieldName) = 'ITYPE')then
    case Column.Field.AsInteger of
      17 : Background := clWhite;
      18 : Background := clInfoBk;
      20 : Background := clSkyBlue;
    end;
end;



procedure TfmAfList.acAddSExecute(Sender: TObject);
begin
  with dmMain do
  begin
    AffinajMode := amS;
    PostDataSet(taAFList);
    taAFList.Insert;
    taAFList.Post;
    ShowDocForm(TfmAffinaj, taAFList, taAFListINVID.AsInteger, quTmp);
    RefreshDataSet(taAFList);
  end;
end;

procedure TfmAfList.acAddLExecute(Sender: TObject);
begin
  with dmMain do
  begin
    PostDataSet(taAFList);
    AffinajMode := amL;
    taAFList.Insert;
    taAFList.Post;
    ShowDocForm(TfmAffinaj, taAFList, taAFListINVID.AsInteger, quTmp);
    RefreshDataSet(taAFList);
  end;
end;

procedure TfmAfList.acDelExecute(Sender: TObject);
var
  SaveEvent : TDataSetNotifyEvent;
  Ref : integer;
  Bookmark : Pointer;
begin
  if(dmMain.taAFListITYPE.AsInteger = 20)then
  begin
    if(not dm.IsAdm)then
      if(dm.User.UserId <> dmMain.taAFListUSERID.AsInteger)then
        raise EWarning.Create(Format(rsAccessDenied, [dm.User.FIO, '��� �������� ���������']));
    if MessageDialog('������� ��� ������?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then SysUtils.Abort;
    SaveEvent := dmMain.taAFList.BeforeDelete;
    dmMain.taAFList.BeforeDelete := nil;
    try
      Ref := dmMain.taAFListINVID.AsInteger;
      dmMain.taAFList.Delete;
    finally
      dmMain.taAFList.BeforeDelete := SaveEvent;
    end;
    Bookmark := dmMain.taAFList.GetBookmark;
    dmMain.taAFList.DisableControls;
    try
      dmMain.taAFList.First;
      while not dmMain.taAFList.Eof do
      begin
        if(dmMain.taAFListREF.AsInteger = Ref)then RefreshDataSet(dmMain.taAFList);
        dmMain.taAFList.Next;
      end
    finally
      dmMain.taAFList.GotoBookmark(Bookmark);
      dmMain.taAFList.EnableControls;
    end;
  end
  else dmMain.taAFList.Delete;
end;

procedure TfmAfList.acEditExecute(Sender: TObject);
begin
  case dmMain.taAFListITYPE.AsInteger of
    17 : dmMain.AffinajMode := amS;
    18 : dmMain.AffinajMode := amL;
  end;
  ShowDocForm(TfmAffinaj, dmMain.taAFList, dmMain.taAFListINVID.AsInteger, dmMain.quTmp);
  RefreshDataSet(dmMain.taAFList);
end;

procedure TfmAfList.acAddExecute(Sender: TObject);
begin
  if ShowEditor(TfmeAffInvT, TfmEditor(fmeAffInvT)) <> mrOk then Exit;
  case Editor.vResult of
    0 : acAddS.Execute;
    1 : acAddL.Execute;
  end;
end;

procedure TfmAfList.acRefreshWExecute(Sender: TObject);
begin
  ExecSQL('execute procedure ReCalc_W_AffinajInv('+dmMain.taAFListINVID.AsString+')', dmMain.quTmp);
  RefreshDataSet(dmMain.taAFList);
end;

procedure TfmAfList.acRefreshWUpdate(Sender: TObject);
begin
  acRefreshW.Enabled := dm.IsAdm;
end;

procedure TfmAfList.acWhTailsExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmWhTails, TForm(fmWhTails));
end;

procedure TfmAfList.acPrintSExecute(Sender: TObject);
begin
  PostDataSet(dmMain.taAFList);
  dmPrint.PrintDocumentA(dkAffS_Inv, VarArrayOf([VarArrayOf([dmMain.taAFListINVID.AsInteger]),
    VarArrayOf([dmMain.taAFListINVID.AsInteger, 0])]));
end;

procedure TfmAfList.acPrintLExecute(Sender: TObject);
begin
  PostDataSet(dmMain.taAFList);
  dmPrint.PrintDocumentA(dkAffL_Inv, VarArrayOf([VarArrayOf([dmMain.taAFListINVID.AsInteger]),
     VarArrayOf([dmMain.taAFListINVID.AsInteger, 0])]));
end;

procedure TfmAfList.acPrintSUpdate(Sender: TObject);
begin
  with gridList.DataSource.DataSet do
    acPrintS.Visible := Active and (not IsEmpty) and (FieldByName('ITYPE').AsInteger = 17);
end;

procedure TfmAfList.acPrintLUpdate(Sender: TObject);
begin
  with gridList.DataSource.DataSet do
    acPrintL.Visible := Active and (not IsEmpty) and (FieldByName('ITYPE').AsInteger = 18);
end;

procedure TfmAfList.acPrintOrderExecute(Sender: TObject);
begin
  PostDataSet(dmMain.taAFList);
  dmPrint.PrintDocumentA(dkAff_Order, VarArrayOf([VarArrayOf([dmMain.taAFListINVID.AsInteger]),
     VarArrayOf([dmMain.taAFListINVID.AsInteger, 1])]));
end;

procedure TfmAfList.acPrintOrderUpdate(Sender: TObject);
begin
  with gridList.DataSource.DataSet do
    acPrintOrder.Enabled := Active and (not IsEmpty) and
    ((FieldByName('ITYPE').AsInteger = 17) or
     (FieldByName('ITYPE').AsInteger = 18));
end;

procedure TfmAfList.acPrintPassportExecute(Sender: TObject);
var
  InvoiceID: Integer;
begin
  inherited;

  InvoiceID := dmMain.taAFListINVID.AsInteger;

  with dmPrint do
  begin
    frxHeader.DataSet := taInvoiceHeader;
    frxItems.DataSet := dmMain.taAffinaj;
    dmPrint.PrintFrxDocument(123, [InvoiceID], [InvoiceID]);
  end;

end;

procedure TfmAfList.acPrintPassportUpdate(Sender: TObject);
begin
  inherited;
  acPrintPassport.Enabled := (dmMain.taAFListW0.AsFloat > 0) and (dmMain.taAFListW1.AsFloat = 0);  
end;

procedure TfmAfList.acMiltiSelectExecute(Sender: TObject);
begin
  if dgMultiSelect in gridList.Options then
  begin
    gridList.Selection.Clear;
    gridList.Options := gridList.Options-[dgMultiSelect];
  end
  else gridList.Options := gridList.Options+[dgMultiSelect];
end;

procedure TfmAfList.acCloseExecute(Sender: TObject);

  procedure UpdateActAffinaj(Ref : integer);
  begin
    if dmMain.taAFList.Locate('INVID', Ref, [])then RefreshDataSet(dmMain.taAFList)
    else ReOpenDataSet(dmMain.taAFList);
  end;

var
  Ref, i, t : integer;
  DocName : string;
  Bookmark : Pointer;
begin
  if(gridList.SelectedRows.Count <= 1)then
  begin
    case dmMain.taAFListITYPE.AsInteger of
      17 : DocName := '����������� ������ (�����)';
      18 : DocName := '����������� ����';
      else raise Exception.Create('����������� ��� �����������');
    end;
    if(acClose.Caption = '�������')then
      ExecSQL('execute procedure CloseDoc('#39+DocName+#39', '+dmMain.taAFListINVID.AsString+')', dmMain.quTmp)
    else
      ExecSQL('execute procedure OpenDoc('#39+DocName+#39', '+dmMain.taAFListINVID.AsString+')', dmMain.quTmp);
    RefreshDataSet(dmMain.taAFList);
    UpdateActAffinaj(dmMain.taAFListREF.AsInteger);
    eXit;
  end;

  Bookmark := dmMain.taAfList.GetBookmark;
  dmMain.taAfList.DisableControls;
  try
    // 1. ��������� ���� ���������� ��������� ���� ������ ����
    dmMain.taAfList.GotoBookmark(Pointer(gridList.SelectedRows.Items[0]));
    t := dmMain.taAFListITYPE.AsInteger;
    for i:=1 to Pred(gridList.SelectedRows.Count) do
    begin
      dmMain.taAfList.GotoBookmark(Pointer(gridList.SelectedRows.Items[i]));
      if(t<>dmMain.taAFListITYPE.AsInteger)then raise Exception.Create('���������� ��������� ������ ���� ������ ����');
    end;
    // 2. ������� ������ �������� � ������ ����������
    dmMain.taAfList.GotoBookmark(Pointer(gridList.SelectedRows.Items[0]));
    case dmMain.taAFListITYPE.AsInteger of
      17 : DocName := '����������� ������ (�����)';
      18 : DocName := '����������� ����';
      else raise Exception.Create('����������� ��� �����������');
    end;
    if(acClose.Caption = '�������')then
      ExecSQL('execute procedure CloseDoc('#39+DocName+#39', '+dmMain.taAFListINVID.AsString+')', dmMain.quTmp)
    else
      ExecSQL('execute procedure OpenDoc('#39+DocName+#39', '+dmMain.taAFListINVID.AsString+')', dmMain.quTmp);
    RefreshDataSet(dmMain.taAFList);
    Ref := dmMain.taAFListREF.AsInteger;
    // 3. �������� ���� Ref ��� ���������� ���������
    for i:=1 to Pred(gridList.SelectedRows.Count) do
    begin
      dmMain.taAfList.GotoBookmark(Pointer(gridList.SelectedRows.Items[i]));
      dmMain.taAFList.Edit;
      dmMain.taAFListREF.AsInteger := Ref;
      dmMain.taAFList.Post;
      if(acClose.Caption = '�������')then
        ExecSQL('execute procedure CloseDoc('#39+DocName+#39', '+dmMain.taAFListINVID.AsString+')', dmMain.quTmp)
      else
        ExecSQL('execute procedure OpenDoc('#39+DocName+#39', '+dmMain.taAFListINVID.AsString+')', dmMain.quTmp);
      RefreshDataSet(dmMain.taAFList);
    end;
    // 4. ��������/�������� ��� ������
    UpdateActAffinaj(Ref);
  finally
    dmMain.taAfList.GotoBookmark(Bookmark);
    dmMain.taAfList.EnableControls;
  end;
end;

procedure TfmAfList.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := dmMain.taAFList.Active and (not dmMain.taAFList.IsEmpty);
end;

procedure TfmAfList.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := dmMain.taAFList.Active;
end;

procedure TfmAfList.acEditUpdate(Sender: TObject);
begin
  acEdit.Enabled := dmMain.taAFList.Active and (not dmMain.taAFList.IsEmpty) and
    (dmMain.taAFListITYPE.AsInteger <> 20);
end;

procedure TfmAfList.acCloseUpdate(Sender: TObject);
begin
  acClose.Enabled := dmMain.taAFList.Active and
                    (not dmMain.taAFList.IsEmpty) and
                    (dmMain.taAFListITYPE.AsInteger<>20) and
                    (dm.User.UserId = dmMain.taAFListUSERID.AsInteger);

  if acClose.Enabled then
  begin

    if(dmMain.taAFListISCLOSE.AsInteger = 1)then
    begin
      acClose.Caption := '�������';
      acClose.ImageIndex := 7;
    end
    else begin
      acClose.Caption := '�������';
      acClose.ImageIndex := 6;
    end;

  end;
end;

end.
