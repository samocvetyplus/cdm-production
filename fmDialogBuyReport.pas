unit fmDialogBuyReport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, dxBar, dxBarExtItems, FIBDataSet, pFIBDataSet,
  cxLabel, cxCalendar, cxBarEditItem, ImgList, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPSCore, ActnList, dxPScxCommon,
  dxPScxGrid6Lnk;

type
  TDialogBuyReport = class(TForm)
    View: TcxGridDBTableView;
    Level: TcxGridLevel;
    Grid: TcxGrid;
    BarManager: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    DataSet: TpFIBDataSet;
    DataSource: TDataSource;
    DataSetNAME: TFIBStringField;
    DataSetSEMIS: TFIBStringField;
    DataSetW: TFIBFloatField;
    DataSetCOST: TFIBFloatField;
    ViewNAME: TcxGridDBColumn;
    ViewW: TcxGridDBColumn;
    ViewCOST: TcxGridDBColumn;
    ViewSEMIS: TcxGridDBColumn;
    ImageList: TImageList;
    dxBarLargeButton3: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    StartDateEdit: TcxBarEditItem;
    cxBarEditItem3: TcxBarEditItem;
    EndDateEdit: TcxBarEditItem;
    Printer: TdxComponentPrinter;
    Actions: TActionList;
    acPrint: TAction;
    acUpdate: TAction;
    PrinterLink: TdxGridReportLink;
    dxBarLargeButton4: TdxBarLargeButton;
    acClose: TAction;
    DataSetW585: TFIBFloatField;
    ViewW585: TcxGridDBColumn;
    procedure DataSetBeforeOpen(DataSet: TDataSet);
    procedure acUpdateExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    class procedure Execute;
  end;

var
  DialogBuyReport: TDialogBuyReport;

implementation

uses DictData, DateUtils;

{$R *.dfm}

procedure TDialogBuyReport.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TDialogBuyReport.acPrintExecute(Sender: TObject);
var
  Link: TdxgridReportLink;
begin
  Link := TdxGridReportLink(Printer.LinkByName('PrinterLink'));
  Link.ReportTitle.Text := '����� �� ������ [' + VarToStr(StartDateEdit.CurEditValue) + ' � ' + VarToStr(EndDateEdit.CurEditValue) + ']';
  Link.ReportDocument.Caption := Link.ReportTitle.Text;
  Link.ReportDocument.CreationDate := Date;
  Link.Preview;
end;

procedure TDialogBuyReport.acUpdateExecute(Sender: TObject);
begin
  if (VarToStr(StartDateEdit.EditValue) = '') or (VarToStr(EndDateEdit.EditValue) = '')
    //or (VarToDateTime(StartDateEdit.EditValue) > VarToDateTime(EndDateEdit.EditValue))
  then
    begin
      ShowMessage('������ ��� ������ ����� �������!');
      Exit;
    end
  else
    begin
      Screen.Cursor := crSQLWait;
      if DataSet.Active then DataSet.Close;
      DataSet.Open;
      View.DataController.Groups.FullExpand;
      Screen.Cursor := crDefault;
    end;
end;

procedure TDialogBuyReport.DataSetBeforeOpen(DataSet: TDataSet);
begin
  with tpFIBDataSet(DataSet).Params do
    begin
      ByName['StartDate'].AsDate := StartDateEdit.CurEditValue;
      ByName['EndDate'].AsDate := EndDateEdit.CurEditValue;
    end;
end;


class procedure TDialogBuyReport.Execute;
begin
  DialogBuyReport := nil;
  try
    DialogBuyReport := TDialogBuyReport.Create(Application);
    DialogBuyReport.ShowModal;
  finally
    FreeAndNil(DialogBuyReport);
  end;
end;

procedure TDialogBuyReport.FormCreate(Sender: TObject);
begin
  StartDateEdit.EditValue := IncMonth(Today, -1);
  EndDateEdit.EditValue := DateOf(Today);

end;

procedure TDialogBuyReport.FormDestroy(Sender: TObject);
begin
  DataSet.Transaction.Commit;
  DataSet.Close;
end;



end.
