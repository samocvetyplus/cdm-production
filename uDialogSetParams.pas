unit uDialogSetParams;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxPC, cxContainer,
  cxEdit, cxDropDownEdit, cxCalendar, StdCtrls, cxTextEdit, cxMaskEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, Buttons, cxDBEdit, DB, dbTree;

type
  TDialogSetParams = class(TForm)
    paramLabel: TLabel;
    GroupBox: TGroupBox;
    startDateBox: TcxDateEdit;
    endDateBox: TcxDateEdit;
    Label2: TLabel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    paramBox: TcxComboBox;
    procedure BitBtn1Click(Sender: TObject);
  private
    constructor Create(AOwner: TComponent; CreationType: SmallInt);
  public
    reportType: Smallint;
    class procedure Execute(CreationType: SmallInt);
  end;

var
  DialogSetParams: TDialogSetParams;

implementation

{$R *.dfm}

uses TechnologyGroups, DateUtils, DictData;

procedure TDialogSetParams.BitBtn1Click(Sender: TObject);
begin
  case reportType of
    1:
      begin
        fmTechnologyGroups.Print.ParamByName('Operation$ID').AsString := TNodeData(paramBox.Properties.Items.Objects[paramBox.ItemIndex]).Code;
      end;
    0, 2:
      begin
        fmTechnologyGroups.Print.ParamByName('Start$Date').AsDate := DateOf(startDateBox.Date);
        fmTechnologyGroups.Print.ParamByName('End$Date').AsDate := DateOf(endDateBox.Date);
      end;
    3:
      begin
        fmTechnologyGroups.Print.ParamByName('Semis').AsString := TNodeData(paramBox.Properties.Items.Objects[paramBox.ItemIndex]).Code;
        fmTechnologyGroups.Print.ParamByName('Start$Date').AsDate := DateOf(startDateBox.Date);
        fmTechnologyGroups.Print.ParamByName('End$Date').AsDate := DateOf(endDateBox.Date);
      end;
  end;
end;

constructor TDialogSetParams.Create(aOwner: TComponent; CreationType: SmallInt);
begin

  inherited Create(aOwner);

  case CreationType of                   
    1:
      begin
        paramLabel.Caption := '��������';
        parambox.Properties.Items.Assign(dm.dOper);
        parambox.Properties.Items.Delete(0);
        paramBox.ItemIndex := 0;
        endDateBox.Enabled := false;
        startDateBox.Enabled := false;
        groupBox.Enabled := false;
      end;
    3:
      begin
        paramLabel.Caption := '������������';
        parambox.Properties.Items.Assign(dm.dSemis);
        parambox.Properties.Items.Delete(0);
        paramBox.ItemIndex := 0;
        endDateBox.Date := DateOF(Now);
        startDateBox.Date := IncYear(DateOf(Now), -1);
      end;
    2:
      begin
        paramBox.Enabled := false;
        paramLabel.Caption := '������������';
        paramLabel.Enabled := false;
        parambox.Visible := false;
        paramLabel.Visible := false;
        endDateBox.Date := DateOf(Now);
        startDateBox.Date := IncYear(DateOf(Now), -1);
      end;
    0:
      begin
        paramBox.Enabled := false;
        paramLabel.Caption := '������������';
        paramLabel.Enabled := false;
        endDateBox.Date := DateOf(fmTechnologyGroups.Consts.FieldByName('EndD').AsDateTime);
        startDateBox.Date := DateOf(fmTechnologyGroups.Consts.FieldByName('StartD').AsDateTime);
      end;
  end;

  ReportType := CreationType;
end;

class procedure TDialogSetParams.Execute(CreationType: Smallint);
begin
  DialogSetParams := nil;
  try
    DialogSetParams := TDialogSetParams.Create(Application, CreationType);
    DialogSetParams.ShowModal;
  finally
    FreeAndNil(DialogSetParams);
  end;
end;



end.
