unit VerificationList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor,  TB2Item, ActnList, Menus,
  ImgList, Grids, DBGridEh, StdCtrls, ExtCtrls, ComCtrls, Mask,
  DBCtrlsEh, DBLookupEh, Db, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmVerificationList = class(TfmListAncestor)
    lbYear: TLabel;
    edYear: TDBNumberEditEh;
    lbComp: TLabel;
    cbComp: TDBComboBoxEh;
    procedure edYearChange(Sender: TObject);
    procedure edYearKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure cbCompChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintDocExecute(Sender: TObject);
    procedure acPrintDocUpdate(Sender: TObject);
  private
    procedure FilterActWorkList(DataSet: TDataSet; var Accept: boolean);
  end;

var
  fmVerificationList: TfmVerificationList;

implementation

uses Data, MainData, dbUtil, DictData, dbTree, ActWork, fmUtils, DocAncestor,
  FIBDatabase, pFIBDataSet, MsgDialog, eActVerification, Editor, DateUtils,
  VerificationAct, PrintData, PrintData2;

{$R *.dfm}

procedure TfmVerificationList.edYearChange(Sender: TObject);
begin
  dmMain.DocListYear := edYear.Value;
  ReOpenDataSet(DataSet);
end;

procedure TfmVerificationList.edYearKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : begin
      dmMain.DocListYear := edYear.Value;
      ReOpenDataSet(DataSet);
    end;
  end;
end;

procedure TfmVerificationList.FormCreate(Sender: TObject);
begin
  gridDocList.DataSource := dmData.dsrVerificationList;
  if not dmData.taVerificationList.Active  then
    dmData.taVerificationList.Open;
  DataSet := dmData.taVerificationList;
  (* ���������� ��� *)
  edYear.OnChange := nil;
  edYear.Value := dmMain.DocListYear;
  edYear.OnChange := edYearChange;
  (* ��������� ������ �� ����������� *)
  cbComp.OnChange := nil;
  cbComp.Items.Clear;
  cbComp.Items.Assign(dm.dComp);
  cbComp.ItemIndex := 0;
  cbComp.OnChange := cbCompChange;
  dmMain.FilterCompId := IntToStr(ROOT_COMP_CODE);
  DataSet.OnFilterRecord := FilterActWorkList;
  DataSet.Filtered := True;
  inherited;
end;

procedure TfmVerificationList.cbCompChange(Sender: TObject);
begin
  if(cbComp.ItemIndex<=0)then dmMain.FilterCompId := IntToStr(ROOT_COMP_CODE)
  else with cbComp do
    dmMain.FilterCompId := IntToStr(TNodeData(Items.Objects[ItemIndex]).Code);
  DataSet.Filtered := False;
  DataSet.Filtered := True;
end;

procedure TfmVerificationList.FilterActWorkList(DataSet: TDataSet; var Accept: boolean);
begin
  Accept := (dmMain.FilterCompId = IntToStr(ROOT_COMP_CODE)) or (dmMain.FilterCompId = DataSet.FieldByName('COMPID').AsString);
end;

procedure TfmVerificationList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  DataSet.OnFilterRecord := nil;
  DataSet.Filtered := False;
end;

procedure TfmVerificationList.acAddDocExecute(Sender: TObject);
begin
  if ShowEditor(TfmeActVerification, TfmEditor(fmeActVerification))<>mrOk then eXit;

  with dmData do
  begin
    DataSet.Append;
    taVerificationListSUPID.AsInteger := vResult[0];
    taVerificationListBD.AsDateTime := StartOfTheDay(VarToDateTime(vResult[1]));
    taVerificationListED.AsDateTime := EndOfTheDay(VarToDateTime(vResult[2]));
    PostDataSet(DataSet);
    ExecSQL('execute procedure Create_AWTolling('+taVerificationListINVID.AsString+')', dm.quTmp);
    ShowDocForm(TfmVerificationAct, dmData.taVerificationList, dmData.taVerificationListINVID.AsInteger, dm.quTmp);
    PostDataSet(DataSet);
    RefreshDataSet(DataSet);
  end;
end;

procedure TfmVerificationList.acEditDocExecute(Sender: TObject);
begin
  with dmData do
  begin
    DataSet.Edit;
    ShowDocForm(TfmVerificationAct, dmData.taVerificationList, dmData.taVerificationListINVID.AsInteger, dm.quTmp);
    with (DataSet as TpFIBDataSet).Transaction do
      if Active then CommitRetaining;
    RefreshDataSet(DataSet);
  end;
end;

procedure TfmVerificationList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if DataSet.FieldByName('ISCLOSE').AsInteger = 1 then Background := clCloseDoc
  else Background := clOpenDoc;
end;

procedure TfmVerificationList.acPrintDocExecute(Sender: TObject);
begin
  PostDataSet(dmData.taVerificationList);
  dmPrint.PrintDocumentA(dkVerificationActTolling, VarArrayOf([
    VarArrayOf([dmData.taVerificationListINVID.AsInteger]),
    VarArrayOf([dmData.taVerificationListINVID.AsInteger])]));
  dmPrint2.taVerActMaster.Open;
end;

procedure TfmVerificationList.acPrintDocUpdate(Sender: TObject);
begin
  acPrintDoc.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

end.
