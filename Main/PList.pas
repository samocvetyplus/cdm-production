{***********************************************}
{  ������ ���������� ��������������             }
{  Copyrigth (C) 2001-2004 basile for CDM       }
{  v2.20                                        }
{***********************************************}
unit PList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, Menus,  ImgList,  StdCtrls,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, ComCtrls, db,
  Buttons, Mask, DBCtrlsEh, DBGridEh, ActnList,  TB2Item,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmPList = class(TfmListAncestor)
    cmbxMat: TDBComboBoxEh;
    acCheckData: TAction;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem5: TTBItem;
    TBItem7: TTBItem;
    ac1C: TAction;
    ppPrint: TTBPopupMenu;
    acPrintSJ: TAction;
    TBItem6: TTBItem;
    TBItem8: TTBItem;
    Label1: TLabel;
    edYear: TDBNumberEditEh;
    procedure FormCreate(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acDelDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure cmbxMatChange(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintDocUpdate(Sender: TObject);
    procedure gridDocListOrderBy(Sender: TObject; OrderBy: String);
    procedure acCheckDataExecute(Sender: TObject);
    procedure acCheckDataUpdate(Sender: TObject);
    procedure ac1CExecute(Sender: TObject);
    procedure ac1CUpdate(Sender: TObject);
    procedure acPrintSJExecute(Sender: TObject);
    procedure acPrintSJUpdate(Sender: TObject);
    procedure acDelDocUpdate(Sender: TObject);
    procedure edYearChange(Sender: TObject);
    procedure edYearKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  protected
    procedure DepClick(Sender : TObject);override;
    procedure RefreshPeriod;
    procedure Access;
  public

  end;

var
  fmPList: TfmPList;

implementation

uses MainData, DocAncestor, Protocol, dbUtil, DictData, fmUtils, dbTree,
  Protocol_d, PrintData, MsgDialog, DateUtils, DbUtilsEh, 
  CheckProtocolData, ProtocolMat, pFIBDataSet;

{$R *.dfm}

procedure TfmPList.FormCreate(Sender: TObject);
begin
  Access;
  edYear.OnChange := nil;
  edYear.Value := dmMain.DocListYear;
  edYear.OnChange := edYearChange;
  FCurrDep := -2;
  dm.WorkMode := 'Protocol';
  // ����������
  with dm.tr do
  begin
    if Active then Commit;
    StartTransaction;
  end;
  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMat);
  cmbxMat.ItemIndex := 0;
  dmMain.FilterMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := cmbxMatChange;
  dmMain.FilterMatId := '1';
  //����� ��������
  //ShowMessage('��������:' + IntToStr(dm.User.AccProd_d and 8));
  if (dm.User.AccProd_d and 8) <> 8
    then
      begin
        acAddDoc.Visible := true;
        acDelDoc.Visible := true;
        gridDocList.ReadOnly := false;

      end
  else
    begin
      acAddDoc.Visible := false;
      acDelDoc.Visible := false;
      gridDocList.ReadOnly := false;
      //dmMain.taPList.CanModify := false;
    end;

  inherited;
end;

procedure TfmPList.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  inherited;
end;

procedure TfmPList.RefreshPeriod;
begin
  FBd := dm.BeginDate;
  FEd := dm.EndDate;
  ShowPeriod;
end;

procedure TfmPList.cmbxMatChange(Sender: TObject);
begin
  dmMain.FilterMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taPList);
end;

procedure TfmPList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if AnsiUpperCase(Column.FieldName)='RECNO' then Background:= clMoneyGreen
  else if AnsiUpperCase(Column.FieldName)='DOCNO' then
         if dmMain.taPListT.AsInteger = 0 then Background:= clSkyBlue
         else Background:= clInfoBk
  else if dmMain.taPListIsClose.AsInteger=0 then Background:=clInfoBk
    else Background:=clBtnFace;
end;

procedure TfmPList.acAddDocExecute(Sender: TObject);
var
  ActId : integer;
begin
  if(dmMain.FilterMatId = ROOT_MAT_CODE)then raise EWarning.Create('�������� ��������');
  if MessageDialog('������� �������� �������������� � '+DateToStr(dm.BeginDate)+
    ' �� '+DateToStr(dm.EndDate), mtConfirmation, [mbYes, mbNo], 0)=mrYes
  then begin
    inherited;
    ActId := dmMain.taPListID.AsInteger;
    Screen.Cursor := crSQLWait;
    try
      dmMain.quTmp.Transaction.CommitRetaining;
      {$ifdef OLD_PROTOCOL}
      // ������ ����
      ExecSQL('select * from S_CalcNorma("'+DateToStr(dm.BeginDate)+'", "'+
         DateTimeToStr(dm.EndDate)+'")', dmMain.quTmp);

      // �������� ���� ��������������
      ExecSQL('execute procedure S_CalcAct("'+DateTimeToStr(dm.BeginDate)+'", "'+
        DateTimeToStr(dm.EndDate)+'",'+IntToStr(dm.User.UserId)+', '+IntToStr(ActId)+')',
        dmMain.quTmp);
      {$else}

      // ������ ����
      ExecSQL('execute procedure G_Norma("'+DateToStr(dm.BeginDate)+'", "'+DateTimeToStr(dm.EndDate)+'")', dmMain.quTmp, False);

      // �������� ���� ��������������
      ExecSQL('execute procedure Create_Protocol_PS('+IntToStr(ActId)+')', dmMain.quTmp, False);

      // �������� ���� �������������� �� ������������
      ExecSQL('execute procedure Create_PArt('+IntToStr(ActId)+')', dmMain.quTmp, False);

      // ��������� ������ � ����
      ExecSQL('execute procedure Create_ActTails('+dmMain.taPListID.AsString+')', dmMain.quTmp);

      // ������ �� ��������� �������
      ExecSQL('execute procedure Create_ProtocolMat('+dmMain.taPListID.AsString+')', dmMain.quTmp);

      Application.ProcessMessages;

      dmMain.quTmp.Transaction.CommitRetaining;

      Screen.Cursor := crDefault;      
      {$endif}
    except
      dmMain.quTmp.Transaction.RollbackRetaining;
      Screen.Cursor := crDefault;
    end;
    {$ifdef OLD_PROTOCOL}
    with dmMain do
      ShowDocForm(TfmProtocol, dmMain.taPList, dmMain.taPListID.AsInteger, dmMain.quTmp);
    {$else}
    ShowDocForm(TfmProtocol_d, dmMain.taPList, dmMain.taPListID.AsInteger, dmMain.quTmp);
    {$endif}
    dmMain.taPList.Refresh;
    RefreshPeriod;
  end;
end;

procedure TfmPList.acDelDocExecute(Sender: TObject);
begin

  if not dm.IsAdm then
    begin
      MessageDlg('��� ��� ������ ������� �������������, �.�. �� �� ��������� ������ ������ ���������������! ', mtInformation, [mbOk], 0);
      eXit;
    end;
  inherited;
  DataSet.Transaction.CommitRetaining;
  dm.InitPeriod;
  RefreshPeriod;
  
end;

procedure TfmPList.acEditDocExecute(Sender: TObject);
begin
  with dmMain do
  begin
    if dmMain.taPListT.AsInteger = 0
      then
        ShowDocForm(TfmProtocol, taPList, taPListID.AsInteger, quTmp)
    else
      ShowDocForm(TfmProtocol_d, taPList, taPListID.AsInteger, quTmp);
    taPList.Refresh;
  end;
  RefreshPeriod;
end;

procedure TfmPList.acPrintDocExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taProtocol_d, dmMain.taPList]);
  dmPrint.ProtocolId := dmMain.taPListID.AsInteger;
  dmPrint.PrintDocumentA(dkProtocol_d, VarArrayOf([VarArrayOf([dmMain.taPListID.AsInteger]),
                VarArrayOf([dmMain.taPListID.AsInteger, 1])]));
end;

procedure TfmPList.acPrintDocUpdate(Sender: TObject);
begin
  acPrintDoc.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmPList.gridDocListOrderBy(Sender: TObject; OrderBy: String);
begin
  OrderBy := StringReplace(OrderBy, 'USERNAME', 'USERID', [rfReplaceAll, rfIgnoreCase]);
  with (DataSet as TpFIBDataSet) do
    SelectSQL.Text := copy(SelectSQL.Text, 1, Pos('ORDER BY', SelectSQL.Text)-1)+OrderBy;
end;

procedure TfmPList.acCheckDataExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmCheckProtocolData, TForm(fmCheckProtocolData));
end;

procedure TfmPList.acCheckDataUpdate(Sender: TObject);
begin
  acCheckData.Enabled := DataSet.Active and (not DataSet.IsEmpty); 
end;

procedure TfmPList.Access;
begin
  with stbrStatus.Panels[0] do
    if dm.IsAdm then Text := 'Adm'
    else if((dm.User.AccProd_d and 8) = 8)then
    begin
      Text := 'View';
      FReadOnly := True;
    end
    else Text := 'Full';
  if dm.IsAdm then eXit;
end;

procedure TfmPList.ac1CExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmProtocolMat, TForm(fmProtocolMat));
end;

procedure TfmPList.ac1CUpdate(Sender: TObject);
begin
  ac1C.Enabled := dmMain.taPList.Active and (not dmMain.taPList.IsEmpty);
end;

procedure TfmPList.acPrintSJExecute(Sender: TObject);
begin
  PostDataSet(dmMain.taPList);
  dmPrint.ProtocolId := dmMain.taPListID.AsInteger;
  dmPrint.PrintDocumentA(dkSJ_d, Null);
end;

procedure TfmPList.acPrintSJUpdate(Sender: TObject);
begin
  acPrintSJ.Enabled := dmMain.taPList.Active and (not dmMain.taPList.IsEmpty);
end;

procedure TfmPList.acDelDocUpdate(Sender: TObject);
begin
  acDelDoc.Enabled := dmMain.taPList.Active and (not dmMain.taPList.IsEmpty) and
      (dmMain.taPListISCLOSE.AsInteger = 0);
end;

procedure TfmPList.edYearChange(Sender: TObject);
begin
  dmMain.DocListYear := edYear.Value;
  ReOpenDataSet(DataSet);
end;

procedure TfmPList.edYearKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : begin
      dmMain.DocListYear := edYear.Value;
      ReOpenDataSet(DataSet);
    end;
  end;

end;

end.
