unit errWhArt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, Grids, DBGridEh,  ImgList, 
  ExtCtrls, ComCtrls, ActnList, DBGridEhGrouping, GridsEh, rxPlacemnt,
  rxSpeedbar;

type
  TfmErrWhArt = class(TfmAncestor)
    gridWhArt: TDBGridEh;
    spitDoIt: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spitDoItClick(Sender: TObject);
  end;

var
  fmErrWhArt: TfmErrWhArt;

implementation

uses MainData, dbUtil, DictData;

{$R *.dfm}

procedure TfmErrWhArt.FormCreate(Sender: TObject);
begin
  inherited;
  Application.ProcessMessages;
  if dmMain.taErrWhArt.Transaction.Active then dmMain.taErrWhArt.Transaction.Commit;
  dmMain.taErrWhArt.Transaction.StartTransaction;
  { TODO : �������� }
  {ExecSQL('execute procedure Build_WhArt_Dep('+IntToStr(-MAXINT)+','+
    IntToStr(MAXINT)+', "01.01.2050")', dmMain.quTmp);}
  dmMain.taErrWhArt.ParamByName('T').AsInteger := 0;
  OpenDataSet(dmMain.taErrWhArt);
end;

procedure TfmErrWhArt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  CloseDataSet(dmMain.taErrWhArt);
  if dmMain.taErrWhArt.Transaction.Active then dmMain.taErrWhArt.Transaction.Commit;
end;

procedure TfmErrWhArt.spitDoItClick(Sender: TObject);
begin
  CloseDataSet(dmMain.taErrWhArt);
  //dmMain.taErrWhArt.Transaction.Commit;
  dmMain.taErrWhArt.ParamByName('T').AsInteger := 1;
  //dmMain.taErrWhArt.Transaction.StartTransaction;
  OpenDataSet(dmMain.taErrWhArt);
  dm.tr.CommitRetaining;
end;

end.
