program Project1;

uses
  ExceptionLog,
  pFIBDatabase,
  Forms,
  SInv_Det_SIList in 'SInv_Det_SIList.pas' {fr_SInv_Det_SIList};

{$R *.res}
var
  fr_SInv_Det_SIList: Tfr_SInv_Det_SIList;
  db: TpFIBDataBase;
  ta: TpFIBTransaction;
begin
  db := TpFIBDataBase.Create(Application);
  ta := TpFIBTransaction.Create(Application);

  with ta do
  begin
    TRParams.Add('read_committed');
    TRParams.Add('rec_version');
    TRParams.Add('nowait');
    DefaultDataBase := db;
  end;

  with db do
  begin
    DefaultTransAction := ta;
    DBName := '192.168.211.111:db/work/production.fdb';
    DBParams.Add('user_name=CDM');
    DBParams.Add('password=cdm');
    DBParams.Add('lc_ctype=WIN1251');
    Open;
  end;

  Application.Initialize;
  Application.CreateForm(Tfr_SInv_Det_SIList, fr_SInv_Det_SIList);

  with fr_SInv_Det_SIList do
  begin
    DataBase := db;
    ShowModal;
  end;

end.
