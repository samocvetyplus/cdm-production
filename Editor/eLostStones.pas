unit eLostStones;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Buttons, Grids, DBGridEh, DBUtil, Editor, ExtCtrls,
  StdCtrls, DBGridEhGrouping, rxSpeedbar, GridsEh;

type
  TfmLostStones = class(TForm)
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    spbrBtn: TSpeedBar;
    SpeedbarSection2: TSpeedbarSection;
    spitAdd1: TSpeedItem;
    spitDel: TSpeedItem;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmLostStones: TfmLostStones;

implementation

uses InvData, eSQ, MainData, DB;

{$R *.dfm}

procedure TfmLostStones.FormCreate(Sender: TObject);
begin
 ReOpenDataSets([dmInv.taArtIns, dmInv.taLostStones]);
end;

procedure TfmLostStones.SpeedButton1Click(Sender: TObject);
begin
   dmMain.SemisW := dmInv.taArtInsWEIGHT.AsFloat;
   dmMain.SemisQ := dmInv.taArtInsQUANTITY.ASInteger;
   if (ShowEditor(TfmeSQ, TfmEditor(fmeSQ),'�����')<>mrOk)then eXit
   else  begin
    if  (dmInv.taArtInsQUANTITY.AsInteger>=dmMain.SemisQ)and(dmInv.taArtInsWEIGHT.AsFloat>=dmMain.SemisW)
         and(dmInv.taArtInsQUANTITY.AsInteger>0)and(dmInv.taArtInsWEIGHT.AsFloat>0)
    then begin
      dmInv.sqlAddLostStones.ParamByName('INVID').AsInteger:=dmInv.taSListINVID.Asinteger;
      dmInv.sqlAddLostStones.ParamByName('ART2ID').AsInteger:=dmInv.taINVItemsART2ID.Asinteger;
      dmInv.sqlAddLostStones.ParamByName('INSID').AsInteger:=dmInv.taArtInsID.Asinteger;
      dmInv.sqlAddLostStones.ParamByName('Q').AsInteger:=dmMain.SemisQ;
      dmInv.sqlAddLostStones.ParamByName('W').AsFloat:=dmMain.SemisW;
      dmInv.sqlAddLostStones.ExecQuery;
      dmInv.sqlAddLostStones.Transaction.CommitRetaining;
     end else showmessage('���������� ���-�� � ��� �� ������ ��������� ���������!')
   end;
   ReOpenDataSet(dmInv.taLostStones);
end;

procedure TfmLostStones.SpeedButton2Click(Sender: TObject);
begin
   dmMain.SemisW := dmInv.taLostStonesWEIGTH.AsFloat;
   dmMain.SemisQ := dmInv.taLostStonesQUANTITY.AsInteger;
   if (ShowEditor(TfmeSQ, TfmEditor(fmeSQ),'�����')<>mrOk)then  eXit
   else begin
      if  (dmInv.taLostStonesQUANTITY.AsInteger>=dmMain.SemisQ)and(dmInv.taLostStonesWEIGTH.AsFloat-dmMain.SemisW>-0.001)
      and(dmMain.SemisQ>0)and(dmMain.SemisW>0)
      then begin
       if dmInv.taLostStones.State in [dsEdit,dsInsert] then dmInv.taLostStones.Post;

       if  (dmInv.taLostStonesQUANTITY.AsInteger=dmMain.SemisQ)and(abs(dmInv.taLostStonesWEIGTH.AsFloat-dmMain.SemisW)<0.001)
       then dmInv.taLostStones.Delete
       else begin
        dmInv.taLostStones.Edit;
        dmInv.taLostStonesQUANTITY.AsInteger:=dmInv.taLostStonesQUANTITY.AsInteger-dmMain.SemisQ;
        dmInv.taLostStonesWEIGTH.AsFloat:=dmInv.taLostStonesWEIGTH.AsFloat-dmMain.SemisW;
        dmInv.taLostStones.Post;
       end;

        dmInv.taLostStones.Transaction.CommitRetaining;
       end else showmessage('�������� ��� ��� ����������!');
    end;
    ReOpenDataSet(dmInv.taLostStones);
end;

procedure TfmLostStones.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_Escape then ModalResult:=mrOk;
end;

procedure TfmLostStones.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
 CloseDataSet(dmInv.taLostStones);
end;

end.
