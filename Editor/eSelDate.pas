unit eSelDate;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh;

type
  TfmSelDate = class(TfmEditor)
    Label1: TLabel;
    edDate: TDBDateTimeEditEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;


function ShowDate(DefaultDate : Variant):boolean;

implementation

{$R *.dfm}

function ShowDate(DefaultDate : Variant):boolean;
var
  fmSelDate: TfmSelDate;
begin
  fmSelDate := TfmSelDate.Create(Application);
  try
    if not VarIsNull(DefaultDate) then fmSelDate.edDate.Value := DefaultDate;
    Result := fmSelDate.ShowModal=mrOk;
  finally
    FreeAndNil(fmSelDate);
  end;
end;

procedure TfmSelDate.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk : vResult := edDate.Value
    else vResult := null;
  end;
  inherited;
end;

end.
