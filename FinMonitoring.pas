unit FinMonitoring;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData,
  cxClasses, dxBar, cxGridLevel, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, FIBDatabase, pFIBDatabase,
  FIBDataSet, pFIBDataSet, ImgList, ExtCtrls, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPSCore, dxPScxCommon, dxPScxGrid6Lnk, FIBQuery,
  pFIBQuery;

type
  TfmFinMonitoring = class(TForm)
    Grid: TcxGrid;
    BarManager: TdxBarManager;
    ToolBar: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    DataSetMaster: TpFIBDataSet;
    Transaction: TpFIBTransaction;
    ImageList: TImageList;
    DataSetMasterCOMPANYID: TFIBIntegerField;
    DataSetMasterCOMPANYNAME: TFIBStringField;
    DataSetMasterINVOICECOST: TFIBBCDField;
    DataSourceMaster: TDataSource;
    Printer: TdxComponentPrinter;
    PrinterLink: TdxGridReportLink;
    cxStyleRepository1: TcxStyleRepository;
    stlRed: TcxStyle;
    stlHighlight: TcxStyle;
    stlDefault: TcxStyle;
    dataSetDetail: TpFIBDataSet;
    dataSetDetailCOMPANYID: TFIBIntegerField;
    dataSetDetailTYPENAME: TFIBStringField;
    dataSetDetailINVOICENUMBER: TFIBIntegerField;
    dataSetDetailINVOICEDATE: TFIBDateTimeField;
    DataSourceDetail: TDataSource;
    dataSetDetailINVOICECOST: TFIBBCDField;
    MasterLevel: TcxGridLevel;
    DetailLevel: TcxGridLevel;
    InsertQuery: TpFIBQuery;
    DataSetMasterINVOICEDATE: TFIBDateTimeField;
    MasterView: TcxGridDBTableView;
    MasterViewCOMPANYID: TcxGridDBColumn;
    MasterViewCOMPANYNAME: TcxGridDBColumn;
    MasterViewINVOICECOST: TcxGridDBColumn;
    DetailView: TcxGridDBTableView;
    DetailViewCOMPANYID: TcxGridDBColumn;
    DetailViewTYPENAME: TcxGridDBColumn;
    DetailViewINVOICENUMBER: TcxGridDBColumn;
    DetailViewINVOICEDATE: TcxGridDBColumn;
    DetailViewINVOICECOST: TcxGridDBColumn;
    PrinterLink2: TdxGridReportLink;
    dxBarPopupMenu: TdxBarPopupMenu;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    procedure FormCreate(Sender: TObject);
    procedure dxBarLargeButton2Click(Sender: TObject);
    procedure dataSetDetailBeforeOpen(DataSet: TDataSet);
    procedure DataSetMasterBeforeOpen(DataSet: TDataSet);
    procedure MasterViewDataControllerDetailExpanding(
      ADataController: TcxCustomDataController; ARecordIndex: Integer;
      var AAllow: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure dxBarButton2Click(Sender: TObject);
    procedure dxBarButton1Click(Sender: TObject);
  private
    constructor Create(AOwner: TComponent; InvType: Integer);
    procedure UpdateStartDates;
  public
    Itype: Integer;
    class procedure Execute(IType: Integer);
  end;

var
  fmFinMonitoring: TfmFinMonitoring;

implementation

{$R *.dfm}
uses DictData, uUtils;


procedure TfmFinMonitoring.dataSetDetailBeforeOpen(DataSet: TDataSet);
begin
  (DataSet as tpFIBDataSet).ParamByName('IType').AsInteger := IType;
end;

procedure TfmFinMonitoring.DataSetMasterBeforeOpen(DataSet: TDataSet);
begin
  (DataSet as tpFIBDataSet).ParamByName('IType').AsInteger := IType;
end;

procedure TfmFinMonitoring.dxBarButton1Click(Sender: TObject);
var
  Link: TdxgridReportLink;
begin
  Link := TdxGridReportLink(Printer.LinkByName('PrinterLink'));
  Link.ReportTitle.Text := '����� ��� ���. �����������';
  Link.ReportDocument.Caption := Link.ReportTitle.Text;
  Link.ReportDocument.CreationDate := Date;
  Link.Preview;
end;

procedure TfmFinMonitoring.dxBarButton2Click(Sender: TObject);
var
  Link: TdxgridReportLink;
begin
  Link := TdxGridReportLink(Printer.LinkByName('PrinterLink2'));
  Link.ReportDocument.CreationDate := Date;
  Link.Preview;
end;

procedure tfmFinMonitoring.UpdateStartDates;
var
  BookMark: Pointer;
  Count, i: Integer;
begin
  BookMark := DataSetMaster.GetBookmark;
  Count := DataSetMaster.RecordCount;
  DataSetMaster.First;
  for i := 1 to Count do
    begin
      try
        try
          InsertQuery.ParamByName('SupID').AsInteger := DataSetMasterCOMPANYID.AsInteger;
          InsertQuery.ParamByName('Date').AsDateTime := DataSetMasterINVOICEDATE.AsDateTime;
          InsertQuery.ParamByName('IType').AsInteger := IType;
          InsertQuery.ExecQuery;
        except
          On E:Exception do
            begin
              ShowMessage(E.Message);
              InsertQuery.Transaction.RollbackRetaining;
            end;
        end;
      finally
//        InsertQuery.Transaction.CommitRetaining;
      end;
      DataSetMaster.Next;
    end;
  DataSetMaster.GotoBookmark(Bookmark);
end;

procedure TfmFinMonitoring.dxBarLargeButton2Click(Sender: TObject);
var Answer: TModalResult;
begin
  Answer := QuestionYesNoCancelMessage('�������� ���� ������ ������� ��� �����������,' + #13#10
                                     + '������ ������� �������� 600 000 ���?');
 if Answer = mrYes then
   begin
     UpdateStartDates;
     Close;
   end
 else
   if Answer = mrNo then
     begin
       DataSetMaster.Close;
       DataSetDetail.Close;
       Close;
     end
   else
     Exit;
end;

procedure TfmFinMonitoring.FormCreate(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  DataSetMaster.Open;  
  DataSetDetail.Open;
  Screen.Cursor := crDefault;
end;

procedure TfmFinMonitoring.FormDestroy(Sender: TObject);
begin
  DatasetDetail.Close;
  dataSetMaster.Close;
  Transaction.Commit;
end;

procedure TfmFinMonitoring.MasterViewDataControllerDetailExpanding(
  ADataController: TcxCustomDataController; ARecordIndex: Integer;
  var AAllow: Boolean);
begin
    ADataController.FocusSelectedRow(ARecordIndex);
end;

constructor TfmFinMonitoring.Create(AOwner: TComponent; InvType: Integer);
begin
  inherited Create(AOwner);
  Itype := InvType;
  Self.Caption := '���������� ���������� ';
  case IType of
    2: Self.Caption := Self.Caption + '(�������)';
    3: Self.Caption := Self.Caption + '(��������)';
   -1: Self.Caption := Self.Caption + '(������)';
  end;
end;


class procedure TfmFinMonitoring.Execute(IType: Integer);
begin
  fmFinMonitoring := nil;
  try
    fmFinMonitoring := TfmFinMonitoring.Create(Application, IType);
    fmFinMonitoring.ShowModal;
  finally
    FreeAndNil(fmFinMonitoring);
  end;
end;

end.
