{***********************************************}
{  ������ ��������� ����������� � ������������  }
{  �� ����� ������� ���������                   }
{                                               }
{  Copyrigth (C) 2002-2003 basile for CDM       }
{  v0.50                                        }
{  created 24.12.2003                           }
{  last updated 25.06.2004                      }
{***********************************************}

unit ASList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, Menus,  ImgList,  StdCtrls,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, ComCtrls, Buttons, db, DBGridEh, ActnList,
  TB2Item, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmASList = class(TfmListAncestor)
    ppDepFrom: TPopupMenu;
    mnitAllDepFrom: TMenuItem;
    N2: TMenuItem;
    lbDepFrom: TLabel;
    SpeedItem1: TSpeedItem;
    TBSeparatorItem2: TTBSeparatorItem;
    acShowID: TAction;
    acFillSortInd: TAction;
    tbsSellInv: TTBSubmenuItem;
    acLogic: TAction;
    SpeedButton1: TSpeedButton;
    TBItem5: TTBItem;
    acPhoto: TAction;
    TBItem6: TTBItem;
    acPrintInvUID: TAction;
    procedure FormCreate(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure grGetCellParams(Sender: TObject; Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintDocExecute(Sender: TObject);
    procedure acPrintDocUpdate(Sender: TObject);
    procedure acShowIDExecute(Sender: TObject);
    procedure acFillSortIndUpdate(Sender: TObject);
    procedure acFillSortIndExecute(Sender: TObject);
    procedure acLogicExecute(Sender: TObject);
    procedure acLogicUpdate(Sender: TObject);
    procedure acPhotoExecute(Sender: TObject);
    procedure acPhotoUpdate(Sender: TObject);
    procedure acPrintInvUIDExecute(Sender: TObject);
    procedure acPrintInvUIDUpdate(Sender: TObject);
  protected
    procedure DepFromClick(Sender : TObject);
    procedure DepClick(Sender : TObject); override; // ����
    procedure acCreateProdInvExecute(Sender: TObject);
    procedure acCreateProdInvUpdate(Sender: TObject);
    procedure Access;
    procedure SetReadOnly(ReadOnly: boolean);
  end;

var
  fmASList: TfmASList;

implementation

uses MainData, DictData, dbUtil, SInvProd, fmUtils, InvData, PrintData,
  INVCopy, MsgDialog, ProductionConsts;

{$R *.dfm}

procedure TfmASList.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  inherited;
end;

procedure TfmASList.DepFromClick(Sender: TObject);
begin
  dm.CurrFromDep := TMenuItem(Sender).Tag;
  lbDepFrom.Caption := TMenuItem(Sender).Caption;
  ReOpenDataSet(DataSet);
end;


procedure TfmASList.FormCreate(Sender: TObject);
var
  i, j1, j2 : integer;
  Item : TMenuItem;
  b : boolean;
  ac : TAction;
  it : TTBItem;
  ProducersField: TField;
  Column: TColumnEh;
begin
  dm.CurrDep := -1;
  mnitAllDepFrom.OnClick := DepFromClick;
  lbDepFrom.Caption := mnitAllDepFrom.Caption;
  //���������� PopupMenu �� FDepInfo;
  j1 := 0; j2 := 0;
  b := False;
  for i:=0 to Pred(dm.CountDep) do
  begin
    (* �����-����������� *)
    if (dm.DepInfo[i].DepTypes and $2)=$2 then
    begin
      Item := TMenuItem.Create(ppDepFrom);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      if not b then b := dm.DepDef = dm.DepInfo[i].DepId;
      Item.OnClick := DepFromClick;
      Item.RadioItem := True;
      if Item.Tag = dm.DepDef then lbDepFrom.Caption := dm.DepInfo[i].SName;
      inc(j1);
      if j1 > 30 then
      begin
        Item.Break := mbBreak;
        j1 := 0;
      end;
      ppDepFrom.Items.Add(Item);
    end;
    (* �����-���������� *)
    if (dm.DepInfo[i].DepTypes and $4)=$4 then
    begin
      Item := TMenuItem.Create(ppDep);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepClick;
      Item.RadioItem := True;
      inc(j2);
      if j2>30 then
      begin
        Item.Break := mbBreak;
        j2 := 0;
      end;
      ppDep.Items.Add(Item);
    end
  end;

  if b then dm.CurrFromDep := dm.DepDef
  else dm.CurrFromDep := -1;

  gridDocList.FieldColumns['INVID'].Visible := False;

  // ���������� ���� ��� ������� ����������
  ReOpenDataSet(dm.taCompLogin);
  
  while not dm.taCompLogin.Eof do
  begin
    ac := TAction.Create(acAEvent);
    ac.OnUpdate := acCreateProdInvUpdate;
    ac.OnExecute := acCreateProdInvExecute;
    ac.Caption := dm.taCompLoginNAME.AsString;
    ac.Tag := dm.taCompLoginCOMPID.AsInteger;
    it := TTBItem.Create(ppDoc);
    it.Action := ac;
    it.Caption := dm.taCompLoginNAME.AsString;
    it.Tag := dm.taCompLoginCOMPID.AsInteger;
    tbsSellInv.Add(it);
    dm.taCompLogin.Next;
  end;

  CloseDataSet(dm.taCompLogin);

  inherited;

  Access;

end;

procedure TfmASList.grGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if dmMain.taASListISCLOSE.AsInteger=0 then Background:=clInfoBk
  else Background:=clBtnFace;
end;

procedure TfmASList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
var
  FieldName: String;
  AnotherProducer: boolean;
begin

  if dmMain.taASListISCLOSE.AsInteger=0 then Background:=clWindow else Background:=clBtnFace;

  FieldName := Column.FieldName;

  AnotherProducer := dmMain.taASListPRODUCERID.AsInteger <> 1;

  if (FieldName = 'DOCNO') then
  begin

    if not dmMain.taASListCOLOR.IsNull then
    begin
      Background := dmMain.taASListCOLOR.AsInteger;
    end;

    if AnotherProducer then Background := clAqua;

  end;
  
end;

procedure TfmASList.acAddDocExecute(Sender: TObject);
begin
  if(FCurrDep = -1)then
  begin
    MessageDialogA(rsNotDefineDepFrom, mtInformation);
    Abort;
  end;
  if dm.CurrDep = -1 then
  begin
    MessageDialogA(rsNotDefineDepTo, mtInformation);
    Abort;
  end;
  inherited;
  ShowAndFreeForm(TfmSInvProd, TForm(fmSInvProd));
  RefreshDataSet(dmMain.taASList);
end;

procedure TfmASList.acEditDocExecute(Sender: TObject);
begin
  if dmMain.taASList.IsEmpty then eXit;
  with dmMain do
    ShowAndFreeForm(TfmSInvProd, TForm(fmSInvProd));
  RefreshDataSet(dmMain.taASList);
end;

procedure TfmASList.acCreateProdInvExecute(Sender: TObject);
var
  c : TCopyInvInfo;
begin
  c.DefaultComp := -1;
  c.SelfCompId := (Sender as TAction).Tag;
  c.CanChangeComp := True;
  c.DefaultDep := -1;
  c.CanChangeDep := True;
  c.DepComp := False;
  c.BetweenOrg := dm.User.SelfCompId <> (Sender as TAction).Tag;
  c.CopyInvId := dmMain.taASListINVID.AsInteger;
  c.CopyTypeFrom := 4;
  c.CopyType := 2;
  c.FromDep := -1;
  c.NDSId := -1;
  ShowInvCopy(c);
end;

procedure TfmASList.acCreateProdInvUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not FReadOnly) and DataSet.Active and (not DataSet.IsEmpty); 
end;

procedure TfmASList.acPrintDocExecute(Sender: TObject);
begin
  dmInv.FCurrINVID := dmMain.taASListINVID.AsInteger;
  dmPrint.PrintDocumentA(dkIn, VarArrayOf([VarArrayOf([dmMain.taASListINVID.AsInteger]),VarArrayOf([dmMain.taASListINVID.AsInteger])]));
end;

procedure TfmASList.acPrintDocUpdate(Sender: TObject);
begin
  acPrintDoc.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmASList.acShowIDExecute(Sender: TObject);
begin
  gridDocList.FieldColumns['INVID'].Visible := not gridDocList.FieldColumns['INVID'].Visible;
end;

procedure TfmASList.acFillSortIndUpdate(Sender: TObject);
begin
  acFillSortInd.Enabled := dm.IsAdm and DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmASList.acFillSortIndExecute(Sender: TObject);
begin
  ExecSQL('execute procedure FILL_SINVPROD_SORTIND('+dmMain.taASListINVID.AsString+')', dmMain.quTmp);
end;

procedure TfmASList.acLogicExecute(Sender: TObject);
begin
  //
end;

procedure TfmASList.acLogicUpdate(Sender: TObject);
begin
  tbsSellInv.Enabled := dmMain.taASList.Active and (not dmMain.taASList.IsEmpty);
end;

procedure TfmASList.acPhotoExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkInvArtPhoto, VarArrayOf([VarArrayOf([dmMain.taASListINVID.AsInteger])]));
end;

procedure TfmASList.acPhotoUpdate(Sender: TObject);
begin
  acPhoto.Enabled := (DataSet.Active) and (not DataSet.IsEmpty);
end;

procedure TfmASList.acPrintInvUIDExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkInvUID, VarArrayOf([VarArrayOf([dmInv.taSListINVID.AsInteger]), VarArrayOf([dmInv.taSListINVID.AsInteger])]) );
end;

procedure TfmASList.acPrintInvUIDUpdate(Sender: TObject);
begin
  acPrintInvUID.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmASList.Access;
begin
  if not (dm.IsAdm or (dm.User.Profile = 2) or (dm.User.Profile = 5)) then
  begin
    SetReadOnly(True);
  end;
end;

procedure TfmASList.SetReadOnly(ReadOnly: boolean);
begin
  spitAdd.Enabled := not ReadOnly;
  spitDel.Enabled := not ReadOnly;
  GridDocList.ReadOnly := ReadOnly;
end;

end.
