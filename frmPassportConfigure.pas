unit frmPassportConfigure;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridBandedTableView,
  cxGridDBBandedTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxDBExtLookupComboBox, cxContainer,
  cxTextEdit, cxDBEdit, DBActns, ActnList, dxBar, dxBarExtItems,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ExtCtrls, cxButtonEdit,
  DBClient, cxLookAndFeels;

type
  TDialogPassportConfigure = class(TForm)
    GridGroupItemsView: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    GridGroupItemsViewOperationTitle: TcxGridDBColumn;
    GridGroupItemsViewPlaceOfStorageTitle: TcxGridDBColumn;
    GridGroupsView: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    BarManager: TdxBarManager;
    GroupsDockControl: TdxBarDockControl;
    dxBarLargeButton1: TdxBarLargeButton;
    GroupItemsDockControl: TdxBarDockControl;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    ActionList: TActionList;
    DataSetInsertGroup: TDataSetInsert;
    DataSetDeleteGroup: TDataSetDelete;
    DataSetDeleteGroupItem: TDataSetDelete;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    DataSetPostGroup: TDataSetPost;
    DataSetCancelGroup: TDataSetCancel;
    DataSetPostGroupItem: TDataSetPost;
    DataSetCancelGroupItem: TDataSetCancel;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    GridVariants: TcxGrid;
    GridVariantsView: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    VariantsDockControl: TdxBarDockControl;
    dxBarLargeButton10: TdxBarLargeButton;
    ActionVariant: TAction;
    DataSetInsertGroupItem: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    ButtonClose: TdxBarLargeButton;
    DeleteGroup: TAction;
    DeleteGroupItem: TAction;
    InsertGroupItem: TAction;
    GridGroupsViewOperationTitle: TcxGridDBColumn;
    GridGroupsViewPlaceOfStorageTitle: TcxGridDBColumn;
    dxBarButton1: TdxBarButton;
    dxBarLargeButton12: TdxBarLargeButton;
    ReturnGroup: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    procedure ActionVariantUpdate(Sender: TObject);
    procedure ActionVariantExecute(Sender: TObject);
    procedure DataSetInsertGroupItemUpdate(Sender: TObject);
    procedure DataSetInsertGroupItemExecute(Sender: TObject);
    procedure ButtonCloseClick(Sender: TObject);
    procedure DeleteGroupItemUpdate(Sender: TObject);
    procedure DeleteGroupUpdate(Sender: TObject);
    procedure DeleteGroupExecute(Sender: TObject);
    procedure DeleteGroupItemExecute(Sender: TObject);
    procedure InsertGroupItemUpdate(Sender: TObject);
    procedure InsertGroupItemExecute(Sender: TObject);
    procedure ReturnGroupUpdate(Sender: TObject);
    procedure ReturnGroupExecute(Sender: TObject);
  private

  public
    function Execute: Boolean;
  end;

var
  DialogPassportConfigure: TDialogPassportConfigure;

implementation

uses frmAffiangePassport;

{$R *.dfm}

procedure TDialogPassportConfigure.ActionVariantExecute(Sender: TObject);
var
  Source: TDataSet;
  Target: TDataSet;
begin
  try
    DialogAffinage.Flag := 1;
    Source := DialogAffinage.GroupItemVariants;
    Target := DialogAffinage.GroupItems;
    Target.Append;
    Target.FieldByName('Operation$ID').AsInteger := Source.FieldByName('Operation$ID').AsInteger;
    Target.FieldByName('Place$Of$Storage$ID').AsInteger := Source.FieldByName('Place$Of$Storage$ID').AsInteger;
    Target.Post;
    Source.Delete;
  finally
    DialogAffinage.Flag := 0;
  end;  
end;

procedure TDialogPassportConfigure.DataSetInsertGroupItemUpdate(Sender: TObject);
var
  Groups: TDataSet;
begin
  Groups := DialogAffinage.Groups;
  DataSetInsertGroupItem.Enabled := Groups.Active and (Groups.RecordCount <> 0)
end;



procedure TDialogPassportConfigure.DataSetInsertGroupItemExecute(
  Sender: TObject);
begin
  DialogAffinage.GroupItems.Append;
end;

procedure TDialogPassportConfigure.ButtonCloseClick(Sender: TObject);
begin
  ModalResult := mrOK;
end;

procedure TDialogPassportConfigure.DeleteGroupUpdate(Sender: TObject);
var
  Groups: TClientDataSet;
  Field: TField;
begin
  Groups := TClientDataSet(GridGroupsView.DataController.DataSource.DataSet);
  Field := Groups.FieldByName('WEIGHT$Remainder$IN');
  DeleteGroup.Enabled := Groups.Active and not Groups.IsEmpty and (Groups.State = dsBrowse) and (Field.AsFloat = 0);
end;


function TDialogPassportConfigure.Execute: Boolean;
begin
  Result := ShowModal = mrOK;
end;

procedure TDialogPassportConfigure.InsertGroupItemUpdate(Sender: TObject);
var
  Groups: TClientDataSet;
  GroupItems: TClientDataSet;
  Field: TField;
  Enabled: Boolean;
begin
  Groups := TClientDataSet(GridGroupsView.DataController.DataSource.DataSet);
  GroupItems := TClientDataSet(GridGroupItemsView.DataController.DataSource.DataSet);

  Field := Groups.FieldByName('WEIGHT$Remainder$IN');
  Enabled := Groups.Active and not Groups.IsEmpty and (Groups.State = dsBrowse);
  if Enabled then Enabled := GroupItems.Active and (GroupItems.State = dsBrowse) and (Field.AsFloat = 0);
  InsertGroupItem.Enabled := Enabled;
end;

procedure TDialogPassportConfigure.ReturnGroupExecute(Sender: TObject);
var
  MainTarget: TDataSet;
  Source: TDataSet;
  Target: TDataSet;
begin
  try
    DialogAffinage.Flag := 1;
    Source := DialogAffinage.GroupItemVariants;
    Target := DialogAffinage.GroupItems;
    MainTarget := DialogAffinage.Groups;
    MainTarget.Append;
    MainTarget.FieldByName('Operation$ID').AsInteger := Source.FieldByName('Operation$ID').AsInteger;
    MainTarget.FieldByName('Place$Of$Storage$ID').AsInteger := Source.FieldByName('Place$Of$Storage$ID').AsInteger;
    MainTarget.Post;
    Target.Append;
    Target.FieldByName('Operation$ID').AsInteger := Source.FieldByName('Operation$ID').AsInteger;
    Target.FieldByName('Place$Of$Storage$ID').AsInteger := Source.FieldByName('Place$Of$Storage$ID').AsInteger;
    Target.Post;
    //Source.Edit;
    Source.Delete;
    //Source.Post;
  finally
    DialogAffinage.Flag := 0;
  end;
end;

procedure TDialogPassportConfigure.ReturnGroupUpdate(Sender: TObject);
var
  Groups: TClientDataSet;
  GroupItems: TClientDataSet;
  Variants: TClientDataSet;
  Enabled: Boolean;
begin
  Groups := TClientDataSet(GridGroupsView.DataController.DataSource.DataSet);
  GroupItems := TClientDataSet(GridGroupItemsView.DataController.DataSource.DataSet);
  Variants := TClientDataSet(GridVariantsView.DataController.DataSource.DataSet);
  Enabled := Groups.Active and (Groups.State = dsBrowse);
  if Enabled then Enabled := GroupItems.Active and (GroupItems.State = dsBrowse);
  if Enabled then Enabled := Variants.Active and not Variants.IsEmpty;
  ReturnGroup.Enabled := Enabled;
end;

procedure TDialogPassportConfigure.DeleteGroupItemUpdate(Sender: TObject);
var
  Groups: TClientDataSet;
  GroupItems: TClientDataSet;
  Field: TField;
begin
  Groups := TClientDataSet(GridGroupsView.DataController.DataSource.DataSet);
  GroupItems := TClientDataSet(GridGroupItemsView.DataController.DataSource.DataSet);

  Field := Groups.FieldByName('WEIGHT$Remainder$IN');
  DeleteGroupItem.Enabled := GroupItems.Active and not GroupItems.IsEmpty and (GroupItems.State = dsBrowse) and (Field.AsFloat = 0);
end;

procedure TDialogPassportConfigure.ActionVariantUpdate(Sender: TObject);
var
  Groups: TClientDataSet;
  GroupItems: TClientDataSet;
  Variants: TClientDataSet;
  Field: TField;
  Enabled: Boolean;
begin
  Groups := TClientDataSet(GridGroupsView.DataController.DataSource.DataSet);
  GroupItems := TClientDataSet(GridGroupItemsView.DataController.DataSource.DataSet);
  Variants := TClientDataSet(GridVariantsView.DataController.DataSource.DataSet);
  Field := Groups.FieldByName('WEIGHT$Remainder$IN');
  Enabled := Groups.Active and not Groups.IsEmpty and (Groups.State = dsBrowse) and (Field.AsFloat = 0);
  if Enabled then Enabled := GroupItems.Active and (GroupItems.State = dsBrowse);
  if Enabled then Enabled := Variants.Active and not Variants.IsEmpty;
  ActionVariant.Enabled := Enabled;
end;

procedure TDialogPassportConfigure.DeleteGroupExecute(Sender: TObject);
var
  Groups: TClientDataSet;
  GroupItems: TClientDataSet;
  AfterDelete: TDataSetNotifyEvent;
begin
  Groups := TClientDataSet(GridGroupsView.DataController.DataSource.DataSet);
  GroupItems := TClientDataSet(GridGroupItemsView.DataController.DataSource.DataSet);
  AfterDelete := GroupItems.AfterDelete;
  GroupItems.AfterDelete := nil;
  GroupItems.First;
  while not GroupItems.Eof do
  GroupItems.Delete;
  Groups.Delete;
  GroupItems.AfterDelete := AfterDelete;
  DialogAffinage.CalculateGroupVariants;
end;

procedure TDialogPassportConfigure.DeleteGroupItemExecute(Sender: TObject);
var
  GroupItems: TClientDataSet;
begin
  GroupItems := TClientDataSet(GridGroupItemsView.DataController.DataSource.DataSet);
  GroupItems.Delete;
end;


procedure TDialogPassportConfigure.InsertGroupItemExecute(Sender: TObject);
var
  GroupItems: TClientDataSet;
begin
  GroupItems := TClientDataSet(GridGroupItemsView.DataController.DataSource.DataSet);
  GroupItems.Append;
end;

end.
