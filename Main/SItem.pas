unit SItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls, ComCtrls,
  Grids, DBGrids, RXDBCtrl, StdCtrls, DBCtrls, db,
  DBGridEh, ActnList, DBGridEhGrouping, GridsEh;

type
  TfmSItem = class(TfmAncestor)
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;
    spitTag: TSpeedItem;
    txtQ: TDBText;
    txtW: TDBText;
    grSItem: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure grSItemGetCellParams(Sender: TObject; Field: TField;
      AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure grSItemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure grSItemKeyPress(Sender: TObject; var Key: Char);
    procedure spitAddClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spitDelClick(Sender: TObject);
  private
    { Private declarations }
  public
  end;

var
  fmSItem: TfmSItem;

implementation

uses MainData, dbUtil, InvData, DictData, MsgDialog;

{$R *.dfm}

procedure TfmSItem.FormCreate(Sender: TObject);
begin
  OpenDataSet(dmInv.taSItem);
  grSItem.SelectedIndex:=2;
end;

procedure TfmSItem.grSItemGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if(Field.FieldName='RecNo') or (Field.FieldName='UID') then Background := clMoneyGreen;
end;

procedure TfmSItem.grSItemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var i, si: integer;

begin
  case Key of
    VK_RETURN: if Shift=[ssCtrl] then Close
               else if Shift=[] then
               with dmINV do
               begin
                 PostDataSets([taSItem]);
                 with taSItem do
                 try
                   DisableControls;
                   si := grSItem.SelectedIndex;
                   i := taSItemSItemId.AsInteger;
                   Active:=False;
                   SelectSQL[SelectSQL.Count-1]:='ORDER BY SZ';
                   Active:=True;
                   Locate('SITEMID', i, []);
                   grSItem.SelectedIndex:=si;
                 finally
                   EnableControls;
                 end;
              end;
//   VK_INSERT: dmINV.FCurrUID := dm.GetId(22);
//   VK_DOWN: dmINV.FCurrUID := dm.GetId(22);
  end;
end;

procedure TfmSItem.grSItemKeyPress(Sender: TObject; var Key: Char);
begin
  if grSItem.SelectedField.FieldName='SZ' then
    if NOT (Key in ['0'..'9', '.' ,'-', #8]) then SysUtils.Abort;
end;

procedure TfmSItem.spitAddClick(Sender: TObject);
var i:integer;
begin
 if dmINV.taSListISCLOSE.ASinteger=1 then
 begin
  ShowMessage('������ �������� �������� ���������!');
  SysUtils.Abort;
 end;
//  inherited;
  with dmInv, taSItem do
    begin
      FCurrUID := dm.GetId(22);
      Append;
//      taSItemSS.AsString:='-';
      with grSItem do
        begin
          i:=0;
          while Columns[i].Field.FieldName<>'W' do Inc(i);
          SelectedIndex:=i;
        end
    end;
end;

procedure TfmSItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
   with dmInv do
    begin
      PostDataSet(taSItem);
      dmInv.Q:=dmInv.taSItem.RecordCount;
      CloseDataSets([taSItem]);
//      ReOpenDataSets([ taINVItems]);
//      WChar:=#0;
    end;
end;

procedure TfmSItem.spitDelClick(Sender: TObject);
begin
   if MessageDialog('������� ������?', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
  if dmINV.taSItem.RecordCount>0 then
  dmINV.taSItem.Delete;
  ReopenDataSets([dmINV.taSItem]);
end;

end.                                        
