unit GODEXIntf;

interface

uses Windows;

const
  GODEX_EZLib = 'ez2000.dll';

  function GODEX_isready : BOOL; stdcall; external GODEX_EZLib name 'isready';
  
  (* 0=LPT1,
     1=COM1,
     2=COM2,
     3=COM3,
     4=COM4,
     5=LPT2
  *)
  procedure GODEX_openport(port:PChar); stdcall; external GODEX_EZLib name 'openport';
  procedure GODEX_closeport; stdcall; external GODEX_EZLib name 'closeport';

  (* a - label size setting (in mm)
     b - darkness (0-19)
     c - printing speed (1-3)
     d - label mode: 0 - label with gap, 1 - plain paper, 2 - black mark label
     e - label gap (in mm)
     f - black top for black mark label
  *)
  procedure GODEX_setup(a, b, c, d, e, f:Integer); stdcall; external GODEX_EZLib name 'setup';
  procedure GODEX_sendcommand(command:PChar); stdcall; external GODEX_EZLib name 'sendcommand';

  (* filename - filename of image file
     image_name - name to stand for the image file to be recalled by Y
     image_type - image file type (example: pcx, bmp)
  *)
  procedure GODEX_intloadimage(filename, image_name, image_type:PChar); stdcall; external GODEX_EZLib name 'intloadimage';
  procedure GODEX_extloadimage(filename, image_name, image_type:PChar); stdcall; external GODEX_EZLib name 'extloadimage';

  (* print TrueType Windows font
    x - left-upper horizontal pos (dots)
    y - left-upper vertical pos (dots)
    b - height of the text
    c - name of the font
    d - data string
  *)
  procedure GODEX_ecTextOut(x:Integer; y:Integer; b:Integer; c:PChar; d:PChar); stdcall; external GODEX_EZLib name 'ecTextOut';

  (* print TrueType Windows font with orientation setting
    x,y,b,c,d - ��. ����
    e - width of the text
    f - specifies the weight of the font in range 0 through 100. For example, 40 is normal and 70 is bold
    g - set the rotation of the output text
  *)
  function GODEX_ecTextOutR(x,y,b:integer; c,d:PChar; e,f,g:Integer):BOOL; stdcall; external GODEX_EZLib name 'ecTextOutR';

  (* Download the TrueType fonts with orientation setting as a bitmap to be recalled by ^Y command
    b - height of the text
    c - name of the font
    d - data string
    e - width of the text
    f - specifies the weight of the font in the range 0 through 100. For example, 40 is normal and 70 is bold
    g - set the rotation of the output text
    font_name - ID name of the downloaded font to be recalled by Y command
  *)
  function GODEX_ecTextDownLoad(b:integer; c,d:PChar; e,f,g:integer; font_name:PChar):BOOL; stdcall; external GODEX_EZLib name 'ecTextDownLoad';

  procedure GODEX_setbaudrate(n:integer);stdcall; external GODEX_EZLib name 'setbaudrate';

  (* Print multi-tone image (format BMP and JPG) directly Halftone will be applied to the image
     x - left-upper horizontal pos (dots)
     y - left-upper vertical pos (dots)
     filename - the file name of the image support BMP and JPG file type
     degree (0 - no rotation, 90 rotate 90degree)
  *)
  function GODEX_putimage(x,y:integer; filename:PChar; degree:integer):BOOL;stdcall; external GODEX_EZLib name 'putimage';

  (* Download multi-tone image (format BMP and JPG) with orientation setting as bitmap to be recalled by ^Y command
     filename - the file name of the image support BMP and JPG file type
     degree - (0 - no rotation, 90 rotate 90degree)
     name - ID name to stand for the downloaded image file to be recalled by Y command
  *)
  function GODEX_downloadimage(filename:PChar;degree:integer;name:PChar):BOOL; stdcall; external GODEX_EZLib name 'downloadimage';


implementation

end.
