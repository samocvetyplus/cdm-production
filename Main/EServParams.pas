unit EServParams;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList,  ImgList,  Grids,
  DBGridEh, Buttons, ExtCtrls, ComCtrls, DBGridEhGrouping, rxPlacemnt,
  rxSpeedbar, GridsEh;

type
  TfmEServParams = class(TfmDictAncestor)
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    FDSMatActive : boolean;
  end;

var
  fmEServParams: TfmEServParams;

implementation

uses DictData, InvData, dbUtil;

{$R *.dfm}

procedure TfmEServParams.FormCreate(Sender: TObject);
begin
  gridDict.DataSource := dmInv.dsrEServParams;
  FDSMatActive := dm.taMat.Active;
  if not FDSMatActive then dm.taMat.Open;
  inherited;
end;


procedure TfmEServParams.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if FDSMatActive then CloseDataSet(dm.taMat);
end;

end.


 