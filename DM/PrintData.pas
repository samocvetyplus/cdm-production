unit PrintData;
//������� ���������� ��� ������
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  pFIBDatabase, FR_DSet, FR_DBSet, FR_Class, Db, pFIBDataSet, DbGrids,
  pFIBQuery, FR_Desgn, FR_E_RTF, FR_E_TXT, FR_E_CSV, FIBDataSet,
  FIBQuery, FIBDatabase, ZRPrntr, bsUtils, FR_View, FR_BarC, ProductionConsts,
  FR_Rich, frOLEExl, frXMLExl, frRtfExp, MsgDialog, frxDesgn, frxBarcode,
  frxClass, frxDBSet, DBClient,
  WinSpool, Printers, CommDlg, StdCtrls, frxExportODF, frxExportPDF;

type
   TDocKind = (
              dkInv16In,  // ��������� ��-16 �� ������
              dkInv16Out, // ��������� ��-16 �� �����
              dkInv16InAssort, // ��������� ��-16 �� ������ � �������������
              dkInv16OutAssort, // ��������� ��-16 �� ����� � �������������
              dkInv16In_Preview, // ��������������� ��������� ��-16 �� ������
              dkInv16Out_Preview, // ��������������� ��������� ��-16 �� �����
              //dkJ, // ��������� ��������� �������� ����������� �������� ��
                  // ����������������������� �����
              //dkJGroup,  // ��������� ��������� �������� ���������� ����� ������
              //dkJGroup_Gold,
              dkActE, // ��� ��������
               // �� ������������ ����������� ��������������
              dkOb, // �������������
              dkProtocol, // �������� ��������������
              dkZp, // ����� � ���������� �����
              dkZpTotal, // ���������� ����� � �/�
              dkIn, // ��������� ���������
              dkSell_Invoice, // �������� ���������
              dkSell_Facture, // ����-�������
              dkSell_Chit, // ����
              dkSellRet,
              dkRet,
              dkACTRePrice,
              dkJournalOrder, // ������-�����
              dkJournalOrder2, // ������-�����
              dkWhAppl,  // ��������� ����� ��������
              dkAInv, // ��������� ������������
              dkProdAppl, // ������ �� ������������
              dkProdAnaliz, // ������ ������ �� ������
              dkWHSemis_Transfer, // ����������� ��������������
              dkWhArt, // ��������� ������ ������������
              dkSIList_Stone, // ����������� ���������� (�����)
              dkSIList_Metall, // ����������� ���������� (������)
              dkWhS,     // ����� ��������������
              dkProtocol_d, // �������� ��������������
              dkOb_d,       // ��������������
              dkTmpWhS, // ��������� ����� ����������
              dkSJ_d, // ����� � �������� �������-������������ ���������
              dkSoel, //������ ����������
              dkSemisReport, // ����� � �������� ����������
              dkVSheet, // ������������ ���������
              dkProtocol_PS, // ������������������ �����
              dkDictArt,  // ��������
              dkAffS_Inv, // ��������� ������� �������� �����
              dkAffL_Inv, // �������� ������� �������� ����
              dkLostStones, // ��� ������ ������
              dkBulkAct, // ��� ������ ����������� ������
              dkKolie, // �� �������
              dkInvReport, // ����� �� ���������� ��������� �� ������,
              djSpft, // ����� �� ���������� ���������,
              dkProdReport, //������ �� ��������
              dkOperOrder, // ������ �� ���������
              dkAssortDet, //����������� �� ������������
              dkArtIns, // ������� ��������
              dkStoreByItems, // ����� �� ��������
              dkStoneInInv, // ���������� � ����. ������� � ��������� �� ������ ������� ���������
              dkTag, // ����� �� ��������� ��������
              dkTag2,
              dkSemisInInv, //
              dkAff_Order, // ��������� ����� ��� ��������
              dkArtDiamond, // ������� ����������� ��������
              dkZpAssort, // ����������� ��������� ��������� �� ��������
              dkDetailArt2Ins, // ����������� ������������� ���2 �� ������
              dkVSheetAssort, // ������ ������������ ��� ������������ ���������
              dkSIList_ActSemisGet, // ��� �������-�������� ������������ ���������
              dkSOList_Inv15, // ��������� ������ ���������� �� �������
              dkActWork, // ��� �����-�����  �����  (�����)
              dkProdTolling, // ����� � �����������
              dkCompManager, // ����� �� ������������ ��� ���������
              dkBuyer_Request, // ������ �� �����������
              dkProtocol_PS_Balance, // ������ � ������������������ ��������� ������� ��������������
              dkActWorkGeneral, // ������� ����� �� ���� ����������� ����� �� ������
              dkVerificationActTolling, // ��� ������ �� ��������
              dkInvMX18, // ��������� MX-18
              dkActWorkRetServ, // ��� � �������� �������� ��� ��������
              dkActWorkRej, // ��� ����� ��� ��������
              dkAWTorg12, // ����-12 �� ������� �� ��������
              dkRevision, // ��������������
              dkInvArtPhoto, // �������� �� ����������
              dkAWMOTotal, // ������� ����� �� �������� ���������� �������-��������
              dkInvUID, // ��������� ����������. ������������ �����
              dkZpK, // ���������� ��������� �������� � �\�
              dkProdTolling_External, // ����� � ����������� (������� �����������)
              dkActWork_External, // ��� ����������� ����� (������� �����������)
              dkMatBalance_Move, // �������� � ������� ����������
              dkArtAll, //���� ���������� ���������
              dkSIList_RequirementInvoice, //����������-���������
              dkOrderReport, // ����� �� ������ �� ������������
              dkActWorkCommon,
              dkProdTollingCommon,
              dkInvMX18Common,
              dkSIEl_M15,  //����������� ����������, ����� �-15
              dkActGoldForReadyProduct,
              dkActUnknownCrude,
              dkActMutualClaims
              );
  TFrxDocKind = (
              xdkAffinageTollingPassport
  );

  TReportSetting = record
    UseDotMatrix : boolean;
  end;

  TdmPrint = class(TDataModule)
    frReport: TfrReport;
    trPrint: TpFIBTransaction;
    quTmp: TpFIBQuery;
    frDesigner: TfrDesigner;
    taSelf: TpFIBDataSet;
    taSelfCOMPID: TIntegerField;
    taSelfCODE: TFIBStringField;
    taSelfNAME: TFIBStringField;
    taSelfSNAME: TFIBStringField;
    taSelfPOSTINDEX: TFIBStringField;
    taSelfCOUNTRY: TFIBStringField;
    taSelfCITY: TFIBStringField;
    taSelfADDRESS: TFIBStringField;
    taSelfPHONE: TFIBStringField;
    taSelfFAX: TFIBStringField;
    taSelfEMAIL: TFIBStringField;
    taSelfWWW: TFIBStringField;
    taSelfBOSSFIO: TFIBStringField;
    taSelfBOSSPHONE: TFIBStringField;
    taSelfBUHFIO: TFIBStringField;
    taSelfBUHPHONE: TFIBStringField;
    taSelfOFIO: TFIBStringField;
    taSelfOFIOPHONE: TFIBStringField;
    taSelfINN: TFIBStringField;
    taSelfBIK: TFIBStringField;
    taSelfOKPO: TFIBStringField;
    taSelfOKONH: TFIBStringField;
    taSelfBANK: TFIBStringField;
    taSelfBILL: TFIBStringField;
    taSelfKBANK: TFIBStringField;
    taSelfKBILL: TFIBStringField;
    taSelfADRBILL: TFIBStringField;
    quInv16: TpFIBDataSet;
    dsrInv16: TDataSource;
    frdsrInv16: TfrDBDataSet;
    quInv16Item: TpFIBDataSet;
    frdsrInv16Item: TfrDBDataSet;
    quInv16LINK: TIntegerField;
    quInv16MOLID: TFIBStringField;
    quInv16DOCNO: TIntegerField;
    quInv16DOCDATE: TDateTimeField;
    quInv16JOBFACE: TFIBStringField;
    quInv16DEPNAME: TFIBStringField;
    quInv16ItemWOITEMID: TIntegerField;
    quInv16ItemITDATE: TDateTimeField;
    quInv16ItemWORDERID: TIntegerField;
    quInv16ItemOPERID: TFIBStringField;
    quInv16ItemUQ: TSmallintField;
    quInv16ItemUW: TSmallintField;
    quInv16ItemSEMIS: TFIBStringField;
    quInv16ItemQ: TFloatField;
    quInv16ItemW: TFloatField;
    quInv16WNO: TIntegerField;
    quInv16OPERATION: TFIBStringField;
    taDoc: TpFIBDataSet;
    taDocID: TIntegerField;
    taDocDOC: TMemoField;
    taDocNAME: TFIBStringField;
    quInv16ItemOPERATION: TFIBStringField;
    taOper: TpFIBDataSet;
    frOper: TfrDBDataSet;
    dsrOper: TDataSource;
    taOperOPERID: TFIBStringField;
    taOperOPERATION: TFIBStringField;
    taOperRATE: TFloatField;
    taMOL: TpFIBDataSet;
    frMOL: TfrDBDataSet;
    dsrMol: TDataSource;
    taMOLMOLID: TIntegerField;
    taMOLFNAME: TFIBStringField;
    taMOLLNAME: TFIBStringField;
    taMOLMNAME: TFIBStringField;
    taMOLFIO: TFIBStringField;
    taMOLSHEETNO: TSmallintField;
    taCalcF: TpFIBDataSet;
    taCalcFTAILS: TFIBStringField;
    taCalcFF: TFloatField;
    frCalcF: TfrDBDataSet;
    taCalcFFTOTAL: TFloatField;
    taCalcFNTOTAL: TFloatField;
    taCalcFGET: TFloatField;
    taCalcFGET_Q: TFloatField;
    taCalcFDONE: TFloatField;
    taCalcFDONE_Q: TFloatField;
    taCalcFRET: TFloatField;
    taCalcFRET_Q: TFloatField;
    taCalcFREJ: TFloatField;
    taCalcFREJ_Q: TFloatField;
    taCalcFREWORK: TFloatField;
    taCalcFREWORK_Q: TFloatField;
    taOperTAILS: TFIBStringField;
    taCalcFR: TFloatField;
    taCalcFOUT: TFloatField;
    taCalcFGET_GR: TFloatField;
    taCalcFDONE_GR: TFloatField;
    taCalcFRET_GR: TFloatField;
    taCalcFREWORK_GR: TFloatField;
    taCalcFREJ_GR: TFloatField;
    taCalcFDESCR_GR: TFIBStringField;
    taProtocol: TpFIBDataSet;
    taPItem: TpFIBDataSet;
    dsrProtocol: TDataSource;
    taProtocolID: TIntegerField;
    taProtocolDOCNO: TIntegerField;
    taProtocolDOCDATE: TDateTimeField;
    frProtocol: TfrDBDataSet;
    frPItem: TfrDBDataSet;
    taMol_T: TpFIBDataSet;
    frMol_T: TfrDBDataSet;
    taMol_TTAILS: TFIBStringField;
    taMol_TF: TFloatField;
    taMol_TFTOTAL: TFloatField;
    taMol_TNTOTAL: TFloatField;
    taMol_TGET: TFloatField;
    taMol_TGET_Q: TFloatField;
    taMol_TDONE: TFloatField;
    taMol_TDONE_Q: TFloatField;
    taMol_TRET: TFloatField;
    taMol_TRET_Q: TFloatField;
    taMol_TREJ: TFloatField;
    taMol_TREJ_Q: TFloatField;
    taMol_TREWORK: TFloatField;
    taMol_TREWORK_Q: TFloatField;
    taMol_TR: TFloatField;
    taMol_TOUT: TFloatField;
    taMol_TGET_GR: TFloatField;
    taMol_TDONE_GR: TFloatField;
    taMol_TRET_GR: TFloatField;
    taMol_TREWORK_GR: TFloatField;
    taMol_TREJ_GR: TFloatField;
    taMol_TDESCR_GR: TFIBStringField;
    taCalcFGET_E: TFloatField;
    taCalcFDONE_E: TFloatField;
    taCalcFRET_E: TFloatField;
    taCalcFREJ_E: TFloatField;
    taCalcFREWORK_E: TFloatField;
    taCalcFR_E: TFloatField;
    taProtocolBD: TDateTimeField;
    taPItemID: TIntegerField;
    taPItemF: TFloatField;
    taPItemN: TFloatField;
    taPItemE: TFloatField;
    taPItemS: TFloatField;
    taPItemI: TFloatField;
    taPItemH: TFloatField;
    taPItemWZ: TFloatField;
    taPItemOUT: TFloatField;
    taActE: TpFIBDataSet;
    frActE: TfrDBDataSet;
    taAppl: TpFIBDataSet;
    frAppl: TfrDBDataSet;
    taApplFIO: TFIBStringField;
    taApplH: TFloatField;
    taApplS: TFloatField;
    taApplI: TFloatField;
    taApplBD: TDateTimeField;
    taApplDOCDATE: TDateTimeField;
    taZP: TpFIBDataSet;
    frZP: TfrDBDataSet;
    taZPOPERNAME: TFIBStringField;
    taZPFIO: TFIBStringField;
    taZPZP: TFloatField;
    taZPU: TFloatField;
    taZPNP: TFloatField;
    taZPNR: TFloatField;
    taZPDONE: TFloatField;
    taZPE: TFloatField;
    taZPS: TFloatField;
    taZPBD: TDateTimeField;
    taZPED: TDateTimeField;
    taZPTotal: TpFIBDataSet;
    taZPTotalZP: TFloatField;
    taZPTotalBD: TDateTimeField;
    taZPTotalDOCDATE: TDateTimeField;
    frZPTotal: TfrDBDataSet;
    taZPZPTOTAL: TFloatField;
    taActESEMISID: TFIBStringField;
    taActESEMIS: TFIBStringField;
    taActEQ: TFloatField;
    taActEW: TFloatField;
    taApplWZ: TFloatField;
    taJ_MatPS: TpFIBDataSet;
    taPS: TpFIBDataSet;
    dsrPS: TDataSource;
    frPS: TfrDBDataSet;
    taPSDEPID: TIntegerField;
    taPSSNAME: TFIBStringField;
    taPSNAME: TFIBStringField;
    taPSSORTIND: TIntegerField;
    frJ_MatPS: TfrDBDataSet;
    taCalcF_PS: TpFIBDataSet;
    frCalcF_PS: TfrDBDataSet;
    taJ_MatPS1: TpFIBDataSet;
    frJ_MatPS1: TfrDBDataSet;
    taCalcF_PSOper: TpFIBDataSet;
    frCalcF_PSOper: TfrDBDataSet;
    taJ_MatPSONO: TIntegerField;
    taJ_MatPSODATE: TDateTimeField;
    taJ_MatPSINO: TIntegerField;
    taJ_MatPSIDATE: TDateTimeField;
    taJ_MatPSITTYPE: TSmallintField;
    taJ_MatPSSEMIS: TFIBStringField;
    taJ_MatPSITDATE: TDateTimeField;
    taJ_MatPSQ: TFloatField;
    taJ_MatPSW: TFloatField;
    taJ_MatPSE: TSmallintField;
    taCalcF_PSOperNTOTAL: TFloatField;
    taCalcF_PSOperGET: TFloatField;
    taCalcF_PSOperGET_Q: TFloatField;
    taCalcF_PSOperDONE: TFloatField;
    taCalcF_PSOperDONE_Q: TFloatField;
    taCalcF_PSOperRET: TFloatField;
    taCalcF_PSOperRET_Q: TFloatField;
    taCalcF_PSOperREJ: TFloatField;
    taCalcF_PSOperREJ_Q: TFloatField;
    taCalcF_PSOperREWORK: TFloatField;
    taCalcF_PSOperREWORK_Q: TFloatField;
    taCalcF_PSOperR: TFloatField;
    taCalcF_PSOperOUT: TFloatField;
    taCalcF_PSOperZ: TFloatField;
    taCalcF_PSOperGET_GR: TFloatField;
    taCalcF_PSOperDONE_GR: TFloatField;
    taCalcF_PSOperRET_GR: TFloatField;
    taCalcF_PSOperREJ_GR: TFloatField;
    taCalcF_PSOperREWORK_GR: TFloatField;
    taCalcF_PSOperDESCR_GR: TFIBStringField;
    taCalcF_PSOperGET_E: TFloatField;
    taCalcF_PSOperDONE_E: TFloatField;
    taCalcF_PSOperRET_E: TFloatField;
    taCalcF_PSOperREJ_E: TFloatField;
    taCalcF_PSOperREWORK_E: TFloatField;
    taCalcF_PSOperR_E: TFloatField;
    taCalcF_PSTAILS: TFIBStringField;
    taCalcF_PSF: TFloatField;
    taCalcF_PSFTOTAL: TFloatField;
    taCalcF_PSNTOTAL: TFloatField;
    taCalcF_PSGET: TFloatField;
    taCalcF_PSGET_Q: TFloatField;
    taCalcF_PSDONE: TFloatField;
    taCalcF_PSDONE_Q: TFloatField;
    taCalcF_PSRET: TFloatField;
    taCalcF_PSRET_Q: TFloatField;
    taCalcF_PSREJ: TFloatField;
    taCalcF_PSREJ_Q: TFloatField;
    taCalcF_PSREWORK: TFloatField;
    taCalcF_PSREWORK_Q: TFloatField;
    taCalcF_PSR: TFloatField;
    taCalcF_PSOUT: TFloatField;
    taCalcF_PSGET_GR: TFloatField;
    taCalcF_PSDONE_GR: TFloatField;
    taCalcF_PSRET_GR: TFloatField;
    taCalcF_PSREWORK_GR: TFloatField;
    taCalcF_PSREJ_GR: TFloatField;
    taCalcF_PSDESCR_GR: TFIBStringField;
    taCalcF_PSGET_E: TFloatField;
    taCalcF_PSDONE_E: TFloatField;
    taCalcF_PSRET_E: TFloatField;
    taCalcF_PSREJ_E: TFloatField;
    taCalcF_PSREWORK_E: TFloatField;
    taCalcF_PSR_E: TFloatField;
    taCalcF_PSINPUT: TFloatField;
    taZPTotalNAME: TFIBStringField;
    taZPTAILS: TFIBStringField;
    frIN: TfrDBDataSet;
    taCalcF_PSOperINPUT: TFloatField;
    taCalcF_PSOperWZP: TFloatField;
    taCalcF_PSWZP: TFloatField;
    taPItemNAME: TFIBStringField;
    taActEQ_AFTER: TFloatField;
    taActEW_AFTER: TFloatField;
    taActENAME: TFIBStringField;
    taInvToPrint: TpFIBDataSet;
    taInvToPrintDOCNO: TIntegerField;
    taInvToPrintDOCDATE: TDateTimeField;
    taInvToPrintSUPNAME: TFIBStringField;
    taInvToPrintCUSTNAME: TFIBStringField;
    taInvToPrintSUPADDRESS: TFIBStringField;
    taInvToPrintSUPPHONE: TFIBStringField;
    taInvToPrintCUSTADDRESS: TFIBStringField;
    taInvToPrintCUSTPHONE: TFIBStringField;
    taInvToPrintSUPINN: TFIBStringField;
    taInvToPrintSUPOKPO: TFIBStringField;
    taInvToPrintSUPOKONH: TFIBStringField;
    taInvToPrintSUPBILL: TFIBStringField;
    taInvToPrintSUPKBILL: TFIBStringField;
    taInvToPrintSUPBIK: TFIBStringField;
    taInvToPrintCUSTINN: TFIBStringField;
    taInvToPrintCUSTOKPO: TFIBStringField;
    taInvToPrintCUSTOKONH: TFIBStringField;
    taInvToPrintCUSTBILL: TFIBStringField;
    taInvToPrintCUSTKBILL: TFIBStringField;
    taInvToPrintCUSTBIK: TFIBStringField;
    frInvToPrint: TfrDBDataSet;
    taInvToPrintSUPBANK: TFIBStringField;
    taInvToPrintCUSTBANK: TFIBStringField;
    taVedomostToPrint: TpFIBDataSet;
    frVedomostTOPrint: TfrDBDataSet;
    taCalcFINPUT: TFloatField;
    taCalcFWZP: TFloatField;
    taACTRePrice: TpFIBDataSet;
    taACTRePriceART: TFIBStringField;
    taACTRePriceART2: TFIBStringField;
    taACTRePriceOLD_PRICE: TFloatField;
    taACTRePriceNEW_PRICE: TFloatField;
    taACTRePriceU: TFIBStringField;
    taACTRePriceINS: TFIBStringField;
    taACTRePriceQ: TIntegerField;
    taACTRePriceW: TFloatField;
    frStones: TfrDBDataSet;
    taProdAppl: TpFIBDataSet;
    frProdAppl: TfrDBDataSet;
    taSellAnaliz: TpFIBDataSet;
    frSellAnaliz: TfrDBDataSet;
    taProdApplART: TFIBStringField;
    taProdApplSZNAME: TFIBStringField;
    taProdApplQ: TFloatField;
    taProdApplAPPLNO: TIntegerField;
    taProdApplAPPLDATE: TDateTimeField;
    taPSOper: TpFIBDataSet;
    dsrPSOper: TDataSource;
    frPSOper: TfrDBDataSet;
    taSJ_MatPS: TpFIBDataSet;
    frSJ_MatPS: TfrDBDataSet;
    taCalcSF_PSOper: TpFIBDataSet;
    frCalcSF_PSOper: TfrDBDataSet;
    taPSOperOPERID: TFIBStringField;
    taPSOperOPERATION: TFIBStringField;
    taPSOperRATE: TFloatField;
    taPSOperTAILS: TFIBStringField;
    taSJ_MatPSID: TFIBStringField;
    taSJ_MatPSSEMIS: TFIBStringField;
    taSJ_MatPSGET: TFloatField;
    taSJ_MatPSREWORK: TFloatField;
    taSJ_MatPSDONE: TFloatField;
    taSJ_MatPSRET: TFloatField;
    taSJ_MatPSREJ: TFloatField;
    taSJ_MatPSOUT: TFloatField;
    taSJ_MatPSGET_Q: TFloatField;
    taSJ_MatPSREWORK_Q: TFloatField;
    taSJ_MatPSDONE_Q: TFloatField;
    taSJ_MatPSRET_Q: TFloatField;
    taSJ_MatPSREJ_Q: TFloatField;
    taSJ_MatPSOUT_Q: TFloatField;
    taSJ_MatPSO1: TFIBStringField;
    taSJ_MatPSEDGSHAPEID: TFIBStringField;
    taSJ_MatPSEDGTID: TFIBStringField;
    taSJ_MatPSCHROMATICITY: TFIBStringField;
    taSJ_MatPSCLEANNES: TFIBStringField;
    taSJ_MatPSSZ: TFIBStringField;
    taACTRePriceSUM_Q0: TFloatField;
    taACTRePriceIN_SUM: TFloatField;
    taACTRePriceOUT_SUM: TFloatField;
    taACTRePriceRAZ_SUM: TFloatField;
    taSItemsPrint: TpFIBDataSet;
    frActRePrice: TfrDBDataSet;
    taSItemsPrintSELID: TIntegerField;
    taSItemsPrintINVID: TIntegerField;
    taSItemsPrintART2ID: TIntegerField;
    taSItemsPrintART2: TFIBStringField;
    taSItemsPrintFULLART: TFIBStringField;
    taSItemsPrintPRICE: TFloatField;
    taSItemsPrintTPRICE: TFloatField;
    taSItemsPrintQUANTITY: TSmallintField;
    taSItemsPrintTOTALWEIGHT: TFloatField;
    taSItemsPrintQ: TIntegerField;
    taSItemsPrintW: TFloatField;
    taSItemsPrintPRICE2: TFloatField;
    taSItemsPrintPNDS: TFloatField;
    taSItemsPrintUNITID: TIntegerField;
    taSItemsPrintNDSID: TIntegerField;
    taSItemsPrintART: TFIBStringField;
    taSItemsPrintUSEMARGIN: TSmallintField;
    taSItemsPrintFULLSUM: TFloatField;
    taSItemsPrintD_INSID: TFIBStringField;
    taSItemsPrintD_ARTID: TIntegerField;
    taSItemsPrintEP2: TFloatField;
    taSItemsPrintSSUM: TFloatField;
    taSItemsPrintSSUMD: TFloatField;
    taSItemsPrintSSUMF: TFloatField;
    taSItemsPrintPRILL: TFIBStringField;
    taSItemsPrintAKCIZ: TFloatField;
    taSItemsPrintGOOD: TFIBStringField;
    frWhArt: TfrDBDataSet;
    taUnit: TpFIBDataSet;
    taSItemsPrintUNIT: TStringField;
    dsUnit: TDataSource;
    taStoneItems: TpFIBDataSet;
    taStoneItemsSTONES: TFIBStringField;
    frStoneItems: TfrDBDataSet;
    taINVHeader: TpFIBDataSet;
    frInvHeader: TfrDBDataSet;
    taSIListToPrint: TpFIBDataSet;
    frSIListTOPrint: TfrDBDataSet;
    taSielToPrint: TpFIBDataSet;
    frSIELToPrint: TfrDBDataSet;
    taVedomostToPrint2: TpFIBDataSet;
    frVedomostToPrint2: TfrDBDataSet;
    taVedomostToPrint2DOCDATE: TDateTimeField;
    taVedomostToPrint2DOCNO: TIntegerField;
    taVedomostToPrint2Q: TIntegerField;
    taVedomostToPrint2W: TFloatField;
    taVedomostToPrint2INSUM: TFloatField;
    taVedomostToPrint2OUTSUM: TFloatField;
    taVedomostToPrint2RETSUM: TFloatField;
    taVedomostToPrint2RETTOPRODSUM: TFloatField;
    taVedomostToPrint2TONAME: TFIBStringField;
    taVedomostToPrint2FROMNAME: TFIBStringField;
    taVedomostToPrint2BDATE: TDateTimeField;
    taVedomostToPrint2EDATE: TDateTimeField;
    taSielToPrintSEMIS: TFIBStringField;
    taSielToPrintPRICE: TFloatField;
    taSielToPrintW: TFloatField;
    taSielToPrintQ: TIntegerField;
    taSielToPrintNDS: TFloatField;
    taSielToPrintUQ: TFIBStringField;
    taSielToPrintUW: TFIBStringField;
    frWhSemis: TfrDBDataSet;
    taVedomostToPrint2INVID: TIntegerField;
    taVedomostToPrint2INQ: TIntegerField;
    taVedomostToPrint2INW: TFloatField;
    taVedomostToPrint2RETQ: TIntegerField;
    taVedomostToPrint2RETW: TFloatField;
    taVedomostToPrint2OUTQ: TIntegerField;
    taVedomostToPrint2OUTW: TFloatField;
    taSielToPrintCOST: TFloatField;
    taSielToPrintSEMIS_NAME: TFIBStringField;
    taPItem_d: TpFIBDataSet;
    frPItem_d: TfrDBDataSet;
    taOb_d: TpFIBDataSet;
    frOb_d: TfrDBDataSet;
    taOb_dFIO: TFIBStringField;
    taOb_dH: TFloatField;
    taOb_dI: TFloatField;
    taOb_dS: TFloatField;
    taOb_dBD: TDateTimeField;
    taOb_dDOCDATE: TDateTimeField;
    taOb_dWZ: TFloatField;
    taPItem_dNAME: TFIBStringField;
    taPItem_dF: TFloatField;
    taPItem_dN: TFloatField;
    taPItem_dE: TFloatField;
    taPItem_dS: TFloatField;
    taPItem_dI: TFloatField;
    taPItem_dH: TFloatField;
    taPItem_dDONEW: TFloatField;
    taPItem_dRW: TFloatField;
    taDIELToPrint: TpFIBDataSet;
    taDIELToPrintID: TIntegerField;
    taDIELToPrintSEMIS: TFIBStringField;
    taDIELToPrintSEMISNAME: TFIBStringField;
    taDIELToPrintW: TFloatField;
    taDIELToPrintQ: TIntegerField;
    taDIELToPrintUQ_NAME: TFIBStringField;
    taDIELToPrintUW_NAME: TFIBStringField;
    taDIELToPrintOPER_NAME: TFIBStringField;
    taDIELToPrintAEL_REF: TIntegerField;
    frDIELToPrint: TfrDBDataSet;
    taAELToPrint: TpFIBDataSet;
    frAELToPrint: TfrDBDataSet;
    taAELToPrintART: TFIBStringField;
    taAELToPrintART2: TFIBStringField;
    taAELToPrintSZ: TFIBStringField;
    taAELToPrintOPERATION: TFIBStringField;
    taAELToPrintQ: TIntegerField;
    taAELToPrintU: TSmallintField;
    taVedToPrint2_SUM: TpFIBDataSet;
    frVedToPrint2_Sum: TfrDBDataSet;
    taVedToPrint2_SUMSUPNAME: TFIBStringField;
    taVedToPrint2_SUMOUTSUM: TFloatField;
    taVedToPrint2_SUMRETSUM: TFloatField;
    taPItem_dU: TFloatField;
    dsrDIel: TDataSource;
    taSJInfo: TpFIBDataSet;
    taSJInfoID: TIntegerField;
    taSJInfoDOCNO: TIntegerField;
    taSJInfoDOCDATE: TDateTimeField;
    taSJInfoUSERID: TIntegerField;
    taSJInfoBD: TDateTimeField;
    taSJInfoISCLOSE: TSmallintField;
    taSJInfoT: TSmallintField;
    taSJInfoMATID: TFIBStringField;
    frSJInfo: TfrDBDataSet;
    taSJ_PSOper: TpFIBDataSet;
    frSJ_PSOper: TfrDBDataSet;
    taSJ_PSOperSEMISNAME: TFIBStringField;
    taSJ_PSOperINW: TFloatField;
    taSJ_PSOperINQ: TIntegerField;
    taSJ_PSOperFQ: TIntegerField;
    taSJ_PSOperFW: TFloatField;
    taSJ_PSOperRQ: TIntegerField;
    taSJ_PSOperRW: TFloatField;
    taSJ_PSOperUQ: TSmallintField;
    taSJ_PSOperUW: TSmallintField;
    taSJ_PSOperGETW: TFloatField;
    taSJ_PSOperGETQ: TIntegerField;
    taSJ_PSOperDONEW: TFloatField;
    taSJ_PSOperDONEQ: TIntegerField;
    taSJ_PSOperRETW: TFloatField;
    taSJ_PSOperRETQ: TIntegerField;
    taSJ_PSOperREJW: TFloatField;
    taSJ_PSOperREJQ: TIntegerField;
    taSJ_PSOperREWORKW: TFloatField;
    taSJ_PSOperREWORKQ: TIntegerField;
    taSJ_PSOperNKW: TFloatField;
    taSJ_PSOperNKQ: TIntegerField;
    taSJ_PSOperIQ: TIntegerField;
    taSJ_PSOperIW: TFloatField;
    taProtPS: TpFIBDataSet;
    frProtPS: TfrDBDataSet;
    taProtPSOper: TpFIBDataSet;
    dsrProtPS: TDataSource;
    frProtPSOper: TfrDBDataSet;
    taProtPSPSID: TIntegerField;
    taProtPSOperOPERID: TFIBStringField;
    taProtPSDEPNAME: TFIBStringField;
    taProtPSOperOPERATION: TFIBStringField;
    taSJ_OperT: TpFIBDataSet;
    frSJ_OperT: TfrDBDataSet;
    taSJ_PST: TpFIBDataSet;
    frSJ_PST: TfrDBDataSet;
    dsrProtPSOper: TDataSource;
    taSJ_OperTF: TFloatField;
    taSJ_OperTN: TFloatField;
    taSJ_OperTU: TFloatField;
    taSJ_OperTINW: TFloatField;
    taSJ_OperTGETW: TFloatField;
    taSJ_OperTDONEW: TFloatField;
    taSJ_OperTRETW: TFloatField;
    taSJ_OperTREJW: TFloatField;
    taSJ_OperTREWORKW: TFloatField;
    taSJ_OperTNKW: TFloatField;
    taSJ_OperTIW: TFloatField;
    taSJ_OperTS: TFloatField;
    taSJ_PSTF: TFloatField;
    taSJ_PSTN: TFloatField;
    taSJ_PSTS: TFloatField;
    taSJ_PSTRQ: TIntegerField;
    taSJ_PSTRW: TFloatField;
    taSJ_PSTU: TFloatField;
    taSJ_PSTINW: TFloatField;
    taSJ_PSTINQ: TIntegerField;
    taSJ_PSTGETW: TFloatField;
    taSJ_PSTGETQ: TIntegerField;
    taSJ_PSTDONEW: TFloatField;
    taSJ_PSTDONEQ: TIntegerField;
    taSJ_PSTRETW: TFloatField;
    taSJ_PSTRETQ: TIntegerField;
    taSJ_PSTREJW: TFloatField;
    taSJ_PSTREJQ: TIntegerField;
    taSJ_PSTREWORKW: TFloatField;
    taSJ_PSTREWORKQ: TIntegerField;
    taSJ_PSTNKW: TFloatField;
    taSJ_PSTNKQ: TIntegerField;
    taSJ_PSTIQ: TIntegerField;
    taSJ_PSTIW: TFloatField;
    taSOelToPrint: TpFIBDataSet;
    frSoelToPrint: TfrDBDataSet;
    taSJ_OperTMATNAME: TFIBStringField;
    taSJ_OperTMATID: TFIBStringField;
    taSJ_PSTMATID: TFIBStringField;
    taSJ_PSTMATNAME: TFIBStringField;
    taSJ_PSTails: TpFIBDataSet;
    frSJ_PSTails: TfrDBDataSet;
    taSJ_PSTailsN: TFloatField;
    taSJ_PSTailsTAILNAME: TFIBStringField;
    taSemisReport: TpFIBDataSet;
    frSemisReport: TfrDBDataSet;
    taProtTails_d: TpFIBDataSet;
    frProtTails_d: TfrDBDataSet;
    taProtTails_dTAILNAME: TFIBStringField;
    taProtTails_dN: TFloatField;
    taSRItem: TpFIBDataSet;
    frSRItem: TfrDBDataSet;
    taInvToPrintCOMMISION: TSmallintField;
    taVList: TpFIBDataSet;
    taVSheet: TpFIBDataSet;
    frVList: TfrDBDataSet;
    frVSheet: TfrDBDataSet;
    taVSheetMat: TpFIBDataSet;
    frVSheetMat: TfrDBDataSet;
    taVSheetMatMATID: TFIBStringField;
    taVSheetMatMATNAME: TFIBStringField;
    taVSheetMatREST_OUT_W: TFloatField;
    taVSheetMatFW: TFloatField;
    taVSheetMatRW: TFloatField;
    taVSheetMatDETAIL: TSmallintField;
    taVSheetMatREST_OUT_W_PURE: TFloatField;
    taVSheetMatFW_PURE: TFloatField;
    taVSheetMatRW_PURE: TFloatField;
    taVSheetMat1: TpFIBDataSet;
    frVSheetMat1: TfrDBDataSet;
    taSItemsPrintACC_SUMM: TFloatField;
    taVedomostToPrint2ACC_SUMM: TFloatField;
    taVedToPrint2_SUMACC_RETSUM: TFloatField;
    taVedToPrint2_SUMACC_INSUM: TFloatField;
    taVedToPrint2_SUMACC_OUTSUM: TFloatField;
    taVedToPrint2_SUMSACC_RETSUM: TFloatField;
    taVedToPrint2_SUMSACC_INSUM: TFloatField;
    taVedToPrint2_SUMSACC_OUTSUM: TFloatField;
    taInv16Assort: TpFIBDataSet;
    frInv16Assort: TfrDBDataSet;
    quInv16ItemAINV: TIntegerField;
    dsrInv16Item: TDataSource;
    frWhAppl: TfrDBDataSet;
    taProtocol_PS: TpFIBDataSet;
    frProtocol_PS: TfrDBDataSet;
    taPItem_PS: TpFIBDataSet;
    frPItem_PS: TfrDBDataSet;
    taPArt_PS: TpFIBDataSet;
    frPArt_PS: TfrDBDataSet;
    taProtocol_PSDOCNO: TIntegerField;
    taProtocol_PSDOCDATE: TDateTimeField;
    taProtocol_PSPSNAME: TFIBStringField;
    taProtocol_PSACTID: TIntegerField;
    taProtocol_PSPSID: TIntegerField;
    taPArt_PSU: TSmallintField;
    taPArt_PSFQ: TIntegerField;
    taPArt_PSART: TFIBStringField;
    taPArt_PSART2: TFIBStringField;
    taPArt_PSOPERNAME: TFIBStringField;
    taPArt_PSSZNAME: TFIBStringField;
    dsrProtocol_PS: TDataSource;
    frDictArt: TfrDBDataSet;
    frAffInv: TfrDBDataSet;
    frAffinaj: TfrDBDataSet;
    taSItemsPrintSZNAME: TFIBStringField;
    taVedomostToPrintREST1_W: TFloatField;
    taVedomostToPrintREST1_Q: TIntegerField;
    taVedomostToPrintREST1_SUM: TFloatField;
    taVedomostToPrintIN_W: TFloatField;
    taVedomostToPrintIN_Q: TIntegerField;
    taVedomostToPrintIN_SUM: TFloatField;
    taVedomostToPrintRET_AKCIZ: TFloatField;
    taVedomostToPrintRET_NDS: TFloatField;
    taVedomostToPrintRET_FULLSUM: TFloatField;
    taVedomostToPrintRET_W: TFloatField;
    taVedomostToPrintRET_Q: TFloatField;
    taVedomostToPrintRET_SUM: TFloatField;
    taVedomostToPrintSELL_W: TFloatField;
    taVedomostToPrintSELL_Q: TIntegerField;
    taVedomostToPrintSELL_SUM: TFloatField;
    taVedomostToPrintSELL_AKCIZ: TFloatField;
    taVedomostToPrintSELL_NDS: TFloatField;
    taVedomostToPrintSELL_FULLSUM: TFloatField;
    taVedomostToPrintREST2_W: TFloatField;
    taVedomostToPrintREST2_Q: TIntegerField;
    taVedomostToPrintREST2_SUM: TFloatField;
    taVedomostToPrintART: TFIBStringField;
    taVedomostToPrintBD: TDateTimeField;
    taVedomostToPrintDOCDATE: TDateTimeField;
    taVedomostToPrintBRACK_Q: TIntegerField;
    taVedomostToPrintBRACK_W: TFloatField;
    taVedomostToPrintBRACK_SUM: TFloatField;
    taVedomostToPrintACT_Q: TFloatField;
    taVedomostToPrintACT_W: TFloatField;
    taVedomostToPrintACT_SUM: TFloatField;
    taBulk: TpFIBDataSet;
    taBulkItem: TpFIBDataSet;
    dsrBulk: TDataSource;
    frBulk: TfrDBDataSet;
    frBulkItem: TfrDBDataSet;
    taBulkINVID: TIntegerField;
    taBulkDOCNO: TIntegerField;
    taBulkDOCDATE: TDateTimeField;
    taBulkQ: TFloatField;
    taBulkW: TFloatField;
    taBulkDEPNAME: TFIBStringField;
    taKolieReport: TpFIBDataSet;
    frKolieReport: TfrDBDataSet;
    taInvChangeReport: TpFIBDataSet;
    frInvChangeReport: TfrDBDataSet;
    taInvChangeReportINVID: TIntegerField;
    taInvChangeReportDOCDATE: TDateTimeField;
    taInvChangeReportDOCNO: TIntegerField;
    taInvChangeReportDEPNAME: TFIBStringField;
    taInvChangeReportSUPNAME: TFIBStringField;
    taInvChangeReportFROMDEPNAME: TFIBStringField;
    taInvChangeReportQ: TIntegerField;
    taInvChangeReportW: TFloatField;
    taInvChangeReportFULLSUM: TFloatField;
    taDelInv: TpFIBDataSet;
    frDelInv: TfrDBDataSet;
    taDelInvDOCNO: TIntegerField;
    taDelInvDOCDATE: TDateTimeField;
    taDelInvINVID: TIntegerField;
    taDelInvUPD: TSmallintField;
    taInvChangeReportITYPENAME: TFIBStringField;
    taAffInv: TpFIBDataSet;
    taAffinaj: TpFIBDataSet;
    dsrAffInv: TDataSource;
    taAffInvINVID: TIntegerField;
    taAffInvDOCNO: TIntegerField;
    taAffInvDOCDATE: TDateTimeField;
    taAffInvQ: TFloatField;
    taAffInvW: TFloatField;
    taAffInvDEPNAME: TFIBStringField;
    taAffInvCOMPNAME: TFIBStringField;
    taAffInvSUPNAME: TFIBStringField;
    frTextExport1: TfrTextExport;
    quInv16ItemRecNo: TIntegerField;
    quInv16ItemUQNAME: TStringField;
    quInv16ItemUWNAME: TStringField;
    quInv16ItemAPPLINV: TIntegerField;
    frOperOrder: TfrDBDataSet;
    frAssortDet: TfrDBDataSet;
    taSJ_PSTDONE_GOLD: TFloatField;
    taSJ_OperTDONE_GOLD: TFloatField;
    frArtIns: TfrDBDataSet;
    taStoreByItems: TpFIBDataSet;
    frStoreByItems: TfrDBDataSet;
    taStoreByItemsUID: TIntegerField;
    taStoreByItemsART: TFIBStringField;
    taStoreByItemsART2: TFIBStringField;
    taStoreByItemsW: TFloatField;
    taStoreByItemsCURPRICE: TFloatField;
    taStoneInInv: TpFIBDataSet;
    frStoneInInv: TfrDBDataSet;
    taStoneInInvW: TFloatField;
    taStoneInInvSEMIS: TFIBStringField;
    taStoneInInvUQ: TSmallintField;
    taStoneInInvUW: TSmallintField;
    taStoreByItemsCOST: TFloatField;
    frMatrixProd: TfrDBDataSet;
    taTagIns: TpFIBDataSet;
    frB_UID: TfrDBDataSet;
    taSemisInInv: TpFIBDataSet;
    frSemisInInv: TfrDBDataSet;
    taSemisInInvQ: TIntegerField;
    taSemisInInvW: TFloatField;
    taSemisInInvSEMIS: TFIBStringField;
    taSJ_OperTRW: TFloatField;
    taSJ_OperTDONE_MAT_RW: TFloatField;
    taSJ_PSTDONE_MAT_RQ: TFloatField;
    taSJ_PSTDONE_MAT_RW: TFloatField;
    taSJ_T: TpFIBDataSet;
    frSJ_T: TfrDBDataSet;
    taSJ_TMATID: TFIBStringField;
    taSJ_TMATNAME: TFIBStringField;
    taSJ_TUQ: TSmallintField;
    taSJ_TUW: TSmallintField;
    taSJ_TW: TFloatField;
    taAffinajSEMISNAME: TFIBStringField;
    taAffinajQ: TIntegerField;
    taAffinajW: TFloatField;
    taAffinajDEPNAME: TFIBStringField;
    taAffinajUQ: TSmallintField;
    taAffinajUW: TSmallintField;
    taAffinajSORTIND: TIntegerField;
    taInvToPrintTRANS_PRICE: TFloatField;
    taSJ_OperTINQ: TFIBBCDField;
    taSJ_OperTRQ: TFIBBCDField;
    taSJ_OperTGETQ: TFIBBCDField;
    taSJ_OperTDONEQ: TFIBBCDField;
    taSJ_OperTRETQ: TFIBBCDField;
    taSJ_OperTREJQ: TFIBBCDField;
    taSJ_OperTREWORKQ: TFIBBCDField;
    taSJ_OperTNKQ: TFIBBCDField;
    taSJ_OperTIQ: TFIBBCDField;
    taSJ_OperTDONE_MAT_RQ: TFIBBCDField;
    taSJ_TQ: TFIBBCDField;
    taSJFact_Mat: TpFIBDataSet;
    taSJFact_PS: TpFIBDataSet;
    frSJFact_Mat: TfrDBDataSet;
    frSJFact_PS: TfrDBDataSet;
    taSJFact_MatMATID: TFIBStringField;
    taSJFact_MatMATNAME: TFIBStringField;
    taSJFact_PSPSNAME: TFIBStringField;
    taSJFact_PSFQ: TFIBBCDField;
    taSJFact_PSFW: TFIBFloatField;
    taZpAssort: TpFIBDataSet;
    frZpAssort: TfrDBDataSet;
    taVedToPrint2_SUMTRANS_SUM: TFIBFloatField;
    taVedToPrint2_SUMOUTW_SUM: TFIBFloatField;
    taVedToPrint2_SUMRETW_SUM: TFIBFloatField;
    frRTFExport1: TfrRTFExport;
    taVSheetAssort: TpFIBDataSet;
    frVSheetAssort: TfrDBDataSet;
    taVSheetOperAssort: TpFIBDataSet;
    frVSheetOperAssort: TfrDBDataSet;
    taVSheetOperAssortVITEMID: TFIBIntegerField;
    taVSheetOperAssortDOCNO: TFIBIntegerField;
    taVSheetOperAssortDOCDATE: TFIBDateTimeField;
    taVSheetOperAssortDEPNAME: TFIBStringField;
    taVSheetOperAssortOPERATION: TFIBStringField;
    taVSheetOperAssortSEMIS: TFIBStringField;
    taStoreByItemsSZ: TFIBStringField;
    frBarCode: TfrBarCodeObject;
    taInvToPrintSUPKPP: TFIBStringField;
    taInvToPrintPAYTYPENAME: TFIBStringField;
    quStickerUID: TpFIBQuery;
    taStickerIns: TpFIBDataSet;
    taStoreByItemsSELFCOMPID: TFIBIntegerField;
    taProdApplRoot: TpFIBDataSet;
    frProdApplRoot: TfrDBDataSet;
    quInv16ItemITTYPE: TFIBSmallIntField;
    quInv16ItemITTYPENAME: TFIBStringField;
    taSItemsPrintMAT: TFIBStringField;
    taSItemsPrintINSSNAME: TFIBStringField;
    frRichObject: TfrRichObject;
    taStoneInInvQ: TFIBBCDField;
    taInvToPrintITYPE: TFIBIntegerField;
    frArtAll: TfrDBDataSet;
    frXMLExcelExport1: TfrXMLExcelExport;
    taSielToPrintQW: TFloatField;
    taSielToPrintUQW: TStringField;
    taOrderReport: TpFIBDataSet;
    taOrderReportORDERITEMID: TFIBIntegerField;
    taOrderReportMODEL: TFIBStringField;
    taOrderReportA: TFIBFloatField;
    taOrderReportB: TFIBFloatField;
    taOrderReportP1: TFIBIntegerField;
    taOrderReportP2: TFIBIntegerField;
    taOrderReportP3: TFIBIntegerField;
    taOrderReportP41: TFIBIntegerField;
    taOrderReportP42: TFIBIntegerField;
    frOrderReport: TfrDBDataSet;
    taInvToPrintSUPFACTADDRESS: TFIBStringField;
    taInvToPrintCUSTFACTADDRESS: TFIBStringField;
    taProdApplRootID: TFIBIntegerField;
    taProdApplRootDESIGNATION: TFIBStringField;
    taProdApplRootQUANTITY: TFIBBCDField;
    taPMat_Ps: TpFIBDataSet;
    frPMat_PS: TfrDBDataSet;
    dsrPMat_Ps: TDataSource;
    taPOper_PS: TpFIBDataSet;
    frPOper_PS: TfrDBDataSet;
    dsrPOper_PS: TDataSource;
    taStoneInInvSum: TpFIBDataSet;
    frStoneInInvSum: TfrDBDataSet;
    taStoneInInvSumQ: TFIBBCDField;
    taStoneInInvSumW: TFIBFloatField;
    taStoneInInvSumMATNAME: TFIBStringField;
    taInvoiceHeader: TpFIBDataSet;
    frInvoiceHeader: TfrDBDataSet;
    taProdApplRing: TpFIBDataSet;
    FIBStringField1: TFIBStringField;
    FloatField1: TFloatField;
    frProdApplRing: TfrDBDataSet;
    frSumOrNotSOEl: TfrDBDataSet;
    taSJ_OperTLOSSESNORM: TFIBFloatField;
    taSJ_PSTN2: TFIBFloatField;
    taINVHeaderSUPNAME: TFIBStringField;
    taINVHeaderFROMDEPNAME: TFIBStringField;
    taINVHeaderDOCDATE: TFIBDateTimeField;
    taINVHeaderDOCNO: TFIBIntegerField;
    taINVHeaderSSFDT: TFIBDateTimeField;
    taINVHeaderSSF: TFIBStringField;
    taINVHeaderSELFNAME: TFIBStringField;
    taINVHeaderDEPNAME: TFIBStringField;
    taINVHeaderDOGNO: TFIBStringField;
    taINVHeaderDOGDATE: TFIBDateTimeField;
    taINVHeaderUSERNAME: TFIBStringField;
    taINVHeaderBOSSFIO: TFIBStringField;
    taINVHeaderOFIO: TFIBStringField;
    taINVHeaderSELFCOMPANYINN: TFIBStringField;
    taINVHeaderSUPPLIERBOSSFIO: TFIBStringField;
    taINVHeaderSELFPHONE: TFIBStringField;
    taINVHeaderSELFADDRESS: TFIBStringField;
    taProdApplResp: TpFIBDataSet;
    frProdApplResp: TfrDBDataSet;
    taProdApplRespSTARTD: TFIBDateTimeField;
    taProdApplRespFINISHD: TFIBDateTimeField;
    taProdApplRespREADYD: TFIBDateTimeField;
    taProdApplRespSTARTN: TFIBStringField;
    taProdApplRespFINISHN: TFIBStringField;
    taProdApplRespREADYN: TFIBStringField;
    taINVHeadersuppliercity: TStringField;
    taINVHeaderSHORTBOSS: TStringField;
    taSielToPrintPRILL_NAME: TFIBStringField;
    taSIListToPrintSUPNAME: TFIBStringField;
    taSIListToPrintFROMDEPNAME: TFIBStringField;
    taSIListToPrintDEPNAME: TFIBStringField;
    taSIListToPrintSSF: TFIBStringField;
    taSIListToPrintSSFDT: TFIBDateTimeField;
    taSIListToPrintDOCNO: TFIBIntegerField;
    taSIListToPrintDOCDATE: TFIBDateTimeField;
    taSIListToPrintSELFNAME: TFIBStringField;
    taSIListToPrintDOGNO: TFIBStringField;
    taSIListToPrintDOGDATE: TFIBDateTimeField;
    taSIListToPrintBOSSFIO: TFIBStringField;
    taSIListToPrintSELFCOMPANYINN: TFIBStringField;
    taSIListToPrintSUPPLIERBOSSFIO: TFIBStringField;
    taSIListToPrintSELFCOMPANYADDRESS: TFIBStringField;
    taSIListToPrintSUPLIERCOMPANYADDRESS: TFIBStringField;
    taSIListToPrintSUPLIERCOMPANYINN: TFIBStringField;
    taStickerUID: TpFIBDataSet;
    frStickerUID: TfrDBDataSet;
    taStickerUIDUID: TFIBIntegerField;
    taStickerUIDART: TFIBStringField;
    taStickerUIDART2: TFIBStringField;
    taStickerUIDART2ID: TFIBIntegerField;
    taStickerUIDSTICKER_COMMENT: TFIBStringField;
    taStickerUIDW: TFIBFloatField;
    taStickerUIDSZNAME: TFIBStringField;
    taStickerUIDSEMIS: TFIBStringField;
    taStickerUIDSNAME: TFIBStringField;
    frxReport: TfrxReport;
    frxBarCode: TfrxBarCodeObject;
    frxDesigner: TfrxDesigner;
    frxItems: TfrxDBDataset;
    frActTolling: TfrDBDataSet;
    taStoreByItemsSNAME: TFIBStringField;
    taVSheetID: TFIBIntegerField;
    taVSheetREST_IN_Q: TFIBIntegerField;
    taVSheetREST_IN_W: TFIBFloatField;
    taVSheetIN_Q: TFIBIntegerField;
    taVSheetIN_W: TFIBFloatField;
    taVSheetIN_MOVE_Q: TFIBIntegerField;
    taVSheetIN_MOVE_W: TFIBFloatField;
    taVSheetIN_PS_Q: TFIBIntegerField;
    taVSheetIN_PS_W: TFIBFloatField;
    taVSheetOUT_Q: TFIBIntegerField;
    taVSheetOUT_W: TFIBFloatField;
    taVSheetOUT_MOVE_Q: TFIBIntegerField;
    taVSheetOUT_MOVE_W: TFIBFloatField;
    taVSheetOUT_PS_Q: TFIBIntegerField;
    taVSheetOUT_PS_W: TFIBFloatField;
    taVSheetREST_OUT_Q: TFIBIntegerField;
    taVSheetREST_OUT_W: TFIBFloatField;
    taVSheetUQ_NAME: TFIBStringField;
    taVSheetUW_NAME: TFIBStringField;
    taVSheetOPERATION: TFIBStringField;
    taVSheetSEMIS: TFIBStringField;
    taVSheetDEPNAME: TFIBStringField;
    taVSheetDEPID: TFIBIntegerField;
    taVSheetSEMISID: TFIBStringField;
    taVSheetOPERID: TFIBStringField;
    taVSheetUQ: TFIBSmallIntField;
    taVSheetUW: TFIBSmallIntField;
    taVSheetFQ: TFIBFloatField;
    taVSheetFW: TFIBFloatField;
    taVSheetRQ: TFIBFloatField;
    taVSheetRW: TFIBFloatField;
    taVSheetSEMISPRICE: TFIBFloatField;
    taSumOrNotSOEl: TpFIBDataSet;
    taSumOrNotSOElSEMISNAME: TFIBStringField;
    taSumOrNotSOElIDSEMIS: TFIBStringField;
    taSumOrNotSOElQ: TFIBFloatField;
    taSumOrNotSOElSUMWEIGHT: TFIBFloatField;
    taSumOrNotSOElFOR_Q_U: TFIBSmallIntField;
    taSumOrNotSOElNDS: TFIBFloatField;
    taSumOrNotSOElSUMCOST: TFIBFloatField;
    taSumOrNotSOElUW: TFIBStringField;
    taSumOrNotSOElUQ: TFIBStringField;
    taSumOrNotSOElOPER: TFIBStringField;
    taSumOrNotSOElHANDPRICE: TFIBFloatField;
    taPrintRequestSummary: TpFIBDataSet;
    taPrintRequestSummaryART: TFIBStringField;
    taPrintRequestSummaryQ: TFIBBCDField;
    frPrintRequestSummary: TfrDBDataSet;
    taReferencedInvoiceItems: TpFIBDataSet;
    taReferencedInvoiceItemsNAME: TFIBStringField;
    taReferencedInvoiceItemsSUBNAME: TFIBStringField;
    taReferencedInvoiceItemsUNIT: TFIBStringField;
    taReferencedInvoiceItemsINCOMEPRICE: TFIBBCDField;
    taReferencedInvoiceItemsSELLPRICE: TFIBBCDField;
    taReferencedInvoiceItemsQUANTITY: TFIBBCDField;
    taReferencedInvoiceItemsWEIGHT: TFIBBCDField;
    taReferencedInvoiceItemsCOST: TFIBBCDField;
    taReferencedInvoiceItemsPRILL: TFIBStringField;
    frReferencedInvoiceItems: TfrDBDataSet;
    frxHeader: TfrxDBDataset;
    taVedomostToPrint2ITYPE: TFIBIntegerField;
    taVedomostToPrint2ACCIN: TFIBBCDField;
    taVedomostToPrint2ACCRET: TFIBBCDField;
    taVedomostToPrint2ACCOUT: TFIBBCDField;
    procedure frDesignerSaveReport(Report: TfrReport; var ReportName: String; SaveAs: Boolean; var Saved: Boolean);
    procedure taDocNewRecord(DataSet: TDataSet);
    procedure quMolBeforeOpen(DataSet: TDataSet);
    procedure quLossBeforeOpen(DataSet: TDataSet);
    procedure taLossOperBeforeOpen(DataSet: TDataSet);
    procedure taJBeforeOpen(DataSet: TDataSet);
    procedure taMOLOperBeforeOpen(DataSet: TDataSet);
    procedure taIn_GrBeforeOpen(DataSet: TDataSet);
    procedure taMol_GrBeforeOpen(DataSet: TDataSet);
    procedure taMolOper_GrBeforeOpen(DataSet: TDataSet);
    procedure taJ_MolBeforeOpen(DataSet: TDataSet);
    procedure taCalcFBeforeOpen(DataSet: TDataSet);
    procedure frReportBeginBand(Band: TfrBand);
    procedure taCalcInBeforeOpen(DataSet: TDataSet);
    procedure taMol_TBeforeOpen(DataSet: TDataSet);
    procedure taJ_MatPSBeforeOpen(DataSet: TDataSet);
    procedure taCalcF_PSBeforeOpen(DataSet: TDataSet);
    procedure taJ_MatPS1BeforeOpen(DataSet: TDataSet);
    procedure taCalcF_PSOperBeforeOpen(DataSet: TDataSet);
    procedure taInvToPrintBeforeOpen(DataSet: TDataSet);
    procedure taVedomostToPrintBeforeOpen(DataSet: TDataSet);
    procedure taACTRePriceBeforeOpen(DataSet: TDataSet);
    procedure UGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure USetText(Sender: TField; const Text: String);
    procedure taPSOperBeforeOpen(DataSet: TDataSet);
    procedure taSJ_PSOperBeforeOpen(DataSet: TDataSet);
    procedure taCalcSF_PSOperBeforeOpen(DataSet: TDataSet);
    procedure taSItemsPrintAfterScroll(DataSet: TDataSet);
    procedure taStoneItemsBeforeOpen(DataSet: TDataSet);
    procedure taVedomostToPrint2BeforeOpen(DataSet: TDataSet);
    procedure taVedToPrint2_SUMBeforeOpen(DataSet: TDataSet);
    procedure taProtPSOperBeforeOpen(DataSet: TDataSet);
    procedure taProtPSBeforeOpen(DataSet: TDataSet);
    procedure taSJInfoBeforeOpen(DataSet: TDataSet);
    procedure taSJ_OperTBeforeOpen(DataSet: TDataSet);
    procedure taSJ_PSTBeforeOpen(DataSet: TDataSet);
    procedure taSJ_PSTailsBeforeOpen(DataSet: TDataSet);
    procedure taProtTails_dBeforeOpen(DataSet: TDataSet);
    procedure taSRItemBeforeOpen(DataSet: TDataSet);
    procedure taVSheetBeforeOpen(DataSet: TDataSet);
    procedure taVSheetMatBeforeOpen(DataSet: TDataSet);
    procedure taVSheetMat1BeforeOpen(DataSet: TDataSet);
    procedure taInv16AssortBeforeOpen(DataSet: TDataSet);
    procedure taPItem_PSBeforeOpen(DataSet: TDataSet);
    procedure taPArt_PSBeforeOpen(DataSet: TDataSet);
    procedure taBulkItemBeforeOpen(DataSet: TDataSet);
    procedure taKolieReportBeforeOpen(DataSet: TDataSet);
    procedure taAffinajBeforeOpen(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure quInv16ItemCalcFields(DataSet: TDataSet);
    procedure UQGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure UWGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taSJ_TBeforeOpen(DataSet: TDataSet);
    procedure quInv16ItemBeforeOpen(DataSet: TDataSet);
    procedure taProtPSOperAfterOpen(DataSet: TDataSet);
    procedure taProtPSOperAfterScroll(DataSet: TDataSet);
    procedure taSJFact_MatAfterOpen(DataSet: TDataSet);
    procedure taSJFact_MatAfterScroll(DataSet: TDataSet);
    procedure taSJFact_PSBeforeOpen(DataSet: TDataSet);
    procedure taSJFact_PSFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure quInv16ItemAfterScroll(DataSet: TDataSet);
    procedure taVSheetAssortBeforeOpen(DataSet: TDataSet);
    procedure taVSheetOperAssortAfterOpen(DataSet: TDataSet);
    procedure taVSheetOperAssortAfterScroll(DataSet: TDataSet);
    procedure taSelfBeforeOpen(DataSet: TDataSet);
    procedure frReportBeforePrint(Memo: TStringList; View: TfrView);
    procedure DataModuleDestroy(Sender: TObject);
    procedure taPMat_PsBeforeOpen(DataSet: TDataSet);
    procedure taPOper_PSBeforeOpen(DataSet: TDataSet);
    procedure taProtocol_PSBeforeOpen(DataSet: TDataSet);
    procedure taPOper_PSAfterOpen(DataSet: TDataSet);
    procedure taSielToPrintAfterOpen(DataSet: TDataSet);
    procedure taINVHeaderBeforeOpen(DataSet: TDataSet);
    procedure taProdApplRespBeforeOpen(DataSet: TDataSet);
    function frxDesignerSaveReport(Report: TfrxReport;
      SaveAs: Boolean): Boolean;
    procedure taSumOrNotSOElBeforeOpen(DataSet: TDataSet);
    function frxReportUserFunction(const MethodName: string;
      var Params: Variant): Variant;
  private
//    FOperation : string;
//    FMol : integer;
    //Ins: String;
    FJBd, FJEd : TDateTime;
    FProtocolId: integer;
    FReportSetting: TReportSetting;
    FUseDotMatrix : boolean;
    FTagSource: byte;
    procedure Inv16GetValue(const ParName: String; var ParValue: Variant);
    procedure Inv13GetValue(const ParName: String; var ParValue: Variant);
    procedure OperOrderGetValue(const ParName: String; var ParValue: Variant);
    procedure JMatPSGetValue(const ParName: String; var ParValue: Variant);
    procedure ActEGetValue(const ParName: String; var ParValue: Variant);
    procedure ObGetValue(const ParName: String; var ParValue: Variant);
    procedure Ob_dGetValue(const ParName : String; var ParValue : Variant);
    procedure ZpGetValue(const ParName: String; var ParValue: Variant);
    procedure ZpTotalGetValue(const ParName: String; var ParValue: Variant);
    procedure ProtocolGetValue(const ParName: String; var ParValue: Variant);
    procedure Protocol_dGetValue(const ParName: String; var ParValue: Variant);
    procedure WhApplGetValue(const ParName: String; var ParValue: Variant);
    procedure AInvGetValue(const ParName: String; var ParValue: Variant);
    procedure ProdApplGetValue(const ParName: String; var ParValue: Variant);
    procedure ProdAnalizGetValue(const ParName: String; var ParValue: Variant);
    procedure WhArtGetValue(const ParName: String; var ParValue: Variant);
    procedure VSheetGetValue(const ParName: String; var ParValue: Variant);
    //procedure InvGetValue(const ParName: String; var ParValue: Variant);
    //procedure StoreGetValue(const ParName: String; var ParValue: Variant);
    procedure ArtInsGetValue(const ParName: String; var ParValue: Variant);
    procedure TagGetValue(const ParName : string; var ParValue:Variant);
    procedure VedGetValue(const ParName : string; var ParValue:Variant);
    procedure LoadFrxDoc(DocId: integer);
    procedure LoadDoc(DocId : integer);
    procedure SetGroupParams(Params : TFIBXSQLDA);
    procedure SetJPeriodParams(Params : TFIBXSQLDA);
    procedure InitJPeriod;
    procedure PrepareTag; // �������� ��������� ������ ��� ������ �����
  private
    FBCPrinterLibHandle: THandle; // ����� ���������� ��� ������ � ��������� ��������
    Procedure SetDataSetParams(DataSet: TpFIBdataSet; Params: array of Variant);
  public
    FDocId : integer;
    FCap:string;
    FDATE:TDateTime;
    FNo:String;
    Group : string;
    GroupId : integer;
    NType:integer; //��� ���������� ��������� ���������
    NSubType:integer; //��� ���������� ��������� ���������
    T0:integer; // ��� ����� ��� ������ ��������� ���������
    MatrixReport : IZReport;
    Ins: String;
       (* �������� ��� ������ ����� 0 - ���������, 1 - �� ������ ������� *)
    property TagSource : byte read FTagSource write FTagSource;
    procedure InvGetValue(const ParName: String; var ParValue: Variant);
    procedure InsGetValueFR4(const VarName: String; var Value: Variant);
    procedure Inv15Topr12GetValue(const ParName: String; var ParValue: Variant);
    procedure EmptyInvGetValue(const ParName : string; var ParValue:Variant);
    procedure TollingActsGetValue(const ParName: string; var ParValue: Variant);    
    procedure InsGetValue(const ParName: String; var ParValue: Variant);
    procedure TechnologyGroupsGetValue(const ParName: String; var ParValue: Variant);
    procedure TechnologySemisGetValue(const ParName: String; var ParValue: Variant);
    property ReportSetting: TReportSetting read FReportSetting;
    procedure InitReportSetting;
    procedure ShowDoc;
    procedure PrintDocumentA(DocKind : TDocKind; const Params : Variant);
    procedure PrintFrxDocument(TemplateID: integer; HeaderParams: array of Variant; ItemsParams: array of Variant);
    property ProtocolId : integer read FProtocolId write FProtocolId;
    // ������� ��� ������ � ��������� ��������
    function BarCode_IsReady : boolean;
    procedure BarCode_OpenPort;
    procedure BarCode_ClosePort;
    procedure BarCode_DownloadImage(memt : byte); // 0 - internal memory, 1- external memory
    procedure BarCode_Backward(step: integer = 1);
    procedure BarCode_Forward(step: integer = 1);
    procedure BarCode_sendcommand(Cmd: string);
    //function PrintSticker(UID:integer):boolean;     //������ �� ������ �������� GODEX. �� ������������
    function PrintStickerNew(UID:integer; FromHistory: boolean):boolean;  //������ �� ����� �������� Zebra LP 2824
  end;

  // ���������� �������
  TProductionLib = class(TfrFunctionLibrary)
  public
    constructor Create; override;
    procedure DoFunction(FNo: Integer; p1, p2, p3: Variant; var val: Variant); override;
  end;


var
  dmPrint: TdmPrint;


implementation

uses dbUtil, DictData, MainData, Variants, InvData, Month, ApplData, ADistr, IniFiles,
  DotMatrixReport, UtilLib, PrintData2, fmUtils,
  {$ifdef GODEX}GODEXIntf,{$endif} Main, ExtCtrls, QStrings,
  uUtils, SOEl, dmReportInventory, SList, TechnologyGroups, VerificationActTolling;

{$R *.DFM}

const
  FRex = '.frf';

//var
 //Form5: TForm5;

{ TdmPrint }

procedure TdmPrint.DataModuleCreate(Sender: TObject);
begin
  InitReportSetting;
  FBCPrinterLibHandle := 0;
  {$ifdef TRIAL}
  frReport.PreviewButtons := frReport.PreviewButtons - [pbPrint];
  {$endif}
end;

procedure TdmPrint.ShowDoc;
begin
  if FUseDotMatrix then
    case dm.DocPreview of
      pvPreview, pvDesign : MatrixReport.Preview;
      pvPrint : MatrixReport.Print;
    end
  else
    case dm.DocPreview of
      pvPreview : frReport.ShowReport;
      pvDesign  : frReport.DesignReport;
      pvPrint   : begin
        frReport.PrepareReport;
        frReport.PrintPreparedReport('', frReport.DefaultCopies, frReport.DefaultCollate, frAll);
      end;
    end;
end;

procedure TdmPrint.Inv16GetValue(const ParName: String; var ParValue: Variant);
begin
  if (ParName = '��.���.���-��')then
    case quInv16ItemUQ.AsInteger of
      0 : ParValue := '��';
      1 : ParValue := '����';
      else ParValue := '-';
    end;
end;

procedure TdmPrint.frDesignerSaveReport(Report: TfrReport; var ReportName: String; SaveAs: Boolean; var Saved: Boolean);
begin
 if dm.IsAdm then
  begin
    if dm.DocPreview = pvDesign then
    begin
      OpenDataSets([taDoc]);

      if not taDoc.Locate('Id', FDocId, []) then
      begin
        taDoc.Insert;
        taDocID.AsInteger := FDocId;
        taDoc.Post;
        taDoc.Transaction.CommitRetaining;
      end;

      Report.SaveToDB(taDoc, FDocId);

      CloseDataSets([taDoc]);

      Beep;

    end;
  end;
end;

procedure TdmPrint.taDocNewRecord(DataSet: TDataSet);
begin
  taDocID.AsInteger := FDocId;
end;

procedure TdmPrint.LoadDoc(DocId: integer);
begin
  taDoc.Open;
  FDocId := DocId;
  frReport.LoadFromDB(taDoc, FDocId);
  taDoc.Close;
end;

procedure TdmPrint.SetDataSetParams(DataSet: TpFIBDataSet; Params: array of Variant);
var
  i: integer;
begin

  if DataSet.Active then
  begin
    DataSet.Close;
  end;

  if DataSet.ParamCount <> length(Params) then
  begin
    MessageDlg('���������� ���������� �� ���������!', mtError, [mbOk], 0);
    Exit;
  end;

  for i := low(Params) to high(Params) do
  begin
    DataSet.Params[i].AsVariant := Params[i];
  end;

  try
    DataSet.Active := true;
  except
    On E: Exception do
    begin
      ShowMessage(E.Message);
    end;
  end;

end;

procedure TdmPrint.PrintFrxDocument(TemplateID: integer; HeaderParams: array of Variant; ItemsParams: array of Variant);
var
  i: Integer;
  DataSet: TpFIBDataSet;
begin

  FDocID := TemplateID;

  DataSet := TpFIBDataSet(frxHeader.DataSet);

  SetDataSetParams(DataSet, HeaderParams);

  DataSet := TpFIBDataSet(frxItems.DataSet);

  SetDataSetParams(DataSet, ItemsParams);

  frxReport.AddFunction('function �����������(Value: Double):String');

  frxReport.AddFunction('function �����������������(Value: Double):String');

  LoadFrxDoc(TemplateID);

  frxReport.PrepareReport;

  if dm.IsAdm then
  begin
    frxReport.DesignReport;
  end else
  begin
    frxReport.PreviewOptions.AllowEdit := false;
    frxReport.ShowPreparedReport;
  end;
  
end;

procedure TdmPrint.LoadFrxDoc(DocId: integer);
var
  Stream: TMemoryStream;
begin
  taDoc.Open;
  if taDoc.Locate('ID', DocID, []) then
  begin
    if not taDocDOC.IsNull then
    begin
      try
        Stream := TMemoryStream.Create;
        taDocDOC.SaveToStream(Stream);
        Stream.Position := 0;
        frxReport.LoadFromStream(Stream);
      finally
        FreeAndNil(Stream);
      end;
    end else
    begin
      MessageDlg('������ � ���� �� �������� ������� (id =' + IntToStr(DocID) + ')!', mtWarning, [mbOk], 0);
    end;
  end else
  begin
    MessageDlg('����������� ������ � ������� Doc (id =' + IntToStr(DocID) + ')!', mtWarning, [mbOk], 0);
  end;
  taDoc.Close;
end;

procedure TdmPrint.quMolBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    Byname['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
  end;
end;

procedure TdmPrint.TechnologySemisGetValue(const ParName: string; var ParValue: Variant);
begin
  if ParName = 'Start$Date' then ParValue := fmtechnologyGroups.Print.ParamByName('Start$Date').AsString;
  if ParName = 'End$Date' then ParValue := fmtechnologyGroups.Print.ParamByName('End$Date').AsString;
end;

procedure TdmPrint.Inv15Topr12GetValue(const ParName: string; var ParValue: Variant);
begin
  if (ParName = '����� ���')then
    ParValue := GetCapitalsWeight(dmMain.taSOListW.AsFloat);
  if (ParName = '����� � ������2')then
    ParValue := GetCapitalsMoney(dmMain.taSOListCOST.AsFloat);
end;

procedure TdmPrint.quLossBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
    ByName['OPERID'].AsString := taOperOPERID.AsString;
  end
end;

procedure TdmPrint.taLossOperBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
  end;
end;

procedure TdmPrint.taJBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
  end;
end;

procedure TdmPrint.taMOLOperBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
  end;
end;

procedure TdmPrint.taIn_GrBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(StrToDateTime('01.01.1900'));
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
  end;
end;

procedure TdmPrint.taMol_GrBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
  end;
end;

procedure TdmPrint.taMolOper_GrBeforeOpen(DataSet: TDataSet);
begin
  dm.SetPeriodParams(TpFIBDataSet(DataSet).Params);
end;

procedure TdmPrint.taJ_MolBeforeOpen(DataSet: TDataSet);
begin
  dm.SetPeriodParams(TpFIBDataSet(DataSet).Params);
end;

procedure TdmPrint.taCalcFBeforeOpen(DataSet: TDataSet);
begin
  trPrint.CommitRetaining;
  SetJPeriodParams(TpFIBDataSet(DataSet).Params);
  SetGroupParams(TpFIBDataSet(DataSet).Params);
end;

procedure TdmPrint.SetGroupParams(Params : TFIBXSQLDA);
begin
  if GroupId = -1 then
    with Params do
    try
      ByName['MATID1'].AsInteger := -MAXINT;
      ByName['MATID2'].AsInteger := MAXINT;
    except
    end
  else
    with Params do
    try
      ByName['MATID1'].AsInteger := GroupId;
      ByName['MATID2'].AsInteger := GroupId;
    except
    end
end;

procedure TdmPrint.frReportBeginBand(Band: TfrBand);
begin
  {if(Band.Name = 'MasterFooter1')then ReOpenDataSets([taCalcF]);
  if(Band.Name = 'DetailFooter1')then ReOpenDataSets([taCalcF_PS]);}
end;

function TdmPrint.frxDesignerSaveReport(Report: TfrxReport;
  SaveAs: Boolean): Boolean;
var ReportStream: TStream;
begin
  try
    taDoc.Open;
    taDoc.Locate('ID', FDocID, []);
    taDoc.Edit;
    ReportStream := TMemoryStream.Create;
    Report.SaveToStream(ReportStream);
    taDocDOC.LoadFromStream(ReportStream);
  finally
    taDoc.Post;
    ReportStream.Free;
  end;
end;


function TdmPrint.frxReportUserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin

  if MethodName = '�����������' then
  begin
    Result := GetCapitalsWeight(Params[0]);
  end;


  if MethodName = '�����������������' then
  begin
    Result := GetCapitalsMoney(Params[0]);
  end;

end;

procedure TdmPrint.taCalcInBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['PSID'].Value := taPSDEPID.AsInteger;
    ByName['BD'].Value := dm.BeginDate;
    ByName['MATID'].Value := GroupId;
  end
end;

procedure TdmPrint.ActEGetValue(const ParName: String; var ParValue: Variant);
begin

end;

procedure TdmPrint.taMol_TBeforeOpen(DataSet: TDataSet);
begin
  dm.SetPeriodParams(TpFIBDataSet(DataSet).Params);
  dmPrint.SetGroupParams(TpFIBDataSet(DataSet).Params);
end;

procedure TdmPrint.ObGetValue(const ParName: String; var ParValue: Variant);
var
  s, h : string;
begin
 if(ParName='�����')then
 begin
   ParValue := '        �, '+taApplFIO.AsString+', ��������(�) �� ������ � '+
      DatetoStr(taApplBD.AsDateTime)+' �� '+DateToStr(taApplDOCDATE.AsDateTime);
   if taApplWZ.AsFloat <> 0 then s := ' ���������������� ������ ( '+
      FormatFloat('0.###', taApplWZ.AsFloat)+' ��.)' else s := '';
   if taApplH.AsFloat <> 0 then h := ' ��������� ' else h := '';

   if s <> '' then ParValue := ParValue + s;
   if h <> '' then
     if s <> '' then ParValue := ParValue + ' , '+h
     else ParValue := ParValue + h;
   ParValue := ParValue + ' ���������� ���������� ����������. ����� �������� '+
     '����� ';
   if s <> '' then ParValue := ParValue + '���������������� ������ ';
   if h <> '' then
     if s <> '' then ParValue := ParValue + ' , ��������� '
     else ParValue := ParValue + '��������� ';
   ParValue := ParValue + ' �� ���� ���������� �����';
 end;
end;

procedure TdmPrint.ZpGetValue(const ParName: String; var ParValue: Variant);
begin
  if(ParName='������')then ParValue := taZPBD.AsDateTime;
  if(ParName='�����')then ParValue := taZpED.AsDateTime;
end;

procedure TdmPrint.TechnologyGroupsGetValue(const ParName: String; var ParValue: Variant);
begin
  if ParName = '��������' then ParValue := fmTechnologyGroups.allGroupArticles;
end;

procedure TdmPrint.ZpTotalGetValue(const ParName: String; var ParValue: Variant);
begin
  if(ParName='������')then ParValue := taZPTotalBD.AsDateTime;
  if(ParName='�����')then ParValue := taZpTotalDOCDATE.AsDateTime;
end;

procedure TdmPrint.ProtocolGetValue(const ParName: String; var ParValue: Variant);
begin
  if(ParName='������')then ParValue := dm.BeginDate;
  if(ParName='�����')then ParValue := dm.EndDate;
  if(ParName='�������')then
  begin
    if taPItemWZ.AsInteger <> 0 then ParValue := '�������� �� �/�'
    else ParValue := '';
    if(taPItemI.AsFloat <> 0)or(taPItemH.AsFloat <> 0)then
      if(ParValue <> '')then ParValue := ParValue + ', �����������'
      else ParValue := '�����������';
  end;
end;

procedure TdmPrint.taJ_MatPSBeforeOpen(DataSet: TDataSet);
begin
  taJ_MatPS.ParamByName('DEPID').AsInteger := taPSDEPID.AsInteger;
  taJ_MatPS.ParamByName('OPERID').AsString := taOperOPERID.AsString;

  SetJPeriodParams(TpFIBDataSet(DataSet).Params);
  dmPrint.SetGroupParams(TpFIBDataSet(DataSet).Params);
end;

procedure TdmPrint.taCalcF_PSBeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('PSID').AsInteger := taPSDEPID.AsInteger;
  SetJPeriodParams(TpFIBDataSet(DataSet).Params);
  SetGroupParams(TpFIBDataSet(DataSet).Params);
end;

procedure tdmPrint.EmptyInvGetValue(const ParName : string; var ParValue:Variant);
begin
  if (ParName = '����������')then
    ParValue := GetCapitalsQuantity(dmInv.taEmptyInvQ.AsInteger);
  if (ParName = '����� ���')then
    ParValue := GetCapitalsWeight(dmInv.taEmptyInvW.AsFloat);
  if (ParName = '����� � ������2')then
    ParValue := GetCapitalsMoney(dmInv.taEmptyInvCOST.AsFloat * (1 + dmInv.taEmptyInvNDS.AsFloat/100) + dmInv.taEmptyInvTrans_Price.AsFloat);
end;

procedure TdmPrint.InvGetValue(const ParName: String; var ParValue: Variant);
begin
  if (ParName = '����������')then
    ParValue := GetCapitalsQuantity(taSItemsPrintQUANTITY.AsInteger);

  if (ParName = '����� ���')then
    ParValue := GetCapitalsWeight(taSItemsPrintTOTALWEIGHT.AsFloat);

  if (ParName = '����� � ������2')then
    ParValue := GetCapitalsMoney(taSItemsPrintSSUMF.AsFloat+taInvToPrintTRANS_PRICE.AsFloat);

  if (ParName = '��.���') then
  begin
    ReOpenDataSet(dminv.taConst);
    dminv.taConst.Locate('Name','������� ���������',[loCaseInsensitive]);
    ParValue:=dminv.taConstSTR_VAL.AsString;
  end;

  if (ParName = '��������') then
  begin
    ReOpenDataSet(dminv.taConst);
    dminv.taConst.Locate('Name','��������',[loCaseInsensitive]);
    ParValue:=dminv.taConstSTR_VAL.AsString;
  end;

{  if (ParName = '��������') and (taInvToPrintCOMMISION.AsInteger=1)
     then ParValue := '������� ��������';}
end;

procedure TdmPrint.TollingActsGetValue(const ParName: string; var ParValue: Variant);
const
  SertificatePrills = [4, 15, 16];
var
  PrillID: Integer;
  SemisID: String;
begin

  if (ParName = '��������') then
  begin

    ParValue := GetCapitalsMoney(dmMain.taSIListCOST.AsCurrency);

  end;

  if (ParName = 'ISAFFINAGE') then
  begin

    SemisID := dm.db.QueryValue('select first 1 semis from siel where InvID = :Invoice$ID', 0, [dmMain.taSIListINVID.AsInteger]);

    PrillID := StrToInt(VarToStr(dm.db.QueryValue('select prill from d_semis where semisid = :Semis$ID', 0, [SemisID])));

    if PrillID in SertificatePrills then
    begin

      ParValue := 0;

    end else
    begin

      ParValue := 1;

    end;

  end;

end;

procedure TdmPrint.Inv13GetValue(const ParName: string; var ParValue: Variant);
var NumberAsString: string;
    Index: integer;
begin
  if (ParName = '����������')then
    begin
      NumberAsString := GetCapitalsQuantity(dmMain.taSIListLinesCount.AsInteger);
      Index := pos('����', NumberAsString);
      delete(NumberAsString, Index, Length(NumberAsString) - Index);
      ParValue := NumberAsString;
    end;
  if (ParName = '����� ���')then
    ParValue := GetCapitalsWeight(dmMain.taSIListW.AsFloat);
  if (ParName = '����� � ������2')then
    ParValue := GetCapitalsMoney(dmMain.taSIListCOST.AsFloat);
end;

procedure TdmPrint.PrintDocumentA(DocKind: TDocKind; const Params: Variant);

  procedure Prepare(DocNo : integer; GetValue : TDetailEvent; DataSets : array of TDataSet; UseDotMatrix : boolean = False);
  var
    i, j : integer;
  begin
    FUseDotMatrix := UseDotMatrix;
    if not UseDotMatrix then
    begin
      LoadDoc(DocNo);
      frReport.OnGetValue := GetValue;
    end
    else
      case DocNo of
        1 : MatrixReport := fmDotMatrixReport.zrInv16In;
        2 : MatrixReport := fmDotMatrixReport.zrInv16Out;
        48 : MatrixReport := fmDotMatrixReport.zrInv16InAssort;
        49 : MatrixReport := fmDotMatrixReport.zrInv16OutAssort;
        50 : MatrixReport := fmDotMatrixReport.zrInv16In_Preview;
        51 : MatrixReport := fmDotMatrixReport.zrInv16Out_Preview;
        else MatrixReport := nil;
      end;

    CloseDataSets(DataSets);
      
    for i:=Low(DataSets) to High(DataSets) do
      begin
        if VarArrayHighBound(Params, 1)>Pred(i) then
          begin
            for j:=VarArrayLowBound(Params[i], 1) to VarArrayHighBound(Params[i], 1) do
              TpFIBDataSet(DataSets[i]).Params[j].Value := Params[i][j];
          end;
      end;


    if UseDotmatrix then
    begin
      case DocNo of
        1 : OpenDataSets([quInv16, quInv16Item]);
        2 : OpenDataSets([quInv16, quInv16Item]);
        48 : OpenDataSets([quInv16, quInv16Item]);
        49 : OpenDataSets([quInv16, quInv16Item]);
        50 : OpenDataSets([quInv16, quInv16Item]);
        51 : OpenDataSets([quInv16, quInv16Item]);
      end
    end
  end;

begin
  if trPrint.Active then trPrint.Commit;
  trPrint.StartTransaction;
  FUseDotMatrix := False;
  try
    case DocKind of
      dkSJ_d : begin
        LoadDoc(44);
        // �����������
        taProtPS.DataSource := nil;
        taProtPSOper.DataSource := dsrProtPS;
        //taSJ_PsOper.DataSource := dsrProtPsOper;
        //taSJ_OperT.DataSource := dsrProtPsOper;
        taSJ_PST.DataSource := dsrProtPS;
        taSJ_PSTails.DataSource := dsrProtPS;
        frReport.OnGetValue := JMatPSGetValue;
      end;
      dkOb         : Prepare(16, ObGetValue, [taAppl]);
      dkOb_d       : Prepare(42, Ob_dGetValue, [taOb_d]);
      dkProtocol   : Prepare(17, ProtocolGetValue, [taProtocol, taPItem]);
      dkProtocol_d : Prepare(43, Protocol_dGetValue, [taProtocol, taPItem_d]);
      dkZp         : Prepare(18, ZpGetValue, [taZp]);
      dkZpTotal    : Prepare(19, ZpTotalGetValue, [taZpTotal]);
      dkActE       : Prepare(14, ActEGetValue, [taActE]);
      dkIn: begin
         OpenDataSets([taUnit, dmINV.taINVItems]);
         Prepare(21, InvGetValue, [taSItemsPrint,taINVheader]);
      end;
       dkRet: begin
               ReOpenDataSets([taUnit,dmINV.taINVItems]);
               Prepare(22,InvGetValue, [taSItemsPrint,taINVheader ]);
              //              frReport.OnGetValue:=dmINV.InvGetValue;
//               LoadDoc(22);
              end;
//       dkSell
       // �������� ��������� (����-�������)
       dkSell_Facture : begin   //�������
         ReOpenDataSets([taUnit,dmINV.taINVItems,taINVTOPrint,taStoneItems]);
         Prepare(25, InvGetValue, [taSItemsPrint, taINVheader]);
       end;
       // �������� ���������
       dkSell_Invoice : begin
         ReOpenDataSets([taUnit, dmINV.taINVItems, taINVTOPrint, taStoneItems]);
         Prepare(23, InvGetValue, [taSItemsPrint, taINVheader]);
       end;
       // ���� �� ������ (�� ��������� �������)
       dkSell_Chit : begin
         ReOpenDataSets([taUnit, dmInv.taINVItems, taInvToPrint, taStoneItems]);
         Prepare(81, InvGetValue, [taSItemsPrint, taInvHeader]);
       end;
       // �������� ��������� �� �������
       dkSellRet: begin
                   ReOpenDataSets([taUnit,dmINV.taINVItems,taINVTOPrint, taStoneItems]);

                   Prepare(23, InvGetValue, [taSItemsPrint,taINVheader ]);
                   ShowDoc;
                   Prepare(27, InvGetValue, [taSItemsPrint,taINVheader ]);
                   CloseDataSets([taINVTOPrint, taStoneItems,taSItemsPrint])
                 end;
       dkJournalOrder: begin
                   ReOpenDataSets([taVedomostToPrint]);
//                   frReport.OnGetValue:=dmINV.InvGetValue;
                   LoadDoc(26);
                 end;
       dkJournalOrder2: begin
                   ReOpenDataSets([taVedomostToPrint2,taVedToPrint2_SUM]);
                   frReport.OnGetValue:=VedGetValue;
                   LoadDoc(39);
                 end;
       dkActRePrice: begin
                      ReOpenDataSets([taACTRePrice, dmInv.taPactsPrint]);
                      LoadDoc(28);
                     end;
       dkWhAppl : Prepare(29, WhApplGetValue, []);
       dkAInv : begin
         LoadDoc(30);
         frReport.OnGetValue := AInvGetValue;
       end;
       dkProdAppl : Prepare(32, ProdApplGetValue, [taProdAppl, taProdApplRoot, taProdApplRing]);

       dkOrderReport : Prepare(101, nil, [taOrderReport]);

       dkProdAnaliz : Prepare(31, ProdAnalizGetValue, [taSellAnaliz]);
       dkWHSemis_Transfer : begin
         taDIELToPrint.DataSource := nil;
         taAELToPrint.DataSource := dsrDIEl;
         Prepare(36, InvGetValue, [taINVHeader,taDIELToPrint]);

       end;
       dkWhArt : Prepare(37, WhArtGetValue, []);
       dkSIList_Stone: begin
                  Prepare(38, InvGetValue, [taSIListToPrint,taSIELToPrint]);
                 end;
       dkSIEl_M15 : Prepare(6, Inv13GetValue, [taSIListToPrint, taSIELToPrint]);
       dkSIList_RequirementInvoice: begin
                  Prepare(10001, InvGetValue, [taSIListToPrint,taSIELToPrint]);
                 end;

       dkSIList_Metall: begin
                  Prepare(41, InvGetValue, [taSIListToPrint,taSIELToPrint]);
                 end;

        dkActGoldForReadyProduct:
          begin

            Prepare(121, TollingActsGetValue, [taSIListToPrint, taSIELToPrint, taReferencedInvoiceItems]);

          end;

        dkActMutualClaims:
          begin

            Prepare(122, TollingActsGetValue, [taSIListToPrint, taSIELToPrint]);

          end;

        dkActUnknownCrude:
          begin

            Prepare(120, TollingActsGetValue, [taSIListToPrint, taSIELToPrint]);

          end;

       dkWhS : Prepare(40, nil, []);
       dkSoel: Prepare(45, nil, [taINVHeader,taSOelToPrint]);
       dkSemisReport: begin
            // ��������� �������� �������������� � ��������
            Prepare(46, nil, [taSemisReport, taSRItem, taVSheetMat1]);
            taVSheetMat1.ParamByName('ACTID').AsInteger := Params[2][0];
         end;
       // ������������ ���������
       dkVSheet : begin
         Prepare(47, VSheetGetValue, [taVList, taVSheet, taVSheetMat]);
         taVSheetMat.ParamByName('ACTID').AsInteger := Params[2][0];
       end;
       // ��������� ��-16
       dkInv16In : Prepare(1, Inv16GetValue, [quInv16], ReportSetting.UseDotMatrix);
       dkInv16Out : Prepare(2, Inv16GetValue, [quInv16], ReportSetting.UseDotMatrix);
       // ��������� ��-16 �� ������ � �������������
       dkInv16InAssort : begin
         taInv16Assort.DataSource := dsrInv16Item;
         Prepare(48, nil, [quInv16], ReportSetting.UseDotMatrix);
       end;
       dkInv16OutAssort : begin
         taInv16Assort.DataSource := dsrInv16Item;
         Prepare(49, nil, [quInv16], ReportSetting.UseDotMatrix);
       end;
       // ��������������� ��������� ��-16
       dkInv16In_Preview : Prepare(50, Inv16GetValue, [quInv16], ReportSetting.UseDotMatrix);
       dkInv16Out_Preview : Prepare(51, Inv16GetValue, [quInv16], ReportSetting.UseDotMatrix);
       //
       dkProtocol_PS : begin
         //taPItem_PS.DataSource := dsrPOper_PS;
         //taPOper_PS.DataSource := dsrPMat_PS;
         //taPMat_PS.DataSource := dsrProtocol_PS;
         //taPArt_PS.DataSource := dsrProtocol_PS;
         Prepare(10052, nil, [taProtocol_PS]);
       end;
       dkDictArt : begin
         Prepare(53, nil, []);
       end;
       // ��������� �� ������� �����
       dkAffS_Inv : begin
         taAffinaj.DataSource := dsrAffInv;
         Prepare(97, nil, [taAffInv, taAffinaj]);
       end;
       // ��������� �� ������� ����
       dkAffL_Inv : begin
         taAffinaj.DataSource := dsrAffInv;
         Prepare(96, nil, [taAffInv, taAffinaj]);
       end;
       dkLostStones: begin
         Prepare(56, nil, []);
       end;
       // ��� ������ ����������� ������
       dkBulkAct : begin
         taBulkItem.DataSource := dsrBulk;
         Prepare(57, nil, [taBulk]);
       end;
       dkKolie : begin  // ����� �� �������
         ReOpenDataSets([taKOLIEReport]);
         LoadDoc(58);
       end;
       dkInvReport : begin  //
         taInvChangeReport.ParamByName('ADATE').AsDate := Params[0];
         ReOpenDataSets([taInvChangeReport, taDelInv]);
         LoadDoc(59);
       end;
       dkProdReport : begin  // ������ �� �����������
         ReOpenDataSets([dm.taRec, taDelInv]);
         LoadDoc(60);
       end;
       dkOperOrder : begin  // �����
         Prepare(62, OperOrderGetValue , []);
       end;
       dkAssortDet : begin  // �����
         Prepare(63, OperOrderGetValue , []);
       end;
       dkArtIns : begin
         Prepare(64, ArtInsGetValue, []);
       end;
       dkStoreByItems : begin  // �����
         //ReOpenDataSets([taStoreByItems]);
         Prepare(65, nil, [taStoreByItems]);
//         LoadDoc(65);
       end;
       dkStoneInInv : Prepare(66, nil, []);
       dkTag : Prepare(67, TagGetValue, []);
       dkTag2 : begin
         Prepare(68, nil, []);
         PrepareTag;
       end;
       dkSemisInInv : Prepare(69, nil, []);
       dkAff_Order : begin
         //taAffinaj.DataSource := dsrAffInv;
         Prepare(70, nil, [taAffInv, taAffinaj]);
       end;
       dkArtDiamond : Prepare(71, nil, []);
       dkZpAssort : Prepare(72, nil, [taZpAssort]);
       dkDetailArt2Ins : Prepare(73, nil, []);
       dkVSheetAssort : begin
         Prepare(74, nil, [taVSheetOperAssort, taVSheetAssort]);
       end;
       dkSIList_ActSemisGet : begin
         Prepare(75, InvGetValue, [taSIListToPrint,taSIELToPrint]);
       end;
       //��������� ������ ���������� �� �������
       dkSOList_Inv15 :
         Prepare(76, nil, [taInvoiceHeader, taSOElToPrint]);
       // ��� ������-����� ����� (�����)
       dkActWork : begin
         Prepare(77, nil, []);
       end;
       // ����� � �����������
       dkProdTolling: begin
         Prepare(78, nil, []);
       end;
       // ����� �� ������������ ��� ���������
       dkCompManager : begin
         Prepare(79, nil, [dmPrint2.taComp]);
       end;
       dkBuyer_Request : begin
         Prepare(80, nil, [dmPrint2.taRequest]);
       end;
       dkProtocol_PS_Balance : begin
         dmPrint2.taPItem_PS_Balance.DataSource := dsrProtocol_PS;
         Prepare(82, nil, [taProtocol_PS]);
       end;
       dkActWorkGeneral : begin
         Prepare(83, nil, [dmPrint2.taAWG, dmPrint2.taAWG_Null]);
         OpenDataSet(dmPrint2.taAWG_Head);
         dmPrint2.taAWG_Head.Edit;
         dmPrint2.taAWG_HeadBD.AsDateTime := Params[2][0];
         dmPrint2.taAWG_HeadED.AsDateTime := Params[2][1];
         dmPrint2.taAWG_HeadSELFCOMPNAME.AsString := Params[2][2];
         dmPrint2.taAWG_HeadSUPNAME.AsString := Params[2][3];
         dmPrint2.taAWG_Head.Post;
       end;
       dkVerificationActTolling: begin
         Prepare(84, nil, [dmPrint2.taVerificationHeader, dmPrint2.taVerificationAct]);
       end;
       dkInvMX18 : begin
         Prepare(85, nil, [dmPrint2.taMX18, dmPrint2.taMX18Item]);
       end;
       dkActWorkRetServ : begin
         Prepare(86, nil, [dmPrint2.taAWRetServ]);
       end;
       dkActWorkRej : begin
         Prepare(87, nil, [dmPrint2.taAWRej]);
       end;
       dkAWTorg12 : begin // ����-12 �� ������� �� ��������
         Prepare(88, nil, [dmPrint2.taAWTorg12, dmPrint2.taAWTorg12Item]);
       end;
       dkRevision : begin //
         Prepare(89, nil, [dmInv.taRev_det, dmPrint2.taRevision_Sum]);
       end;
       dkInvArtPhoto : begin
         Prepare(90, nil, [dmPrint2.taInvArtPhoto]);
       end;
       dkAWMOTotal : begin
         Prepare(91, nil, [dmPrint2.taAWMOTotal]);
       end;
       dkInvUID : begin
         Prepare(92, nil, [taINVTOPrint, dmPrint2.taInvUID]);
       end;
       dkZpK : begin // ���������� ��������� �������� � �\�
         Prepare(93, nil, [dmPrint2.taZpKSemis, dmPrint2.taZpKTech, dmPrint2.taZpKIns, dmPrint2.taZpKW, dmPrint2.taZpKArt]);
       end;
       // ����� � ����������� (�������)
       dkProdTolling_External: begin
         Prepare(94, nil, [dmPrint2.taPTHead_Ext, dmPrint2.taPT_Ext, dmInv.taSListCalcPrihod]);
       end;
       // ��� ������-����� ����� (�����) (������� �����������)
       dkActWork_External : begin
         Prepare(95, nil, [dmPrint2.taAW_Ext, dmPrint2.taAW_SelfMat_Ext]);
       end;
       // �������� � ������� ����������
       dkMatBalance_Move: begin
         Prepare(98, nil, [dmPrint2.taMB_Move]);
       end;
       //���� ���������� ���������
       dkArtAll: begin
         Prepare(100, nil, []);
       end;

       // ???

       dkActWorkCommon : begin
         Prepare(10002, nil, []);
       end;

       dkProdTollingCommon: begin
         Prepare(10003, nil, []);
       end;

       dkInvMX18Common : begin
         Prepare(10004, nil, [dmPrint2.taMX18Common]);
       end;

    end;
    trPrint.CommitRetaining;

    ShowDoc;
  finally
    if trPrint.Active then trPrint.Commit;
  end;
end;

procedure TdmPrint.taJ_MatPS1BeforeOpen(DataSet: TDataSet);
begin
  TpFIBDataSet(DataSet).ParamByName('PSID').AsInteger := taPSDEPID.AsInteger;
  TpFIBDataSet(DataSet).ParamByName('OPERID1').AsString:= taOperOPERID.AsString;
  TpFIBDataSet(DataSet).ParamByName('OPERID2').AsString := taOperOPERID.AsString;
  SetJPeriodParams(TpFIBDataSet(DataSet).Params);
  SetGroupParams(TpFIBDataSet(DataSet).Params);
end;

procedure TdmPrint.taCalcF_PSOperBeforeOpen(DataSet: TDataSet);
begin
  taCalcF_PSOper.ParamByName('PSID').AsInteger := taPSDEPID.AsInteger;
  taCalcF_PSOper.ParamByName('OPERID').AsString := taOperOPERID.AsString;
  SetJPeriodParams(TpFIBDataSet(DataSet).Params);
  SetGroupParams(TpFIBDataSet(DataSet).Params);
end;

procedure TdmPrint.JMatPSGetValue(const ParName: String; var ParValue: Variant);
begin
  if(ParName='������ �������')then ParValue := FJBd;
  if(ParName='����� �������')then ParValue := FJEd;
  if(ParName='������')then ParValue := Group;
  if(ParName='��� ��������')then
    case taJ_MatPSITTYPE.AsInteger of
      0 : ParValue := '������';
      1 : ParValue := '�����';
      3 : ParValue := '�������';
      5 : parValue := '����';
      6 : ParValue := '���������';
    end;
  if(ParName='����� ��������')then ParValue := taPSNAME.AsString;
end;

procedure TdmPrint.SetJPeriodParams(Params : TFIBXSQLDA);
begin
  with Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(FJBd);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(FJEd);
  end;
end;

procedure TdmPrint.InitJPeriod;
var
  bd, ed : Variant;
begin
  bd := ExecSelectSQL('select max(Bd) from Act', dmMain.quTmp);
  ed := ExecSelectSQL('select max(DocDate) from Act', dmMain.quTmp);

  if VarIsNull(bd) then FJBd := dm.BeginDate else FJBd := bd;
  if VarIsNull(ed) then FJEd := dm.EndDate else FJEd := ed;
end;

procedure TdmPrint.taInvToPrintBeforeOpen(DataSet: TDataSet);
begin
  if VarIsNull(dmInv.taSListINVID.AsVariant) then
    taInvToPrint.ParamByName('INVID').AsInteger := dmInv.FCurrInvID
  else
    taInvToPrint.ParamByName('INVID').AsInteger := dmInv.taSListINVID.AsInteger;
end;

procedure TdmPrint.taVedomostToPrintBeforeOpen(DataSet: TDataSet);
begin
  taVedomostToPrint.ParamByName('ACTID').AsInteger:=dminv.taAVListID.AsInteger;
end;

procedure TdmPrint.taACTRePriceBeforeOpen(DataSet: TDataSet);
begin
 taACTRePrice.ParamByName('INVID').AsInteger:=dmInv.taSListINVID.Asinteger;
end;

procedure TdmPrint.WhApplGetValue(const ParName: String; var ParValue: Variant);
begin
  if(ParName='����� ��������')then ParValue := fmADistr.DepFrom;
end;

procedure TdmPrint.UGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
  case Sender.AsInteger of
    1 : Text := '��.';
    2 : Text := '����';
    else Text := '�� ����������'
  end;
end;

procedure TdmPrint.USetText(Sender: TField; const Text: String);
begin
  if(Text = '��.')then Sender.AsInteger := 1
  else if(Text = '����')then Sender.AsInteger := 2
  else Sender.Clear;
end;

procedure TdmPrint.AInvGetValue(const ParName: String; var ParValue: Variant);
begin
  if(ParName = '�����')then ParValue := dmMain.taWOrderListDOCNO.AsString;
  if(ParName = '���������')then ParValue := dmMain.taWOrderDOCNO.AsString;
  if(ParName = '������ ����� ��������')then ParValue := fmADistr.DepFrom;
  if(ParName = '���� ����� ��������')then ParValue := fmADistr.DepTo;
  if(ParName = '������ ���')then ParValue := fmADistr.JobFrom;
  if(ParName = '���� ���')then ParValue := fmADistr.JobTo;
end;

procedure TdmPrint.ProdApplGetValue(const ParName: String; var ParValue: Variant);
begin
  
end;

procedure TdmPrint.ProdAnalizGetValue(const ParName: String; var ParValue: Variant);
begin
  
end;

procedure TdmPrint.taPSOperBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['DEPID'].AsInteger := taPSDEPID.AsInteger;
end;

procedure TdmPrint.taSJ_PSOperBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['PROTOCOLID'].AsInteger := ProtocolId;
    ByName['PSID'].AsInteger := taProtPSPSID.AsInteger;
    ByName['OPERID'].AsString:= taProtPSOperOPERID.AsString;
  end;
end;

procedure TdmPrint.taCalcSF_PSOperBeforeOpen(DataSet: TDataSet);
begin
  taCalcSF_PSOper.ParamByName('PSID').AsInteger := taPSDEPID.AsInteger;
  taCalcSF_PSOper.ParamByName('OPERID').AsString := taPSOperOPERID.AsString;
  SetJPeriodParams(TpFIBDataSet(DataSet).Params);
  SetGroupParams(TpFIBDataSet(DataSet).Params);
end;

procedure TdmPrint.WhArtGetValue(const ParName: String; var ParValue: Variant);
begin
  
end;

procedure TdmPrint.taSItemsPrintAfterScroll(DataSet: TDataSet);
begin
  ReOpenDataSet(taStoneItems);
end;

procedure TdmPrint.taStoneItemsBeforeOpen(DataSet: TDataSet);
begin
 if ((taSItemsPrint.Active) and (not taSItemsPrint.IsEmpty)) then
   taStoneItems.ParamByName('ART2ID').ASInteger:=taSItemsPrintART2ID.AsInteger
 else
   if ((dmPrint2.taInvUID.Active) and (not dmPrint2.taInvUID.IsEmpty)) then
     taStoneItems.ParamByName('ART2ID').ASInteger := dmPrint2.taInvUIDArt2ID.AsInteger;

end;

procedure TdmPrint.taSumOrNotSOElBeforeOpen(DataSet: TDataSet);
begin
  taSumOrNotSOEl.ParamByName('inv_id').asInteger:=dmMain.taSOList.FieldByName('INVID').AsInteger;
end;

procedure TdmPrint.taVedomostToPrint2BeforeOpen(DataSet: TDataSet);
begin
  taVedomostToPrint2.ParamByName('BD').AsTimeStamp :=DateTimeToTimeStamp(fmMonth.VED_BD);
  taVedomostToPrint2.ParamByName('ED').AsTimeStamp :=DateTimeToTimeStamp(fmMonth.VED_ED);
  taVedomostToPrint2.ParamByName('T0').AsInteger :=T0;
  taVedomostToPrint2.ParamByName('FILTER$MATERIAL').AsInteger := dmPrint.NSubType;
  //ShowMessage('FUserId: ' + IntToStr(dm.quUserMOLID.AsInteger));
  //taVedomostToPrint2.ParamByName('FUserID').AsInteger := dm.quUserMolID.AsInteger;
  //showmessage('filter$mat: ' + taVedomostToPrint2.ParamByName('FILTER$MATERIAL').AsString + '  T0: ' + taVedomostToPrint2.ParamByName('T0').AsString);
end;

procedure TdmPrint.Ob_dGetValue(const ParName: String; var ParValue: Variant);
var
  s, h : string;
begin
  if(ParName='�����')then
 begin
   ParValue := '        �, '+taOb_dFIO.AsString+', ��������(�) �� ������ � '+
      DatetoStr(taOb_dBD.AsDateTime)+' �� '+DateToStr(taOb_dDOCDATE.AsDateTime);
   if taOb_dWZ.AsFloat <> 0 then s := ' ���������������� ������ ( '+
      FormatFloat('0.###', taOb_dWZ.AsFloat)+' ��.)' else s := '';
   if taApplH.AsFloat <> 0 then h := ' ��������� ' else h := '';

   if s <> '' then ParValue := ParValue + s;
   if h <> '' then
     if s <> '' then ParValue := ParValue + ' , '+h
     else ParValue := ParValue + h;
   ParValue := ParValue + ' ���������� ���������� ����������. ����� �������� '+
     '����� ';
   if s <> '' then ParValue := ParValue + '���������������� ������ ';
   if h <> '' then
     if s <> '' then ParValue := ParValue + ' , ��������� '
     else ParValue := ParValue + '��������� ';
   ParValue := ParValue + ' �� ���� ���������� �����';
 end;
end;

procedure TdmPrint.Protocol_dGetValue(const ParName: String; var ParValue: Variant);
begin
  if(ParName='������')then ParValue := dm.BeginDate;
  if(ParName='�����')then ParValue := dm.EndDate;
  if(ParName='�������')then
  begin
    if taPItem_dS.AsInteger <> 0 then ParValue := '�������� �� �/�'
    else ParValue := '';
    if(taPItem_dI.AsFloat <> 0)or(taPItem_dH.AsFloat <> 0)then
      if(ParValue <> '')then ParValue := ParValue + ', �����������'
      else ParValue := ParValue + '�����������';
  end;
end;

procedure TdmPrint.taVedToPrint2_SUMBeforeOpen(DataSet: TDataSet);
begin
  taVedToPrint2_SUM.ParamByName('BD').AsTimeStamp:=DateTimeToTimeStamp(fmMonth.VED_BD);
  taVedToPrint2_SUM.ParamByName('ED').AsTimeStamp:=DateTimeToTimeStamp(fmMonth.VED_ED);
  taVedToPrint2_SUM.ParamByName('T0').AsInteger:=T0;
  taVedToPrint2_SUM.ParamByName('FILTER$MATERIAL').AsInteger := dmPrint.NSubType;
end;

procedure TdmPrint.taSJInfoBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['PROTOCOLID'].AsInteger := ProtocolId;
end;

procedure TdmPrint.taProtPSBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['PROTOCOLID'].AsInteger := ProtocolId;
end;

procedure TdmPrint.taProtPSOperBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['PROTOCOLID'].AsInteger := ProtocolId;
    ByName['PSID'].AsInteger := taProtPSPSID.AsInteger;
  end;
end;

procedure TdmPrint.taSJ_OperTBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['PROTOCOLID'].AsInteger := ProtocolID;
    ByName['PSID'].AsInteger := taProtPSPSID.AsInteger;
    ByName['OPERID'].AsString := taProtPSOperOPERID.AsString;
  end;
end;

procedure TdmPrint.taSJ_PSTBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['PROTOCOLID'].AsInteger := ProtocolID;
    ByName['PSID'].AsInteger := taProtPSPSID.AsInteger;
  end;
end;

procedure TdmPrint.taSJ_PSTailsBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['PROTOCOLID'].AsInteger := ProtocolID;
    ByName['PSID'].AsInteger := taProtPSPSID.AsInteger;
  end;
end;

procedure TdmPrint.taProtTails_dBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['PROTOCOLID'].AsInteger := ProtocolId;
end;

procedure TdmPrint.taSRItemBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    if(dmInv.CurrSemis = ROOT_SEMIS_CODE)then
    begin
      ByName['SEMIS1'].AsString := MINSTR;
      ByName['SEMIS2'].AsString := MAXSTR;
    end
    else begin
      ByName['SEMIS1'].AsString := dmInv.CurrSemis;
      ByName['SEMIS2'].AsString := dmInv.CurrSemis;
    end;

    if(dmInv.CurrOper = ROOT_OPER_CODE)then
    begin
      ByName['OPER1'].AsString := MINSTR;
      ByName['OPER2'].AsString := MAXSTR;
    end
    else begin
      ByName['OPER1'].AsString := dmInv.CurrOper;
      ByName['OPER2'].AsString := dmInv.CurrOper;
    end;

    if dmMain.FilterMatId = ROOT_MAT_CODE then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := dmMain.FilterMatId;
      ByName['MATID2'].AsString := dmMain.FilterMatId;
    end;
  end;
end;

procedure TdmPrint.taVSheetBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    if(dmInv.CurrSemis = ROOT_SEMIS_CODE)then
    begin
      ByName['SEMIS1'].AsString := MINSTR;
      ByName['SEMIS2'].AsString := MAXSTR;
    end
    else begin
      ByName['SEMIS1'].AsString := dmInv.CurrSemis;
      ByName['SEMIS2'].AsString := dmInv.CurrSemis;
    end;

    if(dmInv.CurrOper = ROOT_OPER_CODE)then
    begin
      ByName['OPER1'].AsString := MINSTR;
      ByName['OPER2'].AsString := MAXSTR;
    end
    else begin
      ByName['OPER1'].AsString := dmInv.CurrOper;
      ByName['OPER2'].AsString := dmInv.CurrOper;
    end;

    if dmMain.FilterMatId = ROOT_MAT_CODE then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := dmMain.FilterMatId;
      ByName['MATID2'].AsString := dmMain.FilterMatId;
    end;
  end;
end;

procedure TdmPrint.VSheetGetValue(const ParName: String; var ParValue: Variant);
begin
  if(ParName = '��������')then
  begin
    if dmMain.FilterMatId = ROOT_MAT_CODE then ParValue := '*���'
    else ParValue := ExecSelectSQL('select NAME from D_MAT where ID='#39+dmMain.FilterMatId+#39, dmMain.quTmp)
  end;

  if(ParName = '������������')then
  begin
    if(dmInv.CurrSemis = ROOT_SEMIS_CODE)then ParValue := '*���'
    else ParValue := ExecSelectSQL('select SEMIS from D_SEMIS where ID='#39+dmInv.CurrSemis+#39, dmMain.quTmp);
  end;

  if(ParName = '��������')then
  begin
    if(dmInv.CurrOper = ROOT_OPER_CODE)then ParValue := '*���'
    else ParValue := ExecSelectSQL('select OPERATION from D_OPER where ID='#39+dmInv.CurrOper+#39, dmMain.quTmp);
  end;
end;

procedure TdmPrint.taVSheetMatBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    if(dmInv.CurrSemis = ROOT_SEMIS_CODE)then
    begin
      ByName['SEMISID1'].AsString := MINSTR;
      ByName['SEMISID2'].AsString := MAXSTR;
    end
    else begin
      ByName['SEMISID1'].AsString := dmInv.CurrSemis;
      ByName['SEMISID2'].AsString := dmInv.CurrSemis;
    end;

    if(dmMain.FilterOperId = ROOT_OPER_CODE)then
    begin
      ByName['OPERID1'].AsString := MINSTR;
      ByName['OPERID2'].AsString := MAXSTR;
    end
    else if(dmMain.FilterOperId = sNoOperation) then
    begin
      ByName['OPERID1'].AsVariant := Null;
      ByName['OPERID2'].AsVariant := Null;
    end
    else begin
      ByName['OPERID1'].AsString := dmMain.FilterOperId;
      ByName['OPERID2'].AsString := dmMain.FilterOperId;
    end;

    if dmMain.FilterMatId = ROOT_MAT_CODE then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := dmMain.FilterMatId;
      ByName['MATID2'].AsString := dmMain.FilterMatId;
    end;
  end;
end;

procedure TdmPrint.taVSheetMat1BeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    if(dmInv.CurrSemis = ROOT_SEMIS_CODE)then
    begin
      ByName['SEMISID1'].AsString := MINSTR;
      ByName['SEMISID2'].AsString := MAXSTR;
    end
    else begin
      ByName['SEMISID1'].AsString := dmInv.CurrSemis;
      ByName['SEMISID2'].AsString := dmInv.CurrSemis;
    end;

    if(dmMain.FilterOperId = ROOT_OPER_CODE)then
    begin
      ByName['OPERID1'].AsString := MINSTR;
      ByName['OPERID2'].AsString := MAXSTR;
    end
    else if(dmMain.FilterOperId = sNoOperation) then
    begin
      ByName['OPERID1'].AsVariant := Null;
      ByName['OPERID2'].AsVariant := Null;
    end
    else begin
      ByName['OPERID1'].AsString := dmMain.FilterOperId;
      ByName['OPERID2'].AsString := dmMain.FilterOperId;
    end;

    if dmMain.FilterMatId = ROOT_MAT_CODE then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := dmMain.FilterMatId;
      ByName['MATID2'].AsString := dmMain.FilterMatId;
    end;
  end;
end;

procedure TdmPrint.taInv16AssortBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    if not quInv16ItemAINV.IsNull then ByName['INVID'].AsInteger := quInv16ItemAINV.AsInteger
    else ByName['INVID'].AsInteger := quInv16ItemAPPLINV.AsInteger;
end;


procedure TdmPrint.taINVHeaderBeforeOpen(DataSet: TDataSet);
begin
  taInvHeader.ParamByName('InvID').AsInteger := dmInv.FCurrINVID; //dmInv.taSList.FieldByName('InvId').AsInteger;
end;

procedure TdmPrint.taBulkItemBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := taBulkINVID.AsInteger;
end;

procedure TdmPrint.taKolieReportBeforeOpen(DataSet: TDataSet);
begin
  TaKolieREport.ParambyName('BDATE').AsDate:=dm.BeginDate;
  TaKolieREport.ParambyName('EDATE').AsDate:=dm.EndDate;
end;

procedure TdmPrint.taAffinajBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := taAffInvINVID.AsInteger;
end;

procedure TdmPrint.InitReportSetting;
var
  Ini : TIniFile;
  sect : TStringList;
  s : string;
begin
  Ini := TIniFile.Create(GetIniFileName);
  sect := TStringList.Create;
  try
    Ini.ReadSectionValues('ReportSetting', sect);
    FReportSetting.UseDotMatrix := sect.Values['UseDotMatrix'] = '1';
  finally
    Ini.Free;
    sect.Free;
  end;
end;

procedure TdmPrint.quInv16ItemCalcFields(DataSet: TDataSet);
begin
  quInv16ItemRecNo.AsInteger := quInv16Item.RecNo;
  case quInv16ItemUQ.AsInteger of
    -1 : quInv16ItemUQNAME.AsString := '-';
     0 : quInv16ItemUQNAME.AsString := '��.';
     1 : quInv16ItemUQNAME.AsString := '����';
     else quInv16ItemUQNAME.AsString := '-';
  end;
  case quInv16ItemUW.AsInteger of
    -1 : quInv16ItemUWNAME.AsString := '-';
     3 : quInv16ItemUWNAME.AsString := '��';
     4 : quInv16ItemUWNAME.AsString := '�����';
     else quInv16ItemUWNAME.AsString := '-';
  end;
end;

procedure TdmPrint.OperOrderGetValue(const ParName: String;
  var ParValue: Variant);
begin
  if (ParName = '�����')then ParValue:=FNo;
  if (ParName = '����')then ParValue:=FDate;
  if (ParName = '���������')then ParValue:=FCap;
end;

procedure TdmPrint.ArtInsGetValue(const ParName: String; var ParValue: Variant);
begin
  if(ParName = '�������')then
     ParValue := ExecSelectSQL('select ArtIns from Get_ArtIns('+dm.taArtARTID.AsString+')', dm.quTmp);
end;

procedure TdmPrint.UQGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
   if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
          -1 : Text:='���';
           0 : Text:='��';
           1 : Text:='����';
         end;
end;

procedure TdmPrint.UWGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text:=''
  else case Sender.AsInteger of
          -1 : Text:='���';
           3 : Text:='��';
           4 : Text:='�����';
         end;
end;

procedure TdmPrint.TagGetValue(const ParName: string; var ParValue: Variant);
begin
  if(ParName='�����')then ParValue := '������';
  if(ParName='�������')then ParValue := '100001';
  if(ParName='���')then ParValue := '10.19';
end;

procedure TdmPrint.PrepareTag;

  function GetDataSet(i : integer) : string;
  begin
    Result := '';
    case Self.TagSource of
      0 : Result := 'dmMain.taMatrixProd."Col'+IntToStr(i)+'"';
      1 : Result := 'dmMain.taB_UID."Col'+IntToStr(i)+'"';
    end;
  end;

const
   MemoName : array [0..6] of string = ('mmGood', 'mmSz', 'mmArt2', 'mmArt', 'mmUID', 'mmW', 'mmTagDown');
   MemoFunc : array [0..6] of string = ('TAG_GOOD', 'TAG_SZ', 'TAG_ART2', 'TAG_ART', 'TAG_UID', 'TAG_W', 'TAG_INS');
   MemoOrder : array [0..6] of byte = (0, 0, 0, 0, 0, 0, 1);
var
  Memo : TfrMemoView;
  Band : TfrBandView;
  i, j, k : integer;
begin
  // �����
  Band := TfrBandView(frReport.FindObject('CrossData'));
  if Assigned(Band)then
  begin
    OpenDataSet(dm.taRec);
    Band.DataSet := 'MasterData1='+dm.taRecY_MATRIXPROD.AsString+';';
    CloseDataSet(dm.taRec);
  end;
  Band := TfrBandView(frReport.FindObject('CrossData2'));
  if Assigned(Band)then
  begin
    OpenDataSet(dm.taRec);
    Band.DataSet := 'MasterData2='+dm.taRecY_MATRIXPROD.AsString+';';
    CloseDataSet(dm.taRec);
  end;
  Band := TfrBandView(frReport.FindObject('MasterData1'));
  if Assigned(Band)then
    case TagSource of
      0 : Band.DataSet := 'frMatrixProd';
      1 : Band.DataSet := 'frB_UID';
    end;
  Band := TfrBandView(frReport.FindObject('MasterData2'));
  if Assigned(Band) then
    case TagSource of
      0 : Band.DataSet := 'frMatrixProd';
      1 : Band.DataSet := 'frB_UID';
    end;
  // ����-����
  OpenDataSet(dm.taRec);
  for i:=Low(MemoName) to High(MemoName) do
  begin
    Memo := TfrMemoView(frReport.FindObject(MemoName[i]));
    if Assigned(Memo) then
    begin
      Memo.Script.Clear;
      if(MemoOrder[i] = 0)then // ������� ���������� �����
        for j:=0 to Pred(dm.taRecY_MATRIXPROD.AsInteger) do with Memo.Script do
        begin
          if(j <> 0)then Text := Text+' else ';
          Text := Text+' if [COLUMN#]='+IntToStr(j+1)+' then '+MemoName[i]+'.Memo := '+MemoFunc[i]+'(['+GetDataSet(j)+'])';
        end
      else begin
        k := 1;
        for j:=Pred(dm.taRecY_MATRIXPROD.AsInteger)downto 0 do with Memo.Script do
        begin
          if(k <> 1)then Text := Text+' else';
          Text := Text+' if [COLUMN#]='+IntToStr(k)+' then '+MemoName[i]+'.Memo := '+MemoFunc[i]+'(['+GetDataSet(j)+'])';
          inc(k);
        end
      end
    end
  end;
  CloseDataSet(dm.taRec);
end;

procedure TdmPrint.VedGetValue(const ParName: string;
  var ParValue: Variant);
begin
  if(ParName='������������')then
  begin
   if (T0=0) then ParValue :='������� ���������';
   if (T0=1) then ParValue :='������������ ���������';
   if (T0=2) then ParValue :='������� �������';
  end;

  if (ParName = '��������') then
  begin
    ParValue := NSubType;
  end;

  if (ParName = '�����������') then
  begin
    ParValue := dm.User.SelfCompName;
  end;  
  
end;

procedure TdmPrint.InsGetValue(const ParName: string; var ParValue: Variant);
begin
  if (ParName = '�������') then ParValue := Ins;
end;

procedure TdmPrint.InsGetValueFR4(const VarName: string; var Value: Variant);
begin
  if CompareText(VarName, '�������') = 0 then
    Value := Ins;
end;

function TdmPrint.PrintStickerNew(UID: Integer; FromHistory: boolean): boolean;
const
  LP_COMMAND_DELIM = '' + #10;
  MaxInsCount: integer = 3;
var
  lPrinter: Cardinal;
  lDocInfo: TDocInfo1;
  lPrintData: String;
  lPrintedBytes: Cardinal;
  lPrintDataLength: Cardinal;
  PrinterName: string;
  i, SpacePlace: Integer;
  InsItemsArray: array [0..3] of string;
  InsMatArray: array [0..3] of string;
  stickerCommentPart1: string;
  stickerCommentPart2: string;
  ProducerID: Integer;
begin

  if not FromHistory then
  begin

    if dmMain.taASListISCLOSE.AsInteger = 1 then
    begin
      MessageDialogA('��������� �������!', mtError);
      eXit;
    end;

    if dmMain.taASItem.FieldByName('IS$PRINTED').AsInteger = 1 then
    begin

      if MessageDialog('�� ��� �������� ��� �����!'#13#10 + '��������� ������?', mtWarning, [mbYes, mbNo], 0) = mrNo then
      begin
        eXit;
      end;

    end;

  end;

  trPrint.StartTransaction;

  taStickerUID.Close;
  taStickerUID.ParamByName('UID').AsInteger := UID;
  taStickerUID.Open;

  trPrint.CommitRetaining;

  taStickerIns.Close;
  taStickerIns.ParamByName('ART2ID').AsInteger := taStickerUID.FieldByName('ART2ID').AsInteger;
  taStickerIns.Open;
  taStickerIns.First;

  for i := 0 to MaxInsCount do
  begin
    InsMatArray[i] := '';
    InsItemsArray[i] := '';
  end;

  i := 0;

  if not taStickerIns.IsEmpty then
  begin

    while not taStickerIns.Eof do
    begin

      InsMatArray[i] := trim(taStickerIns.FieldByName('Stones').AsString);

      SpacePlace := AnsiPos(' ', InsMatArray[i]);

      if SpacePlace > 0 then
      begin
        InsItemsArray[i] := Copy(InsMatArray[i], SpacePlace + 1, length(InsMatArray[i]) - SpacePlace);
        Delete(InsMatArray[i], SpacePlace, length(InsItemsArray[i])+1);
      end;

      taStickerIns.Next;

      inc(i);

    end;

  end;

    //����� ��������� ������ �� 2 ����� � ��������� ������� �������

  stickerCommentPart1 := trim(taStickerUIDSticker_Comment.AsString);
  stickerCommentPart2 := '';

  i := AnsiPos(' ', stickerCommentPart1);

  if i > 0 then
  begin
    stickerCommentPart2 := Copy(stickerCommentPart1, i + 1, length(stickerCommentPart1) - i);
    Delete(stickerCommentPart1, i, length(stickerCommentPart2) + 1);
  end;

  PrinterName := 'ZDesigner LP 2824';

  ProducerID := Integer(dm.db.QueryValue('select producer$id from get$producer$by$uid(:uid)', 0, [UID]));


  if ProducerID = 1 then
  begin
    lPrintData :=  LP_COMMAND_DELIM
                + 'O' + LP_COMMAND_DELIM
                + 'N' + LP_COMMAND_DELIM
                + 'W' + LP_COMMAND_DELIM
                + 'D7' + LP_COMMAND_DELIM
                + 'Q250' + LP_COMMAND_DELIM
                + 'q430' + LP_COMMAND_DELIM
                + 'I8,C' + LP_COMMAND_DELIM
                + 'A385,40,1,4,1,1,N,"' + taStickerUIDSNAME.AsString +'"' + LP_COMMAND_DELIM
                + 'A360,0,1,2,1,1,N,"���."' + LP_COMMAND_DELIM
                + 'A360,45,1,2,1,1,N," ' + taStickerUIDArt.AsString + '"' + LP_COMMAND_DELIM
                + 'A340,45,1,2,1,1,N," ' + taStickerUIDArt2.AsString + '"' + LP_COMMAND_DELIM
                + 'A320,0,1,2,1,1,N,"���"' + LP_COMMAND_DELIM
                + 'A320,35,1,2,1,1,N," ' + taStickerUIDW.AsString + '"' + LP_COMMAND_DELIM
                + 'A320,105,1,2,1,1,N,"�."' + LP_COMMAND_DELIM
                + 'A320,125,1,2,1,1,N,"' + taStickerUIDSZNAME.AsString + '"' + LP_COMMAND_DELIM
                + 'A300,0,1,1,1,1,N,"' + stickerCommentPart1 + '"' + LP_COMMAND_DELIM
                + 'A287,0,1,1,1,1,N,"' + stickerCommentPart2 + '"' + LP_COMMAND_DELIM
                + 'B260,5,1,E80,2,6,60,B,"0' + IntToStr(UID) + '"' + LP_COMMAND_DELIM
                + 'A185,0,1,1,1,1,N,"' + InsMatArray[0] + '"' + LP_COMMAND_DELIM
                + 'A172,0,1,1,1,1,N,"' + InsItemsArray[0] + '"' + LP_COMMAND_DELIM
                + 'A159,0,1,1,1,1,N,"' + InsMatArray[1] + '"' + LP_COMMAND_DELIM
                + 'A146,0,1,1,1,1,N,"' + InsItemsArray[1] + '"' + LP_COMMAND_DELIM
                + 'A133,0,1,1,1,1,N,"' + InsMatArray[2] + '"' + LP_COMMAND_DELIM
                + 'A120,0,1,1,1,1,N,"' + InsItemsArray[2] + '"' + LP_COMMAND_DELIM
                + 'A107,0,1,1,1,1,N,"' + InsMatArray[3] + '"' + LP_COMMAND_DELIM
                + 'A94,0,1,1,1,1,N,"' + InsItemsArray[3] + '"' + LP_COMMAND_DELIM
                + 'A80,135,1,2,1,1,N,"���"' + LP_COMMAND_DELIM
                + 'P1' + LP_COMMAND_DELIM;
  end else
  begin
    lPrintData :=  LP_COMMAND_DELIM
                + 'O' + LP_COMMAND_DELIM
                + 'N' + LP_COMMAND_DELIM
                + 'W' + LP_COMMAND_DELIM
                + 'D7' + LP_COMMAND_DELIM
                + 'Q160' + LP_COMMAND_DELIM
                + 'q240' + LP_COMMAND_DELIM
                + 'I8,C' + LP_COMMAND_DELIM

                + 'A18,10,0,2,1,1,N,"���."' + LP_COMMAND_DELIM
                + 'A50,10,0,2,1,1,N," ' + taStickerUIDArt.AsString + '"' + LP_COMMAND_DELIM
                + 'A18,30,0,2,1,1,N,"���"' + LP_COMMAND_DELIM
                + 'A50,30,0,2,1,1,N," ' + taStickerUIDW.AsString + '"' + LP_COMMAND_DELIM
                + 'A125,30,0,2,1,1,N,"�."' + LP_COMMAND_DELIM
                + 'A145,30,0,2,1,1,N,"' + taStickerUIDSZNAME.AsString + '"' + LP_COMMAND_DELIM
                + 'B40,68,0,E80,2,6,60,B,"0' + IntToStr(UID) + '"' + LP_COMMAND_DELIM
                + 'P1' + LP_COMMAND_DELIM;
  end;

  if not OpenPrinter(PChar(PrinterName), lPrinter, nil) then
  begin
    raise Exception.Create('������ ����� � ���������!');
  end;

  try

    lDocInfo.pDocName := PCHAR('�����-�������� ��� '  + IntToStr(UID) + ') - ' +  '1 ��.' );

    lDocInfo.pOutputFile := nil;

    lDocInfo.pDatatype := 'RAW';

    lPrintDataLength := Length(lPrintData);

    if StartDocPrinter(lPrinter, 1, @lDocInfo) = 0 then
    begin
      raise Exception.Create('������ ������ ������!');
    end;

    if not WritePrinter(lPrinter, PAnsiChar(AnsiString(lPrintData)), lPrintDataLength, lPrintedBytes) then
    begin
      raise Exception.Create('������ �������� ������ � ���� ��������!');
    end;

    if lPrintedBytes < lPrintDataLength then
    begin
      raise Exception.Create('������� ������� �������!');
    end;

    if not EndDocPrinter(lPrinter) then
    begin
      raise Exception.Create('���������� ��������� ������!');
    end;

  finally
    ClosePrinter(lPrinter);
  end;

if not FromHistory then
  begin
    dm.BarCodePrinter_Count := dm.BarCodePrinter_Count + 1;

    if not (dmMain.taASItem.State = dsEdit) then
    begin
      dmMain.taASItem.Edit;
    end;

    dmMain.taASItem.FieldByName('IS$PRINTED').AsInteger := 1;
    dmMain.taASItem.Post;
  end;

  Result := true;

end;




function TdmPrint.BarCode_IsReady: boolean;
begin

  {$ifdef GODEX}
  Result := GODEX_isready;
  {$endif GODEX}

end;
//
//function TdmPrint.PrintSticker(UID: integer): boolean;
//
//  function GODEX_Print(fn:TFileName):boolean;
//  var
//    p : string;
//    i, sticker_c, sticker_no, f, b, a: integer;
//    sUID : string;
//  begin
//    Result := False;
//    {$ifdef GODEX}
//    sUID := IntToStr(UID);
//    if not BarCode_isready then raise EWarning.Create(rsBarcodePrinter_NotReady);
//    f := 0; b := 0;
//    sticker_c := StrToIntDef( copy(dm.LocalSetting.BarCode_Step, 1, Pos('|', dm.LocalSetting.BarCode_Step)-1), 0);
//    if not(sticker_c = 0)then
//    begin
//      sticker_no := (dm.LocalSetting.BarCode_Count mod sticker_c) + 1;
//      b := StrToIntDef(Q_GetWordN(2, Q_GetWordN(sticker_no+1, dm.LocalSetting.BarCode_Step, '|'), ';'), 0);
//      f := StrToIntDef(Q_GetWordN(1, Q_GetWordN(sticker_no+1, dm.LocalSetting.BarCode_Step, '|'), ';'), 0);
//    end;
//    if (b<>0) then BarCode_Backward(b); //BarCode_Backward(5);
//    if AppDebug then MessageDialog('���-�� ����� #'+IntToStr(dm.LocalSetting.BarCode_Count)+'. ����� '+IntToStr(b)+'��', mtInformation, [mbOk], 0);
//    try
//      p := IntToStr(dm.LocalSetting.BarCode_Port)[1];
//      Godex_openport(PChar(p));
//    except
//      on E:Exception do raise EWarning.Create(rsBarcodePrinter_OpenPort);
//    end;
//    GODEX_sendcommand(PChar('~MDEL'));
//    GODEX_extloadimage(PChar(fn), PChar(sUID), 'bmp');
//    for i:=0 to Pred(dm.Rec.Sticker.Count) do
//      GODEX_sendcommand(PChar(dm.Rec.Sticker[i]));
//    GODEX_sendcommand(PChar('Y0,0,'+sUID));
//    //GODEX_sendcommand(PChar('BN,161,180,1,5,50,3,1,'+sUID));// - I2 of 5
//    //GODEX_sendcommand(PChar('Y0,0,'+sUID, 7, '0')));
//    GODEX_sendcommand(PChar('BB,161,199,2,5,65,3,1,'+Q_PadLeft(sUID, 7, '0')));//Q_PadLeft(sUID, 7, '0'))); // EAN8
//    GODEX_sendcommand('E');
//    GODEX_sendcommand(PChar('~MDELG,'+sUID));
////    BarCode_sendcommand('^B'+IntToStr(5));
//    try
//      GODEX_closeport;
//    except
//      on E:Exception do raise EWarning.Create(rsBarcodePrinter_ClosePort);
//    end;
//    BarCode_Forward(f);
//
//    if AppDebug then MessageDialog('���������� ����� #'+sUID+'. ������ '+IntToStr(f), mtInformation, [mbOk], 0);
//    dm.BarCodePrinter_Count := dm.BarCodePrinter_Count+1;
//    {$endif}
//    Result := True;
//  end;
//
//const GODEX_dpi = 203;
//var
//  i, j, h, h2, hName, InsCount : integer;
//  r : TRect;
//  c : double;
//  tw, tw1 : integer; // ����� �����
//  pArt, pW, pComment, pIns, pName : TPoint;
//  Art, Weight, Sz, Comment, Ins, Name : string;
//  bmp, bmpSticker : TBitmap;
//  fn : TFileName;
//begin
//  AppDebug := False;
//  try
//  // ��������� ����� �� �������
//  //if not BarCode_isready then raise EWarning.Create('������� ��������� �� �����');
//  // ������ �����������
//  c := (Screen.PixelsPerInch/25.1)*(GODEX_dpi/Screen.PixelsPerInch);
//  r.Left := 0;
//  r.Top := 0;
//  r.Right := Trunc(25*c);
//  r.Bottom := Trunc(43*c);
//  if trPrint.Active then trPrint.Commit;
//  trPrint.StartTransaction;
//  quStickerUID.Close;
//  quStickerUID.ParamByName('UID').AsInteger := UID;
//  quStickerUID.ExecQuery;
//  trPrint.CommitRetaining;
//  bmpSticker := TBitMap.Create;
//  with bmpSticker do //fmMain.pb.
//  try
//    Monochrome := True;
//    Width := r.Right;
//    Height := r.Bottom;
//    Canvas.Rectangle(r);
//    Canvas.Brush.Color := clWhite;
//    Canvas.FillRect(r);
//    // ����� ��� ������ ������
//    Canvas.Font.Color := clBlack;
//
//    // ������� ������������
//    Canvas.Font.Name := 'Times New Roman';
//    Canvas.Font.Size := 20;
//    Canvas.Font.Style := [fsBold];
//    hName := Canvas.Font.Height;
//    pName.X := 20;
//    pName.Y := 15;
//    Name := quStickerUID.FieldByName('SName').AsString;
//    Canvas.TextOut(pName.X, pName.Y, Name);
//
//    // ������� ������� � ���2
//    Canvas.Font.Name := 'Times New Roman';
//    Canvas.Font.Size := 18;
//    Canvas.Font.Style := [fsBold];
//    h := Canvas.Font.Height;
//    pArt.X := 10;
//    //pArt.Y := 15; //- ������, ��� ��������
//    pArt.Y := pName.Y - hName; // � ���������
//    tw := Canvas.TextWidth('���. ');
//    Canvas.TextOut(pArt.X, pArt.Y, '���. ');
//    Canvas.Font.Size := 16;
//    Canvas.Font.Style := [];
//    h2 := Canvas.Font.Height;
//    Art := quStickerUID.FieldByName('ART').AsString;
//    Canvas.TextOut(pArt.X+tw, pArt.Y+(h2-h), Art);
//    //������ �������
//    if(quStickerUID.FieldByName('ART2').AsString<>'-') then begin
//      pArt.X := pArt.X;
//      pArt.Y := pArt.Y-Trunc(h{+h/4});
//      Art:=quStickerUID.FieldByName('ART2').AsString;
//      Canvas.TextOut(pArt.X+tw, pArt.Y, Art);
//    end;
//     // ������� ���
//    pW.X := pArt.X;
//    pW.Y := pArt.Y-Trunc(h{+h/4});
//    Canvas.Font.Name := 'Times New Roman';
//    Canvas.Font.Size := 18;
//    Canvas.Font.Style := [fsBold];
//    tw := Canvas.TextWidth('��� ');
//    Canvas.TextOut(pW.X, pW.Y, '���');
//    Canvas.Font.Name := 'Times New Roman';
//    Canvas.Font.Size := 16;
//    Canvas.Font.Style := [];
//    Weight := Format('%.2f', [quStickerUID.FieldByName('W').AsFloat]);
//    Canvas.TextOut(pW.X+tw, pW.Y+(h2-h), Weight);
//     // ������� ������
//    if(quStickerUID.FieldByName('SZNAME').AsString<>'-')then
//    begin
//      Sz := '�.'+quStickerUID.FieldByName('SZNAME').AsString;
//      tw1 := Canvas.TextWidth(Weight+'  ');
//      Canvas.TextOut(pW.X+tw+tw1, pW.Y+(h2-h), Sz);
//    end;
//
//    pComment.X := pW.X;
//    pComment.Y := pW.Y-Trunc(h{+h/4});
//    if(quStickerUID.FieldByName('STICKER_COMMENT').AsString <> '') then
//    begin
//      Comment := quStickerUID.FieldByName('STICKER_COMMENT').AsString;
//      Canvas.Font.Name := 'Times New Roman';
//      Canvas.Font.Size := 14;
//      Canvas.TextOut(pComment.X, pComment.Y, Comment);
//    end;
//      {pComment.X := pComment.X;
//      pComment.Y := pComment.Y-Trunc(h}{+h/4}{);
//    end;
//    Canvas.TextOut(pComment.X, pComment.Y, '��� 117-3-002-95'); }
//         // ������� ����� ���
//    Canvas.Draw(144 - Trunc(2*c), 291, dm.Rec.Sticker_OTK);
//
//    // ������� ��������������
//    taStickerIns.ParamByName('ART2ID').AsInteger := quStickerUID.FieldByName('ART2ID').AsInteger;
//    taStickerIns.Open;
//    taStickerIns.Last;
//    InsCount := taStickerIns.RecordCount;
//    taStickerIns.First;
//    Canvas.Font.Name := 'Times New Roman';
//    if InsCount <= 3 then Canvas.Font.Size := 14
//    else Canvas.Font.Size := 11;
//    h := Canvas.Font.Height;
//    pIns.X := pArt.X;
//    pIns.Y := 240;
//    i := 0;
//    while not taStickerIns.Eof  do
//    begin
//      Ins := taStickerIns.FieldByName('STONES').AsString;
//      Canvas.TextOut(pIns.X, pIns.Y-i*Trunc(h+h/8), Ins);
//      taStickerIns.Next;
//      inc(i);
//    end;
//
//    quStickerUID.Close;
//    taStickerIns.Close;
//    if trPrint.Active then trPrint.Commit;
//
//    if not DirectoryExists('c:\Sticker') then
//    mkdir('c:\Sticker');
//    // ������ �������� � �������� �������� � ����
//    if AppDebug then
//      bmpSticker.SaveToFile('c:\Sticker\'+IntToStr(UID)+'_.bmp');
//    bmp := TBitmap.Create;
//    bmp.Monochrome := True;
//    try
//      bmp.Width := Height;
//      bmp.Height := Width;
//      for i :=  0 to Pred(Width) do
//        for j := 0 to Pred(Height) do
//           bmp.Canvas.Pixels[j, bmp.Height-i] := Canvas.Pixels[i, j];
//      fn := 'c:\Sticker\'+IntToStr(UID)+'.bmp';
//      bmp.SaveToFile(fn);
//    finally
//      bmp.Free;
//    end;
//  finally
//    bmpSticker.Free;
//  end;
//  GODEX_print(fn);
//  if not AppDebug then DeleteFile(fn);
//  finally
//    AppDebug := False;
//  end;
//end;

procedure TdmPrint.BarCode_DownloadImage(memt : byte);
begin
  //Godex_openport('0');
  try
    BarCode_OpenPort;
  except
    ShowMessage('������� ��� �������� �����');
    Exit;
  end;
  {try
    case memt of
      0 : begin
        GODEX_intloadimage('C:\OTK48x48.bmp', 'OTK', 'bmp');
      end;
      1 : begin
        GODEX_extloadimage('C:\OTK48x48.bmp', 'Q', 'bmp');
      end;
    end;
  except
    ShowMessage('������ ��� �������� ��������');
    Exit;
  end;
  BarCode_ClosePort;}
end;

procedure TdmPrint.BarCode_OpenPort;
var
  p : string;
begin
  p := IntToStr(dm.LocalSetting.BarCode_Port)[1];
  {$ifdef GODEX}
  GODEX_openport(PChar(p));
  {$endif}
end;

procedure TdmPrint.BarCode_ClosePort;
begin
  {$ifdef GODEX}
  GODEX_closeport;
  {$endif}
end;

procedure TdmPrint.BarCode_Backward(step: integer);
begin
  if not BarCode_isready then raise EWarning.Create(rsBarcodePrinter_NotReady);
  BarCode_OpenPort;
  BarCode_sendcommand('^B'+IntToStr(step));
  BarCode_ClosePort;
end;

procedure TdmPrint.BarCode_Forward(step: integer);
begin
  if not BarCode_isready then raise EWarning.Create(rsBarcodePrinter_NotReady);
  BarCode_OpenPort;
  BarCode_sendcommand('^M'+IntToStr(step));
  BarCode_ClosePort;
end;

procedure TdmPrint.BarCode_sendcommand(Cmd: string);
begin
  {$ifdef GODEX}
  GODEX_sendcommand(PChar(Cmd));
  {$endif}
end;

{ TProductionLib }

constructor TProductionLib.Create;
begin
  inherited;
  with List do
  begin
    Add('TAG_GOOD');  // ����� - �����
    Add('TAG_ART');   // ����� - �������
    Add('TAG_ART2');  // ����� - ���2
    Add('TAG_SZ');
    Add('TAG_INS');
    Add('TAG_UID');
    Add('TAG_W');
    Add('CAPITALSQUANTITY');
    Add('CAPITALSWEIGH');
    Add('CAPITALSMONEY');
    Add('CAPITALSNAME');
  end;
end;

procedure TProductionLib.DoFunction(FNo: Integer; p1, p2, p3: Variant; var val: Variant);
var
  sl : TStringList;
  Art2Id : integer;
begin
  val := '';
  case FNo of
    0,1,2,3,4,5,6 : begin // TAG_GOOD, TAG_ART, TAG_ART2, TAG_SZ, TAG_INS, TAG_UID, TAG_W
       sl := TStringList.Create();
       try
         sl.Delimiter := ';';
         sl.DelimitedText := frParser.Calc(p1);
         case FNo of
           0 : val := sl.Values['GOOD'];
           1 : val := sl.Values['ART'];
           2 : val := sl.Values['ART2'];
           3 : begin
             val := sl.Values['SZNAME'];
             if(not VarIsNull(val))and(val<>'')and(val<>'-')then val := '�.'+val
             else val := '';
           end;
           4 : begin
             Art2Id := StrToIntDef(sl.Values['ART2ID'], 0);
             if(Art2Id=0)then eXit;
             CloseDataSet(dmPrint.taTagIns);
             dmPrint.taTagIns.ParamByName('ART2ID').AsInteger := Art2Id;
             OpenDataSet(dmPrint.taTagIns);
             val := '';
             while not dmPrint.taTagIns.Eof do
             begin
               val := val+' '+dmPrint.taTagIns.Fields[0].AsString+#13;
               dmPrint.taTagIns.Next;
             end;
             CloseDataSet(dmPrint.taTagIns);
           end;
           5 : val := sl.Values['UID'];
           6 : begin
             if(sl.Values['W']<>'')then val := FormatFloat('0.##', ConvertStrToFloat(sl.Values['W']));
           end;
         end;
       finally
         sl.Free;
       end;
    end;
    // CAPITALSQUANTITY
    7 : val := GetCapitalsQuantity(frParser.Calc(p1));
    // CAPITALSWEIGH
    8 : val := GetCapitalsWeight(frParser.Calc(p1));
    // CAPITALSMONEY
    9 : val := GetCapitalsMoney(frParser.Calc(p1));
    // CAPITALSNAME
    10 : val := GetCapitalsName(frParser.Calc(p1));
  end; // case 
end;

procedure TdmPrint.taSJ_TBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['PROTOCOLID'].AsInteger := ProtocolID;
end;

procedure TdmPrint.quInv16ItemBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['Link'].AsInteger := quInv16LINK.AsInteger;
end;

procedure TdmPrint.taProtPSOperAfterOpen(DataSet: TDataSet);
begin
  ReOpenDataSets([taSJ_PSOper, taSJ_OperT]);
end;

procedure TdmPrint.taProtPSOperAfterScroll(DataSet: TDataSet);
begin
  ReOpenDataSets([taSJ_PSOper, taSJ_OperT]);
end;

procedure TdmPrint.taSJFact_MatAfterOpen(DataSet: TDataSet);
begin
  ReOpenDataSet(taSJFact_PS);
end;

procedure TdmPrint.taSJFact_MatAfterScroll(DataSet: TDataSet);
begin
  ReOpenDataSet(taSJFact_PS);
end;

procedure TdmPrint.taSJFact_PSBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['PROTOCOLID'].AsInteger := ProtocolId;
    ByName['MATID'].AsString := taSJFact_MatMATID.AsString;
  end
end;

procedure TdmPrint.taSJFact_PSFilterRecord(DataSet: TDataSet; var Accept: Boolean);
begin
  Accept := not((taSJFact_PSFQ.AsInteger=0) and (abs(taSJFact_PSFW.AsFloat) < Eps));
end;

procedure TdmPrint.quInv16ItemAfterScroll(DataSet: TDataSet);
begin
  if Assigned(taInv16Assort.DataSource)then ReOpenDataSet(taInv16Assort);
end;

procedure TdmPrint.taVSheetAssortBeforeOpen(DataSet: TDataSet);
begin
  taVSheetAssort.ParamByName('VITEMID').AsInteger := taVSheetOperAssortVITEMID.AsInteger;
end;

procedure TdmPrint.taVSheetOperAssortAfterOpen(DataSet: TDataSet);
begin
  ReOpenDataSet(taVSheetAssort);
end;

procedure TdmPrint.taVSheetOperAssortAfterScroll(DataSet: TDataSet);
begin
  ReOpenDataSet(taVSheetAssort);
end;

procedure TdmPrint.taSelfBeforeOpen(DataSet: TDataSet);
begin
  dm.SetSelfCompParams(taSelf.Params);
end;

procedure TdmPrint.taSielToPrintAfterOpen(DataSet: TDataSet);
begin
//  ShowMessage('InvID: ' + taSielToPrint.ParamByName('InvID').AsString);
end;

procedure TdmPrint.frReportBeforePrint(Memo: TStringList; View: TfrView);
begin
  {$ifdef TRIAL}
  SysUtils.Abort;
  {$endif}
end;

procedure TdmPrint.DataModuleDestroy(Sender: TObject);
begin
  //if FBCPrinterLibHandle >= 32 then FreeLibrary(FBCPrinterLibHandle);
end;

{var
  data, s : string;}



procedure TdmPrint.taPMat_PsBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['ACTID'].AsInteger := dmMain.taPListID.AsInteger;
    ByName['PSID'].AsInteger := dmMain.taProtocol_dPSID.AsInteger;
  end;
end;

procedure TdmPrint.taPOper_PSBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['ACTID'].AsInteger := dmMain.taPListID.AsInteger;
    ByName['PSID'].AsInteger := dmMain.taProtocol_dPSID.AsInteger;
    ByName['MATID'].AsString := taPMat_PS.FieldByName('MATID').AsString;
  end;
  taPItem_PS.Active := False;
end;

procedure TdmPrint.taPItem_PSBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['ACTID'].AsInteger := dmMain.taPListID.AsInteger;
    ByName['PSID'].AsInteger := dmMain.taProtocol_dPSID.AsInteger;
    ByName['MATID'].AsString := taPMat_PS.FieldByName('MATID').AsString;
    ByName['OPERID'].AsString := taPOper_PS.FieldByName('OPERID').AsString;
  end;
end;

procedure TdmPrint.taProdApplRespBeforeOpen(DataSet: TDataSet);
begin
  taProdApplresp.ParamByName('ApplID').AsInteger := dmAppl.taAListID.AsInteger;
end;

procedure TdmPrint.taProtocol_PSBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['ACTID'].AsInteger := dmMain.taPListID.AsInteger;
    ByName['PSID'].AsInteger := dmMain.taProtocol_dPSID.AsInteger;
  end;
end;

procedure TdmPrint.taPArt_PSBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['ACTID'].AsInteger := dmMain.taPListID.AsInteger;
    ByName['PSID'].AsInteger := dmMain.taProtocol_dPSID.AsInteger;
  end;
end;


procedure TdmPrint.taPOper_PSAfterOpen(DataSet: TDataSet);
begin
  taPItem_PS.Active := True;
end;

initialization
  frRegisterFunctionLibrary(TProductionLib);
{  data := '2|2;3|2;2';
  s := copy(data, 1, Pos('|', data)-1);
  Showmessage(s);}

end.


