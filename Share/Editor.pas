unit Editor; 

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls;

type
  TEditorClass = class of TfmEditor;

  TfmEditor = class(TForm)
    plMain: TPanel;
    btnOk: TBitBtn;
    btnCancel: TBitBtn;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  end;

function ShowEditor(FC : TEditorClass; var F : TfmEditor; EditorName : string = '') : word;

var
  vResult : Variant;

implementation

{$R *.dfm}

uses dbUtil, fmUtils;

function ShowEditor(FC : TEditorClass; var F : TfmEditor; EditorName : string = '') : word;
begin
  F := FC.Create(Application);
  try
    if EditorName <> '' then F.Caption := EditorName;
    Result := F.ShowModal;
  finally
    FreeAndNil(F);
  end;
end;


procedure TfmEditor.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : if Shift = [ssCtrl] then ModalResult := mrOk;
  end;
end;

end.
