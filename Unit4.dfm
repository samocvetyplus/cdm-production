object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Form4'
  ClientHeight = 666
  ClientWidth = 992
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object grGroups: TDBGrid
    Left = 0
    Top = 0
    Width = 953
    Height = 329
    DataSource = dsGroups
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object grItems: TDBGrid
    Left = 0
    Top = 352
    Width = 953
    Height = 313
    DataSource = dsItems
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object dsItems: TDataSource
    DataSet = DialogAffinage.DataSetPassportItems
    Left = 960
    Top = 360
  end
  object dsGroups: TDataSource
    DataSet = DialogAffinage.PassportGroups
    Left = 960
  end
end
