unit eActVerification;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh, DBLookupEh,
  DBGridEh;

type
  TfmeActVerification = class(TfmEditor)
    lbName1: TLabel;
    cmbxCompType: TDBComboBoxEh;
    Label1: TLabel;
    lbComp: TLabel;
    lcbxSup: TDBLookupComboboxEh;
    Label2: TLabel;
    edBD: TDBDateTimeEditEh;
    Label3: TLabel;
    edED: TDBDateTimeEditEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FOpenCompDict: boolean;
  end;

var
  fmeActVerification: TfmeActVerification;

implementation

uses DictData, dbUtil, fmUtils, DateUtils, ProductionConsts;

{$R *.dfm}

procedure TfmeActVerification.FormCreate(Sender: TObject);
var
  v : Variant;
begin
  inherited;
  cmbxCompType.ItemIndex := 0;
  lbComp.Caption := dm.User.SelfCompName;
  FOpenCompDict := dm.taComp.Active;
  if not FOpenCompDict then OpenDataSet(dm.taComp);
  // �������� ������ �������
  v := ExecSelectSQL('select max(AEd) from Inv where IsClose=1 and IType=22 and SelfCompId='+IntToStr(dm.User.SelfCompId), dm.quTmp);
  { TODO : ������� � D_Rec }
  if VarIsNull(v) then v := '31.07.2004';
  edBD.Value := IncDay(StartOfTheDay(VarToDateTime(v)));
  edED.Value := Now;
end;



procedure TfmeActVerification.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk: begin
      if cmbxCompType.ItemIndex<0 then
      begin
        ActiveControl := cmbxCompType;
        raise EWarning.Create('�������� ���');
      end;
      if VarIsNull(lcbxSup.KeyValue) then
      begin
        ActiveControl := lcbxSup;
        raise EWarning.Create(rsNotDefineCompId);
      end;
      if VarIsNull(edBD.Value) then
      begin
        ActiveControl := edBD;
        raise EWarning.Create(Format(rsInvalidDate, [VarToStr(edBD.Value)]));
      end;
      if VarIsNull(edED.Value) then
      begin
        ActiveControl := edED;
        raise EWarning.Create(Format(rsInvalidDate, [VarToStr(edED.Value)]))
      end;
      vResult := VarArrayOf([lcbxSup.KeyValue, edBD.Value, edED.Value]);
    end;
  end;
  inherited;
end;

end.
