inherited fmeSQ: TfmeSQ
  Left = 272
  Top = 227
  ActiveControl = edQ
  Caption = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
  ClientHeight = 96
  ClientWidth = 208
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 208
    Height = 96
    object lbQ: TLabel [0]
      Left = 12
      Top = 20
      Width = 59
      Height = 13
      Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbW: TLabel [1]
      Left = 12
      Top = 44
      Width = 19
      Height = 13
      Caption = #1042#1077#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    inherited btnOk: TBitBtn
      Left = 32
      Top = 68
    end
    inherited btnCancel: TBitBtn
      Left = 120
      Top = 68
    end
    object edQ: TRxSpinEdit
      Left = 80
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object edW: TRxSpinEdit
      Left = 80
      Top = 37
      Width = 121
      Height = 21
      Decimal = 3
      ValueType = vtFloat
      TabOrder = 3
    end
  end
end
