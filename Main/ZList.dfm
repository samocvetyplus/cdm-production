inherited fmZList: TfmZList
  Left = 128
  Top = 165
  Caption = #1047#1072#1088#1087#1083#1072#1090#1072
  ClientHeight = 470
  ClientWidth = 784
  WindowState = wsMaximized
  ExplicitWidth = 792
  ExplicitHeight = 504
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 451
    Width = 784
    Panels = <
      item
        Width = 50
      end>
    ExplicitTop = 404
    ExplicitWidth = 731
  end
  inherited tb2: TSpeedBar
    Width = 784
    ExplicitWidth = 731
    inherited laDep: TLabel
      Left = 616
      Visible = False
      ExplicitLeft = 616
    end
    object Label1: TLabel [2]
      Left = 8
      Top = 7
      Width = 73
      Height = 13
      Caption = #1042#1099#1073#1080#1088#1080#1090#1077' '#1075#1086#1076' '
      Transparent = True
    end
    object edYear: TDBNumberEditEh [3]
      Left = 81
      Top = 4
      Width = 60
      Height = 19
      EditButton.Style = ebsUpDownEh
      EditButton.Visible = True
      EditButtons = <>
      Flat = True
      MaxValue = 2050.000000000000000000
      MinValue = 2000.000000000000000000
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnChange = edYearChange
      OnKeyDown = edYearKeyDown
    end
    inherited spitPeriod: TSpeedItem [5]
    end
    inherited spitDep: TSpeedItem [6]
      Visible = False
    end
  end
  inherited tb1: TSpeedBar
    Width = 784
    ExplicitWidth = 731
    inherited spitPrint: TSpeedItem [4]
    end
    inherited spitEdit: TSpeedItem [5]
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000104A2000104A2000104A2000104A2000104A2000104A2000104
        A2000104A2000104A2000104A200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF000104A2005983FF000026FF000030FF000030FB00002FF200002FE900002E
        E1000030D8000031D0000034CB000104A200FF00FF00FF00FF00FF00FF00FF00
        FF000104A200ABC2FF006480FF006688FF006688FF006687FA006787F5006787
        F0005779E9004D70E4004D74E2000104A200FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF000104A2000104A2000104A2000104A2000104A2000104A2000104
        A2000104A2000104A2000104A200FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
      OnClick = acEditDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    Width = 784
    Height = 380
    DataSource = dmMain.dsrZList
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    UseMultiTitle = True
    Visible = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090'|'#1053#1086#1084#1077#1088
        Width = 53
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1044#1086#1082#1091#1084#1077#1085#1090'|'#1044#1072#1090#1072
        Width = 54
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'BD'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076'|'#1053#1072#1095#1072#1083#1086
        Width = 68
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ED'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076'|'#1050#1086#1085#1077#1094
        Width = 150
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
        Width = 181
      end>
  end
  inherited ilButtons: TImageList
    Left = 98
    Top = 184
  end
  inherited fmstr: TFormStorage
    Active = False
    Left = 160
    Top = 180
  end
  inherited ppDep: TPopupMenu
    Left = 216
    Top = 180
  end
  inherited acAEvent: TActionList
    Left = 44
    Top = 212
    inherited acPeriod: TAction
      Visible = False
    end
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
  end
  inherited ppDoc: TTBPopupMenu
    Left = 136
    Top = 128
  end
end
