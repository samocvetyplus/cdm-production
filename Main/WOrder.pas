{***********************************************}
{  �����. �������� ��������������               }
{  Copyrigth (C) 2001-2003 basile for CDM       }
{  v3.93                                        }
{  last update 12.11.2003                       }
{***********************************************}
unit WOrder;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DocAncestor, ImgList, Grids, DBGrids, RXDBCtrl,
  StdCtrls, Mask, DBCtrls, ExtCtrls,
  ComCtrls, Menus, db, Buttons, RXSplit, RxCurrEdit, RxCalc,
  TB2Item, TB2Dock, TB2Toolbar, DBGridEh, DBCtrlsEh, ActnList,
  PrnDbgeh, DBLookupEh, M207Ctrls, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar, ShlObj, Printers;

type
  TTransformInfo = record
    wOrderId : Variant;
    wOrderNo : string;
    JobId : integer;
    EndSemis : string;
    EndSemisName : string;
    FromOperId : string;
    FromOperName : string;
    RetFromOperId : string;
  end;

  TfmWOrder = class(TfmDocAncestor)
    lbOper: TLabel;
    lbExec: TLabel;
    ppInv: TPopupMenu;
    mnitPrint: TMenuItem;
    N3: TMenuItem;
    mnitAddToInv: TMenuItem;
    mnitAddToNew: TMenuItem;
    spitCalcLoss: TSpeedItem;
    mnitDelItem: TMenuItem;
    mnitSep1: TMenuItem;
    mnitCopy: TMenuItem;
    mnitIns: TMenuItem;
    mnitInsOrd: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    N4: TMenuItem;
    SpeedItem1: TSpeedItem;
    lbGet: TLabel;
    lnDone: TLabel;
    lbRet: TLabel;
    lbRej: TLabel;
    lbReWork: TLabel;
    txtReWork: TDBText;
    txtRej: TDBText;
    txtRet: TDBText;
    txtDone: TDBText;
    txtGet: TDBText;
    bvlT: TM207Bevel;
    lbStone: TLabel;
    lbF: TLabel;
    txtF_T: TDBText;
    Label3: TLabel;
    Label4: TLabel;
    txtStoneW: TDBText;
    txtStoneQ: TDBText;
    Label5: TLabel;
    Label1: TLabel;
    lbCurrInv: TLabel;
    Label6: TLabel;
    Bevel1: TBevel;
    txtADocNo: TDBText;
    lbAInv: TLabel;
    lbAInvFrom: TLabel;
    txtADocDt: TDBText;
    Label2: TLabel;
    txtBrW: TDBText;
    txtBrQ: TDBText;
    plExt: TPanel;
    plExtTiitle: TPanel;
    lbMat: TLabel;
    tlbrT: TTBToolbar;
    it0: TTBItem;
    it3: TTBItem;
    it5: TTBItem;
    it1: TTBItem;
    gridSemis: TDBGridEh;
    cmbxMat: TDBComboBoxEh;
    tbdcSemis: TTBDock;
    TBToolbar2: TTBToolbar;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    acEvent: TActionList;
    acAddSemis: TAction;
    acDelSemis: TAction;
    acAddItem: TAction;
    acAddNew: TAction;
    it7: TTBItem;
    Label8: TLabel;
    txtNk: TDBText;
    ppA: TPopupMenu;
    mnitCopyA: TMenuItem;
    mnitPasteA: TMenuItem;
    acACopy: TAction;
    acAPaste: TAction;
    N5: TMenuItem;
    acSCopy: TAction;
    acSPaste0: TAction;
    acSPaste1: TAction;
    acPrint: TAction;
    acArt: TAction;
    TBItem1: TTBItem;
    acTransform0: TAction;
    Label7: TLabel;
    cmbxOper: TDBComboBoxEh;
    TBItem2: TTBItem;
    acAddSemisNew: TAction;
    N6: TMenuItem;
    acPrintGrid: TAction;
    printItem: TPrintDBGridEh;
    lbWhSemisName: TLabel;
    it6: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    Action1: TAction;
    chbxW: TDBCheckBoxEh;
    acE: TAction;
    acExcel: TAction;
    Excel1: TMenuItem;
    acMultiSelect: TAction;
    lbDate: TLabel;
    acShowID: TAction;
    ac0: TAction;
    ac6: TAction;
    ac1: TAction;
    ac3: TAction;
    ac5: TAction;
    ac7: TAction;
    txtDefOper: TDBText;
    txtExec: TDBText;
    txtJobPS: TDBText;
    lcbxJobPS: TDBLookupComboboxEh;
    lcbxExec: TDBLookupComboboxEh;
    lcbxOper: TDBLookupComboboxEh;
    gridAEl: TDBGridEh;
    acPrintPreview: TAction;
    acPrintInvWithAssort: TAction;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    dtADocDate: TDBDateTimeEditEh;
    SpeedItem2: TSpeedItem;
    acTotalWOrder: TAction;
    acProcessAppl: TAction;
    PrinterSetupDialog: TPrinterSetupDialog;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mnitAddToInvClick(Sender: TObject);
    procedure spitCalcLossClick(Sender: TObject);
    procedure spitSumClick(Sender: TObject);
    procedure lcbxOperEnter(Sender: TObject);
    procedure it5Click(Sender: TObject);
    procedure spitStyleClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acAddSemisExecute(Sender: TObject);
    procedure gridSemisDblClick(Sender: TObject);
    procedure gridSemisKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cmbxMatChange(Sender: TObject);
    procedure acAddNewExecute(Sender: TObject);
    procedure acAddItemExecute(Sender: TObject);
    procedure acDelSemisExecute(Sender: TObject);
    procedure cmbxOperChange(Sender: TObject);
    procedure acACopyExecute(Sender: TObject);
    procedure acAPasteExecute(Sender: TObject);
    procedure acSCopyExecute(Sender: TObject);
    procedure acPasteExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acArtExecute(Sender: TObject);
    procedure acTransform0Execute(Sender: TObject);
    procedure acAddSemisNewExecute(Sender: TObject);
    procedure gridItemColumns7EditButtonClick(Sender: TObject; var Handled: Boolean);
    procedure gridItemExit(Sender: TObject);
    procedure gridItemKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh; AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintGridExecute(Sender: TObject);
    procedure acEExecute(Sender: TObject);
    procedure acExcelExecute(Sender: TObject);
    procedure acMultiSelectExecute(Sender: TObject);
    procedure acSPaste0Update(Sender: TObject);
    procedure acSPaste1Update(Sender: TObject);
    procedure acShowIDExecute(Sender: TObject);
    procedure ac0Execute(Sender: TObject);
    procedure ac6Execute(Sender: TObject);
    procedure ac1Execute(Sender: TObject);
    procedure ac3Execute(Sender: TObject);
    procedure ac5Execute(Sender: TObject);
    procedure ac7Execute(Sender: TObject);
    procedure acArtUpdate(Sender: TObject);
    procedure acAPasteUpdate(Sender: TObject);
    procedure acACopyUpdate(Sender: TObject);
    procedure lcbxExecEnter(Sender: TObject);
    procedure lcbxJobPSCloseUp(Sender: TObject; Accept: Boolean);
    procedure lcbxJobPSKeyValueChanged(Sender: TObject);
    procedure gridAElGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure FormResize(Sender: TObject);
    procedure acPrintInvWithAssortExecute(Sender: TObject);
    procedure acPrintPreviewExecute(Sender: TObject);
    procedure acTotalWOrderExecute(Sender: TObject);
    procedure acProcessApplExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure spitCloseClick(Sender: TObject);
    function GetSpecialPath(CSIDL: word): string;
  private
    FCurrDoc : string;
    FUseLog : boolean;
    FExtended : boolean;
    FItType : byte;
    FSearchEnable : boolean;
    procedure SetExtended(const Value: boolean);
    procedure SetItType(const Value: byte); // ����� ������� ���������, ������������ ������ ��� �����������

    function TryDelete: Boolean;
    function TryInsert: Boolean;
    function TryUpdate: Boolean;
  protected
    FTransformInfo : TTransformInfo;
    function FillTransformInfo : boolean;
    procedure e_Transform0;
    procedure e_Transform5;
    procedure Access;
    procedure SetReadOnly(ReadOnly : boolean);
    procedure SetOPeriod;
    procedure SetMode;
    procedure FilterPs(DataSet : TDataSet; var Accept : boolean);
    function IsActionMayBe : boolean;

  public
    procedure SetCurrInv;
    property Extended : boolean read FExtended write SetExtended;
    property ItType : byte read FItType write SetItType; // 0 - ������, 1 - �����
  end;

implementation

uses MainData, dbUtil, DictData, PrintData, fmUtils, Variants, ApplData,
     ADistr, dbTree, DictAncestor, dSemis, eSQ, Editor, DateUtils, dSemisT1,
     Log, M207Export, eInvCreateMode, DbEditor, MsgDialog,
     dOper, DbUtilsEh, dDepT1, WOrderTotal, ProductionConsts,
     frmDialogSemis, uSemisStorage;

{$R *.DFM}

procedure TfmWOrder.FormCreate(Sender: TObject);
begin
  edNoDoc.DataSource := dmMain.dsrWOrderList;
  dtedDateDoc.DataSource := dmMain.dsrWOrderList;
  gridItem.DataSource := dmMain.dsrWOrder;
  gridItem.FieldColumns['INVID'].Visible    := False;
  gridItem.FieldColumns['WOITEMID'].Visible := False;
  gridItem.FieldColumns['WORDERID'].Visible := False;
  gridItem.FieldColumns['ITDATE'].Visible   := False;
  gridItem.FieldColumns['REJREF'].Visible   := False;

  lbWhSemisName.Caption := '��������: '+dm.User.wWhName;
  FDocName := '�����';
  dm.WorkMode := 'wOrder';
  gridSemis.RowLines := 2;
  // ��������� �������� ��� ������ ��������������
  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMat);
  cmbxMat.ItemIndex := 0;
  dm.AMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := cmbxMatChange;

  cmbxOper.OnChange := nil;
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.Items.Insert(1, sNoOperation);
  cmbxOper.ItemIndex := 0;
  dmMain.FilterOperId := ROOT_OPER_CODE;
  cmbxOper.OnChange := cmbxOperChange;

  // ������������� ������
  FSearchEnable := False;
  //edFind.Text := '';
  FSearchEnable := True;

  // ��������� ����� ������
  FItType := 255;

  FExtended := False;//True;
  Extended := True;  //False;
  //spitStyle.BtnCaption := '�����������'; // '�������'

  // ����� �������
  Access;

  inherited;

  OpenDataSets([dm.taSemis, dm.taOper, dmMain.taPsDep, dmMain.taPsMol, dmMain.taWOrder,
    dm.taDepart, dmMain.quWOrder_T]);

  dmMain.taPsOper.Open;

  if not dmMain.taWOrderListJOBDEPID.IsNull and
         dmMain.taWOrderListJOBID.IsNull and
         (not dmMain.taPSMol.IsEmpty) then
  begin
    dmMain.taWOrderList.Edit;
    dmMain.taWOrderListJOBID.AsInteger := dmMain.taPSMolMOLID.AsInteger;
    dmMain.taWOrderList.Post;
    dmMain.taWOrderList.Transaction.CommitRetaining;
  end;

  InitBtn(Boolean(dmMain.taWOrderListISCLOSE.AsInteger));
  SetCurrInv;
  bvlT.Caption := '����� �� ������ � '+DateToStr(dm.BeginDate)+' �� '+
    DateToStr(dm.EndDate);
  SetOPeriod;
  SetMode;
  Self.Resize;
  ActiveControl := gridItem;

  if dmMain.taWOrderListDEPID.AsInteger in [250, 251] then
    begin
      Label5.Caption := '�������';
      lbF.Caption := StringReplace(lbF.Caption, '�����', '������', [rfReplaceAll]);
    end;

  if dm.User.wWhName <> '' then
  begin
    Self.Caption := Self.Caption + ' - ' + dm.User.wWhName;
    stbrStatus.Panels[2].Text := dm.User.wWhName;
  end;
end;

procedure TfmWOrder.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PostDataSets([dmMain.taAssortInv]);
//  if .State in [dsEdit, dsInsert] then dmMain.taWOrder.Post;
  CloseDataSets([dmMain.taPsOper, dmMain.taPsDep, dm.taSemis,
    dm.taDepart, dmMain.quWOrder_T, dmMain.taPsMol, dm.taOper, dmMain.taAssortInv]);
  dm.AMatId := ROOT_MAT_CODE;
inherited;
end;

procedure TfmWOrder.SetCurrInv;
var
  Id : Variant;
begin
  with dmMain do
  begin
    Id := ExecSelectSQL('select InvId from GetCurrInv('+
      IntToStr(taWOrderListWORDERID.AsInteger)+','+IntToStr(dm.User.wWhId)+')', quTmp);
    if VarIsNull(Id)or(Id = 0)then
    begin
      CurrInv := -1;
      FCurrDoc := '����������';
    end
    else begin
      CurrInv := Id;
      if(Id = 0 )then FCurrDoc := ''
      else
        try
          FCurrDoc := ExecSelectSQL('select DocNo from GetCurrInv('+
            IntToStr(taWOrderListWORDERID.AsInteger)+','+IntToStr(dm.User.wWhId)+')', quTmp);
        except
        end
    end;
  end;
  lbCurrInv.Caption := FCurrDoc;
end;

procedure TfmWOrder.mnitAddToInvClick(Sender: TObject);
begin
  dmMain.CurrInv := dmMain.taWOrderINVID.AsInteger;
  dmMain.InvMode := 1;
  dmMain.taWOrder.Append;
  gridItem.SelectedField := dmMain.taWorderSEMISNAME;
end;

procedure TfmWOrder.spitCalcLossClick(Sender: TObject);
begin
  PostDataSets([dmMain.taWOrder]);
  dm.tr.CommitRetaining;
end;

procedure TfmWOrder.spitCloseClick(Sender: TObject);
begin
  inherited;
  Access;
end;

procedure TfmWOrder.spitSumClick(Sender: TObject);
begin
if dmMain.taWOrder.State in [dsEdit, dsInsert] then
  begin
    gridItem.DataSource.DataSet.Post;
    CommitRetaining(gridItem.DataSource.DataSet);
  end;
ReOpenDataSets([dmMain.quWOrder_T]);
end;

procedure TfmWOrder.lcbxOperEnter(Sender: TObject);
begin
  if dmMain.taWOrderListJOBDEPID.IsNull then
  begin
    ActiveControl := lcbxJobPS;
    raise Exception.Create(Format(rsSelectPS, ['']));
  end
  else begin
    ReOpenDataSet(dmMain.taPsOper);
    if not (dmMain.taWOrderList.State in [dsInsert, dsEdit]) then dmMain.taWOrderList.Edit;
    dmMain.taWOrderListDEFOPER.AsString := dmMain.taPsOperOPERID.AsString;
  end;
end;

procedure TfmWOrder.SetExtended(const Value: boolean);
begin
  if Value = FExtended then eXit;
  FExtended := Value;
  if Value then
  begin
    tlbrT.Visible := True;
    tbdcSemis.Visible := True;
    plExt.Visible := True;
    ItType := 0;
  end
  else begin
    plExt.Visible := False;
    tbdcSemis.Visible := False;
    tlbrT.Visible := False;
  end
end;

procedure TfmWOrder.it5Click(Sender: TObject);
begin
  ItType := 5;
end;

procedure TfmWOrder.spitStyleClick(Sender: TObject);
begin
  {if(spitStyle.BtnCaption = '�������') then
  begin
    spitStyle.BtnCaption := '�����������';
    Extended := True;
  end
  else begin
    spitStyle.BtnCaption := '�������';
    Extended := False;
  end}
end;

procedure TfmWOrder.SetItType(const Value: byte);
begin
  if FItType= Value then eXit;
  FItType := Value;
  ReOpenDataSet(dmMain.taWhSemis);
  case FItType of
    0 : begin
      it0.Checked := True;
      acTransform0.Enabled := True; 
    end;
    1 : begin
      it1.Checked := True;
      acTransform0.Enabled := False;
    end;
    3 : begin
      it3.Checked := True;
      acTransform0.Enabled := False;
    end;
    5 : begin
      it5.Checked := True;
      acTransform0.Enabled := True;
    end;
    6 : begin
      it6.Checked := True;
      acTransform0.Enabled := False;
    end;
    7 : begin
      it7.Checked := True;
      acTransform0.Enabled := False;
    end
  end;
end;

procedure TfmWOrder.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    {ord('F'), ord('f'), ord('�'), ord('�') :
      if(Shift = [ssCtrl])and Extended then ActiveControl := edFind;}
    VK_F4 : Extended := not Extended;
  end;

end;

procedure TfmWOrder.acAddSemisExecute(Sender: TObject);
const
  t_1 : set of byte = [0, 6];
  t_2 : set of byte = [1, 3, 5];
  t_3 : set of byte = [7];
  Rec : TRecName = (sId: 'SEMISID'; sName: 'NAME'; sTable: 'D_SEMIS'; sWhere: '');
  RecRej : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_REJ'; sWhere: '');
var
  t : Variant;
  RejId, RejPsId : Variant;

  SourceStorage: TStorage;
  TargetStorage: TStorage;
  SpoilageDelta: TSpoilageDelta;
  SourceWoItemID: Integer;
  TargetWoItemID: Integer;

  ResultQuantity: Integer;
  ResultWeight: Double;
  TotalResultQuantity: Integer;
  TotalResultWeight: Double;

procedure Go;
begin
  with dmMain do
  begin
    case dmMain.WorderMode of
      wmWOrder : begin
        (* ���� ��������� ��������� �� ��������� �� ���� � ����� ������ �� ������
           ����� ��������� ����� ���������, ����� ��������� � ������ *)

        if(CurrInv = -1)then t := ItType
        else
          t := ExecSelectSQL('select max(ItType) from woItem where InvId='+
               IntToStr(dmMain.CurrInv), dmMain.quTmp);
      {   0 - ������
          1 - �����
          3 - �������
          5 - ����
          6 - ���������
          7 - ���������� }
        case t of
          0, 6 : if(ItType in [0, 6])then acAddItem.Execute else acAddNew.Execute;
          1, 3, 5 : if(ItType in [1, 3, 5])then acAddItem.Execute else acAddNew.Execute;
          7 : if(ItType in [7])then acAddItem.Execute else acAddNew.Execute;
          else raise EInternal.Create(Format(rsInternalError, ['101']));
        end;
        taWOrderSEMIS.AsString := taWhSemisSEMISID.AsString;

        if(taWhSemisGOOD.AsInteger = 0) then
        begin
          if(taWhSemisOPERID.AsString = dm.RejOperId)or
            (taWhSemisOPERID.AsString = dm.NkOperId)or
            (taWhSemisOPERID.AsString = dm.TransformOperId)
          then
            case ItType of
               0, 1, 2, 3, 5, 6, 7 : taWOrderFROMOPERID.AsVariant := taWhSemisOPERID.AsVariant;
               else raise EInternal.Create(Format(rsInternalError, ['101']));
            end
          else
            case ItType of
               0, 1, 3, 5, 6, 7 : taWOrderFROMOPERID.AsVariant := Null;
               else raise EInternal.Create(Format(rsInternalError, ['101']));
            end
          end
        else
          case ItType of
             0, 1, 3, 5, 6, 7 : taWOrderFROMOPERID.AsVariant := taWhSemisOPERID.AsVariant;
             else raise EInternal.Create(Format(rsInternalError, ['101']));
          end;

        taWOrderQ.AsInteger := SemisQ;
        taWOrderW.AsFloat := SemisW;
        taWOrderREJID.AsVariant := RejId;
        taWOrderREJPSID.AsVariant := RejPsId;
        taWOrder.Post;
        taWhSemis.Refresh;
      end;
    end;
  end;
end;

begin
  if not TryInsert then Exit;

  if dmMain.taWhSemis.IsEmpty then
  raise Exception.Create(Format(rsNoData, ['��� �����������']));

  if (ItType = 6) and (dmMain.taWhSemisOPERID.AsString = dm.RejOperId)then
  begin
     raise EWarning.Create('�������� �� ��������� � ��������� ����� ���������');
     Exit;
  end;

  RejId := Null;
  RejPsId := Null;

  with dmMain do
  begin
    // ������ ���� � �������� �� ����� ���������
    if (ItType = 0) and (dmMain.taWhSemisOPERID.AsString = dm.RejOperId) then
    begin
      if DialogSemis <> nil then FreeAndNil(DialogSemis);
      try
        DialogSemis := TDialogSemis.Create(Self);

        SourceStorage := TStorage.Create;
        SourceStorage.ID := dmMain.taWhSemisDEPID.AsInteger;
        SourceStorage.Element.Code := Trim(dmMain.taWhSemisSEMISID.AsString);
        SourceStorage.Element.Operation := Trim(dm.RejOperId);
        SourceStorage.Element.UnitQuantity := dmMain.taWhSemisUQ.AsInteger;
        SourceStorage.Element.UnitWeight := dmMain.taWhSemisUW.AsInteger;

        TargetStorage := TStorage.Create;
        TargetStorage.ID := taWOrderListJOBDEPID.AsInteger;
        TargetStorage.Element.Code := Trim(dmMain.taWhSemisSEMISID.AsString);
        TargetStorage.Element.Operation := Trim(taWOrderListDEFOPER.AsString);
        TargetStorage.Element.UnitQuantity := dmMain.taWhSemisUQ.AsInteger;
        TargetStorage.Element.UnitWeight := dmMain.taWhSemisUW.AsInteger;

        if DialogSemis.Execute(SourceStorage, TargetStorage, smOut) then
        begin
          DialogSemis.DataSetTargetSummary.First;
          while not DialogSemis.DataSetTargetSummary.Eof do
          begin
            RejId := DialogSemis.DataSetTargetSummaryReasonID.AsInteger;
            RejPsId := DialogSemis.DataSetTargetSummaryOriginatorID.AsInteger;

            dmMain.SemisQ := DialogSemis.DataSetTargetSummaryQuantity.AsInteger;
            dmMain.SemisW := DialogSemis.DataSetTargetSummaryWeight.AsFloat;

            Go;

            DialogSemis.DataSetTarget.SetRange([RejPsId, RejId], [RejPsId, RejId]);
            DialogSemis.DataSetTarget.First;

            while not DialogSemis.DataSetTarget.Eof do
            begin
              SpoilageDelta.StorageElementID := TargetStorage.Element.ID;
              SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
              SpoilageDelta.OperationID := DialogSemis.DataSetTargetOperationID.AsInteger;
              SpoilageDelta.Quantity := DialogSemis.DataSetTargetQuantity.AsInteger;
              SpoilageDelta.Weight := DialogSemis.DataSetTargetWeight.AsFloat;
              TDialogSemis.Delta(SpoilageDelta);

              SpoilageDelta.StorageElementID := SourceStorage.Element.ID;
              SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
              SpoilageDelta.OperationID := DialogSemis.DataSetTargetOperationID.AsInteger;
              SpoilageDelta.Quantity := - DialogSemis.DataSetTargetQuantity.AsInteger;
              SpoilageDelta.Weight := - DialogSemis.DataSetTargetWeight.AsFloat;
              TDialogSemis.Delta(SpoilageDelta);
              DialogSemis.DataSetTarget.Next;
           end;
           DialogSemis.DataSetTargetSummary.Next;
          end;
       end;
       finally
         if SourceStorage <> nil then FreeAndNil(SourceStorage);
         if TargetStorage <> nil then FreeAndNil(TargetStorage);
         if DialogSemis <> nil then FreeAndNil(DialogSemis);
       end;
    end
    else
    if (ItType = 3) and (dmMain.taWhSemisOPERID.AsString = dm.RejOperId) then
    begin
      if dmMain.taWOrderREJID.AsInteger = 0 then
      begin
        raise EWarning.Create('���������� ������� ����������� �����.');
        Exit;
      end;
      SourceWoItemID := dmMain.taWOrderWOITEMID.AsInteger;

      SourceStorage := TStorage.Create;
      SourceStorage.ID := dm.User.wWhId;
      SourceStorage.Connected := True;
      SourceStorage.Element.Code := dmMain.taWOrderSEMIS.AsString;
      SourceStorage.Element.Operation := dm.RejOperId;
      SourceStorage.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
      SourceStorage.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
      SourceStorage.Element.Connected := True;

      TargetStorage := TStorage.Create;
      TargetStorage.ID := dmMain.taWOrderJOBDEPID.AsInteger;
      TargetStorage.Element.Code := dmMain.taWOrderSEMIS.AsString;
      TargetStorage.Element.Operation := dmMain.taWOrderOPERID.AsString;
      TargetStorage.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
      TargetStorage.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
      TargetStorage.Element.Connected := True;

      if DialogSemis <> nil then FreeAndNil(DialogSemis);
      DialogSemis := TDialogSemis.Create(Self);
      if DialogSemis.Execute(SourceStorage, TargetStorage, smIn, dmMain.taWOrderWOITEMID.AsInteger) then
      begin
        TotalResultQuantity := 0;
        TotalResultWeight := 0;
        DialogSemis.DataSetSource.First;
        while not DialogSemis.DataSetSource.Eof do
        begin
          ResultQuantity := DialogSemis.DataSetSourceQuantity.AsInteger;
          ResultWeight := DialogSemis.DataSetSourceWeight.AsFloat;
          TotalResultQuantity := TotalResultQuantity + ResultQuantity;
          TotalResultWeight := TotalResultWeight + ResultWeight;
          DialogSemis.DataSetSource.Next;
        end;
        dmMain.SemisQ := TotalResultQuantity;
        dmMain.SemisW := TotalResultWeight;
        Go;

        TargetWoItemID := dmMain.taWOrderWOITEMID.AsInteger;
        ExecSQL('update woitem set REF$WOITEMID = ' + IntToStr(SourceWoItemID) + ' where woitemid = ' + IntToStr(TargetWoItemID), dmMain.quTmp);

        DialogSemis.DataSetSource.First;
        while not DialogSemis.DataSetSource.Eof do
        begin
          ResultQuantity := DialogSemis.DataSetSourceQuantity.AsInteger;
          ResultWeight := DialogSemis.DataSetSourceWeight.AsFloat;

          SpoilageDelta.StorageElementID := SourceStorage.Element.ID;
          SpoilageDelta.InvoiceElementID := TargetWoItemID;
          SpoilageDelta.OperationID := DialogSemis.DataSetSourceOperationID.AsInteger;
          SpoilageDelta.Quantity := ResultQuantity;
          SpoilageDelta.Weight := ResultWeight;

          TDialogSemis.Delta(SpoilageDelta);

          SpoilageDelta.StorageElementID := TargetStorage.Element.ID;
          SpoilageDelta.InvoiceElementID := SourceWoItemID;
          SpoilageDelta.OperationID := DialogSemis.DataSetSourceOperationID.AsInteger;
          SpoilageDelta.Quantity := -ResultQuantity;
          SpoilageDelta.Weight := -ResultWeight;
          TDialogSemis.Delta(SpoilageDelta);

          DialogSemis.DataSetSource.Next;
        end;
     end;
     SourceStorage.Free;
     TargetStorage.Free;
     FreeAndNil(DialogSemis);
     Exit;
    end
    else
    // �������� ����� � ����� �������� � ��������
    if (ItType = 5) then
    begin
      if not _ShowEditorQW(dmMain.taWhSemisQ.AsInteger, dmMain.taWhSemisW.AsFloat, chbxW.Checked) then eXit;

      Go;

      TargetStorage := TStorage.Create;
      TargetStorage.ID := dm.User.wWhId;
      TargetStorage.Element.Code := Trim(dmMain.taWhSemisSEMISID.AsString);
      TargetStorage.Element.Operation := dm.RejOperId;
      TargetStorage.Element.UnitQuantity := dmMain.taWhSemisUQ.AsInteger;
      TargetStorage.Element.UnitWeight := dmMain.taWhSemisUW.AsInteger;
      TargetStorage.Connected := True;
      TargetStorage.Element.Connected := True;

      SpoilageDelta.StorageElementID := TargetStorage.Element.ID;
      SpoilageDelta.InvoiceElementID := dmmain.taWOrderWOITEMID.AsInteger;

      //SpoilageDelta.OperationID := ExecSelectSQL('select ID from d_oper where operid = ' + #39 + dmMain.taWhSemisOPERID.AsString + #39, dmMain.quTmp, False);
      SpoilageDelta.OperationID := ExecSelectSQL('select ID from d_oper where operid = ' + #39 + taWOrderListDEFOPER.AsString+ #39, dmMain.quTmp, False);
      SpoilageDelta.Quantity := dmMain.SemisQ;
      SpoilageDelta.Weight := dmMain.SemisW;

      TargetStorage.Free;
      TDialogSemis.Delta(SpoilageDelta);
     end
    else
    begin
      if not _ShowEditorQW(dmMain.taWhSemisQ.AsInteger, dmMain.taWhSemisW.AsFloat, chbxW.Checked) then eXit;
      Go;
    end;

  end;
end;

procedure TfmWOrder.gridSemisDblClick(Sender: TObject);
begin
  acAddSemis.Execute;
end;

procedure TfmWOrder.gridSemisKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_SPACE : begin
       acAddSemis.Execute;
    end;
  end
end;
         
procedure TfmWOrder.cmbxMatChange(Sender: TObject);
begin
  dm.AMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSets([dmMain.taWhSemis]);
end;

procedure TfmWOrder.acAddNewExecute(Sender: TObject);
begin
  dmMain.InvMode := 0;
  dmMain.taWOrder.Append;
  if Extended then
  begin
    dmMain.taWOrderITTYPE.AsInteger := ItType;
    gridItem.SelectedField := dmMain.taWorderSEMISNAME;
  end
  else gridItem.SelectedField := dmMain.taWorderITTYPE;
end;

procedure TfmWOrder.acAddItemExecute(Sender: TObject);
begin
  dmMain.InvMode := 1;
  dmMain.taWOrder.Append;
  if Extended then
  begin
    dmMain.taWOrderITTYPE.AsInteger := ItType;
    gridItem.SelectedField := dmMain.taWorderSEMISNAME;
  end
  else gridItem.SelectedField := dmMain.taWorderITTYPE;
end;

procedure TfmWOrder.acDelSemisExecute(Sender: TObject);
var
  b : boolean;
  t : byte;
  SemisId, OperId : string;
  DepId, Id : integer;
  Ref : Variant;
  Source, Target: TStorage;
  SpoilageDelta: TSpoilageDelta;
  ResultQuantity: Integer;
  ResultWeight: Double;
  TotalResultQuantity: Integer;
  TotalResultWeight: Double;

  SourceWoItemID: Integer;
  TargetWoItemID: Integer;

procedure Go;
begin
  with dmMain do
  begin
     if(dmMain.SemisQ = dmMain.taWOrderQ.AsInteger) and (Abs(dmMain.SemisW - dmMain.taWOrderW.AsFloat)<Eps)then
     begin
         //taWOrder.BeforeDelete := nil;
         taWOrder.Delete;
        //taWOrder.BeforeDelete := taWOrderBeforeDelete;
     end
     else
     begin
       taWOrder.Edit;
       taWOrderQ.AsInteger := taWOrderQ.AsInteger - dmMain.SemisQ;
       taWOrderW.AsFloat := taWOrderW.AsFloat - dmMain.SemisW;
       taWOrder.Post;
     end;
     if not b then
     begin
       taWhSemis.Append;
       taWhSemisDEPID.AsInteger := DepId;
       taWhSemisOPERID.AsString := OperId;
       taWhSemisSEMISID.AsString := SemisId;
       taWhSemisQ.AsInteger := 0;
       taWhSemisW.AsInteger := 0;
       taWhSemis.Post;
     end;
     taWhSemis.Refresh;
  end;
end;

begin
  if not TryDelete then Exit;

  (* ����� �� �������� ������������� *)
  if(dmMain.taWOrderDEPID.AsInteger <> dm.User.wWhId) then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taWOrderDEPNAME.AsString]));
  if dmMain.taWOrder.IsEmpty then raise EWarning.Create(Format(rsNoData, ['��� ��������']));
  with dmMain do
  begin
    case WOrderMode of
      wmWOrder : begin
        if Extended then
        begin
          t := taWOrderITTYPE.AsInteger;
          if t <> ItType then ItType := t;
          if taWOrderFROMOPERID.IsNull then b := taWhSemis.Locate('SEMISID', taWOrderSEMIS.AsString, [])
          else b := taWhSemis.Locate('SEMISID;OPERID', VarArrayOf([taWOrderSEMIS.AsString, taWOrderFROMOPERID.AsString]), []);
          if not b then
          begin
            SemisId := taWOrderSEMIS.AsString;
            OperId := taWOrderFROMOPERID.AsString;
            case ItType of
              0, 6    : DepId := taWOrderDEPID.AsInteger;
              1,3,5,7 : DepId := taWOrderJOBDEPID.AsInteger;
              else raise EInternal.Create(Format(rsInternalError, ['101']));
            end;
          end;
          dmMain.SemisQ := dmMain.taWOrderQ.AsInteger;
          dmMain.SemisW := dmMain.taWOrderW.AsFloat;

          if (taWOrderITTYPE.AsInteger = 0) and (taWOrderREJID.AsInteger <> 0) then
          begin
            Source := TStorage.Create;
            Source.ID := dm.User.wWhId;
            Source.Connected := True;
            Source.Element.Code := dmMain.taWOrderSEMIS.AsString;
            Source.Element.Operation := dm.RejOperId;
            Source.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
            Source.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
            Source.Element.Connected := True;

            Target := TStorage.Create;
            Target.ID := dmMain.taWOrderJOBDEPID.AsInteger;
            Target.Element.Code := dmMain.taWOrderSEMIS.AsString;
            Target.Element.Operation := dmMain.taWOrderOPERID.AsString;
            Target.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
            Target.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
            Target.Element.Connected := True;

            if DialogSemis <> nil then FreeAndNil(DialogSemis);
            DialogSemis := TDialogSemis.Create(Self);
            if DialogSemis.Execute(Source, Target, smIn, dmMain.taWOrderWOITEMID.AsInteger) then
            begin
              DialogSemis.DataSetSource.First;
              TotalResultQuantity := 0;
              TotalResultWeight := 0;
              while not DialogSemis.DataSetSource.Eof do
              begin

               ResultQuantity := DialogSemis.DataSetSourceQuantity.AsInteger;
               ResultWeight := DialogSemis.DataSetSourceWeight.AsFloat;
               TotalResultQuantity := TotalResultQuantity + ResultQuantity;
               TotalResultWeight := TotalResultWeight + ResultWeight;

                SpoilageDelta.StorageElementID := Source.Element.ID;
                SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
                SpoilageDelta.OperationID := DialogSemis.DataSetSourceOperationID.AsInteger;
                SpoilageDelta.Quantity := ResultQuantity;
                SpoilageDelta.Weight := ResultWeight;

                TDialogSemis.Delta(SpoilageDelta);

                SpoilageDelta.StorageElementID := Target.Element.ID;
                SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
                SpoilageDelta.OperationID := DialogSemis.DataSetSourceOperationID.AsInteger;
                SpoilageDelta.Quantity := -ResultQuantity;
                SpoilageDelta.Weight := -ResultWeight;
                TDialogSemis.Delta(SpoilageDelta);

                DialogSemis.DataSetSource.Next;
             end;
             dmMain.SemisQ := TotalResultQuantity;
             dmMain.SemisW := TotalResultWeight;
             Go;
           end;
           Source.Free;
           Target.Free;
           FreeAndNil(DialogSemis);
           Exit;
          end
          else
          if (taWOrderITTYPE.AsInteger = 3) and (taWOrderFROMOPERID.AsString = dm.RejOperId) then
          begin
            Source := TStorage.Create;
            Source.ID := dmMain.taWOrderJOBDEPID.AsInteger;
            Source.Connected := True;
            Source.Element.Code := dmMain.taWOrderSEMIS.AsString;
            Source.Element.Operation := dmMain.taWOrderListDEFOPER.AsString;
            Source.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
            Source.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
            Source.Element.Connected := True;

            Target := TStorage.Create;
            Target.ID := dm.User.wWhId;
            Target.Element.Code := dmMain.taWOrderSEMIS.AsString;
            Target.Element.Operation := dm.RejOperId;
            Target.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
            Target.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
            Target.Element.Connected := True;

            if DialogSemis <> nil then FreeAndNil(DialogSemis);
            DialogSemis := TDialogSemis.Create(Self);
            if DialogSemis.Execute(Source, Target, smIn, dmMain.taWOrderWOITEMID.AsInteger) then
            begin
              TotalResultQuantity := 0;
              TotalResultWeight := 0;
              DialogSemis.DataSetSource.First;
              while not DialogSemis.DataSetSource.Eof do
              begin
                ResultQuantity := DialogSemis.DataSetSourceQuantity.AsInteger;
                ResultWeight := DialogSemis.DataSetSourceWeight.AsFloat;
                TotalResultQuantity := TotalResultQuantity + ResultQuantity;
                TotalResultWeight := TotalResultWeight + ResultWeight;
                DialogSemis.DataSetSource.Next;
              end;
              dmMain.SemisQ := TotalResultQuantity;
              dmMain.SemisW := TotalResultWeight;

              TargetWoItemID := dmMain.taWOrderWOITEMID.AsInteger;

              SourceWoItemID := ExecSelectSQL('select ref$woitemid from woitem where woitemid = ' + IntTostr(TargetWoItemID), dmMain.quTmp);

              Go;
              DialogSemis.DataSetSource.First;
              while not DialogSemis.DataSetSource.Eof do
              begin
                ResultQuantity := DialogSemis.DataSetSourceQuantity.AsInteger;
                ResultWeight := DialogSemis.DataSetSourceWeight.AsFloat;

                SpoilageDelta.StorageElementID := Source.Element.ID;
                SpoilageDelta.InvoiceElementID := SourceWoItemID;
                SpoilageDelta.OperationID := DialogSemis.DataSetSourceOperationID.AsInteger;
                SpoilageDelta.Quantity := ResultQuantity;
                SpoilageDelta.Weight := ResultWeight;

                TDialogSemis.Delta(SpoilageDelta);

                SpoilageDelta.StorageElementID := Target.Element.ID;
                SpoilageDelta.InvoiceElementID := TargetWoItemID;
                SpoilageDelta.OperationID := DialogSemis.DataSetSourceOperationID.AsInteger;
                SpoilageDelta.Quantity := -ResultQuantity;
                SpoilageDelta.Weight := -ResultWeight;

                TDialogSemis.Delta(SpoilageDelta);

                DialogSemis.DataSetSource.Next;
             end;
           end;
           Source.Free;
           Target.Free;
           FreeAndNil(DialogSemis);
           Exit;
          end
          else
          begin
            if(ShowEditor(TfmeSQ, TfmEditor(fmeSQ))<>mrOk)then eXit;
            if taWOrderITTYPE.AsInteger = 5 then
            begin
              Source := TStorage.Create;
              Source.ID := dm.User.wWhId;
              Source.Connected := True;
              Source.Element.Code := dmMain.taWOrderSEMIS.AsString;
              Source.Element.Operation := dm.RejOperId;
              Source.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
              Source.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
              Source.Element.Connected := True;

              SpoilageDelta.StorageElementID := Source.Element.ID;
              SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;

              //SpoilageDelta.OperationID := ExecSelectSQL('select ID from d_oper where operid = ' + #39 + dmMain.taWOrderFROMOPERID.AsString + #39, dmMain.quTmp, False);
              SpoilageDelta.OperationID := ExecSelectSQL('select ID from d_oper where operid = ' + #39 + taWOrderListDEFOPER.AsString+ #39, dmMain.quTmp, False);
              SpoilageDelta.Quantity := - dmMain.SemisQ;
              SpoilageDelta.Weight := - dmMain.SemisW;

              Source.Free;
              TDialogSemis.Delta(SpoilageDelta);
            end;
            Go;
          end;
        end
        else
        begin
          taWOrder.Delete;
        end;
      end;
      wmRej : begin
        // �������� ���������� ������� ������, � ����� �������, ������ ��� �
        // ������ ����� ���� �������� �����������
        case taWOrderITTYPE.AsInteger of
          0 : begin
            Id  := taWOrderWOITEMID.AsInteger;
            if taWOrderREJREF.IsNull then raise EInternal.Create(Format(rsInternalError, ['142']));
            Ref := taWOrderREJREF.AsInteger;
            if not taWOrder.Locate('WOITEMID', taWOrderREJREF.AsInteger, []) then raise EInternal.Create(Format(rsInternalError, ['143']));
          end;
          3 : begin
            Ref := ExecSelectSQL('select WOItemId from WOItem where RejRef = '+taWOrderWOITEMID.AsString, quTmp);
            if VarIsNull(Ref)or(Ref = 0)then raise EInternal.Create(Format(rsInternalError, ['144']));
            if not taWOrder.Locate('WOITEMID', Ref, [])then raise EInternal.Create(Format(rsInternalError, ['146']));
            Id := taWOrderWOITEMID.AsInteger;
          end;
        end;

        // ������� �� ������
        taWOrder.Locate('WOITEMID', Id, []);
        taWOrder.Delete;
        taWOrder.Locate('WOITEMID', Ref, []);
        if taWOrderFROMOPERID.IsNull then b := taWhSemis.Locate('SEMISID', taWOrderSEMIS.AsString, [])
        else b := taWhSemis.Locate('SEMISID;OPERID', VarArrayOf([taWOrderSEMIS.AsString, taWOrderFROMOPERID.AsString]), []);
        if not b then
        begin
          SemisId := taWOrderSEMIS.AsString;
          OperId := taWOrderFROMOPERID.AsString;
          case ItType of
             0, 6    : DepId := taWOrderDEPID.AsInteger;
             1,3,5,7 : DepId := taWOrderJOBDEPID.AsInteger;
             else raise EInternal.Create(Format(rsInternalError, ['101']));
           end;
        end;
        dmMain.SemisQ := dmMain.taWOrderQ.AsInteger;
        dmMain.SemisW := dmMain.taWOrderW.AsFloat;
        taWOrder.Delete;
        if not b then
        begin
          taWhSemis.Append;
          taWhSemisDEPID.AsInteger := DepId;
          taWhSemisOPERID.AsString := OperId;
          taWhSemisSEMISID.AsString := SemisId;
          taWhSemisQ.AsInteger := 0;
          taWhSemisW.AsInteger := 0;
          taWhSemis.Post;
        end;
        taWhSemis.Refresh;
      end;
    end; // case
  end; // with dmMain
end;

procedure TfmWOrder.cmbxOperChange(Sender: TObject);
begin
  with cmbxOper, Items do
    if(Items[ItemIndex] = sNoOperation) then dmMain.FilterOperId := sNoOperation
    else dmMain.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taWHSemis);
end;


procedure TfmWOrder.acACopyExecute(Sender: TObject);
var
  Bookmark : Pointer;
  i : integer;
begin
  with dmMain do
  begin
    if not taAssortEl.Active then eXit;
    ExecSQL('delete from B_AInv where UserId='+IntToStr(dm.User.UserId), dmMain.quTmp);
    if dgMultiSelect in gridAEl.Options then
      for i:=0 to Pred(gridAEl.SelectedRows.Count) do
      begin
        taAssortEl.GotoBookmark(Pointer(gridAEl.SelectedRows.Items[i]));
        ExecSQL('insert into B_AInv(ID, USERID, ART2ID, SZID, '+
           'OPERID, U, Q) values('+IntToStr(dm.GetId(33))+','+IntToStr(dm.User.UserId)+','+
           taAssortElART2.AsString+','+taAssortElSZID.AsString+','#39+
           taAssortElOPERID.AsString+#39','+taAssortElU.AsString+','+taAssortElQ.AsString+')', dmMain.quTmp);
    end
    else begin
      BookMark := taAssortEl.GetBookmark;
      try
        taAssortEl.First;
        while not taAssortEl.Eof do
        begin
          ExecSQL('insert into B_AInv(ID, USERID, ARTID, ART2ID, SZID, '+
             'OPERID, U, Q) values('+IntToStr(dm.GetId(33))+','+IntToStr(dm.User.UserId)+','+
             taAssortElARTID.AsString+','+taAssortElART2ID.AsString+','+
             taAssortElSZID.AsString+','#39+taAssortElOPERID.AsString+#39','+
             taAssortElU.AsString+','+taAssortElQ.AsString+')', dmMain.quTmp);
          taAssortEl.Next;
        end;
        taAssortEl.Transaction.CommitRetaining;
      finally
        taAssortEl.GotoBookmark(BookMark);
      end;
    end
  end
end;

procedure TfmWOrder.acAPasteExecute(Sender: TObject);
var
  DocNo : Variant;
  Q : integer;
begin
  if (dmMain.taWOrderDEPID.AsInteger <> dm.User.wWhId) then
    raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taWOrderDEPNAME.AsString]));
  with dmMain do
  begin
    if taWOrder.IsEmpty then eXit;
    if (dmMain.taWOrderAINV.IsNull)or(dmMain.taWOrderAINV.AsInteger = 0) then
    begin
      if MessageDialog('������� ����� ���������?', mtConfirmation, [mbOk, mbCancel], 0)<>mrOk then eXit;
      if taWOrderJOBDEPID.IsNull then
      begin
        taWOrder.Refresh;
        if taWorderJOBDEPID.IsNull then
          raise Exception.Create('�� ��������� ����� �������� � ������ ���������.  WOITEMID='+IntToStr(taWOrderWOITEMID.AsInteger));
      end;
      dmAppl.AInvId := -1;
      OpenDataSet(dmAppl.taAInv);
      dmAppl.taAInv.Append;
      taWOrder.Edit;
      taWOrderAINV.AsInteger := dmAppl.taAInvINVID.AsInteger;
      dmAppl.taAInv.Post;
      taWOrder.Post;
      CloseDataSet(dmAppl.taAInv);
    end
    else begin
      DocNo := ExecSelectSQL('select DOCNO from Inv where InvId='+taWOrderAINV.AsString, quTmp);
      if VarIsNull(DocNo)or(DocNo=0)then raise EInternal.Create(Format(rsInternalError, ['113']));
      if MessageDialog('�������� � ��������� �'+VarToStr(DocNo), mtConfirmation, [mbOk, mbCancel], 0)<>mrOk then eXit;
    end;
    // ���������� � ���������
    ExecSQL('execute procedure Paste_A('+IntToStr(dm.User.UserId)+', '+taWOrderWOITEMID.AsString+')', quTmp);
    dm.tr.CommitRetaining;
    ReOpenDataSets([taAssortInv, taAssortEl]);
    // �������� ���-�� � ������ ��������� ������
    Q := 0;
    while not taAssortEl.Eof do
    begin
      Q := Q + taAssortElQ.AsInteger;
      taAssortEl.Next;
    end;
    taWOrder.Edit;
    taWOrderQ.AsInteger := Q;
    taWOrder.Post;
  end;
end;

procedure TfmWOrder.acSCopyExecute(Sender: TObject);
var
  i : integer;
begin
  if(not dmMain.tawOrder.Active) then eXit;
  ExecSQL('delete from B_WInv where UserId='+IntToStr(dm.User.UserId), dmMain.quTmp);
  with dmMain do
    if(gridItem.SelectedRows.Count > 1)  then
      for i:=0 to Pred(gridItem.SelectedRows.Count) do
      begin
        taWOrder.GotoBookmark(Pointer(gridItem.SelectedRows.Items[i]));
        ExecSQL('insert into B_WInv(ID, USERID, DEPID, SEMIS, OPERID, Q, W) '+
           ' values('+IntToStr(dm.GetId(35))+','+IntToStr(dm.User.UserId)+','+
             taWOrderDEPID.AsString+','#39+taWOrderSEMIS.AsString+#39', '#39+
             taWOrderOPERID.AsString+#39','+taWOrderQ.AsString+', '#39+
             StringReplace(taWOrderW.AsString, ',', '.', [])+#39')', dmMain.quTmp);
      end
    else begin
      ExecSQL('insert into B_WInv(ID, USERID, DEPID, SEMIS, OPERID, Q, W) '+
           ' values('+IntToStr(dm.GetId(35))+','+IntToStr(dm.User.UserId)+','+
           taWOrderDEPID.AsString+', '#39+taWOrderSEMIS.AsString+#39', '#39+
           taWOrderOPERID.AsString+#39','+taWOrderQ.AsString+','#39+
           StringReplace(taWOrderW.AsString, ',', '.', [])+#39')', dmMain.quTmp);
      taWOrder.Transaction.CommitRetaining;
    end;
end;

procedure TfmWOrder.acPasteExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'OPERID'; sName: 'NAME'; sTable: 'D_OPER'; sWhere: '');
var
  ItType : integer;
  bNew : boolean;
  WhSemisName : string;
begin
  if not dmMain.taWOrder.Active then eXit;
  {if (dmMain.taWOrderDEPID.AsInteger <> dm.User.wWhId) then
    raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taWOrderDEPNAME.AsString]));}
  bNew := False;
  with dmMain do
  begin
    if(CurrInv <> -1)then
    begin
      ItType := ExecSelectSQL('select max(ItType) from woItem where InvId='+IntToStr(dmMain.CurrInv), dmMain.quTmp);
      if(ItType in [0, 6])then
        with (Sender as TComponent) do
          if(Tag = 1)then
            if MessageDialog('������� ����� ���������?', mtConfirmation, [mbOk, mbCancel], 0)<>mrOk then eXit
            else bNew := True
          else
            case MessageDialog('�������� � ������� ���������?', mtConfirmation, [mbNo, mbOk, mbCancel], 0) of
              mrCancel : eXit;
              mrOk : bNew := False;
              mrNo : bNew := True;
            end
      else if(ItType in [1, 3, 5, 7])then
      begin
        if(Tag = 0) then
          if MessageDialog('������� ����� ���������?', mtConfirmation, [mbOk, mbCancel], 0)<>mrOk then eXit
          else bNew := True
        else
          case MessageDialog('�������� � ������� ���������?', mtConfirmation, [mbNo, mbOk, mbCancel], 0) of
            mrCancel : eXit;
            mrOk : bNew := False;
            mrNo : bNew := True;
          end
      end
      else raise EInternal.Create(Format(rsInternalError, ['101']));
    end;
    taSBuffer.Open;
    while not taSBuffer.Eof do
    begin
      if bNew then
      begin
        acAddNew.Execute;
        bNew := False;
      end
      else acAddItem.Execute;

      taWOrderITTYPE.AsInteger := (Sender as TComponent).Tag;
      taWOrderSEMIS.AsString := taSBufferSEMIS.AsString;
      taWOrderQ.AsFloat := taSBufferQ.AsFloat;
      taWOrderW.AsFloat := taSBufferW.AsFloat;

      case (Sender as TComponent).Tag of
        0 : if(taSBufferGOOD.AsInteger = 0) then // ������������ �� �������� �������
              taWOrderFROMOPERID.AsVariant := Null
            else
              if ShowDictform(TfmOper, dm.dsrOper, Rec, '�������� � ����� �������� �������� ������������ '+taSBufferSEMISNAME.AsString, dmSelRecordReadOnly)<> mrOk then
              begin
                taWOrder.Cancel;
                taSBuffer.Delete;
                continue;
              end
              else taWOrderFROMOPERID.AsVariant := DictAncestor.vResult;

        1 : if(taSBufferGOOD.AsInteger = 0) then // ������������ �� �������� �������
              taWOrderFROMOPERID.AsVariant := Null
            else
              if ShowDictform(TfmOper, dm.dsrOper, Rec, '�������� � ����� �������� ����������� ������������ '+taSBufferSEMISNAME.AsString, dmSelRecordReadOnly) <> mrOk then
              begin
                taWOrder.Cancel;
                taSBuffer.Delete;
                continue;
              end
              else taWOrderFROMOPERID.AsVariant := DictAncestor.vResult;
      end;
      taWOrder.Post;
      taSBuffer.Delete;
    end;
    taSBuffer.Close;
    dm.tr.CommitRetaining;
  end;
  // �������� ��������� ������
  WhSemisName := dmMain.taWhSemisSEMISNAME.AsString;
  ReOpenDataSet(dmMain.taWhSemis);
  // ����������� ����� 
  dmMain.taWhSemis.Locate('SEMISNAME', WhSemisName, []);
end;

procedure TfmWOrder.acArtExecute(Sender: TObject);
var
  Position : integer;
  Id : Variant;
begin
  // ��������� ����� �������
  if (dm.User.AccProd and $20)<>$20 then raise Exception.Create('� ��� ��� ���� ������� � ������������!');
  if (dmMain.taWOrderDEPID.AsInteger <> dm.User.wWhId) then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taWOrderDEPNAME.AsString]));
  // ������ �� �����
  if(dmMain.taWOrderListISCLOSE.AsInteger =1)then raise Exception.Create(Format(rsWorderClose, [dmMain.taWOrderListDOCNO.AsString]));
  dmAppl.IsWorkWithStone := False;
  with dmMain do
  begin
    PostDataSet(taWOrder);
    stbrStatus.Panels[1].Text := '���-��: '+IntToStr(dmMain.taWOrderQ.AsInteger);

    case dmMain.WOrderMode of
      wmWorder : begin
        dmAppl.ADistrType := adtWOrder;
        Id := ExecSelectSQL('select InvId from Inv where InvId='+IntToStr(taWOrderAINV.AsInteger), dmMain.quTmp);
        (* ������� �� ���������? *)
        if VarisNull(Id)or(Id=0)then
        begin
          taWOrder.Edit;
          taWOrderAINV.AsVariant := Null;
          taWOrder.Post;
          taWOrder.Transaction.CommitRetaining;
        end;
        if taWOrderAINV.IsNull then
        begin
          if dmAppl.trAppl.Active then dmAppl.trAppl.Commit;
          dmAppl.trAppl.StartTransaction;
          OpenDataSets([dmAppl.taAInv]);
          dmAppl.taAInv.Append;
          taWOrder.Edit;
          taWOrderAINV.AsInteger := dmAppl.taAInvINVID.AsInteger;
          dmAppl.taAInv.Post;
          taWOrder.Post;
          dmAppl.taAInv.Transaction.CommitRetaining;
          taWOrder.Transaction.CommitRetaining;
        end;
        CloseDataSets([dmAppl.taAInv, dmAppl.taAEl]);
        if taWOrderITTYPE.AsInteger in [0,6] then dmAppl.ADistrKind := 0 else dmAppl.ADistrKind := 1;
        ShowAndFreeForm(TfmADistr, TForm(fmADistr));
        dm.WorkMode := 'wOrder';
        (* �������� ��������� ������� ��������� *)
        PostDataSet(taWOrder);
        if dmAppl.IsWorkWithStone then
        begin
          Position := taWOrderWOITEMID.AsInteger;
          ReOpenDataSet(taWOrder);
          taWOrder.Locate('WOITEMID', VarArrayOf([Position]), []);
        end
        else begin
          if not (taWOrderAPPLINV.IsNull and taWOrderAINV.IsNull) then ReOpenDataSets([taAssortInv, taAssortEl])
          else CloseDataSets([taAssortInv, taAssortEl]);
          RefreshDataSet(taWOrder);
        end;
        gridItem.SelectedField := taWOrderQ;
      end;

      wmRej : begin
        dmAppl.ADistrType := adtRej;
        dmAppl.ADistrKind := 0;
        (* ������� �� ���������? *)
        Id := ExecSelectSQL('select InvId from Inv where InvId='+IntToStr(taWOrderAINV.AsInteger), dmMain.quTmp);
        if VarIsNull(Id)or(Id=0)then
        begin
          taWOrder.Edit;
          taWOrderAINV.AsVariant := Null;
          taWOrder.Post;
          taWOrder.Transaction.CommitRetaining;
        end;

        if taWOrderAINV.IsNull then
        begin
           if dmAppl.trAppl.Active then dmAppl.trAppl.Commit;
           dmAppl.trAppl.StartTransaction;
           OpenDataSet(dmAppl.taRejInv);
           dmAppl.taRejInv.Append;
           dmMain.taWOrder.Edit;
           dmMain.taWOrderAINV.AsInteger := dmAppl.taRejInvINVID.AsInteger;
           PostDataSets([dmAppl.taRejInv, dmMain.taWOrder]);
        end;
        CloseDataSets([dmAppl.taRejInv, dmAppl.taRejEl]);
        ShowAndFreeForm(TfmADistr, TForm(fmADistr));
        dm.WorkMode := 'wOrder';
        if not (taWOrderAPPLINV.IsNull and taWOrderAINV.IsNull) then ReOpenDataSets([taAssortInv, taAssortEl])
        else CloseDataSets([taAssortInv, taAssortEl]);
        RefreshDataSet(taWOrder);
        gridItem.SelectedField := taWOrderQ;
      end;
    end;
  end;
end;

procedure TfmWOrder.acTransform0Execute(Sender: TObject);
begin
  FUseLog := True;
  if Extended then // ���������������� ������������ � �������� � ����������� ������
    case ItType of
      0 : e_Transform0;
      5 : e_Transform5;
    end
  else raise EWarning.Create(rsNotImplementation);
end;

function TfmWOrder.FillTransformInfo : boolean;
const
  Rec : TRecName = (sId: 'SEMISID'; sName: 'NAME'; sTable: 'D_SEMIS'; sWhere: '');
var
  EndSemisGood : byte;
begin
  Result := False;
  if(dm.User.Transform_DepId = -2)then
    if FUseLog then
    begin
      fmLog.Add(Format(rsSelectPS, ['�������������']));
      eXit;
    end
    else raise EWarning.Create(Format(rsSelectPS, ['�������������']));
  if(dm.User.Transform_MolId = -2)then
    if FUseLog then
    begin
      fmLog.Add(Format(rsInternalError, ['111']));
      eXit;
    end
    else raise EInternal.Create(Format(rsInternalError, ['111']));
  // ��������� ��������� �� ������������ �� ��������
  if(dm.User.wWhId = -1)then
  begin
    if FUseLog then
    begin
      fmLog.Add(Format(rsIsNotMOL, [dm.User.FIO]));
      eXit;
    end
    else raise EInternal.Create(Format(rsInternalError, ['111']));
  end;
  FillChar(FTransformInfo, SizeOf(TTransformInfo), #0);
  with FTransformInfo do
  begin
    JobId := dm.User.Transform_MolId;
    // � ����� �������� ����������� ������ � �������
    if(dmMain.taWhSemisGOOD.AsInteger = 0) then // ������������ �� �������� �������
    begin
      if(dmMain.taWhSemisOPERID.AsString = dm.RejOperId)or
        (dmMain.taWhSemisOPERID.AsString = dm.NkOperId)or
        (dmMain.taWhSemisOPERID.AsString = dm.TransformOperId)
      then begin
        FromOperId := #39+dmMain.taWhSemisOPERID.AsString+#39;
        FromOperName := dmMain.taWhSemisOPERNAME.AsString;
      end
      else begin
        FromOperId := 'Null';
        FromOperName := sNoOperation;
      end;
    end
    else begin
      if dmMain.taWhSemisOPERID.IsNull then FromOperId := 'Null'
      else FromOperId := #39+dmMain.taWhSemisOPERID.AsString+#39;
      FromOperName := dmMain.taWhSemisOPERID.AsVariant;
    end;

    // ����� �����
    wOrderId := ExecSelectSQL('select max(wOrderId) from wOrder where DefOper='#39+
            dm.TransformOperId+#39' and JobDepId='+IntToStr(dm.User.Transform_DepId)+' and IsClose=0', dmMain.quTmp);
    // ��������� ���� �� ������������ � ������� ����� ������������� ���������
    dmMain.taSemisT1.Open;
    dmMain.taSemisT1.Last;
    case dmMain.taSemisT1.RecordCount of
       0 : if FUseLog then
           begin
             fmLog.Add(Format(rsInvalidTransform1, [dmMain.taWhSemisSEMISNAME.AsString]));
             eXit;
           end
           else raise EWarning.Create(Format(rsInvalidTransform1, [dmMain.taWhSemisSEMISNAME.AsString]));
       1 : EndSemis := dmMain.taSemisT1SEMISID.AsString;
       else if ShowDictForm(TfmSemisT1, dmMain.dsrSemisT1, Rec, '������������� -> ������',
               dmSelRecordReadOnly)=mrOk then EndSemis := DictAncestor.vResult
         else begin
           if FUseLog then fmLog.Add('�� ������ �������� ������������. ������������� ��������.');
           eXit;
         end;
    end;
    EndSemisName := ExecSelectSQL('select Semis from D_Semis where SemisId='#39+EndSemis+#39, dmMain.quTmp);
    if FUseLog then fmLog.Add('������ �������� ������������: '+EndSemisName);
    EndSemisGood := ExecSelectSQL('select Good from D_Semis where SemisId='#39+EndSemis+#39, dmMain.quTmp);
    // � ����� �������� ����������� �������
    if(EndSemisGood = 0)then RetFromOperId := #39+dm.TransformOperId+#39
    else RetFromOperId := FromOperId;

    if(VarIsNull(wOrderId)or(wOrderId=0))then
    begin
      if FUseLog then fmLog.Add('������� ����� �����...');
      with dmMain do
      begin // ������� �����
        wOrderId := dm.GetId(2);
        ExecSQL('insert into wOrder(WORDERID, DOCNO, DOCDATE, DEPID, MOLID, '+
           ' JOBDEPID, JOBFACE, DEFOPER, ISCLOSE) values('+
           IntToStr(wOrderId)+','+IntToStr(dm.GetId(7))+','#39+DateToStr(ToDay)+#39','+
           IntToStr(dm.User.wWhId)+','+IntToStr(dm.User.UserId)+','+
           IntToStr(dm.User.Transform_DepId)+','+IntToStr(dm.User.Transform_MolId)+','#39+
           dm.TransformOperId+#39', 0)', quTmp);
      end;
      wOrderNo := ExecSelectSQL('select DocNo from wOrder where wOrderId='+IntToStr(wOrderId), dmMain.quTmp);
      if FUseLog then fmLog.Add('������ ����� �'+wOrderNo);
    end
    else begin
      wOrderNo := ExecSelectSQL('select DocNo from wOrder where wOrderId='+IntToStr(wOrderId), dmMain.quTmp);
      if FUseLog then fmLog.Add('������ ����� �'+wOrderNo);
    end;
  end;
  Result := True;
end;

procedure TfmWOrder.acAddSemisNewExecute(Sender: TObject);

const
  Rec : TRecName = (sId: 'SEMISID'; sName: 'NAME'; sTable: 'D_SEMIS'; sWhere: '');
  RecRej : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_REJ'; sWhere: '');

var
  RejId, RejPsId : Variant;

  Storage: TStorage;
  SourceStorage: TStorage;
  TargetStorage: TStorage;
  SpoilageDelta: TSpoilageDelta;
  New: Boolean;

  TotalResultQuantity: Integer;
  TotalResultWeight: Double;
  ResultQuantity: Integer;
  ResultWeight: Double;
  SourceWoItemID: Integer;
  TargetWoItemID: Integer;


procedure Go(New: Boolean = True);
begin
  with dmMain do
  begin
    (* C������ ����� ��������� *)
    if New then  acAddNew.Execute
    else acAddItem.Execute;

    taWOrderSEMIS.AsString := taWhSemisSEMISID.AsString;

    if(taWhSemisGOOD.AsInteger=0) then
      case ItType of
        0, 1, 3, 5, 6, 7 : taWOrderFROMOPERID.AsVariant := Null;
        else raise EInternal.Create(Format(rsInternalError, ['101']));
      end
    else
      case ItType of
        0, 1, 3, 5, 6, 7 : taWOrderFROMOPERID.AsVariant := taWhSemisOPERID.AsVariant;
        else raise EInternal.Create(Format(rsInternalError, ['101']));
      end;

    taWOrderQ.Asinteger := SemisQ;
    taWOrderW.AsFloat := SemisW;
    taWOrderREJID.AsVariant := RejId;
    taWOrderREJPSID.AsVariant := RejPsId;
    taWOrder.Post;
    taWhSemis.Refresh;
  end;
end;

begin
  if not TryInsert then Exit;


  if dmMain.taWhSemis.IsEmpty then
  raise Exception.Create(Format(rsNoData, ['��� �����������']));

  RejId := Null;
  RejPsId := Null;

  with dmMain do
  begin

    // ������ ����� � �������� �� ����� ��������

    if (ItType = 0) and (dmMain.taWhSemisOPERID.AsString = dm.RejOperId) then
    begin
      if DialogSemis <> nil then FreeAndNil(DialogSemis);
      try
        DialogSemis := TDialogSemis.Create(Self);

        SourceStorage := TStorage.Create;
        SourceStorage.ID := dmMain.taWhSemisDEPID.AsInteger;
        SourceStorage.Element.Code := Trim(dmMain.taWhSemisSEMISID.AsString);
        SourceStorage.Element.Operation := Trim(dmMain.taWhSemisOPERID.AsString);
        SourceStorage.Element.UnitQuantity := dmMain.taWhSemisUQ.AsInteger;
        SourceStorage.Element.UnitWeight := dmMain.taWhSemisUW.AsInteger;

        TargetStorage := TStorage.Create;
        TargetStorage.ID := taWOrderListJOBDEPID.AsInteger;
        TargetStorage.Element.Code := Trim(dmMain.taWhSemisSEMISID.AsString);
        TargetStorage.Element.Operation := Trim(taWOrderListDEFOPER.AsString);
        TargetStorage.Element.UnitQuantity := dmMain.taWhSemisUQ.AsInteger;
        TargetStorage.Element.UnitWeight := dmMain.taWhSemisUW.AsInteger;

        New := True;

        if DialogSemis.Execute(SourceStorage, TargetStorage, smOut) then
        begin
          DialogSemis.DataSetTargetSummary.First;
          while not DialogSemis.DataSetTargetSummary.Eof do
          begin

            RejId := DialogSemis.DataSetTargetSummaryReasonID.AsInteger;
            RejPsId := DialogSemis.DataSetTargetSummaryOriginatorID.AsInteger;

            dmMain.SemisQ := DialogSemis.DataSetTargetSummaryQuantity.AsInteger;
            dmMain.SemisW := DialogSemis.DataSetTargetSummaryWeight.AsFloat;

            Go(New);
            New := False;

            DialogSemis.DataSetTarget.SetRange([RejPsId, RejId], [RejPsId, RejId]);
            DialogSemis.DataSetTarget.First;

            while not DialogSemis.DataSetTarget.Eof do
            begin
              SpoilageDelta.StorageElementID := TargetStorage.Element.ID;
              SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
              SpoilageDelta.OperationID := DialogSemis.DataSetTargetOperationID.AsInteger;
              SpoilageDelta.Quantity := DialogSemis.DataSetTargetQuantity.AsInteger;
              SpoilageDelta.Weight := DialogSemis.DataSetTargetWeight.AsFloat;

              TDialogSemis.Delta(SpoilageDelta);

              SpoilageDelta.StorageElementID := SourceStorage.Element.ID;
              SpoilageDelta.InvoiceElementID := dmMain.taWOrderWOITEMID.AsInteger;
              SpoilageDelta.OperationID := DialogSemis.DataSetTargetOperationID.AsInteger;
              SpoilageDelta.Quantity := - DialogSemis.DataSetTargetQuantity.AsInteger;
              SpoilageDelta.Weight := - DialogSemis.DataSetTargetWeight.AsFloat;

              TDialogSemis.Delta(SpoilageDelta);

              DialogSemis.DataSetTarget.Next;
           end;
           DialogSemis.DataSetTargetSummary.Next;
          end;
       end;
       finally
         if SourceStorage <> nil then FreeAndNil(SourceStorage);
         if TargetStorage <> nil then FreeAndNil(TargetStorage);
         if DialogSemis <> nil then FreeAndNil(DialogSemis);
       end;
    end
    else
    if (ItType = 3) and (dmMain.taWhSemisOPERID.AsString = dm.RejOperId) then
    begin
      if dmMain.taWOrderREJID.AsInteger = 0 then
      begin
        raise EWarning.Create('���������� ������� ����������� �����.');
        Exit;
      end;
      SourceWoItemID := dmMain.taWOrderWOITEMID.AsInteger;

      SourceStorage := TStorage.Create;
      SourceStorage.ID := dm.User.wWhId;
      SourceStorage.Connected := True;
      SourceStorage.Element.Code := dmMain.taWOrderSEMIS.AsString;
      SourceStorage.Element.Operation := dm.RejOperId;
      SourceStorage.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
      SourceStorage.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
      SourceStorage.Element.Connected := True;

      TargetStorage := TStorage.Create;
      TargetStorage.ID := dmMain.taWOrderJOBDEPID.AsInteger;
      TargetStorage.Element.Code := dmMain.taWOrderSEMIS.AsString;
      TargetStorage.Element.Operation := dmMain.taWOrderOPERID.AsString;
      TargetStorage.Element.UnitQuantity := dmMain.taWOrderUQ.AsInteger;
      TargetStorage.Element.UnitWeight := dmMain.taWOrderUW.AsInteger;
      TargetStorage.Element.Connected := True;

      if DialogSemis <> nil then FreeAndNil(DialogSemis);
      DialogSemis := TDialogSemis.Create(Self);
      if DialogSemis.Execute(SourceStorage, TargetStorage, smIn, dmMain.taWOrderWOITEMID.AsInteger) then
      begin
        TotalResultQuantity := 0;
        TotalResultWeight := 0;
        DialogSemis.DataSetSource.First;
        while not DialogSemis.DataSetSource.Eof do
        begin
          ResultQuantity := DialogSemis.DataSetSourceQuantity.AsInteger;
          ResultWeight := DialogSemis.DataSetSourceWeight.AsFloat;
          TotalResultQuantity := TotalResultQuantity + ResultQuantity;
          TotalResultWeight := TotalResultWeight + ResultWeight;
          DialogSemis.DataSetSource.Next;
        end;
        dmMain.SemisQ := TotalResultQuantity;
        dmMain.SemisW := TotalResultWeight;
        Go;

        TargetWoItemID := dmMain.taWOrderWOITEMID.AsInteger;
        ExecSQL('update woitem set REF$WOITEMID = ' + IntToStr(SourceWoItemID) + ' where woitemid = ' + IntToStr(TargetWoItemID), dmMain.quTmp);

        DialogSemis.DataSetSource.First;
        while not DialogSemis.DataSetSource.Eof do
        begin
          ResultQuantity := DialogSemis.DataSetSourceQuantity.AsInteger;
          ResultWeight := DialogSemis.DataSetSourceWeight.AsFloat;

          SpoilageDelta.StorageElementID := SourceStorage.Element.ID;
          SpoilageDelta.InvoiceElementID := TargetWoItemID;
          SpoilageDelta.OperationID := DialogSemis.DataSetSourceOperationID.AsInteger;
          SpoilageDelta.Quantity := ResultQuantity;
          SpoilageDelta.Weight := ResultWeight;

          TDialogSemis.Delta(SpoilageDelta);

          SpoilageDelta.StorageElementID := TargetStorage.Element.ID;
          SpoilageDelta.InvoiceElementID := SourceWoItemID;
          SpoilageDelta.OperationID := DialogSemis.DataSetSourceOperationID.AsInteger;
          SpoilageDelta.Quantity := -ResultQuantity;
          SpoilageDelta.Weight := -ResultWeight;
          TDialogSemis.Delta(SpoilageDelta);

          DialogSemis.DataSetSource.Next;
        end;
     end;
     SourceStorage.Free;
     TargetStorage.Free;
     FreeAndNil(DialogSemis);
     Exit;
    end
    else
    // �������� ����� � ����� �������� � ��������
    if (ItType = 5) then
    begin
      if not _ShowEditorQW(dmMain.taWhSemisQ.AsInteger, dmMain.taWhSemisW.AsFloat, chbxW.Checked) then eXit;

      Go;

      TargetStorage := TStorage.Create;
      TargetStorage.ID := dm.User.wWhId;
      TargetStorage.Element.Code := Trim(dmMain.taWhSemisSEMISID.AsString);
      TargetStorage.Element.Operation := dm.RejOperId;
      TargetStorage.Element.UnitQuantity := dmMain.taWhSemisUQ.AsInteger;
      TargetStorage.Element.UnitWeight := dmMain.taWhSemisUW.AsInteger;
      TargetStorage.Connected := True;
      TargetStorage.Element.Connected := True;

      SpoilageDelta.StorageElementID := TargetStorage.Element.ID;
      SpoilageDelta.InvoiceElementID := dmmain.taWOrderWOITEMID.AsInteger;
      SpoilageDelta.OperationID := ExecSelectSQL('select ID from d_oper where operid = ' + #39 + taWOrderListDEFOPER.AsString+ #39, dmMain.quTmp, False);
      SpoilageDelta.Quantity := dmMain.SemisQ;
      SpoilageDelta.Weight := dmMain.SemisW;

      TargetStorage.Free;
      TDialogSemis.Delta(SpoilageDelta);
     end
    else
    begin
      if not _ShowEditorQW(dmMain.taWhSemisQ.AsInteger, dmMain.taWhSemisW.AsFloat, chbxW.Checked) then eXit;
      Go;
    end;
  end;
end;

procedure TfmWOrder.gridItemColumns7EditButtonClick(Sender: TObject; var Handled: Boolean);
begin
  acArt.Execute;
end;

procedure TfmWOrder.gridItemExit(Sender: TObject);
begin
  PostDataSet(gridItem.DataSource.DataSet);
end;

procedure TfmWOrder.gridItemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_INSERT : SysUtils.Abort;
  end;
end;


procedure TfmWOrder.gridItemGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
var
  i : integer;
begin
  case dmMain.taWOrderITTYPE.AsInteger of
    0,6 : if (dmMain.taWOrderE.AsInteger = -1) then
            BackGround := clRed
          else
            BackGround := clInfoBk;
    1,3,4,5,7 : if (dmMain.taWOrderE.AsInteger = -1) then
                  BackGround := clGreen
                else
                  Background := cl3DLight;
  end;

  if(not dmMain.taWOrderREF.IsNull) then
    Background := clMoneygreen;

  if(AnsiUpperCase(Column.FieldName)='RECNO')then
    if gridItem.SelectedRows.Find(Column.Field.DataSet.Bookmark, i) then
      Background:= clCream
    else
      Background:= clMoneygreen
  else if(Column.FieldName='ITDATE')and(Column.Field.AsDateTime>dm.EndDate) then
         BackGround := clRed
  else if(Column.FieldName='ITTYPE')then
    case dmMain.taWOrderITTYPE.AsInteger of
      0,6 : BackGround := clInfoBk;
      1,3,4,5,7 : Background := cl3DLight;
    end;
  if(Column.FieldName='DOCDATE')then
  begin
    if(Column.Field.AsDateTime <> dmMain.taWOrderITDATE.AsDateTime) then
      Background:=clRed;
  end
end;

procedure TfmWOrder.acPrintGridExecute(Sender: TObject);
begin

  printItem.BeforeGridText.Text := '�����: '+dmMain.taWOrderListDOCNO.AsString+' �� '+
  dmMain.taWOrderListDOCDATE.AsString+#13#10+' ��������: '+dmMain.taWOrderListDEFOPERNAME.AsString;

  Printer.Orientation := poLandscape;

  printItem.Print;
end;

procedure TfmWOrder.Access;
var
  IsClose: Boolean;
  materialMatches: Boolean;
  WOrderMatID: integer;
begin

  IsClose :=  dmMain.taWOrderListISCLOSE.AsInteger = 1;

  WOrderMatID := ExecSelectSQL('select MO from D_DEP where DEPID = ' + dmMain.taWOrderListDEPID.AsString, dm.quTmp);

  materialMatches := WOrderMatID = dm.User.wWhMatId;

  if (IsClose or (not materialMatches)) then
  begin
    SetReadOnly(True);
    stbrStatus.Panels[0].Text := '��������';
  end
  else
  begin
    with stbrStatus.Panels[0] do
    if dm.IsAdm then Text := '�������������'
    else
    begin
      if ((dm.User.AccProd_d and 1) = 1)then Text := '��������'
      else Text := '������';
    end;

    if dm.IsAdm then eXit;

    if (dm.User.AccProd_d and 1) = 1 then SetReadOnly(True)
    else
    begin
      if(dm.User.wWhId = -1)then
      begin
        ShowMessage('�� �� ���������� �� �� ����� �� ��������.'+#13#10+'�������� ����� ���������');
        SetReadOnly(True);
      end
      else SetReadOnly(False);
    end;
  end;
end;

procedure TfmWOrder.SetReadOnly(ReadOnly: boolean);
begin
  acAddSemisNew.Enabled := not ReadOnly;
  acAddSemis.Enabled := not ReadOnly;
  acDelSemis.Enabled := not ReadOnly;
  acAddItem.Enabled := not ReadOnly;
  acAddNew.Enabled := not ReadOnly;
  acSCopy.Enabled := not ReadOnly;
  acSPaste0.Enabled := not ReadOnly;
  acTransform0.Visible := not ReadOnly;
  acSPaste1.Enabled := not ReadOnly;
  acACopy.Enabled := not ReadOnly;
  acAPaste.Visible := not ReadOnly;
  gridItem.ReadOnly :=  ReadOnly;
  gridAEl.ReadOnly := ReadOnly;
  edNoDoc.Enabled := not ReadOnly;
  dtedDateDoc.Enabled := not ReadOnly;
  lcbxJobPS.Enabled := not ReadOnly;
  lcbxExec.Enabled := not ReadOnly;
  lcbxOper.Enabled := not ReadOnly;
  plExt.Visible := not ReadOnly;
  tbdcSemis.Visible := not ReadOnly;
  tlbrT.Visible := not ReadOnly;
  if (dm.User.AccProd_d and 1) = 1 then spitClose.Enabled := Not ReadOnly;
end;

procedure TfmWOrder.e_Transform0;
const
  Rec : TRecName = (sId: 'SEMISID'; sName: 'NAME'; sTable: 'D_SEMIS'; sWhere: '');
const
  t_1 : set of byte = [0, 6];
  t_2 : set of byte = [1, 3, 5];
  t_3 : set of byte = [7];
//var
//  EndSemisGood : byte;
var
  t : byte;
  Ind : integer;
begin
  fmLog.Clear;
  fmLog.Show;
  if dmMain.taWhSemis.IsEmpty then raise Exception.Create(Format(rsNoData,['��� �������������']));
  //if not FillTransformInfo() then eXit;
  with dmMain, FTransformInfo do
  begin
    SemisQ := dmMain.taWhSemisQ.AsInteger;
    SemisW := dmMain.taWhSemisW.AsFloat;
    if(ShowEditor(TfmeSQ, TfmEditor(fmeSQ))<>mrOk) then eXit;
    // ������ �� ����� �������� �������������
    fmLog.Add('������ �� ����� �������� �������������');
    ExecSQL('execute procedure woItem_I('+IntToStr(dm.GetId(3))+','+IntToStr(wOrderId)+', "'+dm.TransformOperId+'", "'+
       taWhSemisSEMISID.AsString+'", "'+DateToStr(ToDay)+'", '+
       IntToStr(dmMain.SemisQ)+','+
       StringReplace(FloatToStr(dmMain.SemisW), ',', '.', [])+', '+
       '0, Null, '+IntToStr(dm.User.UserId)+' ,'+
       '0, '+IntToStr(dm.GetId(9))+',"'+DateToStr(ToDay)+'", '+IntToStr(dm.User.wWhId)+','+
       IntToStr(dm.User.Transform_DepId)+','+IntToStr(dm.User.Transform_MolId)+', Null, Null, Null, Null, '+
       FromOperId+')', quTmp);
    RefreshDataSet(taWhSemis);
    // ������� �� ����� ������������
    if FUseLog then fmLog.Add('������� �� ����� ������������');
    ExecSQL('execute procedure woItem_I('+IntToStr(dm.GetId(3))+','+IntToStr(wOrderId)+', "'+dm.TransformOperId+'", "'+
        EndSemis+'", "'+DateToStr(ToDay)+'", 0,'+
        StringReplace(FloatToStr(dmMain.SemisW), ',', '.', [])+', '+
        '3, Null, '+IntToStr(dm.User.UserId) + ', ' +
        '0, '+IntToStr(dm.GetId(9))+',"'+DateToStr(ToDay)+'", '+IntToStr(dm.User.wWhId)+','+
        IntToStr(dm.User.Transform_DepId)+','+IntToStr(dm.User.Transform_MolId)+', Null, Null, Null, Null, '+
        RetFromOperId+')', quTmp);
    if FUseLog then
    begin
      fmLog.Add('����������� ��������� ������������� '+dmMain.taWhSemisSEMISNAME.AsString+' � '+
         EndSemisName+'. ����� �'+wOrderNo);
      fmLog.Add('�������� '+EndSemisName+' � ���������');
    end;
    Ind := cmbxOper.Items.IndexOf(dm.TransformOperName);
    if(Ind = -1) then raise EInternal.Create(Format(rsInternalError, ['112']));
    cmbxOper.ItemIndex := Ind;
    // ���� �� ����������� ����� ������
    // ������ ������������ ������ ���� �� �������� �������������
    if not taWhSemis.Locate('SEMISID', EndSemis, []) then
    begin
      ReOpenDataSet(taWhSemis);
      if not taWhSemis.Locate('SEMISID', EndSemis, []) then
        raise EInternal.Create(Format(rsInternalError, ['113']));
    end;

    if ShowEditor(TfmeInvCreateMode, TfmEditor(fmeInvCreateMode)) <> mrOk then eXit;
    case Editor.vResult of
      0 : begin
        (* C������ ����� ��������� *)
        acAddNew.Execute;
        taWOrderSEMIS.AsString := EndSemis;
        taWOrderFROMOPERID.AsVariant := taWhSemisOPERID.AsVariant;
        taWOrderQ.Asinteger := SemisQ;
        taWOrderW.AsFloat := SemisW;
        taWOrder.Post;
        taWhSemis.Refresh;
      end;
      1 : begin
        (* ���� ��������� ��������� �� ��������� �� ���� � ����� ������ �� ������
           ����� ��������� ����� ���������, ����� ��������� � ������ *)
        if(CurrInv = -1)then t := ItType
        else t := ExecSelectSQL('select max(ItType) from woItem where InvId='+
                  IntToStr(dmMain.CurrInv), dmMain.quTmp);
        case t of
          0, 6 : if(ItType in [0, 6])then acAddItem.Execute else acAddNew.Execute;
          1, 3, 5 : if(ItType in [1, 3, 5])then acAddItem.Execute else acAddNew.Execute;
          7 : if(ItType in [7])then acAddItem.Execute else acAddNew.Execute;
          else raise EInternal.Create(Format(rsInternalError, ['101']));
        end;
        taWOrderSEMIS.AsString := EndSemis;
        taWOrderFROMOPERID.AsVariant := taWhSemisOPERID.AsVariant;
        taWOrderQ.Asinteger := SemisQ;
        taWOrderW.AsFloat := SemisW;
        taWOrder.Post;
        taWhSemis.Refresh;
      end;
    end;
  end;
end;

procedure TfmWOrder.e_Transform5;
//var
//  r : word;
begin
  // ���������� �������� ��� �������������
  if not FillTransformInfo then eXit;
  if ShowEditor(TfmeInvCreateMode, TfmEditor(fmeInvCreateMode)) <> mrOk then eXit;
  case Editor.vResult of
    0 : acAddSemisNew.Execute;
    1 : acAddSemis.Execute;
  end;
  fmLog.Clear;
  if FUseLog then fmLog.Add('��������(�) '+dmMain.taWhSemisSEMISNAME.AsString+' � ��������� �'+dmMain.taWOrderDOCNO.AsString);
  fmLog.Show;
  if FUseLog then fmLog.Add('****������ �������������****');
  // �������� ������������� ��� �����
  with dmMain, FTransformInfo do
  begin
    if FUseLog then
      fmLog.Add('������: '+taWOrderSEMISNAME.AsString+' '+taWOrderQ.AsString+', '+taWOrderW.AsString);
    // ������ �� ����� �������� �������������
    ExecSQL('execute procedure woItem_I('+IntToStr(dm.GetId(3))+','+IntToStr(wOrderId)+', "'+dm.TransformOperId+'", "'+
        taWOrderSEMIS.AsString+'", "'+DateToStr(ToDay)+'", '+
        StringReplace(taWOrderQ.AsString, ',', '.', [])+','+
        StringReplace(taWOrderW.AsString, ',', '.', [])+', 0, Null, '+IntToStr(dm.User.UserId)+' ,'+
        '0, '+IntToStr(dm.GetId(9))+',"'+DateToStr(ToDay)+'", '+IntToStr(dm.User.wWhId)+','+
        IntToStr(dm.User.Transform_DepId)+','+IntToStr(dm.User.Transform_MolId)+', Null, Null, Null, Null, '+
        FromOperId+')', quTmp);
    if FUseLog then
      fmLog.Add('�������: '+EndSemisName+' 0, '+taWOrderW.AsString);
    // ������� �� ����� ������������
    ExecSQL('execute procedure woItem_I('+IntToStr(dm.GetId(3))+','+IntToStr(wOrderId)+', "'+dm.TransformOperId+'", "'+
        EndSemis+'", "'+DateToStr(ToDay)+'", 0, '+
        StringReplace(taWOrderW.AsString, ',', '.', [])+', 3, Null, '+IntToStr(dm.User.UserId)+', '+
        '0, '+IntToStr(dm.GetId(9))+',"'+DateToStr(ToDay)+'", '+IntToStr(dm.User.wWhId)+','+
        IntToStr(dm.User.Transform_DepId)+','+IntToStr(dm.User.Transform_MolId)+', Null, Null, Null, Null, '+
        RetFromOperId+')', quTmp);
    if FUseLog then fmLog.Add('****����� �������������****');
  end;
end;

procedure TfmWOrder.acEExecute(Sender: TObject);
begin
  if not (dmMain.taWOrder.State in [dsInsert, dsEdit]) then
    dmMain.taWOrder.Edit;
  dmMain.taWOrderE.AsInteger := not dmMain.taWOrderE.AsInteger;
  dmMain.taWOrder.Post;
end;

procedure TfmWOrder.acExcelExecute(Sender: TObject);
var
  f : TFileName;
  c : string;
begin
  if dmMain.taWOrder.IsEmpty then eXit;
  c := '����� � '+dmMain.taWOrderDOCNO.AsString+' '+dmMain.taWOrderListJOBFACE.AsString;
  f := GetSpecialPath(CSIDL_PERSONAL)+'\'+c+'.xls';
  GridToXLS(gridItem, f, c, True, True);
end;

function TfmWOrder.GetSpecialPath(CSIDL: Word):string;
var s:  string;
begin
  SetLength(s, MAX_PATH);
  if not SHGetSpecialFolderPath(0, PChar(s), CSIDL, true)
  then s := 'C:\';
  result := PChar(s);
end;

procedure TfmWOrder.acMultiSelectExecute(Sender: TObject);
begin
  if(gridItem.AllowedSelections = [])then
    gridItem.AllowedSelections := gridItem.AllowedSelections + [gstRecordBookmarks]
  else begin
    gridItem.Selection.Clear;
    gridItem.AllowedSelections := [];
  end;
end;

procedure TfmWOrder.SetOPeriod;
var
  dt : Variant;
begin
  dm.PsBeginDate := StrToDate('01.01.2050');
  if(not dmMain.taWOrderListJOBDEPID.IsNull) then
  begin
    // ���������� ���� ��������� ��� ����� ��������
    dt := ExecSelectSQL('select max(a.DocDate) from Act a, AItem it '+
                        'where a.Id=it.ActId and '+
                        '      a.T in (0,1) and '+
                        '      a.IsClose=1 and '+
                        '      it.PsId = '+dmMain.taWOrderListJOBDEPID.AsString, dmMain.quTmp);
    if VarIsNull(dt)then dm.PsBeginDate := StrToDate(BeginDateByDefault) else dm.PsBeginDate := VarToDateTime(dt);
  end;

  if(dm.User.wWhId <> -1) then
  begin
    dt := ExecSelectSQL('select max(a.DocDate) from Act a, AItem it '+
                        'where a.Id=it.ActId and '+
                        '      a.T = 2 and '+
                        '      a.IsClose=1 and '+
                        '      a.DepId = '+IntToStr(dm.User.wWhId), dmMain.quTmp);
    if not VarIsNull(dt)then
    begin
      if dm.PsBeginDate < VarToDateTime(dt) then
      begin
        dm.PsBeginDate := VarToDateTime(dt);
        dm.PsTypeBeginDate := 1;
      end
      else dm.PsTypeBeginDate := 0;
    end
    else dm.PsTypeBeginDate := 0;
  end;
  lbDate.Caption := '������: '+DateToStr(dm.PsBeginDate);
end;

procedure TfmWOrder.acSPaste0Update(Sender: TObject);
begin
  (Sender as TAction).Enabled := ExecSelectSQL('select count(*) from B_WInv where UserId='+IntToStr(dm.User.UserId), dmMain.quTmp)<>0;;
end;

procedure TfmWOrder.acSPaste1Update(Sender: TObject);
begin
  acSPaste1.Enabled := acSPaste0.Enabled;
end;

procedure TfmWOrder.acShowIDExecute(Sender: TObject);
begin
  gridItem.FieldColumns['INVID'].Visible    := not gridItem.FieldColumns['INVID'].Visible;
  gridItem.FieldColumns['WOITEMID'].Visible := not gridItem.FieldColumns['WOITEMID'].Visible;
  gridItem.FieldColumns['WORDERID'].Visible := not gridItem.FieldColumns['WORDERID'].Visible;
  gridItem.FieldColumns['ITDATE'].Visible :=  not gridItem.FieldColumns['ITDATE'].Visible;
  gridItem.FieldColumns['REJREF'].Visible := not gridItem.FieldColumns['REJREF'].Visible;
 // gridItem.FieldColumns['REJ'].Visible := not gridItem.FieldColumns['REJ'].Visible;

  gridSemis.FieldColumns['GOOD'].Visible := not gridSemis.FieldColumns['GOOD'].Visible;
  gridSemis.FieldColumns['SEMISID'].Visible := not gridSemis.FieldColumns['SEMISID'].Visible;
  gridSemis.FieldColumns['OPERID'].Visible := not gridSemis.FieldColumns['OPERID'].Visible;
end;

procedure TfmWOrder.SetMode;
var
  i : integer;
begin
  case dmMain.WorderMode of
    wmRej : begin
      ac0.Visible := False;
      ac1.Visible := False;
      ac3.Visible := False;
      ac5.Visible := False;
      ac6.Visible := False;
      ac7.Visible := False;
      acAddSemisNew.Visible := False;
      acAddSemis.Visible := True;
      acAddSemis.Caption := '��������';
      gridItem.FieldColumns['REJPSNAME'].Visible := True;
      acDelSemis.Visible := True;
      acAddItem.Enabled := False;
      acAddNew.Enabled := False;
      lcbxJobPS.Visible := False;
      lcbxExec.Visible := False;
      lcbxOper.Visible := False;
      cmbxMat.Enabled := False;
      with cmbxOper, Items do
        for i:=0 to Pred(Count) do
          if(Items[i] <> sNoOperation)and
            (TNodeData(Objects[i]).Code = dm.RejOperId)
          then begin
            ItemIndex := i;
            break;
          end;
      cmbxOper.Enabled := False;
      gridItem.ReadOnly := True;
    end;
    wmWorder : begin
    end;
  end;
  acTransform0.Visible := False;
end;

procedure TfmWOrder.ac0Execute(Sender: TObject);
begin
  ItType := 0;
end;

procedure TfmWOrder.ac6Execute(Sender: TObject);
begin
  ItType := 6;
end;

procedure TfmWOrder.ac1Execute(Sender: TObject);
begin
  ItType := 1;
end;

procedure TfmWOrder.ac3Execute(Sender: TObject);
begin
  ItType := 3;
end;

procedure TfmWOrder.ac5Execute(Sender: TObject);
begin
  ItType := 5;
end;

procedure TfmWOrder.ac7Execute(Sender: TObject);
begin
  ItType := 7;
end;

procedure TfmWOrder.acArtUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := not((dmMain.WorderMode = wmRej)and
                                     (dmMain.taWOrderITTYPE.AsInteger <> 0));
end;

procedure TfmWOrder.FilterPs(DataSet: TDataSet; var Accept: boolean);
begin
  Accept := (dm.taDepartDEPTYPES.AsInteger and $1 = $1) and
            (dm.taDepartDEPTYPES.AsInteger and $10 <> $10);
end;

function TfmWOrder.IsActionMayBe: boolean;
begin
  Result := not(dmMain.taWOrderITDATE.AsDateTime <= dm.PsBeginDate);
end;

procedure TfmWOrder.acAPasteUpdate(Sender: TObject);
begin
  acAPaste.Enabled := (ExecSelectSQL('select count(*) from B_AInv where UserId='+
    IntToStr(dm.User.UserId), dmMain.quTmp)<>0) and
       IsActionMayBe;
end;

procedure TfmWOrder.acACopyUpdate(Sender: TObject);
begin
  acACopy.Enabled := not dmMain.taAssortEl.IsEmpty;
end;

procedure TfmWOrder.lcbxExecEnter(Sender: TObject);
begin
  if dmMain.taWOrderListJOBDEPID.IsNull then
  begin
    ActiveControl := lcbxJobPS;
    raise Exception.Create(Format(rsSelectPS, ['']));
  end
  else begin
    ReOpenDataSet(dmMain.taPSMol);
    if not (dmMain.taWOrderList.State in [dsInsert, dsEdit]) then dmMain.taWOrderList.Edit;
    dmMain.taWOrderListJOBID.AsInteger := dmMain.taPSMolMOLID.AsInteger;
  end;
  SetOPeriod;
end;

procedure TfmWOrder.lcbxJobPSCloseUp(Sender: TObject; Accept: Boolean);
begin
  ActiveControl := lcbxExec;
  ActiveControl := lcbxOper;
  ActiveControl := lcbxJobPS;
end;

procedure TfmWOrder.lcbxJobPSKeyValueChanged(Sender: TObject);
begin
  ActiveControl := lcbxExec;
  ActiveControl := lcbxOper;
  ActiveControl := lcbxJobPS;
end;

procedure TfmWOrder.gridAElGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if Column.FieldName = 'RecNo' then Background := clMoneyGreen;
end;

procedure TfmWOrder.FormResize(Sender: TObject);
begin
  spitExit.Left := tb1.Width - tb1.BtnWidth - 10;
end;

procedure TfmWOrder.acPrintExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taWOrder,  dmMain.taWOrderList]);
  if(dmMain.taWOrderITTYPE.AsInteger in [0,6])then
    dmPrint.PrintDocumentA(dkInv16In, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]))
  else
    dmPrint.PrintDocumentA(dkInv16Out, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]));
end;

procedure TfmWOrder.acPrintInvWithAssortExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taWOrder, dmMain.taWOrderList]);
  if(dmMain.taWOrderITTYPE.AsInteger in [0,6])then
    dmPrint.PrintDocumentA(dkInv16InAssort, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]))
  else
    dmPrint.PrintDocumentA(dkInv16OutAssort, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]));
end;

procedure TfmWOrder.acPrintPreviewExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taWOrder, dmMain.taWOrderList]);
  if(dmMain.taWOrderITTYPE.AsInteger in [0,6])then
    dmPrint.PrintDocumentA(dkInv16In_Preview, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]))
  else
    dmPrint.PrintDocumentA(dkInv16Out_Preview, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]));
end;

procedure TfmWOrder.acTotalWOrderExecute(Sender: TObject);
begin
  dmMain.PsId_WorderTotal := dmMain.taWOrderListJOBDEPID.AsInteger;
  dmMain.PsName_WOrderTotal := dmMain.taWOrderListJOBDEPNAME.AsString;
  dmMain.OperId_WOrderTotal := dmMain.taWOrderListDEFOPER.AsString;
  dmMain.OperName_WOrderTotal := dmMain.taWOrderListDEFOPERNAME.AsString;
  dmMain.BD_WOrderTotal := dm.BeginDate;
  dmMain.ED_WOrderTotal := dm.EndDate;
  ShowAndFreeForm(TfmWorderTotal, TForm(fmWorderTotal));
end;

procedure TfmWOrder.acProcessApplExecute(Sender: TObject);
begin
  inherited;
  //
end;


procedure TfmWOrder.FormActivate(Sender: TObject);
begin
  inherited;
  FItType := 255;
  ItType := 0;
  OnActivate := nil;
end;

function TfmWOrder.TryDelete: Boolean;
var
  s: string;
  DepartmentID: Integer;
begin
  Result := True;
  if Result and (dmMain.taWOrderListISCLOSE.AsInteger = 1) then
  begin
    Application.MessageBox(PChar(Format(rsWOrderClose, [dmMain.taWOrderListDOCNO.AsString])),'������', MB_OK);
    Result := False;
  end;
  if Result and (dmMain.taWOrderITDATE.AsDateTime <= dm.PsBeginDate) then
  begin
    Result := False;
    case dm.PsTypeBeginDate of
      0: s := '�� �������� ��������������';
      1: s := '�� ������������ ���������';
    end;
    Application.MessageBox(PChar(Format(rsPeriodClose, [dmMain.taWOrderJOBDEPNAME.AsString, DateToStr(dm.PsBeginDate), s])),'������', MB_OK);
  end;
  if Result and not dm.IsAdm then
  begin
    DepartmentID := dmMain.taWOrderDEPID.AsInteger;
    if DepartmentID <> dm.User.wWhId then
    begin
      Result := False;
      Application.MessageBox(PChar(Format(rsAccessDenied, [dm.User.FIO, '��� ������ � '+dmmain.taWOrderDEPNAME.AsString])),'������', MB_OK);
    end;
  end;
end;

function TfmWOrder.TryInsert: Boolean;
var
  s: string;
  DepartmentID: Integer;
begin
  Result := True;
  if Result and (dmMain.taWOrderListISCLOSE.AsInteger = 1) then
  begin
    Application.MessageBox(PChar(Format(rsWOrderClose, [dmMain.taWOrderListDOCNO.AsString])),'������', MB_OK);
    Result := False;
  end;
  if Result and (dmMain.taWOrderListDOCDATE.AsDateTime <= dm.PsBeginDate) then
  begin
    Result := False;
    case dm.PsTypeBeginDate of
      0: s := '�� �������� ��������������';
      1: s := '�� ������������ ���������';
    end;
    Application.MessageBox(PChar(Format(rsPeriodClose, [dmMain.taWOrderJOBDEPNAME.AsString, DateToStr(dm.PsBeginDate), s])),'������', MB_OK);
  end;
end;

function TfmWOrder.TryUpdate: Boolean;
var
  s: string;
  DepartmentID: Integer;
begin
  Result := True;
  if Result and (dmMain.taWOrderListISCLOSE.AsInteger = 1) then
  begin
    Application.MessageBox(PChar(Format(rsWOrderClose, [dmMain.taWOrderListDOCNO.AsString])),'������', MB_OK);
    Result := False;
  end;

  if Result and (dmMain.taWOrderITDATE.AsDateTime <= dm.PsBeginDate) then
  begin
    Result := False;
    case dm.PsTypeBeginDate of
      0: s := '�� �������� ��������������';
      1: s := '�� ������������ ���������';
    end;
    Application.MessageBox(PChar(Format(rsPeriodClose, [dmMain.taWOrderJOBDEPNAME.AsString, DateToStr(dm.PsBeginDate), s])),'������', MB_OK);
  end;

  if Result and not dm.IsAdm then
  begin
    DepartmentID := dmMain.taWOrderDEPID.AsInteger;
    if DepartmentID <> dm.User.wWhId then
    begin
      Result := False;
      Application.MessageBox(PChar(Format(rsAccessDenied, [dm.User.FIO, '��� ������ � '+dmmain.taWOrderDEPNAME.AsString])),'������', MB_OK);
    end;
  end;
end;

end.
