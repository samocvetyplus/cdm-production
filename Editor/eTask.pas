unit eTask;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DbEditor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrlsEh,
  DBGridEh, cxPC, cxControls, cxContainer, cxEdit, cxTextEdit, cxMemo,
  DBLookupEh, Grids, cxDBEdit, Menus,  cxSplitter, ActnList,
  cxImage,
  DBGridEhGrouping, rxSpeedbar, GridsEh, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters;

type
  TfmeTask = class(TfmDbEditor)
    edN: TDBEditEh;
    Label1: TLabel;
    Label2: TLabel;
    edDate: TDBDateTimeEditEh;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    edBriefDescr: TDBEditEh;
    Label6: TLabel;
    Label7: TLabel;
    edK: TDBEditEh;
    Label8: TLabel;
    pc: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cxTabSheet2: TcxTabSheet;
    edType: TDBLookupComboboxEh;
    edUser: TDBEditEh;
    edDescr: TcxDBMemo;
    pcImg: TcxPageControl;
    pmImg: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    sbOperations: TSpeedBar;
    SpeedbarSection1: TSpeedbarSection;
    SpeedItem1: TSpeedItem;
    SpeedItem2: TSpeedItem;
    edDep: TDBEditEh;
    cxSplitter1: TcxSplitter;
    DBGridEh2: TDBGridEh;
    SpeedItem3: TSpeedItem;
    SpeedItem4: TSpeedItem;
    ActionList1: TActionList;
    acGive: TAction;
    acReturn: TAction;
    acSignAdd: TAction;
    acSignDelete: TAction;
    acImgAdd: TAction;
    acImgRemove: TAction;
    acImgRename: TAction;
    procedure FormCreate(Sender: TObject);
    procedure edTypeDropDownBoxGetCellParams(Sender: TObject;
      Column: TColumnEh; AFont: TFont; var Background: TColor;
      State: TGridDrawState);
    procedure acGiveExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acReturnExecute(Sender: TObject);
    procedure acSignDeleteExecute(Sender: TObject);
    procedure acSignDeleteUpdate(Sender: TObject);
    procedure acSignAddExecute(Sender: TObject);
    procedure acSignAddUpdate(Sender: TObject);
    procedure acImgAddExecute(Sender: TObject);
    procedure acImgRemoveUpdate(Sender: TObject);
    procedure acImgRenameExecute(Sender: TObject);
    procedure acImgRemoveExecute(Sender: TObject);
    procedure acImgRenameUpdate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmeTask: TfmeTask;

implementation

uses Data, DBUtil, DB, eGivTask, DictData, pFIBQuery, DBTables, eTaskImg, Jpeg,
     MsgDialog;

{$R *.dfm}

procedure TfmeTask.FormCreate(Sender: TObject);
var
  TS:TcxTabSheet;
  Img:TcxImage;
  Blob:TStream;
  Jpg:TJpegImage;
begin
  inherited;
  with dmData do begin
    ReOpenDataSets([taTaskOperations, taLookupMol, taTaskOperSigns, taTaskImg]);
    while not taTaskImg.Eof do begin
      TS:=TcxTabSheet.Create(pcImg);
      TS.PageControl:=pcImg;
      TS.Caption:=taTaskImgNAME.AsString;
      TS.TabVisible:=true;
      TS.Tag:=taTaskImgID.AsInteger;
      Img:=TcxImage.Create(TS);
      Img.PopupMenu:=pmImg;
      if not taTaskImgIMG.IsNull then begin
        Jpg:=TJpegImage.Create;
        Blob:=dmData.taTaskImg.CreateBlobStream(taTaskImgIMG, bmRead);
        Jpg.LoadFromStream(Blob);
        Img.Picture.Assign(Jpg);
        FreeAndNil(Blob);
        FreeAndNil(Jpg);
      end;
      Img.Parent:=TS;
      Img.AutoSize:=true;
      Img.Align:=alClient;
      Img.Style.BorderStyle:=ebsFlat;
      Img.StyleDisabled.BorderStyle:=ebsFlat;
      Img.StyleFocused.BorderStyle:=ebsFlat;
      Img.StyleHot.BorderStyle:=ebsFlat;
      Img.Properties.Stretch:=true;
      Img.Properties.ReadOnly:=true;
      taTaskImg.Next;
    end;
  end;
  pc.ActivePageIndex:=0;
end;

procedure TfmeTask.edTypeDropDownBoxGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if not dmData.taLookupTaskTypeCOLOR.IsNull then Background:=dmData.taLookupTaskTypeCOLOR.AsInteger;
end;

procedure TfmeTask.acGiveExecute(Sender: TObject);
begin
  inherited;
  with fmeGivTask do begin
    if Assigned(fmeGivTask) then FreeAndNil(fmeGivTask);
    fmeGivTask:=TfmeGivTask.Create(Application);
    Caption:='������';
    with dmData do begin
      ReopenDataSets([taDeps, taOperToGive]);
      edOper.KeyValue:=taTaskOperationsDTASKOPERID.AsInteger;
    end;
    if ShowModal=mrOk then
    with dmData, quAddOper do begin
      if Open then quAddOper.Close;
      ParamByName('DAT').AsDateTime:=fmeGivTask.edDate.Value;
      ParamByName('OPERID').AsInteger:=fmeGivTask.edOper.KeyValue;
      ParamByName('TASKID').AsInteger:=dmData.taTaskListID.AsInteger;
      ParamByName('DEPID').AsInteger:=fmeGivTask.edDeps.Value;
      ParamByName('MOLID').AsInteger:=dm.User.UserID;
      ParamByName('STATE').AsShort:=0;
      ExecQuery;
      dm.tr.CommitRetaining;
      quAddOper.Close;
    end;

    with dmData do begin
      CloseDataSets([taDeps, taOperToGive]);
      ReopenDataSets([taTaskOperations]);
    end;
  end;
end;

procedure TfmeTask.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  with dmData do
    CloseDataSets([taTaskOperSigns, taLookupMol,taTaskOperations, taTaskImg]);
end;

procedure TfmeTask.acReturnExecute(Sender: TObject);
  var i:integer;
begin
  inherited;
  with fmeGivTask do begin
    if Assigned(fmeGivTask) then FreeAndNil(fmeGivTask);
    fmeGivTask:=TfmeGivTask.Create(Application);
    Caption:='�������';
    edDeps.Visible:=false;
    Label2.Visible:=false;
    edOper.Visible:=false;
    Label3.Visible:=false;
    if ShowModal=mrOk then
    with dmData, quAddOper do begin
      if Open then quAddOper.Close;
      for i:=0 to Params.Count-1 do
        Params.Vars[i].Clear;
      ParamByName('DAT').AsDateTime:=fmeGivTask.edDate.Value;
      ParamByName('TASKID').AsInteger:=dmData.taTaskListID.AsInteger;
      ParamByName('MOLID').AsInteger:=dm.User.UserID;
      ParamByName('STATE').AsShort:=1;
      ExecQuery;
      dm.tr.CommitRetaining;
      quAddOper.Close;
    end;

    with dmData do begin
      ReopenDataSets([taTaskOperations]);
    end;
  end;
end;

procedure TfmeTask.acSignDeleteExecute(Sender: TObject);
begin
  inherited;
  dmData.taTaskOperSigns.Delete;
end;

procedure TfmeTask.acSignDeleteUpdate(Sender: TObject);
begin
  inherited;
  acSignDelete.Enabled:=not dmData.taTaskOperSigns.IsEmpty;
end;

procedure TfmeTask.acSignAddExecute(Sender: TObject);
begin
  inherited;
  dmData.taTaskOperSigns.Open;
  dmData.taTaskOperSigns.Append;
end;

procedure TfmeTask.acSignAddUpdate(Sender: TObject);
begin
  inherited;
  acSignAdd.Enabled:=not dmData.taTaskOperationsTASK_OPERID.IsNull;
end;

procedure TfmeTask.acImgAddExecute(Sender: TObject);
var
  Jpg:TJpegImage;
  Blob:TStream;
  TS:TcxTabSheet;
  Img:TcxImage;
begin
  inherited;
  if Assigned(fmeTaskImage) then FreeAndNil(fmeTaskImage);
  fmeTaskImage:=TfmeTaskImage.Create(Application);
  with fmeTaskImage do
    if ShowModal=mrOk then begin
      Jpg:=TJpegImage.Create;
      try
        Jpg.LoadFromFile(dlg.FileName);
        TS:=TcxTabSheet.Create(pcImg);
        TS.PageControl:=pcImg;
        TS.Caption:=edImgName.Text;
        TS.TabVisible:=true;
        Img:=TcxImage.Create(TS);
        Img.PopupMenu:=pmImg;
        Img.Picture.Assign(Jpg);
        with dmData, taTaskImg do begin
          Append;
          taTaskImgNAME.AsString:=edImgName.Text;
          Blob:=taTaskImg.CreateBlobStream(taTaskImgIMG,bmWrite);
          Jpg.SaveToStream(Blob);
          Post;
          TS.Tag:=taTaskImgID.AsInteger;
        {if not taTaskImgIMG.IsNull then begin
          Blob:=dmData.taTaskImg.CreateBlobStream(taTaskImgIMG, bmRead);
          Img.Picture.Bitmap.LoadFromStream(Blob);
          FreeAndNil(Blob);
        end;}
        end;
        Img.Parent:=TS;
        Img.AutoSize:=true;
        Img.Align:=alClient;
        Img.Style.BorderStyle:=ebsFlat;
        Img.StyleDisabled.BorderStyle:=ebsFlat;
        Img.StyleFocused.BorderStyle:=ebsFlat;
        Img.StyleHot.BorderStyle:=ebsFlat;
        Img.Properties.Stretch:=true;
        Img.Properties.ReadOnly:=true;
      finally
        FreeAndNil(Jpg);
      end;
    end;
  FreeAndNil(fmeTaskImage);
end;

procedure TfmeTask.acImgRemoveUpdate(Sender: TObject);
begin
  inherited;
  acImgRemove.Enabled:=pcImg.ActivePageIndex<>-1;
end;

procedure TfmeTask.acImgRenameExecute(Sender: TObject);
begin
  inherited;
  if Assigned(fmeTaskImage) then FreeAndNil(fmeTaskImage);
  fmeTaskImage:=TfmeTaskImage.Create(Application);
  with fmeTaskImage do begin
    Label2.Visible:=false;
    edImgFile.Visible:=false;
    edImgName.Text:=pcImg.ActivePage.Caption;
    edImgName.SelectAll;
    if ShowModal=mrOk then begin
      if not dmData.taTaskImg.Locate('ID',VarArrayOf([pcImg.ActivePage.Tag]),[])then
        MessageDialog('������ ��� ��������������� �����������', mtError, [mbOk],0)
      else begin
        dmData.taTaskImg.Edit;
        pcImg.ActivePage.Caption:=edImgName.Text;
        dmData.taTaskImgNAME.AsString:=edImgName.Text;
        dmData.taTaskImg.Post;
      end;
    end; 
  end;
  try
    
  finally
    FreeAndNil(fmeTaskImage);
  end;
end;

procedure TfmeTask.acImgRemoveExecute(Sender: TObject);
begin
  inherited;
  with dmData, taTaskImg do begin
    if not Locate('ID',VarArrayOf([pcImg.ActivePage.Tag]),[]) then
      MessageDialog('������ ��� ��������� �����������', mtError, [mbOk],0)
    else begin
      pcImg.ActivePage.Free;
      Delete;
    end;
  end;
end;

procedure TfmeTask.acImgRenameUpdate(Sender: TObject);
begin
  inherited;
  acImgRename.Enabled:=pcImg.ActivePageIndex<>-1;
end;

end.
