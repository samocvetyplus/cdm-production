inherited fmeSemisProp: TfmeSemisProp
  Left = 357
  ActiveControl = edSz
  Caption = #1056#1077#1076#1072#1082#1090#1086#1088' '#1093#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082' '
  ClientHeight = 247
  ClientWidth = 230
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 230
    Height = 247
    object lbEdgSh: TLabel [0]
      Left = 8
      Top = 54
      Width = 37
      Height = 13
      Caption = #1060#1086#1088#1084#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbEdgT: TLabel [1]
      Left = 8
      Top = 76
      Width = 43
      Height = 13
      Caption = #1054#1075#1088#1072#1085#1082#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbChromaticity: TLabel [2]
      Left = 8
      Top = 98
      Width = 54
      Height = 13
      Caption = #1062#1074#1077#1090#1085#1086#1089#1090#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbCleannes: TLabel [3]
      Left = 8
      Top = 120
      Width = 42
      Height = 13
      Caption = #1063#1080#1089#1090#1086#1090#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbGr: TLabel [4]
      Left = 8
      Top = 141
      Width = 35
      Height = 13
      Caption = #1043#1088#1091#1087#1087#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbColor: TLabel [5]
      Left = 8
      Top = 164
      Width = 25
      Height = 13
      Caption = #1062#1074#1077#1090
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbSz: TLabel [6]
      Left = 8
      Top = 8
      Width = 39
      Height = 13
      Caption = #1056#1072#1079#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel [7]
      Left = 8
      Top = 30
      Width = 31
      Height = 13
      Caption = #1054#1090#1089#1077#1074
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel [8]
      Left = 8
      Top = 186
      Width = 26
      Height = 13
      Caption = #1062#1077#1085#1072
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    inherited btnOk: TBitBtn
      Left = 46
      Top = 212
      Anchors = [akRight, akBottom]
      TabOrder = 9
    end
    inherited btnCancel: TBitBtn
      Left = 138
      Top = 212
      Anchors = [akRight, akBottom]
      TabOrder = 10
    end
    object edSz: TDBEditEh
      Left = 64
      Top = 6
      Width = 153
      Height = 19
      DataField = 'SZ'
      DataSource = dm.dsrSemis
      EditButtons = <>
      Flat = True
      TabOrder = 0
      Visible = True
    end
    object edO1: TDBEditEh
      Left = 64
      Top = 28
      Width = 153
      Height = 19
      DataField = 'O1'
      DataSource = dm.dsrSemis
      EditButtons = <>
      Flat = True
      TabOrder = 1
      Visible = True
    end
    object lcbxEdgShape: TDBLookupComboboxEh
      Left = 64
      Top = 50
      Width = 153
      Height = 19
      DataField = 'EDGSHAPEID'
      DataSource = dm.dsrSemis
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      EditButtons = <>
      Flat = True
      KeyField = 'ID'
      ListField = 'NAME'
      ListSource = dm.dsrEdgShape
      TabOrder = 2
      Visible = True
    end
    object lcbxEdgT: TDBLookupComboboxEh
      Left = 64
      Top = 72
      Width = 153
      Height = 19
      DataField = 'EDGTID'
      DataSource = dm.dsrSemis
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      EditButtons = <>
      Flat = True
      KeyField = 'ID'
      ListField = 'NAME'
      ListSource = dm.dsrEdgT
      TabOrder = 3
      Visible = True
    end
    object lcbxChromaticity: TDBLookupComboboxEh
      Left = 64
      Top = 94
      Width = 153
      Height = 19
      DataField = 'CHROMATICITY'
      DataSource = dm.dsrSemis
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      EditButtons = <>
      Flat = True
      KeyField = 'ID'
      ListField = 'NAME'
      ListSource = dm.dsrChromaticity
      TabOrder = 4
      Visible = True
    end
    object lcbxCleannes: TDBLookupComboboxEh
      Left = 64
      Top = 116
      Width = 153
      Height = 19
      DataField = 'CLEANNES'
      DataSource = dm.dsrSemis
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      EditButtons = <>
      Flat = True
      KeyField = 'ID'
      ListField = 'NAME'
      ListSource = dm.dsrCleannes
      TabOrder = 5
      Visible = True
    end
    object lcbxIGr: TDBLookupComboboxEh
      Left = 64
      Top = 138
      Width = 153
      Height = 19
      DataField = 'IGR'
      DataSource = dm.dsrSemis
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      EditButtons = <>
      Flat = True
      KeyField = 'ID'
      ListField = 'NAME'
      ListSource = dm.dsrIGr
      TabOrder = 6
      Visible = True
    end
    object lcbxColor: TDBLookupComboboxEh
      Left = 64
      Top = 160
      Width = 153
      Height = 19
      DataField = 'COLOR'
      DataSource = dm.dsrSemis
      DropDownBox.ColumnDefValues.Title.TitleButton = True
      EditButtons = <>
      Flat = True
      KeyField = 'ID'
      ListField = 'NAME'
      ListSource = dm.dsrColor
      TabOrder = 7
      Visible = True
    end
    object edPrice: TDBNumberEditEh
      Left = 64
      Top = 182
      Width = 153
      Height = 19
      DataField = 'PRICE'
      DataSource = dm.dsrSemis
      EditButtons = <>
      Flat = True
      TabOrder = 8
      Visible = True
    end
  end
end
