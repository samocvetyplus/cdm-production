object fmCompLogin: TfmCompLogin
  Left = 332
  Top = 408
  BorderStyle = bsDialog
  Caption = #1042#1099#1073#1086#1088' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
  ClientHeight = 94
  ClientWidth = 377
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 67
    Height = 13
    Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
  end
  object OkBtn: TBitBtn
    Left = 292
    Top = 4
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    TabOrder = 0
    OnClick = OkBtnClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000010000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object CancelBtn: TBitBtn
    Left = 292
    Top = 36
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 1
    Kind = bkCancel
  end
  object lcbxComp: TDBLookupComboboxEh
    Left = 97
    Top = 11
    Width = 189
    Height = 19
    DropDownBox.ColumnDefValues.Title.TitleButton = True
    DropDownBox.Sizable = True
    DropDownBox.Width = -1
    EditButtons = <>
    Flat = True
    KeyField = 'COMPID'
    ListField = 'NAME'
    ListSource = dsrComp
    TabOrder = 2
    Visible = True
  end
  object chbxTolling: TDBCheckBoxEh
    Left = 16
    Top = 40
    Width = 65
    Height = 13
    Action = acTolling
    Flat = True
    TabOrder = 3
    ValueChecked = 'True'
    ValueUnchecked = 'False'
    Visible = False
  end
  object lcbxTolling: TDBLookupComboboxEh
    Left = 176
    Top = 36
    Width = 109
    Height = 19
    DropDownBox.ColumnDefValues.Title.TitleButton = True
    EditButtons = <>
    Flat = True
    KeyField = 'COMPID'
    ListField = 'NAME'
    ListSource = dsrTolling
    TabOrder = 4
    Visible = False
  end
  object cmbxTolling: TDBComboBoxEh
    Left = 97
    Top = 36
    Width = 77
    Height = 19
    EditButtons = <>
    Flat = True
    Items.Strings = (
      #1079#1072#1082#1072#1079#1095#1080#1082
      #1087#1086#1076#1088#1103#1076#1095#1080#1082)
    KeyItems.Strings = (
      '0'
      '1')
    TabOrder = 5
    Visible = False
  end
  object taComp: TpFIBDataSet
    SelectSQL.Strings = (
      'select COMPID, NAME'
      'from D_COMP'
      'where ISLOGIN=1'
      'order by SORTIND')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
    Left = 204
    Top = 52
    poSQLINT64ToBCD = True
  end
  object dsrComp: TDataSource
    DataSet = taComp
    Left = 248
    Top = 52
  end
  object taTolling: TpFIBDataSet
    SelectSQL.Strings = (
      'select COMPID, NAME'
      'from D_COMP'
      'order by SORTIND')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 8
    Top = 56
    poSQLINT64ToBCD = True
  end
  object acEvent: TActionList
    Left = 60
    Top = 56
    object acTolling: TAction
      Caption = #1058#1086#1083#1083#1080#1085#1075
      OnExecute = acTollingExecute
      OnUpdate = acTollingUpdate
    end
  end
  object dsrTolling: TDataSource
    DataSet = taTolling
    Left = 108
    Top = 60
  end
end
