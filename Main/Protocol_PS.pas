{***********************************************}
{  �������������� ���� ��������                 }
{  Copyrigth (C) 2003 basile for CDM            }
{  v0.45                                        }
{  create 24.09.2003                            }
{  last update 18.12.2003                       }
{***********************************************}

unit Protocol_PS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGridEh, StdCtrls, db, pFIBDataSet, ActnList, TB2Dock,
  TB2Toolbar, TB2Item, DBCtrlsEh, Mask, Menus, DBCtrls,
  FIBDataSet, DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar, DBGrids, dbUtil,
  FIBQuery, pFIBQuery, DBClient;

type
  TfmProtocol_PS = class(TfmAncestor)
    Panel2: TPanel;
    plMain: TPanel;
    plSemis: TPanel;
    gridSemis: TDBGridEh;
    Panel5: TPanel;
    Splitter1: TSplitter;
    Panel6: TPanel;
    Panel7: TPanel;
    gridAssort: TDBGridEh;
    taPIt_TQ: TpFIBDataSet;
    taPIt_TW: TpFIBDataSet;
    taPIt_TWINW: TFloatField;
    taPIt_TWGETW: TFloatField;
    taPIt_TWREWORKW: TFloatField;
    taPIt_TWDONEW: TFloatField;
    taPIt_TWRETW: TFloatField;
    taPIt_TWREJW: TFloatField;
    taPIt_TWNKW: TFloatField;
    taPIt_TWIW: TFloatField;
    taPIt_TWFW: TFloatField;
    taPIt_TWRW: TFloatField;
    TBDock1: TTBDock;
    tlbrSemis: TTBToolbar;
    acEvent: TActionList;
    acAddSemis: TAction;
    acDelSemis: TAction;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBDock2: TTBDock;
    tlbrArt: TTBToolbar;
    acAddArt: TAction;
    acDelArt: TAction;
    TBItem3: TTBItem;
    TBItem4: TTBItem;
    spitCreateProtocol: TSpeedItem;
    acCreateProtocol: TAction;
    acCloseProtocol: TAction;
    acDelProtocol: TAction;
    acRefreshProtocol: TAction;
    SpeedItem3: TSpeedItem;
    Label1: TLabel;
    edDocNo: TDBEditEh;
    edDocDate: TDBDateTimeEditEh;
    Label2: TLabel;
    ppSemis: TTBPopupMenu;
    acDetailSemis: TAction;
    acDetailArt: TAction;
    TBItem5: TTBItem;
    TBItem6: TTBItem;
    TBItem7: TTBItem;
    ppArt: TTBPopupMenu;
    TBItem8: TTBItem;
    TBItem9: TTBItem;
    TBItem10: TTBItem;
    acSemisInv: TAction;
    TBItem11: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBSeparatorItem2: TTBSeparatorItem;
    acZeroArt: TAction;
    acZeroSemis: TAction;
    acCopySemis: TAction;
    acCopyArt: TAction;
    TBItem12: TTBItem;
    TBItem13: TTBItem;
    TBItem14: TTBItem;
    TBItem15: TTBItem;
    acPrintProtocol: TAction;
    gridPS: TDBGridEh;
    SpeedItem4: TSpeedItem;
    Label3: TLabel;
    txtUpd: TDBText;
    plWOrder: TPanel;
    Splitter2: TSplitter;
    TBDock3: TTBDock;
    tlbrWOrder: TTBToolbar;
    plExt: TPanel;
    plExtTitle: TPanel;
    lbMat: TLabel;
    Label7: TLabel;
    cmbxMat: TDBComboBoxEh;
    cmbxOper: TDBComboBoxEh;
    gridWhSemis: TDBGridEh;
    tbdcSemis: TTBDock;
    tlbrWOrderControlPanel: TTBToolbar;
    TBItem16: TTBItem;
    TBItem17: TTBItem;
    TBItem18: TTBItem;
    gridItem: TDBGridEh;
    acAddToWOrder: TAction;
    acDelFromWOrder: TAction;
    acAddNewToWOrder: TAction;
    TBControlItem1: TTBControlItem;
    Label4: TLabel;
    TBControlItem2: TTBControlItem;
    txtWOrderNo: TDBText;
    TBItem19: TTBItem;
    acCreateWOrder: TAction;
    TBSeparatorItem3: TTBSeparatorItem;
    TBControlItem3: TTBControlItem;
    Label5: TLabel;
    TBControlItem4: TTBControlItem;
    txtWOrderDate: TDBText;
    tlbrT: TTBToolbar;
    it0: TTBItem;
    TBSeparatorItem4: TTBSeparatorItem;
    it3: TTBItem;
    acItType0: TAction;
    acItType3: TAction;
    ppInv: TPopupMenu;
    mnitAddToInv: TMenuItem;
    mnitAddToNew: TMenuItem;
    mnitDelItem: TMenuItem;
    N3: TMenuItem;
    mnitPrint: TMenuItem;
    N7: TMenuItem;
    acPrintInv: TAction;
    acPrintInvWithAssort: TAction;
    acAssortiment: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    acCloseWOrder: TAction;
    TBItem20: TTBItem;
    TBDock4: TTBDock;
    tlbrPS: TTBToolbar;
    acOpenDetail: TAction;
    acAddNew: TAction;
    acAddItem: TAction;
    TBItem21: TTBItem;
    TBControlItem5: TTBControlItem;
    chbxW: TDBCheckBoxEh;
    acACopy: TAction;
    acAPaste: TAction;
    N4: TMenuItem;
    N5: TMenuItem;
    TBItem22: TTBItem;
    taPIt_TQINQ: TFIBBCDField;
    taPIt_TQGETQ: TFIBBCDField;
    taPIt_TQREWORKQ: TFIBBCDField;
    taPIt_TQDONEQ: TFIBBCDField;
    taPIt_TQRETQ: TFIBBCDField;
    taPIt_TQREJQ: TFIBBCDField;
    taPIt_TQNKQ: TFIBBCDField;
    taPIt_TQIQ: TFIBBCDField;
    taPIt_TQFQ: TFIBBCDField;
    taPIt_TQRQ: TFIBBCDField;
    TBItem23: TTBItem;
    acPrintResult: TAction;
    DBGrid1: TDBGrid;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure gridSemisSumListAfterRecalcAll(Sender: TObject);
    procedure acCreateProtocolExecute(Sender: TObject);
    procedure acAddSemisExecute(Sender: TObject);
    procedure acDelSemisExecute(Sender: TObject);
    procedure acDelArtExecute(Sender: TObject);
    procedure acDetailSemisExecute(Sender: TObject);
    procedure acDetailArtExecute(Sender: TObject);
    procedure acDelSemisUpdate(Sender: TObject);
    procedure acDelArtUpdate(Sender: TObject);
    procedure acSemisInvExecute(Sender: TObject);
    procedure acCloseProtocolExecute(Sender: TObject);
    procedure acRefreshProtocolExecute(Sender: TObject);
    procedure acAddSemisUpdate(Sender: TObject);
    procedure acAddArtExecute(Sender: TObject);
    procedure acAddArtUpdate(Sender: TObject);
    procedure acRefreshProtocolUpdate(Sender: TObject);
    procedure acDelProtocolUpdate(Sender: TObject);
    procedure acCloseProtocolUpdate(Sender: TObject);
    procedure acCreateProtocolUpdate(Sender: TObject);
    procedure acDelProtocolExecute(Sender: TObject);
    procedure taPIt_TQBeforeOpen(DataSet: TDataSet);
    procedure taPIt_TWBeforeOpen(DataSet: TDataSet);
    procedure acZeroArtExecute(Sender: TObject);
    procedure acCopySemisExecute(Sender: TObject);
    procedure acZeroSemisExecute(Sender: TObject);
    procedure acCopyArtExecute(Sender: TObject);
    procedure acCopySemisUpdate(Sender: TObject);
    procedure acZeroSemisUpdate(Sender: TObject);
    procedure acZeroArtUpdate(Sender: TObject);
    procedure acCopyArtUpdate(Sender: TObject);
    procedure acPrintProtocolExecute(Sender: TObject);
    procedure acPrintProtocolUpdate(Sender: TObject);
    procedure acAddNewToWOrderExecute(Sender: TObject);
    procedure it0Click(Sender: TObject);
    procedure it7Click(Sender: TObject);
    procedure acPrintInvExecute(Sender: TObject);
    procedure acPrintInvWithAssortExecute(Sender: TObject);
    procedure acAssortimentExecute(Sender: TObject);
    procedure acAssortimentUpdate(Sender: TObject);
    procedure cmbxMatChange(Sender: TObject);
    procedure cmbxOperChange(Sender: TObject);
    procedure gridWhSemisKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure gridItemExit(Sender: TObject);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure gridItemKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure gridItemOrderBy(Sender: TObject; OrderBy: String);
    procedure acAddToWOrderExecute(Sender: TObject);
    procedure acDelFromWOrderExecute(Sender: TObject);
    procedure acCloseWOrderExecute(Sender: TObject);
    procedure acOpenDetailExecute(Sender: TObject);
    procedure acAddNewExecute(Sender: TObject);
    procedure acAddItemExecute(Sender: TObject);
    procedure acAddNewToWOrderUpdate(Sender: TObject);
    procedure acAddToWOrderUpdate(Sender: TObject);
    procedure acDelFromWOrderUpdate(Sender: TObject);
    procedure acCloseWOrderUpdate(Sender: TObject);
    procedure acCreateWOrderExecute(Sender: TObject);
    procedure acOpenDetailUpdate(Sender: TObject);
    procedure acCreateWOrderUpdate(Sender: TObject);
    procedure acItType0Execute(Sender: TObject);
    procedure acItType3Execute(Sender: TObject);
    procedure acItType0Update(Sender: TObject);
    procedure acItType3Update(Sender: TObject);
    procedure acPrintInvUpdate(Sender: TObject);
    procedure acPrintInvWithAssortUpdate(Sender: TObject);
    procedure acACopyExecute(Sender: TObject);
    procedure acAPasteExecute(Sender: TObject);
    procedure acACopyUpdate(Sender: TObject);
    procedure acAPasteUpdate(Sender: TObject);
    procedure gridItemColumns7EditButtonClick(Sender: TObject;
      var Handled: Boolean);
    procedure acPrintResultExecute(Sender: TObject);
    procedure Button1Click(Sender: TObject);

  private
    FwOrderId : integer;
    FExistProtocol : boolean;
    FSaveAfterScroll : TDataSetNotifyEvent;
    FSaveAfterOpen   : TDataSetNotifyEvent;
    FItType: byte;
    FAccess : boolean;
    FReadOnly: boolean;
    procedure SetItType(const Value: byte);
    procedure Access;
    //procedure InitBtn(T : boolean);
    //procedure SetOPeriod;
    procedure SetReadOnly(ReadOnly : boolean);
    procedure OpenWorder;
    procedure CloseWOrder;
    procedure ReOpenSemis;
    procedure ReOpenArt;
  public
    Groups : TClientDataSet;
    procedure SetCurrInv;
    procedure ReOpenDetail(DataSet : TDataSet);
    procedure CloseDoc;
    procedure OpenDoc;
    property ItType : byte read FItType write SetItType; // 0 - ������, 1 - �����
  end;

var
  fmProtocol_PS: TfmProtocol_PS;

implementation

uses MainData, DictData, MsgDialog, ProtSJInfo, fmUtils, PrintData,
     DictAncestor, DbUtilsEh, ApplData, ADistr, eSQ, dDepT1, dbTree, Editor,
     ProductionConsts, frmAffiangePassport, dmReportInventory,
     PrintPersonalInfo;

{$R *.dfm}

procedure TfmProtocol_PS.FormCreate(Sender: TObject);
var
  Id : Variant;
  Query : tpFIBQuery;
  PassportID : Integer;
  SQL : String;
  Stream : TMemoryStream;
begin
  if (dm.User.Profile = - 1) or ((dm.user.AccProd_d and 8) <> 8)
    then
      FReadOnly := false
  else
    FReadOnly := true;

  //SaveCurrentLosses;  ������ ����� �������� �������� � �������������
                      //id � ������� �� ������� ������.
                      //��������� ������� affinage$losses  ��� ����� ����������
                      //��������. ���������������, �.�. ������ �������������
                      //��������� affinage$losses. (�����, 24.10.2012)

  dmMain.FilterMatId := ROOT_MAT_CODE;
  FSaveAfterScroll := dmMain.taProtocol_d.AfterScroll;
  FSaveAfterOpen := dmMain.taProtocol_d.AfterOpen;
  dmMain.taProtocol_d.AfterOpen := ReOpenDetail;
  dmMain.taProtocol_d.AfterScroll := ReOpenDetail;
  dmMain.taProtocol_d.Filtered := True;
  Id := ExecSelectSQL('select min(Id) from Act where T=1 and A=1 and IsClose=0', dmMain.quTmp);
  if VarIsNull(Id)or(Id = 0)
    then
      FExistProtocol := False
  else
    begin
      FExistProtocol := True;
      OpenDataSet(dmMain.taPList);
      if not dmMain.taPList.Locate('ID', Id, [])
        then
          raise EInternal.Create(Format(rsInternalError, ['70']));
      OpenDataSet(dmMain.taProtocol_d);
    end;
  inherited;
  gridSemis.FieldColumns['INW'].Visible := False;
  gridSemis.FieldColumns['INQ'].Visible := False;
  gridSemis.FieldColumns['GETW'].Visible := False;
  gridSemis.FieldColumns['GETQ'].Visible := False;
  gridSemis.FieldColumns['REWORKW'].Visible := False;
  gridSemis.FieldColumns['REWORKQ'].Visible := False;
  gridSemis.FieldColumns['DONEW'].Visible := False;
  gridSemis.FieldColumns['DONEQ'].Visible := False;
  gridSemis.FieldColumns['RETW'].Visible := False;
  gridSemis.FieldColumns['RETQ'].Visible := False;
  gridSemis.FieldColumns['REJW'].Visible := False;
  gridSemis.FieldColumns['REJQ'].Visible := False;
  gridSemis.FieldColumns['NKW'].Visible := False;
  gridSemis.FieldColumns['NKQ'].Visible := False;

  gridAssort.FieldColumns['REST_IN_Q'].Visible := False;
  gridAssort.FieldColumns['GET_Q'].Visible := False;
  gridAssort.FieldColumns['DONE_Q'].Visible := False;
  gridAssort.FieldColumns['REJ_Q'].Visible := False;

  //tlbrSemis.Skin := dm.TBSkin;
  //tlbrArt.Skin := dm.TBSkin;
  //ppSemis.Skin := dm.ppSkin;
  //ppArt.Skin := dm.ppSkin;
  //tlbrPS.Skin := dm.TBSkin;
  //tlbrWOrder.Skin := dm.TBSkin;

  gridItem.FieldColumns['INVID'].Visible    := False;
  gridItem.FieldColumns['WOITEMID'].Visible := False;
  gridItem.FieldColumns['WORDERID'].Visible := False;
  gridItem.FieldColumns['ITDATE'].Visible   := False;
  gridItem.FieldColumns['REJREF'].Visible   := False;

  dm.WorkMode := 'wOrder';
  gridWhSemis.RowLines := 2;
  // ��������� �������� ��� ������ ��������������
  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMat);
  cmbxMat.ItemIndex := 0;
  dm.AMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := cmbxMatChange;

  cmbxOper.OnChange := nil;
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.Items.Insert(1, sNoOperation);
  cmbxOper.ItemIndex := 0;
  dmMain.FilterOperId := ROOT_OPER_CODE;
  cmbxOper.OnChange := cmbxOperChange;
  FAccess := True;
  FwOrderId := -1;
  cmbxMat.Enabled := False;
  cmbxOper.Enabled := False;
  Self.Resize;

  Query := tpFIBQuery.Create(Self);
  Query.Database := taPit_TW.Database;
  SQL := 'select first 1 ID from Affinage$Passport where ID > 0 and Designation > 0 and State = 2 order by ID desc';
  PassportID := ExecSelectSQL(SQL, Query);
  if not VarIsNull(PassportID) then
    begin
      Query.SQL.Text := 'select groups from affinage$passport passport where id = :id';
      Query.ParamByName('ID').AsInteger := PassportID;
      Query.ExecQuery;

      if not Query.Fields[0].IsNull then
        begin
          Stream := TMemoryStream.Create;
          Query.Fields[0].SaveToStream(Stream);
          Stream.Position := 0;
          Groups := TClientDataSet.Create(Self);
          Groups.IndexFieldNames := 'Place$Of$Storage$ID';
          Groups.LoadFromStream(Stream);
          Groups.LogChanges := false;
          Stream.Free;
        end;
    end;

end;



procedure TfmProtocol_PS.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PostDataSets([dmMain.taPIt_InSemis_d, dmMain.taPArt]);
  CloseDataSets([dmMain.taPIt_InSemis_d, dmMain.taPArt]);
  gridSemis.OnSumListAfterRecalcAll := nil;
  dmMain.taProtocol_d.Filtered := False;
  dmMain.taProtocol_d.AfterOpen := FSaveAfterOpen;
  dmMain.taProtocol_d.AfterScroll := FSaveAfterScroll;
  FreeAndNil(Groups);
  inherited;
end;

procedure TfmProtocol_PS.ReOpenDetail(DataSet: TDataSet);
begin

  CloseDataSets([dmMain.taPIt_InSemis_d, dmMain.taPArt]);

  CloseWOrder;
  
end;

procedure TfmProtocol_PS.gridSemisSumListAfterRecalcAll(Sender: TObject);
begin

  OpenDataSet(taPIt_TW);
  gridSemis.FieldColumns['INW'].Footers[0].Value     := FormatFloat('0.###', taPIt_TWINW.AsFloat);
  gridSemis.FieldColumns['GETW'].Footers[0].Value    := FormatFloat('0.###', taPIt_TWGETW.AsFloat);
  gridSemis.FieldColumns['REWORKW'].Footers[0].Value := FormatFloat('0.###', taPIt_TWREWORKW.AsFloat);
  gridSemis.FieldColumns['DONEW'].Footers[0].Value   := FormatFloat('0.###', taPIt_TWDONEW.AsFloat);
  gridSemis.FieldColumns['RETW'].Footers[0].Value    := FormatFloat('0.###', taPIt_TWRETW.AsFloat);
  gridSemis.FieldColumns['REJW'].Footers[0].Value    := FormatFloat('0.###', taPIt_TWREJW.AsFloat);
  gridSemis.FieldColumns['NKW'].Footers[0].Value     := FormatFloat('0.###', taPIt_TWNKW.AsFloat);
  gridSemis.FieldColumns['W'].Footers[0].Value       := FormatFloat('0.###', taPIt_TWIW.AsFloat+taPIt_TWINW.AsFloat);
  gridSemis.FieldColumns['FW'].Footers[0].Value      := FormatFloat('0.###', taPIt_TWFW.AsFloat);
  gridSemis.FieldColumns['RW'].Footers[0].Value      := FormatFloat('0.###', taPIt_TWIW.AsFloat+taPIt_TWINW.AsFloat-taPIt_TWFW.AsFloat);

  CloseDataSet(taPIt_TW);

  OpenDataSet(taPIt_TQ);

  gridSemis.FieldColumns['INQ'].Footers[0].Value     := taPIt_TQINQ.AsString;
  gridSemis.FieldColumns['GETQ'].Footers[0].Value    := taPIt_TQGETQ.AsString;
  gridSemis.FieldColumns['REWORKQ'].Footers[0].Value := taPIt_TQREWORKQ.AsString;
  gridSemis.FieldColumns['DONEQ'].Footers[0].Value   := taPIt_TQDONEQ.AsString;
  gridSemis.FieldColumns['RETQ'].Footers[0].Value    := taPIt_TQRETQ.AsString;
  gridSemis.FieldColumns['REJQ'].Footers[0].Value    := taPIt_TQREJQ.AsString;
  gridSemis.FieldColumns['NKQ'].Footers[0].Value     := taPIt_TQNKQ.AsString;
  gridSemis.FieldColumns['Q'].Footers[0].Value       := IntToStr(taPIt_TQIQ.AsInteger+taPIt_TQINQ.AsInteger);
  gridSemis.FieldColumns['FQ'].Footers[0].Value      := taPIt_TQFQ.AsString;
  gridSemis.FieldColumns['RQ'].Footers[0].Value      := IntToStr(taPIt_TQIQ.AsInteger-taPIt_TQFQ.AsInteger);

  CloseDataSet(taPIt_TQ);

end;

procedure TfmProtocol_PS.acCreateProtocolExecute(Sender: TObject);
var
  Id : Variant;
  Msg : string;
begin
if not FReadOnly
 then
   begin
     dmMain.FilterMatId := '1';
     Id := ExecSelectSQL('select max(Id) from Act where T=1 and A=1 and IsClose=0', dmMain.quTmp);
     if VarIsNull(Id)or(Id = 0)
       then
         begin
           FExistProtocol := False;
           Msg := '������� �������� �������������� � '+DateToStr(dm.BeginDate)+' �� '+DateToStr(dm.EndDate);
           if MessageDialog(Msg, mtConfirmation, [mbYes, mbNo], 0)<>mrYes
             then
               eXit;
           OpenDataset(dmMain.taPList);
           dmMain.taPList.Append;
           dmMain.taPList.Post;
           Id := dmMain.taPListID.AsInteger;
           Screen.Cursor := crSQLWait;
           Application.ProcessMessages;
           with dmMain do
             try
            // ������ ����
               ExecSQL('execute procedure G_Norma("'+DateToStr(taPListBD.AsDateTime)+'", "'+DateTimeToStr(taPListDOCDATE.AsDateTime)+'")', quTmp);
            // �������� ���� ��������������
               ExecSQL('execute procedure Create_Protocol_PS('+VarToStr(Id)+')', quTmp);
            // �������� ���� �������������� �� ������������
               ExecSQL('execute procedure Create_PArt('+VarToStr(Id)+')', dmMain.quTmp);
             finally
               Screen.Cursor := crDefault;
             end;
           FExistProtocol := True;
         end
     else
       begin
         OpenDataSet(dmMain.taPList);
         if not dmMain.taPList.Locate('ID', Id, [])
           then
             raise EInternal.Create(Format(rsInternalError, ['70']));
         OpenDataSets([dmMain.taProtocol_d]);
         FExistProtocol := True;
       end;
     OpenDataSet(dmMain.taProtocol_d);
   end
else
  ShowMessage('� ��� ��� ���� ��� �������� ��������� ��������������. ���������� � ��������������.');
end;

procedure TfmProtocol_PS.acAddSemisExecute(Sender: TObject);
begin
  dmMain.taPIt_InSemis_d.Insert;
  ActiveControl := gridWhSemis;
  gridSemis.SelectedIndex := 0;
end;

procedure TfmProtocol_PS.acDelSemisExecute(Sender: TObject);
begin
  dmMain.taPIt_InSemis_d.Delete;
end;

procedure TfmProtocol_PS.acDelArtExecute(Sender: TObject);
begin
  dmMain.taPArt.Delete;
end;

procedure TfmProtocol_PS.acDetailSemisExecute(Sender: TObject);
begin
  acDetailSemis.Checked := not acDetailSemis.Checked;
  gridSemis.FieldColumns['INW'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['INQ'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['GETW'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['GETQ'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['REWORKW'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['REWORKQ'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['DONEW'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['DONEQ'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['RETW'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['RETQ'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['REJW'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['REJQ'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['NKW'].Visible := acDetailSemis.Checked;
  gridSemis.FieldColumns['NKQ'].Visible := acDetailSemis.Checked;
end;

procedure TfmProtocol_PS.acDetailArtExecute(Sender: TObject);
begin
  acDetailArt.Checked := not acDetailArt.Checked;
  gridAssort.FieldColumns['REST_IN_Q'].Visible := acDetailArt.Checked;
  gridAssort.FieldColumns['GET_Q'].Visible := acDetailArt.Checked;
  gridAssort.FieldColumns['DONE_Q'].Visible := acDetailArt.Checked;
  gridAssort.FieldColumns['REJ_Q'].Visible := acDetailArt.Checked;
end;

procedure TfmProtocol_PS.acDelSemisUpdate(Sender: TObject);
begin
  acDelSemis.Enabled := not dmMain.taPIt_InSemis_d.IsEmpty;
end;

procedure TfmProtocol_PS.acDelArtUpdate(Sender: TObject);
begin
  acDelArt.Enabled := not dmMain.taPArt.IsEmpty;
end;

procedure TfmProtocol_PS.acSemisInvExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmProtSJInfo, TForm(fmProtSJInfo));
end;

procedure TfmProtocol_PS.acCloseProtocolExecute(Sender: TObject);
begin
if not FReadOnly
 then
   begin
     PostDataSets([dmMain.taPIt_InSemis_d, dmMain.taPArt, dmMain.taProtocol_d, dmMain.taPList]);
     if acCloseProtocol.Caption = '������'
       then
        begin
          CloseDoc;
          acCloseProtocol.Caption := '������';
          acCloseProtocol.Hint := '������� ��������';
          acCloseProtocol.ImageIndex := 5;
          RefreshDataSet(dmMain.taPList);
        end
     else
       begin
         OpenDoc;
         acCloseProtocol.Caption := '������';
         acCloseProtocol.Hint := '������� ��������';
         acCloseProtocol.ImageIndex := 4;
         RefreshDataSet(dmMain.taPList);
       end;
   end
else
  ShowMessage('� ��� ��� ���� ��� �������� ���������.');
end;

procedure TfmProtocol_PS.CloseDoc;
begin
  ExecSQL('execute procedure CloseDoc("��������", '+dmMain.taPListID.AsString+')', dmMain.quTmp);
end;

procedure TfmProtocol_PS.OpenDoc;
begin
  ExecSQL('execute procedure OpenDoc("��������", '+dmMain.taPListID.AsString+')', dmMain.quTmp);
end;

procedure TfmProtocol_PS.acRefreshProtocolExecute(Sender: TObject);
var
  SaveId : integer;
begin
  Screen.Cursor:=crSQLWait;
  with dmMain do
  try
    SaveId := taProtocol_dID.AsInteger;
    CloseDataSets([taPIt_InSemis_d, taPArt, taProtocol_d]);
    // ������ ����
    ExecSQL('execute procedure G_Norma("'+DateToStr(taPListBD.AsDateTime)+'", "'+DateTimeToStr(taPListDOCDATE.AsDateTime)+'")', quTmp);
    // �������� ���� ��������������
    ExecSQL('execute procedure Create_Protocol_PS('+taPListID.AsString+')', quTmp);
    // �������� ���� �������������� �� ������������
    ExecSQL('execute procedure Create_PArt('+taPListID.AsString+')', quTmp);

    //SaveCurrentLosses;

    taProtocol_d.DisableControls;
    taProtocol_d.AfterScroll := nil;
    taProtocol_d.AfterOpen := nil;
    OpenDataSet(taProtocol_d);
    taProtocol_d.Locate('ID', SaveId, []);
    taProtocol_d.EnableControls;
    taProtocol_d.AfterScroll := ReOpenDetail;
    taProtocol_d.AfterOpen := ReOpenDetail;
    OpenDataSets([taPIt_InSemis_d, taPArt]);
  finally
    Screen.Cursor:=crDefault;
  end;
  MessageDialogA('�������� �� �������������� � ������������ �����������', mtInformation);
end;

procedure TfmProtocol_PS.acAddSemisUpdate(Sender: TObject);
begin
  acAddSemis.Enabled := dmMain.taPIt_InSemis_d.Active;
end;

procedure TfmProtocol_PS.acAddArtExecute(Sender: TObject);
begin
  dmMain.taPArt.Insert;
  ActiveControl := gridAssort;
  gridAssort.SelectedIndex := 0;
end;

procedure TfmProtocol_PS.acAddArtUpdate(Sender: TObject);
begin
  acAddArt.Enabled := dmMain.taPArt.Active;
end;

procedure TfmProtocol_PS.acRefreshProtocolUpdate(Sender: TObject);
begin
  acRefreshProtocol.Enabled := FExistProtocol;
end;

procedure TfmProtocol_PS.acDelProtocolUpdate(Sender: TObject);
begin
  acDelProtocol.Enabled := FExistProtocol;
end;

procedure TfmProtocol_PS.acCloseProtocolUpdate(Sender: TObject);
begin
  acCloseProtocol.Enabled := FExistProtocol;
end;

procedure TfmProtocol_PS.acCreateProtocolUpdate(Sender: TObject);
begin
  acCreateProtocol.Enabled := not FExistProtocol;
end;

procedure TfmProtocol_PS.acDelProtocolExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taPIt_InSemis_d, dmMain.taPArt, dmMain.taProtocol_d]);
  dmMain.taPList.Delete;
  CloseDataSets([dmMain.taPIt_InSemis_d, dmMain.taPArt, dmMain.taProtocol_d, dmMain.taPList]);
  FExistProtocol := False;
end;

procedure TfmProtocol_PS.taPIt_TQBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ID'].AsInteger := dmMain.taProtocol_dID.AsInteger;
end;

procedure TfmProtocol_PS.taPIt_TWBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['ID'].AsInteger := dmMain.taProtocol_dID.AsInteger;
end;

procedure TfmProtocol_PS.acZeroArtExecute(Sender: TObject);
var
  Bookmark : Pointer;
  Msg : string;
begin
  Msg := '�� ������������� ������� �������� ������� "����������� ����������"?';
  if MessageDialog(Msg, mtWarning, [mbYes, mbNo], 0)=mrNo then eXit;
  Application.ProcessMessages;
  Screen.Cursor := crSQLWait;
  with dmMain do
  try
    Bookmark := taPArt.GetBookmark;
    taPArt.DisableControls;
    try
      taPArt.First;
      while not taPArt.Eof do
      begin
        taPArt.Edit;
        taPArtFQ.AsFloat := 0;
        taPArt.Post;
        taPArt.Next;
      end;
    finally
      taPArt.EnableControls;
      taPArt.GotoBookmark(Bookmark);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmProtocol_PS.acCopyArtExecute(Sender: TObject);
var
  Bookmark : Pointer;
  Msg : string;
begin
  Msg := '�� ������������� ������� ����������� ������� "������� ���-��" � "����������� ���-��"?';
  if MessageDialog(Msg, mtWarning, [mbYes, mbNo], 0) = mrNo then eXit;
  Application.ProcessMessages;
  Screen.Cursor := crSQLWait;
  with dmMain do
  try
    Bookmark := taPArt.GetBookmark;
    taPArt.DisableControls;
    try
      taPArt.First;
      while not taPArt.Eof do
      begin
        taPArt.Edit;
        taPArtFQ.AsFloat := taPArtQ.AsFloat;
        taPArt.Post;
        taPArt.Next;
      end;
    finally
      taPArt.EnableControls;
      taPArt.GotoBookmark(Bookmark);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmProtocol_PS.acCopySemisExecute(Sender: TObject);
var
  Bookmark : Pointer;
  Msg : string;
begin
  Msg := '�� ������������� ������� ����������� ������� "������� ���-��" � "����������� ���-��"?';
  if MessageDialog(Msg, mtWarning, [mbYes, mbNo], 0) = mrNo then eXit;
  Application.ProcessMessages;
  Screen.Cursor := crSQLWait;
  with dmMain do
  try
    Bookmark := taPIt_InSemis_d.GetBookmark;
    taPIt_InSemis_d.DisableControls;
    try
      taPIt_InSemis_d.First;
      while not taPIt_InSemis_d.Eof do
      begin
        taPIt_InSemis_d.Edit;
        taPIt_InSemis_dFQ.AsFloat := taPIt_InSemis_dQ.AsFloat;
        taPIt_InSemis_dFW.AsFloat := taPIt_InSemis_dW.AsFloat;
        taPIt_InSemis_d.Post;
        taPIt_InSemis_d.Next;
      end;
    finally
      taPIt_InSemis_d.EnableControls;
      taPIt_InSemis_d.GotoBookmark(Bookmark);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmProtocol_PS.acZeroSemisExecute(Sender: TObject);
var
  Bookmark : Pointer;
  Msg : string;
begin
  Msg := '�� ������������� ������� �������� ������� "����������� ����������"?';
  if MessageDialog(Msg, mtWarning, [mbYes, mbNo], 0)=mrNo then eXit;
  Application.ProcessMessages;
  Screen.Cursor := crSQLWait;
  with dmMain do
  try
    Bookmark := taPIt_InSemis_d.GetBookmark;
    taPIt_InSemis_d.DisableControls;
    try
      taPIt_InSemis_d.First;
      while not taPIt_InSemis_d.Eof do
      begin
        taPIt_InSemis_d.Edit;
        taPIt_InSemis_dFQ.AsInteger := 0;
        taPIt_InSemis_dFW.AsFloat := 0;
        taPIt_InSemis_d.Post;
        taPIt_InSemis_d.Next;
      end;
    finally
      taPIt_InSemis_d.EnableControls;
      taPIt_InSemis_d.GotoBookmark(Bookmark);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmProtocol_PS.acCopySemisUpdate(Sender: TObject);
begin
  acCopySemis.Enabled := not dmMain.taPIt_InSemis_d.IsEmpty;
end;

procedure TfmProtocol_PS.acZeroSemisUpdate(Sender: TObject);
begin
  acZeroSemis.Enabled := not dmMain.taPIt_InSemis_d.IsEmpty;
end;


procedure TfmProtocol_PS.Button1Click(Sender: TObject);
begin
  inherited;
ReportInventory.taSubOper.Active := true;
end;

procedure TfmProtocol_PS.acZeroArtUpdate(Sender: TObject);
begin
  acZeroArt.Enabled := not dmMain.taPArt.IsEmpty;
end;

procedure TfmProtocol_PS.acCopyArtUpdate(Sender: TObject);
begin
  acCopyArt.Enabled := not dmMain.taPArt.IsEmpty;
end;

procedure TfmProtocol_PS.acPrintProtocolExecute(Sender: TObject);
var
  WeightRemainderOut : Currency;
  i : integer;
begin
  WeightRemainderOut := 0;
  Groups.Filter := '[Place$Of$Storage$ID] = ' + dmMain.taProtocol_dPSID.AsString + ' and [Weight$Remainder$Out] > 0';
  Groups.Filtered := true;

  for i := 1 to Groups.RecordCount do
    begin
      WeightRemainderOut := WeightRemainderOut + Groups['Weight$Remainder$Out'];
    end;

  Groups.Filter := '';
  Groups.Filtered := false;

  PostDataSets([dmMain.taPIt_InSemis_d, dmMain.taPArt]);
  ReportInventory := TReportInventory.Create(Self);
  ReportInventory.BD := dmMain.taPlist.FieldByName('BD').AsString;
  ReportInventory.ED := dmMain.taPlist.FieldByName('ED').AsString;
  ReportInventory.WeightOut := CurrToStr(WeightRemainderOut);
  ReportInventory.APlaceOfStorageID := dmMain.taProtocol_dPSID.AsInteger;
  ReportInventory.Prepare;
  ReportInventory.Free;
end;

procedure TfmProtocol_PS.acPrintProtocolUpdate(Sender: TObject);
begin
  acPrintProtocol.Enabled := not dmMain.taProtocol_d.IsEmpty;
end;

procedure TfmProtocol_PS.acAddNewToWOrderExecute(Sender: TObject);
const
  Rec : TRecName = (sId: 'SEMISID'; sName: 'NAME'; sTable: 'D_SEMIS'; sWhere: '');
  RecRej : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_REJ'; sWhere: '');
var
  RejId, RejPsId : Variant;
begin
  if dmMain.taWhSemis.IsEmpty then raise Exception.Create(Format(rsNoData, ['��� �����������']));

  if(ItType = 0)and(dmMain.taWhSemisOPERID.AsString = dm.RejOperId)then
  begin
    (* �������� ������� � ��������� ����� *)
    if ShowDictForm(TfmDepT1, dm.dsrRej, RecRej, '����� ������� � ��������� �����', dmSelRecordReadOnly, nil)<>mrOk then SysUtils.Abort;
    RejId := DictAncestor.vResult;
    RejPsId := dDepT1.RejPsId;
  end
  else begin
    RejId := Null;
    RejPsId := Null;
  end;

  with dmMain do
  begin
    if not _ShowEditorQW(taWhSemisQ.AsInteger, taWhSemisW.AsFloat, chbxW.Checked) then eXit;
    (* C������ ����� ��������� *)
    acAddNew.Execute;
    taWOrderSEMIS.AsString := taWhSemisSEMISID.AsString;
    if(taWhSemisGOOD.AsInteger=0) then
      case ItType of
        0, 1, 3, 5, 6, 7 : taWOrderFROMOPERID.AsVariant := Null;
        else raise EInternal.Create(Format(rsInternalError, ['101']));
      end
    else
      case ItType of
        0, 1, 3, 5, 6, 7 : taWOrderFROMOPERID.AsVariant := taWhSemisOPERID.AsVariant;
        else raise EInternal.Create(Format(rsInternalError, ['101']));
      end;
    taWOrderQ.Asinteger := SemisQ;
    taWOrderW.AsFloat := SemisW;
    taWOrderREJID.AsVariant := RejId;
    taWOrderREJPSID.AsVariant := RejPsId;
    taWOrderPROTOCOLID.AsInteger := taProtocol_dACTID.AsInteger;
    taWOrder.Post;
    taWhSemis.Refresh;
  end;
  ReOpenSemis;
end;

procedure TfmProtocol_PS.it0Click(Sender: TObject);
begin
  ItType := 0;
end;

procedure TfmProtocol_PS.it7Click(Sender: TObject);
begin
  ItType := 3;
end;

procedure TfmProtocol_PS.SetItType(const Value: byte);
begin
  if FItType= Value then eXit;
  FItType := Value;
  ReOpenDataSet(dmMain.taWhSemis);
  case FItType of
    0 : it0.Checked := True;
    3 : it3.Checked := True;
  end;
end;

procedure TfmProtocol_PS.acPrintInvExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taWOrder,  dmMain.taWOrderList]);
  if(dmMain.taWOrderITTYPE.AsInteger in [0,6])then
    dmPrint.PrintDocumentA(dkInv16In, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]))
  else
    dmPrint.PrintDocumentA(dkInv16Out, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]));
end;

procedure TfmProtocol_PS.acPrintInvWithAssortExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taWOrder, dmMain.taWOrderList]);
  if(dmMain.taWOrderITTYPE.AsInteger in [0,6])then
    dmPrint.PrintDocumentA(dkInv16InAssort, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]))
  else
    dmPrint.PrintDocumentA(dkInv16OutAssort, VarArrayOf([VarArrayOf([dmMain.taWOrderINVID.AsInteger])]));
end;

procedure TfmProtocol_PS.acAssortimentExecute(Sender: TObject);
var
  Position : integer;
  Id : Variant;
begin
  // ��������� ����� �������
  if (dm.User.AccProd and $20)<>$20 then raise Exception.Create('� ��� ��� ���� ������� � ������������!');
  if (dmMain.taWOrderDEPID.AsInteger <> dm.User.wWhId) then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taWOrderDEPNAME.AsString]));
  // ������ �� �����
  if(dmMain.taWOrderListISCLOSE.AsInteger =1)then raise Exception.Create(Format(rsWorderClose, [dmMain.taWOrderListDOCNO.AsString]));
  dmAppl.IsWorkWithStone := False;
  with dmMain do
  begin
    PostDataSet(taWOrder);
    dmAppl.ADistrType := adtWOrder;
    Id := ExecSelectSQL('select InvId from Inv where InvId='+IntToStr(taWOrderAINV.AsInteger), dmMain.quTmp);
    (* ������� �� ���������? *)
    if VarisNull(Id)or(Id=0)then
    begin
      taWOrder.Edit;
      taWOrderAINV.AsVariant := Null;
      taWOrder.Post;
      taWOrder.Transaction.CommitRetaining;
    end;
    if taWOrderAINV.IsNull then
    begin
      if dmAppl.trAppl.Active then dmAppl.trAppl.Commit;
      dmAppl.trAppl.StartTransaction;
      OpenDataSets([dmAppl.taAInv]);
      dmAppl.taAInv.Append;
      taWOrder.Edit;
      taWOrderAINV.AsInteger := dmAppl.taAInvINVID.AsInteger;
      dmAppl.taAInv.Post;
      taWOrder.Post;
      dmAppl.taAInv.Transaction.CommitRetaining;
      taWOrder.Transaction.CommitRetaining;
    end;
    CloseDataSets([dmAppl.taAInv, dmAppl.taAEl]);
    if taWOrderITTYPE.AsInteger in [0,6] then dmAppl.ADistrKind := 0 else dmAppl.ADistrKind := 1;
    ShowAndFreeForm(TfmADistr, TForm(fmADistr));
    dm.WorkMode := 'wOrder';
    (* �������� ��������� ������� ��������� *)
    PostDataSet(taWOrder);
    if dmAppl.IsWorkWithStone then
    begin
      Position := taWOrderWOITEMID.AsInteger;
      ReOpenDataSet(taWOrder);
      taWOrder.Locate('WOITEMID', VarArrayOf([Position]), []);
    end
    else begin
      if not (taWOrderAPPLINV.IsNull and taWOrderAINV.IsNull) then ReOpenDataSets([taAssortInv, taAssortEl])
      else CloseDataSets([taAssortInv, taAssortEl]);
      RefreshDataSet(taWOrder);
    end;
    gridItem.SelectedField := taWOrderQ;
    ReOpenArt;
  end;
end;

procedure TfmProtocol_PS.acAssortimentUpdate(Sender: TObject);
begin
  acAssortiment.Enabled := (dmMain.taWOrder.Active)and(not dmMain.taWOrder.IsEmpty); 
end;

procedure TfmProtocol_PS.cmbxMatChange(Sender: TObject);
begin
  dm.AMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSets([dmMain.taWhSemis]);
end;

procedure TfmProtocol_PS.cmbxOperChange(Sender: TObject);
begin
  with cmbxOper, Items do
    if(Items[ItemIndex] = sNoOperation) then dmMain.FilterOperId := sNoOperation
    else dmMain.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taWHSemis);
end;

procedure TfmProtocol_PS.gridWhSemisKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  case Key of
    VK_SPACE : begin
       acAddSemis.Execute;
    end;
  end;
end;

procedure TfmProtocol_PS.gridItemExit(Sender: TObject);
begin
  PostDataSet(gridItem.DataSource.DataSet);
end;

procedure TfmProtocol_PS.gridItemGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
var
  i : integer;
begin
  case dmMain.taWOrderITTYPE.AsInteger of
    0,6 : if (dmMain.taWOrderE.AsInteger = -1) then BackGround := clRed
          else BackGround := clInfoBk;
    1,3,4,5,7 : if (dmMain.taWOrderE.AsInteger = -1) then BackGround := clGreen
                else Background := cl3DLight;
  end;
  if(not dmMain.taWOrderREF.IsNull) then Background := clMoneygreen;

  if(AnsiUpperCase(Column.FieldName)='RECNO')then
    if gridItem.SelectedRows.Find(Column.Field.DataSet.Bookmark, i) then Background:= clCream else Background:= clMoneygreen
  else if(Column.FieldName='ITDATE')and(Column.Field.AsDateTime>dm.EndDate) then BackGround := clRed
  else if(Column.FieldName='ITTYPE')then
    case dmMain.taWOrderITTYPE.AsInteger of
      0,6 : BackGround := clInfoBk;
      1,3,4,5,7 : Background := cl3DLight;
    end;
end;

procedure TfmProtocol_PS.gridItemKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_INSERT : SysUtils.Abort;
  end;
end;

procedure TfmProtocol_PS.gridItemOrderBy(Sender: TObject; OrderBy: String);
begin
  OrderBy := StringReplace(OrderBy, 'FROMOPERNAME', 'FROMOPERID', [rfReplaceAll, rfIgnoreCase]);
  OrderBy := StringReplace(OrderBy, 'OPERNAME', 'OPERID', [rfReplaceAll, rfIgnoreCase]);
  OrderBy := StringReplace(OrderBy, 'SEMISNAME', 'SEMIS', [rfReplaceAll, rfIgnoreCase]);
  OrderBy := StringReplace(OrderBy, 'USERNAME', 'USERID', [rfReplaceAll, rfIgnoreCase]);
  OrderBy := StringReplace(OrderBy, 'JOBDEPNAME', 'JOBDEPID', [rfReplaceAll, rfIgnoreCase]);
  OrderBy := StringReplace(OrderBy, 'JOBNAME', 'JOBID', [rfReplaceAll, rfIgnoreCase]);
  OrderBy := StringReplace(OrderBy, 'DEPNAME', 'DEPID', [rfReplaceAll, rfIgnoreCase]);
  dmMain.taWorder.SelectSQL.Text := copy(dmMain.taWorder.SelectSQL.Text, 1,
    Pos('ORDER BY', dmMain.taWorder.SelectSQL.Text)-1)+OrderBy;
end;

procedure TfmProtocol_PS.acAddToWOrderExecute(Sender: TObject);
const
  t_1 : set of byte = [0, 6];
  t_2 : set of byte = [1, 3, 5];
  t_3 : set of byte = [7];
  Rec : TRecName = (sId: 'SEMISID'; sName: 'NAME'; sTable: 'D_SEMIS'; sWhere: '');
  RecRej : TRecName = (sId: 'ID'; sName: 'NAME'; sTable: 'D_REJ'; sWhere: '');
var
  t : Variant;
  RejId, RejPsId : Variant;
begin
  if dmMain.taWhSemis.IsEmpty then raise Exception.Create(Format(rsNoData, ['��� �����������']));
  (* ��������� ������ � ��������� ����� *)
  if(ItType = 0)and(dmMain.taWhSemisOPERID.AsString = dm.RejOperId)then
  begin
    (* �������� ������� � ��������� ����� *)
    if ShowDictForm(TfmDepT1, dm.dsrRej, RecRej, '����� ������� � ��������� �����', dmSelRecordReadOnly, nil)<>mrOk then SysUtils.Abort;
    RejId := DictAncestor.vResult;
    RejPsId := dDepT1.RejPsId;
  end
  else begin
    RejId := Null;
    RejPsId := Null;
  end;

  with dmMain do
  begin
    if not _ShowEditorQW(dmMain.taWhSemisQ.AsInteger, dmMain.taWhSemisW.AsFloat, chbxW.Checked) then eXit;
    case dmMain.WorderMode of
      wmWOrder : begin
        (* ���� ��������� ��������� �� ��������� �� ���� � ����� ������ �� ������
           ����� ��������� ����� ���������, ����� ��������� � ������ *)
        if(CurrInv = -1)then t := ItType
        else
          t := ExecSelectSQL('select max(ItType) from woItem where InvId='+
               IntToStr(dmMain.CurrInv), dmMain.quTmp);
        case t of
          0, 6 : if(ItType in [0, 6])then acAddItem.Execute else acAddNew.Execute;
          1, 3, 5 : if(ItType in [1, 3, 5])then acAddItem.Execute else acAddNew.Execute;
          7 : if(ItType in [7])then acAddItem.Execute else acAddNew.Execute;
          else raise EInternal.Create(Format(rsInternalError, ['101']));
        end;
        taWOrderSEMIS.AsString := taWhSemisSEMISID.AsString;
        if(taWhSemisGOOD.AsInteger = 0) then // ������������ �� �������� �������
        begin
          if(taWhSemisOPERID.AsString = dm.RejOperId)or
            (taWhSemisOPERID.AsString = dm.NkOperId)or
            (taWhSemisOPERID.AsString = dm.TransformOperId)
          then
            case ItType of
               0, 1, 2, 3, 5, 6, 7 : taWOrderFROMOPERID.AsVariant := taWhSemisOPERID.AsVariant;
               else raise EInternal.Create(Format(rsInternalError, ['101']));
            end
          else
            case ItType of
               0, 1, 3, 5, 6, 7 : taWOrderFROMOPERID.AsVariant := Null;
               { TODO : into new version }
               //5, 7 : taWOrderFROMOPERID.AsVariant := taWhSemisOPERID.AsVariant;
               else raise EInternal.Create(Format(rsInternalError, ['101']));
            end
          end
        else
          case ItType of
             0, 1, 3, 5, 6, 7 : taWOrderFROMOPERID.AsVariant := taWhSemisOPERID.AsVariant;
             else raise EInternal.Create(Format(rsInternalError, ['101']));
          end;
        taWOrderQ.AsInteger := SemisQ;
        taWOrderW.AsFloat := SemisW;
        taWOrderREJID.AsVariant := RejId;
        taWOrderREJPSID.AsVariant := RejPsId;
        taWOrderPROTOCOLID.AsInteger := taProtocol_dACTID.AsInteger;
        taWOrder.Post;
        taWhSemis.Refresh;
      end;
    end;
  end;
  ReOpenSemis;
end;

procedure TfmProtocol_PS.acDelFromWOrderExecute(Sender: TObject);
var
  b : boolean;
  t : byte;
  SemisId, OperId : string;
  DepId, Id : integer;
  Ref : Variant;
begin
  (* ����� �� �������� ������������� *)
  if(dmMain.taWOrderDEPID.AsInteger <> dm.User.wWhId) then raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taWOrderDEPNAME.AsString]));
  if dmMain.taWOrder.IsEmpty then raise EWarning.Create(Format(rsNoData, ['��� ��������']));
  with dmMain do
  begin
    case WOrderMode of
      wmWOrder : begin
        t := taWOrderITTYPE.AsInteger;
        if t <> ItType then ItType := t;
        if taWOrderFROMOPERID.IsNull then b := taWhSemis.Locate('SEMISID', taWOrderSEMIS.AsString, [])
        else b := taWhSemis.Locate('SEMISID;OPERID', VarArrayOf([taWOrderSEMIS.AsString, taWOrderFROMOPERID.AsString]), []);
        if not b then
        begin
          SemisId := taWOrderSEMIS.AsString;
          OperId := taWOrderFROMOPERID.AsString;
          case ItType of
            0, 6    : DepId := taWOrderDEPID.AsInteger;
            1,3,5,7 : DepId := taWOrderJOBDEPID.AsInteger;
            else raise EInternal.Create(Format(rsInternalError, ['101']));
          end;
        end;
        dmMain.SemisQ := dmMain.taWOrderQ.AsInteger;
        dmMain.SemisW := dmMain.taWOrderW.AsFloat;
        if(ShowEditor(TfmeSQ, TfmEditor(fmeSQ))<>mrOk)then eXit;
        if(dmMain.SemisQ = dmMain.taWOrderQ.AsInteger) and
          (Abs(dmMain.SemisW - dmMain.taWOrderW.AsFloat)<Eps)then
        begin
          //taWOrder.BeforeDelete := nil;
          taWOrder.Delete;
          //taWOrder.BeforeDelete := taWOrderBeforeDelete;
        end
        else begin
          taWOrder.Edit;
          taWOrderQ.AsInteger := taWOrderQ.AsInteger - dmMain.SemisQ;
          taWOrderW.AsFloat := taWOrderW.AsFloat - dmMain.SemisW;
          taWOrder.Post;
        end;
        if not b then
        begin
          taWhSemis.Append;
          taWhSemisDEPID.AsInteger := DepId;
          taWhSemisOPERID.AsString := OperId;
          taWhSemisSEMISID.AsString := SemisId;
          taWhSemisQ.AsInteger := 0;
          taWhSemisW.AsInteger := 0;
          taWhSemis.Post;
        end;
        taWhSemis.Refresh;
      end;
      wmRej : begin
        // �������� ���������� ������� ������, � ����� �������, ������ ��� �
        // ������ ����� ���� �������� �����������
        case taWOrderITTYPE.AsInteger of
          0 : begin
            Id  := taWOrderWOITEMID.AsInteger;
            if taWOrderREJREF.IsNull then raise EInternal.Create(Format(rsInternalError, ['142']));
            Ref := taWOrderREJREF.AsInteger;
            if not taWOrder.Locate('WOITEMID', taWOrderREJREF.AsInteger, []) then raise EInternal.Create(Format(rsInternalError, ['143']));
          end;
          3 : begin
            Ref := ExecSelectSQL('select WOItemId from WOItem where RejRef = '+taWOrderWOITEMID.AsString, quTmp);
            if VarIsNull(Ref)or(Ref = 0)then raise EInternal.Create(Format(rsInternalError, ['144']));
            if not taWOrder.Locate('WOITEMID', Ref, [])then raise EInternal.Create(Format(rsInternalError, ['146']));
            Id := taWOrderWOITEMID.AsInteger;
          end;
        end;

        // ������� �� ������
        taWOrder.Locate('WOITEMID', Id, []);
        taWOrder.Delete;
        taWOrder.Locate('WOITEMID', Ref, []);
        if taWOrderFROMOPERID.IsNull then b := taWhSemis.Locate('SEMISID', taWOrderSEMIS.AsString, [])
        else b := taWhSemis.Locate('SEMISID;OPERID', VarArrayOf([taWOrderSEMIS.AsString, taWOrderFROMOPERID.AsString]), []);
        if not b then
        begin
          SemisId := taWOrderSEMIS.AsString;
          OperId := taWOrderFROMOPERID.AsString;
          case ItType of
             0, 6    : DepId := taWOrderDEPID.AsInteger;
             1,3,5,7 : DepId := taWOrderJOBDEPID.AsInteger;
             else raise EInternal.Create(Format(rsInternalError, ['101']));
           end;
        end;
        dmMain.SemisQ := dmMain.taWOrderQ.AsInteger;
        dmMain.SemisW := dmMain.taWOrderW.AsFloat;
        taWOrder.Delete;
        if not b then
        begin
          taWhSemis.Append;
          taWhSemisDEPID.AsInteger := DepId;
          taWhSemisOPERID.AsString := OperId;
          taWhSemisSEMISID.AsString := SemisId;
          taWhSemisQ.AsInteger := 0;
          taWhSemisW.AsInteger := 0;
          taWhSemis.Post;
        end;
        taWhSemis.Refresh;
      end;
    end; // case
  end; // with dmMain
  ReOpenSemis;
end;

procedure TfmProtocol_PS.Access;
begin
  with stbrStatus.Panels[0] do
    if dm.IsAdm then Text := 'Adm'
    else if((dm.User.AccProd_d and 1) = 1)then Text := 'View'
    else Text := 'Full';
  if dm.IsAdm then eXit;
  if (dm.User.AccProd_d and 1)=1 then SetReadOnly(True)
  else begin // ��������� �����, ������������ ��� ��������� �� ��������
    if(dm.User.wWhId = -1)then
    begin
      ShowMessage('�� �� ���������� �� �� ����� �� ��������.'+#13#10+'�������� ����� ���������');
      SetReadOnly(True);
    end
    else SetReadOnly(False);
  end;
end;

procedure TfmProtocol_PS.acCloseWOrderExecute(Sender: TObject);
begin
  if(acCloseWOrder.Caption = '������� �����')then
    try
      ExecSQL('execute procedure OpenDoc("�����", '+dmMain.taWOrderListWORDERID.AsString+')', dmMain.quTmp);
      acCloseWOrder.Caption := '������� �����';
      acCloseWOrder.ImageIndex := 5;
      RefreshDataSet(dmMain.taWOrderList);
    except
      on E: Exception do
        { TODO : !!! }
        //if not TranslateIbMsg(E, dm.db, dm.tr) then ShowMessage(E.Message);
    end
  else
    try
      ExecSQL('execute procedure CloseDoc("�����", '+dmMain.taWOrderListWORDERID.AsString+')', dmMain.quTmp);
      acCloseWOrder.Caption := '������� �����';
      acCloseWOrder.ImageIndex := 4;
      RefreshDataSet(dmMain.taWOrderList);
    except
      on E: Exception do
        { TODO : !!! }
        //if not TranslateIbMsg(E, dm.db, dm.tr) then ShowMessage(E.Message);
    end;
end;

{procedure TfmProtocol_PS.InitBtn(T : boolean);
begin
  if not T then // �������� ������
  begin
    acCloseWOrder.Caption := '������� �����';
    acCloseWOrder.ImageIndex := 5;
  end
  else begin
    acCloseWOrder.Caption := '������� �����';
    acCloseWOrder.ImageIndex := 4;
  end
end;}

procedure TfmProtocol_PS.SetCurrInv;
var
  Id : Variant;
begin
  with dmMain do
  begin
    Id := ExecSelectSQL('select InvId from GetCurrInv('+
      IntToStr(taWOrderListWORDERID.AsInteger)+','+IntToStr(dm.User.wWhId)+')', quTmp);
    if VarIsNull(Id)or(Id = 0)then CurrInv := -1
    else CurrInv := Id;
  end;
end;

procedure TfmProtocol_PS.acOpenDetailExecute(Sender: TObject);
begin
  with dmMain do
  begin
    // ��������� ������ �� �������� �������������� � ������������
    ReOpenDataSets([taPIt_InSemis_d, taPArt]);
    // �������������� ������ ��� ������ � �������
    Access;
    dm.CurrDep := -1;
    dm.ShowKindDoc := -1;
    taWOrderList.DisableControls;
    OpenDataSet(taWOrderList);
    if not taWOrderList.Locate('JOBDEPID;DEFOPER;PROTOCOLID',
         VarArrayOf([taProtocol_dPSID.AsInteger, taProtocol_dOPERID.AsString, taProtocol_dACTID.AsInteger]), [])
    then begin
      FwOrderId := -1;
      CloseDataSet(taWOrderList);
    end
    else begin
      FwOrderId := taWOrderListWORDERID.AsInteger;
      OpenWorder;
    end;
    taWOrderList.EnableControls;
  end;
end;

{procedure TfmProtocol_PS.SetOPeriod;
var
  dt : Variant;
begin
  dm.PsBeginDate := StrToDate('01.01.2050');
  if(not dmMain.taWOrderListJOBDEPID.IsNull) then
  begin
    // ���������� ���� ��������� ��� ����� ��������
    dt := ExecSelectSQL('select max(a.DocDate) from Act a, AItem it '+
                        'where a.Id=it.ActId and '+
                        '      a.T in (0,1) and '+
                        '      a.IsClose=1 and '+
                        '      it.PsId = '+dmMain.taWOrderListJOBDEPID.AsString, dmMain.quTmp);
    if VarIsNull(dt)then dm.PsBeginDate := StrToDate(BeginDateByDefault) else dm.PsBeginDate := VarToDateTime(dt);
  end;

  if(dm.User.wWhId <> -1) then
  begin
    dt := ExecSelectSQL('select max(a.DocDate) from Act a, AItem it '+
                        'where a.Id=it.ActId and '+
                        '      a.T = 2 and '+
                        '      a.IsClose=1 and '+
                        '      a.DepId = '+IntToStr(dm.User.wWhId), dmMain.quTmp);
    if not VarIsNull(dt)then
    begin
      if dm.PsBeginDate < VarToDateTime(dt) then
      begin
        dm.PsBeginDate := VarToDateTime(dt);
        dm.PsTypeBeginDate := 1;
      end
      else dm.PsTypeBeginDate := 0;
    end
    else dm.PsTypeBeginDate := 0;
  end;
end;}

procedure TfmProtocol_PS.acAddNewExecute(Sender: TObject);
begin
  dmMain.InvMode := 0;
  dmMain.taWOrder.Append;
  dmMain.taWOrderITTYPE.AsInteger := ItType;
  gridItem.SelectedField := dmMain.taWorderSEMISNAME;
end;

procedure TfmProtocol_PS.acAddItemExecute(Sender: TObject);
begin
  dmMain.InvMode := 1;
  dmMain.taWOrder.Append;
  dmMain.taWOrderITTYPE.AsInteger := ItType;
  gridItem.SelectedField := dmMain.taWorderSEMISNAME;
end;

procedure TfmProtocol_PS.SetReadOnly(ReadOnly: boolean);
begin
  acAddNewToWOrder.Enabled := not ReadOnly;
  acAddToWOrder.Enabled := not ReadOnly;
  acDelFromWOrder.Enabled := not ReadOnly;
  acAddItem.Enabled := not ReadOnly;
  acAddNew.Enabled := not ReadOnly;
  gridItem.ReadOnly := ReadOnly;
  acCloseWOrder.Enabled := not ReadOnly;
  tbdcSemis.Visible := not ReadOnly;
  tlbrT.Visible := not ReadOnly;
end;

procedure TfmProtocol_PS.OpenWorder;
begin
  cmbxMat.Enabled := True;
  cmbxOper.Enabled := True;
  OpenDataSets([dmMain.taWOrder, dmMain.taWhSemis]);
  SetCurrInv;
end;

procedure TfmProtocol_PS.acAddNewToWOrderUpdate(Sender: TObject);
begin
  acAddNewToWOrder.Enabled := FAccess and (FwOrderId <> -1) and
                              (not dmMain.taWhSemis.IsEmpty)and
                              (dmMain.taWOrderListISCLOSE.AsInteger = 0);
end;

procedure TfmProtocol_PS.acAddToWOrderUpdate(Sender: TObject);
begin
  acAddToWOrder.Enabled := FAccess and (FwOrderId <> -1) and
                              (not dmMain.taWhSemis.IsEmpty)and
                              (dmMain.taWOrderListISCLOSE.AsInteger = 0);
end;

procedure TfmProtocol_PS.acDelFromWOrderUpdate(Sender: TObject);
begin
  acDelFromWOrder.Enabled := FAccess and (FwOrderId <> -1) and
                              (not dmMain.taWOrder.IsEmpty) and
                              (dmMain.taWOrderListISCLOSE.AsInteger = 0);
end;

procedure TfmProtocol_PS.acCloseWOrderUpdate(Sender: TObject);
begin
  acCloseWOrder.Enabled := FwOrderId <> -1;
end;

procedure TfmProtocol_PS.acCreateWOrderExecute(Sender: TObject);
begin
  with dmMain do
  begin
    FWOrderId := -1;
    dm.CurrDep := -1;
    dm.ShowKindDoc := -1;
    taWOrderList.DisableControls;
    try
      OpenDataSet(taWOrderList);
      taWOrderList.Insert;
      FwOrderId := taWOrderListWORDERID.AsInteger;
      taWOrderListDEPID.AsInteger := dm.User.DepId;
      taWOrderListMOLID.AsInteger := dm.User.UserId;
      taWOrderListJOBDEPID.AsInteger := taProtocol_dPSID.AsInteger;
      taWOrderListJOBID.AsInteger := ExecSelectSQL('select max(Molid) from D_Ps where DepId='+
        taProtocol_dPSID.AsString, quTmp);
      taWOrderListDEFOPER.AsString := taProtocol_dOPERID.AsString;
      taWOrderListISCLOSE.AsInteger := 0;
      taWOrderListPROTOCOLID.AsInteger := taProtocol_dACTID.AsInteger;
      taWOrderList.Post;
      OpenWOrder;
    finally
      taWOrderList.EnableControls;
    end;
  end;
end;

procedure TfmProtocol_PS.CloseWOrder;
begin
  PostDataSets([dmMain.taWOrder, dmMain.taWOrderList, dmMain.taWhSemis]);
  CloseDataSets([dmMain.taWOrder, dmMain.taWOrderList, dmMain.taWhSemis]);
  FwOrderId := -1;
end;

procedure TfmProtocol_PS.acOpenDetailUpdate(Sender: TObject);
begin
  acOpenDetail.Enabled := not dmMain.taProtocol_d.IsEmpty; 
end;

procedure TfmProtocol_PS.acCreateWOrderUpdate(Sender: TObject);
var
  b : boolean;
begin
  if dmMain.taWOrderList.Active then b := FwOrderId = -1
  else if dmMain.taPIt_InSemis_d.Active then b := True
  else b := False;
  acCreateWOrder.Enabled := b;
end;

procedure TfmProtocol_PS.acItType0Execute(Sender: TObject);
begin
  ItType := 0;
end;

procedure TfmProtocol_PS.acItType3Execute(Sender: TObject);
begin
  ItType := 3;
end;

procedure TfmProtocol_PS.acItType0Update(Sender: TObject);
begin
  acItType0.Enabled := dmMain.taWhSemis.Active;
end;

procedure TfmProtocol_PS.acItType3Update(Sender: TObject);
begin
  acItType3.Enabled := dmMain.taWhSemis.Active;
end;

procedure TfmProtocol_PS.ReOpenSemis;
var
  Id : Variant;
begin
  Id := dmMain.taPIt_InSemis_dID.AsInteger;
  ReOpenDataSet(dmMain.taPIt_InSemis_d);
  dmMain.taPIt_InSemis_d.Locate('ID', Id, []);
end;

procedure TfmProtocol_PS.ReOpenArt;
var
  Id : Variant;
begin
  Id := dmMain.taPArtID.AsVariant;
  ReOpenDataSet(dmMain.taPArt);
  dmMain.taPArt.Locate('ID', Id, []);
end;

procedure TfmProtocol_PS.acPrintInvUpdate(Sender: TObject);
begin
  acPrintInv.Enabled := dmMain.taWOrder.Active and (not dmMain.taWOrder.IsEmpty);
end;

procedure TfmProtocol_PS.acPrintInvWithAssortUpdate(Sender: TObject);
begin
  acPrintInvWithAssort.Enabled := dmMain.taWOrder.Active and (not dmMain.taWOrder.IsEmpty);
end;

procedure TfmProtocol_PS.acACopyExecute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  with dmMain do
  begin
    if not taAssortEl.Active then eXit;
    ExecSQL('delete from B_AInv where UserId='+IntToStr(dm.User.UserId), quTmp);
    BookMark := taAssortEl.GetBookmark;
    try
      taAssortEl.First;
      while not taAssortEl.Eof do
      begin
        ExecSQL('insert into B_AInv(ID, USERID, ARTID, ART2ID, SZID, '+
           'OPERID, U, Q) values('+IntToStr(dm.GetId(33))+','+IntToStr(dm.User.UserId)+','+
           taAssortElARTID.AsString+','+taAssortElART2ID.AsString+','+
           taAssortElSZID.AsString+',"'+taAssortElOPERID.AsString+'",'+
           taAssortElU.AsString+','+taAssortElQ.AsString+')', dmMain.quTmp);
        taAssortEl.Next;
      end;
      taAssortEl.Transaction.CommitRetaining;
    finally
      taAssortEl.GotoBookmark(BookMark);
    end;
  end
end;

procedure TfmProtocol_PS.acAPasteExecute(Sender: TObject);
var
  DocNo : string;
  Q : integer;
begin
  if (dmMain.taWOrderDEPID.AsInteger <> dm.User.wWhId) then
    raise Exception.Create(Format(rsAccessDenied, [dm.User.FIO, '� '+dmMain.taWOrderDEPNAME.AsString]));
  with dmMain do
  begin
    if taWOrder.IsEmpty then eXit;
    if (dmMain.taWOrderAINV.IsNull)or(dmMain.taWOrderAINV.AsInteger = 0) then
    begin
      if MessageDialog('������� ����� ���������?', mtConfirmation, [mbOk, mbCancel], 0)<>mrOk then eXit;
      dmAppl.AInvId := -1;
      OpenDataSet(dmAppl.taAInv);
      dmAppl.taAInv.Append;
      taWOrder.Edit;
      taWOrderAINV.AsInteger := dmAppl.taAInvINVID.AsInteger;
      dmAppl.taAInv.Post;
      taWOrder.Post;
      CloseDataSet(dmAppl.taAInv);
    end
    else begin
      DocNo := ExecSelectSQL('select DOCNO from Inv where InvId='+taWOrderAINV.AsString, quTmp);
      if (DocNo = '0')then raise EInternal.Create(Format(rsInternalError, ['113']));
      if MessageDialog('�������� � ��������� �'+DocNo, mtConfirmation, [mbOk, mbCancel], 0)<>mrOk then eXit;
    end;
    // ���������� � ���������
    ExecSQL('execute procedure Paste_A('+IntToStr(dm.User.UserId)+', '+taWOrderWOITEMID.AsString+')', quTmp);
    dm.tr.CommitRetaining;
    ReOpenDataSets([taAssortInv, taAssortEl]);
    // �������� ���-�� � ������ ��������� ������
    Q := 0;
    while not taAssortEl.Eof do
    begin
      Q := Q + taAssortElQ.AsInteger;
      taAssortEl.Next;
    end;
    taWOrder.Edit;
    taWOrderQ.AsInteger := Q;
    taWOrder.Post;
  end;
end;

procedure TfmProtocol_PS.acACopyUpdate(Sender: TObject);
begin
  acACopy.Enabled := not dmMain.taAssortEl.IsEmpty;
end;

procedure TfmProtocol_PS.acAPasteUpdate(Sender: TObject);
begin
  acAPaste.Enabled := (ExecSelectSQL('select count(*) from B_AInv where UserId='+
    IntToStr(dm.User.UserId), dmMain.quTmp)<>0) and dmMain.taWorder.Active and
    (not dmMain.taWorder.IsEmpty); 
end;

procedure TfmProtocol_PS.gridItemColumns7EditButtonClick(Sender: TObject; var Handled: Boolean);
begin
  acAssortiment.Execute;
end;

procedure TfmProtocol_PS.acPrintResultExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taPIt_InSemis_d, dmMain.taPArt]);
  dmPrint.PrintDocumentA(dkProtocol_PS_Balance, VarArrayOf([VarArrayOf([dmMain.taPListID.AsInteger,
     dmMain.taProtocol_dPSID.AsInteger])]));
end;

end.
