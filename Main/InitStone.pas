unit InitStone;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList,  ImgList,  Grids,
  DBGridEh, Buttons, ExtCtrls, ComCtrls, StdCtrls, Mask, DBCtrlsEh,
  TB2Item, TB2Dock, TB2Toolbar, DBLookupEh, DB, pFIBDataSet, FIBDataSet,
  DBGridEhGrouping, rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmInitStone = class(TfmDictAncestor)
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem3: TTBControlItem;
    Label2: TLabel;
    taADate: TpFIBDataSet;
    dsrADate: TDataSource;
    lcbxADate: TDBLookupComboboxEh;
    TBControlItem1: TTBControlItem;
    taADateADATE: TDateTimeField;
    edDate: TDBDateTimeEditEh;
    TBControlItem2: TTBControlItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edDateCloseUp(Sender: TObject; Accept: Boolean);
    procedure lcbxADateChange(Sender: TObject);
  end;

var
  fmInitStone: TfmInitStone;

implementation

uses DictData, dbUtil, MsgDialog;

{$R *.dfm}

procedure TfmInitStone.FormCreate(Sender: TObject);
begin
  OpenDataSet(taADate);
  if not taADate.IsEmpty then lcbxADate.Value := taADateADATE.AsDateTime
  else lcbxADate.Value := 0;
  inherited;
end;

procedure TfmInitStone.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    PostDataSet(taADate);
  except
    on E: Exception do begin
      MessageDialogA(E.Message, mtError);
      SysUtils.Abort;
    end
  end;
  lcbxADate.OnChange := nil;
  CloseDataSet(taADate);
  inherited;
end;

procedure TfmInitStone.edDateCloseUp(Sender: TObject; Accept: Boolean);
begin
  taADate.Insert;
  taADateADATE.AsDateTime := VarToDateTime(edDate.Value);
  taADate.Post;
  lcbxADate.Value := taADateADATE.AsDateTime;
end;

procedure TfmInitStone.lcbxADateChange(Sender: TObject);
begin
  try
    CloseDataSet(DataSet);
    if VarIsNull(lcbxADate.Value)then eXit;
    dm.InitStone_ADate := VarToDateTime(lcbxADate.Value);
    OpenDataSet(DataSet);
  except
  end
end;

end.
