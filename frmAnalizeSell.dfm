object DialogAnalizeSell: TDialogAnalizeSell
  Left = 181
  Top = 141
  Caption = #1040#1085#1072#1083#1080#1079' '#1087#1088#1086#1076#1072#1078
  ClientHeight = 626
  ClientWidth = 778
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PanelLeft: TPanel
    Left = 0
    Top = 0
    Width = 265
    Height = 600
    Align = alLeft
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 0
    DesignSize = (
      265
      600)
    object ListOrganization: TcxCheckListBox
      Left = 8
      Top = 104
      Width = 249
      Height = 89
      Items = <>
      Style.BorderStyle = cbsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = True
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 0
    end
    object ListCompany: TcxCheckListBox
      Left = 8
      Top = 224
      Width = 249
      Height = 329
      Anchors = [akLeft, akTop, akBottom]
      Items = <>
      Style.BorderStyle = cbsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.NativeStyle = True
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.NativeStyle = True
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.NativeStyle = True
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.NativeStyle = True
      TabOrder = 1
    end
    object cxLabel3: TcxLabel
      Left = 8
      Top = 88
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1080
    end
    object cxLabel4: TcxLabel
      Left = 8
      Top = 200
      Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090#1099
    end
    object PanelLeftBottom: TPanel
      Left = 1
      Top = 558
      Width = 263
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 4
      object ButtonExecute: TcxButton
        Left = 176
        Top = 8
        Width = 75
        Height = 25
        Caption = #1042#1099#1087#1086#1083#1085#1080#1090#1100
        TabOrder = 0
        OnClick = ButtonExecuteClick
        LookAndFeel.Kind = lfOffice11
      end
    end
    object cxGroupBox1: TcxGroupBox
      Left = 8
      Top = 8
      Caption = #1055#1077#1088#1080#1086#1076
      Style.LookAndFeel.Kind = lfOffice11
      Style.Shadow = False
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      TabOrder = 5
      Height = 73
      Width = 249
      object cxLabel1: TcxLabel
        Left = 8
        Top = 24
        Caption = #1053#1072#1095#1072#1083#1086
      end
      object DateEditPeriodBegin: TcxDateEdit
        Left = 8
        Top = 40
        TabOrder = 1
        Width = 113
      end
      object cxLabel5: TcxLabel
        Left = 129
        Top = 24
        Caption = #1050#1086#1085#1077#1094
      end
      object DateEditPeriodEnd: TcxDateEdit
        Left = 128
        Top = 40
        TabOrder = 3
        Width = 113
      end
    end
  end
  object PanelRight: TPanel
    Left = 267
    Top = 0
    Width = 511
    Height = 600
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 1
    DesignSize = (
      511
      600)
    object Grid: TcxGrid
      Left = 8
      Top = 8
      Width = 496
      Height = 543
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      LookAndFeel.Kind = lfOffice11
      object GridDBTableView: TcxGridDBTableView
        NavigatorButtons.ConfirmDelete = False
        DataController.DataSource = DataSource
        DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Format = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' = 0'
            Kind = skCount
            Column = GridDBTableViewUID
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skCount
            Column = GridDBTableViewUID
          end>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnFiltering = False
        OptionsCustomize.GroupBySorting = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GroupRowStyle = grsOffice11
        object GridDBTableViewUID: TcxGridDBColumn
          Caption = #1048#1079#1076#1077#1083#1080#1077
          DataBinding.FieldName = 'UID'
          Options.Sorting = False
          Width = 48
        end
        object GridDBTableViewART: TcxGridDBColumn
          Caption = #1040#1088#1090#1080#1082#1091#1083
          DataBinding.FieldName = 'ART'
          Options.Sorting = False
          Width = 100
        end
        object GridDBTableViewART2: TcxGridDBColumn
          DataBinding.FieldName = 'ART2'
          Options.Sorting = False
          Width = 45
          IsCaptionAssigned = True
        end
        object GridDBTableViewSZ: TcxGridDBColumn
          Caption = #1056#1072#1079#1084#1077#1088
          DataBinding.FieldName = 'SZ'
          Options.Sorting = False
          Width = 56
        end
        object GridDBTableViewWEIGHT: TcxGridDBColumn
          Caption = #1042#1077#1089
          DataBinding.FieldName = 'WEIGHT'
          Options.Sorting = False
        end
        object GridDBTableViewSELFCOMPANYTITLE: TcxGridDBColumn
          Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
          DataBinding.FieldName = 'SELF$COMPANY$TITLE'
          Visible = False
          GroupIndex = 0
          Options.Sorting = False
          Width = 200
        end
        object GridDBTableViewCompanyTitle: TcxGridDBColumn
          Caption = #1050#1086#1085#1088#1072#1075#1077#1085#1090
          DataBinding.FieldName = 'Company$Title'
          Options.Sorting = False
          Width = 196
        end
      end
      object GridLevel: TcxGridLevel
        GridView = GridDBTableView
      end
    end
    object PanelRightBottom: TPanel
      Left = 1
      Top = 558
      Width = 509
      Height = 41
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      object Panel1: TPanel
        Left = 422
        Top = 0
        Width = 87
        Height = 41
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object ButtonOK: TcxButton
          Left = 6
          Top = 8
          Width = 75
          Height = 25
          Caption = #1042#1099#1093#1086#1076
          ModalResult = 1
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
        end
      end
    end
  end
  object StatusBar: TdxStatusBar
    Left = 0
    Top = 600
    Width = 778
    Height = 26
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarContainerPanelStyle'
        PanelStyle.Container = StatusBarContainer0
      end>
    PaintStyle = stpsOffice11
    SizeGrip = False
    LookAndFeel.Kind = lfOffice11
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    object StatusBarContainer0: TdxStatusBarContainerControl
      Left = 2
      Top = 2
      Width = 776
      Height = 22
      object ProgressBar: TcxProgressBar
        Left = 0
        Top = 0
        Align = alClient
        Properties.OverloadBeginColor = clTeal
        Properties.OverloadEndColor = clAqua
        Properties.PeakColor = clGreen
        Properties.PeakValue = 50.000000000000000000
        Style.BorderStyle = ebsOffice11
        Style.LookAndFeel.Kind = lfOffice11
        Style.LookAndFeel.NativeStyle = True
        StyleDisabled.LookAndFeel.Kind = lfOffice11
        StyleDisabled.LookAndFeel.NativeStyle = True
        StyleFocused.LookAndFeel.Kind = lfOffice11
        StyleFocused.LookAndFeel.NativeStyle = True
        StyleHot.LookAndFeel.Kind = lfOffice11
        StyleHot.LookAndFeel.NativeStyle = True
        TabOrder = 0
        Width = 776
      end
    end
  end
  object Panel2: TPanel
    Left = 265
    Top = 0
    Width = 2
    Height = 600
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 3
  end
  object DataSource: TDataSource
    DataSet = Result
    Left = 24
    Top = 368
  end
  object Query: TpFIBDataSet
    SelectSQL.Strings = (
      '')
    Transaction = dm.tr
    Database = dm.db
    Left = 24
    Top = 240
    oFetchAll = True
  end
  object DataSet: TClientDataSet
    Aggregates = <>
    Params = <>
    ProviderName = 'DataSetProvider'
    Left = 24
    Top = 304
  end
  object DataSetProvider: TDataSetProvider
    DataSet = Query
    Left = 24
    Top = 272
  end
  object Result: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 24
    Top = 336
    object ResultUID: TIntegerField
      FieldName = 'UID'
    end
    object ResultART: TStringField
      FieldName = 'ART'
    end
    object ResultART2: TStringField
      FieldName = 'ART2'
    end
    object ResultSZ: TStringField
      FieldName = 'SZ'
    end
    object ResultWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
    object ResultSELFCOMPANYTITLE: TStringField
      FieldName = 'SELF$COMPANY$TITLE'
      Size = 64
    end
    object ResultCompanyTitle: TStringField
      FieldName = 'Company$Title'
      Size = 64
    end
  end
end
