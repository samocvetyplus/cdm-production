inherited fmExport1C: TfmExport1C
  Left = 478
  Top = 356
  Caption = #1042#1099#1075#1088#1091#1079#1082#1072' '#1074' 1'#1057
  ClientHeight = 240
  ClientWidth = 291
  OldCreateOrder = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  ExplicitWidth = 297
  ExplicitHeight = 272
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 291
    Height = 240
    ExplicitWidth = 291
    ExplicitHeight = 240
    object Label1: TLabel [0]
      Left = 8
      Top = 10
      Width = 79
      Height = 13
      Caption = #1060#1072#1081#1083' '#1101#1082#1089#1087#1086#1088#1090#1072
    end
    object Label2: TLabel [1]
      Left = 8
      Top = 32
      Width = 38
      Height = 13
      Caption = #1055#1077#1088#1080#1086#1076
    end
    object Label3: TLabel [2]
      Left = 92
      Top = 32
      Width = 6
      Height = 13
      Caption = #1089
    end
    object Label4: TLabel [3]
      Left = 186
      Top = 32
      Width = 12
      Height = 13
      Caption = #1087#1086
    end
    object Label5: TLabel [4]
      Left = 8
      Top = 76
      Width = 90
      Height = 13
      Caption = #1058#1080#1087#1099' '#1076#1086#1082#1091#1084#1077#1085#1090#1086#1074
    end
    object Label6: TLabel [5]
      Left = 8
      Top = 55
      Width = 49
      Height = 13
      Caption = #1042#1080#1076' '#1091#1095#1105#1090#1072
    end
    inherited btnOk: TBitBtn
      Left = 104
      Top = 208
      OnClick = btnOkClick
      ExplicitLeft = 104
      ExplicitTop = 208
    end
    inherited btnCancel: TBitBtn
      Left = 200
      Top = 208
      ExplicitLeft = 200
      ExplicitTop = 208
    end
    object edFile: TFilenameEdit
      Left = 92
      Top = 6
      Width = 193
      Height = 21
      NumGlyphs = 1
      TabOrder = 2
    end
    object edDate: TDBDateTimeEditEh
      Left = 201
      Top = 28
      Width = 84
      Height = 21
      EditButton.Glyph.Data = {
        36010000424D360100000000000076000000280000001C0000000C0000000100
        040000000000C000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD777777777
        77DDFFFFFFFFFFFF0000DD004044040047D777777777777F0000DDFFFFFFFFFF
        47D7FDFFFDFFFD7F0000DDF000F0008F47D7F777D777DF7F0000DDFF0FF8F80F
        47D7FD7FDDDD7F7F0000DDFF0FFFFF0F47D7FD7FDDFF7D7F0000DDFF0FF0008F
        47D7FD7FD777DD7F0000DDF00FF0FFFF47D7F77FD7FFFF7F0000DDFF0FF0000F
        47D7FD7DD7777D7F0000DDFFFFFFFFFF47D7FDDDDDDDDD7F0000DDF88888888F
        47D7FFFFFFFFFF7F0000DD0000000000DDD777777777777D0000}
      EditButton.NumGlyphs = 2
      EditButton.Style = ebsGlyphEh
      EditButton.Width = 20
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 4
      Visible = True
    end
    object edBD: TDBDateTimeEditEh
      Left = 100
      Top = 28
      Width = 85
      Height = 21
      EditButton.Glyph.Data = {
        36010000424D360100000000000076000000280000001C0000000C0000000100
        040000000000C000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDD777777777
        77DDFFFFFFFFFFFF0000DD004044040047D777777777777F0000DDFFFFFFFFFF
        47D7FDFFFDFFFD7F0000DDF000F0008F47D7F777D777DF7F0000DDFF0FF8F80F
        47D7FD7FDDDD7F7F0000DDFF0FFFFF0F47D7FD7FDDFF7D7F0000DDFF0FF0008F
        47D7FD7FD777DD7F0000DDF00FF0FFFF47D7F77FD7FFFF7F0000DDFF0FF0000F
        47D7FD7DD7777D7F0000DDFFFFFFFFFF47D7FDDDDDDDDD7F0000DDF88888888F
        47D7FFFFFFFFFF7F0000DD0000000000DDD777777777777D0000}
      EditButton.NumGlyphs = 2
      EditButton.Style = ebsGlyphEh
      EditButton.Width = 20
      EditButtons = <>
      Kind = dtkDateEh
      TabOrder = 3
      Visible = True
    end
    object chlbx1CGroup: TCheckListBox
      Left = 8
      Top = 92
      Width = 277
      Height = 110
      OnClickCheck = chlbx1CGroupClickCheck
      ItemHeight = 13
      TabOrder = 6
    end
    object cbContract: TComboBox
      Left = 104
      Top = 52
      Width = 181
      Height = 21
      ItemHeight = 13
      TabOrder = 5
      OnExit = cbContractExit
    end
  end
  object edExp1C: TDBDateTimeEditEh
    Left = 8
    Top = 214
    Width = 74
    Height = 18
    BorderStyle = bsNone
    Color = clBtnFace
    DataField = 'EXP1C'
    DataSource = dsrRec
    EditButton.Glyph.Data = {
      36040000424D3604000000000000360000002800000010000000100000000100
      2000000000000004000000000000000000000000000000000000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009933
      000099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00993300009933000099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF0099330000AA5F1F0099330000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF0099330000BA7D48009933000099330000FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF0099330000CDA27C00D8B59600993300009933
      0000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000E1C6B000ECDCCD00EDDD
      D10099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000F4E9E200FDF9
      F500FBF4EC009933000099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000E1C0AB00F7E9
      DA00F4E0CC00E1BA9C009933000099330000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000F6E6D600F3DEC800F0D5
      BA00E3B9950099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000EDCAA800EAC1
      9900E7B98B00DFA8750099330000FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0099330000E3AE
      7900E0A56B00DD9C5C00DA944F0099330000FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF009933
      00009933000099330000993300009933000099330000FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
      FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
    EditButton.Style = ebsGlyphEh
    EditButton.Width = 18
    EditButtons = <>
    Flat = True
    Kind = dtkDateEh
    TabOrder = 1
    Visible = True
    OnButtonClick = edExp1CButtonClick
    OnChange = edExp1CChange
  end
  object XMLDoc: TXMLDocument
    ParseOptions = [poValidateOnParse]
    XML.Strings = (
      '<?xml version="1.0" encoding="windows-1251"?>'
      '<EXPORTDATA>'
      '</EXPORTDATA>')
    Left = 12
    Top = 100
    DOMVendorDesc = 'MSXML'
  end
  object taMat: TpFIBDataSet
    SelectSQL.Strings = (
      'select * '
      'from D_Mat')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 204
    Top = 68
  end
  object taComp: TpFIBDataSet
    SelectSQL.Strings = (
      'select * '
      'from D_Comp')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 200
    Top = 104
  end
  object taDocument: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from Tmp_1C_Header'
      'where SELFCOMPID=:SELFCOMPID'
      'order by DocDate, Sortind')
    CacheModelOptions.BufferChunks = 1000
    AfterOpen = taDocumentAfterOpen
    AfterScroll = taDocumentAfterScroll
    BeforeOpen = taDocumentBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Filtered = True
    OnFilterRecord = taDocumentFilterRecord
    Left = 248
    Top = 68
  end
  object taDelDocument: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from Del_Inv '
      'where DocDate <= :ADATE and '
      '           DocDate >= (select Exp1C from D_Rec)')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taDelDocumentBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 204
    Top = 4
  end
  object taDocItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from Tmp_1C_Detail'
      'where HEADERID=:ID')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taDocItemBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 244
    Top = 4
    object taDocItemID: TFIBIntegerField
      FieldName = 'ID'
    end
    object taDocItemHEADERID: TFIBIntegerField
      FieldName = 'HEADERID'
    end
    object taDocItemMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taDocItemW: TFIBFloatField
      FieldName = 'W'
    end
    object taDocItemU: TFIBSmallIntField
      FieldName = 'U'
    end
    object taDocItemCOST: TFIBFloatField
      FieldName = 'COST'
    end
    object taDocItemNDSCOST: TFIBFloatField
      FieldName = 'NDSCOST'
    end
    object taDocItemINCOST: TFIBFloatField
      FieldName = 'INCOST'
    end
    object taDocItemW2: TFIBFloatField
      FieldName = 'W2'
    end
    object taDocItemSERVCOST: TFIBFloatField
      FieldName = 'SERVCOST'
    end
    object taDocItemMATOWNER: TFIBSmallIntField
      FieldName = 'MATOWNER'
    end
  end
  object stpBuild_1C: TpFIBStoredProc
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      'EXECUTE PROCEDURE BUILD_EXP1C (?BD, ?ED)')
    StoredProcName = 'BUILD_EXP1C'
    Left = 160
    Top = 104
  end
  object ta1CGroup: TpFIBDataSet
    SelectSQL.Strings = (
      'select GROUP_1C'
      'from D_InvType'
      'where GROUP_1C is not null'
      'union'
      'select GROUP_1C'
      'from D_ActType'
      'where GROUP_1C is not null'
      'union '
      'select GROUP_1C_CASSA '
      'from d_rec'
      'order by 1 desc')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 244
    Top = 104
  end
  object taRec: TpFIBDataSet
    UpdateSQL.Strings = (
      'update D_Rec set'
      '  Exp1C=:Exp1C')
    SelectSQL.Strings = (
      'select Exp1C'
      'from D_Rec')
    CacheModelOptions.BufferChunks = 1000
    AfterPost = taRecAfterPost
    BeforeClose = taRecBeforeClose
    Transaction = dm.tr
    Database = dm.db
    Left = 12
    Top = 8
    object taRecEXP1C: TDateTimeField
      FieldName = 'EXP1C'
      Origin = 'D_REC.EXP1C'
    end
  end
  object dsrRec: TDataSource
    DataSet = taRec
    Left = 52
    Top = 4
  end
  object Losses: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from eserv_mat where'
      '(invid=:invid)and (compid=:compid)')
    Transaction = dm.tr
    Database = dm.db
    Left = 104
    Top = 104
    object LossesID: TFIBIntegerField
      FieldName = 'ID'
    end
    object LossesINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object LossesMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object LossesCOMPID: TFIBIntegerField
      FieldName = 'COMPID'
    end
    object LossesNW: TFIBFloatField
      FieldName = 'NW'
    end
    object LossesSCOST: TFIBFloatField
      FieldName = 'SCOST'
    end
    object LossesN: TFIBFloatField
      FieldName = 'N'
    end
    object LossesGETW: TFIBFloatField
      FieldName = 'GETW'
    end
    object LossesOUTW: TFIBFloatField
      FieldName = 'OUTW'
    end
  end
end
