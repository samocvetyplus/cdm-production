object dmAppl: TdmAppl
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 550
  Width = 841
  object taAList: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE INV'
      'SET '
      '    DOCNO = :DOCNO,'
      '    DOCDATE = :DOCDATE,'
      '    ITYPE = :ITYPE,'
      '    DEPID = :DEPID,'
      '    USERID = :USERID,'
      '    ISCLOSE = :ISCLOSE,'
      '    ISPROCESS = :ISPROCESS,'
      '    ABD = :ABD,'
      '    AED = :AED,'
      '    W = :W,'
      '    Q = :Q,'
      '    REF = :REF,'
      '    OPERID = :OPERID,'
      '    A1BD = :A1BD,'
      '    A1ED = :A1ED'
      'WHERE'
      '   InvID = :ID ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    INV'
      'WHERE'
      '    InvID = :ID    ')
    InsertSQL.Strings = (
      'INSERT INTO INV('
      '    INVID,'
      '    DOCNO,'
      '    DOCDATE,'
      '    ITYPE,'
      '    DEPID,'
      '    USERID,'
      '    ISCLOSE,'
      '    ISPROCESS,'
      '    ABD,'
      '    AED,'
      '    W,'
      '    Q,'
      '    REF,'
      '    OPERID,'
      '    A1BD,'
      '    A1ED'
      ')'
      'VALUES('
      '    :ID,'
      '    :DOCNO,'
      '    :DOCDATE,'
      '    :ITYPE,'
      '    :DEPID,'
      '    :USERID,'
      '    :ISCLOSE,'
      '    :ISPROCESS,'
      '    :ABD,'
      '    :AED,'
      '    :W,'
      '    :Q,'
      '    :REF,'
      '    :OPERID,'
      '    :A1BD,'
      '    :A1ED'
      ')')
    RefreshSQL.Strings = (
      'select i.INVID ID, i.DOCNO, i.DOCDATE, i.ITYPE,'
      '     i.DEPID, i.USERID, i.ISCLOSE, i.ISPROCESS, i.ABD, i.AED, '
      '     i.W, i.Q, i.REF,  i.OPERID, o.OPERATION, d.Name Department,'
      '     i.A1BD, i.A1ED'
      'from Inv i, D_Oper o, D_Dep d'
      'where '
      '   i.invid = :old_invid')
    SelectSQL.Strings = (
      'select'
      ' i.INVID ID,'
      ' i.DOCNO,'
      ' i.DOCDATE,'
      ' i.ITYPE,'
      ' i.DEPID,'
      ' i.USERID,'
      ' i.ISCLOSE,'
      ' i.ISPROCESS,'
      ' i.ABD,'
      ' i.AED,'
      ' i.W,'
      ' i.Q,'
      ' i.REF,'
      ' i.OPERID,'
      ' o.OPERATION,'
      ' d.Name Department,'
      ' i.A1BD,'
      ' i.A1ED,'
      ' ds.Name StartN,'
      ' df.Name FinishN,'
      ' dr.name ReadyN,'
      ' jr.start$semis$date StartD,'
      ' jr.finish$semis$date FinishD,'
      ' jr.ready$semis$date ReadyD'
      'from'
      '  Inv i'
      '  left outer join D_Oper o on (o.OperId = i.OperId)'
      '  left outer join D_Dep d on (d.depid = i.depid)'
      '  left outer join ja_resp_persons jr on (i.invid = jr.invid)'
      
        '  left outer join D_Dep ds on (jr.start$semis$resp$person = ds.d' +
        'epid)'
      
        '  left outer join D_Dep df on (jr.finish$semis$resp$person = df.' +
        'depid)'
      
        '  left outer join D_Dep dr on (jr.ready$prod$resp$person = dr.de' +
        'pid)'
      'where '
      '  i.ITYPE = 1 and'
      '  i.SELFCOMPID = :SelfCompID and'
      '  (i.DOCDATE between :BD and :ED) and'
      '  (i.DEPID >= :DepID1) and (i.DEPID <= :DepID2) and'
      '  o.OperId between :OperID1 and :OperID2'
      'order by'
      '  i.DOCDATE'
      ''
      '           ')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterOpen = taAListAfterOpen
    AfterPost = CommitRetaining
    BeforeDelete = DelConf
    BeforeOpen = taAListBeforeOpen
    OnCalcFields = CalcRecNo
    OnNewRecord = taAListNewRecord
    Transaction = trAppl
    Database = dm.db
    Left = 24
    Top = 64
    object taAListRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taAListID: TIntegerField
      FieldName = 'ID'
      Origin = 'INV.INVID'
      Required = True
    end
    object taAListDOCNO: TIntegerField
      DisplayLabel = #8470' '#1079#1072#1103#1074#1082#1080
      FieldName = 'DOCNO'
      Origin = 'INV.DOCNO'
    end
    object taAListDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1079#1072#1103#1074#1082#1080
      FieldName = 'DOCDATE'
      Origin = 'INV.DOCDATE'
      DisplayFormat = 'dd.mm.yyyy hh:mm AMPM'
    end
    object taAListITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'INV.ITYPE'
    end
    object taAListDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'INV.DEPID'
    end
    object taAListUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'INV.USERID'
    end
    object taAListISCLOSE: TSmallintField
      DefaultExpression = '0'
      FieldName = 'ISCLOSE'
      Origin = 'INV.ISCLOSE'
    end
    object taAListISPROCESS: TSmallintField
      DefaultExpression = '0'
      FieldName = 'ISPROCESS'
      Origin = 'INV.ISPROCESS'
    end
    object taAListUSERNAME: TStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldKind = fkLookup
      FieldName = 'USERNAME'
      LookupDataSet = dmMain.taMol
      LookupKeyFields = 'MOLID'
      LookupResultField = 'FIO'
      KeyFields = 'USERID'
      Size = 60
      Lookup = True
    end
    object taAListABD: TDateTimeField
      DisplayLabel = #1053#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072
      FieldName = 'ABD'
      Origin = 'INV.ABD'
      DisplayFormat = 'DD.MM.YY'
    end
    object taAListAED: TDateTimeField
      DisplayLabel = #1050#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072
      FieldName = 'AED'
      Origin = 'INV.AED'
      DisplayFormat = 'DD.MM.YY'
    end
    object taAListW: TFloatField
      DisplayLabel = #1054#1073#1097#1080#1081' '#1074#1077#1089
      FieldName = 'W'
      Origin = 'INV.W'
      DisplayFormat = '0.##'
    end
    object taAListQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'INV.Q'
    end
    object taAListREF: TIntegerField
      FieldName = 'REF'
      Origin = 'INV.REF'
    end
    object taAListOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'INV.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAListOPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Origin = 'D_OPER.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAListA1BD: TFIBDateTimeField
      FieldName = 'A1BD'
      DisplayFormat = 'dd.mm.yyyy '
    end
    object taAListA1ED: TFIBDateTimeField
      FieldName = 'A1ED'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taAListDEPARTMENT: TFIBStringField
      FieldName = 'DEPARTMENT'
      Size = 40
      EmptyStrToNull = True
    end
    object taAListSTARTN: TFIBStringField
      DisplayLabel = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090'|'#1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1081
      FieldName = 'STARTN'
      Size = 40
      EmptyStrToNull = True
    end
    object taAListFINISHN: TFIBStringField
      DisplayLabel = #1060#1080#1085#1080#1096#1085#1099#1081' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090'|'#1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1081
      FieldName = 'FINISHN'
      Size = 40
      EmptyStrToNull = True
    end
    object taAListREADYN: TFIBStringField
      DisplayLabel = #1043#1086#1090#1086#1074#1086#1077' '#1080#1079#1076#1077#1083#1080#1077'|'#1054#1090#1074#1077#1090#1089#1090#1074#1077#1085#1085#1099#1081
      FieldName = 'READYN'
      Size = 40
      EmptyStrToNull = True
    end
    object taAListSTARTD: TFIBDateTimeField
      DisplayLabel = #1053#1072#1095#1072#1083#1100#1085#1099#1081' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090'|'#1057#1088#1086#1082
      FieldName = 'STARTD'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taAListFINISHD: TFIBDateTimeField
      DisplayLabel = #1060#1080#1085#1080#1096#1085#1099#1081' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090'|'#1057#1088#1086#1082
      FieldName = 'FINISHD'
      DisplayFormat = 'dd.mm.yyyy'
    end
    object taAListREADYD: TFIBDateTimeField
      DisplayLabel = #1043#1086#1090#1086#1074#1086#1077' '#1080#1079#1076#1077#1083#1080#1077'|'#1057#1088#1086#1082
      FieldName = 'READYD'
      DisplayFormat = 'dd.mm.yyyy'
    end
  end
  object dsrAList: TDataSource
    DataSet = taAList
    Left = 20
    Top = 112
  end
  object taAppl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update JA set'
      '  Q=:Q,'
      '  FQ=:FQ,'
      '  R=:R,'
      '  MODEL=:MODEL,'
      '  COMMENT=:COMMENT,'
      '  NQ=:NQ,'
      '  MW=:MW'
      'where ID=:ID')
    DeleteSQL.Strings = (
      'delete from Ja where Id=:OLD_Id')
    InsertSQL.Strings = (
      
        'execute procedure JA_I(:ID, :APPLID, :ARTID, :ART, :ART2ID, :ART' +
        '2, :SZ, '
      '    :SZNAME, :MW, :WQ, :AQ, '
      '   :Q, :INS, :INW, :INQ, :DEBTORS, :DEBTORW, :DEBTORQ,'
      
        '   :REJS, :REJW, :REJQ, :SALES, :SALEW, :SALEQ, :RETS, :RETW, :R' +
        'ETQ, '
      '   :OUTS, :OUTW, :OUTQ,:STORES, :STOREW, :STOREQ, '
      '   :PSTORES, :PSTOREW, :PSTOREQ,  :COMMENT, :NQ, :R, :MODEL);')
    RefreshSQL.Strings = (
      'select   ID, APPLID, ARTID, ART, ART2ID, ART2, SZ, SZNAME, '
      '    MW, WQ, Q, FQ, R, MODEL, INS, INW, INQ, DEBTORS,'
      '    DEBTORW, DEBTORQ, REJS, REJW, REJQ, SALES,'
      
        '    SALEW, SALEQ, RETS, RETW, RETQ, OUTS,  OUTW, OUTQ,          ' +
        '       '
      '    STORES, STOREQ, STOREW,PSTORES, PSTOREQ, PSTOREW,'
      
        '    COMMENT, NQ, PICT, AQ, NQ_APPL, KOEF, REQUESTQ, ARTROOT, REQ' +
        'UESTQ2,'
      '    SELL0, SELL1 '
      'from Appl_R(:ID)'
      '')
    SelectSQL.Strings = (
      'select ID, APPLID, ARTID, ART, ART2ID, ART2, SZ, SZNAME, '
      '    MW, WQ, Q, FQ, R, MODEL, INS, INW, INQ, DEBTORS,'
      '    DEBTORW, DEBTORQ, REJS, REJW, REJQ, SALES,'
      '    SALEW, SALEQ, RETS, RETW, RETQ, OUTS,  OUTW, OUTQ,'
      'STORES, STOREQ, STOREW, PSTORES, PSTOREQ, PSTOREW,'
      'COMMENT, NQ, PICT, AQ, NQ_APPL, KOEF, REQUESTQ,'
      '    ARTROOT, REQUESTQ2, SELL0, SELL1'
      'from APPL_S(:ID)'
      'where ART like :ART_'
      'order by ART, SZNAME')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = taApplAfterPost
    AfterScroll = taApplAfterScroll
    BeforeClose = taApplBeforeClose
    BeforeEdit = taApplBeforeEdit
    BeforeOpen = taApplBeforeOpen
    BeforePost = taApplBeforePost
    OnCalcFields = CalcRecNo
    OnNewRecord = taApplNewRecord
    Transaction = trAppl
    Database = dm.db
    OnFilterRecord = taApplFilterRecord
    Left = 76
    Top = 64
    object taApplRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taApplID: TIntegerField
      FieldName = 'ID'
      Origin = 'JA.ID'
      Required = True
    end
    object taApplAPPLID: TIntegerField
      FieldName = 'APPLID'
      Origin = 'JA.APPLID'
    end
    object taApplART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'APPL_S.ART'
      OnValidate = taApplARTValidate
      FixedChar = True
      EmptyStrToNull = True
    end
    object taApplARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'JA.ARTID'
    end
    object taApplART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'APPL_S.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taApplART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'JA.ART2ID'
    end
    object taApplSZ: TIntegerField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'APPL_S.SZ'
    end
    object taApplMW: TFloatField
      DisplayLabel = #1057#1088#1077#1076#1085#1080#1081' '#1074#1077#1089
      FieldName = 'MW'
      Origin = 'JA.MW'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taApplWQ: TIntegerField
      DisplayLabel = #1042' '#1088#1072#1073#1086#1090#1077
      FieldName = 'WQ'
      Origin = 'JA.WQ'
    end
    object taApplQ: TIntegerField
      DisplayLabel = #1047#1072#1103#1074#1080#1090#1100
      FieldName = 'Q'
      Origin = 'JA.Q'
    end
    object taApplFQ: TIntegerField
      DisplayLabel = #1055#1088#1080#1085#1103#1090#1086' '#1074' '#1088#1072#1073#1086#1090#1091
      FieldName = 'FQ'
      Origin = 'APPL_S.FQ'
    end
    object taApplR: TSmallintField
      DisplayLabel = #1056#1077#1079'?'
      FieldName = 'R'
      Origin = 'JA.R'
    end
    object taApplMODEL: TSmallintField
      DisplayLabel = #1052#1086#1076'?'
      FieldName = 'MODEL'
      Origin = 'JA.MODEL'
    end
    object taApplINS: TFloatField
      FieldName = 'INS'
      Origin = 'JA.INS'
    end
    object taApplINW: TFloatField
      DisplayLabel = #1042#1093'. '#1074#1077#1089
      FieldName = 'INW'
      Origin = 'JA.INW'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taApplINQ: TIntegerField
      DisplayLabel = #1042#1093'. '#1082#1086#1083'-'#1074#1086
      FieldName = 'INQ'
      Origin = 'JA.INQ'
    end
    object taApplDEBTORS: TFloatField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1082#1072
      FieldName = 'DEBTORS'
      Origin = 'JA.DEBTORS'
    end
    object taApplDEBTORW: TFloatField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1082#1072' ('#1074#1077#1089')'
      FieldName = 'DEBTORW'
      Origin = 'JA.DEBTORW'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taApplDEBTORQ: TIntegerField
      DisplayLabel = #1055#1086#1089#1090#1072#1074#1082#1072' ('#1082#1086#1083'-'#1074#1086')'
      FieldName = 'DEBTORQ'
      Origin = 'JA.DEBTORQ'
    end
    object taApplREJS: TFloatField
      FieldName = 'REJS'
      Origin = 'JA.REJS'
    end
    object taApplREJW: TFloatField
      FieldName = 'REJW'
      Origin = 'JA.REJW'
    end
    object taApplREJQ: TIntegerField
      FieldName = 'REJQ'
      Origin = 'JA.REJQ'
    end
    object taApplSALES: TFloatField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1080
      FieldName = 'SALES'
      Origin = 'JA.SALES'
    end
    object taApplSALEW: TFloatField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1080'('#1074#1077#1089')'
      FieldName = 'SALEW'
      Origin = 'JA.SALEW'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taApplSALEQ: TIntegerField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1080'('#1082#1086#1083'-'#1074#1086')'
      FieldName = 'SALEQ'
      Origin = 'JA.SALEQ'
    end
    object taApplRETS: TFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090
      FieldName = 'RETS'
      Origin = 'JA.RETS'
    end
    object taApplRETW: TFloatField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' ('#1074#1077#1089')'
      FieldName = 'RETW'
      Origin = 'JA.RETW'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taApplRETQ: TIntegerField
      DisplayLabel = #1042#1086#1079#1074#1088#1072#1090' ('#1082#1086#1083'-'#1074#1086')'
      FieldName = 'RETQ'
      Origin = 'JA.RETQ'
    end
    object taApplOUTS: TFloatField
      DisplayLabel = #1048#1089#1093'.'
      FieldName = 'OUTS'
      Origin = 'JA.OUTS'
    end
    object taApplOUTW: TFloatField
      DisplayLabel = #1048#1089#1093#1086#1076#1103#1097#1080#1081' '#1074#1077#1089
      FieldName = 'OUTW'
      Origin = 'JA.OUTW'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taApplOUTQ: TIntegerField
      DisplayLabel = #1048#1089#1093#1086#1076'. '#1082#1086#1083'-'#1074#1086
      FieldName = 'OUTQ'
      Origin = 'JA.OUTQ'
    end
    object taApplSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      Origin = 'APPL_S.SZNAME'
      OnChange = taApplSZNAMEChange
      FixedChar = True
      EmptyStrToNull = True
    end
    object taApplSTORES: TFloatField
      DisplayLabel = #1058#1077#1082'. '#1089#1091#1084#1084#1072
      FieldName = 'STORES'
      Origin = 'APPL_S.STORES'
    end
    object taApplSTOREQ: TIntegerField
      DisplayLabel = #1058#1077#1082'. '#1082#1086#1083'-'#1074#1086
      FieldName = 'STOREQ'
      Origin = 'APPL_S.STOREQ'
    end
    object taApplSTOREW: TFloatField
      DisplayLabel = #1058#1077#1082'. '#1074#1077#1089
      FieldName = 'STOREW'
      Origin = 'APPL_S.STOREW'
      DisplayFormat = '0.00'
      EditFormat = '0.00'
    end
    object taApplCOMMENT: TFIBStringField
      DisplayLabel = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
      FieldName = 'COMMENT'
      Origin = 'APPL_S.COMMENT'
      FixedChar = True
      Size = 200
      EmptyStrToNull = True
    end
    object taApplPICT: TBlobField
      FieldName = 'PICT'
      Origin = 'APPL_S.PICT'
      Size = 8
    end
    object taApplNQ: TIntegerField
      DisplayLabel = #1053#1077#1082#1086#1084#1087#1083#1077#1082#1090
      FieldName = 'NQ'
      Origin = 'APPL_S.NQ'
    end
    object taApplAQ: TIntegerField
      DisplayLabel = #1047#1072#1103#1074#1083#1077#1085#1086
      FieldName = 'AQ'
      Origin = 'APPL_S.AQ'
    end
    object taApplNQ_APPL: TIntegerField
      DisplayLabel = #1053#1077#1082#1086#1084#1087#1083#1077#1082#1090
      FieldName = 'NQ_APPL'
      Origin = 'APPL_S.NQ_APPL'
    end
    object taApplPSTOREQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'PSTOREQ'
      Origin = 'APPL_S.PSTOREQ'
    end
    object taApplPSTOREW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'PSTOREW'
      Origin = 'APPL_S.PSTOREW'
      DisplayFormat = '0.##'
    end
    object taApplKOEF: TFloatField
      FieldName = 'KOEF'
      Origin = 'APPL_S.KOEF'
    end
    object taApplREQUESTQ: TFIBIntegerField
      DisplayLabel = #1058#1088#1077#1073'. '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103#1084
      FieldName = 'REQUESTQ'
    end
    object taApplARTROOT: TFIBSmallIntField
      FieldName = 'ARTROOT'
    end
    object taApplREQUESTQ2: TFIBIntegerField
      DisplayLabel = #1058#1088#1077#1073' '#1087#1086' '#1079#1072#1103#1074#1082#1072#1084
      FieldName = 'REQUESTQ2'
    end
    object taApplSELL0: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1076#1072#1085#1086
      FieldName = 'SELL0'
    end
    object taApplSELL1: TFIBIntegerField
      DisplayLabel = #1054#1090#1076#1072#1085#1086' '#1056#1077#1072#1083#1080#1079#1072#1094#1080#1103
      FieldName = 'SELL1'
    end
  end
  object dsrAppl: TDataSource
    DataSet = taAppl
    Left = 76
    Top = 111
  end
  object quTmp: TpFIBQuery
    Transaction = trAppl
    Database = dm.db
    Left = 20
    Top = 8
  end
  object trAppl: TpFIBTransaction
    DefaultDatabase = dm.db
    TimeoutAction = TARollback
    Left = 136
    Top = 8
  end
  object taWHAppl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update WhAppl set'
      '  Id=:ID  '
      'where Id=:OLD_ID')
    InsertSQL.Strings = (
      
        'execute procedure WhAppl_I(:ID, :PSID, :OPERID, :ART2ID, :SZID, ' +
        ':T, :Q, :U)')
    RefreshSQL.Strings = (
      'select ID, PSID, OPERID, ARTID, ART2ID,  SZID,'
      '    T, Q, ART2, ART, SZ, OPERNAME, U, Q1, PATTERN'
      'from WHAppl_R(:ID)')
    SelectSQL.Strings = (
      'select ID, PSID, OPERID, ARTID, ART2ID,  SZID,'
      '    T, Q, ART2, ART, SZ, OPERNAME, U, Q1, PATTERN'
      'from WHAppl_S(:DEPID, :OPERID1, :OPERID2)'
      'order by ART, ART2, SZ')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeDelete = DelConf
    BeforeOpen = taWHApplBeforeOpen
    OnCalcFields = taWHApplCalcFields
    OnNewRecord = taWHApplNewRecord
    Transaction = trAppl
    Database = dm.db
    Left = 136
    Top = 64
    object taWHApplRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taWHApplID: TIntegerField
      FieldName = 'ID'
      Origin = 'WHAPPL_S.ID'
    end
    object taWHApplPSID: TIntegerField
      FieldName = 'PSID'
      Origin = 'WHAPPL_S.PSID'
    end
    object taWHApplOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'WHAPPL_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taWHApplARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'WHAPPL_S.ARTID'
    end
    object taWHApplART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'WHAPPL_S.ART2ID'
    end
    object taWHApplSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'WHAPPL_S.SZID'
    end
    object taWHApplT: TSmallintField
      FieldName = 'T'
      Origin = 'WHAPPL_S.T'
    end
    object taWHApplART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'WHAPPL_S.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taWHApplQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'WHAPPL_S.Q'
    end
    object taWHApplART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'WHAPPL_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taWHApplSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'WHAPPL_S.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taWHApplOPERNAME: TFIBStringField
      DisplayLabel = #1055#1086#1089#1083#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
      FieldName = 'OPERNAME'
      Origin = 'WHAPPL_S.OPERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taWHApplU: TSmallintField
      DisplayLabel = #1045#1076'.'#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'WHAPPL_S.U'
      OnGetText = UGetText
      OnSetText = USetText
    end
    object taWHApplQ1: TIntegerField
      FieldName = 'Q1'
      Origin = 'WHAPPL_S.Q1'
    end
    object taWHApplPATTERN: TFIBStringField
      FieldName = 'PATTERN'
      Size = 4000
      EmptyStrToNull = True
    end
  end
  object taAInv: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from Inv where INVID=:OLD_INVID')
    InsertSQL.Strings = (
      'insert into Inv(INVID, DOCNO, DOCDATE, DEPID, FROMDEPID,'
      '    USERID, Q, JOBID, ITYPE, REF, WORDERID)'
      'values (:INVID, :DOCNO, :DOCDATE, :DEPID, :FROMDEPID,'
      '    :USERID, :Q, :JOBID, :ITYPE, :REF, :WORDERID)')
    RefreshSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID,'
      '    USERID, Q, JOBID, ITYPE, REF, WORDERID, DEPNAME, FROMDEPNAME'
      'from AInv_S(:INVID)')
    SelectSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID,'
      '    USERID, Q, JOBID, ITYPE, REF, WORDERID, DEPNAME,'
      '    FROMDEPNAME'
      'from AInv_S(:ID)')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taAInvBeforeOpen
    OnNewRecord = taAInvNewRecord
    Transaction = trAppl
    Database = dm.db
    Left = 196
    Top = 64
    object taAInvINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'INV.INVID'
      Required = True
    end
    object taAInvDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'INV.DOCNO'
    end
    object taAInvDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'INV.DOCDATE'
    end
    object taAInvDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'INV.DEPID'
    end
    object taAInvFROMDEPID: TIntegerField
      FieldName = 'FROMDEPID'
      Origin = 'INV.FROMDEPID'
    end
    object taAInvUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'INV.USERID'
    end
    object taAInvJOBID: TIntegerField
      FieldName = 'JOBID'
      Origin = 'INV.JOBID'
    end
    object taAInvITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'INV.ITYPE'
    end
    object taAInvREF: TIntegerField
      FieldName = 'REF'
      Origin = 'INV.REF'
    end
    object taAInvWORDERID: TIntegerField
      FieldName = 'WORDERID'
      Origin = 'INV.WORDERID'
    end
    object taAInvQ: TFloatField
      FieldName = 'Q'
      Origin = 'AINV_S.Q'
    end
    object taAInvDEPNAME: TFIBStringField
      FieldName = 'DEPNAME'
      Origin = 'AINV_S.DEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taAInvFROMDEPNAME: TFIBStringField
      FieldName = 'FROMDEPNAME'
      Origin = 'AINV_S.FROMDEPNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
  end
  object dsrWHAppl: TDataSource
    DataSet = taWHAppl
    Left = 136
    Top = 112
  end
  object dsrAInv: TDataSource
    DataSet = taAInv
    Left = 196
    Top = 112
  end
  object taAEl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AEl set  '
      '  Q=:Q'
      'where Id=:OLD_Id')
    DeleteSQL.Strings = (
      'delete from AEl where Id=:OLD_Id')
    InsertSQL.Strings = (
      'insert into AEl(ID, INVID, ARTID, ART2ID, SZ, Q, T, OPERID, '
      '  OPERIDFROM, OLD_ART2ID, U, OLD_U)'
      'values(:ID, :INVID, :ARTID, :ART2ID, :SZID, :Q, :T, :OPERID, '
      '  :OPERIDFROM, :ART2ID_OLD, :U, :U_OLD)')
    RefreshSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      ' OPERID, OPERATION, OPERIDFROM, OPERATIONFROM,'
      ' ART2ID_OLD, ART2_OLD, U, U_OLD, Q1, PATTERN'
      'from AEl_R(:ID)')
    SelectSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      '  OPERID, OPERATION, OPERIDFROM, OPERATIONFROM,'
      '  ART2ID_OLD, ART2_OLD, U, U_OLD, Q1, PATTERN'
      'from AEl_S(:INVID)'
      'order by ART, ART2, SZ')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterOpen = taAElAfterOpen
    AfterPost = CommitRetainingWithRefreshWh
    AfterScroll = taAElAfterScroll
    BeforeCancel = CancelWh
    BeforeClose = PostBeforeClose
    BeforeOpen = taAElBeforeOpen
    OnCalcFields = taAElCalcFields
    OnNewRecord = taAElNewRecord
    Transaction = trAppl
    Database = dm.db
    Left = 244
    Top = 64
    object taAElRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taAElID: TIntegerField
      FieldName = 'ID'
      Origin = 'AEL_S.ID'
    end
    object taAElINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'AEL_S.INVID'
    end
    object taAElARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'AEL_S.ARTID'
    end
    object taAElART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'AEL_S.ART2ID'
    end
    object taAElSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'AEL_S.SZID'
    end
    object taAElART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'AEL_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taAElART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'AEL_S.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taAElSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'AEL_S.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taAElQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'AEL_S.Q'
      OnChange = taAElQChange
    end
    object taAElT: TSmallintField
      FieldName = 'T'
      Origin = 'AEL_S.T'
    end
    object taAElOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AEL_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAElOPERATION: TFIBStringField
      DisplayLabel = #1053#1072' '#1086#1087#1077#1088#1072#1094#1080#1080
      FieldName = 'OPERATION'
      Origin = 'AEL_S.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAElOPERIDFROM: TFIBStringField
      FieldName = 'OPERIDFROM'
      Origin = 'AEL_S.OPERIDFROM'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAElOPERATIONFROM: TFIBStringField
      DisplayLabel = #1055#1086#1089#1083#1077' '#1086#1087#1077#1088#1072#1094#1080#1080
      FieldName = 'OPERATIONFROM'
      Origin = 'AEL_S.OPERATIONFROM'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAElART2ID_OLD: TIntegerField
      FieldName = 'ART2ID_OLD'
      Origin = 'AEL_S.ART2ID_OLD'
    end
    object taAElART2_OLD: TFIBStringField
      DisplayLabel = #1057#1090#1072#1088#1099#1081' '#1072#1088#1090'2'
      FieldName = 'ART2_OLD'
      Origin = 'AEL_S.ART2_OLD'
      OnGetText = taAElART2_OLDGetText
      FixedChar = True
      EmptyStrToNull = True
    end
    object taAElU: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'AEL_S.U'
      OnGetText = UGetText
      OnSetText = USetText
    end
    object taAElU_OLD: TSmallintField
      DisplayLabel = #1057#1090'. '#1077#1076'. '#1080#1079#1084'.'
      FieldName = 'U_OLD'
      Origin = 'AEL_S.U_OLD'
      OnGetText = UGetText
      OnSetText = USetText
    end
    object taAElQ1: TIntegerField
      FieldName = 'Q1'
      Origin = 'AEL_S.Q1'
    end
    object taAElPATTERN: TFIBStringField
      DisplayLabel = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1072
      FieldName = 'PATTERN'
      Origin = 'AEL_S.PATTERN'
      FixedChar = True
      Size = 4000
      EmptyStrToNull = True
    end
  end
  object dsrAEl: TDataSource
    DataSet = taAEl
    Left = 244
    Top = 112
  end
  object taRejInv: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from Inv where INVID=:OLD_INVID')
    InsertSQL.Strings = (
      'insert into Inv(INVID, DOCNO, DOCDATE, DEPID, FROMDEPID,'
      '    USERID, Q, JOBID, ITYPE, REF, WORDERID)'
      'values (:INVID, :DOCNO, :DOCDATE, :DEPID, :FROMDEPID,'
      '    :USERID, :Q, :JOBID, :ITYPE, :REF, :WORDERID)')
    RefreshSQL.Strings = (
      
        'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID, USERID, Q,      ' +
        '   '
      '     JOBID, ITYPE, REF, WORDERID'
      'from AInv_S(:INVID)')
    SelectSQL.Strings = (
      
        'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID, USERID, Q,      ' +
        '   JOBID, ITYPE, REF, WORDERID'
      'from AInv_S(:ID)')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taRejInvBeforeOpen
    OnNewRecord = taRejInvNewRecord
    Transaction = trAppl
    Database = dm.db
    Left = 292
    Top = 64
    object taRejInvINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'INV.INVID'
      Required = True
    end
    object taRejInvDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'INV.DOCNO'
    end
    object taRejInvDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'INV.DOCDATE'
    end
    object taRejInvDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'INV.DEPID'
    end
    object taRejInvFROMDEPID: TIntegerField
      FieldName = 'FROMDEPID'
      Origin = 'INV.FROMDEPID'
    end
    object taRejInvUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'INV.USERID'
    end
    object taRejInvJOBID: TIntegerField
      FieldName = 'JOBID'
      Origin = 'INV.JOBID'
    end
    object taRejInvITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'INV.ITYPE'
    end
    object taRejInvREF: TIntegerField
      FieldName = 'REF'
      Origin = 'INV.REF'
    end
    object taRejInvWORDERID: TIntegerField
      FieldName = 'WORDERID'
      Origin = 'INV.WORDERID'
    end
    object taRejInvQ: TFloatField
      FieldName = 'Q'
      Origin = 'AINV_S.Q'
    end
  end
  object dsrRejInv: TDataSource
    DataSet = taRejInv
    Left = 292
    Top = 112
  end
  object taRejEl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AEl set '
      '  Q=:Q,'
      '  COMMENT=:COMMENT'
      'where ID=:OLD_ID')
    DeleteSQL.Strings = (
      'delete from AEl where ID=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into AEl(ID, INVID, ARTID, ART2ID, SZ, Q, T, OPERID, U, C' +
        'OMMENTID, COMMENT)'
      
        'values(:ID, :INVID, :ARTID, :ART2ID, :SZID, :Q, :T, :OPERID, :U,' +
        ' :COMMENTID, :COMMENT)')
    RefreshSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      '  OPERID, OPERATION, U, COMMENTID, COMMENT, Q1'
      'from AEl_R(:ID)'
      '')
    SelectSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T, '
      '  OPERID, OPERATION, U, COMMENTID, COMMENT, Q1'
      'from AEl_S(:INVID)'
      'order by ART, ART2, SZ')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterOpen = InitQValue
    AfterPost = CommitRetainingWithRefreshWh
    AfterScroll = InitQValue
    BeforeCancel = CancelWh
    BeforeClose = PostBeforeClose
    BeforeOpen = taRejElBeforeOpen
    OnCalcFields = CalcRecNo
    OnNewRecord = taRejElNewRecord
    OnPostError = taRejElPostError
    Transaction = trAppl
    Database = dm.db
    Left = 348
    Top = 64
    object taRejElRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taRejElID: TIntegerField
      FieldName = 'ID'
      Origin = 'AEL_S.ID'
    end
    object taRejElINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'AEL_S.INVID'
    end
    object taRejElARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'AEL_S.ARTID'
    end
    object taRejElART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'AEL_S.ART2ID'
    end
    object taRejElSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'AEL_S.SZID'
    end
    object taRejElART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'AEL_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taRejElART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'AEL_S.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taRejElSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'AEL_S.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taRejElQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'AEL_S.Q'
      OnChange = taRejElQChange
    end
    object taRejElT: TSmallintField
      FieldName = 'T'
      Origin = 'AEL_S.T'
    end
    object taRejElOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AEL_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taRejElOPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Origin = 'AEL_S.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taRejElU: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'AEL_S.U'
      OnGetText = UGetText
      OnSetText = USetText
    end
    object taRejElCOMMENTID: TSmallintField
      DisplayLabel = #1057#1080#1089#1090'.'#1082#1086#1084#1077#1085#1090
      FieldName = 'COMMENTID'
      Origin = 'AEL_S.COMMENTID'
      OnGetText = taRejElCOMMENTIDGetText
    end
    object taRejElCOMMENT: TFIBStringField
      DisplayLabel = #1050#1086#1084#1077#1085#1090#1072#1088#1080#1081
      FieldName = 'COMMENT'
      Origin = 'AEL_S.COMMENT'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taRejElQ1: TIntegerField
      FieldName = 'Q1'
      Origin = 'AEL_S.Q1'
    end
  end
  object dsrRejEl: TDataSource
    DataSet = taRejEl
    Left = 348
    Top = 112
  end
  object taPInv: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from Inv where INVID=:OLD_INVID')
    InsertSQL.Strings = (
      'insert into Inv(INVID, DOCNO, DOCDATE, DEPID, FROMDEPID,'
      '    USERID, Q, JOBID, REF, ITYPE, WORDERID)'
      'values (:INVID, :DOCNO, :DOCDATE, :DEPID, :FROMDEPID,'
      '    :USERID, :Q, :JOBID, :REF, :ITYPE, :WORDERID)'
      '')
    RefreshSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID, USERID, Q,     '
      '   JOBID, REF, ITYPE, WORDERID'
      'from AInv_S(:INVID)')
    SelectSQL.Strings = (
      
        'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID, USERID, Q,     J' +
        'OBID, REF, ITYPE, WORDERID'
      'from AInv_S(:ID)')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taPInvBeforeOpen
    OnNewRecord = taPInvNewRecord
    Transaction = trAppl
    Database = dm.db
    Left = 404
    Top = 64
    object taPInvINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'INV.INVID'
      Required = True
    end
    object taPInvDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'INV.DOCNO'
    end
    object taPInvDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'INV.DOCDATE'
    end
    object taPInvDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'INV.DEPID'
    end
    object taPInvFROMDEPID: TIntegerField
      FieldName = 'FROMDEPID'
      Origin = 'INV.FROMDEPID'
    end
    object taPInvUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'INV.USERID'
    end
    object taPInvQ: TFloatField
      FieldName = 'Q'
      Origin = 'AINV_S.Q'
    end
    object taPInvJOBID: TIntegerField
      FieldName = 'JOBID'
      Origin = 'INV.JOBID'
    end
    object taPInvREF: TIntegerField
      FieldName = 'REF'
      Origin = 'INV.REF'
    end
    object taPInvITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'INV.ITYPE'
    end
    object taPInvWORDERID: TIntegerField
      FieldName = 'WORDERID'
      Origin = 'INV.WORDERID'
    end
  end
  object taPEl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AEl set'
      '  Q=:Q,'
      '  COMMENT=:COMMENT'
      'where ID=:OLD_Id')
    DeleteSQL.Strings = (
      'delete from AEl where Id=:OLD_Id')
    InsertSQL.Strings = (
      
        'insert into AEl(ID, INVID, ARTID, ART2ID, SZ, Q, T, OPERID, U, C' +
        'OMMENTID, COMMENT)'
      
        'values(:ID, :INVID, :ARTID, :ART2ID, :SZID, :Q, :T, :OPERID, :U,' +
        ' :COMMENTID, :COMMENT)')
    RefreshSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      '  OPERID, OPERATION, U, COMMENTID, COMMENT, Q1'
      'from AEl_R(:ID)'
      '')
    SelectSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      '   OPERID, OPERATION, U, COMMENTID, COMMENT, Q1'
      'from AEl_S(:INVID)'
      'order by ART, ART2, SZ')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterOpen = InitQValue
    AfterPost = CommitRetainingWithRefreshWh
    AfterScroll = InitQValue
    BeforeCancel = CancelWh
    BeforeClose = PostBeforeClose
    BeforeOpen = taPElBeforeOpen
    BeforePost = taPElBeforePost
    OnCalcFields = CalcRecNo
    OnNewRecord = taPElNewRecord
    OnPostError = taPElPostError
    Transaction = trAppl
    Database = dm.db
    Left = 456
    Top = 64
    object taPElRecNo: TIntegerField
      DisplayLabel = #8470
      FieldKind = fkCalculated
      FieldName = 'RecNo'
      Calculated = True
    end
    object taPElID: TIntegerField
      FieldName = 'ID'
      Origin = 'AEL_S.ID'
    end
    object taPElINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'AEL_S.INVID'
    end
    object taPElARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'AEL_S.ARTID'
    end
    object taPElART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'AEL_S.ART2ID'
    end
    object taPElSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'AEL_S.SZID'
    end
    object taPElART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'AEL_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taPElART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'AEL_S.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taPElSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'AEL_S.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taPElQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'AEL_S.Q'
      OnChange = taPElQChange
    end
    object taPElT: TSmallintField
      FieldName = 'T'
      Origin = 'AEL_S.T'
    end
    object taPElOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AEL_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taPElOPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Origin = 'AEL_S.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taPElU: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'AEL_S.U'
      OnGetText = UGetText
      OnSetText = USetText
    end
    object taPElCOMMENTID: TSmallintField
      DisplayLabel = #1057#1080#1089#1090'.'#1082#1086#1084#1077#1085#1090
      FieldName = 'COMMENTID'
      Origin = 'AEL_S.COMMENTID'
      OnGetText = taPElCOMMENTIDGetText
    end
    object taPElCOMMENT: TFIBStringField
      DisplayLabel = #1050#1086#1084#1077#1085#1090#1072#1088#1080#1081
      FieldName = 'COMMENT'
      Origin = 'AEL_S.COMMENT'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taPElQ1: TIntegerField
      FieldName = 'Q1'
      Origin = 'AEL_S.Q1'
    end
  end
  object dsrPInv: TDataSource
    DataSet = taPInv
    Left = 404
    Top = 112
  end
  object dsrPEl: TDataSource
    DataSet = taPEl
    Left = 456
    Top = 112
  end
  object taTmp: TpFIBDataSet
    CacheModelOptions.BufferChunks = 1000
    Transaction = trAppl
    Database = dm.db
    Left = 76
    Top = 8
  end
  object taAIns: TpFIBDataSet
    UpdateSQL.Strings = (
      'update woItem set'
      '  SEMIS=:SEMIS,'
      '  Q=:Q,'
      '  W=:W, '
      '  ART2ID=:ART2ID '
      'where woItemId=:WOITEMID'
      '  ')
    DeleteSQL.Strings = (
      'delete from woItem where woItemId=:WOITEMID')
    InsertSQL.Strings = (
      
        'execute procedure AINS_I(:WOITEMID, :WORDERID,  :OPERID, :SEMIS,' +
        '  :ITDATE,'
      
        '         :Q,  :W,  :ITTYPE,  :INVID, :USERID, :E, :DOCNO, :DOCDA' +
        'TE,'
      '         :DEPID, :JOBDEPID, :JOBID, :AINV, :REF, :ART2ID)')
    SelectSQL.Strings = (
      'select w.WOITEMID,  w.WORDERID, w.OPERID,  w.SEMIS,  w.ITDATE,'
      
        '      w.Q,  w.W,  w.ITTYPE, w.INVID, w.DOCNO, w.DOCDATE,       w' +
        '.USERID, w.E, w.DEPID, w.JOBDEPID, w.JOBID, w.AINV, w.REF, w.SEM' +
        'ISNAME1, w.ART2ID'
      'from WOItem_S(:WORDERID) w, D_Semis s, D_Mat m'
      'where  w.ART2ID=:ART2ID and'
      '           w.Semis=s.SemisId and'
      '           s.Mat=m.Id and'
      '           m.IsIns=1'
      'order by ItDate')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeDelete = taAInsBeforeDelete
    BeforeOpen = taAInsBeforeOpen
    BeforePost = taAInsBeforePost
    OnCalcFields = taAInsCalcFields
    OnNewRecord = taAInsNewRecord
    Transaction = dm.tr
    Database = dm.db
    Left = 76
    Top = 244
    object taAInsWOITEMID: TIntegerField
      FieldName = 'WOITEMID'
      Origin = 'WOITEM_S.WOITEMID'
    end
    object taAInsWORDERID: TIntegerField
      FieldName = 'WORDERID'
      Origin = 'WOITEM_S.WORDERID'
    end
    object taAInsOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'WOITEM_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAInsSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Origin = 'WOITEM_S.SEMIS'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAInsITDATE: TDateTimeField
      FieldName = 'ITDATE'
      Origin = 'WOITEM_S.ITDATE'
    end
    object taAInsQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'WOITEM_S.Q'
    end
    object taAInsW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'WOITEM_S.W'
    end
    object taAInsITTYPE: TIntegerField
      DisplayLabel = #1058#1080#1087' '#1087#1077#1088#1077#1076#1072#1095#1080
      FieldName = 'ITTYPE'
      Origin = 'WOITEM_S.ITTYPE'
      OnGetText = taAInsITTYPEGetText
      OnSetText = taAInsITTYPESetText
    end
    object taAInsINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'WOITEM_S.INVID'
    end
    object taAInsDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'WOITEM_S.DOCNO'
    end
    object taAInsDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'WOITEM_S.DOCDATE'
    end
    object taAInsUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'WOITEM_S.USERID'
    end
    object taAInsE: TSmallintField
      FieldName = 'E'
      Origin = 'WOITEM_S.E'
    end
    object taAInsDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'WOITEM_S.DEPID'
    end
    object taAInsJOBDEPID: TIntegerField
      FieldName = 'JOBDEPID'
      Origin = 'WOITEM_S.JOBDEPID'
    end
    object taAInsJOBID: TIntegerField
      FieldName = 'JOBID'
      Origin = 'WOITEM_S.JOBID'
    end
    object taAInsAINV: TIntegerField
      FieldName = 'AINV'
      Origin = 'WOITEM_S.AINV'
    end
    object taAInsREF: TIntegerField
      FieldName = 'REF'
      Origin = 'WOITEM_S.REF'
    end
    object taAInsSEMISNAME1: TFIBStringField
      FieldName = 'SEMISNAME1'
      Origin = 'WOITEM_S.SEMISNAME1'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taAInsSEMISNAME: TStringField
      DisplayLabel = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077' '#1074#1089#1090#1072#1074#1082#1080
      FieldKind = fkLookup
      FieldName = 'SEMISNAME'
      LookupDataSet = dmMain.taStoneSemis
      LookupKeyFields = 'SEMISID'
      LookupResultField = 'SEMIS'
      KeyFields = 'SEMIS'
      Size = 40
      Lookup = True
    end
    object taAInsCLEANNES: TStringField
      DisplayLabel = #1063#1080#1089#1090#1086#1090#1072
      FieldKind = fkCalculated
      FieldName = 'CLEANNES'
      Size = 40
      Calculated = True
    end
    object taAInsCOLOR: TStringField
      DisplayLabel = #1062#1074#1077#1090
      FieldKind = fkCalculated
      FieldName = 'COLOR'
      Size = 40
      Calculated = True
    end
    object taAInsCHROMATICITY: TStringField
      DisplayLabel = #1062#1074#1077#1090#1085#1086#1089#1090#1100
      FieldKind = fkCalculated
      FieldName = 'CHROMATICITY'
      Size = 40
      Calculated = True
    end
    object taAInsEDGSHAPEID: TStringField
      DisplayLabel = #1060#1086#1088#1084#1072
      FieldKind = fkCalculated
      FieldName = 'EDGSHAPEID'
      Size = 40
      Calculated = True
    end
    object taAInsEDGTYPEID: TStringField
      DisplayLabel = #1054#1075#1088#1072#1085#1082#1072
      FieldKind = fkCalculated
      FieldName = 'EDGTYPEID'
      Size = 40
      Calculated = True
    end
    object taAInsIGR: TStringField
      DisplayLabel = #1043#1088#1091#1087#1087#1072
      FieldKind = fkCalculated
      FieldName = 'IGR'
      Size = 40
      Calculated = True
    end
    object taAInsART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'WOITEM_S.ART2ID'
    end
    object taAInsART: TStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldKind = fkCalculated
      FieldName = 'ART'
      Size = 40
      Calculated = True
    end
  end
  object dsrAIns: TDataSource
    DataSet = taAIns
    Left = 76
    Top = 292
  end
  object frAInv: TfrDBDataSet
    DataSet = taAInv
    Left = 196
    Top = 8
  end
  object frAEl: TfrDBDataSet
    DataSet = taAEl
    Left = 244
    Top = 8
  end
  object taWhA2: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select a2.Art2, a2.Art, sz.Name SZ, o.Operation, d.Name DEPNAME,' +
        ' wh.Q'
      'from WhAppl wh, Art2 a2, D_Sz sz, D_Oper o, D_Dep d'
      'where wh.Art2Id=:ART2ID and'
      '          wh.Sz=:SZ and'
      '          a2.Art2Id=wh.Art2Id and'
      '          wh.Sz=sz.Id and'
      '          wh.OperId=o.OperId and'
      '          wh.PsId=d.DepId and'
      '          wh.Q<>0    '
      'order by a2.Art, a2.Art2, sz.Name')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taWhA2BeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 128
    Top = 242
    object taWhA2ART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'ART2.ART2'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taWhA2ART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'ART2.ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taWhA2SZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'D_SZ.NAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taWhA2OPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Origin = 'D_OPER.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taWhA2DEPNAME: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
      FieldName = 'DEPNAME'
      Origin = 'D_DEP.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taWhA2Q: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'WHAPPL.Q'
      Required = True
    end
  end
  object dsrWhA2: TDataSource
    DataSet = taWhA2
    Left = 128
    Top = 292
  end
  object taAProdList: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select i.INVID ID, i.DOCNO, i.DOCDATE, i.W, i.Q, o.OPERATION, m.' +
        'FIO,'
      '          i.ISPROCESS'
      'from Inv i left join D_Oper o on (i.OperId=o.OperId),'
      '        D_Mol m  '
      'where i.INVID=:ID and '
      '           i.USERID=m.MOLID')
    SelectSQL.Strings = (
      
        'select i.INVID ID, i.DOCNO, i.DOCDATE, i.W, i.Q, o.OPERATION, m.' +
        'FIO,'
      '          i.ISPROCESS'
      'from Inv i left join D_Oper o on (i.OperId=o.OperId),'
      '        D_Mol m  '
      'where i.ITYPE=1 and'
      '           i.ISCLOSE=1 and'
      '           i.ISPROCESS=0 and'
      '           i.USERID=m.MOLID'
      'order by i.DOCDATE')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taAProdListBeforeOpen
    Transaction = trAppl
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1057#1087#1080#1089#1086#1082' '#1087#1088#1086#1080#1079#1074'. '#1079#1072#1082#1072#1079#1074#1086' '#1076#1083#1103' '#1074#1099#1073#1086#1088#1072'. '#1042' ADistr'
    Left = 19
    Top = 348
    object taAProdListID: TIntegerField
      FieldName = 'ID'
      Origin = 'INV.INVID'
      Required = True
    end
    object taAProdListDOCNO: TIntegerField
      DisplayLabel = #8470' '#1079#1072#1103#1074#1082#1080
      FieldName = 'DOCNO'
      Origin = 'INV.DOCNO'
    end
    object taAProdListDOCDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1079#1072#1103#1074#1082#1080
      FieldName = 'DOCDATE'
      Origin = 'INV.DOCDATE'
    end
    object taAProdListW: TFloatField
      DisplayLabel = #1042#1077#1089
      FieldName = 'W'
      Origin = 'INV.W'
      DisplayFormat = '0.###'
    end
    object taAProdListQ: TFloatField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'INV.Q'
    end
    object taAProdListOPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Origin = 'D_OPER.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAProdListFIO: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldKind = fkInternalCalc
      FieldName = 'FIO'
      Origin = 'D_MOL.FIO'
      ReadOnly = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAProdListISPROCESS: TSmallintField
      FieldName = 'ISPROCESS'
      Origin = 'INV.ISPROCESS'
    end
  end
  object dsrAProdList: TDataSource
    DataSet = taAProdList
    Left = 20
    Top = 396
  end
  object taAPInv: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from Inv where INVID=:OLD_INVID')
    InsertSQL.Strings = (
      'insert into Inv(INVID, DOCNO, DOCDATE, DEPID, FROMDEPID,'
      '    USERID, Q, JOBID, ITYPE, REF, WORDERID)'
      'values (:INVID, :DOCNO, :DOCDATE, :DEPID, :FROMDEPID,'
      '    :USERID, :Q, :JOBID, :ITYPE, :REF, :WORDERID)')
    RefreshSQL.Strings = (
      
        'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID, USERID, Q,      ' +
        '   '
      '     JOBID, ITYPE, REF, WORDERID'
      'from AInv_S(:INVID)')
    SelectSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID,'
      '    USERID, Q, JOBID, ITYPE, REF, WORDERID'
      'from AInv_S(:ID)')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taAPInvBeforeOpen
    OnNewRecord = taAPInvNewRecord
    Transaction = trAppl
    Database = dm.db
    Left = 344
    Top = 244
    object taAPInvINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'AINV_S.INVID'
    end
    object taAPInvDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'AINV_S.DOCNO'
    end
    object taAPInvDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'AINV_S.DOCDATE'
    end
    object taAPInvDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'AINV_S.DEPID'
    end
    object taAPInvFROMDEPID: TIntegerField
      FieldName = 'FROMDEPID'
      Origin = 'AINV_S.FROMDEPID'
    end
    object taAPInvUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'AINV_S.USERID'
    end
    object taAPInvQ: TFloatField
      FieldName = 'Q'
      Origin = 'AINV_S.Q'
    end
    object taAPInvJOBID: TIntegerField
      FieldName = 'JOBID'
      Origin = 'AINV_S.JOBID'
    end
    object taAPInvITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'AINV_S.ITYPE'
    end
    object taAPInvREF: TIntegerField
      FieldName = 'REF'
      Origin = 'AINV_S.REF'
    end
    object taAPInvWORDERID: TIntegerField
      FieldName = 'WORDERID'
      Origin = 'AINV_S.WORDERID'
    end
  end
  object dsrAPInv: TDataSource
    DataSet = taAPInv
    Left = 344
    Top = 292
  end
  object taAPEl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AEl set  '
      '  Q=:Q'
      'where ID=:OLD_ID')
    DeleteSQL.Strings = (
      'delete from AEl where Id=:OLD_Id')
    InsertSQL.Strings = (
      'insert into AEl(ID, INVID, ARTID, ART2ID, SZ, Q, T, OPERID, U)'
      'values(:ID, :INVID, :ARTID, :ART2ID, :SZID, :Q, :T, :OPERID, :U)')
    RefreshSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      ' OPERID, OPERATION,  U'
      'from AEl_R(:ID)')
    SelectSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      '  OPERID, OPERATION, U'
      'from AEl_S(:INVID)'
      'order by ART, ART2, SZ')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taAPElBeforeOpen
    OnNewRecord = taAPElNewRecord
    OnPostError = taAPElPostError
    Transaction = trAppl
    Database = dm.db
    Left = 400
    Top = 244
    object taAPElID: TIntegerField
      FieldName = 'ID'
      Origin = 'AEL_S.ID'
    end
    object taAPElINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'AEL_S.INVID'
    end
    object taAPElARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'AEL_S.ARTID'
    end
    object taAPElART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'AEL_S.ART2ID'
    end
    object taAPElSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'AEL_S.SZID'
    end
    object taAPElART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'AEL_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taAPElART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'AEL_S.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taAPElSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'AEL_S.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taAPElQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'AEL_S.Q'
      OnChange = taAPElQChange
    end
    object taAPElT: TSmallintField
      FieldName = 'T'
      Origin = 'AEL_S.T'
    end
    object taAPElOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AEL_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taAPElOPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Origin = 'AEL_S.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taAPElU: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'AEL_S.U'
      OnGetText = UGetText
    end
  end
  object dsrAPEl: TDataSource
    DataSet = taAPEl
    Left = 400
    Top = 292
  end
  object taApplInv: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Inv set'
      '  ISPROCESS=:ISPROCESS'
      'where INVID=:ID')
    RefreshSQL.Strings = (
      
        'select i.INVID ID, i.DOCNO, i.DOCDATE, i.W, i.Q, o.OPERATION, m.' +
        'FIO,'
      '          i.ISPROCESS'
      'from Inv i left join D_Oper o on (i.OperId=o.OperId), D_Mol m  '
      'where i.INVID=:ID and '
      '           i.USERID=m.MOLID'
      '           ')
    SelectSQL.Strings = (
      
        'select i.INVID ID, i.DOCNO, i.DOCDATE, i.W, i.Q, o.OPERATION, m.' +
        'FIO,'
      '          i.ISPROCESS'
      'from Inv i left join D_Oper o on (i.OperId=o.OperId), D_Mol m  '
      'where i.INVID=:APPLID and'
      '           i.USERID=m.MOLID'
      '')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taApplInvBeforeOpen
    Transaction = trAppl
    Database = dm.db
    Left = 276
    Top = 244
    object taApplInvID: TIntegerField
      FieldName = 'ID'
      Origin = 'INV.INVID'
      Required = True
    end
    object taApplInvDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'INV.DOCNO'
    end
    object taApplInvDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'INV.DOCDATE'
    end
    object taApplInvW: TFloatField
      FieldName = 'W'
      Origin = 'INV.W'
    end
    object taApplInvQ: TFloatField
      FieldName = 'Q'
      Origin = 'INV.Q'
    end
    object taApplInvOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Origin = 'D_OPER.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taApplInvFIO: TFIBStringField
      FieldKind = fkInternalCalc
      FieldName = 'FIO'
      Origin = 'D_MOL.FIO'
      ReadOnly = True
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taApplInvISPROCESS: TSmallintField
      FieldName = 'ISPROCESS'
      Origin = 'INV.ISPROCESS'
    end
  end
  object dsrApplInv: TDataSource
    DataSet = taApplInv
    Left = 276
    Top = 292
  end
  object taExecAppl: TpFIBDataSet
    SelectSQL.Strings = (
      'select j.ARTID, a2.ART, ja.SZ SZID, sz.NAME SZNAME'
      'from Ja j, Art2 a2, D_Sz sz'
      'where j.ART2ID=a2.ART2ID and'
      
        '          j.Art2id in (select distinct Art2Id from Ja where Appl' +
        'Id=j.ApplId) and'
      '          j.SZ=sz.ID '
      '      ')
    CacheModelOptions.BufferChunks = 1000
    AfterClose = taExecApplAfterClose
    Transaction = dm.tr
    Database = dm.db
    Left = 20
    Top = 158
    object taExecApplARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'JA.ARTID'
      Required = True
    end
    object taExecApplART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'ART2.ART'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taExecApplSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      Origin = 'D_SZ.NAME'
      Required = True
      FixedChar = True
      EmptyStrToNull = True
    end
    object taExecApplSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'JA.SZ'
      Required = True
    end
  end
  object dsrExecAppl: TDataSource
    DataSet = taExecAppl
    Left = 20
    Top = 201
  end
  object quGetExecApplId: TpFIBDataSet
    SelectSQL.Strings = (
      'select APPLID'
      'from Get_ExecApplId')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 92
    Top = 160
    object quGetExecApplIdAPPLID: TIntegerField
      FieldName = 'APPLID'
      Origin = 'GET_EXECAPPLID.APPLID'
    end
  end
  object taRejAdd: TpFIBDataSet
    SelectSQL.Strings = (
      'select ARTID, ART, SZID, SZNAME, ITYPE, Q, COMMENTID,'
      '          COMMENTNAME, COMMENT, USERNAME, INVDATE'
      'from RejAdd(:BD, :ED)'
      'order by ART, SZNAME')
    CacheModelOptions.BufferChunks = 1000
    BeforeOpen = taRejAddBeforeOpen
    Transaction = trAppl
    Database = dm.db
    Left = 168
    Top = 157
    object taRejAddARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'REJADD.ARTID'
    end
    object taRejAddART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'REJADD.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taRejAddSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'REJADD.SZID'
    end
    object taRejAddSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZNAME'
      Origin = 'REJADD.SZNAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taRejAddITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'REJADD.ITYPE'
    end
    object taRejAddQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'REJADD.Q'
    end
    object taRejAddCOMMENTID: TIntegerField
      FieldName = 'COMMENTID'
      Origin = 'REJADD.COMMENTID'
    end
    object taRejAddCOMMENTNAME: TFIBStringField
      DisplayLabel = #1057#1080#1089#1090'. '#1082#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
      FieldName = 'COMMENTNAME'
      Origin = 'REJADD.COMMENTNAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taRejAddCOMMENT: TFIBStringField
      DisplayLabel = #1055#1088#1080#1084#1077#1095#1072#1085#1080#1077
      FieldName = 'COMMENT'
      Origin = 'REJADD.COMMENT'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taRejAddUSERNAME: TFIBStringField
      DisplayLabel = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
      FieldName = 'USERNAME'
      Origin = 'REJADD.USERNAME'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taRejAddINVDATE: TDateTimeField
      DisplayLabel = #1044#1072#1090#1072' '#1076#1077#1081#1089#1090#1074#1080#1103
      FieldName = 'INVDATE'
      Origin = 'REJADD.INVDATE'
    end
  end
  object dsrRejAdd: TDataSource
    DataSet = taRejAdd
    Left = 168
    Top = 201
  end
  object taApplPInv: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from Inv where INVID=:OLD_INVID')
    InsertSQL.Strings = (
      'insert into Inv(INVID, DOCNO, DOCDATE, DEPID, FROMDEPID,'
      '    USERID, Q, JOBID, ITYPE, REF, WORDERID)'
      'values (:INVID, :DOCNO, :DOCDATE, :DEPID, :FROMDEPID,'
      '    :USERID, :Q, :JOBID, :ITYPE, :REF, :WORDERID)')
    RefreshSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID, USERID, Q,     '
      '   JOBID, REF, ITYPE, WORDERID'
      'from AInv_S(:INVID)')
    SelectSQL.Strings = (
      
        'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID, USERID, Q,     J' +
        'OBID, REF, ITYPE, WORDERID'
      'from AInv_S(:ID)')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taApplPInvBeforeOpen
    OnNewRecord = taApplPInvNewRecord
    Transaction = trAppl
    Database = dm.db
    Left = 96
    Top = 348
    object taApplPInvINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'AINV_S.INVID'
    end
    object taApplPInvDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'AINV_S.DOCNO'
    end
    object taApplPInvDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'AINV_S.DOCDATE'
    end
    object taApplPInvDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'AINV_S.DEPID'
    end
    object taApplPInvFROMDEPID: TIntegerField
      FieldName = 'FROMDEPID'
      Origin = 'AINV_S.FROMDEPID'
    end
    object taApplPInvUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'AINV_S.USERID'
    end
    object taApplPInvQ: TFloatField
      FieldName = 'Q'
      Origin = 'AINV_S.Q'
    end
    object taApplPInvJOBID: TIntegerField
      FieldName = 'JOBID'
      Origin = 'AINV_S.JOBID'
    end
    object taApplPInvREF: TIntegerField
      FieldName = 'REF'
      Origin = 'AINV_S.REF'
    end
    object taApplPInvITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'AINV_S.ITYPE'
    end
    object taApplPInvWORDERID: TIntegerField
      FieldName = 'WORDERID'
      Origin = 'AINV_S.WORDERID'
    end
  end
  object taApplPEl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AEl set'
      '  Q=:Q,'
      '  COMMENT=:COMMENT'
      'where ID=:OLD_ID')
    DeleteSQL.Strings = (
      'delete from AEl where ID=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into AEl(ID, INVID, ARTID, ART2ID, SZ, Q, T, OPERID, U, C' +
        'OMMENTID, COMMENT)'
      
        'values(:ID, :INVID, :ARTID, :ART2ID, :SZID, :Q, :T, :OPERID, :U,' +
        ' :COMMENTID, :COMMENT)')
    RefreshSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      '  OPERID, OPERATION, U, COMMENTID, COMMENT'
      'from AEl_R(:ID)')
    SelectSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      '   OPERID, OPERATION, U, COMMENTID, COMMENT'
      'from AEl_S(:INVID)'
      'order by ART, ART2, SZ')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taApplPElBeforeOpen
    OnNewRecord = taApplPElNewRecord
    OnPostError = taApplPElPostError
    Transaction = trAppl
    Database = dm.db
    Left = 150
    Top = 349
    object taApplPElID: TIntegerField
      FieldName = 'ID'
      Origin = 'AEL_S.ID'
    end
    object taApplPElINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'AEL_S.INVID'
    end
    object taApplPElARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'AEL_S.ARTID'
    end
    object taApplPElART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'AEL_S.ART2ID'
    end
    object taApplPElSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'AEL_S.SZID'
    end
    object taApplPElART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'AEL_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taApplPElART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'AEL_S.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taApplPElSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'AEL_S.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taApplPElQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'AEL_S.Q'
    end
    object taApplPElT: TSmallintField
      FieldName = 'T'
      Origin = 'AEL_S.T'
    end
    object taApplPElOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AEL_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taApplPElOPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Origin = 'AEL_S.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taApplPElU: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'AEL_S.U'
    end
    object taApplPElCOMMENTID: TSmallintField
      DisplayLabel = #1057#1080#1089#1090'. '#1082#1086#1084'.'
      FieldName = 'COMMENTID'
      Origin = 'AEL_S.COMMENTID'
    end
    object taApplPElCOMMENT: TFIBStringField
      DisplayLabel = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
      FieldName = 'COMMENT'
      Origin = 'AEL_S.COMMENT'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
  end
  object taApplRejInv: TpFIBDataSet
    DeleteSQL.Strings = (
      'delete from Inv where INVID=:OLD_INVID')
    InsertSQL.Strings = (
      'insert into Inv(INVID, DOCNO, DOCDATE, DEPID, FROMDEPID,'
      '    USERID, Q, JOBID, ITYPE, REF, WORDERID)'
      'values (:INVID, :DOCNO, :DOCDATE, :DEPID, :FROMDEPID,'
      '    :USERID, :Q, :JOBID, :ITYPE, :REF, :WORDERID)')
    RefreshSQL.Strings = (
      'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID, USERID, Q,     '
      '   JOBID, REF, ITYPE, WORDERID'
      'from AInv_S(:INVID)')
    SelectSQL.Strings = (
      
        'select INVID, DOCNO, DOCDATE, DEPID, FROMDEPID, USERID, Q,     J' +
        'OBID, REF, ITYPE, WORDERID'
      'from AInv_S(:ID)')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterOpen = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taApplRejInvBeforeOpen
    OnNewRecord = taApplRejInvNewRecord
    Transaction = trAppl
    Database = dm.db
    Left = 232
    Top = 349
    object taApplRejInvINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'AINV_S.INVID'
    end
    object taApplRejInvDOCNO: TIntegerField
      FieldName = 'DOCNO'
      Origin = 'AINV_S.DOCNO'
    end
    object taApplRejInvDOCDATE: TDateTimeField
      FieldName = 'DOCDATE'
      Origin = 'AINV_S.DOCDATE'
    end
    object taApplRejInvDEPID: TIntegerField
      FieldName = 'DEPID'
      Origin = 'AINV_S.DEPID'
    end
    object taApplRejInvFROMDEPID: TIntegerField
      FieldName = 'FROMDEPID'
      Origin = 'AINV_S.FROMDEPID'
    end
    object taApplRejInvUSERID: TIntegerField
      FieldName = 'USERID'
      Origin = 'AINV_S.USERID'
    end
    object taApplRejInvQ: TFloatField
      FieldName = 'Q'
      Origin = 'AINV_S.Q'
    end
    object taApplRejInvJOBID: TIntegerField
      FieldName = 'JOBID'
      Origin = 'AINV_S.JOBID'
    end
    object taApplRejInvREF: TIntegerField
      FieldName = 'REF'
      Origin = 'AINV_S.REF'
    end
    object taApplRejInvITYPE: TSmallintField
      FieldName = 'ITYPE'
      Origin = 'AINV_S.ITYPE'
    end
    object taApplRejInvWORDERID: TIntegerField
      FieldName = 'WORDERID'
      Origin = 'AINV_S.WORDERID'
    end
  end
  object taApplRejEl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update AEl set '
      '  Q=:Q,'
      '  COMMENT=:COMMENT'
      'where ID=:OLD_ID')
    DeleteSQL.Strings = (
      'delete from AEl where ID=:OLD_ID')
    InsertSQL.Strings = (
      
        'insert into AEl(ID, INVID, ARTID, ART2ID, SZ, Q, T, OPERID, U, C' +
        'OMMENTID, COMMENT)'
      
        'values(:ID, :INVID, :ARTID, :ART2ID, :SZID, :Q, :T, :OPERID, :U,' +
        ' :COMMENTID, :COMMENT)')
    RefreshSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T,'
      '  OPERID, OPERATION, U, COMMENTID, COMMENT'
      'from AEl_R(:ID)')
    SelectSQL.Strings = (
      'select ID, INVID, ARTID, ART2ID, SZID,  ART, ART2, SZ, Q, T, '
      '  OPERID, OPERATION, U, COMMENTID, COMMENT'
      'from AEl_S(:INVID)'
      'order by ART, ART2, SZ')
    CacheModelOptions.BufferChunks = 1000
    AfterDelete = CommitRetaining
    AfterPost = CommitRetaining
    BeforeClose = PostBeforeClose
    BeforeOpen = taApplRejElBeforeOpen
    OnNewRecord = taApplRejElNewRecord
    OnPostError = taApplRejElPostError
    Transaction = trAppl
    Database = dm.db
    Left = 296
    Top = 348
    object taApplRejElID: TIntegerField
      FieldName = 'ID'
      Origin = 'AEL_S.ID'
    end
    object taApplRejElINVID: TIntegerField
      FieldName = 'INVID'
      Origin = 'AEL_S.INVID'
    end
    object taApplRejElARTID: TIntegerField
      FieldName = 'ARTID'
      Origin = 'AEL_S.ARTID'
    end
    object taApplRejElART2ID: TIntegerField
      FieldName = 'ART2ID'
      Origin = 'AEL_S.ART2ID'
    end
    object taApplRejElSZID: TIntegerField
      FieldName = 'SZID'
      Origin = 'AEL_S.SZID'
    end
    object taApplRejElART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'ART'
      Origin = 'AEL_S.ART'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taApplRejElART2: TFIBStringField
      DisplayLabel = #1040#1088#1090'2'
      FieldName = 'ART2'
      Origin = 'AEL_S.ART2'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
    object taApplRejElSZ: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'SZ'
      Origin = 'AEL_S.SZ'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taApplRejElQ: TIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
      Origin = 'AEL_S.Q'
    end
    object taApplRejElT: TSmallintField
      FieldName = 'T'
      Origin = 'AEL_S.T'
    end
    object taApplRejElOPERID: TFIBStringField
      FieldName = 'OPERID'
      Origin = 'AEL_S.OPERID'
      FixedChar = True
      Size = 10
      EmptyStrToNull = True
    end
    object taApplRejElOPERATION: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'OPERATION'
      Origin = 'AEL_S.OPERATION'
      FixedChar = True
      Size = 60
      EmptyStrToNull = True
    end
    object taApplRejElU: TSmallintField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'U'
      Origin = 'AEL_S.U'
    end
    object taApplRejElCOMMENTID: TSmallintField
      DisplayLabel = #1057#1080#1089#1090'.'#1082#1086#1084'.'
      FieldName = 'COMMENTID'
      Origin = 'AEL_S.COMMENTID'
    end
    object taApplRejElCOMMENT: TFIBStringField
      DisplayLabel = #1050#1086#1084#1084#1077#1085#1090#1072#1088#1080#1081
      FieldName = 'COMMENT'
      Origin = 'AEL_S.COMMENT'
      FixedChar = True
      Size = 80
      EmptyStrToNull = True
    end
  end
  object dsrApplPInv: TDataSource
    DataSet = taApplPInv
    Left = 96
    Top = 396
  end
  object dsrApplPEl: TDataSource
    DataSet = taApplPEl
    Left = 152
    Top = 396
  end
  object dsrApplRejInv: TDataSource
    DataSet = taApplRejInv
    Left = 232
    Top = 396
  end
  object dsrApplRejEl: TDataSource
    DataSet = taApplRejEl
    Left = 296
    Top = 396
  end
  object taChangeAssortByAppl: TpFIBDataSet
    UpdateSQL.Strings = (
      'update JA set'
      '  FQ=:AFQ'
      'where ID=:JAID')
    SelectSQL.Strings = (
      
        'select JAID, AARTID, AART, AART2ID, ASZID, ASZNAME, AQ, AFQ, AOP' +
        'ERID, '
      
        '  AOPERNAME, AUNITID, AUNITNAME, WHARTID, WHART, WHART2ID, WHSZI' +
        'D, WHSZNAME,'
      '  WHOPERID, WHOPERNAME, WHUNITID, WHUNITNAME, WHQ, Q, D'
      'from ChangeAssortByAppl_S(:APPLID, :DEPID, :OPERID1, :OPERID2)')
    AfterDelete = CommitRetaining
    AfterOpen = taChangeAssortByApplAfterOpen
    AfterPost = CommitRetaining
    AfterScroll = taChangeAssortByApplAfterScroll
    BeforeOpen = taChangeAssortByApplBeforeOpen
    AllowedUpdateKinds = [ukModify]
    Transaction = trAppl
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 260
    Top = 156
    poSQLINT64ToBCD = True
    object taChangeAssortByApplJAID: TFIBIntegerField
      FieldName = 'JAID'
    end
    object taChangeAssortByApplAARTID: TFIBIntegerField
      FieldName = 'AARTID'
    end
    object taChangeAssortByApplAART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'AART'
      EmptyStrToNull = True
    end
    object taChangeAssortByApplAART2ID: TFIBIntegerField
      FieldName = 'AART2ID'
    end
    object taChangeAssortByApplASZID: TFIBIntegerField
      FieldName = 'ASZID'
    end
    object taChangeAssortByApplASZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'ASZNAME'
      EmptyStrToNull = True
    end
    object taChangeAssortByApplAQ: TFIBIntegerField
      DisplayLabel = #1047#1072#1103#1074#1083#1077#1085#1086
      FieldName = 'AQ'
    end
    object taChangeAssortByApplAOPERID: TFIBStringField
      FieldName = 'AOPERID'
      Size = 10
      EmptyStrToNull = True
    end
    object taChangeAssortByApplAUNITID: TFIBIntegerField
      FieldName = 'AUNITID'
    end
    object taChangeAssortByApplWHARTID: TFIBIntegerField
      FieldName = 'WHARTID'
    end
    object taChangeAssortByApplWHART2ID: TFIBIntegerField
      FieldName = 'WHART2ID'
    end
    object taChangeAssortByApplWHSZID: TFIBIntegerField
      FieldName = 'WHSZID'
    end
    object taChangeAssortByApplWHOPERID: TFIBStringField
      FieldName = 'WHOPERID'
      Size = 10
      EmptyStrToNull = True
    end
    object taChangeAssortByApplWHUNITID: TFIBIntegerField
      FieldName = 'WHUNITID'
    end
    object taChangeAssortByApplAOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'AOPERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taChangeAssortByApplWHART: TFIBStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083
      FieldName = 'WHART'
      EmptyStrToNull = True
    end
    object taChangeAssortByApplWHSZNAME: TFIBStringField
      DisplayLabel = #1056#1072#1079#1084#1077#1088
      FieldName = 'WHSZNAME'
      EmptyStrToNull = True
    end
    object taChangeAssortByApplWHOPERNAME: TFIBStringField
      DisplayLabel = #1054#1087#1077#1088#1072#1094#1080#1103
      FieldName = 'WHOPERNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taChangeAssortByApplAFQ: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1085#1103#1090#1086' '#1074' '#1088#1072#1073#1086#1090#1091
      FieldName = 'AFQ'
    end
    object taChangeAssortByApplWHUNITNAME: TFIBStringField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'WHUNITNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object taChangeAssortByApplAUNITNAME: TFIBStringField
      DisplayLabel = #1045#1076'. '#1080#1079#1084'.'
      FieldName = 'AUNITNAME'
      Size = 10
      EmptyStrToNull = True
    end
    object taChangeAssortByApplWHQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'WHQ'
    end
    object taChangeAssortByApplQ: TFIBIntegerField
      DisplayLabel = #1050#1086#1083'-'#1074#1086
      FieldName = 'Q'
    end
    object taChangeAssortByApplD: TFIBSmallIntField
      FieldName = 'D'
    end
  end
  object dsrChangeAssortByAppl: TDataSource
    DataSet = taChangeAssortByAppl
    Left = 260
    Top = 200
  end
  object taWhA2s: TpFIBDataSet
    SelectSQL.Strings = (
      'select * from wha2(:Application$ID, :Sub$Model$ID, :Size$ID)')
    CacheModelOptions.BufferChunks = 1000
    OnCalcFields = taWhA2sCalcFields
    Transaction = dm.tr
    Database = dm.db
    Left = 568
    Top = 66
    object taWhA2sSUBMODELTITLE: TFIBStringField
      FieldName = 'SUB$MODEL$TITLE'
      Size = 32
      EmptyStrToNull = True
    end
    object taWhA2sMODELTITLE: TFIBStringField
      FieldName = 'MODEL$TITLE'
      Size = 32
      EmptyStrToNull = True
    end
    object taWhA2sSIZETITLE: TFIBStringField
      FieldName = 'SIZE$TITLE'
      Size = 32
      EmptyStrToNull = True
    end
    object taWhA2sOPERATIONTITLE: TFIBStringField
      FieldName = 'OPERATION$TITLE'
      Size = 64
      EmptyStrToNull = True
    end
    object taWhA2sPLACEOFSTORAGETITLE: TFIBStringField
      FieldName = 'PLACE$OF$STORAGE$TITLE'
      Size = 64
      EmptyStrToNull = True
    end
    object taWhA2sQUANTITY: TFIBIntegerField
      FieldName = 'QUANTITY'
    end
    object taWhA2sTOTALQUANTITY: TFIBIntegerField
      FieldName = 'TOTAL$QUANTITY'
    end
    object taWhA2sDifference: TFIBIntegerField
      FieldKind = fkCalculated
      FieldName = 'Difference'
      Calculated = True
    end
  end
  object dsrWhA22: TDataSource
    DataSet = taWhA2s
    Left = 568
    Top = 116
  end
  object taWha2m: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 616
    Top = 64
    object taWha2mSUBMODELTITLE: TStringField
      FieldName = 'SUB$MODEL$TITLE'
      Size = 32
    end
    object taWha2mMODELTITLE: TStringField
      FieldName = 'MODEL$TITLE'
      Size = 32
    end
    object taWha2mSIZETITLE: TStringField
      FieldName = 'SIZE$TITLE'
      Size = 32
    end
    object taWha2mOPERATIONTITLE: TStringField
      FieldName = 'OPERATION$TITLE'
      Size = 64
    end
    object taWha2mPLACEOFSTORAGETITLE: TStringField
      FieldName = 'PLACE$OF$STORAGE$TITLE'
      Size = 64
    end
    object taWha2mQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
    end
    object taWha2mTOTALQUANTITY: TIntegerField
      FieldName = 'TOTAL$QUANTITY'
    end
    object taWha2mDifference: TIntegerField
      FieldName = 'Difference'
    end
  end
  object taWha2d: TClientDataSet
    Aggregates = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'APPLICATION$ID'
        ParamType = ptInputOutput
      end
      item
        DataType = ftInteger
        Name = 'SUB$MODEL$ID'
        ParamType = ptInputOutput
      end
      item
        DataType = ftInteger
        Name = 'SIZE$ID'
        ParamType = ptInputOutput
      end>
    ProviderName = 'DataSetProvider'
    BeforeOpen = taWha2dBeforeOpen
    AfterOpen = taWha2dAfterOpen
    AfterClose = taWha2dAfterClose
    Left = 616
    Top = 128
    object taWha2dSUBMODELTITLE: TStringField
      FieldName = 'SUB$MODEL$TITLE'
      Size = 32
    end
    object taWha2dMODELTITLE: TStringField
      FieldName = 'MODEL$TITLE'
      Size = 32
    end
    object taWha2dSIZETITLE: TStringField
      FieldName = 'SIZE$TITLE'
      Size = 32
    end
    object taWha2dOPERATIONTITLE: TStringField
      FieldName = 'OPERATION$TITLE'
      Size = 64
    end
    object taWha2dPLACEOFSTORAGETITLE: TStringField
      FieldName = 'PLACE$OF$STORAGE$TITLE'
      Size = 64
    end
    object taWha2dQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
    end
    object taWha2dTOTALQUANTITY: TIntegerField
      FieldName = 'TOTAL$QUANTITY'
    end
    object taWha2dDifference: TIntegerField
      FieldName = 'Difference'
      ReadOnly = True
    end
  end
  object dsrWh2m: TDataSource
    DataSet = taWha2m
    Left = 680
    Top = 64
  end
  object dsrWh2d: TDataSource
    DataSet = taWha2d
    Left = 680
    Top = 128
  end
  object DataSetProvider: TDataSetProvider
    DataSet = taWhA2s
    Left = 616
    Top = 176
  end
  object taApplResp: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE JA_RESP_PERSONS'
      'SET '
      '    START$SEMIS$RESP$PERSON = :START$SEMIS$RESP$PERSON,'
      '    FINISH$SEMIS$RESP$PERSON = :FINISH$SEMIS$RESP$PERSON,'
      '    READY$PROD$RESP$PERSON = :READY$PROD$RESP$PERSON,'
      '    START$SEMIS$DATE = :START$SEMIS$DATE,'
      '    FINISH$SEMIS$DATE = :FINISH$SEMIS$DATE,'
      '    READY$SEMIS$DATE = :READY$SEMIS$DATE'
      'WHERE'
      '    INVID = :OLD_INVID'
      '    ')
    DeleteSQL.Strings = (
      '')
    RefreshSQL.Strings = (
      'select'
      '  InvID, '
      '  Start$Semis$resp$Person, '
      '  Finish$Semis$resp$Person,'
      '  Ready$Prod$Resp$Person, '
      '  Start$semis$date, '
      '  Finish$semis$date, '
      '  Ready$Semis$Date'
      'from'
      '  Ja_Resp_Persons'
      'where( '
      '  InvID = :ID'
      '     ) and (     JA_RESP_PERSONS.INVID = :OLD_INVID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '  InvID, '
      '  Start$Semis$resp$Person, '
      '  Finish$Semis$resp$Person,'
      '  Ready$Prod$Resp$Person, '
      '  Start$semis$date, '
      '  Finish$semis$date, '
      '  Ready$Semis$Date'
      'from'
      '  Ja_Resp_Persons'
      'where'
      '  InvID = :ID')
    BeforeOpen = taApplRespBeforeOpen
    Transaction = trAppl
    Database = dm.db
    Left = 504
    Top = 64
    dcForceOpen = True
    object taApplRespINVID: TFIBIntegerField
      FieldName = 'INVID'
    end
    object taApplRespSTARTSEMISRESPPERSON: TFIBIntegerField
      FieldName = 'START$SEMIS$RESP$PERSON'
    end
    object taApplRespFINISHSEMISRESPPERSON: TFIBIntegerField
      FieldName = 'FINISH$SEMIS$RESP$PERSON'
    end
    object taApplRespREADYPRODRESPPERSON: TFIBIntegerField
      FieldName = 'READY$PROD$RESP$PERSON'
    end
    object taApplRespSTARTSEMISDATE: TFIBDateTimeField
      FieldName = 'START$SEMIS$DATE'
    end
    object taApplRespFINISHSEMISDATE: TFIBDateTimeField
      FieldName = 'FINISH$SEMIS$DATE'
    end
    object taApplRespREADYSEMISDATE: TFIBDateTimeField
      FieldName = 'READY$SEMIS$DATE'
    end
  end
  object dsrApplResp: TDataSource
    DataSet = taApplResp
    Left = 504
    Top = 112
  end
end
