inherited fmeTask: TfmeTask
  Left = 227
  Top = 100
  BorderIcons = [biSystemMenu, biMaximize]
  BorderStyle = bsSingle
  Caption = #1047#1072#1076#1072#1095#1072
  ClientHeight = 615
  ClientWidth = 821
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 821
    Height = 615
    ExplicitWidth = 821
    ExplicitHeight = 615
    object Label1: TLabel [0]
      Left = 8
      Top = 12
      Width = 65
      Height = 13
      Caption = #1044#1086#1082#1091#1084#1077#1085#1090' '#8470
    end
    object Label2: TLabel [1]
      Left = 8
      Top = 34
      Width = 77
      Height = 13
      Caption = #1044#1072#1090#1072' '#1089#1086#1079#1076#1072#1085#1080#1103
    end
    object Label3: TLabel [2]
      Left = 249
      Top = 12
      Width = 19
      Height = 13
      Caption = #1058#1080#1087
    end
    object Label4: TLabel [3]
      Left = 249
      Top = 34
      Width = 73
      Height = 13
      Caption = #1055#1086#1083#1100#1079#1086#1074#1072#1090#1077#1083#1100
    end
    object Label5: TLabel [4]
      Left = 8
      Top = 55
      Width = 93
      Height = 13
      Caption = #1050#1088#1072#1090#1082#1086#1077' '#1086#1087#1080#1089#1072#1085#1080#1077
    end
    object Label6: TLabel [5]
      Left = 8
      Top = 77
      Width = 82
      Height = 13
      Caption = #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103
    end
    object Label7: TLabel [6]
      Left = 249
      Top = 77
      Width = 85
      Height = 13
      Caption = #1050#1086#1101#1092'.'#1089#1083#1086#1078#1085#1086#1089#1090#1080
    end
    object Label8: TLabel [7]
      Left = 8
      Top = 96
      Width = 50
      Height = 13
      Caption = #1054#1087#1080#1089#1072#1085#1080#1077
    end
    inherited btnOk: TBitBtn
      Left = 632
      Top = 578
      Anchors = [akRight, akBottom]
      ExplicitLeft = 632
      ExplicitTop = 578
    end
    inherited btnCancel: TBitBtn
      Left = 724
      Top = 578
      Anchors = [akRight, akBottom]
      ExplicitLeft = 724
      ExplicitTop = 578
    end
    object edN: TDBEditEh
      Left = 105
      Top = 8
      Width = 135
      Height = 19
      AlwaysShowBorder = True
      DataField = 'N'
      DataSource = dmData.dsrTaskList
      EditButtons = <>
      Flat = True
      TabOrder = 2
      Visible = True
    end
    object edDate: TDBDateTimeEditEh
      Left = 105
      Top = 30
      Width = 135
      Height = 19
      AlwaysShowBorder = True
      DataField = 'DAT'
      DataSource = dmData.dsrTaskList
      EditButtons = <>
      Flat = True
      Kind = dtkDateEh
      TabOrder = 3
      Visible = True
    end
    object edBriefDescr: TDBEditEh
      Left = 105
      Top = 52
      Width = 392
      Height = 19
      AlwaysShowBorder = True
      Ctl3D = True
      DataField = 'BRIEFDESCR'
      DataSource = dmData.dsrTaskList
      EditButtons = <>
      Flat = True
      ParentCtl3D = False
      TabOrder = 4
      Visible = True
    end
    object edK: TDBEditEh
      Left = 337
      Top = 74
      Width = 160
      Height = 19
      AlwaysShowBorder = True
      DataField = 'K'
      DataSource = dmData.dsrTaskList
      EditButtons = <>
      Flat = True
      TabOrder = 5
      Visible = True
    end
    object pc: TcxPageControl
      Left = 8
      Top = 224
      Width = 803
      Height = 345
      ActivePage = cxTabSheet1
      Anchors = [akLeft, akTop, akRight, akBottom]
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
      TabOrder = 6
      ClientRectBottom = 344
      ClientRectLeft = 1
      ClientRectRight = 802
      ClientRectTop = 21
      object cxTabSheet1: TcxTabSheet
        Caption = #1054#1087#1077#1088#1072#1094#1080#1080
        ImageIndex = 0
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGridEh1: TDBGridEh
          Left = 0
          Top = 29
          Width = 473
          Height = 294
          Align = alClient
          DataGrouping.GroupLevels = <>
          DataSource = dmData.dsTaskOperations
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
          RowDetailPanel.Color = clBtnFace
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              EditButtons = <>
              FieldName = 'TASKOPERNAME'
              Footers = <>
              Width = 180
            end
            item
              EditButtons = <>
              FieldName = 'STATE'
              Footers = <>
            end
            item
              EditButtons = <>
              FieldName = 'GIVEDATE'
              Footers = <>
            end
            item
              EditButtons = <>
              FieldName = 'DONEDATE'
              Footers = <>
            end
            item
              EditButtons = <>
              FieldName = 'DEPNAME'
              Footers = <>
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
        object sbOperations: TSpeedBar
          Left = 0
          Top = 0
          Width = 801
          Height = 29
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          BoundLines = [blTop, blBottom, blLeft, blRight]
          Options = [sbAllowDrag, sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
          BtnOffsetHorz = 3
          BtnOffsetVert = 3
          BtnWidth = 48
          BtnHeight = 23
          TabOrder = 1
          InternalVer = 1
          object SpeedbarSection1: TSpeedbarSection
            Caption = 'Untitled (0)'
          end
          object SpeedItem1: TSpeedItem
            Action = acGive
            BtnCaption = #1042#1099#1076#1072#1090#1100
            Caption = 'SpeedItem1'
            Spacing = 1
            Left = 3
            Top = 3
            Visible = True
            OnClick = acGiveExecute
            SectionName = 'Untitled (0)'
          end
          object SpeedItem2: TSpeedItem
            Action = acReturn
            BtnCaption = #1042#1077#1088#1085#1091#1090#1100
            Caption = 'SpeedItem2'
            Spacing = 1
            Left = 51
            Top = 3
            Visible = True
            OnClick = acReturnExecute
            SectionName = 'Untitled (0)'
          end
          object SpeedItem3: TSpeedItem
            Action = acSignAdd
            BtnCaption = #1044#1086#1073'.'#1087#1086#1076#1087#1080#1089#1100
            Caption = #1044#1086#1073'.'#1087#1086#1076#1087#1080#1089#1100
            Spacing = 1
            Left = 99
            Top = 3
            Visible = True
            OnClick = acSignAddExecute
            SectionName = 'Untitled (0)'
          end
          object SpeedItem4: TSpeedItem
            Action = acSignDelete
            BtnCaption = #1059#1076'.'#1087#1086#1076#1087#1080#1089#1100
            Caption = 'SpeedItem4'
            Spacing = 1
            Left = 147
            Top = 3
            Visible = True
            OnClick = acSignDeleteExecute
            SectionName = 'Untitled (0)'
          end
        end
        object cxSplitter1: TcxSplitter
          Left = 473
          Top = 29
          Width = 8
          Height = 294
          HotZoneClassName = 'TcxXPTaskBarStyle'
          AlignSplitter = salRight
          Control = DBGridEh2
        end
        object DBGridEh2: TDBGridEh
          Left = 481
          Top = 29
          Width = 320
          Height = 294
          Align = alRight
          DataGrouping.GroupLevels = <>
          DataSource = dmData.dsTaskOperSigns
          Flat = True
          FooterColor = clWindow
          FooterFont.Charset = DEFAULT_CHARSET
          FooterFont.Color = clWindowText
          FooterFont.Height = -11
          FooterFont.Name = 'MS Sans Serif'
          FooterFont.Style = []
          OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove]
          RowDetailPanel.Color = clBtnFace
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              EditButtons = <>
              FieldName = 'DAT'
              Footers = <>
            end
            item
              EditButtons = <>
              FieldName = 'MOLNAME'
              Footers = <>
              Width = 200
            end
            item
              EditButtons = <>
              FieldName = 'COMENT'
              Footers = <>
              Width = 200
            end>
          object RowDetailData: TRowDetailPanelControlEh
          end
        end
      end
      object cxTabSheet2: TcxTabSheet
        Caption = #1048#1079#1086#1073#1088#1072#1078#1077#1085#1080#1103
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object pcImg: TcxPageControl
          Left = 0
          Top = 0
          Width = 803
          Height = 322
          Align = alClient
          PopupMenu = pmImg
          TabOrder = 0
          ClientRectBottom = 322
          ClientRectRight = 803
          ClientRectTop = 0
        end
      end
    end
    object edType: TDBLookupComboboxEh
      Left = 337
      Top = 8
      Width = 160
      Height = 19
      AlwaysShowBorder = True
      DataField = 'TTYPE'
      DataSource = dmData.dsrTaskList
      EditButtons = <>
      Flat = True
      KeyField = 'ID'
      ListField = 'NAME'
      ListSource = dmData.dsrLookupTaskType
      TabOrder = 7
      Visible = True
      OnDropDownBoxGetCellParams = edTypeDropDownBoxGetCellParams
    end
    object edUser: TDBEditEh
      Left = 337
      Top = 30
      Width = 160
      Height = 19
      AlwaysShowBorder = True
      DataField = 'CREATEBYUSER'
      DataSource = dmData.dsrTaskList
      EditButtons = <>
      Enabled = False
      Flat = True
      TabOrder = 8
      Visible = True
    end
    object edDescr: TcxDBMemo
      Left = 8
      Top = 112
      Anchors = [akLeft, akTop, akRight]
      DataBinding.DataField = 'DESCR'
      DataBinding.DataSource = dmData.dsrTaskList
      Style.BorderStyle = ebsFlat
      StyleFocused.BorderStyle = ebsFlat
      StyleHot.BorderStyle = ebsFlat
      TabOrder = 9
      Height = 105
      Width = 804
    end
    object edDep: TDBEditEh
      Left = 105
      Top = 74
      Width = 121
      Height = 19
      AlwaysShowBorder = True
      DataField = 'DEPNAME'
      DataSource = dmData.dsrTaskList
      EditButtons = <>
      Enabled = False
      Flat = True
      TabOrder = 10
      Visible = True
    end
  end
  object pmImg: TPopupMenu
    Left = 416
    Top = 264
    object N1: TMenuItem
      Action = acImgAdd
    end
    object N2: TMenuItem
      Action = acImgRemove
    end
    object N3: TMenuItem
      Action = acImgRename
    end
  end
  object ActionList1: TActionList
    Left = 448
    Top = 264
    object acGive: TAction
      Caption = #1042#1099#1076#1072#1090#1100
      OnExecute = acGiveExecute
    end
    object acReturn: TAction
      Caption = #1042#1077#1088#1085#1091#1090#1100
      OnExecute = acReturnExecute
    end
    object acSignAdd: TAction
      Caption = #1044#1086#1073'.'#1087#1086#1076#1087#1080#1089#1100
      OnExecute = acSignAddExecute
      OnUpdate = acSignAddUpdate
    end
    object acSignDelete: TAction
      Caption = #1059#1076'.'#1087#1086#1076#1087#1080#1089#1100
      OnExecute = acSignDeleteExecute
      OnUpdate = acSignDeleteUpdate
    end
    object acImgAdd: TAction
      Category = 'Img'
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
      OnExecute = acImgAddExecute
    end
    object acImgRemove: TAction
      Category = 'Img'
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
      OnExecute = acImgRemoveExecute
      OnUpdate = acImgRemoveUpdate
    end
    object acImgRename: TAction
      Category = 'Img'
      Caption = #1055#1077#1088#1077#1080#1084#1077#1085#1086#1074#1072#1090#1100' '#1080#1079#1086#1073#1088#1072#1078#1077#1085#1080#1077
      OnExecute = acImgRenameExecute
      OnUpdate = acImgRenameUpdate
    end
  end
end
