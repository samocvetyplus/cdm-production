object ItemCostChangeWithJewellery: TItemCostChangeWithJewellery
  Left = 317
  Top = 435
  BorderStyle = bsToolWindow
  Caption = #1056#1072#1089#1095#1077#1090' '#1094#1077#1085
  ClientHeight = 300
  ClientWidth = 383
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TDBGridEh
    Left = 8
    Top = 8
    Width = 289
    Height = 257
    DataSource = DataSource
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghDialogFind]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDrawColumnCell = GridDrawColumnCell
    OnTitleBtnClick = GridTitleBtnClick
    OnTitleClick = GridTitleClick
    Columns = <
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'Check'
        Footers = <>
        Width = 17
      end
      item
        EditButtons = <>
        FieldName = 'Model'
        Footers = <>
        Width = 92
      end
      item
        EditButtons = <>
        FieldName = 'SubModel'
        Footers = <>
        Width = 97
      end
      item
        EditButtons = <>
        FieldName = 'Cost'
        Footers = <>
      end>
  end
  object Button3: TButton
    Left = 304
    Top = 8
    Width = 75
    Height = 25
    Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
    TabOrder = 1
  end
  object Button4: TButton
    Left = 304
    Top = 40
    Width = 73
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
  end
  object DataSource: TDataSource
    DataSet = DataSet
    Left = 16
    Top = 112
  end
  object DataSet: TClientDataSet
    Active = True
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'Check'
        DataType = ftInteger
      end
      item
        Name = 'Model'
        DataType = ftString
        Size = 32
      end
      item
        Name = 'SubModel'
        DataType = ftString
        Size = 32
      end
      item
        Name = 'Cost'
        DataType = ftCurrency
      end>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 16
    Top = 80
    Data = {
      800000009619E0BD010000001800000004000000000003000000800005436865
      636B0400010000000000054D6F64656C01004900000001000557494454480200
      02002000085375624D6F64656C01004900000001000557494454480200020020
      0004436F7374080004000000010007535542545950450200490006004D6F6E65
      79000000}
    object DataSetCheck: TIntegerField
      FieldName = 'Check'
    end
    object DataSetModel: TStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083'| '
      DisplayWidth = 16
      FieldName = 'Model'
      Size = 32
    end
    object DataSetSubModel: TStringField
      DisplayLabel = #1040#1088#1090#1080#1082#1091#1083'| '
      DisplayWidth = 32
      FieldName = 'SubModel'
      Size = 32
    end
    object DataSetCost: TCurrencyField
      DisplayLabel = #1062#1077#1085#1072
      FieldName = 'Cost'
    end
  end
end
