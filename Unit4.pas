unit Unit4;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGrids, DB;

type
  TForm4 = class(TForm)
    dsItems: TDataSource;
    dsGroups: TDataSource;
    grGroups: TDBGrid;
    grItems: TDBGrid;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form4: TForm4;

implementation

uses MainData, DictData, MsgDialog, ProtSJInfo, fmUtils, PrintData,
     DictAncestor, DbUtilsEh, ApplData, ADistr, eSQ, dDepT1, dbTree, Editor,
     ProductionConsts, frmAffiangePassport, dmReportInventory, PrintPersonalInfo;
{$R *.dfm}

end.
