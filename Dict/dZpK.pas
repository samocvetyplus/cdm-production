unit dZpK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList,  ImgList,  DBGridEh,
  Buttons, Grids, DBGrids, RXDBCtrl, ExtCtrls,
  ComCtrls, TB2Dock, TB2Toolbar, StdCtrls, Mask, DBCtrlsEh, TB2Item,
  DB, pFIBDataSet, M207Ctrls, FIBDataSet, DBGridEhGrouping, rxPlacemnt,
  rxSpeedbar, GridsEh;

type
  TfmdZpK = class(TfmDictAncestor)
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    Splitter1: TSplitter;
    taSemis: TpFIBDataSet;
    dsrSemis: TDataSource;
    taSemisSEMIS: TFIBStringField;
    taTech: TpFIBDataSet;
    dsrTech: TDataSource;
    taTechNAME: TFIBStringField;
    taIns: TpFIBDataSet;
    dsrIns: TDataSource;
    taInsNAME: TFIBStringField;
    pl: TPanel;
    lbW: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    bvlW: TM207Bevel;
    M207Bevel1: TM207Bevel;
    M207Bevel2: TM207Bevel;
    M207Bevel3: TM207Bevel;
    edMin: TDBNumberEditEh;
    edMin1: TDBNumberEditEh;
    edMax1: TDBNumberEditEh;
    edK1: TDBNumberEditEh;
    edK2: TDBNumberEditEh;
    edMax: TDBNumberEditEh;
    edK3: TDBNumberEditEh;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    DBGridEh3: TDBGridEh;
    BitBtn1: TBitBtn;
    TBControlItem1: TTBControlItem;
    Label2: TLabel;
    TBControlItem2: TTBControlItem;
    cmbxOperation: TDBComboBoxEh;
    taSemisID: TIntegerField;
    taSemisK: TFloatField;
    taTechK: TFloatField;
    taTechID: TIntegerField;
    taInsID: TIntegerField;
    taInsK: TFloatField;
    taZpK_W: TpFIBDataSet;
    taZpK_WID: TIntegerField;
    taZpK_WMINW: TFloatField;
    taZpK_WMAXW: TFloatField;
    taZpK_WK1: TFloatField;
    taZpK_WK2: TFloatField;
    taZpK_WK3: TFloatField;
    dsrZpK_W: TDataSource;
    taZpK_WOPERID: TFIBStringField;
    Label1: TLabel;
    TBSeparatorItem1: TTBSeparatorItem;
    TBControlItem3: TTBControlItem;
    Label9: TLabel;
    edFilterArt: TDBEditEh;
    TBControlItem4: TTBControlItem;
    SpeedItem4: TSpeedItem;
    acPrint: TAction;
    procedure FormCreate(Sender: TObject);
    procedure cmbxOperationChange(Sender: TObject);
    procedure taZpK_WBeforeOpen(DataSet: TDataSet);
    procedure taSemisBeforeOpen(DataSet: TDataSet);
    procedure taTechBeforeOpen(DataSet: TDataSet);
    procedure taInsBeforeOpen(DataSet: TDataSet);
    procedure CommitRetainig(DataSet: TDataSet);
    procedure taZpK_WNewRecord(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edFilterArtChange(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
  end;

var
  fmdZpK: TfmdZpK;

implementation

uses DictData, dbTree, dbUtil, MainData, PrintData, MsgDialog;

{$R *.dfm}

procedure TfmdZpK.FormCreate(Sender: TObject);
begin
  // ���������������� �������� �������������
  cmbxOperation.OnChange := nil;
  cmbxOperation.Items.Assign(dm.dOper);
  cmbxOperation.Items.Delete(0);
  cmbxOperation.ItemIndex := 0;
  dm.AOPerId := TNodeData(cmbxOperation.Items.Objects[cmbxOperation.ItemIndex]).Code;
  cmbxOperation.OnChange := cmbxOperationChange;
  // ����������
  dm.taZPK.Filtered := False;
  inherited;
  ReOpenDataSets([taSemis, taTech, taIns, taZpK_W]);
end;

procedure TfmdZpK.cmbxOperationChange(Sender: TObject);
begin
  PostDataSets([taSemis, taTech, taIns, taZpK_W, DataSet]);
  dm.AOPerId := TNodeData(cmbxOperation.Items.Objects[cmbxOperation.ItemIndex]).Code;
  ReOpenDataSets([taSemis, taTech, taIns, taZpK_W, DataSet]);
end;

procedure TfmdZpK.taZpK_WBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['OPERID'].AsString := TNodeData(cmbxOperation.Items.Objects[cmbxOperation.ItemIndex]).Code;
end;

procedure TfmdZpK.taSemisBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['OPERID'].AsString := TNodeData(cmbxOperation.Items.Objects[cmbxOperation.ItemIndex]).Code;
end;

procedure TfmdZpK.taTechBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['OPERID'].AsString := TNodeData(cmbxOperation.Items.Objects[cmbxOperation.ItemIndex]).Code;
end;

procedure TfmdZpK.taInsBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['OPERID'].AsString := TNodeData(cmbxOperation.Items.Objects[cmbxOperation.ItemIndex]).Code;
end;

procedure TfmdZpK.CommitRetainig(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure TfmdZpK.taZpK_WNewRecord(DataSet: TDataSet);
begin
  taZpK_WID.AsInteger := dm.GetId(74);
  taZpK_WMINW.AsFloat := 1;
  taZpK_WMAXW.AsFloat := 3;
  taZpK_WK1.AsFloat := 0;
  taZpK_WK2.AsFloat := 0;
  taZpK_WK3.AsFloat := 0;
  taZpK_WOPERID.AsString := TNodeData(cmbxOperation.Items.Objects[cmbxOperation.ItemIndex]).Code;
end;

procedure TfmdZpK.BitBtn1Click(Sender: TObject);
begin
  PostDataSets([taSemis, taTech, taIns, taZpK_W, dm.taZPK]);
  ExecSQL('execute procedure Build_ZpK("'+TNodeData(cmbxOperation.Items.Objects[cmbxOperation.ItemIndex]).Code+'")', dmMain.quTmp);
  ReOpenDataSet(DataSet);
end;

procedure TfmdZpK.PostBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure TfmdZpK.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PostDataSets([taSemis, taTech, taIns, taZpK_W]);
  CloseDataSets([taSemis, taTech, taIns, taZpK_W]);
  inherited;
  dm.taZPK.Filtered := True;
  dm.taZPK.OnFilterRecord := nil;
end;

procedure TfmdZpK.edFilterArtChange(Sender: TObject);
begin
  if(edFilterArt.Text='')then dm.taZpK.Filtered := False
  else begin
    dm.taZpK.DisableScrollEvents;
    dm.taZpK.DisableControls;
    try
      dm.taZpK.Filtered := False;
      dm.taZpK.Filter := 'ART='#39+edFilterArt.Text+'*'+#39;
      dm.taZpK.Filtered := True;
    finally
      dm.taZpK.EnableScrollEvents;
      dm.taZpK.EnableControls;
    end;
  end;
end;

procedure TfmdZpK.acPrintExecute(Sender: TObject);
var
  OperId : string;
begin
  if cmbxOperation.ItemIndex=-1 then
  begin
    ActiveControl := cmbxOperation;
    MessageDialogA('�������� ��������', mtError);
    eXit;
  end;
  OperId := TNodeData(cmbxOperation.Items.Objects[cmbxOperation.ItemIndex]).Code;
  dmPrint.PrintDocumentA(dkZpK, VarArrayOf([
                                  VarArrayOf([OperId]),
                                  VarArrayOf([OperId]),
                                  VarArrayOf([OperId]),
                                  VarArrayOf([OperId]),
                                  VarArrayOf([OperId, edFilterArt.Text+'%'])]));
end;

end.
