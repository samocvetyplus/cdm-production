inherited fmeTaskImage: TfmeTaskImage
  Left = 733
  Top = 590
  Caption = 'fmeTaskImage'
  ClientHeight = 94
  ClientWidth = 361
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 361
    Height = 94
    object Label1: TLabel [0]
      Left = 8
      Top = 8
      Width = 76
      Height = 13
      Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
    end
    object Label2: TLabel [1]
      Left = 8
      Top = 32
      Width = 29
      Height = 13
      Caption = #1060#1072#1081#1083
    end
    inherited btnOk: TBitBtn
      Left = 178
      Top = 59
      Anchors = [akRight, akBottom]
      Enabled = False
    end
    inherited btnCancel: TBitBtn
      Left = 270
      Top = 59
      Anchors = [akRight, akBottom]
    end
    object edImgName: TDBEditEh
      Left = 88
      Top = 6
      Width = 265
      Height = 19
      AlwaysShowBorder = True
      Ctl3D = True
      EditButtons = <>
      Flat = True
      ParentCtl3D = False
      TabOrder = 2
      Visible = True
      OnChange = edImgNameChange
    end
    object edImgFile: TDBEditEh
      Left = 88
      Top = 29
      Width = 265
      Height = 19
      AlwaysShowBorder = True
      Ctl3D = True
      EditButtons = <
        item
          Style = ebsEllipsisEh
          OnClick = edImgFileEditButtons0Click
        end>
      Flat = True
      ParentCtl3D = False
      TabOrder = 3
      Visible = True
      OnChange = edImgNameChange
    end
  end
  object dlg: TOpenPictureDialog
    Filter = 'JPEG Image File (*.jpg,*.jpeg)|*.jpg;*.jpeg'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 8
    Top = 56
  end
end
