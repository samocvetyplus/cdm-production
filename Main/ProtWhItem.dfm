inherited fmProtWhItem: TfmProtWhItem
  Left = 140
  Top = 160
  Width = 808
  Height = 580
  Caption = #1044#1077#1090#1072#1083#1080#1079#1072#1094#1080#1103' '#1087#1086' '#1082#1083#1072#1076#1086#1074#1086#1081'.'
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label18: TLabel [0]
    Left = 440
    Top = 13
    Width = 34
    Height = 13
    Caption = #1050#1086#1083'-'#1074#1086
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label19: TLabel [1]
    Left = 492
    Top = 13
    Width = 19
    Height = 13
    Caption = #1042#1077#1089
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clNavy
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  inherited stbrStatus: TStatusBar
    Top = 527
    Width = 800
  end
  inherited tb1: TSpeedBar
    Width = 800
    inherited spitExit: TSpeedItem
      Left = 499
    end
    object SpeedItem1: TSpeedItem
      Action = acPrintGrid
      BtnCaption = #1055#1077#1095#1072#1090#1100
      Caption = #1055#1077#1095#1072#1090#1100
      Spacing = 1
      Left = 3
      Top = 3
      Visible = True
      OnClick = acPrintGridExecute
      SectionName = 'Untitled (0)'
    end
  end
  object gridWhItem: TDBGridEh [4]
    Left = 0
    Top = 225
    Width = 800
    Height = 302
    Align = alClient
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
    Color = clBtnFace
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmMain.dsrProtWHItem
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghColumnResize, dghColumnMove]
    PopupMenu = ppWhItem
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    OnGetCellParams = gridWhItemGetCellParams
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1053#1086#1084#1077#1088
        Width = 39
      end
      item
        DisplayFormat = 'dd.mm.yy'
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1044#1072#1090#1072
        Width = 51
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WORDERNO'
        Footers = <>
        Title.Caption = #1053#1072#1088#1103#1076'|'#1053#1086#1084#1077#1088
        Width = 38
      end
      item
        DisplayFormat = 'dd.mm.yy'
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WORDERDATE'
        Footers = <>
        Title.Caption = #1053#1072#1088#1103#1076'|'#1044#1072#1090#1072' '
        Width = 41
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEPFROMNAME'
        Footers = <>
        Width = 90
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FROMOPERNAME'
        Footers = <>
        Width = 84
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footer.FieldName = 'Q'
        Footer.ValueType = fvtSum
        Footers = <>
        Width = 42
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Width = 42
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'T'
        Footers = <>
        Title.Caption = #1054#1087#1080#1089#1072#1085#1080#1077'| '
        Width = 40
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'T1'
        Footers = <>
        Title.Caption = #1054#1087#1080#1089#1072#1085#1080#1077'| '
        Width = 68
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'UQ'
        Footers = <>
        Title.Caption = #1045#1076'. '#1080#1079#1084'.|'#1082#1086#1083'-'#1074#1072
        Width = 41
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'UW'
        Footers = <>
        Title.Caption = #1045#1076'. '#1080#1079#1084'.|'#1074#1077#1089#1072
        Width = 35
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Width = 69
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SEMISNAME'
        Footers = <>
        Width = 81
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OPERNAME'
        Footers = <>
        Width = 63
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object TBDock1: TTBDock [5]
    Left = 0
    Top = 42
    Width = 800
    Height = 23
    object TBToolbar1: TTBToolbar
      Left = 0
      Top = 0
      Caption = 'TBToolbar1'
      TabOrder = 0
      object TBControlItem1: TTBControlItem
        Control = lbType
      end
      object TBControlItem2: TTBControlItem
        Control = cmbxType
      end
      object lbType: TLabel
        Left = 0
        Top = 3
        Width = 78
        Height = 13
        Caption = #1058#1080#1087' '#1076#1074#1080#1078#1077#1085#1080#1103': '
      end
      object cmbxType: TDBComboBoxEh
        Left = 78
        Top = 0
        Width = 121
        Height = 19
        EditButtons = <>
        Flat = True
        Items.Strings = (
          '*'#1042#1089#1077
          #1055#1086#1089#1090#1072#1074#1082#1072
          #1056#1072#1089#1093#1086#1076
          #1042#1085#1091#1090#1088#1077#1085#1085#1080#1077' '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077
          #1057' '#1084#1077#1089#1090#1072#1084#1080' '#1093#1088#1072#1085#1077#1085#1080#1103
          #1040#1082#1090' '#1089#1089#1099#1087#1082#1080
          #1040#1092#1092#1080#1085#1072#1078' '#1089#1098#1077#1084#1072
          #1040#1092#1092#1080#1085#1072#1078' '#1083#1086#1084#1072)
        KeyItems.Strings = (
          '-1'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7')
        TabOrder = 0
        Visible = True
        OnDblClick = cmbxTypeDblClick
      end
    end
  end
  object Panel1: TPanel [6]
    Left = 0
    Top = 65
    Width = 800
    Height = 160
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object Label1: TLabel
      Left = 8
      Top = 48
      Width = 39
      Height = 13
      Caption = #1042#1099#1076#1072#1085#1086
    end
    object M207Bevel1: TM207Bevel
      Left = 4
      Top = 20
      Width = 365
      Height = 13
      Shape = bsTopLine
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1077#1078#1076#1091' '#1082#1083#1072#1076#1086#1074#1086#1081' '#1080' '#1084#1077#1089#1090#1072#1084#1080' '#1093#1088#1072#1085#1077#1085#1080#1103
      CaptionOffs = 6
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      BorderWidth = 0
    end
    object Label2: TLabel
      Left = 8
      Top = 64
      Width = 56
      Height = 13
      Caption = #1044#1086#1088#1072#1073#1086#1090#1082#1072
    end
    object Label3: TLabel
      Left = 176
      Top = 48
      Width = 32
      Height = 13
      Caption = #1053#1072#1088#1103#1076
    end
    object M207Bevel2: TM207Bevel
      Left = 364
      Top = 20
      Width = 417
      Height = 13
      Shape = bsTopLine
      Caption = #1044#1074#1080#1078#1077#1085#1080#1077' '#1084#1077#1078#1076#1091' '#1082#1083#1072#1076#1086#1074#1099#1084#1080' '#1080' '#1102#1088'. '#1083#1080#1094#1072#1084#1080
      CaptionOffs = 6
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      BorderWidth = 0
    end
    object M207Bevel3: TM207Bevel
      Left = 337
      Top = 26
      Width = 7
      Height = 115
      Shape = bsRightLine
      CaptionOffs = 6
      BorderWidth = 0
    end
    object Label4: TLabel
      Left = 176
      Top = 64
      Width = 42
      Height = 13
      Caption = #1042#1086#1079#1074#1088#1072#1090
    end
    object Label5: TLabel
      Left = 176
      Top = 80
      Width = 25
      Height = 13
      Caption = #1041#1088#1072#1082
    end
    object Label6: TLabel
      Left = 176
      Top = 96
      Width = 63
      Height = 13
      Caption = #1053#1077#1082#1086#1084#1087#1083#1077#1082#1090
    end
    object Label7: TLabel
      Left = 348
      Top = 48
      Width = 49
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1082#1072
    end
    object Label8: TLabel
      Left = 348
      Top = 80
      Width = 106
      Height = 26
      Caption = #1042#1085#1091#1090#1088'. '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077#13#10#1074' '#1082#1083#1072#1076#1086#1074#1091#1102
    end
    object Label9: TLabel
      Left = 572
      Top = 108
      Width = 59
      Height = 13
      Caption = #1040#1082#1090' '#1089#1089#1099#1087#1082#1080
    end
    object Label10: TLabel
      Left = 572
      Top = 48
      Width = 49
      Height = 13
      Caption = #1057#1087#1080#1089#1072#1085#1080#1077
    end
    object Label11: TLabel
      Left = 572
      Top = 80
      Width = 106
      Height = 26
      Caption = #1042#1085#1091#1090#1088'. '#1087#1077#1088#1077#1084#1077#1097#1077#1085#1080#1077#13#10#1080#1079' '#1082#1083#1072#1076#1086#1074#1086#1081
    end
    object Label12: TLabel
      Left = 72
      Top = 33
      Width = 34
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label13: TLabel
      Left = 120
      Top = 33
      Width = 19
      Height = 13
      Caption = #1042#1077#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 244
      Top = 33
      Width = 34
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 292
      Top = 33
      Width = 19
      Height = 13
      Caption = #1042#1077#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbGetQ: TLabel
      Left = 72
      Top = 48
      Width = 33
      Height = 13
      Caption = 'lbGetQ'
    end
    object lbGetW: TLabel
      Left = 120
      Top = 48
      Width = 36
      Height = 13
      Caption = 'lbGetW'
    end
    object lbReworkQ: TLabel
      Left = 72
      Top = 64
      Width = 53
      Height = 13
      Caption = 'lbReworkQ'
    end
    object lbReworkW: TLabel
      Left = 120
      Top = 64
      Width = 56
      Height = 13
      Caption = 'lbReworkW'
    end
    object lbDoneQ: TLabel
      Left = 244
      Top = 48
      Width = 42
      Height = 13
      Caption = 'lbDoneQ'
    end
    object lbDoneW: TLabel
      Left = 292
      Top = 48
      Width = 45
      Height = 13
      Caption = 'lbDoneW'
    end
    object lbRetQ: TLabel
      Left = 244
      Top = 64
      Width = 33
      Height = 13
      Caption = 'lbRetQ'
    end
    object lbRetW: TLabel
      Left = 292
      Top = 64
      Width = 36
      Height = 13
      Caption = 'lbRetW'
    end
    object lbRejQ: TLabel
      Left = 244
      Top = 80
      Width = 32
      Height = 13
      Caption = 'lbRejQ'
    end
    object lbRejW: TLabel
      Left = 292
      Top = 80
      Width = 35
      Height = 13
      Caption = 'lbRejW'
    end
    object lbNkQ: TLabel
      Left = 244
      Top = 96
      Width = 30
      Height = 13
      Caption = 'lbNkQ'
    end
    object lbNkW: TLabel
      Left = 292
      Top = 96
      Width = 33
      Height = 13
      Caption = 'lbNkW'
    end
    object Label16: TLabel
      Left = 464
      Top = 33
      Width = 34
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label17: TLabel
      Left = 516
      Top = 33
      Width = 19
      Height = 13
      Caption = #1042#1077#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object M207Bevel4: TM207Bevel
      Left = 168
      Top = 48
      Width = 5
      Height = 65
      Shape = bsRightLine
      CaptionOffs = 6
      BorderWidth = 0
    end
    object lbInQ: TLabel
      Left = 464
      Top = 48
      Width = 25
      Height = 13
      Caption = 'lbInQ'
    end
    object lbMoveInQ: TLabel
      Left = 464
      Top = 80
      Width = 52
      Height = 13
      Caption = 'lbMoveInQ'
    end
    object lbBulkQ: TLabel
      Left = 696
      Top = 108
      Width = 37
      Height = 13
      Caption = 'lbBulkQ'
    end
    object lbInW: TLabel
      Left = 516
      Top = 48
      Width = 28
      Height = 13
      Caption = 'lbInW'
    end
    object lbMoveInW: TLabel
      Left = 516
      Top = 80
      Width = 55
      Height = 13
      Caption = 'lbMoveInW'
    end
    object lbBulkW: TLabel
      Left = 744
      Top = 108
      Width = 40
      Height = 13
      Caption = 'lbBulkW'
    end
    object M207Bevel5: TM207Bevel
      Left = 564
      Top = 48
      Width = 5
      Height = 93
      Shape = bsRightLine
      CaptionOffs = 6
      BorderWidth = 0
    end
    object Label20: TLabel
      Left = 696
      Top = 33
      Width = 34
      Height = 13
      Caption = #1050#1086#1083'-'#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label21: TLabel
      Left = 744
      Top = 33
      Width = 19
      Height = 13
      Caption = #1042#1077#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbOutQ: TLabel
      Left = 696
      Top = 48
      Width = 33
      Height = 13
      Caption = 'lbOutQ'
    end
    object lbOutW: TLabel
      Left = 744
      Top = 48
      Width = 36
      Height = 13
      Caption = 'lbOutW'
    end
    object lbMoveOutQ: TLabel
      Left = 696
      Top = 80
      Width = 60
      Height = 13
      Caption = 'lbMoveOutQ'
    end
    object lbMoveOutW: TLabel
      Left = 744
      Top = 80
      Width = 63
      Height = 13
      Caption = 'lbMoveOutW'
    end
    object Label22: TLabel
      Left = 348
      Top = 108
      Width = 85
      Height = 13
      Caption = #1040#1092#1092#1080#1085#1072#1078' '#1089#1098#1077#1084#1072
    end
    object Label23: TLabel
      Left = 348
      Top = 124
      Width = 113
      Height = 13
      Caption = #1040#1092#1092#1080#1085#1072#1078' '#1083#1086#1084#1072' '#1087#1088#1080#1077#1084
    end
    object Label24: TLabel
      Left = 572
      Top = 124
      Width = 118
      Height = 13
      Caption = #1040#1092#1092#1080#1085#1072#1078' '#1083#1086#1084#1072' '#1074#1099#1076#1072#1095#1072
    end
    object lbAffSQ: TLabel
      Left = 464
      Top = 108
      Width = 36
      Height = 13
      Caption = 'lbAffSQ'
    end
    object lbAffSW: TLabel
      Left = 516
      Top = 108
      Width = 39
      Height = 13
      Caption = 'lbAffSW'
    end
    object lbAffLRetQ: TLabel
      Left = 464
      Top = 124
      Width = 52
      Height = 13
      Caption = 'lbAffLRetQ'
    end
    object lbAffLRetW: TLabel
      Left = 516
      Top = 124
      Width = 55
      Height = 13
      Caption = 'lbAffLRetW'
    end
    object lbAffLGetQ: TLabel
      Left = 696
      Top = 124
      Width = 52
      Height = 13
      Caption = 'lbAffLGetQ'
    end
    object lbAffLGetW: TLabel
      Left = 744
      Top = 124
      Width = 55
      Height = 13
      Caption = 'lbAffLGetW'
    end
    object Label25: TLabel
      Left = 348
      Top = 64
      Width = 98
      Height = 13
      Caption = #1042#1086#1079#1074#1088#1072#1090' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
    end
    object lbRetProdQ: TLabel
      Left = 464
      Top = 64
      Width = 55
      Height = 13
      Caption = 'lbRetProdQ'
    end
    object lbRetProdW: TLabel
      Left = 516
      Top = 64
      Width = 58
      Height = 13
      Caption = 'lbRetProdW'
    end
    object Label26: TLabel
      Left = 572
      Top = 64
      Width = 103
      Height = 13
      Caption = #1054#1090#1075#1088#1091#1079#1082#1072' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
    end
    object lbOutProdQ: TLabel
      Left = 696
      Top = 64
      Width = 55
      Height = 13
      Caption = 'lbOutProdQ'
    end
    object lbOutProdW: TLabel
      Left = 744
      Top = 64
      Width = 58
      Height = 13
      Caption = 'lbOutProdW'
    end
    object Label27: TLabel
      Left = 8
      Top = 117
      Width = 30
      Height = 13
      Caption = #1048#1090#1086#1075#1086
    end
    object Label28: TLabel
      Left = 8
      Top = 4
      Width = 51
      Height = 13
      Caption = #1042#1093#1086#1076#1103#1097#1080#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object txtInQ: TDBText
      Left = 68
      Top = 4
      Width = 65
      Height = 17
      DataField = 'REST_IN_Q'
      DataSource = dmInv.dsrSemisReport
    end
    object txtInW: TDBText
      Left = 148
      Top = 4
      Width = 65
      Height = 17
      DataField = 'REST_IN_W'
      DataSource = dmInv.dsrSemisReport
    end
    object M207Bevel6: TM207Bevel
      Left = 0
      Top = 140
      Width = 800
      Height = 20
      Align = alBottom
      Shape = bsTopLine
      CaptionOffs = 6
      BorderWidth = 0
    end
    object Label29: TLabel
      Left = 8
      Top = 145
      Width = 58
      Height = 13
      Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbPSQ: TLabel
      Left = 71
      Top = 116
      Width = 30
      Height = 13
      Caption = 'lbPSQ'
    end
    object M207Bevel7: TM207Bevel
      Left = 0
      Top = 113
      Width = 343
      Height = 6
      Shape = bsTopLine
      CaptionOffs = 6
      BorderWidth = 0
    end
    object lbPSW: TLabel
      Left = 120
      Top = 116
      Width = 33
      Height = 13
      Caption = 'lbPSW'
    end
    object lbQ: TLabel
      Left = 72
      Top = 145
      Width = 16
      Height = 13
      Caption = 'lbQ'
    end
    object lbW: TLabel
      Left = 120
      Top = 145
      Width = 19
      Height = 13
      Caption = 'lbW'
    end
    object txtOutQ: TDBText
      Left = 204
      Top = 144
      Width = 49
      Height = 17
      DataField = 'REST_OUT_Q'
      DataSource = dmInv.dsrSemisReport
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
    object txtOutW: TDBText
      Left = 256
      Top = 143
      Width = 49
      Height = 17
      DataField = 'REST_OUT_W'
      DataSource = dmInv.dsrSemisReport
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = False
    end
  end
  inherited ilButtons: TImageList
    Left = 194
    Top = 284
  end
  inherited fmstr: TFormStorage
    Left = 136
    Top = 288
  end
  object acEvent: TActionList
    Left = 48
    Top = 304
    object acPrintGrid: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      OnExecute = acPrintGridExecute
    end
  end
  object pWhItem: TPrintDBGridEh
    DBGridEh = gridWhItem
    Options = []
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 84
    Top = 244
  end
  object ppWhItem: TTBPopupMenu
    Left = 20
    Top = 264
    object TBItem1: TTBItem
      Action = acPrintGrid
    end
  end
  object Timer: TTimer
    Enabled = False
    Interval = 500
    OnTimer = TimerTimer
    Left = 188
    Top = 336
  end
end
