{***********************************************}
{  �������� �������������� �� ������ ��������.  }
{  �����������                                  }
{  Copyrigth (C) 2003 basile for CDM            }
{  v0.13                                        }
{  create 07.07.20                              }
{  last modify 19.08.2003                       }
{***********************************************}
unit PArt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList, ExtCtrls, ComCtrls,
  Grids, DBGridEh, ActnList, TB2Item, TB2Dock, TB2Toolbar, Menus,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmPArt = class(TfmAncestor)
    gridAssort: TDBGridEh;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acEvent: TActionList;
    acSetFQ0: TAction;
    acCopyQ_FQ: TAction;
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;
    acAdd: TAction;
    acDel: TAction;
    ppAssort: TTBPopupMenu;
    TBItem3: TTBItem;
    acDetail: TAction;
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acCopyQ_FQExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDetailExecute(Sender: TObject);
    procedure acCopyQ_FQUpdate(Sender: TObject);
    procedure acSetFQ0Execute(Sender: TObject);
    procedure acSetFQ0Update(Sender: TObject);
  private
    FDetail: boolean;
    procedure SetDetail(const Value: boolean);
    procedure Access;
  public
    property Detail : boolean read FDetail write SetDetail;
  end;

var
  fmPArt: TfmPArt;

implementation

uses MainData, DictData, MsgDialog, dbUtil;

{$R *.dfm}

procedure TfmPArt.acAddExecute(Sender: TObject);
begin
  dmMain.taPArt.Append;
end;

procedure TfmPArt.acDelExecute(Sender: TObject);
begin
  dmMain.taPArt.Delete;
end;

procedure TfmPArt.acCopyQ_FQExecute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  if MessageDialog('�� ������������� ������� ����������� ������� "������� ���-��" � "����������� ���-��"?',
    mtWarning, [mbYes, mbNo], 0) = mrNo then eXit;
  with dmMain do
  begin
    Bookmark := taPArt.GetBookmark;
    taPArt.DisableControls;
    try
      taPArt.First;
      while not taPArt.Eof do
      begin
        taPArt.Edit;
        taPArtFQ.AsFloat := taPArtQ.AsFloat;
        taPArt.Post;
        taPArt.Next;
      end;
    finally
      taPArt.EnableControls;
      taPArt.GotoBookmark(Bookmark);
    end;
  end;
end;

procedure TfmPArt.FormCreate(Sender: TObject);
begin
  Detail := False;
  Access;
  inherited;
  OpenDataSet(dmMain.taPArt);
  //ppAssort.Skin := dm.ppSkin;
end;

procedure TfmPArt.FormClose(Sender: TObject; var Action: TCloseAction);
{var
  Bookmark : Pointer;
  q, fq : integer;}
begin
  with dmMain do
  begin
    PostDataSet(taPArt);
    {taPArt.First;
    q := 0; fq := 0;
    while not taPArt.Eof do
    begin
      q := q + dmMain.taPArtQ.AsInteger;
      fq := fq + dmMain.taPArtFQ.AsInteger;
      taPArt.Next;
    end;}
    CloseDataSet(dmMain.taPArt);
    {dmMain.taProtocol_d.Edit;
    dmMain.taProtocol_dQ_A.AsInteger := q;
    dmMain.taProtocol_dFQ_A.AsInteger := fq;
    dmMain.taProtocol_d.Post;}
  end;
end;

procedure TfmPArt.acDetailExecute(Sender: TObject);
begin
  Detail := not Detail;
  (Sender as TAction).Checked := Detail;
end;

procedure TfmPArt.SetDetail(const Value: boolean);
begin
  FDetail := Value;
  gridAssort.FieldColumns['REST_IN_Q'].Visible := Value;
  gridAssort.FieldColumns['GET_Q'].Visible := Value;
  gridAssort.FieldColumns['DONE_Q'].Visible := Value;
  gridAssort.FieldColumns['REJ_Q'].Visible := Value;
end;

procedure TfmPArt.acSetFQ0Execute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  if MessageDialog('�� ������������� ������� �������� ������� "����������� ����������"?', mtWarning, [mbYes, mbNo], 0)=mrNo then eXit;
  with dmMain do
  begin
    Bookmark := taPArt.GetBookmark;
    taPArt.DisableControls;
    try
      taPArt.First;
      while not taPArt.Eof do
      begin
        taPArt.Edit;
        taPArtFQ.AsFloat := 0;
        taPArt.Post;
        taPArt.Next;
      end;
    finally
      taPArt.EnableControls;
      taPArt.GotoBookmark(Bookmark);
    end;
  end;
end;

procedure TfmPArt.acCopyQ_FQUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not dmMain.taPArt.IsEmpty) and
                                 (not dmMain.taPArtID.IsNull);
end;

procedure TfmPArt.acSetFQ0Update(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not dmMain.taPArt.IsEmpty) and
                                 (not dmMain.taPArtID.IsNull);
end;

procedure TfmPArt.acDelUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not dmMain.taPArt.IsEmpty) and
                                 (not dmMain.taPArtID.IsNull);
end;


procedure TfmPArt.Access;
begin
  if dm.IsAdm then eXit;
  gridAssort.ReadOnly := True;
  acAdd.Enabled := False;
  acDel.Enabled := False;
  acCopyQ_FQ.Enabled := False;
  acSetFQ0.Enabled := False;
end;

end.
