unit DbLogin;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls, Forms, Dialogs,
     StdCtrls, ExtCtrls, Buttons, Db, RxLogin, RxDBSecur,
     RxGrdCpt, DBCtrlsEh, rxPlacemnt, StrUtils,
{$ifdef IBX}
     IBCustomDataSet, IBDatabase, DBCtrls, DBCtrlsEh
{$endif}
{$ifdef FIBP}
     pFIBDataSet, pFIBDatabase
{$endif}
     ;


{$ifdef IBX}
Type TLoginFunction=function(UsersTable: TIBDataSet; const Password: String): Boolean of object;
{$endif}
{$ifdef FIBP}
Type TLoginFunction=function(UsersTable: TpFIBDataSet; const Password: String): Boolean of object;
{$endif}

  TfmDBLogin = class(TForm)
    AppIcon: TImage;
    KeyImage: TImage;
    HintLabel: TLabel;
    UserNameLabel: TLabel;
    PasswordLabel: TLabel;
    edPassword: TEdit;
    AppTitleLabel: TLabel;
    DatabaseLabel: TLabel;
    cbDB: TComboBox;
    OkBtn: TBitBtn;
    CancelBtn: TBitBtn;
    cbUser: TComboBox;
    fs1: TFormStorage;
    SpeedButton1: TSpeedButton;
    od1: TOpenDialog;
    Label1: TLabel;
    lbComp: TLabel;
    chbxCompLogin: TDBCheckBoxEh;
    lbTolling: TLabel;
    lbTollingName: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure OkBtnClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure KeyImageDblClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    FBlock : boolean;
    Attempt: integer;
   public
    { Public declarations }
    AttemptCount: Integer;
    UserField: string;

    LoginFunction: TLoginFunction;
    SelectDB: boolean;
{$ifdef IBX}
    taUser: TIBDataSet;
    Database: TIBDatabase;
{$endif}
{$ifdef FIBP}
    taUser: TpFIBDataSet;
    Database: TpFIBDatabase;
{$endif}
  end;


const IDH_LOGON: integer=0;
const IDH_ERR_USER: integer=0;
const IDH_ERR_DB: integer=0;

{$ifdef IBX}
function LoginDB(ADatabase: TIBDatabase;
                 AUserTable , AUserField: string;
                 ALoginFunction: TLoginFunction;
                 Attempts: integer;
                 ASelectDB: boolean;
                 var ASelectComp : boolean;                 
                 Block : boolean = False):boolean;
{$endif}
{$ifdef FIBP}
function LoginDB(ADatabase: TpFIBDatabase;
                 AUserTable , AUserField: string;
                 ALoginFunction: TLoginFunction;
                 Attempts: integer;
                 ASelectDB: boolean;
                 var ASelectComp : boolean;
                 Block : boolean = False):boolean;
{$endif}


var DBaseName: string;

implementation

uses RxAppUtils, RxDConst, Consts, RxVclUtils, RxConst, RxStrUtils, IniFiles, FmUtils,
  uUtils;

{$R *.DFM}

{$ifdef IBX}
function LoginDB(ADatabase: TIBDatabase;
                 AUserTable , AUserField: string;
                 ALoginFunction: TLoginFunction;
                 Attempts: integer;
                 ASelectDB: boolean;
                 var ASelectComp : boolean;
                 Block : boolean = False):boolean;
{$endif}
{$ifdef FIBP}
function LoginDB(ADatabase: TpFIBDatabase;
                 AUserTable , AUserField: string;
                 ALoginFunction: TLoginFunction;
                 Attempts: integer;
                 ASelectDB: boolean;
                 var ASelectComp : boolean;
                 Block : boolean = False):boolean;
{$endif}
var fmDBLogin: TfmDbLogin;
begin
  fmDBLogin:=TfmDBLogin.Create(Application);
  try
    fmDBLogin.HelpContext:=IDH_LOGON;
    with fmDBLogin, cbDB do
      begin
        AttemptCount:= Attempts;
        Database:= ADatabase;
{$ifdef IBX}
        taUser:=TIBDataSet.Create(NIL);
{$endif}
{$ifdef FIBP}
        taUser:=TpFIBDataSet.Create(NIL);
{$endif}
        taUser.Database:=ADatabase;
        taUser.SelectSQL.Text:='SELECT * FROM '+AUserTable;
        UserField:=AUserField;
        LoginFunction:=ALoginFunction;
        FBlock := Block;
        SelectDB:= NOT ASelectDB;
        Result:=ShowModal=mrOK;
        ASelectComp := not chbxCompLogin.Checked;
     end;
  finally
    fmDBLogin.Free;
  end
end;

procedure TfmDBLogin.FormCreate(Sender: TObject);
var
  Ini : TIniFile;
  sect : TStringList;
begin
  Icon := Application.Icon;
  if Icon.Empty then Icon.Handle := LoadIcon(0, IDI_APPLICATION);
  AppIcon.Picture.Assign(Icon);
  AppTitleLabel.Caption := '���������� '''+Application.Title+'''';{FmtLoadStr(SAppTitleLabel, [Application.Title])};
  Attempt:=0;
  with cbDB do
    if Items.Count>0 then
      Text:=Items[0];
  (* ��������� ini-���� *)
  Ini := TIniFile.Create(GetIniFileName);
  sect := TStringList.Create;
  try
    Ini.ReadSectionValues('CompLogin', sect);
    lbComp.Caption := sect.Values['CompName'];


    if  lbComp.Caption = '��� "���"' then  Ini.WriteString('CompLogin','CompName','  �� ��������� ������� ����������'); //sect.Values['CompName']:= '�� ��������� ������� ����������';


    if(lbComp.Caption = '')then lbComp.Caption := '�� ����������'
    else chbxCompLogin.Checked := True;
    lbTollingName.Caption := sect.Values['TollingCompName'];
    if(lbTollingName.Caption = '')then lbTolling.Visible := False;
  finally
    Ini.Free;
    sect.Free;
  end;
end;

procedure TfmDBLogin.OkBtnClick(Sender: TObject);
var
  i: integer;
begin
  Inc(Attempt);
  with Database do
    begin
      if not FBlock then
      begin
        if Connected then
        begin
          with DefaultTransaction do
            if Active then Active:=False;
          Connected:=False;
        end;
        if cbDB.Text<>'' then DatabaseName:=ExtractWord(1, cbDB.Text, [';']);
      end;
      try
        if not FBlock then
        begin
          Connected:=True;
          DefaultTransaction.Active:=True;
        end;
        taUser.Active:=True;
        if taUser.Locate(UserField, cbUser.Text, [loCaseInsensitive]) then
          if LoginFunction(taUser, edPassword.Text) then
            begin
              ModalResult:=mrOK;
              with cbUser, Items do
                begin
                  i:=IndexOf(cbUser.Text);
                  if i=-1 then  Insert(0, cbUser.Text)
                  else Move(i, 0);
                end;

              with cbDB, Items do
                begin
                  i:=IndexOf(LowerCase(cbDB.Text));
                  if i=-1 then  Insert(0, cbDB.Text)
                  else Move(i, 0);
                end;
             taUser.Active:=False;
             if not FBlock then
             begin
               Database.Connected:=False;
             end;
             DBaseName:=cbDB.Text;
             exit;
          end;
        taUser.Active:=False;
        if not FBlock then Database.Connected:=False;

        MessageDlg('�������� ��� ������������ ��� ������',  mtError, [mbOK], IDH_ERR_USER);
        edPassword.Text:='';
        ActiveControl:=edPassword;
      except
        on E: Exception do
          MessageDlg('������ ��� ����������� � ��������� ���� �����'+#13#10+E.Message,
                     mtError, [mbOK], IDH_ERR_DB);
      end;
      if not FBlock then if DefaultTransaction.Active then DefaultTransaction.Commit;
    end;
  if Attempt=AttemptCount then ModalResult:=mrCancel;  
end;

procedure TfmDBLogin.FormActivate(Sender: TObject);
begin
  with cbUser do Text:=Items[0];
  with cbDB do Text:=Items[0];
  if FBlock then
  begin
    UserNameLabel.Enabled := False;
    cbUser.Enabled := False;
    KeyImage.OnDblClick := nil;
    //Height:=cbDB.Top+cbDB.Height+ cbDB.Height div 2 +Height-ClientHeight;
    Height:=cbDB.Top-1+Height-ClientHeight;
  end
  else KeyImageDblClick(Self);
end;

procedure TfmDBLogin.KeyImageDblClick(Sender: TObject);
begin
  SelectDB:=NOT SelectDB;
  if SelectDB then Height:=cbDB.Top+cbDB.Height+ cbDB.Height div 2 +Height-ClientHeight
  else Height:=cbDB.Top-1+Height-ClientHeight;
end;

procedure TfmDBLogin.FormDestroy(Sender: TObject);
begin
  try
    if not FBlock then
      if Database.DefaultTransaction.Active then Database.DefaultTransaction.Commit;
    taUser.Free;
  except
  end;
end;

procedure TfmDBLogin.SpeedButton1Click(Sender: TObject);
begin
  if od1.Execute then
    cbDB.Text:=od1.FileName;
end;

end.
