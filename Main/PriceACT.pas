unit PriceACT;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, Menus, ImgList, StdCtrls, DB,
  ExtCtrls, Grids, DBGrids, RXDBCtrl,  ComCtrls, Period, dbUtil, fmUtils,
  DBGridEh, ActnList, TB2Item, DBGridEhGrouping, rxPlacemnt,
  GridsEh, rxSpeedbar;

type
  TfmPriceACT = class(TfmListAncestor)
    SpeedItem1: TSpeedItem;
    acCreateActs: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acDelDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acPrintDocExecute(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acCreateActsExecute(Sender: TObject);
  protected
    procedure DepClick(Sender: TObject); override;
  end;

var
  fmPriceACT: TfmPriceACT;

implementation

uses InvData, DictData, ACTbyItem, PrintData, MsgDialog, MainData, FIBQuery,
  pFIBQuery;

{$R *.dfm}

procedure TfmPriceACT.FormCreate(Sender: TObject);
 var
  i, j : integer;
  Item : TMenuItem;
begin
  dm.CurrDep := 23;
  DataSet:=dmInv.taSList;
  inherited;
  //���������� PopupMenu �� FDepInfo;
  j := 0;
  for i:=0 to Pred(dm.CountDep) do
  begin
  if dm.DepInfo[i].Wh then
  begin
    Item := TMenuItem.Create(ppDep);
    Item.Caption := dm.DepInfo[i].SName;
    Item.Tag := dm.DepInfo[i].DepId;
    Item.OnClick := DepClick;
    Item.RadioItem := True;
    if Item.Tag = dm.DepDef then DepClick(Item);
    inc(j);
    if j>30 then
    begin
      Item.Break := mbBreak;
      j:=0;
    end;
    ppDep.Items.Add(Item);
  end;
  end;
  FBD:=dm.whBeginDate;
  FED:=dm.whEndDate;
  ShowPeriod;
end;

procedure TfmPriceACT.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  inherited;
end;

procedure TfmPriceACT.FormShow(Sender: TObject);
begin
  inherited;
  spitAdd.Visible := False;
  spitDel.Visible := False;
end;

procedure TfmPriceACT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   inherited;
   CloseDataSets([dmINV.taINVNO]);
end;

procedure TfmPriceACT.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(Column.FieldName='IN_SUM')then Background:=clSell;
  if(Column.FieldName='OUT_SUM')then Background:=clOut;
  if(Column.FieldName='RAZ_SUM')then Background:=clReturn;
end;

procedure TfmPriceACT.acAddDocExecute(Sender: TObject);
begin
  //
end;

procedure TfmPriceACT.acDelDocExecute(Sender: TObject);
begin
// if MessageDialog('������� ������?', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
  if dmINV.taSList.RecordCount>0 then begin
  dmINV.taSList.Delete;
//  dmINV.taSList.Transaction.CommitRetaining;
  ReopenDataSets([dmINV.taSList]);
 end;
end;

procedure TfmPriceACT.acEditDocExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmACTbyItem, TForm(fmACTbyItem));
  RefreshDataSet(dmInv.taSList);
end;

procedure TfmPriceACT.acPrintDocExecute(Sender: TObject);
begin
  inherited;
  dmPrint.PrintDocumentA(dkACTRePrice,null);
end;

procedure TfmPriceACT.acPeriodExecute(Sender: TObject);
begin
  if ShowPeriodForm(FBD, FED) then
  begin
    dmMain.ChangePeriod(FBD, FED);
    ShowPeriod;
    Application.ProcessMessages;
    ReOpenDataSets([gridDocList.DataSource.DataSet]);
    Application.ProcessMessages;
  end;
end;

procedure TfmPriceACT.acCreateActsExecute(Sender: TObject);
begin
  inherited;
{  with dmInv, sqlPublic do
  begin
   SQL.Clear;
   SQL.ADD('execute procedure CREATE_ACTS_REPRICE_FOR_PERIOD()');
   Prepare;
   ExecQuery;
   Transaction.CommitRetaining;
  end;
}  
end;

end.
