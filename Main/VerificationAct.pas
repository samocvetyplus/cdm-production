unit VerificationAct;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus, TB2Item, ActnList,  ImgList, Grids,
  DBGridEh, DBCtrlsEh, StdCtrls, Mask, DBCtrls, ExtCtrls, 
  ComCtrls, DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmVerificationAct = class(TfmDocAncestor)
    Label1: TLabel;
    txtSup: TDBText;
    Label2: TLabel;
    edBD: TDBDateTimeEditEh;
    Label3: TLabel;
    edED: TDBDateTimeEditEh;
    SpeedItem1: TSpeedItem;
    acRefresh: TAction;
    procedure FormCreate(Sender: TObject);
    procedure acRefreshExecute(Sender: TObject);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  end;

var
  fmVerificationAct: TfmVerificationAct;

implementation

uses Data, DictData, dbUtil;

{$R *.dfm}

procedure TfmVerificationAct.FormCreate(Sender: TObject);
begin
  FDocName := '��� ������';
  edNoDoc.DataSource := dmData.dsrVerificationList;
  dtedDateDoc.DataSource := dmData.dsrVerificationList;
  gridItem.DataSource := dmData.dsrActVerification;
  inherited;
  InitBtn(dmData.taVerificationListISCLOSE.AsInteger=1);
end;

procedure TfmVerificationAct.acRefreshExecute(Sender: TObject);
begin
  ExecSQL('execute procedure Create_AWTolling('+dmData.taVerificationListINVID.AsString+')', dm.quTmp);
  ReOpenDataSet(DataSet);
end;

procedure TfmVerificationAct.gridItemGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if (dmData.taActVerificationINVSUBTYPEID.AsInteger in [20, 21]) then Background := clMoneyGreen;
end;

end.
