inherited fmCasePS: TfmCasePS
  Left = 180
  Top = 176
  Caption = #1042#1099#1073#1086#1088' '#1084#1077#1089#1090' '#1093#1088#1072#1085#1077#1085#1080#1103
  ClientHeight = 221
  ClientWidth = 360
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 360
    Height = 221
    inherited btnOk: TBitBtn
      Left = 180
      Top = 188
    end
    inherited btnCancel: TBitBtn
      Left = 272
      Top = 188
    end
    object chlbxPS: TCheckListBox
      Left = 8
      Top = 32
      Width = 345
      Height = 149
      BevelKind = bkFlat
      BorderStyle = bsNone
      Columns = 2
      ItemHeight = 13
      TabOrder = 2
    end
    object TBDock1: TTBDock
      Left = 2
      Top = 2
      Width = 356
      Height = 23
      object TBToolbar1: TTBToolbar
        Left = 0
        Top = 0
        BorderStyle = bsNone
        Caption = 'TBToolbar1'
        TabOrder = 0
        DockTextAlign = taLeftJustify
        object TBItem1: TTBItem
          Action = acCheckAll
        end
        object TBItem2: TTBItem
          Action = acUnCheckAll
        end
      end
    end
  end
  object acEvent: TActionList
    Left = 16
    Top = 172
    object acCheckAll: TAction
      Caption = 'Check All'
      OnExecute = acCheckAllExecute
    end
    object acUnCheckAll: TAction
      Caption = 'UnCheck All'
      OnExecute = acUnCheckAllExecute
    end
  end
end
