unit AddEmptyInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, cxDropDownEdit, cxCalc, cxTextEdit, cxMaskEdit,
  cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, DBGridEh, DBCtrlsEh, Mask,
  DBLookupEh, FIBQuery, pFIBQuery, Menus, cxButtons, ActnList, ImgList, ComCtrls;

type
  TfmAddEmptyInv = class(TForm)
    lbComp: TLabel;
    lbWeight: TLabel;
    lbMoneyTotal: TLabel;
    lbNDS: TLabel;
    deWeight: TDBEditEh;
    deCOST: TDBEditEh;
    quInsertEmptyInv: TpFIBQuery;
    addButton: TcxButton;
    cancelButton: TcxButton;
    ImageList: TImageList;
    Actions: TActionList;
    acAddInvoice: TAction;
    acCancelClose: TAction;
    lbQuantity: TLabel;
    deQuantity: TDBEditEh;
    lookupOrganizations: TDBLookupComboboxEh;
    lookupNDS: TDBLookupComboboxEh;
    edComment: TRichEdit;
    lbComment: TLabel;
    edDate: TDBDateTimeEditEh;
    lbDate: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCancelCloseExecute(Sender: TObject);
    procedure acAddInvoiceExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    ExecType: integer;
    function ValidateParams: boolean;
  public
    class procedure Execute(ExecType: Integer);
  end;

var
  fmAddEmptyInv: TfmAddEmptyInv;

implementation

uses MainData, DictData, InvData;

{$R *.dfm}

procedure TfmAddEmptyInv.acAddInvoiceExecute(Sender: TObject);
begin
  if ValidateParams then
    begin
      Screen.Cursor := crHourGlass;
      quInsertEmptyInv.ParamByName('Q').AsInteger := strToInt(deQuantity.Text);
      quInsertEmptyInv.ParamByName('W').AsFloat := strToFloat(deWeight.Text);
      quInsertEmptyInv.ParamByName('Cost').AsFloat := strToFloat(deCost.Text);
      quInsertEmptyInv.ParamByName('SupID').AsInteger := dmInv.taCompCOMPID.AsInteger;
      quInsertEmptyInv.ParamByName('NDSID').AsInteger := dm.taNDSNDSID.AsInteger;
      quInsertEmptyInv.ParamByName('DocDate').AsString := edDate.Text;
      quInsertEmptyInv.ParamByName('Comment').AsVariant := edComment.Text;
      if ExecType = 0 then  //�������
        begin
          quInsertEmptyInv.ParamByName('InvID').AsInteger := -1;
          quInsertEmptyInv.ParamByName('DocNo').Asinteger := -1;
        end
      else
        if ExecType = 1 then   //����������
          begin
            quInsertEmptyInv.ParamByName('InvID').AsInteger := dmInv.taSListINVID.AsInteger;
            quInsertEmptyInv.ParamByName('DocNo').Asinteger := dmInv.taSListDOCNO.AsInteger;
          end;

      quInsertEmptyInv.ExecQuery;
      deQuantity.Text := '';
      deWeight.Text := '';
      deCost.Text := '';
      dmInv.taSList.CloseOpen(false);
      Screen.Cursor := crDefault;

    end
  else MessageDlg('��������� ������� �� ����� ��� �� ���������!', mtError, [mbOk], 0);

  Close;
end;

procedure TfmAddEmptyInv.acCancelCloseExecute(Sender: TObject);
begin
  Close;
end;


function TfmAddEmptyInv.ValidateParams: boolean;
var V: Variant;
begin
  Result := true;
  Result := (deQuantity.Text <> '') and (deWeight.Text <> '') and (deCost.Text <> '') and Result;
  try
    V := strToInt(deQuantity.Text);
    V := strToFloat(deWeight.Text);
    V := strToFloat(deCost.Text);
  except
    Result := false;
  end;
end;

procedure TfmAddEmptyInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dmInv.dsrCOMP.DataSet.Close;
  dm.dsrNDS.DataSet.Close;
end;

procedure TfmAddEmptyInv.FormShow(Sender: TObject);
begin

if not dmInv.dsrCOMP.DataSet.Active then
    dmInv.dsrCOMP.DataSet.Open;
if not dm.dsrNDS.DataSet.Active then
    dm.dsrNDS.DataSet.Open;

  if ExecType = 0 then
    begin
//      with deQuantity, deWeight, deCost, edDate, lookupNDS do
//        begin
//          DataSource := nil;
//          DataField := '';
//        end;
      Self.Caption := '����������' + Self.Caption;
      lookupOrganizations.KeyValue := 249;
      lookupNDS.KeyValue := 1;
    end;

  if ExecType = 1 then
    begin
      edComment.Text := dmInv.taSListMEMO.AsString;
//    with deQuantity, deWeight, deCost, edDate, lookupNDS do
//    DataSource := dmInv.dsrSList;
      deQuantity.Text := dmInv.taSListQ.AsString;
      deWeight.Text := dmInv.taSListW.AsString;
      deCost.Text := dmInv.taSListCOST.AsString;
      edDate.Text := dmInv.taSListDOCDATE.AsString;
      lookupNDS.KeyValue := dmInv.taSListNDSID.AsInteger;
      lookupOrganizations.KeyValue := dmInv.taSListSUPID.AsInteger;
      Self.Caption := '��������������' + Self.Caption;
//      dmInv.taSList.Refresh;
    end;


end;

class procedure TfmAddEmptyInv.Execute(ExecType: Integer);
begin
  fmAddEmptyInv := nil;
  try
    try
      fmAddEmptyInv := TfmAddEmptyInv.Create(Application);
      fmAddEmptyInv.ExecType := ExecType;
      fmAddEmptyInv.ShowModal;
    except
      on E:Exception do ShowMessage(E.Message);
    end;
  finally
    freeAndNil(fmAddEmptyInv);
  end;
end;

end.
