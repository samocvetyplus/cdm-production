object DialogSemisRej: TDialogSemisRej
  Left = 153
  Top = 193
  BorderStyle = bsToolWindow
  Caption = '?'
  ClientHeight = 563
  ClientWidth = 702
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 0
    Width = 702
    Height = 563
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = False
    TabOrder = 0
    ClientRectBottom = 563
    ClientRectRight = 702
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = #1041#1088#1072#1082
      ImageIndex = 0
      object GridSource: TcxGrid
        Left = 16
        Top = 40
        Width = 201
        Height = 417
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        OnEnter = GridSourceEnter
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = False
        object GridSourceView: TcxGridDBTableView
          OnDblClick = GridSourceViewDblClick
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = DataSourceSource
          DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '0'
              Kind = skSum
              Column = GridSourceViewColumn2
            end
            item
              Format = '0.000'
              Kind = skSum
              Column = GridSourceViewColumn3
            end>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsCustomize.ColumnHorzSizing = False
          OptionsCustomize.ColumnMoving = False
          OptionsCustomize.ColumnSorting = False
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.ScrollBars = ssVertical
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.GroupRowStyle = grsOffice11
          object GridSourceViewColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'OPERATION'
            Visible = False
            GroupIndex = 0
            IsCaptionAssigned = True
          end
          object GridSourceViewColumn2: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'QUANTITY'
            Width = 82
          end
          object GridSourceViewColumn3: TcxGridDBColumn
            Caption = #1042#1077#1089
            DataBinding.FieldName = 'WEIGHT'
            Width = 76
          end
        end
        object GridSourceLevel1: TcxGridLevel
          GridView = GridSourceView
        end
      end
      object PanelBottom: TPanel
        Left = 0
        Top = 497
        Width = 702
        Height = 42
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object Bevel1: TBevel
          Left = 0
          Top = 0
          Width = 702
          Height = 2
          Align = alTop
          Shape = bsTopLine
        end
        object ButtonOK: TcxButton
          Left = 536
          Top = 8
          Width = 75
          Height = 25
          Action = ActionOK
          TabOrder = 0
          LookAndFeel.Kind = lfOffice11
        end
        object ButtonCancel: TcxButton
          Left = 616
          Top = 8
          Width = 75
          Height = 25
          Cancel = True
          Caption = #1054#1090#1084#1077#1085#1072
          ModalResult = 2
          TabOrder = 1
          LookAndFeel.Kind = lfOffice11
        end
      end
      object GridTarget: TcxGrid
        Left = 416
        Top = 40
        Width = 273
        Height = 417
        TabOrder = 2
        OnEnter = GridTargetEnter
        LookAndFeel.Kind = lfOffice11
        object GridTargetView: TcxGridDBTableView
          OnDblClick = GridTargetViewDblClick
          NavigatorButtons.ConfirmDelete = False
          DataController.DataSource = DataSourceTarget
          DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Kind = skSum
              Position = spFooter
              Column = GridTargetViewQuantity
            end
            item
              Kind = skSum
              Position = spFooter
              Column = GridTargetViewWeight
            end>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsCustomize.ColumnFiltering = False
          OptionsCustomize.ColumnGrouping = False
          OptionsCustomize.ColumnMoving = False
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          OptionsView.GroupRowStyle = grsOffice11
          object GridTargetViewOperation: TcxGridDBColumn
            Caption = #1054#1087#1077#1088#1072#1094#1080#1103
            DataBinding.FieldName = 'Operation'
            Width = 123
          end
          object GridTargetViewQuantity: TcxGridDBColumn
            Caption = #1050#1086#1083'-'#1074#1086
            DataBinding.FieldName = 'Quantity'
            Options.Editing = False
          end
          object GridTargetViewWeight: TcxGridDBColumn
            Caption = #1042#1077#1089
            DataBinding.FieldName = 'Weight'
            Options.Editing = False
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = GridTargetView
        end
      end
      object PanelCenter: TPanel
        Left = 224
        Top = 40
        Width = 185
        Height = 417
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 3
        object EditorQuantity: TcxTextEdit
          Left = 8
          Top = 24
          TabOrder = 0
          Text = '0'
          Width = 81
        end
        object EditorWeight: TcxTextEdit
          Left = 96
          Top = 24
          TabOrder = 1
          Text = '0'
          Width = 81
        end
        object LabelQuantity: TcxLabel
          Left = 8
          Top = 8
          Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
        end
        object LabelWeight: TcxLabel
          Left = 96
          Top = 8
          Caption = #1042#1077#1089
        end
      end
      object ButtonDelete: TcxButton
        Left = 280
        Top = 464
        Width = 49
        Height = 25
        Action = ActionDelete
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Wingdings'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        Visible = False
        LookAndFeel.Kind = lfOffice11
      end
      object ButtonInsert: TcxButton
        Left = 280
        Top = 8
        Width = 49
        Height = 25
        Action = ActionInsert
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Wingdings'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        LookAndFeel.Kind = lfOffice11
      end
      object ButtonSourceTitle1: TcxButton
        Left = 120
        Top = 8
        Width = 153
        Height = 25
        Caption = '? '
        Enabled = False
        TabOrder = 6
        LookAndFeel.Kind = lfOffice11
      end
      object ButtonTargetTitle1: TcxButton
        Left = 336
        Top = 8
        Width = 153
        Height = 25
        Caption = '?'
        Enabled = False
        TabOrder = 7
        LookAndFeel.Kind = lfOffice11
      end
      object ButtonSourceTitle2: TcxButton
        Left = 120
        Top = 464
        Width = 153
        Height = 25
        Caption = '?'
        Enabled = False
        TabOrder = 8
        Visible = False
        LookAndFeel.Kind = lfOffice11
      end
      object ButtonTargetTitle2: TcxButton
        Left = 336
        Top = 464
        Width = 153
        Height = 25
        Caption = '?'
        Enabled = False
        TabOrder = 9
        Visible = False
        LookAndFeel.Kind = lfOffice11
      end
    end
  end
  object DataSourceSource: TDataSource
    DataSet = DataSetSource
    Left = 48
    Top = 72
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 80
    Top = 72
    PixelsPerInch = 96
    object StyleReadOnly: TcxStyle
      AssignedValues = [svColor]
      Color = clBtnFace
    end
  end
  object DataSetDBSource: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure stub(0)')
    DeleteSQL.Strings = (
      'execute procedure stub(0)')
    InsertSQL.Strings = (
      'execute procedure stub(0)')
    SelectSQL.Strings = (
      'select * from spoilage$source$select(:STORAGE$ELEMENT$ID)')
    BeforeOpen = DataSetDBSourceBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 112
    Top = 120
    oRefreshAfterPost = False
    oFetchAll = True
    object DataSetDBSourceOPERATIONID: TFIBIntegerField
      FieldName = 'OPERATION$ID'
    end
    object DataSetDBSourceOPERATION: TFIBStringField
      FieldKind = fkLookup
      FieldName = 'OPERATION'
      LookupDataSet = DataSetOperation
      LookupKeyFields = 'ID'
      LookupResultField = 'OPERATION'
      KeyFields = 'OPERATION$ID'
      Size = 64
      EmptyStrToNull = True
      Lookup = True
    end
    object DataSetDBSourceQUANTITY: TFIBIntegerField
      FieldName = 'QUANTITY'
    end
    object DataSetDBSourceWEIGHT: TFIBFloatField
      FieldName = 'WEIGHT'
    end
  end
  object DataSetOperation: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select id, cast(stretrim(operation) as varchar(64)) Operation fr' +
        'om d_oper order by operation')
    Transaction = dm.tr
    Database = dm.db
    Left = 16
    Top = 120
    object DataSetOperationID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetOperationOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 64
      EmptyStrToNull = True
    end
  end
  object DataSetTarget: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'OPERATION$ID'
        DataType = ftInteger
      end
      item
        Name = 'QUANTITY'
        DataType = ftInteger
      end
      item
        Name = 'WEIGHT'
        DataType = ftFloat
      end
      item
        Name = 'UNIT$QUANTITY'
        DataType = ftInteger
      end
      item
        Name = 'UNIT$WEIGHT'
        DataType = ftInteger
      end>
    IndexDefs = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'INVOICE$ELEMENT$ID'
        ParamType = ptInputOutput
      end>
    ProviderName = 'DataSetTargetProvider'
    StoreDefs = True
    OnNewRecord = DataSetTargetNewRecord
    Left = 176
    Top = 152
    object DataSetTargetOperationID: TIntegerField
      FieldName = 'Operation$ID'
    end
    object DataSetTargetOperation: TStringField
      FieldKind = fkLookup
      FieldName = 'Operation'
      LookupDataSet = DataSetOperation
      LookupKeyFields = 'ID'
      LookupResultField = 'OPERATION'
      KeyFields = 'Operation$ID'
      Size = 64
      Lookup = True
    end
    object DataSetTargetQuantity: TIntegerField
      FieldName = 'Quantity'
    end
    object DataSetTargetWeight: TFloatField
      FieldName = 'Weight'
    end
    object DataSetTargetUnitQuantity: TIntegerField
      FieldName = 'Unit$Quantity'
    end
    object DataSetTargetUnitWeight: TIntegerField
      FieldName = 'UNIT$WEIGHT'
    end
  end
  object DataSourceTarget: TDataSource
    DataSet = DataSetTarget
    Left = 48
    Top = 216
  end
  object ActionList1: TActionList
    Left = 432
    Top = 376
    object ActionInsert: TAction
      Caption = #1080
      OnExecute = ActionInsertExecute
      OnUpdate = ActionInsertUpdate
    end
    object ActionDelete: TAction
      Caption = #1079
      OnExecute = ActionDeleteExecute
      OnUpdate = ActionDeleteUpdate
    end
    object ActionOK: TAction
      Caption = #1055#1088#1080#1084#1077#1085#1080#1090#1100
      OnExecute = ActionOKExecute
      OnUpdate = ActionOKUpdate
    end
  end
  object DataSetTargetSummary: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 16
    Top = 256
    object DataSetTargetSummaryQuantity: TIntegerField
      FieldName = 'Quantity'
    end
    object DataSetTargetSummaryWeight: TFloatField
      FieldName = 'Weight'
    end
  end
  object Query: TpFIBQuery
    Transaction = dm.tr
    Database = dm.db
    Left = 48
    Top = 256
  end
  object StoredProc: TpFIBStoredProc
    Transaction = dm.tr
    Database = dm.db
    SQL.Strings = (
      
        'EXECUTE PROCEDURE SPOILAGE$DELTA (?STORAGE$ELEMENT$ID, ?OPERATIO' +
        'N$ID, ?WEIGHT, ?QUANTITY, ?INVOICE$ELEMENT$ID)')
    StoredProcName = 'SPOILAGE$DELTA'
    Left = 48
    Top = 296
  end
  object DataSetSourceProvider: TDataSetProvider
    DataSet = DataSetDBSource
    Left = 144
    Top = 120
  end
  object DataSetSource: TClientDataSet
    Aggregates = <>
    FieldDefs = <
      item
        Name = 'OPERATION$ID'
        DataType = ftInteger
      end
      item
        Name = 'OPERATION'
        Attributes = [faReadonly]
        DataType = ftString
        Size = 64
      end
      item
        Name = 'QUANTITY'
        DataType = ftInteger
      end
      item
        Name = 'WEIGHT'
        DataType = ftFloat
      end>
    IndexDefs = <>
    Params = <
      item
        DataType = ftInteger
        Name = 'STORAGE$ELEMENT$ID'
        ParamType = ptInputOutput
      end>
    ProviderName = 'DataSetSourceProvider'
    StoreDefs = True
    Left = 176
    Top = 120
    object DataSetSourceOPERATIONID: TIntegerField
      FieldName = 'OPERATION$ID'
    end
    object DataSetSourceOPERATION: TStringField
      FieldKind = fkLookup
      FieldName = 'OPERATION'
      LookupDataSet = DataSetOperation
      LookupKeyFields = 'ID'
      LookupResultField = 'OPERATION'
      KeyFields = 'OPERATION$ID'
      ReadOnly = True
      Size = 64
      Lookup = True
    end
    object DataSetSourceQUANTITY: TIntegerField
      FieldName = 'QUANTITY'
    end
    object DataSetSourceWEIGHT: TFloatField
      FieldName = 'WEIGHT'
    end
  end
  object DataSetDBTarget: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure stub(0)')
    DeleteSQL.Strings = (
      'execute procedure stub(0)')
    InsertSQL.Strings = (
      'execute procedure stub(0)')
    SelectSQL.Strings = (
      
        'select * from spoilage$target$select(:IN$STORAGE$ELEMENT$ID, :IN' +
        'VOICE$ELEMENT$ID)')
    BeforeOpen = DataSetDBTargetBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 112
    Top = 152
    oRefreshAfterPost = False
    oFetchAll = True
    object DataSetDBTargetOPERATIONID: TFIBIntegerField
      FieldName = 'OPERATION$ID'
    end
    object DataSetDBTargetQUANTITY: TFIBIntegerField
      FieldName = 'QUANTITY'
    end
    object DataSetDBTargetWEIGHT: TFIBFloatField
      FieldName = 'WEIGHT'
    end
    object DataSetDBTargetUNITQUANTITY: TFIBIntegerField
      FieldName = 'UNIT$QUANTITY'
    end
    object DataSetDBTargetUNITWEIGHT: TFIBIntegerField
      FieldName = 'UNIT$WEIGHT'
    end
  end
  object DataSetTargetProvider: TDataSetProvider
    DataSet = DataSetDBTarget
    Left = 144
    Top = 152
  end
  object DataSetSourceSummary: TClientDataSet
    Aggregates = <>
    FieldDefs = <>
    IndexDefs = <>
    Params = <>
    StoreDefs = True
    Left = 16
    Top = 296
  end
end
