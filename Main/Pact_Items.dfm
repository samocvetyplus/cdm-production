inherited fmPact_Items: TfmPact_Items
  Left = 421
  Top = 176
  Width = 545
  Height = 328
  Caption = #1055#1077#1088#1077#1086#1094#1077#1085#1077#1085#1085#1099#1077' '#1080#1079#1076#1077#1083#1080#1103
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 275
    Width = 537
  end
  inherited tb1: TSpeedBar
    Width = 537
    object DBText1: TDBText [0]
      Left = 94
      Top = 4
      Width = 65
      Height = 17
      DataField = 'Q'
      DataSource = dmInv.dsrPriceACTS
      Transparent = True
    end
    object DBText2: TDBText [1]
      Left = 94
      Top = 18
      Width = 65
      Height = 17
      DataField = 'SUM_Q0'
      DataSource = dmInv.dsrPriceACTS
      Transparent = True
    end
    object Label1: TLabel [2]
      Left = 8
      Top = 4
      Width = 79
      Height = 13
      Caption = #1042#1089#1077#1075#1086' '#1082#1086#1083'-'#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Label2: TLabel [3]
      Left = 9
      Top = 19
      Width = 61
      Height = 13
      Caption = #1042#1089#1077#1075#1086' '#1074#1077#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object SpeedItem1: TSpeedItem
      BtnCaption = #1059#1076#1072#1083#1080#1090#1100
      Caption = #1059#1076#1072#1083#1080#1090#1100
      Hint = #1059#1076#1072#1083#1080#1090#1100'|'
      ImageIndex = 2
      Spacing = 1
      Left = 211
      Top = 3
      Visible = True
      OnClick = SpeedItem1Click
      SectionName = 'Untitled (0)'
    end
    object SpeedItem2: TSpeedItem
      BtnCaption = #1044#1086#1073#1072#1074#1080#1090#1100
      Caption = 'SpeedItem2'
      Hint = 'SpeedItem2|'
      ImageIndex = 1
      Spacing = 1
      Left = 147
      Top = 3
      Visible = True
      OnClick = SpeedItem2Click
      SectionName = 'Untitled (0)'
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 42
    Width = 537
    Height = 233
    Align = alClient
    AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
    Color = clWhite
    ColumnDefValues.Title.TitleButton = True
    DataGrouping.GroupLevels = <>
    DataSource = dmInv.dsrPact_Items
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    FooterRowCount = 1
    FrozenCols = 1
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'UID'
        Footers = <>
        Title.Caption = #1059#1085'. '#1085#1086#1084#1077#1088
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OLD_PRICE'
        Footers = <>
        Width = 93
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'NEW_PRICE'
        Footers = <>
      end
      item
        Color = 15659248
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'F_1'
        Footer.FieldName = 'F_1'
        Footer.ValueType = fvtSum
        Footers = <>
      end
      item
        Color = 15659248
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'F_2'
        Footer.FieldName = 'F_2'
        Footer.ValueType = fvtSum
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ilButtons: TImageList
    Left = 350
    Top = 36
  end
  inherited fmstr: TFormStorage
    Left = 360
    Top = 64
  end
end
