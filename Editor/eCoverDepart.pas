unit eCoverDepart;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, CheckLst, Buttons, ExtCtrls, DB, FIBDataSet,
  pFIBDataSet;

type
  TfmeCoverDepart = class(TfmEditor)
    chlistPS: TCheckListBox;
    Label1: TLabel;
    taPS: TpFIBDataSet;
    taPSDEPID: TFIBIntegerField;
    taPSNAME: TFIBStringField;
    taPSCOVERID: TFIBIntegerField;
    procedure taPSBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure chlistPSClickCheck(Sender: TObject);
  end;

var
  fmeCoverDepart: TfmeCoverDepart;

implementation

uses DictData, dbTree, dbUtil;

{$R *.dfm}

procedure TfmeCoverDepart.taPSBeforeOpen(DataSet: TDataSet);
begin
  taPS.ParamByName('DEPID').AsInteger := dm.taDepDEPID.AsInteger;
end;

procedure TfmeCoverDepart.FormCreate(Sender: TObject);
var
  nd : TNodeData;
  i : integer;
begin
  OpenDataSet(taPS);
  i := 0;
  while not taPS.Eof do
  begin
    nd := TNodeData.Create;
    nd.Code := taPSDEPID.AsInteger;
    nd.Name := taPSNAME.AsString;
    nd.Params := taPSCOVERID.AsVariant;
    chlistPS.Items.AddObject(taPSNAME.AsString, nd);
    chlistPS.Checked[i] := not taPSCOVERID.IsNull;
    inc(i);
    taPS.Next;
  end;
  CloseDataSet(taPS);
end;

procedure TfmeCoverDepart.FormClose(Sender: TObject; var Action: TCloseAction);
var
  i : integer;
begin
  case ModalResult of
    mrOk : begin

    end;
  end;
  inherited;
end;

procedure TfmeCoverDepart.chlistPSClickCheck(Sender: TObject);
begin
  if not(chlistPS.Checked[chlistPS.ItemIndex]) then
   ExecSQL('delete from D_PSCover where COVERDEPID='+dm.taDepDEPID.AsString+
      ' and DEPID='+VarToStr(TNodeData(chlistPS.Items.Objects[chlistPS.ItemIndex]).Code), dm.quTmp)
  else begin
    ExecSQL('insert into D_PSCOVER(ID,COVERDEPID,DEPID) values(gen_id(gen_pscover_id, 1),'+
       dm.taDepDEPID.AsString+', '+VarToStr(TNodeData(chlistPS.Items.Objects[chlistPS.ItemIndex]).Code)+')', dm.quTmp);
  end;  
end;

end.
