unit dmEventAlerts;

interface

uses
  SysUtils, Classes, SIBEABase, SIBFIBEA, FIBDatabase, pFIBDatabase, Dialogs;

const

  GROUPS_ALERT = 1;

  MessageGroupsStart = '������� �������� ���. ����!'#13#10 +
                       '������������ ������� ������������ �� ��������� ������, �������� �� �������!';

  MessageGroupsEnd = '�������� ���.���� ��������!';

type

  TAlertMode = (amStart, amEnd);

  TEventAlertsData = class(TDataModule)
    DataBase: TpFIBDatabase;
    Transaction: TpFIBTransaction;
    Alerter: TSIBfibEventAlerter;
    procedure DataModuleCreate(Sender: TObject);
    procedure AlerterEventAlert(Sender: TObject; EventName: string;
      EventCount: Integer);
    procedure DataModuleDestroy(Sender: TObject);
  private

  public
    function CloneDBParams(Source: TpFIBDataBase; Target: TpFIBDataBase) : boolean;
    procedure Alert(EventID:Integer; Mode: TAlertMode);
  end;

var
  EventAlertsData: TEventAlertsData;

implementation

uses DictData, MsgDialog;

{$R *.dfm}

procedure TEventAlertsData.AlerterEventAlert(Sender: TObject; EventName: string;
  EventCount: Integer);
begin

  if EventName = 'TechnologyGroups.end' then
  begin
    MessageDialog(MessageGroupsEnd, mtInformation, [mbOk], 0);
  end;

  if EventName = 'TechnologyGroups.start' then
  begin
    MessageDialog(MessageGroupsStart, mtInformation, [mbOk], 0);
  end;

end;

function TEventAlertsData.CloneDBParams(Source: TpFIBDataBase; Target: TpFIBDataBase): boolean;
begin

  Target.DBParams := Source.DBParams;

  Target.DBName := Source.DBName;

  Target.SQLDialect := Source.SQLDialect;

  Target.LibraryName := Source.LibraryName;

  Target.Timeout := Source.Timeout;

  Target.WaitForRestoreConnect := Source.WaitForRestoreConnect;

  try

    Target.Connected := true;

    Result := true;

  except

    Result := false;

  end;

end;

procedure TEventAlertsData.DataModuleCreate(Sender: TObject);
begin

  if CloneDBParams(dm.db, DataBase) then
  begin
    Transaction.StartTransaction;
    
    Alerter.AutoRegister := true;
  end;

end;

procedure TEventAlertsData.Alert(EventID:Integer; Mode: TAlertMode);
var
  SQL: AnsiString;
begin

  case Mode of
     amStart: SQL := 'execute procedure Event$Alert$Start(:EventID)';
     amEnd: SQL := 'execute procedure Event$Alert$End(:EventID)';
  end;

  SQL := StringReplace(SQL, ':EventID', IntToStr(EventID), [rfIgnoreCase]);

  DataBase.Execute(SQL);

  Transaction.CommitRetaining;

end;

procedure TEventAlertsData.DataModuleDestroy(Sender: TObject);
begin

  if Transaction.InTransaction then
  begin
    Transaction.Commit;
  end;

  DataBase.Connected := false;

end;

end.
