unit frmNormLosses;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, FIBDataSet, cxControls,
  cxGridCustomView, pFIBDataSet, cxClasses, cxGridLevel, cxGrid,
  cxLookAndFeelPainters, ExtCtrls, StdCtrls, cxButtons, cxContainer,
  cxLabel, DBActns, ActnList, ImgList, dxBar, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  dxBarExtDBItems, cxLookAndFeels, Menus;

type
  TDialogNormLosses = class(TForm)
    GridLevel: TcxGridLevel;
    Grid: TcxGrid;
    DataSource: TDataSource;
    DataSet: TpFIBDataSet;
    DataSetMATERIALID: TFIBIntegerField;
    DataSetBEGINDATE: TFIBDateField;
    DataSetNORM: TFIBFloatField;
    View: TcxGridDBTableView;
    ViewBEGINDATE: TcxGridDBColumn;
    ViewNORM: TcxGridDBColumn;
    DataSetCOMPANYID: TFIBIntegerField;
    ButtonClose: TcxButton;
    Bevel: TBevel;
    DataSetMaterial: TpFIBDataSet;
    DataSetMaterialID2: TFIBIntegerField;
    DataSetMaterialTITLE: TFIBStringField;
    cxLabel1: TcxLabel;
    LabelMaterial: TcxLabel;
    BarManager: TdxBarManager;
    dxBarButton1: TdxBarButton;
    dxBarButton2: TdxBarButton;
    dxBarDockControl1: TdxBarDockControl;
    EnabledImages: TImageList;
    Actions: TActionList;
    DataSetInsert: TDataSetInsert;
    DataSetDelete: TDataSetDelete;
    DataSetPost: TDataSetPost;
    DataSetCancel: TDataSetCancel;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    DataSetCompany: TpFIBDataSet;
    DataSetCompanyID2: TFIBIntegerField;
    DataSetCompanyTITLE: TFIBStringField;
    ComboBoxCompany: TcxLookupComboBox;
    DataSourceCompany: TDataSource;
    cxStyleRepository1: TcxStyleRepository;
    StyleDisabled: TcxStyle;
    procedure DataSetNewRecord(DataSet: TDataSet);
    procedure DataSetBeforeOpen(DataSet: TDataSet);
    procedure DataSetBeforePost(DataSet: TDataSet);
    procedure DataSetAfterPost(DataSet: TDataSet);
    procedure ComboBoxCompanyPropertiesChange(Sender: TObject);
  private
    MaterialID: Integer;
    CompanyID: Integer;
    procedure DoExecute(AMaterialID: Integer);
  public
    class procedure Execute(MaterialID: Integer);
  end;


implementation

uses DictData, uDialogs;

{$R *.dfm}

procedure TDialogNormLosses.DataSetNewRecord(DataSet: TDataSet);
begin
  DataSetCOMPANYID.AsInteger := CompanyID;
  DataSetBEGINDATE.AsDateTime := Date;
  DataSetMATERIALID.AsInteger := MaterialID;
end;


procedure TDialogNormLosses.DataSetBeforeOpen(DataSet: TDataSet);
begin
  Self.DataSet.ParamByName('Company$ID').AsInteger := CompanyID;
  Self.DataSet.ParamByName('Material$ID').AsInteger := MaterialID;
end;

procedure TDialogNormLosses.DoExecute(AMaterialID: Integer);
begin
  CompanyID := 0;
  DataSet.Active := False;
  View.Styles.Background := StyleDisabled;
  MaterialID := AMaterialID;
  DataSetMaterial.Active := True;
  if DataSetMaterial.Locate('ID', MaterialID, []) then
  begin
    DataSetCompany.Active := True;
    LabelMaterial.Caption := DataSetMaterialTITLE.AsString;
    ShowModal;
  end
  else DialogErrorOkMessage('�������� �� ����������.');
end;

class procedure TDialogNormLosses.Execute(MaterialID: Integer);
var
  DialogNormLosses: TDialogNormLosses;
begin
  DialogNormLosses := TDialogNormLosses.Create(Application);
  DialogNormLosses.DoExecute(MaterialID);
  DialogNormLosses.Free;
end;

procedure TDialogNormLosses.DataSetBeforePost(DataSet: TDataSet);
begin
  //
end;

procedure TDialogNormLosses.DataSetAfterPost(DataSet: TDataSet);
begin
  Self.DataSet.Transaction.CommitRetaining;
end;

procedure TDialogNormLosses.ComboBoxCompanyPropertiesChange(Sender: TObject);
begin
  CompanyID := ComboBoxCompany.EditValue;
  DataSet.Active := False;
  DataSet.Active := True;
  View.Styles.Background := nil;
end;

end.
