{*******************************************}
{  ��������� �� ���������� �����            }
{  Copyrigth (C) 2002-2003 basile for CDM   }
{  v1.22                                    }
{*******************************************}
unit Zp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus,  ImgList, Grids, DBGrids, RXDBCtrl,
  RxToolEdit, StdCtrls, Mask, DBCtrls, ExtCtrls,
  ComCtrls, db, DBGridEh, DBCtrlsEh, ActnList, TB2Item, TB2Dock,
  TB2Toolbar, PrnDbgeh, FIBDataSet, pFIBDataSet, DBGridEhGrouping,
  rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmZp = class(TfmDocAncestor)
    acEvent: TActionList;
    acAsstReport: TAction;
    Pages: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    gridAssembly: TDBGridEh;
    ppAssembly: TTBPopupMenu;
    TBItem1: TTBItem;
    pgridAssembly: TPrintDBGridEh;
    acRefreshArtK: TAction;
    ppZp: TTBPopupMenu;
    acPrintZp: TAction;
    acPrintZpTotal: TAction;
    acPrintAssembly: TAction;
    TBItem6: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem5: TTBItem;
    TBItem7: TTBItem;
    TBItem8: TTBItem;
    TBItem9: TTBItem;
    TBItem10: TTBItem;
    acDetailAssort: TAction;
    TBItem2: TTBItem;
    spitRefresh: TSpeedItem;
    acRefreshZp: TAction;
    acPrintZpAssort: TAction;
    TBItem3: TTBItem;
    TBItem4: TTBItem;
    TabSheet3: TTabSheet;
    DBGridEh1: TDBGridEh;
    ppZpT: TTBPopupMenu;
    TBItem11: TTBItem;
    acReCalcZpT: TAction;
    TBItem12: TTBItem;
    PageK: TTabSheet;
    GridK: TDBGridEh;
    dsK: TpFIBDataSet;
    dsKOPERATION: TFIBStringField;
    dsKK: TFIBFloatField;
    dsrK: TDataSource;
    procedure grGetCellParams(Sender: TObject; Field: TField; AFont: TFont;
      var Background: TColor; Highlight: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure acAsstReportExecute(Sender: TObject);
    procedure acPrintZpExecute(Sender: TObject);
    procedure acPrintZpTotalExecute(Sender: TObject);
    procedure acPrintAssemblyExecute(Sender: TObject);
    procedure acDetailAssortExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acRefreshZpExecute(Sender: TObject);
    procedure spitAddClick(Sender: TObject);
    procedure acRefreshZpUpdate(Sender: TObject);
    procedure spitDelClick(Sender: TObject);
    procedure spitCloseClick(Sender: TObject);
    procedure acPrintZpAssortExecute(Sender: TObject);
    procedure acReCalcZpTUpdate(Sender: TObject);
    procedure acReCalcZpTExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure dsKBeforeOpen(DataSet: TDataSet);
    procedure PagesChange(Sender: TObject);
  private
    procedure Access;
  end;

var
  fmZp: TfmZp;

implementation

uses MainData, PrintData, dbUtil, DictData, ZpAssort, fmUtils,
  Data;

{$R *.dfm}

procedure TfmZp.FormCreate(Sender: TObject);
var
  i : integer;
begin
  gridItem.DataSource := dmMain.dsrZItem;
  edNoDoc.DataSource := dmMain.dsrZList;
  dtedDateDoc.DataSource := dmMain.dsrZList;
  Access;
  inherited;
  gridItem.Parent := TabSheet1;
  for i:=0 to Pred(gridItem.Columns.Count) do
    gridItem.Columns[i].Title.EndEllipsis := True;
  OpenDataSets([dmMain.taZ_Assembly, dmData.taZpTPS]);
  //ppZp.Skin := dm.ppSkin;
  //ppAssembly.Skin := dm.ppSkin;
  //ppZpT.Skin := dm.ppSkin;
end;

procedure TfmZp.FormShow(Sender: TObject);
begin
  inherited;
  Pages.ActivePageIndex := 0;
  ActiveControl := TabSheet1;
end;

procedure TfmZp.grGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if(Field.FieldName='TZP')then Background := clWhite;
end;

procedure TfmZp.acAsstReportExecute(Sender: TObject);
begin
  CloseDataSet(dmMain.taZ_Assembly);
  ExecSQL('execute procedure Build_Zp_Assembly('+dmMain.taZListID.AsString+')', dmMain.quTmp);
  OpenDataSet(dmMain.taZ_Assembly);
end;

procedure TfmZp.acPrintZpExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taZItem, dmMain.taZList]);
  dmPrint.PrintDocumentA(dkZp, VarArrayOf([VarArrayOf([dmMain.taZListID.AsInteger])]));
end;

procedure TfmZp.acPrintZpTotalExecute(Sender: TObject);
begin
  PostDataSets([dmMain.taZItem, dmMain.taZList]);
  dmPrint.PrintDocumentA(dkZpTotal, VarArrayOf([VarArrayOf([dmMain.taZListID.AsInteger])]));
end;

procedure TfmZp.acPrintAssemblyExecute(Sender: TObject);
begin
  pgridAssembly.Print;
end;

procedure TfmZp.acDetailAssortExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmZpAssort, TForm(fmZpAssort));
end;

procedure TfmZp.gridItemGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  if(Column.FieldName = 'S')and(abs(Column.Field.AsFloat) > 0.001)then Background := clRed
  else if(Column.FieldName='EP1')and(Column.Field.AsFloat > 10)then Background := clSkyBlue;
end;

procedure TfmZp.acRefreshZpExecute(Sender: TObject);
var
  Id : integer;
begin
  Pages.ActivePageIndex := 0;
  dsK.Active := False;
  PostDataSet(dmMain.taZItem);
  Application.ProcessMessages;
  Screen.Cursor := crSQLWait;
  showMessage(dmMain.taZListID.AsString);
  with dmMain do
  try
    ExecSQL('execute procedure G_Norma("'+DateToStr(dm.zBeginDate)+'", "'+DateTimeToStr(dm.zEndDate)+'")', quTmp);
    Application.ProcessMessages;
    ExecSQL('execute procedure Create_Zp('+taZListID.AsString+')', quTmp);
    Application.ProcessMessages;
    ExecSQL('execute procedure Build_Zp_Assembly('+taZListID.AsString+')', quTmp);
    Id := dmMain.taZItemID.AsInteger;
    ReOpenDataSet(dmMain.taZItem);
    dmMain.taZItem.Locate('ID', Id, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmZp.spitAddClick(Sender: TObject);
begin
  if FReadOnly then eXit;
  inherited;
  gridItem.SelectedField := dmMain.taZItemPSNAME;
end;

procedure TfmZp.Access;
begin
  with stbrStatus.Panels[0] do
    if dm.IsAdm then Text := 'Adm'
    else if((dm.User.AccProd_d and 4) = 4)then
    begin
     Text := 'View';
     FReadOnly := True;
    end
    else Text := 'Full';
  if dm.IsAdm then eXit;
end;

procedure TfmZp.acRefreshZpUpdate(Sender: TObject);
begin
  acRefreshZp.Enabled := (not FReadOnly);
end;

procedure TfmZp.spitDelClick(Sender: TObject);
begin
  if FReadOnly then eXit;
  inherited;
end;

procedure TfmZp.spitCloseClick(Sender: TObject);
begin
  if FReadOnly then eXit;
  inherited;
end;

procedure TfmZp.acPrintZpAssortExecute(Sender: TObject);
begin
  dmPrint.PrintDocumentA(dkZpAssort, VarArrayOf([VarArrayOf([dmMain.taZItemID.AsInteger])]));
end;

procedure TfmZp.acReCalcZpTUpdate(Sender: TObject);
begin
  acReCalcZpT.Visible := (dm.User.Profile = -1) or (dm.User.Profile = 3);
end;

procedure TfmZp.acReCalcZpTExecute(Sender: TObject);
begin
  CloseDataSet(dmData.taZpTPS);
  ExecSQL('execute procedure Create_Zp_T('+dmMain.taZListID.AsString+', null)', dmMain.quTmp);
  OpenDataSet(dmData.taZpTPS);
end;

procedure TfmZp.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if dsK.Active then dsK.Active := False;
  CloseDataSets([dmMain.taZ_Assembly, dmData.taZpTPS]);
end;

procedure TfmZp.dsKBeforeOpen(DataSet: TDataSet);
begin
  dsK.ParamByName('zpid').AsInteger := dmMain.taZListID.AsInteger;

end;

procedure TfmZp.PagesChange(Sender: TObject);
begin
  if Pages.ActivePage = PageK then
  begin
    if not dsK.Active then
    dsK.Active := True;
  end;
end;

end.
