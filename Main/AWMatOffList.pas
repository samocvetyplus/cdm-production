unit AWMatOffList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, TB2Item, ActnList, Menus,
  ImgList, Grids, DBGridEh, StdCtrls, ExtCtrls, ComCtrls, DB,
  FIBDataSet, pFIBDataSet, Mask, DBCtrlsEh, DBGridEhGrouping, rxPlacemnt,
  GridsEh, rxSpeedbar;

type
  TfmAWMatOffList = class(TfmListAncestor)
    taAddComp: TpFIBDataSet;
    acCloseAll: TAction;
    acRefreshAll: TAction;
    acOpenAll: TAction;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem8: TTBItem;
    TBItem9: TTBItem;
    TBItem10: TTBItem;
    ppPrint: TTBPopupMenu;
    TBItem5: TTBItem;
    acPrintSvod: TAction;
    Label1: TLabel;
    cmbxDoc: TDBComboBoxEh;
    taDocList: TpFIBDataSet;
    taDocListDOCNO: TFIBIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acAllExecute(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintSvodExecute(Sender: TObject);
    procedure cmbxDocChange(Sender: TObject);
    procedure acPeriodUpdate(Sender: TObject);
    procedure taDocListBeforeOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure FillDocList;
  end;

var
  fmAWMatOffList: TfmAWMatOffList;

implementation

uses Data, AWMatOff, DictData, eSelDate, Editor, dbUtil, DocAncestor,
  PrintData, DateUtils, dbTree;

{$R *.dfm}

procedure TfmAWMatOffList.FormCreate(Sender: TObject);
begin
  dm.WorkMode := 'AWMatOff';
  gridDocList.DataSource := dmData.dsrAWMOList;
  FillDocList;
  inherited;
end;

procedure TfmAWMatOffList.acAddDocExecute(Sender: TObject);
var
  r: Variant;
//  Msg : string;
  DocNo : Variant;
  f : boolean;
  i : integer;
begin
  if not ShowDate(Now) then eXit;
  // ����� ����
  DocNo :=  ExecSelectSQL('select coalesce(max(DocNo)+1, 1) from Inv where IType=26 and SelfCompId='+IntToStr(dm.User.SelfCompId), dm.quTmp);
  if VarIsNull(DocNo) then DocNo := 1;
  // ������� �����������, ��� ������� ���� ������
  taAddComp.Close;
  taAddComp.ParamByName('SELFCOMPID').AsInteger := dm.User.SelfCompId;
  taAddComp.ParamByName('ED').AsTimeStamp :=  DateTimeToTimeStamp(VarToDateTime(Editor.vResult));
  taAddComp.Open;
  while not taAddComp.Eof do
  begin
    r := ExecSelectSQL('select BD, REFID from GetAWMatOffPeriod('+taAddComp.Fields[0].AsString+')', dm.quTmp);
    if VarToDateTime(r[0]) > VarToDateTime(Editor.vResult) then
    begin
      taAddComp.Next;
      continue;
    end;
    dmData.taAWMOList.Append;
    dmData.taAWMOListDOCNO.AsInteger := DocNo;
    dmData.taAWMOListABD.AsDateTime := StartOfTheDay(IncDay(VarToDateTime(r[0])));
    dmData.taAWMOListAED.AsDateTime := EndOfTheDay(VarToDateTime(Editor.vResult));
    dmData.taAWMOListSUPID.AsInteger := taAddComp.Fields[0].AsInteger;
    dmData.taAWMOListREF.AsVariant := r[1];
    dmData.taAWMOList.Post;
    dmData.taAWMOList.Transaction.CommitRetaining;
    dmData.taAWMOList.Refresh;
    taAddComp.Next;
  end;
  taAddComp.Close;
  FillDocList;
  f := False;
  for i:=0 to Pred(cmbxDoc.Items.Count) do
    if TNodeData(cmbxDoc.Items.Objects[i]).Code = DocNo then
    begin
      cmbxDoc.ItemIndex := i;
      f := True;
      break;
    end;
  if not f then cmbxDoc.ItemIndex := -1;
  //CloseDataSet(dmData.taAWMOList);
  //dmData.taAWMOList.ParamByName('DOCNO').AsInteger := DocNo;
  //OpenDataSet(dmData.taAWMOList);
end;

procedure TfmAWMatOffList.acEditDocExecute(Sender: TObject);
begin
  ShowDocForm(TfmAWMatOff, dmData.taAWMOList, dmData.taAWMOListINVID.AsInteger, dm.quTmp);
  dmData.taAWMOList.Refresh;
end;

procedure TfmAWMatOffList.acAllExecute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  Bookmark := DataSet.GetBookmark;
  DataSet.DisableControls;
  DataSet.DisableScrollEvents;
  try
    DataSet.First;
    while not DataSet.Eof do
    begin
      case TComponent(Sender).Tag of 
        1 : ExecSQL('execute procedure OPENDOC("�������� ���������� �������",'+dmData.taAWMOListINVID.AsString+')', dm.quTmp);
        2 : ExecSQL('execute procedure CLOSEDOC("�������� ���������� �������",'+dmData.taAWMOListINVID.AsString+')', dm.quTmp);
        3 : if dmData.taAWMOListISCLOSE.AsINteger = 0 then ExecSQL('execute procedure CREATE_AWMATOFF('+dmData.taAWMOListINVID.AsString+')', dm.quTmp);
      end;
      DataSet.Refresh;
      DataSet.Next;
    end;
  finally
    DataSet.GotoBookmark(Bookmark);
    DataSet.EnableScrollEvents;
    DataSet.EnableControls;
  end;
end;

procedure TfmAWMatOffList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if DataSet.FieldByName('ISCLOSE').AsInteger = 1 then Background := clCloseDoc
  else Background := clOpenDoc;
end;

procedure TfmAWMatOffList.acPrintSvodExecute(Sender: TObject);
begin
  if cmbxDoc.ItemIndex < 0 then eXit;
  dmPrint.PrintDocumentA(dkAWMOTotal, VarArrayOf([VarArrayOf([TNodeData(cmbxDoc.Items.Objects[cmbxDoc.ItemIndex]).Code, dm.User.SelfCompId])]));
end;

procedure TfmAWMatOffList.cmbxDocChange(Sender: TObject);
begin
  CloseDataSet(dmData.taAWMOList);
  if cmbxDoc.ItemIndex < 0 then eXit;
  dmData.taAWMOList.ParamByName('DOCNO').AsInteger := TNodeData(cmbxDoc.Items.Objects[cmbxDoc.ItemIndex]).Code;
  OpenDataSet(dmData.taAWMOList);
end;

procedure TfmAWMatOffList.acPeriodUpdate(Sender: TObject);
begin
  inherited;
  acPeriod.Visible := False;
end;

procedure TfmAWMatOffList.FillDocList;
var
  nd : TNodeData;
  r : Variant;
begin
  // ������������ ������� �� �������
  cmbxDoc.OnChange := nil;
  cmbxDoc.Items.Clear;
  OpenDataSet(taDocList);
  while not taDocList.Eof do
  begin
    nd := TNodeData.Create;
    nd.Code := taDocListDOCNO.AsInteger;
    r := ExecSelectSQL('select first 1 ABD, AED from Inv where DOCNO=?DOCNO and ITYPE=26 and SELFCOMPID=?SELFCOMPID',
                       VarArrayOf([taDocListDOCNO.AsInteger, dm.User.SelfCompId]), dm.quTmp);
    if not VarIsNull(r) then cmbxDoc.Items.AddObject('#'+taDocListDOCNO.AsString+' c '+DateTimeToStr(r[0])+' �� '+DateTimeToStr(r[1]), nd)
    else cmbxDoc.Items.AddObject('#'+taDocListDOCNO.AsString, nd);
    taDocList.Next;
  end;
  CloseDataSet(taDocList);
  cmbxDoc.OnChange := cmbxDocChange;
end;

procedure TfmAWMatOffList.taDocListBeforeOpen(DataSet: TDataSet);
begin
   dm.SetSelfCompParams(taDocList.Params);
end;

procedure TfmAWMatOffList.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CloseDataSet(dmData.taAWMOList);
end;

end.
