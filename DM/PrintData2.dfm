object dmPrint2: TdmPrint2
  OldCreateOrder = False
  Height = 568
  Width = 799
  object frActWorkList: TfrDBDataSet
    DataSet = dmData.taActWorkList
    Left = 32
    Top = 28
  end
  object frActWork: TfrDBDataSet
    DataSet = dmData.taActWork
    Left = 100
    Top = 28
  end
  object taComp: TpFIBDataSet
    SelectSQL.Strings = (
      'select c.*, co.*'
      'from CompOwner co, D_Comp c'
      'where co.CompId=c.CompId and'
      '      co.SelfCompId between :COMPID1 and :COMPID2'
      'order by c.SortInd')
    AfterOpen = taCompAfterOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = '80 '#1057#1087#1080#1089#1086#1082' '#1086#1088#1075#1072#1085#1080#1079#1072#1094#1080#1081
    Left = 172
    Top = 8
    poSQLINT64ToBCD = True
  end
  object frComp: TfrDBDataSet
    DataSet = taComp
    Left = 172
    Top = 52
  end
  object taRequest: TpFIBDataSet
    SelectSQL.Strings = (
      'select a2.ART, sz.NAME SZNAME, r.Q, r.MW, '
      '       i.ABD, i.AED, c.NAME COMPNAME'
      'from Buyer_Request r, Art2 a2, D_Sz sz, Inv i, D_Comp c'
      'where r.INVID = i.INVID and '
      '      r.ART2ID = a2.ART2ID and'
      '      r.SZID = sz.ID and'
      '      r.Q <>0 and'
      '      i.INVID = :INVID and'
      '      i.SUPID = c.COMPID'
      'order by a2.Art, sz.Name')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = '81 '#1047#1072#1103#1074#1082#1072' '#1086#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1103
    Left = 232
    Top = 8
    poSQLINT64ToBCD = True
  end
  object frRequest: TfrDBDataSet
    DataSet = taRequest
    Left = 232
    Top = 52
  end
  object taPItem_PS_Balance: TpFIBDataSet
    SelectSQL.Strings = (
      'select  s.SEMIS SEMISNAME, a.RQ, a.RW'
      'from AIt_InSemis a, D_Semis s'
      
        'where a.PROTOCOLID=:PROTOCOLID and                              ' +
        ' '
      '      a.OPERID=:OPERID and'
      '      a.SEMISID=s.SEMISID and'
      '      not (a.RQ=0 and equal(RW, 0, 0.001)=1)'
      'order by s.SORTIND')
    BeforeOpen = taPItem_PS_BalanceBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = '82 '#1054#1089#1090#1072#1090#1086#1082' '#1087#1086' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1072#1084' '#1087#1086' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1080
    Left = 320
    Top = 8
    poSQLINT64ToBCD = True
  end
  object frPItem_PS: TfrDBDataSet
    DataSet = taPItem_PS_Balance
    Left = 320
    Top = 52
  end
  object taAWG: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from DOC_ACTWORKLISTGENERAL(:BD, :ED, :SUPID1, :SUPID2) '
      '')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1057#1074#1086#1076#1085#1099#1081' '#1086#1090#1095#1077#1090' '#1087#1086' '#1072#1082#1090#1072#1084' '#1074#1099#1087#1086#1083#1085#1077#1085#1099#1093' '#1088#1072#1073#1086#1090
    Left = 24
    Top = 100
    poSQLINT64ToBCD = True
  end
  object frAWG: TfrDBDataSet
    DataSet = taAWG
    Left = 24
    Top = 144
  end
  object taVerificationAct: TpFIBDataSet
    SelectSQL.Strings = (
      'select case it.InvSubTypeId '
      '         when 20 then -1000'
      '         when 21 then 1000'
      '         else it.InvSubTypeId'
      '       end, it.ID, it.INVID, it.MATID, it.INVSUBTYPEID,'
      '    it.Q, it.W, it.N, st.Name INVSUBNAME, m.Name MATNAME    '
      
        'from AWItem it left join D_InvSubType st on (it.InvSubTypeId=st.' +
        'Id),'
      '     D_Mat m '
      'where it.INVID=:INVID and'
      '      it.MatId = m.Id'
      'order by m.SORTIND, 1')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 260
    Top = 104
    poSQLINT64ToBCD = True
  end
  object frVerificationAct: TfrDBDataSet
    DataSet = taVerificationAct
    Left = 260
    Top = 148
  end
  object taVerificationHeader: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.*, c1.NAME SELFCOMPNAME, c2.NAME SUPNAME'
      'from Inv i, D_Comp c1, D_Comp c2'
      'where i.INVID=:INVID and'
      '      i.SELFCOMPID=c1.COMPID and'
      '      i.SUPID=c2.COMPID'
      '      ')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 352
    Top = 104
    poSQLINT64ToBCD = True
  end
  object frVerificationHeader: TfrDBDataSet
    DataSet = taVerificationHeader
    Left = 356
    Top = 148
  end
  object taMX18: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.DOCNO, i.DOCDATE, c.OKPO, c.NAME COMPNAME,'
      '       c.PHONE, c.ADDRESS, s1.NAME SUPNAME, c.BOSSFIO'
      'from Inv i, D_Comp c, D_Comp s1'
      'where i.INVID=:INVID and'
      '      i.SELFCOMPID = c.COMPID and'
      '      i.SUPID=s1.COMPID'
      ' '
      '        ')
    AfterOpen = CommitRetaining
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 440
    Top = 104
    poSQLINT64ToBCD = True
  end
  object frMX18: TfrDBDataSet
    DataSet = taMX18
    Left = 440
    Top = 148
  end
  object frMX18Item: TfrDBDataSet
    DataSet = taMX18Item
    Left = 504
    Top = 148
  end
  object taMX18Item: TpFIBDataSet
    SelectSQL.Strings = (
      'select count(*) Q, sum(it.W) W, sum(s.SCost) SCOST, '
      '       a2.ART, a2.ART2, p.prill'
      'from ServItem s, SItem it, Art2 a2, d_semis se, d_prill p'
      'where s.INVID=:INVID and'
      '      it.SITEMID = s.SITEMID and'
      '      a2.Art2Id = it.Art2Id'
      '      and se.semisid = a2.semis'
      '      and p.id = se.prill'
      'group by a2.ART, a2.ART2, p.prill')
    AfterOpen = CommitRetaining
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 504
    Top = 104
    poSQLINT64ToBCD = True
  end
  object taAWRetServ: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select sum(it.W) W, i.DOCNO, i.DOCDATE, i.COST, co.no_contract, ' +
        'co.bd_contract'
      'from INV i, ServItem si, SItem it, compowner co'
      'where i.INVID=:INVID and'
      '      si.INVID=i.INVID and'
      '      si.SITEMID=it.SITEMID and'
      '      co.compid = i.selfcompid and   '
      '      co.selfcompid = i.supid'
      
        'group by i.DOCNO, i.DOCDATE, i.COST, co.no_contract, co.bd_contr' +
        'act'
      ''
      '')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1040#1082#1090' '#1086' '#1074#1079#1072#1080#1084#1085#1099#1093' '#1088#1072#1089#1095#1077#1090#1072#1093
    Left = 24
    Top = 196
    poSQLINT64ToBCD = True
  end
  object frAWRetServ: TfrDBDataSet
    DataSet = taAWRetServ
    Left = 24
    Top = 240
  end
  object taAWRej: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.DOCNO, i.DOCDATE, it.W, s.SEMIS SEMISNAME,  '
      '       a2.ART, si.COMMENT, c.NAME PRODNAME'
      'from Inv i, ServItem si, SItem it, D_Semis s, ART2 a2,'
      '     D_COMP c'
      'where i.INVID=:INVID and'
      '      si.INVID=i.INVID and'
      '      si.SITEMID=it.SITEMID and'
      '      it.ART2ID=a2.ART2ID and'
      '      a2.SEMIS=s.SEMISID and'
      '      a2.D_COMPID=c.COMPID'
      'order by a2.ART '
      '  '
      '')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1040#1082#1090' '#1073#1088#1072#1082#1072
    Left = 92
    Top = 196
    poSQLINT64ToBCD = True
  end
  object frAWRej: TfrDBDataSet
    DataSet = taAWRej
    Left = 92
    Top = 244
  end
  object taAWTorg12: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT '
      '          DOCNO ,'
      '          DOCDATE ,'
      '          SUPNAME ,'
      '          CUSTNAME ,'
      '          SUPADDRESS ,'
      '          SUPPHONE ,'
      '          CUSTADDRESS ,'
      '          CUSTPHONE ,'
      '          SUPINN ,'
      '          SUPOKPO ,'
      '          SUPOKONH, '
      '          SUPBILL ,'
      '          SUPKBILL,'
      '          SUPBIK ,'
      '          SUPBANK ,'
      '          CUSTINN ,'
      '          CUSTOKPO ,'
      '          CUSTOKONH ,'
      '          CUSTBILL ,'
      '          CUSTKBILL ,'
      '          CUSTBIK ,'
      '          CUSTBANK,'
      '          COMMISION,'
      '          TRANS_PRICE,'
      '          SUPKPP,'
      '          PAYTYPENAME '
      'from INV_TO_PRINT(:INVID)')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 164
    Top = 196
    poSQLINT64ToBCD = True
  end
  object taAWTorg12Item: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from REPORT$ACT$TRADE12(:INVID)')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 248
    Top = 197
    poSQLINT64ToBCD = True
  end
  object frAWTorg12: TfrDBDataSet
    DataSet = taAWTorg12
    Left = 164
    Top = 244
  end
  object frAWTorg12Item: TfrDBDataSet
    DataSet = taAWTorg12Item
    Left = 248
    Top = 244
  end
  object frRevision_Sum: TfrDBDataSet
    DataSet = taRevision_Sum
    Left = 440
    Top = 56
  end
  object taRevision_Sum: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT'
      '    OVER_COUNT,'
      '    OVER_W,'
      '    NEED_COUNT,'
      '    NEED_W'
      'FROM'
      '    REVISION_SUM(:INVID) ')
    BeforeOpen = taRevision_SumBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 440
    Top = 8
    poSQLINT64ToBCD = True
    oProtectedEdit = True
    object taRevision_SumOVER_COUNT: TFIBIntegerField
      FieldName = 'OVER_COUNT'
    end
    object taRevision_SumOVER_W: TFIBFloatField
      FieldName = 'OVER_W'
    end
    object taRevision_SumNEED_COUNT: TFIBIntegerField
      FieldName = 'NEED_COUNT'
    end
    object taRevision_SumNEED_W: TFIBFloatField
      FieldName = 'NEED_W'
    end
  end
  object taAWG_Null: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.DOCNO, i.DOCDATE, i.W, c.NAME COMPNAME, '
      '  (select FSumm from Inv_Sum(i.INVID)) COST'
      'from Inv i left join D_Comp c on (i.SUPID=c.COMPID)'
      'where i.W2=0  and'
      '      i.DOCDATE between :BD and :ED and'
      '      i.ITYPE=2 and'
      '      i.SELFCOMPID<>:SELFCOMPID'
      'order by i.DOCDATE')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = 
      #1042' '#1089#1074#1086#1076#1085#1086#1084' '#1086#1090#1095#1077#1090#1077' '#1074#1099#1074#1086#1076#1080#1090#1089#1103' '#1074#1085#1080#1079#1091' '#1089#1087#1080#1089#1086#1082' '#1085#1072#1082#1083#1072#1076#1085#1099#1093', '#1089' '#1089#1091#1084#1084#1086#1081' '#1091#1089#1083#1091 +
      #1075' '#1088#1072#1074#1085#1086#1081' 0'
    Left = 160
    Top = 100
    poSQLINT64ToBCD = True
  end
  object frAWG_Null: TfrDBDataSet
    DataSet = taAWG_Null
    Left = 160
    Top = 144
  end
  object taInvArtPhoto: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct a.ART'
      'from SEL e, D_ART a '
      'where e.INVID=:INVID and'
      '      e.D_ARTID=a.D_ARTID and'
      '      a.Pict is null'
      '      ')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1042#1099#1073#1088#1072#1090#1100' '#1080#1079' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1072#1088#1090#1080#1082#1091#1083#1099' '#1091' '#1082#1086#1090#1086#1088#1099#1093' '#1085#1077#1090' '#1092#1086#1090#1086#1075#1088#1072#1092#1080#1081
    Left = 440
    Top = 196
    poSQLINT64ToBCD = True
  end
  object frInvArtPhoto: TfrDBDataSet
    DataSet = taInvArtPhoto
    Left = 440
    Top = 244
  end
  object taAWMOTotal: TpFIBDataSet
    SelectSQL.Strings = (
      'select c.NAME SUPNAME, i.INVID, i.W2 P'
      'from Inv i, D_Comp c'
      'where i.DOCNO = :DOCNO and'
      '      i.SUPID=c.COMPID and'
      '      i.ITYPE=26 and'
      '      i.SELFCOMPID=:SELFCOMPID'
      'order by c.SORTIND')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 516
    Top = 196
    poSQLINT64ToBCD = True
  end
  object frAWMOTotal: TfrDBDataSet
    DataSet = taAWMOTotal
    Left = 516
    Top = 244
  end
  object taAWMOTotalItem: TpFIBDataSet
    SelectSQL.Strings = (
      'select *'
      'from DOC_AWMOTOTALITEM(:INVID,:BD,:ED)'
      ' ')
    BeforeOpen = taAWMOTotalItemBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DataSource = dsrAWMOTotal
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 600
    Top = 196
    poSQLINT64ToBCD = True
    dcForceOpen = True
  end
  object frAWMOTotalItem: TfrDBDataSet
    DataSet = taAWMOTotalItem
    Left = 600
    Top = 244
  end
  object dsrAWMOTotal: TDataSource
    DataSet = taAWMOTotal
    Left = 520
    Top = 296
  end
  object taInvUID: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  it.UID, '
      '  a2.ART, '
      '  a2.ART2, '
      '  it.W, '
      '  sz.NAME SZNAME,'
      '  s.semis,'
      '  e.tprice,'
      '  e.price2,'
      '  p.prill,'
      '  cast(1 as integer) Q,'
      '  m.Name MatName,'
      '  i.Name InsName,'
      '  a2.Art2ID Art2ID'
      'from '
      '  SEl e '
      '  left outer join SITEM it on (e.SELID=it.SELID)  '
      '  left outer join ART2 a2 on (it.ART2ID=a2.ART2ID) '
      '  left outer join D_SZ sz on (it.SZ=sz.ID)'
      '  left outer join D_Semis s on (it.SemisId = s.SemisID)'
      '  left outer join D_Prill p on (s.prill = p.id)'
      '  left outer join d_mat m on (s.mat = m.id)'
      '  left outer join d_ins i on (a2.d_insid = i.d_insid)'
      'where '
      '  e.INVID=:INVID'
      'order by '
      '  a2.ART, '
      '  a2.ART2, '
      '  sz.SORTIND'
      '')
    AfterScroll = taInvUIDAfterScroll
    BeforeOpen = taInvUIDBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 24
    Top = 296
    poSQLINT64ToBCD = True
    object taInvUIDUID: TFIBIntegerField
      FieldName = 'UID'
    end
    object taInvUIDART: TFIBStringField
      FieldName = 'ART'
      EmptyStrToNull = True
    end
    object taInvUIDART2: TFIBStringField
      FieldName = 'ART2'
      EmptyStrToNull = True
    end
    object taInvUIDW: TFIBFloatField
      FieldName = 'W'
    end
    object taInvUIDSZNAME: TFIBStringField
      FieldName = 'SZNAME'
      EmptyStrToNull = True
    end
    object taInvUIDSEMIS: TFIBStringField
      FieldName = 'SEMIS'
      Size = 40
      EmptyStrToNull = True
    end
    object taInvUIDTPRICE: TFIBFloatField
      FieldName = 'TPRICE'
    end
    object taInvUIDPRICE2: TFIBFloatField
      FieldName = 'PRICE2'
    end
    object taInvUIDPRILL: TFIBStringField
      FieldName = 'PRILL'
      EmptyStrToNull = True
    end
    object taInvUIDQ: TFIBIntegerField
      FieldName = 'Q'
    end
    object taInvUIDMATNAME: TFIBStringField
      FieldName = 'MATNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taInvUIDINSNAME: TFIBStringField
      FieldName = 'INSNAME'
      EmptyStrToNull = True
    end
    object taInvUIDART2ID: TFIBIntegerField
      FieldName = 'ART2ID'
    end
  end
  object frInvUID: TfrDBDataSet
    DataSet = taInvUID
    Left = 24
    Top = 340
  end
  object taZpKArt: TpFIBDataSet
    SelectSQL.Strings = (
      'select k.ID, k.ARTID, k.OPERID, a.ART, k.K, k.ARTK, a.MW'
      'from ZPK k, D_Art a'
      'where k.OPERID=:OPERID and           '
      '      k.ARTID=a.D_ARTID and'
      '      a.Art like :ART  and'
      '      (k.K+k.ARTK) <> 100'
      'order by a.ART')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 344
    Top = 296
    poSQLINT64ToBCD = True
  end
  object frZpKSemis: TfrDBDataSet
    DataSet = taZpKSemis
    Left = 100
    Top = 340
  end
  object frZpKArt: TfrDBDataSet
    DataSet = taZpKArt
    Left = 344
    Top = 340
  end
  object taZpKSemis: TpFIBDataSet
    UpdateSQL.Strings = (
      'update ZPK_SEMIS set'
      '  K=:K'
      'where ID=:ID')
    SelectSQL.Strings = (
      'select zs.ID, s.SEMIS, zs.K'
      'from D_Semis s, ZpK_Semis zs  '
      'where bit(s.Types, 1)=1 and '
      '           zs.OPERID=:OPERID and'
      '           s.SEMISID=zs.SEMISID'
      'order by s.SORTIND')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 100
    Top = 296
    poSQLINT64ToBCD = True
    object taZpKSemisID: TIntegerField
      FieldName = 'ID'
      Origin = 'ZPK_SEMIS.ID'
    end
    object taZpKSemisSEMIS: TFIBStringField
      DisplayLabel = #1055#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090
      FieldName = 'SEMIS'
      Origin = 'D_SEMIS.SEMIS'
      Required = True
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taZpKSemisK: TFloatField
      DisplayLabel = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
      FieldName = 'K'
      Origin = 'ZPK_SEMIS.K'
    end
  end
  object taZpKTech: TpFIBDataSet
    UpdateSQL.Strings = (
      'update ZpK_Tech set'
      '  K=:K'
      'where ID=:ID')
    SelectSQL.Strings = (
      'select zt.ID, t.NAME, zt.K'
      'from D_Tech t, ZpK_Tech zt'
      'where zt.OPERID=:OPERID and'
      '           zt.TECHID=t.ID'
      'order by t.SORTIND')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 164
    Top = 296
    poSQLINT64ToBCD = True
    object taZpKTechID: TIntegerField
      FieldName = 'ID'
      Origin = 'ZPK_TECH.ID'
      Required = True
    end
    object taZpKTechNAME: TFIBStringField
      DisplayLabel = #1058#1077#1093#1085#1086#1083#1086#1075#1080#1103
      FieldName = 'NAME'
      Origin = 'D_TECH.NAME'
      FixedChar = True
      Size = 40
      EmptyStrToNull = True
    end
    object taZpKTechK: TFloatField
      DisplayLabel = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
      FieldName = 'K'
      Origin = 'ZPK_TECH.K'
      Required = True
    end
  end
  object taZpKIns: TpFIBDataSet
    UpdateSQL.Strings = (
      'update ZpK_Ins set'
      '  K=:K'
      'where ID=:ID')
    SelectSQL.Strings = (
      'select zi.ID, i.NAME, zi.K'
      'from D_Ins i, ZpK_Ins zi'
      'where zi.OPERID=:OPERID and'
      '           zi.INSID=i.D_INSID '
      'order by i.SORTIND')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 228
    Top = 296
    poSQLINT64ToBCD = True
    object taZpKInsID: TIntegerField
      FieldName = 'ID'
      Origin = 'ZPK_INS.ID'
      Required = True
    end
    object taZpKInsNAME: TFIBStringField
      DisplayLabel = #1050#1072#1084#1077#1085#1100
      FieldName = 'NAME'
      Origin = 'D_INS.NAME'
      FixedChar = True
      EmptyStrToNull = True
    end
    object taZpKInsK: TFloatField
      DisplayLabel = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090
      FieldName = 'K'
      Origin = 'ZPK_INS.K'
      Required = True
    end
  end
  object taZpKW: TpFIBDataSet
    UpdateSQL.Strings = (
      'update ZpK_W set'
      '  K1=:K1,'
      '  K2=:K2,'
      '  K3=:K3,'
      '  MINW=:MINW,'
      '  MAXW=:MAXW'
      'where ID=:ID')
    InsertSQL.Strings = (
      'insert into ZpK_W(ID, OPERID, MINW, MAXW, K1, K2, K3, OPERID)'
      'values (:ID, :OPERID, :MINW, :MAXW, :K1, :K2, :K3, :OPERID)')
    SelectSQL.Strings = (
      'select z.ID, z.MINW, z.MAXW, z.K1, z.K2, z.K3, o.OPERATION'
      'from ZpK_W z, D_Oper o '
      'where z.OPERID=:OPERID and'
      '      z.OPERID=o.OPERID ')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 284
    Top = 296
    poSQLINT64ToBCD = True
    object taZpKWID: TIntegerField
      FieldName = 'ID'
      Origin = 'ZPK_W.ID'
      Required = True
    end
    object taZpKWMINW: TFloatField
      DefaultExpression = '0'
      FieldName = 'MINW'
      Origin = 'ZPK_W.MINW'
    end
    object taZpKWMAXW: TFloatField
      DefaultExpression = '0'
      FieldName = 'MAXW'
      Origin = 'ZPK_W.MAXW'
      Required = True
    end
    object taZpKWK1: TFloatField
      DefaultExpression = '0'
      FieldName = 'K1'
      Origin = 'ZPK_W.K1'
    end
    object taZpKWK2: TFloatField
      DefaultExpression = '0'
      FieldName = 'K2'
      Origin = 'ZPK_W.K2'
    end
    object taZpKWK3: TFloatField
      DefaultExpression = '0'
      FieldName = 'K3'
      Origin = 'ZPK_W.K3'
    end
    object taZpKWOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object frZpKTech: TfrDBDataSet
    DataSet = taZpKTech
    Left = 164
    Top = 340
  end
  object frZpKIns: TfrDBDataSet
    DataSet = taZpKIns
    Left = 228
    Top = 340
  end
  object frZpKW: TfrDBDataSet
    DataSet = taZpKW
    Left = 284
    Top = 340
  end
  object taAW_SelfMat_Ext: TpFIBDataSet
    SelectSQL.Strings = (
      'select m.NAME MATNAME, i.GETW, i.SCOST'
      'from EServ_Mat i, D_Mat m'
      'where i.INVID=:INVID and'
      '      i.COMPID=:SELFCOMPID and'
      '      i.MATID = m.ID')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1040#1082#1090' '#1074#1099#1087#1086#1083#1085#1077#1085#1085#1099#1093' '#1088#1072#1073#1086#1090' '#1042#1085#1077#1096#1085#1080#1081
    Left = 100
    Top = 408
    poSQLINT64ToBCD = True
  end
  object taPTHead_Ext: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select i.DOCNO, i.DOCDATE, c1.NAME SELFNAME, c1.BOSSFIO SELFBOSS' +
        'FIO,'
      '       c2.NAME SUPNAME, co.No_Contract, co.Bd_Contract,'
      '       (select FSUMM from INV_SUM(:INVID)) COST, '
      '       c1.BOSSPOST SELFBOSSPOST'
      'from Inv i left join D_Comp c2 on (i.SupId=c2.CompId)'
      '           left join D_Comp c1 on (i.SelfCompId=c1.CompId)'
      
        '           left join CompOwner co on (co.SELFCOMPID=:SELFCOMPID ' +
        'and co.COMPID=c2.COMPID)'
      'where i.INVID=:INVID')
    AfterOpen = taPTHead_ExtAfterOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1054#1090#1095#1077#1090' '#1086' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080' ('#1074#1085#1077#1096#1085#1080#1081') '#1064#1072#1087#1082#1072
    Left = 208
    Top = 408
    poSQLINT64ToBCD = True
  end
  object taPT_Ext: TpFIBDataSet
    SelectSQL.Strings = (
      'select m.NAME MATNAME, i.GETW, i.SCOST, i.NW,'
      '    i.SCOST,'
      '    i.N, i.GETW, i.OUTW '
      'from EServ_Mat i, D_Mat m'
      'where i.INVID=:INVID and'
      '      i.COMPID=:SUPID and'
      '      i.MATID = m.ID')
    AfterOpen = taPT_ExtAfterOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Description = #1054#1090#1095#1077#1090' '#1086' '#1087#1077#1088#1077#1088#1072#1073#1086#1090#1082#1080' ('#1074#1085#1077#1096#1085#1080#1081') '#1058#1072#1073#1083#1080#1094#1072
    Left = 280
    Top = 408
    poSQLINT64ToBCD = True
    object taPT_ExtMATNAME: TFIBStringField
      FieldName = 'MATNAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taPT_ExtGETW: TFIBFloatField
      FieldName = 'GETW'
    end
    object taPT_ExtSCOST: TFIBFloatField
      FieldName = 'SCOST'
    end
    object taPT_ExtNW: TFIBFloatField
      FieldName = 'NW'
    end
    object taPT_ExtSCOST1: TFIBFloatField
      FieldName = 'SCOST1'
    end
    object taPT_ExtN: TFIBFloatField
      FieldName = 'N'
    end
    object taPT_ExtGETW1: TFIBFloatField
      FieldName = 'GETW1'
    end
    object taPT_ExtOUTW: TFIBFloatField
      FieldName = 'OUTW'
    end
  end
  object frPTHead_Ext: TfrDBDataSet
    DataSet = taPTHead_Ext
    Left = 208
    Top = 452
  end
  object frPT_Ext: TfrDBDataSet
    DataSet = taPT_Ext
    Left = 280
    Top = 452
  end
  object frAW_SelfMat_ext: TfrDBDataSet
    DataSet = taAW_SelfMat_Ext
    Left = 100
    Top = 452
  end
  object frAW_Ext: TfrDBDataSet
    Left = 16
    Top = 452
  end
  object taAW_Ext: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select i.DOCNO, i.DOCDATE, c1.NAME SELFNAME, c1.BOSSFIO SELFBOSS' +
        'FIO,'
      '       c2.NAME SUPNAME, co.No_Contract, co.Bd_Contract,'
      '       (select FSUMM from INV_SUM(:INVID)) COST '
      'from Inv i left join D_Comp c2 on (i.SupId=c2.CompId)'
      '           left join D_Comp c1 on (i.SelfCompId=c1.CompId)'
      
        '           left join CompOwner co on (co.SELFCOMPID=:SELFCOMPID ' +
        'and co.COMPID=c2.COMPID)'
      'where i.INVID=:INVID')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 16
    Top = 408
    poSQLINT64ToBCD = True
  end
  object taAWG_Head: TpFIBDataSet
    UpdateSQL.Strings = (
      'execute procedure Stub;')
    SelectSQL.Strings = (
      'select cast('#39'NOW'#39' as timestamp) BD, '
      '      cast('#39'NOW'#39' as timestamp) ED, '
      '      cast('#39#39' as char(60)) SELFCOMPNAME, '
      '      cast('#39#39' as char(60)) SUPNAME'
      'from rdb$database')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 88
    Top = 100
    poSQLINT64ToBCD = True
    object taAWG_HeadBD: TFIBDateTimeField
      FieldName = 'BD'
    end
    object taAWG_HeadED: TFIBDateTimeField
      FieldName = 'ED'
    end
    object taAWG_HeadSELFCOMPNAME: TFIBStringField
      FieldName = 'SELFCOMPNAME'
      Size = 60
      EmptyStrToNull = True
    end
    object taAWG_HeadSUPNAME: TFIBStringField
      FieldName = 'SUPNAME'
      Size = 60
      EmptyStrToNull = True
    end
  end
  object frAWG_Head: TfrDBDataSet
    DataSet = taAWG_Head
    Left = 88
    Top = 144
  end
  object taMB_Move: TpFIBDataSet
    SelectSQL.Strings = (
      'select it.ID, it.INVID, it.MATID, it.W, it.DESCR,'
      '       m.NAME MATNAME, it.ZNAK'
      'from MatBalance it, D_Mat m'
      'where it.INVID=:INVID and'
      '      it.MATID=m.ID and'
      '      it.T=1')
    Transaction = dm.tr
    Database = dm.db
    Description = #1044#1086#1082#1091#1084#1077#1085#1090' '#1041#1072#1083#1072#1085#1089' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074', '
    Left = 348
    Top = 408
  end
  object frMB_Move: TfrDBDataSet
    DataSet = taMB_Move
    Left = 348
    Top = 456
  end
  object frXMLExcelExport1: TfrXMLExcelExport
    Left = 352
    Top = 224
  end
  object frActWorkCommon: TfrDBDataSet
    DataSet = dmData.taActWorkCommon
    Left = 600
    Top = 12
  end
  object frActWorkListCommon: TfrDBDataSet
    DataSet = dmData.taActWorkListCommon
    Left = 608
    Top = 60
  end
  object taMX18Common: TpFIBDataSet
    SelectSQL.Strings = (
      'select i.DOCNO, i.DOCDATE, c.OKPO, c.NAME COMPNAME,'
      '       c.PHONE, c.ADDRESS, s1.NAME SUPNAME, c.BOSSFIO'
      'from Inv i, D_Comp c, D_Comp s1'
      'where i.INVID=:INVID and'
      '      i.SELFCOMPID = c.COMPID and'
      '      i.SUPID=s1.COMPID'
      ' '
      '        ')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 640
    Top = 432
    poSQLINT64ToBCD = True
  end
  object taMX18ItemCommon: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '   count(*) Q,'
      '   sum(it.W) W,'
      '   sum(s.SCost) SCOST, '
      '   a2.ART,'
      '   a2.ART2,'
      '   p.prill'
      ''
      'from ACTWORKLIST_S(:BD, :ED) a,'
      '     ServItem s,'
      '     SItem it,'
      '     Art2 a2,'
      '     d_semis se,'
      '     d_prill p'
      'where'
      '   a.compid = :Company$ID'
      '   and s.INVID= a.invid'
      '   and s.SITEMID=it.SITEMID'
      '   and it.Art2Id=a2.Art2Id'
      '   and se.semisid = a2.semis'
      '   and p.id = se.prill'
      'group by a2.ART, a2.ART2, p.prill'
      '      ')
    BeforeOpen = taMX18ItemCommonBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 640
    Top = 480
    poSQLINT64ToBCD = True
  end
  object frMX18Common: TfrDBDataSet
    DataSet = taMX18Common
    Left = 728
    Top = 436
  end
  object frMX18ItemCommon: TfrDBDataSet
    DataSet = taMX18ItemCommon
    Left = 728
    Top = 484
  end
  object taVerActMaster: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct'
      '  i.supid,'
      '  dc.name'
      'from d_comp dc'
      '  left outer join inv i on (dc.compid = i.supid)'
      '  left outer join awitem a on (i.invid = a.invid)'
      'where '
      '  a.Invid = :invid'
      ' ')
    BeforeOpen = taVerActMasterBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 456
    Top = 376
  end
  object taVerActDetail: TpFIBDataSet
    SelectSQL.Strings = (
      'select case it.InvSubTypeId '
      '         when 20 then -1000'
      '         when 21 then 1000'
      '         else it.InvSubTypeId'
      '       end, it.ID, it.INVID, it.MATID, it.INVSUBTYPEID,'
      '    it.Q, it.W, it.N, st.Name INVSUBNAME, m.Name MATNAME    '
      'from AWItem it '
      '  left outer join D_InvSubType st on (it.InvSubTypeId=st.Id)'
      '  left outer join D_Mat m on (it.MatId = m.Id)'
      '  left outer join inv i on (it.invid = i.invid)'
      'where '
      '  it.INVID=:INVID and'
      '  i.supid = ?supid    '
      '      '
      'order by m.SORTIND, 1')
    BeforeOpen = taVerActDetailBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DataSource = dsrVerActMaster
    Left = 536
    Top = 376
    dcForceOpen = True
  end
  object frVerActMaster: TfrDBDataSet
    DataSource = dsrVerActMaster
    Left = 456
    Top = 464
  end
  object frVerActDetail: TfrDBDataSet
    DataSource = dsrVerActDetail
    Left = 544
    Top = 464
  end
  object dsrVerActMaster: TDataSource
    DataSet = taVerActMaster
    Left = 456
    Top = 424
  end
  object dsrVerActDetail: TDataSource
    DataSet = taVerActDetail
    Left = 544
    Top = 424
  end
  object taInvListPrint: TpFIBDataSet
    Transaction = dm.tr
    Database = dm.db
    Left = 608
    Top = 120
  end
  object frInvListPrint: TfrDBDataSet
    DataSet = taInvListPrint
    Left = 688
    Top = 120
  end
end
