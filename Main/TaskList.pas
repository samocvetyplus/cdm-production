unit TaskList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, TB2Item, ActnList, Menus, 
  ImgList, Grids, DBGridEh,  StdCtrls, ExtCtrls, ComCtrls,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmTaskList = class(TfmListAncestor)
    procedure acAddDocExecute(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure spitEditClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmTaskList: TfmTaskList;

implementation

uses Data, DB, eTask, DBUtil, DBEditor;

{$R *.dfm}

procedure TfmTaskList.acAddDocExecute(Sender: TObject);
begin
  DataSet.Append;
  DataSet.Post;
  DataSet.Transaction.CommitRetaining;
end;

procedure TfmTaskList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if (Column.FieldName='TYPE') and (not dmData.taTaskListCOLOR.IsNull) then
    BackGround:=dmData.taTaskListCOLOR.AsInteger;
end;

procedure TfmTaskList.spitEditClick(Sender: TObject);
begin
  inherited;
  with dmData do
  begin
    ShowDBEditor(TfmeTask, dsrTaskList);
    taTaskList.Transaction.CommitRetaining;
    RefreshDataSet(taTaskList);
    FreeAndNil(fmeTask);
  end;
end;

end.
