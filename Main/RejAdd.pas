unit RejAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls, ComCtrls,
  Grids, DBGridEh, PrnDbgeh, ActnList, DBGridEhGrouping, GridsEh;

type
  TfmRejAdd = class(TfmAncestor)
    gridRejAdd: TDBGridEh;
    SpeedItem1: TSpeedItem;
    acEvent: TActionList;
    acPrint: TAction;
    PrintGrid: TPrintDBGridEh;
    procedure acPrintExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmRejAdd: TfmRejAdd;

implementation

uses ApplData, dbUtil;

{$R *.dfm}

procedure TfmRejAdd.acPrintExecute(Sender: TObject);
begin
  PrintGrid.Preview;
end;

procedure TfmRejAdd.FormCreate(Sender: TObject);
var
  Bd, Ed : TDateTime; 
begin
  if dmAppl.trAppl.Active then dmAppl.trAppl.CommitRetaining
  else dmAppl.trAppl.StartTransaction;
  OpenDataSet(dmAppl.taRejAdd);
  stbrStatus.SimplePanel := True;
  dmAppl.trAppl.CommitRetaining;
  dmAppl.RejAddPeriod(Bd, Ed);
  stbrStatus.SimpleText := '������ � '+DateToStr(Bd)+' �� '+DateToStr(Ed);
end;

procedure TfmRejAdd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmAppl.taRejAdd);
  dmAppl.trAppl.CommitRetaining;
end;

end.
