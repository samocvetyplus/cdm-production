{**************************************}
{  ������ ����������� � ����������� �  }
{  ��������������� ������              }
{  Copyrigth (C) 2004 basile for CDM   }
{  v1.02                               }
{  create 14.04.2004                   }  
{**************************************}
unit NeedStone;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList, ImgList, ExtCtrls,
  ComCtrls, VirtualTrees, TB2Dock, TB2Toolbar, TB2Item, StdCtrls, Mask,
  DBCtrlsEh, Menus, DB, FIBDataSet, pFIBDataSet, rxPlacemnt, rxSpeedbar;

type
  TfmNeedStone = class(TfmAncestor)
    vstSource: TVirtualStringTree;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    acAddAppl: TAction;
    TBControlItem3: TTBControlItem;
    Label1: TLabel;
    TBSeparatorItem1: TTBSeparatorItem;
    cmbxAlg: TDBComboBoxEh;
    TBControlItem4: TTBControlItem;
    ppSource: TTBPopupMenu;
    TBItem1: TTBItem;
    Splitter1: TSplitter;
    acCalcNeed: TAction;
    SpeedItem1: TSpeedItem;
    taAnlz_Diamond: TpFIBDataSet;
    taAnlz_Fianit: TpFIBDataSet;
    vstDiamond: TVirtualStringTree;
    taAnlz_DiamondItem: TpFIBDataSet;
    acDelAppl: TAction;
    TBItem2: TTBItem;
    taAnlz_DiamondQ: TFIBBCDField;
    taAnlz_DiamondART: TFIBStringField;
    taAnlz_DiamondQA: TFIBBCDField;
    taAnlz_DiamondOPERATION: TFIBStringField;
    taAnlz_DiamondQR: TFIBBCDField;
    taAnlz_DiamondItemART: TFIBStringField;
    taAnlz_DiamondItemQA: TFIBIntegerField;
    taAnlz_DiamondItemOPERATION: TFIBStringField;
    taAnlz_DiamondItemQR: TFIBIntegerField;
    taAnlz_DiamondSZDIAMOND: TFIBStringField;
    taAnlz_DiamondItemSZDIAMOND: TFIBStringField;
    vstFianit: TVirtualStringTree;
    taAnlz_FianitItem: TpFIBDataSet;
    taAnlz_FianitINSNAME: TFIBStringField;
    taAnlz_FianitART: TFIBStringField;
    taAnlz_FianitOPERATION: TFIBStringField;
    taAnlz_FianitQ: TFIBBCDField;
    taAnlz_FianitQA: TFIBBCDField;
    taAnlz_FianitINSID: TFIBStringField;
    taAnlz_FianitItemINSID: TFIBStringField;
    taAnlz_FianitItemINSNAME: TFIBStringField;
    taAnlz_FianitItemART: TFIBStringField;
    taAnlz_FianitItemOPERATION: TFIBStringField;
    taAnlz_FianitItemQ: TFIBIntegerField;
    taAnlz_FianitItemQA: TFIBIntegerField;
    taAnlz_DiamondItemQ: TFIBBCDField;
    spitPrint: TSpeedItem;
    acPrint: TAction;
    PrintDialog: TPrintDialog;
    procedure vstSourceInitNode(Sender: TBaseVirtualTree; ParentNode,
      Node: PVirtualNode; var InitialStates: TVirtualNodeInitStates);
    procedure FormCreate(Sender: TObject);
    procedure vstSourceGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure vstSourceInitChildren(Sender: TBaseVirtualTree;
      Node: PVirtualNode; var ChildCount: Cardinal);
    procedure acAddApplExecute(Sender: TObject);
    procedure acCalcNeedExecute(Sender: TObject);
    procedure taAnlz_DiamondBeforeOpen(DataSet: TDataSet);
    procedure taAnlz_DiamondItemBeforeOpen(DataSet: TDataSet);
    procedure vstDiamondGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure vstFianitGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure taAnlz_FianitBeforeOpen(DataSet: TDataSet);
    procedure taAnlz_FianitItemBeforeOpen(DataSet: TDataSet);
    procedure vstFianitPaintText(Sender: TBaseVirtualTree;
      const TargetCanvas: TCanvas; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType);
    procedure vstDiamondPaintText(Sender: TBaseVirtualTree;
      const TargetCanvas: TCanvas; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType);
    procedure vstDiamondCompareNodes(Sender: TBaseVirtualTree; Node1,
      Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
    procedure vstFianitCompareNodes(Sender: TBaseVirtualTree; Node1,
      Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
    procedure acPrintExecute(Sender: TObject);
    procedure vstDiamondHeaderClick(Sender: TVTHeader;
      HitInfo: TVTHeaderHitInfo);
    procedure vstFianitHeaderClick(Sender: TVTHeader;
      HitInfo: TVTHeaderHitInfo);
  public
    procedure InitDiamondResult;
    procedure InitFianitResult;
    procedure vstDiamondHeaderClick2(Sender: TVTHeader;  Column: TColumnIndex; Button: TMouseButton; Shift: TShiftState; X,   Y: Integer);
    procedure vstFianitHeaderClick2(Sender: TVTHeader; Column: TColumnIndex; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);    
  end;

var
  fmNeedStone: TfmNeedStone;

implementation

uses DictData, dbUtil, eSelAppl, Editor, Printers;

{$R *.dfm}
type
  PEntry = ^TEntry;
  TEntry = record
    Caption: WideString;
    Id : integer;
  end;

  POperation = ^TOperation;
  TOperation = record
    Id : string;
    Operation : string; 
  end;

  PAppl = ^TAppl;
  TAppl = record
    Id : integer;
    ApplNo : string;
  end;

  PDiamond = ^TDiamond;
  TDiamond = record
    SzDiamond : string;
    Q : integer;
    QR : integer;
    Art : string;
    QA : integer;
    Operation : string;
  end;

  PFianit = ^TFianit;
  TFianit = record
    Name : string;
    Q : integer;
    Art : string;
    QA : integer;
    Operation : string;
  end;


var
  TreeEntries: array[0..1] of TEntry = (
    (Caption: '�����������'; Id:1 ),
    (Caption: '������'; Id:2 )
  );

  OperationEntries : array of TOperation;
  ApplEntries: array of TAppl;

procedure TfmNeedStone.FormCreate(Sender: TObject);
var
  i : integer;
  StoneTitle: string;
  StoneKey: Integer;
begin
  if not dm.tr.Active then dm.tr.StartTransaction;
  dm.tr.CommitRetaining;
  // ���������� �������� ������������
  OpenDataSets([dm.taOper, dm.taInsBasic]);
  dm.taOper.Last;
  SetLength(OperationEntries, dm.taOper.RecordCount);
  dm.taOper.First;
  i:=0;
  while not dm.taOper.Eof do
  begin
    OperationEntries[i].Id := dm.taOperOPERID.AsString;
    OperationEntries[i].Operation := dm.taOperOPERATION.AsString;
    inc(i);
    dm.taOper.Next;
  end;
  dm.taInsBasic.First;
  while not dm.taInsBasic.Eof do
  begin
    StoneKey := dm.taInsBasicISBASIC.AsInteger;
    if StoneKey <> 32767 then
    begin
      StoneTitle := dm.taInsBasicTITLE.AsString;
      cmbxAlg.KeyItems.Add(IntToStr(StoneKey));
      cmbxAlg.Items.Add(StoneTitle);
    end;
    dm.taInsBasic.Next;
  end;
  CloseDataSets([dm.taOper, dm.taInsBasic]);
  // ���������� ������
  SetLength(ApplEntries, 1);
  ApplEntries[0].Id := -1;
  ApplEntries[0].ApplNo := '��������, �� �� ������������ ������';

  //ppSource.Skin := dm.ppSkin;

  vstSource.NodeDataSize := SizeOf(TOperation);
  vstSource.RootNodeCount := 2;
  vstDiamond.NodeDataSize := SizeOf(TDiamond);
  vstFianit.NodeDataSize := SizeOf(TFianit);
  vstDiamond.Align := alClient;
  vstFianit.Align := alClient;
end;

procedure TfmNeedStone.vstSourceGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  Data: PEntry;
  Operation : POperation;
  Appl : PAppl;
begin
  if Node.Parent = Sender.RootNode then
  begin
    Data := Sender.GetNodeData(Node);
    CellText := Data.Caption
  end
  else
    case Sender.RootNode.Index of
      0 : begin
        Operation := Sender.GetNodeData(Node);
        CellText := Operation.Operation;
      end;
      1 : begin
        Appl := Sender.GetNodeData(Node);
        CellText := Appl.ApplNo;
      end;
    end
end;

procedure TfmNeedStone.vstSourceInitChildren(Sender: TBaseVirtualTree;
  Node: PVirtualNode; var ChildCount: Cardinal);
begin
  case Node.Index of
    0 : ChildCount:= High(OperationEntries);
    1 : ChildCount:= High(ApplEntries)+1;
  end;
end;

procedure TfmNeedStone.vstSourceInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode;
  var InitialStates: TVirtualNodeInitStates);
var
  Data: PEntry;
  Operation : POperation;
  Appl : PAppl;
begin
  if ParentNode = nil then
  begin
    Include(InitialStates, ivsHasChildren);
    Data := Sender.GetNodeData(Node);
    Data^ := TreeEntries[Node.Index mod 2];
    Node.CheckType := ctTriStateCheckBox;
  end
  else begin
    case ParentNode.Index of
      0 : begin
        Operation := Sender.GetNodeData(Node);
        Operation^ := OperationEntries[Node.Index mod (High(OperationEntries)+1)];
      end;
      1 : begin
        Appl := Sender.GetNodeData(Node);
        Appl^ := ApplEntries[Node.Index mod (High(ApplEntries)+1)];
        if(Appl.Id > 0) then Node.CheckState := csCheckedNormal;
      end;
    end;
    Node.CheckType := TCheckType(Sender.GetNodeLevel(Node));
    //if(Node.Parent.CheckState = csCheckedNormal)then
    Node.CheckState := Node.Parent.CheckState;
  end;
end;

procedure TfmNeedStone.acAddApplExecute(Sender: TObject);
var
  sl, slDescr : TStringList;
  i, offset : integer;
  ApplRoot, Appl : PVirtualNode;
  Data : PAppl;
begin
  if ShowEditor(TfmeSelAppl, TfmEditor(fmeSelAppl))=mrOk then
  begin
    sl := TStringList.Create;
    slDescr := TStringList.Create;
    try
      ApplRoot := vstSource.RootNode.FirstChild;
      ApplRoot := ApplRoot.NextSibling;
      sl.Delimiter := ';';
      sl.DelimitedText := eSelAppl.ApplIds;
      slDescr.Delimiter := ';';
      slDescr.DelimitedText := eSelAppl.ApplDescr;
      offset :=High(ApplEntries)+1;
      SetLength(ApplEntries, offset+sl.Count);
      vstSource.BeginUpdate;
      for i:=0 to Pred(sl.Count)do
      begin
        ApplEntries[i+offset].Id := StrToIntDef(sl[i], 0);
        ApplEntries[i+offset].ApplNo := slDescr[i];
        vstSource.ChildCount[ApplRoot] := vstSource.ChildCount[ApplRoot]+1;
        vstSource.Expanded[ApplRoot] := True;
        vstSource.InvalidateToBottom(ApplRoot);
      end;
      vstSource.EndUpdate;
    finally
      sl.Free;
    end;
  end;
end;

procedure TfmNeedStone.acCalcNeedExecute(Sender: TObject);
var
  Root, Node : PVirtualNode;
  DataAppl : PAppl;
  DataOper : POperation;
  ApplAll, Appl1, OperAll : string;
  ApplIds, OperIds : string;
  fExpand : boolean;
  StoneKey: Integer;
begin
  if(cmbxAlg.ItemIndex = -1)then
  begin
    ActiveControl := cmbxAlg;
    raise Exception.Create('�������� �������� �������');
  end;
  OperAll := '0'; ApplAll := '0'; Appl1 := '0';
  ApplIds := ''; OperIds := '';
  // ������ ������
  // �������� � ������������
  Root  := vstSource.RootNode.FirstChild;
  fExpand :=  vstSource.Expanded[Root];
  vstSource.BeginUpdate;
  try
    vstSource.Expanded[Root] := True;
    if(Root.CheckState = csCheckedNormal)then OperAll := '1'
    else begin
      Node := Root.FirstChild;
      while Assigned(Node) do
      begin
        if(Node.CheckState = csCheckedNormal)then
        begin
          DataOper := vstSource.GetNodeData(Node);
          OperIds := OperIds+DataOper.Id+';';
        end;
        Node := Node.NextSibling;
      end;
    end;
  finally
    vstSource.Expanded[Root] := fExpand;
    vstSource.EndUpdate;
  end;
  // ������
  Root := vstSource.RootNode.FirstChild.NextSibling;
  fExpand :=  vstSource.Expanded[Root];
  vstSource.BeginUpdate;
  try
    if(Root.CheckState = csCheckedNormal)then Appl1 := '1'
    else begin
      vstSource.Expanded[Root] := True;
      if(Root.FirstChild.CheckState = csCheckedNormal)then Appl1 := '1' else Appl1 := '0';
      Node := Root.FirstChild.NextSibling;
      while Assigned(Node) do
      begin
        if(Node.CheckState = csCheckedNormal)then
        begin
          DataAppl := vstSource.GetNodeData(Node);
          ApplIds := ApplIds+IntToStr(DataAppl.Id)+';';
        end;
        Node := Node.NextSibling;
      end;
    end;
  finally
    vstSource.Expanded[Root] := fExpand;
    vstSource.EndUpdate;
  end;
  Application.ProcessMessages;
  ExecSQL('execute procedure ANLZ__STONE_INFO('+IntToStr(dm.User.UserId)+','+OperAll+','#39+OperIds+#39','+
    Appl1+','#39+ApplIds+#39')', dm.quTmp);
  Application.ProcessMessages;
  vstDiamond.Visible := False;
  vstFianit.Visible := False;
  Application.ProcessMessages;
  StoneKey := StrToInt(cmbxAlg.KeyItems[cmbxAlg.ItemIndex]);
  if StoneKey = 0 then
  begin
     ExecSQL('execute procedure ANLZ__FIANIT('+IntToStr(dm.User.UserId)+')', dm.quTmp);
     Application.ProcessMessages;
     InitFianitResult;
  end
  else
  begin
    ExecSQL('execute procedure ANLZ__STONE('+IntToStr(dm.User.UserId)+', ' + IntToStr(StoneKey) + ')', dm.quTmp);
    Application.ProcessMessages;
    InitDiamondResult;
  end;
end;

procedure TfmNeedStone.taAnlz_DiamondBeforeOpen(DataSet: TDataSet);
begin
  taAnlz_Diamond.ParamByName('USERID').AsInteger := dm.User.UserId;
end;

procedure TfmNeedStone.taAnlz_DiamondItemBeforeOpen(DataSet: TDataSet);
begin
  taAnlz_DiamondItem.ParamByName('USERID').AsInteger := dm.User.UserId;
  taAnlz_DiamondItem.ParamByName('SZDIAMOND').AsString := taAnlz_DiamondSZDIAMOND.AsString;
end;

procedure TfmNeedStone.vstDiamondGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  Data: PDiamond;
begin
  Data := Sender.GetNodeData(Node);
  case Column of
    0 : CellText := Data.SzDiamond;
    1 : CellText := IntToStr(Data.Q);
    2 : CellText := IntToStr(Data.QR);
    3 : CellText := Data.Art;
    4 : CellText := IntToStr(Data.QA);
    5 : CellText := Data.Operation;
  end;
end;

procedure TfmNeedStone.InitDiamondResult;
var
  i : integer;
  Data : PDiamond;
  Node, ChildNode : PVirtualNode;
  Diamond: TDiamond;
begin
  vstDiamond.Visible := True;
  // �������� ������
  if dm.tr.Active then dm.tr.CommitRetaining
  else dm.tr.StartTransaction;
  taAnlz_Diamond.Open;
  vstDiamond.BeginUpdate;
  vstDiamond.Clear;
  i := 0;
  try
    while not taAnlz_Diamond.Eof do
    begin
      Diamond.SzDiamond := taAnlz_DiamondSZDIAMOND.AsString;
      Diamond.Q := taAnlz_DiamondQ.AsInteger;
      Diamond.QR := taAnlz_DiamondQR.AsInteger;
      Diamond.Art := taAnlz_DiamondART.AsString;
      Diamond.QA := taAnlz_DiamondQA.AsInteger;
      Diamond.Operation := taAnlz_DiamondOPERATION.AsString;
      Node := vstDiamond.AddChild(vstDiamond.RootNode);
      Data := vstDiamond.GetNodeData(Node);
      Data^ := Diamond;
      // �����������
      OpenDataSet(taAnlz_DiamondItem);
      try
        while not taAnlz_DiamondItem.Eof do
        begin
          Diamond.SzDiamond := '';//taAnlz_DiamondItemSZDIAMOND.AsString;
          Diamond.Q := taAnlz_DiamondItemQ.AsInteger;
          Diamond.QR := taAnlz_DiamondItemQR.AsInteger;
          Diamond.QA := taAnlz_DiamondItemQA.AsInteger;
          Diamond.Art := taAnlz_DiamondItemART.AsString;
          Diamond.Operation := taAnlz_DiamondItemOPERATION.AsString;
          ChildNode := vstDiamond.AddChild(Node);
          Data := vstDiamond.GetNodeData(ChildNode);
          Data^ := Diamond;
          taAnlz_DiamondItem.Next;
        end;
      finally
        CloseDataSet(taAnlz_DiamondItem);
      end;
      taAnlz_Diamond.Next;
      inc(i);
    end;
    taAnlz_Diamond.Close;
  finally
    vstDiamond.EndUpdate;
  end;
end;

procedure TfmNeedStone.vstFianitGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  Data: PFianit;
begin
  Data := Sender.GetNodeData(Node);
  case Column of
    0 : CellText := Data.Name;
    1 : CellText := IntToStr(Data.Q);
    2 : CellText := Data.Art;
    3 : CellText := IntToStr(Data.QA);
    4 : CellText := Data.Operation;
  end;
end;

procedure TfmNeedStone.InitFianitResult;
var
  i : integer;
  Data : PFianit;
  Node, ChildNode : PVirtualNode;
  Fianit: TFianit;
begin
  vstFianit.Visible := True;
  // �������� ������
  if dm.tr.Active then dm.tr.CommitRetaining
  else dm.tr.StartTransaction;
  taAnlz_Fianit.Open;
  vstFianit.BeginUpdate;
  vstFianit.Clear;
  i := 0;
  try
    while not taAnlz_Fianit.Eof do
    begin
      Fianit.Name := taAnlz_FianitINSNAME.AsString;
      Fianit.Q := taAnlz_FianitQ.AsInteger;
      Fianit.Art := taAnlz_FianitART.AsString;
      Fianit.QA := taAnlz_FianitQA.AsInteger;
      Fianit.Operation := taAnlz_FianitOPERATION.AsString;
      Node := vstFianit.AddChild(vstFianit.RootNode);
      Data := vstFianit.GetNodeData(Node);
      Data^ := Fianit;
      // �����������
      OpenDataSet(taAnlz_FianitItem);
      try
        while not taAnlz_FianitItem.Eof do
        begin
          Fianit.Name := '';
          Fianit.Q := taAnlz_FianitItemQ.AsInteger;
          Fianit.QA := taAnlz_FianitItemQA.AsInteger;
          Fianit.Art := taAnlz_FianitItemART.AsString;
          Fianit.Operation := taAnlz_FianitItemOPERATION.AsString;
          ChildNode := vstFianit.AddChild(Node);
          Data := vstFianit.GetNodeData(ChildNode);
          Data^ := Fianit;
          taAnlz_FianitItem.Next;
        end;
      finally
        CloseDataSet(taAnlz_FianitItem);
      end;
      taAnlz_Fianit.Next;
      inc(i);
    end;
    taAnlz_Fianit.Close;
  finally
    vstFianit.EndUpdate;
  end;
end;

procedure TfmNeedStone.taAnlz_FianitBeforeOpen(DataSet: TDataSet);
begin
  taAnlz_Fianit.ParamByName('USERID').AsInteger := dm.User.UserId;
end;

procedure TfmNeedStone.taAnlz_FianitItemBeforeOpen(DataSet: TDataSet);
begin
  taAnlz_FianitItem.ParamByName('USERID').AsInteger := dm.User.UserId;
  taAnlz_FianitItem.ParamByName('INSID').AsString := taAnlz_FianitINSID.AsString;
end;

procedure TfmNeedStone.vstFianitPaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType);
begin
  if(Node.Parent <> Sender.RootNode) then
    TargetCanvas.Font.Color := clNavy;
end;

procedure TfmNeedStone.vstDiamondPaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType);
begin
  if(Node.Parent <> Sender.RootNode) then
    TargetCanvas.Font.Color := clNavy;
end;


procedure TfmNeedStone.vstDiamondCompareNodes(Sender: TBaseVirtualTree;
  Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
var
  Data1, Data2: PDiamond;
begin
  Data1 := Sender.GetNodeData(Node1);
  Data2 := Sender.GetNodeData(Node2);
  if(Node1.Parent = Sender.RootNode)or(Node2.Parent = Sender.RootNode)or
    (Node1.Parent <> Node2.Parent) then
  begin
    Result := -2;
    eXit;
  end;
  case Column of
    3 : Result := CompareText(Data1.Art, Data2.Art);
    5 : Result := CompareText(Data1.Operation, Data2.Operation);
  end;
end;

procedure TfmNeedStone.vstFianitCompareNodes(Sender: TBaseVirtualTree;
  Node1, Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
var
  Data1, Data2: PFianit;
begin
  Data1 := Sender.GetNodeData(Node1);
  Data2 := Sender.GetNodeData(Node2);
  if(Node1.Parent = Sender.RootNode)or(Node2.Parent = Sender.RootNode)or
    (Node1.Parent <> Node2.Parent) then
  begin
    Result := -2;
    eXit;
  end;
  case Column of
    2 : Result := CompareText(Data1.Art, Data2.Art);
    4 : Result := CompareText(Data1.Operation, Data2.Operation);
  end;
end;


procedure TfmNeedStone.acPrintExecute(Sender: TObject);
begin
  if PrintDialog.Execute then
    case cmbxAlg.ItemIndex of
      0: begin
         vstFianit.Print(Printer, True);
      end;
      1: begin
        vstDiamond.Print(Printer, True);
      end;
    end;
end;

procedure TfmNeedStone.vstDiamondHeaderClick2(Sender: TVTHeader;
  Column: TColumnIndex; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  if Button = mbLeft then
  begin
    with Sender do
    begin
      if not(Column in [3,5]) then
        SortColumn := NoColumn
      else
      begin
        if SortColumn = NoColumn then
        begin
          SortColumn := Column;
          SortDirection := sdAscending;
        end
        else begin
          SortColumn := Column;
          if SortDirection = sdAscending then
            SortDirection := sdDescending
          else
            SortDirection := sdAscending;
        end;
        Treeview.SortTree(SortColumn, SortDirection, False);
      end;
    end;
  end;
end;

procedure TfmNeedStone.vstFianitHeaderClick2(Sender: TVTHeader;
  Column: TColumnIndex; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  if Button = mbLeft then
  begin
    with Sender do
    begin
      if not(Column in [2,4]) then
        SortColumn := NoColumn
      else
      begin
        if SortColumn = NoColumn then
        begin
          SortColumn := Column;
          SortDirection := sdAscending;
        end
        else begin
          SortColumn := Column;
          if SortDirection = sdAscending then
            SortDirection := sdDescending
          else
            SortDirection := sdAscending;
        end;
        Treeview.SortTree(SortColumn, SortDirection, False);
      end;
    end;
  end;  
end;



procedure TfmNeedStone.vstDiamondHeaderClick(Sender: TVTHeader;
  HitInfo: TVTHeaderHitInfo);
begin
  vstDiamondHeaderClick2(Sender, HitInfo.Column, HitInfo.Button, HitInfo.Shift, HitInfo.X, HitInfo.Y);
end;

procedure TfmNeedStone.vstFianitHeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
begin
  vstFianitHeaderClick2(Sender, HitInfo.Column, HitInfo.Button, HitInfo.Shift, HitInfo.X, HitInfo.Y);
end;

end.
