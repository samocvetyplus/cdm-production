object fmPaymentsAndBalance: TfmPaymentsAndBalance
  Left = 0
  Top = 0
  Align = alClient
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1042#1079#1072#1080#1084#1086#1088#1072#1089#1095#1105#1090#1099
  ClientHeight = 581
  ClientWidth = 1089
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object GridBalance: TcxGrid
    Left = 201
    Top = 0
    Width = 888
    Height = 561
    Align = alClient
    TabOrder = 0
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.SkinName = ''
    ExplicitLeft = 153
    ExplicitWidth = 936
    object GridBalanceView: TcxGridDBTableView
      NavigatorButtons.ConfirmDelete = False
      DataController.DataSource = dsPayments
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Kind = skSum
          Position = spFooter
          Column = GridBalanceViewOutcome
        end
        item
          Kind = skSum
          Position = spFooter
          Column = GridBalanceViewPayments
        end
        item
          Kind = skSum
          Position = spFooter
          Column = GridBalanceViewRefunds
        end
        item
          Kind = skSum
          Position = spFooter
          Column = GridBalanceViewSells
        end
        item
          Kind = skSum
          Position = spFooter
          Column = GridBalanceViewIncome
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skSum
          Column = GridBalanceViewIncome
        end
        item
          Kind = skSum
          Column = GridBalanceViewSells
        end
        item
          Kind = skSum
          Column = GridBalanceViewRefunds
        end
        item
          Kind = skSum
          Column = GridBalanceViewPayments
        end
        item
          Kind = skSum
          Column = GridBalanceViewOutcome
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1090' '#1076#1072#1085#1085#1099#1093' '#1076#1083#1103' '#1086#1090#1086#1073#1088#1072#1078#1077#1085#1080#1103'>'
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfVisibleWhenExpanded
      object GridBalanceViewNumber: TcxGridDBColumn
        Caption = '#'
        DataBinding.FieldName = 'N'
        Width = 47
      end
      object GridBalanceViewContragent: TcxGridDBColumn
        Caption = #1050#1086#1085#1090#1088#1072#1075#1077#1085#1090
        DataBinding.FieldName = 'Name'
        Width = 257
      end
      object GridBalanceViewIncome: TcxGridDBColumn
        Caption = #1042#1093#1086#1076#1103#1097#1080#1077
        DataBinding.FieldName = 'balance$start'
        Width = 116
      end
      object GridBalanceViewSells: TcxGridDBColumn
        Caption = #1055#1088#1086#1076#1072#1078#1080
        DataBinding.FieldName = 'cost$sell'
        Width = 117
      end
      object GridBalanceViewRefunds: TcxGridDBColumn
        Caption = #1042#1086#1079#1074#1088#1072#1090#1099
        DataBinding.FieldName = 'cost$return'
        Width = 111
      end
      object GridBalanceViewPayments: TcxGridDBColumn
        Caption = #1055#1083#1072#1090#1077#1078#1080
        DataBinding.FieldName = 'cost$payment'
        Width = 115
      end
      object GridBalanceViewOutcome: TcxGridDBColumn
        Caption = #1048#1089#1093#1086#1076#1103#1097#1080#1077
        DataBinding.FieldName = 'balance$end'
        Width = 111
      end
    end
    object GridBalanceLevel1: TcxGridLevel
      GridView = GridBalanceView
    end
  end
  object StatusBar: TdxStatusBar
    Left = 0
    Top = 561
    Width = 1089
    Height = 20
    Panels = <>
    PaintStyle = stpsOffice11
    LookAndFeel.Kind = lfOffice11
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Splitter: TcxSplitter
    Left = 193
    Top = 0
    Width = 8
    Height = 561
    HotZoneClassName = 'TcxMediaPlayer9Style'
    AutoSnap = True
    MinSize = 190
    ResizeUpdate = True
    Control = GroupBox
    ExplicitLeft = 200
  end
  object GroupBox: TcxGroupBox
    Left = 0
    Top = 0
    Align = alLeft
    Caption = ' '#1055#1072#1088#1072#1084#1077#1090#1088#1099' '
    Style.Edges = [bLeft, bTop, bBottom]
    Style.LookAndFeel.Kind = lfOffice11
    Style.LookAndFeel.NativeStyle = False
    StyleDisabled.LookAndFeel.Kind = lfOffice11
    StyleDisabled.LookAndFeel.NativeStyle = False
    StyleFocused.LookAndFeel.Kind = lfOffice11
    StyleFocused.LookAndFeel.NativeStyle = False
    StyleHot.LookAndFeel.Kind = lfOffice11
    StyleHot.LookAndFeel.NativeStyle = False
    TabOrder = 3
    Height = 561
    Width = 193
    object LabelPeriodStart: TLabel
      Left = 36
      Top = 193
      Width = 101
      Height = 16
      Alignment = taCenter
      Caption = #1053#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LabelPeriodEnd: TLabel
      Left = 36
      Top = 266
      Width = 93
      Height = 16
      Alignment = taCenter
      Caption = #1050#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object PeriodBeginDateEdit: TcxDateEdit
      Left = 36
      Top = 215
      Hint = #1053#1072#1095#1072#1083#1086
      EditValue = 0d
      ParentShowHint = False
      Properties.Alignment.Horz = taCenter
      Properties.SaveTime = False
      Properties.ShowTime = False
      ShowHint = True
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.SkinName = ''
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.SkinName = ''
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.SkinName = ''
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.SkinName = ''
      TabOrder = 0
      Width = 101
    end
    object PeriodEndDateEdit: TcxDateEdit
      Left = 36
      Top = 288
      Hint = #1050#1086#1085#1077#1094
      EditValue = 0d
      Properties.Alignment.Horz = taCenter
      Properties.SaveTime = False
      Properties.ShowTime = False
      Style.BorderStyle = ebsOffice11
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.SkinName = ''
      StyleDisabled.BorderStyle = ebsOffice11
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.SkinName = ''
      StyleFocused.BorderStyle = ebsOffice11
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.SkinName = ''
      StyleHot.BorderStyle = ebsOffice11
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.SkinName = ''
      TabOrder = 1
      Width = 101
    end
    object ButtonCalcPayments: TcxButton
      Left = 36
      Top = 376
      Width = 101
      Height = 33
      Action = acRecalcPayments
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.SkinName = ''
    end
    object ButtonPrint: TcxButton
      Left = 36
      Top = 432
      Width = 101
      Height = 33
      Action = acPrint
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      LookAndFeel.Kind = lfOffice11
      LookAndFeel.SkinName = ''
    end
    object CheckListCompany: TcxCheckListBox
      Left = 3
      Top = 24
      Width = 200
      Height = 130
      IntegralHeight = True
      Items = <
        item
        end
        item
        end
        item
        end
        item
        end
        item
        end>
      ParentFont = False
      Style.Color = clBtnFace
      Style.Edges = []
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.LookAndFeel.Kind = lfOffice11
      Style.LookAndFeel.SkinName = 'Foggy'
      Style.IsFontAssigned = True
      StyleDisabled.LookAndFeel.Kind = lfOffice11
      StyleDisabled.LookAndFeel.SkinName = 'Foggy'
      StyleFocused.LookAndFeel.Kind = lfOffice11
      StyleFocused.LookAndFeel.SkinName = 'Foggy'
      StyleHot.LookAndFeel.Kind = lfOffice11
      StyleHot.LookAndFeel.SkinName = 'Foggy'
      TabOrder = 4
    end
  end
  object DataSetCompany: TpFIBDataSet
    SelectSQL.Strings = (
      'select ID, Name from WEB$CONTRAGENTS$COMPANY')
    Transaction = trPayments
    Database = dm.db
    Left = 176
    Top = 432
    object DataSetCompanyID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetCompanyNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 64
      EmptyStrToNull = True
    end
  end
  object DataSetPayments: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  N,'
      '  company$id,'
      '  name,'
      '  cost$sell,'
      '  cost$return,'
      '  cost$payment,'
      '  balance$start,'
      '  balance$end'
      'from'
      
        '  web$contragents$balance(:SelfCompID, :PeriodBegin, :PeriodEnd)' +
        ' '
      '  ')
    BeforeOpen = DataSetPaymentsBeforeOpen
    Transaction = trPayments
    Database = dm.db
    Left = 176
    Top = 464
  end
  object trPayments: TpFIBTransaction
    DefaultDatabase = dm.db
    TimeoutAction = TARollback
    Left = 176
    Top = 400
  end
  object dsCompany: TDataSource
    DataSet = DataSetCompany
    Left = 208
    Top = 432
  end
  object dsPayments: TDataSource
    Left = 208
    Top = 464
  end
  object ActionList: TActionList
    Left = 176
    Top = 368
    object acRecalcPayments: TAction
      Caption = #1056#1072#1089#1095#1105#1090
      ShortCut = 116
      OnExecute = acRecalcPaymentsExecute
    end
    object acPrint: TAction
      Caption = #1055#1077#1095#1072#1090#1100
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
  end
  object Printer: TdxComponentPrinter
    CurrentLink = GridBalancePrinterLink
    PreviewOptions.Caption = #1055#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1099#1081' '#1087#1088#1086#1089#1084#1086#1090#1088
    PrintTitle = #1055#1077#1095#1072#1090#1100
    Version = 0
    Left = 176
    Top = 336
    object GridBalancePrinterLink: TdxGridReportLink
      Active = True
      Component = GridBalance
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 10000
      PrinterPage.Margins.Left = 10000
      PrinterPage.Margins.Right = 10000
      PrinterPage.Margins.Top = 10000
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.Caption = #1055#1077#1095#1072#1090#1100' '#1074#1079#1072#1080#1084#1086#1088#1072#1089#1095#1105#1090#1086#1074
      ReportDocument.CreationDate = 42580.417630277780000000
      ShrinkToPageWidth = True
      OptionsCards.AutoWidth = True
      OptionsExpanding.ExpandMasterRows = True
      OptionsOnEveryPage.Footers = False
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.ExpandButtons = False
      OptionsView.FilterBar = False
      OptionsView.GroupFooters = False
      BuiltInReportLink = True
    end
  end
  object DataSetPaymentsAll: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  company$id,'
      '  name,'
      '  sum(cost$sell) cost$sell,'
      '  sum(cost$return) cost$return,'
      '  sum(cost$payment) cost$payment,'
      '  sum(balance$start) balance$start,'
      '  sum(balance$end) balance$end'
      'from'
      '  web$contragents$balance2(:PeriodBegin, :PeriodEnd)'
      'where'
      '  self$company$id in (Companys)'
      'group by '
      '  company$id,'
      '  name')
    BeforeOpen = DataSetPaymentsAllBeforeOpen
    Transaction = trPayments
    Database = dm.db
    Left = 176
    Top = 496
  end
end
