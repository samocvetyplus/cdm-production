{***********************************************}
{  ������ ������ ��� ������������               }
{  Copyrigth (C) 2002-2003 basile for CDM       }
{  v1.10                                        }
{  last update  26.01.2004                      }
{***********************************************}

unit AList;

interface
                       
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor, Menus, ImgList, StdCtrls,
  ExtCtrls, Grids, DBGrids, RXDBCtrl, ComCtrls, db,
  Mask, DBCtrlsEh, ActnList, Buttons, DBGridEh, TB2Item,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar, PrnDbgeh, Printers;

type
  TfmAList = class(TfmListAncestor)
    lbOper: TLabel;
    cmbxOper: TDBComboBoxEh;
    acWork: TAction;
    acRejAdd: TAction;
    acWorkCrr: TAction;
    TBItem5: TTBItem;
    TBSeparatorItem3: TTBSeparatorItem;
    acMultiSelect: TAction;
    PrintGridDocList: TPrintDBGridEh;
    TBSeparatorItem2: TTBSeparatorItem;
    TBItem6: TTBItem;
    procedure FormCreate(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acEditDocExecute(Sender: TObject);
    procedure acWorkExecute(Sender: TObject);
    procedure acRejAddExecute(Sender: TObject);
    procedure cmbxOperChange(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acWorkCrrExecute(Sender: TObject);
    procedure acMultiSelectExecute(Sender: TObject);
    procedure acAddDocUpdate(Sender: TObject);
    procedure TBItem6Click(Sender: TObject);
  protected
    procedure DepClick(Sender : TObject); override;
  end;

var
  ApplKind : string;
  FCreated : boolean;

implementation

uses ApplData, MainData, DocAncestor, Appl, dbUtil, DateUtils, Period, fmUtils,
  DictData, dWhA2, dbTree, ExecAppl, RejAdd;

{$R *.dfm}

procedure TfmAList.FormCreate(Sender: TObject);
var
  DepartmentItem : TMenuItem;

procedure InitDepartments;
var
  i: Integer;
begin
  dmAppl.ApplWh := 0;
  dmAppl.ApplNewOperId := '';
  DepartmentItem := nil;
  for i := 0 to Pred(dm.CountDep) do
  if dm.DepInfo[i].Wh then
  begin
    dmAppl.ApplWh := dm.DepInfo[i].DepId;
    break;
  end;
end;

begin
  cmbxOper.OnChange := nil;
  cmbxOper.Items.Clear;
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.ItemIndex := 0;
  cmbxOper.OnChange := cmbxOperChange;

  dm.CurrDep := -1;
  InitDepartments;

  inherited;

  if ApplKind<>'������'then
  begin
    gridDocList.FieldColumns['ABD'].Visible := False;
    gridDocList.FieldColumns['AED'].Visible := False;
    spitAdd.Enabled := False;
    spitDel.Enabled := False;
  end;

  FCreated := false;
end;

procedure TfmAList.DepClick(Sender: TObject);
begin
  inherited;
end;

procedure TfmAList.acWorkExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmExecAppl, TForm(fmExecAppl));
end;

procedure TfmAList.cmbxOperChange(Sender: TObject);
begin
  if(TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code = ROOT_OPER_CODE) then dmAppl.ApplNewOperId := ''
  else dmAppl.ApplNewOperId  := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSet(DataSet);
end;

procedure TfmAList.acRejAddExecute(Sender: TObject);
begin
  ShowAndFreeForm(TfmRejAdd, TForm(fmRejAdd));
end;

procedure TfmAList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(ApplKind = '������')then
  begin
    if dmAppl.taAListISCLOSE.AsInteger = 0 then Background:=clInfoBk
    else if dmAppl.taAListISPROCESS.AsInteger = 1 then Background:=clBtnFace
    else Background := clLime;
  end
  else begin
    if dmAppl.taAListISCLOSE.AsInteger = 0 then Background := clRed
    else if dmAppl.taAListISPROCESS.AsInteger = 1 then Background:=clBtnFace
         else Background := clInfoBk;
  end;
end;

procedure TfmAList.TBItem6Click(Sender: TObject);
var
  Period: AnsiString;
  GetCellIvent: TGetCellEhParamsEvent;
begin
  inherited;
  Period := laPeriod.Caption;
  Delete(Period, 1, 2);
  dmAppl.taAList.Filter := 'IsProcess = 0 and IsClose = 1';
  dmAppl.taAList.Filtered := true;
  //showmessage('Filtred');
  if dmAppl.taAList.IsEmpty then
      ShowMessage('��� ������ ��� ������!')
  else
    begin
      GridDocList.Columns[2].Visible := false;
      GetCellIvent := GridDocList.OnGetCellParams;
      GridDocList.OnGetCellParams := nil;
      PrintGridDocList.BeforeGridText.Text := '������ �������� ������� ��� ������������ c' + UpperCase(Period);
      Printer.Orientation := poLandscape;
      PrintgridDocList.DBGridEh.FixedColor := clWhite;
      //PrintGridDocList.Preview;
      PrintGridDocList.Print;
      GridDocList.OnGetCellParams := GetCellIvent;
      GridDocList.Columns[2].Visible := true;
    end;
    dmAppl.taAList.Filter := '';
    dmAppl.taAList.Filtered := false;
end;

procedure TfmAList.acAddDocExecute(Sender: TObject);
begin
  if dm.User.wWhId <> 0 then
  begin
    with cmbxOper, Items do
    dmAppl.ApplNewOperId  := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
    DataSet.Append;
    DataSet.Post;
    DataSet.Transaction.CommitRetaining;
    with dmAppl do
    ShowDocForm(TfmAppl, taAppl, taAListID.AsInteger, dmMain.quTmp);
    RefreshDataSets([dmAppl.taAList]);
    FCreated := true;
  end
  else raise Exception.Create('������������ �� ����� � ��������');
end;

procedure TfmAList.acAddDocUpdate(Sender: TObject);
var
  DepartmentID: Integer;
  Department: TDepInfo;
  Enabled: Boolean;
begin
  Enabled := (not ReadOnly) and DataSet.Active;
  if Enabled then
  begin
    if (TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code <> ROOT_OPER_CODE) then
    begin
      {
      DepartmentID := dmAppl.ApplWh;
      if DepartmentID <> 0 then
      begin
        Department := dm.DepInfoById[DepartmentID];
        Enabled := Department.Access;
      end
      else Enabled := False;
      }
    end
    else Enabled := False;
  end;

  acAddDoc.Enabled := Enabled;
end;

procedure TfmAList.acEditDocExecute(Sender: TObject);
begin
  if(ApplKind<>'������')and(dmAppl.taAListISCLOSE.AsInteger=0) then
    raise Exception.CreateRes(ErrorResId+12);
  with dmAppl do
    ShowDocForm(TfmAppl, taAList, taAListID.AsInteger, dmMain.quTmp);
  RefreshDataSets([dmAppl.taAList]);
end;

procedure TfmAList.acWorkCrrExecute(Sender: TObject);
var
  s : string;
  i : integer;
begin
  //for i:=0 to Pred(gridDocList.SelectedRows.Count)
  with dmAppl do
  try
    s := dmAppl.quGetExecApplId.SelectSQL.Text;
    if(gridDocList.SelectedRows.Count > 1)  then
    begin
      taAList.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[0]));
      quGetExecApplId.SelectSQL.Text := 'select '+taAListID.AsString+' APPLID from D_Rec';
      for i:=1 to Pred(gridDocList.SelectedRows.Count) do
      begin
        taAList.GotoBookmark(Pointer(gridDocList.SelectedRows.Items[i]));
        quGetExecApplId.SelectSQL.Text := quGetExecApplId.SelectSQL.Text + ' union '+
          'select '+taAListID.AsString+' APPLID from D_Rec';
      end
    end
    else quGetExecApplId.SelectSQL.Text := 'select '+taAListID.AsString+' APPLID from D_Rec';
    ShowAndFreeForm(TfmExecAppl, TForm(fmExecAppl));  
  finally
    CloseDataSet(dmAppl.quGetExecApplId);
    dmAppl.quGetExecApplId.SelectSQL.Text := s;
  end
end;

procedure TfmAList.acMultiSelectExecute(Sender: TObject);
begin
  if dgMultiSelect in gridDocList.Options then
  begin
    gridDocList.Selection.Clear;
    gridDocList.Options := gridDocList.Options - [dgMultiSelect];
  end
  else gridDocList.Options := gridDocList.Options+[dgMultiSelect];
end;



end.








