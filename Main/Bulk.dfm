inherited fmBulk: TfmBulk
  Left = 230
  Top = 150
  Caption = #1040#1082#1090' '#1085#1072' '#1086#1073#1098#1077#1076#1080#1085#1077#1085#1080#1077' ('#1088#1072#1079#1098#1077#1076#1080#1085#1077#1085#1080#1077') '#1076#1088#1072#1075#1086#1094#1077#1085#1085#1099#1093' '#1082#1072#1084#1085#1077#1081
  ClientHeight = 448
  ClientWidth = 581
  WindowState = wsMaximized
  ExplicitWidth = 589
  ExplicitHeight = 482
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 429
    Width = 581
    Panels = <
      item
        Width = 50
      end>
    ExplicitTop = 429
    ExplicitWidth = 581
  end
  inherited tb1: TSpeedBar
    Width = 581
    ExplicitWidth = 581
  end
  inherited plMain: TPanel
    Width = 581
    Height = 55
    ExplicitWidth = 581
    ExplicitHeight = 55
    inherited lbDep: TLabel
      Width = 49
      Caption = #1050#1083#1072#1076#1086#1074#1072#1103
      ExplicitWidth = 49
    end
    inherited txtDep: TDBText
      Left = 264
      DataField = 'DEPNAME'
      DataSource = dmMain.dsrBulkList
      ExplicitLeft = 264
    end
    inherited edNoDoc: TDBEditEh
      DataField = 'DOCNO'
      DataSource = dmMain.dsrBulkList
    end
    inherited dtedDateDoc: TDBDateTimeEditEh
      DataField = 'DOCDATE'
      DataSource = dmMain.dsrBulkList
    end
  end
  inherited gridItem: TDBGridEh
    Top = 97
    Width = 581
    Height = 332
    DataSource = dmMain.dsrBulk
    FooterRowCount = 1
    SumList.Active = True
    UseMultiTitle = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        DropDownRows = 20
        EditButtons = <>
        FieldName = 'SEMISNAME'
        Footers = <>
        Width = 136
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q0'
        Footer.FieldName = 'Q0'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1044#1086' '#1086#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103' ('#1088#1072#1079#1098#1077#1076#1080#1085#1077#1085#1080#1103')|'#1082#1086#1083'-'#1074#1086
        Width = 57
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W0'
        Footer.FieldName = 'W0'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1044#1086' '#1086#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103' ('#1088#1072#1079#1098#1077#1076#1080#1085#1077#1085#1080#1103')|'#1074#1077#1089
        Width = 47
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footer.FieldName = 'Q'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1086#1089#1083#1077' '#1086#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103' ('#1088#1072#1079#1098#1077#1076#1080#1085#1077#1085#1080#1103')|'#1082#1086#1083'-'#1074#1086
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1055#1086#1089#1083#1077' '#1086#1073#1098#1077#1076#1080#1085#1077#1085#1080#1103' ('#1088#1072#1079#1098#1077#1076#1080#1085#1077#1085#1080#1103')|'#1074#1077#1089
        Width = 58
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RW'
        Footer.FieldName = 'RW'
        Footer.ValueType = fvtSum
        Footers = <>
      end>
  end
  inherited ilButtons: TImageList
    Left = 74
    Top = 228
  end
  inherited fmstr: TFormStorage
    Active = False
  end
  inherited ppPrint: TTBPopupMenu
    Left = 124
    Top = 178
    object TBItem1: TTBItem
      Action = acPrintAct
    end
  end
  object acEvent: TActionList
    Left = 124
    Top = 228
    object acPrintAct: TAction
      Caption = #1040#1082#1090' '#1089#1089#1099#1087#1082#1080
      OnExecute = acPrintActExecute
    end
  end
end
