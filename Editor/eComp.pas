unit eComp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DbEditor, StdCtrls, Buttons, ExtCtrls, DBCtrlsEh, Mask, ImgList,
  ActnList, DBCtrls, TB2Item, TB2Dock, TB2Toolbar, Grids, DBGridEh,
  ComCtrls, DBGridEhGrouping, GridsEh;

type
  TfmeComp = class(TfmDbEditor)
    Panel2: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label23: TLabel;
    Label25: TLabel;
    Label19: TLabel;
    Label7: TLabel;
    Label18: TLabel;
    Label21: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label24: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    edCountry: TDBEditEh;
    edPostIndex: TDBEditEh;
    edAddress: TDBEditEh;
    edPhone: TDBEditEh;
    edFax: TDBEditEh;
    edEMail: TDBEditEh;
    cbSeller: TDBCheckBoxEh;
    cbBuyer: TDBCheckBoxEh;
    chbxLawFace1: TDBCheckBoxEh;
    chbxPhysFace: TDBCheckBoxEh;
    edName: TDBEditEh;
    edINN: TDBEditEh;
    edBoss: TDBEditEh;
    edBossPhone: TDBEditEh;
    edBuh: TDBEditEh;
    edBuhPhone: TDBEditEh;
    edOKPO: TDBEditEh;
    edOKONH: TDBEditEh;
    edKPP: TDBEditEh;
    edOFIO: TDBEditEh;
    edOFIOPhone: TDBEditEh;
    edFactAddress: TDBEditEh;
    edPostAddress: TDBEditEh;
    pgctl: TPageControl;
    tbshProps: TTabSheet;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label6: TLabel;
    Label9: TLabel;
    Label8: TLabel;
    Label30: TLabel;
    Label5: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    Label22: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    edBIK: TDBEditEh;
    edBank: TDBEditEh;
    edBill: TDBEditEh;
    edKBank: TDBEditEh;
    edKBill: TDBEditEh;
    edPaspSer: TDBEditEh;
    edPaspNum: TDBEditEh;
    edDistrPlace: TDBEditEh;
    deDistrDate: TDBDateTimeEditEh;
    edCertN: TDBEditEh;
    dtCertDate: TDBDateTimeEditEh;
    edNo_GovReg: TDBEditEh;
    edD_GovReg: TDBDateTimeEditEh;
    edNo_FisLPlace: TDBEditEh;
    edD_FisLPlace: TDBDateTimeEditEh;
    TabSheet2: TTabSheet;
    gridCompOwner: TDBGridEh;
    TBDock1: TTBDock;
    tlbrCompOwner: TTBToolbar;
    TBItem2: TTBItem;
    TBItem1: TTBItem;
    tbshDOG: TTabSheet;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    edDogNo: TDBEditEh;
    dtedDogBd: TDBDateTimeEditEh;
    dtedDogED: TDBDateTimeEditEh;
    dtedREUED: TDBDateTimeEditEh;
    TabSheet1: TTabSheet;
    mmComment: TDBMemo;
    acEvent: TActionList;
    acAdd: TAction;
    acDel: TAction;
    acSort: TAction;
    acExit: TAction;
    acAddCompOwner: TAction;
    acDelCompOwner: TAction;
    acPrintCompList: TAction;
    ilButtons: TImageList;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmeComp: TfmeComp;

implementation

uses DictData;

{$R *.dfm}

end.
