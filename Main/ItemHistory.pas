unit ItemHistory;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGrids, RXDBCtrl, dbUtil, DB, StdCtrls,
  RXSpin, DBGridEh, ActnList, Menus, TB2Item, Buttons, DBGridEhGrouping,
  GridsEh, Mask, rxPlacemnt, rxSpeedbar;

type
  TfmItemHistory = class(TfmAncestor)
    Label1: TLabel;
    edUID: TRxSpinEdit;
    SpeedItem1: TSpeedItem;
    gridUIDHistory: TDBGridEh;
    SpeedItem2: TSpeedItem;
    acPrintSticker: TAction;
    SpeedItem3: TSpeedItem;
    acIns: TAction;
    ppOther: TTBPopupMenu;
    spitBarCode: TSpeedItem;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acCheckBarCodeReady: TAction;
    acForwardBarCode: TAction;
    acBackwardBarCode: TAction;
    TBItem3: TTBItem;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
    procedure edUIDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure gridUIDHistoryGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintStickerExecute(Sender: TObject);
    procedure acInsExecute(Sender: TObject);
    procedure acInsUpdate(Sender: TObject);
    procedure acPrintStickerUpdate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acForwardBarCodeExecute(Sender: TObject);
    procedure acBackwardBarCodeExecute(Sender: TObject);
    procedure acForwardBarCodeUpdate(Sender: TObject);
    procedure acCheckBarCodeReadyUpdate(Sender: TObject);
    procedure acBackwardBarCodeUpdate(Sender: TObject);
  end;

var
  fmItemHistory: TfmItemHistory;

implementation

uses InvData, PrintData, fmUtils, DictData, eArtIns, eStickerDirection, Editor;

{$R *.dfm}

procedure TfmItemHistory.FormShow(Sender: TObject);
begin
  inherited;
  Caption:=Caption+' '+IntToStr(dmInv.FCurrUID);
  edUID.Value:= dmInv.FCurrUID;
  if dmInv.FCurrUID = 0 then CloseDataSet(dmInv.taItemHistory)
  else OpenDataSet(dmInv.taItemHistory);
end;

procedure TfmItemHistory.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CloseDataSets([dmINV.taItemHistory]);
end;

procedure TfmItemHistory.SpeedItem1Click(Sender: TObject);
begin
  inherited;
  Caption:='������� ������� '+edUID.Text;
  dmINV.FCurrUID:=edUID.AsInteger;
  ReOpenDataSets([dmINv.taItemHistory]);
end;

procedure TfmItemHistory.edUIDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key=VK_RETURN   then  SpeedItem1Click(self);
end;

procedure TfmItemHistory.gridUIDHistoryGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if Column.FieldName='OPNAME' then
  begin
   if dmInv.taItemHistoryOPNAME.AsString='�������' then Background:=clMoneyGreen;
   if dmInv.taItemHistoryOPNAME.AsString='�������' then Background:=clSell;
   if dmInv.taItemHistoryOPNAME.AsString='��������' then Background:=clIn;
  end;
  if AnsiUpperCase(Column.FieldName)='CLOSE_' then
  begin
    case dmInv.taItemHistoryCLOSE_.AsInteger of
      0 : Background := clRed;
      else Background := clBtnFace;
    end;
  end;
end;

procedure TfmItemHistory.acPrintStickerExecute(Sender: TObject);
var
  UID: integer;
begin
  // ����������� �����
  if not dm.LocalSetting.BarCode_Use then EWarning.Create('� ���������� �� ������� ������������� �������� ��������');
  UID := StrToIntDef(edUID.Text, 0);
  if(UID=0) then
  begin
    ActiveControl := edUID;
    EWarning.Create('������� ����� �������');
  end;
  dmPrint.PrintStickerNew(UID, true);
end;

procedure TfmItemHistory.acInsExecute(Sender: TObject);
begin
  dmInv.CurrArt2Id := dmInv.taItemHistoryART2ID.AsInteger;
  if fmArtIns = nil then
  begin
    fmArtIns:=TfmArtIns.Create(Self);
    fmArtIns.ShowModal;
  end
end;

procedure TfmItemHistory.acInsUpdate(Sender: TObject);
begin
  acIns.Enabled := dmInv.taItemHistory.Active and (not dmInv.taItemHistory.IsEmpty);
end;

procedure TfmItemHistory.acPrintStickerUpdate(Sender: TObject);
begin
  acPrintSticker.Enabled := dmInv.taItemHistory.Active and (not dmInv.taItemHistory.IsEmpty);
end;

procedure TfmItemHistory.FormCreate(Sender: TObject);
begin
  spitBarCode.Visible := dm.LocalSetting.BarCode_Use;
  inherited;
end;

procedure TfmItemHistory.acForwardBarCodeExecute(Sender: TObject);
begin
  if ShowEditor(TfmeStikerDirection, TfmEditor(fmeStikerDirection), '������')<>mrOk then eXit;
  dmPrint.BarCode_Forward(Editor.vResult);
end;

procedure TfmItemHistory.acBackwardBarCodeExecute(Sender: TObject);
begin
  if ShowEditor(TfmeStikerDirection, TfmEditor(fmeStikerDirection), '�����')<>mrOk then eXit;
  dmPrint.BarCode_Backward(Editor.vResult);
end;

procedure TfmItemHistory.acForwardBarCodeUpdate(Sender: TObject);
begin
  acForwardBarCode.Enabled := dm.LocalSetting.BarCode_Use;
end;

procedure TfmItemHistory.acCheckBarCodeReadyUpdate(Sender: TObject);
begin
  acCheckBarCodeReady.Enabled := dm.LocalSetting.BarCode_Use;
end;

procedure TfmItemHistory.acBackwardBarCodeUpdate(Sender: TObject);
begin
  acBackwardBarCode.Enabled := dm.LocalSetting.BarCode_Use;
end;

end.

