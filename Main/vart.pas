{****************************************}
{  ������������ ���������. �����������   }
{  Copyrigth (C) 2003 basile for CDM     }
{  v0.12                                 }
{****************************************}
unit VArt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGridEh, ActnList, TB2Item, TB2Dock, TB2Toolbar, Menus, PrnDbgeh,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmVArt = class(TfmAncestor)
    gridAssort: TDBGridEh;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acEvent: TActionList;
    acSetFQ0: TAction;
    acCopyQ_FQ: TAction;
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;
    acAdd: TAction;
    acDel: TAction;
    ppAssort: TTBPopupMenu;
    TBItem3: TTBItem;
    acDetail: TAction;
    pVart: TPrintDBGridEh;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem4: TTBItem;
    acPrintGrid: TAction;
    spitPrintGrid: TSpeedItem;
    procedure acAddExecute(Sender: TObject);
    procedure acDelExecute(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure acCopyQ_FQExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDetailExecute(Sender: TObject);
    procedure acCopyQ_FQUpdate(Sender: TObject);
    procedure acSetFQ0Execute(Sender: TObject);
    procedure acSetFQ0Update(Sender: TObject);
    procedure acPrintGridExecute(Sender: TObject);
  private
    FDetail: boolean;
    procedure SetDetail(const Value: boolean);
    procedure Access;
  public
    property Detail : boolean read FDetail write SetDetail;
  end;

var
  fmVArt: TfmVArt;

implementation

uses MainData, DictData, MsgDialog, dbUtil, InvData, PrintData;

{$R *.dfm}

procedure TfmVArt.acAddExecute(Sender: TObject);
begin
  dmMain.taVArt.Append;
end;

procedure TfmVArt.acDelExecute(Sender: TObject);
begin
  dmMain.taVArt.Delete;
end;

procedure TfmVArt.acCopyQ_FQExecute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  if MessageDialog('�� ������������� ������� ����������� ������� "������� ���-��" � "����������� ���-��"?',
    mtWarning, [mbYes, mbNo], 0) = mrNo then eXit;
  with dmMain do
  begin
    Bookmark := taVArt.GetBookmark;
    taVArt.DisableControls;
    try
      taVArt.First;
      while not taVArt.Eof do
      begin
        taVArt.Edit;
        taVArtFQ.AsFloat := taVArtQ.AsFloat;
        taVArt.Post;
        taVArt.Next;
      end;
    finally
      taVArt.EnableControls;
      taVArt.GotoBookmark(Bookmark);
    end;
  end;
end;

procedure TfmVArt.FormCreate(Sender: TObject);
begin
  Detail := False;
  Access;
  Caption := '����������� �� ������������. ��������: '+dmInv.taSemisReportDEPNAME.AsString+
       ' ��������: '+dmInv.taSemisReportOPERATION.AsString;
  inherited;
  OpenDataSet(dmMain.taVArt);
end;

procedure TfmVArt.FormClose(Sender: TObject; var Action: TCloseAction);
var
 // BookMark : Pointer;
  q, fq : integer;
begin
  with dmMain do
  begin
    PostDataSet(taVArt);
    taVArt.First;
    q := 0; fq := 0;
    while not taVArt.Eof do
    begin
      q := q + dmMain.taVArtQ.AsInteger;
      fq := fq + dmMain.taVArtFQ.AsInteger;
      taVArt.Next;
    end;
  end;
  CloseDataSet(dmMain.taVArt);

  if dmMain.taVListISCLOSE.AsInteger = 0 then
  begin
    dmInv.taSemisReport.Edit;
    dmInv.taSemisReportQA.AsInteger := q;
    dmInv.taSemisReportFQA.AsInteger := fq;
    dmInv.taSemisReport.Post;
  end;
end;

procedure TfmVArt.acDetailExecute(Sender: TObject);
begin
  Detail := not Detail;
  (Sender as TAction).Checked := Detail;
end;

procedure TfmVArt.SetDetail(const Value: boolean);
begin
  FDetail := Value;
  gridAssort.FieldColumns['REST_IN_Q'].Visible := Value;
  gridAssort.FieldColumns['IN7_Q'].Visible := Value;
  gridAssort.FieldColumns['IN9_Q'].Visible := Value;
  gridAssort.FieldColumns['MOVE_IN_Q'].Visible := Value;
  gridAssort.FieldColumns['RET_PROD_Q'].Visible := Value;
  gridAssort.FieldColumns['PROD_Q'].Visible := Value;
  gridAssort.FieldColumns['OUT_Q'].Visible := Value;
  gridAssort.FieldColumns['MOVE_OUT_Q'].Visible := Value;
end;

procedure TfmVArt.acSetFQ0Execute(Sender: TObject);
var
  Bookmark : Pointer;
begin
  if MessageDialog('�� ������������� ������� �������� ������� "����������� ����������"?', mtWarning, [mbYes, mbNo], 0)=mrNo then eXit;
  with dmMain do
  begin
    Bookmark := taVArt.GetBookmark;
    taVArt.DisableControls;
    try
      taVArt.First;
      while not taVArt.Eof do
      begin
        taVArt.Edit;
        taVArtFQ.AsFloat := 0;
        taVArt.Post;
        taVArt.Next;
      end;
    finally
      taVArt.EnableControls;
      taVArt.GotoBookmark(Bookmark);
    end;
  end;
end;

procedure TfmVArt.acCopyQ_FQUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not dmMain.taVArt.IsEmpty) and
                                 (not dmMain.taVArtID.IsNull);
end;

procedure TfmVArt.acSetFQ0Update(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not dmMain.taVArt.IsEmpty) and
                                 (not dmMain.taVArtID.IsNull);
end;

procedure TfmVArt.acDelUpdate(Sender: TObject);
begin
  (Sender as TAction).Enabled := (not dmMain.taVArt.IsEmpty) and
                                 (not dmMain.taVArtID.IsNull);
end;


procedure TfmVArt.Access;
begin
  if dm.IsAdm then eXit;
  gridAssort.ReadOnly := True;
  acAdd.Enabled := False;
  acDel.Enabled := False;
  acCopyQ_FQ.Enabled := False;
  acSetFQ0.Enabled := False;
end;

procedure TfmVArt.acPrintGridExecute(Sender: TObject);
begin
  dmPrint.FCap:=Caption;
  dmPrint.PrintDocumentA(dkAssortDet, null);


{  gridAssort.FieldColumns['DEPNAME'].Visible := False;
  gridAssort.FieldColumns['OPERNAME'].Visible := False;
  try
    pVart.Title.Text := Caption;
    pVart.Print;
  finally
    gridAssort.FieldColumns['DEPNAME'].Visible := True;
    gridAssort.FieldColumns['OPERNAME'].Visible := True;
  end;
}
end;

end.
