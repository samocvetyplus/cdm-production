{***********************************************}
{  ��������� ����� ����������                   }
{  Copyrigth (C) 2003 basile for CDM            }
{  v0.1                                         }
{***********************************************}
unit TmpWhSemis;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGridEh, StdCtrls, Mask, DBCtrlsEh, TB2Item, TB2Dock, TB2Toolbar,
  ActnList, PrnDbgeh, DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmTmpWHSemis = class(TfmAncestor)
    gridWhSemis: TDBGridEh;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    cmbxSemis: TDBComboBoxEh;
    TBControlItem2: TTBControlItem;
    TBControlItem3: TTBControlItem;
    Label2: TLabel;
    cmbxDep: TDBComboBoxEh;
    TBControlItem4: TTBControlItem;
    TBControlItem5: TTBControlItem;
    Label3: TLabel;
    cmbxOper: TDBComboBoxEh;
    TBControlItem6: TTBControlItem;
    TBControlItem7: TTBControlItem;
    Label4: TLabel;
    cmbxMat: TDBComboBoxEh;
    TBControlItem8: TTBControlItem;
    spitPrint: TSpeedItem;
    TBDock2: TTBDock;
    TBToolbar2: TTBToolbar;
    TBControlItem9: TTBControlItem;
    Label5: TLabel;
    edBd: TDBDateTimeEditEh;
    TBControlItem10: TTBControlItem;
    TBControlItem11: TTBControlItem;
    Label6: TLabel;
    edEd: TDBDateTimeEditEh;
    TBControlItem12: TTBControlItem;
    TBItem1: TTBItem;
    acEvent: TActionList;
    acGo: TAction;
    PrintGird: TPrintDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure cmbxDepChange(Sender: TObject);
    procedure cmbxMatChange(Sender: TObject);
    procedure cmbxOperChange(Sender: TObject);
    procedure cmbxSemisChange(Sender: TObject);
    procedure spitPrintClick(Sender: TObject);
    procedure acGoExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmTmpWHSemis: TfmTmpWHSemis;

implementation

uses MainData, DictData, dbUtil, dbTree, PrintData;

{$R *.dfm}

procedure TfmTmpWHSemis.FormCreate(Sender: TObject);
var
  SaveEvent : TNotifyEvent;
begin
  dm.WorkMode := 'aWhSemis';
  // ����������
  with dm.tr do
  begin
    if Active then Commit;
    StartTransaction;
  end;
  // ���������������� ������� ������ ������������
  SaveEvent := cmbxOper.OnChange;
  cmbxOper.OnChange := nil;
  cmbxOper.Items.Assign(dm.dOper);
  cmbxOper.ItemIndex := 0;
  cmbxOper.Items.Insert(1, sNoOperation);

  dmMain.FilterOperId := ROOT_OPER_CODE;
  cmbxOper.OnChange := SaveEvent;

  SaveEvent := cmbxDep.OnChange;
  cmbxDep.OnChange := nil;
  cmbxDep.Items.Assign(dm.dPS);
  cmbxDep.ItemIndex := 0;
  dmMain.FilterDepId := ROOT_PS_CODE;
  cmbxDep.OnChange := SaveEvent;

  SaveEvent := cmbxSemis.OnChange;
  cmbxSemis.OnChange := nil;
  cmbxSemis.Items.Assign(dm.dSemis);
  cmbxSemis.ItemIndex := 0;
  dmMain.FilterSemisId := ROOT_SEMIS_CODE;
  cmbxSemis.OnChange := SaveEvent;

  SaveEvent := cmbxMat.OnChange;
  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMat);
  cmbxMat.ItemIndex := 0;
  dmMain.FilterMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := SaveEvent;
  // ������� ������ ������
  OpenDataSet(dmMain.taTmpWhSemis);
end;

procedure TfmTmpWHSemis.cmbxDepChange(Sender: TObject);
begin
  dmMain.FilterDepId := TNodeData(cmbxDep.Items.Objects[cmbxDep.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taTmpWHSemis);
end;

procedure TfmTmpWHSemis.cmbxMatChange(Sender: TObject);
begin
  dmMain.FilterMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taTmpWHSemis);
end;

procedure TfmTmpWHSemis.cmbxOperChange(Sender: TObject);
begin
  with cmbxOper, Items do
    if Items[ItemIndex] = sNoOperation then dmMain.FilterOperId := '���'
    else dmMain.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taTmpWHSemis);
end;

procedure TfmTmpWHSemis.cmbxSemisChange(Sender: TObject);
begin
  dmMain.FilterSemisId := TNodeData(cmbxSemis.Items.Objects[cmbxSemis.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taTmpWHSemis);
end;

procedure TfmTmpWHSemis.spitPrintClick(Sender: TObject);
begin
  PrintGird.Preview;
end;

procedure TfmTmpWHSemis.acGoExecute(Sender: TObject);
begin
  ExecSQL('execute procedure Build_WhSemis("'+edBd.Text+'", "'+edEd.Text+'")', dmMain.quTmp);
  ReOpenDataSet(dmMain.taTmpWhSemis);
end;

procedure TfmTmpWHSemis.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmMain.taTmpWhSemis);
  inherited;
end;

end.
