inherited fmIns: TfmIns
  Width = 496
  Height = 396
  Caption = ''
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 343
    Width = 488
  end
  inherited plDict: TPanel
    Width = 488
    Height = 301
    inherited plSelect: TPanel
      Top = 258
      Width = 484
    end
    inherited gridDict: TDBGridEh
      Width = 484
      Height = 256
      DataSource = dm.dsrIns
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Width = 24
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'D_INSID'
          Footers = <>
          Width = 52
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NAME'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077'|'#1055#1086#1083#1085#1086#1077
          Width = 93
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SNAME'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077'|'#1050#1088#1072#1090#1082#1086#1077' '
          Width = 94
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'UNITID'
          Footers = <>
          Visible = False
          Width = 41
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'EDGSHID'
          Footers = <>
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'EDGTID'
          Footers = <>
          Visible = False
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'GR'
          Footers = <>
          Visible = False
        end>
    end
  end
  inherited tb1: TSpeedBar
    Width = 488
  end
  inherited ilButtons: TImageList
    Top = 188
  end
  inherited fmstr: TFormStorage
    Top = 140
  end
end
