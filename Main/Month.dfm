object fmMonth: TfmMonth
  Left = 613
  Top = 176
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = #1042#1099#1073#1086#1088' '#1087#1077#1088#1080#1086#1076#1072
  ClientHeight = 91
  ClientWidth = 217
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 1
    Width = 215
    Height = 59
    Shape = bsFrame
  end
  object RxLabel1: TRxLabel
    Left = 3
    Top = 36
    Width = 78
    Height = 13
    Caption = #1050#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072
  end
  object RxLabel2: TRxLabel
    Left = 2
    Top = 12
    Width = 84
    Height = 13
    Caption = #1053#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072
  end
  object deED: TDateEdit
    Left = 88
    Top = 33
    Width = 121
    Height = 21
    Color = clInfoBk
    DefaultToday = True
    NumGlyphs = 2
    TabOrder = 0
  end
  object deBD: TDateEdit
    Left = 88
    Top = 8
    Width = 121
    Height = 21
    Color = clInfoBk
    DefaultToday = True
    NumGlyphs = 2
    TabOrder = 1
  end
  object BitBtn1: TBitBtn
    Left = 72
    Top = 64
    Width = 75
    Height = 25
    TabOrder = 2
    OnClick = BitBtn1Click
    Kind = bkOK
  end
end
