object fmAddToInv: TfmAddToInv
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1042#1099#1073#1086#1088' '#1085#1072#1082#1083#1072#1076#1085#1086#1081' '#1076#1083#1103' '#1076#1086#1073#1072#1074#1083#1077#1085#1080#1103
  ClientHeight = 208
  ClientWidth = 291
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lNum: TLabel
    Left = 8
    Top = 11
    Width = 128
    Height = 13
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#8470' '#1085#1072#1082#1083#1072#1076#1085#1086#1081':'
  end
  object lDate: TLabel
    Left = 48
    Top = 40
    Width = 88
    Height = 13
    Caption = #1044#1072#1090#1072' '#1085#1072#1082#1083#1072#1076#1085#1086#1081':'
  end
  object lDep: TLabel
    Left = 66
    Top = 67
    Width = 70
    Height = 13
    Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103':'
  end
  object lKlad: TLabel
    Left = 82
    Top = 96
    Width = 54
    Height = 13
    Caption = #1050#1083#1072#1076#1086#1074#1072#1103':'
  end
  object lNDS: TLabel
    Left = 70
    Top = 128
    Width = 66
    Height = 13
    Caption = #1057#1090#1072#1074#1082#1072' '#1053#1044#1057':'
  end
  object tDate: TDBText
    Left = 160
    Top = 40
    Width = 3
    Height = 13
    AutoSize = True
    DataField = 'DOCDATE'
    DataSource = dsrINV
    Transparent = True
  end
  object tDep: TDBText
    Left = 160
    Top = 67
    Width = 3
    Height = 13
    AutoSize = True
    DataField = 'SUPID'
    DataSource = dsrINV
  end
  object tKlad: TDBText
    Left = 160
    Top = 96
    Width = 3
    Height = 13
    AutoSize = True
    DataField = 'DEPNAME'
    DataSource = dsrINV
  end
  object tNDS: TDBText
    Left = 160
    Top = 128
    Width = 3
    Height = 13
    AutoSize = True
    DataField = 'NDSID'
    DataSource = dsrINV
  end
  object AddButton: TBitBtn
    Left = 39
    Top = 164
    Width = 89
    Height = 25
    Caption = #1044#1086#1073#1072#1074#1080#1090#1100
    TabOrder = 0
    Kind = bkOK
  end
  object CancelButton: TBitBtn
    Left = 176
    Top = 164
    Width = 75
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 2
    Kind = bkCancel
  end
  object NumCBox: TDBLookupComboboxEh
    Left = 148
    Top = 8
    Width = 135
    Height = 21
    DropDownBox.ColumnDefValues.Title.TitleButton = True
    DropDownBox.Rows = 15
    EditButtons = <>
    KeyField = 'CONTRACTID'
    ListField = 'CONTRACTNAME'
    ListSource = dsrINV
    TabOrder = 1
    Visible = True
  end
  object taINV: TpFIBDataSet
    SelectSQL.Strings = (
      'SELECT * '
      'FROM SList_S(:DEPID1, :DEPID2, :BD, :ED,:ITYPE_)')
    Transaction = dm.tr
    Database = dm.db
    Left = 24
    Top = 64
  end
  object dsrINV: TDataSource
    DataSet = taINV
    Left = 24
    Top = 112
  end
end
