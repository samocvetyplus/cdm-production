unit eAffInvType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls;

type
  TfmeAffInvT = class(TfmEditor)
    rgrType: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  end;

var
  fmeAffInvT: TfmeAffInvT;

implementation

{$R *.dfm}

uses fmUtils, DictData, ProductionConsts;

procedure TfmeAffInvT.FormCreate(Sender: TObject);
begin
  rgrType.ItemIndex := 0;
end;

procedure TfmeAffInvT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk : if(rgrType.ItemIndex = -1)then raise EWarning.Create(rsNotDefineAffInvType);
  end;
  vResult := rgrType.ItemIndex;
  inherited;
end;

procedure TfmeAffInvT.FormShow(Sender: TObject);
begin
  ActiveControl := FindNextControl(btnCancel, True, True, False);
end;

end.
