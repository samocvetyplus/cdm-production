unit CompDoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls,
  ComCtrls, Grids, DBGridEh, DB, FIBDataSet, pFIBDataSet, DBGridEhGrouping,
  GridsEh;

type
  TfmCompDoc = class(TfmAncestor)
    gridDoc: TDBGridEh;
    taDoc: TpFIBDataSet;
    dsrDoc: TDataSource;
    taDocDOCDATE: TFIBDateTimeField;
    taDocSELFCOMPNAME: TFIBStringField;
    taDocITYPENAME: TFIBStringField;
    taDocDOCNO: TFIBStringField;
    procedure taDocBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmCompDoc: TfmCompDoc;

implementation

uses DictData, dbUtil;

{$R *.dfm}

procedure TfmCompDoc.taDocBeforeOpen(DataSet: TDataSet);
begin
  taDoc.ParamByName('SUPID').AsInteger := dm.taCompCOMPID.AsInteger;
end;

procedure TfmCompDoc.FormCreate(Sender: TObject);
begin
  OpenDataSet(taDoc);
  inherited;

end;

procedure TfmCompDoc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  CloseDataSet(taDoc);
end;

end.
