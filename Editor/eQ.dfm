inherited fmeQ: TfmeQ
  Left = 291
  ActiveControl = edQ
  Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1072#1088#1090#1080#1082#1091#1083#1086#1074
  ClientHeight = 94
  ClientWidth = 197
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  ExplicitWidth = 203
  ExplicitHeight = 126
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 197
    Height = 94
    ExplicitWidth = 197
    ExplicitHeight = 94
    object lbQ: TLabel [0]
      Left = 12
      Top = 16
      Width = 59
      Height = 13
      Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbUnit: TLabel [1]
      Left = 12
      Top = 36
      Width = 39
      Height = 13
      Caption = #1045#1076'. '#1080#1079#1084
    end
    inherited btnOk: TBitBtn
      Left = 18
      Top = 59
      Anchors = [akRight, akBottom]
      TabOrder = 2
      ExplicitLeft = 18
      ExplicitTop = 59
    end
    inherited btnCancel: TBitBtn
      Left = 106
      Top = 59
      Anchors = [akRight, akBottom]
      TabOrder = 3
      ExplicitLeft = 106
      ExplicitTop = 59
    end
    object cmbxU: TDBComboBoxEh
      Left = 80
      Top = 32
      Width = 101
      Height = 19
      EditButtons = <>
      Flat = True
      Items.Strings = (
        #1064#1090'.'
        #1055#1072#1088#1072'.')
      KeyItems.Strings = (
        '1'
        '2')
      TabOrder = 1
      Visible = True
    end
    object edQ: TDBNumberEditEh
      Left = 80
      Top = 12
      Width = 101
      Height = 19
      EditButton.Style = ebsUpDownEh
      EditButton.Visible = True
      EditButtons = <>
      Flat = True
      TabOrder = 0
      Value = 0.000000000000000000
      Visible = True
    end
  end
end
