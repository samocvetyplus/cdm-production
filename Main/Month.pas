unit Month;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, RXCtrls, Mask, RxToolEdit, ExtCtrls, DateUtils;

type
  TfmMonth = class(TForm)
    deED: TDateEdit;
    RxLabel1: TRxLabel;
    RxLabel2: TRxLabel;
    deBD: TDateEdit;
    BitBtn1: TBitBtn;
    Bevel1: TBevel;
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    VED_BD, VED_ED:TDateTime;
  end;

var
  fmMonth: TfmMonth;


implementation

uses PrintData, DictData;

{$R *.dfm}

procedure TfmMonth.BitBtn1Click(Sender: TObject);
begin
 Screen.Cursor:=crHourGlass;
 Close;
 VED_BD:=deBD.Date;
 VED_ED:=deED.Date;
 case dmPrint.NTYPE of
  0:  dmPrint.PrintDocumentA(dkJournalOrder,null); // �� ����������
  1:  dmPrint.PrintDocumentA(dkJournalOrder2,null); // �� ���������
  2:  begin
       dm.BeginDate:=VED_BD;
       dm.EndDate:=VED_ED;
       dmPrint.PrintDocumentA(dkKolie,null); // �� �������
    end
 end;
 Screen.Cursor:=crDefault;
end;

procedure TfmMonth.FormCreate(Sender: TObject);
begin
 deBD.Date:=StartOfTheMonth(Today);
 deED.Date:=EndOfTheMonth(Today);
end;

end.
