unit ImportMC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList,  ImgList,  ExtCtrls,
  ComCtrls, TB2Dock, TB2Toolbar, StdCtrls, Mask, DBCtrlsEh, TB2Item,
  FIBDatabase, pFIBDatabase, FIBQuery, pFIBQuery, DB, FIBDataSet,
  pFIBDataSet, Grids, DBGridEh, strUtils, DBGridEhGrouping, GridsEh,
  rxPlacemnt, rxSpeedbar;

type
  TfmImportMC = class(TfmAncestor)
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    TBControlItem2: TTBControlItem;
    Label1: TLabel;
    edPath: TDBEditEh;
    dlgDb: TOpenDialog;
    trMC: TpFIBTransaction;
    quTmpMC: TpFIBQuery;
    dbMC: TpFIBDatabase;
    taInvList: TpFIBDataSet;
    gridInvList: TDBGridEh;
    dsrInvList: TDataSource;
    taInvListINVID: TFIBIntegerField;
    taInvListDOCNO: TFIBIntegerField;
    taInvListDOCDATE: TFIBDateTimeField;
    taInvListITYPE: TFIBSmallIntField;
    taInvListSUPNAME: TFIBStringField;
    taInvListSSF: TFIBStringField;
    taInvListISCLOSE: TFIBSmallIntField;
    taInvListQ: TFIBIntegerField;
    taInvListSUMM1: TFIBFloatField;
    taInvListSUMM2: TFIBFloatField;
    taInvListSSFDT: TFIBDateTimeField;
    taInvListW: TFIBFloatField;
    taInvListACC_SUMM: TFIBFloatField;
    taInvListTRANS_PRICE: TFIBFloatField;
    taInvListAKCIZ: TFIBFloatField;
    taInvListPAYTYPEID: TFIBIntegerField;
    taInvListTRANS_PERCENT: TFIBFloatField;
    taInvListSHIFT: TFIBSmallIntField;
    spitImport: TSpeedItem;
    acImport: TAction;
    TBSeparatorItem1: TTBSeparatorItem;
    chbxCreateRest: TDBCheckBoxEh;
    TBControlItem3: TTBControlItem;
    taInvFromMC: TpFIBDataSet;
    taInvFromMCINVID: TFIBIntegerField;
    taRec: TpFIBDataSet;
    taInvListDEPID: TFIBIntegerField;
    taInvListSUPID: TFIBIntegerField;
    taComp: TpFIBDataSet;
    taInvListNDSID: TFIBIntegerField;
    taInvListCOST: TFIBFloatField;
    taInvListISBRACK: TFIBSmallIntField;
    taInvListMEMO: TMemoField;
    taInvListCOMMISION: TFIBSmallIntField;
    taInvListUPPRICE: TFIBFloatField;
    taInvUID: TpFIBDataSet;
    taClient: TpFIBDataSet;
    taInvUIDUID: TFIBIntegerField;
    taInvUIDCHECK_NO: TFIBStringField;
    taInvUIDCLIENTID: TFIBIntegerField;
    acClearDb: TAction;
    taWh: TpFIBDataSet;
    taCash: TpFIBDataSet;
    taCashCOST: TFIBFloatField;
    taCashNDS: TFIBFloatField;
    taCashOPERDATE: TFIBDateTimeField;
    taCashNDOC: TFIBStringField;
    taCashT: TFIBSmallIntField;
    taCashCOMPID: TFIBIntegerField;
    taRecINVID: TFIBIntegerField;
    taRecINVNO: TFIBIntegerField;
    taRecDEPID: TFIBIntegerField;
    taWhUID: TFIBIntegerField;
    taInvUIDPRICE2: TFIBFloatField;
    taInvUIDNDSID: TFIBIntegerField;
    taWhPRICE: TFIBFloatField;
    dbCopy: TpFIBDatabase;
    trCopy: TpFIBTransaction;
    quCopy: TpFIBQuery;
    TBItem1: TTBItem;
    TBSeparatorItem2: TTBSeparatorItem;
    taInvListSELFCOMPNAME: TFIBStringField;
    taInvListSELFCOMPID: TFIBIntegerField;
    taWhEXT_INVID: TFIBIntegerField;
    procedure edPathEditButtons0Click(Sender: TObject; var Handled: Boolean);
    procedure taInvListBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure taInvListSHIFTGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure gridInvListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acImportExecute(Sender: TObject);
    procedure acImportUpdate(Sender: TObject);
    procedure taInvUIDBeforeOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acClearDbExecute(Sender: TObject);
    procedure edPathKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acClearDbUpdate(Sender: TObject);
  private
    procedure OpenMC(err : byte);
  end;

var
  fmImportMC: TfmImportMC;

implementation

{$R *.dfm}

uses MsgDialog, dbUtil, fmUtils, DictData, UtilLib,  FileCtrl,
  ProductionConsts;

procedure TfmImportMC.edPathEditButtons0Click(Sender: TObject; var Handled: Boolean);
begin
  if dlgDb.Execute then
  begin
    edPath.Text := dlgDb.FileName;
    OpenMC(1);
  end;
  Handled := True;
end;

procedure TfmImportMC.OpenMC(err: byte);
var
  Bookmark : Pointer;
  fApply : boolean;
begin
  if trMc.Active then trMc.Rollback;
  dbMC.CloseDataSets;
  dbMC.Close;
  dbMC.DatabaseName := edPath.Text;
  try
    dbMC.Open;
    // ������� ���������� �� ��� ��� ���� ������ ������� ��� �������
    try
      if trMC.Active then trMC.Commit;
      trMC.StartTransaction;
      quTmpMC.SQL.Text := 'select pmc from D_Rec';
      quTmpMC.ExecQuery;
      quTmpMC.Close;
      trMC.Commit;
    except
      on E:Exception do
      begin
        dbMC.Close;
        if(err=1)then MessageDialogA('�� ������ ������ �� ������� "��������� ������"', mtWarning);
        eXit;
      end;
    end;
    OpenDataSet(taInvList);
  except
    on E:Exception do MessageDialogA(E.Message, mtError);
  end;
end;

procedure TfmImportMC.taInvListBeforeOpen(DataSet: TDataSet);
begin
  taInvList.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(StrToDateTime('01.01.1900'));
  taInvList.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(StrToDateTime('01.01.2050'));
  taInvList.ParamByName('ITYPE_').AsInteger := 2;
  taInvList.ParamByName('SELFCOMPID').AsInteger := dm.User.SelfCompId;
end;

procedure TfmImportMC.FormShow(Sender: TObject);
begin
  if FileExists(edPath.Text)then
  begin
    OpenMC(0);
  end;
end;

procedure TfmImportMC.taInvListSHIFTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  case Sender.AsInteger of
    0 : Text := '���';
    1 : Text := '�������';
    else Text := '';
  end;
end;

procedure TfmImportMC.gridInvListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  case taInvListSHIFT.AsInteger of
    0 : Background := clWindow;
    1 : Background := clInfoBk;
  end;
end;

type
  TDescr = class(TObject)
    Descr : string;
  end;

procedure TfmImportMC.acImportExecute(Sender: TObject);

const
  cDelim = ';';

var
  SQLScript : TStringList;


  procedure ImportDict(DataSet : TpFIBDataSet; TableName, Id, Exc, Gen, Name: string);
  var
    Ins_, Upd_ : boolean;
    Statement, s : string;
    i, IdVal : integer;
    Descr : TDescr;
  begin
    OpenDataSet(DataSet);
    while not DataSet.Eof do
    begin
      Ins_ := DataSet.FieldByName('INS_').AsInteger=1;
      Upd_ := DataSet.FieldByName('UPD_').AsInteger=1;
      IdVal := 0;
      if(Ins_)then
      begin
        IdVal := ExecSelectSQL('select gen_id('+Gen+', 1) from rdb$database', dm.quTmp);
        IdVal := -IdVal;
        ExecSQL('update '+TableName+' set '+Id+'='+IntToStr(IdVal)+' where '+Id+'='+DataSet.FieldByName(ID).AsString, quTmpMC);
        Statement := 'insert into '+TableName+'('+Id;
        s := 'values('+IntToStr(abs(IdVal));
        for i:=0 to Pred(DataSet.FieldsCount) do
        begin
          if(DataSet.Fields[i].FieldName = Id)or
            (DataSet.Fields[i].FieldName = 'INS_')or
            (DataSet.Fields[i].FieldName = 'UPD_')then continue;
          Statement := Statement+','+DataSet.Fields[i].FieldName;
          case DataSet.Fields[i].DataType of
            ftString, ftWideString : s:=s+','+fif(DataSet.Fields[i].IsNull, 'null', #39+DataSet.Fields[i].AsString+#39);
            ftFloat, ftCurrency : s:=s+','+fif(DataSet.Fields[i].IsNull, 'null', StringReplace(DataSet.Fields[i].AsString,',','.',[]));
            ftDateTime, ftDate : s := s+','+fif(DataSet.Fields[i].IsNull, 'null', #39+DataSet.Fields[i].AsString+#39);
            else s:=s+','+fif(DataSet.Fields[i].IsNull, 'null', DataSet.Fields[i].AsString);
          end;
        end;
        Statement := Statement+')'+s+')';
        Descr := TDescr.Create;
        Descr.Descr := '���������� '+DataSet.FieldByName(Name).AsString;
        SQLScript.AddObject(Statement, Descr);
      end;
      if(Upd_)then
      begin
        if (IdVal=0)then IdVal := DataSet.FieldByName(ID).AsInteger;
        Statement := 'update '+TableName+' set ';
        for i:=0 to Pred(DataSet.FieldsCount) do
        begin
          if(DataSet.Fields[i].FieldName = Id)or
            (DataSet.Fields[i].FieldName = 'INS_')or
            (DataSet.Fields[i].FieldName = 'UPD_')then continue;
          Statement := Statement+' '+ DataSet.Fields[i].FieldName+'=';
          case DataSet.Fields[i].DataType of
            ftString, ftWideString :
              Statement := Statement+fif(DataSet.Fields[i].IsNull, 'null', #39+DataSet.Fields[i].AsString+#39)+',';
            ftDateTime, ftDate :
              Statement := Statement+fif(DataSet.Fields[i].IsNull, 'null', #39+DataSet.Fields[i].AsString+#39)+',';
            ftFloat, ftCurrency :
              Statement := Statement+fif(DataSet.Fields[i].IsNull, 'null', StringReplace(DataSet.Fields[i].AsString,',','.',[]))+',';
            else
              Statement := Statement+fif(DataSet.Fields[i].IsNull, 'null', DataSet.Fields[i].AsString)+',';
          end;
        end;
        Statement := copy(Statement, 1, Length(Statement)-1)+' where '+Id+'='+IntToStr(abs(IdVal));
        Descr := TDescr.Create;
        Descr.Descr := '���������� '+DataSet.FieldByName(Name).AsString;
        SQLScript.AddObject(Statement, Descr);
      end;
      DataSet.Next;
    end;
    CloseDataSet(DataSet);
  end;


procedure ImportComp(DataSet : TpFIBDataSet; TableName, Id, Exc, Gen, Name: string);
  var
    Ins_, Upd_ : boolean;
    Statement, s : string;
    i, IdVal : integer;
    Descr : TDescr;
    CompanyID: Integer;
    Value: Variant;
    inn: string;

function Exists(CompanyID: Integer): Boolean;
var
   Value: Variant;
begin
  Value := ExecSelectSQL('select COMPID from d_comp where compid = ' + IntToStr(CompanyID), dm.quTmp);
  Result := not VarIsNull(Value);
end;

function MCExists(CompanyID: Integer): Boolean;
var
   Value: Variant;
begin
  Value := ExecSelectSQL('select COMPID from d_comp where compid = ' + IntToStr(CompanyID), quTmpMC);
  Result := not VarIsNull(Value);
end;


function GetCompanyByINN(INN: string): Integer;
var
   Value: Variant;
begin
  Value := ExecSelectSQL('select COMPID from d_comp where inn = ' + '''' + Inn + '''', dm.quTmp);
  if VarIsNull(Value) then Result := 0
  else Result := Value;
end;

begin
    OpenDataSet(DataSet);
    while not DataSet.Eof do
    begin

      Ins_ := DataSet.FieldByName('INS_').AsInteger=1;
      Upd_ := DataSet.FieldByName('UPD_').AsInteger=1;

      if Ins_ or Upd_ then
      begin
        if Ins_ then
        begin
          Upd_ := False;

          Inn := Trim(DataSet.FieldByName('INN').AsString);

          if Inn <> '' then
          begin
            CompanyID := GetCompanyByINN(inn);
            if CompanyID <> 0 then
            begin
              Ins_ := False;
              Upd_ := True;
            end;
          end
          else
          begin
            CompanyID := 0;
          end;
        end
        else
        begin
          Ins_ := False;
          CompanyID := DataSet.FieldByName(ID).AsInteger;
          if not Exists(CompanyID) then
          begin
            CompanyID := 0;
            Ins_ := True;
            Upd_ := False;
          end;
        end;
      end
      else
      begin
        CompanyID := DataSet.FieldByName(ID).AsInteger;
        if not Exists(CompanyID) then
        begin

          Inn := Trim(DataSet.FieldByName('INN').AsString);

          if Inn <> '' then
          begin
            CompanyID := GetCompanyByINN(inn);
            if CompanyID <> 0 then
            begin
              Ins_ := False;
              Upd_ := True;
            end
            else
            begin
              CompanyID := 0;
              Ins_ := True;
              Upd_ := False;
            end;
          end
          else
          begin
            CompanyID := 0;
            Ins_ := True;
            Upd_ := False;
          end;
        end;
      end;

      if Ins_ or Upd_ then
      begin
        IdVal := 0;

        if(Ins_)then
        begin
          IdVal := ExecSelectSQL('select gen_id('+Gen+', 1) from rdb$database', dm.quTmp);
          IdVal := -IdVal;
          ExecSQL('update '+TableName+' set '+Id+'='+IntToStr(IdVal)+' where '+Id+'='+DataSet.FieldByName(ID).AsString, quTmpMC);
          Statement := 'insert into '+TableName+'('+Id;
          s := 'values('+IntToStr(abs(IdVal));
          for i:=0 to Pred(DataSet.FieldsCount) do
          begin
            if(DataSet.Fields[i].FieldName = Id)or
              (DataSet.Fields[i].FieldName = 'INS_')or
              (DataSet.Fields[i].FieldName = 'UPD_')then continue;
            Statement := Statement+','+DataSet.Fields[i].FieldName;
            case DataSet.Fields[i].DataType of
              ftString, ftWideString : s:=s+','+fif(DataSet.Fields[i].IsNull, 'null', #39+DataSet.Fields[i].AsString+#39);
              ftFloat, ftCurrency : s:=s+','+fif(DataSet.Fields[i].IsNull, 'null', StringReplace(DataSet.Fields[i].AsString,',','.',[]));
              ftDateTime, ftDate : s := s+','+fif(DataSet.Fields[i].IsNull, 'null', #39+DataSet.Fields[i].AsString+#39);
              else s:=s+','+fif(DataSet.Fields[i].IsNull, 'null', DataSet.Fields[i].AsString);
            end;
          end;
          Statement := Statement+')'+s+')';
          Descr := TDescr.Create;
          Descr.Descr := '���������� '+DataSet.FieldByName(Name).AsString;
          SQLScript.AddObject(Statement, Descr);
        end
        else
        if(Upd_)then
        begin
          IdVal := CompanyID;

          if DataSet.FieldByName(ID).AsInteger <> CompanyID then
          begin
            if not MCExists(IdVal) then
            begin
              ExecSQL('insert into d_comp (compid, inn) values (' + IntToStr(IdVal) + ', ' + '''' + '#' + IntToStr(idVal) + '''' + ')' , dm.quTmp);
              dm.quTmp.transaction.commitretaining;///!!!!!!!!!!!!!!!!!!!
            end;
            ExecSQL('update cash set compid = '+IntToStr(IdVal)+' where compid = ' + DataSet.FieldByName(ID).AsString, quTmpMC);
            ExecSQL('update inv set supid = '+IntToStr(IdVal)+' where supid= '+DataSet.FieldByName(ID).AsString, quTmpMC);
            quTmpMC.Transaction.CommitRetaining; //!!!!!!!!!!!!!!!!!!!!!!!!!

          end;

          Statement := 'update '+TableName+' set ';
          for i:=0 to Pred(DataSet.FieldsCount) do
          begin
            if(DataSet.Fields[i].FieldName = Id)or
              (DataSet.Fields[i].FieldName = 'INS_')or
              (DataSet.Fields[i].FieldName = 'UPD_')then continue;
            Statement := Statement+' '+ DataSet.Fields[i].FieldName+'=';
            case DataSet.Fields[i].DataType of
              ftString, ftWideString :
                Statement := Statement+fif(DataSet.Fields[i].IsNull, 'null', #39+DataSet.Fields[i].AsString+#39)+',';
              ftDateTime, ftDate :
                Statement := Statement+fif(DataSet.Fields[i].IsNull, 'null', #39+DataSet.Fields[i].AsString+#39)+',';
              ftFloat, ftCurrency :
                Statement := Statement+fif(DataSet.Fields[i].IsNull, 'null', StringReplace(DataSet.Fields[i].AsString,',','.',[]))+',';
              else
                Statement := Statement+fif(DataSet.Fields[i].IsNull, 'null', DataSet.Fields[i].AsString)+',';
            end;
          end;
          Statement := copy(Statement, 1, Length(Statement)-1)+' where '+Id+'='+IntToStr(abs(IdVal));
          Descr := TDescr.Create;
          Descr.Descr := '���������� '+DataSet.FieldByName(Name).AsString;
          SQLScript.AddObject(Statement, Descr);
        end;
      end;
      DataSet.Next;
    end;
    CloseDataSet(DataSet);
  end;


  procedure _CopyFile(f1, f2 : string; Overwrite: boolean);
  begin
    //SetProgress('���������� ��...');
    try
      if not CopyFile(PChar(f1), PChar(f2), Overwrite) then
      begin
        MessageDialog('������ ��� ����������� �� #'+IntToStr(GetLastError), mtError, [mbOk], 0);
        SysUtils.Abort;
      end
    finally
      //UnProgress;
    end;
  end;

const
  cArchiveDir = 'Backup';
var
  RootInvId, RootInvNo, t : Variant;
  Msg: string;
  i, InvId, DocNo, MinDepId : integer;
  ArchiveName, BaseDir, Statement, CopyDbName, DbName : string;
  y,d,m,h,min,s,ms : word;
  Descr : TDescr;
  i1, i2: Cardinal;
  i3: string;
begin
  dbName := dm.db.DatabaseName;
  {if not FileExists(dbName) then
    raise Exception.Create('���������� �������� ������ ��� ��������� ���� ������');}

  // �������� �����
  //SetProgress('�������� �������� ����� �� ��������� ������...');
  {
  try
    BaseDir := ExtractFileDir(dbMC.DatabaseName)+'\'+cArchiveDir;
    if not ForceDirectories(BaseDir) then
      raise Exception.Create('�� ������� ������� ���������� '+BaseDir);
    ArchiveName := BaseDir+'\'+ExtractFileName(dbMC.DatabaseName);
    CopyFile(PChar(dbMC.DatabaseName), PChar(ArchiveName), False);
    DecodeDate(Now, y,m,d);
    DecodeTime(Now, h,min,s,ms);
    ExecuteAndWait('rar a -ep -m3 "'+
      ChangeFileExt(ArchiveName, '')+Format('%.2d%.2d%.4d', [d,m,y])+'-'+Format('%.2d%.2d%.2d', [h,min,s])+'.rar'+'" '+
      '"'+ArchiveName+'"', False);
    DeleteFile(ArchiveName);
  finally
    //UnProgress;
  end;
  }

  { TODO : ����������. ����������� � �� ����� ���� ������ ��������� }
  // �������� ������� ��
  {CopyDbName := BaseDir+'\'+ExtractFileName(DbName);
  if FileExists(CopyDbName) then
  begin
    if MessageDlg('�������� ����� ��?', mtConfirmation, [mbYes, mbNo], 0)=mrYes then
      _copyfile(DbName, CopyDbName, False)
  end
  else _copyfile(DbName, CopyDbName, False);}

  // �������� id ���������, ����� ������� ����� ����� ��� ����������� ���������
  {RootInvId := ExecSelectSQL('select InvId from D_Rec', quTmpMC);
  if VarIsNull(RootInvId) then
    raise EWarning.Create('Db Logic Error. �� ����� ����� ���������');
  RootInvNo := ExecSelectSQL('select InvNo from D_Rec', quTmpMC);
  t := ExecSelectSQL('select DocNo from Inv where InvId='+VarToStr(RootInvId), dm.quTmp);
  if VarIsNull(t) or (t <> RootInvNo) then
    raise EWarning.Create('Db Logic Error. �� ������������ ������ ���������');}

  SQLScript := TStringList.Create;
  try
    // ������������� �����������
    stbrStatus.SimpleText := '����������� ������ �������� ������ �� ���������� �������';
    Self.Update;
    //ImportComp(taComp, 'D_COMP', 'COMPID', '', 'GEN_COMP_ID', 'NAME');//������ ����������� �����������');
    ImportComp(taComp, 'D_COMP', 'COMPID', '', 'GEN_COMP_ID', 'NAME');//������ ����������� �����������');
    taComp.Transaction.CommitRetaining;
    //ImportDict(taClient, 'D_CLIENT', 'CLIENTID', 'PK_D_CLIENT', 'GEN_CLIENT_ID', 'FNAME');// '������ ����������� ��������� �����������');
    ImportDict(taClient, 'D_CLIENT', 'CLIENTID', 'PK_D_CLIENT', 'GEN_CLIENT_ID', 'FNAME');// '������ ����������� ��������� �����������');
    taClient.Transaction.CommitRetaining;

    // ������� ��������� ��������� � ������� ����� ���������
    ReOpenDataSet(taInvFromMC);
    while not taInvFromMC.Eof do
    begin
      t := ExecSelectSQL('select T from Check_State_InvMC('+taInvFromMCINVID.AsString+')', dm.quTmp);
      case t of
        1 : Msg := '��������� �� ��� ���������� �������';
        2 : Msg := '��������� �� ��������� � ������� ��������� ������';
        3 : Msg := '��������� ������� ���';
        4 : Msg := '�� ������ ��� ���������';
      end;
      if(t<>0)then
        if MessageDialog(Msg+#13'����������?', mtInformation, [mbYes, mbNo], 0)=mrYes then
        begin
          taInvFromMC.Next;
          continue;
        end
        else begin
          CloseDataSet(taInvFromMC);
          eXit;
        end;
      Descr := TDescr.Create;
      Descr.Descr := '�������� ��������� ����������� � ������� ��������� ������ ID='+taInvFromMCINVID.AsString;
      SQLScript.AddObject('update Inv set MC=setbit(MC, 1, 0), isClose=0 where InvId='+taInvFromMCINVID.AsString, Descr);
      SQLScript.AddObject('delete from Inv where InvId='+taInvFromMCINVID.AsString, Descr);
      taInvFromMC.Next;
    end;
    CloseDataSet(taInvFromMC);
    // ���������� ��������� ��������� �� ���������� ������� � �������� �������
    ReOpenDataSet(taInvList);
    taInvList.First;
    while not taInvList.Eof do
    begin
      if taInvListSUPID.IsNull then
        if MessageDialog('�� ����� ����������. ��������� �� ����� �������������!', mtInformation, [mbOk], 0)=mrOk then
        begin
          taInvList.Next;
          continue;
        end;
      InvId := dm.GetId(12);
      DocNo := dm.GetId(11);
      Statement := 'insert into INV(INVID, DOCNO, MCNO, DOCDATE, ITYPE, DEPID, USERID, SUPID,'+
        'ISCLOSE, Q, W, NDSID, PAYTYPEID, AKCIZ, COST, ISBRACK, MEMO, COMMISION, UPPRICE, '+
        'TRANS_PRICE,TRANS_PERCENT,MC, CREATED)'+
        'values('+IntToStr(InvId)+','+taInvListDOCNO.AsString+','+taInvListDOCNO.AsString+','+
         #39+DateTimeToStr(taInvListDOCDATE.AsDateTime)+#39','+taInvListITYPE.AsString+','+
         taInvListDEPID.AsString+','+IntToStr(dm.User.UserId)+','+
         fif(taInvListSUPID.IsNull, 'null', taInvListSUPID.AsString)+','+
         '0,0,0,'+fif( taInvListNDSID.IsNull, 'null', #39+taInvListNDSID.AsString+#39)+','+
         fif(taInvListPAYTYPEID.IsNull, 'null', #39+taInvListPAYTYPEID.AsString+#39)+','+
         StringReplace(taInvListAKCIZ.AsString,',','.',[])+','+
         StringReplace(taInvListCOST.AsString,',','.',[])+','+
         fif(taInvListISBRACK.IsNull, 'null', taInvListISBRACK.AsString)+', null,'+
         taInvListCOMMISION.AsString+','+
         StringReplace(taInvListUPPRICE.AsString,',','.',[])+','+
         StringReplace(taInvListTRANS_PRICE.AsString,',','.',[])+','+
// !! - ����������
//         StringReplace(taInvListTRANS_PERCENT.AsString,',','.',[])+', 5)';
         StringReplace(taInvListTRANS_PERCENT.AsString,',','.',[])+', 4,'#39 + DateTimeToStr(taInvListSSFDT.AsDateTime) + #39')';
      Descr := TDescr.Create;
      Descr.Descr := '������ ��������� �'+taInvListDOCNO.AsString+' �� '+DateTimeToStr(taInvListDOCDATE.AsDateTime);
      SQLScript.AddObject(Statement, Descr);

      // �������� ������� � ���������
      ReOpenDataSet(taInvUID);
      while not taInvUID.Eof do
      begin
        if (taInvListSHIFT.AsInteger = 0)then
          SQLScript.AddObject('execute procedure INSERT_UID2INV('+taInvUIDUID.AsString+','+IntToStr(InvId)+','+
            StringReplace(taInvUIDPRICE2.AsString,',','.',[])+','+taInvUIDNDSID.AsString+')', Descr)
        else
          SQLScript.AddObject('execute procedure INSERT_UID2INV2('+taInvUIDUID.AsString+','+IntToStr(InvId)+','+
            StringReplace(taInvUIDPRICE2.AsString,',','.',[])+','+taInvUIDNDSID.AsString+','+
            fif(taInvUIDCHECK_NO.IsNull, 'null', #39+taInvUIDCHECK_NO.AsString+#39)+','+
            fif(taInvUIDCLIENTID.IsNull, 'null', taInvUIDCLIENTID.AsString)+')', Descr);
        taInvUID.Next;
      end;
      CloseDataSet(taInvUID);
      taInvList.Next;
      Application.ProcessMessages;
    end;

    // ��������� ������ �����
    ReOpenDataSet(taCash);
    while not taCash.Eof do
    begin
      Statement := 'insert into Cash(ID,COMPID,COST,NDS,OPERDATE,NDOC,T)'+
        ' values(gen_id(gen_cash_id,1),'+taCashCOMPID.AsString+','+
        StringReplace(taCashCOST.AsString,',','.',[])+','+
        StringReplace(taCashNDS.AsString,',','.',[])+','+#39+
        taCashOPERDATE.AsString+#39','#39+taCashNDOC.AsString+#39','+taCashT.AsString+')';
      Descr := TDescr.Create;
      Descr.Descr := '������ �������� ��������';
      SQLScript.AddObject(Statement, Descr);
      taCash.Next;
    end;
    CloseDataSet(taCash);
    // ���� ���������� ����, ������� ��������� ��������
    if chbxCreateRest.Checked then
    begin
      ReOpenDataSets([taWh, taRec, dm.taRec]);
      MinDepId := ExecSelectSQL('select min(DepId) from D_Dep where bit(DepTypes, 2)=1', dm.quTmp);
      InvId := dm.GetId(12);
      DocNo := dm.GetId(11);
      Statement := 'insert into Inv(INVID,DOCNO,DOCDATE,ITYPE,DEPID,FROMDEPID,SUPID,'+
        'USERID, ISCLOSE, NDSID,COMMISION,UPPRICE,PAYTYPEID, MC)'+
        'values ('+IntToStr(InvId)+','+IntToStr(DocNo)+','#39'NOW'#39','+
        '2,'+fif(taRecDEPID.IsNull, IntToStr(MinDepId), taRecDEPID.AsString)+','+
        'null,null,'+IntToStr(dm.User.UserId)+','+
        '0,null,0,0,null,0)';
      Descr := TDescr.Create;
      Descr.Descr := '��������� ��������� ��������';
      SQLScript.AddObject(Statement, Descr);
      while not taWh.Eof do
      begin
        if not(ExecSelectSQL('select si.uid from inv i, sel s, sitem si where i.invid = :ext_invid and i.invid = s.invid and s.selid = si.selid and si.uid = :UID',VarArrayOf([taWhEXT_INVID.AsINteger,taWhUID.AsInteger]), dm.quTmp)=null) then
        SQLScript.AddObject('execute procedure INSERT_UID2INV('+taWhUID.AsString+','+IntToStr(InvId)+','+
          StringReplace(taWhPRICE.AsString,',','.',[])+','+dm.taRecNDSID.AsString+')', Descr);
        taWh.Next;
      end;
      ShowMessage(IntToStr(SQLScript.Count));
      CloseDataSets([taWh, taRec]);
    end;
    dm.quTmp.Transaction.CommitRetaining;
    stbrStatus.SimpleText := '';
    Self.Update;


    // ����������� ������ ������� �� ����� ��
    {dbCopy.DBName := CopyDbName;
    dbCopy.Connected := False;
    dbCopy.Connected := True;
    for i:=0 to Pred(SQLScript.Count) do
      SQLScript[i] := SQLScript[i]+cDelim;
    SQLScript.SaveToFile('c:\script.sql');
    for i:=0 to Pred(SQLScript.Count) do
    begin
      if trCopy.Active then trCopy.Commit;
      trCopy.StartTransaction;
      quCopy.Close;
      quCopy.SQL.Text := SQLScript[i];
      try
        quCopy.ExecQuery;
        quCopy.Close;
        trCopy.Commit;
      except
        on E:Exception do begin
          dm.quTmp.Transaction.RollbackRetaining;
          MessageDialog('��� ������� ������ �������� ������.'#13+
            SQLScript[i]+#13+
            E.Message+#13+
            '���������� � �������������!', mtWarning, [mbOk], 0);
          dbCopy.Connected := False;
          eXit;
        end;
      end
    end;
    if trCopy.Active then trCopy.Commit;
    dbCopy.Connected := False;}

    // ��������� ������ �������
    if dm.tr.Active then dm.tr.Commit;
    dm.tr.StartTransaction;
    for i:=0 to Pred(SQLScript.Count) do
    begin
      dm.quTmp.Close;
      dm.quTmp.SQL.Text := SQLScript[i];
      try
        if Assigned(SQLScript.Objects[i]) then
        begin
          stbrStatus.SimpleText := TDescr(SQLScript.Objects[i]).Descr;
          Self.Update;
        end;
        dm.quTmp.ExecQuery;
        dm.quTmp.Close;
        dm.quTmp.Transaction.CommitRetaining;
      except
        on E:Exception do begin
          dm.quTmp.Transaction.RollbackRetaining;
          MessageDialog('��� ������� ������ �������� ������.'#13+
            SQLScript[i]+#13+
            E.Message+#13+
            '������ �� ���������� �� �����!'#13, mtWarning, [mbOk], 0);
        end;
      end
    end;

    //SQLScript.SaveToFile('c:\script.txt');

    dm.tr.Commit;
    stbrStatus.SimpleText := '';
    Self.Update;
    MessageDialogA('������ �������� �������', mtInformation);
    acClearDb.Execute;
  finally
    stbrStatus.SimpleText := '';
    Self.Update;
    {for i:=0 to Pred(SQLScript.Count) do
      if Assigned(SQLScript.Objects[i]) then
      begin
        SQLScript.Objects[i].Free;
        SQLScript.Objects[i] := nil;
      end;         }
    SQLScript.Free;
  end;
end;

procedure TfmImportMC.acImportUpdate(Sender: TObject);
begin
  acImport.Enabled := dbMC.Connected and taInvList.Active and (not taInvList.IsEmpty);
end;

procedure TfmImportMC.taInvUIDBeforeOpen(DataSet: TDataSet);
begin
  taInvUID.ParamByName('INVID').AsInteger := taInvListINVID.AsInteger;
end;

procedure TfmImportMC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if trMC.Active then trMC.Commit;
  dbMC.Close;
  inherited;
end;

procedure TfmImportMC.acClearDbExecute(Sender: TObject);
begin
  if MessageDialog(rsMC_ClearDb, mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  Self.Update;
  try
    CloseDataSet(taInvList);
    ExecSQL('delete from SellRet', quTmpMC);
    ExecSQL('delete from Inv', quTmpMC);
    ExecSQL('delete from Sel', quTmpMC);
    ExecSQL('delete from SItem', quTmpMC);
    ExecSQL('delete from SInfo', quTmpMC);
    ExecSQL('delete from Ins', quTmpMC);
    ExecSQL('delete from Cash', quTmpMC);
    ExecSQL('delete from D_Comp', quTmpMC);
    ExecSQL('delete from D_Client', quTmpMC);
    ExecSQL('delete from D_NDS', quTmpMC);
    ExecSQL('delete from D_PayType', quTmpMC);
    ExecSQL('delete from Ins', quTmpMC);
    ExecSQL('delete from Cash', quTmpMC);
    ExecSQL('delete from Ext_Inv', quTmpMC);
    ExecSQL('delete from D_Rec', quTmpMC);
    OpenDataSet(taInvList);
  except
    on E:Exception do MessageDialog('������ ��� ���������� �������:'#13+quTmpMC.SQL.Text+#13+
      E.Message+#13+rsCallSupport, mtError, [mbOk], 0);
  end;
end;

procedure TfmImportMC.edPathKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_RETURN : OpenMC(1);
  end;
end;

procedure TfmImportMC.acClearDbUpdate(Sender: TObject);
begin
  acClearDb.Enabled := dbMC.Connected;
end;

end.
