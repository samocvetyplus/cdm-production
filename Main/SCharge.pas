{**************************************}
{  ��� �������� ����������� ������     }
{  Copyrigth (C) 2004 basile for CDM   }
{  create 26.01.2004                   }
{  last update 26.01.2004              }
{**************************************}

unit SCharge;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus, TB2Item, ActnList,  ImgList, Grids,
  DBGridEh, DBCtrlsEh, StdCtrls, Mask, DBCtrls, ExtCtrls, 
  ComCtrls, TB2Dock, TB2Toolbar,  DBGridEhGrouping, rxPlacemnt,
  GridsEh, rxSpeedbar;

type
  TfmSCharge = class(TfmDocAncestor)
    plExt: TPanel;
    gridSemis: TDBGridEh;
    tbdcSemis: TTBDock;
    TBToolbar2: TTBToolbar;
    TBItem6: TTBItem;
    TBItem5: TTBItem;
    acAddSemis: TAction;
    acDelSemis: TAction;
    Action1: TAction;
    plExtTiitle: TPanel;
    lbMat: TLabel;
    cmbxMat: TDBComboBoxEh;
    chbxW: TDBCheckBoxEh;
    procedure spitCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cmbxMatChange(Sender: TObject);
    procedure acAddSemisExecute(Sender: TObject);
    procedure acAddSemisUpdate(Sender: TObject);
    procedure acDelSemisUpdate(Sender: TObject);
    procedure acDelSemisExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure InitBtn(T : boolean);
  protected
    procedure CloseDoc;override;
    procedure OpenDoc;override;
  end;

var
  fmSCharge: TfmSCharge;

implementation

uses MainData, DictData, dbUtil, fmUtils, DB, dbTree;

{$R *.dfm}

procedure TfmSCharge.CloseDoc;
begin
  ExecSQL('execute procedure CloseDoc("��� �������� ���", '+dmMain.taSChargeListINVID.AsString+')', dmMain.quTmp);
end;

procedure TfmSCharge.FormCreate(Sender: TObject);
begin
  dm.WorkMode := 'aSCharge';
  gridSemis.RowLines := 2;
  inherited;
  InitBtn(dmMain.taSChargeListISCLOSE.AsInteger=1);
  // ��������� �������� ��� ������ ��������������
  cmbxMat.OnChange := nil;
  cmbxMat.Items.Assign(dm.dMat);
  cmbxMat.ItemIndex := 0;
  dm.AMatId := ROOT_MAT_CODE;
  cmbxMat.OnChange := cmbxMatChange;
  // ���������� �����������
  dmMain.FilterOperId := ROOT_OPER_CODE;
  OpenDataSets([dmMain.taSChargeWH]);
  (* �������� ������� *)
  dm.DepBeginDate := dm.GetDepBeginDate(dmMain.taSChargeListDEPID.AsInteger);
  if(dm.DepBeginDate > dmMain.taSChargeListDOCDATE.AsDateTime)then
  begin
    stbrStatus.Panels[0].Text := '�������� ������ ���������� � '+DateToStr(dm.DepBeginDate);
    lbNoDoc.Enabled := False;
    edNoDoc.Enabled := False;
    lbDateDoc.Enabled := False;
    dtedDateDoc.Enabled := False;
    gridItem.ReadOnly := True;
  end;
end;

procedure TfmSCharge.InitBtn(T: boolean);
begin
  if not T then // �������� ������
  begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 6;
  end
  else begin
    spitClose.BtnCaption := '������';
    spitClose.Hint := '������� ��������';
    spitClose.ImageIndex := 5;
  end
end;

procedure TfmSCharge.OpenDoc;
begin
  ExecSQL('execute procedure OpenDoc("��� �������� ���", '+dmMain.taSChargeListINVID.AsString+')', dmMain.quTmp);
end;

procedure TfmSCharge.spitCloseClick(Sender: TObject);
begin
  PostDataSet(dmMain.taSCharge);
  if(dmMain.taSCharge.RecordCount = 0) then raise EWarning.Create('������ ������� ���������!');

  if spitClose.BtnCaption = '������' then
    try
      PostDataSet(dmMain.taSChargeList);
      CloseDoc;
      spitClose.BtnCaption := '������';
      spitClose.Hint := '������� ��������';
      spitClose.ImageIndex := 5;
      RefreshDataSet(dmMain.taSChargeLIst);
    except
      on E: Exception do { TODO : !!! }//if not TranslateIbMsg(E, dmMain.taSCharge.Database, dmMain.taSCharge.Transaction)
           //then
           ShowMessage(E.Message);
    end
  else
    try
      PostDataSet(dmMain.taSChargeList);
      OpenDoc;
      spitClose.BtnCaption := '������';
      spitClose.Hint := '������� ��������';
      spitClose.ImageIndex := 6;
      RefreshDataSet(dmMain.taSChargeList);
    except
      on E: Exception do { TODO : !!! }//if not TranslateIbMsg(E, dmMain.taSIEl.Database, dmMain.taSIEl.Transaction)
           //then
           ShowMessage(E.Message);
    end
end;

procedure TfmSCharge.cmbxMatChange(Sender: TObject);
begin
  dm.AMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taSChargeWH);
end;

procedure TfmSCharge.acAddSemisExecute(Sender: TObject);
begin
  with dmMain do
  begin
    PostDataSet(taSChargeList);
    if not _ShowEditorQW(taSChargeWHQ.AsInteger, taSChargeWHW.AsFloat, chbxW.Checked) then eXit;
    taSCharge.Insert;
    taSChargeSEMIS.AsString := taSChargeWHSEMISID.AsString;
    taSChargeOPERID.AsVariant := taSChargeWHOPERID.AsVariant;
    taSChargeQ.AsInteger := SemisQ;
    taSChargeW.AsFloat := SemisW;
    taSCharge.Post;
    taSChargeWH.Refresh;
  end;
end;

procedure TfmSCharge.acAddSemisUpdate(Sender: TObject);
begin
  acAddSemis.Enabled := DataSet.Active and dmMain.taSChargeWH.Active and
             (not dmMain.taSChargeWH.IsEmpty);
end;

procedure TfmSCharge.acDelSemisUpdate(Sender: TObject);
begin
  acDelSemis.Enabled := DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmSCharge.acDelSemisExecute(Sender: TObject);
var
  b : boolean;
  SemisId, OperName : string;
  DepId, Id, UW, UQ : integer;
  OperId : Variant;
begin
  PostDataSet(DataSet);
  with dmMain do
  begin
    b := taSChargeWH.Locate('OPERID;SEMISID;UQ;UW',
      VarArrayOf([taSChargeOPERID.AsVariant, taSChargeSEMIS.AsString, taSChargeUQ.AsInteger, taSChargeUW.AsInteger]), []);    if not b then
    begin
      OperId := taSChargeOPERID.AsVariant;
      SemisId := taSChargeSEMIS.AsString;
      UQ := taSChargeUQ.AsInteger;
      UW := taSChargeUW.AsInteger;
     end;
     SemisQ := taSChargeQ.AsInteger;
     SemisW := taSChargeW.AsFloat;
     if not _ShowEditorQW(SemisQ, SemisW, chbxW.Checked) then eXit;
     if(SemisQ = taSChargeQ.AsInteger)and(Abs(SemisW - taSChargeW.AsFloat)<Eps)then
     begin
       taSCharge.BeforeDelete := nil;
       taSCharge.Delete;
       taSCharge.BeforeDelete := dmMain.taSChargeBeforeDelete;
     end
     else begin
       taSCharge.Edit;
       taSChargeQ.AsInteger := taSChargeQ.AsInteger - SemisQ;
       taSChargeW.AsFloat := taSChargeW.AsFloat - SemisW;
       taSCharge.Post;
     end;
     if not b then
     begin
       taSChargeWH.Append;
       taSChargeWHDEPID.AsInteger := taSChargeListDEPID.AsInteger;
       taSChargeWHOPERID.AsVariant := OperId;
       taSChargeWHSEMISID.AsString := SemisId;
       taSChargeWHUQ.AsInteger := UQ;
       taSChargeWHUW.AsInteger := UW;
       taSChargeWHQ.AsInteger := 0;
       taSChargeWHW.AsInteger := 0;
       taSChargeWH.Post;
     end;
     taSChargeWH.Refresh;
  end;
end;

procedure TfmSCharge.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  CloseDataSet(dmMain.taSChargeWH);
end;

end.
