{**************************************}
{  ��������� �������� ���� � �����     }
{  Copyrigth (C) 2003-2004 basile      }
{  ��� ��������� �������� � ���������� }
{  dmMain.AffinajMode.                 }
{     amS - ��������� �������� �����   }
{     amL - ��������� �������� ����    }
{  created 01.10.2003                  }
{  last update 20.07.2004              }
{  v0.19                               }
{**************************************}

unit Affinaj;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DocAncestor, Menus, TB2Item,  ImgList, Grids, DBGridEh,
  DBCtrlsEh, StdCtrls, Mask, DBCtrls, ExtCtrls,  ComCtrls,
  DBLookupEh, TB2Dock, TB2Toolbar, ActnList,  DB,
  pFIBDataSet, Buttons, FIBDataSet, DBGridEhGrouping, rxPlacemnt, GridsEh,
  rxSpeedbar;

type
  TfmAffinaj = class(TfmDocAncestor)
    plWH: TPanel;
    Label1: TLabel;
    lcbxDep: TDBLookupComboboxEh;
    lcbxSup: TDBLookupComboboxEh;
    gridWh: TDBGridEh;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    acEvent: TActionList;
    acRet: TAction;
    acGet: TAction;
    TBSeparatorItem1: TTBSeparatorItem;
    tbdcSemis: TTBDock;
    TBToolbar2: TTBToolbar;
    TBItem3: TTBItem;
    TBItem4: TTBItem;
    acAdd: TAction;
    acDel: TAction;
    taDep: TpFIBDataSet;
    dsrDep: TDataSource;
    taDepDEPID: TIntegerField;
    taDepNAME: TFIBStringField;
    Label2: TLabel;
    Splitter1: TSplitter;
    taComp: TpFIBDataSet;
    dsrComp: TDataSource;
    acPrintInv: TAction;
    TBItem5: TTBItem;
    acActivateWh: TAction;
    acCheckDep: TAction;
    SpeedButton1: TSpeedButton;
    plFilter: TPanel;
    cmbxMat: TDBComboBoxEh;
    Label7: TLabel;
    cmbxOper: TDBComboBoxEh;
    lbMat: TLabel;
    acPrintOrder: TAction;
    cbForeignGold: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acGetExecute(Sender: TObject);
    procedure acRetExecute(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure gridWhDblClick(Sender: TObject);
    procedure gridItemGetCellParams(Sender: TObject; Column: TColumnEh; AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acDelExecute(Sender: TObject);
    procedure gridWhKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acPrintInvExecute(Sender: TObject);
    procedure acPrintInvUpdate(Sender: TObject);
    procedure acActivateWhExecute(Sender: TObject);
    procedure acCheckDepUpdate(Sender: TObject);
    procedure acCheckDepExecute(Sender: TObject);
    procedure lcbxDepUpdateData(Sender: TObject; var Handled: Boolean);
    procedure cmbxMatChange(Sender: TObject);
    procedure cmbxOperChange(Sender: TObject);
    procedure acPrintOrderExecute(Sender: TObject);
  private
    FRadOnly: boolean;
    FisModifyDataSet : boolean;
    procedure Access;
    procedure SetReadOnly(ReadOnly: Boolean);
  end;

var
  fmAffinaj: TfmAffinaj;

implementation

uses MainData, DictData, dbUtil, eSQ, Editor, MsgDialog, PrintData, eDSemis,
     dbTree, ProductionConsts;

{$R *.dfm}


procedure TfmAffinaj.Access;
var
  OurDepartment: boolean;
begin

  if dm.IsAdm then
  begin

    Exit;

  end;

  OurDepartment := dm.User.wWhId = dmMain.taAFListDEPID.AsInteger;

  if ((dm.User.Profile = 1) and OurDepartment) then
  begin

    SetReadOnly(False);

  end else
  begin

    SetReadOnly(True);

  end;

end;

procedure TfmAffinaj.SetReadOnly(ReadOnly: Boolean);
begin

  plWH.Visible := not ReadOnly;

  tbdcSemis.Visible := not ReadOnly;

  edNoDoc.Enabled := not ReadOnly;

  dtedDateDoc.Enabled := not Readonly;

  lcbxSup.Enabled := not ReadOnly;

  cbForeignGold.Enabled := not ReadOnly;

  lcbxDep.Enabled := not ReadOnly;

  gridItem.ReadOnly := ReadOnly;

  spitClose.Visible := not ReadOnly;

  FReadOnly := ReadOnly;

end;

procedure TfmAffinaj.FormCreate(Sender: TObject);
begin
  dm.WorkMode := 'Affinaj';
  edNoDoc.DataSource := dmMain.dsrAFList;
  dtedDateDoc.DataSource := dmMain.dsrAFList;
  txtDep.DataSource := dmMain.dsrAFList;
  FisModifyDataSet := True;
  case dmMain.AffinajMode of
    amS : begin
      FDocName := '����������� ����� ������';
      Caption := '��������� ����������� �����';
      gridWh.FieldColumns['SEMISNAME'].Title.Caption := '����';
      gridWh.FieldColumns['Q'].Visible := False;
      gridWh.FieldColumns['W'].Visible := True;
      gridWh.FieldColumns['OPERNAME'].Visible := False;
      gridWh.FieldColumns['DEPNAME'].Visible := True;
      dmMain.WhAffinajMode := 0;
      plFilter.Visible := False;
    end;
    amL : begin
      FDocName := '����������� ����';
      Caption := '��������� ����������� ����';
      gridWh.FieldColumns['SEMISNAME'].Title.Caption := '������������';
      gridWh.FieldColumns['Q'].Visible := True;
      gridWh.FieldColumns['W'].Visible := True;
      gridWh.FieldColumns['OPERNAME'].Visible := True;
      gridWh.FieldColumns['DEPNAME'].Visible := False;
      dmMain.WhAffinajMode := 1;
      plFilter.Visible := True;
      // ��������� �������� ��� ������ ��������������
      cmbxMat.OnChange := nil;
      cmbxMat.Items.Assign(dm.dMat);
      cmbxMat.ItemIndex := 0;
      dm.AMatId := ROOT_MAT_CODE;
      cmbxMat.OnChange := cmbxMatChange;

      cmbxOper.OnChange := nil;
      cmbxOper.Items.Assign(dm.dOper);
      cmbxOper.Items.Insert(1, sNoOperation);
      cmbxOper.ItemIndex := 0;
      dmMain.FilterOperId := ROOT_OPER_CODE;
      cmbxOper.OnChange := cmbxOperChange;
    end;
  end;
  OpenDataSets([dmMain.taWhAffinaj, taComp, taDep, dm.taSemisAff]);
  inherited;
  //ppPrint.Skin := dm.ppSkin;
  InitBtn(dmMain.taAFListISCLOSE.AsInteger = 1);
  Access;
end;

procedure TfmAffinaj.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  CloseDataSets([dmMain.taWhAffinaj, taComp, taDep, dm.taSemisAff]);
end;

procedure TfmAffinaj.acGetExecute(Sender: TObject);
begin
  if acGet.Checked then eXit;
  acGet.Checked := True;
  case dmMain.AffinajMode of
    amS : begin
      dmMain.WhAffinajMode := 0;
      gridWh.FieldColumns['SEMISNAME'].Title.Caption := '����';
      gridWh.FieldColumns['Q'].Visible := False;
      gridWh.FieldColumns['W'].Visible := True;
      gridWh.FieldColumns['OPERNAME'].Visible := False;
      gridWh.FieldColumns['DEPNAME'].Visible := True;
    end;
    amL : begin
      dmMain.WhAffinajMode := 1;
      gridWh.FieldColumns['SEMISNAME'].Title.Caption := '������������';
      gridWh.FieldColumns['Q'].Visible := True;
      gridWh.FieldColumns['W'].Visible := True;
      gridWh.FieldColumns['OPERNAME'].Visible := True;
      gridWh.FieldColumns['DEPNAME'].Visible := False;
    end;
    else raise EInternal.Create(Format(rsInternalError, ['402']));
  end; // case
  ReOpenDataSet(dmMain.taWhAffinaj);
end;

procedure TfmAffinaj.acRetExecute(Sender: TObject);
begin
  if acRet.Checked then eXit;
  acRet.Checked := True;
  dmMain.WhAffinajMode := 2;
  gridWh.FieldColumns['SEMISNAME'].Title.Caption := '������������';
  gridWh.FieldColumns['OPERNAME'].Visible := False;
  gridWh.FieldColumns['Q'].Visible := False; 
  gridWh.FieldColumns['W'].Visible := False;
  gridWh.FieldColumns['DEPNAME'].Visible := False;
  ReOpenDataSet(dmMain.taWhAffinaj);
end;

procedure TfmAffinaj.acAddExecute(Sender: TObject);
begin
  PostDataSet(dmMain.taAFList);
  case dmMain.WhAffinajMode of
    0 : begin
      dmMain.SemisQ := dmMain.taWhAffinajQ.AsInteger;
      dmMain.SemisW := dmMain.taWhAffinajW.AsFloat;
      if(ShowEditor(TfmeSQ, TfmEditor(fmeSQ))<>mrOk) then eXit;
    end;
    1 : begin
      dmMain.SemisQ := dmMain.taWhAffinajQ.AsInteger;
      dmMain.SemisW := dmMain.taWhAffinajW.AsFloat;
      if(ShowEditor(TfmeSQ, TfmEditor(fmeSQ))<>mrOk) then eXit;
    end;
    2 : begin
      dmMain.SemisQ := 0;
      dmMain.SemisW := 0;
      //if(dmMain.taWhAffinajGOOD.AsInteger = 1) then
      eDSemis.CanChangeOperation := True;
      if ShowDSemisEditor(sNoOperation, dmMain.taWhAffinajGOOD.AsInteger = 1) <> mrOk then eXit;
    end;
  end;

  with dmMain do
  begin
    taAffinaj.Insert;
    taAffinajSEMISID.AsString := taWhAffinajSEMISID.AsString;
    case AffinajMode of
      amS :
        case WhAffinajMode of
          0 : begin
            taAffinajT.AsInteger := 4;
            taAffinajQ0.AsInteger := taAffinajQ.AsInteger;
            taAffinajW0.AsFloat := taAffinajW.AsFloat;
            taAffinajDEPID.AsVariant := taAFListDEPID.AsVariant;
            taAffinajOPERID.AsVariant := taWhAffinajOPERID.AsVariant;
            taAffinajCOVERDEPID.AsVariant := taWhAffinajDEPID.AsVariant;
          end;
          2 : begin
            taAffinajT.AsInteger := 5;
            taAffinajQ0.AsVariant := Null;
            taAffinajW0.AsVariant := Null;
            taAffinajDEPID.AsInteger := taAFListDEPID.AsInteger;
            if DOperId = sNoOperation then taAffinajOPERID.AsVariant := Null
            else taAffinajOPERID.AsVariant := DOperId;
            taAffinajCOVERDEPID.AsVariant := Null;
          end;
        end;
      amL :
        case WhAffinajMode of
          1 : begin
            taAffinajT.AsInteger := 6;
            taAffinajQ0.AsVariant := taAffinajQ.AsInteger;
            taAffinajW0.AsVariant := taAffinajW.AsFloat;
            taAffinajDEPID.AsInteger := taAFListDEPID.AsInteger;
            taAffinajOPERID.AsVariant := taWhAffinajOPERID.AsVariant;
            taAffinajCOVERDEPID.AsVariant := Null;
            taAffinajCOVERDEPNAME.AsVariant := Null;
          end;
          2 : begin
            taAffinajT.AsInteger := 7;
            taAffinajQ0.AsVariant := Null;
            taAffinajW0.AsVariant := Null;
            taAffinajDEPID.AsInteger := taAFListDEPID.AsInteger;
            if DOperId = sNoOperation then taAffinajOPERID.AsVariant := Null
            else taAffinajOPERID.AsVariant := DOperId;
            taAffinajCOVERDEPID.AsVariant := Null;
            taAffinajCOVERDEPNAME.AsVariant := Null;
          end;
        end;
    end;
    taAffinaj.Post;
    taWhAffinaj.Refresh;
    gridItem.SelectedField := dmMain.taAffinajSEMISAFFNAME;
    gridItem.EditorMode := True;
    FisModifyDataSet := True;
  end;
end;

procedure TfmAffinaj.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := (not dmMain.taWhAffinaj.IsEmpty) and (not FReadOnly);
end;

procedure TfmAffinaj.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := (not dmMain.taAffinaj.IsEmpty) and (not FReadOnly);
end;

procedure TfmAffinaj.gridWhDblClick(Sender: TObject);
begin
  acAdd.Execute;
end;

procedure TfmAffinaj.gridItemGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(Column.FieldName='T')then
    case dmMain.taAffinajT.AsInteger of
      4,6 : BackGround := clInfoBk;
      5,7 : Background := cl3DLight;
    end;
end;

procedure TfmAffinaj.acDelExecute(Sender: TObject);
var
  b : boolean;
  SemisId, OperName, CoverDepName : string;
  Id, UW, UQ : integer;
  OperId, CoverDepId : Variant;
begin
  case dmMain.AffinajMode of
    amS : with dmMain do begin
       if(taAffinajT.AsInteger = 4)and(dmMain.WhAffinajMode = 2) then acGet.Execute
       else if(taAffinajT.AsInteger = 5)and(dmMain.WhAffinajMode = 0)then acRet.Execute;
       b := taWhAffinaj.Locate('SEMISID;DEPID', VarArrayOf([taAffinajSEMISID.AsString, taAffinajCOVERDEPID.AsVariant]), []);
       if not b then
       begin
         SemisId := taAffinajSEMISID.AsString;
         CoverDepId := taAffinajCOVERDEPID.AsVariant;
         CoverDepName := taAffinajCOVERDEPNAME.AsString;
         UW := taAffinajUW.AsInteger;
         UQ := taAffinajUQ.AsInteger;
       end;
       SemisQ := taAffinajQ.AsInteger;
       SemisW := taAffinajW.AsFloat;
       if(ShowEditor(TfmeSQ, TfmEditor(fmeSQ))<>mrOk)then eXit;
       if(SemisQ = taAffinajQ.AsInteger)and(Abs(SemisW - taAffinajW.AsFloat)<Eps)then
       begin
         taAffinaj.BeforeDelete := nil;
         taAffinaj.Delete;
         taAffinaj.BeforeDelete := dmMain.taAffinajBeforeDelete;
       end
       else begin
          taAffinaj.Edit;
          taAffinajQ.AsInteger := taAffinajQ.AsInteger - SemisQ;
          taAffinajW.AsFloat := taAffinajW.AsFloat - SemisW;
          taAffinaj.Post;
       end;
       if not b then
       begin
         taWhAffinaj.Append;
         taWhAffinajSEMISID.AsString := SemisId;
         taWhAffinajDEPID.AsVariant := CoverDepId;
         taWhAffinajDEPNAME.AsString := CoverDepName;
         taWhAffinajT.AsInteger := WhAffinajMode;
         taWhAffinajOPERID.AsVariant := Null;
         taWhAffinajOPERNAME.AsString := sNoOperation;
         taWhAffinajUQ.AsInteger := UQ;
         taWhAffinajUW.AsInteger := UW;
         taWhAffinajQ.AsInteger := 0;
         taWhAffinajW.AsInteger := 0;
         taWhAffinaj.Post;
       end;
       taWhAffinaj.Refresh;
    end;
    amL : with dmMain do begin
       if(taAffinajT.AsInteger = 6)and(dmMain.WhAffinajMode = 2) then acGet.Execute
       else if(taAffinajT.AsInteger = 7)and(dmMain.WhAffinajMode = 1)then acRet.Execute;
       b := taWhAffinaj.Locate('SEMISID;OPERID;UQ;UW',
          VarArrayOf([taAffinajSEMISID.AsString, taAffinajOPERID.AsVariant,
                      taAffinajUQ.AsInteger, taAffinajUW.AsInteger]), []);
       if not b then
       begin
         SemisId := taAffinajSEMISID.AsString;
         OperId := taAffinajOPERID.AsVariant;
         OperName := taAffinajOPERNAME.AsString;
         UW := taAffinajUW.AsInteger;
         UQ := taAffinajUQ.AsInteger;
       end;
       SemisQ := taAffinajQ.AsInteger;
       SemisW := taAffinajW.AsFloat;
       if(ShowEditor(TfmeSQ, TfmEditor(fmeSQ))<>mrOk)then eXit;
       if(SemisQ = taAffinajQ.AsInteger)and(Abs(SemisW - taAffinajW.AsFloat)<Eps)then
       begin
         taAffinaj.BeforeDelete := nil;
         taAffinaj.Delete;
         taAffinaj.BeforeDelete := dmMain.taAffinajBeforeDelete;
       end
       else begin
          taAffinaj.Edit;
          taAffinajQ.AsInteger := taAffinajQ.AsInteger - SemisQ;
          taAffinajW.AsFloat := taAffinajW.AsFloat - SemisW;
          taAffinaj.Post;
       end;
       if not b then
       begin
         taWhAffinaj.Append;
         taWhAffinajSEMISID.AsString := SemisId;
         taWhAffinajDEPID.AsInteger := taAFListDEPID.AsInteger;
         taWhAffinajT.AsInteger := WhAffinajMode;
         taWhAffinajOPERID.AsVariant := OperId;
         taWhAffinajOPERNAME.AsString := OperName;
         taWhAffinajUW.AsInteger := UW;
         taWhAffinajUQ.AsInteger := UQ;
         taWhAffinajQ.AsInteger := 0;
         taWhAffinajW.AsInteger := 0;
         taWhAffinaj.Post;
       end;
       taWhAffinaj.Refresh;
    end;
  end;
  FisModifyDataSet := True;
end;

procedure TfmAffinaj.gridWhKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_SPACE : acAdd.Execute;
  end;
end;

procedure TfmAffinaj.acPrintInvExecute(Sender: TObject);
begin
  PostDataSets([MasterDataSet, DataSet]);
  case dmMain.AffinajMode of
    amS : dmPrint.PrintDocumentA(dkAffS_Inv, VarArrayOf([VarArrayOf([dmMain.taAFListINVID.AsInteger]),
      VarArrayOf([dmMain.taAFListINVID.AsInteger, 0])]));
    amL : dmPrint.PrintDocumentA(dkAffL_Inv, VarArrayOf([VarArrayOf([dmMain.taAFListINVID.AsInteger]),
      VarArrayOf([dmMain.taAFListINVID.AsInteger, 0])]));
  end;
end;

procedure TfmAffinaj.acPrintInvUpdate(Sender: TObject);
begin
  acPrintInv.Enabled := not dmMain.taAffinaj.IsEmpty;
end;

procedure TfmAffinaj.acActivateWhExecute(Sender: TObject);
begin
  ActiveControl := gridWh;
end;

procedure TfmAffinaj.acCheckDepUpdate(Sender: TObject);
begin
  if FisModifyDataSet then
  begin
    case dmMain.AffinajMode of
      amS : lcbxDep.Enabled := (0=ExecSelectSQL('select count(*) from SIEl where InvId='+dmMain.taAFListINVID.AsString+' and T=5', dmMain.quTmp));
      amL : lcbxDep.Enabled := (not dmMain.taAffinaj.Active) or dmMain.taAffinaj.IsEmpty;
    end;
    FisModifyDataSet := False;
  end;
end;

procedure TfmAffinaj.acCheckDepExecute(Sender: TObject);
begin
  //
end;

procedure TfmAffinaj.lcbxDepUpdateData(Sender: TObject; var Handled: Boolean);
var
  SemisId, OperId : Variant;
  UQ, UW : smallint;
begin
  SemisId := dmMain.taWhAffinajSEMISID.AsVariant;
  dmMain.taAFListDEPID.AsInteger := lcbxDep.KeyValue;
  OperId := dmMain.taWhAffinajOPERID.AsVariant;
  UQ := dmMain.taWhAffinajUQ.AsInteger;
  UW := dmMain.taWhAffinajUW.AsInteger;
  ReOpenDataSet(dmMain.taWhAffinaj);
  dmMain.taWhAffinaj.Locate('SEMISID;OPERID;UQ;UW', VarArrayOf([SemisId, OperId, UQ, UW]), []);
end;

procedure TfmAffinaj.cmbxMatChange(Sender: TObject);
begin
  dm.AMatId := TNodeData(cmbxMat.Items.Objects[cmbxMat.ItemIndex]).Code;
  ReOpenDataSets([dmMain.taWhAffinaj]);
end;

procedure TfmAffinaj.cmbxOperChange(Sender: TObject);
begin
  with cmbxOper, Items do
    if(Items[ItemIndex] = sNoOperation) then dmMain.FilterOperId := sNoOperation
    else dmMain.FilterOperId := TNodeData(cmbxOper.Items.Objects[cmbxOper.ItemIndex]).Code;
  ReOpenDataSet(dmMain.taWHAffinaj);
end;

procedure TfmAffinaj.acPrintOrderExecute(Sender: TObject);
begin
  PostDataSets([DataSet, MasterDataSet]);
  dmPrint.PrintDocumentA(dkAff_Order, VarArrayOf([VarArrayOf([dmMain.taAFListINVID.AsInteger]),
     VarArrayOf([dmMain.taAFListINVID.AsInteger, 1])]));
end;

end.
