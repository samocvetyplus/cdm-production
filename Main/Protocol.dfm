inherited fmProtocol: TfmProtocol
  Left = 76
  Top = 161
  Caption = #1055#1088#1086#1090#1086#1082#1086#1083' '#1080#1085#1074#1077#1085#1090#1072#1088#1080#1079#1072#1094#1080#1086#1085#1086#1081' '#1082#1086#1084#1080#1089#1089#1080#1080
  ClientHeight = 492
  ClientWidth = 869
  ExplicitWidth = 877
  ExplicitHeight = 526
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 473
    Width = 869
    ExplicitTop = 473
    ExplicitWidth = 869
  end
  inherited tb1: TSpeedBar
    Width = 869
    ExplicitWidth = 869
  end
  inherited plMain: TPanel
    Width = 869
    Height = 59
    ExplicitWidth = 869
    ExplicitHeight = 59
    inherited lbDateDoc: TLabel
      Top = 28
      Width = 47
      Caption = #1055#1077#1088#1080#1086#1076' '#1089
      ExplicitTop = 28
      ExplicitWidth = 47
    end
    inherited lbDep: TLabel
      Top = 8
      Width = 80
      Caption = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      Visible = False
      ExplicitTop = 8
      ExplicitWidth = 80
    end
    inherited txtDep: TDBText
      Left = 296
      Top = 8
      ExplicitLeft = 296
      ExplicitTop = 8
    end
    object lbTo: TLabel [4]
      Left = 208
      Top = 28
      Width = 12
      Height = 13
      Caption = #1087#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    inherited edNoDoc: TDBEditEh
      DataField = 'DOCNO'
      DataSource = dmMain.dsrPList
    end
    inherited dtedDateDoc: TDBDateTimeEditEh
      DataField = 'BD'
      DataSource = dmMain.dsrPList
      Enabled = False
    end
    object DBDateEdit1: TDBDateEdit
      Left = 228
      Top = 25
      Width = 97
      Height = 21
      DataField = 'DOCDATE'
      DataSource = dmMain.dsrPList
      Color = clInfoBk
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
    end
  end
  inherited gridItem: TDBGridEh
    Top = 101
    Width = 869
    Height = 372
    DataSource = dmMain.dsrProtocol
    UseMultiTitle = True
    OnGetCellParams = gridItemGetCellParams
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'RecNo'
        Footers = <>
        Width = 27
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'PSNAME'
        Footers = <>
        Width = 98
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OPERNAME'
        Footers = <>
        Width = 105
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'F'
        Footers = <>
        Width = 50
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'N'
        Footers = <>
        Width = 48
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'E'
        Footers = <>
        Width = 58
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'S'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'I'
        Footers = <>
        Width = 55
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'H'
        Footers = <>
        Width = 67
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'WZ'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OUT1'
        Footers = <>
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'OUT_PS1'
        Footers = <>
      end>
  end
  inherited ilButtons: TImageList
    Left = 74
    Top = 176
  end
  inherited fmstr: TFormStorage
    Top = 128
  end
  inherited ppPrint: TTBPopupMenu
    Left = 192
    Top = 132
  end
end
