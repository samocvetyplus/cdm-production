inherited fmdCoverDepart: TfmdCoverDepart
  Left = 248
  Top = 173
  Height = 319
  Caption = #1057#1086#1089#1090#1072#1074' '#1091#1095#1072#1089#1090#1082#1072' '#1089#1098#1077#1084#1072
  OldCreateOrder = True
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 273
  end
  inherited tb1: TSpeedBar
    Visible = False
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 42
    Width = 396
    Height = 231
    Align = alClient
    TabOrder = 2
  end
  object taPS: TpFIBDataSet
    Database = dm.db
    Transaction = dm.tr
    SelectSQL.Strings = (
      'select d.DEPID, d.NAME, c.ID COVERID'
      'from D_Dep d left join D_PSCOVER c on (d.DEPID=c.DEPID)'
      'where bit(d.DEPTYPES, 0)=1 and'
      '      bit(d.DEPTYPES, 4)=0 and'
      '      c.COVERDEPID = :DEPID'
      'order by SORTIND')
    BeforeOpen = taPSBeforeOpen
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 16
    Top = 64
    poSQLINT64ToBCD = True
    object taPSDEPID: TFIBIntegerField
      FieldName = 'DEPID'
    end
    object taPSNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taPSCOVERID: TFIBIntegerField
      FieldName = 'COVERID'
    end
  end
end
