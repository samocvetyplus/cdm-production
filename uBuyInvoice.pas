
{*****************************************}
{                                         }
{         Delphi XML Data Binding         }
{                                         }
{         Generated on: 15.01.2010 10:24:53 }
{       Generated from: C:\invoice.xml    }
{                                         }
{*****************************************}
unit uBuyInvoice;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLDocumentType = interface;
  IXMLElementsType = interface;
  IXMLElementType = interface;

{ IXMLDocumentType }

  IXMLDocumentType = interface(IXMLNode)
    ['{078E0042-D10D-4C80-97DE-3EF44DFEBB6C}']
    { Property Accessors }
    function Get_Elements: IXMLElementsType;
    { Methods & Properties }
    property Elements: IXMLElementsType read Get_Elements;
  end;

{ IXMLElementsType }

  IXMLElementsType = interface(IXMLNodeCollection)
    ['{AA9C2603-9150-4D9E-B713-6FD30B511B13}']
    { Property Accessors }
    function Get_Element(Index: Integer): IXMLElementType;
    { Methods & Properties }
    function Add: IXMLElementType;
    function Insert(const Index: Integer): IXMLElementType;
    property Element[Index: Integer]: IXMLElementType read Get_Element; default;
  end;

{ IXMLElementType }

  IXMLElementType = interface(IXMLNode)
    ['{030BD568-B5AC-4D29-A71B-12760C3E1435}']
    { Property Accessors }
    function Get_ProbeID: Integer;
    function Get_MemoID: Integer;
    function Get_Weight: Double;
    function Get_Price: Double;
    procedure Set_ProbeID(Value: Integer);
    procedure Set_MemoID(Value: Integer);
    procedure Set_Weight(Value: Double);
    procedure Set_Price(Value: Double);
    { Methods & Properties }
    property ProbeID: Integer read Get_ProbeID write Set_ProbeID;
    property MemoID: Integer read Get_MemoID write Set_MemoID;
    property Weight: Double read Get_Weight write Set_Weight;
    property Price: Double read Get_Price write Set_Price;
  end;

{ Forward Decls }

  TXMLDocumentType = class;
  TXMLElementsType = class;
  TXMLElementType = class;

{ TXMLDocumentType }

  TXMLDocumentType = class(TXMLNode, IXMLDocumentType)
  protected
    { IXMLDocumentType }
    function Get_Elements: IXMLElementsType;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLElementsType }

  TXMLElementsType = class(TXMLNodeCollection, IXMLElementsType)
  protected
    { IXMLElementsType }
    function Get_Element(Index: Integer): IXMLElementType;
    function Add: IXMLElementType;
    function Insert(const Index: Integer): IXMLElementType;
  public
    procedure AfterConstruction; override;
    property Item[Index: Integer]: IXMLElementType read Get_Element;
  end;

{ TXMLElementType }

  TXMLElementType = class(TXMLNode, IXMLElementType)
  protected
    { IXMLElementType }
    function Get_ProbeID: Integer;
    function Get_MemoID: Integer;
    function Get_Weight: Double;
    function Get_Price: Double;
    procedure Set_ProbeID(Value: Integer);
    procedure Set_MemoID(Value: Integer);
    procedure Set_Weight(Value: Double);
    procedure Set_Price(Value: Double);
  end;

{ Global Functions }

function GetDocument(Doc: IXMLDocument): IXMLDocumentType;
function LoadDocument(const FileName: WideString): IXMLDocumentType;
function NewDocument: IXMLDocumentType;

implementation

{ Global Functions }

function GetDocument(Doc: IXMLDocument): IXMLDocumentType;
begin
  Result := Doc.GetDocBinding('Document', TXMLDocumentType) as IXMLDocumentType;
end;
function LoadDocument(const FileName: WideString): IXMLDocumentType;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('Document', TXMLDocumentType) as IXMLDocumentType;
end;

function NewDocument: IXMLDocumentType;
begin
  Result := NewXMLDocument.GetDocBinding('Document', TXMLDocumentType) as IXMLDocumentType;
end;

{ TXMLDocumentType }

procedure TXMLDocumentType.AfterConstruction;
begin
  RegisterChildNode('Elements', TXMLElementsType);
  inherited;
end;

function TXMLDocumentType.Get_Elements: IXMLElementsType;
begin
  Result := ChildNodes['Elements'] as IXMLElementsType;
end;

{ TXMLElementsType }

procedure TXMLElementsType.AfterConstruction;
begin
  RegisterChildNode('Element', TXMLElementType);
  ItemTag := 'Element';
  ItemInterface := IXMLElementType;
  inherited;
end;

function TXMLElementsType.Get_Element(Index: Integer): IXMLElementType;
begin
  Result := List[Index] as IXMLElementType;
end;

function TXMLElementsType.Add: IXMLElementType;
begin
  Result := AddItem(-1) as IXMLElementType;
end;

function TXMLElementsType.Insert(const Index: Integer): IXMLElementType;
begin
  Result := AddItem(Index) as IXMLElementType;
end;


{ TXMLElementType }

function TXMLElementType.Get_ProbeID: Integer;
begin
  Result := AttributeNodes['ProbeID'].NodeValue;
end;

procedure TXMLElementType.Set_ProbeID(Value: Integer);
begin
  SetAttribute('ProbeID', Value);
end;

function TXMLElementType.Get_MemoID: Integer;
begin
  Result := AttributeNodes['MemoID'].NodeValue;
end;

procedure TXMLElementType.Set_MemoID(Value: Integer);
begin
  SetAttribute('MemoID', Value);
end;

function TXMLElementType.Get_Weight: Double;
begin
  Result := AttributeNodes['Weight'].NodeValue;
end;

procedure TXMLElementType.Set_Weight(Value: Double);
begin
  SetAttribute('Weight', Value);
end;

function TXMLElementType.Get_Price: Double;
begin
  Result := AttributeNodes['Price'].NodeValue;
end;

procedure TXMLElementType.Set_Price(Value: Double);
begin
  SetAttribute('Price', Value);
end;

end.
