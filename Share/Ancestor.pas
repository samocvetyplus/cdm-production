unit Ancestor;  

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  RxSpeedBar, ExtCtrls, ComCtrls, ImgList, RxPlacemnt, ActnList;

type
  TfmAncestor = class(TForm)
    stbrStatus: TStatusBar;
    fmstr: TFormStorage;
    ilButtons: TImageList;
    SpeedbarSection1: TSpeedbarSection;
    spitExit: TSpeedItem;
    tb1: TSpeedBar;
    ActionList2: TActionList;
    procedure spitExitClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormResize(Sender: TObject);
  private
    procedure WMSysCommand(var Message: TWMSysCommand); message WM_SYSCOMMAND;
  end;


var
  fmAncestor: TfmAncestor;

implementation

{$R *.DFM}

uses fmUtils;

procedure TfmAncestor.WMSysCommand(var Message: TWMSysCommand);
begin
  if (Message.CmdType = SC_MINIMIZE) then MinimizeApp
  else inherited;
end;

procedure TfmAncestor.spitExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfmAncestor.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_F12 : if Shift = [] then Close;
  end;
end;

procedure TfmAncestor.FormResize(Sender: TObject);
begin
  spitExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

end.
