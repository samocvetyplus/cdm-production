unit ListAncestor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Ancestor, StdCtrls, Grids, DBGrids, RXDBCtrl,
  ImgList, ExtCtrls, ComCtrls, Menus,
  RXCtrls,  db, DBGridEh, ActnList, TB2Item, pFIBQuery, pFIBDataSet,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;
//  {$ifdef IBX}, IbSQL, IbCustomDataSet {$endif IBX};
//  {$ifdef FIBP}, pFIBQuery, pFIBDataSet {$endif FIBP};

type
  TOnChangePeriod = procedure(BD, ED :TDateTime ) of object;
  TListAncestorClass = class of TfmListAncestor;

  TfmListAncestor = class(TfmAncestor)
    tb2: TSpeedBar;
    laDep: TLabel;
    laPeriod: TLabel;
    SpeedbarSection2: TSpeedbarSection;
    spitDep: TSpeedItem;
    spitPeriod: TSpeedItem;
    ppDep: TPopupMenu;
    mnitAllWh: TMenuItem;
    mnitSep: TMenuItem;
    spitAdd: TSpeedItem;
    spitDel: TSpeedItem;         
    spitEdit: TSpeedItem;
    spitPrint: TSpeedItem;
    gridDocList: TDBGridEh;
    acAEvent: TActionList;
    acPeriod: TAction;
    acAddDoc: TAction;
    acDelDoc: TAction;
    acEditDoc: TAction;
    ppDoc: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    TBItem3: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    TBItem4: TTBItem;
    acPrintDoc: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure spitAddClick(Sender: TObject);
    procedure grDblClick(Sender: TObject);
    procedure grGetCellParams(Sender: TObject; Field: TField; AFont: TFont; var Background: TColor; Highlight: Boolean);
    procedure gridDocListDblClick(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh; AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPeriodExecute(Sender: TObject);
    procedure acAddDocExecute(Sender: TObject);
    procedure acDelDocExecute(Sender: TObject);
    procedure acAddDocUpdate(Sender: TObject);
    procedure acDelDocUpdate(Sender: TObject);
    procedure acEditDocUpdate(Sender: TObject);
    procedure acPrintDocUpdate(Sender: TObject);
  protected
    FCurrDep : integer;
    {$ifdef IBX}
    FQuery : TIBSQL;
    FDataSet : TIbCustomDataSet;
    {$endif IBX}
    {$ifdef FIBP}
    FQuery : TpFIBQuery;
    FDataSet : TpFIBDataSet;
    {$endif FIBP}
    FBD, FED: TDateTime;
    FOnChangePeriod : TOnChangePeriod;
    FReadOnly : boolean;  // ����� ��������� �����
    procedure DepClick(Sender : TObject); virtual;
    procedure ShowPeriod; virtual;
    procedure SetReadOnly(ReadOnly : boolean); virtual;
  public
    {$ifdef IBX}
    property DataSet : TIbCustomDataSet read FDataSet write FDataSet;
    constructor Create(AOwner : TComponent; DataSet : TIbCustomDataSet;
      CurrDep : integer; BD, ED :TDateTime; ChangePeriod : TOnChangePeriod; Query : TIBSQL);
    {$endif IBX}
    {$ifdef FIBP}
    property DataSet : TpFIBDataSet read FDataSet write FDataSet;
    property ReadOnly: Boolean read FReadOnly;    
    constructor Create(AOwner : TComponent; DataSet : TpFIBDataSet;
      CurrDep : integer; BD, ED :TDateTime; ChangePeriod : TOnChangePeriod; Query : TpFIBQuery);
    {$endif FIBP}
  end;


  {$ifdef IBX}
  function ShowListForm(FC : TListAncestorClass; DataSet : TIbCustomDataSet;
     CurrDep : integer; BD, ED : TDateTime; ChangePeriod : TOnChangePeriod;
     Query : TIBSQL) : word;
  {$endif IBX}

  {$ifdef FIBP}
  function ShowListForm(FC : TListAncestorClass; DataSet : TpFIBDataSet;
     CurrDep : integer; BD, ED : TDateTime; ChangePeriod : TOnChangePeriod;
     Query : TpFIBQuery) : word;
  {$endif FIBP}



implementation

uses dbUtil, Period, fmUtils, MsgDialog;

{$R *.DFM}

{$ifdef IBX}
function ShowListForm(FC : TListAncestorClass;
     DataSet : TIbCustomDataSet; CurrDep : integer; BD, ED : TDateTime;
     ChangePeriod : TOnChangePeriod; Query : TIBSQL) : word;
{$endif IBX}
{$ifdef FIBP}
function ShowListForm(FC : TListAncestorClass;
     DataSet : TpFIBDataSet; CurrDep : integer; BD, ED : TDateTime;
     ChangePeriod : TOnChangePeriod; Query : TpFIBQuery) : word;
{$endif FIBP}
var
  List : TfmListAncestor;
begin
  Screen.Cursor:=crHourGlass;
  try
    Application.ProcessMessages;
    List := nil;
    List := FC.Create(Application, DataSet, CurrDep, BD, ED, ChangePeriod, Query);
    Screen.Cursor:=crDefault;
    //if List.spitPrint.Enabled and List.spitPrint.Visible then ShowMessage('ShowListForm: Enable & Visible')
    //else ShowMessage('ShowListForm: Disable or  Invisible');
    Result := List.ShowModal;
  finally
    Screen.Cursor:=crDefault;
    FreeAndNil(List);
  end;

end;


(*
  ����������� �����������
   OnCreate
   DepClick

procedure TfmListAncestor.FormCreate(Sender: TObject);
var
  i, j : integer;
  Item : TMenuItem;
begin
  dmMain.CurrDep := dm.DepDef;
  inherited;
  //���������� PopupMenu �� FDepInfo;
  j := 0;
  for i:=0 to Pred(dmMain.CountDep) do
  begin
    Item := TMenuItem.Create(ppDep);
    Item.Caption := dmMain.DepInfo[i]^.SName;
    Item.Tag := dmMain.DepInfo[i]^.DepId;
    Item.OnClick := DepClick;
    Item.RadioItem := True;
    if Item.Tag = dm.DepDef then DepClick(Item);
    inc(j);
    if j>30 then
    begin
      Item.Break := mbBreak;
      j:=0;
    end;
    ppDep.Items.Add(Item);
  end;
  laPeriod.Caption:='� '+DateToStr(dmMain.BeginDate) + ' �� ' +DateToStr(dmMain.EndDate);
end; *)

procedure TfmListAncestor.FormCreate(Sender: TObject);
begin
  mnitAllWh.OnClick := DepClick;
  //laDep.Caption := mnitAllWh.Caption;
  inherited;
  // ��������� ������
  if Assigned(gridDocList.DataSource)and Assigned(gridDocList.DataSource.DataSet) then
  begin
    if not DataSet.Transaction.Active then DataSet.Transaction.StartTransaction;
    ReOpenDataSets([DataSet]);
  end;
  // ���������� ������
  ShowPeriod;
end;


procedure TfmListAncestor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  if Assigned(gridDocList.DataSource) then
  begin
    if DataSet.State in [dsEdit, dsInsert] then
        PostDataSets([DataSet]);
    if DataSet.Transaction.Active then DataSet.Transaction.Commit;
    CloseDataSets([DataSet]);
  end;
  Action := caFree;
end;

procedure TfmListAncestor.spitAddClick(Sender: TObject);
begin
  if FCurrDep <> -1 then
  begin
    DataSet.Append;
    DataSet.Post;
    DataSet.Transaction.CommitRetaining;
  end
  else begin
    MessageDialogA('���������� ������� �����', mtInformation, 196);
    Abort;
  end;
end;

procedure TfmListAncestor.DepClick(Sender: TObject);
begin
  laDep.Caption := TMenuItem(Sender).Caption;
  FCurrDep := TMenuItem(Sender).Tag;
  ReOpenDataSets([DataSet]);
end;

{$ifdef IBX}
constructor TfmListAncestor.Create(AOwner : TComponent; DataSet : TIbCustomDataSet;
   CurrDep : integer; BD, ED :TDateTime; ChangePeriod : TOnChangePeriod; Query : TIBSQL);
{$endif IBX}
{$ifdef FIBP}
constructor TfmListAncestor.Create(AOwner : TComponent; DataSet : TpFIBDataSet;
   CurrDep : integer; BD, ED :TDateTime; ChangePeriod : TOnChangePeriod; Query : TpFIBQuery);
{$endif FIBP}
begin
  FDataSet := DataSet;
  FCurrDep := CurrDep;
  FQuery := Query;
  FBD := BD;
  FED := ED;
  FOnChangePeriod := ChangePeriod;
  FReadOnly := False;
  inherited Create(AOwner);
end;

procedure TfmListAncestor.ShowPeriod;
begin
  laPeriod.Caption:='� '+DateToStr(FBD) + ' �� ' +DateToStr(FED);
end;

procedure TfmListAncestor.grDblClick(Sender: TObject);
begin
  inherited;
  if Assigned(spitEdit.OnClick) then spitEdit.OnClick(Sender);
end;

procedure TfmListAncestor.grGetCellParams(Sender: TObject; Field: TField;
  AFont: TFont; var Background: TColor; Highlight: Boolean);
begin
  if (Assigned(Field)and(Field.FieldName='RecNo'))then Background := clMoneyGreen;
end;

procedure TfmListAncestor.gridDocListDblClick(Sender: TObject);
begin
  inherited;
  if Assigned(spitEdit.OnClick) then spitEdit.OnClick(Sender);
end;

procedure TfmListAncestor.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  inherited;
  if AnsiUpperCase(Column.FieldName)='RECNO' then Background := clMoneyGreen;
end;

procedure TfmListAncestor.acPeriodExecute(Sender: TObject);
begin
  if ShowPeriodForm(FBD, FED) then
  begin
    FOnChangePeriod(FBD, FED);
    ShowPeriod;
    try
      Screen.Cursor:=crHourGlass;
      Application.ProcessMessages;
      ReOpenDataSets([DataSet]);
      Application.ProcessMessages;
    finally
      Screen.Cursor:=crDefault;
    end;
  end;
end;

procedure TfmListAncestor.acPrintDocUpdate(Sender: TObject);
begin
  inherited;
  acPrintDoc.Enabled := dataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmListAncestor.acAddDocExecute(Sender: TObject);
begin
  if FCurrDep <> -1 then
  begin
    DataSet.Append;
    DataSet.Post;
    DataSet.Transaction.CommitRetaining;
  end
  else begin
    MessageDialogA('���������� ������� �����', mtWarning);
    Abort;
  end;
end;

procedure TfmListAncestor.acDelDocExecute(Sender: TObject);
begin
  DataSet.Delete;
end;

procedure TfmListAncestor.acAddDocUpdate(Sender: TObject);
begin
  acAddDoc.Enabled := (not FReadOnly) and DataSet.Active;
end;

procedure TfmListAncestor.acDelDocUpdate(Sender: TObject);
begin
  acDelDoc.Enabled := (not FReadOnly) and DataSet.Active and (not DataSet.IsEmpty);
end;



procedure TfmListAncestor.acEditDocUpdate(Sender: TObject);
begin
  acEditDoc.Enabled := //(not FReadOnly) and
  DataSet.Active and (not DataSet.IsEmpty);
end;

procedure TfmListAncestor.SetReadOnly(ReadOnly: boolean);
begin
  FReadOnly := ReadOnly;
end;

end.
