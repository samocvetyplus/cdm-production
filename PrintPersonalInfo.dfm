object PlaceOfStorageInventoryPrint: TPlaceOfStorageInventoryPrint
  Left = 0
  Top = 0
  Caption = #1042#1099#1073#1086#1088' '#1090#1080#1087#1072' '#1086#1087#1080#1089#1080
  ClientHeight = 119
  ClientWidth = 377
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 208
    Top = 8
    Width = 8
    Height = 16
    Caption = 'C'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 208
    Top = 42
    Width = 17
    Height = 16
    Caption = #1055#1054
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object BDEdit: TDateEdit
    Left = 248
    Top = 8
    Width = 121
    Height = 21
    DialogTitle = #1042#1099#1073#1077#1088#1080#1090#1077' '#1085#1072#1095#1072#1083#1086' '#1087#1077#1088#1080#1086#1076#1072
    NumGlyphs = 2
    TabOrder = 0
  end
  object EDEdit: TDateEdit
    Left = 248
    Top = 37
    Width = 120
    Height = 21
    DateAutoBetween = False
    DefaultToday = True
    DialogTitle = #1042#1099#1073#1077#1088#1080#1090#1077' '#1082#1086#1085#1077#1094' '#1087#1077#1088#1080#1086#1076#1072
    NumGlyphs = 2
    Weekends = [Sun, Sat]
    TabOrder = 1
    Text = '05.03.2012'
  end
  object Button1: TButton
    Left = 0
    Top = 79
    Width = 377
    Height = 39
    Caption = #1057#1086#1079#1076#1072#1090#1100' '#1086#1087#1080#1089#1100' '#1087#1086' '#1084#1077#1089#1090#1091' '#1093#1088#1072#1085#1077#1085#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnClick = Button1Click
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 0
    Width = 185
    Height = 73
    Caption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1074#1080#1076' '#1086#1087#1080#1089#1080': '
    TabOrder = 3
    object Current: TRadioButton
      Left = 16
      Top = 22
      Width = 153
      Height = 17
      Caption = #1047#1072' '#1090#1077#1082#1091#1097#1080#1081' '#1087#1077#1088#1080#1086#1076
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = CurrentClick
    end
    object Random: TRadioButton
      Left = 16
      Top = 45
      Width = 153
      Height = 17
      Caption = #1047#1072' '#1087#1088#1086#1080#1079#1074#1086#1083#1100#1085#1099#1081' '#1087#1077#1088#1080#1086#1076
      TabOrder = 1
      OnClick = RandomClick
    end
  end
end
