unit eNotInInv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DBGridEh, DBUtil, DBGridEhGrouping, GridsEh;

type
  TfmNotInInv = class(TForm)
    DBGridEh1: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmNotInInv: TfmNotInInv;

implementation

uses InvData;

{$R *.dfm}

procedure TfmNotInInv.FormCreate(Sender: TObject);
begin
 ReOpenDataSet(dminv.taNotInInv);
end;

procedure TfmNotInInv.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 CloseDataSet(dminv.taNotInInv);
end;

end.
