unit eJaSz;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  DbEditor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, RxDBComb, Editor;

type
  TfmJaSzEditor = class(TfmEditor)
    Label2: TLabel;
    edSz: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmJaSzEditor: TfmJaSzEditor;
  Artid : integer;

implementation

uses MainData, pFIBQuery, DB, dbUtil, //bsUtils,
ApplData;

{$R *.dfm}

procedure TfmJaSzEditor.FormCreate(Sender: TObject);
begin
  ActiveControl := edSz;
end;

procedure TfmJaSzEditor.FormClose(Sender: TObject; var Action: TCloseAction);
var
  JaId : Variant;
begin

  if ModalResult = mrOk then
  begin

    if edSz.Text = '' then raise Exception.CreateRes(ErrorResId + 8);

    Editor.vResult := ExecSelectSQL('select Id from D_Sz where Name='#39+edSz.Text+#39, dmAppl.quTmp);

    if VarIsNull(vResult) then raise Exception.Create('����������� ������');

    JaId := ExecSelectSQL('select Id from Ja where Sz='+IntToStr(Editor.vResult)+' and Art2Id='+
    dmAppl.taApplART2ID.AsString+' and ApplId='+dmAppl.taApplAPPLID.AsString, dmAppl.quTmp);

    if not VarIsNull(JaId) and (JaId <> 0) then
      raise Exception.Create('����� ������ ��� ���� � ������');

  end;

  inherited;
  
end;

end.
