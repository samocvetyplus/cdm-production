unit DbEditor; 

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, StdCtrls, Buttons, ExtCtrls, Db;

type
  TDbEditorMode = (emAppend, emEdit, emBrowse);
  TDbEditorClass = class of TfmDbEditor;

  TfmDbEditor = class(TfmEditor)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
    FDataSet: TDataSet;
    FMode: TDbEditorMode;
  protected
    FDataSource : TDataSource;
    FPosted : boolean;
  public
    property DataSet : TDataSet read FDataSet write FDataSet;
    property EditorMode : TDbEditorMode read FMode write FMode;
    constructor Create(AOwner : TComponent; DataSource : TDataSource; Mode : TDbEditorMode; Posted : boolean); overload;
  end;

var
  fmDbEditor: TfmDbEditor;

(*
  + 1. ������ ��������� � ����� ������ ��� �������� ������ ��������. �� ���� �������� reopendataset
  - 2. ������ ����������� � ������� ���������, ��� ����������� ����������� ������
        ���������� ������� id ���� ������, ���������� ����� ������, � ����� ����� �� ��� ������
        � ������� locate.
  ������ ������� ������� �� ����� �������. ���������� ��� ���� �� ����������. �������� ������������� �
  ������ ���� ������ �� ������������ � �����-���� control'�.
*)

function ShowDbEditor(FC : TDbEditorClass; DataSource : TDataSource; Mode:  TDbEditorMode = emEdit;
   Posted : boolean = True) : word;

implementation

uses dbUtil, fmUtils
     {$ifdef IBX}, IbCustomDataSet {$endif}
     {$ifdef FIBP}, pFIBDataSet {$endif}, DbGrids, DbCtrls;

{$R *.dfm}

function ShowDbEditor(FC : TDbEditorClass; DataSource : TDataSource; Mode:  TDbEditorMode = emEdit; Posted : boolean = True) : word;
var
  F : TfmDbEditor;
begin
  F := FC.Create(Application, DataSource, Mode, Posted);
  try
    Result := F.ShowModal;
  finally
    FreeAndNil(F);
  end;
end;

{ TfmDbEditor }

constructor TfmDbEditor.Create(AOwner: TComponent; DataSource: TDataSource; Mode: TDbEditorMode; Posted : boolean);
begin
  FDataSet := DataSource.DataSet;
  FDataSource := DataSource;
  FMode := Mode;
  FPosted := Posted;
  inherited Create(AOwner);
end;

procedure TfmDbEditor.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  case ModalResult of
    mrOk : if FPosted then PostDataSet(DataSet);
    else CancelDataSets([DataSet]);
  end;
  {$ifdef IBX}
  TIbDataSet(DataSet).Transaction.CommitRetaining;
  {$endif}
  {$ifdef FIBP}
  TpFIBDataSet(DataSet).Transaction.CommitRetaining;
  {$endif}
  Action := caFree;
end;

procedure TfmDbEditor.FormCreate(Sender: TObject);
var
  i : integer;
begin
  inherited;
  OpenDataSet(FDataSet);
  case FMode of
    emAppend : DataSet.Insert;
    emEdit : DataSet.Edit;
    emBrowse : 
      for i:=0 to Pred(ComponentCount) do
      begin
       if Components[i].InheritsFrom(TDbGrid) then TDbGrid(Components[i]).ReadOnly := True;
       if Components[i].InheritsFrom(TDbEdit)then TDbEdit(Components[i]).ReadOnly := True;
       if Components[i].InheritsFrom(TDbCheckBox) then TDbCheckBox(Components[i]).ReadOnly := True;
       if Components[i].InheritsFrom(TDbMemo) then TDbMemo(Components[i]).ReadOnly := True;
       if Components[i].InheritsFrom(TDbImage) then TDbImage(Components[i]).ReadOnly := True;
      end
  end;
end;

end.
