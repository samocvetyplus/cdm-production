unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Grids, DBGrids;

type
  TForma = class(TForm)
    DBGrid1: TDBGrid;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;



var
  Forma: TForma;

implementation

uses dmReportInventory;

{$R *.dfm}

procedure TForma.Button1Click(Sender: TObject);
begin
  ReportInventory.taTotalLosses.Close;
  ShowMessage('Closed');
  ReportInventory.taTotalLosses.Open;
  ShowMessage('Opened');
end;

end.
