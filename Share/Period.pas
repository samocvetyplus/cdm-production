unit Period; 

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, RxToolEdit, Buttons, DBCtrlsEh;

type
  TfmPeriod = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    deBD: TDBDateTimeEditEh;
    deED: TDBDateTimeEditEh;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
   
  private
    FBD: TDateTime;
    FED: TDateTime;
  public
    property BeginDate : TDateTime read FBD write FBD;
    property EndDate : TDateTime read FED write FED;
    constructor Create(AOwner : TComponent; BD, ED : TDateTime; Caption : string);
  end;

function ShowPeriodForm(var BD,ED : TDateTime; Caption : string = '����� ��������� �������') : boolean;

implementation

{$R *.DFM}

uses DateUtils, Variants, UtilLib;

function ShowPeriodForm(var BD,ED : TDateTime; Caption : string = '����� ��������� �������') : boolean;
var
  fmPeriod: TfmPeriod;
  r : word;
begin
  Application.ProcessMessages;
  fmPeriod := TfmPeriod.Create(Application, BD, ED, Caption);
  try
    r  := fmPeriod.ShowModal;
    Result := (r=mrOk)or(r=mrYes)or(r=mrYesToAll);
    if Result then
    begin
      BD := fmPeriod.BeginDate;
      ED := EndOfTheDay(fmPeriod.EndDate);
    end
  finally
    FreeAndNil(fmPeriod);
  end;
end;

procedure TfmPeriod.FormCreate(Sender: TObject);
begin
  deBD.Value := DateToStr(BeginDate);
  deED.Value := DateToStr(EndDate);
end;

procedure TfmPeriod.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 if ModalResult=mrOk then
 begin
  if not ConvertVarToDateTime(deBD.Value, FBD) then
  begin
    ActiveControl := deBD;
    raise Exception.Create('�� ������ ����');
  end;

  if not ConvertVarToDateTime(deED.Value, FED) then
  begin
    ActiveControl := deED;
    raise Exception.Create('�� ������ ����');
  end;
 end;
end;



constructor TfmPeriod.Create(AOwner: TComponent; BD, ED: TDateTime; Caption : string);
begin
  FBD := BD;
  FED := ED;
  inherited Create(AOwner);
  Self.Caption := Caption;
end;

end.
