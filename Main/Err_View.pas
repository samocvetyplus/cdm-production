unit Err_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ImgList, ExtCtrls, ComCtrls,
  Grids, DBGrids, RXDBCtrl,  dbUtil, DBGridEh, ActnList,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmErrArt = class(TfmAncestor)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    SpeedItem1: TSpeedItem;
    DBGridEh1: TDBGridEh;
    procedure SpeedItem1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmErrArt: TfmErrArt;

implementation

uses InvData;

{$R *.dfm}

procedure TfmErrArt.SpeedItem1Click(Sender: TObject);
begin
  inherited;
  case PageControl1.ActivePageIndex of
    0: begin
        Screen.Cursor:=crSQLWait;
        ReOpenDataSet(dmInv.taError_NEA);
        Screen.Cursor:=crDefault;
        if dmInv.taError_NEA.RecordCount=0 then ShowMessage('������ �� ����������!');
      end
  end;
end;

end.
