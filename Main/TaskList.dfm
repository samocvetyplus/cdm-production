inherited fmTaskList: TfmTaskList
  Left = 340
  Top = 219
  Caption = #1047#1072#1076#1072#1095#1080
  PixelsPerInch = 96
  TextHeight = 13
  inherited tb2: TSpeedBar
    inherited laDep: TLabel
      Visible = False
    end
    inherited spitPeriod: TSpeedItem [3]
    end
    inherited spitDep: TSpeedItem [4]
      Visible = False
    end
  end
  inherited tb1: TSpeedBar
    inherited spitPrint: TSpeedItem [4]
    end
    inherited spitEdit: TSpeedItem [5]
      OnClick = spitEditClick
    end
  end
  inherited gridDocList: TDBGridEh
    DataSource = dmData.dsrTaskList
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    Visible = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'RECNO'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'N'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'DAT'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'TYPE'
        Footers = <>
        Width = 120
      end
      item
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Width = 120
      end
      item
        EditButtons = <>
        FieldName = 'BRIEFDESCR'
        Footers = <>
        Width = 150
      end
      item
        EditButtons = <>
        FieldName = 'CREATEBYUSER'
        Footers = <>
        Width = 200
      end>
  end
end
