unit eJa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DbEditor, StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, RxDBComb,
  Editor, DBCtrlsEh, DB, FIBDataSet, pFIBDataSet;

type
  TfmJaEditor = class(TfmEditor)
    lbArt: TLabel;
    edArt: TDBEditEh;
    lbComment: TLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmJaEditor: TfmJaEditor;

implementation

//uses dbUtil;

{$R *.dfm}

procedure TfmJaEditor.FormClose(Sender: TObject; var Action: TCloseAction);
//var
//  Msg : string;
begin
  case ModalResult of
    mrOk : begin
      vResult := edArt.Text;
    end;
  end;
  inherited;
end;

end.
