object fmNewPriceU: TfmNewPriceU
  Left = 441
  Top = 208
  BorderStyle = bsDialog
  Caption = #1053#1086#1074#1072#1103' '#1094#1077#1085#1072' '#1079#1072' '#1096#1090'.'
  ClientHeight = 108
  ClientWidth = 216
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 216
    Height = 73
    Shape = bsFrame
  end
  object laDep: TRxLabel
    Left = 74
    Top = 8
    Width = 30
    Height = 13
    Caption = 'laDep'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object RxLabel2: TRxLabel
    Left = 6
    Top = 6
    Width = 33
    Height = 13
    Caption = #1057#1082#1083#1072#1076
  end
  object RxLabel3: TRxLabel
    Left = 6
    Top = 52
    Width = 91
    Height = 13
    Caption = #1044#1072#1090#1072' '#1087#1077#1088#1077#1086#1094#1077#1085#1082#1080
  end
  object SpeedButton1: TSpeedButton
    Left = 62
    Top = 80
    Width = 81
    Height = 22
    Caption = 'Ok'
    OnClick = SpeedButton1Click
  end
  object RxLabel1: TRxLabel
    Left = 6
    Top = 29
    Width = 61
    Height = 13
    Caption = #1053#1086#1074#1072#1103' '#1094#1077#1085#1072
  end
  object EdDate: TDateEdit
    Left = 97
    Top = 47
    Width = 112
    Height = 21
    Color = clInfoBk
    NumGlyphs = 2
    TabOrder = 0
  end
  object ceK: TRxCalcEdit
    Left = 96
    Top = 24
    Width = 113
    Height = 21
    AutoSize = False
    Color = clInfoBk
    NumGlyphs = 2
    TabOrder = 1
    Value = 1.000000000000000000
  end
end
