unit eArtSzDiamond;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls,
  ComCtrls, Grids, DBGridEh, TB2Item, TB2Dock, TB2Toolbar, DB,
  FIBDataSet, pFIBDataSet, DBGridEhGrouping, GridsEh;

type
  TfmeArtSzDiamond = class(TfmAncestor)
    gridSzDiamond: TDBGridEh;
    tbdcSemis: TTBDock;
    TBToolbar2: TTBToolbar;
    TBItem2: TTBItem;
    TBItem5: TTBItem;
    acAdd: TAction;
    acDel: TAction;
    DBGridEh2: TDBGridEh;
    taArtSzDiamond: TpFIBDataSet;
    taArtSzDiamondQ: TFIBIntegerField;
    taArtSzDiamondSZ: TFIBStringField;
    dsrArtSzDiamond: TDataSource;
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acDelExecute(Sender: TObject);
  end;

var
  fmeArtSzDiamond: TfmeArtSzDiamond;

implementation

uses DictData, dbUtil, Editor, eQ1;

{$R *.dfm}

procedure TfmeArtSzDiamond.acAddExecute(Sender: TObject);
begin
  if ShowEditor(TfmeQ1, TfmEditor(fmeQ1))<>mrOk then eXit;
  if VarIsNumeric(Editor.vResult)and(Editor.vResult>0)then
  begin
    if not taArtSzDiamond.Locate('SZ', dm.taSzDiamondSZ.AsString, [])then
    begin
      taArtSzDiamond.Insert;
      taArtSzDiamondSZ.AsString := dm.taSzDiamondSZ.AsString;
      taArtSzDiamondQ.AsInteger := Editor.vResult;
      taArtSzDiamond.Post;
    end
    else begin
      taArtSzDiamond.Edit;
      taArtSzDiamondQ.AsInteger := taArtSzDiamondQ.AsInteger+Editor.vResult;
      taArtSzDiamond.Post;
    end
  end;
end;

procedure TfmeArtSzDiamond.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := dm.taSzDiamond.Active and (not dm.taSzDiamond.IsEmpty);
end;

procedure TfmeArtSzDiamond.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := taArtSzDiamond.Active and (not taArtSzDiamond.IsEmpty);
end;

procedure TfmeArtSzDiamond.FormCreate(Sender: TObject);
var
  sl : TStringList;
  i : integer;
begin
  OpenDataSets([dm.taSzDiamond, taArtSzDiamond]);
  taArtSzDiamond.Delete;
  sl := TStringList.Create;
  try
    dm.ArtSzDiamond := copy(dm.ArtSzDiamond, 1, Length(dm.ArtSzDiamond)-1);
    sl.Delimiter := ';';
    sl.DelimitedText := dm.ArtSzDiamond;
    for i:=0 to Pred(sl.Count) do
    begin
      taArtSzDiamond.Insert;
      taArtSzDiamondQ.AsInteger := StrToIntDef(sl.Values[sl.Names[i]], 0);
      taArtSzDiamondSZ.AsString := sl.Names[i];
      taArtSzDiamond.Post;
    end;
  finally
    sl.Free;
  end;
  spitExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;

procedure TfmeArtSzDiamond.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  dm.ArtSzDiamond := '';
  taArtSzDiamond.First;
  while not taArtSzDiamond.Eof do
  begin
    dm.ArtSzDiamond := dm.ArtSzDiamond+taArtSzDiamondSZ.AsString+'='+taArtSzDiamondQ.AsString+';';
    taArtSzDiamond.Next;
  end;
  CloseDataSet(dm.taSzDiamond);
  inherited;
end;

procedure TfmeArtSzDiamond.acDelExecute(Sender: TObject);
begin
  taArtSzDiamond.Delete;
end;

end.
