inherited fmVerificationList: TfmVerificationList
  Left = 0
  Top = 0
  Caption = #1057#1087#1080#1089#1086#1082' '#1072#1082#1090#1086#1074' '#1089#1074#1077#1088#1082#1080
  ClientHeight = 402
  ClientWidth = 676
  Position = poDesigned
  ExplicitWidth = 684
  ExplicitHeight = 436
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 383
    Width = 676
    ExplicitTop = 383
    ExplicitWidth = 676
  end
  inherited tb2: TSpeedBar
    Width = 676
    ExplicitWidth = 676
    inherited laDep: TLabel
      Left = 496
      Visible = False
      ExplicitLeft = 496
    end
    inherited laPeriod: TLabel
      Left = 392
      Visible = False
      ExplicitLeft = 392
    end
    object lbYear: TLabel [2]
      Left = 8
      Top = 7
      Width = 73
      Height = 13
      Caption = #1042#1099#1073#1080#1088#1080#1090#1077' '#1075#1086#1076' '
      Transparent = True
    end
    object lbComp: TLabel [3]
      Left = 160
      Top = 7
      Width = 67
      Height = 13
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
      Transparent = True
      Visible = False
    end
    object edYear: TDBNumberEditEh [4]
      Left = 81
      Top = 4
      Width = 60
      Height = 19
      EditButton.Style = ebsUpDownEh
      EditButton.Visible = True
      EditButtons = <>
      Flat = True
      MaxValue = 2050.000000000000000000
      MinValue = 2000.000000000000000000
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnChange = edYearChange
      OnKeyDown = edYearKeyDown
    end
    object cbComp: TDBComboBoxEh [5]
      Left = 231
      Top = 4
      Width = 134
      Height = 19
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 1
      Text = 'cbComp'
      Visible = True
      OnChange = cbCompChange
    end
    inherited spitDep: TSpeedItem
      Visible = False
    end
    inherited spitPeriod: TSpeedItem
      Visible = False
    end
  end
  inherited tb1: TSpeedBar
    Width = 676
    ExplicitWidth = 676
    inherited spitPrint: TSpeedItem [4]
      Visible = False
      OnClick = acPrintDocExecute
    end
    inherited spitEdit: TSpeedItem [5]
      OnClick = acEditDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    Width = 676
    Height = 312
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    SumList.Active = True
    UseMultiTitle = True
    Columns = <
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1040#1082#1090'|'#1053#1086#1084#1077#1088
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1040#1082#1090'|'#1044#1072#1090#1072
        Width = 72
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'BD'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076'|'#1053#1072#1095#1072#1083#1086
        Width = 63
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'ED'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076'|'#1050#1086#1085#1077#1094
        Width = 72
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SUPNAME'
        Footers = <>
        Width = 119
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
        Width = 96
      end>
  end
  inherited ilButtons: TImageList
    Top = 192
  end
  inherited ppDep: TPopupMenu
    Top = 92
  end
  inherited acAEvent: TActionList
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
    inherited acPrintDoc: TAction
      OnExecute = acPrintDocExecute
    end
  end
  inherited ppDoc: TTBPopupMenu
    Left = 212
    Top = 132
  end
end
