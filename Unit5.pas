unit Unit5;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Provider, DBClient, FR_DSet, FR_DBSet, FIBDatabase, pFIBDatabase, FR_Class,
  ImgList, FIBQuery, pFIBQuery, pFIBStoredProc, FIBDataSet, cxHint,
  pFIBDataSet, ActnList, strUtils, dxmdaset,
  StdCtrls, Grids, DBGrids, DBGridEhGrouping, GridsEh, DBGridEh;

type
  TForm5 = class(TForm)
    DBGridEh1: TDBGridEh;
    Label1: TLabel;
    ds: TDataSource;
  private

  public
    class procedure Execute(DataSet: TDataSet);
  end;

var
  Form5: TForm5;

implementation


{$R *.dfm}

class procedure TForm5.Execute(DataSet: TDataSet);
begin
  Form5 := nil;
  try
    Form5 := TForm5.Create(nil);
    Form5.ds.DataSet := DataSet;
    Form5.ShowModal;
  finally
    FreeAndNil(Form5);
  end;
end;

end.
