unit LostStones_det;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList,  ExtCtrls, ComCtrls,
  Grids, DBGridEh, dbUtil, ActnList, DBGridEhGrouping, GridsEh, rxPlacemnt,
  rxSpeedbar;

type
  TfmLostStones_det = class(TfmAncestor)
    DBGridEh1: TDBGridEh;
    SpeedItem1: TSpeedItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SpeedItem1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmLostStones_det: TfmLostStones_det;

implementation

uses InvData, PrintData;

{$R *.dfm}                  

procedure TfmLostStones_det.FormCreate(Sender: TObject);
begin
  inherited;
  OpenDataSet(dmInv.taAllLostStones);

end;

procedure TfmLostStones_det.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CloseDataSet(dmInv.taAllLostStones);
end;

procedure TfmLostStones_det.SpeedItem1Click(Sender: TObject);
begin
  inherited;
  dmPrint.PrintDocumentA(dkLostStones,null);
end;

end.
