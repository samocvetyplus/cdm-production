unit eOperOrder;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Editor, DBCtrlsEh, StdCtrls, Mask, Buttons, ExtCtrls, DateUtils;

type
  TfmOperOrder = class(TfmEditor)
    Label1: TLabel;
    Label2: TLabel;
    edNo: TDBEditEh;
    edDate: TDBDateTimeEditEh;
    Label3: TLabel;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmOperOrder: TfmOperOrder;

implementation

{$R *.dfm}

procedure TfmOperOrder.FormCreate(Sender: TObject);
begin
  inherited;
  edDate.Value:=Today;
end;

end.
