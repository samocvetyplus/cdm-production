inherited fmOperOut: TfmOperOut
  Caption = #1056#1077#1079#1091#1083#1100#1090#1072#1090' '#1086#1087#1077#1088#1072#1094#1080#1080
  PixelsPerInch = 96
  TextHeight = 13
  inherited plDict: TPanel
    inherited gridDict: TDBGridEh
      DataSource = dm.dsrOperOut
      FrozenCols = 2
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Width = 30
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'OPERATION'
          Footers = <>
          Width = 115
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'SEMISNAME'
          Footers = <>
          Width = 131
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          DropDownRows = 15
          DropDownSpecRow.ShortCut = 16573
          DropDownSpecRow.Visible = True
          DropDownWidth = -1
          EditButtons = <>
          FieldName = 'MATNAME'
          Footers = <>
          Width = 99
        end>
    end
  end
end
