{********************************************}
{  ������ ����� �������� ����������� ������  }
{  Copyrigth (C) 2004 basile for CDM         }
{  create 26.01.2004                         }
{  last update 26.01.2004                    }
{********************************************}
unit SChargeList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ListAncestor,  TB2Item, ActnList, Menus,
  ImgList, Grids, DBGridEh, StdCtrls, ExtCtrls, ComCtrls,
  DBGridEhGrouping, rxPlacemnt, GridsEh, rxSpeedbar;

type
  TfmSChargeList = class(TfmListAncestor)
    procedure acAddDocExecute(Sender: TObject);
    procedure spitEditClick(Sender: TObject);
    procedure gridDocListGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure FormCreate(Sender: TObject);
  public
    procedure DepClick(Sender : TObject); override;
  end;

var
  fmSChargeList: TfmSChargeList;

implementation

uses MainData, SCharge, dbUtil, fmUtils, DictData, DocAncestor;

{$R *.dfm}

procedure TfmSChargeList.acAddDocExecute(Sender: TObject);
begin
  inherited;
  with dmMain do
  begin
    PostDataSet(taSChargeList);
    ShowDocForm(TfmSCharge, taSCharge, taSChargeListINVID.AsInteger, dmMain.quTmp);
    //ShowAndFreeForm(TfmSCharge, TForm(fmSCharge));
    PostDataSet(taSChargeList);
    RefreshDataSet(taSChargeList);
  end;
end;

procedure TfmSChargeList.spitEditClick(Sender: TObject);
begin
  inherited;
  with dmMain do
  begin
    taSChargeList.Edit;
    ShowDocForm(TfmSCharge, taSCharge, taSChargeListINVID.AsInteger, dmMain.quTmp);
    //ShowAndFreeForm(TfmSCharge, TForm(fmSCharge));
    taSChargeList.Transaction.CommitRetaining;
    RefreshDataSet(taSChargeList);
  end;
end;

procedure TfmSChargeList.gridDocListGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if DataSet.FieldByName('ISCLOSE').AsInteger = 1 then Background := clCloseDoc
  else begin
    if(Column.FieldName = 'REFDOCNO')or(Column.FieldName = 'REFDOCDATE')
    then Background := clSkyBlue
    else Background := clOpenDoc;
  end
end;

procedure TfmSChargeList.DepClick(Sender: TObject);
begin
  dm.CurrDep := TMenuItem(Sender).Tag;
  inherited;
end;

procedure TfmSChargeList.FormCreate(Sender: TObject);
var
  i, j : integer;
  Item : TMenuItem;
begin
  dm.CurrDep := dm.DepDef;
  FBD := dm.whBeginDate;
  FED := dm.whEndDate;
  ShowPeriod;
  inherited;
  //���������� PopupMenu �� FDepInfo;
  j := 0;
  for i:=0 to Pred(dm.CountDep) do
  begin
    if (dm.DepInfo[i].DepTypes and $2)=$2 then
    begin
      Item := TMenuItem.Create(ppDep);
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepClick;
      Item.RadioItem := True;
      if Item.Tag = dm.DepDef then DepClick(Item);
      inc(j);
      if j>30 then
      begin
        Item.Break := mbBreak;
        j:=0;
      end;
      ppDep.Items.Add(Item);
    end;
  end;
end;

end.
