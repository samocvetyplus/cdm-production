unit dPriceList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ActnList,  ImgList,  ExtCtrls,
  ComCtrls, Grids, DBGridEh, DBUtil, StdCtrls, Mask, DBCtrlsEh, Buttons, DB,
  DBGridEhGrouping, GridsEh, rxPlacemnt, rxSpeedbar;

type
  TfmPriceList = class(TfmAncestor)
    DBGridEh1: TDBGridEh;
    Label1: TLabel;
    edArt: TDBEditEh;
    Label2: TLabel;
    edPrice: TDBEditEh;
    SpeedButton1: TSpeedButton;
    acSetPrice: TAction;
    acFilter: TAction;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure edArtChange(Sender: TObject);
    procedure acFilterExecute(Sender: TObject);
    procedure acSetPriceExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmPriceList: TfmPriceList;

implementation

uses InvData, DictData;

{$R *.dfm}

procedure TfmPriceList.FormCreate(Sender: TObject);
begin
  inherited;
  OpenDataSet(dminv.taPriceList);
end;

procedure TfmPriceList.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  CloseDataSet(dminv.taPriceList);
end;

procedure TfmPriceList.edArtChange(Sender: TObject);
begin
  inherited;
  dmINV.AArt := edArt.Text;
  ReOpenDataSet(dminv.taPriceList);
end;

procedure TfmPriceList.acFilterExecute(Sender: TObject);
begin
  inherited;
  ReOpenDataSet(dminv.taPriceList);
end;

procedure TfmPriceList.acSetPriceExecute(Sender: TObject);
var Price:single;
begin
  inherited;
try
  Price:=StrToFloat(edPrice.Text);
 except
   ShowMessage('�������� ����');
 end;
 if dminv.taPriceList.State in [dsEdit, dsInsert] then
 dminv.taPriceList.Post;
 dminv.taPriceList.First;
 while not dminv.taPriceList.Eof do
 begin
  dminv.taPriceList.Edit;
  dminv.taPriceListPRICE.AsFloat:=Price;
  dminv.taPriceList.Post;
  dminv.taPriceList.Next;
 end;

end;

end.
