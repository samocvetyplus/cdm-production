inherited fmDSemis: TfmDSemis
  Left = 277
  Top = 198
  ActiveControl = edQ
  Caption = #1056#1077#1076#1072#1082#1090#1086#1088' '#1074#1077#1089#1072
  ClientHeight = 115
  ClientWidth = 229
  KeyPreview = True
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 229
    Height = 115
    object lbQ: TLabel [0]
      Left = 12
      Top = 12
      Width = 59
      Height = 13
      Caption = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbW: TLabel [1]
      Left = 12
      Top = 32
      Width = 19
      Height = 13
      Caption = #1042#1077#1089
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object lbOper: TLabel [2]
      Left = 12
      Top = 52
      Width = 50
      Height = 13
      Caption = #1054#1087#1077#1088#1072#1094#1080#1103
    end
    inherited btnOk: TBitBtn
      Left = 52
      Top = 80
      TabOrder = 3
    end
    inherited btnCancel: TBitBtn
      Left = 140
      Top = 80
      TabOrder = 4
    end
    object cmbxOper: TDBComboBoxEh
      Left = 80
      Top = 48
      Width = 141
      Height = 19
      EditButton.ShortCut = 16397
      EditButtons = <>
      Flat = True
      TabOrder = 2
      Visible = True
    end
    object edW: TDBNumberEditEh
      Left = 80
      Top = 28
      Width = 137
      Height = 19
      DecimalPlaces = 3
      DisplayFormat = '0.###'
      EditButtons = <>
      Flat = True
      TabOrder = 1
      Visible = True
    end
    object edQ: TDBNumberEditEh
      Left = 80
      Top = 8
      Width = 137
      Height = 19
      DecimalPlaces = 0
      EditButton.Style = ebsUpDownEh
      EditButtons = <>
      Flat = True
      TabOrder = 0
      Visible = True
    end
  end
end
