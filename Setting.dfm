object fmSetting: TfmSetting
  Left = 80
  Top = 42
  Caption = #1053#1072#1089#1090#1088#1086#1081#1082#1072
  ClientHeight = 697
  ClientWidth = 914
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object plSetup: TPanel
    Left = 0
    Top = 0
    Width = 914
    Height = 697
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 169
      Top = 0
      Height = 697
    end
    object plOrg: TPanel
      Tag = 1
      Left = 189
      Top = 5
      Width = 580
      Height = 441
      BevelInner = bvLowered
      TabOrder = 0
      object plDep: TPanel
        Left = 2
        Top = 29
        Width = 576
        Height = 410
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Splitter2: TSplitter
          Left = 169
          Top = 0
          Height = 410
        end
        object plParams: TPanel
          Left = 172
          Top = 0
          Width = 404
          Height = 410
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label2: TLabel
            Left = 6
            Top = 27
            Width = 106
            Height = 13
            Caption = #1062#1074#1077#1090' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1103
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object Label8: TLabel
            Left = 6
            Top = 4
            Width = 115
            Height = 13
            AutoSize = False
            Caption = #1050#1088#1072#1090'. '#1085#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object edSName: TDBEdit
            Left = 124
            Top = 2
            Width = 173
            Height = 21
            BevelInner = bvSpace
            BevelKind = bkFlat
            BevelWidth = 2
            BorderStyle = bsNone
            DataField = 'SNAME'
            DataSource = dm.dsrDep
            TabOrder = 0
          end
          object dbclrDep: TM207DBColor
            Left = 125
            Top = 24
            Width = 172
            Height = 19
            Alignment = taCenter
            BorderStyle = bsNone
            Color = clWhite
            DirectInput = False
            NumGlyphs = 1
            TabOrder = 1
            Text = #1062#1074#1077#1090
            DataField = 'COLOR'
            DataSource = dm.dsrDep
          end
          object Panel2: TPanel
            Left = 0
            Top = 291
            Width = 404
            Height = 20
            Align = alBottom
            BevelInner = bvLowered
            Caption = #1057#1087#1080#1089#1086#1082' '#1083#1102#1076#1077#1081', '#1079#1072#1082#1088#1077#1087#1083#1077#1085#1085#1099#1093' '#1079#1072' '#1084#1077#1089#1090#1086#1084' '#1093#1088#1072#1085#1077#1085#1085#1080#1103
            Color = clMoneyGreen
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 8
          end
          object plListOper: TPanel
            Left = 0
            Top = 162
            Width = 404
            Height = 17
            Align = alBottom
            BevelInner = bvLowered
            Caption = #1057#1087#1080#1089#1086#1082' '#1086#1087#1077#1088#1072#1094#1080#1080#1081', '#1079#1072#1082#1088#1077#1087#1083#1077#1085#1085#1099#1093' '#1079#1072' '#1084#1077#1089#1090#1086#1084' '#1093#1088#1072#1085#1077#1085#1080#1103
            Color = clMoneyGreen
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
          object btsDepTypes: TM207DBBits
            Left = 6
            Top = 48
            Width = 195
            Height = 81
            BorderStyle = bsNone
            Color = clBtnFace
            ItemHeight = 13
            Items.Strings = (
              #1052#1077#1089#1090#1086' '#1093#1088#1072#1085#1077#1085#1080#1103' ('#1088#1072#1073#1086#1095#1080#1077' '#1084#1077#1089#1090#1086')'
              #1050#1083#1072#1076#1086#1074#1072#1103' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077
              'C'#1082#1083#1072#1076' '#1075#1086#1090#1086#1074#1086#1081' '#1087#1088#1086#1076#1091#1082#1094#1080#1080
              #1057#1082#1083#1072#1076' '#1087#1086#1083#1091#1092#1072#1073#1088#1080#1082#1072#1090#1086#1074
              #1056#1072#1089#1092#1086#1088#1084#1080#1088#1086#1074#1072#1085#1086
              #1059#1095#1072#1089#1090#1086#1082' '#1089#1098#1077#1084#1072)
            TabOrder = 2
            DataField = 'DEPTYPES'
            DataSource = dm.dsrDep
          end
          object TBDock2: TTBDock
            Left = 0
            Top = 179
            Width = 404
            Height = 26
            Position = dpBottom
            object tlbrOper: TTBToolbar
              Left = 0
              Top = 0
              BorderStyle = bsNone
              DockMode = dmCannotFloat
              Images = im16
              TabOrder = 0
              object TBItem5: TTBItem
                Action = acAddOper
              end
              object TBItem4: TTBItem
                Action = acDelOper
              end
            end
          end
          object gridOperList: TDBGridEh
            Left = 0
            Top = 205
            Width = 404
            Height = 86
            Align = alBottom
            AllowedOperations = [alopUpdateEh]
            ColumnDefValues.Title.TitleButton = True
            DataGrouping.GroupLevels = <>
            DataSource = dm.dsrPsOper
            Flat = True
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            FrozenCols = 1
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
            RowDetailPanel.Color = clBtnFace
            TabOrder = 4
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnGetCellParams = gridOperListGetCellParams
            Columns = <
              item
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'RecNo'
                Footers = <>
                Width = 25
              end
              item
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'OPERNAME'
                Footers = <>
                Width = 183
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
          object grPs: TDBGridEh
            Left = 0
            Top = 337
            Width = 404
            Height = 73
            Align = alBottom
            AllowedOperations = [alopUpdateEh]
            ColumnDefValues.Title.TitleButton = True
            DataGrouping.GroupLevels = <>
            DataSource = dm.dsrPs
            Flat = True
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'MS Sans Serif'
            FooterFont.Style = []
            FrozenCols = 1
            OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
            RowDetailPanel.Color = clBtnFace
            TabOrder = 5
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnGetCellParams = grPsGetCellParams
            Columns = <
              item
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'RecNo'
                Footers = <>
                Width = 27
              end
              item
                DropDownBox.ColumnDefValues.Title.TitleButton = True
                EditButtons = <>
                FieldName = 'MOLNAME'
                Footers = <>
                Width = 211
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
          object TBDock: TTBDock
            Left = 0
            Top = 311
            Width = 404
            Height = 26
            Position = dpBottom
            object tlbrPS: TTBToolbar
              Left = 0
              Top = 0
              BorderStyle = bsNone
              DockMode = dmCannotFloat
              Images = im16
              TabOrder = 0
              object TBItem7: TTBItem
                Action = acAddMOL
              end
              object TBItem6: TTBItem
                Action = acDelMOL
              end
            end
          end
          object Button1: TButton
            Left = 212
            Top = 48
            Width = 169
            Height = 25
            Action = acFormCoverDepart
            TabOrder = 9
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 169
          Height = 410
          Align = alLeft
          BevelInner = bvRaised
          BevelOuter = bvLowered
          Caption = 'Panel3'
          TabOrder = 1
          object TBDock1: TTBDock
            Left = 2
            Top = 2
            Width = 165
            Height = 26
            object tlbrTree: TTBToolbar
              Left = 0
              Top = 0
              BorderStyle = bsNone
              DockMode = dmCannotFloat
              Images = im16
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              object TBItem2: TTBItem
                Action = acAddDepart
              end
              object TBItem1: TTBItem
                Action = acDelDepart
              end
              object TBItem3: TTBItem
                Action = acEditDepart
              end
              object TBSeparatorItem1: TTBSeparatorItem
              end
              object TBItem8: TTBItem
                Action = acSort
              end
            end
          end
          object tv1: TTreeView
            Left = 2
            Top = 28
            Width = 165
            Height = 380
            Align = alClient
            BorderStyle = bsNone
            Color = clWhite
            DragMode = dmAutomatic
            HideSelection = False
            Indent = 19
            PopupMenu = pmOrg
            RightClickSelect = True
            TabOrder = 1
            OnChange = tv1Change
            OnChanging = tv1Changing
            OnDeletion = tv1Deletion
            OnDragDrop = tv1DragDrop
            OnDragOver = tv1DragOver
            OnEdited = tv1Edited
            OnEditing = tv1Editing
            OnExit = tv1Exit
            OnExpanding = tv1Expanding
            OnGetImageIndex = tv1GetImageIndex
            OnGetSelectedIndex = tv1GetSelectedIndex
            OnStartDrag = tv1StartDrag
          end
        end
      end
      object plRoot: TPanel
        Left = 2
        Top = 2
        Width = 576
        Height = 27
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 7
          Top = 8
          Width = 229
          Height = 13
          Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103' '#1074' '#1082#1086#1090#1086#1088#1086#1081' '#1088#1072#1073#1086#1090#1072#1077#1090' '#1087#1088#1086#1075#1088#1072#1084#1084#1072
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object lbCompName: TLabel
          Left = 240
          Top = 8
          Width = 3
          Height = 13
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object lstvSetting: TListView
      Left = 0
      Top = 0
      Width = 169
      Height = 697
      Align = alLeft
      Columns = <>
      Constraints.MaxWidth = 250
      Constraints.MinWidth = 150
      Items.ItemData = {
        01A00000000400000000000000FFFFFFFFFFFFFFFF00000000000000000B1E04
        4004330430043D04380437043004460438044F0400000000FFFFFFFFFFFFFFFF
        0000000000000000091A043E043D044104420430043D0442044B0400000000FF
        FFFFFFFFFFFFFF0000000000000000091F043004400430043C04350442044004
        4B0400000000FFFFFFFFFFFFFFFF0000000000000000072804300431043B043E
        043D044B04}
      TabOrder = 1
      ViewStyle = vsList
      OnChange = lstvSettingChange
      OnClick = lstvSettingClick
    end
    object Panel1: TPanel
      Left = 480
      Top = 420
      Width = 205
      Height = 129
      BevelInner = bvLowered
      Caption = #1042#1099#1073#1080#1088#1080#1090#1077' '#1086#1076#1080#1085' '#1080#1079' '#1087#1091#1085#1082#1090#1086#1074
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object plConst: TPanel
      Tag = 2
      Left = 728
      Top = 160
      Width = 353
      Height = 297
      BevelInner = bvLowered
      TabOrder = 3
      DesignSize = (
        353
        297)
      object Label3: TLabel
        Left = 154
        Top = 3
        Width = 55
        Height = 13
        Anchors = [akTop]
        Caption = #1050#1086#1085#1089#1090#1072#1085#1090#1099
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label16: TLabel
        Left = 12
        Top = 200
        Width = 152
        Height = 26
        Caption = #1054#1087#1077#1088#1072#1094#1080#1103' '#1087#1086' '#1091#1084#1086#1083#1095#1072#1085#1080#1102' '#1076#1083#1103' '#13#10#1080#1079#1084#1077#1085#1077#1085#1080#1103' '#1087#1086' '#1087#1088'-'#1085#1086#1084#1091' '#1079#1072#1082#1072#1079#1091
      end
      object DBGridEh1: TDBGridEh
        Left = 2
        Top = 2
        Width = 349
        Height = 187
        Align = alTop
        AllowedOperations = [alopInsertEh, alopUpdateEh, alopDeleteEh]
        Color = clBtnFace
        ColumnDefValues.Title.TitleButton = True
        DataGrouping.GroupLevels = <>
        DataSource = dm.dsrConst
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'MS Sans Serif'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghAutoSortMarking, dghMultiSortMarking, dghIncSearch, dghRowHighlight, dghColumnResize, dghColumnMove]
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Color = 14673635
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'NAME'
            Footers = <>
            Title.Alignment = taCenter
          end
          item
            Color = 13757930
            DropDownBox.ColumnDefValues.Title.TitleButton = True
            EditButtons = <>
            FieldName = 'VAL'
            Footers = <>
            Title.Alignment = taCenter
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBNavigator1: TDBNavigator
        Left = 2
        Top = 16
        Width = 240
        Height = 25
        DataSource = dm.dsrConst
        Flat = True
        TabOrder = 1
      end
      object cmbxChangeApplOperId: TDBLookupComboboxEh
        Left = 176
        Top = 200
        Width = 145
        Height = 19
        DataField = 'CHANGEAPPLOPERID'
        DataSource = dm.dsrRec
        DropDownBox.Rows = 15
        EditButtons = <>
        Flat = True
        KeyField = 'OPERID'
        ListField = 'OPERATION'
        ListSource = dm.dsrOper
        TabOrder = 2
        Visible = True
      end
    end
    object Panel4: TPanel
      Tag = 3
      Left = 0
      Top = 68
      Width = 389
      Height = 521
      BevelInner = bvLowered
      TabOrder = 4
      object M207Bevel3: TM207Bevel
        Left = 10
        Top = 419
        Width = 305
        Height = 66
        Shape = bsFrame
        Caption = #1057#1082#1072#1085#1077#1088' '#1096#1088#1080#1093#1082#1086#1076#1072
        CaptionOffs = 6
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        BorderWidth = 0
      end
      object Label4: TLabel
        Left = 8
        Top = 53
        Width = 169
        Height = 13
        Caption = #1050#1086#1083'-'#1074#1086' '#1089#1090#1088#1086#1082' '#1074' '#1084#1072#1090#1088#1080#1094#1077' '#1086#1090#1075#1088#1091#1079#1082#1080
      end
      object Label5: TLabel
        Left = 8
        Top = 76
        Width = 187
        Height = 13
        Caption = #1050#1086#1083'-'#1074#1086' '#1089#1090#1086#1083#1073#1094#1086#1074' '#1074' '#1084#1072#1090#1088#1080#1094#1077' '#1086#1090#1075#1088#1091#1079#1082#1080
      end
      object lbNDS: TLabel
        Left = 12
        Top = 154
        Width = 77
        Height = 13
        Caption = #1057#1090#1072#1074#1082#1072' '#1053#1044#1057', %'
      end
      object Label6: TLabel
        Left = 8
        Top = 96
        Width = 237
        Height = 13
        Caption = #1055#1086#1088#1090' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103' '#1092#1080#1089#1082#1072#1083#1100#1085#1086#1075#1086' '#1088#1077#1075#1080#1089#1090#1088#1072#1090#1086#1088#1072
      end
      object Label7: TLabel
        Left = 8
        Top = 184
        Width = 96
        Height = 13
        Caption = #1055#1091#1090#1100' '#1082' '#1088#1072#1073#1086#1095#1077#1081' '#1041#1044
      end
      object Label9: TLabel
        Left = 8
        Top = 204
        Width = 99
        Height = 13
        Caption = #1055#1091#1090#1100' '#1082' '#1074#1085#1077#1096#1085#1077#1081' '#1041#1044
        Visible = False
      end
      object lbPortBarCodePrinter: TLabel
        Left = 16
        Top = 268
        Width = 197
        Height = 13
        Caption = #1055#1086#1088#1090' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103' '#1087#1088#1080#1085#1090#1077#1088#1072' '#1096#1088#1080#1093#1082#1086#1076#1072
      end
      object Label10: TLabel
        Left = 16
        Top = 252
        Width = 141
        Height = 13
        Caption = #1052#1086#1076#1077#1083#1100' '#1087#1088#1080#1085#1090#1077#1088#1072' '#1096#1088#1080#1093#1082#1086#1076#1072
      end
      object lbModelNameBarCodePrinter: TLabel
        Left = 224
        Top = 250
        Width = 79
        Height = 13
        Caption = 'GODEX EZ1100'
      end
      object M207Bevel1: TM207Bevel
        Left = 10
        Top = 312
        Width = 311
        Height = 105
        Shape = bsFrame
        Caption = #1042#1077#1089#1099
        CaptionOffs = 6
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        BorderWidth = 0
      end
      object Label11: TLabel
        Left = 16
        Top = 342
        Width = 72
        Height = 13
        Caption = #1052#1086#1076#1077#1083#1100' '#1074#1077#1089#1086#1074
      end
      object Label12: TLabel
        Left = 16
        Top = 358
        Width = 128
        Height = 13
        Caption = #1055#1086#1088#1090' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103' '#1074#1077#1089#1086#1074
      end
      object Label13: TLabel
        Left = 16
        Top = 372
        Width = 151
        Height = 39
        Caption = 
          #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1077#1088#1077#1076#1072#1095#1080'  9600'#1073#1086#1076#13#10#1041#1080#1090' '#1087#1072#1088#1080#1090#1077#1090#1072'            None'#13#10#1060#1091#1085#1082#1094#1080 +
          #1103' '#1087#1077#1095#1072#1090#1080'       INSTANT'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object M207Bevel2: TM207Bevel
        Left = 16
        Top = 220
        Width = 305
        Height = 89
        Shape = bsFrame
        Caption = #1055#1088#1080#1085#1090#1077#1088' '#1096#1088#1080#1093#1082#1086#1076#1072
        CaptionOffs = 6
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        BorderWidth = 0
      end
      object Label14: TLabel
        Left = 16
        Top = 452
        Width = 95
        Height = 13
        Caption = #1055#1086#1088#1090' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103
      end
      object Label15: TLabel
        Left = 16
        Top = 468
        Width = 148
        Height = 13
        Caption = #1057#1082#1086#1088#1086#1089#1090#1100' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1080#1103'  9600'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object lbBarCode_Step: TLabel
        Left = 16
        Top = 286
        Width = 168
        Height = 13
        Caption = #1050#1086#1084#1072#1085#1076#1072' '#1091#1087#1088#1072#1074#1083#1077#1085#1080#1077' '#1076#1074#1080#1078#1077#1085#1080#1077#1084
      end
      object chbxUseDotMatrix: TDBCheckBoxEh
        Left = 8
        Top = 8
        Width = 305
        Height = 17
        Caption = #1047#1072#1075#1088#1091#1078#1072#1090#1100' '#1087#1077#1095#1072#1090#1085#1099#1077' '#1092#1086#1088#1084#1099' '#1076#1083#1103' '#1084#1072#1090#1088#1080#1095#1085#1086#1075#1086' '#1087#1088#1080#1085#1090#1077#1088#1072
        Flat = True
        TabOrder = 0
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object edY_MatrixProd: TDBNumberEditEh
        Left = 208
        Top = 52
        Width = 57
        Height = 19
        DataField = 'X_MATRIXPROD'
        DataSource = dm.dsrRec
        EditButton.Style = ebsUpDownEh
        EditButton.Visible = True
        EditButtons = <>
        Flat = True
        MinValue = 1.000000000000000000
        TabOrder = 1
        Visible = True
      end
      object edX_MatrixProd: TDBNumberEditEh
        Left = 208
        Top = 71
        Width = 57
        Height = 19
        DataField = 'Y_MATRIXPROD'
        DataSource = dm.dsrRec
        EditButton.Style = ebsUpDownEh
        EditButton.Visible = True
        EditButtons = <>
        Flat = True
        MinValue = 1.000000000000000000
        TabOrder = 2
        Visible = True
      end
      object chbxShowDialogStone: TDBCheckBoxEh
        Left = 8
        Top = 28
        Width = 325
        Height = 17
        Caption = #1055#1086#1082#1072#1079#1099#1074#1072#1090#1100' '#1076#1080#1072#1083#1086#1075' '#1074#1099#1076#1072#1095#1080' '#1076#1088#1072#1075#1086#1094#1077#1085#1085#1099#1093' '#1082#1072#1084#1085#1077#1081' '#1074' '#1085#1072#1088#1103#1076#1077
        Flat = True
        TabOrder = 3
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object cmbxCassaNDS: TDBCheckBoxEh
        Left = 12
        Top = 132
        Width = 93
        Height = 17
        Alignment = taLeftJustify
        Caption = #1042#1082#1083#1102#1095#1072#1090#1100' '#1053#1044#1057
        Flat = True
        TabOrder = 4
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object edCassaNDS: TDBNumberEditEh
        Left = 94
        Top = 150
        Width = 59
        Height = 19
        DecimalPlaces = 0
        EditButton.ShortCut = 16397
        EditButton.Style = ebsUpDownEh
        EditButton.Visible = True
        EditButtons = <>
        Flat = True
        MaxValue = 100.000000000000000000
        TabOrder = 5
        Value = 18.000000000000000000
        Visible = True
      end
      object cmbxCOM: TDBComboBoxEh
        Left = 252
        Top = 92
        Width = 73
        Height = 19
        EditButtons = <>
        Flat = True
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4')
        TabOrder = 6
        Visible = True
      end
      object edDbPath: TDBEditEh
        Left = 116
        Top = 180
        Width = 181
        Height = 19
        DataField = 'DBPATH'
        DataSource = dm.dsrRec
        EditButtons = <>
        Flat = True
        TabOrder = 7
        Visible = True
      end
      object edExtDbPath: TDBEditEh
        Left = 116
        Top = 200
        Width = 181
        Height = 19
        DataField = 'EXTDBPATH'
        DataSource = dm.dsrRec
        EditButtons = <>
        Flat = True
        TabOrder = 8
        Visible = False
      end
      object cmbxPortBarCodePrinter: TDBComboBoxEh
        Left = 224
        Top = 265
        Width = 73
        Height = 19
        EditButtons = <>
        Flat = True
        Items.Strings = (
          'LPT1'
          'COM1'
          'COM2'
          'COM3'
          'COM4'
          'LPT2')
        TabOrder = 9
        Visible = True
      end
      object chbxUseScale: TDBCheckBoxEh
        Left = 16
        Top = 324
        Width = 221
        Height = 17
        Alignment = taLeftJustify
        Caption = #1042#1077#1089#1099' '#1087#1086#1076#1082#1083#1102#1095#1077#1085#1099
        Flat = True
        TabOrder = 10
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object cmbxPortScale: TDBComboBoxEh
        Left = 224
        Top = 364
        Width = 73
        Height = 19
        EditButtons = <>
        Flat = True
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4')
        TabOrder = 11
        Visible = True
      end
      object chbxUseBarCodePrinter: TDBCheckBoxEh
        Left = 16
        Top = 232
        Width = 221
        Height = 17
        Alignment = taLeftJustify
        Caption = #1055#1088#1080#1085#1090#1077#1088' '#1096#1088#1080#1093#1082#1086#1076#1072' '#1087#1086#1076#1082#1083#1102#1095#1077#1085
        Flat = True
        TabOrder = 12
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object chbxUseBarCodeScaner: TDBCheckBoxEh
        Left = 16
        Top = 432
        Width = 221
        Height = 17
        Alignment = taLeftJustify
        Caption = #1057#1082#1072#1085#1077#1088' '#1096#1088#1080#1093#1082#1086#1076#1072' '#1087#1086#1076#1082#1083#1102#1095#1077#1085
        Flat = True
        TabOrder = 13
        ValueChecked = 'True'
        ValueUnchecked = 'False'
      end
      object cmbxPortBarCodeScaner: TDBComboBoxEh
        Left = 224
        Top = 452
        Width = 73
        Height = 19
        EditButtons = <>
        Flat = True
        Items.Strings = (
          'COM1'
          'COM2'
          'COM3'
          'COM4')
        TabOrder = 14
        Visible = True
      end
      object edBarCodePrinterStep: TDBEditEh
        Left = 224
        Top = 284
        Width = 73
        Height = 19
        EditButtons = <>
        Flat = True
        TabOrder = 15
        Visible = True
      end
      object cbScaleClass: TDBComboBoxEh
        Left = 224
        Top = 340
        Width = 89
        Height = 19
        EditButtons = <>
        Flat = True
        Items.Strings = (
          #1055#1045#1058#1042#1045#1057' '#1045'410'#1052
          'Shinko HTR-80CE')
        TabOrder = 16
        Visible = True
      end
    end
    object PanelPatternSettings: TPanel
      Tag = 4
      Left = 219
      Top = 40
      Width = 598
      Height = 334
      Caption = 'PanelPatternSettings'
      TabOrder = 5
      Visible = False
      object Layout: TdxLayoutControl
        Left = 1
        Top = 1
        Width = 596
        Height = 332
        Align = alClient
        TabOrder = 0
        TabStop = False
        LayoutLookAndFeel = dxLayoutCxLookAndFeel
        object EditPriceFilePath: TcxDBButtonEdit
          Left = 271
          Top = 62
          BeepOnEnter = False
          DataBinding.DataField = 'FILE$NAME'
          DataBinding.DataSource = SourceSettings
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.ValidateOnEnter = False
          Properties.OnButtonClick = PropertiesButtonClick
          Style.HotTrack = False
          TabOrder = 1
          Width = 121
        end
        object EditPatternExportFolder: TcxDBButtonEdit
          Left = 271
          Top = 89
          DataBinding.DataField = 'OUTPUT$DIRECTORY'
          DataBinding.DataSource = SourceSettings
          Properties.Buttons = <
            item
              Default = True
              Kind = bkEllipsis
            end>
          Properties.OnButtonClick = PropertiesButtonClick
          Style.HotTrack = False
          TabOrder = 2
          Width = 121
        end
        object EditDecimalDigits: TcxDBSpinEdit
          Left = 271
          Top = 146
          DataBinding.DataField = 'DECIMAL$DIGITS'
          DataBinding.DataSource = SourceSettings
          Properties.UseLeftAlignmentOnEditing = False
          Style.HotTrack = False
          TabOrder = 3
          Width = 121
        end
        object EditMaxSkipCellCount: TcxDBSpinEdit
          Left = 271
          Top = 173
          DataBinding.DataField = 'MAX$SKIP$CELLS$COUNT'
          DataBinding.DataSource = SourceSettings
          Properties.MaxValue = 10.000000000000000000
          Properties.UseLeftAlignmentOnEditing = False
          Style.HotTrack = False
          TabOrder = 4
          Width = 121
        end
        object CheckBoxIgnoreZeroPrice: TcxDBCheckBox
          Left = 44
          Top = 218
          Caption = #1048#1075#1085#1086#1088#1080#1088#1086#1074#1072#1090#1100' '#1072#1088#1090#1080#1082#1091#1083#1099' '#1089' '#1085#1091#1083#1077#1074#1086#1081' '#1094#1077#1085#1086#1081
          DataBinding.DataField = 'IGNORE$ZERO$PRICE'
          DataBinding.DataSource = SourceSettings
          Properties.Alignment = taLeftJustify
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Style.HotTrack = False
          TabOrder = 5
          Width = 121
        end
        object CheckBoxIgnoreZeroAverageWeight: TcxDBCheckBox
          Left = 44
          Top = 272
          Caption = #1048#1075#1085#1086#1088#1080#1088#1086#1074#1072#1090#1100' '#1072#1088#1090#1080#1082#1091#1083#1099' '#1089' '#1085#1091#1083#1077#1074#1099#1084' '#1089#1088#1077#1076#1085#1080#1084' '#1074#1077#1089#1086#1084
          DataBinding.DataField = 'IGNORE$ZERO$WEIGHT'
          DataBinding.DataSource = SourceSettings
          Properties.Alignment = taLeftJustify
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Style.HotTrack = False
          TabOrder = 7
          Width = 121
        end
        object CheckBoxIgnoreNotIsANumber: TcxDBCheckBox
          Left = 44
          Top = 245
          Caption = #1048#1075#1085#1086#1088#1080#1088#1086#1074#1072#1090#1100' '#1072#1088#1090#1080#1082#1091#1083#1099', '#1089#1086#1076#1077#1088#1078#1072#1097#1080#1077' '#1085#1077' '#1094#1080#1092#1088#1099
          DataBinding.DataField = 'IGNORE$NAN$SYMBOLS'
          DataBinding.DataSource = SourceSettings
          Properties.Alignment = taLeftJustify
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
          Style.HotTrack = False
          TabOrder = 6
          Width = 121
        end
        object DockControl: TdxBarDockControl
          Left = 10000
          Top = 10000
          Width = 100
          Height = 22
          Align = dalTop
          BarManager = BarManadger
          Visible = False
        end
        object Grid: TcxGrid
          Left = 10000
          Top = 10000
          Width = 363
          Height = 205
          Align = alClient
          TabOrder = 9
          Visible = False
          object View: TcxGridDBTableView
            NavigatorButtons.ConfirmDelete = False
            DataController.DataSource = SourcePatterns
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnFiltering = False
            OptionsCustomize.ColumnGrouping = False
            OptionsView.NoDataToDisplayInfoText = '<>'
            OptionsView.GroupByBox = False
            object ViewNAME: TcxGridDBColumn
              Caption = #1053#1072#1079#1074#1072#1085#1080#1077' '#1074' '#1087#1088#1072#1081#1089#1077
              DataBinding.FieldName = 'NAME'
              Width = 364
            end
            object ViewPATTERNORDER: TcxGridDBColumn
              Caption = #1055#1086#1088#1103#1076#1086#1082' '#1087#1088#1080' '#1074#1099#1075#1088#1091#1079#1082#1077
              DataBinding.FieldName = 'PATTERN$ORDER'
              Width = 128
            end
            object ViewFIELDNAME: TcxGridDBColumn
              Caption = #1048#1084#1103' '#1087#1086#1083#1103' '#1074' '#1076#1072#1090#1072#1089#1077#1090#1077
              DataBinding.FieldName = 'FIELD$NAME'
              Width = 122
            end
            object ViewISTOLLING: TcxGridDBColumn
              Caption = #1055#1077#1088#1077#1088#1072#1073#1086#1090#1082#1072
              DataBinding.FieldName = 'is$Tolling'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.ValueChecked = 1
              Properties.ValueGrayed = 'null'
              Properties.ValueUnchecked = 0
              Width = 79
            end
          end
          object Level: TcxGridLevel
            GridView = View
          end
        end
        object LayoutGroupRoot: TdxLayoutGroup
          AlignHorz = ahClient
          AlignVert = avClient
          LayoutLookAndFeel = dxLayoutCxLookAndFeel
          ButtonOptions.Buttons = <>
          Hidden = True
          LayoutDirection = ldTabbed
          ShowBorder = False
          object GroupSettings: TdxLayoutGroup
            AlignHorz = ahClient
            AlignVert = avClient
            CaptionOptions.Text = #1053#1072#1089#1090#1088#1086#1081#1082#1080
            LayoutLookAndFeel = dxLayoutCxLookAndFeel
            ButtonOptions.Buttons = <>
            object GroupFilesAndFolders: TdxLayoutGroup
              AlignHorz = ahClient
              AlignVert = avTop
              CaptionOptions.Text = #1055#1091#1090#1080' '#1080' '#1076#1080#1088#1077#1082#1090#1086#1088#1080#1080
              LayoutLookAndFeel = dxLayoutCxLookAndFeel
              ButtonOptions.Buttons = <>
              ItemIndex = 1
              object ItemPriceFilePath: TdxLayoutItem
                CaptionOptions.Text = #1060#1072#1081#1083' '#1087#1088#1072#1081#1089#1072
                LayoutLookAndFeel = dxLayoutCxLookAndFeel
                Control = EditPriceFilePath
                ControlOptions.ShowBorder = False
              end
              object ItemPatternExportFolder: TdxLayoutItem
                CaptionOptions.Text = #1050#1072#1090#1072#1083#1086#1075' '#1076#1083#1103' '#1096#1072#1073#1083#1086#1085#1086#1074
                LayoutLookAndFeel = dxLayoutCxLookAndFeel
                Control = EditPatternExportFolder
                ControlOptions.ShowBorder = False
              end
            end
            object GroupOptions: TdxLayoutGroup
              AlignHorz = ahClient
              AlignVert = avTop
              CaptionOptions.Text = #1053#1072#1089#1090#1088#1086#1081#1082#1080
              LayoutLookAndFeel = dxLayoutCxLookAndFeel
              ButtonOptions.Buttons = <>
              object ItemDecimalDigits: TdxLayoutItem
                CaptionOptions.Text = #1050#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1076#1077#1089#1103#1090#1080#1095#1085#1099#1093' '#1079#1085#1072#1082#1086#1074
                LayoutLookAndFeel = dxLayoutCxLookAndFeel
                Control = EditDecimalDigits
                ControlOptions.ShowBorder = False
              end
              object ItemMaxSkipCellCount: TdxLayoutItem
                CaptionOptions.Text = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1086#1077' '#1082#1086#1083#1080#1095#1077#1089#1090#1074#1086' '#1085#1077#1079#1085#1072#1095#1072#1097#1080#1093' '#1089#1090#1088#1086#1082
                LayoutLookAndFeel = dxLayoutCxLookAndFeel
                Control = EditMaxSkipCellCount
                ControlOptions.ShowBorder = False
              end
              object GroupAdditional: TdxLayoutGroup
                CaptionOptions.Text = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1077
                LayoutLookAndFeel = dxLayoutCxLookAndFeel
                ButtonOptions.Buttons = <>
                object ItemIgnoreZeroPrice: TdxLayoutItem
                  CaptionOptions.Text = 'cxDBCheckBox1'
                  CaptionOptions.Visible = False
                  LayoutLookAndFeel = dxLayoutCxLookAndFeel
                  Control = CheckBoxIgnoreZeroPrice
                  ControlOptions.ShowBorder = False
                end
                object ItemIgnoreNotIsANumber: TdxLayoutItem
                  CaptionOptions.Text = 'cxDBCheckBox3'
                  CaptionOptions.Visible = False
                  LayoutLookAndFeel = dxLayoutCxLookAndFeel
                  Control = CheckBoxIgnoreNotIsANumber
                  ControlOptions.ShowBorder = False
                end
                object ItemIgnoreZeroAverageWeight: TdxLayoutItem
                  CaptionOptions.Text = 'cxDBCheckBox2'
                  CaptionOptions.Visible = False
                  LayoutLookAndFeel = dxLayoutCxLookAndFeel
                  Control = CheckBoxIgnoreZeroAverageWeight
                  ControlOptions.ShowBorder = False
                end
              end
            end
          end
          object GroupPatterns: TdxLayoutGroup
            AlignHorz = ahClient
            AlignVert = avClient
            CaptionOptions.Text = #1064#1072#1073#1083#1086#1085#1099
            LayoutLookAndFeel = dxLayoutCxLookAndFeel
            ButtonOptions.Buttons = <>
            object GroupMenu: TdxLayoutGroup
              AlignHorz = ahClient
              AlignVert = avTop
              CaptionOptions.Text = 'New Group'
              LayoutLookAndFeel = dxLayoutCxLookAndFeel
              ButtonOptions.Buttons = <>
              Hidden = True
              ShowBorder = False
              object ItemMenu: TdxLayoutItem
                LayoutLookAndFeel = dxLayoutCxLookAndFeel
                Control = DockControl
                ControlOptions.AutoColor = True
                ControlOptions.ShowBorder = False
              end
            end
            object GroupGrid: TdxLayoutGroup
              AlignHorz = ahClient
              AlignVert = avClient
              AllowRemove = False
              CaptionOptions.Text = 'New Group'
              LayoutLookAndFeel = dxLayoutCxLookAndFeel
              ButtonOptions.Buttons = <>
              Hidden = True
              ShowBorder = False
              object ItemGrid: TdxLayoutItem
                LayoutLookAndFeel = dxLayoutCxLookAndFeel
                Control = Grid
                ControlOptions.ShowBorder = False
              end
            end
          end
        end
      end
    end
  end
  object fmstr: TFormStorage
    UseRegistry = False
    StoredProps.Strings = (
      'plParams.Width')
    StoredValues = <>
    Left = 41
    Top = 599
  end
  object pmOrg: TPopupMenu
    Images = im16
    Left = 76
    Top = 600
    object mnitAddDep: TMenuItem
      Action = acAddDepart
    end
    object mnitDelDep: TMenuItem
      Action = acDelDepart
    end
    object mnitEditDep: TMenuItem
      Action = acEditDepart
    end
  end
  object acEvent: TActionList
    Images = im16
    Left = 4
    Top = 628
    object acAddDepart: TAction
      Category = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      ImageIndex = 0
      OnExecute = acAddDepartExecute
    end
    object acDelDepart: TAction
      Category = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      Hint = #1059#1076#1072#1083#1080#1090#1100' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      ImageIndex = 1
      OnExecute = acDelDepartExecute
    end
    object acEditDepart: TAction
      Category = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100
      Hint = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      ImageIndex = 2
      OnExecute = acEditDepartExecute
    end
    object acAddOper: TAction
      Category = #1054#1087#1077#1088#1072#1094#1080#1080
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      HelpKeyword = #1044#1086#1073#1072#1074#1080#1090#1100' '#1086#1087#1077#1088#1072#1094#1080#1102
      ImageIndex = 3
      OnExecute = acAddOperExecute
    end
    object acDelOper: TAction
      Category = #1054#1087#1077#1088#1072#1094#1080#1080
      Caption = #1059#1076#1072#1083#1080#1090#1100
      HelpKeyword = #1059#1076#1072#1083#1080#1090#1100' '#1086#1087#1077#1088#1072#1094#1080#1102
      ImageIndex = 4
      OnExecute = acDelOperExecute
      OnUpdate = acDelOperUpdate
    end
    object acAddMOL: TAction
      Category = #1052#1054#1051
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100
      ImageIndex = 3
      OnExecute = acAddMOLExecute
    end
    object acDelMOL: TAction
      Category = #1052#1054#1051
      Caption = #1059#1076#1072#1083#1080#1090#1100
      ImageIndex = 4
      OnExecute = acDelMOLExecute
      OnUpdate = acDelMOLUpdate
    end
    object acSort: TAction
      Category = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      Caption = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100
      Hint = #1057#1086#1088#1090#1080#1088#1086#1074#1072#1090#1100' '#1087#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1103
      ImageIndex = 5
      OnExecute = acSortExecute
    end
    object acFormCoverDepart: TAction
      Category = #1055#1086#1076#1088#1072#1079#1076#1077#1083#1077#1085#1080#1077
      Caption = #1057#1092#1086#1088#1084#1080#1088#1086#1074#1072#1090#1100' '#1091#1095#1072#1089#1090#1086#1082' '#1089#1098#1077#1084#1072
      OnExecute = acFormCoverDepartExecute
    end
    object AddPattern: TAction
      Category = #1064#1072#1073#1083#1086#1085#1099
      Caption = 'AddPattern'
      ImageIndex = 7
      OnExecute = AddPatternExecute
    end
    object DeletePattern: TAction
      Category = #1064#1072#1073#1083#1086#1085#1099
      Caption = 'DeletePattern'
      ImageIndex = 6
      OnExecute = DeletePatternExecute
    end
  end
  object im16: TImageList
    Left = 108
    Top = 600
    Bitmap = {
      494C010108000900040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000073000000730000006B000000630000006300000063000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      B5000000B5000008AD003939B5005A5AB5005252AD002929940000006B000000
      6B0000006B0000000000000000000000000000000000000000001863AD001863
      AD001863AD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000010CE000810
      C6006B73DE00CED6F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6DE005A5A
      9C0000006B0000007B00000000000000000000000000000000001863AD00319C
      FF001863AD000000000000000000000000001084210010842100108421001084
      2100108421001084210010842100108421000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000007F00FF007F00FF007F00FF007F00FF000000000000
      000000000000000000000000000000000000000000000018DE000818D600ADB5
      EF00FFFFFF00FFFFFF00BDBDEF008C8CD6008C8CD600CECEEF00FFFFFF00FFFF
      FF008C8CBD0000006B00000073000000000000000000000000001863AD00319C
      FF001863AD000000000000000000000000001084210010842100108421001084
      2100108421001084210010842100108421000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000007F00FF36E289FF36E48BFF007F00FF000000000000
      000000000000000000000000000000000000000000000018DE00949CEF00FFFF
      FF00E7EFFF004A52CE000000AD001018BD001818B500000094005A5ABD00F7F7
      FF00FFFFFF006363A500000073000000000000000000000000001863AD00319C
      FF001863AD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000007F00FF47FBADFF47FCADFF007F00FF000000000000
      0000000000000000000000000000000000000018F7002142E700FFFFFF00F7FF
      FF00394ADE000000C6001018BD001018B5000808A50008089C0000008C005252
      BD00FFFFFF00DEDEEF0008087B0000007B0000000000000000001863AD00319C
      FF001863AD000000000000000000000000001084210010842100108421001084
      2100108421001084210000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000007F00FF00EF7CFF00EF7CFF007F00FF000000000000
      0000000000000000000000000000000000000018F7007384F700FFFFFF0094A5
      F7000008DE000008D6001018CE001818AD000808A5000008A50000009C000000
      9400ADADDE00FFFFFF005252AD0000007B0000000000000000001863AD00319C
      FF001863AD000000000000000000000000001084210010842100108421001084
      21001084210010842100000000000000000000000000000CABFF000CABFF000C
      ABFF000CABFF000CABFF000CABFF000CABFF000CABFF000CABFF000CABFF000C
      ABFF000CABFF000CABFF000CABFF000000000000000000000000000000000000
      00000000000000000000007F00FF2AE17BFF28E17AFF007F00FF000000000000
      0000000000000000000000000000000000000021FF00ADBDFF00FFFFFF004263
      F7001031EF00FFFFFF00FFFFFF00FFFFFF00F7F7FF00F7F7FF00F7F7FF000808
      A5005A5ABD00FFFFFF008484D60000007B0000000000000000001863AD00399C
      FF001863AD000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000CABFF2F51E0FF2D53
      E2FF2D56E4FF2D58E6FF2D5AE8FF2D5AE8FF2D5AE8FF2D5AE7FF2D58E6FF2D56
      E4FF2D52E2FF2E50DFFF000CABFF0000000000000000007F00FF007F00FF007F
      00FF007F00FF007F00FF007F00FF3CD878FF39D876FF007F00FF007F00FF007F
      00FF007F00FF007F00FF007F00FF000000000839FF00C6CEFF00FFFFFF00315A
      FF00214AFF00C6CEFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFEFFF001018
      B5004A4AC600FFFFFF009494DE000000A50000000000000000001863AD0042A5
      FF001863AD000000000000000000000000001084210010842100108421001084
      21000000000000000000000000000000000000000000000CABFF2558EBFF245D
      EEFF2461F3FF2465F6FF2467F7FF2468F8FF2467F8FF2465F6FF2462F3FF245E
      EFFF2458EBFF2553E6FF000CABFF0000000000000000279F31FF66D382FF61D4
      82FF5FD784FF5BD886FF4BD478FF47D375FF43D272FF4CD479FF52D57FFF4FD2
      78FF4DCE71FF4DCA6CFF1A9D25FF000000003963FF00C6D6FF00FFFFFF005A7B
      FF000029FF001039FF001029EF000821E7000018DE000810D6000000B5000000
      B500737BD600FFFFFF007B7BD6000000AD0000000000000000001863AD0052B5
      FF001863AD000000000000000000000000001084210010842100108421001084
      21000000000000000000000000000000000000000000000CABFF6A86EBFF6685
      EDFF6688EFFF668BF1FF668CF2FF668CF2FF668CF2FF668AF1FF6688EFFF6686
      EDFF6682EAFF6A81E8FF000CABFF0000000000000000279D2CFF71D17FFF6AD1
      7DFF68D27FFF66D882FF5CD37BFF54D076FF51CF73FF55D176FF59D577FF56CD
      70FF54C969FF54C664FF1D9B22FF000000001039FF00B5C6FF00FFFFFF00C6D6
      FF000029FF000021FF000829FF001031EF000018E7000010DE000810D6000818
      CE00DEDEF700FFFFFF003139CE000000AD0000000000000000001863AD0063C6
      FF001863AD000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000CABFF7687E6FF758B
      E8FF758FEBFF7592EDFF7595F0FF7595F0FF7596F1FF7595F1FF7595EFFF7592
      EEFF758EEBFF768BE8FF000CABFF0000000000000000015938FF268249FF2583
      4AFF25864CFF257F4BFF3BA159FF68D37DFF64D27BFF3DA05AFF267F4CFF2587
      4CFF25854BFF258449FF015D37FF00000000000000008CA5FF00FFFFFF00FFFF
      FF008CA5FF000021FF000029FF000021FF001839FF000018EF000018DE00A5AD
      F700FFFFFF00B5BDEF000000BD00000000001863AD001863AD001863AD007BCE
      FF001863AD001863AD001863AD00000000001084210010842100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000050A35AFF77D483FF73D380FF53A25CFF000000000000
      00000000000000000000000000000000000000000000738CFF00DEE7FF00FFFF
      FF00FFFFFF00B5C6FF00395AFF001039FF001039FF004A63FF00C6CEFF00FFFF
      FF00EFEFFF003142D6000000BD0000000000000000001863AD0094E7FF008CDE
      FF007BD6FF001863AD0000000000000000001084210010842100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000053A159FF86D489FF82D386FF57A15CFF000000000000
      00000000000000000000000000000000000000000000000000008CA5FF00E7EF
      FF00FFFFFF00FFFFFF00FFFFFF00E7EFFF00E7EFFF00FFFFFF00FFFFFF00DEE7
      FF003952E7000008CE00000000000000000000000000000000001863AD009CE7
      FF001863AD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005AA25DFF9ADC99FF97DB95FF5FA262FF000000000000
      0000000000000000000000000000000000000000000000000000000000008CA5
      FF00BDC6FF00EFEFFF00FFFFFF00FFFFFF00FFFFFF00E7EFFF008C9CF7001039
      E7000008CE000000000000000000000000000000000000000000000000001863
      AD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000005D9B63FF82C686FF80C784FF619B67FF000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008CA5FF008CA5FF0094A5FF007B94FF004A6BFF001842EF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000049984CFF3B973FFF3B973FFF4A984CFF000000000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000073000000730000006B000000630000006300000063000000
      0000000000000000000000000000000000000000000000000000CE630000CE63
      0000CE630000D66B00009C390800FFCE9400EFA55A00DE8C29009C3908000000
      0000000000000000000000000000000000000000000000000000CE630000CE63
      0000CE630000D66B00009C390800FFCE9400EFA55A00DE8C29009C3908000000
      0000000000000000000000000000000000000000000000000000CE630000CE63
      0000CE630000D66B00009C390800FFCE9400EFA55A00DE8C29009C3908000000
      0000000000000000000000000000000000000000000000000000000000000000
      B5000000B5000008AD003939B5005A5AB5005252AD002929940000006B000000
      6B0000006B000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      00000000000000000000000000000000000000000000000000000010CE000810
      C6006B73DE00CED6F700FFFFFF00FFFFFF00FFFFFF00FFFFFF00C6C6DE005A5A
      9C0000006B0000007B0000000000000000000000000000000000CE6300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000018DE000818D600ADB5
      EF00FFFFFF00FFFFFF00BDBDEF008C8CD6008C8CD600CECEEF00FFFFFF00FFFF
      FF008C8CBD0000006B0000007300000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      000000000000000000000000000000000000000000000018DE00949CEF00FFFF
      FF00E7EFFF004A52CE000000AD0000009C0000009400000094005A5ABD00F7F7
      FF00FFFFFF006363A50000007300000000000000000000000000CE630000CE63
      0000CE630000CE6300009C390800FFCE9400EFA55A00DE8C29009C3908000000
      0000000000000000000000000000000000000000000000000000CE630000CE63
      0000CE630000CE6300009C390800FFCE9400EFA55A00DE8C29009C3908000000
      0000000000000000000000000000000000000000000000000000CE630000CE63
      0000CE630000CE6300009C390800FFCE9400EFA55A00DE8C29009C3908000000
      0000000000000000000000000000000000000018F7002142E700FFFFFF00F7FF
      FF00394ADE000000C6001018BD00B5B5E700A5A5E7000808A50000008C005252
      BD00FFFFFF00DEDEEF0008087B0000007B000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000018F7007384F700FFFFFF0094A5
      F7000008DE000008D6001018CE00F7F7FF00DEE7F7000008AD0000009C000000
      9400ADADDE00FFFFFF005252AD0000007B000000000000000000CE6300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000021FF00ADBDFF00FFFFFF004263
      F7001031EF00A5ADF700BDBDF700FFFFFF00F7F7FF00B5BDEF00949CDE000808
      A5005A5ABD00FFFFFF008484D60000007B000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000839FF00C6CEFF00FFFFFF00315A
      FF00214AFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFEFFF001018
      B5004A4AC600FFFFFF009494DE000000A5000000000000000000CE630000CE63
      0000CE630000CE6300009C390800FFCE9400EFA55A00DE8C29009C3908000000
      0000000000000000000000000000000000000000000000000000CE630000CE63
      0000CE630000CE6300009C390800FFCE9400EFA55A00DE8C29009C3908000000
      0000000000000000000000000000000000000000000000000000CE630000CE63
      0000CE630000CE6300009C390800FFCE9400EFA55A00DE8C29009C3908000000
      0000000000000000000000000000000000003963FF00C6D6FF00FFFFFF005A7B
      FF000029FF00315AFF005A6BF700F7F7FF00E7E7FF004A5ADE003142D6000000
      B500737BD600FFFFFF007B7BD6000000AD000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000297B1800317B180000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000000000000000000000CE6300000000
      000000000000000000009C3908009C3908009C3908009C3908009C3908000000
      0000000000000000000000000000000000001039FF00B5C6FF00FFFFFF00C6D6
      FF000029FF000021FF001039FF00F7F7FF00E7E7FF000018DE000000CE000818
      CE00DEDEF700FFFFFF003139CE000000AD009C3908009C3908009C3908009C39
      08009C3908000000000000000000000000000000000000000000000000000000
      0000317B18003184180000000000000000009C3908009C3908009C3908009C39
      08009C3908000000000000000000000000000000000000000000000000000010
      DE000010DE00000000000010DE000010DE009C3908009C3908009C3908009C39
      08009C3908000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008CA5FF00FFFFFF00FFFF
      FF008CA5FF000021FF000029FF00426BFF004263FF000018EF000018DE00A5AD
      F700FFFFFF00B5BDEF000000BD00000000009C390800FFCE9400EFA55A00DE8C
      29009C390800000000000000000000000000000000000000000029841800298C
      290029942900298C290029842100317B18009C390800FFCE9400EFA55A00DE8C
      29009C3908000000000000000000000000000000000000000000000000000010
      DE000010DE000010DE000010DE000010DE009C390800FFCE9400EFA55A00DE8C
      29009C3908000000000000000000000000000000000000000000000000000000
      0000000000000039FF00000000000000000000000000738CFF00DEE7FF00FFFF
      FF00FFFFFF00B5C6FF00395AFF001039FF001039FF004A63FF00C6CEFF00FFFF
      FF00EFEFFF003142D6000000BD00000000009C3908009C3908009C3908009C39
      08009C390800000000000000000000000000000000000000000029841800298C
      2900299431002994290029842100317B18009C3908009C3908009C3908009C39
      08009C3908000000000000000000000000000000000000000000000000000000
      00000010DE000010DE000010DE00000000009C3908009C3908009C3908009C39
      08009C3908000000000000000000000000000000000000000000000000000000
      00000039FF00000000000039FF000000000000000000000000008CA5FF00E7EF
      FF00FFFFFF00FFFFFF00FFFFFF00E7EFFF00E7EFFF00FFFFFF00FFFFFF00DEE7
      FF003952E7000008CE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000299429002994290000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000010
      DE000010DE000010DE000010DE000010DE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000039FF000000000000000000000000008CA5
      FF00BDC6FF00EFEFFF00FFFFFF00FFFFFF00FFFFFF00E7EFFF008C9CF7001039
      E7000008CE000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000298421002984210000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000010
      DE000010DE00000000000010DE000010DE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000039FF000000000000000000000000000000
      0000000000008CA5FF008CA5FF0094A5FF007B94FF004A6BFF001842EF000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000F81FFFFFFFFFFFFFE007C7FFFFFFFFFF
      C003C700FFFFFC3F8001C700FFFFFC3F8001C7FFFFFFFC3F0000C703FFFFFC3F
      0000C7038001FC3F0000C7FF800180010000C70F800180010000C70F80018001
      0000C7FF800180018001013FFFFFFC3F8001833FFFFFFC3FC003C7FFFFFFFC3F
      E007EFFFFFFFFC3FF81FFFFFFFFFFC3FDC1FDC1FDC1FF81FC01FC01FC01FE007
      DC1FDC1FDC1FC003DFFFDFFFDFFF8001DC1FDC1FDC1F8001C01FC01FC01F0000
      DC1FDC1FDC1F0000DFFFDFFFDFFF0000DC1FDC1FDC1F0000C01FC01FC01F0000
      DC13DC1FDC1F000007F307E407E1800107C007E007E9800107C007F107E5C003
      FFF3FFE0FFE0E007FFF3FFE4FFFEF81F}
  end
  object LayoutLookAndFeel: TdxLayoutLookAndFeelList
    Left = 8
    Top = 600
    object dxLayoutCxLookAndFeel: TdxLayoutCxLookAndFeel
      LookAndFeel.Kind = lfOffice11
    end
  end
  object BarManadger: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = im16
    LookAndFeel.Kind = lfOffice11
    PopupMenuLinks = <>
    Style = bmsOffice11
    UseSystemFont = True
    Left = 40
    Top = 632
    DockControlHeights = (
      0
      0
      0
      0)
    object Bar: TdxBar
      AllowClose = False
      AllowCustomizing = False
      AllowQuickCustomizing = False
      AllowReset = False
      BorderStyle = bbsNone
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockControl = DockControl
      DockedDockControl = DockControl
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 722
      FloatTop = 315
      FloatClientWidth = 51
      FloatClientHeight = 22
      ItemLinks = <
        item
          Visible = True
          ItemName = 'ButtonAdd'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'ButtonDelete'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      UseRestSpace = True
      Visible = True
      WholeRow = False
    end
    object ButtonAdd: TdxBarButton
      Action = AddPattern
      Category = 0
    end
    object ButtonDelete: TdxBarButton
      Action = DeletePattern
      Category = 0
    end
  end
  object DataSetSettings: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Pattern$Settings set'
      '  File$Name = :File$Name,'
      '  Output$Directory = :Output$Directory,'
      '  Decimal$Digits = :Decimal$Digits,'
      '  Max$Skip$Cells$Count = :Max$Skip$Cells$Count,'
      '  Bit$Params = :Bit$Params'
      'where Company$ID = :OLD_Company$ID')
    RefreshSQL.Strings = (
      'select '
      '  File$Name, '
      '  Output$Directory, '
      '  Decimal$Digits,'
      '  Max$Skip$Cells$Count,'
      '  Bit$Params'
      'from '
      '  Pattern$Settings'
      'where '
      '  Company$ID = :OLD_Company$ID')
    SelectSQL.Strings = (
      'select '
      '  File$Name, '
      '  Output$Directory, '
      '  Decimal$Digits,'
      '  Max$Skip$Cells$Count,'
      '  Bit$Params,'
      '  bit(bit$params, 0) Ignore$Zero$Price,'
      '  bit(bit$params, 1) Ignore$Zero$Weight,'
      '  bit(bit$params, 2) Ignore$NaN$Symbols'
      'from '
      '  Pattern$Settings'
      'where '
      '  Company$ID = :Company$ID')
    AfterPost = DataSetSettingsAfterPost
    BeforeClose = DataSetSettingsBeforeClose
    BeforeOpen = DataSetSettingsBeforeOpen
    BeforePost = DataSetSettingsBeforePost
    AllowedUpdateKinds = [ukModify]
    Transaction = dm.tr
    Database = dm.db
    Left = 80
    Top = 632
    poCanEditComputedFields = True
    oRefreshAfterPost = False
    object DataSetSettingsDECIMALDIGITS: TFIBIntegerField
      FieldName = 'DECIMAL$DIGITS'
    end
    object DataSetSettingsMAXSKIPCELLSCOUNT: TFIBSmallIntField
      FieldName = 'MAX$SKIP$CELLS$COUNT'
    end
    object DataSetSettingsBITPARAMS: TFIBIntegerField
      FieldName = 'BIT$PARAMS'
    end
    object DataSetSettingsIGNOREZEROPRICE: TFIBIntegerField
      FieldName = 'IGNORE$ZERO$PRICE'
    end
    object DataSetSettingsIGNOREZEROWEIGHT: TFIBIntegerField
      FieldName = 'IGNORE$ZERO$WEIGHT'
    end
    object DataSetSettingsIGNORENANSYMBOLS: TFIBIntegerField
      FieldName = 'IGNORE$NAN$SYMBOLS'
    end
    object DataSetSettingsFILENAME: TFIBStringField
      FieldName = 'FILE$NAME'
      Size = 256
      EmptyStrToNull = True
    end
    object DataSetSettingsOUTPUTDIRECTORY: TFIBStringField
      FieldName = 'OUTPUT$DIRECTORY'
      Size = 256
      EmptyStrToNull = True
    end
  end
  object DataSetPatterns: TpFIBDataSet
    UpdateSQL.Strings = (
      'update Pattern$Items set'
      '  Name = :Name,'
      '  Pattern$Order = :Pattern$order,'
      '  Field$Name = :Field$Name,'
      '  Bit$Params = :Bit$Params'
      'where ID = :ID')
    DeleteSQL.Strings = (
      'delete from Pattern$Items where ID = :ID')
    InsertSQL.Strings = (
      
        'insert into Pattern$Items(ID, Name, Pattern$Order, Field$Name, B' +
        'it$Params, Company$ID)'
      'values(:ID, :Name, :Pattern$Order, null, 0, :Company$ID)')
    RefreshSQL.Strings = (
      'Select '
      '  ID, '
      '  Name, '
      '  Pattern$Order, '
      '  Field$Name, '
      '  Bit$Params,'
      '  bit(Bit$Params, 0) Is$Tolling,'
      '  Company$ID '
      'from'
      '  Pattern$Items'
      'where '
      '  ID = :ID')
    SelectSQL.Strings = (
      'Select '
      '  ID, '
      '  Name, '
      '  Pattern$Order, '
      '  Field$Name, '
      '  Bit$Params,'
      '  bit(bit$params, 0) Is$Tolling,'
      '  Company$ID'
      'from'
      '  Pattern$Items'
      'where '
      '  Company$ID = :Company$ID'
      'order by'
      '  Pattern$Order')
    BeforeClose = DataSetPatternsBeforeClose
    BeforeOpen = DataSetPatternsBeforeOpen
    BeforePost = DataSetPatternsBeforePost
    Transaction = dm.tr
    Database = dm.db
    Left = 112
    Top = 632
    oRefreshAfterPost = False
    object DataSetPatternsID: TFIBIntegerField
      FieldName = 'ID'
    end
    object DataSetPatternsNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 256
      EmptyStrToNull = True
    end
    object DataSetPatternsPATTERNORDER: TFIBIntegerField
      FieldName = 'PATTERN$ORDER'
    end
    object DataSetPatternsFIELDNAME: TFIBStringField
      FieldName = 'FIELD$NAME'
      Size = 64
      EmptyStrToNull = True
    end
    object DataSetPatternsBITPARAMS: TFIBIntegerField
      FieldName = 'BIT$PARAMS'
    end
    object DataSetPatternsCOMPANYID: TFIBIntegerField
      FieldName = 'COMPANY$ID'
    end
    object DataSetPatternsISTOLLING: TFIBIntegerField
      FieldName = 'IS$TOLLING'
    end
  end
  object SourceSettings: TDataSource
    DataSet = DataSetSettings
    Left = 80
    Top = 664
  end
  object SourcePatterns: TDataSource
    DataSet = DataSetPatterns
    Left = 112
    Top = 664
  end
  object OpenDialog: TOpenDialog
    OnClose = OpenDialogClose
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 48
    Top = 664
  end
  object BrowserDialog: TcxShellBrowserDialog
    FolderLabelCaption = #1042#1099#1073#1077#1088#1080#1090#1077' '#1082#1072#1090#1072#1083#1086#1075' '#1076#1083#1103' '#1096#1072#1073#1083#1086#1085#1086#1074
    LookAndFeel.Kind = lfOffice11
    Options.ShowHidden = True
    Path = 
      'C:\Documents and Settings\'#1040#1076#1084#1080#1085#1080#1089#1090#1088#1072#1090#1086#1088'\Application Data\'#1055#1088#1086#1080#1079#1074#1086 +
      #1076#1089#1090#1074#1086
    Root.BrowseFolder = bfDrives
    Left = 8
    Top = 664
  end
end
