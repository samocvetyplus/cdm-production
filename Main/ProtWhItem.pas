{****************************************}
{  ����������� �������� �� ������������� }
{  Copyrigth (C) 2003 basile for CDM     }
{  v0.16                                 }
{****************************************}
unit ProtWhItem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor,  ImgList, ExtCtrls, ComCtrls,
  Grids, DBGridEh, StdCtrls, Mask, DBCtrlsEh, TB2Item, TB2Dock, TB2Toolbar, db,
  ActnList, PrnDbgeh, Menus, DBCtrls, M207Ctrls, DBGridEhGrouping, GridsEh,
  rxPlacemnt, rxSpeedbar;

type
  TfmProtWhItem = class(TfmAncestor)
    gridWhItem: TDBGridEh;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    lbType: TLabel;
    cmbxType: TDBComboBoxEh;
    TBControlItem2: TTBControlItem;
    acEvent: TActionList;
    acPrintGrid: TAction;
    pWhItem: TPrintDBGridEh;
    ppWhItem: TTBPopupMenu;
    TBItem1: TTBItem;
    SpeedItem1: TSpeedItem;
    Panel1: TPanel;
    Label1: TLabel;
    M207Bevel1: TM207Bevel;
    Label2: TLabel;
    Label3: TLabel;
    M207Bevel2: TM207Bevel;
    M207Bevel3: TM207Bevel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    lbGetQ: TLabel;
    lbGetW: TLabel;
    lbReworkQ: TLabel;
    lbReworkW: TLabel;
    lbDoneQ: TLabel;
    lbDoneW: TLabel;
    lbRetQ: TLabel;
    lbRetW: TLabel;
    lbRejQ: TLabel;
    lbRejW: TLabel;
    lbNkQ: TLabel;
    lbNkW: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    M207Bevel4: TM207Bevel;
    lbInQ: TLabel;
    lbMoveInQ: TLabel;
    lbBulkQ: TLabel;
    lbInW: TLabel;
    lbMoveInW: TLabel;
    lbBulkW: TLabel;
    M207Bevel5: TM207Bevel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    lbOutQ: TLabel;
    lbOutW: TLabel;
    lbMoveOutQ: TLabel;
    lbMoveOutW: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    lbAffSQ: TLabel;
    lbAffSW: TLabel;
    lbAffLRetQ: TLabel;
    lbAffLRetW: TLabel;
    lbAffLGetQ: TLabel;
    lbAffLGetW: TLabel;
    Label25: TLabel;
    lbRetProdQ: TLabel;
    lbRetProdW: TLabel;
    Label26: TLabel;
    lbOutProdQ: TLabel;
    lbOutProdW: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    txtInQ: TDBText;
    txtInW: TDBText;
    M207Bevel6: TM207Bevel;
    Label29: TLabel;
    lbPSQ: TLabel;
    M207Bevel7: TM207Bevel;
    lbPSW: TLabel;
    lbQ: TLabel;
    lbW: TLabel;
    txtOutQ: TDBText;
    txtOutW: TDBText;
    Timer: TTimer;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cmbxTypeDblClick(Sender: TObject);
    procedure gridWhItemGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
    procedure acPrintGridExecute(Sender: TObject);
    procedure TimerTimer(Sender: TObject);
  private
    FType : smallint;
    procedure Filter(DataSet : TDataSet; var Accept : boolean);
  end;

var
  fmProtWhItem: TfmProtWhItem;

implementation

uses MainData, dbUtil, InvData, DictData;

{$R *.dfm}

procedure TfmProtWhItem.FormCreate(Sender: TObject);
const
  iMax = 20;
  jMax = 20;
var
  Q : array [0..iMax] of array [0..jMax] of integer;
  W : array [0..iMax] of array [0..jMax] of double;
  i, j : integer;
begin
  FType := -1;
  cmbxType.OnChange := nil;
  cmbxType.ItemIndex := 0;
  dmMain.taProtWhItem.OnFilterRecord := Filter;
  dmMain.taProtWhItem.Filtered := True;
  cmbxType.OnChange := cmbxTypeDblClick;
  gridWhItem.Color := clWindow;
  Caption := '����������� �������� �� ������������� '+dmInv.taSemisReportSEMISNAME.AsString+'. '+
    '��������: '+dmInv.taSemisReportDEPNAME.AsString+' '+
    '��������: '+dmInv.taSemisReportOPERATION.AsString;
  Screen.Cursor := crSQLWait;
  with dmMain do
  try
    for i:=0 to iMax do
      for j:=0 to jMax do
      begin
        Q[i, j] := 0;
        W[i, j] := 0;
      end;
    OpenDataSet(taProtWhItem);
    taProtWhItem.DisableControls;
    try
      while not dmMain.taProtWhItem.Eof do
      begin
        i := taProtWhItemT.AsInteger;
        j := taProtWhItemT1.AsInteger;
        Q[i][j] := taProtWhItemQ.AsInteger + Q[i][j];
        W[i][j] := taProtWhItemW.AsFloat + W[i][j];
        dmMain.taProtWhItem.Next;
      end;
      dmMain.taProtWhItem.First;
    finally
      taProtWhItem.EnableControls;
    end;
    {1 : '������';
          1 : Text := '����������� ����������';  2 : Text := '������� ������� ���������';
    2 : Text := '������';
          1 : Text := '������ ����������';  2 : Text := '�������� ������� ���������';
    3 : Text := '��';
          1 : Text := '����';  2 : Text := '������';
    4 : Text := 'MX';
          0 : Text := '������'; 1 : Text := '�����'; 3 : Text := '�������';
          5 : Text := '����';   6 : Text := '���������';  7 : Text := '����������';
    5 : Text := '��� ��������';
    6 : Text := '������� �����';
    7 : Text := '������� ����';
          6 : Text := '������';  7 : Text := '�������';}
    try
      lbInQ.Caption      := IntToStr(Q[1][1]);  lbInW.Caption      := FormatFloat('0.###', W[1][1]);
      lbRetProdQ.Caption := IntToStr(Q[1][2]);  lbRetProdW.Caption := FormatFloat('0.###', W[1][2]);
      lbOutQ.Caption     := IntToStr(Q[2][1]);  lbOutW.Caption     := FormatFloat('0.###', W[2][1]);
      lbOutProdQ.Caption := IntToStr(Q[2][2]);  lbOutProdW.Caption := FormatFloat('0.###', W[2][2]);
      lbMoveInQ.Caption  := IntToStr(Q[3][1]);  lbMoveInW.Caption  := FormatFloat('0.###', W[3][1]);
      lbMoveOutQ.Caption := IntToStr(Q[3][2]);  lbMoveOutW.Caption := FormatFloat('0.###', W[3][2]);
      lbGetQ.Caption     := IntToStr(Q[4][0]);  lbGetW.Caption     := FormatFloat('0.###', W[4][0]);
      lbReworkQ.Caption  := IntToStr(Q[4][6]);  lbReworkW.Caption  := FormatFloat('0.###', W[4][6]);
      lbDoneQ.Caption    := IntToStr(Q[4][1]);  lbDoneW.Caption    := FormatFloat('0.###', W[4][1]);
      lbRetQ.Caption     := IntToStr(Q[4][3]);  lbRetW.Caption     := FormatFloat('0.###', W[4][3]);
      lbRejQ.Caption     := IntToStr(Q[4][5]);  lbRejW.Caption     := FormatFloat('0.###', W[4][5]);
      lbNkQ.Caption      := IntToStr(Q[4][7]);  lbNkW.Caption      := FormatFloat('0.###', W[4][7]);
      lbBulkQ.Caption    := IntToStr(Q[5][1]);  lbBulkW.Caption    := FormatFloat('0.###', W[5][1]);
      lbAffSQ.Caption    := IntToStr(Q[6][1]);  lbAffSW.Caption    := FormatFloat('0.###', W[6][1]);
      lbAffLGetQ.Caption := IntToStr(Q[7][6]);  lbAffLGetW.Caption := FormatFloat('0.###', W[7][6]);
      lbAffLRetQ.Caption := IntToStr(Q[7][7]);  lbAffLRetW.Caption := FormatFloat('0.###', W[7][7]);

      lbPSQ.Caption := IntToStr( -(Q[4][0]+Q[4][6]) + (Q[4][1]+Q[4][3]+Q[4][5]+Q[4][7]) );
      lbPSW.Caption := FormatFloat('0.###', -(W[4][0]+W[4][6]) + (W[4][1]+W[4][3]+W[4][5]+W[4][7]) );

      lbQ.Caption := IntToStr( dmInv.taSemisReportREST_IN_Q.AsInteger + Q[1][1] + Q[1][2] - Q[2][1] - Q[2][2] + Q[3][1] - Q[3][2]
                               - Q[4][0] - Q[4][6] + Q[4][1] + Q[4][3] + Q[4][5] + Q[4][7]
                               + Q[5][1] + Q[6][1] - Q[7][6] + Q[7][7] );

      lbW.Caption :=  FormatFloat('0.###', dmInv.taSemisReportREST_IN_W.AsFloat + W[1][1] + W[1][2] - W[2][1] - W[2][2] + W[3][1] - W[3][2]
                               - W[4][0] - W[4][6] + W[4][1] + W[4][3] + W[4][5] + W[4][7]
                               + W[5][1] + W[6][1] - W[7][6] + W[7][7] );
      if (Abs(StrToFloat(lbW.Caption) - dmInv.taSemisReportREST_OUT_W.AsFloat) > Eps)or
         (StrToInt(lbQ.Caption) <> dmInv.taSemisReportREST_OUT_Q.AsInteger) then
      begin
        txtOutQ.Visible := True;
        txtOutW.Visible := True;
        Timer.Enabled := True;
      end;
    except
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfmProtWhItem.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(dmMain.taProtWhItem);
  dmMain.taProtWhItem.OnFilterRecord := nil;
end;

procedure TfmProtWhItem.cmbxTypeDblClick(Sender: TObject);
begin
  dmMain.taProtWhItem.Filtered := False;
  FType := StrToInt(cmbxType.KeyItems[cmbxType.ItemIndex]);
  dmMain.taProtWhItem.Filtered := True;
end;

procedure TfmProtWhItem.Filter(DataSet: TDataSet; var Accept: boolean);
begin
  if(FType = -1) then Accept := True
  else Accept := dmMain.taProtWhItemT.AsInteger = FType;
end;

procedure TfmProtWhItem.gridWhItemGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(Column.FieldName = 'T') or (Column.FieldName = 'T1') then
    case dmMain.taProtWhItemT.AsInteger of
      1 : Background := $00DDE8E1;
      2 : Background := $00B8E4B9;
      3 : Background := $00D8D5C5;
      4 : Background := $00D7C5D8;
      5 : Background := $00DDE8E1;
      6,7 : Background := $00DCC8BE;
    end;
  if(Column.FieldName = 'T1')and(dmMain.taProtWhItemT.AsInteger = 4)then
    case dmMain.taProtWhItemT1.AsInteger of
      0, 6 : Background := clInfoBk;
      else Background := clBtnFace;
    end;
end;

procedure TfmProtWhItem.acPrintGridExecute(Sender: TObject);
begin
  pWhItem.Title.Text := Self.Caption;
  pWhItem.Preview;
end;

procedure TfmProtWhItem.TimerTimer(Sender: TObject);
begin
  Label29.Enabled := not Label29.Enabled;
end;

end.
