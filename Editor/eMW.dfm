inherited fmMW: TfmMW
  Left = 332
  Top = 181
  ActiveControl = edMW
  Caption = #1053#1086#1074#1099#1081' '#1072#1088#1090#1080#1082#1091#1083
  ClientHeight = 94
  ClientWidth = 185
  OldCreateOrder = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 185
    Height = 94
    object lbMW: TLabel [0]
      Left = 12
      Top = 12
      Width = 64
      Height = 13
      Caption = #1057#1088#1077#1076#1085#1080#1081' '#1074#1077#1089
    end
    object lbU: TLabel [1]
      Left = 12
      Top = 36
      Width = 42
      Height = 13
      Caption = #1045#1076'. '#1080#1079#1084'.'
    end
    inherited btnOk: TBitBtn
      Left = 56
      Top = 60
    end
    inherited btnCancel: TBitBtn
      Left = 216
      Top = 8
    end
    object edMW: TDBNumberEditEh
      Left = 84
      Top = 8
      Width = 89
      Height = 19
      DisplayFormat = '0.##'
      EditButtons = <>
      Flat = True
      TabOrder = 2
      Value = 0
      Visible = True
    end
    object cmbxU: TDBComboBoxEh
      Left = 84
      Top = 30
      Width = 89
      Height = 19
      EditButtons = <>
      Flat = True
      Items.Strings = (
        #1064#1090'.'
        #1043#1088'.')
      TabOrder = 3
      Visible = True
    end
  end
end
