unit InvData;

interface

uses
  SysUtils, Classes, DB, pFIBDataSet, pFIBQuery, dbUtil, Dialogs,
  Controls, DateUtils, MainData, DictData, pFIBDatabase, FR_Class, FR_DSet,
  FR_DBSet, pFIBStoredProc, Forms, ArtImage, FIBQuery, 
  FIBDataSet, FIBDatabase;

const

      clSell=$00C7FCFC;
      clStore=$00FEFDCF;
      clReturn=$00D8F5EE;
      clOut=$00D8FCC9;
      clOut2=$00D7FC99;
      clIn=$00E4E3B8;
      clDebtor=$00C4D9D7;
      clBtnFace2 = $00EDF0F1;
      ROOT_STATE_CODE=-1;
      FCurrNDS=1;

type

  TARTChunk= record
     ART:shortstring;
     SZ:shortstring;
     INQ:integer;
     INW:double;
     DEBTORQ:integer;
     DEBTORW:double;
     RETQ:integer;
     RETW:double;
     REJQ:integer;
     REJW:double;
     SALEQ:integer;
     SALEW:double;
     OUTQ:integer;
     OUTW:double;
     STOREQ:integer;
     STOREW:double;
    end;

  TdmInv = class(TDataModule)
    taSList: TpFIBDataSet;
    taSListINVID: TIntegerField;
    taSListDOCNO: TIntegerField;
    taSListDOCDATE: TDateTimeField;
    taSListITYPE: TSmallintField;
    taSListDEPID: TIntegerField;
    taSListUSERID: TIntegerField;
    taSListSUPID: TIntegerField;
    taSListFIO: TFIBStringField;
    taSListSUPNAME: TFIBStringField;
    taSListISCLOSE: TSmallintField;
    taSListDEPNAME: TFIBStringField;
    dsrSList: TDataSource;
    taINVItems: TpFIBDataSet;
    dsrINVItems: TDataSource;
    taDepName: TpFIBDataSet;
    dsrDEPNAME: TDataSource;
    dsSItem: TDataSource;
    taSItem: TpFIBDataSet;
    taA2: TpFIBDataSet;
    dsA2: TDataSource;
    taA2ART2ID: TIntegerField;
    taA2ART2: TFIBStringField;
    taA2RQ: TIntegerField;
    taA2RW: TFloatField;
    dsUnit: TDataSource;
    taUnit: TpFIBDataSet;
    taPrice: TpFIBDataSet;
    dsPrice: TDataSource;
    taA2TPRICE: TFloatField;
    taA2DDEPID: TIntegerField;
    taA2PRICE: TFloatField;
    dsrStoreByArt: TDataSource;
    taStoreByArt: TpFIBDataSet;
    taStoreByArtART: TFIBStringField;
    taStoreByArtART2: TFIBStringField;
    taStoreByArtSZ: TFIBStringField;
    taStoreByArtRQ: TIntegerField;
    taStoreByArtRW: TFloatField;
    taStoreByArtFULLART: TFIBStringField;
    taStoreByArtART2ID: TIntegerField;
    taStoreByArtSZID: TIntegerField;
    dsrINVStore: TDataSource;
    taINVStore: TpFIBDataSet;
    taINVStoreUID: TIntegerField;
    taINVStoreART: TFIBStringField;
    taINVStoreART2: TFIBStringField;
    taINVStoreFULLART: TFIBStringField;
    taINVStoreSZ: TFIBStringField;
    taINVStoreW: TFloatField;
    taINVStoreSZID: TIntegerField;
    taComp: TpFIBDataSet;
    dsrCOMP: TDataSource;
    taSListFROMDEPID: TIntegerField;
    taCompCOMPID: TIntegerField;
    taCompCODE: TFIBStringField;
    taCompNAME: TFIBStringField;
    taDepNameDEPID: TIntegerField;
    taDepNameNAME: TFIBStringField;
    dsrStateItems: TDataSource;
    taStateItems: TpFIBDataSet;
    taA2W: TFloatField;
    taStoreByItems: TpFIBDataSet;
    dsrStoreByItems: TDataSource;
    taItemHistory: TpFIBDataSet;
    dsrItemHistory: TDataSource;
    taItemHistoryDOCDATE: TDateTimeField;
    taItemHistoryITYPE: TSmallintField;
    taItemHistoryDOCNO: TIntegerField;
    taItemHistoryCLOSE_: TSmallintField;
    taItemHistoryRecNo: TIntegerField;
    taItemHistoryOPNAME: TFIBStringField;
    taSListFROMDEPNAME: TFIBStringField;
    taSListNDSID: TIntegerField;
    dsrPriceACTS: TDataSource;
    taPriceACTS: TpFIBDataSet;
    taSListREF: TIntegerField;
    dsrINVNO: TDataSource;
    taINVNO: TpFIBDataSet;
    taStoreItems: TpFIBDataSet;
    taStoreItemsUID: TIntegerField;
    taStoreItemsART: TFIBStringField;
    taStoreItemsART2: TFIBStringField;
    taStoreItemsFULLART: TFIBStringField;
    taStoreItemsSZ: TFIBStringField;
    taStoreItemsW: TFloatField;
    dsrStoreItems: TDataSource;
    taPriceACTSOLD_PRICE: TFloatField;
    taPriceACTSNEW_PRICE: TFloatField;
    taPriceACTSART: TFIBStringField;
    taPriceACTSART2: TFIBStringField;
    taPriceACTSRecNo: TIntegerField;
    taPriceACTSACTDATE: TDateTimeField;
    sqlPrice: TpFIBQuery;
    dsrSUM: TDataSource;
    taSUM: TpFIBDataSet;
    taSUMASSORT: TIntegerField;
    taSUMQ: TIntegerField;
    taSUMW: TFloatField;
    taSUMSUMM: TFloatField;
    taSListSUMM1: TFloatField;
    taSListSUMM2: TFloatField;
    taInvByItems: TpFIBDataSet;
    dsrINVByItems: TDataSource;
    taSZbyName: TpFIBDataSet;
    taSZbyNameID: TIntegerField;
    taCURUID: TpFIBDataSet;
    taCURUIDUID: TIntegerField;
    taSELExport: TpFIBDataSet;
    taSELExportSELID: TIntegerField;
    taSELExportINVID: TIntegerField;
    taSELExportART2ID: TIntegerField;
    taSELExportART2: TFIBStringField;
    taSELExportFULLART: TFIBStringField;
    taSELExportPRICE: TFloatField;
    taSELExportTPRICE: TFloatField;
    taSELExportQUANTITY: TSmallintField;
    taSELExportTOTALWEIGHT: TFloatField;
    taSELExportQ: TIntegerField;
    taSELExportW: TFloatField;
    taSELExportPRICE2: TFloatField;
    taSELExportPNDS: TFloatField;
    taSELExportUNITID: TIntegerField;
    taSELExportNDSID: TIntegerField;
    taSELExportART: TFIBStringField;
    taSELExportUSEMARGIN: TSmallintField;
    taSELExportFULLSUM: TFloatField;
    taSELExportD_INSID: TFIBStringField;
    taSELExportD_ARTID: TIntegerField;
    taSELExportEP2: TFloatField;
    taSELExportSSUM: TFloatField;
    taSELExportSSUMD: TFloatField;
    taSELExportSSUMF: TFloatField;
    taSELExportPRILL: TFIBStringField;
    taSELExportAKCIZ: TFloatField;
    taSELExportGOOD: TFIBStringField;
    taSIExport: TpFIBDataSet;
    taSIExportSITEMID: TIntegerField;
    taSIExportSELID: TIntegerField;
    taSIExportW: TFloatField;
    taSIExportSS: TIntegerField;
    taSIExportUID: TIntegerField;
    taSIExportITYPE: TSmallintField;
    taSIExportDT: TDateTimeField;
    taSIExportARTID: TIntegerField;
    taSIExportART2ID: TIntegerField;
    taSIExportAKCIZ: TFloatField;
    taSIExportDEPID: TIntegerField;
    taINVStoreDOCDATE: TDateTimeField;
    taPriceDict: TpFIBDataSet;
    dsrPriceDict: TDataSource;
    taPriceDictART: TFIBStringField;
    taPriceDictART2: TFIBStringField;
    taPriceDictDEP: TFIBStringField;
    taPriceDictPRICE: TFloatField;
    taPriceDictUNIT: TFIBStringField;
    sqlACTPrice: TpFIBQuery;
    taPriceDictTPRICE: TFloatField;
    taPriceACTSACT_ITEMID: TIntegerField;
    taPriceACTSINVID: TIntegerField;
    taPriceACTSPRICEID: TIntegerField;
    taPriceDictPRICEID: TIntegerField;
    taPACTS: TpFIBDataSet;
    dsrPACTS: TDataSource;
    taPACTSACT_ITEMID: TIntegerField;
    taPACTSOLD_PRICE: TFloatField;
    taPACTSNEW_PRICE: TFloatField;
    taPACTSPRICEID: TIntegerField;
    taPACTSINVID: TIntegerField;
    taASItem: TpFIBDataSet;
    dsrASItem: TDataSource;
    taASItemRecNo: TIntegerField;
    taASItemID: TIntegerField;
    taASItemSELID: TIntegerField;
    taASItemARTID: TIntegerField;
    taASItemART: TFIBStringField;
    taASItemART2ID: TIntegerField;
    taASItemART2: TFIBStringField;
    taASItemOPERID: TFIBStringField;
    taASItemOPERNAME: TFIBStringField;
    taASItemUID: TIntegerField;
    taASItemW: TFloatField;
    taASItemSZID: TIntegerField;
    taASItemSZNAME: TFIBStringField;
    taASItemU: TSmallintField;
    taASItemP0: TSmallintField;
    taASItemINVID: TIntegerField;
    taASItemPRICE: TFloatField;
    taASItemTPRICE: TFloatField;
    taASItemUA: TSmallintField;
    taSListQ: TIntegerField;
    taStoneItems: TpFIBDataSet;
    taStoneItemsSTONES: TFIBStringField;
    dsrStoneItems: TDataSource;
    taInvByItemsUID: TIntegerField;
    taInvByItemsART: TFIBStringField;
    taInvByItemsART2: TFIBStringField;
    taInvByItemsSZ: TFIBStringField;
    taInvByItemsW: TFloatField;
    taInvByItemsINS: TFIBStringField;
    taInvByItemsU: TFIBStringField;
    taInvByItemsSEMIS: TFIBStringField;
    taInvByItemsPRILL: TFIBStringField;
    taInvByItemsART2ID: TIntegerField;
    taInvByItemsPRICE: TFloatField;
    sqlCreateAnaliz: TpFIBQuery;
    sqlUpdateARTSum: TpFIBQuery;
    taPriceACTSSUM_Q0: TFloatField;
    taPriceACTSIN_SUM: TFloatField;
    taPriceACTSOUT_SUM: TFloatField;
    taPriceACTSRAZ_SUM: TFloatField;
    taPriceACTSQ: TIntegerField;
    dsrError_NEA: TDataSource;
    taError_NEA: TpFIBDataSet;
    taError_NEAARTID: TIntegerField;
    taError_NEAART: TFIBStringField;
    dsrPact_Items: TDataSource;
    taPact_Items: TpFIBDataSet;
    taPact_ItemsUID: TIntegerField;
    taPact_ItemsW: TFloatField;
    taSListIN_SUM: TFloatField;
    taSListOUT_SUM: TFloatField;
    taSListRAZ_SUM: TFloatField;
    taWH_items: TpFIBDataSet;
    dsrWH_items: TDataSource;
    taWH_itemsSEMISID: TFIBStringField;
    taWH_itemsSEMISNAME: TFIBStringField;
    taWH_itemsFROM_Q: TIntegerField;
    taWH_itemsFROM_W: TFloatField;
    taWH_itemsDEP_Q: TIntegerField;
    taWH_itemsDEP_W: TFloatField;
    taSIELItems: TpFIBDataSet;
    dsrSIEItems: TDataSource;
    taSIELItemsSEMISNAME: TFIBStringField;
    taSIELItemsW: TFloatField;
    taSIELItemsQ: TIntegerField;
    taSIELItemsINVID_: TIntegerField;
    taSIELItemsSEMIS: TFIBStringField;
    taSIELItemsT: TSmallintField;
    taSIELItemsID: TIntegerField;
    taSIELItemsRecNo: TIntegerField;
    taSIELItemsUQ: TSmallintField;
    taSIELItemsUW: TSmallintField;
    taSIELItemsPRICE: TFloatField;
    taSIELItemsTPRICE: TFloatField;
    taSIELItemsNDSID: TIntegerField;
    taSIELItemsNDS: TFloatField;
    sqlUpdateJA: TpFIBQuery;
    sqlJA_SUM_AQ: TpFIBQuery;
    taStoreByItemsRecNo: TIntegerField;
    taConst: TpFIBDataSet;
    dsrConst: TDataSource;
    taConstNAME: TFIBStringField;
    taConstVAL: TFloatField;
    taINVItemsSELID: TIntegerField;
    taINVItemsINVID: TIntegerField;
    taINVItemsART2ID: TIntegerField;
    taINVItemsART2: TFIBStringField;
    taINVItemsFULLART: TFIBStringField;
    taINVItemsPRICE: TFloatField;
    taINVItemsTPRICE: TFloatField;
    taINVItemsQUANTITY: TSmallintField;
    taINVItemsTOTALWEIGHT: TFloatField;
    taINVItemsQ: TIntegerField;
    taINVItemsW: TFloatField;
    taINVItemsPRICE2: TFloatField;
    taINVItemsPNDS: TFloatField;
    taINVItemsUNITID: TIntegerField;
    taINVItemsNDSID: TIntegerField;
    taINVItemsART: TFIBStringField;
    taINVItemsUSEMARGIN: TSmallintField;
    taINVItemsFULLSUM: TFloatField;
    taINVItemsD_INSID: TFIBStringField;
    taINVItemsD_ARTID: TIntegerField;
    taINVItemsEP2: TFloatField;
    taINVItemsSSUM: TFloatField;
    taINVItemsSSUMD: TFloatField;
    taINVItemsSSUMF: TFloatField;
    taINVItemsGOOD: TFIBStringField;
    taINVItemsRecNo: TIntegerField;
    taINVItemsPRILL: TFIBStringField;
    taINVItemsAKCIZ: TFloatField;
    taINVItemsNDSNAME: TStringField;
    taINVItemsUNIT: TStringField;
    taINVItemsUA: TSmallintField;
    sqlRepriceForU: TpFIBQuery;
    sqlAddSel: TpFIBQuery;
    sqlAddINV: TpFIBQuery;
    taSListISBRACK: TSmallintField;
    sqlAddPact: TpFIBQuery;
    taINVItemsOLDUA: TSmallintField;
    sqlRepriceINV: TpFIBQuery;
    taSemisReport: TpFIBDataSet;
    dsrSemisReport: TDataSource;
    taSemisReportREST_IN_Q: TIntegerField;
    taSemisReportREST_IN_W: TFloatField;
    taSemisReportIN_Q: TIntegerField;
    taSemisReportIN_W: TFloatField;
    taSemisReportIN_MOVE_Q: TIntegerField;
    taSemisReportIN_MOVE_W: TFloatField;
    taSemisReportIN_PS_Q: TIntegerField;
    taSemisReportIN_PS_W: TFloatField;
    taSemisReportOUT_Q: TIntegerField;
    taSemisReportOUT_W: TFloatField;
    taSemisReportOUT_MOVE_Q: TIntegerField;
    taSemisReportOUT_MOVE_W: TFloatField;
    taSemisReportOUT_PS_Q: TIntegerField;
    taSemisReportOUT_PS_W: TFloatField;
    taSemisReportUQ_NAME: TFIBStringField;
    taSemisReportUW_NAME: TFIBStringField;
    taSemisReportDEPNAME: TFIBStringField;
    taSemisReportREST_OUT_Q: TIntegerField;
    taSemisReportREST_OUT_W: TFloatField;
    taSemisReportOPERATION: TFIBStringField;
    taSemisReportSEMIS: TFIBStringField;
    taSemisReportDEPID: TIntegerField;
    taSemisReportSEMISID: TFIBStringField;
    taSemisReportOPERID: TFIBStringField;
    taSemisReportUQ: TSmallintField;
    taSemisReportUW: TSmallintField;
    taSemisReportFQ: TFloatField;
    taSemisReportFW: TFloatField;
    taSemisReportRQ: TFloatField;
    taSemisReportRW: TFloatField;
    taSemisReportRQ1: TFloatField;
    taSemisReportRW1: TFloatField;
    taSemisReportID: TIntegerField;
    taSListCOMMISION: TSmallintField;
    taStoreByItems2: TpFIBDataSet;
    dsrStoreByItems2: TDataSource;
    taStoreByItems2UID: TIntegerField;
    taStoreByItems2ART: TFIBStringField;
    taStoreByItems2ART2: TFIBStringField;
    taStoreByItems2SZ: TFIBStringField;
    taStoreByItems2W: TFloatField;
    taStoreByItems2UNIT: TFIBStringField;
    taStoreByItems2SEMIS: TFIBStringField;
    taStoreByItems2STATE: TSmallintField;
    taStoreByItems2INS: TFIBStringField;
    taStoreByItems2STATE_NAME: TFIBStringField;
    taStoreByItems2CURPRICE: TFloatField;
    taStoreByItems2LASTDATE: TDateTimeField;
    sqlCreateProdStore: TpFIBQuery;
    sqlUpdateStoresHeader: TpFIBQuery;
    sqlUpdateOutStores: TpFIBQuery;
    taOutStore: TpFIBDataSet;
    dsrOutStore: TDataSource;
    taOutStoreART: TFIBStringField;
    taOutStoreARTID: TIntegerField;
    taOutStoreCOMPID: TIntegerField;
    taOutStoreSTOREW: TFloatField;
    taOutStoreSTOREQ: TIntegerField;
    taOutStoreSZ: TIntegerField;
    taOutStoreSZNAME: TFIBStringField;
    taCompADATE: TDateTimeField;
    taFindArts: TpFIBDataSet;
    taFindArtsARTID: TIntegerField;
    taFindArtsART2ID: TIntegerField;
    taSItemSITEMID: TIntegerField;
    taSItemSELID: TIntegerField;
    taSItemW: TFloatField;
    taSItemSZ: TIntegerField;
    taSItemUID: TIntegerField;
    taSItemITYPE: TSmallintField;
    taSItemDT: TDateTimeField;
    taSItemARTID: TIntegerField;
    taSItemART2ID: TIntegerField;
    taSItemAKCIZ: TFloatField;
    taSItemDEPID: TIntegerField;
    taSItemP0: TSmallintField;
    taSItemOPERID: TFIBStringField;
    taSItemRecNo: TIntegerField;
    taSItemszname: TStringField;
    taSListUPPRICE: TFloatField;
    taINVItemsUPPRICE: TFloatField;
    taSListRESTSUM: TFloatField;
    taSemisReportQA: TIntegerField;
    taSemisReportFQA: TIntegerField;
    taStoreByItems2ART2ID: TIntegerField;
    taStoreByItems2ARTID: TIntegerField;
    taStoreByItems2SZID: TIntegerField;
    taSemisReportACTID: TIntegerField;
    taSemisReportOPERNAME: TStringField;
    taSemisReportSEMISNAME: TStringField;
    taSListACC_SUMM: TFloatField;
    taItemHistoryDEPNAME: TFIBStringField;
    taARTPrice: TpFIBDataSet;
    dsrArtPrice: TDataSource;
    taARTPriceART: TFIBStringField;
    taARTPriceARTID: TIntegerField;
    taARTPriceK1: TFloatField;
    taARTPriceK2: TFloatField;
    taARTPriceK3: TFloatField;
    taARTPriceK: TFloatField;
    taInvByItemsPRICE2: TFloatField;
    taARTPriceDet: TpFIBDataSet;
    dsrArtPriceDet: TDataSource;
    taARTPriceDetPRICEID: TIntegerField;
    taARTPriceDetPRICE: TFloatField;
    taARTPriceDetTPRICE: TFloatField;
    taARTPriceDetDEPID: TIntegerField;
    taARTPriceDetART2: TFIBStringField;
    sqlReturnPrice: TpFIBQuery;
    taARTPriceDetART2ID: TIntegerField;
    taARTPriceDetCALC_PRICE: TFloatField;
    sqlCalcPrice: TpFIBQuery;
    sqlDEalPrice: TpFIBQuery;
    taA2SZ: TFIBStringField;
    taConst2: TpFIBDataSet;
    IBStringField1: TFIBStringField;
    FloatField1: TFloatField;
    dsrConst2: TDataSource;
    taItemHistoryPRICE: TFloatField;
    taItemHistoryPRICE2: TFloatField;
    sqlCreateActPrice: TpFIBQuery;
    taStoreByItemsUID: TIntegerField;
    taStoreByItemsSTATE: TIntegerField;
    taStoreByItemsART: TFIBStringField;
    taStoreByItemsART2: TFIBStringField;
    taStoreByItemsDOCDATE: TDateTimeField;
    taStoreByItemsSZ: TFIBStringField;
    taStoreByItemsW: TFloatField;
    taStoreByItemsU: TFIBStringField;
    taStoreByItemsPRICE: TFloatField;
    taStoreByItemsSUM_: TFloatField;
    taStoreByItemsSTATE_NAME: TFIBStringField;
    taStoreByItemsART2ID: TIntegerField;
    taStoreByItemsARTID: TIntegerField;
    taStoreByItemsSZID: TIntegerField;
    taINVItemsACC_SUMM: TFloatField;
    taARTPriceART_PRICE: TFloatField;
    taARTPricePICT: TBlobField;
    taARTPriceU: TSmallintField;
    taArtIns: TpFIBDataSet;
    dsrArtIns: TDataSource;
    taArtInsQUANTITY: TSmallintField;
    taArtInsWEIGHT: TFloatField;
    taArtInsID: TIntegerField;
    taArtInsART2ID: TIntegerField;
    taArtInsSEMIS: TFIBStringField;
    taArtInsPRICE: TFloatField;
    taArtInsSUM: TFloatField;
    sqlDeleteArt2: TpFIBQuery;
    sqlAddLostStones: TpFIBQuery;
    taLostStones: TpFIBDataSet;
    dsrLostStones: TDataSource;
    taLostStonesQUANTITY: TSmallintField;
    taLostStonesWEIGTH: TFloatField;
    taLostStonesSEMIS: TFIBStringField;
    taSemisReportBULK_Q: TIntegerField;
    taSemisReportBULK_W: TFloatField;
    taPact_ItemsOLD_PRICE: TFloatField;
    taPact_ItemsNEW_PRICE: TFloatField;
    taArtVedomost: TpFIBDataSet;
    dsrArtVedomost: TDataSource;
    sqlUpdateInvPrice: TpFIBQuery;
    taARTPriceDetUP: TSmallintField;
    sqlClearSelectUp: TpFIBQuery;
    sqlCheckArt: TpFIBQuery;
    taAVList: TpFIBDataSet;
    dsrAVList: TDataSource;
    taAVListID: TIntegerField;
    taAVListDOCNO: TIntegerField;
    taAVListDOCDATE: TDateTimeField;
    taAVListUSERID: TIntegerField;
    taAVListBD: TDateTimeField;
    taAVListISCLOSE: TSmallintField;
    taAVListT: TSmallintField;
    taAVListUPD: TSmallintField;
    sqlPublic: TpFIBQuery;
    taSemisReportAFF_S_Q: TIntegerField;
    taSemisReportAFF_S_W: TFloatField;
    taSemisReportAFF_L_GET_Q: TIntegerField;
    taSemisReportAFF_L_GET_W: TFloatField;
    taSemisReportAFF_L_RET_Q: TIntegerField;
    taSemisReportAFF_L_RET_W: TFloatField;
    taArtVedomostACT_Q: TFloatField;
    taArtVedomostACT_SUM: TFloatField;
    taArtVedomostACT_W: TFloatField;
    taArtVedomostACTID: TIntegerField;
    taArtVedomostART: TFIBStringField;
    taArtVedomostARTID: TIntegerField;
    taArtVedomostBRACK_Q: TIntegerField;
    taArtVedomostBRACK_SUM: TFloatField;
    taArtVedomostBRACK_W: TFloatField;
    taArtVedomostID: TIntegerField;
    taArtVedomostIN_Q: TIntegerField;
    taArtVedomostIN_SUM: TFloatField;
    taArtVedomostIN_W: TFloatField;
    taArtVedomostREST1_Q: TIntegerField;
    taArtVedomostREST1_SUM: TFloatField;
    taArtVedomostREST1_W: TFloatField;
    taArtVedomostREST2_Q: TIntegerField;
    taArtVedomostREST2_SUM: TFloatField;
    taArtVedomostREST2_W: TFloatField;
    taArtVedomostRET_Q: TFloatField;
    taArtVedomostRET_SUM: TFloatField;
    taArtVedomostRET_W: TFloatField;
    taArtVedomostSELL_Q: TIntegerField;
    taArtVedomostSELL_SUM: TFloatField;
    taArtVedomostSELL_W: TFloatField;
    taLostStonesID: TIntegerField;
    taLostActs: TpFIBDataSet;
    dsrLostActs: TDataSource;
    taLostActsDOCNO: TIntegerField;
    taLostActsDOCDATE: TDateTimeField;
    taLostActsUSERID: TIntegerField;
    taLostActsUSERNAME: TStringField;
    taLostActsDOCNO1: TIntegerField;
    taAllLostStones: TpFIBDataSet;
    dsrAllLostStones: TDataSource;
    taLostActsINVID: TIntegerField;
    taAllLostStonesINSID: TFIBStringField;
    taAllLostStonesQUANTITY: TIntegerField;
    taAllLostStonesWEIGTH: TFloatField;
    taAllLostStonesEDGESHAPEID: TFIBStringField;
    taAllLostStonesEDGTYPEID: TFIBStringField;
    taAllLostStonesCHROMATICITY: TFIBStringField;
    taAllLostStonesCLEANNES: TFIBStringField;
    taAllLostStonesCOLOR: TFIBStringField;
    taAllLostStonesART: TFIBStringField;
    taAllLostStonesART2: TFIBStringField;
    taAllLostStonesSEMIS: TFIBStringField;
    frLostStones: TfrDBDataSet;
    taSListREFNO: TIntegerField;
    taSListREFTYPE: TIntegerField;
    taPact_ItemsF_1: TFloatField;
    taPact_ItemsF_2: TFloatField;
    taPactsPrint: TpFIBDataSet;
    frPactsPrint: TfrDBDataSet;
    taProdREport: TpFIBDataSet;
    dsrProdReport: TDataSource;
    taProdREportART: TFIBStringField;
    taProdREportSZ: TFIBStringField;
    taProdREportSELL_W: TFloatField;
    taProdREportSELL_SUMM: TFloatField;
    taProdREportRET_W: TFloatField;
    taProdREportRET_SUMM: TFloatField;
    taProdREportF_2: TFloatField;
    taProdREportF_3: TFloatField;
    frProdReport: TfrDBDataSet;
    frStoreByItem: TfrDBDataSet;
    taSListW: TFloatField;
    taSListMEMO: TMemoField;
    taSListANALIZ: TSmallintField;
    taSListRESTQ: TIntegerField;
    taSListRESTW: TFloatField;
    taSemisReportCHARGE_Q: TIntegerField;
    taSemisReportCHARGE_W: TFloatField;
    taSListPAYTYPEID: TIntegerField;
    taInvByItemsSAMID: TFIBStringField;
    taSListISMC: TSmallintField;
    taSListTRANS_PRICE: TFloatField;
    taSListTRANS_PERCENT: TFloatField;
    taAllLostStonesIGR: TFIBStringField;
    dsrCash: TDataSource;
    taCash: TpFIBDataSet;
    taCashID: TFIBIntegerField;
    taCashCOMPID: TFIBIntegerField;
    taCashCOST: TFIBFloatField;
    taCashNDS: TFIBFloatField;
    taCashOPERDATE: TFIBDateTimeField;
    taCashCOMPNAME: TFIBStringField;
    taCashNDOC: TFIBStringField;
    taCashT: TFIBSmallIntField;
    taProdREportSELL_Q: TFIBIntegerField;
    taProdREportRET_Q: TFIBIntegerField;
    taProdREportF_1: TFIBBCDField;
    sqlAdd_All_in_INV: TpFIBQuery;
    taConstCONST_ID: TFIBIntegerField;
    taConstSTR_VAL: TFIBStringField;
    taCompNDOG: TFIBStringField;
    taCompDOGBD: TFIBDateTimeField;
    taSListSSF: TFIBStringField;
    taSListASSORT: TFIBIntegerField;
    taSListSSFDT: TFIBDateTimeField;
    taSListCOST: TFIBFloatField;
    taSListCONTRACTID: TFIBIntegerField;
    taSListCONTRACTNAME: TFIBStringField;
    taInv_Store4ArtId: TpFIBDataSet;
    dsrInv_Store4ArtId: TDataSource;
    taSItem4ArtId: TpFIBDataSet;
    dsrSItem4ArtId: TDataSource;
    taSItem4ArtIdSITEMID: TFIBIntegerField;
    taSItem4ArtIdSELID: TFIBIntegerField;
    taSItem4ArtIdW: TFIBFloatField;
    taSItem4ArtIdSZ: TFIBIntegerField;
    taSItem4ArtIdUID: TFIBIntegerField;
    taSItem4ArtIdARTID: TFIBIntegerField;
    taSItem4ArtIdART2ID: TFIBIntegerField;
    taSItem4ArtIdART2: TFIBStringField;
    taInv_Store4ArtIdARTID: TFIBIntegerField;
    taInv_Store4ArtIdART2ID: TFIBIntegerField;
    taInv_Store4ArtIdUID: TFIBIntegerField;
    taInv_Store4ArtIdART: TFIBStringField;
    taInv_Store4ArtIdART2: TFIBStringField;
    taInv_Store4ArtIdFULLART: TFIBStringField;
    taInv_Store4ArtIdSZ: TFIBStringField;
    taInv_Store4ArtIdW: TFIBFloatField;
    taInv_Store4ArtIdSZID: TFIBIntegerField;
    taInv_Store4ArtIdDOCDATE: TFIBDateTimeField;
    taSItem4ArtIdSZNAME: TFIBStringField;
    taInv_Store4ArtIdSPRICE: TFIBFloatField;
    taInv_Store4ArtIdDEPID: TFIBIntegerField;
    taInv_Store4ArtIdQ0: TFIBFloatField;
    taInv_Store4ArtIdSCOST: TFIBFloatField;
    taInv_Store4ArtIdSTATE_: TFIBSmallIntField;
    taSItem4ArtIdCOST2: TFIBFloatField;
    taSListSCHEMAID: TFIBIntegerField;
    taSListINVSUBTYPEID: TFIBIntegerField;
    taSListINVSUBTYPENAME: TFIBStringField;
    taSListINVCOLORID: TFIBIntegerField;
    taSListCOLOR: TFIBIntegerField;
    taINVItemsSERVCOST: TFIBFloatField;
    taINVItemsCHECK_NO: TFIBStringField;
    taCashCOSTSELL: TFIBFloatField;
    taCashCOSTRET: TFIBFloatField;
    taArtInsNAME: TFIBStringField;
    taItemHistorySELFNAME: TFIBStringField;
    taSListPAYED: TFIBDateTimeField;
    taSListSENDED: TFIBDateTimeField;
    taPriceList: TpFIBDataSet;
    dsrPriceList: TDataSource;
    taPriceListID: TFIBIntegerField;
    taPriceListARTID: TFIBIntegerField;
    taPriceListPRICE: TFIBFloatField;
    taPriceListART: TFIBStringField;
    taStoreByItems2CURPRICE2: TFIBFloatField;
    taStoreByItemsPRICE2: TFIBFloatField;
    taSListW2: TFIBFloatField;
    taRev_det: TpFIBDataSet;
    dsrRev_Det: TDataSource;
    taRev_detUID: TFIBIntegerField;
    taRev_detSTATE: TFIBSmallIntField;
    taRev_detID: TFIBIntegerField;
    taRev_detW: TFIBFloatField;
    taRev_detART2ID: TFIBIntegerField;
    taRev_detART2: TFIBStringField;
    taRev_detART: TFIBStringField;
    frRev_det: TfrDBDataSet;
    taRev_detPRICE: TFIBFloatField;
    taNotInInv: TpFIBDataSet;
    dsrNotInInv: TDataSource;
    taNotInInvARTID: TFIBIntegerField;
    taNotInInvART2ID: TFIBIntegerField;
    taNotInInvW: TFIBFloatField;
    taNotInInvSZ: TFIBIntegerField;
    taNotInInvUID: TFIBIntegerField;
    taNotInInvCHECK_: TFIBSmallIntField;
    taNotInInvART: TFIBStringField;
    taNotInInvART2: TFIBStringField;
    taNotInInvSITEMID: TFIBIntegerField;
    taStoreByItemsSELFCOMPID: TFIBIntegerField;
    taStoreByItemsSELFCOMPNAME: TFIBStringField;
    taArtInsSEMISID: TFIBStringField;
    taStoreByItems2UNITID: TFIBIntegerField;
    taItemHistoryID: TFIBIntegerField;
    taItemHistoryART2ID: TFIBIntegerField;
    taItemHistoryARTID: TFIBIntegerField;
    taStoneItemsMATNAME: TFIBStringField;
    taStoneItemsQUANTITY: TFIBIntegerField;
    taStoneItemsWEIGHT: TFIBFloatField;
    taStoneItemsCOLORNAME: TFIBStringField;
    taStoneItemsEDGSHAPEID: TFIBStringField;
    taStoneItemsEDGTYPEID: TFIBStringField;
    taStoneItemsCHROMATICITY: TFIBStringField;
    taStoneItemsCLEANNES: TFIBStringField;
    taStoneItemsIGR: TFIBStringField;
    taStoreByItems2SELFCOMPID: TFIBIntegerField;
    taStoreByItems2SELFCOMPNAME: TFIBStringField;
    taEServParams: TpFIBDataSet;
    dsrEServParams: TDataSource;
    taEServParamsID: TFIBIntegerField;
    taEServParamsINVID: TFIBIntegerField;
    taEServParamsMATID: TFIBStringField;
    taEServParamsN: TFIBFloatField;
    taEServParamsSERVCOST: TFIBFloatField;
    taEServParamsEXTRA: TFIBFloatField;
    taEServParamsMATNAME: TFIBStringField;
    taEServParamsU: TFIBSmallIntField;
    taEServMat: TpFIBDataSet;
    dsrEServMat: TDataSource;
    taEServMatID: TFIBIntegerField;
    taEServMatINVID: TFIBIntegerField;
    taEServMatMATID: TFIBStringField;
    taEServMatCOMPID: TFIBIntegerField;
    taEServMatNW: TFIBFloatField;
    taEServMatSCOST: TFIBFloatField;
    taEServMatN: TFIBFloatField;
    taEServMatGETW: TFIBFloatField;
    taEServMatOUTW: TFIBFloatField;
    taEServMatCOMPNAME: TStringField;
    taEServMatMATNAME: TStringField;
    taRev_detSZNAME: TFIBStringField;
    taSListREVISIONPERCENT: TFloatField;
    taInvByItemsCOMMENTART: TFIBStringField;
    taStoreByItems2BUH_10: TFIBSmallIntField;
    taSListPRIHOD_ID: TFIBIntegerField;
    taSListPRIHOD: TFIBStringField;
    taSListCalcPrihod: TpFIBDataSet;
    taSListCalcPrihodNEED_W: TFIBFloatField;
    taSListCalcPrihodDIFF_W: TFIBFloatField;
    dsrSListCalcPrihod: TDataSource;
    taSListCalcPrihodNORMA: TFIBFloatField;
    taSListCalcPrihodPOTERI: TFIBFloatField;
    taSListCalcPrihodPRIHOD_W: TFIBFloatField;
    taArtInsMAT: TFIBStringField;
    taStoreByItems2Material: TFIBStringField;
    frEmptyInv: TfrDBDataSet;
    taEmptyInv: TpFIBDataSet;
    taEmptyInvQ: TFIBFloatField;
    taEmptyInvW: TFIBFloatField;
    taEmptyInvCOST: TFIBFloatField;
    taEmptyInvNDSNAME: TFIBStringField;
    taEmptyInvNDS: TFIBFloatField;
    taEmptyInvTRANS_PRICE: TFIBFloatField;
    taSListCalcPrihodPRILLNAME: TFIBStringField;
    taSListCREATED: TFIBDateTimeField;
    procedure CalcRecNo(DataSet: TDataSet);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure DelConf(DataSet: TDataSet);
    procedure taSListBeforeOpen(DataSet: TDataSet);
    procedure taSListAfterPost(DataSet: TDataSet);
    procedure taSListBeforeDelete(DataSet: TDataSet);
    procedure taSListNewRecord(DataSet: TDataSet);
    procedure taINVItemsBeforeOpen(DataSet: TDataSet);
    procedure taINVItemsNewRecord(DataSet: TDataSet);
    procedure taA2BeforeOpen(DataSet: TDataSet);
    procedure taSItemNewRecord(DataSet: TDataSet);
    procedure taSItemBeforeOpen(DataSet: TDataSet);
    procedure taINVItemsAfterDelete(DataSet: TDataSet);
    procedure taSItemAfterDelete(DataSet: TDataSet);
    procedure taSItemBeforeDelete(DataSet: TDataSet);
    procedure taA2BeforeEdit(DataSet: TDataSet);
    procedure taA2AfterPost(DataSet: TDataSet);
    procedure taStoreByArtBeforeOpen(DataSet: TDataSet);
    procedure taStoreItemsBeforeOpen(DataSet: TDataSet);
    procedure taINVStoreBeforeOpen(DataSet: TDataSet);
    procedure taStoreByItemsBeforeOpen(DataSet: TDataSet);
    procedure taItemHistoryCalcFields(DataSet: TDataSet);
    procedure taItemHistoryBeforeOpen(DataSet: TDataSet);
    procedure taINVItemsAfterEdit(DataSet: TDataSet);
    procedure taPriceACTSBeforeOpen(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure taA2BeforeRefresh(DataSet: TDataSet);
    procedure taInvByItemsBeforeOpen(DataSet: TDataSet);
    procedure taSELExportNewRecord(DataSet: TDataSet);
    procedure taSELExportBeforeOpen(DataSet: TDataSet);
    procedure taSIExportNewRecord(DataSet: TDataSet);
    procedure taPriceDictBeforeOpen(DataSet: TDataSet);
    procedure taPriceDictAfterPost(DataSet: TDataSet);
    procedure taPriceDictBeforeEdit(DataSet: TDataSet);
    procedure taASItemAfterPost(DataSet: TDataSet);
    procedure taASItemAfterDelete(DataSet: TDataSet);
    procedure taASItemNewRecord(DataSet: TDataSet);
    procedure taASItemBeforeOpen(DataSet: TDataSet);
    procedure taASItemUGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
    procedure taStoneItemsBeforeOpen(DataSet: TDataSet);
    procedure taINVItemsAfterScroll(DataSet: TDataSet);
    procedure taPact_ItemsBeforeOpen(DataSet: TDataSet);
    procedure taWH_itemsBeforeOpen(DataSet: TDataSet);
    procedure taSIELItemsBeforeOpen(DataSet: TDataSet);
    procedure taSIELItemsNewRecord(DataSet: TDataSet);
    procedure taSIELItemsAfterPost(DataSet: TDataSet);
    procedure taSIELItemsEditError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure taSemisReportBeforeOpen(DataSet: TDataSet);
    procedure taINVItemsBeforePost(DataSet: TDataSet);
    procedure taSemisReportCalcFields(DataSet: TDataSet);
    procedure taOutStoreBeforeOpen(DataSet: TDataSet);
    procedure taSemisReportBeforeDelete(DataSet: TDataSet);
    procedure taSemisReportNewRecord(DataSet: TDataSet);
    procedure taSemisReportBeforePost(DataSet: TDataSet);
    procedure taARTPriceDetBeforeOpen(DataSet: TDataSet);
    procedure taARTPriceAfterScroll(DataSet: TDataSet);
    procedure taARTPriceDetAfterPost(DataSet: TDataSet);
    procedure taARTPriceBeforeScroll(DataSet: TDataSet);
    procedure taARTPriceAfterPost(DataSet: TDataSet);
    procedure taStoreByItemsFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure taARTPriceFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure taArtInsBeforeOpen(DataSet: TDataSet);
    procedure taARTPriceDetAfterScroll(DataSet: TDataSet);
    procedure taArtInsCalcFields(DataSet: TDataSet);
    procedure taARTPriceDetBeforeDelete(DataSet: TDataSet);
    procedure taArtInsAfterEdit(DataSet: TDataSet);
    procedure taLostStonesBeforeOpen(DataSet: TDataSet);
    procedure taStoreByItems2BeforeOpen(DataSet: TDataSet);
    procedure taAVListBeforeOpen(DataSet: TDataSet);
    procedure taArtVedomostBeforeOpen(DataSet: TDataSet);
    procedure taLostActsBeforeOpen(DataSet: TDataSet);
    procedure taAllLostStonesBeforeOpen(DataSet: TDataSet);
    procedure taPactsPrintBeforeOpen(DataSet: TDataSet);
    procedure taPriceACTSNewRecord(DataSet: TDataSet);
    procedure taPACTSNewRecord(DataSet: TDataSet);
    procedure taSemisReportBeforeEdit(DataSet: TDataSet);
    procedure taSItemUpdateRecord(DataSet: TDataSet; UpdateKind: TUpdateKind; var UpdateAction: TFIBUpdateAction);
    procedure taCashBeforeOpen(DataSet: TDataSet);
    procedure taCashTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure taA2AfterScroll(DataSet: TDataSet);
    procedure taSItem4ArtIdBeforeOpen(DataSet: TDataSet);
    procedure taInv_Store4ArtIdBeforeOpen(DataSet: TDataSet);
    procedure taCashCalcFields(DataSet: TDataSet);
    procedure taCompBeforeOpen(DataSet: TDataSet);
    procedure taPriceListBeforeOpen(DataSet: TDataSet);
    procedure taDepNameBeforeOpen(DataSet: TDataSet);
    procedure taRev_detBeforeOpen(DataSet: TDataSet);
    procedure taNotInInvBeforeOpen(DataSet: TDataSet);
    procedure taStoreByItems2AfterOpen(DataSet: TDataSet);
    procedure taEServParamsBeforeOpen(DataSet: TDataSet);
    procedure taEServParamsNewRecord(DataSet: TDataSet);
    procedure PostBeforeClose(DataSet: TDataSet);
    procedure taSListCalcFields(DataSet: TDataSet);
    procedure taStoreByItems2BUH_10GetText(Sender: TField;
      var Text: String; DisplayText: Boolean);
    procedure taINVItemsAfterRefresh(DataSet: TDataSet);
    procedure taSListCalcPrihodBeforeOpen(DataSet: TDataSet);
    procedure taINVItemsAfterOpen(DataSet: TDataSet);
    procedure taSListAfterOpen(DataSet: TDataSet);
  private
    FABD: TDateTime;
    FEBD: TDateTime;
    FStype: byte;
    FIsMayChangeSz: boolean;
    FCurrSemis: string;
    FCurroper: string;
    FCompName: string;
  public
    taSListCalcPrihodMode: Integer;
    Q : integer;// ���������� ������� � ������
    QSEL : integer; // ���������� ����� � ���������
    ITYPE : integer; // ��� ������������� ���������
    //CopyType : integer;// ��� ���������� ���������
    //CopyTypeFrom : integer; // ��� �� ����� ���������� ���������
    //CopyInvId : integer; // id ���������
    FCurrDep : integer;
    FCurrState: integer;// ������� ������ ��� ������
    FCurrUID:integer;
    FCurrW:double;
    FCurrARTID:integer;
    FCurrART2ID:integer;
    FCurrSz:integer;
    FCurrART:string;
    FCurrFullArt:string;
    FCurrSAMUID:integer;
    FCurrINVID :integer;
    OLD_PRICE: double;

    FCurrActPrice:integer;// ������� ��� ����������
    FCurrPrice:single;
    FCurrPrice2:single;

    FCurrWH:integer;
    FCurrPWH:integer;

    FCurrSIEL_W:single;
    FCurrSIEL_Q:integer;

    FCurrU:integer;

    FCurrMat: string;

    AArt:string;
    EServParamID : integer;
    SScanZ:string;

    WSZ:boolean; //�� ��������
    (* ������� ��������� ������� �������� *)
    IsPrint:boolean;

    //
    FSupname:string;
                            (*       �����        *)
    FCassaMoney : double; // ����� �� �����
    FCassaGiveMoney : double; // �������� � �����
    FCassaUseNDS : boolean;
    FCassaNDSMoney : double;
    FCassaCompId : integer;
    (* ������ ������� *)
    property SType : byte read FSType write FSType; // ��� ����� ��� ������������ ��������
                                                  // 0 - ������� ������
                                                  // 1 - � ������������ �������
    property CompName: string read FCompName write FCompName;
    property IsMayChangeSz : boolean read FIsMayChangeSz write FIsMayChangeSz;
    property ABD : TDateTime read FABD write FABD;
    property AED : TDateTime read FEBD write FEBD;
    property CurrSemis:String read FCurrSemis write FCurrSemis;
    property CurrArt2Id:integer read FCurrArt2id write FCurrArt2id;
    property Curroper:String read FCurroper write FCurrOper;

    procedure SetArtFilter(Params: TFIBXSQLDA);
    procedure InitArtParams;
    procedure ChangeWhPeriod(BD, ED: TDateTime);// �������� ������ ������
    procedure InvGetValue(const ParName: String; var ParValue: Variant);
    procedure ImportStore(const FileName: string);
    function STR_TO_Chunk(var Str: string): TARTChunk;
    procedure UpdateOutStores(const Chunk: TArtChunk);

    procedure SaveInv(const FileName:string); // ��������� ���������
    procedure LoadInv(const FileName:string); // ��������� ���������

    constructor Create(AOwner : TComponent); override;

  end;

var
  dmInv: TdmInv;

implementation

uses Store, SInv_det, bsUtils, PrintData, ApplData, MsgDialog, Variants,
  dPrice, SInvProd, eArtIns, ProductionConsts;

{$R *.dfm}


Function TdmInv.STR_TO_Chunk(var Str:string):TARTChunk;
var Chunk:TARtChunk;
    st:string;
    i:integer;
//    s:string;
//    kolichestvo:integer;
begin

   if DecimalSeparator=',' then    str:=StringReplace(str,'.',',',[rfReplaceAll])
   else str:=StringReplace(str,',','.',[rfReplaceAll]);

   st:='';
   i:=1;
   //�������
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.ART:=st;


   //������
   st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.SZ:=st;

   // ��.���-��
   st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.INQ:=StrToInt(st);
   // ��. ���
   st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.INW:=StrToFloat(st);
   // ������ ���-��
    st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.DEBTORQ:=StrToInt(st);
   // ������ ���
    st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.DEBTORW:=StrToFloat(st);
   //������� ���-��
    st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.SALEQ:=StrToInt(st);
   //�������� ���
    st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.SALEW:=StrToFloat(st);
   //�������� ���-��
    st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
  Chunk.REJQ:=StrToInt(st);
   //�������� ���
    st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
  Chunk.REJW:=StrToFloat(st);

   // ��������?
    st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.RETQ:=StrToInt(st);
   // ��������?
    st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.RETW:=StrToFloat(st);
   // �����. ���-��
    st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.OUTQ:=StrToInt(st);
   // ����� ���
    st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.OUTW:=StrToFloat(st);

   st:='';
   while str[i]=' ' do inc(i);
   while str[i]<>' 'do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.STOREQ:=StrToInt(st);

   // ����� ���
    st:='';
   while str[i]=' ' do inc(i);
   while (str[i]<>' ')and (i<=length(str)) do
   begin
    st:=st+str[i];
    inc(i);
   end;
   Chunk.STOREW:=StrToFloat(st);

   REsult:=Chunk;
end;


procedure TdmINV.InvGetValue(const ParName: String; var ParValue: Variant);
begin

  if (ParName = '����������')then ParValue := GetCapitalsQuantity(taINVItemsQUANTITY.AsInteger);
  if (ParName = '����� ���')then ParValue := GetCapitalsWeight(taINVItemsTOTALWEIGHT.AsFloat);
  if (ParName = '����� � ������2')then ParValue := GetCapitalsMoney(taINVItemsSSUMF.AsFloat);

//  if (ParName = '����� ���') then ParValue := GetCapitalsMoney(dmPrint.frReport.COmp);
end;


procedure TdmINV.ChangeWhPeriod(BD, ED: TDateTime);
begin
  if dm.BeginDate <> BD  then dm.BeginDate := BD;
  if dm.EndDate <> ED then dm.EndDate := ED;
end;

procedure TdmInv.CalcRecNo(DataSet: TDataSet);
begin
  DataSet.FieldByName('RecNo').Value := DataSet.RecNo;
end;

procedure TdmInv.CommitRetaining(DataSet: TDataSet);
begin
  if DataSet.Name = 'taInvItems' then
         ShowMessage('Before CR taInvItemsPRICE2: ' + FloatToStr(taInvItemsPRICE2.AsFloat));
  dbUtil.CommitRetaining(DataSet);
  if DataSet.Name = 'taInvItems' then
         ShowMessage('After CR taInvItemsPRICE2: ' + FloatToStr(taInvItemsPRICE2.AsFloat));
end;

procedure TdmInv.DelConf(DataSet: TDataSet);
begin
  if MessageDialog('������� ������?', mtConfirmation, [mbOK, mbCancel], 0)=mrCancel then SysUtils.Abort;
end;

procedure TdmInv.taSListBeforeOpen(DataSet: TDataSet);
//var j : integer;
begin
  Screen.Cursor := crHourGlass;
  taSLIST.ParambyName('ITYPE_').ASInteger:=ITYPE;
  taSLIST.ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
  taSLIST.ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);

  if FSupname=ROOT_KNAME_CODE then
  begin
    taSLIST.ParamByName('SUPNAME1').AsString :=Low(Char) ;
    taSLIST.ParamByName('SUPNAME2').AsString :=High(char) ;
  end
  else begin
    taSLIST.ParamByName('SUPNAME1').AsString :=FSupname;
    taSLIST.ParamByName('SUPNAME2').AsString :=FSupname;
  end;

  dm.SetDepParams(TpFIBDataSet(DataSet).Params);
  //for taSlist do
  {with taSList do
    begin
      for j:=0 to 10 do
          Showmessage(Params[j].AsString);
    end;}

end;

procedure TdmInv.taSListAfterOpen(DataSet: TDataSet);
begin
  Screen.Cursor := crDefault;
end;

procedure TdmInv.taSListAfterPost(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TdmInv.taSListBeforeDelete(DataSet: TDataSet);
begin
  if MessageDialog('������� ���������?', mtConfirmation, [mbYes, mbNo], 0)=mrNo then SysUtils.Abort;
end;

procedure TdmInv.taSListNewRecord(DataSet: TDataSet);
begin
  taSListINVID.AsInteger := dm.GetId(12);
  taSListDOCDATE.AsDateTime := dm.ServerDateTime;
  taSListUSERID.AsInteger := dm.User.UserId;
  taSListISCLOSE.AsInteger := 0;
  taSListCONTRACTID.AsInteger := dm.Rec.DefaultProdContractId;
  if ITYPE=14
  then taSListFromDEPID.AsInteger := dm.CurrDep
  else taSListDEPID.AsInteger := dm.CurrDep;

  {if ITYPE in [3,4] then taSListNDSID.AsInteger := -1
  else} taSListNDSID.AsInteger := 1;
  taSListITYPE.AsInteger:=dmInv.ITYPE;
  taSListFIO.AsString:=dm.User.FIO;
  case ITYPE of
    2: taSListDOCNO.AsInteger := dm.getId(24);
    3: taSListDOCNO.AsInteger := dm.getId(25);
    4: taSListDOCNO.AsInteger := dm.getId(20);
    5: taSListDOCNO.AsInteger := dm.getId(26);
   11: taSListDOCNO.AsInteger := dm.getId(28);
   14: taSListDOCNO.AsInteger := dm.getId(32);
   24: taSListDOCNO.AsInteger := dm.getId(92);
  end;
  taSListUPPRICE.AsFloat := 0;
  taSListTRANS_PERCENT.AsFloat := 0;
  taSListTRANS_PRICE.AsFloat := 0;
end;

procedure TdmInv.taINVItemsBeforeOpen(DataSet: TDataSet);
begin

  taINVitems.ParamByName('ASINVID').AsInteger:=FcurrINVID;
end;

procedure TdmInv.SetArtFilter(Params: TFIBXSQLDA);
begin
  with Params, dm do
  begin
     if AMatId=ROOT_MAT_CODE then
      try
        ByName['MATID1'].AsString:=MINSTR;
        ByName['MATID2'].AsString:=MAXSTR;
      except
      end
    else
      try
        ByName['MATID1'].AsString:=AMATID;
        ByName['MATID2'].AsString:=AMATID;
      except
      end;

    if AInsId=ROOT_INS_CODE then
      try
        ByName['INSID1'].AsString:=MINSTR;
        ByName['INSID2'].AsString:=MAXSTR;
      except
      end
    else
      try
        ByName['INSID1'].AsString:=AINSID;
        ByName['INSID2'].AsString:=AINSID;
      except
      end;

     if ASemisId=ROOT_SEMIS_CODE then
      try
        ByName['SEMISID1'].AsString:=MINSTR;
        ByName['SEMISID2'].AsString:=MAXSTR;
      except
      end
    else
      try
        ByName['SEMISID1'].AsString:=ASEMISID;
        ByName['SEMISID2'].AsString:=ASEMISID;
      except
      end;

     if APrillId=ROOT_PRILL_CODE then
      try
        ByName['PRILL1'].AsString:=MINSTR;
        ByName['PRILL2'].AsString:=MAXSTR;
      except
      end
    else
      try
        ByName['PRILL1'].AsString:=ASEMISID;
        ByName['PRILL2'].AsString:=ASEMISID;
      except
      end;

    if AArt=ROOT_ART_CODE then
      try
        ByName['ART1'].AsString:=MINSTR;
        ByName['ART2'].AsString:=MAXSTR;
      except
      end
    else
      try
        ByName['ART1'].AsString:=AART;
        ByName['ART2'].AsString:=AART;
      except
      end;

  end;
end;

procedure TdmInv.InitArtParams;
begin
 with dm do
 begin
  AMatId  := ROOT_MAT_CODE;
  AInsId  := ROOT_INS_CODE;
  APrillId := ROOT_PRILL_CODE;
  ASemisId := ROOT_SEMIS_CODE;
  AART     := ROOT_ART_CODE;
 end;
end;


procedure TdmInv.taINVItemsNewRecord(DataSet: TDataSet);
begin
  with dmINV do
  begin
    taINVItemsSELID.AsInteger := dm.GetId(21);
    taInvitemsArt2Id.AsInteger := FCurrART2ID;
    taINVItemsD_ARTID.AsInteger := FCurrARTID;
    taINVItemsINVID.AsInteger := taSListINVID.AsInteger;
//    taInvitemsPrice.AsFloat   := FCurrPrice;
//    taInvitemsTPrice.AsFloat  := FCurrPrice;
     if (FCurrPrice<>FCurrPrice2) and (FCurrPrice2>0)
     then taInvitemsPrice2.AsFloat:=FCurrPrice2
     else taInvitemsPrice2.AsFloat:=FCurrPrice;
//    taInvitemsPrice2.AsFloat  := taInvitemsPrice.AsFloat*(1+taInvitemsAKCIZ.AsFloat/100)*(1+taINVItemsPNDS.AsFloat/100) ;
//    taInvitems.AsFloat      := taInvitemsPrice.AsFloat;
    if ITYPE = 4 then     taInvitemsNDSID.AsInteger     := -1
    else   taInvitemsNDSID.AsInteger     := taSListNDSID.AsInteger;
    taInvitemsArt2.AsString       := FCurrArt;
    taInvitemsUA.AsInteger := 1;// �� ���������
    taInvitemsOLDUA.AsInteger := 1;// �� ���������
//    taInvitemsUnitId.AsInteger    := dm.taArtUnitId.AsInteger;
end;
end;

procedure TdmInv.taA2BeforeOpen(DataSet: TDataSet);
begin
  taA2.Params[0].AsInteger := dm.taArtARTID.Asinteger;
  taA2.Params[1].AsInteger := dmINV.taSListDEPID.AsInteger;
end;

procedure TdmInv.taSItemNewRecord(DataSet: TDataSet);
begin
  if(taSListISCLOSE.AsInteger=1)then
    raise Exception.Create(Format(rsInvClose, [taSListDOCNO.AsString]));
  taSItemSITEMID.AsInteger := dm.GetId(29);
  taSItemW.AsFloat:=FCurrW;

  taSItemSZ.AsInteger:=FCurrSZ;
//  taSItemSS.AsInteger:=-1;
  taSItemARTID.AsInteger:=taINVItemsD_ARTID.AsInteger;
  taSItemART2ID.AsInteger:=taINVItemsART2ID.AsInteger;
 //  taSItemPRICE.AsFloat:=taA2PRICE.AsFloat;
//  if FCurrPrice2>0 then
//  taSItemPRICE2.AsFloat:=FCurrPrice2;
  taSItemSElId.AsInteger := taINVItemsSELID.AsInteger;
  taSItemAKCIZ.AsFloat:=0;
  taSItemITYPE.AsInteger:=dmInv.ITYPE;
  taSItemDEPID.AsInteger:=taSListDEPID.AsInteger;
  taSItemDT.AsDateTime:=taSListDOCDATE.AsDateTime;

  if taSListITYPE.AsInteger = 4
  then   taSItemUID.AsInteger := dm.GetId(22) // �����
  else  taSItemUID.AsInteger := FCurrUID;

  if SType = 1 then taSItemOPERID.AsString := dmAppl.taWHApplOPERID.AsString;
  taSItemP0.AsInteger := SType;
end;

procedure TdmInv.taSItemBeforeOpen(DataSet: TDataSet);
begin
  taSItem.ParamByName('SELID').AsInteger:=taINVItemsSELID.AsInteger;
end;

procedure TdmInv.taINVItemsAfterDelete(DataSet: TDataSet);
begin
  taINVItems.Transaction.CommitRetaining;
end;

procedure TdmInv.taSItemAfterDelete(DataSet: TDataSet);
begin
  ReOpenDataSets([dmInv.taSItem]);
  dmInv.taSItem.Transaction.CommitRetaining;
end;

procedure TdmInv.taSItemBeforeDelete(DataSet: TDataSet);
begin
if dmINV.taSListISCLOSE.AsInteger = 1 then
  begin
    ShowMessage('������ �������� �������� ���������!');
    SysUtils.Abort;
  end;
end;


procedure TdmInv.taA2BeforeEdit(DataSet: TDataSet);
begin
//  taA2DDEPID.AsInteger:=taSListDEPID.Asinteger;
//  if taA2W.
  taA2.Params[0].AsInteger:=taA2ART2ID.AsInteger;
  taA2.Params[1].AsInteger:=taSListDEPID.AsInteger;
end;

procedure TdmInv.taA2AfterPost(DataSet: TDataSet);
begin

 sqlPrice.ParamByName('ART2ID').Asinteger:=taA2ART2ID.Asinteger;
 sqlPrice.ParamByName('INVID').Asinteger:=taSListINVID.Asinteger;
 sqlPrice.ParamByName('PRICE').AsFloat:=taA2TPRICE.AsFloat;
 sqlPrice.Prepare;
 sqlPrice.ExecQuery;
 sqlPrice.Transaction.CommitRetaining;
 RefreshDataSets([taINVItems]);
 ReopenDataSets([dmINv.taA2]);
end;

procedure TdmInv.taStoreByArtBeforeOpen(DataSet: TDataSet);
begin
   if FCurrDep=ROOT_COMP_CODE then
   begin
    taStorebyART.Params[0].AsInteger:=-MAXINT;
    taStorebyART.Params[1].AsInteger:=MAXINT;
   end
   else begin
    taStorebyART.Params[0].AsInteger:=FCurrDep;
    taStorebyART.Params[1].AsInteger:=FCurrDep;
   end;
  if WSZ then    taStorebyART.Params[2].AsInteger:=0
  else taStorebyART.Params[2].AsInteger:=1;
end;

procedure TdmInv.taStoreItemsBeforeOpen(DataSet: TDataSet);
begin
  if FCurrDep=ROOT_COMP_CODE then
   begin
    taStoreItems.Params[0].AsInteger:=-MAXINT;
    taStoreItems.Params[1].AsInteger:=MAXINT;
   end
   else begin
    taStoreItems.Params[0].AsInteger:=FCurrDep;
    taStoreItems.Params[1].AsInteger:=FCurrDep;
   end;
   taStoreItems.Params[2].AsInteger:=taStorebyARTART2ID.ASinteger;
   if dmINV.WSZ then begin
      taStoreItems.Params[3].AsInteger:=1;
      taStoreItems.Params[4].AsInteger:=taStoreByArtszid.ASinteger;
     end
   else  begin
     taStoreItems.Params[3].AsInteger:=0;
     taStoreItems.Params[4].AsInteger:=0;
   end


end;

procedure TdmInv.taINVStoreBeforeOpen(DataSet: TDataSet);
begin
 if FCurrDep=ROOT_COMP_CODE then
   begin
    taINVStore.Params[0].AsInteger:=-MAXINT;
    taINVStore.Params[1].AsInteger:=MAXINT;
   end
   else begin
    taINVStore.Params[0].AsInteger:=taSListDEPID.AsInteger;
    taINVStore.Params[1].AsInteger:=taSListDEPID.AsInteger;
   end;

   if dm.AArt2Id=ROOT_ART2_CODE then
      try
        taINVStore.ParamByName('ART2ID_1').AsInteger := -MAXINT;
        taINVStore.ParamByName('ART2ID_2').AsInteger := MAXINT;
      except
      end
    else
      try
         taINVStore.ParamByName('ART2ID_1').AsInteger := taINVItemsART2ID.ASinteger;
         taINVStore.ParamByName('ART2ID_2').AsInteger := taINVItemsART2ID.ASinteger;
      except
      end;

   case dmInv.ITYPE of
   2: taINVStore.Params[4].AsInteger:=0;
   3: taINVStore.Params[4].AsInteger:=1;
   12: taINVStore.Params[4].AsInteger:=0;
   end;
end;

procedure TdmInv.taStoreByItemsBeforeOpen(DataSet: TDataSet);
begin
  if FCurrState=ROOT_STATE_CODE then
   begin
    taStoreByItems.ParamByName('ST1').AsInteger :=-1;
    taStoreByItems.ParamByName('ST2').AsInteger:=10;
   end
   else begin
      taStoreByItems.ParamByName('ST1').AsInteger:=FCurrState;
      taStoreByItems.ParamByName('ST2').AsInteger:=FCurrState;
   end;

   if Screen.ActiveForm<>fmSInv_det then taStoreByItems.ParamByName('ART').AsString:='';


   taStoreByItems.ParamByName('SUPID').AsInteger:=taSListSUPID.AsInteger;
   taStoreByItems.ParamByName('EDATE').AsTimeStamp:=DateTimeToTimeStamp(taSListDOCDATE.AsDateTime);
end;

procedure TdmInv.taItemHistoryCalcFields(DataSet: TDataSet);
begin
   DataSet.FieldByName('RecNo').Value := DataSet.RecNo;
end;

procedure TdmInv.taItemHistoryBeforeOpen(DataSet: TDataSet);
begin
  taItemHistory.ParamByName('UID').AsInteger:=dmINV.FCurrUID;
end;

procedure TdmInv.taINVItemsAfterEdit(DataSet: TDataSet);
begin
// RefreshDataSets([dm.taARt]);
//
end;

procedure TdmInv.taPriceACTSBeforeOpen(DataSet: TDataSet);
begin
  taPriceACTS.ParamByName('INVID').AsInteger:=taSListINVID.Asinteger;
end;

procedure TdmInv.DataModuleCreate(Sender: TObject);
begin
  FSupname:=ROOT_KNAME_CODE;
  AArt:='';
// whBeginDate:=
end;

procedure TdmInv.taA2BeforeRefresh(DataSet: TDataSet);
begin
 taA2.Params[0].AsInteger := dm.taArtARTID.Asinteger;
 taA2.Params[1].AsInteger := dmINV.taSListDEPID.AsInteger;
end;

procedure TdmInv.taInvByItemsBeforeOpen(DataSet: TDataSet);
begin
 taINVbyItems.ParamByName('INVID').AsInteger:=taSListINVID.AsInteger;
end;

procedure TdmInv.taSELExportNewRecord(DataSet: TDataSet);
begin
with dmINV do
 begin
  taSELExportSELID.AsInteger := dm.GetId(21);
  taSELExportINVID.AsInteger:=taSListINVID.AsInteger;
  taSELExportART2ID.AsInteger:=taINVItemsART2ID.AsInteger;
  taSELExportD_ARTID.AsInteger:=taINVItemsD_ARTID.AsInteger;
  taSELExportPRICE.AsFloat:=taINVItemsPRICE.AsFloat;
  taSELExportTPRICE.AsFloat:=taINVItemsTPRICE.AsFloat;
//  taSELExportPRICE2.AsFloat:=taINVItemsTPRICE.AsFloat;
  taSELExportNDSID.AsInteger:=taSListNDSID.AsInteger;
  taSELExportUNITID.AsInteger:=taINVItemsUNITID.AsInteger;
  taSELExportD_INSID.AsString:=taINVItemsD_INSID.AsString;
  taSELExportPRILL.AsInteger:=taINVItemsPrill.AsInteger;
  taSELExportAKCIZ.AsInteger:=5;
end;
end;

procedure TdmInv.taSELExportBeforeOpen(DataSet: TDataSet);
begin
  taINVitems.ParamByName('ASINVID').AsInteger:=taSListINVID.AsInteger;
end;

procedure TdmInv.taSIExportNewRecord(DataSet: TDataSet);
begin
  taSIExportSITEMID.AsInteger := dm.GetId(29);
  taSIExportSELID.Asinteger:=taSELExportSELID.AsInteger;
  taSIExportW.AsFloat:=taSItemw.Asfloat;
  taSIExportSS.Asinteger:=taSItemSZ.AsInteger;
  taSIExportUID.Asinteger:=taSItemUID.AsInteger;
  taSIExportITYPE.Asinteger:=taSListITYPE.AsInteger;
  taSIExportITYPE.Asinteger:=taSListITYPE.AsInteger;
  taSIExportdt.AsDateTime:=taSListDOCDATE.AsDateTime;
  taSIExportDEpid.Asinteger:=taSListDEPID.AsInteger;
  taSIExportAKCIZ.AsFloat:=taSELExportAKCIZ.AsFloat;
  taSIExportARTID.Asinteger:=taSItemARTID.AsInteger;
  taSIExportART2ID.Asinteger:=taSItemART2ID.AsInteger;
end;

procedure TdmInv.taPriceDictBeforeOpen(DataSet: TDataSet);
begin
  if FCurrDep=ROOT_COMP_CODE then
   begin
    taPriceDict.ParamByName('DEPID1').AsInteger :=-MAXINT;
    taPriceDict.ParamByName('DEPID2').AsInteger:=MAXINT;
   end
   else begin
      taPriceDict.ParamByName('DEPID1').AsInteger:=FCurrDEP;
      taPriceDict.ParamByName('DEPID2').AsInteger:=FCurrDEP;
   end;
    taPriceDict.ParamByName('ART_').AsString :='';
end;

procedure TdmInv.taPriceDictAfterPost(DataSet: TDataSet);
begin
 if taPriceDictPRICE.AsFloat <> taPriceDictTPRICE.AsFloat
 then begin
  if FCurrActPrice=0 then
  begin
   taSList.Append;
   taSListITYPE.AsInteger:=11;// ��� ����������
   taSListDOCNO.AsInteger:=dm.GetID(28);
   taSList.Post;
   taSList.Transaction.CommitRetaining;
   FCurrActPrice:=taSListINVID.AsInteger;
  end;
  OpenDataSet(taPACTS);
  taPACTS.APPEND;
  taPACTSPRICEID.AsInteger:=taPriceDictPRICEID.AsInteger;
  taPACTSOLD_PRICE.AsFloat:=Old_PRICE;
  taPACTSNEW_PRICE.AsFloat:=taPriceDictTPRICE.AsFloat;
  taPACTSINVID.AsInteger:=FCurrActPrice;
  taPACTS.Post;
  taPriceACTS.Transaction.CommitRetaining;
  CloseDataSet(taPACTS);
end;

end;

procedure TdmInv.taPriceDictBeforeEdit(DataSet: TDataSet);
begin
 OLD_PRICE:=taPriceDictPRICE.AsFloat;
end;

procedure TdmInv.taASItemAfterPost(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TdmInv.taASItemAfterDelete(DataSet: TDataSet);
begin
  dbUtil.CommitRetaining(DataSet);
end;

procedure TdmInv.taASItemNewRecord(DataSet: TDataSet);
begin
  taASItemID.AsInteger := dm.GetId(29);
  taASItemINVID.AsInteger := taSListINVID.AsInteger;
  taASItemARTID.AsInteger := dmAppl.taWHApplARTID.AsInteger;
  taASItemART2ID.AsInteger := dmAppl.taWHApplART2ID.AsInteger;
  taASItemSZID.AsInteger := dmAppl.taWHApplSZID.AsInteger;
  taASItemOPERID.AsString := dmAppl.taWHApplOPERID.AsString;
  taASItemW.AsFloat := dmAppl.ADistrW;
  taASItemUID.AsInteger := dm.GetId(22);
  taASItemP0.AsInteger := 1;
  taASItemUA.AsInteger := dmAppl.taWHApplU.AsInteger;
end;

procedure TdmInv.taASItemBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
    ByName['INVID'].AsInteger := taSListINVID.AsInteger;
end;

procedure TdmInv.taASItemUGetText(Sender: TField; var Text: String;
  DisplayText: Boolean);
begin
 If Sender.isnull then Text:=''
 else case
 sender.AsInteger of
 0:Text:='��.';
 1:Text:='��.';
 else Text:='';
 end;
end;

procedure TdmInv.taStoneItemsBeforeOpen(DataSet: TDataSet);
begin
  if IsPrint then taStoneItems.ParamByName('ART2ID').ASInteger:=taINVItemsART2ID.AsInteger
  else taStoneItems.ParamByName('ART2ID').ASInteger:=taINVbyItemsART2ID.AsInteger;
end;

procedure TdmInv.taINVItemsAfterScroll(DataSet: TDataSet);
begin
  ReOpenDataSet(taStoneItems);
  if fmArtIns<>nil then begin
    dminv.CurrArt2Id:=dminv.taINVItemsART2ID.AsInteger;
    ReOpenDataSet(taArtIns);
  end;
end;

procedure TdmInv.taPact_ItemsBeforeOpen(DataSet: TDataSet);
begin
 taPact_Items.ParamByName('ACT_ITEMID').AsInteger:=taPriceACTSACT_ITEMID.AsInteger;
end;

procedure TdmInv.taWH_itemsBeforeOpen(DataSet: TDataSet);
begin
  taWH_items.ParamByName('FROMDEPID').AsInteger:=taSListFROMDEPID.AsInteger;
  taWH_items.ParamByName('DEPID').AsInteger:=taSListDEPID.AsInteger;
end;

procedure TdmInv.taSIELItemsBeforeOpen(DataSet: TDataSet);
begin
 taSielItems.ParamByName('INVID').ASinteger:=taSListINVID.Asinteger;
end;

procedure TdmInv.taSIELItemsNewRecord(DataSet: TDataSet);
begin
  taSIELItemsID.AsInteger := dm.GetId(42);
  taSIELItemsQ.AsInteger:=FCurrSIEL_Q;
  taSIELItemsW.AsFloat:=FCurrSIEL_W;
  taSIELItemsINVID_.AsInteger:=taSListINVID.AsInteger;
  taSIELItemsSEMIS.AsString:=taWH_itemsSEMISID.AsString;
  taSIELItemsT.AsInteger:=1;
end;

procedure TdmInv.taSIELItemsAfterPost(DataSet: TDataSet);
begin
 dbUtil.CommitRetaining(DataSet);
 ReopenDataSets([taWH_items]);
 //RefreshDataSets([taA2]);
end;

procedure TdmInv.taSIELItemsEditError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
 taSIELItems.Cancel;
 PostDataSet(taSIELItems);
// taSIELItems.Transaction.Rollback;
 RefreshDataSet(taSIELItems);
end;

procedure TdmInv.taSemisReportBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    taSemisReport.ParamByName('ACTID').AsInteger := dmMain.taVListID.Asinteger;
    if(CurrSemis = ROOT_SEMIS_CODE)then
    begin
      taSemisReport.ParamByName('SEMIS1').AsString := MINSTR;
      taSemisReport.ParamByName('SEMIS2').AsString := MAXSTR;
    end
    else begin
      taSemisReport.ParamByName('SEMIS1').AsString:=CurrSemis;
      taSemisReport.ParamByName('SEMIS2').AsString:=CurrSemis;
    end;

    if(CurrOper = ROOT_OPER_CODE)then
    begin
      taSemisReport.ParamByName('OPER1').AsString := MINSTR;
      taSemisReport.ParamByName('OPER2').AsString := MAXSTR;
    end
    else begin
      taSemisReport.ParamByName('OPER1').AsString:=CurrOper;
      taSemisReport.ParamByName('OPER2').AsString:=CurrOper;
    end;

    if dmMain.FilterMatId = ROOT_MAT_CODE then
    begin
      ByName['MATID1'].AsString := MINSTR;
      ByName['MATID2'].AsString := MAXSTR;
    end
    else begin
      ByName['MATID1'].AsString := dmMain.FilterMatId;
      ByName['MATID2'].AsString := dmMain.FilterMatId;
    end;
  end;
end;

procedure TdmInv.taINVItemsBeforePost(DataSet: TDataSet);
begin
  if taINVItemsPRICE2.IsNull then taINVItemsPRICE2.AsFloat := 0
  else if taINVItemsPRICE2.AsFloat = 0 then
     case MessageDialog('��������� ���� ����� 0!'#13+'����������?', mtWarning, [mbYes, mbNo], 0) of
       mrYes : ;
       mrNo : SysUtils.Abort;
     end;
end;

procedure TdmInv.taSemisReportCalcFields(DataSet: TDataSet);
begin
  taSemisReportRQ1.AsFloat := taSemisReportREST_OUT_Q.AsFloat - taSemisReportFQ.AsFloat;
  taSemisReportRW1.AsFloat := taSemisReportREST_OUT_W.AsFloat - taSemisReportFW.AsFloat;
end;

procedure TdmInv.ImportStore(const FileName: string);
var F:textfile;
//    FS:TfileStream;
//    R:TReader;
    str:string;
    FBDATEANALIZ,FEDATEANALIZ:TDateTime;
    Chunk:TARTChunk;
begin

  try
   AssignFile(f,FIleName);
   Reset(f);
   readln(F,str);
   COMPNAME:=AnsiUpperCase(str);
   readln(F,str);
   readln(F,str);
   FBDATEANALIZ:=StrToDate(Str);
   readln(F,str);
   FEDATEANALIZ:=StrToDate(Str);
{   FS:=TFileStream.Create(FileName,fmOpenRead);
   R:=TReader.Create(FS,256);
   str:=R.readString;
   COMPNAME:=AnsiUpperCase(str);
   str:=R.readString;
   str:=R.readString;
   FBDATEANALIZ:=StrToDate(Str);
   str:=R.readString;
   FEDATEANALIZ:=StrToDate(Str);}

   if not sqlUpdateStoresHeader.Transaction.Active then sqlUpdateStoresHeader.Transaction.StartTransaction;
   sqlUpdateStoresHeader.ParamByName('ADATE').AsDateTime:=FEDATEANALIZ;
   sqlUpdateStoresHeader.ParamByName('SUPNAME').AsString:=COMPNAME;
   sqlUpdateStoresHeader.ExecQuery;
{?} sqlUpdateStoresHeader.Transaction.CommitRetaining;

   while not eof(f) do
   begin
    Readln(f,str);
    try
     Chunk:=STR_TO_Chunk(str);
    except
      MessageDialog('������ ��� StrToChunk '+str,mtError,[mbOk],0);
    end;
    try
       UpdateOutStores(Chunk);
      dmInv.sqlUpdateOutStores.Transaction.CommitRetaining;
    except
      MessageDialog('������ ��� UpdateOutStores',mtError,[mbOk],0);
      dmInv.sqlUpdateOutStores.Transaction.Rollback;
    end;
   end;
  finally
   CLoseFile(f);
  end;
end;

//���������� ���������� � �������
procedure TdmInv.UpdateOutStores(const Chunk: TArtChunk);
begin
 try
  sqlUpdateOutStores.ParamByName('COMPNAME').AsString:=UpperCase(CompName);
  sqlUpdateOutStores.ParamByName('ART').AsString:=Chunk.ART;
  sqlUpdateOutStores.ParamByName('SZ').AsString:=Chunk.SZ;
  sqlUpdateOutStores.ParamByName('STOREQ').AsInteger:=Chunk.STOREQ;
  sqlUpdateOutStores.ParamByName('STOREW').AsFloat:=Chunk.STOREW;
  sqlUpdateOutStores.ExecQuery;
 except
   raise Exception.Create('������ ��� ������� �������');
 end;
end;


procedure TdmInv.taOutStoreBeforeOpen(DataSet: TDataSet);
begin
  taOutStore.ParamByName('COMPID').AsInteger:=taSListSUPID.AsInteger;
  taOutStore.ParamByName('ARTID').AsInteger:=dm.taArtARTID.AsInteger;
end;

procedure TdmInv.taSemisReportBeforeDelete(DataSet: TDataSet);
begin
  if(dmMain.taVListISCLOSE.AsInteger = 1)then
    raise Exception.Create(Format(rsVSheetClose, ['']));
  if MessageDialog('������� ������������ �� ������������ ���������?', mtConfirmation, [mbYes, mbNo], 0) = mrNo then SysUtils.Abort; 
end;

procedure TdmInv.taSemisReportNewRecord(DataSet: TDataSet);
begin
  taSemisReportID.AsInteger := dm.GetId(73);
  taSemisReportACTID.AsInteger := dmMain.taVListID.AsInteger;
  taSemisReportOPERID.AsVariant := Null;
  taSemisReportSEMISID.AsVariant := Null;
  taSemisReportDEPID.AsInteger := dmMain.taVListDEPID.AsInteger;
  taSemisReportDEPNAME.AsString := dmMain.taVListDEPNAME.AsString;
  taSemisReportREST_IN_Q.AsInteger := 0;
  taSemisReportREST_IN_W.AsFloat := 0;
  taSemisReportIN_Q.AsInteger := 0;
  taSemisReportIN_W.AsFloat := 0;
  taSemisReportIN_MOVE_Q.AsInteger := 0;
  taSemisReportIN_MOVE_W.AsFloat := 0;
  taSemisReportOUT_Q.AsInteger := 0;
  taSemisReportOUT_W.AsFloat := 0;
  taSemisReportOUT_MOVE_Q.AsInteger := 0;
  taSemisReportOUT_MOVE_W.AsFloat := 0;
  taSemisReportIN_PS_Q.AsInteger := 0;
  taSemisReportIN_PS_W.AsFloat := 0;
  taSemisReportOUT_PS_Q.AsInteger := 0;
  taSemisReportOUT_PS_W.AsFloat := 0;
  taSemisReportREST_OUT_Q.AsInteger := 0;
  taSemisReportREST_OUT_W.AsFloat := 0;
  taSemisReportUQ.AsVariant := Null;
  taSemisReportUW.AsVariant := Null;
  taSemisReportFQ.AsInteger := 0;
  taSemisReportFW.AsInteger  := 0;
  taSemisReportQA.AsInteger  := 0;
  taSemisReportFQA.AsInteger := 0;
  taSemisReportBULK_Q.AsInteger := 0;
  taSemisReportBULK_W.AsFloat := 0;
  taSemisReportAFF_S_Q.AsInteger := 0;
  taSemisReportAFF_S_W.AsFloat := 0;
  taSemisReportAFF_L_GET_Q.AsInteger := 0;
  taSemisReportAFF_L_GET_W.AsFloat := 0;
  taSemisReportAFF_L_RET_Q.AsInteger := 0;
  taSemisReportAFF_L_RET_W.AsFloat := 0;
  taSemisReportCHARGE_Q.AsInteger := 0;
  taSemisReportCHARGE_W.AsFloat := 0;
end;

procedure TdmInv.taSemisReportBeforePost(DataSet: TDataSet);
begin
  if taSemisReportSEMISID.IsNull then raise Exception.Create(rsNotSelFieldSemis);   
end;

procedure TdmInv.taARTPriceDetBeforeOpen(DataSet: TDataSet);
begin
   taARTPriceDet.ParamByName('ARTID').AsInteger:=taARTPriceARTID.AsInteger;
end;

procedure TdmInv.taARTPriceAfterScroll(DataSet: TDataSet);
begin
  ReOpenDataSet(taARTPriceDet);
  if Assigned(dmAppl.FArtImage) then dmAppl.FArtImage.Caption := '�������: '+taARTPriceART.AsString;
  if (Screen.ActiveForm=fmPriceDict)and(fmPriceDict.ActiveControl<>fmPriceDict.edART) then
   begin
     fmPriceDict.ActiveControl:=fmPriceDict.grArt2;
     fmPriceDict.grArt2.SelectedIndex:=4;
   end;
end;

procedure TdmInv.taARTPriceDetAfterPost(DataSet: TDataSet);
begin
  taARTPriceDet.Transaction.CommitRetaining;
  RefreshDataSet(taARTPriceDet);
end;

procedure TdmInv.taARTPriceBeforeScroll(DataSet: TDataSet);
begin
  PostDataSet(taArtPriceDet);
end;

procedure TdmInv.taARTPriceAfterPost(DataSet: TDataSet);
begin
    sqlCalcPrice.ParamByName('ART2ID').AsInteger:=taARTPriceDetART2ID.AsInteger;
    sqlCalcPrice.ParamByName('ALL_').AsInteger:=0;
    sqlCalcPrice.ExecQuery;
    sqlCalcPrice.Transaction.CommitRetaining;
    RefreshDataSet(taARTPriceDet);

end;

procedure TdmInv.taStoreByItemsFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
//  Accept:=DataSet['W']>2;
end;

procedure TdmInv.taARTPriceFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
  case FCurrU of
   2:  Accept:=((DataSet['U']=1)or(DataSet['U']=3));
   1:  Accept:=((DataSet['U']=0));
  end;
end;

procedure TdmInv.taArtInsBeforeOpen(DataSet: TDataSet);
begin
 taArtIns.ParamByName('ART2ID').AsInteger:=CurrArt2Id;
{ if Screen.ActiveForm = fmPriceDict then
  taArtIns.ParamByName('ART2ID').AsInteger:=taARTPriceDetART2ID.AsInteger;
 if Screen.ActiveForm = fmSInv_det then
  taArtIns.ParamByName('ART2ID').AsInteger:=taINVItemsART2ID.AsInteger;
}
end;

procedure TdmInv.taARTPriceDetAfterScroll(DataSet: TDataSet);
begin
 ReOpenDataSet(taArtIns);
end;

procedure TdmInv.taArtInsCalcFields(DataSet: TDataSet);
begin
   taArtInsSUM.AsFloat:=taArtInsWEIGHT.AsFloat*taArtInsPRICE.AsFloat;
end;

procedure TdmInv.taARTPriceDetBeforeDelete(DataSet: TDataSet);
begin
 // taARTPriceDet.ParamByName('Art2id').AsInteger:=taARTPriceDetART2ID.AsInteger;
end;

procedure TdmInv.taArtInsAfterEdit(DataSet: TDataSet);
begin
  taArtIns.Transaction.CommitRetaining;
end;

procedure TdmInv.taLostStonesBeforeOpen(DataSet: TDataSet);
begin
 taLostStones.ParamByName('INVID').Asinteger:=taSLIStINVId.AsInteger;
 taLostStones.ParamByName('ART2ID').Asinteger:=taArtInsART2ID.AsInteger;
end;

procedure TdmInv.taStoreByItems2BeforeOpen(DataSet: TDataSet);
begin
  if FCurrState=ROOT_STATE_CODE then
  begin
    taStoreByItems2.ParamByName('ST1').AsInteger :=-1;
    taStoreByItems2.ParamByName('ST2').AsInteger:=10;
  end
  else begin
     taStoreByItems2.ParamByName('ST1').AsInteger:=FCurrState;
     taStoreByItems2.ParamByName('ST2').AsInteger:=FCurrState;
  end;
  if FCurrMat = '' then
  begin
    taStoreByItems2.ParamByName('MAT1').AsString := #32;
    taStoreByItems2.ParamByName('MAT2').AsString := #255;
  end
  else
  begin
    taStoreByItems2.ParamByName('MAT1').AsString := FCurrMat;
    taStoreByItems2.ParamByName('MAT2').AsString := FCurrMat;
  end;
  //taStoreByItems2.ParamByName('SELFCOMPID').AsInteger:=dm.User.SelfCompId;
end;

procedure TdmInv.taAVListBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.whBeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.whEndDate);
  end;
end;

procedure TdmInv.taArtVedomostBeforeOpen(DataSet: TDataSet);
begin
   taArtVedomost.ParamByName('ACTID').AsInteger:=taAVListID.AsInteger;
end;

procedure TdmInv.taLostActsBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(dm.EndDate);
  end;
end;

procedure TdmInv.taAllLostStonesBeforeOpen(DataSet: TDataSet);
begin
  taAllLostStones.ParamByName('INVID').AsInteger:=taLostActsINVID.AsInteger;
end;

procedure TdmInv.taPactsPrintBeforeOpen(dataSet: TDataSet);
begin
 taPactsPrint.ParamByName('INVID').AsInteger:=taSListINVID.AsInteger;
end;

procedure TdmInv.taPriceACTSNewRecord(DataSet: TDataSet);
begin
  taPriceACTSACT_ITEMID.AsInteger := dm.GetId(72);
end;

procedure TdmInv.taPACTSNewRecord(DataSet: TDataSet);
begin
  taPACTSACT_ITEMID.AsInteger := dm.GetId(72);
end;

procedure TdmInv.taSemisReportBeforeEdit(DataSet: TDataSet);
begin
  if(dmMain.taVListISCLOSE.AsInteger = 1)then
    raise Exception.Create(Format(rsVSheetClose, ['']));
end;

constructor TdmInv.Create(AOwner: TComponent);
begin
  inherited;
  //
end;

procedure TdmInv.taSItemUpdateRecord(DataSet: TDataSet;
  UpdateKind: TUpdateKind; var UpdateAction: TFIBUpdateAction);
begin
  if dmINV.taSListISCLOSE.ASinteger=1 then
   raise Exception.Create('������ �������� �������� ���������!');
 if not IsMayChangeSz then
   raise Exception.Create('���������� �������� ������');
end;

procedure TdmInv.taCashBeforeOpen(DataSet: TDataSet);
begin
  dm.SetPeriodParams(taCash.Params);
  taCash.ParamByName('SELFCOMPID').AsInteger := dm.User.SelfCompId; 
end;

procedure TdmInv.taCashTGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  case Sender.AsInteger of
    1 : Text := '�������';
    2 : Text := '������� �������';
    else Text := '������';
  end
end;

procedure TdmInv.taA2AfterScroll(DataSet: TDataSet);
begin
 dminv.CurrArt2Id:=dminv.taA2ART2ID.AsInteger;
 if fmArtIns<>nil then ReOpenDataSet(taArtIns);
end;

procedure TdmInv.taSItem4ArtIdBeforeOpen(DataSet: TDataSet);
begin
  taSItem4ArtId.ParamByName('INVID').AsInteger := taSListINVID.AsInteger;
  taSItem4ArtId.ParamByName('ARTID').AsInteger := FCurrARTID;
end;

procedure TdmInv.taInv_Store4ArtIdBeforeOpen(DataSet: TDataSet);
begin
  if FCurrDep=ROOT_COMP_CODE then
  begin
    taInv_Store4ArtId.ParamByName('DEPID1').AsInteger :=-MAXINT;
    taInv_Store4ArtId.ParamByName('DEPID2').AsInteger := MAXINT;
  end
  else begin
    taInv_Store4ArtId.ParamByName('DEPID1').AsInteger := FCurrDep;
    taInv_Store4ArtId.ParamByName('DEPID2').AsInteger := FCurrDep;
  end;

   taInv_Store4ArtId.ParamByName('ARTID1').AsInteger := FCurrArtId;
   taInv_Store4ArtId.ParamByName('ARTID2').AsInteger := FCurrArtId;

   case dmInv.ITYPE of
     2, 12 : begin
       taInv_Store4ArtId.ParamByName('STATE').AsInteger:=0;
       taInv_Store4ArtId.ParamByName('SUPID').AsVariant:=Null;
     end;
     3: begin
       taInv_Store4ArtId.ParamByName('STATE').AsInteger:=1;
       taInv_Store4ArtId.ParamByName('SUPID').AsInteger := taSListSUPID.AsInteger;
     end;
     else raise Exception.Create(rsNotImplementation);
   end;
end;

procedure TdmInv.taCashCalcFields(DataSet: TDataSet);
begin
  case taCashT.AsInteger of
    1 : begin
      taCashCOSTSELL.AsFloat := taCashCOST.AsFloat;
      taCashCOSTRET.AsFloat := 0;
    end;
    2 : begin
      taCashCOSTSELL.AsFloat := 0;
      taCashCOSTRET.AsFloat := taCashCOST.AsFloat;
    end;
    else begin
      taCashCOSTSELL.AsFloat := 0;
      taCashCOSTRET.AsFloat := 0;
    end;
  end
end;

procedure TdmInv.taCompBeforeOpen(DataSet: TDataSet);
begin
  taComp.ParamByName('SELFCOMPID').AsInteger := dm.User.SelfCompId;
end;

procedure TdmInv.taPriceListBeforeOpen(DataSet: TDataSet);
begin
 //if AART='' then AArt:='%';
 AART:=AART+'%';
 taPriceList.ParamByName('AART').AsString:=AArt;
end;

procedure TdmInv.taDepNameBeforeOpen(DataSet: TDataSet);
var
  ParamIndex : integer;
begin
  if taDepName.ParamExist('SELFCOMPID', ParamIndex) then
    taDepName.ParamByName('SELFCOMPID').asinteger:=dm.User.SelfCompId;
end;

procedure TdmInv.LoadInv(const FileName: string);
var f:textfile;
    str:string;
    uid:integer;
    st:string;
    i:integer;
    price:real;
begin
 try
  AssignFile(f,FileName);
  Reset(f);
  taINVbyItems.First;
  while not Eof(f) do
  begin
   readln(f,str);
   st:='';
   i:=0;
   while (str[i]<>' ')
   do begin
    st:=st+str[i];
    inc(i);
   end;
   uid:=StrTOInt(st);
   //����

   st:='';
   while (i<Length(str))
   do begin
    st:=st+str[i];
    inc(i);
   end;
   price:=StrTOFloat(st);

   uid:=StrTOInt(str);
   taStoreByItems.Locate('UID',uid,[loCaseInsensitive])
  end;
 finally
  CloseFile(f);
 end;
end;

procedure TdmInv.SaveInv(const FileName: string);
var f:textfile;
    str:string;
begin
 try
  ReOpenDataSet(taINVbyItems);
  AssignFile(f,FileName);
  Rewrite(f);
  taINVbyItems.First;
  while not taINVbyItems.Eof do
  begin
   str:=IntTOStr(taINVbyItemsUID.asinteger);
   write(f,str+' ');
   str:=FloatTOStr(taINVbyItemsPRICE2.asfloat);
   writeln(f,str);
   taINVbyItems.Next;
  end;
 finally
  CloseDataSet(taINVbyItems);
  CloseFile(f);
 end;
end;

procedure TdmInv.taRev_detBeforeOpen(DataSet: TDataSet);
begin
 taRev_det.ParamByName('invid').asinteger:=taSListINVID.AsInteger;
end;

procedure TdmInv.taNotInInvBeforeOpen(DataSet: TDataSet);
begin
 if Screen.ActiveForm=fmSINVProd
 then taNotInInv.ParamByName('INVID').AsInteger:= dmmain.taASListINVID.AsInteger
 else taNotInInv.ParamByName('INVID').AsInteger:=taSListINVID.AsInteger;
end;

procedure TdmInv.taStoreByItems2AfterOpen(DataSet: TDataSet);
begin
  if Assigned(fmArtIns) then
  begin
    CurrArt2Id := taStoreByItems2ART2ID.AsInteger;
    ReOpenDataSet(taArtIns);
  end;
end;

procedure TdmInv.taEServParamsBeforeOpen(DataSet: TDataSet);
var InvoiceID: Integer;
begin
  if taSList.Active = false then
    InvoiceID := dmMain.taSIListINVID.AsInteger
  else InvoiceID := taSListINVID.AsInteger;

  taEServParams.ParamByName('INVID').AsInteger := InvoiceID;
end;

procedure TdmInv.taEServParamsNewRecord(DataSet: TDataSet);
var InvoiceID: Integer;
begin
  if taSList.Active = false then
    InvoiceID := dmMain.taSIListINVID.AsInteger
  else InvoiceID := taSListINVID.AsInteger;
  //
  taEServParamsID.AsInteger := dm.GetId(98);
  EServParamID := taEServParamsID.AsInteger;
  taEServParamsINVID.AsInteger := InvoiceID;
  taEServParamsN.AsFloat := 0;
  taEServParamsSERVCOST.AsFloat := 0;
  taEServParamsEXTRA.AsFloat := 0;
  taEServParamsU.AsInteger:= 3;
end;

procedure TdmInv.PostBeforeClose(DataSet: TDataSet);
begin
  PostDataSet(DataSet);
end;

procedure TdmInv.taSListCalcFields(DataSet: TDataSet);
begin
  if taSListQ.AsInteger = 0 then
    taSListREVISIONPERCENT.AsFloat := 0
  else
    taSListREVISIONPERCENT.AsFloat := (taSListW.AsFloat/taSListQ.AsInteger)*100;
end;

procedure TdmInv.taStoreByItems2BUH_10GetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  case Sender.AsInteger of
    0 : Text := '���43';
    1 : Text := '���10';
    2 : Text := '���003';
  end;
end;

procedure TdmInv.taINVItemsAfterRefresh(DataSet: TDataSet);
begin
  if not taSList.FieldByName('Prihod_ID').IsNull then
  begin
    taSListCalcPrihod.Close;
    taSListCalcPrihod.Open;
  end;
end;

procedure TdmInv.taSListCalcPrihodBeforeOpen(DataSet: TDataSet);
begin
  taSListCalcPrihod.ParamByName('InvID').ASInteger := taSList.FieldByName('InvID').AsInteger;
  taSListCalcPrihod.ParamByName('Mode').AsInteger := taSListCalcPrihodMode;
end;

procedure TdmInv.taINVItemsAfterOpen(DataSet: TDataSet);
begin
  if not taSList.FieldByName('Prihod_ID').IsNull then
  begin
    taSListCalcPrihod.Close;
    taSListCalcPrihod.Open;
  end;
end;

end.


