inherited fmPaytypeDict: TfmPaytypeDict
  Left = 125
  Top = 108
  Width = 484
  Height = 397
  Caption = #1042#1080#1076#1099' '#1086#1087#1083#1072#1090
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 344
    Width = 476
  end
  inherited plDict: TPanel
    Width = 476
    Height = 302
    inherited plSelect: TPanel
      Top = 259
      Width = 472
      inherited btnOk: TSpeedButton
        Left = 294
      end
      inherited btnCancel: TSpeedButton
        Left = 378
      end
    end
    inherited gridDict: TDBGridEh
      Width = 472
      Height = 257
      DataSource = dm.dsrPaytype
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Title.EndEllipsis = True
          Width = 24
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'NAME'
          Footers = <>
          Title.Caption = #1053#1072#1080#1084#1077#1085#1086#1074#1072#1085#1080#1077
          Title.EndEllipsis = True
          Width = 199
        end
        item
          DisplayFormat = '0.##'
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'EXTRA'
          Footers = <>
          Title.Caption = #1053#1072#1094#1077#1085#1082#1072',%'
          Title.EndEllipsis = True
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'APPLT'
          Footers = <>
          KeyList.Strings = (
            '0'
            '1')
          PickList.Strings = (
            #1087#1088#1086#1076#1072#1078#1072
            #1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1102)
          Title.EndEllipsis = True
          Width = 67
        end>
    end
  end
  inherited tb1: TSpeedBar
    Width = 476
  end
end
