unit ExportMC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, ExtCtrls, ActnList,  ImgList,
  ComCtrls, TB2Item, TB2Dock, TB2Toolbar, Grids, DBGridEh, 
  StdCtrls, Mask, DBCtrlsEh, FIBDatabase, pFIBDatabase, DB, FIBDataSet,
  pFIBDataSet, FIBQuery, pFIBQuery, DateUtils, DBGridEhGrouping, GridsEh,
  rxPlacemnt, rxSpeedbar;

type
  TfmExportMC = class(TfmAncestor)
    plLeft: TPanel;
    tbdcSemis: TTBDock;
    TBToolbar2: TTBToolbar;
    TBItem6: TTBItem;
    TBItem5: TTBItem;
    acAdd: TAction;
    acDel: TAction;
    gridInvToMC: TDBGridEh;
    plRight: TPanel;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    TBControlItem1: TTBControlItem;
    Label1: TLabel;
    edPath: TDBEditEh;
    TBControlItem2: TTBControlItem;
    taInv: TpFIBDataSet;
    dbMC: TpFIBDatabase;
    taInvToMC: TpFIBDataSet;
    taInvToMCINVID: TIntegerField;
    taInvToMCDOCNO: TIntegerField;
    taInvToMCDOCDATE: TDateTimeField;
    taInvToMCQ: TIntegerField;
    taInvToMCW: TFloatField;
    taInvToMCSUMM1: TFloatField;
    taInvToMCSUMM2: TFloatField;
    taInvToMCACC_SUMM: TFloatField;
    dsrInvToMC: TDataSource;
    trMC: TpFIBTransaction;
    taInvFromMC: TpFIBDataSet;
    gridInvFromMC: TDBGridEh;
    dsrInvFromMC: TDataSource;
    quTmpMC: TpFIBQuery;
    taInvFromMCINVID: TIntegerField;
    taInvFromMCDOCNO: TIntegerField;
    taInvFromMCDOCDATE: TDateTimeField;
    taInvFromMCQ: TIntegerField;
    taInvFromMCW: TFloatField;
    taInvFromMCSUMM1: TFloatField;
    taInvFromMCSUMM2: TFloatField;
    taInvFromMCACC_SUMM: TFloatField;
    dlgDb: TOpenDialog;
    spitExport: TSpeedItem;
    acExport: TAction;
    taComp: TpFIBDataSet;
    taNDS: TpFIBDataSet;
    taPayType: TpFIBDataSet;
    splMain: TSplitter;
    taInvFromMCSTATUS: TSmallintField;
    taRec: TpFIBDataSet;
    taExpInv: TpFIBDataSet;
    taA2Ins: TpFIBDataSet;
    taClient: TpFIBDataSet;
    TBDock2: TTBDock;
    tlbrDep: TTBToolbar;
    acSelDep: TAction;
    sitDep: TTBSubmenuItem;
    TBControlItem3: TTBControlItem;
    lbDep: TLabel;
    taRecDEPID: TIntegerField;
    taRecID: TFIBIntegerField;
    taRecPMC: TFIBIntegerField;
    taRecRETAILCOMPID: TFIBIntegerField;
    taRecUNKNOWNCLIENTID: TFIBIntegerField;
    taRecINVNO: TIntegerField;
    taRecCDMID: TIntegerField;
    taInvFromMCNDSID: TFIBIntegerField;
    taInvFromMCSUPID: TFIBIntegerField;
    TBItem1: TTBItem;
    TBSeparatorItem1: TTBSeparatorItem;
    acClearDbMC: TAction;
    procedure taInvToMCBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CommitRetaining(DataSet: TDataSet);
    procedure taInvFromMCBeforeOpen(DataSet: TDataSet);
    procedure edPathEditButtons0Click(Sender: TObject; var Handled: Boolean);
    procedure edPathKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acExportExecute(Sender: TObject);
    procedure acExportUpdate(Sender: TObject);
    procedure acAddExecute(Sender: TObject);
    procedure acAddUpdate(Sender: TObject);
    procedure acDelUpdate(Sender: TObject);
    procedure taInvFromMCSTATUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
    procedure acDelExecute(Sender: TObject);
    procedure gridInvFromMCGetCellParams(Sender: TObject;
      Column: TColumnEh; AFont: TFont; var Background: TColor;
      State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure taExpInvBeforeOpen(DataSet: TDataSet);
    procedure taA2InsBeforeOpen(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure taRecCalcFields(DataSet: TDataSet);
    procedure acClearDbMCExecute(Sender: TObject);
    procedure acClearDbMCUpdate(Sender: TObject);
  private
    FDepId : integer; // ����� ������� ���������, ������ ������������ ��������
    procedure OpenMC(err : byte);
    procedure DepClick(Sender : TObject);
  end;

var
  fmExportMC: TfmExportMC;

implementation

uses DictData, dbUtil, MsgDialog, MainData, fmUtils, ProductionConsts;

{$R *.dfm}

procedure TfmExportMC.taInvToMCBeforeOpen(DataSet: TDataSet);
begin
  with TpFIBDataSet(DataSet).Params do
  begin
    ByName['BD'].AsTimeStamp := DateTimeToTimeStamp(dm.BeginDate);
    ByName['ED'].AsTimeStamp := DateTimeToTimeStamp(StrToDateTime('01.01.2050'));
    ByName['DEPID'].AsInteger := FDepId;
  end;
end;

procedure TfmExportMC.FormCreate(Sender: TObject);
var
  Item : TTBItem;
  i : integer;
begin
  if taInvToMC.Transaction.Active then taInvToMC.Transaction.CommitRetaining;
  FDepId := -1;
  for i:=0 to Pred(dm.CountDep) do
    if (dm.DepInfo[i].DepTypes and $4)=$4 then
    begin
      Item := TTBItem.Create(sitDep);
      Item.OnClick := DepClick;
      Item.Caption := dm.DepInfo[i].SName;
      Item.Tag := dm.DepInfo[i].DepId;
      Item.OnClick := DepClick;
      sitDep.Add(Item);
      if Item.Tag = dm.DepDef then
      begin
        FDepId := dm.DepDef;
        DepClick(Item);
      end;
    end;
  OpenDataSets([taInvToMC, taInvFromMC]);
end;

procedure TfmExportMC.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSet(taInvToMC);
  taInvToMC.Transaction.CommitRetaining;
  dbMC.Close;
end;

procedure TfmExportMC.CommitRetaining(DataSet: TDataSet);
begin
  if DataSet.InheritsFrom(TpFIBDataSet) then
    TpFIbDataSet(DataSet).Transaction.CommitRetaining;
end;

procedure TfmExportMC.taInvFromMCBeforeOpen(DataSet: TDataSet);
var
  s : string;
begin
  with TpFIbDataSet(DataSet).Params do
  begin
    taInv.Open;
    if taInv.IsEmpty then s:='0'
    else begin
      s := '';
      while not taInv.Eof do
      begin
        s := s+taInv.Fields[0].AsString+',';
        taInv.Next;
      end;
      taInv.Close;
      s:=copy(s, 1, Length(s)-1);
    end;
    taInvFromMC.SelectSQL[4] := 'INVID in ('+s+')';
    TpFIBDataSet(DataSet).ParamByName('BD').AsTimeStamp := DateTimeToTimeStamp(StrToDateTime('01.01.1900'));
    TpFIBDataSet(DataSet).ParamByName('ED').AsTimeStamp := DateTimeToTimeStamp(StrToDateTime('01.01.2050'));
    TpFIBDataSet(DataSet).ParamByName('DEPID').AsInteger := FDepId;
  end;
end;

procedure TfmExportMC.edPathEditButtons0Click(Sender: TObject; var Handled: Boolean);
begin
  if dlgDb.Execute then
  begin
    edPath.Text := dlgDb.FileName;
    OpenMC(1);
  end;
  Handled := True;
end;

procedure TfmExportMC.edPathKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN : OpenMC(1);
  end;
end;

procedure TfmExportMC.OpenMC;
var
  Bookmark : Pointer;
  fApply : boolean;
begin
  if dbMC.Connected then
  begin
    // ���� �� �� �������� ���������
    if taInvFromMC.Active then
    begin
      taInvFromMC.DisableControls;
      BookMark := taInvFromMC.GetBookmark;
      try
        taInvFromMC.First;
        fApply := False;
        while not taInvFromMC.Eof do
        begin
          if(not taInvFromMCSTATUS.IsNull)then
            if(taInvFromMCSTATUS.AsInteger = 0)then
            begin
              fApply := True;
              break;
            end;
          taInvFromMC.Next;
        end;
      finally
        taInvFromMC.GotoBookmark(BookMark);
        taInvFromMC.EnableControls;
      end;
      if(fApply)and(MessageDialog('������� �� �������� ���������. �������?', mtWarning, [mbYes, mbNo], 0)=mrYes) then
        acExport.Execute
      else ReOpenDataSet(taInvToMC);
      CloseDataSet(taInvFromMC);
      taInvFromMC.Transaction.CommitRetaining;
    end;
  end;
  if trMc.Active then trMc.Rollback;
  dbMC.CloseDataSets;
  dbMC.Close;
  dbMC.DatabaseName := edPath.Text;
  try
    dbMC.Open;
    // ������� ���������� �� ��� ��� ���� ������ ������� ��� �������
    try
      if trMC.Active then trMC.Commit;
      trMC.StartTransaction;
      quTmpMC.SQL.Text := 'select pmc from D_Rec';
      quTmpMC.ExecQuery;
      quTmpMC.Close;
      trMC.Commit;
    except
      on E:Exception do
      begin
        dbMC.Close;
        if(err=1)then MessageDialogA('�� ������ ������ �� ������� "��������� ������"', mtWarning);
        eXit;
      end;
    end;    
    OpenDataSet(taInvFromMC);
  except
    on E:Exception do MessageDialogA(E.Message, mtError);
  end;
end;

procedure TfmExportMC.acExportExecute(Sender: TObject);

  procedure AddDictPacket(DataSet : TDataSet; TableName : string; GeneratorName : string; EmptyTable: boolean = True);
  var
    i, genval : integer;
    s1, s2 : string;
  begin
    try
      if EmptyTable then
      begin
        quTmpMC.Close;
        quTmpMC.SQL.Text := 'delete from '+TableName;
        quTmpMC.ExecQuery;
        quTmpMC.Transaction.CommitRetaining;
      end;
      OpenDataSet(DataSet);
      DataSet.First;
      while not DataSet.Eof do
      begin
        s1 := 'insert into '+TableName+'(';
        s2 := 'values(';
        for i:=0 to Pred(DataSet.FieldCount) do
        begin
          s1 := s1+DataSet.Fields[i].FieldName+',';
          if DataSet.Fields[i].IsNull then s2 := s2+'NULL,'
          else case DataSet.Fields[i].DataType of
            ftString, ftFixedChar, ftWideString : s2 := s2+#39+StringReplace(DataSet.Fields[i].AsString,#39,'"',[rfReplaceAll])+#39',';
            ftSmallint, ftInteger, ftLargeint, ftWord : s2 := s2+IntToStr(DataSet.Fields[i].AsInteger)+',';
            ftFloat, ftCurrency : s2 := s2+StringReplace(FloatToStr(DataSet.Fields[i].AsFloat), DecimalSeparator, '.', [])+',';
            ftDateTime : s2 := s2+#39+DateTimeToStr(DataSet.Fields[i].AsDateTime)+#39',';
            ftDate     : s2 := s2+#39+DateToStr(DataSet.Fields[i].AsDateTime)+#39',';
            else raise Exception.Create('�� ��������� ��� ������');
          end;
        end;
        s1:=copy(s1,1,Length(s1)-1)+') ';
        s2:=copy(s2,1,Length(s2)-1)+')';
        quTmpMC.Close;
        quTmpMC.SQL.Text := s1+s2;
        quTmpMC.ExecQuery;
        DataSet.Next;
      end;
      quTmpMC.Transaction.CommitRetaining;
      CloseDataSet(DataSet);
      // ���������� �������� ����������
      if(GeneratorName<>'')then
      begin
        genval := ExecSelectSQL('select gen_id('+GeneratorName+', 0) from rdb$database', dm.quTmp);
        quTmpMC.Close;
        quTmpMC.SQL.Text := 'set generator '+GeneratorName+' to '+IntToStr(genval+1);
        quTmpMC.ExecQuery;
        quTmpMC.Transaction.CommitRetaining;
      end;
    finally
    end;
  end;

  procedure AddInvPacket(DataSet : TDataSet; TableName : string; GeneratorName : string);
  var
    i, genval : integer;
    s1, s2 : string;
  begin
    try
      OpenDataSet(DataSet);
      DataSet.First;
      while not DataSet.Eof do
      begin
        s1 := 'insert into '+TableName+'(';
        s2 := 'values(';
        for i:=0 to Pred(DataSet.FieldCount) do
        begin
          s1 := s1+DataSet.Fields[i].FieldName+',';
          if DataSet.Fields[i].IsNull then s2 := s2+'NULL,'
          else case DataSet.Fields[i].DataType of
            ftString, ftFixedChar, ftWideString : s2 := s2+#39+StringReplace(DataSet.Fields[i].AsString,#39,'"',[rfReplaceAll])+#39',';
            ftSmallint, ftInteger, ftLargeint, ftWord : s2 := s2+IntToStr(DataSet.Fields[i].AsInteger)+',';
            ftFloat, ftCurrency : s2 := s2+StringReplace(FloatToStr(DataSet.Fields[i].AsFloat), DecimalSeparator, '.', [])+',';
            ftDateTime : s2 := s2+#39+DateTimeToStr(DataSet.Fields[i].AsDateTime)+#39',';
            ftDate     : s2 := s2+#39+DateToStr(DataSet.Fields[i].AsDateTime)+#39',';
            else raise Exception.Create('�� ��������� ��� ������');
          end;
        end;
        s1:=copy(s1,1,Length(s1)-1)+') ';
        s2:=copy(s2,1,Length(s2)-1)+')';
        quTmpMC.Close;
        quTmpMC.SQL.Text := s1+s2;
        try
        quTmpMC.ExecQuery;
        except
         on E: exception do ShowMessage(quTmpMC.SQL.Text);
        end;
        DataSet.Next;
      end;
      quTmpMC.Transaction.CommitRetaining;
      CloseDataSet(DataSet);

      // ���������� �������� ����������
      if(GeneratorName<>'')then
      begin
        genval := ExecSelectSQL('select gen_id('+GeneratorName+', 0) from rdb$database', dm.quTmp);
        quTmpMC.Close;
        quTmpMC.SQL.Text := 'set generator '+GeneratorName+' to '+IntToStr(genval+1);
        quTmpMC.ExecQuery;
        quTmpMC.Transaction.CommitRetaining;
      end;
    finally
    end;
  end;

var
  c : integer;
  str:string;
  Make_DoubleInv : boolean;
begin
  Make_DoubleInv := MessageDialog(rsMC_MakeDoubleInv, mtConfirmation, [mbYes, mbNo], 0)=mrYes;
  Self.Update;

  // ��������� ���� �� ��������� � �� ��������� ������
  c:= ExecSelectSQL('select count(*) from Inv', quTmpMC);
  if(c<>0)and (MessageDialog('� ������� ��������� ������ ���� �� ����������� ���������.'#13+
    '������� ��?', mtWarning, [mbYes, mbNo], 0)=mrYes)
  then   // ������� ���������
    ExecSQL('delete from Inv', quTmpMC);

  Self.Update;

  // ��������� ���� �� ������ �����
  c:= ExecSelectSQL('select count(*) from Cash', quTmpMC);
  if (c<>0)and(MessageDialog('� ������� ��������� ������ ���� �� ����������� ������ �� �������.'#13+
    '������� ��?', mtWarning, [mbYes, mbNo], 0)=mrYes) then
      // ������� ������
    ExecSQL('delete from Cash', quTmpMC);

  Self.Update;


  // ����� �� ����� ��
  if (FDepId = -1) then
    raise Exception.Create('�������� ����� ������� ���������');
  // ������������ �����������
  if (MessageDialog('���������� �����������?', mtWarning, [mbYes, mbNo], 0)=mrYes) then begin
    AddDictPacket(taComp, 'D_COMP', 'GEN_COMP_ID');
    AddDictPacket(taNDS, 'D_NDS', 'GEN_NDS_ID');
    AddDictPacket(taPayType, 'D_PAYTYPE', 'GEN_PAYTYPE_ID');
    AddDictPacket(taClient, 'D_CLIENT', 'GEN_CLIENT_ID');
    AddDictPacket(taRec, 'D_REC', '');
  end;
  // �������� ���������� �������

  ExecSQL('delete from Ins where not(Art2Id in (select distinct Art2Id from SInfo))', quTmpMC);
  // ������������ ���������
  taInvFromMC.First;
  while not taInvFromMC.Eof do
  begin
    if(taInvFromMCSTATUS.AsInteger=0)then
    begin
      str := 'execute procedure Ext_Inv_I('+taInvFromMCINVID.AsString+','+taInvFromMCDOCNO.AsString+','+
        QuotedStr(DateTimeToStr(taInvFromMCDOCDATE.AsDateTime));
      if taInvFromMCNDSID.IsNull then str := str+',null' else str := str+','+taInvFromMCNDSID.AsString;
      if taInvFromMCSUPID.IsNull then str := str+',null' else str := str+','+taInvFromMCSUPID.AsString;
      if Make_DoubleInv then str := str+',1' else str := str+',0';
      str := str+')';
      ExecSQL(str,quTmpMC);
      AddInvPacket(taExpInv, 'SINFO', 'GEN_SINFO_ID');
      AddDictPacket(taA2Ins, 'INS', '', False);
      ExecSQL('update Inv set MC=setbit(MC, 1, 1) where InvId='+taInvFromMCINVID.AsString, dm.quTmp);
      taInvFromMC.Edit;
      taInvFromMCSTATUS.AsInteger := 1;
      taInvFromMC.Post;
    end;
    taInvFromMC.Next;
  end;
  // ��������� ������������ ���������
  if Make_DoubleInv then ExecSQL('execute procedure DOUBLE_INV',quTmpMC);
end;

procedure TfmExportMC.acExportUpdate(Sender: TObject);
begin
  acExport.Enabled := dbMC.Connected and taInvFromMC.Active;
end;

procedure TfmExportMC.acAddExecute(Sender: TObject);
var
  InvId : integer;
begin
  // ��������� ���� ��� ����� � ���� ��������� ���������
  if taInvToMCDOCNO.IsNull then raise EWarning.Create(rsNotSelFieldDocNo);
  if taInvToMCDOCDATE.IsNull then raise EWarning.Create(rsNotSelFieldDocDate);
  InvId := taInvToMCINVID.AsInteger;
  taInvToMC.Delete;
  taInvFromMC.Insert;
  taInvFromMCINVID.AsInteger := InvId;
  taInvFromMCSTATUS.AsInteger := 0;
  taInvFromMC.Post;
end;

procedure TfmExportMC.acAddUpdate(Sender: TObject);
begin
  acAdd.Enabled := taInvToMC.Active and taInvFromMC.Active and (not taInvToMC.IsEmpty);
end;

procedure TfmExportMC.acDelUpdate(Sender: TObject);
begin
  acDel.Enabled := taInvToMC.Active and taInvFromMC.Active and (not taInvFromMC.IsEmpty);
end;

procedure TfmExportMC.taInvFromMCSTATUSGetText(Sender: TField; var Text: String; DisplayText: Boolean);
begin
  if Sender.IsNull then Text := ''
  else case Sender.AsInteger of
    0 : Text := '�� ������';
    1 : Text := '������';
  end;
end;

procedure TfmExportMC.acDelExecute(Sender: TObject);
var
  InvId : integer;
  c : integer;
begin
  if(taInvFromMCSTATUS.AsInteger = 1)then
  begin
    c:= ExecSelectSQL('select count(*) from Inv', quTmpMC);
    if(c<>0)and (MessageDialog('� ������� ��������� ������ ���� �� ����������� ���������.'#13+
      '� ����� ����������� ������� �� ��� ����� �������.'#13+
      '����������?', mtWarning, [mbYes, mbNo], 0)=mrNo)
    then eXit;
    Self.Update;    
    ExecSQL('delete from Ext_Inv where INVID='+taInvFromMCINVID.AsString, quTmpMC);
    ExecSQL('execute procedure Del_Empty_Inv', quTmpMC);    
  end;
  InvId := taInvFromMCINVID.AsInteger;
  taInvFromMC.Delete;
  taInvToMC.Insert;
  taInvToMCINVID.AsInteger := InvId;
  taInvToMC.Post;
  ExecSQL('update Inv set MC=setbit(MC, 1, 0) where InvId='+taInvToMCINVID.AsString, dm.quTmp);
end;

procedure TfmExportMC.gridInvFromMCGetCellParams(Sender: TObject;
  Column: TColumnEh; AFont: TFont; var Background: TColor;
  State: TGridDrawState);
begin
  if(Column.FieldName='STATUS')and(Column.Field.AsInteger=0) then AFont.Color := clRed;
end;

procedure TfmExportMC.FormShow(Sender: TObject);
begin
  if FileExists(edPath.Text)then
  begin
    OpenMC(0);
  end
end;

procedure TfmExportMC.taExpInvBeforeOpen(DataSet: TDataSet);
begin
  taExpInv.ParamByName('INVID').AsInteger := taInvFromMCINVID.AsInteger;
end;

procedure TfmExportMC.taA2InsBeforeOpen(DataSet: TDataSet);
begin
  taA2Ins.ParamByName('INVID').AsInteger := taInvFromMCINVID.AsInteger;
end;

procedure TfmExportMC.Button1Click(Sender: TObject);
begin
  inherited;
  ShowMessage(taInvFromMC.SelectSQL.Text);
end;

procedure TfmExportMC.DepClick(Sender: TObject);
begin
  FDepId := (Sender as TTBItem).Tag;
  ReOpenDataSets([taInvToMC, taInvFromMC]);
  if FDepId = -1 then lbDep.Caption := '�� ������'
  else lbDep.Caption := (Sender as TTBItem).Caption;
end;

procedure TfmExportMC.taRecCalcFields(DataSet: TDataSet);
var
  Id : Variant;
begin
  taRecDEPID.AsInteger := FDepId;
  Id := ExecSelectSQL('select max(DocNo) from Inv where IType=2 and EYear(DOCDATE)=EYEAR('+#39'NOW'#39+') and MC=3', dm.quTmp);
  if VarIsNull(Id)or(Id=0)then
  begin
    Id := ExecSelectSQL('select OFFSET_MC from D_Rec', dm.quTmp);
    if VarIsNull(Id)or(Id=0)then Id := 1000;
  end;
  taRecINVNO.AsInteger := Id;
  taRecCDMID.AsInteger := dm.User.SelfCompId;
end;

procedure TfmExportMC.acClearDbMCExecute(Sender: TObject);
begin
  if MessageDialog(rsMC_ClearDb, mtConfirmation, [mbYes, mbNo], 0)=mrNo then eXit;
  Self.Update;
  try
    CloseDataSet(taInvFromMC);
    ExecSQL('delete from SellRet', quTmpMC);
    ExecSQL('delete from Inv', quTmpMC);
    ExecSQL('delete from Sel', quTmpMC);
    ExecSQL('delete from SItem', quTmpMC);
    ExecSQL('delete from SInfo', quTmpMC);
    ExecSQL('delete from Ins', quTmpMC);
    ExecSQL('delete from Cash', quTmpMC);
    ExecSQL('delete from D_Comp', quTmpMC);
    ExecSQL('delete from D_Client', quTmpMC);
    ExecSQL('delete from D_NDS', quTmpMC);
    ExecSQL('delete from D_PayType', quTmpMC);
    ExecSQL('delete from Ins', quTmpMC);
    ExecSQL('delete from Cash', quTmpMC);
    ExecSQL('delete from Ext_Inv', quTmpMC);
    ExecSQL('delete from D_Rec', quTmpMC);
    OpenDataSet(taInvFromMC);
  except
    on E:Exception do MessageDialog('������ ��� ���������� �������:'#13+quTmpMC.SQL.Text+#13+
      E.Message+#13+rsCallSupport, mtError, [mbOk], 0);
  end;
end;

procedure TfmExportMC.acClearDbMCUpdate(Sender: TObject);
begin
  acClearDbMC.Enabled := dbMC.Connected;
end;

end.


