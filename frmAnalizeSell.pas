unit frmAnalizeSell;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxLabel, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxCalendar, cxControls, cxContainer, cxCheckListBox,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, DB,
  cxDBData, cxLookAndFeelPainters, StdCtrls, cxButtons, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, FIBDataSet, pFIBDataSet, cxGroupBox, Provider,
  DBClient, cxGridBandedTableView, cxGridDBBandedTableView, cxSplitter,
  dxStatusBar, cxProgressBar, cxLookAndFeels, Menus;

type
  TDialogAnalizeSell = class(TForm)
    ListOrganization: TcxCheckListBox;
    ListCompany: TcxCheckListBox;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    PanelLeft: TPanel;
    PanelRight: TPanel;
    GridLevel: TcxGridLevel;
    Grid: TcxGrid;
    PanelLeftBottom: TPanel;
    ButtonExecute: TcxButton;
    DataSource: TDataSource;
    Query: TpFIBDataSet;
    PanelRightBottom: TPanel;
    DataSet: TClientDataSet;
    DataSetProvider: TDataSetProvider;
    Result: TClientDataSet;
    cxGroupBox1: TcxGroupBox;
    cxLabel1: TcxLabel;
    DateEditPeriodBegin: TcxDateEdit;
    cxLabel5: TcxLabel;
    DateEditPeriodEnd: TcxDateEdit;
    ResultUID: TIntegerField;
    ResultART: TStringField;
    ResultART2: TStringField;
    ResultSZ: TStringField;
    ResultWEIGHT: TFloatField;
    ResultSELFCOMPANYTITLE: TStringField;
    ResultCompanyTitle: TStringField;
    Panel1: TPanel;
    ButtonOK: TcxButton;
    StatusBar: TdxStatusBar;
    GridDBTableView: TcxGridDBTableView;
    GridDBTableViewUID: TcxGridDBColumn;
    GridDBTableViewART: TcxGridDBColumn;
    GridDBTableViewART2: TcxGridDBColumn;
    GridDBTableViewSZ: TcxGridDBColumn;
    GridDBTableViewWEIGHT: TcxGridDBColumn;
    GridDBTableViewSELFCOMPANYTITLE: TcxGridDBColumn;
    GridDBTableViewCompanyTitle: TcxGridDBColumn;
    Panel2: TPanel;
    StatusBarContainer0: TdxStatusBarContainerControl;
    ProgressBar: TcxProgressBar;
    procedure FormCreate(Sender: TObject);
    procedure ButtonExecuteClick(Sender: TObject);
  private
    function GetData(SQL: string; Params: array of Variant): OleVariant;
  public
    { Public declarations }
  end;

var
  DialogAnalizeSell: TDialogAnalizeSell;

implementation

uses DictData, DateUtils;

{$R *.dfm}

procedure TDialogAnalizeSell.FormCreate(Sender: TObject);
var
  ID: Integer;
  Title: string;
  Organizations: TClientDataSet;
  Companies: TClientDataSet;
  Item: TcxCheckListBoxItem;
  OrganizationID: Integer;
begin
  OrganizationID := dm.User.SelfCompId;
  BoundsRect := Screen.WorkAreaRect;

  Organizations := TClientDataSet.Create(Self);
  Organizations.Data := GetData('select compid ID, name Title from d_comp where islogin = 1 order by name', []);

  Organizations.First;
  ListOrganization.Items.BeginUpdate;
  while not Organizations.Eof do
  begin
    ID := Organizations.FieldByName('ID').AsInteger;
    Title := Organizations.FieldByName('Title').AsString;
    Item := ListOrganization.Items.Add;
    Item.Tag := ID;
    Item.Text := Title;
    if ID = OrganizationID then Item.Checked := True;
    Organizations.Next;
  end;
  ListOrganization.Items.EndUpdate;
  Organizations.Free;

  Companies := TClientDataSet.Create(Self);
  Companies.Data := GetData('select compid ID, name Title from d_comp where compid <> -1 and islogin is null and name is not null and name <> '#39'.'#39' order by name', []);

  Companies.First;
  ListCompany.Items.BeginUpdate;
  while not Companies.Eof do
  begin
    ID := Companies.FieldByName('ID').AsInteger;
    Title := Companies.FieldByName('Title').AsString;
    Item := ListCompany.Items.Add;
    Item.Tag := ID;
    Item.Text := Title;
    Companies.Next;
  end;
  ListCompany.Items.EndUpdate;
  Companies.Free;

  DateEditPeriodBegin.Date := StartOfTheYear(Date);
  DateEditPeriodEnd.Date := Date;
end;

function TDialogAnalizeSell.GetData(SQL: string; Params: array of Variant): OleVariant;
var
  i, j: Integer;
begin
  DataSet.Active := False;
  j := 0;
  Query.SelectSQL.Text := SQL;
  DataSet.FetchParams;
  for i := Low(Params) to High(Params) do
  begin
    DataSet.Params[j].Value := Params[i];
    Inc(j);
  end;
  DataSet.Active := True;
  Result := DataSet.Data;
  DataSet.Active := False;
end;

procedure TDialogAnalizeSell.ButtonExecuteClick(Sender: TObject);
var
  i: Integer;
  c, c1, c2: Integer;
  p1, p2, p3, p4: Variant;
  Data: TClientDataSet;
begin

  c1 := 0;
  for i := 0 to ListOrganization.Items.Count - 1 do
  if ListOrganization.Items[i].Checked then Inc(c1);


  c2 := 0;
  for i := 0 to ListCompany.Items.Count - 1 do
  if ListCompany.Items[i].Checked then Inc(c2);


  c := c1 * c2;


  ProgressBar.Properties.Min := 0;
  ProgressBar.Position := 0;
  ProgressBar.Properties.Max := c;


  c := 0;

  Result.Active := False;
  Result.CreateDataSet;
  Result.DisableControls;
  Data := TClientDataSet.Create(Self);

  p3 := DateEditPeriodBegin.Date;
  p4 := DateEditPeriodEnd.Date;

  for c1 := 0 to ListOrganization.Items.Count - 1 do
  if ListOrganization.Items[c1].Checked then
  begin
    p1 := ListOrganization.Items[c1].Tag;
    for c2 := 0 to ListCompany.Items.Count - 1 do
    if ListCompany.Items[c2].Checked then
    begin
      p2 := ListCompany.Items[c2].Tag;
      Data.Active := False;
      Data.Data := GetData('select * from analize$sell(:p1, :p2, :p3, :p4)', [p1, p2, p3, p4]);
      Data.First;
      while not Data.Eof do
      begin
        Result.Append;
        for i := 0 to Data.Fields.Count - 1 do
        Result.Fields[i].Value := Data.Fields[i].Value;
        ResultCompanyTitle.AsString := ListCompany.Items[c2].Text;
        Result.Post;
        Data.Next;
      end;
      Inc(c);
      ProgressBar.Position := c;
      ProgressBar.Perform(WM_PAINT, 0, 0);
    end;
  end;
  Data.Free;
  Result.IndexFieldNames := 'art;sz;company$title';
  Result.EnableControls;
end;

end.
