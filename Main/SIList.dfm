inherited fmSIList: TfmSIList
  Left = 187
  Top = 122
  ActiveControl = gridDocList
  Caption = #1055#1086#1089#1090#1091#1087#1083#1077#1085#1080#1077' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
  ClientHeight = 606
  ClientWidth = 1215
  ExplicitWidth = 1223
  ExplicitHeight = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 587
    Width = 1215
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
    ExplicitTop = 587
    ExplicitWidth = 1215
  end
  inherited tb2: TSpeedBar
    Width = 1215
    ExplicitWidth = 1215
    object lbComp: TLabel [2]
      Left = 624
      Top = 6
      Width = 58
      Height = 13
      Caption = #1055#1086#1089#1090#1072#1074#1097#1080#1082
      Transparent = True
    end
    object Label1: TLabel [3]
      Left = 824
      Top = 6
      Width = 69
      Height = 13
      Caption = #1042#1080#1076' '#1076#1086#1075#1086#1074#1086#1088#1072
      Transparent = True
    end
    object cbComp: TDBComboBoxEh [4]
      Left = 688
      Top = 4
      Width = 121
      Height = 19
      DropDownBox.Rows = 15
      DropDownBox.Sizable = True
      DropDownBox.Width = -1
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 0
      Text = 'cbComp'
      Visible = True
      OnCloseUp = cbCompCloseUp
    end
    object cbContract: TDBLookupComboboxEh [5]
      Left = 904
      Top = 4
      Width = 193
      Height = 21
      EditButtons = <>
      KeyField = 'CONTRACTID'
      ListField = 'CONTRACTNAME'
      ListSource = dmMain.DataSourceFilterContract
      ShowHint = True
      TabOrder = 1
      Visible = True
      OnCloseUp = cbContractCloseUp
    end
    inherited spitPeriod: TSpeedItem [7]
    end
    inherited spitDep: TSpeedItem [8]
    end
  end
  inherited tb1: TSpeedBar
    Width = 1215
    Options = [sbFlatBtns, sbGrayedBtns, sbTransparentBtns]
    ExplicitWidth = 1215
    inherited spitAdd: TSpeedItem [1]
      Hint = #1044#1086#1073#1072#1074#1080#1090#1100'|'
    end
    inherited spitDel: TSpeedItem [2]
    end
    inherited spitExit: TSpeedItem [3]
      Left = 819
    end
    inherited spitPrint: TSpeedItem [4]
      DropDownMenu = ppPrint
      OnClick = acPrintDocExecute
    end
    inherited spitEdit: TSpeedItem [5]
      OnClick = acEditDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    Width = 1215
    Height = 516
    DataSource = dmMain.dsrSIList
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    SumList.Active = True
    UseMultiTitle = True
    Columns = <
      item
        Color = 15792119
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SSF'
        Footers = <>
        Title.Caption = #1057#1095#1077#1090'-'#1092#1072#1082#1090#1091#1088#1072'|'#1053#1086#1084#1077#1088
        Title.EndEllipsis = True
        Width = 55
      end
      item
        Color = 15792119
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SSFDT'
        Footers = <>
        Title.Caption = #1057#1095#1077#1090'-'#1092#1072#1082#1090#1091#1088#1072'|'#1044#1072#1090#1072
        Title.EndEllipsis = True
        Width = 55
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'SUPNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 100
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1085#1086#1084#1077#1088
        Title.EndEllipsis = True
        Width = 42
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1044#1072#1090#1072
        Title.EndEllipsis = True
        Width = 55
      end
      item
        EditButtons = <>
        FieldName = 'CREATED'
        Footers = <>
        Title.Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103'|'#1044#1072#1090#1072' '#1089#1086#1089#1090#1086#1103#1085#1080#1103
        Width = 65
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'DEPNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 104
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'CONTRACTNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 98
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'FIO'
        Footers = <>
        Title.EndEllipsis = True
        Width = 89
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'Q'
        Footer.FieldName = 'Q'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1050#1086#1083'-'#1074#1086
        Title.EndEllipsis = True
        Width = 44
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'W'
        Footer.FieldName = 'W'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1042#1077#1089
        Title.EndEllipsis = True
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'W3'
        Footer.FieldName = 'W3'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1063#1080#1089#1090#1099#1081' '#1074#1077#1089
      end
      item
        DropDownBox.ColumnDefValues.Title.TitleButton = True
        EditButtons = <>
        FieldName = 'COST'
        Footer.FieldName = 'COST'
        Footer.ValueType = fvtSum
        Footers = <>
        Title.Caption = #1048#1090#1086#1075#1086'|'#1057#1090#1086#1080#1084#1086#1089#1090#1100
        Title.EndEllipsis = True
        Width = 59
      end>
  end
  inherited ilButtons: TImageList
    Left = 174
    Top = 244
  end
  inherited fmstr: TFormStorage
    Left = 200
    Top = 236
  end
  inherited ActionList2: TActionList
    Left = 212
    Top = 184
  end
  inherited ppDep: TPopupMenu
    Left = 308
    Top = 212
  end
  inherited acAEvent: TActionList
    Left = 172
    Top = 196
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
    inherited acPrintDoc: TAction
      OnExecute = acPrintDocExecute
    end
    object acPrintStone: TAction
      Caption = #1055#1088#1080#1093#1086#1076#1085#1099#1081' '#1086#1088#1076#1077#1088
      OnExecute = acPrintStoneExecute
      OnUpdate = acPrintStoneUpdate
    end
    object acPrintMetal: TAction
      Caption = #1040#1082#1090' '#1087#1088#1080#1077#1084#1082#1080
      OnExecute = acPrintMetalExecute
      OnUpdate = acPrintMetalUpdate
    end
    object acCreateSOInv: TAction
      Caption = #1057#1086#1079#1076#1072#1090#1100' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1088#1072#1089#1093#1086#1076#1072' '#1084#1072#1090#1077#1088#1080#1072#1083#1086#1074
      OnExecute = acCreateSOInvExecute
      OnUpdate = acCreateSOInvUpdate
    end
    object acPrintActSemisGet: TAction
      Caption = #1040#1082#1090' '#1087#1088#1080#1077#1084#1082#1080'-'#1087#1077#1088#1072#1076#1072#1095#1080
      OnExecute = acPrintActSemisGetExecute
      OnUpdate = acPrintActSemisGetUpdate
    end
    object acImportDoc: TAction
      Caption = #1048#1084#1087#1086#1088#1090' '#1085#1072#1082#1083#1072#1076#1085#1086#1081
      OnExecute = acImportDocExecute
    end
    object acPrintRequirementInvoice: TAction
      Caption = #1058#1088#1077#1073#1086#1074#1072#1085#1080#1077'-'#1085#1072#1082#1083#1072#1076#1085#1072#1103
      OnExecute = acPrintRequirementInvoiceExecute
      OnUpdate = acPrintRequirementInvoiceUpdate
    end
    object acAddToExistingInvSOEL: TAction
      Caption = 'acAddToExistingInvSOEL'
    end
    object acUnionToOneSOElInv: TAction
      Caption = #1054#1073#1098#1077#1076#1080#1085#1080#1090#1100' '#1074' '#1086#1076#1085#1091' '#1085#1072#1082#1083#1072#1076#1085#1091#1102' '#1088#1072#1089#1093#1086#1076#1072
    end
    object acPrintActUnknownCrude: TAction
      Caption = #1040#1082#1090' '#1087#1088#1077#1076#1074#1072#1088#1080#1090#1077#1083#1100#1085#1086#1081' '#1087#1077#1088#1077#1076#1072#1095#1080
      OnExecute = acPrintActUnknownCrudeExecute
    end
    object acPrintActGoldForReadyProduct: TAction
      Caption = #1040#1082#1090' '#1087#1088#1080#1077#1084#1072' '#1079#1086#1083#1086#1090#1072' '#1076#1083#1103' '#1080#1079#1075#1086#1090'. '#1080#1079#1076'.'
      OnExecute = acPrintActGoldForReadyProductExecute
    end
    object acPrintActMutualClaims: TAction
      Caption = #1040#1082#1090' '#1079#1072#1095#1077#1090#1072' '#1074#1079#1072#1080#1084#1085#1099#1093' '#1090#1088#1077#1073#1086#1074#1072#1085#1080#1081
      OnExecute = acPrintActMutualClaimsExecute
    end
  end
  inherited ppDoc: TTBPopupMenu
    Left = 540
    Top = 180
    object TBItem10: TTBItem [4]
      Action = acAddToExistingInvSOEL
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100' '#1082' '#1088#1072#1085#1077#1077' '#1089#1086#1079#1076#1072#1085#1085#1086#1081
    end
    object TBItem7: TTBItem [5]
      Action = acCreateSOInv
    end
    object TBSeparatorItem2: TTBSeparatorItem [6]
    end
    object TBSeparatorItem3: TTBSeparatorItem
    end
    object TBItem12: TTBItem
      Caption = #1055#1077#1095#1072#1090#1100' '#1089#1087#1080#1089#1082#1072' '#1085#1072#1082#1083#1072#1076#1085#1099#1093
      ImageIndex = 4
      OnClick = TBItem11Click
    end
  end
  object ppPrint: TTBPopupMenu
    Left = 240
    Top = 216
    object TBItem6: TTBItem
      Action = acPrintStone
    end
    object TBItem9: TTBItem
      Action = acPrintRequirementInvoice
    end
    object TBItem17: TTBItem
      Action = acPrintActGoldForReadyProduct
      Caption = #1040#1082#1090' '#1087#1088#1080#1077#1084#1072' '#1076#1083#1103' '#1080#1079#1075#1086#1090'. '#1080#1079#1076'.'
    end
    object TBItem16: TTBItem
      Action = acPrintActUnknownCrude
      Caption = #1040#1082#1090' '#1087#1088#1077#1076#1074#1072#1088'. '#1087#1077#1088#1077#1076#1072#1095#1080
    end
    object TBItem15: TTBItem
      Action = acPrintActMutualClaims
      Caption = #1040#1082#1090' '#1074#1079#1072#1080#1084#1085#1099#1093' '#1090#1088#1077#1073#1086#1074#1072#1085#1080#1081
    end
    object TBItem5: TTBItem
      Action = acPrintMetal
    end
    object TBItem8: TTBItem
      Action = acPrintActSemisGet
    end
    object TBItem13: TTBItem
      Caption = #1053#1072#1082#1083#1072#1076#1085#1072#1103' '#1052'-15'
      OnClick = TBItem13Click
    end
    object TBSeparatorItem4: TTBSeparatorItem
    end
    object TBItem11: TTBItem
      Caption = #1055#1077#1095#1072#1090#1100' '#1089#1087#1080#1089#1082#1072' '#1085#1072#1082#1083#1072#1076#1085#1099#1093
      OnClick = TBItem11Click
    end
    object TBSeparatorItem5: TTBSeparatorItem
    end
    object TBItem14: TTBItem
      Caption = #1058#1086#1074#1072#1088
      ImageIndex = 4
    end
  end
  object PrintSIListGrid: TPrintDBGridEh
    DBGridEh = gridDocList
    Options = [pghFitGridToPageWidth]
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'Tahoma'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'Tahoma'
    PageHeader.Font.Style = []
    Units = MM
    Left = 416
    Top = 264
  end
end
