{********************
  �����: ��������� �������
  ������: 04.12.2004
*********************}
unit ScaleData;

interface

uses
  SysUtils, Windows, Classes, ExtCtrls, CPort, ComDrv32;

type
  TdmScale = class(TDataModule)
    Com1: TComPort;
    Com0: TComPort;
    CommPortDriver1: TCommPortDriver;
    procedure DataModuleCreate(Sender: TObject);
    procedure Com1RxChar(Sender: TObject; Count: Integer);
    procedure DataModuleDestroy(Sender: TObject);
    procedure Com0RxChar(Sender: TObject; Count: Integer);
  private
    Com: TComPort;
    ComBuffer: string;
    DataBuffer: TStringList;
    ComClass: Integer;
  public
    function GetWeigth: double;
  end;

var
  dmScale: TdmScale;

implementation

{$R *.dfm}

uses Forms, Dialogs, Main, DictData, ProductionConsts, fmUtils, MsgDialog, UtilLib;

{ TdmScale }

procedure TdmScale.DataModuleCreate(Sender: TObject);
begin
  DataBuffer := TStringList.Create;

  ComClass := dm.LocalSetting.ScaleClass;

  if ComClass <> -1 then
  begin
   Com := TComPort(FindComponent('Com' + IntToStr(ComClass)));

   case dm.LocalSetting.Scale_Port of
      0 : com.Port := 'COM1';
      1 : com.Port := 'COM2';
      2 : com.Port := 'COM3';
      3 : com.Port := 'COM4';
    else
      com.Port := 'COM1';
    end;
  end;
end;

procedure TdmScale.DataModuleDestroy(Sender: TObject);
begin
  DataBuffer.Free;
end;


function TdmScale.GetWeigth: Double;
var
  i: integer;
  Answer: string;
  Weight: string;
  AWeight: string;
  Command: string;
  ADecimalSeparator: Char;


function ReadData: string;
var
  TimeOut: Boolean;
  TickBegin: Cardinal;
  TickEnd: Cardinal;
begin
  if DataBuffer.Count <> 0 then
  begin
    Result := DataBuffer[0];
    DataBuffer.Delete(0);
  end
  else
  begin
    TimeOut := False;
    TickBegin := GetTickCount;
    while not TimeOut and (DataBuffer.Count = 0) do
    begin
      Application.ProcessMessages;
      if DataBuffer.Count = 0 then
      begin
        TickEnd := GetTickCount;
        TimeOut := ((TickEnd - TickBegin) div 1000) > 5;
      end;
    end;
    if not TimeOut then
    begin
      Result := DataBuffer[0];
      DataBuffer.Delete(0);
    end
    else
    begin
      Result := '';
      MessageDialog(Format(rsScale_TimeOut, [5000]), mtWarning, [mbOk], 0);
    end;
  end;
end;

begin
  sleep(300);

  Result := 0;

  if Com = nil then
  begin
    MessageDialog('�� ��������� ��� �����.', mtError, [mbOk], 0);
    Exit;
  end;

  ComBuffer := '';
  DataBuffer.Clear;
  Com.Connected := True;

  if ComClass = 0 then Command := 'p'  else
  if ComClass = 1 then Command := chr($4F)+ chr($38) + #13#10;

  Com.WriteStr(Command);

  Answer := ReadData;

  if Answer = 'A00' then
  begin
    AWeight := ReadData;
    AWeight := Trim(AWeight);
    if Length(AWeight) > 8 then
    SetLength(AWeight, 8);
    Weight := '';
    i := 2;
    while (i <= Length(AWeight)) and (AWeight[i] in ['0'..'9', '.']) do
    begin
      Weight := Weight + AWeight[i];
      Inc(i);
    end;
    Weight := Trim(Weight);
    ADecimalSeparator := DecimalSeparator;
    DecimalSeparator := '.';
    if not TryStrToFloat(Weight, Result) then
    Result := 0;
    DecimalSeparator := ADecimalSeparator;
  end
  else Result := 0;

  Com.Connected := False;
end;

procedure TdmScale.Com1RxChar(Sender: TObject; Count: Integer);
var
  Data: string;
  ComData: string;
  AComBuffer: string;
  Position: Integer;
begin
  if Count <> 0 then
  begin
    Com.ReadStr(ComData, Count);
    ComBuffer := ComBuffer + ComData;
    Position := Pos(#10 , ComBuffer);
    while Position <> 0 do
    begin
      Data := Copy(ComBuffer, 1, Position);
      SetLength(Data, Length(Data) - 2);
      DataBuffer.Add(Data);
      AComBuffer := Copy(ComBuffer, Position + 1, Length(ComBuffer));
      ComBuffer := AComBuffer;
      Position := Pos(#10 , ComBuffer);
    end;
  end;
end;


procedure TdmScale.Com0RxChar(Sender: TObject; Count: Integer);
var
  Data: string;
  ComData: string;
  AComBuffer: string;
  Position: Integer;
begin
  if Count <> 0 then
  begin
    Com.ReadStr(ComData, Count);
    ComBuffer := ComBuffer + ComData;
    Position := Pos(#10 , ComBuffer);
    while Position <> 0 do
    begin
      DataBuffer.Add('A00');
      Data := Copy(ComBuffer, 1, Position);
      SetLength(Data, Length(Data) - 2);
      DataBuffer.Add(Data);
      AComBuffer := Copy(ComBuffer, Position + 1, Length(ComBuffer));
      ComBuffer := AComBuffer;
      Position := Pos(#13 , ComBuffer);
    end;
  end;
end;

end.
