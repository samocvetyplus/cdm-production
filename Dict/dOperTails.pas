unit dOperTails;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  DictAncestor, ActnList,  ImgList,  Buttons,
  Grids, DBGrids, RXDBCtrl, ExtCtrls, ComCtrls,
  DBGridEh, DBGridEhGrouping, rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmOperTails = class(TfmDictAncestor)
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

implementation

uses DictData, dbUtil;

{$R *.dfm}

procedure TfmOperTails.FormCreate(Sender: TObject);
begin
  OpenDataSet(dm.taSemis);
  inherited;
end;

procedure TfmOperTails.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  CloseDataSet(dm.taSemis);
  inherited;
end;

end.
