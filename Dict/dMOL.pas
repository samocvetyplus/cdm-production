unit dMOL;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DictAncestor,  ImgList, Grids, DBGrids, RXDBCtrl,
  ComCtrls,  ExtCtrls, db, StdCtrls, CheckLst,
  Buttons, ActnList, pFIBDataSet, DBGridEh, TB2Item, Menus, FIBDataSet,
  DBGridEhGrouping, rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmMOL = class(TfmDictAncestor)
    spitAccess: TSpeedItem;
    taPSforMol: TpFIBDataSet;
    dsrPsForMol: TDataSource;
    taPSforMolDEPID: TIntegerField;
    taPSforMolNAME: TFIBStringField;
    acAccess: TAction;
    acPswd: TAction;
    spitPswd: TSpeedItem;
    ppMol: TTBPopupMenu;
    TBItem1: TTBItem;
    TBItem2: TTBItem;
    procedure taPSforMolBeforeOpen(DataSet: TDataSet);
    procedure gridDictColumns6EditButtonClick(Sender: TObject; var Handled: Boolean);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acAccessExecute(Sender: TObject);
    procedure acPswdExecute(Sender: TObject);
    procedure acAccessUpdate(Sender: TObject);
    procedure acPswdUpdate(Sender: TObject);
    procedure gridDictGetCellParams(Sender: TObject; Column: TColumnEh;
      AFont: TFont; var Background: TColor; State: TGridDrawState);
  public
  end;

implementation

uses DictData, dbUtil, DbEditor, ePswd, Access, fmUtils, dTransformPs,
  InvData;

{$R *.DFM}

procedure TfmMOL.taPSforMolBeforeOpen(DataSet: TDataSet);
begin
  with TpFIbDataSet(DataSet).Params do
    ByName['MOLID'].AsInteger := dm.taMOLMOLID.AsInteger;
end;

procedure TfmMOL.gridDictColumns6EditButtonClick(Sender: TObject; var Handled: Boolean);
const
  Rec : TRecName = (sId: 'DEPID'; sName: 'NAME'; sTable: 'D_COLOR'; sWhere: '');
var
  s : string;
begin
  PostDataSet(DataSet);
  s := '�� ������������ ��� '+DataSet.FieldByName('FNAME').AsString;
  if ShowDictForm(TfmTransformPs, dsrPsForMol, Rec, s, dmSelRecordReadOnly)<>mrOk then eXit;
  DataSet.Edit;
  dm.taMOLTRANSFORM_MOLID.AsInteger := dm.taMOLMOLID.AsInteger;
  dm.taMOLTRANSFORM_DEPID.AsInteger := vResult;
  DataSet.Post;
end;

procedure TfmMOL.FormResize(Sender: TObject);
begin
  spitExit.Left:=tb1.Width-tb1.BtnWidth-10;
end;



procedure TfmMOL.FormCreate(Sender: TObject);
begin
  inherited;
  //ppMol.Skin := dm.ppSkin;
  if(dm.User.Profile = -1)or(dm.User.Profile = 3)then
  begin
    gridDict.FieldColumns['PROFILE'].Visible := True;
    gridDict.FieldColumns['DOCPREVIEW'].Visible := True;
    acAccess.Visible := True;
    acPswd.Visible := True;
  end
  else begin
    gridDict.FieldColumns['PROFILE'].Visible := False;
    gridDict.FieldColumns['DOCPREVIEW'].Visible := False;
    acAccess.Visible := False;
    acPswd.Visible := False;
  end;
end;

procedure TfmMOL.acAccessExecute(Sender: TObject);
begin
  PostDataSet(DataSet);
  Access.FIO := dm.taMOLFNAME.AsString+' '+dm.taMOLLNAME.AsString+' '+dm.taMOLMNAME.AsString;
  ShowAndFreeForm(TfmAccess, TForm(fmAccess));
  Access.FIO := '';
end;

procedure TfmMOL.acPswdExecute(Sender: TObject);
begin
  ShowDbEditor(TfmPswd, dm.dsrMOL, emEdit, True);
end;

procedure TfmMOL.acAccessUpdate(Sender: TObject);
begin
  acAccess.Enabled := dm.IsAdm;
end;

procedure TfmMOL.acPswdUpdate(Sender: TObject);
begin
  acPswd.Enabled := dm.IsAdm;
end;

procedure TfmMOL.gridDictGetCellParams(Sender: TObject; Column: TColumnEh;
  AFont: TFont; var Background: TColor; State: TGridDrawState);
begin
  (*
   -1 �������������
   0 ������������
   1 ��������� ������������
   2 ��������� ������ ������� ���������
   3 ��������
   4 ���������
   5 �������� ������ ��������
  *)
  case DataSet.FieldByName('PROFILE').AsInteger of
   -1 : Background := clBtnFace2;
    0 : Background := clWindow;
    1,2,5 : Background := clMoneyGreen;
    3 : Background := clCream;
    4 : Background := clMoneyGreen;
  end;
  inherited;
end;

end.
