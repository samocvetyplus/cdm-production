inherited fmA2Ins: TfmA2Ins
  Left = 91
  Top = 188
  Width = 651
  Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1082#1080' '#1072#1088#1090'2'
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Width = 643
  end
  inherited plDict: TPanel
    Width = 643
    inherited plSelect: TPanel
      Width = 639
      inherited btnOk: TSpeedButton
        Left = 461
      end
      inherited btnCancel: TSpeedButton
        Left = 545
      end
    end
    inherited gridDict: TDBGridEh
      Width = 639
      DataSource = dm.dsrA2Ins
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'RecNo'
          Footers = <>
          Title.EndEllipsis = True
          Width = 31
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'INSNAME'
          Footers = <>
          Title.EndEllipsis = True
          Width = 129
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'QUANTITY'
          Footers = <>
          Title.EndEllipsis = True
          Width = 47
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'WEIGHT'
          Footers = <>
          Title.EndEllipsis = True
          Width = 36
        end
        item
          Checkboxes = True
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'MAIN'
          Footers = <>
          KeyList.Strings = (
            '1'
            '0')
          Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1082#1072'|'#1054#1089#1085#1086#1074#1085#1072#1103' '#1074#1089#1090#1072#1074#1082#1072
          Title.EndEllipsis = True
          Width = 57
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'EDGTNAME'
          Footers = <>
          Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1082#1072'|'#1054#1075#1088#1072#1085#1082#1072
          Title.EndEllipsis = True
          Width = 41
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'EDGSHAPENAME'
          Footers = <>
          Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1082#1072'|'#1060#1086#1088#1084#1072
          Title.EndEllipsis = True
          Width = 40
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'COLOR'
          Footers = <>
          Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1082#1072'|'#1062#1074#1077#1090
          Title.EndEllipsis = True
          Width = 33
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'CHROMATICITY'
          Footers = <>
          Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1082#1072'|'#1062#1074#1077#1090#1085#1086#1089#1090#1100
          Title.EndEllipsis = True
          Width = 57
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'IGR'
          Footers = <>
          Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1082#1072'|'#1043#1088#1091#1087#1087#1072
          Title.EndEllipsis = True
          Width = 44
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'CLEANNES'
          Footers = <>
          Title.Caption = #1061#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1082#1072'|'#1063#1080#1089#1090#1086#1090#1072
          Title.EndEllipsis = True
          Width = 47
        end>
    end
  end
  inherited tb1: TSpeedBar
    Width = 643
  end
  inherited ilButtons: TImageList
    Left = 90
    Top = 140
  end
  inherited fmstr: TFormStorage
    Left = 36
    Top = 192
  end
end
