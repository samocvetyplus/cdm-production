inherited fmeCheckOpt: TfmeCheckOpt
  Left = 174
  Top = 185
  ActiveControl = cmbxComp
  BorderStyle = bsToolWindow
  Caption = #1063#1077#1082
  ClientHeight = 129
  ClientWidth = 214
  OldCreateOrder = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  inherited plMain: TPanel
    Width = 214
    Height = 129
    object Label1: TLabel [0]
      Left = 8
      Top = 30
      Width = 40
      Height = 13
      Caption = #1057#1059#1052#1052#1040
    end
    object Label2: TLabel [1]
      Left = 8
      Top = 73
      Width = 62
      Height = 13
      Caption = #1059#1055#1051#1040#1063#1045#1053#1054
    end
    object lbComp: TLabel [2]
      Left = 8
      Top = 8
      Width = 67
      Height = 13
      Caption = #1054#1088#1075#1072#1085#1080#1079#1072#1094#1080#1103
    end
    inherited btnOk: TBitBtn
      Left = 32
      Top = 96
      TabOrder = 5
    end
    inherited btnCancel: TBitBtn
      Left = 124
      Top = 96
      TabOrder = 6
    end
    object edCost: TDBNumberEditEh
      Left = 104
      Top = 28
      Width = 105
      Height = 19
      EditButton.ShortCut = 16397
      EditButton.Style = ebsUpDownEh
      EditButton.Visible = True
      EditButtons = <>
      Flat = True
      TabOrder = 1
      Visible = True
      OnChange = edCostChange
      OnExit = edCostExit
    end
    object chbxNDS: TDBCheckBoxEh
      Left = 8
      Top = 48
      Width = 105
      Height = 17
      Caption = #1074' '#1090'.'#1095'. '#1053#1044#1057
      Flat = True
      TabOrder = 2
      ValueChecked = 'True'
      ValueUnchecked = 'False'
    end
    object edNDS: TDBNumberEditEh
      Left = 144
      Top = 48
      Width = 65
      Height = 19
      EditButton.ShortCut = 16397
      EditButton.Style = ebsUpDownEh
      EditButton.Visible = True
      EditButtons = <>
      Flat = True
      TabOrder = 3
      Visible = True
    end
    object edGive: TDBNumberEditEh
      Left = 104
      Top = 68
      Width = 105
      Height = 19
      EditButton.ShortCut = 16397
      EditButton.Style = ebsUpDownEh
      EditButton.Visible = True
      EditButtons = <>
      Flat = True
      TabOrder = 4
      Visible = True
    end
    object cmbxComp: TDBComboBoxEh
      Left = 80
      Top = 8
      Width = 129
      Height = 19
      DropDownBox.AutoDrop = True
      DropDownBox.Rows = 15
      DropDownBox.Width = -1
      EditButtons = <>
      Flat = True
      TabOrder = 0
      Visible = True
    end
  end
end
