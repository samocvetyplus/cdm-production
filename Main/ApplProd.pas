unit ApplProd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Ancestor, RxPlacemnt, ImgList, RxSpeedBar, ExtCtrls, ComCtrls,
  TB2Item, TB2Dock, TB2Toolbar, RxToolEdit, RXDBCtrl, StdCtrls, Mask,
  DBCtrls, Buttons, DBCtrlsEh, Grids, DBGridEh, ActnList, DBGridEhGrouping,
  GridsEh;

type
  TfmApplProd = class(TfmAncestor)
    plMain: TPanel;
    lbNoDoc: TLabel;
    lbDateDoc: TLabel;
    SpeedButton1: TSpeedButton;
    TBDock1: TTBDock;
    TBToolbar1: TTBToolbar;
    edDocNo: TDBEditEh;
    edDocDate: TDBDateTimeEditEh;
    TBControlItem1: TTBControlItem;
    Label2: TLabel;
    edFilterArt: TDBEditEh;
    TBControlItem2: TTBControlItem;
    gridAppl: TDBGridEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmApplProd: TfmApplProd;

implementation

uses ApplData;

{$R *.dfm}

end.
