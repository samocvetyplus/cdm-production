inherited fmInitStone: TfmInitStone
  Left = 141
  Top = 181
  Caption = #1053#1072#1095#1072#1083#1100#1085#1099#1077' '#1086#1089#1090#1072#1090#1082#1080' '#1087#1086' '#1082#1072#1084#1085#1103#1084
  PixelsPerInch = 96
  TextHeight = 13
  inherited plDict: TPanel
    inherited gridDict: TDBGridEh
      Top = 25
      Height = 156
      DataSource = dm.dsrInitStone
      FooterRowCount = 1
      SumList.Active = True
      Columns = <
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          DropDownRows = 15
          DropDownWidth = -1
          EditButtons = <>
          FieldName = 'SEMISNAME'
          Footers = <>
          Width = 201
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'UQ'
          Footers = <>
          KeyList.Strings = (
            '-1'
            '0'
            '1')
          PickList.Strings = (
            #1053#1077#1090
            #1064#1090
            #1055#1072#1088#1072)
          Title.Caption = #1045#1076'. '#1080#1079#1084'.|'#1082#1086#1083'-'#1074#1072
          Width = 40
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'UW'
          Footers = <>
          KeyList.Strings = (
            '-1'
            '3'
            '4')
          PickList.Strings = (
            #1053#1077#1090
            #1043#1088
            #1050#1072#1088#1072#1090)
          Title.Caption = #1045#1076'. '#1080#1079#1084'.|'#1074#1077#1089#1072
          Width = 39
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'Q'
          Footer.FieldName = 'Q'
          Footer.ValueType = fvtSum
          Footers = <>
        end
        item
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          FieldName = 'W'
          Footer.FieldName = 'W'
          Footer.ValueType = fvtSum
          Footers = <>
        end>
    end
    object TBDock1: TTBDock
      Left = 2
      Top = 2
      Width = 424
      Height = 23
      object TBToolbar1: TTBToolbar
        Left = 0
        Top = 0
        BorderStyle = bsNone
        DockMode = dmCannotFloat
        TabOrder = 0
        object TBControlItem3: TTBControlItem
          Control = Label2
        end
        object TBControlItem1: TTBControlItem
          Control = lcbxADate
        end
        object TBControlItem2: TTBControlItem
          Control = edDate
        end
        object Label2: TLabel
          Left = 0
          Top = 3
          Width = 101
          Height = 13
          Caption = #1044#1072#1090#1072' '#1072#1082#1090#1091#1072#1083#1100#1085#1086#1089#1090#1080' '
        end
        object lcbxADate: TDBLookupComboboxEh
          Left = 101
          Top = 0
          Width = 96
          Height = 19
          DropDownBox.ColumnDefValues.Title.TitleButton = True
          EditButtons = <>
          Flat = True
          KeyField = 'ADATE'
          ListField = 'ADATE'
          ListSource = dsrADate
          TabOrder = 0
          Visible = True
          OnChange = lcbxADateChange
        end
        object edDate: TDBDateTimeEditEh
          Left = 197
          Top = 0
          Width = 17
          Height = 19
          EditButton.Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000120B0000120B00000000000000000000FF00FFFF00FF
            262FA31D20B5FF00FF4B4B4B5B5B5B87817B87817BFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FF82776F5F61BC2126B2666059A3A3A4FAFAFAE3
            E0DCD1CEC99A979582807E7874715D5A58FF00FFFF00FFFF00FFFF00FFA89381
            7B75C42024B2BA9775A3A3A4FFFFFFFFFFFFFFFFFDD6D0CAC8C0B8F6EBDDEDE0
            D1A59B916B6864FF00FFFF00FFA893817873C32024B2B99B7EA3A3A4FEFFFFFF
            FFFF9F9FA07371713535377E7B76FFF5E7FFF2E1878685FF00FFFF00FFA89483
            7873C52024B3BAA18AA3A3A4FEFFFFFFFFFFCACACCF5F3F2FFFDF9353537F3EA
            E0FDF0E2878685FF00FFFF00FFA8978B7876CA2024B2BBA896A3A3A4FEFFFFFF
            FFFFB3B3B4828283A2A1A2353537F8F2EBFDF2E9878685FF00FFFF00FF918984
            7779CF2024B2BCADA0A3A3A4FEFFFFFFFFFF9A9A9B3535377C7C7ECCCCCBFFFE
            FBFCF6EF878685FF00FFFF00FF8E8888787BD42024B1BDB2A9A3A3A4FEFEFFFF
            FFFFBCBCBD353537353537353537FFFEFDFCF9F5878685FF00FFFF00FF8E8C8E
            787EDA2024B1B4B3B9A3A3A4FEFEFEE9E9E9E2E2E2BCBCBCA2A2A3DFDFE0FFFF
            FFFCFBFB878685FF00FFFF00FF8E8F947880DE2023B1B4B3B9A3A3A4FFFFFFA5
            A5A5BBBBBB9F9FA0C6C6C7C5C5C6E2E2E3FEFEFE878685FF00FFFF00FF8E8F95
            7880DF2023B1B4B3B9A3A3A4FFFFFFB9B9B9B3B3B4ACACADA8A8A9B9B9BAE0E0
            E1FFFFFF878685FF00FFFF00FF9091977B83E22023B0B6B5B6A3A3A4FFFFFF79
            7979E1E2E2FBFBFCEDEDEDB9B9B9DBDBDCFFFFFF878685FF00FFFF00FF83848A
            646AD21D23BA6F71919E9DAFBBB9BC7F7D738D8C88EDEAE0E7E7E36465629090
            90FFFFFF878685FF00FFFF00FFFF00FF4141632E317D3034833F43933A3D9440
            42964444605655996D6893736E885E5B61918D8A616161FF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FF4B4B4BFF00FF3B3D624B4B4B373765FF00FF3333833838
            65141477FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF4B4B4B4B
            4B4BFF00FFFF00FF4B4B4E4C4C4CFF00FFFF00FFFF00FFFF00FF}
          EditButton.Style = ebsGlyphEh
          EditButton.Width = 16
          EditButtons = <>
          Flat = True
          Kind = dtkDateEh
          TabOrder = 1
          Visible = True
          OnCloseUp = edDateCloseUp
        end
      end
    end
  end
  inherited ActionList2: TActionList
    Left = 92
  end
  object taADate: TpFIBDataSet
    InsertSQL.Strings = (
      'execute procedure F_INSERT(0)')
    SelectSQL.Strings = (
      'select distinct ADate'
      'from Init_Stone'
      'order by ADate'
      '')
    CacheModelOptions.BufferChunks = 1000
    Transaction = dm.tr
    Database = dm.db
    Left = 152
    Top = 96
    object taADateADATE: TDateTimeField
      FieldName = 'ADATE'
      Origin = 'INIT_STONE.ADATE'
      Required = True
    end
  end
  object dsrADate: TDataSource
    DataSet = taADate
    Left = 152
    Top = 144
  end
end
