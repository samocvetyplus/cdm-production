object fmExport1C_Old: TfmExport1C_Old
  Left = 292
  Top = 150
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = #1069#1082#1089#1087#1086#1088#1090' '#1074' 1'#1057
  ClientHeight = 130
  ClientWidth = 277
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object Bevel1: TBevel
    Left = 0
    Top = 0
    Width = 276
    Height = 97
    Shape = bsFrame
  end
  object Label1: TLabel
    Left = 8
    Top = 2
    Width = 79
    Height = 13
    Caption = #1060#1072#1081#1083' '#1101#1082#1089#1087#1086#1088#1090#1072
  end
  object Label2: TLabel
    Left = 8
    Top = 44
    Width = 98
    Height = 13
    Caption = #1044#1072#1090#1072' '#1072#1082#1090#1091#1072#1083#1100#1085#1086#1089#1090#1080
  end
  object BitBtn1: TBitBtn
    Left = 104
    Top = 104
    Width = 75
    Height = 25
    TabOrder = 0
    OnClick = BitBtn1Click
    Kind = bkOK
  end
  object edFile: TFilenameEdit
    Left = 8
    Top = 18
    Width = 265
    Height = 21
    NumGlyphs = 1
    TabOrder = 1
    Text = 'c:\export.txt'
  end
  object edDate: TDateEdit
    Left = 152
    Top = 40
    Width = 121
    Height = 21
    NumGlyphs = 2
    TabOrder = 2
  end
  object CheckBox2: TCheckBox
    Left = 8
    Top = 72
    Width = 257
    Height = 17
    Caption = #1042#1099#1075#1088#1091#1078#1072#1090#1100' '#1091#1076#1072#1083#1077#1085#1085#1099#1077' '#1085#1072#1082#1083#1072#1076#1085#1099#1077
    Checked = True
    State = cbChecked
    TabOrder = 3
  end
  object XMLDoc: TXMLDocument
    ParseOptions = [poValidateOnParse]
    XML.Strings = (
      '<?xml version="1.0" encoding="ISO-8859-5"?>'
      '<EXPORTDATA>'
      '</EXPORTDATA>')
    Left = 8
    Top = 16
    DOMVendorDesc = 'MSXML'
  end
  object taComp: TIBDataSet
    Database = dm.db
    Transaction = dm.tr
    BufferChunks = 1000
    CachedUpdates = False
    SelectSQL.Strings = (
      'select * '
      'from D_Comp'
      'where Upd=1')
    Left = 52
    Top = 64
  end
  object taMat: TIBDataSet
    Database = dm.db
    Transaction = dm.tr
    BufferChunks = 1000
    CachedUpdates = False
    SelectSQL.Strings = (
      'select * '
      'from D_Mat'
      'where Upd=1')
    Left = 8
    Top = 64
  end
end
