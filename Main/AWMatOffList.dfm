inherited fmAWMatOffList: TfmAWMatOffList
  Left = 154
  Top = 353
  Caption = #1057#1087#1080#1089#1086#1082' '#1072#1082#1090#1086#1074' '#1089#1087#1080#1089#1072#1085#1080#1103' '#1084#1072#1090#1077#1088#1080#1083#1086#1074' '#1085#1072' '#1086#1089#1085#1086#1074#1072#1085#1080#1080' '#1086#1087#1083#1072#1090' '#1087#1086#1082#1091#1087#1072#1090#1077#1083#1077#1081
  ClientHeight = 424
  ClientWidth = 990
  ExplicitWidth = 998
  ExplicitHeight = 458
  PixelsPerInch = 96
  TextHeight = 13
  inherited stbrStatus: TStatusBar
    Top = 405
    Width = 990
    ExplicitTop = 405
    ExplicitWidth = 990
  end
  inherited tb2: TSpeedBar
    Width = 990
    ExplicitWidth = 990
    inherited laDep: TLabel
      Left = 652
      Visible = False
      ExplicitLeft = 652
    end
    inherited laPeriod: TLabel
      Left = 500
      Visible = False
      ExplicitLeft = 500
    end
    object Label1: TLabel [2]
      Left = 8
      Top = 8
      Width = 95
      Height = 13
      Caption = #1042#1099#1073#1080#1088#1080#1090#1077' '#1086#1090#1095#1077#1090' '#1079#1072
      Transparent = True
    end
    object cmbxDoc: TDBComboBoxEh [3]
      Left = 112
      Top = 4
      Width = 233
      Height = 19
      DropDownBox.Sizable = True
      DropDownBox.Width = -1
      EditButtons = <>
      Flat = True
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnChange = cmbxDocChange
    end
    inherited spitPeriod: TSpeedItem [5]
      Left = 411
    end
    inherited spitDep: TSpeedItem [6]
      Visible = False
    end
  end
  inherited tb1: TSpeedBar
    Width = 990
    ExplicitWidth = 990
    inherited spitPrint: TSpeedItem [4]
      Action = nil
      DropDownMenu = ppPrint
    end
    inherited spitEdit: TSpeedItem [5]
      OnClick = acEditDocExecute
    end
  end
  inherited gridDocList: TDBGridEh
    Width = 990
    Height = 334
    DataSource = dmData.dsrAWMOList
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
    UseMultiTitle = True
    Visible = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'DOCNO'
        Footers = <>
        Title.Caption = #1040#1082#1090'|'#1053#1086#1084#1077#1088
        Title.EndEllipsis = True
        Width = 61
      end
      item
        EditButtons = <>
        FieldName = 'DOCDATE'
        Footers = <>
        Title.Caption = #1040#1082#1090'|'#1044#1072#1090#1072
        Title.EndEllipsis = True
        Width = 72
      end
      item
        EditButtons = <>
        FieldName = 'ABD'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076'|'#1053#1072#1095#1072#1083#1086
        Title.EndEllipsis = True
        Width = 78
      end
      item
        DisplayFormat = 'dd.mm.yyyy hh:mm'
        EditButtons = <>
        FieldName = 'AED'
        Footers = <>
        Title.Caption = #1055#1077#1088#1080#1086#1076'|'#1054#1082#1086#1085#1095#1072#1085#1080#1077' '
        Title.EndEllipsis = True
        Width = 86
      end
      item
        EditButtons = <>
        FieldName = 'USERNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 117
      end
      item
        EditButtons = <>
        FieldName = 'SUPNAME'
        Footers = <>
        Title.EndEllipsis = True
        Width = 114
      end
      item
        EditButtons = <>
        FieldName = 'UE'
        Footers = <>
        Title.Caption = #1058#1086#1074#1072#1088'|'#1042#1093#1086#1076'.'
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'Q'
        Footers = <>
        Title.Caption = #1058#1086#1074#1072#1088'|'#1055#1088#1086#1076#1072#1085#1086
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'W'
        Footers = <>
        Title.Caption = #1058#1086#1074#1072#1088'|'#1042#1086#1079#1074#1088#1072#1090
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'TRANS_PERCENT'
        Footers = <>
        Title.Caption = #1058#1086#1074#1072#1088'|'#1048#1089#1093'.'
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'W2'
        Footers = <>
        Title.EndEllipsis = True
      end
      item
        EditButtons = <>
        FieldName = 'AKCIZ'
        Footers = <>
        Title.EndEllipsis = True
        Width = 82
      end>
  end
  inherited ilButtons: TImageList
    Left = 26
    Top = 200
  end
  inherited fmstr: TFormStorage
    Left = 136
    Top = 148
  end
  inherited ActionList2: TActionList
    Left = 80
    Top = 200
  end
  inherited ppDep: TPopupMenu
    Left = 24
    Top = 148
  end
  inherited acAEvent: TActionList
    Left = 80
    Top = 148
    inherited acPeriod: TAction
      OnUpdate = acPeriodUpdate
    end
    inherited acEditDoc: TAction
      OnExecute = acEditDocExecute
    end
    object acOpenAll: TAction
      Tag = 1
      Caption = #1054#1090#1082#1088#1099#1090#1100' '#1074#1089#1105
      OnExecute = acAllExecute
    end
    object acCloseAll: TAction
      Tag = 2
      Caption = #1047#1072#1082#1088#1099#1090#1100' '#1074#1089#1105
      OnExecute = acAllExecute
    end
    object acRefreshAll: TAction
      Tag = 3
      Caption = #1054#1073#1085#1086#1074#1080#1090#1100' '#1074#1089#1105
      OnExecute = acAllExecute
    end
    object acPrintSvod: TAction
      Caption = #1057#1074#1086#1076#1085#1099#1081' '#1086#1090#1095#1077#1090
      OnExecute = acPrintSvodExecute
    end
  end
  inherited ppDoc: TTBPopupMenu
    Left = 240
    Top = 148
    object TBSeparatorItem2: TTBSeparatorItem
    end
    object TBItem10: TTBItem
      Action = acOpenAll
    end
    object TBItem9: TTBItem
      Action = acCloseAll
    end
    object TBItem8: TTBItem
      Action = acRefreshAll
    end
  end
  object taAddComp: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct c.COMPID'
      'from CashBank c'
      'where c.PAYDATE <= :ED and'
      '      c.SELFCOMPID=:SELFCOMPID')
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 24
    Top = 248
    poSQLINT64ToBCD = True
  end
  object ppPrint: TTBPopupMenu
    Left = 292
    Top = 148
    object TBItem5: TTBItem
      Action = acPrintSvod
    end
  end
  object taDocList: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct i.DOCNO'
      'from Inv i'
      'where i.ITYPE=26 and'
      '      i.SELFCOMPID=:SELFCOMPID  ')
    BeforeOpen = taDocListBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 96
    Top = 248
    poSQLINT64ToBCD = True
    object taDocListDOCNO: TFIBIntegerField
      FieldName = 'DOCNO'
    end
  end
end
