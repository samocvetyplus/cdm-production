object ReportInventory: TReportInventory
  OldCreateOrder = False
  Height = 357
  Width = 369
  object taMat: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct'
      '  ait.matid,'
      '  m.name '
      'from '
      '  AIt_InSemis ait'
      '  left outer join AItem a on (ait.protocolid = a.id) '
      '  left outer join D_Mat m on (ait.matid = m.id) '
      '  left outer join Act ac on (a.actid = ac.id)'
      'where'
      
        '  onlydate(ac.DocDate) between OnlyDate(:BD) and OnlyDate(:ED)  ' +
        'and'
      '  a.psid = :PlaceOfStorageID and '
      '  ait.matid <> 32')
    BeforeOpen = taMatBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 24
    Top = 56
    object taMatMATID: TFIBStringField
      FieldName = 'MATID'
      Size = 10
      EmptyStrToNull = True
    end
    object taMatNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
  end
  object taOper: TpFIBDataSet
    SelectSQL.Strings = (
      'select distinct'
      '  a.operid,'
      '  o.operation,'
      '  a.s,'
      '  a.e,'
      '  a.n,'
      '  o.a'
      'from'
      '  AItem a'
      '  left outer join AIt_InSemis ait on (a.id = ait.protocolid)'
      '  left outer join Act ac on (a.actid = ac.id)'
      '  left outer join D_Oper o on (a.operid = o.operid)'
      'where '
      '  a.psid = :PlaceOfStorageID and'
      '  ait.matid = ?MatId and'
      '  OnlyDate(ac.DocDate) between (:BD) and (:ED)')
    BeforeOpen = taOperBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DataSource = dsrMat
    Left = 24
    Top = 112
    dcForceOpen = True
    object taOperOPERID: TFIBStringField
      FieldName = 'OPERID'
      Size = 10
      EmptyStrToNull = True
    end
    object taOperOPERATION: TFIBStringField
      FieldName = 'OPERATION'
      Size = 60
      EmptyStrToNull = True
    end
    object taOperS: TFIBFloatField
      FieldName = 'S'
    end
    object taOperE: TFIBFloatField
      FieldName = 'E'
    end
    object taOperN: TFIBFloatField
      FieldName = 'N'
    end
    object taOperA: TFIBSmallIntField
      FieldName = 'A'
    end
  end
  object taSubOper: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  ID Name0,'
      '  Name,'
      '  Weight,'
      '  Quantity'
      'from'
      
        '  Report$Inventory(:BD, :ED, :PlaceOfStorageID, ?OperID, :Materi' +
        'alID)'
      'order by 1')
    BeforeOpen = taSubOperBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DataSource = dsrOper
    Left = 24
    Top = 168
    object taSubOperNAME0: TFIBStringField
      FieldName = 'NAME0'
      Size = 10
      EmptyStrToNull = True
    end
    object taSubOperNAME: TFIBStringField
      FieldName = 'NAME'
      Size = 40
      EmptyStrToNull = True
    end
    object taSubOperWEIGHT: TFIBBCDField
      FieldName = 'WEIGHT'
      Size = 3
      RoundByScale = True
    end
    object taSubOperQUANTITY: TFIBIntegerField
      FieldName = 'QUANTITY'
    end
  end
  object dsrMat: TDataSource
    DataSet = taMat
    Left = 104
    Top = 56
  end
  object dsrOper: TDataSource
    DataSet = taOper
    Left = 104
    Top = 112
  end
  object dsrSubOper: TDataSource
    DataSet = taSubOper
    Left = 104
    Top = 168
  end
  object frMat: TfrDBDataSet
    DataSource = dsrMat
    Left = 184
    Top = 56
  end
  object frOper: TfrDBDataSet
    DataSource = dsrOper
    Left = 184
    Top = 112
  end
  object frSubOper: TfrDBDataSet
    DataSource = dsrSubOper
    Left = 184
    Top = 168
  end
  object Report: TfrReport
    InitialZoom = pzDefault
    PreviewButtons = [pbZoom, pbLoad, pbSave, pbPrint, pbFind, pbHelp, pbExit]
    ReportType = rtMultiple
    RebuildPrinter = False
    OnGetValue = ReportGetValue
    Left = 280
    Top = 8
    ReportForm = {19000000}
  end
  object taHeader: TpFIBDataSet
    SelectSQL.Strings = (
      'select '
      '  sname'
      'from '
      '  d_dep'
      'where '
      '  depid = :PlaceOfStorageID ')
    BeforeOpen = taHeaderBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 24
  end
  object frHeader: TfrDBDataSet
    DataSource = dsrHeader
    Left = 184
  end
  object dsrHeader: TDataSource
    DataSet = taHeader
    Left = 104
  end
  object frSubreport: TfrDBDataSet
    DataSource = dsrSubreport
    Left = 184
    Top = 280
  end
  object taSubReport: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '   p.U Unit,'
      '   p.FQ Quantity,'
      '   a2.ART Model,'
      '   a2.ART2 SubModel,'
      '   o.OPERATION OperationName,'
      '   sz.NAME SizeName'
      'from PArt p, Art2 a2, D_Sz sz, D_Oper o, act ac'
      
        'where onlydate(ac.DocDate) between OnlyDate(:BD) and OnlyDate(:E' +
        'D) and'
      '           p.ActId = ac.Id and'
      '           p.PSID = :PlaceOfStorageID and'
      '           p.OPERID =o.OPERID and     '
      '           p.ART2ID=a2.ART2ID and'
      '           p.SZID=sz.ID and'
      '           p.FQ <> 0'
      'order by a2.ART, a2.ART2, sz.NAME, o.SORTIND')
    BeforeOpen = taSubReportBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    Left = 24
    Top = 280
  end
  object dsrSubreport: TDataSource
    DataSet = taSubReport
    Left = 104
    Top = 280
  end
  object taTotalLosses: TpFIBDataSet
    SelectSQL.Strings = (
      'select'
      '  TotalSjem,'
      '  TotalLossesNorm,'
      '  Sjem,'
      '  LossesNorm,'
      '  ResultLosses,'
      '  SjemNorm,'
      '  PolishSjemNorm,'
      '  PolishSjemNormMonth'
      'from'
      '  TotalLosses2(:PlaceOfStorageID, :ED, ?MatID)')
    BeforeOpen = taTotalLossesBeforeOpen
    Transaction = dm.tr
    Database = dm.db
    DataSource = dsrMat
    Left = 24
    Top = 224
    dcForceOpen = True
    object taTotalLossesTOTALSJEM: TFIBFloatField
      FieldName = 'TOTALSJEM'
    end
    object taTotalLossesTOTALLOSSESNORM: TFIBFloatField
      FieldName = 'TOTALLOSSESNORM'
    end
    object taTotalLossesSJEM: TFIBBCDField
      FieldName = 'SJEM'
      Size = 3
      RoundByScale = True
    end
    object taTotalLossesLOSSESNORM: TFIBBCDField
      FieldName = 'LOSSESNORM'
      Size = 3
      RoundByScale = True
    end
    object taTotalLossesRESULTLOSSES: TFIBFloatField
      FieldName = 'RESULTLOSSES'
    end
    object taTotalLossesSJEMNORM: TFIBBCDField
      FieldName = 'SJEMNORM'
      Size = 3
      RoundByScale = True
    end
    object taTotalLossesPOLISHSJEMNORM: TFIBBCDField
      FieldName = 'POLISHSJEMNORM'
      Size = 3
      RoundByScale = True
    end
    object taTotalLossesPOLISHSJEMNORMMONTH: TFIBBCDField
      FieldName = 'POLISHSJEMNORMMONTH'
      Size = 3
      RoundByScale = True
    end
  end
  object dsrTotalLosses: TDataSource
    DataSet = taTotalLosses
    Left = 104
    Top = 224
  end
  object frTotalLosses: TfrDBDataSet
    DataSource = dsrTotalLosses
    Left = 184
    Top = 224
  end
end
