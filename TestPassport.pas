unit TestPassport;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxStyles, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxButtonEdit,
  cxDBExtLookupComboBox, cxCalendar, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGrid6Lnk, Provider,
  DBClient, FR_DSet, FR_DBSet, FIBDatabase, pFIBDatabase, FR_Class,
  ImgList, FIBQuery, pFIBQuery, pFIBStoredProc, FIBDataSet, cxHint,
  pFIBDataSet, ActnList, dxBar, dxBarExtItems, cxClasses, ExtCtrls,
  cxSplitter, cxVGrid, cxDBVGrid, cxInplaceContainer, cxGridDBTableView,
  cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridBandedTableView, cxGridDBBandedTableView, cxControls,
  cxGridCustomView, cxGrid, cxPC, dxStatusBar, cxCurrencyEdit,
  cxGridCardView, cxGridDBCardView, Grids, DBGrids, cxImageComboBox,
  cxDropDownEdit, cxMRUEdit, cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore,
  dxSkinsDefaultPainters, dxSkinsdxStatusBarPainter, dxSkinscxPCPainter,
  dxSkinsdxBarPainter, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxSkinBlack, dxSkinBlue, dxSkinCaramel,
  dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, DBGridEhGrouping, GridsEh, DBGridEh;

type
  TfrmTEstPassport = class(TForm)
    DBGridEh3: TDBGridEh;
    DBGridEh1: TDBGridEh;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTEstPassport: TfrmTEstPassport;
implementation


{$R *.dfm}

end.
