unit dOperIn;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList,  ImgList,  Buttons,
  Grids, DBGrids, RXDBCtrl, ExtCtrls, ComCtrls, DBGridEh, DBGridEhGrouping,
  rxPlacemnt, rxSpeedbar, GridsEh;

type
  TfmOperIn = class(TfmDictAncestor)
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;

var
  fmOperIn: TfmOperIn;

implementation

uses DictData, dbUtil;

{$R *.dfm}

procedure TfmOperIn.FormCreate(Sender: TObject);
begin
  OpenDataSets([dm.taSemis, dm.taMat]);
  inherited;
end;

procedure TfmOperIn.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  CloseDataSets([dm.taSemis, dm.taMat]);
  inherited;
end;

end.
