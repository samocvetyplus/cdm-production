unit ImportData;

interface

uses
  SysUtils, Classes, DB, DBClient, FIBQuery, pFIBQuery, pFIBStoredProc,
  FIBDataSet, pFIBDataSet, rxStrUtils, StrUtils, uDialogs, Variants;

const
  DefaultDelimiter : Char = ';';
  SQLUpdateOuterDocParams = 'update inv set SSF = :SSF, SSFDT = :SSFDT where invid = :InvoiceID';

type

  //������� ��� ��� ���� ���������� � ������ �������
  TInvoiceItemPositions = Record
    ArticleWord: Integer;
    SizeWord: Integer;
    UnitNameWord: Integer;
    QuantityWord: Integer;
    WeightWord: Integer;
    PriceWord: Integer;
    TaxWord: Integer;
  End;

  TdmImportData = class(TDataModule)
    Producers: TpFIBDataSet;
    ProducersCOMPANYID: TFIBIntegerField;
    ProducersKEYWORD: TFIBStringField;
    ProducersHEADERPARAMS: TFIBStringField;
    ProducersITEMPARAMS: TFIBStringField;
    ProducersHEADERDELIMITER: TFIBStringField;
    ProducersITEMDELIMITER: TFIBStringField;
    Invoice: TpFIBQuery;
    InvoiceItems: TpFIBQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
    procedure InvoiceBeforeExecute(Sender: TObject);
  private
    ImportFile: TStringList;
    ProducerID: Integer;
    InvoiceID: Integer;
    function GetProducerID: Integer;
    function ImportInvoiceHeader: Boolean;
    function ImportInvoiceItems: Boolean;
    function InitFile: Boolean;
  public
    FileName: AnsiString;
    function Import(const AFileName: AnsiString): Boolean;
  end;

var
  dmImportData: TdmImportData;

implementation

{$R *.dfm}

uses DictData, MainData;

function TdmImportData.InitFile: Boolean;
begin

  Result := true;

  try
    ImportFile.LoadFromFile(FileName);
  except
    Result := false;
    FreeAndNil(ImportFile);
  end;

end;


procedure TdmImportData.InvoiceBeforeExecute(Sender: TObject);
begin

  with Invoice.Params do
  begin
    ByName['DepID'].AsInteger := dm.CurrDep;
    ByName['FromDepID'].AsInteger := dm.CurrFromDep;
    ByName['UserId'].AsInteger := dm.User.UserId;
    ByName['ProducerID'].AsInteger := ProducerID;
  end;

end;

function TdmImportData.Import(const AFileName: AnsiString): Boolean;
begin

  Result := false;

  FileName := AFileName;

  if not InitFile then
  begin
    DialogErrorOkMessage('������ ��� �������� �����: ' + FileName);
    Exit;
  end;

  ProducerID := GetProducerID;

  if ProducerID > 0 then
  begin

    if ImportInvoiceHeader and ImportInvoiceItems then
    begin

      Result := true;

    end;

  end;

end;


function TdmImportData.ImportInvoiceHeader: Boolean;
var
  Params: String;
  NumberRow, NumberPosition: Integer;
  DateRow, DatePosition: Integer;
  InvoiceDate: TDateTime;
  InvoiceNumber: Integer;
  Delimiter: AnsiChar;
begin

  Result := true;

  Params := ProducersHEADERPARAMS.AsString;

  Delimiter := ProducersHEADERDELIMITER.AsString[1];

  Result := Result and TryStrToInt(ExtractWord(1, Params, [DefaultDelimiter]), NumberRow);
  Result := Result and TryStrToInt(ExtractWord(2, Params, [DefaultDelimiter]), NumberPosition);
  Result := Result and TryStrToInt(ExtractWord(3, Params, [DefaultDelimiter]), DateRow);
  Result := Result and TryStrToInt(ExtractWord(4, Params, [DefaultDelimiter]), DatePosition);

  Result := Result and TryStrToInt(ExtractWord(NumberPosition, DelSpace1(ImportFile[NumberRow]), [Delimiter]), InvoiceNumber);
  Result := Result and TryStrToDateTime(ExtractWord(DatePosition, DelSpace1(ImportFile[DateRow]), [Delimiter]), InvoiceDate);

  if Result then
  begin

    dmMain.taASList.Append;

    dmMain.taASListPRODUCERID.AsInteger := ProducerID;

    dmMain.taASList.Post;
{
  taASListINVID.AsInteger := dm.GetId(12);
  taASListDOCNO.AsInteger := dm.GetId(20);
  taASListDOCDATE.AsDateTime := dm.ServerDateTime;
  taASListUSERID.AsInteger := dm.User.UserId;
  taASListFIO.AsString := dm.User.FIO;
  taASListShipedBy.AsInteger := dm.User.UserId;
  taASListNDSID.AsInteger := -1;
  taASListISCLOSE.AsInteger := 0;
  taASListITYPE.AsInteger := 4;
  taASListDEPID.AsInteger := dm.CurrDep;
  taASListFROMDEPID.AsInteger := dm.CurrFromDep;
  taASListCONTRACTID.AsInteger := dm.Rec.DefaultProdContractId;
  taASListINVCOLORID.AsVariant := Null;
}
    InvoiceID := dmMain.taASListINVID.AsInteger;

    Result := Result and dm.ExecuteSQL(SQLUpdateOuterDocParams, VarArrayOf([InvoiceNumber, InvoiceDate, InvoiceID]));

  end;

end;

function TdmImportData.ImportInvoiceItems: Boolean;
var
  Params: String;
  Delimiter: AnsiChar;
  Item: TInvoiceItemPositions;
  StartRowNumber: Integer;
  EndRow: String;
  i: Integer;
  Row: String;
  AWord: String;
begin

  Result := true;

  Params := ProducersITEMPARAMS.AsString;

  Delimiter := ProducersITEMDELIMITER.AsString[1];

  Result := Result and tryStrToInt(ExtractWord(1, Params, [DefaultDelimiter]), StartRowNumber);

  EndRow := ExtractWord(2, Params, [DefaultDelimiter]);

  Result := Result and tryStrToInt(ExtractWord(3, Params, [DefaultDelimiter]), Item.ArticleWord);
  Result := Result and tryStrToInt(ExtractWord(4, Params, [DefaultDelimiter]), Item.SizeWord);
  Result := Result and tryStrToInt(ExtractWord(5, Params, [DefaultDelimiter]), Item.UnitNameWord);
  Result := Result and tryStrToInt(ExtractWord(6, Params, [DefaultDelimiter]), Item.QuantityWord);
  Result := Result and tryStrToInt(ExtractWord(7, Params, [DefaultDelimiter]), Item.WeightWord);
  Result := Result and tryStrToInt(ExtractWord(8, Params, [DefaultDelimiter]), Item.PriceWord);
  Result := Result and tryStrToInt(ExtractWord(9, Params, [DefaultDelimiter]), Item.TaxWord);

  if Result then
  begin

    i := StartRowNumber;

    repeat

      Row := DelSpace1(ImportFile[i]);

      AWord := DelSpace(ExtractWord(Item.ArticleWord, Row, [Delimiter]));

    until (CompareText(Row, EndRow) <> 0);

  end;


end;

procedure TdmImportData.DataModuleCreate(Sender: TObject);
begin
  ImportFile := TStringList.Create;
end;

procedure TdmImportData.DataModuleDestroy(Sender: TObject);
begin
  FreeAndNil(ImportFile);
end;

function TdmImportData.GetProducerID: Integer;
var
  Keyword: AnsiString;
begin

  Result := -1;

  if Producers.Active then Producers.First;

  while not Producers.Eof do
  begin

    Keyword := ProducersKEYWORD.AsString;

    if ContainsText(FileName, Keyword) then
    begin
      Result := ProducersCOMPANYID.AsInteger;
      break;
    end;

  end;

end;

end.
