unit dArtRoot;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DictAncestor, ActnList, RxPlacemnt, ImgList, RxSpeedBar, Grids,
  DBGridEh, Buttons, ExtCtrls, ComCtrls, DBGridEhGrouping, GridsEh;

type
  TfmdArtRoot = class(TfmDictAncestor)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmdArtRoot: TfmdArtRoot;

implementation

uses DictData;

{$R *.dfm}

end.
